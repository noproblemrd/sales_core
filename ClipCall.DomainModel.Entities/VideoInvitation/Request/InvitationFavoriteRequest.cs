﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Request
{
    public class InvitationFavoriteRequest : VideoChatRequestBase
    {
        public Guid ExpertiseId { get; set; }
    }
}
