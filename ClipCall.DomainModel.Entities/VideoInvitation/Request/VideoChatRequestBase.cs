﻿using System;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Request
{
    public class VideoChatRequestBase
    {
        
        public Guid invitationId { get; set; }
    }
}
