﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Request
{
    public class VideoChatSupplierBaseRequest : VideoChatRequestBase
    {
        public Guid SupplierId { get; set; }
    }
}
