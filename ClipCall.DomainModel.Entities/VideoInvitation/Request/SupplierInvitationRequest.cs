﻿using System;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Request
{
    public class SupplierInvitationRequest:VideoChatRequestBase
    {
        public Guid SupplierId { get; set; }
    }
}
