﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Request
{
    public class OpentokArchiveStatusChangesRequest
    {
        public Guid id { get; set; }
        public string @event { get; set; }
        public long createdAt { get; set; }
        public long duration { get; set; }
        public string name { get; set; }
        public int partnerId { get; set; }
        public string reason { get; set; }
        public string sessionId { get; set; }
        public int size { get; set; }
        public string status { get; set; }
        public string url { get; set; }
    }
}
/*
 * long 	CreatedAt [get, set] 
long 	Duration [get, set] 
Guid 	Id [get, set] 
string 	Name [get, set] 
int 	PartnerId [get, set] 
String 	SessionId [get, set] 
String 	Reason [get, set] 
bool 	HasVideo [get, set] 
bool 	HasAudio [get, set] 
OutputMode 	OutputMode [get, set] 
int 	Size [get, set] 
ArchiveStatus 	Status [get, set] 
String 	Url [get, set]
 * 
 * 
 ArchiveStatus 	Status:
 AVAILABLE 	
The archive file is available for download from the OpenTok cloud. You can get the URL of the download file by getting the Url property of the Archive object.
DELETED 	
The archive file has been deleted.
FAILED 	
The recording of the archive failed.
PAUSED 	
The archive is in progress and no clients are publishing streams to the session. When an archive is in progress and any client publishes a stream, the status is STARTED. When an archive is PAUSED, nothing is recorded. When a client starts publishing a stream, the recording starts (or resumes). If all clients disconnect from a session that is being archived, the status changes to PAUSED, and after 60 seconds the archive recording stops (and the status changes to STOPPED).
STARTED 	
The archive recording has started and is in progress.
STOPPED 	
The archive recording has stopped, but the file is not available.
UPLOADED 	
The archive file is available at the target Amazon S3 bucket or Windows Azure container you set at the OpenTok dashboard.
EXPIRED 	
The archive file is no longer available for download from the OpenTok cloud.
UNKOWN 
 */
