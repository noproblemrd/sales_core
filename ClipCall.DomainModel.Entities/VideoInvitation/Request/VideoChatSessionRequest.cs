﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Request
{
    public class VideoChatSessionRequest:VideoChatRequestBase
    {
        public string SessionId { get; set; }
    }
}
