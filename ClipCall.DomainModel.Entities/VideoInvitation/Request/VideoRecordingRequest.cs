﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Request
{
    public class VideoRecordingRequest:VideoChatRequestBase
    {
        public string RecordingId { get; set; }

    }
}
