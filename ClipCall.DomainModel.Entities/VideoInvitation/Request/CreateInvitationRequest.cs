﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Request
{
    public class CreateInvitationRequest
    {
        public Guid SupplierId { get; set; }
        public eInvitationType InvitationType { get; set; }

        public enum eInvitationType
        {
            VIDEO_CHAT = 1,
            VIDEO = 2
        }
    }
}
