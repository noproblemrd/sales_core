﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Response
{
    public class CreateVideoChatConversationResponse : VideoSession
    {
        public eCreateVideoChatConversationStatus status { get; set; }
        
        public enum eCreateVideoChatConversationStatus
        {
            SUCCESS,
            FAILED_CREATE_SESSION,
            FAILED_PUSH_ADVERTISER
        }
    }
}
