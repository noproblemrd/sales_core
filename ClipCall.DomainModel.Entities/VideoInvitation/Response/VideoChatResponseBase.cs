﻿namespace ClipCall.DomainModel.Entities.VideoInvitation.Response
{
    public class VideoChatResponseBase
    {
        public string SessionId { get; set; }
    }
}
