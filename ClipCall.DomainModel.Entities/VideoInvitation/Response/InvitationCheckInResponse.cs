﻿using ClipCall.DomainModel.Entities.VideoInvitation.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Response
{
    public class InvitationCheckInResponse //: VideoSession
    {
        public string supplierName { get; set; }
        public string text { get; set; }
        public string supplierImage { get; set; }
        public CreateInvitationRequest.eInvitationType invitationType { get; set; }
        public eInvitationCheckInStatus checkInStatus { get; set; }
        public Guid categoryId { get; set; }
        public string categoryName { get; set; }

        public enum eInvitationCheckInStatus
        {
            success,
            notAvailable,
            failed,
            failedToFindSupplier
        }
    }
}
