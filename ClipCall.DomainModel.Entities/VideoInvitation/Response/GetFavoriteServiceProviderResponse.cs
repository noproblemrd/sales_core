﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Response
{
    public class GetFavoriteServiceProviderResponse
    {
        public string AccountName { get; set; }
        public Guid AccountId { get; set; }
        public string ImageUrl { get; set; }
    }
}
