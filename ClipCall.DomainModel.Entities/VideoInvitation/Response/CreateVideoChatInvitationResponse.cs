﻿using ClipCall.DomainModel.Entities.VideoInvitation.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoInvitation.Response
{
    public class CreateVideoChatInvitationResponse
    {
        public string message { get; set; }
        public Guid invitationId { get; set; }
        public CreateInvitationRequest.eInvitationType invitationType { get; set; }
    }
}
