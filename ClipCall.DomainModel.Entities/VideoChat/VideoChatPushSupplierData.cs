﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoChat
{
    public class VideoChatPushSupplierData
    {
        public Guid supplierId { get; set; }
        public string customerPhone { get; set; }
        public string customerImage { get; set; }
    }
}
