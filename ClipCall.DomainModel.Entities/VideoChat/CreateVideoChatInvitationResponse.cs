﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoChat
{
    public class CreateVideoChatInvitationResponse
    {
        public string message { get; set; }
        public Guid invitationId { get; set; }
        
    }
}
