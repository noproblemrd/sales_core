﻿using System;

namespace ClipCall.DomainModel.Entities.VideoChat
{
    public class VideoRecording
    {
        public long CreatedAt { get; set; }

        /// The duration of the archive, in milliseconds.
        public long Duration { get; set; }

        /// The archive ID.
        public Guid Id { get; set; }

        /// The name of the archive.
        public string Name { get; set; }

        /// The OpenTok API key associated with the archive.
        public int PartnerId { get; set; }

        /// The session ID of the OpenTok session associated with this archive.
        public string SessionId { get; set; }

        /// For archives with the status "stopped", this can be set to "90 mins exceeded",
        ///             "failure", "session ended", or "user initiated". For archives with the status "failed",
        ///             this can be set to "system failure".
        public string Reason { get; set; }

        /// Whether the archive includes a video track (true) or not (false).
        public bool HasVideo { get; set; }

        /// Whether the archive includes an audio track (true) or not (false).
        public bool HasAudio { get; set; }

        /// The size of the MP4 file. For archives that have not been generated, this value is set
        ///             to 0.
        public int Size { get; set; }

        public string Url { get; set; }
    }
}
