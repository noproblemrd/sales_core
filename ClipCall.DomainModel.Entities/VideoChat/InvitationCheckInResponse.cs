﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClipCall.DomainModel.Entities.VideoChat
{
    public class InvitationCheckInResponse //: VideoSession
    {
        public string supplierName { get; set; }
        public string text { get; set; }
        public string supplierImage { get; set; }
        public eInvitationCheckInStatus checkInStatus { get; set; }

        public enum eInvitationCheckInStatus
        {
            success,
            notAvailable,
            failed,
            failedToFindSupplier
        }
    }
}
