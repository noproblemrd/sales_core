﻿namespace ClipCall.DomainModel.Entities.VideoChat
{
    public class VideoSession
    {
        public int ApiKey { get; set; }
        public string SessionId { get; set; }
        public string Token { get; set; }
        /*
        public static explicit operator InvitationCheckInResponse(VideoSession vs)
        {
            return new InvitationCheckInResponse()
            {
                ApiKey = vs.ApiKey,
                SessionId = vs.SessionId,
                Token = vs.Token
            };
        }
         
        public InvitationCheckInResponse GetInvitationCheckInResponse()
        {
            return new InvitationCheckInResponse()
            {
                ApiKey = this.ApiKey,
                SessionId = this.SessionId,
                Token = this.Token
            };
        }
         * * */
    }
}
