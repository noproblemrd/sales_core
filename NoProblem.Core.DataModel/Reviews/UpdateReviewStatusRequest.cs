﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reviews
{
    public class UpdateReviewStatusRequest
    {
        public Guid ReviewId { get; set; }
        public Xrm.new_review.ReviewStatus ReviewStatus { get; set; }
    }
}
