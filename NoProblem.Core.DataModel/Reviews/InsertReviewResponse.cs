﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reviews
{
    public class InsertReviewResponse
    {
        public InsertReviewStatus InsertionStatus { get; set; }
    }
}
