﻿
namespace NoProblem.Core.DataModel.Reviews
{
    public enum InsertReviewStatus
    {
        OK,
        NoCaseFound,
        AlreadyExists,
        MissingParams
    }
}