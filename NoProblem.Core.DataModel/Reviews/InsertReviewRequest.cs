﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reviews
{
    public class InsertReviewRequest
    {
        public string Description { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public bool IsLike { get; set; }
        public Guid SupplierId { get; set; }
    }
}
