﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reviews
{
    public class GetReviewsRequest
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public Xrm.new_review.ReviewStatus? ReviewStatus { get; set; }
        public Guid? SupplierId { get; set; }
    }
}
