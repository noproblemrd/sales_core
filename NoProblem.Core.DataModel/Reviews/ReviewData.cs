﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reviews
{
    public class ReviewData
    {
        public Guid ReviewId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Phone { get; set; }
        public Xrm.new_review.ReviewStatus ReviewStatus { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public Guid IncidentId { get; set; }
        public string IncidentNumber { get; set; }
        public string Description { get; set; }
        public bool IsLike { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CostumerId { get; set; }
    }
}
