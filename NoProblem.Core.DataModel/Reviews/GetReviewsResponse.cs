﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reviews
{
    public class GetReviewsResponse
    {
        public List<ReviewData> Reviews { get; set; }
    }
}
