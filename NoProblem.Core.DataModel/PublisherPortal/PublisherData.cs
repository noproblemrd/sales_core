﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.PublisherPortal.PublisherData
//  File: PublisherData.cs
//  Description: Container that represents a single publisher.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class PublisherData
    {
        public string AccountNumber { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public int SecurityLevel { get; set; }
        public Guid AccountId { get; set; }
        public bool CanHearAllRecordings { get; set; }
        public string BizId { get; set; }
    }
}
