﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetAllPricingPackagesResponse
    {
        public List<PricingPackageData> Packages { get; set; }
    }
}
