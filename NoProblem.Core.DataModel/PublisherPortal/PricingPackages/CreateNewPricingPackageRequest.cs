﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class CreateNewPricingPackageRequest
    {
        public string Name { get; set; }
        public int FromAmount { get; set; }
        public int BonusPercent { get; set; }
        public Xrm.new_pricingpackage.PricingPackageSupplierType SupplierType { get; set; }
    }
}
