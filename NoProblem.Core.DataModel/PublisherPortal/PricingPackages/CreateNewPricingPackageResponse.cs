﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class CreateNewPricingPackageResponse
    {
        public Guid PricingPackageId { get; set; }
    }
}
