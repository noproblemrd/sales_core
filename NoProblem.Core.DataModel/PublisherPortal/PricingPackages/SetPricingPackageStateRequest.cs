﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class SetPricingPackageStateRequest
    {
        public Guid PricingPackageId { get; set; }
        public bool IsActive { get; set; }
    }
}
