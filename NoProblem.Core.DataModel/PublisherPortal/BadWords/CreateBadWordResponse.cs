﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal.BadWords
{
    public class CreateBadWordResponse
    {
        public CreateBadWordStatus Status { get; set; }
        public Guid BadWordId { get; set; }
    }

    public enum CreateBadWordStatus
    {
        OK,
        WordAlreadyExists,
        WordIsNullOrEmpty
    }
}
