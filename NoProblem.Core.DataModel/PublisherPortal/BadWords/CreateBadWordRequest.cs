﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal.BadWords
{
    public class CreateBadWordRequest
    {
        public string Word { get; set; }
    }
}
