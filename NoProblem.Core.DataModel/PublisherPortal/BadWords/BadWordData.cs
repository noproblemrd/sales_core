﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal.BadWords
{
    public class BadWordData
    {
        public Guid BadWordId { get; set; }
        public string Word { get; set; }
    }
}
