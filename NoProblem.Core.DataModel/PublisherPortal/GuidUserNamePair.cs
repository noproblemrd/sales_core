﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GuidUserNamePair
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
    }
}
