﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.PublisherPortal.SetBlackListStatusRequest
//  File: SetBlackListStatusRequest.cs
//  Description: Container to use as request for the SetBalckListStatus service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class SetBlackListStatusRequest
    {
        public string PhoneNumber { get; set; }
        public bool BlackListStatusToSet { get; set; }
        public Guid IncidentId { get; set; }
    }
}
