﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.PublisherPortal.UserLoginResponse
//  File: UserLoginResponse.cs
//  Description: Container to use as response for the UserLogin service.
//                  Enum eUserLoginStatus are the posible answers.                
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public enum eUserLoginStatus
    {
        OK,
        NotFound,
        UserInactive,
        BadPassword,
        RegistrationNotFinishedStart,
        RegistrationNotFinishedHeading,
        RegistrationNotFinishedMap,
        WaitingForInvitation
    }

    public class UserLoginResponse
    {
        public Guid UserId { get; set; }

        public string Name { get; set; }

        public Decimal Balance { get; set; }

        public int SecurityLevel { get; set; }

        public eUserLoginStatus UserLoginStatus { get; set; }

        public string Site { get; set; }

        public int StageInRegistration { get; set; }

        public Guid AffiliateOrigin { get; set; }//if user is affiliate, we give his origin here.

        public bool CanListenToRecords { get; set; }
    }
}
