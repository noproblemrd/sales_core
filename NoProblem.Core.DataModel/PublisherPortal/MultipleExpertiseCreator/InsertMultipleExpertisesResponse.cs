﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal.MultipleExpertiseCreator
{
    public class InsertMultipleExpertisesResponse
    {
        public List<ExpertiseResponseData> ResponseData { get; set; }
    }
}
