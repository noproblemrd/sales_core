﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal.MultipleExpertiseCreator
{
    public class ExpertiseResponseData
    {
        public int LineInExcel { get; set; }
        public bool SavedOK { get; set; }
        public string FailureReason { get; set; }
    }
}
