﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal.MultipleExpertiseCreator
{
    public class PrimaryExpertiseData
    {
        public int LineInExcel { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool? IsAuction { get; set; }
        public bool? ShowCertificate { get; set; }
        public decimal? MinimalPrice { get; set; }
    }
}
