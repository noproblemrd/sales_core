﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.SearchConsumersResponse
//  File: SearchConsumersResponse.cs
//  Description: Container to use as response for the SearchConsumers service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class SearchConsumersResponse
    {
        public List<ConsumerMinimalData> Consumers { get; set; }
    }
}
