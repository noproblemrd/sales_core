﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.GetConsumerIncidentDataResponse
//  File: GetConsumerIncidentDataResponse.cs
//  Description: Container to use as response for the GetConsumerIncidentData service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using NoProblem.Core.DataModel.Case;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetConsumerIncidentDataResponse
    {
        public List<ConsumerIncidentAccountData> IncidentAccounts { get; set; }
    }

    public class RelaunchLeadResponse
    {
        public bool IsSuccess { get; set; }
        public NoProblem.Core.DataModel.ClipCall.Request.eRequestsStatus requestStatus { get; set; }
    }

    public class UpdateCustomerProfileResponse
    {
        
    }

    public class CloseLeadResponse
    {
        public bool IsSuccess { get; set; }
        
    }

    public class GetCustomerAdvertiserAboutResponse
    {
        public AdvertiserAboutData AboutData { get; set; }
    }

    public class GetAdvertiserCallHistoryResponse
    {
        public List<CallRecordData> CallRecords { get; set; }
    }
}
