﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.GetConsumerNotesResponse
//  File: GetConsumerNotesResponse.cs
//  Description: Container to use as response for the GetConsumerNotes service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetConsumerNotesResponse
    {
        public List<ConsumerNoteData> Notes { get; set; }
    }
}
