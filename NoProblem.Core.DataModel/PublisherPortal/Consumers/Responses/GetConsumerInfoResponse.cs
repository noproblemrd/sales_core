﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.GetConsumerInfoResponse
//  File: GetConsumerInfoResponse.cs
//  Description: Container to use as response for the GetConsumerInfo service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetConsumerInfoResponse
    {
        public string ConsumerNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string PhoneNumber1 { get; set; }
        public string Email { get; set; }
        public int NumberOfRequests { get; set; }
        public bool IsBlackList { get; set; }
    }
}
