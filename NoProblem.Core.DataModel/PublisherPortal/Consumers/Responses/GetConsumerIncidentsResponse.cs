﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.GetConsumerIncidentsResponse
//  File: GetConsumerIncidentsResponse.cs
//  Description: Container to use as response for the GetConsumerIncidents service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetConsumerIncidentsResponse
    {
        public List<ConsumerIncidentData> ConsumerIncidents { get; set; }
    }

    public class GetCustomerProfileResponse
    {
        public CustomerProfileData Profile { get; set; }
    }

    public class GetCustomerLeadResponse
    {
        public ConsumerIncidentData Lead { get; set; }
    }

    public class GetIncidentSuppliersResponse
    {
        
    }
}
