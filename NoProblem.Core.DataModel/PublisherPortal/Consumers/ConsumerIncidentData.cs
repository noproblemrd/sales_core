﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.ConsumerIncidentData
//  File: ConsumerIncidentData.cs
//  Description: Container that represents an incident. Used to retrieve data about consumer's cases.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class ConsumerIncidentData
    {
        public Guid IncidentId { get; set; }
        public string Description { get; set; }
        public int NumberOfSuppliersContacted { get; set; }
        public string IncidentNumber { get; set; }
        public string ExpertiseName { get; set; }
        public string RegionName { get; set; }
        public SupplierAvailabilityStatus SupplierAvailabilityStatus { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? OrderedProviders { get; set; }
        public bool CanReviveIncident { get; set; }
        public string VideoUrl { get; set; }
        public string PreviewVideoImageUrl { get; set; }
        public int? VideoDuration { get; set; }
        public eAvailableAction AvailableAction { get; set; }
        public int IncidentStatus { get; set; }
        public int ReviewStatus { get; set; }
    }

     public enum SupplierAvailabilityStatus     
     {
         AVAILABLE_SUPPLIERS = 1,
         NO_AVAILABLE_SUPPLIERS = 2,
         LOCATING_SUPPLIER = 3
     }

    public enum eAvailableAction
    {
        Relaunch = 1,
        NoMoreCalls = 2,
        None = 0
    }


    public class CustomerProfileData
    {
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ImageUrl { get; set; }
    }
}
