﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.ConsumerNoteData
//  File: ConsumerNoteData.cs
//  Description: Container that represents a consumer's note. Used to hold data about a consumer's note.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class ConsumerNoteData
    {
        public string Description { get; set; }
        public Guid NoteId { get; set; }

        /// <summary>
        /// The GUID of the publisher user (account) that created the note.
        /// </summary>
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
