﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.SearchConsumersRequest
//  File: SearchConsumersRequest.cs
//  Description: Container to use as request for the SearchConsumers service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class SearchConsumersRequest
    {
        public string SearchParameter { get; set; }
    }
}
