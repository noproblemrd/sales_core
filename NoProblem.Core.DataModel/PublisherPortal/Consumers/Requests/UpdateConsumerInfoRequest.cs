﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.UpdateConsumerInfoRequest
//  File: UpdateConsumerInfoRequest.cs
//  Description: Container to use as request for the UpdateConsumerInfo service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class UpdateConsumerInfoRequest
    {
        public Guid ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string Email { get; set; }
        public bool IsBlackList { get; set; }
    }
}
