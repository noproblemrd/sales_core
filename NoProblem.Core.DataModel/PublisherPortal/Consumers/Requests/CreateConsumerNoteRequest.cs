﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.CreateConsumerNoteRequest
//  File: CreateConsumerNoteRequest.cs
//  Description: Container to use as request for the CreateConsumerNote service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class CreateConsumerNoteRequest
    {
        public string Description { get; set; }
        public Guid UserId { get; set; }
        public Guid ContactId { get; set; }
    }
}
