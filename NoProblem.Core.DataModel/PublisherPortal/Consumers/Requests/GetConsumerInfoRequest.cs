﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.GetConsumerInfoRequest
//  File: GetConsumerInfoRequest.cs
//  Description: Container to use as request for the GetConsumerInfo service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetConsumerInfoRequest
    {
        public Guid ContactId { get; set; }
    }
}
