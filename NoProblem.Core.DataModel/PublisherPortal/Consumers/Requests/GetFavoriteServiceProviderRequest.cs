﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal.Consumers.Requests
{
    public class GetFavoriteServiceProviderRequest
    {
        public Guid CustomerId { get; set; }
        public Guid ExpertiseId { get; set; }
    }
}
