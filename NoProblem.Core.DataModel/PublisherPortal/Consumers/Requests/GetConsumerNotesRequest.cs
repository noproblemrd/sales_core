﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.GetConsumerNotesRequest
//  File: GetConsumerNotesRequest.cs
//  Description: Container to use as request for the GetConsumerNotes service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetConsumerNotesRequest
    {
        public Guid ContactId { get; set; }
    }
}
