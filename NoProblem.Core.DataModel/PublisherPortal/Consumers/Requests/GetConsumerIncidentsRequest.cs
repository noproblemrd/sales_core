﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.GetConsumerIncidentsRequest
//  File: GetConsumerIncidentsRequest.cs
//  Description: Container to use as request for the GetConsumerIncident service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class RelaunchLeadRequest
    {
        public Guid CustomerId { get; set; }
        public Guid LeadId { get; set; }
    }

    public class CloseLeadRequest
    {
        public Guid CustomerId { get; set; }
        public Guid LeadId { get; set; }
    }

    public class GetCustomerAdvertiserAboutRequest
    {
        public Guid CustomerId { get; set; }
        public Guid AdvertiserId { get; set; }
    }

    public class UpdateCustomerProfileRequest
    {
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ImageUrl { get; set; }
    }

    public class CustomerSettingsData
    {
        public bool RecordMyCalls { get; set; }
    }

    public class UpdateCustomerSettingsRequest
    {
        public Guid CustomerId { get; set; }
        public bool RecordMyCalls { get; set; }
    }

    public class GetAdvertiserCallHistoryRequest
    {
        public Guid LeadAccountId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid AdvertiserId { get; set; }
    }

    public class GetLeadSuppliersRequest
    {
        public Guid CustomerId { get; set; }
        public Guid LeadId { get; set; }
    }


    public class GetCustomerLeadRequest
    {
        public Guid LeadId { get; set; }
        public Guid CustomerId { get; set; }
    }
    public class GetCustomerLeadsRequest
    {
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public Guid CustomerId { get; set; }
        public LeadStatus LeadStatus { get; set; }
    }

    public enum LeadStatus
    {
        Any,
        Live,
        Dead
    }
}
