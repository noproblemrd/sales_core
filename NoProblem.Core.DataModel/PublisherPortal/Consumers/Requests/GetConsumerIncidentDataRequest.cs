﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.GetConsumerIncidentDataRequest
//  File: GetConsumerIncidentDataRequest.cs
//  Description: Container to use as request for the GetConsumerIncidentData service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetConsumerIncidentDataRequest
    {
        public Guid LeadId { get; set; }
        public Guid CustomerId { get; set; }
    }
}
