﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.ConsumerIncidentData
//  File: ConsumerIncidentData.cs
//  Description: Container that represents a consumer. Used to hold basic data about a consumer.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class ConsumerMinimalData
    {
        public Guid ContactId { get; set; }
        public string ConsumerNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public int NumberOfRequests { get; set; }
    }
}
