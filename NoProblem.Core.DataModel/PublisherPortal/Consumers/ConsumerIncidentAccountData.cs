﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.PublisherPortal.ConsumerIncidentAccountData
//  File: ConsumerIncidentAccountData.cs
//  Description: Container that represents a new_incidentaccount. Used to retrieve data about consumer's calls.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class ConsumerIncidentAccountData
    {
        public string RecordFileLocation { get; set; }
        public bool AllowedToHear { get; set; }
        public string SupplierName { get; set; }
        public Guid SupplierId { get; set; }
        public Guid LeadAccountId { get; set; }
        public double? GoogleRating { get; set; }
        public double? YelpRating { get; set; }
        public int MyLove { get; set; }
        public int AllLove { get; set; }
        public string ImageUrl { get; set; }
        public string GoogleUrl { get; set; }
        public string YelpUrl { get; set; }
    }
}
