﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.PublisherPortal.GetAllPublishersResponse
//  File: GetAllPublishersResponse.cs
//  Description: Container to use as response for the GetAllPublishers service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetAllPublishersResponse
    {
        public List<PublisherData> Publishers { get; set; }
    }
}
