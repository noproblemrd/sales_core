﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class OriginMinDataHolder
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public bool IsUpsaleOrigin { get; set; }
        public bool IsAutoUpsaleOrigin { get; set; }
        public Guid ParentOriginId { get; set; }
    }
}
