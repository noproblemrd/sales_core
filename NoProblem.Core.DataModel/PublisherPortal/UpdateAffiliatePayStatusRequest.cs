﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class UpdateAffiliatePayStatusRequest
    {
        public Guid PayStatusId { get; set; }
        public Guid PayReasonId { get; set; }
        public Guid CaseId { get; set; }
    }
}
