﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class StringBoolPair : IEquatable<StringBoolPair>
    {
        public StringBoolPair() { }
        public StringBoolPair(string name, bool IsTrue)
        {
            this.IsTrue = IsTrue;
            this.Name = name;
        }

        public bool IsTrue { get; set; }
        public string Name { get; set; }

        public bool Equals(StringBoolPair other)
        {
            return (Name == other.Name);
        }
    }
}
