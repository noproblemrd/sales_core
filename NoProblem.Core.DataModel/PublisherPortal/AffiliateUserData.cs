﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class AffiliateUserData
    {
        public string AccountNumber { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public Guid AccountId { get; set; }
        public string OriginName { get; set; }
        public Guid OriginId { get; set; }
        public string ContactName { get; set; }
    }
}
