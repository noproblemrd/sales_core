﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetSuppliersDirectNumbersResponse
    {
        public List<DirectNumberData> DirectNumberCollection { get; set; }
    }
}
