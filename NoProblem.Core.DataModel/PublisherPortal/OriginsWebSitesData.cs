﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class OriginsWebSitesData
    {
        public string OriginName { get; set; }
        public Guid OriginId { get; set; }
        public List<GuidStringPair> WebSites { get; set; }

        public override bool Equals(object obj)
        {
            OriginsWebSitesData other = (OriginsWebSitesData)obj;
            return this.OriginId == other.OriginId;
        }

        public override int GetHashCode()
        {
            return OriginId.GetHashCode();
        }
    }
}
