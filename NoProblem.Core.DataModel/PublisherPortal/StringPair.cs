﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class StringPair
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
