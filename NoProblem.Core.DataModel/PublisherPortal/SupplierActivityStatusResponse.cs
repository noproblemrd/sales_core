﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public class SupplierActivityStatusResponse
    {
        public List<Xrm.account.SupplierStatus> SupplierAvailabilityStatus { get; set; }
    }
}
