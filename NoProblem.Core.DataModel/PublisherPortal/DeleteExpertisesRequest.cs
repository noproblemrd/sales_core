﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class DeleteExpertisesRequest
    {
        public List<Guid> ExpertiseIds { get; set; }
    }
}
