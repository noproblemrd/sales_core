﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class DirectNumberData
    {
        public string ExpertiseName { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string OriginName { get; set; }
        public string Number { get; set; }
    }
}
