﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class GetUserNamesRequest
    {
        public List<Guid> Guids { get; set; }
    }
}
