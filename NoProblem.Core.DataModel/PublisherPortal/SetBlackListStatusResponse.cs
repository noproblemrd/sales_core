﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.PublisherPortal
{
    public class SetBlackListStatusResponse
    {
        public BlackListStatus Status { get; set; }
    }

    public enum BlackListStatus
    {
        OK,
        NoChange
    }
}
