﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Injection
{
    public class InjectionCampaign
    {
        public Guid InjectionId { get; set; }
        public Guid OriginId { get; set; }
        public int DayCounter { get; set; }
        public int HitCounter { get; set; }
    }
}
