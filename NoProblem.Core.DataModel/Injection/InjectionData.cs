﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Injection
{
    public class InjectionData
    {
        public Guid InjectionId { get; set; }
        public string Name { get; set; }
        public string ScriptBefore { get; set; }
        public string ScriptUrl { get; set; }
        public bool BlockWithUs { get; set; }
        public string ScriptElementId { get; set; }
    }
}
