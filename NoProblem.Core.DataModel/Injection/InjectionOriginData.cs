﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Injection
{
    public class InjectionOriginData
    {
        public Guid InjectionId { get; set; }
        public Guid OriginId { get; set; }
        public int HitCounter { get; set; }
        public int DayCounter { get; set; }
        public string InjectionName { get; set; }
        public string OriginName { get; set; }
    }
}
