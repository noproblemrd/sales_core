﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Injection
{
    public class InjectionCampaignParasite
    {
        public Guid InjectionId { get; set; }
        public Guid OriginId { get; set; }
        public int WaitHits { get; set; }
        public int HourInterval { get; set; }
        public bool IsDefault { get; set; }
    }
}
