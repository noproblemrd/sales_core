﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.OnecallUtilities.LoadAdvertisersRequest
//  File: LoadAdvertisersRequest.cs
//  Description: Container to use as request for the LoadAvertisers service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.OnecallUtilities
{
    public enum eAdvertisersType
    {
        Auction,
        DirectNumbers
    }

    public class LoadAdvertisersRequest
    {
        public eAdvertisersType AdvertisersType { get; set; }

        public int ExpertiseIndex { get; set; }

        /// <summary>
        /// The number of advertisers that will be created in the system.
        /// </summary>
        public int NumberOfAdvertisersToCreate { get; set; }

        /// <summary>
        /// The way all advertiser's names will begin.
        /// </summary>
        public string NamesPrefix { get; set; }

        /// <summary>
        /// The first number to use as ending in the advertiser's names. +1 will be added to each name.
        /// </summary>
        public int FirstIndexForNames { get; set; }

        /// <summary>
        /// The first true phone number to give the advertisers. +1 will be added to each phone.
        /// </summary>
        public string TruePhoneNumbersStart { get; set; }

        /// <summary>
        /// The first virtual phone number to give the advetisers. +1 will be added to each phone.
        /// </summary>
        public string VirtualNumbersStart { get; set; }
    }
}
