﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.OnecallUtilities.LoadAdvertisersResponse
//  File: LoadAdvertisersResponse.cs
//  Description: Container to use as response for the LoadAvertisers service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace NoProblem.Core.DataModel.OnecallUtilities
{
    public class LoadAdvertisersResponse
    {
    }
}
