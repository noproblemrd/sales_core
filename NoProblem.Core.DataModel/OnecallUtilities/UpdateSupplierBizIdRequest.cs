﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.OnecallUtilities
{
    public class UpdateSupplierBizIdRequest
    {
        public string SiteId { get; set; }
        public Guid NP_Id { get; set; }
        public string Biz_Id { get; set; }
    }
}
