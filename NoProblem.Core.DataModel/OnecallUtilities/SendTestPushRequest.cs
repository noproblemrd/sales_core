﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.OnecallUtilities
{
    public class SendTestPushRequest
    {
        public string DeviceId { get; set; }
        public string PushType { get; set; }
        public List<PushDataPair> Data { get; set; }

        public class PushDataPair
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }
        /*
                { "id", ia.new_incidentaccountid.ToString() },
                { "description", incident.description },
                { "max_bid", maxPrice.ToString() },
                { "min_bid", minPrice.ToString() },
                { "category", incident.new_new_primaryexpertise_incident.new_name }
        */
    }
}
