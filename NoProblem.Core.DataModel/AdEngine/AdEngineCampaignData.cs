﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdEngine
{
    public class AdEngineCampaignData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
