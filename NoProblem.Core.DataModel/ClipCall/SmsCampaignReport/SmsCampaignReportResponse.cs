﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.SmsCampaignReport
{
    public class SmsCampaignReportResponse
    {
        public DateTime date { get; set; }
        public int bulkId { get; set; }
        public string campaign { get; set; }
        public int TotalPhonesAttempts { get; set; }
        public int sent { get; set; }
        public int didntTryToSent { get; set; }        
    }
}
