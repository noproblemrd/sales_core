﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;


namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class IncidentData
    {
        public string Title { get; set; }
        public string VideoUrl { get; set; }
        public int ProvidedAdvertisers { get; set; }
        public int RequestedAdvertisers { get; set; }
        public string CategoryName { get; set; }
        public Guid CategoryId { get; set; }
        
        public int RegionLevel { get; set; }
        public string RegionName { get; set; }
        public Guid CustomerId { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
  
        public DateTime CreatedOn { get; set; }
        public DateTime RelaunchDate { get; set; }
        public incident.Status IncidentStatus { get; set; }
        public incident.IncidentReviewStatus ReviewStatus { get; set; }
        public incident.CaseTypeCode caseTypeStatus { get; set; }

        public List<IncidentAccountData> Calls { get; set; }
        public List<AarIncidentData> AarCalls { get; set; }
        public string TicketNumber { get; set; }       
  //      public bool IsStoppedManually { get; set; }     
        public List<RelaunchStopData> RelaunchStopDataList { get; set; }

        //calculate values
        public bool IsLiveIncident { get; private set; }
        public bool CanRelaunch { get; private set; }
        public bool CanBeStopped { get; private set; }

        public void SetValues()
        {
           this.IsLiveIncident = incident.Is_Open(this.IncidentStatus);
           this.CanBeStopped = this.IsLiveIncident && caseTypeStatus != incident.CaseTypeCode.VideoChat;
           this.CanRelaunch = !this.IsLiveIncident && caseTypeStatus != incident.CaseTypeCode.VideoChat;//&& this.ReviewStatus == incident.IncidentReviewStatus.Passed
        }


        public class RelaunchStopData
        {
            public bool IsRelaunch { get; set; }
            public DateTime Date { get; set; }
            public eRelaunchStopBy By { get; set; }
            public string OldRegion { get; set; }
            public string NewRegion { get; set; }
            public string OldCategory { get; set; }
            public string NewCategory { get; set; }
        }
        public enum eRelaunchStopBy
        {
            Customer = 1,
            Publisher = 2,
            System = 3
        }
        /*
        public enum TypeExecuteUser
        {
            Customer = 1,
            Publisher = 2,
            System = 3
        }
         * */
    }
    
}
