﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class RelaunchClipCallIncidentRequest
    {
        public Guid IncidentId { get; set; }
        public Guid expertiseId { get; set; }
        public string regionName { get; set; }
        public int regionLevel { get; set; }
        public string desc { get; set; }
        public int numExper {get;set;}
        public string name{get;set;}
        public string email { get; set; }
    }
    public class RelaunchClipCallIncidentResponse
    {
        public bool IsSuccess { get; set; }
    }
    public class RelaunchChangeValues
    {
        public Guid OldRegionId { get; set; }
        public Guid NewRegionId { get; set; }
        public Guid OldExpertiseId { get; set; }
        public Guid NewExpertiseId { get; set; }
        public RelaunchChangeValues(Guid oldRegionId, Guid newRegionId, Guid oldExpertiseId, Guid newExpertiseId )
        {
            this.OldRegionId = oldRegionId;
            this.NewRegionId = newRegionId;
            this.OldExpertiseId = oldExpertiseId;
            this.NewExpertiseId = newExpertiseId;
        }
    }
    public class RelaunchJob
    {
        public RelaunchChangeValues changeValues { get; set; }
        public Guid IncidentId { get; set; }
    }
}
