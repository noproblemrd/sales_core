﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    
    public enum eRequestsStatus
    {
        ALL,
        LIVE,
        CLOSE,
        REVIEW,
        WAITING_FOR_PHONE_VALIDATION,
        VIDEO_CHAT
    }
}
