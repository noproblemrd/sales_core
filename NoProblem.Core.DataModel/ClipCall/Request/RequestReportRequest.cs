﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class RequestReportRequest
    {
        public DateTime from { get; set; }
        public DateTime to { get; set; }
        public eRequestsStatus requestStatus { get; set; }
        public Guid category { get; set; }
        public string CaseNumber { get; set; }
        public int? numberProvidedAdvertisers { get; set; }
        public bool? aarInitiated { get; set; }
        public string region { get; set; }
        public DateTime GetFrom
        {
            get
            {
                return from;
            }
        }
        public DateTime GetTo
        {
            get
            {
                return to.AddDays(1);
            }
        }
    }
}
