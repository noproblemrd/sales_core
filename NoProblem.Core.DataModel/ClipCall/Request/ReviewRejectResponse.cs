﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class ReviewRejectConfirmResponse
    {
        public bool IsSuccess { get; set; }
        public eMessage message { get; set; }
        public enum eMessage
        {
            OK,
            ALREADY_REVIEWED

        }
    }
}
