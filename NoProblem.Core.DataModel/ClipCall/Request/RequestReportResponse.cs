﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class RequestReportResponse
    {
        public Guid incidentId { get; set; }
        public string caseNumber { get; set; }
        public Guid contactId { get; set; }
        public string contactName { get; set; }
        public string contactPhone { get; set; }
        public string category { get; set; }
        public string region { get; set; }
        public DateTime date { get; set; }
        public int advertisersProvided { get; set; }
        public int requestedAdvertisers { get; set; }
        public decimal? revenue { get; set; }
        public int statuscode { get; set; }
        public eRequestsStatus caseStatus { get; set; }
        public NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus reviewStatus { get; set; }
        public NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode caseType { get; set; }
        public string videoUrl { get; set; }
        public bool isTestQa { get; set; }
    }
}
/*, inc_eb.New_relaunchDate
 * inc.incidentid ,inc.ticketnumber ,con.ContactId ,con.FullName ContactFullName, inc_eb.new_telephone1 ,pexp.new_name ExpertiseName
		,reg.New_englishname RegionName,inc.createdon, inc_eb.new_createdonlocal
		,(case when inc.statuscode = @InReview then CAST(1 as bit) else CAST(0 as bit) end) IsInReview		
		,ISNULL(inc_eb.New_ordered_providers, 0) AdvertisersProvided
		,case when isnull(new_requiredaccountno, 0) = 0 then 1 else new_requiredaccountno end as RequestedAdvertisers
		,(select sum(new_potentialincidentprice) 
			from dbo.New_incidentaccountBase iab with(nolock)
			inner join dbo.New_incidentaccountExtensionBase iaeb with(nolock) on iab.New_incidentaccountId = iaeb.New_incidentaccountId
			where new_tocharge = 1 and deletionstatecode = 0 and new_incidentid = inc.incidentid) as Revenue		
					,inc.statuscode
					,inc_eb.New_passedReview*/
