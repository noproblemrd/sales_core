﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class AddAarSupplierToLeadRequest
    {
        public string name { get; set; }
        public string phoneNumber { get; set; }
        public Guid incidentId { get; set; }
    }
}
