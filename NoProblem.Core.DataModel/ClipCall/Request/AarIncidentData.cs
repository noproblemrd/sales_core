﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class AarIncidentData
    {
        static readonly string[] mobiles =
            new string[4] { "iphone", "ipad", "android", "window" };

        private string UserAgent;
        public string SetUserAgent
        {
            set { UserAgent = value; }
        }

        public string AdvertiserName { get; set; }
        public string AdvertiserPhone { get; set; }
        public string CallStatus { get; set; }
        public DateTime Date { get; set; }        
        public string Device { get; set; }
        public bool WatchedVideo { get; set; }
        public int ViewVideoCount { get; set; }
        public bool ViewAllVideo { get; set; }
        public string CallRecord { get; set; }
        public int CallDuration { get; set; }
        public string TextMessage { get; set; }
        public string CallBackMessage { get; set; }
        public void SetValues()
        {
            WatchedVideo = ViewVideoCount > 0;
            if (string.IsNullOrEmpty(UserAgent))
            {
                Device = string.Empty;
                return;
            }
            string ua = UserAgent.ToLower();            
            foreach(String device in mobiles)
            {
                if (ua.Contains(device))
                {
                    Device = device;
                    return;
                }
            }
            Device = "other";
        }
    }
}
