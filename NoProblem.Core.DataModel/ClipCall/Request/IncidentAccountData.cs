﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class IncidentAccountData
    {
        public string AdvertiserName { get; set; }
        public string AdvertiserPhone { get; set; }
        public NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status Status { get; set; }
        public DateTime Date { get; set; }
        public string CallRecord { get; set; }
        public int CallRecordDuration { get; set; }
        public bool HasMobileApp { get; set; }
    }
  //  SELECT acc.Name AccountName, acc.Telephone1, ia.statuscode, ia.New_mobileInConversationDate,
//	ia.New_recordfilelocation, ia.New_RecordDuration
}
