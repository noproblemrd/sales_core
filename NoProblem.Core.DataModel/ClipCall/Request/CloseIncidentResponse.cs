﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class CloseIncidentResponse
    {
        public enum eCloseIncidentStatus
        {
            SUCCESS, 
            FAILED,
            INCIDENT_ALREADY_CLOSED
        }
        public eCloseIncidentStatus Status { get; set; }
        public bool IsSuccess { get; set; }
    }
}
