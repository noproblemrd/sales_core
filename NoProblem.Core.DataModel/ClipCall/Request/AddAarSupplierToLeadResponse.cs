﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class AddAarSupplierToLeadResponse
    {
        public enum AddAarSupplierStatus
        {
            SUCCESS,
            INVALID_PHONE_NUMBER,
            INVALID_INCIDENT,
            INCIDENT_CLOSED,
            INCIDENT_IN_REVIEW,
            MISSING_SUPPLIER_NAME
        }
        public AddAarSupplierStatus status { get; set; }
        public bool isSuccess { get; set; }
    }
}
