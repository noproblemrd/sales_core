﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class RelaunchSearchSuppliersRequest
    {
        public Guid CategoryId { get; set; }
        public string RegionName { get; set; }
        public int RegionLevel { get; set; }
        public Guid IncidentId { get; set; }
    }
    public class RelaunchSearchSuppliersResponse
    {
        public Guid AccountId { get; set; }
        public string AccountName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }
}
