﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Request
{
    public class ReviewRejectConfirmRequest
    {
        public bool toConfirm { get; set; }
        public Guid incidentId { get; set; }
    }
}
