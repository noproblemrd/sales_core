﻿using System;

namespace NoProblem.Core.DataModel.ClipCall
{
    public interface IIdentifiable
    {
        Guid Id { get; }
    }
}
