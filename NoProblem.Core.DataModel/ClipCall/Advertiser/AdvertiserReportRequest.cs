﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NoProblem.Core.DataModel.ClipCall.Advertiser
{
    public class AdvertiserReportRequest : FromToDateTimeRequest
    {
        public eStepInRegistration stepInRegistration { get; set; }
    }
    public enum eStepInRegistration
    {
       
        ALL,    
        BUSINESS_DETAILS,    
        COVER_AREA,     
        COMPLETE
    }
}
