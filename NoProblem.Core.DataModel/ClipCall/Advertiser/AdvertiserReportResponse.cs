﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.Advertiser
{
    public class AdvertiserReportResponse
    {
        public AdvertiserReportResponse()
        {
            categories = new List<string>();
        }
        public Guid accountId { get; set; }
        public DateTime createdOn { get; set; }
        public string name { get; set; }
        public List<string> categories { get; set; }
        public string address { get; set; }
        public eStepInRegistration stepInRegistration { get; set; }
        public string phone { get; set; }
    }
    
}
