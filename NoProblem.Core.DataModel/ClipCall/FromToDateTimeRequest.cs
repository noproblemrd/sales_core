﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall
{
    public abstract class FromToDateTimeRequest
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public DateTime GetFrom
        {
            get { return From; }
        }
        public DateTime GetTo
        {
            get { return To.AddDays(1); }
        }
    }
}
