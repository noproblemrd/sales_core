﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ClipCall.FunnelSummary
{
    public class FunnelSummaryResponse
    {
        public Guid incidentId { get; set; }
        public DateTime date { get; set; }
        public string caseNumber { get; set; }
        public string category { get; set; }
        public string videoLink { get; set; }
        public int videoDuration { get; set; }
        public string region { get; set; }
        public int SMS_Sent { get; set; }
        public int LP_Visits { get; set; }
        public int videoViews { get; set; }
        public int connection { get; set; }
        public bool isQaTest { get; set; }
        public int P5 { get; set; }
        public int P4 { get; set; }
        public int P3 { get; set; }
        public int P2 { get; set; }
        public int P1 { get; set; }
    }
}
