﻿using System;

namespace NoProblem.Core.DataModel.ClipCall
{
    public class ResourceNotFoundException:Exception
    {
        private const string ResourceNotFoundMessageFormat = "Count not find resource {0} with id {1}";
        public ResourceNotFoundException(string resourceName, Guid resourceId)
            : base(string.Format(ResourceNotFoundMessageFormat, resourceName, resourceId))
        {
            
        }
    }
}
