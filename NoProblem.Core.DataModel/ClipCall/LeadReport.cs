﻿using System;

namespace NoProblem.Core.DataModel.ClipCall
{
    public class LeadReport
    {
        public Guid SupplierId { get; set; }
        public Guid IncidentAccountId { get; set; }
        public string Comment { get; set; }
        public int ReasonCode { get; set; }
    }
}
