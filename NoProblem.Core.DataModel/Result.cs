﻿/////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                     //
//    Class: NoProblem.Core.DataModel.Result                                             //
//    File: Result.cs                                                                                  //
//    Description: A general object to be used as return value in services.                            //
//    Created On: 2/9/10                                                                               //
//    Author: Alain Adler                                                                              //
//    Company: Onecall                                                                                 //
//                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    [Serializable]
    public class Result
    {
        #region Enums
        [Serializable]
        public enum eResultType
        {
            Success,
            Failure
        }
        #endregion

        #region Properties
        public eResultType Type { get; set; }
        public List<string> Messages { get; set; }

        public bool IsSuccess
        {
            get
            {
                return Type == eResultType.Success;
            }
        }

        #endregion

        #region Ctor
        public Result()
        {
            Type = eResultType.Success;
            Messages = new List<string>();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Return all the messages in this.Messages, separated by a coma.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            int messagesCount = Messages.Count;
            for (int i = 0; i < messagesCount; i++)
            {
                builder.Append(Messages.ElementAt(i));
                if (i < messagesCount - 1)
                {
                    builder.Append(", ");
                }
            }
            return builder.ToString();
        }

        #endregion
    }

    [Serializable]
    public class Result<T> : Result
    {
        #region Properties
        public T Value { get; set; }
        #endregion
    }
}
