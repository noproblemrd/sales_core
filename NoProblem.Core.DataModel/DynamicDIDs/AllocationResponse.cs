﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.DynamicDIDs
{
    public class AllocationResponse
    {
        public string RealPhone { get; set; }
        public string DID { get; set; }
        public bool IsMapped { get; set; }
        public string NotMappedReason { get; set; }

        public AllocationResponse(string realPhone)
        {
            RealPhone = realPhone;
            IsMapped = false;
        }

        /// <summary>
        /// Parameterless ctor to make the class serializable
        /// </summary>
        public AllocationResponse()
        {

        }
    }
}
