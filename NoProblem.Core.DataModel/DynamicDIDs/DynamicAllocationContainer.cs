﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.DynamicDIDs
{
    public class DynamicAllocationContainer
    {
        public bool IsFound { get; set; }
        public string RealPhone { get; set; }
        public Guid AllocationId { get; set; }
        public Guid DynamicDidId { get; set; }
        public Guid HeadingId { get; set; }
        public Guid WebsiteId { get; set; }

        public DynamicAllocationContainer()
        {
            IsFound = false;
        }
    }
}