﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.DynamicDIDs
{
    public class AllocationRequest
    {
        public string RealPhone { get; set; }
        public string RegionCode { get; set; }
        public string HeadingCode { get; set; }
        public string WebSite { get; set; }
        public Guid OriginId { get; set; }
        public string Url { get; set; }
    }
}
