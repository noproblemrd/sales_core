﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class CustomerLogOnResponse
    {
        public Guid CustomerId { get; set; }
        public string ApiToken { get; set; }
    }
}
