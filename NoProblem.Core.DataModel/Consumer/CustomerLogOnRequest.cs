﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class CustomerLogOnRequest
    {
        public string PhoneNumber { get; set; }
        public NoProblem.Core.DataModel.SupplierModel.Registration2014.Mobile.MobileLogOnData MobileLogOnData { get; set; }
    }
}
