﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class ServiceRequestDuiAttorneyData : ServiceRequestTwoStepWaitingData
    {
        public bool AffordAttorney { get; set; }
        public eDuiStatusOfTheDui Status { get; set; }
        public eDuiAreYouRepresented Represented { get; set; }
        public eFinancing Financing { get; set; }

        public enum eDuiAreYouRepresented
        {
            No = 1, Yes, YesButLookingForNewRepresentation
        }

        public enum eDuiStatusOfTheDui
        {
            GotDuiButNoCourtDate = 1,
            GotDuiAndHaveCourtDate,
            WantPastDuiRemoved
        }

        public enum eFinancing
        {
            Borrowing = 1,
            PersonalSavings,
            FamilySupport,
            CurrentIncome,
            WillDiscussWithAttorney,
            CannotAfford
        }
    }
}
