﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class AddOnActionRequest
    {
        public Guid OriginId { get; set; }
        public List<string> MacAddresses { get; set; }
        public string IP { get; set; }
        public bool IsInstallation { get; set; }
        public string AppType { get; set; }
        public bool? IsDuplicate { get; set; }
        public string SubId { get; set; }
        public string Country { get; set; }
        public bool IsUpdater { get; set; }
    }

    public class ReportInstallationRequest
    {
        public string ApplicationName { get; set; }
        public string IpAddress { get; set; }
    }
}
