﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    [Serializable]
    public class ServiceRequestMoverData
    {
        public eMoveSize? MoveSize { get; set; }
        public eMoveType? MoveType { get; set; }
        public DateTime? MoveDate { get; set; }
        public string MoveToZipCode { get; set; }

        public enum eMoveSize
        {
            studio,
            oneBdr,
            twoBdr,
            threeBdr,
            fourBdr,
            fiveBdr,
            larger
        }

        public enum eMoveType
        {
            residential,
            office,
            autoTransport,
            truckRentalQuotes,
            uLoadTheyDrive,
            International,
            StorageOnly
        }
    }
}
