﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer.ClipCall
{
    public class LeadRequestVideoChat : Interfaces.ILeadRequest
    {
        public Guid invitationId;
       // public Guid ArchiveId;
        public LeadRequestVideoChat(Guid invitationId)
        {
            this.invitationId = invitationId;
   //         this.ArchiveId = ArchiveId;
        }
        public Guid CategoryId { get; set; }
        public string ZipCode { get; set; }
        public Guid CustomerId { get; set; }
        public string VideoLeadUrl { get; set; }
        public string PreviewPicUrl { get; set; }
        public int VideoDuration { get; set; }
        public string CategoryName { get; set; }
        public Guid RegionId { get; set; }
        public string country { get; set; }
        public string CustomerPhone { get; set; }
        public Guid AccountId { get; set; }
        public Guid OriginId { get; set; }
        
    }
}
