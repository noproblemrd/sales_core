﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer.ClipCall.Interfaces
{
    public interface ILeadRequest 
    {
        Guid CategoryId { get; set; }
        string ZipCode { get; set; }
        Guid CustomerId { get; set; }
        Guid AccountId { get; set; }
        string VideoLeadUrl { get; set; }
        string PreviewPicUrl { get; set; }
        int VideoDuration { get; set; }
        string CategoryName { get; set; }
        Guid RegionId { get; set; }
        string country { get; set; }
        string CustomerPhone { get; set; }
        Guid OriginId { get; set; }
    //    void SetValues();
    }
}
