﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class ServiceRequestStorageData : ServiceRequestTwoStepWaitingData
    {
        public eTypeOfStorage? TypeOfStorage { get; set; }
        public ePlanningToUseStorage? PlanningToUseStorage { get; set; }
        public eHowManyContainers? HowManyContainers { get; set; }
        public int? HowManyContainersSpecify { get; set; }
        public eStorageLength? StorageLength { get; set; }
        public int? StorageLengthSpecify { get; set; }
        public eFinanceStorage? FinanceStorage { get; set; }
        public eDeliveryStorage? DeliveryStorage { get; set; }
        public string ZipCodeOther { get; set; }

        public enum eTypeOfStorage
        {
            Business = 1, Personal
        }

        public enum ePlanningToUseStorage
        {
            Storage_at_my_property = 1,
            Storage_at_a_climate_controlled_warehouse,
            Moving_and_storage,
            Moving_only
        }

        public enum eHowManyContainers
        {
            one = 1, two_to_four, five_to_seven, eight_plus
        }

        public enum eStorageLength
        {
            not_sure = 1,
            ten, twenty, forty, forty_eight, 
            other
        }

        public enum eFinanceStorage
        {
            not_sure = 1,
            Rent_or_lease,
            Purchase,
            Lease_to_purchase
        }

        public enum eDeliveryStorage
        {
            ASAP = 1, one_month, two_months, three_months_or_more
        }
    }
}
