﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class ServiceRequestDivorceAttorneyData : ServiceRequestTwoStepWaitingData
    {
        public eTermsOfDivorce Terms { get; set; }
        public eDivorceHowSoon Soon { get; set; }
        public bool Live { get; set; }
        public bool BeenFiled { get; set; }
        public bool Represented { get; set; }
        public eDivorceFinancing Financing { get; set; }
        public eDivorceIncome Income { get; set; }
        public eDivorceIncome SpouseIncome { get; set; }
        public eDivorceProperty Property { get; set; }
        public eDivorceChildren Children { get; set; }

        public enum eTermsOfDivorce
        {
            WeAgreeOnSomeButNotAll = 1,
            WeAgreeOnAllTerms,
            WeDoNotAgree
        }

        public enum eDivorceHowSoon
        {
            Immediately = 1,
            OneToSixMonths,
            SixMonthsOrLater,
            WeDoNotWantToFile
        }

        public enum eDivorceFinancing
        {
            IWillDiscussPaymentOptionsWithMyAttorney = 1,
            CreditCardOrPersonalLoan,
            PersonalSavings,
            FamilySupport,
            ICannotAffordLegalFees
        }

        public enum eDivorceIncome
        {
            LessThan10K = 1,
            TenKTo30K,
            ThirtyKTo60K,
            SixtyKTo100K,
            MoreThan100K
        }

        public enum eDivorceProperty
        {
            Other = 1,
            HouseOrCondo,
            Vehicles,
            VacationProperty,
            Jewelry,
            Pension,
            StocksOrBonds,
            IRA
        }

        public enum eDivorceChildren
        {
            Two = 1,
            Zero,
            One,
            Three,
            Four,
            Five,
            MoreThan5
        }
    }
}
