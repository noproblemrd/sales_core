﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer.VideoRequests
{
    public class NewVideoRequest
    {
        public Guid CategoryId { get; set; }
        public string ZipCode { get; set; }
        public Guid CustomerId { get; set; }
        public string VideoLeadUrl { get; set; }
        public string PreviewPicUrl { get; set; }
        public int VideoDuration { get; set; }
        public string CategoryName { get; set; }

        public bool IsPrivate { get; set; }
        public Guid[] FavoriteSupplierIds { get; set; }
    }
}
