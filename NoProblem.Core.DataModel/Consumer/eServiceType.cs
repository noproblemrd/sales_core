﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public enum eServiceType
    {
        Normal,
        Scheduled,
        SupplierSpecified
    }
}
