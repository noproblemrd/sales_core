﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer.Interfaces
{
    public interface IServiceResponse
    {
        StatusCode Status { get; set; }

        string ServiceRequestId { get; set; }

        string Message { get; set; }

        DataModel.Xrm.new_channel.ChannelCode? ChannelCode { get; set; }

        bool SentToBroker { get; set; }
    }
}
