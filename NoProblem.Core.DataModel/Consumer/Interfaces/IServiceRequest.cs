﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer.Interfaces
{
    public interface IServiceRequest
    {
        Guid ContactId { get; set; }
        string ContactPhoneNumber { get; set; }
        string ContactFullName { get; set; }
        string ContactAddress { get; set; }
        string ContactWeight { get; set; }
        string ContactHeight { get; set; }
        int ContactHeightFeet { get; set; }
        int ContactHeightInches { get; set; }
        string ExpertiseCode { get; set; }
        int ExpertiseType { get; set; }
        DateTime PrefferedCallTime { get; set; }
        string RequestDescription { get; set; }
        string RegionCode { get; set; }
        int RegionLevel { get; set; }
        Guid OriginId { get; set; }
        int NumOfSuppliers { get; set; }
        string PlaceInWebSite { get; set; }
        string PageName { get; set; }
        string SiteId { get; set; }
        string Url { get; set; }
        string Domain { get; set; }
        string ControlName { get; set; }
        string SessionId { get; set; }
        Guid UpsaleId { get; set; }
        Guid UpsaleRequestId { get; set; }
        Guid UserId { get; set; }
        string Keyword { get; set; }
        string ToolbarId { get; set; }
        string ContactEmail { get; set; }
        string IP { get; set; }
        string SiteTitle { get; set; }
        string Browser { get; set; }
        string BrowserVersion { get; set; }
        string ExposureId { get; set; }
        eBudget? Budget { get; set; }
        DateTime? DateOfBirth { get; set; }
        bool IsFemaleGender { get; set; }
        string Country { get; set; }
        int? Type { get; set; }
        int? AppType { get; set; }
        int? DesktopAppCameFrom { get; set; }

        /*
public enum eAppType
{
    All = 1,
    Slider = 2,
    Intext= 3,
    PopApp = 4,
    DeskApp = 5
}
 
 
public enum eDesktopAppCameFrom
{
    None = 1,
    Browser = 2,
    SearchFormApp = 3
}          
         */

        /// <summary>
        /// Token from company LeadId.com
        /// </summary>
        string LeadIdToken { get; set; }
    }
}
