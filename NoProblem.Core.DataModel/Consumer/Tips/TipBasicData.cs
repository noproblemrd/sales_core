﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer.Tips
{
    public class TipBasicData
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public Guid CategoryId { get; set; }
        public Guid TipId { get; set; }
    }
}
