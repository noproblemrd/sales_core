﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer.Tips
{
    public class TipFeedback
    {
        public Guid TipId { get; set; }
        public Guid SupplierId { get; set; }
        public bool IsYelpSupplier { get; set; }
        public Guid IncidentId { get; set; }
        public bool IsPositiveFeedback { get; set; }
    }
}
