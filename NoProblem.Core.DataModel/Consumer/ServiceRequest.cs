﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using NoProblem.Core.DataModel.Consumer.Interfaces;

namespace NoProblem.Core.DataModel
{
    [Serializable]
    public class ServiceRequest : IServiceRequest
    {
        public Guid ContactId { get; set; }
        public string SiteId { get; set; }
        public string ContactFullName { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactAddress { get; set; }
        public string ExpertiseCode { get; set; }
        public int ExpertiseType { get; set; }
        public string RegionCode { get; set; }
        public int RegionLevel { get; set; }
        public int NumOfSuppliers { get; set; }
        public DateTime PrefferedCallTime { get; set; }
        public Guid OriginId { get; set; }
        public string RequestDescription { get; set; }
        public Guid UserId { get; set; }
        public Guid UpsaleId { get; set; }
        public Guid UpsaleRequestId { get; set; }
        public string PlaceInWebSite { get; set; }
        public string PageName { get; set; }
        public string Url { get; set; }
        public string Domain { get; set; }
        public string ControlName { get; set; }
        public string SessionId { get; set; }
        public string Keyword { get; set; }
        public string ToolbarId { get; set; }
        public bool IsAutoUpsale { get; set; }
        public string ContactEmail { get; set; }
        public DataModel.Consumer.ServiceRequestMoverData MoverData { get; set; }
        public string SubType { get; set; }
        public string IP { get; set; }
        public string SiteTitle { get; set; }
        public DataModel.Consumer.ServiceRequestPayDayLoanData PaydayLoanData { get; set; }
        public DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData PersonalInjuryAttorneyData { get; set; }
        public DataModel.Consumer.ServiceRequestCriminalLawAttorneyData CriminalLawAttorneyData { get; set; }
        public DataModel.Consumer.ServiceRequestDuiAttorneyData DuiAttorneyData { get; set; }
        public DataModel.Consumer.ServiceRequestDivorceAttorneyData DivorceAttorneyData { get; set; }
        public DataModel.Consumer.ServiceRequestBankruptcyAttorneyData BankruptcyAttorneyData { get; set; }
        public DataModel.Consumer.ServiceRequestSocialSecurityDisabilityData SocialSecurityDisabilityData { get; set; }
        public DataModel.Consumer.ServiceRequestStorageData StorageData { get; set; }
        public DataModel.Consumer.ServiceRequestHealthInsuranceData HealthInsuranceData { get; set; }
        public int Step { get; set; }
        public string Browser { get; set; }
        public string BrowserVersion { get; set; }
        public string ExposureId { get; set; }
        public NoProblem.Core.DataModel.Consumer.eBudget? Budget { get; set; }
        public string Country { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public bool IsFemaleGender { get; set; }

        public Guid PreSoldToAdvertiserId { get; set; }
        public string PreSoldMessage { get; set; }
        public decimal? PreSoldPrice { get; set; }

        public string ContactWeight { get; set; }
        public string ContactHeight { get; set; }
        public int ContactHeightFeet { get; set; }
        public int ContactHeightInches { get; set; }

        public string LeadIdToken { get; set; }
        public DataModel.Consumer.ServiceRequestHomeSecurityData HomeSecurityData { get; set; }

        public int? Type { get; set; }

        public int? AppType { get; set; }
        public int? DesktopAppCameFrom { get; set; }
    }
}
