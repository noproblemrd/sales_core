﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class SendEmailToCustomerAfterCaseRequest
    {
        public string Email { get; set; }
        public Guid IncidentId { get; set; }
    }
}
