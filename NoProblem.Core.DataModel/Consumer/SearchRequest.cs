﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    [Serializable]
    public class SearchRequest
    {
        public string SiteId { get; set; }
        public string ExpertiseCode { get; set; }
        public int ExpertiseLevel { get; set; }
        public string RegionCode { get; set; }
        public int RegionLevel { get; set; }
        public int MaxResults { get; set; }
        public string OriginId { get; set; }
       // public string IncidentId { get; set; }
        public Guid UpsaleId { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
