﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class SupplierServiceResponse : NoProblem.Core.DataModel.Consumer.Interfaces.IServiceResponse
    {
        #region IServiceResponse Members

        public StatusCode Status { get; set; }

        public string ServiceRequestId { get; set; }

        public string Message { get; set; }

        public DataModel.Xrm.new_channel.ChannelCode? ChannelCode { get; set; }

        public bool SentToBroker { get; set; }

        #endregion
    }
}
