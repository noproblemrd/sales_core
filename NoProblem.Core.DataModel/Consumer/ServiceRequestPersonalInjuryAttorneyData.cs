﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class ServiceRequestPersonalInjuryAttorneyData : ServiceRequestTwoStepWaitingData
    {
        public eAnyoneHasInsurance DoesAnyoneHasVehicleInsurance { get; set; }
        public eEstimatedMedicalBills EstimatedMedicalBills { get; set; }
        public bool Whiplash { get; set; }
        public bool BrokenBones { get; set; }
        public bool LostLimb { get; set; }
        public bool SpinalCordInjuryOrParalysis { get; set; }
        public bool BrainInjury { get; set; }
        public bool LossOfLife { get; set; }
        public bool Other { get; set; }
        public bool CurrentlyRepresented { get; set; }
        public eAccidentResult AccidentResult { get; set; }
        public bool IsYourFault { get; set; }

        public enum eAnyoneHasInsurance
        {
            Yes = 1, No, DontKnow
        }

        public enum eAccidentResult
        {
            hospitalization = 1,
            medicalTreatment,
            surgery,
            missedWork,
            wrongfulDeath,
            noneOfTheAbove
        }

        public enum eEstimatedMedicalBills
        {
            lessThan1k = 1,
            e1kTo5k,
            e5kTo10k,
            e10kTo25k,
            e25kTo100k,
            moreThan100k,
            dontKnow,
            noMedicalBills
        }
    }
}
