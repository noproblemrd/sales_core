﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public enum eBudget
    {

        UpTo500 = 1,
        From500To1000,
        From1000To2500,
        From2500To5000,
        From5000To10000,
        From10000To15000,
        From15000To25000,
        From25000To50000,
        From50000To100000,
        From100000To200000,
        From200000AndMore
    }
}
