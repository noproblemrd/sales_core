﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public class DescriptionValidationLogRequest
    {
        public string Heading { get; set; }
        public string Region { get; set; }
        public string Url { get; set; }
        public string Session { get; set; }
        public string Keyword { get; set; }
        public enumQualityCheckName QualityCheckName { get; set; }
        public string Description { get; set; }
        public Guid OriginId { get; set; }
        public string Phone { get; set; }
        public string ZipCodeCheckName { get; set; }
        public string Event { get; set; }
        public string Flavor { get; set; }
        public string Step { get; set; }
        public string Control { get; set; }
    }

    public enum enumQualityCheckName
    {
        Warn_morethan140characters,
        Error_3samecharsinarow,
        Error_3or4words,
        Error_lessthan3words,
        Error_nonAtoZ,
        Warn_badword,
        Error_3or4wordsandlongword,
        OK
    }
}
