﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class ServiceRequestPayDayLoanData : ServiceRequestTwoStepWaitingData
    {
        public int? RequestedLoanAmount { get; set; }
        public bool? IsRent { get; set; }
        public int? MonthlyIncome { get; set; }
        public eAccountType? AccountType { get; set; }
        public bool? DirectDeposit { get; set; }
        public ePayPeriod? PayPeriod { get; set; }
        public DateTime? NextPayDate { get; set; }
        public DateTime? SecondPayDate { get; set; }
        public int? MonthsAtResidence { get; set; }
        public eIncomeType? IncomeType { get; set; }
        public bool? IsActiveMilitary { get; set; }
        public string Ocupation { get; set; }
        public string Employer { get; set; }
        public string WorkPhone { get; set; }
        public int? MonthsEmployed { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public int MonthsWithBank { get; set; }
        public string DrivingLicenseState { get; set; }
        public string DrivingLicenseNumber { get; set; }
        public string SocialSecurityNumber { get; set; }

        public enum eAccountType
        {
            Checking = 1,
            Saving
        }

        public enum ePayPeriod
        {
            Weekly = 1,
            Biweekly,
            TwiceMonthly,
            Monthly
        }

        public enum eIncomeType
        {
            Employment = 1,
            Benefits
        }
    }
}
