﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class ServiceRequestHealthInsuranceData : ServiceRequestTwoStepWaitingData
    {
        public eExistingCondition? ExistingCondition { get; set; }
        public bool? IsSmoker { get; set; }
        public bool? IsInsured { get; set; }
        public bool? ExpectantParent { get; set; }
        public bool? PreviouslyDenied { get; set; }
        public int? Household { get; set; }
        public int? Income { get; set; }

        public enum eExistingCondition
        {
            None,
            AidsOrHiv,
            Diabetes,
            Liver,
            Alzheimers,
            Lung,
            DrugAbuse,
            Mental,
            Cancer,
            Heart,
            Stroke,
            Kidney,
            Vascular
        }
    }
}
