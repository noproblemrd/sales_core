﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer.DesktopApp
{
    public class CustomerPaymentInDesktopApp
    {
        public string UserAgent { get; set; }
        public string UserIP { get; set; }
        public string UserHost { get; set; }
        public Guid ContactId { get; set; }
        public string EncryptedCreditCardNumber { get; set; }
        public string EcryptedCvv { get; set; }
        public string Last4Digits { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string CreditCardType { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public string BillingAddress { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
        public string BillingPhone { get; set; }
        public string BillingEmail { get; set; }
        public decimal AmountDollars { get; set; }
    }
}
