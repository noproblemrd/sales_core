﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class ServiceRequestSocialSecurityDisabilityData : ServiceRequestTwoStepWaitingData
    {
        public bool? MissWork { get; set; }
        public bool? AlreadyReceiveBenefits { get; set; }
        public bool? HaveAttorney { get; set; }
        public bool? AbleToWork { get; set; }
        public bool? PrescribedMedication { get; set; }
        public bool? HasDoctor { get; set; }
        public bool? Work5Of10 { get; set; }
    }
}
