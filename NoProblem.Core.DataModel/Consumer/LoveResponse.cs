﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class LoveResponse
    {
        public int MyLove { get; set; }
        public int AllLove { get; set; }
    }
}
