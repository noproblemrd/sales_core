﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class ServiceRequestHomeSecurityData
    {
        public bool? OwnProperty { get; set; }
        public eHomeSecurityPropertyType? PropertyType { get; set; }
        public eHomeSecurityTimeFrame? TimeFrame { get; set; }
        public eHomeSecurityCreditRankings? CreditRanking { get; set; }

        public enum eHomeSecurityPropertyType
        {
            Business = 1,
            Residence
        }

        public enum eHomeSecurityTimeFrame
        {
            now = 1,
            one_two_weeks,
            two_four_weeks,
            two_three_months,
            over_three_months,            
            unknown
        }

        public enum eHomeSecurityCreditRankings
        {
            Excellent = 1, Good, Fair, Poor
        }
    }
}
