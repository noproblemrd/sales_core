﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer.ServiceRequestEnums
{
    public enum eAppType
    {
        All = 1,
        Slider = 2,
        Intext = 3,
        PopApp = 4,
        DeskApp = 5
    }
}
