﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer.ServiceRequestEnums
{
    public enum eDesktopAppCameFrom
    {
        None = 1,
        Browser = 2,
        SearchFormApp = 3
    }
}
