﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class HourRangeData
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
