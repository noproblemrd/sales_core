﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class LoveRequest
    {
        public Guid ContactId { get; set; }
        public Guid SupplierId { get; set; }
    }
}
