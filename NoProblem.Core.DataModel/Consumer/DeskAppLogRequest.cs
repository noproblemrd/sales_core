﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class DeskAppLogRequest
    {
        public string Type { get; set; }
        public string OperatingSystemName { get; set; }
        public string OperatingSystemEdition { get; set; }
        public string ServicePack { get; set; }
        public string OperatingSystemVersion { get; set; }
        public string OperatingSystemBites { get; set; }
        public string DotNetVersions { get; set; }
        public bool ProxyEnabled { get; set; }
        public string ProxyServer { get; set; }
        public bool ProxySettingPerUser { get; set; }
        public string UserId { get; set; }
        public Guid OriginId { get; set; }
        public string NotificationText { get; set; }
        public string ConfigurationScriptElement { get; set; }
        public string ConfigurationModifiedOn { get; set; }
        public string ConfigurationVersion { get; set; }
        public int Port { get; set; }
        public string InstalledPrograms { get; set; }
        public string AppType { get; set; }
    }
}
