﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class SupplierServiceRequest : NoProblem.Core.DataModel.Consumer.Interfaces.IServiceRequest
    {
        public Guid ContactId { get; set; }
        public Guid SupplierId { get; set; }
        public string SiteId { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactFullName { get; set; }
        public string ContactAddress { get; set; }
        public string ExpertiseCode { get; set; }
        public int ExpertiseType { get; set; }
        public string RegionCode { get; set; }
        public int RegionLevel { get; set; }
        public DateTime PrefferedCallTime { get; set; }
        public Guid OriginId { get; set; }
        public string RequestDescription { get; set; }
        public int NumOfSuppliers { get; set; }
        public Xrm.new_channel.ChannelCode Channel { get; set; }
        public string PlaceInWebSite { get; set; }
        public string PageName { get; set; }
        public string Url { get; set; }
        public string Domain { get; set; }
        public string ControlName { get; set; }
        public string SessionId { get; set; }
        public string Keyword { get; set; }
        public Guid UpsaleId { get; set; }
        public Guid UpsaleRequestId { get; set; }
        public Guid UserId { get; set; }
        public string ToolbarId { get; set; }
        public string ContactEmail { get; set; }
        public string IP { get; set; }
        public string SiteTitle { get; set; }
        public string Browser { get; set; }
        public string BrowserVersion { get; set; }
        public string ExposureId { get; set; }
        public NoProblem.Core.DataModel.Consumer.eBudget? Budget { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public bool IsFemaleGender { get; set; }
        public string ContactWeight { get; set; }
        public string ContactHeight { get; set; }
        public int ContactHeightFeet { get; set; }
        public int ContactHeightInches { get; set; }
        public string Country { get; set; }
        public string LeadIdToken { get; set; }
        public int? Type { get; set; }

        public int? AppType { get; set; }
        public int? DesktopAppCameFrom { get; set; }
    }
}
