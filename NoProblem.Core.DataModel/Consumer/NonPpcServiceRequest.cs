﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class NonPpcServiceRequest
    {
        public string AdvertiserPhoneNumber { get; set; }
        public string AdvertiserPhoneNumber2 { get; set; }
        public string AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public string ContactPhoneNumber { get; set; }
    }
}
