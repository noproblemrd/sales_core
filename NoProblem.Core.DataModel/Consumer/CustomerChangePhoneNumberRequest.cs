﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class CustomerChangePhoneNumberRequest : CustomerLogOnRequest
    {
        public Guid customerId { get; set; }
    }
}
