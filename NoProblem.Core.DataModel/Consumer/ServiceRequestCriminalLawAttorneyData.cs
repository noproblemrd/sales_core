﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class ServiceRequestCriminalLawAttorneyData : ServiceRequestTwoStepWaitingData
    {
        public bool CurrentCharges { get; set; }
        public bool AffordAttorney { get; set; }
    }
}
