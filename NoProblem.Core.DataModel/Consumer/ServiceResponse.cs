﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    [Serializable]
    public class ServiceResponse : NoProblem.Core.DataModel.Consumer.Interfaces.IServiceResponse
    {
        public StatusCode Status { get; set; }

        public string Message { get; set; }

        public string ServiceRequestId { get; set; }

        public DataModel.Xrm.new_channel.ChannelCode? ChannelCode { get; set; }

        public bool SentToBroker { get; set; }
    }

    public enum StatusCode
    {
        Success = 0,
        BlackList = 1,
        NoSuppliers = 2,
        NoDefaultUser = 3,
        CreateIncidentFailed = 4,
        CreateIncidentAccountFailed = 5,
        BadWord = 6,
        BadParametersReceived = 7,
        UnexpectedError = 8,
        Duplicate = 9,
        WaitingForPhoneValidation = 10
    }
}
