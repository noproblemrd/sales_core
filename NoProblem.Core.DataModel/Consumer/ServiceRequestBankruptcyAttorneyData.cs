﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class ServiceRequestBankruptcyAttorneyData : ServiceRequestTwoStepWaitingData
    {
        public eConsideringBankruptcy Considering { get; set; }
        public eBillsBankruptcy Bills { get; set; }
        public eMoneyEstimateBankruptcy Expenses { get; set; }
        public bool BehindRealEstate { get; set; }
        public bool BehindAutomobile { get; set; }
        public bool Assets { get; set; }
        public eTypesOfIncomeBankruptcy IncomeTypes { get; set; }
        public eMoneyEstimateBankruptcy Income { get; set; }
        public bool AffordAttorney { get; set; }

        public enum eConsideringBankruptcy
        {
            Other = 1,
            Garnishment,
            CreditorHarassment,
            Repossession,
            Foreclosure,
            Lawsuits,
            Illness,
            Disability,
            LicenseSuspension,
            Divorce,
            LossOfIncome
        }

        public enum eBillsBankruptcy
        {
            Other = 1,
            CreditCards,
            StoreCards,
            PersonalLoans,
            ChildSupport,
            StudentLoans,
            AutoLoansIncomeTaxes,
            PaydayLoans,
            MedicalBills
        }

        public enum eMoneyEstimateBankruptcy
        {
            LessThan3K = 1,
            ThreeKTo6K,
            SixKTo10K,
            TenKTo15K,
            MoreThan15K
        }

        public enum eTypesOfIncomeBankruptcy
        {
            Other = 1,
            EmployedFullTime,
            EmployedPartTime,
            SocialSecurity,
            Pension,
            RetirementChildSupport,
            Maintenance,
            NoIncome
        }
    }
}
