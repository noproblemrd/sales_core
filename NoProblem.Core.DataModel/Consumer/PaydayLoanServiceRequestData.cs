﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.DataModel.Consumer
{
    public class PaydayLoanServiceRequestData
    {
        public int? LoanAmount { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public new_paydayloandata.pdlBestTimeToCall? BestTimeToCall { get; set; }
        public string StreetAddress { get; set; }
        public int? LengthAtAddressInMonths { get; set; }
        public bool? IsAddressRent { get; set; }
        public string DriverLicense { get; set; }
        public string DriverLicenseState { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string SSN { get; set; }
        public new_paydayloandata.pdlIncomeSource? IncomeSource { get; set; }
        public int? MonthlyNetIncome { get; set; }
        public string EmployerName { get; set; }
        public string JobTitle { get; set; }
        public int? TimeEmployedInMonths { get; set; }
        public bool? IsActiveMilitary { get; set; }
        public new_paydayloandata.pdlPayFrequency? PayFrequency { get; set; }
        public bool? IsDirectDeposit { get; set; }
        public DateTime? NextPayday { get; set; }
        public DateTime? SecondPayday { get; set; }
        public string AbaRouting { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankName { get; set; }
        public string BankPhone { get; set; }
        public int? LengthOfAccountInMonths { get; set; }
        public new_paydayloandata.pdlAccountType? AccountType { get; set; }
    }
}
