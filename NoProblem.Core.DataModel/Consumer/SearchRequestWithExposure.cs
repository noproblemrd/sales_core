﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public class SearchRequestWithExposure : SearchRequest
    {
        public string ControlName { get; set; }
        public string Domain { get; set; }
        public string PageName { get; set; }
        public string PlaceInWebSite { get; set; }
        public string SessionId { get; set; }
        public string Url { get; set; }
    }
}
