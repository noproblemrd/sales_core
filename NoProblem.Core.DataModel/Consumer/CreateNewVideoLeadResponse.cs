﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Consumer
{
    public class CreateNewVideoLeadResponse
    {
        public Guid LeadId { get; set; }
        public bool UpdateCache { get; set; }
        public eLeadStatus leadStatus { get; set; }
        public enum eLeadStatus
        {
            Success,
            Failed,
            WatingForPhoneValidation,
            NotFavorite
        }
    }
}
