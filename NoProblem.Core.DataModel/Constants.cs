﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public static class Constants
    {
        public const string BLOCKED_PHONE_NUMBER = "0";
        public const string CONST_ERROR_NOT_AVAILABLE_TODAY = "-10";
        //public const int EARTH_RADIUS_KM = 6378;
        public const int EARTH_RADIUS_MILES = 3963;

        public static class Countries
        {
            public const string UNITED_STATES = "UNITED STATES";
            public const string UNITED_KINGDOM = "UNITED KINGDOM";
            public const string ISRAEL = "ISRAEL";
        }

        public static class Trunks
        {
            public const string ISR_012 = "012";
            public const string XCASTLABS = "xcastlabs";
            public const string SIPUS = "sipus";
        }

        public static class InjectionNames
        {
            public const string SUPERFISH = "superfish_shopping";
            public const string DEALPLY = "dealply_shopping";
            public const string JOLLYWALLET = "jollywallet_shopping";
            public const string INTER_YIELD = "advertise.com_interyield_newtab";
            public const string REVIZER = "revizer_popup";
            public const string INTEXT = "advertiser.com_intext";
            public const string FIRST_OFFERZ = "firstofferz_shopping";
            public const string ADMEDIA_INTEXT = "admedia_intext";
            public const string ADCASH = "adcash";
            public const string ENGAGEYA = "engageya";
        }

        public static class SiteIds
        {
            public const string QA = "1015";
            public const string SALES = "Sales";
        }
    }
}
