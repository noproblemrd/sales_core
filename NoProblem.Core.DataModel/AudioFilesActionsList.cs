﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public class AudioFilesActionsList : List<KeyValuePair<string, string>>
    {
        public void Add(string type, string value)
        {
            KeyValuePair<string, string> pair = new KeyValuePair<string, string>(type, value);
            base.Add(pair);
        }
    }
}
