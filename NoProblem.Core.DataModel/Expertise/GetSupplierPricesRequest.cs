﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise
{
    public class GetSupplierPricesRequest
    {
        public Guid SupplierId { get; set; }
    }
}
