﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise
{
    public class ExpertiseContainer
    {
        /// <summary>
        /// AccountExpertiseId
        /// </summary>
        public Guid Id { get; set; }
        public Guid ExpertiseId { get; set; }
        public string ExpertiseName { get; set; }
        public decimal? Price { get; set; }
        public int? Ranking { get; set; }
        public bool IsBid { get; set; }
        public decimal MinimumPrice { get; set; }
        public bool ExpertiseIsAuction { get; set; }
    }
}
