﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise
{
    public class ExpertiseInSpecialCategoryContainer
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public bool DisplayInSlider { get; set; }
        public Guid HeadingId { get; set; }
    }
}
