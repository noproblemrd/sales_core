﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise
{
    public class UpdateSupplierPricesRequest : Audit.AuditRequest
    {
        public List<ExpertiseContainer> Expertises { get; set; }
        public Guid SupplierId { get; set; }
        public int RankingNotification { get; set; }
    }
}
