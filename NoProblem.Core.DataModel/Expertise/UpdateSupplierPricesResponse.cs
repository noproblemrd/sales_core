﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise
{
    public class UpdateSupplierPricesResponse
    {
        public int RechargePrice { get; set; }
        public UpdateSupplierPricesStatus Status { get; set; }

        public enum UpdateSupplierPricesStatus
        {
            OK,
            PriceOverRechargePrice
        }
    }
}
