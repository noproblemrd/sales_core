﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise
{
    public class DidYouMeanResponse
    {
        public DidYouMeanResponse()
        {
            Categories = new List<DataModel.PublisherPortal.GuidStringPair>();
        }

        public List<DataModel.PublisherPortal.GuidStringPair> Categories { get; private set; }

        public void AddCategory(string categoryName, Guid categoryId)
        {
            Categories.Add(new PublisherPortal.GuidStringPair()
            {
                Id = categoryId,
                Name = categoryName
            });
        }
    }
}
