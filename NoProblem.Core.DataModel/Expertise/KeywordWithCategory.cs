﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise
{
    public class KeywordWithCategory
    {
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Keyword { get; set; }
    }

    public class Category
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
