﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise.Campaigns
{
    public class BannerCampaignData
    {
        public int BannerHeight { get; set; }
        public int BannerWidth { get; set; }
        public string Title { get; set; }
        public string HtmlCode { get; set; }
        public string DestinationPage { get; set; }
    }
}
