﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise.Campaigns
{
    public class HeadingIdAndIsAvailable
    {
        public Guid HeadingId { get; set; }
        public bool IsSomeoneAvailable { get; set; }
    }
}
