﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise.Campaigns
{
    public class CampaignData
    {
        public BannerCampaignData Banner { get; set; }
        public PhoneDidSliderCampaignData PhoneDid { get; set; }
        public Guid HeadingId { get; set; }
        public string HeadingCode { get; set; }
        public string HeadingName { get; set; }
        public bool UseBannerCampaignsLandingPage { get; set; }
        public Xrm.new_primaryexpertise.eCampaignMode Campaign { get; set; }

        public override int GetHashCode()
        {
            return HeadingId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            CampaignData other = (CampaignData)obj;
            return other.HeadingId == this.HeadingId;
        }
    }
}
