﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise.Campaigns
{
    public class PhoneDidSliderCampaignData
    {
        public PhoneDidSliderCampaignData()
        {
            DIDs = new List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>();
            WorkingHoursData = new List<WorkingHoursData>();
        }

        public string TitleOther { get; set; }
        public string TitleOtherSecond { get; set; }
        public bool UseTitleOther { get; set; }
        public bool UseTitleOtherSecond { get; set; }
        public List<PublisherPortal.GuidStringPair> DIDs { get; set; }
        public string Image { get; set; }
        public List<WorkingHoursData> WorkingHoursData { get; set; }
        public string TimeZoneStandardName { get; set; }
        public bool IsRealTimeRouting { get; set; }
    }
}
