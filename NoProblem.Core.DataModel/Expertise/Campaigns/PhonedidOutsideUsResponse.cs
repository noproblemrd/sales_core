﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise.Campaigns
{
    public class PhonedidOutsideUsResponse
    {
        public string number { get; set; }
        public Guid OriginId { get; set; }
        public string Country { get; set; }
        public string ExpertiseCode { get; set; }
        public Guid ExpertiseId { get; set; }
    }
    
}
