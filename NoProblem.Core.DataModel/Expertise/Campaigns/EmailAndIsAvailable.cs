﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise.Campaigns
{
    public class EmailAndIsAvailable
    {
        public string Email { get; set; }
        public bool IsAvailable { get; set; }
    }
}
