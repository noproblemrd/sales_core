﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise.Campaigns
{
    public class WorkingHoursData
    {
        public DayOfWeek DayOfWeek { get; set; }
        public int From { get; set; }
        public int To { get; set; }
    }
}
