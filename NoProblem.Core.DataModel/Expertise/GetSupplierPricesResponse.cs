﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise
{
    public class GetSupplierPricesResponse
    {
        public int StageInRegistration { get; set; }
        public List<ExpertiseContainer> Expertises { get; set; }
        public int RankingNotification { get; set; }
    }
}
