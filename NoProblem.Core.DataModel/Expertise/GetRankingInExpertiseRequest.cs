﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Expertise
{
    public class GetRankingInExpertiseRequest
    {
        public Guid ExpertiseId { get; set; }
        public decimal Price { get; set; }
        public Guid SupplierId { get; set; }
    }
}
