﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.DataModel.Expertise
{
    public class CategoryGroupContainer
    {
        public string Name { get; set; }
        public Guid CategoryGroupId { get; set; }
        public List<GuidStringPair> Headings { get; set; }

        public override bool Equals(object obj)
        {
            CategoryGroupContainer other = (CategoryGroupContainer)obj;
            return other.CategoryGroupId == this.CategoryGroupId;
        }

        public override int GetHashCode()
        {
            return CategoryGroupId.GetHashCode();
        }
    }
}
