﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AjaxHandlersContainers
{
    public abstract class AjaxResponseBase
    {
        private const string OK = "OK";

        public AjaxResponseBase()
        {
            Success = true;
            Message = OK;
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
