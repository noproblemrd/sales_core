﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public class AudioFilesConsts
    {
        public const string VARIABLE_PREFIX = "VAR_";
        public const string HEADING_POSTFIX = "HEADING";
        public const string DESCRIPTION_POSTFIX = "DESCRIPTION";
        public const string LEADS_LEFT_POSTFIX = "LEADSLEFT";
        public const string FILE_ACTION = "FILE";
        public const string TTS_ACTION = "TTS";
        public const char SPLITTER = ';';
        public const string HEADING_RECORDINGS_FOLDER = "headings";
        public const string NATURAL_NUMBERS_RECORDINGS_FOLDER = "naturalnumbers";
        public const string SLASH = "/";
        public const string MP3_FILE_EXTENSION = ".mp3";
        public const string WAV_FILE_EXTENSION = ".wav";
        public const string TEST_CALL_RECORDINGS_FOLDER = "testcall";
        public const string WAITING_MUSIC = "/waitingMusic.mp3";
        public const string INCOMING_CALLS = "incomingcalls";
        public const string FAKE_RING = "/fakering.wav";
        public const string SHARON = "sharon";
        public const string SHIELA = "shiela";
        public const string MICHAEL = "michael";
        public const string AAR = "AARText";
        public const string MOBILE = "mobile";
        public static string GetAarFolder
        {
            get
            {
                return SLASH + AAR + SLASH;
            }
        }
    }
}
