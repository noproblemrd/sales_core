﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class SupplierStatusContainer
    {
        public bool IsAvailableHours { get; set; }
        public SupplierStatus SupplierStatus { get; set; }
        public bool CanGoOnDuty { get; set; }
        public Guid UnavailabilityReasonId { get; set; }
        public string UnavailabilityReasonName { get; set; }
        public UnavailavilityTypes? UnavailavilityType { get; set; }
        public int DaysToEndOfOverRefundPercent { get; set; }
        public bool FistPaymentNeeded { get; set; }
        public decimal Balance { get; set; }
        public bool IsInactiveWithMoney { get; set; }
        public bool PaymentMethodFailed { get; set; }
        public bool UpdatedDefaultPrices { get; set; }
        public bool EmailJustVerified { get; set; }
    }

    public enum UnavailavilityTypes
    {
        UserCreated,
        OverRefundPercent,
        OutOfMoney,
        DailyBudget
    }
}
