﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class MiniSiteData
    {
        public int NumberOfEmployees { get; set; }
        public string LongDescription { get; set; }
        public string VideoUrl { get; set; }
        public string Name { get; set; }
        public int SumAssistanceRequests { get; set; }
        public string StreetName { get; set; }
        public string HouseNum { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string Zipcode { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
    }
}
