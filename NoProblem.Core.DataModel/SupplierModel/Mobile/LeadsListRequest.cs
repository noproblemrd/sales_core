﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Mobile
{
    public class LeadsListRequest
    {
        public Guid SupplierId { get; set; }
        public SupplierLeadStatus supplierLeadStatus { get; set; }
        /*
        public bool All { get; set; }
        public bool OnlyWon { get; set; }
         * */
        public int PageSize { get; set; }
        public int PageNumber { get; set; }

        public enum SupplierLeadStatus
        {
            NEW,
            WIN,
            LOST,
            ANY
        }
    }
}
