﻿using System;
using System.Collections.Generic;

namespace NoProblem.Core.DataModel.SupplierModel.Mobile
{
    public class GetLeadForMobileResponse
    {
        public GetLeadForMobileResponse()
        {
            Recordings = new List<RecordingInLead>();
        }

        public string Address { get; set; }

        public string ZipCode { get; set; }

        public bool CanRefund { get; set; }
        
        public SupplierCategory Category { get; set; }

        public string CustomerName { get; set; }

        public string EvaluatingText { get; set; }

        public bool IsFree { get; set; }

        public Guid IncidentAccountId { get; set; }

        public string LostReason { get; set; }

        public string PhoneNumber { get; set; }

        public string PreviewVideoImageUrl { get; set; }

        public decimal Price { get; set; }

        public List<RecordingInLead> Recordings { get; set; }

        public string Status { get; set; }

        public DateTime CreatedOn { get; set; }

        public int Videoduration { get; set; }

        public string VideoUrl { get; set; }

        public decimal WinPrice { get; set; }
        public string CustomerProfileImage { get; set; }
        
    }

    public class RecordingInLead
    {
        public DateTime CreatedOn { get; set; }

        public int Duration { get; set; }

        public string Url { get; set; }
    }

    public class SupplierCategory
    {
        public decimal MaxPrice { get; set; }

        public decimal MediumPrice { get; set; }

        public decimal MinPrice { get; set; }

        public string Name { get; set; }
    }
}
