﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Mobile
{
    public class SetBidForLeadRequest
    {
        public Guid SupplierId { get; set; }
        public Guid IncidentAccountId { get; set; }
        public decimal Price { get; set; }
        public string Status { get; set; }
        public string IP { get; set; }
        public string UserAgent { get; set; }
        public string Host { get; set; }

        public class StatusOptions
        {
            public const string ACCEPTED = "ACCEPTED";
            public const string REJECTED = "REJECTED";
        //    public const string MISSED = "MISSED";
        }
    }
}
