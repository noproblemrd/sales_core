﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Mobile
{
    public class BusinessDescriptionResponse
    {
        public string description { get; set; }
        public string licenseId { get; set; }
    }
}
