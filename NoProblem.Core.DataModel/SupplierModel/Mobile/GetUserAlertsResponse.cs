﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Mobile
{
    public class GetUserAlertsResponse
    {
        public List<Alert> Alerts { get; set; }

        public GetUserAlertsResponse()
        {
            Alerts = new List<Alert>();
        }

        public class Alert
        {
            public string Type { get; set; }
            public string Text { get; set; }
        }
    }
}
