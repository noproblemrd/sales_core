﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Mobile
{
    public class SetBidForLeadResponse
    {
        public SetBidForLeadResponse()
        {
            Ok = true;
        }

        public bool Ok { get; set; }
        public string FaildMessage { get; set; }
        public class BidForLeadResponseMessage
        {
            public const string DUPLICATE = "DUPLICATE";
            public const string INCIDENT_GENERAL_PROBLEM = "PROBLEMATIC LEAD";
            public const string INCIDENT_NOT_FOUND = "LEAD NOT FOUND";
            public const string MISSED = "MISSED";
            public const string DONT_HAVE_FREE_LEAD = "DONT HAVE FREE LEAD";
        }
    }
}
