﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Mobile
{
    public class LeadsListRequestV2
    {
        public Guid SupplierId { get; set; }       
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
}
