﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Mobile
{
    public class LeadsListResponse
    {
        public List<LeadsListLeadEntry> Leads { get; set; }

        public class LeadsListLeadEntry
        {
    //        public string Description { get; set; }
            public string CategoryName { get; set; }
            public string Region { get; set; }
            public DateTime CreatedOn { get; set; }
            public string Status { get; set; }
  //          public decimal LostBy { get; set; }
            public Guid IncidentAccountId { get; set; }
            public string VideoUrl { get; set; }
            public string PreviewVideoImageUrl { get; set; }
            public int Videoduration { get; set; }
            public string CustomerProfileImage { get; set; }
            public string WonTitle { get; set; }
        }
    }
}
