﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Mobile
{
    public class GetLeadForMobileRequest
    {
        public Guid SupplierId { get; set; }
        public Guid IncidentAccountId { get; set; }
    }
}
