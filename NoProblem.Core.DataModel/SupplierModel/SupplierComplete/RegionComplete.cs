﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.SupplierComplete
{
    public class RegionComplete
    {
        public string Code { get; set; }
        public int Level { get; set; }
    }
}
