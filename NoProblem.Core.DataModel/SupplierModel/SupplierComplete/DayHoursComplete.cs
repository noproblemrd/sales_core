﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.SupplierComplete
{
    public class DayHoursComplete
    {
        public DayOfWeek DayOfWeek { get; set; }
        public int FromHour { get; set; }
        public int ToHour { get; set; }
    }
}
