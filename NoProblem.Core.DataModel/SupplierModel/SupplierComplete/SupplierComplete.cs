﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.SupplierComplete
{
    public class SupplierComplete
    {
        public bool IsUpdate { get; set; }
        public Guid SupplierId { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public List<HeadingComplete> Headings { get; set; }
        public List<RegionComplete> Regions { get; set; }
        public int PaymentAmount { get; set; }
        public List<DayHoursComplete> DaysAvailability { get; set; }
    }
}
