﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.SupplierComplete
{
    public class HeadingComplete
    {
        public string Code { get; set; }
        public int Price { get; set; }
    }
}
