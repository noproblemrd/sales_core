﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class AdvertiserGeneralInfo
    {
        public string LongDescription { get; set; }
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public int NumberOfEmployees { get; set; }
        public int Complaints { get; set; }
        public int WeightedScore { get; set; }
        public int SurveyGradeCounter { get; set; }
        public string Certificate2 { get; set; }
        public string ShortDescription { get; set; }
        public int SumAssistanceRequests { get; set; }
        public string VideoUrl { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactPhone { get; set; }
        public string CompanyPhone { get; set; }
        public string SmsNumber { get; set; }
        public string Fax{ get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public Guid StateId { get; set; }
        public string CityName { get; set; }
        public Guid CityId { get; set; }
        public string DistrictName { get; set; }
        public Guid DistrictId { get; set; }
        public string StreetName { get; set; }
        public Guid StreetId { get; set; }
        public string HouseNum { get; set; }
        public string Zipcode { get; set; }
        public int TimeZone { get; set; }
        public string CompanyId { get; set; }
        public string CompanySite { get; set; }
        public int MyProperty { get; set; }
        public int RegistrationStage { get; set; }
        public int PublisherRanking { get; set; }
        public bool AllowRecordings { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
        public string Password { get; set; }
        public Guid ManagerId { get; set; }
        public string BizId { get; set; }
        public bool IsRarahQuote { get; set; }
        public bool DoNotSendSMS { get; set; }
        public DataModel.Xrm.account.DapazStatus DapazStatus { get; set; }
        public DataModel.Xrm.account.FundsStatus FundsStatus { get; set; }
    }
}
