﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class ExpertiseData
    {
        public string ExpertiseName { get; set; }
        public string Code { get; set; }
        public int Level { get; set; }
        public int SupplierCount { get; set; }
    }
}
