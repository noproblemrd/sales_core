﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class SupplierSignUpRequest
    {
        public string MobilePhone { get; set; }
        public string ReferralCode { get; set; }
        public string Email { get; set; }
    }
}
