﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class SupplierIncidentData
    {
        public bool? IsJobDone { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string RefundNote { get; set; }
        public string RefundNoteForAdvertiser { get; set; }
        public Guid CustomerId { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public decimal WinningPrice { get; set; }
        public bool Won { get; set; }
        public Xrm.new_incidentaccount.RefundSatus? RefundStatus { get; set; }
        public int? RefundReason { get; set; }
        public Guid IncidentId { get; set; }
        public string TicketNumber { get; set; }
        public bool ToCharge { get; set; }
        public RefundProcessState refundProcessState { get; set; }
        public bool RecordsCalls { get; set; }
        public int RefundDaysBackLimit { get; set; }

        public SupplierIncidentData()
        {

        }

        public SupplierIncidentData(SupplierIncidentDataExpanded other)
        {
            this.IsJobDone = other.IsJobDone;
            this.Description = other.Description;
            this.Price = other.Price;
            this.RefundNote = other.RefundNote;
            this.RefundNoteForAdvertiser = other.RefundNoteForAdvertiser;
            this.CustomerId = other.CustomerId;
            this.CustomerPhoneNumber = other.CustomerPhoneNumber;
            this.WinningPrice = other.WinningPrice;
            this.Won = other.Won;
            this.RefundStatus = other.RefundStatus;
            this.RefundReason = other.RefundReason;
            this.IncidentId = other.IncidentId;
            this.TicketNumber = other.TicketNumber;
            this.ToCharge = other.ToCharge;
            this.refundProcessState = other.refundProcessState;
            this.RecordsCalls = other.RecordsCalls;
        }
    }

    public enum RefundProcessState
    {
        OK,
        OverPercentageLimit,
        OverDaysLimit,
        NoPhoneConnection,
        NoRecording,
        NotAvailable
    }

    public class SupplierIncidentDataExpanded : SupplierIncidentData
    {
        public bool IsRefundBlocked { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool HasRecording { get; set; }
        //public bool IsCallChannel { get; set; }
        //public bool IsInvitedBySMS { get; set; }
        public Guid SupplierId { get; set; }
    }
}
