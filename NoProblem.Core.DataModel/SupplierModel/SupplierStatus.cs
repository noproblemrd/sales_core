﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public enum SupplierStatus
    {
        Inactive,
        Candidate,
        Available,
        Unavailable,
        Trial,
        Rarah
    }
}
