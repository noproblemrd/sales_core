﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class SupplierSignUpResponse
    {
        public SupplierSignUpStatus SignUpStatus { get; set; }
        public Guid SupplierId { get; set; }
    }

    public enum SupplierSignUpStatus
    {
        OkIsAar,
        OkIsNew,
        TransferToLogin,
        EmailDuplicate,
        ReferralCodeNotFound,
        ReferralCodeAlreadyUsed
    }
}
