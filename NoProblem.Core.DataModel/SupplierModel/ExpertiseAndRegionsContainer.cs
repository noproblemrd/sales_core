﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public class ExpertiseAndRegionsContainer
    {
        public Guid RelationId { get; set; }
        public int StateCode { get; set; }
        public Guid RegionId { get; set; }
        public bool IsCompleted { get; set; }
    }
}
