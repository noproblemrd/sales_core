﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public class ExpertiseContainer
    {
        public bool AllreadyHandled { get; set; }
        public bool IsActive { get; set; }
        public Guid AccountExpertiseId { get; set; }
        public Guid PrimaryExpertiseId { get; set; }
        public List<Guid> SecondaryExpertiseIds { get; set; }
        public decimal IncidentPrice { get; set; }
    }
}
