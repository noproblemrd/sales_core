﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class DeactivateSupplierResponse
    {
        public bool IsInactiveWithMoney { get; set; }
    }
}
