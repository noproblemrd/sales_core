﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class CheckEmailInRegistrationRequestResponse
    {
        public eEmailCheck EmailCheckResult { get; set; }

        public enum eEmailCheck
        {
            Continue,
            Login
        }
    }
}
