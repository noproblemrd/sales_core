﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class DeleteAccountRequest
    {
        public Guid SupplierId { get; set; }
        public string Reason { get; set; }
        public string ReasonInYourOwnWords { get; set; }
        public string Password { get; set; }
    }
}
