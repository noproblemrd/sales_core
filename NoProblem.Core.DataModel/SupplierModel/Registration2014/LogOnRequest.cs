﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public abstract class LogOnRequest
    {
        public string Email { get; set; }
        public Guid ReferrerId { get; set; }
        public string ComeFrom { get; set; }
        public Guid RegistrationHitId { get; set; }
        public bool IsFromMobileApp { get; set; }
        public Mobile.MobileLogOnData MobileLogOnData { get; set; }
    }
}
