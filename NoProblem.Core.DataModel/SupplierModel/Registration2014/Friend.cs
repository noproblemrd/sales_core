﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class Friend
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
