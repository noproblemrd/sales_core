﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class SendInvitationEmailsRequest
    {
        public List<Friend> Friends { get; set; }
        public Guid SupplierId { get; set; }
    }
}
