﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public abstract class RegistrationRequest
    {
        public Guid SupplierId { get; set; }
    }
}
