﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class LandedRegistraionPageRequest
    {
        public Guid ID { get; set; }
        public string IP { get; set; }
        public string UrlReferer { get; set; }
        public string Browser { get; set; }
        public string BrowserVersion { get; set; }
        public string Flavor { get; set; }
        public DateTime date { get; set; }
        public eAdvertiserRegistrationLandingPageType LandingPage { get; set; }
        public LandedRegistraionPageRequest()
        {
        }
        
    }
    public enum eAdvertiserRegistrationLandingPageType
    {
        A,
        B,
        C,
        D
    }
}
