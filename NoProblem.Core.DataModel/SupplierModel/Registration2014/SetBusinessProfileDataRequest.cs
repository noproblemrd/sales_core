﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class SetBusinessProfileDataRequest
    {
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public string FullAddress { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public string ContactPerson { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string About { get; set; }
        public int? NumberOfEmployees { get; set; }
        public List<string> Videos { get; set; }
        public bool PayWithAMEX { get; set; }
        public bool PayWithMASTERCARD { get; set; }
        public bool PayWithVISA { get; set; }
        public bool PayWithCASH { get; set; }

        public SetBusinessProfileDataRequest()
        {
            Videos = new List<string>();
        }
    }
}
