﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class ChoosePlanRequest
    {
        public Guid SupplierId { get; set; }
        /// <summary>
        /// One of the constants in SubscriptionPlansContract.Plans
        /// </summary>
        public string PlanSelected { get; set; }       
    }
}
