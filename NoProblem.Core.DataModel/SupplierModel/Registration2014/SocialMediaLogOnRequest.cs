﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class SocialMediaLogOnRequest : LogOnRequest
    {
        public string FacebookId { get; set; }
        public string GoogleId { get; set; }
        public string ContactName { get; set; }
    }
}
