﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class LogOnResponse
    {
        public Guid SupplierId { get; set; }
        public int Step { get; set; }
        public string StatusCode { get; set; }
        public string ContactName { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public bool HasFacebook { get; set; }
        public bool HasGoogle { get; set; }
        public string ApiToken { get; set; }

        public LogOnResponse()
        {
            StatusCode = StatusCodes.OK;
        }

        public static class StatusCodes
        {
            public const string OK = "OK";
            public const string WRONG_EMAIL_PASSWORD_COMBINATION = "WRONG_EMAIL_PASSWORD_COMBINATION";
            public const string ANOTHER_ACCOUNT_ALREADY_HAS_THIS_EMAIL = "ANOTHER_ACCOUNT_ALREADY_HAS_THIS_EMAIL";
            public const string INACTIVE_ACCOUNT = "INACTIVE_ACCOUNT";
            public const string SEND_TO_LEGACY_PROCESS = "SEND_TO_LEGACY_PROCESS";
        }
    }
}
