﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    [System.Xml.Serialization.XmlRoot("Areas")]
    public class GetCoverAreaResponse
    {
        private readonly List<AreaNode> areasList = new List<AreaNode>();

        [System.Xml.Serialization.XmlElement("Area")]
        public List<AreaNode> AreasList { get { return areasList; } }

        [System.Xml.Serialization.XmlAttribute("SubscriptionPlan")]
        public String SubscriptionPlan { get; set; }

        [System.Xml.Serialization.XmlType("Area")]
        public class AreaNode
        {
            [System.Xml.Serialization.XmlAttribute()]
            public decimal Longitude { get; set; }
            [System.Xml.Serialization.XmlAttribute()]
            public decimal Latitude { get; set; }
            [System.Xml.Serialization.XmlAttribute()]
            public string Radius { get; set; }
            [System.Xml.Serialization.XmlAttribute()]
            public string FullAddress { get; set; }
            [System.Xml.Serialization.XmlAttribute()]
            public int NumberOfZipcodes { get; set; }
            [System.Xml.Serialization.XmlAttribute()]
            public int Order { get; set; }

            public List<SelectedArea> SelectedAreas { get; set; }

            public AreaNode()
            {
                SelectedAreas = new List<SelectedArea>();
            }
        }

        [System.Xml.Serialization.XmlType("LargeArea")]
        public class SelectedArea
        {
            [System.Xml.Serialization.XmlAttribute()]
            public string RegionId { get; set; }
            [System.Xml.Serialization.XmlAttribute()]
            public string Name { get; set; }
            [System.Xml.Serialization.XmlAttribute()]
            public string Level { get; set; }
        }
    }
}
