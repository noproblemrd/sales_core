﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014.Mobile
{
    public class SupplierLogoUploadRequest
    {
        public Guid SupplierId { get; set; }
        public string ImageUrl { get; set; }
        public string SiteId { get; set; }
    }
}
