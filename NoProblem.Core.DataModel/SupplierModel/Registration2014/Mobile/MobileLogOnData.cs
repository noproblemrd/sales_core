﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014.Mobile
{
    public class MobileLogOnData
    {
        public string DeviceUId { get; set; }
        public string DeviceOS { get; set; }
        public string DeviceName { get; set; }
        public string DeviceOSVersion { get; set; }
        public string NpAppVersion { get; set; }
        public bool isDebug { get; set; }
    }
}
