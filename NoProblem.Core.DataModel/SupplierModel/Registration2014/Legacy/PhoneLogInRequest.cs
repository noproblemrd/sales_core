﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014.Legacy
{
    public class PhoneLogInRequest
    {
        public string Phone { get; set; }
        public string Password { get; set; }
    }
}
