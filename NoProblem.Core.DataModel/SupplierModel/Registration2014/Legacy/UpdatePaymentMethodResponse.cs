﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014.Legacy
{
    public class UpdatePaymentMethodResponse : LegacyResponseBase
    {
        public string Email { get; set; }
    }
}
