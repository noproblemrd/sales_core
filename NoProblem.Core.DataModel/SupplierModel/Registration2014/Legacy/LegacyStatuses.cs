﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014.Legacy
{
    public static class LegacyStatuses
    {
        public const string SEND_TO_LEGACY_STEP = "SEND_TO_LEGACY_STEP";
        public const string SEND_TO_NORMAL_STEP = "SEND_TO_NORMAL_STEP";
        public const string PHONE_PASSWORD_COMBINATION_NOT_FOUND = "PHONE_PASSWORD_COMBINATION_NOT_FOUND";
        public const string MORE_THAN_ONE_ACCOUNT_FOUND_SEND_TO_EMAIL_LOGIN = "MORE_THAN_ONE_ACCOUNT_FOUND_SEND_TO_EMAIL_LOGIN";
        public const string NOT_LEGACY_SEND_TO_EMAIL_LOGIN = "NOT_LEGACY_SEND_TO_EMAIL_LOGIN";
        public const string LEGACY_FINISHED_SEND_TO_EMAIL_LOGIN = "LEGACY_FINISHED_SEND_TO_EMAIL_LOGIN";
    }
}
