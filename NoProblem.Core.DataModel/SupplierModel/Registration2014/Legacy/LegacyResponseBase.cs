﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014.Legacy
{
    public abstract class LegacyResponseBase
    {
        public string LegacyStatus { get; set; }
        public string StepLegacy { get; set; }
        public Guid SupplierId { get; set; }
        public int Step { get; set; }
        public string SubscriptionPlan { get; set; }
        public string ContactName { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
    }
}
