﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014.Legacy
{
    public class SendVerificationEmailResponse
    {
        public string SendEmailStatus { get; set; }

        public static class SendEmailStatuses
        {
            public const string SENT = "SENT";
            public const string FAILED_TO_SEND = "FAILED_TO_SEND";
            public const string DUPLICATE_EMAIL = "DUPLICATE_EMAIL";
        }
    }
}
