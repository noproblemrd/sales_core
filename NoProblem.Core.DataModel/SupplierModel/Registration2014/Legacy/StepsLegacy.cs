﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014.Legacy
{
    public static class StepsLegacy
    {
        public const string CONFIRM_EMAIL_1 = "1_CONFIRM_EMAIL";
        public const string PAYMENT_METHOD_2 = "2_PAYMENT_METHOD";
        public const string FINISHED_3 = "3_FINISHED";
    }
}
