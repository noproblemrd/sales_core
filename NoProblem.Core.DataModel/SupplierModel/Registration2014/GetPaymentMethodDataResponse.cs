﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class GetPaymentMethodDataResponseInner
    {
        public string Last4Digits { get; set; }
        public string ExpDate { get; set; }
        public string CcOwnerName { get; set; }
        public string BillingAddress { get; set; }
        public string CreditCardType { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
       
    }
    public class GetPaymentMethodDataResponse
    {
        public GetPaymentMethodDataResponseInner Data { get; set; }
        public bool IsActive { get; set; }
    }
}
