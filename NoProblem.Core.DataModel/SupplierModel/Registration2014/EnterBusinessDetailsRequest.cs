﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class EnterBusinessDetailsRequest
    {
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public string StreetAddress { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ContactPersonName { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string ReferralCode { get; set; }
    }
}
