﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public static class SubscriptionPlansContract
    {
        public static class Plans
        {
            public const string FEATURED_LISTING = "FEATURED_LISTING";
            public const string LEAD_BOOSTER_AND_FEATURED_LISTING = "LEAD_BOOSTER_AND_FEATURED_LISTING";
            public const string CLIP_CALL_SUBSCRIPTION = "CLIP_CALL_SUBSCRIPTION";

        }
    }
}
