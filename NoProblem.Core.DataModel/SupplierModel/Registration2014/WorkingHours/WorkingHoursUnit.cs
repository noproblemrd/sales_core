﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014.WorkingHours
{
    public class WorkingHoursUnit
    {
        public int FromHour { get; set; }
        public int FromMinute { get; set; }
        public int ToHour { get; set; }
        public int ToMinute { get; set; }
        public bool AllDay { get; set; }
        public List<DayOfWeek> DaysOfWeek { get; set; }
        public int Order { get; set; }

        public WorkingHoursUnit()
        {
            DaysOfWeek = new List<DayOfWeek>();
        }

        public WorkingHoursUnit(Xrm.new_workinghoursunit item)
            : this()
        {
            FromHour = item.new_fromhour.Value;
            ToHour = item.new_tohour.Value;
            FromMinute = item.new_fromminute.Value;
            ToMinute = item.new_tominute.Value;
            AllDay = item.new_allday.Value;
            if (item.new_sunday.Value)
                DaysOfWeek.Add(DayOfWeek.Sunday);
            if (item.new_monday.Value)
                DaysOfWeek.Add(DayOfWeek.Monday);
            if (item.new_tuesday.Value)
                DaysOfWeek.Add(DayOfWeek.Tuesday);
            if (item.new_wednesday.Value)
                DaysOfWeek.Add(DayOfWeek.Wednesday);
            if (item.new_thursday.Value)
                DaysOfWeek.Add(DayOfWeek.Thursday);
            if (item.new_friday.Value)
                DaysOfWeek.Add(DayOfWeek.Friday);
            if (item.new_saturday.Value)
                DaysOfWeek.Add(DayOfWeek.Saturday);
            Order = item.new_order.Value;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is WorkingHoursUnit))
                return false;
            WorkingHoursUnit other = (WorkingHoursUnit)obj;
            return
                this.AllDay == other.AllDay
                && this.FromHour == other.FromHour
                && this.FromMinute == other.FromMinute
                && this.ToHour == other.ToHour
                && this.ToMinute == other.ToMinute;
        }
    }
}
