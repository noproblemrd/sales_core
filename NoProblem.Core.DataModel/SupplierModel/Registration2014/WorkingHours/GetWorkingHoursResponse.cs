﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014.WorkingHours
{
    public class GetWorkingHoursResponse
    {
        public List<WorkingHoursUnit> Units { get; set; }
        public DateTime? VacationFrom { get; set; }
        public DateTime? VacationTo { get; set; }

        public GetWorkingHoursResponse()
        {
            Units = new List<WorkingHoursUnit>();
        }
    }
}
