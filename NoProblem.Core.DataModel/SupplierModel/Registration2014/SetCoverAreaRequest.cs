﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class SetCoverAreasRequest : RegistrationRequest
    {
        public List<NoProblem.Core.DataModel.SupplierModel.Registration2014.SetCoverAreasRequest.AreaRequest> Areas { get; set; }

        public class AreaRequest
        {
            public string Address { get; set; }
            public decimal Latitude { get; set; }
            public decimal Longitude { get; set; }
            public string Radius { get; set; }
            public int NumberOfZipcodes { get; set; }
            public string SelectedArea1Name { get; set; }
            public string SelectedArea1Id { get; set; }
            public string SelectedArea2Name { get; set; }
            public string SelectedArea2Id { get; set; }
            public string SelectedArea3Name { get; set; }
            public string SelectedArea3Id { get; set; }
            public int Order { get; set; }
        }
    }
}
