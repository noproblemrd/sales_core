﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class SubscriptionCostAndBonus
    {
        public decimal Cost { get; set; }
        public decimal Bonus { get; set; }
    }
}
