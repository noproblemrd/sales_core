﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class SetBussinessAddressRequest : RegistrationRequest
    {
        public string BusinessAddress { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
    }
}
