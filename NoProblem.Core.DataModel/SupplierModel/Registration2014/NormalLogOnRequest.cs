﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class NormalLogOnRequest : LogOnRequest
    {
        public string Password { get; set; }
    }
}
