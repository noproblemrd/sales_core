﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel.Registration2014
{
    public class GetBusinessDetailsResponse
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string Phone { get; set; }
        public string SubscriptionPlan { get; set; }
        public string ContactPersonName { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string ReferralCode { get; set; }
    }
}
