﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class UpdateJobDoneRequest
    {
        public Guid CallId { get; set; }
        public bool? IsJobDone { get; set; }
    }
}
