﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public class RegionMap
    {
        public bool Handled { get; set; }
        public Guid RegionId { get; set; }
        public Guid ParentRegionId { get; set; }
    }
}
