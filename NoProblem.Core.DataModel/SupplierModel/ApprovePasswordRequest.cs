﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class ApprovePasswordRequest
    {
        public Guid SupplierId { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
