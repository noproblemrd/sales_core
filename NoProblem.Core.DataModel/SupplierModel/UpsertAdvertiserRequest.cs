﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class UpsertAdvertiserRequest : Audit.AuditRequest
    {
        public Guid SupplierId { get; set; }
        [CrmField("emailaddress1")]
        public string Email { get; set; }
        [CrmField("telephone1")]
        public string ContactPhone { get; set; }
        [CrmField("new_videourl")]
        public string VideoUrl { get; set; }
        [CrmField("description")]
        public string LongDescription { get; set; }
        [CrmField("new_description")]
        public string ShortDescription { get; set; }
        [CrmField("new_firstname")]
        public string FirstName { get; set; }
        [CrmField("new_lastname")]
        public string LastName { get; set; }
        [CrmField("numberofemployees")]
        public int? NumberOfEmployees { get; set; }
        [CrmField("name")]
        public string Company { get; set; }
        [CrmField("telephone2")]
        public string CompanyPhone { get; set; }
        [CrmField("fax")]
        public string Fax { get; set; }
        [CrmField("address1_country")]
        public string CountryName { get; set; }
        [CrmField("address1_stateorprovince")]
        public string StateName { get; set; }
        [CrmField("new_address1_stateorprovince_regionid")]
        public Guid? StateId { get; set; }
        [CrmField("address1_city")]
        public string CityName { get; set; }
        [CrmField("new_address1_city_regionid")]
        public Guid? CityId { get; set; }
        [CrmField("address1_county")]
        public string DistrictName { get; set; }
        [CrmField("new_address1_county_regionid")]
        public Guid? DistrictId { get; set; }
        [CrmField("address1_line1")]
        public string StreetName { get; set; }
        [CrmField("new_address1_line1_regionid")]
        public Guid? StreetId { get; set; }
        [CrmField("address1_line2")]
        public string HouseNum { get; set; }
        [CrmField("address1_postalcode")]
        public string Zipcode { get; set; }
        [CrmField("address1_utcoffset")]
        public int? TimeZone { get; set; }
        [CrmField("new_accountgovernmentid")]
        public string CompanyId { get; set; }
        [CrmField("websiteurl")]
        public string CompanySite { get; set; }
        [CrmField("new_publisherranking")]
        public int? PublisherRanking { get; set; }
        [CrmField("new_securitylevel")]
        public int? SecurityLevel { get; set; }
        [CrmField("new_custom1")]
        public string Custom1 { get; set; }
        [CrmField("new_custom2")]
        public string Custom2 { get; set; }
        [CrmField("new_custom3")]
        public string Custom3 { get; set; }
        [CrmField("new_custom4")]
        public string Custom4 { get; set; }
        [CrmField("new_custom5")]
        public string Custom5 { get; set; }
        [CrmField("new_recordcalls")]
        public bool? AllowRecordings { get; set; }
        [CrmField("new_managerid")]
        public Guid? ManagerId { get; set; }
        [CrmField("new_affiliateoriginid")]
        public Guid? AffiliateOriginId { get; set; }
        [CrmField("new_password")]
        public string Password { get; set; }
        [CrmField("new_canhearallrecordings")]
        public bool CanHearAllRecordings { get; set; }
        [CrmField("new_fulladdress")]
        public string FullAddress { get; set; }
        public bool IsFromAAR { get; set; }
        public bool IsFromAdvertiserPortal { get; set; }
        [CrmField("new_bizid")]
        public string BizId { get; set; }
        [CrmField("telephone3")]
        public string SmsNumber { get; set; }
        public NoProblem.Core.DataModel.Dapaz.DapazStatusChangeOptions? NewDapazStatus { get; set; }
        [CrmField("new_fundsstatus")]
        public DataModel.Xrm.account.FundsStatus FundsStatus { get; set; }
        [CrmField("new_userbizid")]
        public string UserBizId { get; set; }
        [CrmField("new_donotsendsms")]
        public bool? DoNotSendSms { get; set; }
    }

    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class CrmFieldAttribute : System.Attribute
    {
        public string field;

        public CrmFieldAttribute(string field)
        {
            this.field = field;
        }
    }
}
