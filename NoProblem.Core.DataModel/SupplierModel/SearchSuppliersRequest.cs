﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class SearchSuppliersRequest : PagingRequest
    {
        public string SearchValue { get; set; }
    }
}
