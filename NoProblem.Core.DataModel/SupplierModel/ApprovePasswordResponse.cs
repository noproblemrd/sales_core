﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class ApprovePasswordResponse
    {
        public bool IsApproved { get; set; }
        public Xrm.account.TrialStatusReasonCode? TrialStatusReasonCode { get; set; }
        public decimal Balance { get; set; }
    }
}
