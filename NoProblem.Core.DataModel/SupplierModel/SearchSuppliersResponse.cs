﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class SearchSuppliersResponse : PagingResponse
    {
        public List<SupplierSearchRow> Rows { get; set; }
    }
}
