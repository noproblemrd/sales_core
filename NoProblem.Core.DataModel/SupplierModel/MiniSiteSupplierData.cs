﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class MiniSiteSupplierData
    {
        public Guid AccountId { get; set; }
        public string Name { get; set; }
        public string Video { get; set; }
        public string DirectNumber { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public int SurveyNumber { get; set; }
        public int EmployeeNumber { get; set; }
        public int CallsNumber { get; set; }
        public List<string> Expertises { get; set; }
        public List<string> Regions { get; set; }
        public bool Certificate { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
    }
}
