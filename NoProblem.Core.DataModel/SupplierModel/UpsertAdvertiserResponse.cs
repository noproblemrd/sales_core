﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class UpsertAdvertiserResponse
    {
        public Guid SupplierId { get; set; }
        public UpsertAdvertiserStatus Status { get; set; }
    }

    public enum UpsertAdvertiserStatus
    {
        Success,
        EmailDuplicate,
        PhoneDuplicate,
        BizIdDuplicate
    }
}
