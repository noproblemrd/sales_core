﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierModel
{
    public class SupplierSearchRow
    {
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone1 { get; set; }
        public string Email { get; set; }
        public string Number { get; set; }
        public decimal CashBalance { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string ZipCode { get; set; }
        public string Telephone2 { get; set; }
        public string Fax { get; set; }
        public string WebSite { get; set; }
        public int NumberOfEmployees { get; set; }
        public SupplierModel.SupplierStatus SupplierStatus { get; set; }
        public bool IsInactiveWithMoney { get; set; }
        public bool IsGoldenNumberAdvertiser { get; set; }
        public DataModel.Xrm.account.DapazStatus DapazStatus { get; set; }
    }
}
