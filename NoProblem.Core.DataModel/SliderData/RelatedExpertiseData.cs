﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SliderData
{
    public class RelatedExpertiseData
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is RelatedExpertiseData))
            {
                return false;
            }
            RelatedExpertiseData other = (RelatedExpertiseData)obj;
            return this.Code.Equals(other.Code);
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }
    }
}
