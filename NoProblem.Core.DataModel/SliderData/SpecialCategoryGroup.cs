﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SliderData
{
    public class SpecialCategoryGroup
    {
        public string ConnectivityName { get; set; }
        public List<Guid> HeadingIds{ get; set; }

        public override bool Equals(object obj)
        {
            SpecialCategoryGroup other = (SpecialCategoryGroup)obj;
            return this.ConnectivityName.Equals(other.ConnectivityName);
        }

        public override int GetHashCode()
        {
            return ConnectivityName.GetHashCode();
        }
    }
}
