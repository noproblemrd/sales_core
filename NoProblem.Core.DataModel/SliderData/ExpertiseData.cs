﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SliderData
{
    public class ExpertiseData
    {
        public string Code { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Plural { get; set; }
        public string Singular { get; set; }
        public string Watermark { get; set; }
        public string ImageBack { get; set; }
        public string SpeakToButtonName { get; set; }
        public string Slogan { get; set; }
        public string MobileDeviceTitel { get; set; }
        public string MobileDeviceDescription { get; set; }
        public bool InluceGoogleAddon { get; set; }
        public List<RelatedExpertiseData> RelatedExpertises { get; set; }
        public bool Intext { get; set; }
    }
}
