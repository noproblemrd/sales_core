﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SliderData
{
    public class CategoryGroupForDisplay
    {
        public string Name { get; set; }
        public List<string> HeadingCodes { get; set; }

        public override bool Equals(object obj)
        {
            CategoryGroupForDisplay other = (CategoryGroupForDisplay)obj;
            return other.Name == this.Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
