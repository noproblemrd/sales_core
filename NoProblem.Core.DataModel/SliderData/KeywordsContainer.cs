﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SliderData
{
    public class KeywordsContainer
    {
        public List<NoProblem.Core.DataModel.SliderData.KeywordHeadingIdPair> Keywords { get; set; }
        public List<NoProblem.Core.DataModel.SliderData.KeywordHeadingIdPair> NotKeywords { get; set; }

        public KeywordsContainer()
        {
            Keywords = new List<KeywordHeadingIdPair>();
            NotKeywords = new List<KeywordHeadingIdPair>();
        }
    }
}
