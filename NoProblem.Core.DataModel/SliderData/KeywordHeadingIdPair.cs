﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SliderData
{
    public class KeywordHeadingIdPair
    {
        public string Keyword { get; set; }
        public Guid HeadingId { get; set; }
        public int Priority { get; set; }
        public bool IsInWhiteList { get; set; }
    }
}
