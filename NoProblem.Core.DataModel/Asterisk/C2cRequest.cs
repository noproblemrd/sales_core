﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Asterisk
{
    public class C2cRequest
    {
        public InternalWrapper call_init { get; set; }

        public class InternalWrapper
        {
            public string host { get; set; }
            public string originator { get; set; }
            public int call_limit { get; set; }
            public int recording { get; set; }
            public string returnAddress { get; set; }
            public string schedule { get; set; }
            public string app_name { get; set; }
            public string app_session_id { get; set; }
            public int retries { get; set; }
            public string moh_class { get; set; }
            public int forceCall { get; set; }
            public string prompts_class { get; set; }
            public int validity { get; set; }
            public string ring_policy { get; set; }
            public string targetProto { get; set; }
            public string originatorProto { get; set; }
            public string target { get; set; }
            public string cid { get; set; }
            public int record_target { get; set; }
            public int record_originator { get; set; }
            [Newtonsoft.Json.JsonProperty(NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string ttsAddress { get; set; }
            [Newtonsoft.Json.JsonProperty(NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public Dictionary<string, string> Keys { get; set; }
            [Newtonsoft.Json.JsonProperty(NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
            public string consumer_prompt_address { get; set; }
        }

        public class C2cClickMethods
        {
            public const string REJECT_CALL = "reject_call";
            public const string PASS = "pass";
            public const string HANGUP = "hangup";
            public const string REPROMPT = "reprompt";
        }
    }
}
