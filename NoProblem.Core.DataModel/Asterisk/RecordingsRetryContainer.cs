﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.DataModel.Asterisk
{
    public class RecordingsRetryContainer
    {
        public Timer Timer { get; set; }
        public string AsteriskSessionId { get; set; }
        public int TryCount { get; set; }
        public string SessionServer { get; set; }
    }
}
