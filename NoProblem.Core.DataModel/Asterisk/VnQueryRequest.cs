﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Asterisk
{
    public class VnQueryRequest
    {
        public class PropertyNames
        {
            public const string IVR_SESSION = "ivr_session";
            public const string IVR_INBOUND = "ivr_inbound";
            public const string IVR_CALLERID = "ivr_callerid";
            public const string SOURCE_URL = "source_url";
        }

        public string IvrSession { get; set; }
        public string IvrInbound { get; set; }
        public string IvrCallerId { get; set; }
        public string SourceUrl { get; set; }
    }
}
