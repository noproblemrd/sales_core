﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Asterisk
{
    public class C2cStatusCode
    {
        public const string ACK = "ack";
        public const string NO_ANSWER = "NO ANSWER";
        public const string BUSY = "BUSY";
        public const string HANGUP = "HANGUP";
        public const string FAILED = "FAILED";
        public const string ANSWER = "ANSWER";
        public const string CANCEL = "CANCEL";
        public const string IN_PROGRESS = "IN PROGRESS";
        public const string DONE = "DONE";
        public const string APPROVED = "APPROVED";
        public const string REJECTED = "REJECTED";
    }
}
