﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Asterisk
{
    public class VnStatusCode
    {
        public const string CALL_DIALING = "CALL_DIALING";
        public const string CALL_CONNECT = "CALL_CONNECT";
        public const string CALL_DISCONNECT = "CALL_DISCONNECT";
        public const string CALL_CDR = "CALL_CDR";
        public const string CALL_FAILED = "CALL_FAILED";
        public const string CALL_CONGESTION = "CALL_CONGESTION";
        public const string CALL_BUSY = "CALL_BUSY";
        public const string CALL_NOANSWER = "CALL_NOANSWER";
        public const string CALL_CANCEL = "CALL_CANCEL";
        public const string CANCEL = "CANCEL";
        public const string CALL_CHANUNAVAIL = "CALL_CHANUNAVAIL";
        public const string CALL_ANSWER = "CALL_ANSWER";
    }
}
