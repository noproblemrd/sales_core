﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Asterisk
{
    public class TtsStatusContainer
    {
        public class PropertyNames
        {
            public const string INTERNAL_ID = "internal_id";
            public const string STATUS = "status";
            public const string DIGIT = "digit";
        }

        public string Internal_Id { get; set; }
        public string Status { get; set; }
        public string Digit { get; set; }
    }
}
