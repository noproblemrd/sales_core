﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Asterisk
{
    public class TtsStatusCode
    {
        public const string ADVERTISER_ANSWERED = "ADVERTISER_ANSWERED";
        public const string ANSWER = "ANSWER";
        public const string BUSY = "BUSY";
        public const string FAILED = "FAILED";
        public const string NO_ANSWER = "NO ANSWER";
        public const string HANGUP = "HANGUP";
        public const string CANCEL = "CANCEL";
    }
}
