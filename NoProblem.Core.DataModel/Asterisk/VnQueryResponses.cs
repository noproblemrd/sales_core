﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Asterisk
{
    public class VnQueryResponse
    {
        public VnQueryResponse()
        {
            IvrTech = "SIP";
            ErrorMsg = "success";
            IvrTrunk = string.Empty;
            IvrDisplayClid = string.Empty;
            IvrCallerAnnounce = string.Empty;
            IvrAdvertiserAnnounce = string.Empty;
            IvrFindAlternativeAdvertiser = string.Empty;
            IvrNoAlternativeFound = string.Empty;
            Inc2Session = string.Empty;
            ErrorMsg = string.Empty;
            Recording = string.Empty;
            ReturnAddress = string.Empty;
            IvrTarget = new string[0];
            IsWaitForDigit = "0";
        }

        public class PropertyNames
        {
            public const string ERRORCODE = "errorcode";
            public const string IVR_TRUNK = "ivr_trunk";
            public const string IVR_DISPLAY_CLID = "ivr_display_clid";
            public const string IVR_CALLER_ANNOUNCE = "ivr_caller_announce";
            public const string IVR_ADVERTISER_ANNOUNCE = "ivr_advertiser_announce";
            public const string IVR_FIND_ALTERNATIVE_ADVERTISER = "ivr_find_alternative_advertiser";
            public const string IVR_NO_ALTERNATIVE_FOUND = "ivr_no_alternative_found";
            public const string IVR_TECH = "ivr_tech";
            public const string INC2_SESSION = "inc2_session";
            public const string IVR_TARGET = "ivr_target";
            public const string ERRORMSG = "errormsg";
            public const string RECORDING = "recording";
            public const string RETURNADDRESS = "returnAddress";
            public const string IS_WAIT_FOR_DIGIT = "isWaitForDigit";//0 for normal, 'dapaz_books' for dapaz printed books project.
        }

        public enum enumErrorCode
        {
            success = 0,
            error = 1,
            play_ivr_caller_announce_wait_for_input = 2
        }

        public class RecordingValues
        {
            public const string YES = "yes";
            public const string NO = "no";
            public const string IN = "in";
            public const string OUT = "out";
        }

        public int ErrorCode { get; set; }
        public string IvrTrunk { get; set; }
        public string IvrDisplayClid { get; set; }
        public string IvrCallerAnnounce { get; set; }
        public string IvrAdvertiserAnnounce { get; set; }
        public string IvrFindAlternativeAdvertiser { get; set; }
        public string IvrNoAlternativeFound { get; set; }
        public string IvrTech { get; set; }
        public string Inc2Session { get; set; }
        public string[] IvrTarget { get; set; }
        public string ErrorMsg { get; set; }
        public string Recording { get; set; }
        public string ReturnAddress { get; set; }
        public string IsWaitForDigit { get; set; }
    }
}
