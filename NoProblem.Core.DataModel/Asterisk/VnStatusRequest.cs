﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Asterisk
{
    public class VnStatusRequest
    {
        public class PropertyNames
        {
            public const string IVR_SESSION= "ivr_session";
            public const string IVR_STATUS = "ivr_status";
            public const string DIGIT = "digit";
            public const string IS_WAIT_FOR_DIGIT = "isWaitForDigit";
        }

        public string IvrSession { get; set; }
        public string IvrStatus { get; set; }
        public string Digit { get; set; }
        public string IsWaitForDigit { get; set; }
    }
}
