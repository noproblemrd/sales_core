﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Asterisk
{
    public class C2cStatusContainer
    {
        public const string TIME_FORMAT = "yyyyMMdd-HHmmss";
        public const string ADV_SIDE = "adv";
        public const string CON_SIDE = "con";

        public class PropertyNames
        {
            public const string TYPE = "type";
            public const string SIDE = "side";
            public const string STATUS = "status";
            public const string SESSION = "session";
            public const string TIME = "time";
            public const string HOST_NAME = "hostname";
        }

        public DateTime Time { get; set; }
        public string Session { get; set; }
        public string Status { get; set; }
        public string Side { get; set; }
        public string Type { get; set; }
        public string HostName { get; set; }
    }
}
