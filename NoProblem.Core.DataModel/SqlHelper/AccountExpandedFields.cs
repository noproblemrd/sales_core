﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SqlHelper
{
    public class AccountExpandedFields
    {
        public Guid SupplierId { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }
        public string ZipCode { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public string Crm3Url { get; set; }
        public string RealPhone { get; set; }
    }
}
