﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SqlHelper
{
    public class RegionMinData
    {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public int Level { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int TimeZoneCode { get; set; }
        public string EnglishName { get; set; }
        public int NumberOfZipcodes { get; set; }

        public override bool Equals(object obj)
        {
            RegionMinData other = (RegionMinData)obj;
            return other.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
