﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SqlHelper
{
    public class SecondaryExpertiseMinData
    {
        public Guid PrimaryExpertiseId { get; set; }
        public Guid Id { get; set; }
    }
}
