﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SqlHelper
{
    public class RegionAccountMinData
    {
        public Guid RegionId { get; set; }
        public NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState RegionState { get; set; }
    }
}
