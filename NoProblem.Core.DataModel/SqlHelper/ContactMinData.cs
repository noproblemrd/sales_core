﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SqlHelper
{
    public class ContactMinData
    {
        public Guid Id { get; set; }
        public string MobilePhone { get; set; }
        public bool IsBlackList { get; set; }
        public bool IsBlockedNumberContact { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Guid AccountId { get; set; }
    }
}
