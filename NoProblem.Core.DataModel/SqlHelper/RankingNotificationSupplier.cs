﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SqlHelper
{
    public class RankingNotificationSupplier
    {
        public Guid SupplierId { get; set; }
        public decimal IncidentPrice { get; set; }
        public Guid ExpertiseId { get; set; }
        public int RankingNotification { get; set; }
    }
}
