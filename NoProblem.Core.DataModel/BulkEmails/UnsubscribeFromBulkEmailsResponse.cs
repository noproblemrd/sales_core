﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.BulkEmails
{
    public class UnsubscribeFromBulkEmailsResponse
    {
        public bool Success { get; set; }
        public string Email { get; set; }
    }
}
