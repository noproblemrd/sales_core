﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.BulkEmails
{
    public class UnsubscribeFromBulkEmailsRequest
    {
        public Guid SupplierId { get; set; }
    }
}
