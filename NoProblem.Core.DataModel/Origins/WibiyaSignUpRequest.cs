﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Origins
{
    public class WibiyaSignUpRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string WebSiteUrl { get; set; }
        public string BusinessName { get; set; }
        public string WibiyaToken { get; set; }
        public string SiteId { get; set; }
    }
}
