﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Origins
{
    public class WibiyaLoginResponse
    {
        public string FirstName { get; set; }
        public Guid OriginId { get; set; }
    }
}
