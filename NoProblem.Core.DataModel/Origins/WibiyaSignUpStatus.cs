﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Origins
{
    public enum WibiyaSignUpStatus
    {
        OK,
        AlreadyExists,
        CouldNotUpdateOriginIdInWibiya,
        EmailWithPasswordFailed
    }
}
