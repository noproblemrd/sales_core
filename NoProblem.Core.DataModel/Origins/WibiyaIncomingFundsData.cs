﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Origins
{
    public class WibiyaIncomingFundsData
    {
        public int Requests { get; set; }
        public int Views { get; set; }
        public double ConversionRate { get; set; }
        public DateTime? LastCashOut { get; set; }
        public int CashEarned { get; set; }
        public string PayPalEmail { get; set; }
    }
}
