﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Origins
{
    public class WibiyaLoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool UpdateXmlInWibiya { get; set; }
        public string WibiyaToken { get; set; }
        public string SiteId { get; set; }
    }
}
