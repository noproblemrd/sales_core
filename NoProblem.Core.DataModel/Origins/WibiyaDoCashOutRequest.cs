﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Origins
{
    public class WibiyaDoCashOutRequest
    {
        public Guid OriginId { get; set; }
        public string PayPalEmail { get; set; }
        public bool RememberPayPalEmail { get; set; }
        public string Currency { get; set; }
    }
}
