﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Origins.InjectionsCockpit
{
    public class OriginData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool? UseSameInUpsale { get; set; }
        public bool? IsInFinancialDashboard { get; set; }
        public string SliderBroughtToYouByName { get; set; }
        public bool? IncludeCampaign { get; set; }
        public bool? ShowNoProblemInSlider { get; set; }
        public bool? IsDistribution { get; set; }
        public bool? IsApiSeller { get; set; }
        public decimal? CostPerRequest { get; set; }
        public decimal? CostPerInstall { get; set; }
        public decimal? RevenueShare { get; set; }
        public bool? AdEngineEnabled { get; set; }
        public bool? SendInstallationReport { get; set; }
        public decimal? Factor { get; set; }
        public string EmailToSendInstallationReport { get; set; }
        public bool? SendMaxFactorReport { get; set; }
        public decimal? MaxFactor { get; set; }
        public string EmailToSendMaxFactorReport { get; set; }
    }
}
