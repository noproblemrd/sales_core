﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Origins.InjectionsCockpit
{
    public class InjectionWithDAUs
    {
        public Guid InjecitonId { get; set; }
        public string InjectionName { get; set; }
        public int DAUs { get; set; }
    }
}
