﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Origins.InjectionsCockpit
{
    public class OriginWithDAUs
    {
        public Guid OriginId { get; set; }
        public string OriginName { get; set; }
        public int DAUs { get; set; }
    }
}
