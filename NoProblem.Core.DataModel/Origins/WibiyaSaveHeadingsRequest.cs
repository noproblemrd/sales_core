﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Origins
{
    public class WibiyaSaveHeadingsRequest
    {
        public Guid OriginId { get; set; }
        public List<Guid> Headings { get; set; }
    }
}
