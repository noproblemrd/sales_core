﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.BusinessClosures
{
    public class ClosureDateData
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Name { get; set; }
        public Guid ClosureDateId { get; set; }
    }
}
