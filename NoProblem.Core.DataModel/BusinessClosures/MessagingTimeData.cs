﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.BusinessClosures
{
    public class MessagingTimeData
    {
        public Xrm.new_availability.DayOfWeek DayOfWeek { get; set; }
        public string FromHour { get; set; }
        public string ToHour { get; set; }
    }
}
