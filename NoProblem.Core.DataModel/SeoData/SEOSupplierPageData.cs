﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SEOSupplierPageData
    {
        public SEOSupplierPageData()
        {
            Leads = new List<SEOLeadData>();
            WorkingHours = new List<SEOSupplierDaysData>();
            Services = new List<string>();
            Videos = new List<string>();
        }

        public List<SEOLeadData> Leads { get; set; }
        public List<string> Services { get; set; }
        public string Address { get; set; }
        public string RegionOfLeads { get; set; }
        public int LeadCount { get; set; }
        public string Phone { get; set; }
        public List<SEOSupplierDaysData> WorkingHours { get; set; }
        public string Name { get; set; }
        public int Radius { get; set; }
        public List<SEOFeaturedSupplierData> FeaturedSuppliers { get; set; }
        public bool IsOnDuty { get; set; }
        public bool IsPaying { get; set; }
        public string ServicesIdsStr { get; set; }
        public List<string> Videos { get; set; }
        public string About { get; set; }
        public string Website { get; set; }
        public bool PayWithAMEX { get; set; }
        public bool PayWithMASTERCARD { get; set; }
        public bool PayWithVISA { get; set; }
        public bool PayWithCASH { get; set; }
    }
}
