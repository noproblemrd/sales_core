﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SEOLeadsAndHeadingForGroup
    {
        public SEOLeadsAndHeadingForGroup()
        {
            Leads = new List<SEOLeadData>();
            Headings = new List<string>();
        }

        public int Count { get; set; }
        public List<SEOLeadData> Leads { get; set; }
        public List<string> Headings { get; set; }
    }
}
