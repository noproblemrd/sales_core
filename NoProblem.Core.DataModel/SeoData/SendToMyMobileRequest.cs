﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SendToMyMobileRequest
    {
        public string Url { get; set; }
        public Guid SupplierId { get; set; }
        public string Phone { get; set; }
    }
}
