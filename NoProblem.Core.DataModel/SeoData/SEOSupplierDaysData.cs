﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SEOSupplierDaysData
    {
        public SEOSupplierDaysData()
        {
            DaysOfWeek = new List<DayOfWeek>();
        }

        public List<DayOfWeek> DaysOfWeek { get; set; }
        public int From { get; set; }
        public int FromMinute { get; set; }
        public int To { get; set; }
        public int ToMInute { get; set; }
        public bool IsClosed { get; set; }

        public override bool Equals(object obj)
        {
            SEOSupplierDaysData other = (SEOSupplierDaysData)obj;
            return (this.IsClosed && other.IsClosed) || (this.From == other.From & this.To == other.To);
        }

        public override int GetHashCode()
        {
            return IsClosed.GetHashCode() + From.GetHashCode() + To.GetHashCode();
        }
    }
}
