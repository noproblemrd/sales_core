﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SEOGetSupplierPageDataRequest
    {
        public Guid SupplierId { get; set; }
        public string Zipcode { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        
    }
}
