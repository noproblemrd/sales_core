﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SEOLeadsForHeading
    {
        public SEOLeadsForHeading()
        {
            Leads = new List<SEOLeadData>();
        }

        public int Count { get; set; }
        public List<SEOLeadData> Leads { get; set; }
    }
}
