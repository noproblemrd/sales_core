﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SEOPopularCityData
    {
        public string CityName { get; set; }
        public string StateAbbreviation { get; set; }
        public string StateFullName { get; set; }
        public Guid RegionId { get; set; }
    }
}
