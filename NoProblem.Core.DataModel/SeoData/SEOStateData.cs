﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SEOStateData
    {
        public string FullName { get; set; }
        public string Abbreviation { get; set; }
        public bool IsPopular { get; set; }
    }
}
