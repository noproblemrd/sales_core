﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SEOSupplierData
    {
        public SEOSupplierData()
        {
            Services = new List<string>();
        }

        public string Region { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public List<string> Services { get; set; }
        public Guid Id { get; set; }
        public bool OnDuty { get; set; }
        public bool IsPaying { get; set; }
    }
}
