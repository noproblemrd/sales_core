﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SEOLeadData
    {
        public string Description { get; set; }
        public string Region { get; set; }
        public string HeadingName { get; set; }
        public bool IsNew { get; set; }
    }
}
