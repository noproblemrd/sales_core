﻿using System.Collections.Generic;

namespace NoProblem.Core.DataModel.SeoData
{
    public class SEOLeadsAndSupplierForHeadingAndRegion
    {
        public SEOLeadsAndSupplierForHeadingAndRegion()
        {
            Leads = new List<SEOLeadData>();
            Suppliers = new List<SEOSupplierData>();
        }

        public List<SEOLeadData> Leads { get; set; }
        public int LeadsCount { get; set; }
        public int SupplierCount { get; set; }
        public List<SEOSupplierData> Suppliers { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string StateAbbreviation { get; set; }
    }
}
