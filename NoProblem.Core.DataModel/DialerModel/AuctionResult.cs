﻿
namespace NoProblem.Core.DataModel
{
    public class AuctionResult
    {
        public string ServiceRequestId { get; set; }
        public AuctionProvider[] Providers { get; set; }
    }
}
