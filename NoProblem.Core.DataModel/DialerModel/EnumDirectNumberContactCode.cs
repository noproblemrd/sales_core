﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public enum enumDirectNumberContactCode
    {
        CONTACTED,
        SUPPLIER_UNREACHED,
        SUPPLIER_UNREACHED_AFTER_ALTERNATIVE
    }
}
