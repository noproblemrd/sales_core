﻿using System;
using System.Collections.Generic;

namespace NoProblem.Core.DataModel
{
    public class DNQueryResult
    {
        public string TenantId { get; set; }
        public string DirectPhoneNumber { get; set; }
        public List<string> TruePhoneNumber { get; set; }
        public string DisplayPhoneNumber { get; set; }
        public Guid ProviderId { get; set; }
        public bool IsRecording { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsBlacklisted { get; set; }
        public string IvrCallerAnnounce { get; set; }
        public string IvrAdvertiserAnnounce { get; set; }
        public string IvrFindAlternativeAdvertiser { get; set; }
        public string IvrNoAlternativeFound { get; set; }
        public string IsWaitForDigit { get; set; }
        public string CallType { get; set; }
        public string MappingStatus { get; set; }
        public bool Reroute { get; set; }
        public Guid RerouteHeadingId { get; set; }
        public Guid RerouteRegionId { get; set; }
        public Guid RerouteOriginId { get; set; }
        public string IvrCallerAnnounceAdvertiserSpecific { get; set; }
        public int LimitCallDurationSeconds { get; set; }
        public string SupplierCountry { get; set; }
        public string CustomField { get; set; }

        public DNQueryResult()
        {
            IsWaitForDigit = "0";
            TruePhoneNumber = new List<string>();
            IvrCallerAnnounce = string.Empty;
            IvrAdvertiserAnnounce = string.Empty;
            IvrFindAlternativeAdvertiser = string.Empty;
            IvrNoAlternativeFound = string.Empty;
        }
    }
}
