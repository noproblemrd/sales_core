﻿
namespace NoProblem.Core.DataModel
{
    public class AuctionProvider
    {
        public string ProviderId { get; set; }
        public decimal BidValue { get; set; }
        public bool IsRealTimeBid { get; set; }
        public bool IsWinner { get; set; }
        public bool IsRejected { get; set; }
    }
}
