﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.DialerModel
{
    public class RerouteGetNextSupplierResponse
    {
        public bool RecordCall { get; set; }
        public string IvrAdvertiserAnnounce { get; set; }
        public string TargetPhoneNumber { get; set; }
        public string DisplayPhoneNumber { get; set; }
        public Guid SupplierId { get; set; }
    }
}
