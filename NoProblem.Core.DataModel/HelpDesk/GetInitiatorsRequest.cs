﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.HelpDesk
{
    public class GetInitiatorsRequest
    {
        public Guid HelpDeskTypeId { get; set; }
        public string SearchInput { get; set; }
        public Guid InitiatorId { get; set; }
    }
}
