﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.HelpDesk
{
    public class HdEntryData
    {
        public Guid Id { get; set; }
        public Guid TypeId { get; set; }
        public Guid SeverityId { get; set; }
        public Guid StatusId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TicketNumber { get; set; }
        public Guid OwnerId { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? FollowUp { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid RegardingObjectId { get; set; }
        public string RegardingObjectName { get; set; }
        public string SecondaryRegardingObject { get; set; }
        public Guid SecondaryRegardingObjectId { get; set; }
        public Guid InitiatorId { get; set; }
        public string InitiatorName { get; set; }
    }
}
