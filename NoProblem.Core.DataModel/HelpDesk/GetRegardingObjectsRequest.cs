﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.HelpDesk
{
    public class GetRegardingObjectsRequest
    {
        public Guid InitiatorId { get; set; }
        public string SearchInput { get; set; }
        public Guid HelpDeskTypeId { get; set; }
        public Guid RegardingObjectId { get; set; }
    }
}
