﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.HelpDesk
{
    public class GetHdEntriesRequest
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string TicketNumber { get; set; }
        public Guid? StatusId { get; set; }
        public Guid? OwnerId { get; set; }
        public Guid? InitiatorId { get; set; }
        public string SecondaryRegardingObjectName { get; set; }
        public DateTime? FollowUp { get; set; }
        public Guid HdTypeId { get; set; }
        public Guid? RegardingObjectId { get; set; }
    }
}
