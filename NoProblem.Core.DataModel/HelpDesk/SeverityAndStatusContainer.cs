﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.HelpDesk
{
    public class SeverityAndStatusContainer
    {
        public List<HdStatusData> StatusList { get; set; }
        public List<HdSeverityData> SeverityList { get; set; }
    }
}
