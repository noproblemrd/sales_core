﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.HelpDesk
{
    public class HdStatusData
    {
        public Guid TypeId { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string IdSmall { get; set; }
        public bool Inactive { get; set; }
    }
}
