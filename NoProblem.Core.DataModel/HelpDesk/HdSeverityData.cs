﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.HelpDesk
{
    public class HdSeverityData
    {
        public Guid TypeId { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Inactive { get; set; }
        public string IdSmall { get; set; }
    }
}
