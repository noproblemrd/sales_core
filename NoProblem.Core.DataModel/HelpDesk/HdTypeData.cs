﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.HelpDesk
{
    public class HdTypeData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Xrm.new_helpdesktype.RegardingObjectType RegardingObjectType { get; set; }
        public Xrm.new_helpdesktype.InitiatorType InitiatorType { get; set; }
    }
}
