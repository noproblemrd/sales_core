﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Expertise;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.DataModel.Case
{
    public class CustomerPageCaseData
    {
        public string Description { get; set; }
        public Guid HeadingId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string RegionName { get; set; }
        public Guid CaseId { get; set; }
        public string ZipCode { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
    }


    public class CallRecordData
    {
        public Guid Id { get; set; }
        public string CallRecordUrl { get; set; }
        public DateTime CreateDate { get; set; }  
     //   public string Status { get; set; }
        public ConsumerIncidentAccountData Advertiser { get; set; }
        public int Duration { get; set; }
        public ConsumerIncidentData Customer { get; set; }
    }

    public class AdvertiserAboutData
    {
        public string PhoneNumber { get; set; }
        public string FullAddress { get; set; }
        public string ContactName { get; set; }
        public List<Category> Categories { get; set; }
        public string BusinessName { get; set; }
        public string BusinessDescription { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public int? NumberOfEmployees { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double YelpRating { get; set; }
        public double GoogleRating { get; set; }
    }
}
