﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Case
{
    public class CallData
    {
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public decimal PredefinedPrice { get; set; }
        public decimal ActualPrice { get; set; }
        public bool IsCharged { get; set; }
        public EndStatus Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Duration { get; set; }
        public string AarCandidateStatus { get; set; }
        public bool? IsAnsweringMachine { get; set; }
        public string DtmfPressed { get; set; }
        public string IvrFlavor { get; set; }
        public bool IsRejected { get; set; }
        public string Recording { get; set; }
        public string RejectReason { get; set; }
        public decimal BillingRate { get; set; }
        public decimal QualityRate { get; set; }
        public string LeadBuyerName { get; set; }

        public void SetQualityRate()
        {
            this.QualityRate = BillingRate * ActualPrice;
        }
    }
}
