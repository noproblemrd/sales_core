﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Case
{
    public class CaseData
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal WinningPrice { get; set; }
        public int ProvidedAdvertisers { get; set; }
        public string HeadingName { get; set; }
        public string HeadingCode { get; set; }
        public int RegionLevel { get; set; }
        public string RegionName { get; set; }
        public string OriginName { get; set; }
        public Guid ConsumerId { get; set; }
        public string ConsumerName { get; set; }
        public string ConsumerEmail { get; set; }
        public CaseType Type { get; set; }
        public DateTime CreatedOn { get; set; }
        public int RequestedAdvertisers { get; set; }
        public decimal Revenue { get; set; }
        public bool ShowUpsaleButton { get; set; }
        public string CostumerPhone { get; set; }
        public List<CallData> Calls { get; private set; }
        public string TicketNumber { get; set; }
        public Guid PayStatusId { get; set; }
        public Guid PayReasonId { get; set; }
        public Guid UpsaleId { get; set; }
        public string ExposureUrl { get; set; }
        public string ExposureControlName { get; set; }
        public string Keyword { get; set; }
        public bool IsStoppedManually { get; set; }
        public bool CanBeStopped { get; set; }
        public string ParentRequestTicket { get; set; }
        public Guid ParentRequestId { get; set; }
        public List<PublisherPortal.GuidStringPair> ConnectedRequests { get; set; }

        public CaseData()
        {
            Calls = new List<CallData>();
            ConnectedRequests = new List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>();
        }
    }
}
