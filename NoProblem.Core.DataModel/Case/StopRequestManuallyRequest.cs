﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Case
{
    public class StopRequestManuallyRequest
    {
        public Guid CaseId { get; set; }
        public Guid UserId { get; set; }
    }
}
