﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Pusher.CaseFlow
{
    public class CallEndedContainer
    {
        public bool HasMoreSuppliers { get; set; }
    }
}
