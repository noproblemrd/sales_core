﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Pusher.CaseFlow
{
    public class CaseAllDataContainer
    {
        public List<AdvertiserData> Advertisers { get; set; }
        public int InterestedAdvertisers { get; set; }

        public CaseAllDataContainer()
        {
            Advertisers = new List<AdvertiserData>();
        }
    }
}
