﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Pusher.CaseFlow
{
    public class AdvertiserData
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string YelpRating { get; set; }
        public string YelpReview { get; set; }
        public List<string> PayWith { get; set; }
        public int Index { get; set; }
        public Guid SupplierId { get; set; }
        public bool RecordingCall { get; set; }
        public string YelpLink { get; set; }
        public List<TipPercent> TipsPercentages { get; set; }

        public class TipPercent
        {
            public Guid TipId { get; set; }
            public double Percent { get; set; }
            public bool Show { get; set; }
        }

        public AdvertiserData()
        {
            PayWith = new List<string>();
        }
    }
}
