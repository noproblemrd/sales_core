﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dashboard.FinancialDashboard
{
    public class FinancialDashboardRequest
    {
        public int LifeTime { get; set; }
        public Guid OriginId { get; set; }
        public Guid HeadingId { get; set; }
        public string Flavor { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool UseCompare { get; set; }
        public Guid CompareOriginId { get; set; }
        public Guid CompareHeadingId { get; set; }
        public string CompareFlavor { get; set; }
        public DateTime CompareFromDate { get; set; }
        public DateTime CompareToDate { get; set; }
        public string GraphToShow { get; set; }
        public bool OnlyGraph { get; set; }
        public bool IsInjections { get; set; }
        public bool UseOnlySoldHeadings { get; set; }
    }
}
