﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dashboard.FinancialDashboard
{
    public class FinancialDashboardResponse
    {
        public FinancialTableData MainTableData { get; set; }
        public FinancialTableData CompareTableData { get; set; }
        public FinancialTableData GapTableData { get; set; }
        public List<FinancialGraphNode> MainGraph { get; set; }
        public List<FinancialGraphNode> CompareGraph { get; set; }
    }
}
