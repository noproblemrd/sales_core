﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dashboard.FinancialDashboard
{
    public class FinancialDashboardFiltersContainer
    {
        public List<PublisherPortal.GuidStringPair> AddOnOrigins { get; set; }
        public List<PublisherPortal.GuidStringPair> InjectionOrigins { get; set; }
        public List<string> Flavors { get; set; }
    }
}
