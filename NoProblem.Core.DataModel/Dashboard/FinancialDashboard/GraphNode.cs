﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dashboard.FinancialDashboard
{
    public class FinancialGraphNode
    {
        public DateTime Date { get; set; }
        public double Value { get; set; }
    }
}
