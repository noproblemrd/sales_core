﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dashboard.FinancialDashboard
{
    public class FinancialTableData
    {
        /// <summary>
        /// sum(daus)/days
        /// </summary>
        public double AverageDau { get; set; }

        /// <summary>
        /// sum(exposures)
        /// </summary>
        public double Exposures { get; set; }

        /// <summary>
        /// sum(requests)
        /// </summary>
        public double Requests { get; set; }

        /// <summary>
        /// sum(upsales)
        /// </summary>
        public double Upsales { get; set; }
        public double UpsalesAgain { get; set; }

        /// <summary>
        /// SUM (Exposures) / days
        /// </summary>
        public double AverageDailyExposures { get; set; }

        /// <summary>
        /// AverageDailyExposures / AverageDau
        /// </summary>
        public double AverageDailyExposuresDividedByAverageDau { get; set; }

        /// <summary>
        /// SUM(PPC of all calls)
        /// </summary>
        public double PotentialRevenue { get; set; }

        /// <summary>
        /// (Potential Revenue / Days in range) 
        /// </summary>
        public double DailyPotentialRevenue { get; set; }

        /// <summary>
        /// Daily Potential Revenue / Average DAU
        /// </summary>
        public double PotentialARDAU { get; set; }

        /// <summary>
        /// Potential ARDAU * Life time (calculated)
        /// </summary>
        public double LifetimeTimesPotentialARDAU { get; set; }

        /// <summary>
        /// SUM(PPC of all calls to paying advertisers)
        /// </summary>
        public double ActualRevenue { get; set; }

        /// <summary>
        /// SUM(Requests that generated actual revenues )
        /// </summary>
        public double RequestsThatGeneratedActualRevenues { get; set; }

        /// <summary>
        /// Actual revenues / Days in range
        /// </summary>
        public double DailyActualRevenue { get; set; }

        /// <summary>
        /// Daily Actual Revenue / Average DAU
        /// </summary>
        public double ActualARDAU { get; set; }

        /// <summary>
        /// Actual ARDAU * Life time (calculated)
        /// </summary>
        public double LifetimeTimesActualARDAU { get; set; }

        /// <summary>
        /// SUM(PPC of all calls to paying advertisers ) - SUM (PPC of all Refunded Calls to paying advertisers)
        /// </summary>
        public double NetActualRevenue { get; set; }

        public double NetActualRevenueUpsales { get; set; }

        /// <summary>
        /// SUM(Requests that generated Net Actual Revenue)
        /// </summary>
        public double RequestsThatGeneratedNetActualRevenue { get; set; }

        /// <summary>
        /// Net Actual Revenue / Days in range
        /// </summary>
        public double DailyNetActualRevenue { get; set; }

        /// <summary>
        /// Daily Actual Revenue / Average DAU
        /// </summary>
        public double NetActualARDAU { get; set; }

        /// <summary>
        /// Actual ARDAU * Life time (calculated)
        /// </summary>
        public double LifetimeTimesNetActualRevenueARDAU { get; set; }

        /// <summary>
        /// sum(requests)/sum(exposures)*100
        /// </summary>
        public double Conversion { get; set; }

        /// <summary>
        /// Potential Revenue / requests
        /// </summary>
        public double AveragePotentialPPL { get; set; }

        /// <summary>
        /// Actual Revenue / Requests that generated actual revenues
        /// </summary>
        public double AverageActualPPL { get; set; }

        /// <summary>
        /// Net Actual Revenue / Requests that generated Net Actual Revenue
        /// </summary>
        public double AverageNetActualPPL { get; set; }

        public double TotalCostOfRequestsPotential { get; set; }
        public double TotalCostOfRequestsActual { get; set; }
        public double TotalCostOfRequestsNetActual { get; set; }

        public double TotalCostOfRequestsNetActualUpsales { get; set; }

        public double CostPerRequestPotential { get; set; }
        public double CostPerRequestActual { get; set; }
        public double CostPerRequestNetActual { get; set; }

        public double CostPerMilliaPotential { get; set; }
        public double RevenuePerMilliaPotential { get; set; }
        public double CostPerMilliaActual { get; set; }
        public double RevenuePerMilliaActual { get; set; }
        public double CostPerMilliaNetActual { get; set; }
        public double RevenuePerMilliaNetActual { get; set; }

        public double RoiPotential { get; set; }
        public double RoiActual  { get; set; }
        public double RoiNetActual { get; set; }

        public double CostOfAddOnInstallation { get; set; }

        public double FullFullfilmentRevenue { get; set; }
        public double TotalCostOfRequestsFullFullfilment { get; set; }
        public double CostPerRequestFullFullfilment { get; set; }
        public double CostPerMilliaFullFullfilment { get; set; }
        public double RevenuePerMilliaFullFullfilment { get; set; }
        public double FullFullfilmentARDAU { get; set; }
        public double LifetimeTimesFullFullfilmentRevenueARDAU { get; set; }
        public double DailyFullFullfilmentRevenue { get; set; }
        public double RoiFullFullfilment { get; set; }

        public double  AverageFullFullfilmentPPL { get; set; }

        public double CostOfRevenueGeneratingLeadsPotential { get; set; }
        public double CostOfRevenueGeneratingLeadsActual { get; set; }
        public double CostOfRevenueGeneratingLeadsNetActual { get; set; }
        public double CostOfRevenueGeneratingLeadsFullFullfilment { get; set; }
    }
}
