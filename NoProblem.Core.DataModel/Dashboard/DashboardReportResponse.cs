﻿using System.Collections.Generic;

namespace NoProblem.Core.DataModel.Dashboard
{
    public class DashboardReportResponse
    {
        public List<TableData> TableRows { get; set; }
        public List<GraphNode> CurrentGraph { get; set; }
        public List<GraphNode> PastGraph { get; set; }
        public List<GraphNode> ObjectivesGraph { get; set; }
    }
}
