﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dashboard
{
    public class AarDashboardReportRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public AarCriteria ShowMe { get; set; }
        public bool CompareToPast { get; set; }
        public DateTime PastFromDate { get; set; }
        public DateTime PastToDate { get; set; }
        public bool OnlyGraphs { get; set; }
        public Guid HeadingId { get; set; }
    }
}
