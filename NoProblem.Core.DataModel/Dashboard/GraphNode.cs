﻿
namespace NoProblem.Core.DataModel.Dashboard
{
    public class GraphNode
    {
        public string X { get; set; }
        public decimal Y { get; set; }
    }
}
