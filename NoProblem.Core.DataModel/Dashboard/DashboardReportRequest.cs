﻿using System;

namespace NoProblem.Core.DataModel.Dashboard
{
    public class DashboardReportRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Criteria ShowMe { get; set; }
        public Interval Interval { get; set; }
        public bool CompareToPast { get; set; }
        public DateTime PastFromDate { get; set; }
        public DateTime PastToDate { get; set; }
        public bool ShowObjectives { get; set; }
        public bool OnlyGraphs { get; set; }
        public string CurrencySymbol { get; set; }
    }
}
