﻿
namespace NoProblem.Core.DataModel.Dashboard
{
    public class TableData
    {
        public Criteria Row { get; set; }
        public string Total { get; set; }
        public string Objective { get; set; }
        public string Percentage { get; set; }
        public string PastPeriod { get; set; }
        public string Gap { get; set; }
        public Color PercentageColor { get; set; }
        public Color GapColor { get; set; }
        public int Group { get; set; }
    }
}
