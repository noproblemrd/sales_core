﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dashboard
{
    public class AarDashboardReportResponse
    {
        public List<AarTableData> TableRows { get; set; }
        public List<GraphNode> CurrentGraph { get; set; }
        public List<GraphNode> PastGraph { get; set; }
    }
}
