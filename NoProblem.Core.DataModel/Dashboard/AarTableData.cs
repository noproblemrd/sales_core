﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dashboard
{
    public class AarTableData
    {
        public AarCriteria Row { get; set; }
        public string Total { get; set; }
        public string PastPeriod { get; set; }
        public string Gap { get; set; }
        public Color GapColor { get; set; }
    }
}
