﻿using System;

namespace NoProblem.Core.DataModel.Dashboard
{
    public class ObjectiveData
    {
        public Guid ObjectiveId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal Amount { get; set; }
        public Criteria Criteria { get; set; }
    }
}
