﻿
namespace NoProblem.Core.DataModel.Dashboard
{
    public class CreateObjectiveResponse
    {
        public bool NeedsApproval { get; set; }
    }
}
