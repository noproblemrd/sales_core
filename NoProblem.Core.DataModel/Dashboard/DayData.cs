﻿using System;

namespace NoProblem.Core.DataModel.Dashboard
{
    public class DayData
    {
        public decimal SoldCallsPerRequets { get; set; }
        public decimal AveragePreDefinedPrice { get; set; }
        public decimal AmountOfAdvertisers { get; set; }
        public decimal AmountOfDeposits { get; set; }
        public decimal RevenueFromDeposits { get; set; }
        public decimal AverageDeposits { get; set; }
        public decimal AmountOfCalls { get; set; }
        public decimal AmountOfCallsRealMoney { get; set; }
        public decimal RevenueFromCalls { get; set; }
        public decimal RevenueCallsRealMoney { get; set; }
        public decimal AveragePricePerCall { get; set; }
        public decimal AvgPricePerCallRealMoney { get; set; }
        public decimal ConversionRateDN { get; set; }
        public decimal ConversionRateSR { get; set; }
        public decimal AmountOfRequests { get; set; }
        public decimal AmountOfRefundedCalls { get; set; }
        public decimal PercentageOfRefunds { get; set; }
        public decimal MoneyRefunded { get; set; }
        public decimal AmountOfRecurringDeposits { get; set; }
        public decimal RevenueFromRecurringDeposits { get; set; }
        public decimal AverageRecurringDeposits { get; set; }
        public decimal BalanceSum { get; set; }
        public decimal OutOfMoneyCount { get; set; }
        public decimal ExposuresListing { get; set; }
        public decimal ExposuresWidget { get; set; }
        public decimal RequestsListing { get; set; }
        public decimal RequestsWidget { get; set; }
        public decimal AmountOfWonCalls { get; set; }
        public decimal AvailableAdvertisersCount { get; set; }
        public decimal CustomerServiceUnavailableCount { get; set; }
        public decimal TemporaryUnavailableCount { get; set; }
        public decimal CandidatesCount { get; set; }
        public decimal VirtualMoneyDeposits { get; set; }
        public decimal VirtualMoneyRecurringDeposits { get; set; }
        public decimal AmountOfAccessibles { get; set; }
        public decimal AmountOfDoNotCallList { get; set; }
        public decimal AmountOfFreshlyImported { get; set; }
        public decimal AmountOfITCs { get; set; }
        public decimal AmountOfMachines { get; set; }
        public decimal AmountOfExpired { get; set; }
        public decimal AmountOfInRegistration { get; set; }
        public decimal AmountOfRemoveMe { get; set; }
        public decimal RerouteCallsCount { get; set; }
        public decimal RerouteCallsUnansweredCount { get; set; }
        public decimal RerouteCallsReroutedCount { get; set; }
        public decimal RerouteCallsUnansweredPercent { get; set; }
        public decimal RerouteCallsReroutedPercent { get; set; }
        public decimal AmountOfWebRegistrants { get; set; }
        public decimal PayingAdvertisers { get; set; }
        public decimal AutoUpsaleCallbacksConfirmed { get; set; }
        public decimal AutoUpsaleCallbacks { get; set; }
        public decimal RejectedCalls { get; set; }
        public decimal NumberOfAttemptsAar { get; set; }
        public decimal ConversionRateSrNoUpsales { get; set; }
        public decimal RequestsWidgetNoUpsales { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
