﻿using System;

namespace NoProblem.Core.DataModel.Dashboard
{
    public class CreateExposureRequest
    {
        public Xrm.new_exposure.ExposureType Type { get; set; }
        public Guid OriginId { get; set; }
        public string PlaceInWebSite { get; set; }
        public string PageName { get; set; }
        public string SiteId { get; set; }
        public Xrm.new_channel.ChannelCode? Channel { get; set; }
        public string Url { get; set; }
        public string Domain { get; set; }
        public string ControlName { get; set; }
        public string HeadingCode { get; set; }
        public int HeadingLevel { get; set; }
        public string RegionCode { get; set; }
        public int RegionLevel { get; set; }
        public string SessionId { get; set; }
        public int NumAdvertisersReturned { get; set; }
        public string Keyword { get; set; }
        public string ToolbarId { get; set; }
        public bool IsWelcomeScreenEvent { get; set; }
        public string WelcomeScreenEventType { get; set; }
        public string UserId { get; set; }
        public string IP { get; set; }
        public string SiteTitle { get; set; }
        public string Browser { get; set; }
        public string BrowserVersion { get; set; }

        public class WelcomScreenEventTypes
        {
            public const string OPENED = "opened";
            public const string YES = "yes";
            public const string NO = "no";
            public const string CLOSE_ENABLE = "closeEnable";
        }
    }
}
