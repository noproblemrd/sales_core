﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dashboard.Convertion
{
    public class ExposuresPicklistsContainer
    {
        public List<string> Controls { get; set; }
        public List<PublisherPortal.GuidStringPair> Origins { get; set; }
    }
}
