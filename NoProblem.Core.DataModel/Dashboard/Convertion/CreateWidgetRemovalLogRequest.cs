﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dashboard.Convertion
{
    public class CreateWidgetRemovalLogRequest
    {
        public Guid OriginId { get; set; }
        public string HeadingCode { get; set; }
        public string Keyword { get; set; }
        public string ControlName { get; set; }
        public string Url { get; set; }
        public string RemovePeriod { get; set; }
        public WidgetRemovalLogType LogType { get; set; }

        public enum WidgetRemovalLogType
        {
            Settings, XButton
        }
    }
}
