﻿namespace NoProblem.Core.DataModel.Dashboard
{
    public enum Interval
    {
        Days,
        Weeks,
        Months,
        Quarters,
        Years
    }

    // Don't change the numbers becuase they are connected to picklist new_criteria in entity new_objecive.
    public enum Criteria
    {
        AmountOfCalls = 1,
        RevenueFromCalls = 2,
        AveragePricePerCall = 3,
        AveragePreDefinedPrice = 10,
        AmountOfRequests = 15,
        SoldCallsPerRequets = 9,
        AmountOfDeposits = 4,
        AmountOfRecurringDeposits = 17,
        RevenueFromDeposits = 5,
        RevenueFromRecurringDeposits = 14,
        AverageDeposits = 6,
        AverageRecurringDeposits = 7,
        BalanceSum = 12,
        AmountOfAdvertisers = 8,
        OutOfMoneyCount = 13,
        ConversionRateDN = 16,
        ConversionRateSR = 11,
        AmountOfRefundedCalls = 18,
        PercentageOfRefunds = 19,
        MoneyRefunded = 20,
        AvailableAdvertisersCount = 21,
        CustomerServiceUnavailableCount = 22,
        TemporaryUnavailableCount = 23,
        CandidatesCount = 24,
        VirtualMoneyDeposits = 25,
        VirtualMoneyRecurringDeposits = 26,
        AmountOfAccessibles = 27,
        AmountOfDoNotCallList = 28,
        AmountOfFreshlyImported = 29,
        AmountOfITCs = 30,
        AmountOfMachines = 31,
        AmountOfExpired = 32,
        AmountOfInRegistration = 33,
        AmountOfRemoveMe = 34,
        RerouteCallsCount = 35,
        RerouteCallsUnansweredCount = 36,
        RerouteCallsUnansweredPercent = 37,
        RerouteCallsReroutedCount = 38,
        RerouteCallsReroutedPercent = 39,
        AmountOfWebRegistrants = 40,
        PayingAdvertisers = 41,
        AutoUpsaleCallbacksConfirmed = 42,
        AutoUpsaleCallbacks = 43,
        RejectedCalls = 44,
        NumberOfAttemptsAar = 45,
        ConversionRateSrNoUpsales = 46,
        AmountOfCallsRealMoney = 47,
        RevenueCallsRealMoney = 48,
        AvgPricePerCallRealMoney = 49
    }

    //    AmountOfCalls = 1,
    //    RevenueFromCalls = 2,
    //    AveragePricePerCall = 3,
    //    AveragePreDefinedPrice = 4,
    //    AmountOfRequests = 5,
    //    SoldCallsPerRequets = 6,
    //    AmountOfDeposits = 7,
    //    AmountOfRecurringDeposits = 8,
    //    RevenueFromDeposits = 9,
    //    RevenueFromRecurringDeposits = 10,
    //    AverageDeposits = 11,
    //    AverageRecurringDeposits = 12,
    //    BalanceSum = 13,
    //    AmountOfAdvertisers = 14,
    //    OutOfMoneyCount = 15,
    //    ConversionRateDN = 16,
    //    ConversionRateSR = 17,
    //    AmountOfRefundedCalls = 18,
    //    PercentageOfRefunds = 19,
    //    MoneyRefunded = 20

    public enum Color
    {
        Red, Green
    }

    public enum AarCriteria
    {
        AvgMinutesForAcPress1,
        AvgMinutesForItcPress1,
        AvgMinutesForItcLastPress1,
        AvgAttemptsForAcPress1,
        AvgAttemptsForItcPress1,
        AvgAttemptsForItcLastPress1,
        AvgMinutesForFullfilment,
        AvgAttemptsForFullfilment,
        NumberOfFullfiledCases,
        NumberOfNotFullfiledCases,
        NumberOfAttemptsToAcs,
        NumberOfAttemptsToItc,
        NumberOfAttemptsToItcLast
    }
}