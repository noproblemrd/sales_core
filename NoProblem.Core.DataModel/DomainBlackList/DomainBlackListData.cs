﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.DomainBlackList
{
    public class DomainBlackListData
    {
        public string Domain { get; set; }
        public string Keyword { get; set; }
        public int Requests { get; set; }
        public int Exposures { get; set; }
        public decimal? RecruitmentRevenue { get; set; }
        public decimal MinConversionConst { get; set; }
        public DateTime? DateOfFirstExposure { get; set; }
        public decimal? PotentialRevenue { get; set; }
        public decimal? NetActualRevenue { get; set; }
        public decimal? ActualRevenue { get; set; }
        public decimal MinNetActualRpmConst { get; set; }
        public bool Passed { get; set; }
        public int WebExposures { get; set; }
    }
}
