﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Deskapp
{
    public enum eConsumerSignUpDesktopStatus
    {
        PhoneAllreadyExists,
        OK,
        PhoneValidationFaild,
        SmsFaildToSend

    }
    public class ConsumerSignUpDesktopResponse
    {
        public string PinCode { get; set; }
        public eConsumerSignUpDesktopStatus Status { get; set; }
        public Guid ConsumerId { get; set; }
    }
}
