﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Deskapp
{
    public class CallRecordRequest
    {
        public Guid ContactId { get; set; }
        public string BobPhone { get; set; }
        public bool BlockCallerId { get; set; }
        public int TimezoneOffset { get; set; }
    }
}
