﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Deskapp
{
    public class ConsumerRegisterPincodeRequest
    {
        public string phone { get; set; }
        public string PinCode { get; set; }
    }
}
