﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Deskapp
{
    public class ConsumerSignUpDesktopRequest
    {
        public string phone { get; set; }
        public string IP { get; set; }
        public string Browser { get; set; }
        public string Country { get; set; }
        public Guid UserUniqueId { get; set; }
    }
}
