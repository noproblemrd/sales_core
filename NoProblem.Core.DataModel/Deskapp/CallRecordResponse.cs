﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Deskapp
{
    public enum eCallRecordResponseStatus
    {
        StartSuccessfully,
        NoSecondRecordLeft,
        ContactIdNotFound,
        ThereIsNotContactPhoneNumber,
        Faild
    }
    public class CallRecordResponse
    {
        public eCallRecordResponseStatus ResponseStatus { get; set; }
        public Guid CallId { get; set; }
    }
}
