﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Tables
{
    public class UpsertUnavailabilityReasonRequest
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public bool IsSystem { get; set; }
        public int GroupId { get; set; }
    }
}
