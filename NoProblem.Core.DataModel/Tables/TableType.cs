﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Tables
{
    public enum eTableType
    {
        ComplaintStatus,
        ComplaintSeverity,
        InactivityReason,
        UpsaleLoseReason,
        RefundReason,
        UpsalePendingReason,
        ExtraBonusReason,
        VoucherReason
    }
}
