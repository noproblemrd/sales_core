﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Tables
{
    public class UpsertTableDataRequest
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public eTableType Type { get; set; }
    }
}
