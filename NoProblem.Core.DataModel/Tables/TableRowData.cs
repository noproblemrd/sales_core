﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Tables
{
    public class TableRowData
    {
        public Guid Guid { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Inactive { get; set; }
    }
}
