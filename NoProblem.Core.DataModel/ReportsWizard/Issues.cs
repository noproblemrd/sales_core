﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ReportsWizard
{
    public enum Issues
    {
        Advertisers,
        Calls,
        Requests,
        Deposits
    }
}
