﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ReportsWizard
{
    public class Filter
    {
        public string Operator { get; set; }
        public object Value { get; set; }
        public string FilterField { get; set; }
    }
}
