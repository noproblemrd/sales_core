﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.ReportsWizard
{
    public class WizardReportRequest
    {
        public List<Filter> Filters { get; set; }
        public List<string> Columns { get; set; }
        public string OrderBy { get; set; }
        public ReportsWizard.Issues Issue { get; set; }
        public bool Descending { get; set; }
        public string GroupBy { get; set; }
    }
}
