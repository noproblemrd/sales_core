﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.Soleo
{
    public class SoleoPingResponse
    {
        const string SUCCESS = "success";
        const string FAILED = "failed";
        public SoleoPingResponse()
        {
            listings = new List<SoleoDataResponse>();
        }
        public List<SoleoDataResponse> listings { get; set; }
        public string result { get; set; }
        public string searchid { get; set; }
        public void SetResult(bool isSuccess)
        {
            result = isSuccess ? SUCCESS : FAILED;
        }
    }
    public class SoleoDataResponse
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string listingid { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public string address1 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
       // public string latitude { get; set; }
       // public string longtitude { get; set; }
        public decimal price { get; set; }
        public string recordedname { get; set; }
        public int duration { get; set; }
    }
   
}
