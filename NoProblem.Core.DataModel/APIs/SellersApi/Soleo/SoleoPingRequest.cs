﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.Soleo
{
    public class SoleoPingRequest
    {
        public string category { get; set; }
        public string location { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public int count { get; set; }
        public string listingtype { get; set; }
        public string requesterid { get; set; }
        public Guid sellerId { get; set; }
        public string callerId { get; set; }
        public bool isTest { get; set; }
    }
}
