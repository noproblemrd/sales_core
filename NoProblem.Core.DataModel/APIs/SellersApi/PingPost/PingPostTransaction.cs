﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.PingPost
{
    public class PingPostTransaction : MongoModel
    {
        public PingPostTransaction()
        {
            PingOn = DateTime.Now;
            Buyers = new List<Buyer>();
            PingRejects = new List<Buyer>();
            StatusReasons = new List<string>();
            PostMessages = new List<string>();
            PostData = new PostLeadData();
            PingData = new PingLeadData();
        }

        public static class StatusOptions
        {
            public const string PING_ACCEPTED = "ping_accepted";
            public const string PING_REJECTED = "ping_rejected";
            public const string POST_SUCCESS = "post_success";
            public const string POST_FAILED = "post_failed";
        }

        public class Buyer
        {
            public string SupplierId { get; set; }
            public decimal Price { get; set; }
            public string PingId { get; set; }
            public bool Sold { get; set; }
            public decimal Rank { get; set; }
            public string LeadBuyerName { get; set; }
            public string ExternalCode { get; set; }
            public string Message { get; set; }
        }

        public class PostLeadData
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public string StreetAddress { get; set; }
            public string IP { get; set; }
            public string Description { get; set; }
            public string AdditionalInfo { get; set; }
        }

        public class PingLeadData
        {
            public string IP { get; set; }
            public string Description { get; set; }
        }

        public override MongoDB.Bson.ObjectId _id { get; set; }
        public string Status { get; set; }
        public List<string> StatusReasons { get; set; }
        public List<string> PostMessages { get; set; }
        public decimal Price { get; set; }
        public bool IsStaticPricing { get; set; }
        public decimal RevenueSharePercentage { get; set; }
        public string LeadId { get; set; }
        public string CategoryCode { get; set; }
        public string Zip { get; set; }
        public string OriginId { get; set; }
        public List<Buyer> Buyers { get; set; }
        public List<Buyer> PingRejects { get; set; }
        public PostLeadData PostData { get; set; }
        public PingLeadData PingData { get; set; }
        public bool Test { get; set; }
        public DateTime PingOn { get; set; }
        public DateTime PostOn { get; set; }
        public bool IsDuplicate { get; set; }
    }
}

/*
{
	"_id":"",
	"status":"",
	"status_reasons":["","",""],
	"post_messages":["",""],
	"price":9,
	"lead_id":"blabla",
	"category_code":"2090",
	"zip":"10001",
	"origin_id":"asasdfasdf-asdfasdf-asdf",
	"buyers":[
			{
				"id":"aasdf-asdf-asdf",
				"price":15,
				"pingid":"asdf",
				"sold":true
			},
			{
				"id":"aasdf-asdf-asdf",
				"price":12,
				"pingid":"asdf",
				"sold":false
			}
		],
	"post_data":{
		"first_name":"",
		"last_name":"",
		"email":"",
		"phone":"",
		"street_address":""
	}
}
*/