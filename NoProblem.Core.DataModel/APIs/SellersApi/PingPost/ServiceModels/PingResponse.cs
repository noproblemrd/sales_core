﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.PingPost
{
    public class PingResponse
    {
        public bool Interested { get; set; }
        public string NpPingId { get; set; }
        public decimal Price { get; set; }
    }
}
