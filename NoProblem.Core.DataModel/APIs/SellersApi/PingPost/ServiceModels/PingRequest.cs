﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.PingPost
{
    public class PingRequest
    {
        public string CategoryCode { get; set; }

        public string Zip { get; set; }

        public string LeadId { get; set; }

        public Guid SellerId { get; set; }

        public int Test { get; set; }

        public string IP { get; set; }

        public string Description { get; set; }
    }
}
