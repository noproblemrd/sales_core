﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.PingPost
{
    public class PostResponse
    {
        public bool Success { get; set; }
        public List<string> Messages { get; set; }
    }
}
