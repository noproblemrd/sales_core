﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.PingPost
{
    public class PostRequest
    {
        public string Phone { get; set; }

        public string StreetAddress { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string NpPingId { get; set; }

        public Guid SellerId { get; set; }

        public string IP { get; set; }

        public string Description { get; set; }

        public string AdditionalInfo { get; set; }
    }
}
