﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.PingPost
{
    public class SellerData
    {
        public bool IsStaticPricing { get; set; }
        public decimal StaticPrice { get; set; }
        public decimal RevenueSharePercentage { get; set; }
    }
}
