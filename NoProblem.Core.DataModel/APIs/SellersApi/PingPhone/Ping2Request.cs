﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.PingPhone
{
    public class Ping2Request
    {
        public string CallerId { get; set; }
        public bool IsCallerIdReal { get; set; }
        public string LeadId { get; set; }
        public string NpPingId { get; set; }
        public string CallbackUrl { get; set; }
        public string SellerId { get; set; }    }
}
