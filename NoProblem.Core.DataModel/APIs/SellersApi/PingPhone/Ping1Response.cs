﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.PingPhone
{
    public class Ping1Response
    {
        public Ping1Response()
        {
            IsValid = true;
            Interested = true;
        }

        public bool Interested { get; set; }
        public string NpPingId { get; set; }
        public decimal Price { get; set; }
        public int BillingCallDurationSeconds { get; set; }
        public bool IsValid { get; set; }
        public string ValidationError { get; set; }
    }
}
