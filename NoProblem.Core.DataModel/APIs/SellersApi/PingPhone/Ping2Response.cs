﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.PingPhone
{
    public class Ping2Response
    {
        public Ping2Response()
        {
            IsValid = true;
        }

        public string NpPhone { get; set; }
        public bool IsValid { get; set; }
        public string ValidationError { get; set; }
    }
}
