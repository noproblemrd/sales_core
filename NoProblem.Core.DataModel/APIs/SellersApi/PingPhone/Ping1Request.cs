﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.SellersApi.PingPhone
{
    public class Ping1Request
    {
        public string Category { get; set; }
        public string Zip { get; set; }
        public string LeadId { get; set; }
        public string SellerId { get; set; }
        public int Test { get; set; }
        public string TestProviderPhone { get; set; }        
    }
}
