﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.MobileApi
{
    public class LiveBid
    {
        public decimal MinBid { get; set; }
        public decimal MaxBid { get; set; }
        public DateTime BidTimeOutAtUtc { get; set; }
        public string Description { get; set; }
        public Guid IncidentAccountId { get; set; }
        public string Category { get; set; }
        public string CustomerHiddenPhone { get; set; }
        public string CustomerCityAndState { get; set; }
        public string CustomerHiddenName { get; set; }
        public decimal DefaultBid { get; set; }
        public DateTime LeadCreatedOnUtc { get; set; }
    }
}
