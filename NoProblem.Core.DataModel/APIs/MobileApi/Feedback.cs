﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.MobileApi
{
    public class Feedback
    {
        public Guid SupplierId { get; set; }
        public string Header { get; set; }
        public string Comment { get; set; }
    }
}
