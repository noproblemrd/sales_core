﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.APIs.MobileApi
{
    public class Settings
    {
        public DateTime? StopNotificationsUntil { get; set; }
        public bool RecordMyCalls { get; set; }
        public string CreditCardLastFourDigits { get; set; }
    }
}
