﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.Refunds.RefundReasonData
//  File: RefundReasonData.cs
//  Description: Container that represents a single refund reason.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
namespace NoProblem.Core.DataModel.Refunds
{
    public class RefundReasonData
    {
        public int Code { get; set; }
        public string Name { get; set; }
        public bool Inactive { get; set; }
        public Guid Guid { get; set; }
    }
}
