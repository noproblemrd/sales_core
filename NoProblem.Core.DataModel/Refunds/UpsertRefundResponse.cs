﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Refunds
{
    public class UpsertRefundResponse
    {
        public UpsertRefundStatus Status { get; set; }
        public decimal NewBalance { get; set; }
    }

    public enum UpsertRefundStatus
    {
        OK,
        BlockApprovalNeeded,
        CallWasNotCharged,
        NoRefundAllowed
    }
}
