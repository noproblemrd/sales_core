﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Refunds
{
    public class RefundData
    {
        public Guid CaseId { get; set; }
        public string TicketNumber { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string RefundReasonName { get; set; }
        public string RefundReasonCode { get; set; }
        public DateTime RefundRequestDate { get; set; }
        public string ExpertiseName { get; set; }
        public string RefundNote { get; set; }
        public string RefundNoteForAdvertiser { get; set; }
        public string RecordFileLocation { get; set; }
        public string RefundStatus { get; set; }
        public decimal Price { get; set; }
        public double RefundRequestPercentage { get; set; }
        public double RefundsApprovedPercentage { get; set; }
    }
}
