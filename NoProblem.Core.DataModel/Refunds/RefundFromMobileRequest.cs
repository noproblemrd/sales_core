﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Refunds
{
    public class RefundFromMobileRequest
    {
        public Guid IncidentAccountId { get; set; }
        public Guid SupplierId { get; set; }
        public string Comment { get; set; }
        public int ReasonCode { get; set; }
    }
}
