﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.Refunds.GetAllRefundReasonsResponse
//  File: GetAllRefundReasonsResponse.cs
//  Description: Container to use as response for the GetAllRefundReasons service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace NoProblem.Core.DataModel.Refunds
{
    public class GetAllRefundReasonsResponse
    {
        public List<DataModel.Refunds.RefundReasonData> RefundReasons { get; set; }
    }
}
