﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.Refunds.GetRefundsReportResponse
//  File: GetRefundsReportResponse.cs
//  Description: Container to use as response for the GetRefundsReport service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace NoProblem.Core.DataModel.Refunds
{
    public class GetRefundsReportResponse : PagingResponse
    {
        public List<DataModel.Refunds.RefundData> RefundRequests { get; set; }
    }
}
