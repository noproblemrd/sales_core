﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.Refunds.GetRefundsReportRequest
//  File: GetRefundsReportRequest.cs
//  Description: Container to use as request for the GetRefundsReport service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.Refunds
{
    public class GetRefundsReportRequest : PagingRequest
    {
        public Xrm.new_incidentaccount.RefundSatus? RefundStatus { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
