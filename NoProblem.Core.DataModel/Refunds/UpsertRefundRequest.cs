﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.Refunds.UpsertRefundRequest
//  File: UpsertRefundRequest.cs
//  Description: Container to use as request for the UpsertRefund service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.Refunds
{
    public class UpsertRefundRequest
    {
        public Guid SupplierId { get; set; }
        public Guid IncidentId { get; set; }
        public Guid IncidentAccountId { get; set; }
        public int? RefundReasonCode { get; set; }
        public string RefundNote { get; set; }
        public Guid RefundOwnerId { get; set; }
        public Xrm.new_incidentaccount.RefundSatus? RefundStatus { get; set; }
        public string RefundNoteForAdvertiser { get; set; }
        public bool IsBlockApproved { get; set; }
    }
}
