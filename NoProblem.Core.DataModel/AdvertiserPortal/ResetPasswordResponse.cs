﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.AdvertiserPortal.ResetPasswordResponse
//  File: ResetPasswordResponse.cs
//  Description: Container to use as response for the ResetPassword service.
//                  Enum ResetPasswordResponseStatus is the posible answers.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace NoProblem.Core.DataModel.AdvertiserPortal
{
    public class ResetPasswordResponse
    {
        public ResetPasswordResponseStatus ResetPasswordStatus { get; set; }
    }

    public enum ResetPasswordResponseStatus
    {
        OK,
        UserNotFound
    }
}
