﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserPortal
{
    public class SetAvailabilityRequest : Audit.AuditRequest
    {
        /// <summary>
        /// Old string xml request.
        /// </summary>
        public string Request { get; set; }
        public bool IsFromAAR { get; set; }
        public bool IsFromNewAdvertiserPortal { get; set; }
        public Guid SupplierId { get; set; }
    }
}
