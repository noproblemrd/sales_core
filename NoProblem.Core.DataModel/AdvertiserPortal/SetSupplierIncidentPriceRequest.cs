﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserPortal
{
    public class SetSupplierIncidentPriceRequest : Audit.AuditRequest
    {
        public string SupplierId { get; set; }
        public decimal IncidentPrice { get; set; }
        public bool IsBid { get; set; }
    }
}
