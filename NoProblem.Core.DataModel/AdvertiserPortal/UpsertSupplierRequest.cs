﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserPortal
{
    public class UpsertSupplierRequest : Audit.AuditRequest
    {
        /// <summary>
        /// Old string xml request.
        /// </summary>
        public string Request { get; set; }
    }
}
