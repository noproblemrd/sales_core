﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserPortal
{
    public class CreateSupplierExpertiseRequest : Audit.AuditRequest
    {
        /// <summary>
        /// Old string xml request.
        /// </summary>
        public string Request { get; set; }
        public bool IsFromAAR { get; set; }
        public Guid UserId { get; set; }
    }
}
