﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.AdvertiserPortal.ResetPasswordRequest
//  File: ResetPasswordRequest.cs
//  Description: Container to use as request for the ResetPassword service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace NoProblem.Core.DataModel.AdvertiserPortal
{
    public class ResetPasswordRequest
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
