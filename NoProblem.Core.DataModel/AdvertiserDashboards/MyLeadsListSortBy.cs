﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public enum MyLeadsListSortBy
    {
        Dates,
        Status,
        Heading
    }
}
