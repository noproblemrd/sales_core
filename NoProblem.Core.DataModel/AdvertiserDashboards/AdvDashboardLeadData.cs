﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class AdvDashboardLeadData
    {
        public DateTime CreatedOn { get; set; }
        public DateTime CreatedOnSupplierTime { get; set; }
        public string HeadingName { get; set; }
        public string RecordFileLocation { get; set; }
        public string Description { get; set; }
        public string Region { get; set; }
        public string CustomerPhone { get; set; }
        public AdvDashboardLeadStatus Status { get; set; }
        public string LeadNumber { get; set; }
        public Guid IncidentId { get; set; }
        public int MyPrice { get; set; }
        public int WinningPrice { get; set; }
        public DateTime RefundedOn { get; set; }
        public bool Charged { get; set; }

        public enum AdvDashboardLeadStatus
        {
            Won,
            WonNoAudio,
            Refunded,
            Lost,
            Rejected
        }
    }
}
