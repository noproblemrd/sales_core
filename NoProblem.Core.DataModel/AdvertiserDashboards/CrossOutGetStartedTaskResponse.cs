﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class CrossOutGetStartedTaskResponse
    {
        public decimal Balance { get; set; }
        public bool AllTasksDone { get; set; }
        public int TasksLeft { get; set; }
        public int BonusAmount { get; set; }
    }
}
