﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class LiveTestCallStatus
    {
        public bool ShowInReport { get; set; }
        public LiveTestCallStage Stage { get; set; }

        public enum LiveTestCallStage
        {
            failed,
            ringing,
            advertiser_answered,
            advertiser_confirmed,
            completed,
            busy,
            no_answer
        }        
    }
}
