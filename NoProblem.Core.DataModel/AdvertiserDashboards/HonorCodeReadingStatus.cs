﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class HonorCodeReadingStatus
    {
        public int Counter { get; set; }
        public bool IsClickedDone { get; set; }
    }
}
