﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class GetStartedTasksStatusResponse
    {
        public List<GetStartedTasksStatus> TasksList { get; set; }
        public int BonusAmount { get; set; }
        public GetStartedTasksStatus Register { get; set; }
        public GetStartedTasksStatus DefaultPrice { get; set; }
        public GetStartedTasksStatus ListenRecording { get; set; }
        public GetStartedTasksStatus CallTest { get; set; }
        public GetStartedTasksStatus HonorCode { get; set; }
        public GetStartedTasksStatus VerifyEmail { get; set; }
        public GetStartedTasksStatus WorkHours { get; set; }
        public int TasksLeft { get; set; }
        public string CallerId { get; set; }
        public string SupplierPhone { get; set; }
    }
}
