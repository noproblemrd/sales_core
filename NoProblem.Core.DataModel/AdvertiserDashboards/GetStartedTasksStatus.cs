﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class GetStartedTasksStatus
    {
        public GetStartedTaskType TaskType { get; set; }
        public bool Done { get; set; }
        public string Description { get; set; }
        public int Rank { get; set; }
    }
}
