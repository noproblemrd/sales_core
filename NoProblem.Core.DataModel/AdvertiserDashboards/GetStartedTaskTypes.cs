﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public enum GetStartedTaskType
    {
        work_hours,
        honor_code,
        call_test,
        listen_recording,
        register,
        default_price,
        verify_email
    }
}
