﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class GetMyLeadsResponse
    {
        public bool IsRecordsCalls { get; set; }
        public List<AdvDashboardLeadData> LeadsList { get; set; }
        public bool LeadsExist { get; set; }
        public bool IsLeadBroker { get; set; }
    }
}
