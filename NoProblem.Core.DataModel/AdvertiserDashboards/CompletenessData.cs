﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class CompletenessData
    {
        public int PercentageDone { get; set; }
        public CompletenessDataLinkOptions? LinkTo { get; set; }
        public int PercentageInLink { get; set; }

        public enum CompletenessDataLinkOptions
        {
            WorkHours,
            Details,
            Payment
        }
    }
}
