﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview
{
    public class TransactionRow2014
    {
        public decimal Amount { get; set; }
        public DateTime DateLocal { get; set; }
        public DateTime DateServer { get; set; }
        public string Type { get; set; }
    }
}
