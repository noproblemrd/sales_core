﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview
{
    public class GetAccountActivityResponse2014
    {
        public int Month { get; set; }
        public int LeadCount { get; set; }
        public decimal LeadMoney { get; set; }
        public int RefundCount { get; set; }
        public decimal RefundMoney { get; set; }
        public int EarnedFromFriendCount { get; set; }
        public decimal EarnedFromFriendMoney { get; set; }
        public int GetStartedBonusCount { get; set; }
        public decimal GetStartedBonusMoney { get; set; }
        public decimal TenLeadsBonusCount { get; set; }
        public decimal TenLeadsBonusMoney { get; set; }
        public decimal MonthlyFee { get; set; }
        public decimal MonthlyFeeRefund { get; set; }
        public decimal TotalUsed { get; set; }
        public decimal TotalBonus { get; set; }
    }
}
