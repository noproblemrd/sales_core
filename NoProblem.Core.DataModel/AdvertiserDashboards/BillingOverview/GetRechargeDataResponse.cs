﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview
{
    public class GetRechargeDataResponse
    {
        public int RechargeAmount { get; set; }
        public decimal MinPrice { get; set; }
    }
}
