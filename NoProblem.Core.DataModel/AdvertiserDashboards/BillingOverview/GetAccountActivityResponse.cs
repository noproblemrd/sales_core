﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview
{
    public class GetAccountActivityResponse
    {
        public int Month { get; set; }
        public int LeadCount { get; set; }
        public int LeadMoney { get; set; }
        public int RefundCount { get; set; }
        public int RefundMoney { get; set; }
        public int EarnedCount { get; set; }
        public int EarnedMoney { get; set; }
        public int BonusCount { get; set; }
        public int BonusMoney { get; set; }        
        public int TotalUsed { get; set; }
        public int TotalBonus { get; set; }
    }
}
