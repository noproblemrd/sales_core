﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class PhoneAvailabilityData
    {
        public int PercentageOfWeeklyHours { get; set; }
        public int LeadsOn { get; set; }
        public int LeadOff { get; set; }
    }
}
