﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class GetStartedTestCallLeadData
    {
        public Guid Id { get; set; }
        public List<string> RegionsHirarchy { get; set; }
        public string HeadingIvrName { get; set; }
        public int TimeZoneCode { get; set; }
        public string RecordingUrl { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
