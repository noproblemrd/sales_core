﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards
{
    public class GetMyLeadsListRequest
    {
        public Guid SupplierId { get; set; }
        public MyLeadsListPeriod Period { get; set; }
        public MyLeadsListSortBy SortBy { get; set; }
        public bool SortDescending { get; set; }
    }
}
