﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards.Invitations
{
    public class CreateInvitationRequest
    {
        public Guid SupplierId { get; set; }
        public string InvitedEmail { get; set; }
        public string PersonalMessage { get; set; }
    }
}
