﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards.Invitations
{
    public class InvitationData
    {
        public int InvitationsLeft { get; set; }
        public int InvitationSuccessBonus { get; set; }
        public int InvitationLifetimeBonus { get; set; }
        public string Email { get; set; }
        public bool RequestedMoreInvites { get; set; }
        public bool JustGotMoreInvites { get; set; }
    }
}
