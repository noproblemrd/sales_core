﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserDashboards.Invitations
{
    public class CreateInvitationResponse
    {
        public enum CreateInvitationStatus
        {
            OK,
            NoInvitationsLeft,
            AlreadyInvited,
            AleradyRegistered,
            EmailFailed,
            NeedsEmailVerification
        }

        public CreateInvitationStatus Status { get; set; }
        public int InvitationsLeft { get; set; }
    }
}
