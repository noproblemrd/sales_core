﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Upsale
{
    public class UpsaleData
    {
        public Guid UpsaleId { get; set; }
        public string UpsaleNumber { get; set; }
        public Guid ConsumerId { get; set; }
        public string ConsumerPhone { get; set; }
        public string ExpertiseName { get; set; }
        public int NumOfRequestedAdvertisers { get; set; }
        public int NumOfConnectedAdvertisers { get; set; }
        public Xrm.new_upsale.UpsaleStatus UpsaleStatus { get; set; }
        public Guid StatusReasonId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime TimeToCall { get; set; }
        public string ExpertiseCode { get; set; }
    }
}
