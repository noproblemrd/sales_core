﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Upsale
{
    public class UpsaleDataExpanded : UpsaleData
    {
        public List<ConnectedSupplierData> ConnectedSuppliers { get; set; }
        public List<string> Regions { get; set; }
    }
}
