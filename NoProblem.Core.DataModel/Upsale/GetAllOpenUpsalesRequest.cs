﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Upsale
{
    public class GetAllOpenUpsalesRequest
    {
        public Guid ExpertiseId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
