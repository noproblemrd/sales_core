﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Upsale
{
    public class UpdateUpsaleRequest
    {
        public Guid UpsaleId { get; set; }
        public Guid UserId { get; set; }
        public Xrm.new_upsale.UpsaleStatus Status { get; set; }
        public Guid StatusReasonId { get; set; }
        public DateTime TimeToCall { get; set; }
    }
}
