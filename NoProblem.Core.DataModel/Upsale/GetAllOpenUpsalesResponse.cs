﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Upsale
{
    public class GetAllOpenUpsalesResponse : PagingResponse
    {
        public List<Upsale.UpsaleData> DataList { get; set; }
    }
}
