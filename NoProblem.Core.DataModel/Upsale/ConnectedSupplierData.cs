﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Upsale
{
    public class ConnectedSupplierData
    {
        public Guid CaseId { get; set; }
        public string CaseNumber { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public DataModel.Case.EndStatus CallStatus { get; set; }
        public DateTime CallTime { get; set; }
        public string RegionName { get; set; }
        public DataModel.Xrm.incident.CaseTypeCode CaseType { get; set; }
    }
}
