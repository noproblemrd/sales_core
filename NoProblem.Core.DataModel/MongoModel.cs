﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel
{
    public abstract class MongoModel
    {
        public abstract MongoDB.Bson.ObjectId _id { get; set; }
    }
}
