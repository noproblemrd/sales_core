﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AAR
{
    public enum ClickHandlerStatus
    {
        ConnectToConsumer,
        Representative,
        HowToAccess,
        DisabledClick,
        OnlyText,
        HangUp
    }
}
