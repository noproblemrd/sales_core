﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class SupplierData
    {
        public Guid SupplierId { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public eCompletedRegistrationStep? StageInRegistration { get; set; }
        public int StageInTrialRegistration { get; set; }
        public decimal Balance { get; set; }
        public DateTime CreatonOn { get; set; }        
        public SupplierStatus Status { get; set; }
        public bool IsFromAar { get; set; }
        public string TimeZone { get; set; }
    }
}
