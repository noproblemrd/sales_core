﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class PreDefinedPriceData
    {
        public DateTime Date { get; set; }
        public decimal PreDefinedPrice { get; set; }
        public decimal ActualPrice { get; set; }
    }
}
