﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class BrokersSummaryReportRequest
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public bool CalculateBillingRateByDates { get; set; }
    }
}
