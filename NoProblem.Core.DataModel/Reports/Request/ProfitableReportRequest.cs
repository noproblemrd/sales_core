﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class ProfitableReportRequest
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Guid HeadingId { get; set; }
        public Guid OriginId { get; set; }
        public bool OnlyNonUpsales { get; set; }
        public bool UseCompare { get; set; }
        public DateTime FromCompare { get; set; }
        public DateTime ToCompare { get; set; }
        public Guid HeadingIdCompare { get; set; }
        public Guid OriginIdCompare { get; set; }
        public bool OnlyNonUpsalesCompare { get; set; }
        public eProfitFilter ProfitFilter { get; set; }
        public int Country { get; set; }
        public int CountryCompare { get; set; }
        public int SliderType { get; set; }
        public int SliderTypeCompare { get; set; }

        public enum eProfitFilter
        {
            All,
            Losing,
            Profitable
        }
    }
}
