﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class AddOnInstallationReportRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Guid[] Origins { get; set; }
    //    public bool IsSummery { get; set; }
        public string Country { get; set; }
    }
}
