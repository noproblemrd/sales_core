﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.CallsReport
{
    public class CallsReportRequest : PagingRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Guid ExpertiseId { get; set; }
        public CallType CallType { get; set; }
        public Guid UserId { get; set; }
        public Guid SupplierOriginId { get; set; }//The origin where the supplier registered (not the call came from).
    }

    public enum CallType
    {
        All, 
        DirectNumber, 
        SelfService,
        Click2
    }
}
