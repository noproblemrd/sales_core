﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class RootUrlExposuresFullUrlRequest : RootUrlExposuresKeywordDrillDownRequest
    {
        public string Keyword { get; set; }
    }
}
