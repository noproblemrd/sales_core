﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Request
{
    public class InjectionReportRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Guid[] Origins { get; set; }
        public string Country { get; set; }
        public Guid InjectionId { get; set; }
    }
}
