﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Request
{
    public class NP_DistributionReportRequest
    {
        public DateTime _from { get; set; }
        public DateTime _to { get; set; }
        
        public NP_DistributionReportRequest() { }

        public DateTime From
        {
            get { return _from; }
        }
        public DateTime To
        {
            get { return _to.AddDays(1); }
        }
    }
}
