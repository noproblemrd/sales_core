﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Request
{
    public enum eHeartBeatChartType
    {
        Injection,
        Logic,
        Hits_KDAUs
    }
    public class HeartBeatInjectionRequest
    {
        public Guid[] InjectionId { get; set; }
        public Guid CampaignId { get; set; }
        public DateTime _from { get; set; }
        public DateTime _to { get; set; }
        public eHeartBeatChartType HeartBeatChartType { get; set; }
        public HeartBeatInjectionRequest() { }
        public DateTime From { get { return _from; } }
        public DateTime To { get { return _to.AddDays(1); } }
        public string GetInjectionsIds()
        {
            if (InjectionId == null || InjectionId.Length == 0)
                return string.Empty;
            StringBuilder sb = new StringBuilder();
            foreach(Guid _id in InjectionId)
                sb.Append(_id.ToString() + ";");
            return sb.ToString();
        }


        
    }
}
