﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class NonPpcCallsReportRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public eGroupBy GroupBy { get; set; }
        public string AdvertiserId { get; set; }

        public enum eGroupBy
        {
            None,
            Advertisers,
            Dates
        }
    }
}
