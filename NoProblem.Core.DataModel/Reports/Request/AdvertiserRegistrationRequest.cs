﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Request
{
    public class AdvertiserRegistrationRequest
    {
        const int INTERVAL_TO_DATE = 1;//For sql query

        public eRegisterCameFrom CameFrom { get; set; }
        public eRegisterPlan Plan { get; set; }
        public eRegisterLandingPageType landingPage { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Guid? SalseManId { get; set; }
        public AdvertiserRegistrationRequest CompareRequest { get; set; }
        public bool ShowStep1 { get; set; }
        public string SliderFlavor { get; set; }
        public void SetToDate()
        {
            To = To.AddDays(INTERVAL_TO_DATE);
            if (CompareRequest != null)
                CompareRequest.SetToDate();
        }
        public DateTime OriginalToDate
        {
            get { return To.AddDays(-1 * INTERVAL_TO_DATE); }
        }
        public string GetSliderFlavor
        {
            get
            {
                if (string.IsNullOrEmpty(this.SliderFlavor) || this.SliderFlavor.ToLower().StartsWith("all"))
                    return null;
                return this.SliderFlavor;
            }
        }       
    
    
    }

    public enum eRegisterCameFrom
    {
        All = 1,
        Slider = 2,
        RecruitmentSlider = 3,
        Claim = 4,
        Email = 5,
        Other = 6
        
    }

    public enum eRegisterPlan
    {
        All = 1,
        FeaturedListing = 2,
        LeadsBooster = 3
    }

    public enum eRegisterLandingPageType
    {
        All=1,
        A=2,
        B=3,
        C=4,
        D=5
    }
}
