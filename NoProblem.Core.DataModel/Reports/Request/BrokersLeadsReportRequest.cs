﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class BrokersLeadsReportRequest
    {
        public Guid BrokerId { get; set; }
        /*
        public int Month { get; set; }
        public int Year { get; set; }
         * */
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public bool IncludeNonBilledCalls { get; set; }
    }
}
