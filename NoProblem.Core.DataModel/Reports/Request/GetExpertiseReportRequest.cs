﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.Reports.GetExpertiseReportRequest
//  File: GetExpertiseReportRequest.cs
//  Description: Container to use as request for the GetExpertiseReport service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace NoProblem.Core.DataModel.Reports
{
    public class GetExpertiseReportRequest
    {
        public Guid ExpertiseId { get; set; }
        public int ExpertiseLevel { get; set; }
    }
}
