﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class DepositReportRequest : PagingRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public eDepositCreatedBy CreatedBy { get; set; }
        public eDepositType DepositType { get; set; }
        public Guid ExpertiseId { get; set; }

        public enum eDepositCreatedBy
        {
            All,
            Publisher, 
            Advertiser, 
            Auto
        }

        public enum eDepositType
        {
            Recurring = 0,
            First = 1,
            All = 2
        }
    }
}
