﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class ConversionDrillDownRequest
    {
        public DateTime from { get; set; }
        public DateTime to { get; set; }
        public Guid ExpertiseId { get; set; }
        public string ControlName { get; set; }
        public string Domain { get; set; }
        public eConversionInterval Interval { get; set; }
        public int CountryId { get; set; }
        public int SliderType { get; set; }
    }
}
