﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class RequestsReportRequest : PagingRequest
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public Guid ExpertiseId { get; set; }
        public string CaseNumber { get; set; }
        public CaseType Type { get; set; }
        public Guid AffiliateOrigin { get; set; }
        public int? SuppliersProvided { get; set; }
        public Guid CaseOrigin { get; set; }
        public string Url { get; set; }
        public bool OnlyForUpsale { get; set; }
        public Guid ToolbarId { get; set; }
        public int? NumberOfUpsales { get; set; }
        public eCallDurationType WithMaxCallDuration { get; set; }
        public bool OnlyRejectByEmailOrName { get; set; }
        public int SliderType { get; set; }

        public enum CaseType
        {
            All,
            DirectNumber,
            ServiceRequest,
            Click2
        }
        public enum eCallDurationType
        {
            Without = 1,
            With = 2,
            Zero = 3
        }
    }
}
