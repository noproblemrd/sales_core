﻿using System;

namespace NoProblem.Core.DataModel.Reports
{
    public class DescriptionValidationReportRequest : PagingRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public enumQualityCheckName? QualityCheckName { get; set; }
        public bool? Passed { get; set; }
        public string Flavor { get; set; }
        public string ZipCodeCheckName { get; set; }
        public string Event { get; set; }
        public string Control { get; set; }
        public string Step { get; set; }
    }
}
