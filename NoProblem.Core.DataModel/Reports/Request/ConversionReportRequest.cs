﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class ConversionReportRequest
    {
        public Guid ExpertiseId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ControlName { get; set; }
        public Guid OriginId { get; set; }
        public string Domain { get; set; }
        public eConversionInterval Interval { get; set; }
        public IntervalTimeReport interval_time_report { get; set; }
        public int CountryId { get; set; }
        public int SliderType { get; set; }
    }
}
