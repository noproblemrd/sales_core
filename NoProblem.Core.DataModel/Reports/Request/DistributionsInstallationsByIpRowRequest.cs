﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Request
{
    public class DistributionsInstallationsByIpRowRequest
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Guid OriginId { get; set; }
    }
}
