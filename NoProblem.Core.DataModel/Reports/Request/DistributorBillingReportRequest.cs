﻿using System;

namespace NoProblem.Core.DataModel.Reports
{
    public class DistributorBillingReportRequest
    {
        public Guid OriginId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
    }
}
