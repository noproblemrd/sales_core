﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class RefundReportRequest : PagingRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Guid HeadingId { get; set; }
    }
}
