﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class MyCallsReportRequest
    {
        public bool IsPublisher { get; set; }
        public Guid SupplierId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public IncidentWinStatus Status { get; set; }
    }
}
