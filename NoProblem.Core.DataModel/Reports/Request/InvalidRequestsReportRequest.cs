﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class InvalidRequestsReportRequest
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public InvalidRequestStatus InvalidStatus { get; set; }
        public string PhoneNumber { get; set; }
        public string BadWord { get; set; }
    }

    public enum InvalidRequestStatus
    {
        All,
        BlackList,
        BadWord,
        InvalidPhoneNumber
    }
}
