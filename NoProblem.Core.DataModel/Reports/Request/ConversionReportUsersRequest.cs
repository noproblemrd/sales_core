﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Request
{
    public class ConversionReportUsersRequest
    {
        public Guid ExpertiseId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
      //  public string ControlName { get; set; }
        public Guid[] Origins { get; set; }
        public eConversionInterval Interval { get; set; }
        public IntervalTimeReport interval_time_report { get; set; }
        public int CountryId { get; set; }
        public int SliderType { get; set; }
        public string OriginsForSQL
        {
            get
            {
                string _origins = "";
                foreach (Guid id in Origins)
                {
                    _origins += id.ToString();
                }
                return _origins;
            }
        }
    }
}
