﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Request
{
    public class PplReportRequest
    {
        public Guid HeadingId { get; set; }
        public Guid RegionId { get; set; }
        public string ZipCode { get; set; }
    }
}
