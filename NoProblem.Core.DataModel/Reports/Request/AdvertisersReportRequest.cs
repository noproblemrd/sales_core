﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NoProblem.Core.DataModel.Reports
{
    public class AdvertisersReportRequest //: PagingRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal? MaxBalance { get; set; }
        public SupplierStatus SupplierStatus { get; set; }
        public bool? IsFromAAR { get; set; }
        public bool? IsWebRegistrant { get; set; }
        public eCompletedRegistrationStep RegistrationStep { get; set; }
    }

    public enum SupplierStatus
    {
        Available,
        Unavailable,
        Candidate,
        Inactive,
        All,
        ITC,
        AC,
        Expired,
        FreshlyImported,
        InRegistration,
        RemoveMe,
        DoNotCallList,
        Machine
    }
    public enum eCompletedRegistrationStep
    {
        
        LandedOnJoinPage = 0,
        UserCreation = 1,
        EmailVerification = 2,
        ChoosePlan = 3,
        BusinessDetails = 4,
        Categories = 5,
        CoverArea = 6,
        Checkout = 7,
        PaymentRegistered = 8,
        All = 9
    }
}
