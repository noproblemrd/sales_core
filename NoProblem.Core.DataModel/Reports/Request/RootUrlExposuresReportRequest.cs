﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class RootUrlExposuresReportRequest : PagingRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Guid OriginId { get; set; }
        public int ExposuresCount { get; set; }
        public eRootUrlExposuresCount? RootUrlExposuresCountType { get; set; }
        public string FreeText { get; set; }

        public enum eRootUrlExposuresCount
        {
            GreaterThan, LessThan
        }
    }
}
