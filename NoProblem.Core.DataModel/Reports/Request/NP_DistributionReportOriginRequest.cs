﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Request
{
    public class NP_DistributionReportOriginRequest : NP_DistributionReportRequest
    {
        public Guid OriginId { get; set; }
    }
}
