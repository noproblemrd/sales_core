﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class PagedList<T>
    {
        [Obsolete("This ctor exists so this class can be serialized, but is not to be used.", true)]
        public PagedList()
        {

        }

        public PagedList(IEnumerable<T> collection, int pageSize, int currentPage)
        {
            List = new List<T>(collection.Skip((currentPage - 1) * pageSize).Take(pageSize));
            NumberOfRecords = collection.Count();
            NumberOfPages = (int)Math.Ceiling(((double)NumberOfRecords) / ((double)pageSize));
            CurrentPage = currentPage;        
        }

        //The properties have public getters and setters becuase they need to be passed in web services.
        public List<T> List { get; set; }
        public int NumberOfPages { get; set; }
        public int CurrentPage { get; set; }
        public int NumberOfRecords { get; set; }
    }
}
