﻿
namespace NoProblem.Core.DataModel.Reports
{
    public class DistributorsReportSummaryRow
    {
        public string OriginName { get; set; }
        public int RequestCount { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal RevenueShare { get; set; }
    }
}
