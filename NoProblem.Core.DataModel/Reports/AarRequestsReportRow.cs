﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class AarRequestsReportRow
    {
        public string HeadingName { get; set; }
        public Guid HeadingId { get; set; }
        public float AvgMinutesForAcPress1 { get; set; }
        public float AvgMinutesForItcPress1 { get; set; }
        public float AvgMinutesForItcLastPress1 { get; set; }
        public float AvgAttemptsForAcPress1 { get; set; }
        public float AvgAttemptsForItcPress1 { get; set; }
        public float AvgAttemptsForItcLastPress1 { get; set; }
        public float AvgMinutesForFullfilment { get; set; }
        public float AvgAttemptsForFullfilment { get; set; }
        public int NumberOfFullfiledCases { get; set; }
        public int NumberOfNotFullfiledCases { get; set; }
        public int NumberOfAttemptsToAcs { get; set; }
        public int NumberOfAttemptsToItc { get; set; }
        public int NumberOfAttemptsToItcLast { get; set; }
    }
}
