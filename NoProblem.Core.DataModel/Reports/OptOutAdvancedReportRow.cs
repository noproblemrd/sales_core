﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class OptOutAdvancedReportRow
    {
        public string Keyword { get; set; }
        public string Flavor { get; set; }
        public int LeadCount { get; set; }
        public int OptOutCount { get; set; }
    }
}
