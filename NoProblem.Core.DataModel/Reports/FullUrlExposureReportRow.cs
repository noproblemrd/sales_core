﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class FullUrlExposureReportRow
    {
        public string Url { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
