﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    
    public class IntervalTimeReport
    {
      //  public eConversionInterval Interval { get; set; }
        public DayOfWeek? day { get; set; }
        public int from { get; set; }
        public int to { get; set; }
        
        public IntervalTimeReport() 
        {
            from = 0;
            to = 24;
        }
        public Dictionary<DateTime, DateTime> GetIntervalForQuery(DateTime _from, DateTime _to)
        {            
            Dictionary<DateTime, DateTime> dic = new Dictionary<DateTime, DateTime>();
            while (_from < _to)
            {
                if (day.HasValue && _from.DayOfWeek != day.Value)
                {
                    _from = _from.AddDays(1);
                    continue;
                }
                DateTime dic_from = _from.AddHours(from);
                DateTime dic_to = _from.AddHours(to);
                dic.Add(dic_from, dic_to);
                _from = _from.AddDays(1);
            }
            return dic;
        }

    }
}
