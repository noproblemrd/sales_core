﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class DepositData
    {
        public DateTime CreatedOn { get; set; }
        public int Amount { get; set; }
        public int Credit { get; set; }
        public int VirtualMoney { get; set; }
        public int Total { get; set; }
    }
}
