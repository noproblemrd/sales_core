﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class KeywordReportRow
    {
        public string Keyword { get; set; }
        public string Heading { get; set; }
        public int Requests { get; set; }
        public int Exposures { get; set; }
        public int Hits { get; set; }
        public int OptOutRest { get; set; }
        public int OptOutForever { get; set; }
        public decimal PotentialRevenue { get; set; }
        public decimal ActualRevenue { get; set; }
        public decimal NetActualRevenue { get; set; }
    }
}
