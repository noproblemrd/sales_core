﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class SalesmanDeposits
    {
        public string SalesmanName { get; set; }
        public List<SalesmanDepositData> Deposits { get; set; }
    }
}
