﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class SalesmanDepositData
    {
        public DateTime DepositCreatedOn { get; set; }
        public int MoneyAmount { get; set; }
        public bool IsFirstDeposit { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public DateTime? AccountApprovedOn { get; set; }
        public string SalesmanName { get; set; }
        public string BizId { get; set; }
        public string RechargeType { get; set; }//max monthly or low balance
        public string PaymentMethod { get; set; }//taken from last payment
        public decimal AmountForCalls { get; set; }//money that goes the the calls balance
        public decimal AmountNotForCalls { get; set; }//money that does not go to calls balance like monthly payment
    }
}
