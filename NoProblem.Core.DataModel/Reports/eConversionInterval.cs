﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public enum eConversionInterval
    {
        HOUR,
        DAY,
        WEEK,
        MONTH,
        DAILY_AVERAGE
    }
}
