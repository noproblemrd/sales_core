﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class RootUrlExposuresReportRow
    {
        public string UrlRoot { get; set; }
        public int Exposures { get; set; }
        public int Requests { get; set; }
        public double CTR { get; set; }
    }
}
