﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class CallDataCountPerDate : CallDataContainers.CallDataBase
    {
        public DateTime Date { get; set; }
        public int Count { get; set; }
    }
}
