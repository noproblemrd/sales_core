﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class CallData : CallDataContainers.CallDataBase
    {
        public DateTime Date { get; set; }
        public string CaseNumber { get; set; }
        public string Expertise { get; set; }
        public string Supplier { get; set; }
        public decimal Price { get; set; }
        public decimal DefaultPrice { get; set; }
        public string Recording { get; set; }
        public bool IsAllowedRecording { get; set; }
        public string SupplierNumber { get; set; }
        public Guid SupplierId { get; set; }
        public int Duration { get; set; }
        public decimal Balance { get; set; }
    }
}
