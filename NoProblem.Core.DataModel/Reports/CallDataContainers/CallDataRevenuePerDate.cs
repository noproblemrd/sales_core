﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class CallDataRevenuePerDate : CallDataContainers.CallDataBase
    {
        public DateTime Date { get; set; }
        public decimal Revenue { get; set; }
    }
}
