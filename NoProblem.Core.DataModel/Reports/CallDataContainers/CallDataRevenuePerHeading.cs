﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class CallDataRevenuePerHeading : CallDataContainers.CallDataBase
    {
        public string Expertise { get; set; }
        public decimal Revenue { get; set; }
    }
}
