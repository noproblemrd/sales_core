﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class AarRequestReportDrillDownRow
    {
        public string CaseNumber { get; set; }
        public bool IsFullfiled { get; set; }
        public float AvgAttemptsAcsPress1 { get; set; }
        public float AvgAttemptsItcsPress1 { get; set; }
        public float AvgAttemptsItcsLastPress1 { get; set; }
        public float AvgMinutesAcsPress1 { get; set; }
        public float AvgMinutesItcsPress1 { get; set; }
        public float AvgMinutesItcsLastPress1 { get; set; }
        public int totalAttemptsAcs { get; set; }
        public int totalAttemptsItcs { get; set; }
        public int totalAttemptsItcsLast { get; set; }
        public float MinutesToFillfilment { get; set; }
        public float CallsToFullfilment { get; set; }
    }
}