﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class IncidentData
    {
        public DateTime CreatedOn { get; set; }
        public Guid CaseId { get; set; }
        public string CaseNumber { get; set; }
        public string ExpertiseName { get; set; }
        public string RegionName { get; set; }
        public string ConsumerName { get; set; }
        public Guid ConsumerId { get; set; }
        public int NumberOfRequestedAdvertisers { get; set; }
        public int NumberOfAdvertisersProvided { get; set; }
        public decimal Revenue { get; set; }
        public Guid OriginId { get; set; }
        public bool IsToPay { get; set; }
        public string PayStatusReason { get; set; }
        public bool ShowAffiliate { get; set; }
        public string RequestStatus { get; set; }
        public int Upsales { get; set; }
        public int MaxCallDuration { get; set; }
    }
}
