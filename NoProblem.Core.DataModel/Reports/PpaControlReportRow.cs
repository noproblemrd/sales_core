﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class PpaControlReportRow
    {
        public Guid SupplierId { get; set; }
        public string BizId { get; set; }
        public string Name { get; set; }
        public bool IsOldFromSaar { get; set; }
        public DataModel.Xrm.account.RechargeTypeCode? RechargeTypeCode { get; set; }
        public decimal RechargeAmount { get; set; }
        public decimal Balance { get; set; }
        public string Heading { get; set; }
        public int Calls { get; set; }
        public int Charged { get; set; }
        public decimal Revenue { get; set; }
        public string AccountType { get; set; }
        public string Status { get; set; }
        public decimal PricePerCall { get; set; }
        public decimal Deposits { get; set; }
        public bool North { get; set; }
        public bool Sharon { get; set; }
        public bool Center { get; set; }
        public bool Jerusalem { get; set; }
        public bool Shfela { get; set; }
        public bool South { get; set; }
        public bool IsTotalRow { get; set; }
    }
}
