﻿using System;

namespace NoProblem.Core.DataModel.Reports
{
    public class DistributorBillingReportIndividualRow
    {
        public DateTime Date { get; set; }
        public string Region { get; set; }
        public string Heading { get; set; }
        public string Consumer { get; set; }
        public bool Payment { get; set; }
        public string PaymentReason { get; set; }
    }
}
