﻿using System;

namespace NoProblem.Core.DataModel.Reports
{
    public class VoucherData
    {
        public DateTime CreatedOn { get; set; }
        public string Number { get; set; }
        public int OriginalBalance { get; set; }
        public int CurrentBalance { get; set; }
        public int AmountOfDeposits { get; set; }
    }
}
