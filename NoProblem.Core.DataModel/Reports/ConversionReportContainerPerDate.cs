﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class ConversionReportContainerPerDate
    {
        public ConversionReportContainerPerDate(DateTime date, int mainCount)
            : this(date, mainCount, 0)
        {

        }

        public ConversionReportContainerPerDate(DateTime date, int mainCount, int secondaryCount)
        {
            this.Date = date;
            this.MainCount = mainCount;
            this.SecondaryCount = secondaryCount;
        }

        public DateTime Date { get; set; }
        public int MainCount { get; set; }
        public int SecondaryCount { get; set; }

        public override bool Equals(object obj)
        {
            ConversionReportContainerPerDate other = (ConversionReportContainerPerDate)obj;
            return this.Date == other.Date;
        }

        public override int GetHashCode()
        {
            return this.Date.GetHashCode();
        }
    }
}
