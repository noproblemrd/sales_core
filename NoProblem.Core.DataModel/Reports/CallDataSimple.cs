﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class CallDataSimple
    {
        public string AdvertiserName { get; set; }
        public Guid AdvertiserId { get; set; }
        public decimal Price { get; set; }
        public decimal PreDefinedPrice { get; set; }
        public string Recording { get; set; }
        public bool IsAllowedRecording { get; set; }
    }
}
