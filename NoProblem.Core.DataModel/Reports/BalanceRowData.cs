﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class BalanceRowData
    {
        public DateTime CreatedOn { get; set; }
        public Xrm.new_balancerow.Action Action { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
    }
}
