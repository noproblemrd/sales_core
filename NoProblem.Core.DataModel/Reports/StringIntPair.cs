﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class StringIntPair
    {
        public string Str { get; set; }
        public int Int { get; set; }
    }
}
