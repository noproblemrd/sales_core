﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class NonPpcCaseData
    {
        public DateTime CreatedOn { get; set; }
        public string ConsumerPhone { get; set; }
        public string SupplierName { get; set; }
        public string SupplierId { get; set; }
        public string SupplierPhone { get; set; }
        public string SupplierPhone2 { get; set; }
        public int NumberOfCalls { get; set; }
    }
}
