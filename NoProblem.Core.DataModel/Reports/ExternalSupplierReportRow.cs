﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class ExternalSupplierReportRow
    {
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public decimal Balance { get; set; }
        public int VoucherDepositSum { get; set; }
        public int VoucherBonusDepositSum { get; set; }
        public int VoucherExtraBonusDepositSum { get; set; }
    }
}
