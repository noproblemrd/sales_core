﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class RefundReportRow
    {
        public DateTime Date { get; set; }
        public int CallsWon { get; set; }
        public int AmountOfRefundRequests { get; set; }
        public double PercentRefundRequestsOfCalls { get; set; }
        public int AmountOfApprovedRequests { get; set; }
        public double PercentApprovedFromCalls { get; set; }
        public double PercentApprovedFromRequests { get; set; }
    }
}
