﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class DescriptionValidationGroupBySessionRow
    {
        public string Session { get; set; }
        public int Count { get; set; }
        public string LastDescription { get; set; }
        public List<DataModel.PublisherPortal.GuidStringPair> Cases { get; set; }
        public string Heading { get; set; }
    }
}
