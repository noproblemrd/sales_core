﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class BrokerSummaryRow
    {
        public bool IsSubAccount { get; set; }
        public Guid ParentAccountId { get; set; }
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public decimal Money { get; set; }
        public decimal BillingRate { get; set; }
        public decimal Rank { get; set; }
        public int RefundCount { get; set; }
        public decimal RefundSum { get; set; }
        public decimal RevenueGross { get; set; }
        public double BillingRatePerDates { get; set; }
    }
}
