﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class DescriptionValidationReportRow
    {
        public string Description { get; set; }
        public string Keyword { get; set; }
        public string Phone { get; set; }
        public string Heading { get; set; }
        public string QualityCheckName { get; set; }
        public string Region { get; set; }
        public string Session { get; set; }
        public string Url { get; set; }
        public string Origin { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Event { get; set; }
        public string ZipCodeCheckName { get; set; }
        public string Flavor { get; set; }
        public string Step { get; set; }
        public string Control { get; set; }
    }
}
