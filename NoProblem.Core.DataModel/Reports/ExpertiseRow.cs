﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class ExpertiseRow
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Inactive { get; set; }
        public int Candidate { get; set; }
        public int Available { get; set; }
        public int NotAvailable { get; set; }
        public int Total { get; set; }
        public bool IsPrimary { get; set; }
    }
}
