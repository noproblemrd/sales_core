﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class ConversionReportRow
    {
        public DateTime Date { get; set; }
        public int ExposureCount { get; set; }
        public int ExposureHits { get; set; }
        public int RequestCount { get; set; }
        public int CallCount { get; set; }
        public int DAUS { get; set; }
        public int LogicHits { get; set; }
        public decimal Revenue { get; set; }
        public decimal Cost { get; set; }
        public int NonUpsalesCount { get; set; }
        public int UpsalesCount { get; set; }
        public int StoppedRequests { get; set; }
        public int UniqueInstalls { get; set; }

        public double TotalRequests_Exposures { get; set; }
        public double CallsPercent { get; set; }
        public double OriginalRequests_Exposures { get; set; }       
        public double SaleRate { get; set; }      
        public double Exposure_KDAUs { get; set; }
        public double Request_KDAUs { get; set; }
        public double Revenue_KDAUs { get; set; }
     //   public double Conversion_KDAUs { get; set; }
        public double ExpHits_KDAUs { get; set; }
        public double ExpHits_Exposures { get; set; }
        public double LogicHits_DAUS { get; set; }
        public double LogicHits_Exposure { get; set; }
        public double CPM { get; set; }
        public double RPM { get; set; }

        public void SetValues()
        {
            this.TotalRequests_Exposures = (ExposureCount == 0) ? -1d : (double)RequestCount / (double)ExposureCount;
            this.CallsPercent = (ExposureCount == 0) ? -1d : ((double)CallCount / (double)ExposureCount) * 100d;
            this.OriginalRequests_Exposures = (ExposureCount == 0) ? -1d :((double)NonUpsalesCount / (double)ExposureCount) * 100d;
            this.SaleRate = (RequestCount - StoppedRequests == 0) ? -1d : ((double)CallCount / (double)(RequestCount - StoppedRequests)) * 100d;
            this.Exposure_KDAUs = (DAUS == 0) ? -1d : (double)ExposureCount / ((double)DAUS / 1000d);
            this.Request_KDAUs = (DAUS == 0) ? -1d : (double)RequestCount / ((double)DAUS / 1000d);
            this.Revenue_KDAUs = (DAUS == 0) ? -1d : (double)Revenue / ((double)DAUS / 1000d);
         //   this.Conversion_KDAUs = (DAUS == 0) ? 0d : Request_KDAUs / ((double)DAUS / 1000d);
            this.ExpHits_KDAUs = (DAUS == 0) ? -1d : (double)ExposureHits / ((double)DAUS / 1000d);
            this.ExpHits_Exposures = (ExposureCount == 0) ? -1d : (double)ExposureHits / (double)ExposureCount;
            this.LogicHits_DAUS = (DAUS == 0) ? -1d : (double)LogicHits / (double)DAUS;
            this.CPM = (ExposureCount == 0) ? -1d :
               (double)Cost / ((double)ExposureCount / 1000d);
            this.RPM = (ExposureCount == 0) ? -1d :
                (double)Revenue / ((double)ExposureCount / 1000d);
            this.LogicHits_Exposure = (ExposureCount == 0) ? -1d :
               (double)LogicHits / (double)ExposureCount;
        }
        
    }
}
