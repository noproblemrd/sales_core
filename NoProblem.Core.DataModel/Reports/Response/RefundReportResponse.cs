﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class RefundReportResponse : PagingResponse
    {
        public List<RefundReportRow> DataList { get; set; }
    }
}
