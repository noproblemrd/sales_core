﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class ExternalSupplierReportResponse : PagingResponse
    {
        public List<ExternalSupplierReportRow> Rows { get; set; }

        public ExternalSupplierReportResponse()
        {
            this.Rows = new List<ExternalSupplierReportRow>();
        }
    }
}
