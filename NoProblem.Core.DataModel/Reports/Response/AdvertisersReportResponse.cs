﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class AdvertisersReportResponse //: PagingResponse
    {
        public List<SupplierData> Suppliers { get; set; }
    }
}
