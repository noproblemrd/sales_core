﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class AdEngineReportResponse
    {
        public DateTime Date { get; set; }
        public int Hits { get; set; }
        public AdEngineReportResponse() { }
    }
}
