﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class NonPpcCallsReportResponse
    {
        public List<DataModel.Reports.NonPpcCaseData> Cases { get; set; }
    }
}
