﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response.Calls
{
    public class CallsReportCountPerDateResponse : CallsReportResponseBase
    {
        public List<CallDataCountPerDate> Calls { get; set; }
    }
}
