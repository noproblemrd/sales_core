﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response.Calls
{
    public class CallsReportResponse : PagingResponse
    {
        public List<Reports.CallData> Calls { get; set; }
    }
}
