﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response.Calls
{
    public class CallsReportRevenuePerDateResponse : CallsReportResponseBase
    {
        public List<CallDataRevenuePerDate> Calls { get; set; }
    }
}
