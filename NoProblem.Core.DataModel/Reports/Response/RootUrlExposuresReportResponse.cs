﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class RootUrlExposuresReportResponse : PagingResponse
    {
        public List<DataModel.Reports.RootUrlExposuresReportRow> Rows { get; set; }

        public RootUrlExposuresReportResponse()
        {
            this.Rows = new List<RootUrlExposuresReportRow>();      
        }
    }
}
