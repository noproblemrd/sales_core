﻿using System;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class C2CData
    {
        public Guid RequestId { get; set; }
        public DateTime? ConsumerCallStart { get; set; }
        public DateTime? AdvertiserCallStart { get; set; }
        public DateTime? AdvertiserCallEnd { get; set; }
        public DateTime? ConsumerCallEnd { get; set; }
        public string CustomerPhone { get; set; }
        public string AdvertiserPhone { get; set; }
        public int EndReasonAdvertiser { get; set; }
        public int EndReasonConsumer { get; set; }
        public DateTime EntryCreatedOn { get; set; }
        public C2CData()
        {
        }

    }
}
