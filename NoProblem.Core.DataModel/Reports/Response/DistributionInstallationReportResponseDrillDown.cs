﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class DistributionInstallationReportResponseDrillDown
    {
        public List<DistributionInstallationReportRowDrillDown> Data { get; set; }
        public void SetValues(List<InstallationReportInjectDetails> Installation_data, List<DataModel.Reports.Response.InstallationReportTotalDetailsDrillDown> list_total)
        {
            if (Data == null || Installation_data == null)
                return;
            foreach (DistributionInstallationReportRowDrillDown _row in Data)
            {
                var query_total = from x in list_total
                                  where x.OriginId == _row.OriginId
                                  select x;
                if (query_total.Count() > 0)
                {
                    InstallationReportTotalDetailsDrillDown irddd = query_total.FirstOrDefault();
                    _row.TotalCost = irddd.TotalCost;
                    _row.TotalRevenue = irddd.TotalRevenue;
                    _row.TotalUniqueInstallation = irddd.TotalUniqueInstalls;
                    _row.Total_DAUS = irddd.TotalDAUs;
                }
                _row.SetValues(Installation_data);

            }
        }
    }
    public class DistributionInstallationReportRowDrillDown : DistributionInstallationReportRow
    {
        public string OriginName { get; set; }
        public Guid OriginId { get; set; }

        public override void SetValues(List<InstallationReportInjectDetails> Installation_data)
        {

            var query = from x in Installation_data
                        where x.OriginId == this.OriginId
                        select x;
            foreach (InstallationReportInjectDetails irid in query)
            {
                switch (irid.Name.ToLower())
                {
                    case (Constants.InjectionNames.SUPERFISH):
                        this.SuperfishRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.DEALPLY):
                        this.DealplyRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.JOLLYWALLET):
                        this.JollywalletRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.INTER_YIELD):
                        this.InterYieldRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.REVIZER):
                        this.RevizerPopupRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.INTEXT):
                        this.IntextRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.FIRST_OFFERZ):
                        this.FirstOfferzRevenue = irid.Revenue;
                        break;
                }
            }
            SetValues();
        }
    }
    public class InstallationReportTotalDetailsDrillDown : InstallationReportTotalDetails
    {
        public Guid OriginId { get; set; }
    }
}
