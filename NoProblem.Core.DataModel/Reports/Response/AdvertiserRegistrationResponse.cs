﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class AdvertiserRegistrationResponse
    {
        public AdvertiserRegistrationResponseCollection CurrentData { get; set; }
        public AdvertiserRegistrationResponseCollection CompareData { get; set; }
        public List<RegisterDoublePair> GAP { get; set; }
        public List<DatetimeIntPair> RegiterUsers { get; set; }
        public void SetValues(bool IncludeStep0Current, bool IncludeStep0Compare, DateTime FromDate, DateTime ToDate)
        {
            CurrentData.SetValues(IncludeStep0Current);
            if (CompareData != null)
            {
                CompareData.SetValues(IncludeStep0Compare);
                SetGap();                
            }
            SetRegisterUsers(FromDate, ToDate);
        }

        private void SetRegisterUsers(DateTime FromDate, DateTime ToDate)
        {
            if(RegiterUsers == null)
                return;
            List<DatetimeIntPair> new_list = new List<DatetimeIntPair>();
            while (FromDate <= ToDate)
            {
                IEnumerable<DatetimeIntPair> query = from x in RegiterUsers
                                                     where x.key == FromDate
                                                     select x;
                if (query.Count() == 0)
                    new_list.Add(new DatetimeIntPair(FromDate, 0));
                else
                    new_list.Add(query.FirstOrDefault());
                FromDate = FromDate.AddDays(1);
            }
            RegiterUsers = new_list;
        }
        void SetGap()
        {
            GAP = new List<RegisterDoublePair>();
            int _min = GetMinStep();
            int _max = GetMaxStep();
            for (int i = _min; i <= _max; i++)
            {
                GAP.Add(new RegisterDoublePair((eCompletedRegistrationStep)i, PercentGap(i)));
            }
        }
        double? PercentGap(int _step)
        {
            IEnumerable<AdvertiserRegistrationResponseData> Curr_data = from x in CurrentData
                                                                        where x.Step == _step
                                                                        select x;
            if (Curr_data.Count() == 0)
                return null;
            IEnumerable<AdvertiserRegistrationResponseData> Comp_data = from x in CompareData
                                                                        where x.Step == _step
                                                                        select x;
            if (Comp_data.Count() == 0)
                return null;
            int Current_users = Curr_data.FirstOrDefault().Users;
            if (Current_users == 0)
            {
                if (Comp_data.FirstOrDefault().Users == 0)
                    return 0;
                return null;
            }
            else
                return ((double)(100 * Comp_data.FirstOrDefault().Users) / (double)Current_users) - 100d;
        }
        int GetMinStep()
        {
            int _min = MinStep(CurrentData);
            int _min2 = MinStep(CompareData);
            return _min > _min2 ? _min2 : _min;
        }
        int GetMaxStep()
        {
            int _max = MaxStep(CurrentData);
            int _max2 = MaxStep(CompareData);
            return _max > _max2 ? _max2 : _max;
        }
        int MinStep(AdvertiserRegistrationResponseCollection coll)
        {
            int _min = coll.Min(x => x.Step);
            return _min;
                       
        }
        int MaxStep(AdvertiserRegistrationResponseCollection coll)
        {
            int _max = coll.Max(x => x.Step);
            return _max;
        }
    }
    public class AdvertiserRegistrationResponseCollection : List<AdvertiserRegistrationResponseData>
    {
        public void SetValues(bool IncludeStep0)
        {
           /*
            int _sumRegister = (from x in this
                        where x.Step > 0
                        select x.Users).Sum();
            var query_sumLandingPage = (from x in this
                                   where x.Step == 0
                                   select x);
            int _sumLandingPage = 0;
            if (query_sumLandingPage.Count() > 0)
                _sumLandingPage = query_sumLandingPage.Sum(x => x.Users);

            int _total = _sumRegister + _sumLandingPage;

            int ReduceSum = _total;//_sum;
            
            * */
            int Last = -1;
            int MinStep = IncludeStep0 ? 0 : 1;
            
            IEnumerable<AdvertiserRegistrationResponseData> _query = from x in this
                                                                    where x.Step == MinStep
                                                                    select x;
            int _total = _query.Count() == 0 ? 0 : _query.Sum(x => x.Users);
            foreach(eCompletedRegistrationStep Registration_Step in Enum.GetValues(typeof(eCompletedRegistrationStep)))
            {
                if (Registration_Step == eCompletedRegistrationStep.All)
                    continue;
               
                int _step = (int)Registration_Step;
                if (_step == 0 && !IncludeStep0)
                    continue;

                
                IEnumerable<AdvertiserRegistrationResponseData> queryExsist = from x in this
                                                                        where x.Step == _step
                                                                        select x;
                AdvertiserRegistrationResponseData data; 
          //          this.a

            //    AdvertiserRegistrationResponseData data = new AdvertiserRegistrationResponseData();
                
                if (queryExsist.Count() == 0)
                {
                    data = new AdvertiserRegistrationResponseData();
                    data.Step = _step;                    
                    data.Users = 0;
                    

                }
                else
                    data = queryExsist.FirstOrDefault();
                if (Last == -1)
                {
                    data.Passed = 100;
                    data.DropOff = -1;
                    data.DropOffFromStep = -1;
                    data.PassedFromStep = -1;
                    /*
                    if (data.Step > 0)
                    {
                        data.UsersShow = ReduceSum;

                    }
                    else
                    {
                        data.UsersShow = _total;//data.Users;


                    }
                     * */
                }
                else
                {
              //      data.UsersShow = ReduceSum;
                    double _passed = (_total == 0) ? -1 : ((double)(data.Users * 100)) / (double)_total;
                    double _drop = (_total == 0) ? -1 : ((double)((_total - data.Users) * 100)) / (double)_total;
                    double _PassedOffFromStep = (Last == 0) ? -1 : ((double)(data.Users * 100)) / (double)Last;
                    double _DropOffFromStep = (Last == 0) ? -1 : ((double)((Last - data.Users) * 100)) / (double)Last;
                    data.Passed = _passed;
                    data.DropOff = _drop;
                    data.DropOffFromStep = _DropOffFromStep;
                    data.PassedFromStep = _PassedOffFromStep;
                }
          //       ReduceSum = ReduceSum - data.Users;


                 Last = data.Users;
                this.Add(data);
            }
            this.Sort(delegate(AdvertiserRegistrationResponseData x, AdvertiserRegistrationResponseData y)
            {
                return x.Step - y.Step;
            });
        }
    }
    public class AdvertiserRegistrationResponseData
    {
        public int Step { get; set; }
        public int Users { get; set; }
       // public int UsersShow { get; set; }
        public double DropOff { get; set; }
        public double Passed { get; set; }
        public double DropOffFromStep { get; set; }
        public double PassedFromStep { get; set; }
        public AdvertiserRegistrationResponseData()
        {
        }
        
    }
    public class RegisterDoublePair
    {
        public eCompletedRegistrationStep key { get; set; }
        public double? value { get; set; }
        public RegisterDoublePair() { }
        public RegisterDoublePair(eCompletedRegistrationStep _key, double? _value)
        {
            this.key = _key;
            this.value = _value;
        }
    }
    public class DatetimeIntPair
    {
        public DateTime key { get; set; }
        public int value { get; set; }
        public DatetimeIntPair() { }
        public DatetimeIntPair(DateTime _key, int _value)
        {
            this.key = _key;
            this.value = _value;
        }
    }
    /*
    public class IntAdvertiserRegistrationResponseData
    {
        public int key { get; set; }
        public AdvertiserRegistrationResponseData CurrentValue { get; set; }
        public AdvertiserRegistrationResponseData CompareValue { get; set; }
        public double GAP { get; set; }
    }
     * */
}
