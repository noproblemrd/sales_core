﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class DescriptionValidationGroupBySessionResponse : PagingResponse
    {
        public List<DescriptionValidationGroupBySessionRow> Rows { get; set; }

        public DescriptionValidationGroupBySessionResponse()
        {
            Rows = new List<DescriptionValidationGroupBySessionRow>();
        }
    }
}
