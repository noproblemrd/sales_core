﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class InjectionReportResponseRow
    {
        public DateTime Date { get; set; }
        public int DAUS { get; set; }
        public decimal Revenue { get; set; }
        public int PPI_DAU { get; set; }
        public double InjectionPercent { get; set; }
        public decimal DUV { get; set; }
        public void SetValues()
        {
         //   i.	DUV- Revenue*100/DAUs for all rows
            this.DUV = (DAUS == 0) ? -1m :
                (this.Revenue * 100m) / (decimal)this.DAUS;

            //ii.	Injection percent: DAUs*100/PPI DAUs in percent.
            this.InjectionPercent = (DAUS == 0) ? -1d :
                ((double)DAUS * 100d) / (double)PPI_DAU;
       
        }
    }
    public class InjectionReportResponse : List<InjectionReportResponseRow>
    {
        public void SetValues()
        {
            InjectionReportResponseRow TotalRow = new InjectionReportResponseRow();
            TotalRow.Date = DateTime.MaxValue;
            TotalRow.DAUS = this.Sum(x => x.DAUS);
            TotalRow.Revenue = this.Sum(x => x.Revenue);
            TotalRow.PPI_DAU = this.Sum(x => x.PPI_DAU);
            this.Insert(0, TotalRow);
            foreach (InjectionReportResponseRow row in this)
                row.SetValues();
        }
    }
}
