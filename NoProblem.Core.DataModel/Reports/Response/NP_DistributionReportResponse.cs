﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class NP_DistributionReportResponse
    {
        public int Installs { get; set; }
        public int UniqueInstalls { get; set; }
        public int EffectiveInstalls { get; set; }
        public Guid OriginId { get; set; }
        public string OriginName { get; set; }
        public decimal Cost { get; set; }

        public NP_DistributionReportResponse() { }
    }
}
