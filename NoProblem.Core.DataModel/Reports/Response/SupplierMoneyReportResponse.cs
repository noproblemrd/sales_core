﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class SupplierMoneyReportResponse
    {
        public int DepositReal { get; set; }
        public int DepositBonus { get; set; }
        public int UsedReal { get; set; }
        public int UsedBonus { get; set; }
        public int LeftReal { get; set; }
        public int LeftBonus { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string SupplierNumber { get; set; }
    }
}
