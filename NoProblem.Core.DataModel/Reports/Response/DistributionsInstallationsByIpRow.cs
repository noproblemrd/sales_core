﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class DistributionsInstallationsByIpRow
    {
        public int Count { get; set; }
        public string Ip { get; set; }
        public DateTime UtcTime { get; set; }
        public DateTime EasternStandardTime { get; set; }
    }
}
