﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class ProfitableReportResponse
    {
        public List<ProfitableReportRow> MainData { get; set; }
        public decimal MainProfitTotal { get; set; }
        public decimal MainProfitPercentageTotal { get; set; }

        public List<ProfitableReportRow> CompareData { get; set; }
        public decimal CompareProfitTotal { get; set; }
        public decimal CompareProfitPercentageTotal { get; set; }
    }

    public class ProfitableReportRow
    {
        public Guid HeadingId { get; set; }
        public int Requests { get; set; }
        public int TotalRequest { get; set; }
        public int Exposures { get; set; }
        public decimal Cost { get; set; }
        public decimal Revenue { get; set; }
        public decimal Profit { get; set; }
        public decimal ProfitPercentage { get; set; }
        public int Users { get; set; }
        /** Calculate properties*/
        public double Exposure_KDAUs { get; set; }
        public double Request_KDAUs { get; set; }
       // public double Exposure_Request { get; set; }
        public double Request_Exposures { get; set; }
        public double Revenue_TotalRequest { get; set; }
        public double EPPL { get; set; }
        public double CPM { get; set; }
        public double RPM { get; set; }

        public void SetValues()
        {
            Exposure_KDAUs = (Users == 0) ? -1d :
                (double)Exposures / ((double)Users / 1000d);
            Request_KDAUs = (Users == 0) ? -1d :
                (double)TotalRequest / ((double)Users / 1000d);
            EPPL = (Requests == 0) ? -1d :
                (double)Revenue / (double)Requests;
            /*
            Exposure_Request = (TotalRequest == 0) ? -1d :
                    (double)Exposures / (double)TotalRequest;
             * */
            Request_Exposures = (Exposures == 0) ? -1d :
                (double)TotalRequest / (double)Exposures;
            Revenue_TotalRequest = (TotalRequest == 0) ? -1d : 
                (double)Revenue / (double)TotalRequest;
            CPM = (Exposures == 0) ? -1d :
                (double)Cost / ((double)Exposures / 1000d);
            RPM = (Exposures == 0) ? -1d :
                (double)Revenue / ((double)Exposures / 1000d);
        }
    }
    /*
     * ,(CASE 
							WHEN (ISNULL(@users, 0)) = 0 THEN 0.0
							ELSE CAST(ISNULL(Exposure.Exposures	, 0) as real) / (CAST(@users as real) / CAST(1000 as real))
							END)
							Exposure_KDAUs
						,(CASE 
							WHEN (ISNULL(@users, 0)) = 0 THEN 0.0
							ELSE CAST(ISNULL(Request.TotalRequest	, 0) as real) / (CAST(@users as real) / CAST(1000 as real))
							END)
							Request_KDAUs
                         ,(CASE 
							WHEN (isnull(Request.requests, 0)) = 0 THEN 0.0
							ELSE CAST(isnull(Request.revenue, 0) as real) / CAST(Request.requests as real)
							END)
							EPPL
                        ,@users users
     * */
}
