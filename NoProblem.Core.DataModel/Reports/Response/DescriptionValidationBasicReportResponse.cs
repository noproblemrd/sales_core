﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class DescriptionValidationBasicReportResponse : PagingResponse
    {
        public List<DescriptionValidationReportRow> Rows { get; set; }

        public DescriptionValidationBasicReportResponse()
        {
            Rows = new List<DescriptionValidationReportRow>();
        }
    }
}
