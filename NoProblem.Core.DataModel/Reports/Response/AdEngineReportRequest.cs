﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class AdEngineReportRequest
    {
        public DateTime _from { get; set; }
        public DateTime _to { get; set; }
        public Guid OriginId { get; set; }
        public Guid AdEngineCampaign { get; set; }
        public string Country { get; set; }
        public AdEngineReportRequest() { }
        public DateTime FromDateQuery
        {
            get { return _from; }
        }
        public DateTime ToDateQuery
        {
            get { return _to.AddDays(1); }
        }
    }
}
