﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class FullUrlExposureReportResponse : PagingResponse
    {
        public List<FullUrlExposureReportRow> Rows { get; set; }

        public FullUrlExposureReportResponse()
        {
            this.Rows = new List<FullUrlExposureReportRow>();
        }
    }
}
