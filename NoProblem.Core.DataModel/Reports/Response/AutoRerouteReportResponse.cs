﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class AutoRerouteReportResponse
    {
        public int NumberOfSuppliersInThisService { get; set; }
        public int NumberOfReroutes { get; set; }
        public int NumberOfReroutedCallsThatWereAnswered { get; set; }
        public int NumberOfReroutedCallsThatWereNotAnswered { get; set; }
        public double PercentageOfAnsweredCallsFromReroutedCalls { get; set; }
    }
}
