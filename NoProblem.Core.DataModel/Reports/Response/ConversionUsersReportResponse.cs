﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class ConversionUsersReportResponse
    {
        public DateTime date { get; set; }
        public string OriginName { get; set; }
        public Guid OriginId { get; set; }
        public int DAUS { get; set; }
        public decimal Revenue { get; set; }
    }
   
}
