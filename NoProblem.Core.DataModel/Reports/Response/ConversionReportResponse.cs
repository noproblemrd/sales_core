﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class ConversionReportResponse
    {
        public ConversionReportResponse()
        {
            DataList = new List<ConversionReportRow>();
        }

        public List<ConversionReportRow> DataList { get; set; }
        public DateTime ExposuresLastUpdate { get; set; }
        public DateTime RequestsLastUpdate { get; set; }
    }
}
