﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class PplReportData
    {
        /*
        public Guid ExpertiseId { get; set; }
        public string ExpertiseName { get; set; }
        
        public decimal Max { get; set; }
        public decimal Min { get; set; }
        public decimal Avg { get; set; }
         */
        public Guid AccountId { get; set; }
        public string AccountName { get; set; }
        public decimal Price { get; set; }
        public string LeadBuyerInterface { get; set; }
        
    }
}
