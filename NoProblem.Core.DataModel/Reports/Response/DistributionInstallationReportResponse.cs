﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class DistributionInstallationReportResponse
    {
        public List<DistributionInstallationReportRow> Data { get; set; }
        public DistributionInstallationReportRow DataTotal { get; set; }
        public InstallationReportTotalDetails TotalDetails { get; set; }

        public void SetValues(List<InstallationReportInjectDetails> Installation_data, DateTime _from, DateTime _to)
        {
            if (Data == null)
                return;
            if (TotalDetails == null)
            {
                TotalDetails = new InstallationReportTotalDetails();
                TotalDetails.TotalCost = 0;
                TotalDetails.TotalDAUs = 0;
                TotalDetails.TotalUniqueInstalls = 0;
                TotalDetails.TotalRevenue = 0;
                TotalDetails.TotalEffectiveUniqueInstalls = 0;
            }

            while (_from <= _to)
            {
                var query = from x in Data
                            where x.Date == _from
                            select x;
                DistributionInstallationReportRow row;
                if (query.Count() == 0)
                {
                    row = new DistributionInstallationReportRow();
                    row.Date = _from;
                    Data.Add(row);
                }
                else
                    row = query.FirstOrDefault();
                var queryTotal = from x in Data
                                 where x.Date <= _from
                                 select new
                                 {
                                     TotalDAUS = x.DAUS,
                                     TotalRevenue = x.Revenue,
                                     TotalUniqueInstalles = x.UniqeInstallations,
                                     TotalCost = x.Cost,
                                     TotalEffectiveUniqueInstalls = x.EffectiveUniqueInstalls
                                 };
                if (queryTotal.Count() > 0)
                {
                    row.Total_DAUS = queryTotal.Sum(x => x.TotalDAUS) + TotalDetails.TotalDAUs;
                    row.TotalUniqueInstallation = queryTotal.Sum(x => x.TotalUniqueInstalles) + TotalDetails.TotalUniqueInstalls;
                    row.TotalRevenue = queryTotal.Sum(x => x.TotalRevenue) + TotalDetails.TotalRevenue;
                    row.TotalCost = queryTotal.Sum(x => x.TotalCost) + TotalDetails.TotalCost;
                    row.TotalEffectiveUniqueInstalls = queryTotal.Sum(x => x.TotalEffectiveUniqueInstalls) + TotalDetails.TotalEffectiveUniqueInstalls;
                }
                else
                {
                    row.Total_DAUS = TotalDetails.TotalDAUs;
                    row.TotalUniqueInstallation = TotalDetails.TotalUniqueInstalls;
                    row.TotalRevenue = TotalDetails.TotalRevenue;
                    row.TotalCost = TotalDetails.TotalCost;
                    row.TotalEffectiveUniqueInstalls = TotalDetails.TotalEffectiveUniqueInstalls;
                }
                row.SetValues(Installation_data);

                _from = _from.AddDays(1);
            }
            var queryDataTotal = from x in Data
                                 select new
                                 {
                                     TotalDAUS = x.DAUS,
                                     TotalRevenue = x.Revenue,
                                     TotalInstalles = x.UniqeInstallations,
                                     TotalEffectiveUniqueInstalls = x.EffectiveUniqueInstalls,
                                     TotalCost = x.Cost
                                 };

            DataTotal = new DistributionInstallationReportRow();
            DataTotal.DAUS = Data.Sum(x => x.DAUS);
            DataTotal.DealplyRevenue = Data.Sum(x => x.DealplyRevenue);
            DataTotal.Installations = Data.Sum(x => x.Installations);
            DataTotal.UniqeInstallations = Data.Sum(x => x.UniqeInstallations);
            DataTotal.EffectiveUniqueInstalls = Data.Sum(x => x.EffectiveUniqueInstalls);
            DataTotal.InterYieldRevenue = Data.Sum(x => x.InterYieldRevenue);
            DataTotal.JollywalletRevenue = Data.Sum(x => x.JollywalletRevenue);
            DataTotal.NoProblemRevenue = Data.Sum(x => x.NoProblemRevenue);
            DataTotal.SuperfishRevenue = Data.Sum(x => x.SuperfishRevenue);
            DataTotal.RevizerPopupRevenue = Data.Sum(x => x.RevizerPopupRevenue);
            DataTotal.IntextRevenue = Data.Sum(x => x.IntextRevenue);
            DataTotal.FirstOfferzRevenue = Data.Sum(x => x.FirstOfferzRevenue);
            DataTotal.AdmediaIntext = Data.Sum(x => x.AdmediaIntext);
            DataTotal.AdcashRevenue = Data.Sum(x => x.AdcashRevenue);
            DataTotal.EngageyaRevenue = Data.Sum(x => x.EngageyaRevenue);

            DataTotal.Revenue = Data.Sum(x => x.Revenue);
            DataTotal.Uninstallation = Data.Sum(x => x.Uninstallation);
            DataTotal.Cost = Data.Sum(x => x.Cost);

            DataTotal.Total_DAUS = queryDataTotal.Sum(x => x.TotalDAUS) + TotalDetails.TotalDAUs;
            DataTotal.TotalUniqueInstallation = queryDataTotal.Sum(x => x.TotalInstalles) + TotalDetails.TotalUniqueInstalls;
            DataTotal.TotalEffectiveUniqueInstalls = queryDataTotal.Sum(x => x.TotalEffectiveUniqueInstalls) + TotalDetails.TotalEffectiveUniqueInstalls;
            DataTotal.TotalRevenue = queryDataTotal.Sum(x => x.TotalRevenue) + TotalDetails.TotalRevenue;
            DataTotal.TotalCost = queryDataTotal.Sum(x => x.TotalCost) + TotalDetails.TotalCost;

            DataTotal.SetValues();
            Data.Sort();
        }
    }

    public class DistributionInstallationReportRow : IEquatable<DistributionInstallationReportRow>, IComparable<DistributionInstallationReportRow>
    {
        public DateTime Date { get; set; }
        public int Installations { get; set; }
        public int UniqeInstallations { get; set; }
        public int Uninstallation { get; set; }
        public int EffectiveUniqueInstalls { get; set; }

        public long DAUS { get; set; }
        public decimal DAUS_TotalInstallation_percent { get; set; }

        public decimal NoProblemRevenue { get; set; }
        public decimal NonNoProblemRevenue { get; set; }
        public decimal DealplyRevenue { get; set; }
        public decimal JollywalletRevenue { get; set; }
        public decimal InterYieldRevenue { get; set; }
        public decimal SuperfishRevenue { get; set; }
        public decimal RevizerPopupRevenue { get; set; }
        public decimal IntextRevenue { get; set; }
        public decimal FirstOfferzRevenue { get; set; }
        public decimal AdmediaIntext { get; set; }
        public decimal AdcashRevenue { get; set; }
        public decimal EngageyaRevenue { get; set; }

        public decimal Revenue { get; set; }
        public decimal Cost { get; set; }


        public decimal AULT { get; set; }
        public decimal AMUV_NoProblem { get; set; }
        public decimal DUV_NoProblem { get; set; }
        public decimal AMUV { get; set; }
        public decimal DUV { get; set; }
        public decimal ROI { get; set; }

        public int TotalUniqueInstallation { get; set; }
        public int TotalEffectiveUniqueInstalls { get; set; }
        public long Total_DAUS { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalRevenue { get; set; }


        public virtual void SetValues(List<InstallationReportInjectDetails> Installation_data)
        {

            var query = from x in Installation_data
                        where x.Date == this.Date
                        select x;
            foreach (InstallationReportInjectDetails irid in query)
            {//admedia_intext
                switch (irid.Name.ToLower())
                {
                    case (Constants.InjectionNames.SUPERFISH):
                        this.SuperfishRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.DEALPLY):
                        this.DealplyRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.JOLLYWALLET):
                        this.JollywalletRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.INTER_YIELD):
                        this.InterYieldRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.REVIZER):
                        this.RevizerPopupRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.INTEXT):
                        this.IntextRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.FIRST_OFFERZ):
                        this.FirstOfferzRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.ADMEDIA_INTEXT):
                        this.AdmediaIntext = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.ADCASH):
                        this.AdcashRevenue = irid.Revenue;
                        break;
                    case (Constants.InjectionNames.ENGAGEYA):
                        this.EngageyaRevenue = irid.Revenue;
                        break;
                }
            }
            SetValues();
        }

        public void SetValues()//(decimal AvgInstallCost)
        {

            //       this.TotalCost = (decimal)TotalInstallation * AvgInstallCost;
            //      this.Cost = (decimal)this.Installations * AvgInstallCost;
            this.NonNoProblemRevenue = DealplyRevenue + JollywalletRevenue + InterYieldRevenue + SuperfishRevenue + RevizerPopupRevenue + IntextRevenue + FirstOfferzRevenue;
            this.NoProblemRevenue = Revenue - this.NonNoProblemRevenue;
            this.AULT = (TotalUniqueInstallation == 0) ? 0m : (decimal)Total_DAUS / (decimal)TotalUniqueInstallation;
            this.DUV_NoProblem = (DAUS == 0) ? 0m : (NoProblemRevenue / (decimal)DAUS) * 100m;
            this.AMUV_NoProblem = (DAUS == 0) ? 0m : this.DUV_NoProblem * 30m;
            this.DUV = (DAUS == 0) ? 0m : (Revenue / (decimal)DAUS) * 100m;
            this.AMUV = (DAUS == 0) ? 0m : this.DUV * 30m;
            this.ROI = (TotalCost == 0m) ? 0m : ((TotalRevenue / TotalCost) - 1m) * 100m;
            this.DAUS_TotalInstallation_percent = (TotalUniqueInstallation == 0) ? 0m : ((decimal)(DAUS * 100)) / (decimal)TotalUniqueInstallation;
        }


        public bool Equals(DistributionInstallationReportRow other)
        {
            return this.Date == other.Date;
        }

        public int CompareTo(DistributionInstallationReportRow other)
        {
            if (this.Date == other.Date)
                return 0;
            if (this.Date > other.Date)
                return -1;
            return 1;
        }
    }

    public class InstallationReportTotalDetails
    {
        public long TotalDAUs { get; set; }
        public decimal TotalRevenue { get; set; }
        public int TotalUniqueInstalls { get; set; }
        //   public decimal AvgInstallCost { get; set; }
        public decimal TotalCost { get; set; }
        public int TotalEffectiveUniqueInstalls { get; set; }
    }
    public class InstallationReportInjectDetails
    {
        public decimal Revenue { get; set; }
        public Guid? InjectionId { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public Guid OriginId { get; set; }
    }
}
