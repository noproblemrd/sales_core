﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class ConversionUsersReportExposureResponse
    {
        public DateTime date { get; set; }
        public string OriginName { get; set; }
        public Guid OriginId { get; set; }
        public int DAUS { get; set; }
        public int Exposure { get; set; }
        public int Requests { get; set; }
        public Double EXP_KDAUS { get; set; }
        public Double Request_KDAUS { get; set; }
        public void Set_EXP_KDAUS()
        {
            if (DAUS == 0)
                this.EXP_KDAUS = 0;
            else
                this.EXP_KDAUS = (double)Exposure / ((double)DAUS / 1000d);
        }
        public void Set_Request_KDAUS()
        {
            if (DAUS == 0)
                this.Request_KDAUS = 0;
            else
                this.Request_KDAUS = (double)Requests / ((double)DAUS / 1000d);
        }
             
    }
}
