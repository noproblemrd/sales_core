﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class HeartBeatInjectionResponse
    {
        public DateTime date { get; set; }
        public int Hits { get; set; }
        public Guid InjectionId { get; set; }
        public double Hits_KDaus { get; set; }
        public HeartBeatInjectionResponse() { }

        public bool SetHitsDaus(Dictionary<Guid, int> DAUs)
        {
            if (!DAUs.ContainsKey(InjectionId))
                return false;
            if (DAUs[InjectionId] == 0)
                Hits_KDaus = 0;
            else
                Hits_KDaus = (double)Hits / ((double)(DAUs[InjectionId]) / 1000.0);
            return true;
        }

        internal DateTime FloorDatetimeByDay
        {
            get
            {
                TimeSpan span = new TimeSpan(1, 0, 0, 0);
                long ticks = date.Ticks / span.Ticks;
                return new DateTime(ticks * span.Ticks);
            }
        }
       
    }
    public class HeartBeatInjectionResponseCollection : List<HeartBeatInjectionResponse>
    {
        public Dictionary<DateTime, Dictionary<Guid, int>> DicDAUs { get; set; }
        public HeartBeatInjectionResponseCollection() { }
        public HeartBeatInjectionResponseCollection(Dictionary<DateTime, Dictionary<Guid, int>> DAUs)
        {
            DicDAUs = DAUs;
        }

        public void SetHitsDaus()
        {
            for(int i = 0; i < this.Count; i++)
            {
                HeartBeatInjectionResponse data = this[i];
                IEnumerable<Dictionary<Guid, int>> query = from x in DicDAUs
                                                           where x.Key == data.FloorDatetimeByDay
                                                           select x.Value;
                Dictionary<Guid, int> dic;
                if (query.Count() == 0)
                {
                    dic = new Dictionary<Guid, int>();
                    dic.Add(data.InjectionId, 0);
                    DicDAUs.Add(data.FloorDatetimeByDay, dic);
                }
                else
                    dic = query.FirstOrDefault();
                bool IsSetValue = data.SetHitsDaus(dic); 
                if (!IsSetValue)
                {
                    dic.Add(data.InjectionId, 0);
                    IsSetValue = data.SetHitsDaus(dic);
                }
            }
        }
       
         


    }
}
