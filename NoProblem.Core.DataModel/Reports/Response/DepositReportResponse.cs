﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class DepositReportResponse : PagingResponse
    {
        public List<DepositData> DataList { get; set; }
        public DepositData TotalRow { get; set; }
    }
}
