﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class KeywordReportResponse
    {
        public List<KeywordReportRow> DataList { get; set; }
        public DateTime Adjusted { get; set; }

        public KeywordReportResponse()
        {
            DataList = new List<KeywordReportRow>();
        }
    }
}
