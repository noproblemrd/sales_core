﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class MyCallsReportResponse
    {
        public Guid CallId { get; set; }
        public string TicketNumber { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Heading { get; set; }
        public string Title { get; set; }
        public bool RefundStatus { get; set; }
        public bool Win { get; set; }
        public string RecordLocation { get; set; }
    }
}
