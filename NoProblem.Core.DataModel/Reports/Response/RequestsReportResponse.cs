﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class RequestsReportResponse : PagingResponse
    {
        public List<IncidentData> Incidents { get; set; }
        public int ToPay { get; set; }
        public int NotToPay { get; set; }
    }
}
