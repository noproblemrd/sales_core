﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class BrokersLeadsDataForReportResponse
    {
        public DateTime CreatedOn { get; set; }
        public Guid IncidentId { get; set; }
        public string TicketNumber { get; set; }
        public string HeadingName { get; set; }
        public string Region { get; set; }
        public decimal Price { get; set; }
        public bool IsMarkedAsBadManually { get; set; }
        public string BrokersId { get; set; }
        public string RecordingLocation { get; set; }
        public string Notes { get; set; }
        public int? Duration { get; set; }
        public string Phone { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
