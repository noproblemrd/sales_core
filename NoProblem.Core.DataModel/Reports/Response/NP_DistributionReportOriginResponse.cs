﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports.Response
{
    public class NP_DistributionReportOriginResponse
    {
        public DateTime Date { get; set; }
        public int Installs { get; set; }
        public int UniqueInstalls { get; set; }
        public int EffectiveInstalls { get; set; }        
        public decimal CalcCost { get; set; }
        public decimal FixCost { get; set; }

        public NP_DistributionReportOriginResponse() { }
    }
}
