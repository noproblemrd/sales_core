﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.Reports.GetExpertiseReportResponse
//  File: GetExpertiseReportResponse.cs
//  Description: Container to use as response for the GetExpertiseReport service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
namespace NoProblem.Core.DataModel.Reports
{
    public class GetExpertiseReportResponse
    {
        public List<DataModel.Reports.ExpertiseRow> Report { get; set; }
    }
}
