﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class GetValidationReportDropDownListsResponse
    {
        public List<string> Flavors { get; set; }
        public List<string> Events { get; set; }
        public List<string> ZipCodeCheckNames { get; set; }
        public List<string> Steps { get; set; }
        public List<string> Controls { get; set; }
    }
}
