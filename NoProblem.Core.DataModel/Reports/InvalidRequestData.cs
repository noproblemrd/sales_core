﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Reports
{
    public class InvalidRequestData
    {
        public string CaseNumber { get; set; }
        public Guid CaseId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ConsumerPhoneNumber { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public InvalidRequestStatus InvalidStatus { get; set; }
    }
}
