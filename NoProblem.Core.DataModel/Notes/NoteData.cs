﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Notes
{
    public class NoteData
    {
        public Guid RegargingObjectId { get; set; }
        public string Description { get; set; }
        public string File { get; set; }
        public Guid CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
