﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.IvrWidget
{
    public enum ProcessStatus
    {
        Continue,
        GoBack,
        Repeat,
        HangUp
    }
}
