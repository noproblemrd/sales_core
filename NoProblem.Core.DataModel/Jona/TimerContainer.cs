﻿using System;
using System.Threading;

namespace NoProblem.Core.DataModel.Jona
{
    public class TimerContainer
    {
        public Timer Timer { get; set; }
        public Guid JonaId { get; set; }
        public DateTime TimerStartTime { get; set; }
    }
}
