﻿
namespace NoProblem.Core.DataModel.Jona
{
    public enum enumTtsStatus
    {
        ADVERTISER_ANSWERED,
        QUEUED,
        CONFIRMED,
        NOT_CONFIRMED,
        BUSY,
        UNKNOWN,
        CALL_INIT_TIMEOUT,
        ERROR,
        REJECT
    };
}
