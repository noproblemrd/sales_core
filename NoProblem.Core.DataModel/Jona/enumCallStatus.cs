﻿
namespace NoProblem.Core.DataModel.Jona
{
    public enum enumCallStatus
    {
        QUEUED,
        ADVERTISER_ANSWERED,
        ADVERTISER_CONFIRMED,
        CUSTOMER_BUSY,
        ADVERTISER_BUSY,
        CALL_IN_PROGRESS,
        CALL_COMPLETED,
        CALL_TERMINATED,
        ADVERTISER_NOT_CONFIRMED,
        UNKNOWN,
        CALL_INIT_TIMEOUT,
        ERROR,
        REJECTED
    };
}
