﻿using System;

namespace NoProblem.Core.DataModel.Jona
{
    public class RecordingsRetryContainerJona : Asterisk.RecordingsRetryContainer
    {
        public Guid IncidentAccountId { get; set; }
    }
}
