﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.AdvertiserRankings
{
    public class AarSupplierEntry
    {
        public Guid SupplierId { get; set; }
        public decimal Balance { get; set; }
        public int TrialAttempts { get; set; }
        public int TrialCallsAnswered { get; set; }
        public Xrm.account.TrialStatusReasonCode TrailStatusReason { get; set; }
        public string Phone { get; set; }
        public decimal IncidentPrice { get; set; }
        public int InitialTrialBudget { get; set; }
        public decimal AvailableCashBalance { get; set; }
        public Xrm.account.StageInTrialRegistration StageInTrialRegistration { get; set; }
    }
}
