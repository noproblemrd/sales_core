﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.AdvertiserRankings.GetRankingParametersResponse
//  File: GetRankingParametersResponse.cs
//  Description: Container to use as response for the GetRankingParameters service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace NoProblem.Core.DataModel.AdvertiserRankings
{
    public class GetRankingParametersResponse
    {
        public List<RankingParametersEntry> Parameters { get; set; }
    }
}
