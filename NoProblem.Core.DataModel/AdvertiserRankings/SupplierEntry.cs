﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.AdvertiserRankings.SupplierEntry
//  File: SupplierEntry.cs
//  Description: Container that represents a single supplier.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
namespace NoProblem.Core.DataModel.AdvertiserRankings
{
    public class SupplierEntry
    {
        public Guid SupplierId { get; set; }
        public string SupplierNumber { get; set; }
        public string SupplierName { get; set; }
        public string SumSurvey { get; set; }
        public string SumAssistanceRequests { get; set; }
        public string NumberOfEmployees { get; set; }
        public string ShortDescription { get; set; }
        public string Certificate { get; set; }
        public string DirectNumber { get; set; }
        public decimal IncidentPrice { get; set; }
        public string Telephone1 { get; set; }
        public decimal ConsiderAsPrice { get; set; }
    }
}
