﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.AdvertiserRankings.SetAdvertiserRankingsRequest
//  File: SetAdvertiserRankingsRequest.cs
//  Description: Container to use as request for the SetAdvertiserRankings service.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

namespace NoProblem.Core.DataModel.AdvertiserRankings
{
    [Serializable]
    public class SetAdvertiserRankingsRequest
    {
        public List<RankParamCodeValuePair> Parameters { get; set; }
    }

    [Serializable]
    public class RankParamCodeValuePair
    {
        public int Weight { get; set; }
        public int Code { get; set; }
    }
}
