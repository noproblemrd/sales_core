﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataModel.AdvertiserRankings.RankingParametersEntry
//  File: RankingParametersEntry.cs
//  Description: Container that represents a ranking parameter entry.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace NoProblem.Core.DataModel.AdvertiserRankings
{
    public class RankingParametersEntry
    {
        public int Weight { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
