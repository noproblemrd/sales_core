﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Audit
{
    public class LogFlavorUsabilityRequest
    {
        public string Flavor { get; set; }
        public string Action { get; set; }
    }
}
