﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Audit
{
    public class AuditRequest
    {
        public bool AuditOn { get; set; }
        public List<AuditEntry> AuditEntries { get; set; }
    }
}
