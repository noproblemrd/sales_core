﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Audit
{
    public class AuditEntry
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public Guid AdvertiseId { get; set; }
        public string AdvertiserName { get; set; }
        public string FieldName { get; set; }
        public string FieldId { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string PageId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Xrm.new_auditentry.Status AuditStatus { get; set; }
        public string PageName { get; set; }
    }
}
