﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Audit
{
    public class AuditReportRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string PageId { get; set; }
        public Guid SupplierId { get; set; }
    }
}
