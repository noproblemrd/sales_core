﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoProblem.Core.DataModel
{
    /// <summary>
    /// This enum is used as input parameter fro GetSupplierIncidents.
    /// To tell incidents in which win status are required.
    /// </summary>
    public enum IncidentWinStatus
    {
        Lose,
        Win,
        ALL
    }

    ///// <summary>
    ///// This enum is equivalent to the int received in Phones.asmx methods
    ///// </summary>
    //public enum enumCallStatusCode
    //{
    //    QUEUED = 0,
    //    ADVERTISER_ANSWERED,
    //    ADVERTISER_CONFIRMED,
    //    CUSTOMER_BUSY,
    //    ADVERTISER_BUSY,
    //    CALL_IN_PROGRESS,
    //    CALL_COMPLETED,
    //    CALL_TERMINATED,
    //    ADVERTISER_NOT_CONFIRMED,
    //    UNKNOWN,
    //    CALL_INIT_TIMEOUT,
    //    ERROR
    //}

    public enum enumCustomerNotificationTypes
    {
        NEW_SERVICE_REQUEST,
        SUPPLIERS_CONTACTED,
        SUPPLIER_INFO,
        CUSTOMER_NOT_AVAILABLE,
        SUPPLIER_UNREACHED,
        DIRECT_NUMBERS,
        DIRECT_NUMBERS_UNRECHED,
        CONSUMER_UNAVAILABLE,
        AFTER_CANDIDATE_CONNECTION_TO_CONSUMER
    }

    public enum enumSupplierNotificationTypes
    {
        INCIDENT_INFO,
        SUPPLIER_UNREACHED,
        SUPPLIER_SIGNUP,
        LOW_BALANCE,
        BALANCE_UNDER_INCIDENT_PRICE,
        SUPPLIER_LOSER,
        CONSUMER_UNAVAILABLE,
        REFUNDED,
        RANK_LOW,
        WEEKLY_LOST_CALLS,
        ACCESSIBLE_CANDIDATE_UNREACHED,
        IN_TRIAL_CANDIDATE_UNREACHED,
        AFTER_FIRST_PAYMENT,
        REMOVE_ME_FROM_TRIAL,
        AFTER_CANDIDATE_CONNECTION_TO_CANDIDATE,
        SUPPLIER_UNREACHED_GOLDEN_NUMBER_ADV
    }

    public enum enumNotificationMethod
    {
        NONE = 0,
        ONLY_MAIL = 1,
        ONLY_SMS = 2,
        MAIL_OR_SMS = 3,
        SMS_OR_MAIL = 4,
        MAIL_AND_SMS = 5
    }

    public enum enumServiceAreaType
    {
        ZipCode = 0
    }

    public enum enumExpertiseType
    {
        Primary = 1,
        Secondary = 2
    }

    public class FindSupplierParams
    {
        public string ExpertiseCode { get; set; }
        public int ExpertiseLevel { get; set; }
        public string AreaCode { get; set; }
        public int AreaLevel { get; set; }
        public List<Guid> Incidents { get; set; }
        public DateTime Availability { get; set; }
        public Guid OriginId { get; set; }
        public bool IsWithNoTimeNorCash { get; set; }
        public Guid DirectNumberRegionId { get; set; }
        public bool IsAar { get; set; }
        public int MaxTrialAttempts { get; set; }
        public bool IsIncludingTrial { get; set; }
        public bool ExcludePartners { get; set; }
    }

    public class TemplateNames
    {
        public const string CUSTOMER_NEW_SERVICE_REQUEST = "Customer New Service Request - Email";
        public const string CUSTOMER_NEW_SERVICE_REQUEST_SMS = "Customer New Service Request - SMS";
        public const string SUPPLIER_PASSWORD_BY_MAIL = "Send Password To Supplier - Email";
        public const string SUPPLIER_PASSWORD_BY_SMS = "Send Password To Supplier - SMS";
        public const string ADVERTISER_WELCOME_BY_MAIL = "Advertiser Welcome Message - Email";
        public const string ADVERTISER_WELCOME_BY_SMS = "Advertiser Welcome Message - SMS";
        public const string CUSTOMER_NOT_AVAILABLE = "Customer Not Available - Email";
        public const string CUSTOMER_NOT_AVAILABLE_SMS = "Customer Not Available - SMS";
        public const string CUSTOMER_ALL_SUPPLIERS_CONTACTED = "Customer Request Completed - Email";
        public const string CUSTOMER_ALL_SUPPLIERS_CONTACTED_SMS = "Customer Request Completed - SMS";
        public const string CUSTOMER_SUPPLIER_INFO = "Customer Supplier Info - Email";
        public const string CUSTOMER_SUPPLIER_INFO_SMS = "Customer Supplier Info - SMS";
        public const string CUSTOMER_SUPPLIER_UNREACHED = "Customer Supplier Unreached";
        public const string CUSTOMER_SUPPLIER_UNREACHED_SMS = "Customer Supplier Unreached - SMS";
        public const string SUPPLIER_INCIDENT_INFO = "Advertiser Incident Info - Email";
        public const string SUPPLIER_INCIDENT_INFO_SMS = "Advertiser Incident Info - SMS";
        public const string CUSTOMER_REACHED_DIRECT_NUMBERS = "Customer Reached Direct Numbers - SMS";
        public const string CUSTOMER_UNREACHED_DIRECT_NUMBERS = "Customer Unreached Direct Numbers - SMS";
        public const string SUPPLIER_UNREACHED_INFO = "Supplier Unreached - Email";
        public const string SUPPLIER_UNREACHED_INFO_SMS = "Supplier Unreached - SMS";
        public const string SUPPLIER_LOW_AMOUNT_SMS = "Supplier Amount Low - SMS";
        public const string SUPPLIER_BALANCE_UNDER_INCIDENT_PRICE_SMS = "Supplier Balance Under Incident Price - SMS";
        public const string SUPPLIER_BALANCE_UNDER_INCIDENT_PRICE = "Supplier Balance Under Incident Price";
        public const string SUPPLIER_LOSER_SMS = "Supplier Loser - SMS";
        public const string SUPPLIER_CONSUMER_UNAVAILABLE_SMS = "Supplier - Consumer Unavailable - SMS";
        public const string CONSUMER_CONSUMER_UNAVAILABLE_SMS = "Consumer - Consumer Unavailable - SMS";
        public const string EMAIL_LEAD = "Email Lead";
        public const string SMS_LEAD = "SMS Lead";
        public const string REFUND_ALERT = "RefundAlert";
        public const string SUPPLIER_REFUNDED = "Supplier Refunded - sms";
        public const string RANK_LOW = "RankLow - sms";
        public const string RECHARGE_FAILURE_ALERT_FOR_PUBLISHER = "RechargeFailureAlertForPublisher";
        public const string RECHARGE_FAILURE_ALERT_FOR_SUPPLIER = "RechargeFailureAlertForSupplier";
        public const string WEEKLY_LOST_CALLS_SMS = "Weekly Lost Calls - SMS";
        public const string ACCESSIBLE_CANDIDATE_UNREACHED_SMS = "ACCESSIBLE_CANDIDATE_UNREACHED_SMS";
        public const string IN_TRIAL_CANDIDATE_UNREACHED_SMS = "IN_TRIAL_CANDIDATE_UNREACHED_SMS";
        public const string AFTER_FIRST_PAYMENT_SMS = "AFTER_FIRST_PAYMENT_SMS";
        public const string PASSWORD_REMINDER_SMS = "PASSWORD_REMINDER_SMS";
        public const string PASSWORD_REMINDER_EMAIL = "PASSWORD_REMINDER_EMAIL";
        public const string REMOVE_ME_FROM_TRAIL_SMS = "REMOVE_ME_FROM_TRAIL_SMS";
        public const string AFTER_CANDIDATE_CONNECTION_TO_CANDIDATE = "AFTER_CANDIDATE_CONNECTION_TO_CANDIDATE";
        public const string AFTER_CANDIDATE_CONNECTION_TO_CONSUMER = "AFTER_CANDIDATE_CONNECTION_TO_CONSUMER";
        public const string REGISTRATION_FINISHED_ALERT = "REGISTRATION_FINISHED_ALERT";
        public const string NEW_REQUEST_ALERT = "NEW_REQUEST_ALERT";
        public const string NEW_STOPPED_REQUEST_ALERT = "NEW_STOPPED_REQUEST_ALERT";
        public const string WIBIYA_EMAIL_CONFIRMATION = "WIBIYA_EMAIL_CONFIRMATION";
        public const string WIBIYA_CASH_OUT_ALERT = "WIBIYA_CASH_OUT_ALERT";
        public const string DAPAZ_C2C_CUSTOMER = "DAPAZ_C2C_CUSTOMER";
        public const string DAPAZ_C2C_ADVERTISER = "DAPAZ_C2C_ADVERTISER";
        public const string SUPPLIER_UNREACHED_INFO_GOLDEN_NUMBER_ADV = "SUPPLIER_UNREACHED_INFO_GOLDEN_NUMBER_ADV";
        public const string SUPPLIER_UNREACHED_INFO_GOLDEN_NUMBER_ADV_SMS = "SUPPLIER_UNREACHED_INFO_GOLDEN_NUMBER_ADV_SMS";
        public const string PHONE_API_DIDNT_PRESS_ONE_SMS = "PHONE_API_DIDNT_PRESS_ONE_SMS";
    }

    public class MandrillTemplates
    {
        public const string E_AD_InviteSent = "E_AD_InviteSent";
        public const string E_Invitee_InviteSent = "E_Invitee_InviteSent";
        public const string E_Invitee_InviteSent_NoMessage = "E_Invitee_InviteSent_NoMessage";
        public const string E_AD_Reg_ITC = "E_AD_Reg_ITC";
        public const string E_Requester_WaitingList = "E_Requester_WaitingList";
        public const string E_Invitee_InviteeRegisteredAAR = "E_Invitee_InviteeRegisteredAAR";
        public const string E_Invitee_InviteeRegistered = "E_Invitee_InviteeRegistered";
        public const string E_AD_InviteeRegistered = "E_AD_InviteeRegistered";
        public const string E_AD_Deposit = "E_AD_Deposit";
        public const string E_AD_InviteeLost = "E_AD_InviteeLost";
        public const string E_Requester_LoginCodeAAR = "E_Requester_LoginCodeAAR";
        public const string E_Requester_LoginCode = "E_Requester_LoginCode";
        public const string E_AD_EmailVerification = "E_AD_EmailVerification";
        public const string E_AD_EmailVerificaiton_Social_Registration2014 = "E_AD_EmailVerificaiton_Social_Registration2014";
        public const string E_AD_EmailVerificaiton_No_Social_Registration2014 = "E_AD_EmailVerificaiton_No_Social_Registration2014";
        public const string E_AD_WelcomeToNoProblem = "E_AD_WelcomeToNoProblem";
        public const string E_AD_Delete = "E_AD_Delete";
        public const string E_Invitee_Invitation_2014 = "E_Invitee_Invitation_2014";
        public const string E_AD_ForgotPassword = "E_AD_ForgotPassword";
        public const string E_AD_EmailVerificaiton_Legacy2014 = "E_AD_EmailVerificaiton_Legacy2014";
        public const string E_AD_PaymentFailed = "E_AD_PaymentFailed";
        public const string E_AD_INVOICE_SINGLE_LEAD_PAYMENT = "E_AD_INVOICE_SINGLE_LEAD_PAYMENT";
        public const string E_AD_INVOICE_LEAD_BOOSTER_REG = "E_AD_INVOICE_LEAD_BOOSTER_REG";
        public const string E_AD_INVOICE_FEATURED_LISTING_REG = "E_AD_INVOICE_FEATURED_LISTING_REG";
        public const string E_AD_INVOICE_FEATURED_LISTING_MONTH = "E_AD_INVOICE_FEATURED_LISTING_MONTH";
        public const string E_AD_INVOICE_LEAD_BOOSTER_MONTH = "E_AD_INVOICE_LEAD_BOOSTER_MONTH";
        public const string E_AD_EmailVerification_Getting_Started = "E_AD_EmailVerification_Getting_Started";
        public const string E_AD_INVOICE_LEAD_BOOSTER_REG_WITH_EMAIL_VERIFICATION = "E_AD_INVOICE_LEAD_BOOSTER_REG_WITH_EMAIL_VERIFICATION";
        public const string Lead_To_Broker = "Lead_To_Broker";
        public const string E_CUSTOMER_POST_LEAD_DESKTOP_APP = "E_CUSTOMER_POST_LEAD_DESKTOP_APP";
        public const string E_CUSTOMER_POST_CALL_AND_RECORD_DESKTOP_APP = "E_CUSTOMER_POST_CALL_AND_RECORD_DESKTOP_APP";


        public const string E_AD_INVOICE_CLIPCALL_REG = "E_AD_INVOICE_CLIPCALL_REG";
        public const string E_AD_INVOICE_CLIPCALL_SINGLE_LEAD_PAYMENT = "E_AD_INVOICE_CLIPCALL_SINGLE_LEAD_PAYMENT";


    }

    public class ConfigurationKeys
    {
        public const string ADVERTISER_DISPLAY_NUMBER = "AdvertiserDisplayNumber";
        public const string NEW_INCIDENT_TIME_PASS = "NewIncidentTimePass";
        public const string AFFILIATE_PAY_TIME_PASS = "AffiliatePayTimePass";
        public const string COUNTRY_PHONE_PREFIX = "CountryPhonePrefix";
        public const string RECORD_CALLS = "RecordCalls";
        public const string DIRECT_NUMBER_INCIDENT_TITLE = "DirectNumberIncidentTitle";
        public const string SELF_SERVICE_INCIDENT_TITLE = "SelfServiceIncidentTitle";
        public const string CALL_CENTER_PHONE = "CallCenterPhone";
        public const string EXTENSION_NUMBER = "ExtensionNumber";
        public const string SEND_SMS_RETRY_AMOUNT = "SendSmsRetryAmount";
        public const string PUBLISHER_TIME_ZONE = "PublisherTimeZone";
        public const string ATTEMPTED_TRIES_INTERVAL = "AttemptedTriesInterval";
        public const string SUPPLIER_ATTEMPTED_TRIES = "SupplierAttemptedTries";
        public const string SUPPLIER_NOTIFY_BALANCE_AMOUNT = "SupplierNotifyBalanceAmount";
        public const string REFUND_SENTENCE = "RefundSentence";
        public const string AUCTION_SUPPLIERS_MULTIPLIER = "AuctionSuppliersMultiplier";
        public const string MAX_REGION_LEVEL = "MaxRegionLevel";
        public const string FTP_URL_BASE = "FtpUrlBase";
        public const string FTP_USERNAME = "FtpUserName";
        public const string FTP_PASSWORD = "FtpPassword";
        public const string INTERNAL_PHONE_PREFIX = "InternalPhonePrefix";
        public const string EMAIL_SENDER = "EmailSender";
        public const string BLOCKED_NUMBER_NAME = "BlockedNumberName";
        public const string CHECK_SUPPLIER_DUPLICATES = "CheckSupplierDuplicates";
        public const string SMS_CALLER_ID = "SmsCallerId";
        public const string REFUND_ALERT_USER = "RefundAlertUser";
        public const string MIN_DEPOSIT_PRICE = "MinDepositPrice";
        public const string TTS_FOR_JONA = "TtsForJona";
        public const string UNAVAILABILITY_LIMIT = "UnavailabilityLimit";
        public const string SOLEK = "Solek";
        public const string RECHARGE_BONUS_PERCENT = "RechargeBonusPercent";
        public const string RECHARGE_FAILED_ALERT_USER = "RechargeFailedAlertUser";
        public const string INVOICE_DISPATCHER = "InvoiceDispatcher";
        public const string CHECK_CASE_IS_DUPLICATE_SECONDS = "CheckCaseIsDuplicateSeconds";
        public const string DIRECT_NUMBERS_HAVE_REGION = "DirectNumbersHaveRegion";
        public const string AUTO_UPSALE_ENABLED = "AutoUpsaleEnabled";
        public const string AUTO_UPSALE_HOURS_BACK = "AutoUpsaleHoursBack";
        public const string WEEKLY_LOST_CALLS_HOUR = "WeeklyLostCallsHour";
        public const string WEEKLY_LOST_CALLS_DAY = "WeeklyLostCallsDay";
        public const string WEEKLY_LOST_CALLS_MINIMUM = "WeeklyLostCallsMinimum";
        public const string CASH = "Cash";
        public const string CHECK = "Check";
        public const string CREDITCARD = "CreditCard";
        public const string WIRETRANSFER = "WireTransfer";
        public const string VOUCHER = "Voucher";
        public const string REFUND_REQUEST_PERCENTAGE_LIMIT = "RefundRequestPercentageLimit";
        public const string REFUND_DAYS_BACK_LIMIT = "RefundDaysBackLimit";
        public const string INVOICE_SEND_OPTIONS_ENABLED = "InvoiceSendOptionsEnabled";
        public const string MINIMAL_CALL_LENGTH_TO_CHARGE = "MinimalCallLengthToCharge";
        public const string CHECK_PHONE_DUPLICATE = "CheckPhoneDuplicate";
        public const string AAR_ENABLED = "AarEnabled";
        public const string MACHINE_CHECK_FROM_PHONE = "MachineCheckFromPhone";
        public const string OUTBOUND_CALL_FROM_PHONE = "OutboundCallFromPhone";
        public const string INITIAL_TRAIL_CREDIT = "InitialTrialCredit";
        public const string TRIAL_VOUCHER_NUMBER = "TrialVoucherNumber";
        public const string TRIAL_VOUCHER_REASON = "TrialVoucherReason";
        public const string MAX_ACCESSIBLE_ATTEMPTS = "MaxAccessibleAttempts";
        public const string MACHINE_CHECK_BULK_SIZE = "MachineCheckBulkSize";
        public const string DISTANCE_UNIT = "DistanceUnit";
        public const string USE_DO_NO_CALL_LIST = "UseDoNotCallList";
        public const string IVR_FIND_ALTERNATIVE_ADVERTISER = "IVR_FIND_ALTERNATIVE_ADVERTISER";
        public const string IVR_NO_ALTERNATIVE_FOUND = "IVR_NO_ALTERNATIVE_FOUND";
        public const string IVR_ADVERTISER_ANNOUNCE = "IVR_ADVERTISER_ANNOUNCE";
        public const string IVR_CALLER_ANNOUNCE = "IVR_CALLER_ANNOUNCE";
        public const string DDID_SESSION_TIME = "DDID_SESSION_TIME";
        public const string DDID_USED = "DDID_USED";
        public const string CHECK_MACHINE_IN_ALL_AAR_CALLS = "CHECK_MACHINE_IN_ALL_AAR_CALLS";
        public const string PUBLISHER_INVOICES_EMAIL = "PUBLISHER_INVOICES_EMAIL";
        public const string REGISTRATION_FINISHED_ALERT_EMAIL = "REGISTRATION_FINISHED_ALERT_EMAIL";
        public const string SMTP_HOST = "SMTP_HOST";
        public const string AUTO_SCRAPING_ENABLED = "AUTO_SCRAPING_ENABLED";
        public const string AAR_VOUCHER_NUMBER = "AAR_VOUCHER_NUMBER";
        public const string AAR_VOUCHER_REASON_ID = "AAR_VOUCHER_REASON_ID";
        public const string NEW_REQUEST_ALERT_EMAIL = "NEW_REQUEST_ALERT_EMAIL";
        public const string WIBIYA_CASH_OUT_ALERT_EMAIL = "WIBIYA_CASH_OUT_ALERT_EMAIL";
        public const string ASTERISK_DIRECT = "ASTERISK_DIRECT";
        public const string DONT_USE_AVERAGE_AS_MIN_PRICE = "DONT_USE_AVERAGE_AS_MIN_PRICE";
        public const string CONSUMER_CONNECTION_PROMPT = "CONSUMER_CONNECTION_PROMPT";
        public const string AUDIO_FILES_URL_BASE = "AUDIO_FILES_URL_BASE";
        public const string GET_STARTED_CALL_TEST_FAKE_CUSTOMER_NUMBER = "GET_STARTED_CALL_TEST_FAKE_CUSTOMER_NUMBER";
        public const string INVITATION_SUCCESS_BONUS = "INVITATION_SUCCESS_BONUS";
        public const string INVITATION_LIFETIME_BONUS = "INVITATION_LIFETIME_BONUS";
        public const string LAME = "LAME";
        public const string FONE_API_ON = "FONE_API_ON";
        public const string LEAD_BROKER_FACTOR = "LEAD_BROKER_FACTOR";
        public const string VIRTUAL_MONEY_ONLY_FACTOR = "VIRTUAL_MONEY_ONLY_FACTOR";
        public const string EMAIL_VERIFICATION_URL = "EMAIL_VERIFICATION_URL";
        public const string DAPAZ_PPA_SYNC = "DAPAZ_PPA_SYNC";
        public const string DAPAZ_PPA_SYNC_DIR = "DAPAZ_PPA_SYNC_DIR";
        public const string DB_NAME = "DB_NAME";
        public const string FINANCIAL_DASHBOARD_ENABLED = "FINANCIAL_DASHBOARD_ENABLED";
        public const string HOT_CALL_SECONDS = "HOT_CALL_SECONDS";
        public const string FINANCIAL_DASHBOARD_ONLY_SOLD_HEADINGS = "FINANCIAL_DASHBOARD_ONLY_SOLD_HEADINGS";
        public const string PRICE_CHANGE_NOTIFY_EMAILS = "PRICE_CHANGE_NOTIFY_EMAILS";
        public const string RARAH_QUOTE_VOUCHER = "RARAH_QUOTE_VOUCHER";
        public const string RARAH_QUOTE_VOUCHER_REASON_ID = "RARAH_QUOTE_VOUCHER_REASON_ID";
        public const string RARAH_QUOTE_LOAD_ERROR_EMAILS = "RARAH_QUOTE_LOAD_ERROR_EMAILS";
        public const string RARAH_QUOTE_LOAD_ERROR_EMAIL_SUBJECT = "RARAH_QUOTE_LOAD_ERROR_EMAIL_SUBJECT";
        public const string RARAH_QUOTE_LOAD_ERROR_EMAIL_BODY = "RARAH_QUOTE_LOAD_ERROR_EMAIL_BODY";
        public const string GOLDEN_ADVERTISER_IN_HEBREW = "GOLDEN_ADVERTISER_IN_HEBREW";
        public const string RARAH_QUOTE_SUCCESS_CALL_SECONDS = "RARAH_QUOTE_SUCCESS_CALL_SECONDS";
        public const string IVR_CALLER_ANNOUNCE_RARAH_QUOTE = "IVR_CALLER_ANNOUNCE_RARAH_QUOTE";
        public const string IVR_ADVERTISER_ANNOUNCE_RARAH_QUOTE = "IVR_ADVERTISER_ANNOUNCE_RARAH_QUOTE";
        public const string OUTBOUND_CALL_FROM_PHONE_REGULAR_NUMBER = "OUTBOUND_CALL_FROM_PHONE_REGULAR_NUMBER";
        public const string MONTHLY_PAYMENT_FOR_PPA_ENABLED = "MONTHLY_PAYMENT_FOR_PPA_ENABLED";
        public const string MONTHLY_PAYMENT_FEE = "MONTHLY_PAYMENT_FEE";
        public const string MIN_AMOUNT_FOR_AUTO_RECHARGE_LOW_BALANCE = "MIN_AMOUNT_FOR_AUTO_RECHARGE_LOW_BALANCE";
        public const string MIN_AMOUNT_FOR_AUTO_RECHARGE_MAX_MONTHLY = "MIN_AMOUNT_FOR_AUTO_RECHARGE_MAX_MONTHLY";
        public const string CALL_PRICE_PICKER_TOOL_ENABLED = "CALL_PRICE_PICKER_TOOL_ENABLED";
        public const string DAPAZ_USERS_SYNC_ENABLED = "DAPAZ_USERS_SYNC_ENABLED";
        public const string DAPAZ_USERS_SYNC_EMAIL_SUBJECT = "DAPAZ_USERS_SYNC_EMAIL_SUBJECT";
        public const string DAPAZ_USERS_SYNC_EMAIL_BODY = "DAPAZ_USERS_SYNC_EMAIL_BODY";
        public const string STUPID_EMAIL_TO = "STUPID_EMAIL_TO";
        public const string NOT_FOR_SALE_DONT_PAY_AFFILIATES = "NOT_FOR_SALE_DONT_PAY_AFFILIATES";
        public const string UNLIMITED_UPSALES_ENABLED = "UNLIMITED_UPSALES_ENABLED";
        public const string UNLIMITED_UPSALES_HOURS = "UNLIMITED_UPSALES_HOURS";
        public const string VAT = "VAT";
        public const string EXTERNAL_PAYMENTS_SYSTEM = "EXTERNAL_PAYMENTS_SYSTEM";
        public const string ZAP_PAYMENTS_SYSTEM_URL = "ZAP_PAYMENTS_SYSTEM_URL";
        public const string ZAP_PAYMENTS_SYSTEM_LOGIN_PREFIX = "ZAP_PAYMENTS_SYSTEM_LOGIN_PREFIX";
        public const string AUTO_REROUTE_ENABLED = "AUTO_REROUTE_ENABLED";
        public const string ZAP_ORDERS_SYSTEM_URL = "ZAP_ORDERS_SYSTEM_URL";
        public const string ZAP_ORDERS_SYSTEM_ENABLED = "ZAP_ORDERS_SYSTEM_ENABLED";
        public const string ZAP_ORDERS_SYSTEM_LOGIN_PREFIX = "ZAP_ORDERS_SYSTEM_LOGIN_PREFIX";
        public const string BITLY_API_TOKEN = "BITLY_API_TOKEN";
        public const string PHONE_API_IMPLEMENTATION = "PHONE_API_IMPLEMENTATION";
        public const string REALTIMEROUTINGERRORPREVENTION_HOURS_BACK = "REALTIMEROUTINGERRORPREVENTION_HOURS_BACK";
        public const string REALTIMEROUTINGERRORPREVENTION_DROP_ALLOWED = "REALTIMEROUTINGERRORPREVENTION_DROP_ALLOWED";
        public const string REALTIMEROUTINGERRORPREVENTION_EMAILS = "REALTIMEROUTINGERRORPREVENTION_EMAILS";
        public const string AUTO_RESUBMISSION_ENABLED = "AUTO_RESUBMISSION_ENABLED";
        public const string FREE_BLOCKED_CALLS_FOR_TWO_MONTHS_ENABLED = "FREE_BLOCKED_CALLS_FOR_TWO_MONTHS_ENABLED";
        public const string PPA_ONLINE_SYNC_ENABLED = "PPA_ONLINE_SYNC_ENABLED";
        public const string BILLING_RATE_CALCULATION_ENABLED = "BILLING_RATE_CALCULATION_ENABLED";
        public const string PPA_ONLINE_SYNC_PASSWORD = "PPA_ONLINE_SYNC_PASSWORD";
        public const string PPA_ONLINE_SYNC_URL = "PPA_ONLINE_SYNC_URL";
        public const string MIN_CALL_DURATION_FOR_EXEMPT_OF_MONTHLY_PAY = "MIN_CALL_DURATION_FOR_EXEMPT_OF_MONTHLY_PAY";
        public const string EMAIL_VERIFICATION_URL_REGISTRATION_2014 = "EMAIL_VERIFICATION_URL_REGISTRATION_2014";
        public const string ZERO_DURATION_NOTIFIER_NOTIFY_COUNT = "ZERO_DURATION_NOTIFIER_NOTIFY_COUNT";
        public const string ZERO_DURATION_NOTIFIER_EMAILS = "ZERO_DURATION_NOTIFIER_EMAILS";
        public const string USA_SYSTEM = "USA_SYSTEM";
        public const string MAX_BID = "MAX_BID";
        public const string FEATURED_LISTING_SUBSCRIPTION_COST = "FEATURED_LISTING_SUBSCRIPTION_COST";
        public const string INVITATION_URL_BASE = "INVITATION_URL_BASE";
        public const string TEN_LEADS_BONUS = "TEN_LEADS_BONUS";
        public const string FORGOT_PASSWORD_URL = "FORGOT_PASSWORD_URL";
        public const string TEST_CALL_CALLER_ID_TWILIO_DID = "TEST_CALL_CALLER_ID_TWILIO_DID";
        public const string EMAIL_VERIFICATION_URL_LEGACY_2014 = "EMAIL_VERIFICATION_URL_LEGACY_2014";
        public const string PAYMENT_FAILED_EMAIL_URL = "PAYMENT_FAILED_EMAIL_URL";
        public const string PAYMENT_FAILED_SMS_URL = "PAYMENT_FAILED_SMS_URL";
        public const string DEFAULT_COUNTRY = "DEFAULT_COUNTRY";
        public const string INJECTION_EMAIL_REPORTS_GMAIL_USER = "INJECTION_EMAIL_REPORTS_GMAIL_USER";
        public const string INJECTION_EMAIL_REPORTS_GMAIL_PASSWORD = "INJECTION_EMAIL_REPORTS_GMAIL_PASSWORD";
        public const string REGISTRATION_BONUS = "REGISTRATION_BONUS";
        public const string GCM_API_KEY = "GCM_API_KEY";
        public const string GCM_API_KEY_V2 = "GCM_API_KEY_V2";
        public const string UNKNOWN_ORIGIN_ID = "UNKNOWN_ORIGIN_ID";
        public const string PING_POST_API_STATIC_PRICING_MINIMAL_PROFIT = "PING_POST_API_STATIC_PRICING_MINIMAL_PROFIT";
        public const string APNS_CERTIFICATE_PASSWORD = "APNS_CERTIFICATE_PASSWORD";
        public const string APNS_CERTIFICATE_PATH = "APNS_CERTIFICATE_PATH";
        public const string PUSHER_APP_SECRET = "PUSHER_APP_SECRET";
        public const string PUSHER_APP_KEY = "PUSHER_APP_KEY";
        public const string PUSHER_APP_ID = "PUSHER_APP_ID";
        public const string YELP_CONSUMER_KEY = "YELP_CONSUMER_KEY";
        public const string YELP_CONSUMER_KEY_SECRET = "YELP_CONSUMER_KEY_SECRET";
        public const string YELP_ACCESS_TOKEN_SECRET = "YELP_ACCESS_TOKEN_SECRET";
        public const string YELP_ACCESS_TOKEN = "YELP_ACCESS_TOKEN";
        public const string GOOGLE_API_KEY = "GOOGLE_API_KEY";
        public const string AAR_VIDEO_DESTINATION_PAGE = "AAR_VIDEO_DESTINATION_PAGE";
        public const string AAR_VIDEOLEAD_READAIL_TIMES = "AAR_VIDEOLEAD_READAIL_TIMES";
        public const string AAR_VIDEOLEAD_READAIL_INTERVAL_MINUTE = "AAR_VIDEOLEAD_READAIL_INTERVAL_MINUTE";
        public const string GOOGLE_SHORTEN_URL_API_KEY = "GOOGLE_SHORTEN_URL_API_KEY";
        public const string MOBILE_AAR_TIMEOUT = "MOBILE_AAR_TIMEOUT";
        public const string MOBILE_BID_TIMEOUT = "MOBILE_BID_TIMEOUT";
        public const string MOBILE_INCIDENT_TIMEOUT = "MOBILE_INCIDENT_TIMEOUT";
        public const string MOBILE_DEFAULT_MAX_PRICE = "MOBILE_DEFAULT_MAX_PRICE";
        public const string MOBILE_DEFAULT_MED_PRICE = "MOBILE_DEFAULT_MED_PRICE";
        public const string MOBILE_DEFAULT_MIN_PRICE = "MOBILE_DEFAULT_MIN_PRICE";
        public const string DOWNLOAD_MOBILE_APP_DESTINATION = "DOWNLOAD_MOBILE_APP_DESTINATION";
        public const string MOBILE_APP_USE_AAR = "MOBILE_APP_USE_AAR";
        public const string SMS_PHONE_NUMBER_CAMPAIGN = "SMS_PHONE_NUMBER_CAMPAIGN";
        public const string CITYGRID_API_KEY = "CITYGRID_API_KEY";
        public const string YP_API_KEY = "YP_API_KEY";
        public const string MOBILE_FREE_LEADS = "MOBILE_FREE_LEADS";
        public const string CNAME_DOMAIN_SHORTENURL = "CNAME_DOMAIN_SHORTENURL";

    }

    public class AsteriskConfigurationKeys
    {
        public const string PHONE_PROTOCOL = "PHONE_PROTOCOL";
        public const string CALL_LIMIT = "CALL_LIMIT";
        public const string HOST = "HOST";
        public const string C2C_RETURN_ADDRESS = "C2C_RETURN_ADDRESS";
        public const string TTS_RETURN_ADDRESS = "TTS_RETURN_ADDRESS";
        public const string APP_NAME = "APP_NAME";
        public const string CID = "CID";
        public const string ENCODING = "ENCODING";
        public const string LANG = "LANG";
        public const string PROMPTS_CLASS = "PROMPTS_CLASS";
        public const string PROMPTS_CLASS_OLD = "PROMPTS_CLASS_OLD";
        public const string TTS_ADDRESS = "TTS_ADDRESS";
        public const string VOICE = "VOICE";
        public const string COUNTRY_CODE = "COUNTRY_CODE";
        public const string INTERNAL_COUNTRY_PREFIX = "INTERNAL_COUNTRY_PREFIX";
        public const string LOCAL_AREA_CODE = "LOCAL_AREA_CODE";
        public const string INTERNATIONAL_EXTENSION = "INTERNATIONAL_EXTENSION";
        public const string ASTERISK_C2C_URL = "ASTERISK_C2C_URL";
        public const string ASTERISK_TTS_URL = "ASTERISK_TTS_URL";
        public const string ASTERISK_RECORDINGS_URL = "ASTERISK_RECORDINGS_URL";
        public const string RECORDINGS_FOLDER = "RECORDINGS_FOLDER";
        public const string VIENNA_TRUNK = "VIENNA_TRUNK";
        public const string SUCCESSFUL_CALL_MIN_LENGTH = "SUCCESSFUL_CALL_MIN_LENGTH";
        public const string RECORD_LEGS = "RECORD_LEGS";
        public const string TTS_FILES_SAVE_DIRECTORY = "TTS_FILES_SAVE_DIRECTORY";
        public const string TTS_FILES_URL_BASE = "TTS_FILES_URL_BASE";
        public const string VN_ANTI_FRAUD_SECONDS_BACK = "VN_ANTI_FRAUD_SECONDS_BACK";
        public const string VN_ANTI_FRAUD_BLOCKED_NUMBER_ATTEMPTS = "VN_ANTI_FRAUD_BLOCKED_NUMBER_ATTEMPTS";
        public const string VN_ANTI_FRAUD_NORMAL_NUMBER_ATTEMPTS = "VN_ANTI_FRAUD_NORMAL_NUMBER_ATTEMPTS";
        public const string NON_PPC_RETURN_ADDRESS = "NON_PPC_RETURN_ADDRESS";
        public const string BILLING_CODE = "BILLING_CODE";
        public const string FAPI_KEY = "FAPI_KEY";
        public const string FAPI_SECRET = "FAPI_SECRET";
        public const string FONEAPI_URL_BASE = "FONEAPI_URL_BASE";
        public const string FONEAPI_TRUNK = "FONEAPI_TRUNK";
        public const string DELAY_BETWEEN_JONA_RETRIES_SECONDS = "DELAY_BETWEEN_JONA_RETRIES_SECONDS";
        public const string MAX_FAULTS_ALLOWED_JONA = "MAX_FAULTS_ALLOWED_JONA";
        public const string CLIP_CALL_CID = "CLIP_CALL_CID";
    }

    public class NotificationConfigurationKeys
    {
        public const string NOTIFICATION_SUPPLIER_SIGNUP = "SupplierSignupNotification";
        public const string NOTIFICATION_CUSTOMER_NEW_SERVICE_REQUEST = "CustomerNewServiceRequestNotification";
        public const string NOTIFICATION_CUSTOMER_NOT_AVAILABLE = "CustomerNotAvailableNotification";
        public const string NOTIFICATION_CUSTOMER_ALL_SUPPLIERS_CONTACTED = "CustomerRequestCompletedNotification";
        public const string NOTIFICATION_CUSTOMER_SUPPLIER_INFO = "CustomerSupplierInfoNotification";
        public const string NOTIFICATION_SUPPLIER_INCIDENT_INFO = "SupplierIncidentInfoNotification";
        public const string NOTIFICATION_CUSTOMER_RECHED_DIRECT_NUMBERS = "CustomerDirectNumberNotification";
        public const string NOTIFICATION_CUSTOMER_UNREACHED_DIRECT_NUMBERS = "CustomerUnreachedDirectNumberNotification";
        public const string NOTIFICATION_SUPPLIER_UNREACHED = "SupplierUnreachedNotification";
        public const string NOTIFICATION_SUPPLIER_LOW_BALANCE = "SupplierLowBalanceNotification";
        public const string NOTIFICATION_SUPPLIER_BALANCE_UNDER_INCIDENT_PRICE = "SupplierBalanceUnderIncidentPriceNotification";
        public const string NOTIFICATION_SUPPLIER_LOSER = "SupplierLoserNotification";
        public const string NOTIFICATION_SUPPLIER_CONSUMER_UNAVAILABLE = "SupplierConsumerUnavailableNotification";
        public const string NOTIFICATION_CONSUMER_CONSUMER_UNAVAILABLE = "ConsumerConsumerUnavailableNotification";
        public const string NOTIFICATION_SUPPLIER_REFUNDED = "SupplierRefundedNotification";
        public const string NOTIFICATION_RANK_LOW = "RankLowNotification";
        public const string NOTIFICATION_WEEKLY_LOST_CALLS = "WeeklyLostCallsNotification";
        public const string NOTIFICATION_IN_TRIAL_CANDIDATE_UNREACHED = "NOTIFICATION_IN_TRIAL_CANDIDATE_UNREACHED";
        public const string NOTIFICATION_ACCESSIBLE_CANDIDATE_UNREACHED = "NOTIFICATION_ACCESSIBLE_CANDIDATE_UNREACHED";
        public const string NOTIFICATION_AFTER_FIRST_PAYMENT = "NOTIFICATION_AFTER_FIRST_PAYMENT";
        public const string NOTIFICATION_REMOVE_ME_FROM_TRAIL = "NOTIFICATION_REMOVE_ME_FROM_TRAIL";
        public const string NOTIFICATION_CUSTOMER_SUPPLIER_UNREACHED = "NOTIFICATION_CUSTOMER_SUPPLIER_UNREACHED";
        public const string NOTIFICATION_AFTER_CANDIDATE_CONNECTION_TO_CANDIDATE = "NOTIFICATION_AFTER_CANDIDATE_CONNECTION_TO_CANDIDATE";
        public const string NOFIFICATION_AFTER_CANDIDATE_CONNECTION_TO_CONSUMER = "NOFIFICATION_AFTER_CANDIDATE_CONNECTION_TO_CONSUMER";
        public const string NOTIFICATION_SUPPLIER_UNREACHED_GOLDEN_NUMBER_ADV = "NOTIFICATION_SUPPLIER_UNREACHED_GOLDEN_NUMBER_ADV";
    }
}
