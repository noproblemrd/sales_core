﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class RegionRequest
    {
        public int level { get; set; }
        public Guid RegionParentId { get; set; }
    }
}
