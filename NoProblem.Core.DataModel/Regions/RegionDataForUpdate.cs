﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class RegionDataForUpdate
    {
        public Guid RegionId { get; set; }
        public DataModel.Xrm.new_regionaccountexpertise.RegionState RegionState { get; set; }
        public bool IsDirty { get; set; }
        public int Level { get; set; }
        public Guid Parent1 { get; set; }
        public Guid Parent2 { get; set; }
        public Guid Parent3 { get; set; }
        public Guid Parent4 { get; set; }
        public Guid Parent5 { get; set; }
    }
}
