﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class RegionDataForSearch : RegionDataForRetrieve
    {
        public List<RegionDataForSearch> Children { get; set; }
    }
}
