﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class RegionAccountMinData
    {
        public Guid Id { get; set; }
        public Guid RegionId { get; set; }
        public Guid SupplierId { get; set; }
        public Xrm.new_regionaccountexpertise.RegionState RegionState { get; set; }
        public int Level { get; set; }
    }
}
