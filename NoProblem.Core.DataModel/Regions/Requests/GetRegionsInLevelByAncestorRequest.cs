﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class GetRegionsInLevelByAncestorRequest
    {
        public Guid AncestorId { get; set; }
        public int Level { get; set; }
    }
}
