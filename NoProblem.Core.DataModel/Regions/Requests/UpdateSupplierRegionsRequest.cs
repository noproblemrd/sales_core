﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class UpdateSupplierRegionsRequest
    {
        public Guid SupplierId { get; set; }
        public List<RegionDataForUpdate> RegionDataList { get; set; }
        public bool IsFromAAR { get; set; }
    }
}
