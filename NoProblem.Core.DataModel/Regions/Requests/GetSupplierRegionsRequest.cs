﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class GetSupplierRegionsRequest
    {
        public Guid SupplierId { get; set; }
        public Guid ParentRegionId { get; set; }
    }
}
