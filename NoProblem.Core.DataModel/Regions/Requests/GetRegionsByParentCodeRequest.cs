﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions.Requests
{
    public class GetRegionsByParentCodeRequest
    {
        public string ParentCode { get; set; }
        public string SiteId { get; set; }
    }
}
