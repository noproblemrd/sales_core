﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class GetSupplierRegionsSearchRequest
    {
        public Guid SupplierId { get; set; }
        public string RegionName { get; set; }
    }
}
