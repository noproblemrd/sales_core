﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class RegionData
    {
        public string RegionName { get; set; }
        public Guid RegionId { get; set; }
    }
}
