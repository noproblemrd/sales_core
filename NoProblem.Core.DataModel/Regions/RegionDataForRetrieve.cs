﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class RegionDataForRetrieve : RegionData
    {
        public Xrm.new_regionaccountexpertise.RegionState RegionState { get; set; }
    }
}
