﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class RegionCodeNamePair
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
