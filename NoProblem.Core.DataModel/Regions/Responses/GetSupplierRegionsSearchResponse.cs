﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class GetSupplierRegionsSearchResponse
    {
        public List<RegionDataForSearch> RegionDataList { get; set; }
    }
}
