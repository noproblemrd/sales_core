﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Regions
{
    public class GetSupplierRegionsResponse
    {
        public List<RegionDataForRetrieve> RegionDataList { get; set; }
        public int StageInRegistration { get; set; }
    }
}
