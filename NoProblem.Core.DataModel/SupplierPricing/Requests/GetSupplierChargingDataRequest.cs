﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class GetSupplierChargingDataRequest
    {
        public Guid SupplierId { get; set; }
    }
}
