﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class UpdateExemptionOfMonthlyPaymentRequest : Audit.AuditRequest
    {
        public Guid SupplierId { get; set; }
        public bool IsExemptOfMonthlyPayment { get; set; }
    }
}
