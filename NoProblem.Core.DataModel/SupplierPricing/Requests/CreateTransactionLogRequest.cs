﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class CreateTransactionLogRequest
    {
        public List<TransactionLogKeyValuePair> ResultValueDictionary { get; set; }
        public Guid PricingId { get; set; }
        public Guid SupplierId { get; set; }
    }
}
