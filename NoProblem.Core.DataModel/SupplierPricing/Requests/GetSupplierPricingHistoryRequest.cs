﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class GetSupplierPricingHistoryRequest : PagingRequest
    {
        public Guid SupplierId { get; set; }
        public string PricingNumber { get; set; }
    }
}
