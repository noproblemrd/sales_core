﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class UpdateAutoRechargeOptionsRequest
    {
        public Guid SupplierId { get; set; }
        public int RechargeAmount { get; set; }
        public bool IsAutoRecharge { get; set; }
        public Xrm.account.RechargeTypeCode? RechargeTypeCode { get; set; }
        public int DayOfMonth { get; set; }
        public bool IsFromDollar { get; set; }
        public Guid CreatedByUserId { get; set; }
    }
}
