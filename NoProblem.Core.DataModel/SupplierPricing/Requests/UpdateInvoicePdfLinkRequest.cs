﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class UpdateInvoicePdfLinkRequest
    {
        public string InvoicePdfLink { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceNumberRefund { get; set; }
        public Guid PricingId { get; set; }
    }
}
