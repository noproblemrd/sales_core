﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class SupplierPricingComponent
    {
        public eSupplierPricingComponentType Type { get; set; }
        public decimal AmountPayed { get; set; }
        public decimal BaseAmount { get; set; }

        public enum eSupplierPricingComponentType
        {
            MonthlyFeePayment,
            AutoMaxMonthly,
            AutoLow,
            Manual
        }
    }
}
