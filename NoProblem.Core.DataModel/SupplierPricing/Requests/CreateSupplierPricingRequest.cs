﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class CreateSupplierPricingRequest : Audit.AuditRequest
    {
        public CreateSupplierPricingRequest()
        {
            BonusType = Xrm.new_supplierpricing.BonusType.None;
        }

        public Guid SupplierId { get; set; }
        public Xrm.new_paymentmethod.ePaymentMethod PaymentMethod { get; set; }
        public int PaymentAmount { get; set; }
        public int BonusAmount { get; set; }
        public bool IsAutoRenew { get; set; }
        public string CreditCardToken { get; set; }
        public string CVV2 { get; set; }
        public string Last4DigitsCC { get; set; }
        public string TransactionId { get; set; }
        public string CreditCardOwnerId { get; set; }
        public string CreditCardOwnerName { get; set; }
        public string ChargingCompany { get; set; }
        public string VoucherNumber { get; set; }
        public int RechargeAmount { get; set; }
        public Xrm.account.RechargeTypeCode RechargeTypeCode { get; set; }
        public int RechargeDayOfMonth { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankAccount { get; set; }
        public Guid VoucherReasonId { get; set; }
        public Guid CreatedByUserId { get; set; }
        public Xrm.new_supplierpricing.BonusType BonusType { get; set; }
        public int ExtraBonus { get; set; }
        public Guid ExtraBonusReasonId { get; set; }
        public int NumberOfPayments { get; set; }
        public DateTime? CheckDate { get; set; }
        public string CheckNumber { get; set; }
        public DateTime? TransferDate { get; set; }
        public bool UpdateRechargeFields { get; set; }
        public string InvoiceNumber { get; set; }
        public bool LookForInvoiceInRefund { get; set; }
        public bool IsFromAAR { get; set; }
        public string ChargingCompanyAccountNumber { get; set; }
        public string ChargingCompanyContractId { get; set; }
        public string CreditCardType { get; set; }
        public string UserAgent { get; set; }
        public string UserIP { get; set; }
        public string UserRemoteHost { get; set; }
        public string UserAcceptLanguage { get; set; }
        public DataModel.Xrm.new_balancerow.Action? Action { get; set; }
        public string Description { get; set; }
        public List<SupplierPricingComponent> Components { get; set; }
        public string CreditCardExpDate { get; set; }
        public bool IsFromDollar { get; set; }
        public string BillingAddress { get; set; }
        public string InvoiceUrl { get; set; }
        public string BillingPhone { get; set; }
        public string BillingEmail { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
    }
}
