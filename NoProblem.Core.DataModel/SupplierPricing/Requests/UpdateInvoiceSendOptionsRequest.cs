﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class UpdateInvoiceSendOptionsRequest
    {
        public Guid SupplierId { get; set; }
        public string InvoiceSendAddress { get; set; }
        public DataModel.Xrm.account.InvoiceSendOption InvoiceSendOption { get; set; }
    }
}
