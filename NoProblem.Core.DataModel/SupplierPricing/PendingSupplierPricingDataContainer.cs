﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class PendingSupplierPricingDataContainer
    {
        public Guid PendingId { get; set; }
        public Guid SupplierId { get; set; }
        public string transactionId { get; set; }
        public decimal Total { get; set; }
    }
}
