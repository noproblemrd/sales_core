﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class PricingRowData
    {
        public DateTime CreatedOn { get; set; }
        public string Number { get; set; }
        public int Amount { get; set; }
        public int Total { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceNumberRefund { get; set; }
        public string InvoicePdfLink { get; set; }
        public Xrm.new_paymentmethod.ePaymentMethod PaymentMethod { get; set; }
        public Guid SupplierPricingId { get; set; }
    }
}
