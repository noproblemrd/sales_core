﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class TransactionLogKeyValuePair
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
