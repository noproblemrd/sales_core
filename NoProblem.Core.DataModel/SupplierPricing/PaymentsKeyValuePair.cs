﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class PaymentsKeyValuePair
    {
        public int FromPrice { get; set; }
        public int NumberOfPayments { get; set; }
    }
}
