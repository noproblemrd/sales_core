﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class ChargingDataForHeadingPaymentPage
    {
        public ChargingDataForHeadingPaymentPage()
        {
            HeadingsData = new List<PublisherPortal.GuidStringPair>();
        }

        public List<PublisherPortal.GuidStringPair> HeadingsData { get; set; }
        public int MinAmountForAutoRechargeMaxMonthly { get; set; }
        public int MinAmountForAutoRechargeLowBalance { get; set; }
        public bool HasCreditCardToken { get; set; }
        public string SupplierBizId { get; set; }
        public string AccountManagerBizId { get; set; }
        public Xrm.account.RechargeTypeCode? RechargeTypeCode { get; set; }
        public int RechargeAmount { get; set; }
        public int MonthlyPaymentBase { get; set; }
        public int MonthlyPaymentToPay { get; set; }
        public decimal Vat { get; set; }
        public bool CanChargeByFundsStatus { get; set; }
        public decimal Balance { get; set; }
        public DataModel.Xrm.account.DapazStatus DapazStatus { get; set; }
        public bool IsExemptOfMonthlyPayment { get; set; }
    }
}
