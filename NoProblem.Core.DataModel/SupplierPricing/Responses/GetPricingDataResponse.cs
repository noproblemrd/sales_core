﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class GetPricingDataResponse
    {
        public Guid SupplierPricingId { get; set; }
        public int Amount { get; set; }
        public string Number { get; set; }
        public int Bonus { get; set; }
        public Xrm.new_supplierpricing.BonusType BonusType { get; set; }
        public int ExtraBonus { get; set; }
        public Guid ExtraBonusReasonId { get; set; }
        public Xrm.new_paymentmethod.ePaymentMethod PaymentMethod { get; set; }
        public Guid VoucherReasonId { get; set; }
        public string VoucherNumber { get; set; }
        public string CreatedByUserName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankAccount { get; set; }
        public decimal SupplierBalance { get; set; }
        public int NumberOfPayments { get; set; }
        public string InvoicePdfLink { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceNumberRefund { get; set; }
        public DateTime? CheckDate { get; set; }
        public DateTime? TransferDate { get; set; }
        public string CheckNumber { get; set; }
    }
}
