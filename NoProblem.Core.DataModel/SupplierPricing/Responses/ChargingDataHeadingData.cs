﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class ChargingDataHeadingData
    {
        public Guid HeadingId { get; set; }
        public string HeadingName { get; set; }
        public bool AlreadyPaidThisMonth { get; set; }
        public decimal BaseAmount { get; set; }
        public decimal AmountToPay { get; set; }
    }
}
