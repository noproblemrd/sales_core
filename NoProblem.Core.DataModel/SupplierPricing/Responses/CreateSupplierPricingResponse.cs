﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class CreateSupplierPricingResponse
    {
        public Guid PricingId { get; set; }
        public eDepositStatus DepositStatus { get; set; }
        public string AccountNumber { get; set; }
        public string AccountManagerId { get; set; }
        public string ErrorDesc { get; set; }
    }

    public enum eDepositStatus
    {
        OK,
        Repeated,
        BadVoucherNumber,
        NotEnoughCashInVoucher,
        NotCompleted,
        InvalidInvoiceNumber,
        RefundBiggerThanBalance,
        ChargingWithExternalSystemFailed
    }
}
