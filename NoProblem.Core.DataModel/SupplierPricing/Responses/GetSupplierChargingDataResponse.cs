﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.DataModel.SupplierPricing
{
    public class GetSupplierChargingDataResponse
    {
        public string CreditCardToken { get; set; }
        public string CVV2 { get; set; }
        public string Last4DigitsCC { get; set; }
        public int LastPaymentAmount { get; set; }
        public Xrm.new_paymentmethod.ePaymentMethod LastPaymentMethod { get; set; }
        public int LastBonusAmount { get; set; }
        public string AddressCity { get; set; }
        public string AddressStreet { get; set; }
        public string AddressCounty { get; set; }
        public string AddressState { get; set; }
        public string AddressCountry { get; set; }
        public string AddressPostalCode { get; set; }
        public string AddressHouseNumber { get; set; }
        public List<PricingPackageData> Packages { get; set; }
        public decimal CurrentBalance { get; set; }
        public string Currency { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyId { get; set; }
        public int StageInRegistration { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string OtherPhone { get; set; }
        public string WebSite { get; set; }
        public string CreditCardOwnerId { get; set; }
        public string CreditCardOwnerName { get; set; }
        public decimal MinDepositPrice { get; set; }
        public bool IsAutoRenew { get; set; }
        public int? RechargeAmount { get; set; }
        public decimal MaxIncidentPrice { get; set; }
        public bool RechargeEnabled { get; set; }
        public int RechargeBonusPercent { get; set; }
        public string AccountNumber { get; set; }
        public DateTime TokenDate { get; set; }
        public bool HasCreditCardToken { get; set; }
        public Xrm.account.RechargeTypeCode RechargeTypeCode { get; set; }
        public int RechargeDayOfMonth { get; set; }
        public List<PaymentsKeyValuePair> PaymentsTable { get; set; }
        public string InvoiceSendAddress { get; set; }
        public Xrm.account.InvoiceSendOption InvoiceSendOption { get; set; }
        public bool InvoiceSendOptionsEnabled { get; set; }
        public string ChargingCompanyAccountNumber { get; set; }
        public string ChargingCompanyContractId { get; set; }
        public string CreditCardType { get; set; }
        public string UserAgent { get; set; }
        public string UserIP { get; set; }
        public string UserRemoteHost { get; set; }
        public string UserAcceptLanguage { get; set; }
        public string SupplierBizId { get; set; }
        public string AccountManagerBizId { get; set; }
        public decimal Vat { get; set; }
        public bool CanChargeByFundsStatus { get; set; }
    }
}
