﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class DeskAppLogEntryFactory
    {
        public static Xrm.new_deskapplogentry Create(DataModel.Consumer.DeskAppLogRequest deskAppLogRequest, Guid addonUserId)
        {
            Xrm.new_deskapplogentry entity = new Xrm.new_deskapplogentry();
            entity.new_type = deskAppLogRequest.Type;
            entity.new_operatingsystemname = deskAppLogRequest.OperatingSystemName;
            entity.new_operatingsystemedition = deskAppLogRequest.OperatingSystemEdition;
            entity.new_servicepack = deskAppLogRequest.ServicePack;
            entity.new_operatingsystemversion = deskAppLogRequest.OperatingSystemVersion;
            entity.new_operatingsystembites = deskAppLogRequest.OperatingSystemBites;
            entity.new_dotnetversions = deskAppLogRequest.DotNetVersions;
            entity.new_proxyenabled = deskAppLogRequest.ProxyEnabled;
            entity.new_proxyserver = deskAppLogRequest.ProxyServer;
            entity.new_proxysettingperuser = deskAppLogRequest.ProxySettingPerUser;
            entity.new_userid = deskAppLogRequest.UserId;
            entity.new_originid = deskAppLogRequest.OriginId.ToString();
            if (deskAppLogRequest.NotificationText != null)
            {
                string text = deskAppLogRequest.NotificationText;
                if (text.Length > 4000)
                {
                    text = text.Substring(0, 4000);
                }
                entity.new_notificationtext = text;
            }
            entity.new_configurationscriptelement = deskAppLogRequest.ConfigurationScriptElement;
            entity.new_configurationmodifiedon = deskAppLogRequest.ConfigurationModifiedOn;
            entity.new_configurationversion = deskAppLogRequest.ConfigurationVersion;
            entity.new_port = deskAppLogRequest.Port;
            entity.new_apptype = deskAppLogRequest.AppType;
            entity.new_addonuserid = addonUserId;
            return entity;
        }
    }
}
