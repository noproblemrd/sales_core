﻿using NoProblem.Core.DataModel.Origins.InjectionsCockpit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class OriginFactory
    {
        public static DataModel.Xrm.new_origin Build(
            OriginData data
            )
        {
            if (String.IsNullOrEmpty(data.Name))
                throw new ArgumentException("name of origin cannot be empty");
            if (!data.UseSameInUpsale.HasValue)
                throw new ArgumentException("UseSameInUpsale of origin is required");
            if (!data.IsInFinancialDashboard.HasValue)
                throw new ArgumentException("IsInFinancialDashboard of origin is required");
            if (!data.IncludeCampaign.HasValue)
                throw new ArgumentException("IncludeCampaign of origin is required");
            if (!data.ShowNoProblemInSlider.HasValue)
                throw new ArgumentException("ShowNoProblemInSlider of origin is required");
            if (!data.IsDistribution.HasValue)
                throw new ArgumentException("IsDistribution of origin is required");
            if (!data.IsApiSeller.HasValue)
                throw new ArgumentException("IsApiSeller of origin is required");
            Xrm.new_origin origin = new Xrm.new_origin();
            origin.new_name = data.Name;
            origin.new_usesameinupsale = data.UseSameInUpsale;
            origin.new_isinfinancialdashboard = data.IsInFinancialDashboard;
            origin.new_sliderbroughtbyname = data.SliderBroughtToYouByName;
            origin.new_includecampaign = data.IncludeCampaign;
            origin.new_shownoprobleminslider = data.ShowNoProblemInSlider;
            origin.new_isdistribution = data.IsDistribution;
            origin.new_isapiseller = data.IsApiSeller;
            origin.new_costperrequest = data.CostPerRequest;
            origin.new_costofinstallation = data.CostPerInstall;
            origin.new_revenueshare = data.RevenueShare;
            origin.new_adengineenabled = data.AdEngineEnabled;
            origin.new_sendinstallationdailyemail = (data.SendInstallationReport.HasValue) ? data.SendInstallationReport : false;
            origin.new_installationdailyemail = data.EmailToSendInstallationReport;
            origin.new_factor = data.Factor;
            origin.new_maxfactor = data.MaxFactor;
            origin.new_maxfactorsenddailyemail = data.SendMaxFactorReport;
            origin.new_maxfactordailyemail = data.EmailToSendMaxFactorReport;
            return origin;
        }
    }
}
