﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class MobileDeviceFactory
    {
        public static Xrm.new_mobiledevice Create(
            Guid supplierId, Guid customerId, string deviceName, string deviceUId, string deviceOS, string deviceOSVersion, string npAppVersion,
            bool isDebug)
        {
            Xrm.new_mobiledevice entity = new Xrm.new_mobiledevice();
            if (supplierId != Guid.Empty)
                entity.new_accountid = supplierId;
            if (customerId != Guid.Empty)
                entity.new_contactid = customerId;
            entity.new_uid = deviceUId;
            entity.new_name = deviceName;
            entity.new_os = GetOsConstant(deviceOS);
            entity.new_osversion = deviceOSVersion;
            entity.new_npappversion = npAppVersion;
            entity.new_token = Guid.NewGuid().ToString();
            entity.new_isdebug = isDebug;
            entity.new_isauthorized = true;
            entity.new_isactive = true;
            return entity;
        }

        private static string GetOsConstant(string deviceOS)
        {
            string name = deviceOS.ToUpper();
            if (name != Xrm.new_mobiledevice.MobileOSs.ANDROID
                && name != Xrm.new_mobiledevice.MobileOSs.IOS)
            {
                throw new ArgumentException("OS of mobile device must be ANDROID or IOS");
            }
            return name;
        }
    }
}
