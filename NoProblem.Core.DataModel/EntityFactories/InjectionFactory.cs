﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class InjectionFactory
    {
        public static DataModel.Xrm.new_injection Build(string name, string scriptBefore, string url, bool blockWithUs, string ScriptId)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentException("name of injection cannot be empty", name);
            if (String.IsNullOrEmpty(url))
                throw new ArgumentException("url of injection cannot be empty", url);
            Xrm.new_injection injection = new Xrm.new_injection();
            injection.new_url = url;
            injection.new_name = name;
            injection.new_scriptbefore = scriptBefore;
            injection.new_blockwithus = blockWithUs;
            injection.new_scriptelementid = ScriptId;
            return injection;
        }
    }
}
