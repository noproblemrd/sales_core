﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class CustomerTipAdveritserResponseFactory
    {
        public static DataModel.Xrm.new_customertipadvertiserresponse Build(Guid customerTipId, Guid supplierId, Guid incidentId, bool isPositive, bool isYelpSupplier)
        {
            DataModel.Xrm.new_customertipadvertiserresponse entity =
                new Xrm.new_customertipadvertiserresponse();
            if (isYelpSupplier)
                entity.new_yelpsupplierid = supplierId;
            else
                entity.new_accountid = supplierId;
            entity.new_customertipid = customerTipId;
            entity.new_incidentid = incidentId;
            entity.new_positivefeedback = isPositive;
            return entity;
        }
    }
}
