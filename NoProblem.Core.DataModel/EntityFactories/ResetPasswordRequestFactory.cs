﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class ResetPasswordRequestFactory
    {
        public static DataModel.Xrm.new_resetpasswordrequest Create(Guid supplierId)
        {
            DataModel.Xrm.new_resetpasswordrequest entity = new Xrm.new_resetpasswordrequest();
            entity.new_accountid = supplierId;
            entity.new_isused = false;
            entity.new_token = Guid.NewGuid().ToString();
            return entity;
        }
    }
}
