﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class PingPhoneApiEntryFactory
    {
        public static Xrm.new_pingphoneapientry Create(bool interested, string leadId, Guid categoryId, Guid regionId, Guid originId, Guid supplierId, decimal price, bool isTest, string testProviderPhone, bool isError)
        {
            Xrm.new_pingphoneapientry entity = new Xrm.new_pingphoneapientry();
            if (isError)
            {
                entity.new_status = (int)Xrm.new_pingphoneapientry.Status.Ping1Error;
            }
            else if (interested)
            {
                entity.new_status = (int)Xrm.new_pingphoneapientry.Status.Ping1Accepted;
            }
            else
            {
                entity.new_status = (int)Xrm.new_pingphoneapientry.Status.Ping1NotInterested;
            }
            entity.new_leadid = leadId;
            if (categoryId != Guid.Empty)
                entity.new_primaryexpertiseid = categoryId;
            if (regionId != Guid.Empty)
                entity.new_regionid = regionId;
            entity.new_originid = originId;
            if (supplierId != Guid.Empty)
                entity.new_accountid = supplierId;
            entity.new_price = price;
            entity.new_istest = isTest;
            entity.new_testproviderphone = testProviderPhone;
            return entity;
        }
        public static Xrm.new_pingphoneapientry Create(bool interested, string leadId, Guid categoryId, Guid regionId, Guid originId, Guid supplierId, 
            decimal price, bool isTest, string testProviderPhone, bool isError, string callerId, bool isTrueCallerId)
        {
            Xrm.new_pingphoneapientry entity = new Xrm.new_pingphoneapientry();
            if (isError)
            {
                entity.new_status = (int)Xrm.new_pingphoneapientry.Status.Ping1Error;
            }
            else if (interested)
            {
                entity.new_status = (int)Xrm.new_pingphoneapientry.Status.Ping1Accepted;
            }
            else
            {
                entity.new_status = (int)Xrm.new_pingphoneapientry.Status.Ping1NotInterested;
            }
            entity.new_leadid = leadId;
            if (categoryId != Guid.Empty)
                entity.new_primaryexpertiseid = categoryId;
            if (regionId != Guid.Empty)
                entity.new_regionid = regionId;
            entity.new_originid = originId;
            if (supplierId != Guid.Empty)
                entity.new_accountid = supplierId;
            entity.new_price = price;
            entity.new_istest = isTest;
            entity.new_testproviderphone = testProviderPhone;
            if(!string.IsNullOrEmpty(callerId))
            {
                entity.new_callerid = callerId;
                entity.new_iscalleridreal = isTrueCallerId;
            }
            return entity;
        }
    }
}
