﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class MobileAppFeddbackFactory
    {
        public static DataModel.Xrm.new_mobileappfeedback Build(Guid supplierId, string comment, string header)
        {
            DataModel.Xrm.new_mobileappfeedback entity =
                new Xrm.new_mobileappfeedback();
            entity.new_accountid = supplierId;
            entity.new_comment = comment;
            entity.new_header = header;
            return entity;
        }
    }
}
