﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class UserInstalledProgramFactory
    {
        public static Xrm.new_userinstalledprogram Create(string programName, Guid deskAppLogEntryId)
        {
            Xrm.new_userinstalledprogram entity = new Xrm.new_userinstalledprogram();
            entity.new_deskapplogentryid = deskAppLogEntryId;
            if (programName.Length > 300)
                programName = programName.Substring(0, 300);
            entity.new_name = programName;
            return entity;
        }
    }
}
