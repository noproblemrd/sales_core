﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class AddOnMacAddressFactory
    {
        public static Xrm.new_addonmacaddress Create(string name, Guid addonUserId)
        {
            Xrm.new_addonmacaddress entity = new Xrm.new_addonmacaddress();
            entity.new_name = name;
            entity.new_addonuserid = addonUserId;
            return entity;
        }
    }
}
