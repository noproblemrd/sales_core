﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class InjectionReportEntityFactory
    {
        public static Xrm.new_injectionreport Create(Guid? originId, Guid injectionId, decimal revenue, DateTime date)
        {
            Xrm.new_injectionreport entity = new Xrm.new_injectionreport();
            entity.new_injectionid = injectionId;
            entity.new_revenue = revenue;
            entity.new_date = date.Date;
            if (originId.HasValue && originId.Value != Guid.Empty)
            {
                entity.new_originid = originId;
            }
            return entity;
        }
    }
}
