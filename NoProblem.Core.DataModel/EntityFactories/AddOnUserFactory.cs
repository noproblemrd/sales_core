﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.EntityFactories
{
    public class AddOnUserFactory
    {
        public static Xrm.new_addonuser Create(bool isInstalled, Guid originId, string appType)
        {
            Xrm.new_addonuser entity = new Xrm.new_addonuser();
            entity.new_isinstalled = isInstalled;
            entity.new_originid = originId;
            entity.new_apptype = appType;
            return entity;
        }
    }
}
