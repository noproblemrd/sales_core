﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.UsersSync
{
    public class UserContainer
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Email { get; set; }
        public int SecurityLevel { get; set; }
    }
}
