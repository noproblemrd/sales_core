﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.UsersSync
{
    public class CurrentUserContainer
    {
        public Guid Id { get; set; }
        public string UserBizId { get; set; }
        public bool IsActive { get; set; }
    }
}
