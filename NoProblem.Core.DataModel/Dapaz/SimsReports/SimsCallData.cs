﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.SimsReports
{
    public class SimsCallData
    {
        public Guid RequestId { get; set; }
        public DateTime? AdvertiserCallStart { get; set; }
        public DateTime? AdvertiserCallEnd { get; set; }
        public string CallCLINo { get; set; }
        public string AdvertiserPhone { get; set; }
        public DateTime EntryCreatedOn { get; set; }
        public string CategoryCode { get; set; }
        public string Media { get; set; }
        public string VirtualNumber { get; set; }
        public string CallStatus { get; set; }
        public string AccountNumberZap { get; set; }
        public string SimsPhoneLocal { get; set; }
        public string CallId { get; set; }

        public class CallStatusOptions
        {
            public const string ANSWER = "ANSWER";
            public const string NO_ANSWER = "NO_ANSWER";
            public const string NOT_ACTIVE = "NOT_ACTIVE";
            public const string OUT_OF_TIME = "OUT_OF_TIME";
            public const string WRONG_NUMBER = "WRONG_NUMBER";
            public const string CALLER_CANCEL = "CALLER_CANCEL";
            public const string BUSY = "BUSY";
            public const string BLACK_LIST = "BLACK_LIST";
        }
    }
}
