﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.DapazAccountsOnlineSync
{
    public class KeyForAccountOnlineSync_Zap
    {
        public string AccountMedia { get; set; }
        public string AccountSegment { get; set; }
        public string AccountKeyStatus { get; set; }
        public List<string> AccountSubLocals { get; set; }

        public static class AccountSubLocals_KeyForAccountOnlineSync_Zap_options
        {
            public const string PPA = "PPA";
            public const string FIX = "FIX";
            public const string RARAH = "RARAH";
        }
    }
}
