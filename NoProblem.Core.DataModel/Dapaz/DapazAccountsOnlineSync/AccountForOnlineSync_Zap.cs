﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.DapazAccountsOnlineSync
{
    public class AccountForOnlineSync_Zap
    {
        public string CustomerNo { get; set; }
        public string AccountName { get; set; }
        public bool IsActive { get; set; }
        public string BusinessId { get; set; }
        public string AccountInvoiceName { get; set; }
        public string AccountPrimaryCityName { get; set; }
        public string AccountPrimaryStreetName { get; set; }
        public string AccountPrimaryHouseNum { get; set; }
        public string AccountPrimaryZipcode { get; set; }
        public string Email { get; set; }
        public string AccountContactFirstName { get; set; }
        public string AccountContactLastName { get; set; }
        public string AccountPrimaryLocal { get; set; }
        public string PhoneNumberRelatedForSims { get; set; }
        public string PhoneNumberRelatedForSMS { get; set; }
        public string AccountFaxNum { get; set; }
        public string AccountPhoneNum2 { get; set; }
        public string AccountInternetSite { get; set; }
        public string FinanceStatus { get; set; }
        public List<KeyForAccountOnlineSync_Zap> ListOfNoProblemKeys { get; set; }
        public string PrimaryHeadingCode { get; set; }
        public string PrimaryLocalAreaCode { get; set; }
    }
}
