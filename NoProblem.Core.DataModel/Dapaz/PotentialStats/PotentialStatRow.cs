﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.PotentialStats
{
    public class PotentialStatRow
    {
        public string CutomerNo { get; set; }
        public string SimsPhoneNumber { get; set; }
        public string CustomerStatus { get; set; }
        public string SimsMedia { get; set; }
        public string SimsPhoneSegment { get; set; }
        public string SimsPhoneLocal { get; set; }
        public DateTime SimsStartDate { get; set; }
        public int? SimsLimit { get; set; }
        public DateTime? SimsLimitStartDate { get; set; }
        public DateTime? SimsLimitEndDate { get; set; }

        public static class CustomerStatuses
        {
            public const string PPA = "PPA";
            public const string Gold = "Gold";
            public const string RarahQuote = "RarahQuote";
        }
    }
}
