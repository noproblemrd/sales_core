﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.PpaSync
{
    public class PpaSyncData
    {
        public List<PpaSyncCategoryRegionData> CategoryRegionList { get; set; }

        public PpaSyncData()
        {
            CategoryRegionList = new List<PpaSyncCategoryRegionData>();
        }
    }
}
