﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.PpaSync
{
    public class PpaSyncAccountData
    {
        public string AccountNumberDapaz { get; set; }
        public string AccountNumberNP { get; set; }
        public int OrderPriority { get; set; }
        public string VirtualPhoneNumber { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }

        public override bool Equals(object obj)
        {
            PpaSyncAccountData other = (PpaSyncAccountData)obj;
            return other.AccountNumberDapaz == this.AccountNumberDapaz;
        }

        public override int GetHashCode()
        {
            return AccountNumberDapaz.GetHashCode();
        }
    }
}
