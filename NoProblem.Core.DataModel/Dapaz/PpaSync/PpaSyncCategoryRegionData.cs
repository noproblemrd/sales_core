﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.PpaSync
{
    public class PpaSyncCategoryRegionData
    {
        public string RegionCode { get; set; }
        public string CategoryCode { get; set; }
        public List<PpaSyncAccountData> AccountList { get; set; }

        public PpaSyncCategoryRegionData()
        {
            AccountList = new List<PpaSyncAccountData>();
        }
    }
}
