﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace NoProblem.Core.DataModel.Dapaz.PpaOnlineSync
{
    public class SyncObject
    {
        [Newtonsoft.Json.JsonProperty("customerData")]
        public AccountSyncData CustomerData { get; set; }
        [Newtonsoft.Json.JsonProperty("password")]
        public string Password { get; set; }

        public SyncObject(string password)
        {
            CustomerData = new DataModel.Dapaz.PpaOnlineSync.AccountSyncData();
            Password = password;
        }
    }

    public class AccountSyncData
    {
        public string AccountNumberDapaz { get; set; }
        public string AccountNumberNP { get; set; }
        public int CountCalls { get; set; }
        public IEnumerable<CategoryData> Categories { get; set; }
        public IEnumerable<MekomonData> Mekomonim { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string LogoUrl { get; set; }
        public string SundayFrom { get; set; }
        public string SundayTo { get; set; }
        public string MondayFrom { get; set; }
        public string MondayTo { get; set; }
        public string TuesdayFrom { get; set; }
        public string TuesdayTo { get; set; }
        public string WednesdayFrom { get; set; }
        public string WednesdayTo { get; set; }
        public string ThursdayFrom { get; set; }
        public string ThursdayTo { get; set; }
        public string FridayFrom { get; set; }
        public string FridayTo { get; set; }
        public string SaturdayFrom { get; set; }
        public string SaturdayTo { get; set; }

        [JsonConverter(typeof(JSONCustomDateConverter))]
        public DateTime? UnavailableFrom { get; set; }

        [JsonConverter(typeof(JSONCustomDateConverter))]
        public DateTime? UnavailableTo { get; set; }

        public bool IsActive { get; set; }

        public class CategoryData
        {
            public string Code { get; set; }
            public bool IsActive { get; set; }
            public decimal CallPrice { get; set; }
            public IEnumerable<VirtualNumberData> VirtualNumbers { get; set; }
        }

        public class VirtualNumberData
        {
            public string OriginCode { get; set; }
            public string Number { get; set; }
        }

        public class MekomonData
        {
            public string MekomonId { get; set; }
            public bool IsActive { get; set; }
        }
    }
}
