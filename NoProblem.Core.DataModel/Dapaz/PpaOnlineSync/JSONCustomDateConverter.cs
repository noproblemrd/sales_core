﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.PpaOnlineSync
{
    class JSONCustomDateConverter : Newtonsoft.Json.Converters.DateTimeConverterBase
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime?);
        }

        public override bool CanRead
        {
            get
            {
                return false;
            }
        }

        public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
        {
            DateTime? date = (DateTime?)value;
            if (date.HasValue)
            {
                writer.WriteValue(date.Value.ToString("yyyy-MM-dd"));
            }
            else
            {
                writer.WriteValue(date);
            }
            writer.Flush();
        }
    }
}
