﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers
{
    public class CreateGoldenNumberAdvertiserRequest : Audit.AuditRequest
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string SmsNumber { get; set; }
        public string VirtualNumber { get; set; }
        public string BizId { get; set; }
        public bool IsActive { get; set; }
        public bool DoNotSendSMS { get; set; }
        public string ExpertiseName { get; set; }
        public string Region { get; set; }
        public eTypeOfGolden TypeOfGolden { get; set; }
        public Xrm.account.FundsStatus FundsStatus { get; set; }

        public enum eTypeOfGolden
        {
            Fix, Rarah
        }
    }
}
