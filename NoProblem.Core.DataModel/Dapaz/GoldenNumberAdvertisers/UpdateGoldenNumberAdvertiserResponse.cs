﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers
{
    public class UpdateGoldenNumberAdvertiserResponse
    {
        public UpdateGoldenNumberAdvertiserResponse()
        {
            this.ResponseStatus = eUpdateGoldenNumberAdvertiserStatus.OK;
        }

        public eUpdateGoldenNumberAdvertiserStatus ResponseStatus { get; set; }

        public enum eUpdateGoldenNumberAdvertiserStatus
        {
            OK,
            SupplierDoesNotExist,
            SupplierIsNotDapazGoldenNumberAdvertiser,
            VirtualNumberNotAvailable
        }
    }
}
