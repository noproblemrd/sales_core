﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers
{
    public class CreateGoldenNumberAdvertiserResponse
    {
        public CreateGoldenNumberAdvertiserResponse()
        {
            this.ResponseStatus = eCreateGoldenNumberAdvertiserStatus.OK;
        }

        public eCreateGoldenNumberAdvertiserStatus ResponseStatus { get; set; }
        public Guid SupplierId { get; set; }

        public enum eCreateGoldenNumberAdvertiserStatus
        {
            OK,
            VirtualNumberNotAvailable
        }
    }
}
