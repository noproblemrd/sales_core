﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers
{
    public class UpdateGoldenNumberAdvertiserRequest : CreateGoldenNumberAdvertiserRequest
    {
        public Guid SupplierId { get; set; }
        public DapazStatusChangeOptions? NewDapazStatus { get; set; }
    }
}
