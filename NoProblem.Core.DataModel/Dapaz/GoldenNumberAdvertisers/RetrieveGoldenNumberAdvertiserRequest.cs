﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers
{
    public class RetrieveGoldenNumberAdvertiserRequest
    {
        public Guid SupplierId { get; set; }
    }
}
