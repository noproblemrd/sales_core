﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers
{
    public class GetAvailableVirtualNumbersResponse
    {
        public List<string> NumbersList { get; set; }
    }
}
