﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers
{
    public class RetrieveGoldenNumberAdvertiserResponse : UpdateGoldenNumberAdvertiserRequest
    {
        public RetrieveGoldenNumberAdvertiserResponse()
        {
            this.ResponseStatus = eRetrieveGoldenNumberAdvertiserStatus.OK;
        }

        public eRetrieveGoldenNumberAdvertiserStatus ResponseStatus { get; set; }
        public Xrm.account.DapazStatus DapazStatus { get; set; }

        public enum eRetrieveGoldenNumberAdvertiserStatus
        {
            OK,
            SupplierDoesNotExist,
            SupplierIsNotDapazGoldenNumberAdvertiser
        }
    }
}
