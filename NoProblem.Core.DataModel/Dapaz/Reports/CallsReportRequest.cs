﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.Reports
{
    public class DapazCallsReportRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool? CallStatus { get; set; }
        public Guid SupplierId { get; set; }
        public eDapazCallsReportGroupBy? GroupBy { get; set; }
        public DataModel.Xrm.account.DapazStatus? DapazStatus { get; set; }

        public enum eDapazCallsReportGroupBy
        {
            Hour, Day, Month
        }
    }
}
