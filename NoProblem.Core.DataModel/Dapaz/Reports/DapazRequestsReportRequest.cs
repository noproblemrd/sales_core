﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.Reports
{
    public class DapazRequestsReportRequest
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Guid HeadingId { get; set; }
        public string CaseNumber { get; set; }
        public DataModel.Xrm.incident.CaseTypeCode? CaseTypeCode { get; set; }
        public Guid OriginId { get; set; }
    }
}
