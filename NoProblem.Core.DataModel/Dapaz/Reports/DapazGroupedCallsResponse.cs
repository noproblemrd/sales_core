﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.Reports
{
    public class DapazGroupedCallsResponse
    {
        public List<DapazGroupedCallsContainer> DataList { get; set; }
        public int TotalMinutes { get; set; }
        public int TotalCallers { get; set; }
        public int AverageDurationInSeconds { get; set; }
    }
}
