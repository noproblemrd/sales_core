﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.Reports
{
    public class DapazGroupByCallerIdContainer
    {
        public string CallerId { get; set; }
        public int Count { get; set; }
    }
}
