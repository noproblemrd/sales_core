﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.Reports
{
    public class DapazRequestsReportResponse : List<NoProblem.Core.DataModel.Dapaz.Reports.DapazRequestsReportResponse.DapazRequestsReportRow>
    {
        public class DapazRequestsReportRow
        {
            public DateTime Date { get; set; }
            public string CaseNumber { get; set; }
            public string HeadingName { get; set; }
            public string ConsumerPhone { get; set; }
            public int RequestsAdvertisers { get; set; }
            public int ProvidedAdvertisers { get; set; }
            public decimal Revenue { get; set; }
            public string RegionName { get; set; }
            public string Status { get; set; }
            public bool IsRerouting { get; set; }
            public string OriginName { get; set; }
            public string CaseTypeCodeName { get; set; }
            public string BizId { get; set; }
            public string Name { get; set; }
            public bool IsDollar { get; set; }
            public string RechargeTypeName { get; set; }
            public bool IsPPA { get; set; }
        }
    }
}
