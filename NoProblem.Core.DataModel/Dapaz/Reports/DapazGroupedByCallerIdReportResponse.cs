﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.Reports
{
    public class DapazGroupedByCallerIdReportResponse
    {
        public List<DapazGroupByCallerIdContainer> DataList { get; set; }
        public int TotalMinutes { get; set; }
        public int TotalCallers { get; set; }
        public int AverageDurationInSeconds { get; set; }
    }
}
