﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.Reports
{
    public class DapazGroupedCallsContainer
    {
        public DateTime Time { get; set; }
        public int NumberOfCallers { get; set; }
        public int CallsTimeInSeconds { get; set; }
        public int AverageDurationInSeconds { get; set; }
    }
}
