﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.Reports
{
    public class DapazReportsCallContainer
    {
        public string BizId { get; set; }
        public string SupplierName { get; set; }
        public Guid SupplierId { get; set; }
        public DateTime CallDateTime { get; set; }
        public string CallerId { get; set; }
        public string VirtualNumber { get; set; }
        public string TargerNumber { get; set; }
        public int CallDurationInSeconds { get; set; }
        public string CallStatus { get; set; }
        public string RecordFileLocation { get; set; }
    }
}
