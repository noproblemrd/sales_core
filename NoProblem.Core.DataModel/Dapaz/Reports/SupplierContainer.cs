﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.Reports
{
    public class DapazReportsSupplierContainer
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public string BizId { get; set; }
    }
}
