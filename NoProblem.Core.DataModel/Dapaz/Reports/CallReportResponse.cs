﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Dapaz.Reports
{
    public class DapazCallReportResponse
    {
        public int AverageCallTimeInSeconds { get; set; }
        public int NumberOfCallers { get; set; }
        public int NumberOfMinutes { get; set; }
        public List<DapazReportsCallContainer> CallsList { get; set; }
    }
}
