﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.DataModel
{
    public class XrmDataContext
    {
        private const string xrmConnectionStringName = "XrmDataContext";

        public static DataContext Create()
        {
            DataContext context = new DataContext(xrmConnectionStringName);
            return context;
        }

        public static void ClearCache(string entityName)
        {
            const string format = "adxdependency:crm:entity:{0}";
            var dependency = string.Format(format, entityName).ToLower();

            var cache = Microsoft.Xrm.Client.Caching.CacheManager.GetBaseCache();
            cache.Remove(dependency);
        }
    }
}
