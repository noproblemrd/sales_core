﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_divorceattorneycasedata
    {
        public enum TermsOfDivorce
        {
            WeAgreeOnSomeButNotAll = 1,
            WeAgreeOnAllTerms,
            WeDoNotAgree
        }

        public enum DivorceHowSoon
        {
            Immediately = 1,
            OneToSixMonths,
            SixMonthsOrLater,
            WeDoNotWantToFile
        }

        public enum DivorceFinancing
        {
            IWillDiscussPaymentOptionsWithMyAttorney = 1,
            CreditCardOrPersonalLoan,
            PersonalSavings,
            FamilySupport,
            ICannotAffordLegalFees
        }

        public enum DivorceIncome
        {
            LessThan10K = 1,
            TenKTo30K,
            ThirtyKTo60K,
            SixtyKTo100K,
            MoreThan100K
        }

        public enum DivorceProperty
        {
            Other = 1,
            HouseOrCondo,
            Vehicles,
            VacationProperty,
            Jewelry,
            Pension,
            StocksOrBonds,
            IRA
        }

        public enum DivorceChildren
        {
            Two = 1,
            Zero,
            One,
            Three,
            Four,
            Five,
            MoreThan5
        }
    }
}
