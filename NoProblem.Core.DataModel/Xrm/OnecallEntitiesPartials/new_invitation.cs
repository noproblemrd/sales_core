﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_invitation
    {
        public enum InvitationStatus
        {
            Pending = 1,
            Linked = 3,
            Success = 4,
            Lost = 5
        }
    }
}
