﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_nonppcrequest
    {
        public enum Status
        {
            New = 1,
            Calling = 3,
            Close = 5,
            SMS = 6,
            Error = 7,
            Lost = 8,
            ConsumerDidntAnswer = 9,
            CallInProgress = 10,
            AdvAnswered = 11
        }
    }
}
