﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_personalinjurycasedata
    {
        public enum AnyoneHasInsurance
        {
            Yes = 1, No, DontKnow
        }

        public enum AccidentResult
        {
            hospitalization = 1,
            medicalTreatment,
            surgery,
            missedWork,
            wrongfulDeath,
            noneOfTheAbove
        }

        public enum EstimatedMedicalBills
        {
            lessThan1k = 1,
            e1kTo5k,
            e5kTo10k,
            e10kTo25k,
            e25kTo100k,
            moreThan100k,
            dontKnow,
            noMedicalBills
        }
    }
}
