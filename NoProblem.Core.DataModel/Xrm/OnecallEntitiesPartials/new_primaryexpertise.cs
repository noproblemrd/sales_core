﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_primaryexpertise
    {
        public enum eCampaignMode
        {
            FormSlider = 1,
            PhoneDidSlider = 2,
            BannerCampaign = 3,
            FormAndDid = 4
        }

        public enum ePostSubmissionLandingPage
        {
            NpDestination = 1,
            BannerCampaignsLandingPage = 2
        }

        public enum ePhoneDidOtherTitleOptions
        {
            SameAsInFormSlider = 1,
            Other = 2
        }

        public enum ePhoneDidOtherSecondTitleOptions
        {
            UseFieldsValuesIvrSingular = 1,
            Other = 2
        }
        public enum eGoogleCategoryType
        {
            accounting	= 1,
            car_dealer	= 2,
            car_rental	= 3,
            car_repair = 4,
            dentist	= 5,
            doctor	= 6,
            electrician	= 7,
            florist	= 8,
            general_contractor	= 9,
            health	= 10,
            insurance_agency	= 11,
            lawyer	= 12,
            locksmith	= 13,
            moving_company	= 14,
            painter	= 15,
            physiotherapist	= 16,
            plumber	= 17,
            real_estate_agency	= 18,
            roofing_contractor	= 19,
            storage	= 20,
            veterinary_care	= 21
            
        }
    }
}
