﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_unavailabilityreasongroup
    {
        public enum Code
        {
            OutOfMoney = 1,
            CustomerService =2,
            TemporaryUnavailability = 3
        }
    }
}
