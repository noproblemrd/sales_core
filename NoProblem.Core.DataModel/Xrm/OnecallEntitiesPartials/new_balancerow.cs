﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_balancerow
    {
        public enum Action
        {
            Deposit = 1,
            Call = 2,
            Refund = 3,
            Interval = 4,
            AutoRecharge = 5,
            CreditEarned = 6,
            BonusCredit = 7,
            TenLeadsBonus = 8,
            MonthlyFee = 9,
            MonthlyFeeRefund = 10
        }
    }
}
