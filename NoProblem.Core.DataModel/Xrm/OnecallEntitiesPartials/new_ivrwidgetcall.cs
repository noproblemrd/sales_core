﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_ivrwidgetcall
    {
        public enum ProcessStatus
        {
            NoNumberFound = 1,
            InvalidPhone = 2,
            TooManyInvalidPhones = 3,
            PhoneValid = 4,
            PhoneConfirmed = 5,
            TooManyNotConfirmedPhone = 6,
            InvalidArea = 7,
            TooManyInvalidAreas = 8,
            AreaValid = 9,
            AreaConfirmed = 10,
            TooManyNotConfirmedArea = 11
        }
    }
}
