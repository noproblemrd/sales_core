﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_pricingmethod
    {
        public enum PricingMethodCode
        {
            Fix = 1,
            PPC = 2,
            IncidentBank = 3,
            CashBank = 4,
            Credit = 5
        }
    }
}
