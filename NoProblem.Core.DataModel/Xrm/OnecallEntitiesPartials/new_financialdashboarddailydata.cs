﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_financialdashboarddailydata
    {
        public enum Type
        {
            AllInjectionsAndAllAndAll = 1,
            AllInjectionsAndHeadingAndAll = 2,
            DistributionAndAllAndAll = 3,
            DistributionAndHeadingAndAll = 4,
            AllDistributionsAndAllAndAll = 5,
            AllDistributionsAndHeadingAndAll = 6,
            AllInjectionsAndAllAndFlavor = 7,
            AllInjectionsAndHeadingAndFlavor = 8,
            DistributionAndAllAndFlavor = 9,
            DistributionAndHeadingAndFlavor = 10,
            AllDistributionsAndAllAndFlavor = 11,
            AllDistributionsAndHeadingAndFlavor = 12,
            InjectionAndAllAndAll = 13,
            InjectionAndHeadingAndAll = 14,
            InjectionAndAllAndFlavor = 15,
            InjectionAndHeadingAndFlavor = 16
        }
    }
}
