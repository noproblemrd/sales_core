﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_wibiyacashout
    {
        public enum WibiyaCashOutStatusCode
        {
            Payed = 1,
            Cancelled = 3,
            WaitingApproval = 4,
            Approved = 5
        }
    }
}
