﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_homesecuritycasedata
    {
        public enum eHomeSecurityTimeFrame
        {
            now = 1,
            one_two_weeks,
            two_four_weeks,
            two_three_months,
            over_three_months,
            unknown
        }

        public enum eHomeSecurityPropertyType
        {
            Business = 1,
            Residence
        }

        public enum eHomeSecurityCreditRanking
        {
            Excellent = 1, Fair, Good, Poor
        }
    }
}
