﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_bankruptcyattorneycasedata
    {
        public enum ConsideringBankruptcy
        {
            Other = 1,
            Garnishment,
            CreditorHarassment,
            Repossession,
            Foreclosure,
            Lawsuits,
            Illness,
            Disability,
            LicenseSuspension,
            Divorce,
            LossOfIncome
        }

        public enum BillsBankruptcy
        {
            Other = 1,
            CreditCards,
            StoreCards,
            PersonalLoans,
            ChildSupport,
            StudentLoans,
            AutoLoansIncomeTaxes,
            PaydayLoans,
            MedicalBills
        }

        public enum MoneyEstimateBankruptcy
        {
            LessThan3K = 1,
            ThreeKTo6K,
            SixKTo10K,
            TenKTo15K,
            MoreThan15K
        }

        public enum TypesOfIncomeBankruptcy
        {
            Other = 1,
            EmployedFullTime,
            EmployedPartTime,
            SocialSecurity,
            Pension,
            RetirementChildSupport,
            Maintenance,
            NoIncome
        }
    }
}
