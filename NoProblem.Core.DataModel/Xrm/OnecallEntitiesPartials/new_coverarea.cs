﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_coverarea 
    {
        public override bool Equals(object obj)
        {
            if (!(obj is new_coverarea))
                return false;
            new_coverarea other = (new_coverarea)obj;
            return
                this.new_fulladdress == other.new_fulladdress
                && this.new_latitude == other.new_latitude
                && this.new_longitude == other.new_longitude
                && this.new_radius == other.new_radius
                && this.new_order == other.new_order;
        }
    }
}
