﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_duiattorneycasedata
    {
        public enum StatusOfTheDui
        {
            GotDuiButNoCourtDate = 1,
            GotDuiAndHaveCourtDate,
            WantPastDuiRemoved
        }

        public enum AreYouRepresented
        {
            No = 1,
            Yes,
            YesButLookingForNewRepresentation
        }

        public enum Financing
        {
            Borrowing = 1,
            PersonalSavings,
            FamilySupport,
            CurrentIncome,
            WillDiscussWithAttorney,
            CannotAfford
        }
    }
}
