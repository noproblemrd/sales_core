﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_ivrtype
    {
        public enum IvrTypeCode
        {
            InTrial = 1,
            InTrialLast = 2,
            Expired = 3,
            Accessible = 4,
            AnsweringMachineIdentified = 5,
            AnsweringMachineUnknown = 6
        }
    }
}
