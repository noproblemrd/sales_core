﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_exposure
    {
        public enum ExposureType
        {
            Listing = 1,
            Widget = 2
        }

        public enum WelcomeScreenEventType
        {
            Opened = 1,
            ClickedYes = 2,
            ClickedNo = 3,
            ClickedCloseEnable = 4
        }
    }
}
