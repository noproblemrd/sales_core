﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_helpdesktype
    {
        public enum RegardingObjectType
        {
            Call = 1,
            Case = 2
        }

        public enum InitiatorType
        {
            Consumer = 1,
            Supplier = 2
        }
    }
}
