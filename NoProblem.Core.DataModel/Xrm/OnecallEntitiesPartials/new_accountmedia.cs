﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_accountmedia
    {
        public static class Types
        {
            public const string PHOTO = "PHOTO";
            public const string VIDEO = "VIDEO";
        }

        public static class Source
        {
            public const string FACEBOOK = "FACEBOOK";
            public const string GOOGLE = "GOOGLE";
            public const string YOUTUBE = "YOUTUBE";
            public const string LOCAL = "LOCAL";
        }
    }
}
