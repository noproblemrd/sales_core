﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class incident
    {
         [System.Xml.Serialization.XmlType(Namespace = "http://tempuri.org/", TypeName = "IncidentStatus")]
        public enum Status
        {
            WORK_DONE = 1,
            WAITING = 2,
            MISSING_DETAILS = 3,
            INVITED = 4,
            NEW = 200000,
            ONYX_HISTORY = 200001,
            NO_AVAILABLE_SUPPLIER = 200002,
            CUSTOMER_NOT_AVAILABLE = 200003,
            SUPPLIER_NOT_FOUND_IN_EXPERTISE = 200004,
            SUPPLIER_NOT_FOUND_IN_SERVICEAREA = 200005,
            SURVEY_DONE = 200006,
            CUSTOMER_NOT_AVAILABLE_DIALER = 200007,
            BLACKLIST = 200013,
            BADWORD = 200014,
            InvalidConsumerPhone = 200015,
            DUPLICATE = 200016,
            OnAar = 200017,
            Scraping = 200018,
        //    CLOSED_BY_CUSTOMER = 200019,
       //     RELAUNCH_BY_CUSTOMER = 200020,
            AFTER_AUCTION = 200022,
            IN_REVIEW = 200023,
            WATING_FOR_PHONE_VALIDATION = 200024,
             PRIVATE = 200025
        }
        public enum IncidentReviewStatus
        {
            WaitnigForReview = 1,
            Passed = 2,
            Failed = 3
        }

        public bool IsOpen
        {
            get
            {
                if (this.statuscode.HasValue == false)
                    return false;

                var incidentStatus = (Status) this.statuscode.Value;
                return incidentStatus == Status.NEW ||
                       incidentStatus == Status.WAITING ||
                       incidentStatus == Status.AFTER_AUCTION ||
                       incidentStatus == Status.IN_REVIEW;

            }
        }
       
        public bool IsInvalid
        {
            get
            {
                if (this.statuscode.HasValue == false)
                    return true;

                var incidentStatus = (Status)this.statuscode.Value;
                return incidentStatus == Status.BADWORD ||
                       incidentStatus == Status.BLACKLIST ||
                       incidentStatus == Status.DUPLICATE ||
                       incidentStatus == Status.MISSING_DETAILS;
            }
        }
        
        public bool IsClosed
        {
            get { return !this.IsInvalid && !this.IsOpen; }
               
        }
       
       
        public enum CaseTypeCode
        {
            SupplierSpecified = 1,
            Request = 3,
            ScheduledRequest = 200000,
            DirectNumber = 200001,
            PingPhone = 200002,
            PingPost = 200003,
            Video = 200004,
            VideoChat = 200005
        }

        public enum DialerStatus
        {
            REQUEST_NOT_DELIVERED = 1,
            DELIVERED_REQUEST = 2,
            REQUEST_CANCELED = 3,
            FAILED = 4,
            REQUEST_COMPLETED = 5,
            ERROR = 6,
            WAITING_FOR_UPDATE = 7,
            AUCTION_STARTED = 8,
            RELAUNCH_REQUEST = 9
        }
        #region static method
        public static bool Is_Closed(int _status)
        {
            var incidentStatus = (Status)_status;
           // return !Is_Invalid(_status) && !Is_Open(_status);
            return Is_Closed(incidentStatus);
        }
        public static bool Is_Closed(Status incidentStatus)
        {
            return !Is_Invalid(incidentStatus) && !Is_Open(incidentStatus);

        }
        public static bool Is_Invalid(int _status)
        {
            var incidentStatus = (Status)_status;
            /*
            return incidentStatus == Status.BADWORD ||
                   incidentStatus == Status.BLACKLIST ||
                   incidentStatus == Status.DUPLICATE ||
                   incidentStatus == Status.MISSING_DETAILS;
             */
            return Is_Invalid(incidentStatus);
        }
        public static bool Is_Invalid(Status incidentStatus)
        {
            return incidentStatus == Status.BADWORD ||
                   incidentStatus == Status.BLACKLIST ||
                   incidentStatus == Status.DUPLICATE ||
                   incidentStatus == Status.MISSING_DETAILS;
        }
        public static bool Is_Open(int _status)
        {
            var incidentStatus = (Status)_status;
            /*
            return incidentStatus == Status.NEW ||
                   incidentStatus == Status.WAITING ||
                   incidentStatus == Status.AFTER_AUCTION ||
                   incidentStatus == Status.IN_REVIEW;
             * */
            return Is_Open(incidentStatus);
        }
        public static bool Is_Open(Status incidentStatus)
        {
       //     var incidentStatus = (Status)_status;
            return incidentStatus == Status.NEW ||
                   incidentStatus == Status.WAITING ||
                   incidentStatus == Status.AFTER_AUCTION ||
                   incidentStatus == Status.IN_REVIEW;
        }

        
        #endregion
    }
}
