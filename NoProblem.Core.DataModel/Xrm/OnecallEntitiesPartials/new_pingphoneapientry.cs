﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_pingphoneapientry
    {
        public enum Status
        {
            Ping1Accepted = 1,
            Ping2Done,
            InCall,
            CallDone,
            TimedOut,
            Ping1NotInterested,
            Ping1Error,
            Ping2Error,
            CallError
        }
    }
}
