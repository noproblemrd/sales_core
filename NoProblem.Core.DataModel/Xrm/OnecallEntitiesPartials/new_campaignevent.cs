﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_campaignevent : IComparable<new_campaignevent>
    {
        public enum CampaignEventStatusCode
        {
            Open = 1,
            Sent = 3,
            Discarded = 4
        }

        #region IComparable<new_campaignevent> Members

        public int CompareTo(new_campaignevent other)
        {
            int retVal = 0;
            if (other == null)
            {
                retVal = 1;
            }
            else if (this.new_campaignactivityid.Value != other.new_campaignactivityid.Value)
            {
                if (this.new_campaignactivity_campaignevent.new_priority.Value
                    < other.new_campaignactivity_campaignevent.new_priority.Value)
                {
                    retVal = 1;
                }
                else if (this.new_campaignactivity_campaignevent.new_priority.Value
                    > other.new_campaignactivity_campaignevent.new_priority.Value)
                {
                    retVal = -1;
                }
            }
            if (retVal == 0)
            {
                if (this.createdon.Value <= other.createdon.Value)
                {
                    retVal = 1;
                }
                else
                {
                    retVal = -1;
                }
            }
            return retVal;
        }            

        #endregion
    }
}
