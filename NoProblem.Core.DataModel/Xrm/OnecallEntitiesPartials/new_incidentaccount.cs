﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.ClipCall;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_incidentaccount:IIdentifiable
    {

        public bool IsConnectedSuccessfullyWithSupplier
        {
            get
            {
                if (this.statuscode.HasValue == false)
                    return false;

                var status = (Status) this.statuscode.Value;
                if( status== Status.CLOSE)
                    return true;

                if(status == Status.CLOSED_BY_DIRECT_NUMBERS)
                    return true;

                if (status == Status.INVITED_BY_SMS)
                    return true;

                return false;

            }
        }

        public enum RefundSatus
        {
            OnHold = 1,
            Cancelled = 2,
            Approved = 3,
            Pending = 4
        }
        public enum  CallTracing
        {
            DIAL_SUPPLIER = 1,
            SUPPLIER_ANSWER = 2,
            SUPPLIER_PRESS_DTMF_AND_DIAL_CUSTOMER = 3,
            CUSTOMER_ANSWER_AND_BRIDGE = 4,
            SUPPLIER_DIDNT_PRESS_DTMF = 5
        }
        [System.Xml.Serialization.XmlType(Namespace = "http://tempuri.org/", TypeName = "IncidentAccountStatus")]
        public enum Status
        {
            /// <summary>
            /// מתאים
            /// </summary>
            MATCH = 1,
            /// <summary>
            /// ביצע עבודה    //ia.StatusCode = 14 or ia.StatusCode = 15 or ia.StatusCode = 10
            /// </summary>
            WORK_DONE = 4,
            /// <summary>
            /// סגר בקשת שירות
            /// </summary>
            CLOSE = 10,
            /// <summary>
            /// הוזמן במסרון
            /// </summary>
            INVITED_BY_SMS = 12,
            /// <summary>
            /// סגר בקשת שירות – מ"י
            /// </summary>
            CLOSED_BY_DIRECT_NUMBERS = 13,
            /// <summary>
            /// נמצא בתהליך מכרז
            /// </summary>
            IN_AUCTION = 14,
            /// <summary>
            ///  He is currently in conversation wit hthe customer
            /// </summary>
            IN_CONVERSATION = 15,
            /// <summary>
            /// Lost the job
            /// </summary>
            LOST = 16,
            /// <summary>
            /// Telephony error received.
            /// </summary>
            ERROR = 17,
            Waiting = 18,
            MISSED = 19,
            REJECT = 20
        }

        public Guid Id { get { return this.new_incidentaccountid; } }

        public enum CallProcess
        {
            Pending = 1,
            WaitForCall = 2,
            InConversation = 3,
            CallDone = 4
        }
    }
}
