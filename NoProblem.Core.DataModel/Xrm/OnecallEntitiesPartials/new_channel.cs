﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_channel
    {
        public enum ChannelCode
        {
            Call = 1,
            Email = 2,
            SMS = 3,
            Fax = 4
        }
    }
}
