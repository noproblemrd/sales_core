﻿
using System;
using NoProblem.Core.DataModel.ClipCall;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class account : IIdentifiable
    {
        private const string PHONE_API_NAME = "phone";
        private const string ELOCAL_DYNAMIC_PHONE = "elocaldynamicphone";

        public bool IsPhoneApi()
        {
            return IsLeadBuyer() && (new_leadbuyername == PHONE_API_NAME || new_leadbuyername == ELOCAL_DYNAMIC_PHONE);
        }

        public bool IsLeadBuyer()
        {
            return new_isleadbuyer.HasValue && new_isleadbuyer.Value;
        }

        public enum SupplierStatus
        {
            Approved = 1,
            Candidate = 2,
            Inactive = 3,
            Trial = 4,
            Rarah = 5
        }

        public enum StageInRegistration
        {
            GENERAL_INFO = 1,
            EXPERTISE = 2,
            CITIES = 3,
            WORKS_HOUR = 4,
            PAYMENT = 5,
            PAID = 6
        }

        public enum StageInTrialRegistration
        {
            EnteredMobile = 1,
            EnteredCode = 2,
            EnteredHeading = 3,
            EnteredLocation = 4
        }

        public enum RechargeTypeCode
        {
            Monthly = 1,
            BalanceLow = 2,
            MaxMonthly = 3
        }

        public enum InvoiceSendOption
        {
            Email = 1,
            Mail = 2
        }

        public enum TrialStatusReasonCode
        {
            FreshlyImported = 1,
            Machine = 2,
            Accessible = 3,
            InTrial = 4,
            RemoveMe = 5,
            Expired = 6,
            InRegistration = 7,
            DoNotCallList = 8
        }

        public enum DapazStatus
        {
            PPA = 1,
            FixGold,
            Rarah,
            RarahQuote,
            Fix,
            RarahGold,
            PPAHold
        }

        public enum FundsStatus
        {
            Normal = 1,
            DelayedSale,
            LegalHandling,
            HasDelayingDebt,
            HasDebt,
            ChecksOnly,
            SalesAgentTreatment,
            DontSellOpenDebt
        }

        public enum StepInRegistration2014
        {
            VerifyYourEmail_1 = 1,
            ChoosePlan_2,
            BusinessDetails_General_3,
            BusinessDetails_Category_4,
            BusinessDetails_Area_5,
            Checkout_6,
            Dashboard_7
        }

        public Guid Id { get { return this.accountid; } }
    }
}
