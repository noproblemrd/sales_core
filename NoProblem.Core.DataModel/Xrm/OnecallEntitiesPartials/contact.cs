﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class contact
    {
        public enum CustomerTypeCode
        {
            DefaultValue = 1,
            ANONYMOUS = 200000
        }

        public enum GenderCode
        {
            MALE = 1,
            FEMALE = 2
        }
        public enum eStatusCode
        {
            Active = 1,
            Inactive = 200000
        }
    }
}
