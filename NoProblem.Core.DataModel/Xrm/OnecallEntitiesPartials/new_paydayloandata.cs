﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_paydayloandata
    {
        public enum pdlBestTimeToCall
        {
            Anytime = 1,
            Morning,
            Afternoon,
            Evening
        }

        public enum pdlIncomeSource
        {
            JobIncome = 1,
            Benefit
        }

        public enum pdlPayFrequency
        {
            Weekly = 1,
            Biweekly,
            TwiceMonthly,
            Monthly
        }

        public enum pdlAccountType
        {
            Checking = 1,
            Saving
        }
    }
}
