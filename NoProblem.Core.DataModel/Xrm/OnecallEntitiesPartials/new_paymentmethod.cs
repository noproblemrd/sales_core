﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_paymentmethod
    {
        public enum ePaymentMethod
        {
            Cash = 1,
            Check = 2,
            CreditCard = 3,
            WireTransfer = 4,
            Voucher = 5
        }
    }
}
