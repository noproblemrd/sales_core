﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataModel.Xrm
{
    public partial class new_paymentcomponent
    {
        public enum ePaymentComponentType
        {
            MonthlyFeePayment = 1,
            AutoMaxMonthly,
            AutoLow,
            Manual,
            Monthly
        }
    }
}
