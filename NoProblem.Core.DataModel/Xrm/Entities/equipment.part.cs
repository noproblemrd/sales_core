﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Resource that can be scheduled. (equipment)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("equipmentid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("equipment", "equipments")]
	[global::System.Data.Services.IgnoreProperties("businessunitid")]
	public partial class equipment : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "equipment";
		private const string _primaryKeyLogicalName = "equipmentid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public equipment()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public equipment(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public equipment(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public equipment(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the associated business unit. (Attribute for businessunitid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitid")]
		public global::Microsoft.Crm.Sdk.Lookup businessunitid
		{
			get { return this.GetPropertyValue<global::Microsoft.Crm.Sdk.Lookup>("businessunitid"); }
			set { this.SetPropertyValue("businessunitid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier of the associated business unit. (Attribute for businessunitid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("businessunitid")]
		public global::System.Guid? business_unit_equipment_id
		{
			get { return this.GetPropertyValue<global::System.Guid?>("businessunitid"); }
			set { this.SetPropertyValue("businessunitid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "businessunit"); }
		}

		/// <summary>
		/// Unique identifier of the associated business unit. (N:1 Association for business_unit_equipment)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("businessunitid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("business_unit_equipment")]
		public global::NoProblem.Core.DataModel.Xrm.businessunit business_unit_equipment
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.businessunit>("businessunitid", "businessunitid"); }
			set { this.SetRelatedEntity("businessunitid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier of the associated business unit. (Attribute for businessunitid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("businessunitid")]
		public global::System.Guid? equipment_systemuser_id
		{
			get { return this.GetPropertyValue<global::System.Guid?>("businessunitid"); }
			set { this.SetPropertyValue("businessunitid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the associated business unit. (N:1 Association for equipment_systemuser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("businessunitid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("equipment_systemuser")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser equipment_systemuser
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("businessunitid", "businessunitid"); }
			set { this.SetRelatedEntity("businessunitid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Fiscal calendar associated with the facility/equipment. (Attribute for calendarid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("calendarid")]
		public global::System.Guid? calendarid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("calendarid"); }
			set { this.SetPropertyValue("calendarid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "calendar"); }
		}

		/// <summary>
		/// Fiscal calendar associated with the facility/equipment. (N:1 Association for calendar_equipment)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("calendarid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("calendar_equipment")]
		public global::NoProblem.Core.DataModel.Xrm.calendar calendar_equipment
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.calendar>("calendarid", "calendarid"); }
			set { this.SetRelatedEntity("calendarid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier of the user who created the facility/equipment entry. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the facility/equipment entry. (N:1 Association for lk_equipment_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_equipment_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_equipment_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the facility/equipment entry was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Description of the facility/equipment. (Attribute for description)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("description")]
		public global::System.String description
		{
			get { return this.GetPropertyValue<global::System.String>("description"); }
			set { this.SetPropertyValue("description", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// For internal use only. (Attribute for displayinserviceviews)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("displayinserviceviews")]
		public global::System.Boolean? displayinserviceviews
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("displayinserviceviews"); }
			set { this.SetPropertyValue("displayinserviceviews", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// Email address of person to contact about the use of the facility/equipment. (Attribute for emailaddress)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("emailaddress")]
		public global::System.String emailaddress
		{
			get { return this.GetPropertyValue<global::System.String>("emailaddress"); }
			set { this.SetPropertyValue("emailaddress", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the facility/equipment. (Attribute for equipmentid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("equipmentid", true)]
		public global::System.Guid equipmentid
		{
			get { return this.GetPropertyValue<global::System.Guid>("equipmentid"); }
			set { this.SetPropertyValue("equipmentid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier of the data import or data migration that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Whether the facility/equipment is disabled or operational. (Attribute for isdisabled)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("isdisabled")]
		public global::System.Boolean? isdisabled
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("isdisabled"); }
			set { this.SetPropertyValue("isdisabled", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the facility/equipment. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the facility/equipment. (N:1 Association for lk_equipment_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_equipment_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_equipment_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the facility/equipment entry was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Name of the facility/equipment. (Attribute for name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("name")]
		public global::System.String name
		{
			get { return this.GetPropertyValue<global::System.String>("name"); }
			set { this.SetPropertyValue("name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the parent business unit. (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier of the parent business unit. (N:1 Association for organization_equipment)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_equipment")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_equipment
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Site where the facility/equipment is located. (Attribute for siteid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("siteid")]
		public global::System.Guid? siteid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("siteid"); }
			set { this.SetPropertyValue("siteid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "site"); }
		}

		/// <summary>
		/// Site where the facility/equipment is located. (N:1 Association for site_equipment)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("siteid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("site_equipment")]
		public global::NoProblem.Core.DataModel.Xrm.site site_equipment
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.site>("siteid", "siteid"); }
			set { this.SetRelatedEntity("siteid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Skills needed to operate the facility/equipment. (Attribute for skills)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("skills")]
		public global::System.String skills
		{
			get { return this.GetPropertyValue<global::System.String>("skills"); }
			set { this.SetPropertyValue("skills", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Local time zone where the facility/equipment is located. (Attribute for timezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezonecode")]
		public global::System.Int32? timezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezonecode"); }
			set { this.SetPropertyValue("timezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// For internal use only. (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Time zone code that was in use when the record was created. (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for business_map_equipment)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("business_map_equipment", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.businessunitmap> business_map_equipment
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.businessunitmap>("businessunitid", "subbusinessid"); }
		}

		/// <summary>
		///  (1:N Association for equipment_accounts)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("equipment_accounts", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.account> equipment_accounts
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.account>("equipmentid", "preferredequipmentid"); }
		}

		/// <summary>
		///  (1:N Association for equipment_activity_parties)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("equipment_activity_parties", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.activityparty> equipment_activity_parties
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.activityparty>("equipmentid", "partyid"); }
		}

		/// <summary>
		///  (1:N Association for Equipment_Annotation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("Equipment_Annotation", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.annotation> Equipment_Annotation
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.annotation>("equipmentid", "objectid"); }
		}

		/// <summary>
		///  (1:N Association for Equipment_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("Equipment_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> Equipment_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("equipmentid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for Equipment_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("Equipment_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> Equipment_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("equipmentid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for equipment_contacts)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("equipment_contacts", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.contact> equipment_contacts
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.contact>("equipmentid", "preferredequipmentid"); }
		}

		/// <summary>
		///  (1:N Association for Equipment_DuplicateBaseRecord)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("Equipment_DuplicateBaseRecord", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.duplicaterecord> Equipment_DuplicateBaseRecord
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.duplicaterecord>("equipmentid", "baserecordid"); }
		}

		/// <summary>
		///  (1:N Association for Equipment_DuplicateMatchingRecord)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("Equipment_DuplicateMatchingRecord", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.duplicaterecord> Equipment_DuplicateMatchingRecord
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.duplicaterecord>("equipmentid", "duplicaterecordid"); }
		}

		/// <summary>
		///  (1:N Association for equipment_resources)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("equipment_resources", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.resource> equipment_resources
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.resource>("equipmentid", "resourceid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for businessunitiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitiddsc")]
		public global::System.Int32? businessunitiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("businessunitiddsc"); }
			private set { this.SetPropertyValue("businessunitiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for businessunitidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitidname")]
		public global::System.String businessunitidname
		{
			get { return this.GetPropertyValue<global::System.String>("businessunitidname"); }
			private set { this.SetPropertyValue("businessunitidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyyominame")]
		public global::System.String createdbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyyominame"); }
			private set { this.SetPropertyValue("createdbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for displayinserviceviewsname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("displayinserviceviewsname")]
		public global::System.Object displayinserviceviewsname
		{
			get { return this.GetPropertyValue<global::System.Object>("displayinserviceviewsname"); }
			private set { this.SetPropertyValue("displayinserviceviewsname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for isdisabledname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("isdisabledname")]
		public global::System.Object isdisabledname
		{
			get { return this.GetPropertyValue<global::System.Object>("isdisabledname"); }
			private set { this.SetPropertyValue("isdisabledname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyyominame")]
		public global::System.String modifiedbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyyominame"); }
			private set { this.SetPropertyValue("modifiedbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for organizationiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationiddsc")]
		public global::System.Int32? organizationiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("organizationiddsc"); }
			private set { this.SetPropertyValue("organizationiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for organizationidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationidname")]
		public global::System.String organizationidname
		{
			get { return this.GetPropertyValue<global::System.String>("organizationidname"); }
			private set { this.SetPropertyValue("organizationidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for siteiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("siteiddsc")]
		public global::System.Int32? siteiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("siteiddsc"); }
			private set { this.SetPropertyValue("siteiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for siteidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("siteidname")]
		public global::System.String siteidname
		{
			get { return this.GetPropertyValue<global::System.String>("siteidname"); }
			private set { this.SetPropertyValue("siteidname", value, typeof(global::System.String)); }
		}

		*/

		#endregion
	}
}
