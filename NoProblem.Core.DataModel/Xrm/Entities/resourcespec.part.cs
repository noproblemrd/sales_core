﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Selection rule that allows the scheduling engine to select a number of resources from a pool of resources. The rules can be associated with a service. (resourcespec)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("resourcespecid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("resourcespec", "resourcespecs")]
	public partial class resourcespec : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "resourcespec";
		private const string _primaryKeyLogicalName = "resourcespecid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public resourcespec()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public resourcespec(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public resourcespec(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public resourcespec(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the business unit with which the resource specification is associated. (Attribute for businessunitid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitid")]
		public global::System.Guid? businessunitid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("businessunitid"); }
			set { this.SetPropertyValue("businessunitid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "businessunit"); }
		}

		/// <summary>
		/// Unique identifier of the business unit with which the resource specification is associated. (N:1 Association for business_unit_resource_specs)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("businessunitid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("business_unit_resource_specs")]
		public global::NoProblem.Core.DataModel.Xrm.businessunit business_unit_resource_specs
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.businessunit>("businessunitid", "businessunitid"); }
			set { this.SetRelatedEntity("businessunitid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Additional constraints, specified as expressions, which are used to filter a set of valid resources. (Attribute for constraints)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("constraints")]
		public global::System.String constraints
		{
			get { return this.GetPropertyValue<global::System.String>("constraints"); }
			set { this.SetPropertyValue("constraints", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the user who created the resource specification. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the resource specification. (N:1 Association for lk_resourcespec_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_resourcespec_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_resourcespec_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the resource specification was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Selection rule that allows the scheduling engine to select a number of resources from a pool of resources. The rules can be associated with a service. (Attribute for description)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("description")]
		public global::System.String description
		{
			get { return this.GetPropertyValue<global::System.String>("description"); }
			set { this.SetPropertyValue("description", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Number that specifies the minimal effort required from resources. (Attribute for effortrequired)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("effortrequired")]
		public global::System.Double? effortrequired
		{
			get { return this.GetPropertyValue<global::System.Double?>("effortrequired"); }
			set { this.SetPropertyValue("effortrequired", value, typeof(global::Microsoft.Crm.Sdk.CrmFloat)); }
		}

		/// <summary>
		/// Unique identifier of the scheduling group with which the resource specification is associated. (Attribute for groupobjectid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("groupobjectid")]
		public global::System.Guid? groupobjectid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("groupobjectid"); }
			set { this.SetPropertyValue("groupobjectid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the scheduling group with which the resource specification is associated. (Attribute for groupobjectid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("groupobjectid")]
		public global::System.Guid? constraint_based_group_resource_specs_id
		{
			get { return this.GetPropertyValue<global::System.Guid?>("groupobjectid"); }
			set { this.SetPropertyValue("groupobjectid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier), "constraintbasedgroup"); }
		}

		/// <summary>
		/// Unique identifier of the scheduling group with which the resource specification is associated. (N:1 Association for constraint_based_group_resource_specs)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("groupobjectid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("constraint_based_group_resource_specs")]
		public global::NoProblem.Core.DataModel.Xrm.constraintbasedgroup constraint_based_group_resource_specs
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.constraintbasedgroup>("groupobjectid", "constraintbasedgroupid"); }
			set { this.SetRelatedEntity("groupobjectid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the scheduling group with which the resource specification is associated. (Attribute for groupobjectid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("groupobjectid")]
		public global::System.Guid? team_resource_specs_id
		{
			get { return this.GetPropertyValue<global::System.Guid?>("groupobjectid"); }
			set { this.SetPropertyValue("groupobjectid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier), "team"); }
		}

		/// <summary>
		/// Unique identifier of the scheduling group with which the resource specification is associated. (N:1 Association for team_resource_specs)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("groupobjectid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("team_resource_specs")]
		public global::NoProblem.Core.DataModel.Xrm.team team_resource_specs
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.team>("groupobjectid", "teamid"); }
			set { this.SetRelatedEntity("groupobjectid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the resource specification. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the resource specification. (N:1 Association for lk_resourcespec_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_resourcespec_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_resourcespec_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the resource specification was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Name of the resource specification. (Attribute for name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("name")]
		public global::System.String name
		{
			get { return this.GetPropertyValue<global::System.String>("name"); }
			set { this.SetPropertyValue("name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Search strategy to use for the resource specification. (Attribute for objectiveexpression)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("objectiveexpression")]
		public global::System.String objectiveexpression
		{
			get { return this.GetPropertyValue<global::System.String>("objectiveexpression"); }
			set { this.SetPropertyValue("objectiveexpression", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Type of entity with which the resource specification is associated. (Attribute for objecttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("objecttypecode", "objecttypecodeLabel")]
		public global::System.String objecttypecode
		{
			get { return this.GetPropertyValue<global::System.String>("objecttypecode"); }
			set { this.SetPropertyValue("objecttypecode", value, typeof(global::Microsoft.Crm.Sdk.EntityNameReference)); }
		}

		/// <summary>
		/// Type of entity with which the resource specification is associated. (Label for objecttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("objecttypecode")]
		public global::System.String objecttypecodeLabel
		{
			get { return this.GetPropertyLabel("objecttypecode"); }
		}

		/// <summary>
		/// Unique identifier of the organization with which the resource specification is associated. (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier of the organization with which the resource specification is associated. (N:1 Association for organization_resource_specs)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_resource_specs")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_resource_specs
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Required number of resources that must be available. Use -1 to indicate all resources. (Attribute for requiredcount)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("requiredcount")]
		public global::System.Int32? requiredcount
		{
			get { return this.GetPropertyValue<global::System.Int32?>("requiredcount"); }
			set { this.SetPropertyValue("requiredcount", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the resource specification. (Attribute for resourcespecid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("resourcespecid", true)]
		public global::System.Guid resourcespecid
		{
			get { return this.GetPropertyValue<global::System.Guid>("resourcespecid"); }
			set { this.SetPropertyValue("resourcespecid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Value that specifies that all valid and available resources must be in the same site. (Attribute for samesite)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("samesite")]
		public global::System.Boolean? samesite
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("samesite"); }
			set { this.SetPropertyValue("samesite", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for ActivityPartyResourceSpec)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("ActivityPartyResourceSpec", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.activityparty> ActivityPartyResourceSpec
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.activityparty>("resourcespecid", "resourcespecid"); }
		}

		/// <summary>
		///  (1:N Association for resource_spec_services)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("resource_spec_services", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.service> resource_spec_services
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.service>("resourcespecid", "resourcespecid"); }
		}

		/// <summary>
		///  (1:N Association for ResourceSpec_Annotation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("ResourceSpec_Annotation", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.annotation> ResourceSpec_Annotation
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.annotation>("resourcespecid", "objectid"); }
		}

		/// <summary>
		///  (1:N Association for ResourceSpec_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("ResourceSpec_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> ResourceSpec_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("resourcespecid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for ResourceSpec_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("ResourceSpec_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> ResourceSpec_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("resourcespecid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for resourcespec_resources)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("resourcespec_resources", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.resource> resourcespec_resources
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.resource>("resourcespecid", "resourceid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for businessunitiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitiddsc")]
		public global::System.Int32? businessunitiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("businessunitiddsc"); }
			private set { this.SetPropertyValue("businessunitiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for businessunitidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitidname")]
		public global::System.String businessunitidname
		{
			get { return this.GetPropertyValue<global::System.String>("businessunitidname"); }
			private set { this.SetPropertyValue("businessunitidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyyominame")]
		public global::System.String createdbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyyominame"); }
			private set { this.SetPropertyValue("createdbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyyominame")]
		public global::System.String modifiedbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyyominame"); }
			private set { this.SetPropertyValue("modifiedbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for objecttypecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("objecttypecodename")]
		public global::System.Object objecttypecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("objecttypecodename"); }
			private set { this.SetPropertyValue("objecttypecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for organizationiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationiddsc")]
		public global::System.Int32? organizationiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("organizationiddsc"); }
			private set { this.SetPropertyValue("organizationiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for organizationidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationidname")]
		public global::System.String organizationidname
		{
			get { return this.GetPropertyValue<global::System.String>("organizationidname"); }
			private set { this.SetPropertyValue("organizationidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for samesitename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("samesitename")]
		public global::System.Object samesitename
		{
			get { return this.GetPropertyValue<global::System.Object>("samesitename"); }
			private set { this.SetPropertyValue("samesitename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
