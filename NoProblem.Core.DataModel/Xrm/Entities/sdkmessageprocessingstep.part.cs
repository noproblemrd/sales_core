﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Stage in the execution pipeline that a plug-in is to execute. (sdkmessageprocessingstep)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("sdkmessageprocessingstepid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("sdkmessageprocessingstep", "sdkmessageprocessingsteps")]
	public partial class sdkmessageprocessingstep : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "sdkmessageprocessingstep";
		private const string _primaryKeyLogicalName = "sdkmessageprocessingstepid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public sdkmessageprocessingstep()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public sdkmessageprocessingstep(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public sdkmessageprocessingstep(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public sdkmessageprocessingstep(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Step-specific configuration for the plug-in type. Passed to the plug-in constructor at run time. (Attribute for configuration)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("configuration")]
		public global::System.String configuration
		{
			get { return this.GetPropertyValue<global::System.String>("configuration"); }
			set { this.SetPropertyValue("configuration", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the user who created the SDK message processing step. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the SDK message processing step. (N:1 Association for createdby_sdkmessageprocessingstep)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("createdby_sdkmessageprocessingstep")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser createdby_sdkmessageprocessingstep
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the SDK message processing step was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Customization level of the SDK message processing step. (Attribute for customizationlevel)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("customizationlevel")]
		public global::System.Int32? customizationlevel
		{
			get { return this.GetPropertyValue<global::System.Int32?>("customizationlevel"); }
			private set { this.SetPropertyValue("customizationlevel", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Description of the SDK message processing step. (Attribute for description)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("description")]
		public global::System.String description
		{
			get { return this.GetPropertyValue<global::System.String>("description"); }
			set { this.SetPropertyValue("description", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Comma-separated list of attributes. If at least one of these attributes is modified, the plug-in should execute. (Attribute for filteringattributes)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("filteringattributes")]
		public global::System.String filteringattributes
		{
			get { return this.GetPropertyValue<global::System.String>("filteringattributes"); }
			set { this.SetPropertyValue("filteringattributes", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the user to impersonate context when step is executed. (Attribute for impersonatinguserid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("impersonatinguserid")]
		public global::System.Guid? impersonatinguserid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("impersonatinguserid"); }
			set { this.SetPropertyValue("impersonatinguserid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user to impersonate context when step is executed. (N:1 Association for impersonatinguserid_sdkmessageprocessingstep)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("impersonatinguserid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("impersonatinguserid_sdkmessageprocessingstep")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser impersonatinguserid_sdkmessageprocessingstep
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("impersonatinguserid", "systemuserid"); }
			set { this.SetRelatedEntity("impersonatinguserid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Identifies if a plug-in should be executed from a parent pipeline, a child pipeline, or both. (Attribute for invocationsource)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("invocationsource", "invocationsourceLabel")]
		public global::System.Int32? invocationsource
		{
			get { return this.GetPropertyValue<global::System.Int32?>("invocationsource"); }
			set { this.SetPropertyValue("invocationsource", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Identifies if a plug-in should be executed from a parent pipeline, a child pipeline, or both. (Label for invocationsource)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("invocationsource")]
		public global::System.String invocationsourceLabel
		{
			get { return this.GetPropertyLabel("invocationsource"); }
		}

		/// <summary>
		/// Run-time mode of execution, for example, synchronous or asynchronous. (Attribute for mode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("mode", "modeLabel")]
		public global::System.Int32? mode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("mode"); }
			set { this.SetPropertyValue("mode", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Run-time mode of execution, for example, synchronous or asynchronous. (Label for mode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("mode")]
		public global::System.String modeLabel
		{
			get { return this.GetPropertyLabel("mode"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the SDK message processing step. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the SDK message processing step. (N:1 Association for modifiedby_sdkmessageprocessingstep)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("modifiedby_sdkmessageprocessingstep")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser modifiedby_sdkmessageprocessingstep
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the SDK message processing step was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier of the organization with which the SDK message processing step is associated. (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier of the organization with which the SDK message processing step is associated. (N:1 Association for organization_sdkmessageprocessingstep)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_sdkmessageprocessingstep")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_sdkmessageprocessingstep
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier of the plug-in type associated with the step. (Attribute for plugintypeid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("plugintypeid")]
		public global::System.Guid? plugintypeid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("plugintypeid"); }
			set { this.SetPropertyValue("plugintypeid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "plugintype"); }
		}

		/// <summary>
		/// Unique identifier of the plug-in type associated with the step. (N:1 Association for plugintypeid_sdkmessageprocessingstep)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("plugintypeid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("plugintypeid_sdkmessageprocessingstep")]
		public global::NoProblem.Core.DataModel.Xrm.plugintype plugintypeid_sdkmessageprocessingstep
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.plugintype>("plugintypeid", "plugintypeid"); }
			set { this.SetRelatedEntity("plugintypeid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Processing order within the stage. (Attribute for rank)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("rank")]
		public global::System.Int32? rank
		{
			get { return this.GetPropertyValue<global::System.Int32?>("rank"); }
			set { this.SetPropertyValue("rank", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the SDK message filter. (Attribute for sdkmessagefilterid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessagefilterid")]
		public global::System.Guid? sdkmessagefilterid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("sdkmessagefilterid"); }
			set { this.SetPropertyValue("sdkmessagefilterid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "sdkmessagefilter"); }
		}

		/// <summary>
		/// Unique identifier of the SDK message filter. (N:1 Association for sdkmessagefilterid_sdkmessageprocessingstep)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("sdkmessagefilterid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("sdkmessagefilterid_sdkmessageprocessingstep")]
		public global::NoProblem.Core.DataModel.Xrm.sdkmessagefilter sdkmessagefilterid_sdkmessageprocessingstep
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.sdkmessagefilter>("sdkmessagefilterid", "sdkmessagefilterid"); }
			set { this.SetRelatedEntity("sdkmessagefilterid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier of the SDK message. (Attribute for sdkmessageid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessageid")]
		public global::System.Guid? sdkmessageid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("sdkmessageid"); }
			set { this.SetPropertyValue("sdkmessageid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "sdkmessage"); }
		}

		/// <summary>
		/// Unique identifier of the SDK message. (N:1 Association for sdkmessageid_sdkmessageprocessingstep)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("sdkmessageid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("sdkmessageid_sdkmessageprocessingstep")]
		public global::NoProblem.Core.DataModel.Xrm.sdkmessage sdkmessageid_sdkmessageprocessingstep
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.sdkmessage>("sdkmessageid", "sdkmessageid"); }
			set { this.SetRelatedEntity("sdkmessageid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier of the SDK message processing step entity. (Attribute for sdkmessageprocessingstepid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessageprocessingstepid", true)]
		public global::System.Guid sdkmessageprocessingstepid
		{
			get { return this.GetPropertyValue<global::System.Guid>("sdkmessageprocessingstepid"); }
			set { this.SetPropertyValue("sdkmessageprocessingstepid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier of the SDK message processing step. (Attribute for sdkmessageprocessingstepidunique)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessageprocessingstepidunique")]
		public global::System.Guid? sdkmessageprocessingstepidunique
		{
			get { return this.GetPropertyValue<global::System.Guid?>("sdkmessageprocessingstepidunique"); }
			set { this.SetPropertyValue("sdkmessageprocessingstepidunique", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the Sdk message processing step secure configuration. (Attribute for sdkmessageprocessingstepsecureconfigid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessageprocessingstepsecureconfigid")]
		public global::System.Guid? sdkmessageprocessingstepsecureconfigid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("sdkmessageprocessingstepsecureconfigid"); }
			set { this.SetPropertyValue("sdkmessageprocessingstepsecureconfigid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "sdkmessageprocessingstepsecureconfig"); }
		}

		/// <summary>
		/// Unique identifier of the Sdk message processing step secure configuration. (N:1 Association for sdkmessageprocessingstepsecureconfigid_sdkmessageprocessingstep)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("sdkmessageprocessingstepsecureconfigid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("sdkmessageprocessingstepsecureconfigid_sdkmessageprocessingstep")]
		public global::NoProblem.Core.DataModel.Xrm.sdkmessageprocessingstepsecureconfig sdkmessageprocessingstepsecureconfigid_sdkmessageprocessingstep
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.sdkmessageprocessingstepsecureconfig>("sdkmessageprocessingstepsecureconfigid", "sdkmessageprocessingstepsecureconfigid"); }
			set { this.SetRelatedEntity("sdkmessageprocessingstepsecureconfigid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Stage in the execution pipeline that the SDK message processing step is in. (Attribute for stage)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("stage", "stageLabel")]
		public global::System.Int32? stage
		{
			get { return this.GetPropertyValue<global::System.Int32?>("stage"); }
			set { this.SetPropertyValue("stage", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Stage in the execution pipeline that the SDK message processing step is in. (Label for stage)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("stage")]
		public global::System.String stageLabel
		{
			get { return this.GetPropertyLabel("stage"); }
		}

		/// <summary>
		/// Status of the SDK message processing step. (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the SDK message processing step. (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the SDK message processing step. (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		/// Deployment that the SDK message processing step should be executed on; server, client, or both. (Attribute for supporteddeployment)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("supporteddeployment", "supporteddeploymentLabel")]
		public global::System.Int32? supporteddeployment
		{
			get { return this.GetPropertyValue<global::System.Int32?>("supporteddeployment"); }
			set { this.SetPropertyValue("supporteddeployment", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Deployment that the SDK message processing step should be executed on; server, client, or both. (Label for supporteddeployment)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("supporteddeployment")]
		public global::System.String supporteddeploymentLabel
		{
			get { return this.GetPropertyLabel("supporteddeployment"); }
		}

		/// <summary>
		/// Number that identifies a specific revision of the SDK message processing step.  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for sdkmessageprocessingstepid_sdkmessageprocessingstepimage)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("sdkmessageprocessingstepid_sdkmessageprocessingstepimage", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.sdkmessageprocessingstepimage> sdkmessageprocessingstepid_sdkmessageprocessingstepimage
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.sdkmessageprocessingstepimage>("sdkmessageprocessingstepid", "sdkmessageprocessingstepid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for invocationsourcename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("invocationsourcename")]
		public global::System.Object invocationsourcename
		{
			get { return this.GetPropertyValue<global::System.Object>("invocationsourcename"); }
			private set { this.SetPropertyValue("invocationsourcename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for modename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modename")]
		public global::System.Object modename
		{
			get { return this.GetPropertyValue<global::System.Object>("modename"); }
			private set { this.SetPropertyValue("modename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for stagename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("stagename")]
		public global::System.Object stagename
		{
			get { return this.GetPropertyValue<global::System.Object>("stagename"); }
			private set { this.SetPropertyValue("stagename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for supporteddeploymentname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("supporteddeploymentname")]
		public global::System.Object supporteddeploymentname
		{
			get { return this.GetPropertyValue<global::System.Object>("supporteddeploymentname"); }
			private set { this.SetPropertyValue("supporteddeploymentname", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
