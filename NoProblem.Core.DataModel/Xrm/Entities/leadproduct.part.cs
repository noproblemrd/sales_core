﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (leadproduct)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("leadproductid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("leadproduct", "leadproducts")]
	public partial class leadproduct : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "leadproduct";
		private const string _primaryKeyLogicalName = "leadproductid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public leadproduct()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public leadproduct(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public leadproduct(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public leadproduct(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		///  (Attribute for leadid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("leadid")]
		public global::System.Guid? leadid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("leadid"); }
			private set { this.SetPropertyValue("leadid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the lead product. (Attribute for leadproductid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("leadproductid", true)]
		public global::System.Guid leadproductid
		{
			get { return this.GetPropertyValue<global::System.Guid>("leadproductid"); }
			set { this.SetPropertyValue("leadproductid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		///  (Attribute for productid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("productid")]
		public global::System.Guid? productid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("productid"); }
			private set { this.SetPropertyValue("productid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (N:N Association for leadproduct_association)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("leadproduct_association", false)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.lead> leadproduct_association
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.lead>("productid", "leadproduct", "leadid", "leadid"); }
		}

		*/

		#endregion
	}
}
