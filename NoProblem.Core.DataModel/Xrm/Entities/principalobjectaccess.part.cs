﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (principalobjectaccess)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("principalobjectaccessid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("principalobjectaccess", "principalobjectaccesses")]
	public partial class principalobjectaccess : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "principalobjectaccess";
		private const string _primaryKeyLogicalName = "principalobjectaccessid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public principalobjectaccess()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public principalobjectaccess(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public principalobjectaccess(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public principalobjectaccess(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		///  (Attribute for accessrightsmask)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("accessrightsmask")]
		public global::System.Int32? accessrightsmask
		{
			get { return this.GetPropertyValue<global::System.Int32?>("accessrightsmask"); }
			set { this.SetPropertyValue("accessrightsmask", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for changedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("changedon")]
		public global::System.DateTime? changedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("changedon"); }
			set { this.SetPropertyValue("changedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for inheritedaccessrightsmask)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("inheritedaccessrightsmask")]
		public global::System.Int32? inheritedaccessrightsmask
		{
			get { return this.GetPropertyValue<global::System.Int32?>("inheritedaccessrightsmask"); }
			set { this.SetPropertyValue("inheritedaccessrightsmask", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for objectid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("objectid")]
		public global::System.Guid? objectid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("objectid"); }
			private set { this.SetPropertyValue("objectid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		///  (Attribute for objecttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("objecttypecode", "objecttypecodeLabel")]
		public global::System.String objecttypecode
		{
			get { return this.GetPropertyValue<global::System.String>("objecttypecode"); }
			set { this.SetPropertyValue("objecttypecode", value, typeof(global::Microsoft.Crm.Sdk.EntityNameReference)); }
		}

		/// <summary>
		///  (Label for objecttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("objecttypecode")]
		public global::System.String objecttypecodeLabel
		{
			get { return this.GetPropertyLabel("objecttypecode"); }
		}

		/// <summary>
		///  (Attribute for principalid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("principalid")]
		public global::System.Guid? principalid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("principalid"); }
			private set { this.SetPropertyValue("principalid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the principal object access. (Attribute for principalobjectaccessid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("principalobjectaccessid", true)]
		public global::System.Guid principalobjectaccessid
		{
			get { return this.GetPropertyValue<global::System.Guid>("principalobjectaccessid"); }
			set { this.SetPropertyValue("principalobjectaccessid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		///  (Attribute for principaltypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("principaltypecode", "principaltypecodeLabel")]
		public global::System.String principaltypecode
		{
			get { return this.GetPropertyValue<global::System.String>("principaltypecode"); }
			set { this.SetPropertyValue("principaltypecode", value, typeof(global::Microsoft.Crm.Sdk.EntityNameReference)); }
		}

		/// <summary>
		///  (Label for principaltypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("principaltypecode")]
		public global::System.String principaltypecodeLabel
		{
			get { return this.GetPropertyLabel("principaltypecode"); }
		}

		/// <summary>
		/// For internal use only. (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Time zone code that was in use when the record was created. (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for objecttypecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("objecttypecodename")]
		public global::System.Object objecttypecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("objecttypecodename"); }
			private set { this.SetPropertyValue("objecttypecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for principaltypecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("principaltypecodename")]
		public global::System.Object principaltypecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("principaltypecodename"); }
			private set { this.SetPropertyValue("principaltypecodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
