﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (systemuserroles)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("systemuserroleid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("systemuserroles", "systemuserroleses")]
	public partial class systemuserroles : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "systemuserroles";
		private const string _primaryKeyLogicalName = "systemuserroleid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public systemuserroles()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public systemuserroles(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public systemuserroles(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public systemuserroles(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		///  (Attribute for roleid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("roleid")]
		public global::System.Guid? roleid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("roleid"); }
			private set { this.SetPropertyValue("roleid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		///  (Attribute for systemuserid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("systemuserid")]
		public global::System.Guid? systemuserid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("systemuserid"); }
			private set { this.SetPropertyValue("systemuserid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// For internal use only. (Attribute for systemuserroleid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("systemuserroleid", true)]
		public global::System.Guid systemuserroleid
		{
			get { return this.GetPropertyValue<global::System.Guid>("systemuserroleid"); }
			set { this.SetPropertyValue("systemuserroleid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (N:N Association for systemuserroles_association)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("systemuserroles_association", false)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.systemuser> systemuserroles_association
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.systemuser>("roleid", "systemuserroles", "systemuserid", "systemuserid"); }
		}

		*/

		#endregion
	}
}
