﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// For internal use only. (sdkmessageresponsefield)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("sdkmessageresponsefieldid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("sdkmessageresponsefield", "sdkmessageresponsefields")]
	public partial class sdkmessageresponsefield : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "sdkmessageresponsefield";
		private const string _primaryKeyLogicalName = "sdkmessageresponsefieldid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public sdkmessageresponsefield()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public sdkmessageresponsefield(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public sdkmessageresponsefield(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public sdkmessageresponsefield(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Common language runtime (CLR)-based formatter of the SDK message response field. (Attribute for clrformatter)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("clrformatter")]
		public global::System.String clrformatter
		{
			get { return this.GetPropertyValue<global::System.String>("clrformatter"); }
			set { this.SetPropertyValue("clrformatter", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the user who created the SDK message response field. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the SDK message response field. (N:1 Association for createdby_sdkmessageresponsefield)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("createdby_sdkmessageresponsefield")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser createdby_sdkmessageresponsefield
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the SDK message response field was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Customization level of the SDK message response field. (Attribute for customizationlevel)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("customizationlevel")]
		public global::System.Int32? customizationlevel
		{
			get { return this.GetPropertyValue<global::System.Int32?>("customizationlevel"); }
			private set { this.SetPropertyValue("customizationlevel", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Formatter for the SDK message response field. (Attribute for formatter)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("formatter")]
		public global::System.String formatter
		{
			get { return this.GetPropertyValue<global::System.String>("formatter"); }
			set { this.SetPropertyValue("formatter", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the SDK message response field. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the SDK message response field. (N:1 Association for modifiedby_sdkmessageresponsefield)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("modifiedby_sdkmessageresponsefield")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser modifiedby_sdkmessageresponsefield
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the SDK message response field was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Name of the SDK message response field. (Attribute for name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("name")]
		public global::System.String name
		{
			get { return this.GetPropertyValue<global::System.String>("name"); }
			set { this.SetPropertyValue("name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the organization with which the SDK message response field is associated. (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier of the organization with which the SDK message response field is associated. (N:1 Association for organization_sdkmessageresponsefield)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_sdkmessageresponsefield")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_sdkmessageresponsefield
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Position of the Sdk message response field (Attribute for position)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("position")]
		public global::System.Int32? position
		{
			get { return this.GetPropertyValue<global::System.Int32?>("position"); }
			private set { this.SetPropertyValue("position", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Public name of the SDK message response field. (Attribute for publicname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("publicname")]
		public global::System.String publicname
		{
			get { return this.GetPropertyValue<global::System.String>("publicname"); }
			set { this.SetPropertyValue("publicname", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the SDK message response field entity. (Attribute for sdkmessageresponsefieldid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessageresponsefieldid", true)]
		public global::System.Guid sdkmessageresponsefieldid
		{
			get { return this.GetPropertyValue<global::System.Guid>("sdkmessageresponsefieldid"); }
			set { this.SetPropertyValue("sdkmessageresponsefieldid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier of the SDK message response field. (Attribute for sdkmessageresponsefieldidunique)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessageresponsefieldidunique")]
		public global::System.Guid? sdkmessageresponsefieldidunique
		{
			get { return this.GetPropertyValue<global::System.Guid?>("sdkmessageresponsefieldidunique"); }
			set { this.SetPropertyValue("sdkmessageresponsefieldidunique", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the message response with which the SDK message response field is associated. (Attribute for sdkmessageresponseid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessageresponseid")]
		public global::System.Guid? sdkmessageresponseid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("sdkmessageresponseid"); }
			private set { this.SetPropertyValue("sdkmessageresponseid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "sdkmessageresponse"); }
		}

		/// <summary>
		/// Unique identifier of the message response with which the SDK message response field is associated. (N:1 Association for messageresponse_sdkmessageresponsefield)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("sdkmessageresponseid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("messageresponse_sdkmessageresponsefield")]
		public global::NoProblem.Core.DataModel.Xrm.sdkmessageresponse messageresponse_sdkmessageresponsefield
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.sdkmessageresponse>("sdkmessageresponseid", "sdkmessageresponseid"); }
			private set { this.SetRelatedEntity("sdkmessageresponseid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Actual value of the SDK message response field. (Attribute for value)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("value")]
		public global::System.String value
		{
			get { return this.GetPropertyValue<global::System.String>("value"); }
			set { this.SetPropertyValue("value", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// For internal use only. (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

	}
}
