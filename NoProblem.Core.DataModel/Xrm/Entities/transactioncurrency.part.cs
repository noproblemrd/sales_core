﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Currency in which a financial transaction is carried out. (transactioncurrency)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("transactioncurrencyid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("transactioncurrency", "transactioncurrencytransactioncurrencies")]
	public partial class transactioncurrency : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "transactioncurrency";
		private const string _primaryKeyLogicalName = "transactioncurrencyid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public transactioncurrency()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public transactioncurrency(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public transactioncurrency(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public transactioncurrency(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the transaction currency. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the transaction currency. (N:1 Association for lk_transactioncurrencybase_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_transactioncurrencybase_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_transactioncurrencybase_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the transaction currency was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Name of the transaction currency. (Attribute for currencyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("currencyname")]
		public global::System.String currencyname
		{
			get { return this.GetPropertyValue<global::System.String>("currencyname"); }
			set { this.SetPropertyValue("currencyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Symbol for the transaction currency. (Attribute for currencysymbol)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("currencysymbol")]
		public global::System.String currencysymbol
		{
			get { return this.GetPropertyValue<global::System.String>("currencysymbol"); }
			set { this.SetPropertyValue("currencysymbol", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Exchange rate between the transaction currency and the base currency. (Attribute for exchangerate)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("exchangerate")]
		public global::System.Decimal? exchangerate
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("exchangerate"); }
			set { this.SetPropertyValue("exchangerate", value, typeof(global::Microsoft.Crm.Sdk.CrmDecimal)); }
		}

		/// <summary>
		/// Unique identifier of the data import or data migration that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// ISO currency code for the transaction currency. (Attribute for isocurrencycode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("isocurrencycode")]
		public global::System.String isocurrencycode
		{
			get { return this.GetPropertyValue<global::System.String>("isocurrencycode"); }
			set { this.SetPropertyValue("isocurrencycode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the transaction currency. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the transaction currency. (N:1 Association for lk_transactioncurrencybase_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_transactioncurrencybase_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_transactioncurrencybase_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the transaction currency was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier of the organization associated with the transaction currency. (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier of the organization associated with the transaction currency. (N:1 Association for organization_transactioncurrencies)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_transactioncurrencies")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_transactioncurrencies
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Status of the transaction currency. (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the transaction currency. (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the transaction currency. (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		/// Unique identifier of the transaction currency. (Attribute for transactioncurrencyid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("transactioncurrencyid", true)]
		public global::System.Guid transactioncurrencyid
		{
			get { return this.GetPropertyValue<global::System.Guid>("transactioncurrencyid"); }
			set { this.SetPropertyValue("transactioncurrencyid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// For internal use only. (Attribute for uniquedscid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("uniquedscid")]
		public global::System.Guid? uniquedscid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("uniquedscid"); }
			private set { this.SetPropertyValue("uniquedscid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for basecurrency_organization)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("basecurrency_organization", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.organization> basecurrency_organization
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.organization>("transactioncurrencyid", "basecurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_account)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_account", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.account> transactioncurrency_account
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.account>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_annualfiscalcalendar)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_annualfiscalcalendar", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.annualfiscalcalendar> transactioncurrency_annualfiscalcalendar
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.annualfiscalcalendar>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> TransactionCurrency_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("transactioncurrencyid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_campaign)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_campaign", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.campaign> transactioncurrency_campaign
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.campaign>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_campaignactivity)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_campaignactivity", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.campaignactivity> transactioncurrency_campaignactivity
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.campaignactivity>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_competitor)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_competitor", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.competitor> transactioncurrency_competitor
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.competitor>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_contact)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_contact", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.contact> transactioncurrency_contact
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.contact>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_contract)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_contract", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.contract> transactioncurrency_contract
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.contract>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_contractdetail)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_contractdetail", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.contractdetail> transactioncurrency_contractdetail
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.contractdetail>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_discount)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_discount", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.discount> transactioncurrency_discount
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.discount>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_discounttype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_discounttype", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.discounttype> transactioncurrency_discounttype
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.discounttype>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_fixedmonthlyfiscalcalendar)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_fixedmonthlyfiscalcalendar", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.fixedmonthlyfiscalcalendar> transactioncurrency_fixedmonthlyfiscalcalendar
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.fixedmonthlyfiscalcalendar>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_Incident)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_Incident", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.incident> TransactionCurrency_Incident
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.incident>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_invoice)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_invoice", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.invoice> transactioncurrency_invoice
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.invoice>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_invoicedetail)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_invoicedetail", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.invoicedetail> transactioncurrency_invoicedetail
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.invoicedetail>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_lead)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_lead", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.lead> transactioncurrency_lead
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.lead>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_list)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_list", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.list> transactioncurrency_list
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.list>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_monthlyfiscalcalendar)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_monthlyfiscalcalendar", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.monthlyfiscalcalendar> transactioncurrency_monthlyfiscalcalendar
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.monthlyfiscalcalendar>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_accountexpertise)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_accountexpertise", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_accountexpertise> TransactionCurrency_new_accountexpertise
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_accountexpertise>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_balancerow)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_balancerow", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_balancerow> TransactionCurrency_new_balancerow
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_balancerow>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_bid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_bid", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_bid> TransactionCurrency_new_bid
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_bid>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_collectionevent)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_collectionevent", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_collectionevent> TransactionCurrency_new_collectionevent
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_collectionevent>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_communicationprice)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_communicationprice", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_communicationprice> TransactionCurrency_new_communicationprice
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_communicationprice>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_incidentaccount)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_incidentaccount", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_incidentaccount> TransactionCurrency_new_incidentaccount
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_incidentaccount>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_obligation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_obligation", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_obligation> TransactionCurrency_new_obligation
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_obligation>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_primaryexpertise)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_primaryexpertise", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_primaryexpertise> TransactionCurrency_new_primaryexpertise
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_primaryexpertise>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_supplierpricing)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_supplierpricing", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_supplierpricing> TransactionCurrency_new_supplierpricing
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_supplierpricing>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_wibiyacashout)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_wibiyacashout", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_wibiyacashout> TransactionCurrency_new_wibiyacashout
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_wibiyacashout>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for TransactionCurrency_new_wibiyaconfiguration)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("TransactionCurrency_new_wibiyaconfiguration", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_wibiyaconfiguration> TransactionCurrency_new_wibiyaconfiguration
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_wibiyaconfiguration>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_opportunity)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_opportunity", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.opportunity> transactioncurrency_opportunity
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.opportunity>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_opportunityclose)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_opportunityclose", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.opportunityclose> transactioncurrency_opportunityclose
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.opportunityclose>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_opportunityproduct)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_opportunityproduct", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.opportunityproduct> transactioncurrency_opportunityproduct
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.opportunityproduct>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_pricelevel)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_pricelevel", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.pricelevel> transactioncurrency_pricelevel
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.pricelevel>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_product)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_product", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.product> transactioncurrency_product
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.product>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_productpricelevel)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_productpricelevel", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.productpricelevel> transactioncurrency_productpricelevel
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.productpricelevel>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_quarterlyfiscalcalendar)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_quarterlyfiscalcalendar", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.quarterlyfiscalcalendar> transactioncurrency_quarterlyfiscalcalendar
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.quarterlyfiscalcalendar>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_quote)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_quote", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.quote> transactioncurrency_quote
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.quote>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_quotedetail)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_quotedetail", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.quotedetail> transactioncurrency_quotedetail
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.quotedetail>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_salesorder)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_salesorder", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.salesorder> transactioncurrency_salesorder
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.salesorder>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_salesorderdetail)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_salesorderdetail", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.salesorderdetail> transactioncurrency_salesorderdetail
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.salesorderdetail>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_semiannualfiscalcalendar)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_semiannualfiscalcalendar", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.semiannualfiscalcalendar> transactioncurrency_semiannualfiscalcalendar
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.semiannualfiscalcalendar>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_userfiscalcalendar)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_userfiscalcalendar", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.userfiscalcalendar> transactioncurrency_userfiscalcalendar
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.userfiscalcalendar>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		/// <summary>
		///  (1:N Association for transactioncurrency_usersettings)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("transactioncurrency_usersettings", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.usersettings> transactioncurrency_usersettings
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.usersettings>("transactioncurrencyid", "transactioncurrencyid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyyominame")]
		public global::System.String createdbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyyominame"); }
			private set { this.SetPropertyValue("createdbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyyominame")]
		public global::System.String modifiedbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyyominame"); }
			private set { this.SetPropertyValue("modifiedbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
