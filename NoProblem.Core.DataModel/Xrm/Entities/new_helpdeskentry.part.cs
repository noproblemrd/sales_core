﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (new_helpdeskentry)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("new_helpdeskentryid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("new_helpdeskentry", "new_helpdeskentrynew_helpdeskentries")]
	[global::System.Data.Services.IgnoreProperties("ownerid")]
	public partial class new_helpdeskentry : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "new_helpdeskentry";
		private const string _primaryKeyLogicalName = "new_helpdeskentryid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public new_helpdeskentry()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public new_helpdeskentry(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public new_helpdeskentry(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public new_helpdeskentry(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (N:1 Association for lk_new_helpdeskentry_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_helpdeskentry_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_helpdeskentry_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Sequence number of the import that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (N:1 Association for lk_new_helpdeskentry_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_helpdeskentry_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_helpdeskentry_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier for Account associated with Help Desk Entry. (Attribute for new_createdbyid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_createdbyid")]
		public global::System.Guid? new_createdbyid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_createdbyid"); }
			set { this.SetPropertyValue("new_createdbyid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "account"); }
		}

		/// <summary>
		/// Unique identifier for Account associated with Help Desk Entry. (N:1 Association for new_createdby_helpdeskentry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_createdbyid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_createdby_helpdeskentry")]
		public global::NoProblem.Core.DataModel.Xrm.account new_createdby_helpdeskentry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.account>("new_createdbyid", "accountid"); }
			set { this.SetRelatedEntity("new_createdbyid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_description)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_description")]
		public global::System.String new_description
		{
			get { return this.GetPropertyValue<global::System.String>("new_description"); }
			set { this.SetPropertyValue("new_description", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_followup)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_followup")]
		public global::System.DateTime? new_followup
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("new_followup"); }
			set { this.SetPropertyValue("new_followup", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier for entity instances (Attribute for new_helpdeskentryid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_helpdeskentryid", true)]
		public global::System.Guid new_helpdeskentryid
		{
			get { return this.GetPropertyValue<global::System.Guid>("new_helpdeskentryid"); }
			set { this.SetPropertyValue("new_helpdeskentryid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier for Help Desk Severity associated with Help Desk Entry. (Attribute for new_helpdeskseverityid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_helpdeskseverityid")]
		public global::System.Guid? new_helpdeskseverityid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_helpdeskseverityid"); }
			set { this.SetPropertyValue("new_helpdeskseverityid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_helpdeskseverity"); }
		}

		/// <summary>
		/// Unique identifier for Help Desk Severity associated with Help Desk Entry. (N:1 Association for new_helpdeskseverity_helpdeskentry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_helpdeskseverityid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_helpdeskseverity_helpdeskentry")]
		public global::NoProblem.Core.DataModel.Xrm.new_helpdeskseverity new_helpdeskseverity_helpdeskentry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_helpdeskseverity>("new_helpdeskseverityid", "new_helpdeskseverityid"); }
			set { this.SetRelatedEntity("new_helpdeskseverityid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier for Help Desk Status associated with Help Desk Entry. (Attribute for new_helpdeskstatusid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_helpdeskstatusid")]
		public global::System.Guid? new_helpdeskstatusid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_helpdeskstatusid"); }
			set { this.SetPropertyValue("new_helpdeskstatusid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_helpdeskstatus"); }
		}

		/// <summary>
		/// Unique identifier for Help Desk Status associated with Help Desk Entry. (N:1 Association for new_helpdeskstatus_helpdeskentry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_helpdeskstatusid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_helpdeskstatus_helpdeskentry")]
		public global::NoProblem.Core.DataModel.Xrm.new_helpdeskstatus new_helpdeskstatus_helpdeskentry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_helpdeskstatus>("new_helpdeskstatusid", "new_helpdeskstatusid"); }
			set { this.SetRelatedEntity("new_helpdeskstatusid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier for Help Desk Type associated with Help Desk Entry. (Attribute for new_helpdesktypeid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_helpdesktypeid")]
		public global::System.Guid? new_helpdesktypeid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_helpdesktypeid"); }
			set { this.SetPropertyValue("new_helpdesktypeid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_helpdesktype"); }
		}

		/// <summary>
		/// Unique identifier for Help Desk Type associated with Help Desk Entry. (N:1 Association for new_helpdesktype_helpdeskentry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_helpdesktypeid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_helpdesktype_helpdeskentry")]
		public global::NoProblem.Core.DataModel.Xrm.new_helpdesktype new_helpdesktype_helpdeskentry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_helpdesktype>("new_helpdesktypeid", "new_helpdesktypeid"); }
			set { this.SetRelatedEntity("new_helpdesktypeid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_initiatorid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_initiatorid")]
		public global::System.String new_initiatorid
		{
			get { return this.GetPropertyValue<global::System.String>("new_initiatorid"); }
			set { this.SetPropertyValue("new_initiatorid", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// The name of the custom entity. (Attribute for new_name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_name")]
		public global::System.String new_name
		{
			get { return this.GetPropertyValue<global::System.String>("new_name"); }
			set { this.SetPropertyValue("new_name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for Account associated with Help Desk Entry. (Attribute for new_ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_ownerid")]
		public global::System.Guid? new_ownerid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_ownerid"); }
			set { this.SetPropertyValue("new_ownerid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "account"); }
		}

		/// <summary>
		/// Unique identifier for Account associated with Help Desk Entry. (N:1 Association for new_owner_helpdeskentry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_ownerid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_owner_helpdeskentry")]
		public global::NoProblem.Core.DataModel.Xrm.account new_owner_helpdeskentry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.account>("new_ownerid", "accountid"); }
			set { this.SetRelatedEntity("new_ownerid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_regardingobjectid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_regardingobjectid")]
		public global::System.String new_regardingobjectid
		{
			get { return this.GetPropertyValue<global::System.String>("new_regardingobjectid"); }
			set { this.SetPropertyValue("new_regardingobjectid", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_title)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_title")]
		public global::System.String new_title
		{
			get { return this.GetPropertyValue<global::System.String>("new_title"); }
			set { this.SetPropertyValue("new_title", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("ownerid")]
		public global::Microsoft.Crm.Sdk.Owner ownerid
		{
			get { return this.GetPropertyValue<global::Microsoft.Crm.Sdk.Owner>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		public global::System.Guid? user_new_helpdeskentry_id
		{
			get { return this.GetPropertyValue<global::System.Guid?>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner), "systemuser"); }
		}

		/// <summary>
		/// Owner Id (N:1 Association for user_new_helpdeskentry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_helpdeskentry")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_helpdeskentry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("ownerid", "systemuserid"); }
			set { this.SetRelatedEntity("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (Attribute for owningbusinessunit)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owningbusinessunit")]
		public global::System.Guid? owningbusinessunit
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owningbusinessunit"); }
			private set { this.SetPropertyValue("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "businessunit"); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (N:1 Association for business_unit_new_helpdeskentry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owningbusinessunit")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("business_unit_new_helpdeskentry")]
		public global::NoProblem.Core.DataModel.Xrm.businessunit business_unit_new_helpdeskentry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.businessunit>("owningbusinessunit", "businessunitid"); }
			private set { this.SetRelatedEntity("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Status of the Help Desk Entry (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the Help Desk Entry (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the Help Desk Entry (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		///  (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for new_helpdeskentry_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_helpdeskentry_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> new_helpdeskentry_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("new_helpdeskentryid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_helpdeskentry_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_helpdeskentry_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> new_helpdeskentry_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("new_helpdeskentryid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_helpdeskentry_helpdesknote)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_helpdeskentry_helpdesknote", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_helpdesknote> new_helpdeskentry_helpdesknote
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_helpdesknote>("new_helpdeskentryid", "new_helpdeskentryid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_createdbyiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_createdbyiddsc")]
		public global::System.Int32? new_createdbyiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_createdbyiddsc"); }
			private set { this.SetPropertyValue("new_createdbyiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_createdbyidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_createdbyidname")]
		public global::System.String new_createdbyidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_createdbyidname"); }
			private set { this.SetPropertyValue("new_createdbyidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_createdbyidyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_createdbyidyominame")]
		public global::System.String new_createdbyidyominame
		{
			get { return this.GetPropertyValue<global::System.String>("new_createdbyidyominame"); }
			private set { this.SetPropertyValue("new_createdbyidyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_helpdeskseverityiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_helpdeskseverityiddsc")]
		public global::System.Int32? new_helpdeskseverityiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_helpdeskseverityiddsc"); }
			private set { this.SetPropertyValue("new_helpdeskseverityiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_helpdeskseverityidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_helpdeskseverityidname")]
		public global::System.String new_helpdeskseverityidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_helpdeskseverityidname"); }
			private set { this.SetPropertyValue("new_helpdeskseverityidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_helpdeskstatusiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_helpdeskstatusiddsc")]
		public global::System.Int32? new_helpdeskstatusiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_helpdeskstatusiddsc"); }
			private set { this.SetPropertyValue("new_helpdeskstatusiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_helpdeskstatusidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_helpdeskstatusidname")]
		public global::System.String new_helpdeskstatusidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_helpdeskstatusidname"); }
			private set { this.SetPropertyValue("new_helpdeskstatusidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_helpdesktypeiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_helpdesktypeiddsc")]
		public global::System.Int32? new_helpdesktypeiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_helpdesktypeiddsc"); }
			private set { this.SetPropertyValue("new_helpdesktypeiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_helpdesktypeidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_helpdesktypeidname")]
		public global::System.String new_helpdesktypeidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_helpdesktypeidname"); }
			private set { this.SetPropertyValue("new_helpdesktypeidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_owneriddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_owneriddsc")]
		public global::System.Int32? new_owneriddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_owneriddsc"); }
			private set { this.SetPropertyValue("new_owneriddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_owneridname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_owneridname")]
		public global::System.String new_owneridname
		{
			get { return this.GetPropertyValue<global::System.String>("new_owneridname"); }
			private set { this.SetPropertyValue("new_owneridname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_owneridyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_owneridyominame")]
		public global::System.String new_owneridyominame
		{
			get { return this.GetPropertyValue<global::System.String>("new_owneridyominame"); }
			private set { this.SetPropertyValue("new_owneridyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for owneriddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneriddsc")]
		public global::System.Int32? owneriddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("owneriddsc"); }
			private set { this.SetPropertyValue("owneriddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Name of the owner (Attribute for owneridname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridname")]
		public global::System.String owneridname
		{
			get { return this.GetPropertyValue<global::System.String>("owneridname"); }
			private set { this.SetPropertyValue("owneridname", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Owner Id Type (Attribute for owneridtype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridtype")]
		public global::System.String owneridtype
		{
			get { return this.GetPropertyValue<global::System.String>("owneridtype"); }
			set { this.SetPropertyValue("owneridtype", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (Attribute for owninguser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owninguser")]
		public global::System.Guid? owninguser
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owninguser"); }
			set { this.SetPropertyValue("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (N:1 Association for user_new_helpdeskentry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owninguser")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_helpdeskentry")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_helpdeskentry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("owninguser", "systemuserid"); }
			set { this.SetRelatedEntity("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
