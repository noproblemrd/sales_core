﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// For internal use only. (systemuserprincipals)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("systemuserprincipalid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("systemuserprincipals", "systemuserprincipalses")]
	public partial class systemuserprincipals : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "systemuserprincipals";
		private const string _primaryKeyLogicalName = "systemuserprincipalid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public systemuserprincipals()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public systemuserprincipals(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public systemuserprincipals(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public systemuserprincipals(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// For internal use only. (Attribute for principalid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("principalid")]
		public global::System.Guid? principalid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("principalid"); }
			set { this.SetPropertyValue("principalid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// For internal use only. (Attribute for systemuserid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("systemuserid")]
		public global::System.Guid? systemuserid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("systemuserid"); }
			set { this.SetPropertyValue("systemuserid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier), "systemuser"); }
		}

		/// <summary>
		/// For internal use only. (N:1 Association for sup_principalid_systemuser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("systemuserid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("sup_principalid_systemuser")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser sup_principalid_systemuser
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("systemuserid", "systemuserid"); }
			set { this.SetRelatedEntity("systemuserid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// For internal use only. (Attribute for systemuserprincipalid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("systemuserprincipalid", true)]
		public global::System.Guid systemuserprincipalid
		{
			get { return this.GetPropertyValue<global::System.Guid>("systemuserprincipalid"); }
			set { this.SetPropertyValue("systemuserprincipalid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

	}
}
