﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Filter that defines which SDK messages are valid for each type of entity. (sdkmessagefilter)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("sdkmessagefilterid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("sdkmessagefilter", "sdkmessagefilters")]
	public partial class sdkmessagefilter : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "sdkmessagefilter";
		private const string _primaryKeyLogicalName = "sdkmessagefilterid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public sdkmessagefilter()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public sdkmessagefilter(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public sdkmessagefilter(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public sdkmessagefilter(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Identifies where a method will be exposed. 0 - Server, 1 - Client, 2 - both. (Attribute for availability)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("availability")]
		public global::System.Int32? availability
		{
			get { return this.GetPropertyValue<global::System.Int32?>("availability"); }
			set { this.SetPropertyValue("availability", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who created the SDK message filter. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the SDK message filter. (N:1 Association for createdby_sdkmessagefilter)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("createdby_sdkmessagefilter")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser createdby_sdkmessagefilter
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the SDK message filter was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Customization level of the SDK message filter. (Attribute for customizationlevel)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("customizationlevel")]
		public global::System.Int32? customizationlevel
		{
			get { return this.GetPropertyValue<global::System.Int32?>("customizationlevel"); }
			private set { this.SetPropertyValue("customizationlevel", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Indicates whether a custom SDK message processing step is allowed. (Attribute for iscustomprocessingstepallowed)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("iscustomprocessingstepallowed")]
		public global::System.Boolean? iscustomprocessingstepallowed
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("iscustomprocessingstepallowed"); }
			set { this.SetPropertyValue("iscustomprocessingstepallowed", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the SDK message filter. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the SDK message filter. (N:1 Association for modifiedby_sdkmessagefilter)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("modifiedby_sdkmessagefilter")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser modifiedby_sdkmessagefilter
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the SDK message filter was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier of the organization with which the SDK message filter is associated. (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier of the organization with which the SDK message filter is associated. (N:1 Association for organization_sdkmessagefilter)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_sdkmessagefilter")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_sdkmessagefilter
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Type of entity with which the SDK message filter is primarily associated. (Attribute for primaryobjecttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("primaryobjecttypecode", "primaryobjecttypecodeLabel")]
		public global::System.String primaryobjecttypecode
		{
			get { return this.GetPropertyValue<global::System.String>("primaryobjecttypecode"); }
			private set { this.SetPropertyValue("primaryobjecttypecode", value, typeof(global::Microsoft.Crm.Sdk.EntityNameReference)); }
		}

		/// <summary>
		/// Type of entity with which the SDK message filter is primarily associated. (Label for primaryobjecttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("primaryobjecttypecode")]
		public global::System.String primaryobjecttypecodeLabel
		{
			get { return this.GetPropertyLabel("primaryobjecttypecode"); }
		}

		/// <summary>
		/// Unique identifier of the SDK message filter entity. (Attribute for sdkmessagefilterid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessagefilterid", true)]
		public global::System.Guid sdkmessagefilterid
		{
			get { return this.GetPropertyValue<global::System.Guid>("sdkmessagefilterid"); }
			set { this.SetPropertyValue("sdkmessagefilterid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier of the SDK message filter. (Attribute for sdkmessagefilteridunique)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessagefilteridunique")]
		public global::System.Guid? sdkmessagefilteridunique
		{
			get { return this.GetPropertyValue<global::System.Guid?>("sdkmessagefilteridunique"); }
			set { this.SetPropertyValue("sdkmessagefilteridunique", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the related SDK message. (Attribute for sdkmessageid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessageid")]
		public global::System.Guid? sdkmessageid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("sdkmessageid"); }
			set { this.SetPropertyValue("sdkmessageid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "sdkmessage"); }
		}

		/// <summary>
		/// Unique identifier of the related SDK message. (N:1 Association for sdkmessageid_sdkmessagefilter)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("sdkmessageid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("sdkmessageid_sdkmessagefilter")]
		public global::NoProblem.Core.DataModel.Xrm.sdkmessage sdkmessageid_sdkmessagefilter
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.sdkmessage>("sdkmessageid", "sdkmessageid"); }
			set { this.SetRelatedEntity("sdkmessageid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Type of entity with which the SDK message filter is secondarily associated. (Attribute for secondaryobjecttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("secondaryobjecttypecode", "secondaryobjecttypecodeLabel")]
		public global::System.String secondaryobjecttypecode
		{
			get { return this.GetPropertyValue<global::System.String>("secondaryobjecttypecode"); }
			private set { this.SetPropertyValue("secondaryobjecttypecode", value, typeof(global::Microsoft.Crm.Sdk.EntityNameReference)); }
		}

		/// <summary>
		/// Type of entity with which the SDK message filter is secondarily associated. (Label for secondaryobjecttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("secondaryobjecttypecode")]
		public global::System.String secondaryobjecttypecodeLabel
		{
			get { return this.GetPropertyLabel("secondaryobjecttypecode"); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for sdkmessagefilterid_sdkmessageprocessingstep)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("sdkmessagefilterid_sdkmessageprocessingstep", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.sdkmessageprocessingstep> sdkmessagefilterid_sdkmessageprocessingstep
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.sdkmessageprocessingstep>("sdkmessagefilterid", "sdkmessagefilterid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for primaryobjecttypecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("primaryobjecttypecodename")]
		public global::System.Object primaryobjecttypecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("primaryobjecttypecodename"); }
			private set { this.SetPropertyValue("primaryobjecttypecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for secondaryobjecttypecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("secondaryobjecttypecodename")]
		public global::System.Object secondaryobjecttypecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("secondaryobjecttypecodename"); }
			private set { this.SetPropertyValue("secondaryobjecttypecodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
