﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (new_homesecuritycasedata)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("new_homesecuritycasedataid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("new_homesecuritycasedata", "new_homesecuritycasedatas")]
	[global::System.Data.Services.IgnoreProperties("ownerid")]
	public partial class new_homesecuritycasedata : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "new_homesecuritycasedata";
		private const string _primaryKeyLogicalName = "new_homesecuritycasedataid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public new_homesecuritycasedata()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public new_homesecuritycasedata(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public new_homesecuritycasedata(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public new_homesecuritycasedata(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (N:1 Association for lk_new_homesecuritycasedata_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_homesecuritycasedata_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_homesecuritycasedata_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Sequence number of the import that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (N:1 Association for lk_new_homesecuritycasedata_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_homesecuritycasedata_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_homesecuritycasedata_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for new_creditranking)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_creditranking", "new_creditrankingLabel")]
		public global::System.Int32? new_creditranking
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_creditranking"); }
			set { this.SetPropertyValue("new_creditranking", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		///  (Label for new_creditranking)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_creditranking")]
		public global::System.String new_creditrankingLabel
		{
			get { return this.GetPropertyLabel("new_creditranking"); }
		}

		/// <summary>
		/// Unique identifier for entity instances (Attribute for new_homesecuritycasedataid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_homesecuritycasedataid", true)]
		public global::System.Guid new_homesecuritycasedataid
		{
			get { return this.GetPropertyValue<global::System.Guid>("new_homesecuritycasedataid"); }
			set { this.SetPropertyValue("new_homesecuritycasedataid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// The name of the custom entity. (Attribute for new_name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_name")]
		public global::System.String new_name
		{
			get { return this.GetPropertyValue<global::System.String>("new_name"); }
			set { this.SetPropertyValue("new_name", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_ownproperty)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_ownproperty")]
		public global::System.Boolean? new_ownproperty
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_ownproperty"); }
			set { this.SetPropertyValue("new_ownproperty", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for new_propertytype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_propertytype", "new_propertytypeLabel")]
		public global::System.Int32? new_propertytype
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_propertytype"); }
			set { this.SetPropertyValue("new_propertytype", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		///  (Label for new_propertytype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_propertytype")]
		public global::System.String new_propertytypeLabel
		{
			get { return this.GetPropertyLabel("new_propertytype"); }
		}

		/// <summary>
		///  (Attribute for new_timeframe)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_timeframe", "new_timeframeLabel")]
		public global::System.Int32? new_timeframe
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_timeframe"); }
			set { this.SetPropertyValue("new_timeframe", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		///  (Label for new_timeframe)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_timeframe")]
		public global::System.String new_timeframeLabel
		{
			get { return this.GetPropertyLabel("new_timeframe"); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("ownerid")]
		public global::Microsoft.Crm.Sdk.Owner ownerid
		{
			get { return this.GetPropertyValue<global::Microsoft.Crm.Sdk.Owner>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		public global::System.Guid? user_new_homesecuritycasedata_id
		{
			get { return this.GetPropertyValue<global::System.Guid?>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner), "systemuser"); }
		}

		/// <summary>
		/// Owner Id (N:1 Association for user_new_homesecuritycasedata)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_homesecuritycasedata")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_homesecuritycasedata
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("ownerid", "systemuserid"); }
			set { this.SetRelatedEntity("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (Attribute for owningbusinessunit)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owningbusinessunit")]
		public global::System.Guid? owningbusinessunit
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owningbusinessunit"); }
			private set { this.SetPropertyValue("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "businessunit"); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (N:1 Association for business_unit_new_homesecuritycasedata)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owningbusinessunit")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("business_unit_new_homesecuritycasedata")]
		public global::NoProblem.Core.DataModel.Xrm.businessunit business_unit_new_homesecuritycasedata
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.businessunit>("owningbusinessunit", "businessunitid"); }
			private set { this.SetRelatedEntity("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Status of the Home Security Case Data (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the Home Security Case Data (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the Home Security Case Data (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		///  (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for new_homesecuritycasedata_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_homesecuritycasedata_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> new_homesecuritycasedata_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("new_homesecuritycasedataid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_homesecuritycasedata_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_homesecuritycasedata_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> new_homesecuritycasedata_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("new_homesecuritycasedataid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_homesecuritycasedata_incident)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_homesecuritycasedata_incident", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.incident> new_homesecuritycasedata_incident
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.incident>("new_homesecuritycasedataid", "new_homesecuritycasedataid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_creditrankingname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_creditrankingname")]
		public global::System.Object new_creditrankingname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_creditrankingname"); }
			private set { this.SetPropertyValue("new_creditrankingname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_ownpropertyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_ownpropertyname")]
		public global::System.Object new_ownpropertyname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_ownpropertyname"); }
			private set { this.SetPropertyValue("new_ownpropertyname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_propertytypename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_propertytypename")]
		public global::System.Object new_propertytypename
		{
			get { return this.GetPropertyValue<global::System.Object>("new_propertytypename"); }
			private set { this.SetPropertyValue("new_propertytypename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_timeframename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_timeframename")]
		public global::System.Object new_timeframename
		{
			get { return this.GetPropertyValue<global::System.Object>("new_timeframename"); }
			private set { this.SetPropertyValue("new_timeframename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for owneriddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneriddsc")]
		public global::System.Int32? owneriddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("owneriddsc"); }
			private set { this.SetPropertyValue("owneriddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Name of the owner (Attribute for owneridname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridname")]
		public global::System.String owneridname
		{
			get { return this.GetPropertyValue<global::System.String>("owneridname"); }
			private set { this.SetPropertyValue("owneridname", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Owner Id Type (Attribute for owneridtype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridtype")]
		public global::System.String owneridtype
		{
			get { return this.GetPropertyValue<global::System.String>("owneridtype"); }
			set { this.SetPropertyValue("owneridtype", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (Attribute for owninguser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owninguser")]
		public global::System.Guid? owninguser
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owninguser"); }
			set { this.SetPropertyValue("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (N:1 Association for user_new_homesecuritycasedata)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owninguser")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_homesecuritycasedata")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_homesecuritycasedata
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("owninguser", "systemuserid"); }
			set { this.SetRelatedEntity("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
