﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (new_obligation)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("new_obligationid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("new_obligation", "new_obligations")]
	[global::System.Data.Services.IgnoreProperties("ownerid")]
	public partial class new_obligation : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "new_obligation";
		private const string _primaryKeyLogicalName = "new_obligationid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public new_obligation()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public new_obligation(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public new_obligation(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public new_obligation(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (N:1 Association for lk_new_obligation_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_obligation_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_obligation_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Exchange rate for the currency associated with the entity with respect to the base currency. (Attribute for exchangerate)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("exchangerate")]
		public global::System.Decimal? exchangerate
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("exchangerate"); }
			private set { this.SetPropertyValue("exchangerate", value, typeof(global::Microsoft.Crm.Sdk.CrmDecimal)); }
		}

		/// <summary>
		/// Sequence number of the import that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (N:1 Association for lk_new_obligation_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_obligation_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_obligation_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier for Account associated with Obligation. (Attribute for new_accountid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_accountid")]
		public global::System.Guid? new_accountid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_accountid"); }
			set { this.SetPropertyValue("new_accountid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "account"); }
		}

		/// <summary>
		/// Unique identifier for Account associated with Obligation. (N:1 Association for new_account_new_obligation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_accountid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_account_new_obligation")]
		public global::NoProblem.Core.DataModel.Xrm.account new_account_new_obligation
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.account>("new_accountid", "accountid"); }
			set { this.SetRelatedEntity("new_accountid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_amount)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_amount")]
		public global::System.Int32? new_amount
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_amount"); }
			set { this.SetPropertyValue("new_amount", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier for collection event associated with Obligation. (Attribute for new_collectioneventid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_collectioneventid")]
		public global::System.Guid? new_collectioneventid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_collectioneventid"); }
			set { this.SetPropertyValue("new_collectioneventid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_collectionevent"); }
		}

		/// <summary>
		/// Unique identifier for collection event associated with Obligation. (N:1 Association for new_new_collectionevent_new_obligation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_collectioneventid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_new_collectionevent_new_obligation")]
		public global::NoProblem.Core.DataModel.Xrm.new_collectionevent new_new_collectionevent_new_obligation
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_collectionevent>("new_collectioneventid", "new_collectioneventid"); }
			set { this.SetRelatedEntity("new_collectioneventid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_description)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_description")]
		public global::System.String new_description
		{
			get { return this.GetPropertyValue<global::System.String>("new_description"); }
			set { this.SetPropertyValue("new_description", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_discount)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_discount")]
		public global::System.Int32? new_discount
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_discount"); }
			set { this.SetPropertyValue("new_discount", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier for Incident Account associated with Obligation. (Attribute for new_incidentaccountid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentaccountid")]
		public global::System.Guid? new_incidentaccountid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_incidentaccountid"); }
			set { this.SetPropertyValue("new_incidentaccountid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_incidentaccount"); }
		}

		/// <summary>
		/// Unique identifier for Incident Account associated with Obligation. (N:1 Association for new_new_incidentaccount_new_obligation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_incidentaccountid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_new_incidentaccount_new_obligation")]
		public global::NoProblem.Core.DataModel.Xrm.new_incidentaccount new_new_incidentaccount_new_obligation
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_incidentaccount>("new_incidentaccountid", "new_incidentaccountid"); }
			set { this.SetRelatedEntity("new_incidentaccountid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_incidentprice)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentprice")]
		public global::System.Decimal? new_incidentprice
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("new_incidentprice"); }
			set { this.SetPropertyValue("new_incidentprice", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Value of the incident price in base currency. (Attribute for new_incidentprice_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentprice_base")]
		public global::System.Decimal? new_incidentprice_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("new_incidentprice_base"); }
			private set { this.SetPropertyValue("new_incidentprice_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// The name of the custom entity. (Attribute for new_name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_name")]
		public global::System.String new_name
		{
			get { return this.GetPropertyValue<global::System.String>("new_name"); }
			set { this.SetPropertyValue("new_name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for entity instances (Attribute for new_obligationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_obligationid", true)]
		public global::System.Guid new_obligationid
		{
			get { return this.GetPropertyValue<global::System.Guid>("new_obligationid"); }
			set { this.SetPropertyValue("new_obligationid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		///  (Attribute for new_obligationnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_obligationnumber")]
		public global::System.Int32? new_obligationnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_obligationnumber"); }
			set { this.SetPropertyValue("new_obligationnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_sequence)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_sequence")]
		public global::System.String new_sequence
		{
			get { return this.GetPropertyValue<global::System.String>("new_sequence"); }
			set { this.SetPropertyValue("new_sequence", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for Supplier Pricing associated with Obligation. (Attribute for new_supplierpricingid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_supplierpricingid")]
		public global::System.Guid? new_supplierpricingid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_supplierpricingid"); }
			set { this.SetPropertyValue("new_supplierpricingid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_supplierpricing"); }
		}

		/// <summary>
		/// Unique identifier for Supplier Pricing associated with Obligation. (N:1 Association for new_new_supplierpricing_new_obligation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_supplierpricingid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_new_supplierpricing_new_obligation")]
		public global::NoProblem.Core.DataModel.Xrm.new_supplierpricing new_new_supplierpricing_new_obligation
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_supplierpricing>("new_supplierpricingid", "new_supplierpricingid"); }
			set { this.SetRelatedEntity("new_supplierpricingid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_topay)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_topay")]
		public global::System.Decimal? new_topay
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("new_topay"); }
			set { this.SetPropertyValue("new_topay", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Value of the topay in base currency. (Attribute for new_topay_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_topay_base")]
		public global::System.Decimal? new_topay_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("new_topay_base"); }
			private set { this.SetPropertyValue("new_topay_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		///  (Attribute for new_total)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_total")]
		public global::System.Decimal? new_total
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("new_total"); }
			set { this.SetPropertyValue("new_total", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Value of the Total in base currency. (Attribute for new_total_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_total_base")]
		public global::System.Decimal? new_total_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("new_total_base"); }
			private set { this.SetPropertyValue("new_total_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		///  (Attribute for new_totalobligations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_totalobligations")]
		public global::System.Int32? new_totalobligations
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_totalobligations"); }
			set { this.SetPropertyValue("new_totalobligations", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("ownerid")]
		public global::Microsoft.Crm.Sdk.Owner ownerid
		{
			get { return this.GetPropertyValue<global::Microsoft.Crm.Sdk.Owner>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		public global::System.Guid? user_new_obligation_id
		{
			get { return this.GetPropertyValue<global::System.Guid?>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner), "systemuser"); }
		}

		/// <summary>
		/// Owner Id (N:1 Association for user_new_obligation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_obligation")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_obligation
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("ownerid", "systemuserid"); }
			set { this.SetRelatedEntity("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (Attribute for owningbusinessunit)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owningbusinessunit")]
		public global::System.Guid? owningbusinessunit
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owningbusinessunit"); }
			private set { this.SetPropertyValue("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "businessunit"); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (N:1 Association for business_unit_new_obligation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owningbusinessunit")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("business_unit_new_obligation")]
		public global::NoProblem.Core.DataModel.Xrm.businessunit business_unit_new_obligation
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.businessunit>("owningbusinessunit", "businessunitid"); }
			private set { this.SetRelatedEntity("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Status of the Obligation (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the Obligation (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the Obligation (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		///  (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the currency associated with the entity. (Attribute for transactioncurrencyid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("transactioncurrencyid")]
		public global::System.Guid? transactioncurrencyid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("transactioncurrencyid"); }
			set { this.SetPropertyValue("transactioncurrencyid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "transactioncurrency"); }
		}

		/// <summary>
		/// Unique identifier of the currency associated with the entity. (N:1 Association for TransactionCurrency_new_obligation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("transactioncurrencyid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("TransactionCurrency_new_obligation")]
		public global::NoProblem.Core.DataModel.Xrm.transactioncurrency TransactionCurrency_new_obligation
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.transactioncurrency>("transactioncurrencyid", "transactioncurrencyid"); }
			set { this.SetRelatedEntity("transactioncurrencyid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for new_obligation_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_obligation_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> new_obligation_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("new_obligationid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_obligation_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_obligation_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> new_obligation_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("new_obligationid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_obligation_DuplicateBaseRecord)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_obligation_DuplicateBaseRecord", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.duplicaterecord> new_obligation_DuplicateBaseRecord
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.duplicaterecord>("new_obligationid", "baserecordid"); }
		}

		/// <summary>
		///  (1:N Association for new_obligation_DuplicateMatchingRecord)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_obligation_DuplicateMatchingRecord", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.duplicaterecord> new_obligation_DuplicateMatchingRecord
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.duplicaterecord>("new_obligationid", "duplicaterecordid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_accountiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_accountiddsc")]
		public global::System.Int32? new_accountiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_accountiddsc"); }
			private set { this.SetPropertyValue("new_accountiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_accountidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_accountidname")]
		public global::System.String new_accountidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_accountidname"); }
			private set { this.SetPropertyValue("new_accountidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_accountidyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_accountidyominame")]
		public global::System.String new_accountidyominame
		{
			get { return this.GetPropertyValue<global::System.String>("new_accountidyominame"); }
			private set { this.SetPropertyValue("new_accountidyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_collectioneventiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_collectioneventiddsc")]
		public global::System.Int32? new_collectioneventiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_collectioneventiddsc"); }
			private set { this.SetPropertyValue("new_collectioneventiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_collectioneventidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_collectioneventidname")]
		public global::System.String new_collectioneventidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_collectioneventidname"); }
			private set { this.SetPropertyValue("new_collectioneventidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_incidentaccountiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentaccountiddsc")]
		public global::System.Int32? new_incidentaccountiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_incidentaccountiddsc"); }
			private set { this.SetPropertyValue("new_incidentaccountiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_incidentaccountidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentaccountidname")]
		public global::System.String new_incidentaccountidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_incidentaccountidname"); }
			private set { this.SetPropertyValue("new_incidentaccountidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_supplierpricingiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_supplierpricingiddsc")]
		public global::System.Int32? new_supplierpricingiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_supplierpricingiddsc"); }
			private set { this.SetPropertyValue("new_supplierpricingiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_supplierpricingidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_supplierpricingidname")]
		public global::System.String new_supplierpricingidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_supplierpricingidname"); }
			private set { this.SetPropertyValue("new_supplierpricingidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for owneriddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneriddsc")]
		public global::System.Int32? owneriddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("owneriddsc"); }
			private set { this.SetPropertyValue("owneriddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Name of the owner (Attribute for owneridname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridname")]
		public global::System.String owneridname
		{
			get { return this.GetPropertyValue<global::System.String>("owneridname"); }
			private set { this.SetPropertyValue("owneridname", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Owner Id Type (Attribute for owneridtype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridtype")]
		public global::System.String owneridtype
		{
			get { return this.GetPropertyValue<global::System.String>("owneridtype"); }
			set { this.SetPropertyValue("owneridtype", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (Attribute for owninguser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owninguser")]
		public global::System.Guid? owninguser
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owninguser"); }
			set { this.SetPropertyValue("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (N:1 Association for user_new_obligation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owninguser")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_obligation")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_obligation
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("owninguser", "systemuserid"); }
			set { this.SetRelatedEntity("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for transactioncurrencyiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("transactioncurrencyiddsc")]
		public global::System.Int32? transactioncurrencyiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("transactioncurrencyiddsc"); }
			private set { this.SetPropertyValue("transactioncurrencyiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for transactioncurrencyidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("transactioncurrencyidname")]
		public global::System.String transactioncurrencyidname
		{
			get { return this.GetPropertyValue<global::System.String>("transactioncurrencyidname"); }
			private set { this.SetPropertyValue("transactioncurrencyidname", value, typeof(global::System.String)); }
		}

		*/

		#endregion
	}
}
