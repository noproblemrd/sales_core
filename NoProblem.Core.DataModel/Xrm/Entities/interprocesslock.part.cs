﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Inter Process Locks. (interprocesslock)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("interprocesslockid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("interprocesslock", "interprocesslocks")]
	public partial class interprocesslock : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "interprocesslock";
		private const string _primaryKeyLogicalName = "interprocesslockid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public interprocesslock()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public interprocesslock(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public interprocesslock(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public interprocesslock(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the Inter Process Lock record. (Attribute for interprocesslockid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("interprocesslockid", true)]
		public global::System.Guid interprocesslockid
		{
			get { return this.GetPropertyValue<global::System.Guid>("interprocesslockid"); }
			set { this.SetPropertyValue("interprocesslockid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Date and time when the record was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Lock token. (Attribute for token)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("token")]
		public global::System.Guid? token
		{
			get { return this.GetPropertyValue<global::System.Guid?>("token"); }
			set { this.SetPropertyValue("token", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

	}
}
