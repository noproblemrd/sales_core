﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Deprecated. Group of interrelated steps, or actions, and the rules that drive the transition between these steps. (wfprocess)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("processid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("wfprocess", "wfprocesses")]
	public partial class wfprocess : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "wfprocess";
		private const string _primaryKeyLogicalName = "processid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public wfprocess()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public wfprocess(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public wfprocess(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public wfprocess(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the business unit with which the workflow process is associated. (Attribute for businessunitid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitid")]
		public global::System.Guid? businessunitid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("businessunitid"); }
			set { this.SetPropertyValue("businessunitid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for clonedprocessid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("clonedprocessid")]
		public global::System.Guid? clonedprocessid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("clonedprocessid"); }
			private set { this.SetPropertyValue("clonedprocessid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier of the user who created the workflow process. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the workflow process was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Description of the workflow process. (Attribute for description)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("description")]
		public global::System.String description
		{
			get { return this.GetPropertyValue<global::System.String>("description"); }
			set { this.SetPropertyValue("description", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Type of entity for the workflow process. (Attribute for entitytype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("entitytype")]
		public global::System.String entitytype
		{
			get { return this.GetPropertyValue<global::System.String>("entitytype"); }
			set { this.SetPropertyValue("entitytype", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Type of event for the workflow process. (Attribute for eventtypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("eventtypecode", "eventtypecodeLabel")]
		public global::System.Int32? eventtypecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("eventtypecode"); }
			set { this.SetPropertyValue("eventtypecode", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Type of event for the workflow process. (Label for eventtypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("eventtypecode")]
		public global::System.String eventtypecodeLabel
		{
			get { return this.GetPropertyLabel("eventtypecode"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the workflow process. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the workflow process was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Name of the workflow process. (Attribute for name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("name")]
		public global::System.String name
		{
			get { return this.GetPropertyValue<global::System.String>("name"); }
			set { this.SetPropertyValue("name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Priority of the workflow process. (Attribute for priority)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("priority")]
		public global::System.Int32? priority
		{
			get { return this.GetPropertyValue<global::System.Int32?>("priority"); }
			set { this.SetPropertyValue("priority", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// For customer use. (Attribute for processcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("processcode", "processcodeLabel")]
		public global::System.Int32? processcode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("processcode"); }
			set { this.SetPropertyValue("processcode", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// For customer use. (Label for processcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("processcode")]
		public global::System.String processcodeLabel
		{
			get { return this.GetPropertyLabel("processcode"); }
		}

		/// <summary>
		/// Unique identifier of the workflow process. (Attribute for processid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("processid", true)]
		public global::System.Guid processid
		{
			get { return this.GetPropertyValue<global::System.Guid>("processid"); }
			set { this.SetPropertyValue("processid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Type of workflow process. (Attribute for processtypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("processtypecode", "processtypecodeLabel")]
		public global::System.Int32? processtypecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("processtypecode"); }
			set { this.SetPropertyValue("processtypecode", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Type of workflow process. (Label for processtypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("processtypecode")]
		public global::System.String processtypecodeLabel
		{
			get { return this.GetPropertyLabel("processtypecode"); }
		}

		/// <summary>
		/// For internal use only. (Attribute for processtypeversion)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("processtypeversion")]
		public global::System.Int32? processtypeversion
		{
			get { return this.GetPropertyValue<global::System.Int32?>("processtypeversion"); }
			set { this.SetPropertyValue("processtypeversion", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Status of the workflow process. (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// User context under which the workflow process will run. (Attribute for usercontext)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("usercontext")]
		public global::System.Guid? usercontext
		{
			get { return this.GetPropertyValue<global::System.Guid?>("usercontext"); }
			set { this.SetPropertyValue("usercontext", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for eventtypecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("eventtypecodename")]
		public global::System.Object eventtypecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("eventtypecodename"); }
			private set { this.SetPropertyValue("eventtypecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for processcodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("processcodename")]
		public global::System.Object processcodename
		{
			get { return this.GetPropertyValue<global::System.Object>("processcodename"); }
			private set { this.SetPropertyValue("processcodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for processtypecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("processtypecodename")]
		public global::System.Object processtypecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("processtypecodename"); }
			private set { this.SetPropertyValue("processtypecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
