﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (teammembership)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("teammembershipid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("teammembership", "teammemberships")]
	public partial class teammembership : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "teammembership";
		private const string _primaryKeyLogicalName = "teammembershipid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public teammembership()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public teammembership(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public teammembership(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public teammembership(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		///  (Attribute for systemuserid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("systemuserid")]
		public global::System.Guid? systemuserid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("systemuserid"); }
			private set { this.SetPropertyValue("systemuserid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		///  (Attribute for teamid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("teamid")]
		public global::System.Guid? teamid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("teamid"); }
			private set { this.SetPropertyValue("teamid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the team membership. (Attribute for teammembershipid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("teammembershipid", true)]
		public global::System.Guid teammembershipid
		{
			get { return this.GetPropertyValue<global::System.Guid>("teammembershipid"); }
			set { this.SetPropertyValue("teammembershipid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (N:N Association for teammembership_association)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("teammembership_association", false)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.team> teammembership_association
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.team>("systemuserid", "teammembership", "teamid", "teamid"); }
		}

		*/

		#endregion
	}
}
