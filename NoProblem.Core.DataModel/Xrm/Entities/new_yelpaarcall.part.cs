﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (new_yelpaarcall)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("new_yelpaarcallid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("new_yelpaarcall", "new_yelpaarcalls")]
	[global::System.Data.Services.IgnoreProperties("ownerid")]
	public partial class new_yelpaarcall : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "new_yelpaarcall";
		private const string _primaryKeyLogicalName = "new_yelpaarcallid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public new_yelpaarcall()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public new_yelpaarcall(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public new_yelpaarcall(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public new_yelpaarcall(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (N:1 Association for lk_new_yelpaarcall_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_yelpaarcall_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_yelpaarcall_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Sequence number of the import that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (N:1 Association for lk_new_yelpaarcall_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_yelpaarcall_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_yelpaarcall_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier for Aar Incident associated with Yelp AAR Call. (Attribute for new_aarincidentid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_aarincidentid")]
		public global::System.Guid? new_aarincidentid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_aarincidentid"); }
			set { this.SetPropertyValue("new_aarincidentid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_aarincident"); }
		}

		/// <summary>
		/// Unique identifier for Aar Incident associated with Yelp AAR Call. (N:1 Association for new_new_aarincident_new_yelpaarcall)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_aarincidentid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_new_aarincident_new_yelpaarcall")]
		public global::NoProblem.Core.DataModel.Xrm.new_aarincident new_new_aarincident_new_yelpaarcall
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_aarincident>("new_aarincidentid", "new_aarincidentid"); }
			set { this.SetRelatedEntity("new_aarincidentid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_aarpromptname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_aarpromptname")]
		public global::System.String new_aarpromptname
		{
			get { return this.GetPropertyValue<global::System.String>("new_aarpromptname"); }
			set { this.SetPropertyValue("new_aarpromptname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_browser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_browser")]
		public global::System.String new_browser
		{
			get { return this.GetPropertyValue<global::System.String>("new_browser"); }
			set { this.SetPropertyValue("new_browser", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_callendstatuscustomer)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_callendstatuscustomer")]
		public global::System.String new_callendstatuscustomer
		{
			get { return this.GetPropertyValue<global::System.String>("new_callendstatuscustomer"); }
			set { this.SetPropertyValue("new_callendstatuscustomer", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_callendstatussupplier)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_callendstatussupplier")]
		public global::System.String new_callendstatussupplier
		{
			get { return this.GetPropertyValue<global::System.String>("new_callendstatussupplier"); }
			set { this.SetPropertyValue("new_callendstatussupplier", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_callidcustomer)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_callidcustomer")]
		public global::System.String new_callidcustomer
		{
			get { return this.GetPropertyValue<global::System.String>("new_callidcustomer"); }
			set { this.SetPropertyValue("new_callidcustomer", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_callidsupplier)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_callidsupplier")]
		public global::System.String new_callidsupplier
		{
			get { return this.GetPropertyValue<global::System.String>("new_callidsupplier"); }
			set { this.SetPropertyValue("new_callidsupplier", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_calllengthcustomer)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_calllengthcustomer")]
		public global::System.Int32? new_calllengthcustomer
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_calllengthcustomer"); }
			set { this.SetPropertyValue("new_calllengthcustomer", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_calllengthsupplier)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_calllengthsupplier")]
		public global::System.Int32? new_calllengthsupplier
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_calllengthsupplier"); }
			set { this.SetPropertyValue("new_calllengthsupplier", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_cametolandingpage)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_cametolandingpage")]
		public global::System.Boolean? new_cametolandingpage
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_cametolandingpage"); }
			set { this.SetPropertyValue("new_cametolandingpage", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for new_dtmfpressed)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_dtmfpressed")]
		public global::System.String new_dtmfpressed
		{
			get { return this.GetPropertyValue<global::System.String>("new_dtmfpressed"); }
			set { this.SetPropertyValue("new_dtmfpressed", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// The first time the supplier viewed the video lead (Attribute for new_firstvideoviewdate)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_firstvideoviewdate")]
		public global::System.DateTime? new_firstvideoviewdate
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("new_firstvideoviewdate"); }
			set { this.SetPropertyValue("new_firstvideoviewdate", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for new_getapprequestcount)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_getapprequestcount")]
		public global::System.Int32? new_getapprequestcount
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_getapprequestcount"); }
			set { this.SetPropertyValue("new_getapprequestcount", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier for Incident Account associated with Yelp AAR Call. (Attribute for new_incidentaccountid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentaccountid")]
		public global::System.Guid? new_incidentaccountid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_incidentaccountid"); }
			set { this.SetPropertyValue("new_incidentaccountid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_incidentaccount"); }
		}

		/// <summary>
		/// Unique identifier for Incident Account associated with Yelp AAR Call. (N:1 Association for new_new_incidentaccount_new_yelpaarcall)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_incidentaccountid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_new_incidentaccount_new_yelpaarcall")]
		public global::NoProblem.Core.DataModel.Xrm.new_incidentaccount new_new_incidentaccount_new_yelpaarcall
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_incidentaccount>("new_incidentaccountid", "new_incidentaccountid"); }
			set { this.SetRelatedEntity("new_incidentaccountid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier for Case associated with Yelp AAR Call. (Attribute for new_incidentid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentid")]
		public global::System.Guid? new_incidentid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_incidentid"); }
			set { this.SetPropertyValue("new_incidentid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "incident"); }
		}

		/// <summary>
		/// Unique identifier for Case associated with Yelp AAR Call. (N:1 Association for new_incident_yelpaarcall)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_incidentid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_incident_yelpaarcall")]
		public global::NoProblem.Core.DataModel.Xrm.incident new_incident_yelpaarcall
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.incident>("new_incidentid", "incidentid"); }
			set { this.SetRelatedEntity("new_incidentid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_ip)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_ip")]
		public global::System.String new_ip
		{
			get { return this.GetPropertyValue<global::System.String>("new_ip"); }
			set { this.SetPropertyValue("new_ip", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_landingon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_landingon")]
		public global::System.DateTime? new_landingon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("new_landingon"); }
			set { this.SetPropertyValue("new_landingon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for new_landingongetapplink)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_landingongetapplink")]
		public global::System.DateTime? new_landingongetapplink
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("new_landingongetapplink"); }
			set { this.SetPropertyValue("new_landingongetapplink", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for new_leadviewed)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_leadviewed")]
		public global::System.Boolean? new_leadviewed
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_leadviewed"); }
			set { this.SetPropertyValue("new_leadviewed", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// The name of the custom entity. (Attribute for new_name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_name")]
		public global::System.String new_name
		{
			get { return this.GetPropertyValue<global::System.String>("new_name"); }
			set { this.SetPropertyValue("new_name", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_platform)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_platform")]
		public global::System.String new_platform
		{
			get { return this.GetPropertyValue<global::System.String>("new_platform"); }
			set { this.SetPropertyValue("new_platform", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_recording)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_recording")]
		public global::System.String new_recording
		{
			get { return this.GetPropertyValue<global::System.String>("new_recording"); }
			set { this.SetPropertyValue("new_recording", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_requestedleadcount)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_requestedleadcount")]
		public global::System.Int32? new_requestedleadcount
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_requestedleadcount"); }
			set { this.SetPropertyValue("new_requestedleadcount", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_requestedleaddate)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_requestedleaddate")]
		public global::System.DateTime? new_requestedleaddate
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("new_requestedleaddate"); }
			set { this.SetPropertyValue("new_requestedleaddate", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for new_sendtextmessage)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_sendtextmessage")]
		public global::System.Boolean? new_sendtextmessage
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_sendtextmessage"); }
			set { this.SetPropertyValue("new_sendtextmessage", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for new_successfulcall)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_successfulcall")]
		public global::System.Boolean? new_successfulcall
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_successfulcall"); }
			set { this.SetPropertyValue("new_successfulcall", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for new_textmessage)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_textmessage")]
		public global::System.String new_textmessage
		{
			get { return this.GetPropertyValue<global::System.String>("new_textmessage"); }
			set { this.SetPropertyValue("new_textmessage", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_type)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_type", "new_typeLabel")]
		public global::System.Int32? new_type
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_type"); }
			set { this.SetPropertyValue("new_type", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		///  (Label for new_type)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_type")]
		public global::System.String new_typeLabel
		{
			get { return this.GetPropertyLabel("new_type"); }
		}

		/// <summary>
		///  (Attribute for new_useragent)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_useragent")]
		public global::System.String new_useragent
		{
			get { return this.GetPropertyValue<global::System.String>("new_useragent"); }
			set { this.SetPropertyValue("new_useragent", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_viewvideocount)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_viewvideocount")]
		public global::System.Int32? new_viewvideocount
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_viewvideocount"); }
			set { this.SetPropertyValue("new_viewvideocount", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier for entity instances (Attribute for new_yelpaarcallid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_yelpaarcallid", true)]
		public global::System.Guid new_yelpaarcallid
		{
			get { return this.GetPropertyValue<global::System.Guid>("new_yelpaarcallid"); }
			set { this.SetPropertyValue("new_yelpaarcallid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier for Yelp Supplier associated with Yelp AAR Call. (Attribute for new_yelpsupplierid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_yelpsupplierid")]
		public global::System.Guid? new_yelpsupplierid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_yelpsupplierid"); }
			set { this.SetPropertyValue("new_yelpsupplierid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_yelpsupplier"); }
		}

		/// <summary>
		/// Unique identifier for Yelp Supplier associated with Yelp AAR Call. (N:1 Association for new_yelpsupplier_yelpaarcall)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_yelpsupplierid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_yelpsupplier_yelpaarcall")]
		public global::NoProblem.Core.DataModel.Xrm.new_yelpsupplier new_yelpsupplier_yelpaarcall
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_yelpsupplier>("new_yelpsupplierid", "new_yelpsupplierid"); }
			set { this.SetRelatedEntity("new_yelpsupplierid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("ownerid")]
		public global::Microsoft.Crm.Sdk.Owner ownerid
		{
			get { return this.GetPropertyValue<global::Microsoft.Crm.Sdk.Owner>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		public global::System.Guid? user_new_yelpaarcall_id
		{
			get { return this.GetPropertyValue<global::System.Guid?>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner), "systemuser"); }
		}

		/// <summary>
		/// Owner Id (N:1 Association for user_new_yelpaarcall)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_yelpaarcall")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_yelpaarcall
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("ownerid", "systemuserid"); }
			set { this.SetRelatedEntity("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (Attribute for owningbusinessunit)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owningbusinessunit")]
		public global::System.Guid? owningbusinessunit
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owningbusinessunit"); }
			private set { this.SetPropertyValue("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "businessunit"); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (N:1 Association for business_unit_new_yelpaarcall)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owningbusinessunit")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("business_unit_new_yelpaarcall")]
		public global::NoProblem.Core.DataModel.Xrm.businessunit business_unit_new_yelpaarcall
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.businessunit>("owningbusinessunit", "businessunitid"); }
			private set { this.SetRelatedEntity("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Status of the Yelp AAR Call (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the Yelp AAR Call (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the Yelp AAR Call (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		///  (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for new_yelpaarcall_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_yelpaarcall_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> new_yelpaarcall_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("new_yelpaarcallid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_yelpaarcall_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_yelpaarcall_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> new_yelpaarcall_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("new_yelpaarcallid", "regardingobjectid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_aarincidentiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_aarincidentiddsc")]
		public global::System.Int32? new_aarincidentiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_aarincidentiddsc"); }
			private set { this.SetPropertyValue("new_aarincidentiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_aarincidentidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_aarincidentidname")]
		public global::System.String new_aarincidentidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_aarincidentidname"); }
			private set { this.SetPropertyValue("new_aarincidentidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_cametolandingpagename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_cametolandingpagename")]
		public global::System.Object new_cametolandingpagename
		{
			get { return this.GetPropertyValue<global::System.Object>("new_cametolandingpagename"); }
			private set { this.SetPropertyValue("new_cametolandingpagename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_incidentaccountiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentaccountiddsc")]
		public global::System.Int32? new_incidentaccountiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_incidentaccountiddsc"); }
			private set { this.SetPropertyValue("new_incidentaccountiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_incidentaccountidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentaccountidname")]
		public global::System.String new_incidentaccountidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_incidentaccountidname"); }
			private set { this.SetPropertyValue("new_incidentaccountidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_incidentiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentiddsc")]
		public global::System.Int32? new_incidentiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_incidentiddsc"); }
			private set { this.SetPropertyValue("new_incidentiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_incidentidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentidname")]
		public global::System.String new_incidentidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_incidentidname"); }
			private set { this.SetPropertyValue("new_incidentidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_leadviewedname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_leadviewedname")]
		public global::System.Object new_leadviewedname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_leadviewedname"); }
			private set { this.SetPropertyValue("new_leadviewedname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_sendtextmessagename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_sendtextmessagename")]
		public global::System.Object new_sendtextmessagename
		{
			get { return this.GetPropertyValue<global::System.Object>("new_sendtextmessagename"); }
			private set { this.SetPropertyValue("new_sendtextmessagename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_successfulcallname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_successfulcallname")]
		public global::System.Object new_successfulcallname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_successfulcallname"); }
			private set { this.SetPropertyValue("new_successfulcallname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_typename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_typename")]
		public global::System.Object new_typename
		{
			get { return this.GetPropertyValue<global::System.Object>("new_typename"); }
			private set { this.SetPropertyValue("new_typename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_yelpsupplieriddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_yelpsupplieriddsc")]
		public global::System.Int32? new_yelpsupplieriddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_yelpsupplieriddsc"); }
			private set { this.SetPropertyValue("new_yelpsupplieriddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_yelpsupplieridname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_yelpsupplieridname")]
		public global::System.String new_yelpsupplieridname
		{
			get { return this.GetPropertyValue<global::System.String>("new_yelpsupplieridname"); }
			private set { this.SetPropertyValue("new_yelpsupplieridname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for owneriddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneriddsc")]
		public global::System.Int32? owneriddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("owneriddsc"); }
			private set { this.SetPropertyValue("owneriddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Name of the owner (Attribute for owneridname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridname")]
		public global::System.String owneridname
		{
			get { return this.GetPropertyValue<global::System.String>("owneridname"); }
			private set { this.SetPropertyValue("owneridname", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Owner Id Type (Attribute for owneridtype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridtype")]
		public global::System.String owneridtype
		{
			get { return this.GetPropertyValue<global::System.String>("owneridtype"); }
			set { this.SetPropertyValue("owneridtype", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (Attribute for owninguser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owninguser")]
		public global::System.Guid? owninguser
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owninguser"); }
			set { this.SetPropertyValue("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (N:1 Association for user_new_yelpaarcall)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owninguser")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_yelpaarcall")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_yelpaarcall
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("owninguser", "systemuserid"); }
			set { this.SetRelatedEntity("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
