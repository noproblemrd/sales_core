﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (new_pingphoneapientry)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("new_pingphoneapientryid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("new_pingphoneapientry", "new_pingphoneapientrynew_pingphoneapientries")]
	[global::System.Data.Services.IgnoreProperties("ownerid")]
	public partial class new_pingphoneapientry : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "new_pingphoneapientry";
		private const string _primaryKeyLogicalName = "new_pingphoneapientryid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public new_pingphoneapientry()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public new_pingphoneapientry(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public new_pingphoneapientry(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public new_pingphoneapientry(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (N:1 Association for lk_new_pingphoneapientry_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_pingphoneapientry_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_pingphoneapientry_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Sequence number of the import that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (N:1 Association for lk_new_pingphoneapientry_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_pingphoneapientry_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_pingphoneapientry_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier for Account associated with Ping Phone API Entry. (Attribute for new_accountid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_accountid")]
		public global::System.Guid? new_accountid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_accountid"); }
			set { this.SetPropertyValue("new_accountid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "account"); }
		}

		/// <summary>
		/// Unique identifier for Account associated with Ping Phone API Entry. (N:1 Association for new_account_pingphoneapientry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_accountid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_account_pingphoneapientry")]
		public global::NoProblem.Core.DataModel.Xrm.account new_account_pingphoneapientry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.account>("new_accountid", "accountid"); }
			set { this.SetRelatedEntity("new_accountid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_callbackurl)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_callbackurl")]
		public global::System.String new_callbackurl
		{
			get { return this.GetPropertyValue<global::System.String>("new_callbackurl"); }
			set { this.SetPropertyValue("new_callbackurl", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_callerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_callerid")]
		public global::System.String new_callerid
		{
			get { return this.GetPropertyValue<global::System.String>("new_callerid"); }
			set { this.SetPropertyValue("new_callerid", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_iscalleridreal)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_iscalleridreal")]
		public global::System.Boolean? new_iscalleridreal
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_iscalleridreal"); }
			set { this.SetPropertyValue("new_iscalleridreal", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for new_istest)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_istest")]
		public global::System.Boolean? new_istest
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_istest"); }
			set { this.SetPropertyValue("new_istest", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// seller generated id (Attribute for new_leadid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_leadid")]
		public global::System.String new_leadid
		{
			get { return this.GetPropertyValue<global::System.String>("new_leadid"); }
			set { this.SetPropertyValue("new_leadid", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// The name of the custom entity. (Attribute for new_name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_name")]
		public global::System.String new_name
		{
			get { return this.GetPropertyValue<global::System.String>("new_name"); }
			set { this.SetPropertyValue("new_name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for Origin associated with Ping Phone API Entry. (Attribute for new_originid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_originid")]
		public global::System.Guid? new_originid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_originid"); }
			set { this.SetPropertyValue("new_originid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_origin"); }
		}

		/// <summary>
		/// Unique identifier for Origin associated with Ping Phone API Entry. (N:1 Association for new_origin_pingphoneapientry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_originid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_origin_pingphoneapientry")]
		public global::NoProblem.Core.DataModel.Xrm.new_origin new_origin_pingphoneapientry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_origin>("new_originid", "new_originid"); }
			set { this.SetRelatedEntity("new_originid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier for entity instances (Attribute for new_pingphoneapientryid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_pingphoneapientryid", true)]
		public global::System.Guid new_pingphoneapientryid
		{
			get { return this.GetPropertyValue<global::System.Guid>("new_pingphoneapientryid"); }
			set { this.SetPropertyValue("new_pingphoneapientryid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		///  (Attribute for new_price)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_price")]
		public global::System.Decimal? new_price
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("new_price"); }
			set { this.SetPropertyValue("new_price", value, typeof(global::Microsoft.Crm.Sdk.CrmDecimal)); }
		}

		/// <summary>
		/// Unique identifier for Primary Expertise associated with Ping Phone API Entry. (Attribute for new_primaryexpertiseid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_primaryexpertiseid")]
		public global::System.Guid? new_primaryexpertiseid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_primaryexpertiseid"); }
			set { this.SetPropertyValue("new_primaryexpertiseid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_primaryexpertise"); }
		}

		/// <summary>
		/// Unique identifier for Primary Expertise associated with Ping Phone API Entry. (N:1 Association for new_primaryexpertise_pingphoneapientry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_primaryexpertiseid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_primaryexpertise_pingphoneapientry")]
		public global::NoProblem.Core.DataModel.Xrm.new_primaryexpertise new_primaryexpertise_pingphoneapientry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_primaryexpertise>("new_primaryexpertiseid", "new_primaryexpertiseid"); }
			set { this.SetRelatedEntity("new_primaryexpertiseid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier for Region associated with Ping Phone API Entry. (Attribute for new_regionid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_regionid")]
		public global::System.Guid? new_regionid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_regionid"); }
			set { this.SetPropertyValue("new_regionid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_region"); }
		}

		/// <summary>
		/// Unique identifier for Region associated with Ping Phone API Entry. (N:1 Association for new_region_pingphoneapientry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_regionid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_region_pingphoneapientry")]
		public global::NoProblem.Core.DataModel.Xrm.new_region new_region_pingphoneapientry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_region>("new_regionid", "new_regionid"); }
			set { this.SetRelatedEntity("new_regionid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_status)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_status", "new_statusLabel")]
		public global::System.Int32? new_status
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_status"); }
			set { this.SetPropertyValue("new_status", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		///  (Label for new_status)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_status")]
		public global::System.String new_statusLabel
		{
			get { return this.GetPropertyLabel("new_status"); }
		}

		/// <summary>
		///  (Attribute for new_testproviderphone)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_testproviderphone")]
		public global::System.String new_testproviderphone
		{
			get { return this.GetPropertyValue<global::System.String>("new_testproviderphone"); }
			set { this.SetPropertyValue("new_testproviderphone", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("ownerid")]
		public global::Microsoft.Crm.Sdk.Owner ownerid
		{
			get { return this.GetPropertyValue<global::Microsoft.Crm.Sdk.Owner>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		public global::System.Guid? user_new_pingphoneapientry_id
		{
			get { return this.GetPropertyValue<global::System.Guid?>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner), "systemuser"); }
		}

		/// <summary>
		/// Owner Id (N:1 Association for user_new_pingphoneapientry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_pingphoneapientry")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_pingphoneapientry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("ownerid", "systemuserid"); }
			set { this.SetRelatedEntity("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (Attribute for owningbusinessunit)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owningbusinessunit")]
		public global::System.Guid? owningbusinessunit
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owningbusinessunit"); }
			private set { this.SetPropertyValue("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "businessunit"); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (N:1 Association for business_unit_new_pingphoneapientry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owningbusinessunit")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("business_unit_new_pingphoneapientry")]
		public global::NoProblem.Core.DataModel.Xrm.businessunit business_unit_new_pingphoneapientry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.businessunit>("owningbusinessunit", "businessunitid"); }
			private set { this.SetRelatedEntity("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Status of the Ping Phone API Entry (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the Ping Phone API Entry (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the Ping Phone API Entry (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		///  (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for new_pingphoneapientry_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_pingphoneapientry_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> new_pingphoneapientry_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("new_pingphoneapientryid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_pingphoneapientry_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_pingphoneapientry_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> new_pingphoneapientry_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("new_pingphoneapientryid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_pingphoneapientry_incident)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_pingphoneapientry_incident", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.incident> new_pingphoneapientry_incident
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.incident>("new_pingphoneapientryid", "new_pingphoneapientryid"); }
		}

		/// <summary>
		///  (1:N Association for new_pingphoneapientry_pingphoneapifailuremap)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_pingphoneapientry_pingphoneapifailuremap", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_pingphoneapifailedincomingmap> new_pingphoneapientry_pingphoneapifailuremap
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_pingphoneapifailedincomingmap>("new_pingphoneapientryid", "new_pingphoneapientryid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_accountiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_accountiddsc")]
		public global::System.Int32? new_accountiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_accountiddsc"); }
			private set { this.SetPropertyValue("new_accountiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_accountidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_accountidname")]
		public global::System.String new_accountidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_accountidname"); }
			private set { this.SetPropertyValue("new_accountidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_accountidyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_accountidyominame")]
		public global::System.String new_accountidyominame
		{
			get { return this.GetPropertyValue<global::System.String>("new_accountidyominame"); }
			private set { this.SetPropertyValue("new_accountidyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_iscalleridrealname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_iscalleridrealname")]
		public global::System.Object new_iscalleridrealname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_iscalleridrealname"); }
			private set { this.SetPropertyValue("new_iscalleridrealname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_istestname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_istestname")]
		public global::System.Object new_istestname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_istestname"); }
			private set { this.SetPropertyValue("new_istestname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_originiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_originiddsc")]
		public global::System.Int32? new_originiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_originiddsc"); }
			private set { this.SetPropertyValue("new_originiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_originidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_originidname")]
		public global::System.String new_originidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_originidname"); }
			private set { this.SetPropertyValue("new_originidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_primaryexpertiseiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_primaryexpertiseiddsc")]
		public global::System.Int32? new_primaryexpertiseiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_primaryexpertiseiddsc"); }
			private set { this.SetPropertyValue("new_primaryexpertiseiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_primaryexpertiseidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_primaryexpertiseidname")]
		public global::System.String new_primaryexpertiseidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_primaryexpertiseidname"); }
			private set { this.SetPropertyValue("new_primaryexpertiseidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_regioniddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_regioniddsc")]
		public global::System.Int32? new_regioniddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_regioniddsc"); }
			private set { this.SetPropertyValue("new_regioniddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_regionidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_regionidname")]
		public global::System.String new_regionidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_regionidname"); }
			private set { this.SetPropertyValue("new_regionidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_statusname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_statusname")]
		public global::System.Object new_statusname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_statusname"); }
			private set { this.SetPropertyValue("new_statusname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for owneriddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneriddsc")]
		public global::System.Int32? owneriddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("owneriddsc"); }
			private set { this.SetPropertyValue("owneriddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Name of the owner (Attribute for owneridname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridname")]
		public global::System.String owneridname
		{
			get { return this.GetPropertyValue<global::System.String>("owneridname"); }
			private set { this.SetPropertyValue("owneridname", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Owner Id Type (Attribute for owneridtype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridtype")]
		public global::System.String owneridtype
		{
			get { return this.GetPropertyValue<global::System.String>("owneridtype"); }
			set { this.SetPropertyValue("owneridtype", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (Attribute for owninguser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owninguser")]
		public global::System.Guid? owninguser
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owninguser"); }
			set { this.SetPropertyValue("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (N:1 Association for user_new_pingphoneapientry)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owninguser")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_pingphoneapientry")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_pingphoneapientry
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("owninguser", "systemuserid"); }
			set { this.SetRelatedEntity("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
