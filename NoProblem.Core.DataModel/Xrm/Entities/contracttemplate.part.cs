﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Template for a contract containing the standard attributes of a contract. (contracttemplate)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("contracttemplateid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("contracttemplate", "contracttemplates")]
	public partial class contracttemplate : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "contracttemplate";
		private const string _primaryKeyLogicalName = "contracttemplateid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public contracttemplate()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public contracttemplate(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public contracttemplate(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public contracttemplate(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Abbreviation of the contract template name. (Attribute for abbreviation)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("abbreviation")]
		public global::System.String abbreviation
		{
			get { return this.GetPropertyValue<global::System.String>("abbreviation"); }
			set { this.SetPropertyValue("abbreviation", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Criteria for the contracts based on the template, such as number of cases, time, or coverage dates. (Attribute for allotmenttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("allotmenttypecode", "allotmenttypecodeLabel")]
		public global::System.Int32? allotmenttypecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("allotmenttypecode"); }
			set { this.SetPropertyValue("allotmenttypecode", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Criteria for the contracts based on the template, such as number of cases, time, or coverage dates. (Label for allotmenttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("allotmenttypecode")]
		public global::System.String allotmenttypecodeLabel
		{
			get { return this.GetPropertyLabel("allotmenttypecode"); }
		}

		/// <summary>
		/// How often the customer or account is to be billed in contracts that are based on the template. (Attribute for billingfrequencycode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("billingfrequencycode", "billingfrequencycodeLabel")]
		public global::System.Int32? billingfrequencycode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("billingfrequencycode"); }
			set { this.SetPropertyValue("billingfrequencycode", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// How often the customer or account is to be billed in contracts that are based on the template. (Label for billingfrequencycode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("billingfrequencycode")]
		public global::System.String billingfrequencycodeLabel
		{
			get { return this.GetPropertyLabel("billingfrequencycode"); }
		}

		/// <summary>
		/// Unique identifier of the level of service specified in contracts that are based on the template. (Attribute for contractservicelevelcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("contractservicelevelcode", "contractservicelevelcodeLabel")]
		public global::System.Int32? contractservicelevelcode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("contractservicelevelcode"); }
			set { this.SetPropertyValue("contractservicelevelcode", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Unique identifier of the level of service specified in contracts that are based on the template. (Label for contractservicelevelcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("contractservicelevelcode")]
		public global::System.String contractservicelevelcodeLabel
		{
			get { return this.GetPropertyLabel("contractservicelevelcode"); }
		}

		/// <summary>
		/// Unique identifier of the contract template. (Attribute for contracttemplateid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("contracttemplateid", true)]
		public global::System.Guid contracttemplateid
		{
			get { return this.GetPropertyValue<global::System.Guid>("contracttemplateid"); }
			set { this.SetPropertyValue("contracttemplateid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier of the user who created the contract template. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the contract template. (N:1 Association for lk_contracttemplatebase_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_contracttemplatebase_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_contracttemplatebase_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the contract template was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Description of the contract template. (Attribute for description)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("description")]
		public global::System.String description
		{
			get { return this.GetPropertyValue<global::System.String>("description"); }
			set { this.SetPropertyValue("description", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Days of the week and times for which contracts based on the template are effective. (Attribute for effectivitycalendar)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("effectivitycalendar")]
		public global::System.String effectivitycalendar
		{
			get { return this.GetPropertyValue<global::System.String>("effectivitycalendar"); }
			set { this.SetPropertyValue("effectivitycalendar", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the data import or data migration that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the contract template. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the contract template. (N:1 Association for lk_contracttemplatebase_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_contracttemplatebase_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_contracttemplatebase_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the contract template was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Name of the contract template. (Attribute for name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("name")]
		public global::System.String name
		{
			get { return this.GetPropertyValue<global::System.String>("name"); }
			set { this.SetPropertyValue("name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the organization associated with the contract template. (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier of the organization associated with the contract template. (N:1 Association for organization_contract_templates)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_contract_templates")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_contract_templates
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Specifies whether the discount is a percentage or a monetary amount in contracts based on the template. (Attribute for usediscountaspercentage)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("usediscountaspercentage")]
		public global::System.Boolean? usediscountaspercentage
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("usediscountaspercentage"); }
			set { this.SetPropertyValue("usediscountaspercentage", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for contract_template_contracts)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("contract_template_contracts", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.contract> contract_template_contracts
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.contract>("contracttemplateid", "contracttemplateid"); }
		}

		/// <summary>
		///  (1:N Association for ContractTemplate_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("ContractTemplate_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> ContractTemplate_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("contracttemplateid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for ContractTemplate_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("ContractTemplate_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> ContractTemplate_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("contracttemplateid", "regardingobjectid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for allotmenttypecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("allotmenttypecodename")]
		public global::System.Object allotmenttypecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("allotmenttypecodename"); }
			private set { this.SetPropertyValue("allotmenttypecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for billingfrequencycodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("billingfrequencycodename")]
		public global::System.Object billingfrequencycodename
		{
			get { return this.GetPropertyValue<global::System.Object>("billingfrequencycodename"); }
			private set { this.SetPropertyValue("billingfrequencycodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for contractservicelevelcodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("contractservicelevelcodename")]
		public global::System.Object contractservicelevelcodename
		{
			get { return this.GetPropertyValue<global::System.Object>("contractservicelevelcodename"); }
			private set { this.SetPropertyValue("contractservicelevelcodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyyominame")]
		public global::System.String createdbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyyominame"); }
			private set { this.SetPropertyValue("createdbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyyominame")]
		public global::System.String modifiedbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyyominame"); }
			private set { this.SetPropertyValue("modifiedbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for organizationiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationiddsc")]
		public global::System.Int32? organizationiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("organizationiddsc"); }
			private set { this.SetPropertyValue("organizationiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for organizationidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationidname")]
		public global::System.String organizationidname
		{
			get { return this.GetPropertyValue<global::System.String>("organizationidname"); }
			private set { this.SetPropertyValue("organizationidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for usediscountaspercentagename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("usediscountaspercentagename")]
		public global::System.Object usediscountaspercentagename
		{
			get { return this.GetPropertyValue<global::System.Object>("usediscountaspercentagename"); }
			private set { this.SetPropertyValue("usediscountaspercentagename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
