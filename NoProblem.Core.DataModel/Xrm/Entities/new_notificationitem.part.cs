﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (new_notificationitem)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("new_notificationitemid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("new_notificationitem", "new_notificationitems")]
	[global::System.Data.Services.IgnoreProperties("ownerid")]
	public partial class new_notificationitem : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "new_notificationitem";
		private const string _primaryKeyLogicalName = "new_notificationitemid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public new_notificationitem()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public new_notificationitem(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public new_notificationitem(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public new_notificationitem(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (N:1 Association for lk_new_notificationitem_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_notificationitem_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_notificationitem_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Sequence number of the import that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (N:1 Association for lk_new_notificationitem_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_notificationitem_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_notificationitem_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier for Channel associated with Notification Item. (Attribute for new_channelid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_channelid")]
		public global::System.Guid? new_channelid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_channelid"); }
			set { this.SetPropertyValue("new_channelid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_channel"); }
		}

		/// <summary>
		/// Unique identifier for Channel associated with Notification Item. (N:1 Association for new_channel_notificationitem)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_channelid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_channel_notificationitem")]
		public global::NoProblem.Core.DataModel.Xrm.new_channel new_channel_notificationitem
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_channel>("new_channelid", "new_channelid"); }
			set { this.SetRelatedEntity("new_channelid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier for Incident Account associated with Notification Item. (Attribute for new_incidentaccountid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentaccountid")]
		public global::System.Guid? new_incidentaccountid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_incidentaccountid"); }
			set { this.SetPropertyValue("new_incidentaccountid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_incidentaccount"); }
		}

		/// <summary>
		/// Unique identifier for Incident Account associated with Notification Item. (N:1 Association for new_incidentaccount_notificationitem)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_incidentaccountid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_incidentaccount_notificationitem")]
		public global::NoProblem.Core.DataModel.Xrm.new_incidentaccount new_incidentaccount_notificationitem
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_incidentaccount>("new_incidentaccountid", "new_incidentaccountid"); }
			set { this.SetRelatedEntity("new_incidentaccountid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// The name of the custom entity. (Attribute for new_name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_name")]
		public global::System.String new_name
		{
			get { return this.GetPropertyValue<global::System.String>("new_name"); }
			set { this.SetPropertyValue("new_name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for entity instances (Attribute for new_notificationitemid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_notificationitemid", true)]
		public global::System.Guid new_notificationitemid
		{
			get { return this.GetPropertyValue<global::System.Guid>("new_notificationitemid"); }
			set { this.SetPropertyValue("new_notificationitemid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		///  (Attribute for new_timetosend)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_timetosend")]
		public global::System.DateTime? new_timetosend
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("new_timetosend"); }
			set { this.SetPropertyValue("new_timetosend", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("ownerid")]
		public global::Microsoft.Crm.Sdk.Owner ownerid
		{
			get { return this.GetPropertyValue<global::Microsoft.Crm.Sdk.Owner>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Owner Id (Attribute for ownerid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		public global::System.Guid? user_new_notificationitem_id
		{
			get { return this.GetPropertyValue<global::System.Guid?>("ownerid"); }
			set { this.SetPropertyValue("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner), "systemuser"); }
		}

		/// <summary>
		/// Owner Id (N:1 Association for user_new_notificationitem)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("ownerid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_notificationitem")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_notificationitem
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("ownerid", "systemuserid"); }
			set { this.SetRelatedEntity("ownerid", value, typeof(global::Microsoft.Crm.Sdk.Owner)); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (Attribute for owningbusinessunit)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owningbusinessunit")]
		public global::System.Guid? owningbusinessunit
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owningbusinessunit"); }
			private set { this.SetPropertyValue("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "businessunit"); }
		}

		/// <summary>
		/// Unique identifier for the business unit that owns the record (N:1 Association for business_unit_new_notificationitem)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owningbusinessunit")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("business_unit_new_notificationitem")]
		public global::NoProblem.Core.DataModel.Xrm.businessunit business_unit_new_notificationitem
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.businessunit>("owningbusinessunit", "businessunitid"); }
			private set { this.SetRelatedEntity("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Status of the Notification Item (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the Notification Item (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the Notification Item (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		///  (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for new_notificationitem_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_notificationitem_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> new_notificationitem_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("new_notificationitemid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_notificationitem_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_notificationitem_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> new_notificationitem_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("new_notificationitemid", "regardingobjectid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_channeliddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_channeliddsc")]
		public global::System.Int32? new_channeliddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_channeliddsc"); }
			private set { this.SetPropertyValue("new_channeliddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_channelidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_channelidname")]
		public global::System.String new_channelidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_channelidname"); }
			private set { this.SetPropertyValue("new_channelidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_incidentaccountiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentaccountiddsc")]
		public global::System.Int32? new_incidentaccountiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_incidentaccountiddsc"); }
			private set { this.SetPropertyValue("new_incidentaccountiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_incidentaccountidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_incidentaccountidname")]
		public global::System.String new_incidentaccountidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_incidentaccountidname"); }
			private set { this.SetPropertyValue("new_incidentaccountidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for owneriddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneriddsc")]
		public global::System.Int32? owneriddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("owneriddsc"); }
			private set { this.SetPropertyValue("owneriddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Name of the owner (Attribute for owneridname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridname")]
		public global::System.String owneridname
		{
			get { return this.GetPropertyValue<global::System.String>("owneridname"); }
			private set { this.SetPropertyValue("owneridname", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Owner Id Type (Attribute for owneridtype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owneridtype")]
		public global::System.String owneridtype
		{
			get { return this.GetPropertyValue<global::System.String>("owneridtype"); }
			set { this.SetPropertyValue("owneridtype", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (Attribute for owninguser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owninguser")]
		public global::System.Guid? owninguser
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owninguser"); }
			set { this.SetPropertyValue("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier for the user that owns the record. (N:1 Association for user_new_notificationitem)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("owninguser")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("user_new_notificationitem")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser user_new_notificationitem
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("owninguser", "systemuserid"); }
			set { this.SetRelatedEntity("owninguser", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
