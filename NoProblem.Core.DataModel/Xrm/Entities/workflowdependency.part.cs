﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Dependencies for a workflow. (workflowdependency)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("workflowdependencyid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("workflowdependency", "workflowdependencyworkflowdependencies")]
	public partial class workflowdependency : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "workflowdependency";
		private const string _primaryKeyLogicalName = "workflowdependencyid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public workflowdependency()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public workflowdependency(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public workflowdependency(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public workflowdependency(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the workflow dependency. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the workflow dependency. (N:1 Association for workflow_dependency_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("workflow_dependency_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser workflow_dependency_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the workflow dependency was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Name of the entity used in the workflow. (Attribute for customentityname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("customentityname")]
		public global::System.String customentityname
		{
			get { return this.GetPropertyValue<global::System.String>("customentityname"); }
			set { this.SetPropertyValue("customentityname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Attribute Name. (Attribute for dependentattributename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("dependentattributename")]
		public global::System.String dependentattributename
		{
			get { return this.GetPropertyValue<global::System.String>("dependentattributename"); }
			set { this.SetPropertyValue("dependentattributename", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Entity Name. (Attribute for dependententityname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("dependententityname")]
		public global::System.String dependententityname
		{
			get { return this.GetPropertyValue<global::System.String>("dependententityname"); }
			set { this.SetPropertyValue("dependententityname", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Comma separated list of attributes that will be passed to workflow instance. (Attribute for entityattributes)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("entityattributes")]
		public global::System.String entityattributes
		{
			get { return this.GetPropertyValue<global::System.String>("entityattributes"); }
			set { this.SetPropertyValue("entityattributes", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the workflow dependency. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the workflow dependency. (N:1 Association for workflow_dependency_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("workflow_dependency_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser workflow_dependency_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the workflow dependency was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier of the business unit that owns the workflow dependency. (Attribute for owningbusinessunit)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owningbusinessunit")]
		public global::System.Guid? owningbusinessunit
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owningbusinessunit"); }
			private set { this.SetPropertyValue("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the user who owns the workflow dependency. (Attribute for owninguser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owninguser")]
		public global::System.Guid? owninguser
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owninguser"); }
			private set { this.SetPropertyValue("owninguser", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Name of workflow parameter. (Attribute for parametername)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("parametername")]
		public global::System.String parametername
		{
			get { return this.GetPropertyValue<global::System.String>("parametername"); }
			set { this.SetPropertyValue("parametername", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Fully qualified name of the CLR type of the local parameter. (Attribute for parametertype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("parametertype")]
		public global::System.String parametertype
		{
			get { return this.GetPropertyValue<global::System.String>("parametertype"); }
			set { this.SetPropertyValue("parametertype", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Attribute of the primary entity that specifies related entity. (Attribute for relatedattributename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("relatedattributename")]
		public global::System.String relatedattributename
		{
			get { return this.GetPropertyValue<global::System.String>("relatedattributename"); }
			set { this.SetPropertyValue("relatedattributename", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Name of the related entity. (Attribute for relatedentityname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("relatedentityname")]
		public global::System.String relatedentityname
		{
			get { return this.GetPropertyValue<global::System.String>("relatedentityname"); }
			set { this.SetPropertyValue("relatedentityname", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the SDK message. (Attribute for sdkmessageid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("sdkmessageid")]
		public global::System.Guid? sdkmessageid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("sdkmessageid"); }
			set { this.SetPropertyValue("sdkmessageid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "sdkmessage"); }
		}

		/// <summary>
		/// Unique identifier of the SDK message. (N:1 Association for sdkmessageid_workflow_dependency)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("sdkmessageid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("sdkmessageid_workflow_dependency")]
		public global::NoProblem.Core.DataModel.Xrm.sdkmessage sdkmessageid_workflow_dependency
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.sdkmessage>("sdkmessageid", "sdkmessageid"); }
			set { this.SetRelatedEntity("sdkmessageid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Type of workflow dependency. (Attribute for type)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("type", "typeLabel")]
		public global::System.Int32? type
		{
			get { return this.GetPropertyValue<global::System.Int32?>("type"); }
			set { this.SetPropertyValue("type", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Type of workflow dependency. (Label for type)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("type")]
		public global::System.String typeLabel
		{
			get { return this.GetPropertyLabel("type"); }
		}

		/// <summary>
		/// Unique identifier of the workflow dependency. (Attribute for workflowdependencyid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("workflowdependencyid", true)]
		public global::System.Guid workflowdependencyid
		{
			get { return this.GetPropertyValue<global::System.Guid>("workflowdependencyid"); }
			set { this.SetPropertyValue("workflowdependencyid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier of the workflow with which the dependency is associated. (Attribute for workflowid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("workflowid")]
		public global::System.Guid? workflowid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("workflowid"); }
			set { this.SetPropertyValue("workflowid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "workflow"); }
		}

		/// <summary>
		/// Unique identifier of the workflow with which the dependency is associated. (N:1 Association for workflow_dependencies)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("workflowid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("workflow_dependencies")]
		public global::NoProblem.Core.DataModel.Xrm.workflow workflow_dependencies
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.workflow>("workflowid", "workflowid"); }
			set { this.SetRelatedEntity("workflowid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyyominame")]
		public global::System.String createdbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyyominame"); }
			private set { this.SetPropertyValue("createdbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyyominame")]
		public global::System.String modifiedbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyyominame"); }
			private set { this.SetPropertyValue("modifiedbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for typename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("typename")]
		public global::System.Object typename
		{
			get { return this.GetPropertyValue<global::System.Object>("typename"); }
			private set { this.SetPropertyValue("typename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
