﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Entity that defines pricing levels. (pricelevel)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("pricelevelid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("pricelevel", "pricelevels")]
	public partial class pricelevel : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "pricelevel";
		private const string _primaryKeyLogicalName = "pricelevelid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public pricelevel()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public pricelevel(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public pricelevel(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public pricelevel(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Date on which the price list becomes effective. (Attribute for begindate)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("begindate")]
		public global::System.DateTime? begindate
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("begindate"); }
			set { this.SetPropertyValue("begindate", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier of the user who created the price list. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the price list. (N:1 Association for lk_pricelevelbase_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_pricelevelbase_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_pricelevelbase_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the price list was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Description of the price list. (Attribute for description)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("description")]
		public global::System.String description
		{
			get { return this.GetPropertyValue<global::System.String>("description"); }
			set { this.SetPropertyValue("description", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Date that is the last day the price list is valid. (Attribute for enddate)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("enddate")]
		public global::System.DateTime? enddate
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("enddate"); }
			set { this.SetPropertyValue("enddate", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Freight terms for the price list. (Attribute for freighttermscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("freighttermscode", "freighttermscodeLabel")]
		public global::System.Int32? freighttermscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("freighttermscode"); }
			set { this.SetPropertyValue("freighttermscode", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Freight terms for the price list. (Label for freighttermscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("freighttermscode")]
		public global::System.String freighttermscodeLabel
		{
			get { return this.GetPropertyLabel("freighttermscode"); }
		}

		/// <summary>
		/// Unique identifier of the data import or data migration that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the price list. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the price list. (N:1 Association for lk_pricelevelbase_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_pricelevelbase_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_pricelevelbase_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the price list was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Name of the price list. (Attribute for name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("name")]
		public global::System.String name
		{
			get { return this.GetPropertyValue<global::System.String>("name"); }
			set { this.SetPropertyValue("name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the organization associated with the price list. (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier of the organization associated with the price list. (N:1 Association for organization_price_levels)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_price_levels")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_price_levels
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Payment terms to use with the price list. (Attribute for paymentmethodcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("paymentmethodcode", "paymentmethodcodeLabel")]
		public global::System.Int32? paymentmethodcode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("paymentmethodcode"); }
			set { this.SetPropertyValue("paymentmethodcode", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Payment terms to use with the price list. (Label for paymentmethodcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("paymentmethodcode")]
		public global::System.String paymentmethodcodeLabel
		{
			get { return this.GetPropertyLabel("paymentmethodcode"); }
		}

		/// <summary>
		/// Unique identifier of the price list. (Attribute for pricelevelid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("pricelevelid", true)]
		public global::System.Guid pricelevelid
		{
			get { return this.GetPropertyValue<global::System.Guid>("pricelevelid"); }
			set { this.SetPropertyValue("pricelevelid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Method of shipment for products in the price list. (Attribute for shippingmethodcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("shippingmethodcode", "shippingmethodcodeLabel")]
		public global::System.Int32? shippingmethodcode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("shippingmethodcode"); }
			set { this.SetPropertyValue("shippingmethodcode", value, typeof(global::Microsoft.Crm.Sdk.Picklist)); }
		}

		/// <summary>
		/// Method of shipment for products in the price list. (Label for shippingmethodcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("shippingmethodcode")]
		public global::System.String shippingmethodcodeLabel
		{
			get { return this.GetPropertyLabel("shippingmethodcode"); }
		}

		/// <summary>
		/// Status of the price list. (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the price list. (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the price list. (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		/// For internal use only. (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the currency associated with the price level. (Attribute for transactioncurrencyid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("transactioncurrencyid")]
		public global::System.Guid? transactioncurrencyid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("transactioncurrencyid"); }
			set { this.SetPropertyValue("transactioncurrencyid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "transactioncurrency"); }
		}

		/// <summary>
		/// Unique identifier of the currency associated with the price level. (N:1 Association for transactioncurrency_pricelevel)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("transactioncurrencyid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("transactioncurrency_pricelevel")]
		public global::NoProblem.Core.DataModel.Xrm.transactioncurrency transactioncurrency_pricelevel
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.transactioncurrency>("transactioncurrencyid", "transactioncurrencyid"); }
			set { this.SetRelatedEntity("transactioncurrencyid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Time zone code that was in use when the record was created. (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for price_level_accounts)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("price_level_accounts", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.account> price_level_accounts
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.account>("pricelevelid", "defaultpricelevelid"); }
		}

		/// <summary>
		///  (1:N Association for price_level_contacts)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("price_level_contacts", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.contact> price_level_contacts
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.contact>("pricelevelid", "defaultpricelevelid"); }
		}

		/// <summary>
		///  (1:N Association for price_level_invoices)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("price_level_invoices", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.invoice> price_level_invoices
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.invoice>("pricelevelid", "pricelevelid"); }
		}

		/// <summary>
		///  (1:N Association for price_level_opportunties)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("price_level_opportunties", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.opportunity> price_level_opportunties
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.opportunity>("pricelevelid", "pricelevelid"); }
		}

		/// <summary>
		///  (1:N Association for price_level_orders)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("price_level_orders", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.salesorder> price_level_orders
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.salesorder>("pricelevelid", "pricelevelid"); }
		}

		/// <summary>
		///  (1:N Association for price_level_product_price_levels)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("price_level_product_price_levels", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.productpricelevel> price_level_product_price_levels
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.productpricelevel>("pricelevelid", "pricelevelid"); }
		}

		/// <summary>
		///  (1:N Association for price_level_products)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("price_level_products", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.product> price_level_products
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.product>("pricelevelid", "pricelevelid"); }
		}

		/// <summary>
		///  (1:N Association for price_level_quotes)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("price_level_quotes", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.quote> price_level_quotes
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.quote>("pricelevelid", "pricelevelid"); }
		}

		/// <summary>
		///  (1:N Association for PriceLevel_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("PriceLevel_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> PriceLevel_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("pricelevelid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for PriceLevel_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("PriceLevel_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> PriceLevel_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("pricelevelid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for PriceList_Campaigns)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("PriceList_Campaigns", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.campaign> PriceList_Campaigns
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.campaign>("pricelevelid", "pricelistid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyyominame")]
		public global::System.String createdbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyyominame"); }
			private set { this.SetPropertyValue("createdbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for freighttermscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("freighttermscodename")]
		public global::System.Object freighttermscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("freighttermscodename"); }
			private set { this.SetPropertyValue("freighttermscodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyyominame")]
		public global::System.String modifiedbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyyominame"); }
			private set { this.SetPropertyValue("modifiedbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for organizationiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationiddsc")]
		public global::System.Int32? organizationiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("organizationiddsc"); }
			private set { this.SetPropertyValue("organizationiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for organizationidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationidname")]
		public global::System.String organizationidname
		{
			get { return this.GetPropertyValue<global::System.String>("organizationidname"); }
			private set { this.SetPropertyValue("organizationidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for paymentmethodcodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("paymentmethodcodename")]
		public global::System.Object paymentmethodcodename
		{
			get { return this.GetPropertyValue<global::System.Object>("paymentmethodcodename"); }
			private set { this.SetPropertyValue("paymentmethodcodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for shippingmethodcodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("shippingmethodcodename")]
		public global::System.Object shippingmethodcodename
		{
			get { return this.GetPropertyValue<global::System.Object>("shippingmethodcodename"); }
			private set { this.SetPropertyValue("shippingmethodcodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for transactioncurrencyiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("transactioncurrencyiddsc")]
		public global::System.Int32? transactioncurrencyiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("transactioncurrencyiddsc"); }
			private set { this.SetPropertyValue("transactioncurrencyiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for transactioncurrencyidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("transactioncurrencyidname")]
		public global::System.String transactioncurrencyidname
		{
			get { return this.GetPropertyValue<global::System.String>("transactioncurrencyidname"); }
			private set { this.SetPropertyValue("transactioncurrencyidname", value, typeof(global::System.String)); }
		}

		*/

		#endregion
	}
}
