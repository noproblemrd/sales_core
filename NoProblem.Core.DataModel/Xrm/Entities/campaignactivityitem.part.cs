﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Work item of a campaign activity, such as a list or sales literature. (campaignactivityitem)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("campaignactivityitemid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("campaignactivityitem", "campaignactivityitems")]
	public partial class campaignactivityitem : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "campaignactivityitem";
		private const string _primaryKeyLogicalName = "campaignactivityitemid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public campaignactivityitem()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public campaignactivityitem(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public campaignactivityitem(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public campaignactivityitem(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the campaign activity item. (Attribute for campaignactivityitemid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("campaignactivityitemid", true)]
		public global::System.Guid campaignactivityitemid
		{
			get { return this.GetPropertyValue<global::System.Guid>("campaignactivityitemid"); }
			set { this.SetPropertyValue("campaignactivityitemid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier of the item. (Attribute for itemid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("itemid")]
		public global::System.Guid? itemid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("itemid"); }
			set { this.SetPropertyValue("itemid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Identification of the type of the campaign activity item. (Attribute for itemobjecttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("itemobjecttypecode", "itemobjecttypecodeLabel")]
		public global::System.String itemobjecttypecode
		{
			get { return this.GetPropertyValue<global::System.String>("itemobjecttypecode"); }
			set { this.SetPropertyValue("itemobjecttypecode", value, typeof(global::Microsoft.Crm.Sdk.EntityNameReference)); }
		}

		/// <summary>
		/// Identification of the type of the campaign activity item. (Label for itemobjecttypecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("itemobjecttypecode")]
		public global::System.String itemobjecttypecodeLabel
		{
			get { return this.GetPropertyLabel("itemobjecttypecode"); }
		}

		/// <summary>
		/// Unique identifier of the business unit that owns the campaign activity item. (Attribute for owningbusinessunit)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owningbusinessunit")]
		public global::System.Guid? owningbusinessunit
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owningbusinessunit"); }
			private set { this.SetPropertyValue("owningbusinessunit", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the user that owns the campaign activity item. (Attribute for owninguser)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("owninguser")]
		public global::System.Guid? owninguser
		{
			get { return this.GetPropertyValue<global::System.Guid?>("owninguser"); }
			private set { this.SetPropertyValue("owninguser", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		/// Unique identifier of the campaign activity for the item. (Attribute for campaignactivityid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("campaignactivityid")]
		public global::System.Guid? campaignactivityid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("campaignactivityid"); }
			set { this.SetPropertyValue("campaignactivityid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "activitypointer"); }
		}

		/// <summary>
		/// Unique identifier of the campaign activity for the item. (N:1 Association for ActivityPointer_CampaignActivityItems)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("campaignactivityid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("ActivityPointer_CampaignActivityItems")]
		public global::NoProblem.Core.DataModel.Xrm.activitypointer ActivityPointer_CampaignActivityItems
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.activitypointer>("campaignactivityid", "activityid"); }
			set { this.SetRelatedEntity("campaignactivityid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Identification of the type code name of the campaign activity item. (Attribute for itemobjecttypecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("itemobjecttypecodename")]
		public global::System.Object itemobjecttypecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("itemobjecttypecodename"); }
			private set { this.SetPropertyValue("itemobjecttypecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (N:N Association for campaignactivitylist_association)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("campaignactivitylist_association", false)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.campaignactivity> campaignactivitylist_association
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.campaignactivity>("itemid", "campaignactivityitem", "campaignactivityid", "activityid"); }
		}

		/// <summary>
		///  (N:N Association for campaignactivitysalesliterature_association)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("campaignactivitysalesliterature_association", false)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.campaignactivity> campaignactivitysalesliterature_association
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.campaignactivity>("itemid", "campaignactivityitem", "campaignactivityid", "activityid"); }
		}

		*/

		#endregion
	}
}
