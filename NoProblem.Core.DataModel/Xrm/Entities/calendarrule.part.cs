﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Defines free/busy times for a service and for resources or resource groups, such as working, non-working, vacation, and blocked. (calendarrule)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("calendarruleid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("calendarrule", "calendarrules")]
	public partial class calendarrule : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "calendarrule";
		private const string _primaryKeyLogicalName = "calendarruleid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public calendarrule()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public calendarrule(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public calendarrule(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public calendarrule(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the business unit with which the calendar rule is associated. (Attribute for businessunitid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitid")]
		public global::System.Guid? businessunitid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("businessunitid"); }
			private set { this.SetPropertyValue("businessunitid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Unique identifier of the calendar with which the calendar rule is associated. (Attribute for calendarid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("calendarid")]
		public global::System.Guid? calendarid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("calendarid"); }
			set { this.SetPropertyValue("calendarid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "calendar"); }
		}

		/// <summary>
		/// Unique identifier of the calendar with which the calendar rule is associated. (N:1 Association for calendar_calendar_rules)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("calendarid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("calendar_calendar_rules")]
		public global::NoProblem.Core.DataModel.Xrm.calendar calendar_calendar_rules
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.calendar>("calendarid", "calendarid"); }
			set { this.SetRelatedEntity("calendarid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier of the calendar rule. (Attribute for calendarruleid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("calendarruleid", true)]
		public global::System.Guid calendarruleid
		{
			get { return this.GetPropertyValue<global::System.Guid>("calendarruleid"); }
			set { this.SetPropertyValue("calendarruleid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier of the user who created the calendar rule. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the calendar rule. (N:1 Association for lk_calendarrule_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_calendarrule_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_calendarrule_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the calendar rule was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Defines free/busy times for a service and for resources or resource groups, such as working, non-working, vacation, and blocked. (Attribute for description)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("description")]
		public global::System.String description
		{
			get { return this.GetPropertyValue<global::System.String>("description"); }
			set { this.SetPropertyValue("description", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Duration of the calendar rule in minutes. (Attribute for duration)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("duration")]
		public global::System.Int32? duration
		{
			get { return this.GetPropertyValue<global::System.Int32?>("duration"); }
			set { this.SetPropertyValue("duration", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Effective interval end of the calendar rule. (Attribute for effectiveintervalend)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("effectiveintervalend")]
		public global::System.DateTime? effectiveintervalend
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("effectiveintervalend"); }
			set { this.SetPropertyValue("effectiveintervalend", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Effective interval start of the calendar rule. (Attribute for effectiveintervalstart)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("effectiveintervalstart")]
		public global::System.DateTime? effectiveintervalstart
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("effectiveintervalstart"); }
			set { this.SetPropertyValue("effectiveintervalstart", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Effort available for a resource during the time described by the calendar rule. (Attribute for effort)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("effort")]
		public global::System.Double? effort
		{
			get { return this.GetPropertyValue<global::System.Double?>("effort"); }
			set { this.SetPropertyValue("effort", value, typeof(global::Microsoft.Crm.Sdk.CrmFloat)); }
		}

		/// <summary>
		/// For internal use only. (Attribute for endtime)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("endtime")]
		public global::System.DateTime? endtime
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("endtime"); }
			set { this.SetPropertyValue("endtime", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Extent of the calendar rule. (Attribute for extentcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("extentcode")]
		public global::System.Int32? extentcode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("extentcode"); }
			set { this.SetPropertyValue("extentcode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the group. (Attribute for groupdesignator)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("groupdesignator")]
		public global::System.String groupdesignator
		{
			get { return this.GetPropertyValue<global::System.String>("groupdesignator"); }
			set { this.SetPropertyValue("groupdesignator", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the inner calendar for non-leaf calendar rules. (Attribute for innercalendarid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("innercalendarid")]
		public global::System.Guid? innercalendarid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("innercalendarid"); }
			set { this.SetPropertyValue("innercalendarid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "calendar"); }
		}

		/// <summary>
		/// Unique identifier of the inner calendar for non-leaf calendar rules. (N:1 Association for inner_calendar_calendar_rules)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("innercalendarid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("inner_calendar_calendar_rules")]
		public global::NoProblem.Core.DataModel.Xrm.calendar inner_calendar_calendar_rules
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.calendar>("innercalendarid", "calendarid"); }
			set { this.SetRelatedEntity("innercalendarid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// For internal use only. (Attribute for ismodified)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("ismodified")]
		public global::System.Boolean? ismodified
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("ismodified"); }
			set { this.SetPropertyValue("ismodified", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// Flag used in vary-by-day calendar rules. (Attribute for isselected)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("isselected")]
		public global::System.Boolean? isselected
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("isselected"); }
			set { this.SetPropertyValue("isselected", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// Flag used in vary-by-day calendar rules. (Attribute for issimple)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("issimple")]
		public global::System.Boolean? issimple
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("issimple"); }
			set { this.SetPropertyValue("issimple", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// Flag used in leaf nonrecurring rules. (Attribute for isvaried)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("isvaried")]
		public global::System.Boolean? isvaried
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("isvaried"); }
			set { this.SetPropertyValue("isvaried", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the calendar rule. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the calendar rule. (N:1 Association for lk_calendarrule_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_calendarrule_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_calendarrule_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the calendar rule was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Name of the calendar rule. (Attribute for name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("name")]
		public global::System.String name
		{
			get { return this.GetPropertyValue<global::System.String>("name"); }
			set { this.SetPropertyValue("name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Start offset for leaf nonrecurring rules. (Attribute for offset)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("offset")]
		public global::System.Int32? offset
		{
			get { return this.GetPropertyValue<global::System.Int32?>("offset"); }
			set { this.SetPropertyValue("offset", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the organization with which the calendar rule is associated. (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		/// Pattern of the rule recurrence. (Attribute for pattern)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("pattern")]
		public global::System.String pattern
		{
			get { return this.GetPropertyValue<global::System.String>("pattern"); }
			set { this.SetPropertyValue("pattern", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Rank of the calendar rule. (Attribute for rank)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("rank")]
		public global::System.Int32? rank
		{
			get { return this.GetPropertyValue<global::System.Int32?>("rank"); }
			set { this.SetPropertyValue("rank", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the service with which the calendar rule is associated. (Attribute for serviceid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("serviceid")]
		public global::System.Guid? serviceid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("serviceid"); }
			set { this.SetPropertyValue("serviceid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "service"); }
		}

		/// <summary>
		/// Unique identifier of the service with which the calendar rule is associated. (N:1 Association for service_calendar_rules)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("serviceid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("service_calendar_rules")]
		public global::NoProblem.Core.DataModel.Xrm.service service_calendar_rules
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.service>("serviceid", "serviceid"); }
			set { this.SetRelatedEntity("serviceid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Start time for the rule. (Attribute for starttime)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("starttime")]
		public global::System.DateTime? starttime
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("starttime"); }
			set { this.SetPropertyValue("starttime", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Sub-type of calendar rule. (Attribute for subcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("subcode")]
		public global::System.Int32? subcode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("subcode"); }
			set { this.SetPropertyValue("subcode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Type of calendar rule such as working hours, break, holiday, or time off. (Attribute for timecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timecode")]
		public global::System.Int32? timecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timecode"); }
			set { this.SetPropertyValue("timecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Local time zone for the calendar rule. (Attribute for timezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezonecode")]
		public global::System.Int32? timezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezonecode"); }
			set { this.SetPropertyValue("timezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyyominame")]
		public global::System.String createdbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyyominame"); }
			private set { this.SetPropertyValue("createdbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for ismodifiedname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("ismodifiedname")]
		public global::System.Object ismodifiedname
		{
			get { return this.GetPropertyValue<global::System.Object>("ismodifiedname"); }
			private set { this.SetPropertyValue("ismodifiedname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for isselectedname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("isselectedname")]
		public global::System.Object isselectedname
		{
			get { return this.GetPropertyValue<global::System.Object>("isselectedname"); }
			private set { this.SetPropertyValue("isselectedname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for issimplename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("issimplename")]
		public global::System.Object issimplename
		{
			get { return this.GetPropertyValue<global::System.Object>("issimplename"); }
			private set { this.SetPropertyValue("issimplename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for isvariedname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("isvariedname")]
		public global::System.Object isvariedname
		{
			get { return this.GetPropertyValue<global::System.Object>("isvariedname"); }
			private set { this.SetPropertyValue("isvariedname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyyominame")]
		public global::System.String modifiedbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyyominame"); }
			private set { this.SetPropertyValue("modifiedbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for serviceiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("serviceiddsc")]
		public global::System.Int32? serviceiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("serviceiddsc"); }
			private set { this.SetPropertyValue("serviceiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for serviceidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("serviceidname")]
		public global::System.String serviceidname
		{
			get { return this.GetPropertyValue<global::System.String>("serviceidname"); }
			private set { this.SetPropertyValue("serviceidname", value, typeof(global::System.String)); }
		}

		*/

		#endregion
	}
}
