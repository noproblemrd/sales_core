﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (new_primaryexpertise_categorygroup)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("new_primaryexpertise_categorygroupid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("new_primaryexpertise_categorygroup", "new_primaryexpertise_categorygroups")]
	public partial class new_primaryexpertise_categorygroup : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "new_primaryexpertise_categorygroup";
		private const string _primaryKeyLogicalName = "new_primaryexpertise_categorygroupid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public new_primaryexpertise_categorygroup()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public new_primaryexpertise_categorygroup(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public new_primaryexpertise_categorygroup(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public new_primaryexpertise_categorygroup(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		///  (Attribute for new_categorygroupid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_categorygroupid")]
		public global::System.Guid? new_categorygroupid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_categorygroupid"); }
			private set { this.SetPropertyValue("new_categorygroupid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		///  (Attribute for new_primaryexpertise_categorygroupid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_primaryexpertise_categorygroupid", true)]
		public global::System.Guid new_primaryexpertise_categorygroupid
		{
			get { return this.GetPropertyValue<global::System.Guid>("new_primaryexpertise_categorygroupid"); }
			private set { this.SetPropertyValue("new_primaryexpertise_categorygroupid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		///  (Attribute for new_primaryexpertiseid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_primaryexpertiseid")]
		public global::System.Guid? new_primaryexpertiseid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_primaryexpertiseid"); }
			private set { this.SetPropertyValue("new_primaryexpertiseid", value, typeof(global::Microsoft.Crm.Sdk.UniqueIdentifier)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (N:N Association for new_primaryexpertise_categorygroup)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_primaryexpertise_categorygroup", false)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_primaryexpertise> new_primaryexpertise_categorygroup
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_primaryexpertise>("new_categorygroupid", "new_primaryexpertise_categorygroup", "new_primaryexpertiseid", "new_primaryexpertiseid"); }
		}

		*/

		#endregion
	}
}
