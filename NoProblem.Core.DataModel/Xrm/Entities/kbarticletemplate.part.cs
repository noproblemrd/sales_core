﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Template for a knowledge base article that contains the standard attributes of an article. (kbarticletemplate)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("kbarticletemplateid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("kbarticletemplate", "kbarticletemplates")]
	public partial class kbarticletemplate : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "kbarticletemplate";
		private const string _primaryKeyLogicalName = "kbarticletemplateid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public kbarticletemplate()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public kbarticletemplate(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public kbarticletemplate(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public kbarticletemplate(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the knowledge base article template. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the knowledge base article template. (N:1 Association for lk_kbarticletemplatebase_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_kbarticletemplatebase_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_kbarticletemplatebase_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the knowledge base article template was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Description of the knowledge base article template. (Attribute for description)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("description")]
		public global::System.String description
		{
			get { return this.GetPropertyValue<global::System.String>("description"); }
			set { this.SetPropertyValue("description", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// XML format of the knowledge base article template. (Attribute for formatxml)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("formatxml")]
		public global::System.String formatxml
		{
			get { return this.GetPropertyValue<global::System.String>("formatxml"); }
			set { this.SetPropertyValue("formatxml", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier of the data import or data migration that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Information about whether the knowledge base article is active. (Attribute for isactive)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("isactive")]
		public global::System.Boolean? isactive
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("isactive"); }
			set { this.SetPropertyValue("isactive", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// Unique identifier of the knowledge base article template. (Attribute for kbarticletemplateid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("kbarticletemplateid", true)]
		public global::System.Guid kbarticletemplateid
		{
			get { return this.GetPropertyValue<global::System.Guid>("kbarticletemplateid"); }
			set { this.SetPropertyValue("kbarticletemplateid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Language of the Article Template (Attribute for languagecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("languagecode")]
		public global::System.Int32? languagecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("languagecode"); }
			set { this.SetPropertyValue("languagecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the knowledge base article template. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the knowledge base article template. (N:1 Association for lk_kbarticletemplatebase_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_kbarticletemplatebase_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_kbarticletemplatebase_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the knowledge base article template was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier of the organization associated with the template. (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier of the organization associated with the template. (N:1 Association for organization_kb_article_templates)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_kb_article_templates")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_kb_article_templates
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// XML structure of the knowledge base article. (Attribute for structurexml)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("structurexml")]
		public global::System.String structurexml
		{
			get { return this.GetPropertyValue<global::System.String>("structurexml"); }
			set { this.SetPropertyValue("structurexml", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Title of the knowledge base article template. (Attribute for title)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("title")]
		public global::System.String title
		{
			get { return this.GetPropertyValue<global::System.String>("title"); }
			set { this.SetPropertyValue("title", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for kb_article_template_kb_articles)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("kb_article_template_kb_articles", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.kbarticle> kb_article_template_kb_articles
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.kbarticle>("kbarticletemplateid", "kbarticletemplateid"); }
		}

		/// <summary>
		///  (1:N Association for KbArticleTemplate_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("KbArticleTemplate_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> KbArticleTemplate_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("kbarticletemplateid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for KbArticleTemplate_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("KbArticleTemplate_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> KbArticleTemplate_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("kbarticletemplateid", "regardingobjectid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyyominame")]
		public global::System.String createdbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyyominame"); }
			private set { this.SetPropertyValue("createdbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for isactivename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("isactivename")]
		public global::System.Object isactivename
		{
			get { return this.GetPropertyValue<global::System.Object>("isactivename"); }
			private set { this.SetPropertyValue("isactivename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyyominame")]
		public global::System.String modifiedbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyyominame"); }
			private set { this.SetPropertyValue("modifiedbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for organizationiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationiddsc")]
		public global::System.Int32? organizationiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("organizationiddsc"); }
			private set { this.SetPropertyValue("organizationiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for organizationidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationidname")]
		public global::System.String organizationidname
		{
			get { return this.GetPropertyValue<global::System.String>("organizationidname"); }
			private set { this.SetPropertyValue("organizationidname", value, typeof(global::System.String)); }
		}

		*/

		#endregion
	}
}
