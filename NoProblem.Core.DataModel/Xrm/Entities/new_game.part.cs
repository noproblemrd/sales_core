﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (new_game)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("new_gameid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("new_game", "new_games")]
	public partial class new_game : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "new_game";
		private const string _primaryKeyLogicalName = "new_gameid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public new_game()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public new_game(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public new_game(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public new_game(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (N:1 Association for lk_new_game_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_game_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_game_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Sequence number of the import that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (N:1 Association for lk_new_game_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_game_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_game_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier for Court associated with Game. (Attribute for new_courtid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_courtid")]
		public global::System.Guid? new_courtid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_courtid"); }
			set { this.SetPropertyValue("new_courtid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_fields"); }
		}

		/// <summary>
		/// Unique identifier for Court associated with Game. (N:1 Association for new_new_fields_new_game)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_courtid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_new_fields_new_game")]
		public global::NoProblem.Core.DataModel.Xrm.new_fields new_new_fields_new_game
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_fields>("new_courtid", "new_fieldsid"); }
			set { this.SetRelatedEntity("new_courtid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier for entity instances (Attribute for new_gameid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_gameid", true)]
		public global::System.Guid new_gameid
		{
			get { return this.GetPropertyValue<global::System.Guid>("new_gameid"); }
			set { this.SetPropertyValue("new_gameid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		///  (Attribute for new_gamenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_gamenumber")]
		public global::System.Int32? new_gamenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_gamenumber"); }
			set { this.SetPropertyValue("new_gamenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_gameresultteam1)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_gameresultteam1")]
		public global::System.Int32? new_gameresultteam1
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_gameresultteam1"); }
			set { this.SetPropertyValue("new_gameresultteam1", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_gameresultteam2)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_gameresultteam2")]
		public global::System.Int32? new_gameresultteam2
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_gameresultteam2"); }
			set { this.SetPropertyValue("new_gameresultteam2", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// The name of the custom entity. (Attribute for new_name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_name")]
		public global::System.String new_name
		{
			get { return this.GetPropertyValue<global::System.String>("new_name"); }
			set { this.SetPropertyValue("new_name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for the organization (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier for the organization (N:1 Association for organization_new_game)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_new_game")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_new_game
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Status of the Game (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the Game (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the Game (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		///  (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for new_game_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_game_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> new_game_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("new_gameid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_game_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_game_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> new_game_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("new_gameid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_game_DuplicateBaseRecord)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_game_DuplicateBaseRecord", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.duplicaterecord> new_game_DuplicateBaseRecord
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.duplicaterecord>("new_gameid", "baserecordid"); }
		}

		/// <summary>
		///  (1:N Association for new_game_DuplicateMatchingRecord)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_game_DuplicateMatchingRecord", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.duplicaterecord> new_game_DuplicateMatchingRecord
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.duplicaterecord>("new_gameid", "duplicaterecordid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_courtiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_courtiddsc")]
		public global::System.Int32? new_courtiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_courtiddsc"); }
			private set { this.SetPropertyValue("new_courtiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_courtidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_courtidname")]
		public global::System.String new_courtidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_courtidname"); }
			private set { this.SetPropertyValue("new_courtidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for organizationiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationiddsc")]
		public global::System.Int32? organizationiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("organizationiddsc"); }
			private set { this.SetPropertyValue("organizationiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for organizationidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationidname")]
		public global::System.String organizationidname
		{
			get { return this.GetPropertyValue<global::System.String>("organizationidname"); }
			private set { this.SetPropertyValue("organizationidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
