﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	/// Monthly fiscal calendar of an organization. A span of time during which the financial activities of an organization are calculated. (monthlyfiscalcalendar)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("userfiscalcalendarid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("monthlyfiscalcalendar", "monthlyfiscalcalendars")]
	public partial class monthlyfiscalcalendar : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "monthlyfiscalcalendar";
		private const string _primaryKeyLogicalName = "userfiscalcalendarid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public monthlyfiscalcalendar()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public monthlyfiscalcalendar(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public monthlyfiscalcalendar(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public monthlyfiscalcalendar(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		///  (Attribute for businessunitid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitid")]
		public global::System.Guid? businessunitid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("businessunitid"); }
			private set { this.SetPropertyValue("businessunitid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier of the user who created the fiscal calendar. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the fiscal calendar. (N:1 Association for lk_monthlyfiscalcalendar_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_monthlyfiscalcalendar_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_monthlyfiscalcalendar_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the quota for the monthly fiscal calendar was modified. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Date and time when the monthly fiscal calendar sales quota takes effect. (Attribute for effectiveon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("effectiveon")]
		public global::System.DateTime? effectiveon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("effectiveon"); }
			set { this.SetPropertyValue("effectiveon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Exchange rate for the currency associated with the monthly fiscal calendar with respect to the base currency. (Attribute for exchangerate)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("exchangerate")]
		public global::System.Decimal? exchangerate
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("exchangerate"); }
			private set { this.SetPropertyValue("exchangerate", value, typeof(global::Microsoft.Crm.Sdk.CrmDecimal)); }
		}

		/// <summary>
		/// Type of fiscal period used in the sales quota. (Attribute for fiscalperiodtype)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("fiscalperiodtype")]
		public global::System.Int32? fiscalperiodtype
		{
			get { return this.GetPropertyValue<global::System.Int32?>("fiscalperiodtype"); }
			private set { this.SetPropertyValue("fiscalperiodtype", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the quota for the monthly fiscal calendar. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who last modified the quota for the monthly fiscal calendar. (N:1 Association for lk_monthlyfiscalcalendar_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_monthlyfiscalcalendar_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_monthlyfiscalcalendar_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the quota for the monthly fiscal calendar was last modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Sales quota for the first month in the fiscal year. (Attribute for month1)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month1")]
		public global::System.Decimal? month1
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month1"); }
			set { this.SetPropertyValue("month1", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the first month in the fiscal year. (Attribute for month1_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month1_base")]
		public global::System.Decimal? month1_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month1_base"); }
			private set { this.SetPropertyValue("month1_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the tenth month in the fiscal year. (Attribute for month10)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month10")]
		public global::System.Decimal? month10
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month10"); }
			set { this.SetPropertyValue("month10", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the tenth month in the fiscal year. (Attribute for month10_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month10_base")]
		public global::System.Decimal? month10_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month10_base"); }
			private set { this.SetPropertyValue("month10_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the eleventh month in the fiscal year. (Attribute for month11)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month11")]
		public global::System.Decimal? month11
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month11"); }
			set { this.SetPropertyValue("month11", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the eleventh month in the fiscal year. (Attribute for month11_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month11_base")]
		public global::System.Decimal? month11_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month11_base"); }
			private set { this.SetPropertyValue("month11_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the twelfth month in the fiscal year. (Attribute for month12)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month12")]
		public global::System.Decimal? month12
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month12"); }
			set { this.SetPropertyValue("month12", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the twelfth month in the fiscal year. (Attribute for month12_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month12_base")]
		public global::System.Decimal? month12_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month12_base"); }
			private set { this.SetPropertyValue("month12_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the second month in the fiscal year. (Attribute for month2)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month2")]
		public global::System.Decimal? month2
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month2"); }
			set { this.SetPropertyValue("month2", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the second month in the fiscal year. (Attribute for month2_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month2_base")]
		public global::System.Decimal? month2_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month2_base"); }
			private set { this.SetPropertyValue("month2_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the third month in the fiscal year. (Attribute for month3)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month3")]
		public global::System.Decimal? month3
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month3"); }
			set { this.SetPropertyValue("month3", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the third month in the fiscal year. (Attribute for month3_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month3_base")]
		public global::System.Decimal? month3_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month3_base"); }
			private set { this.SetPropertyValue("month3_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the fourth month in the fiscal year. (Attribute for month4)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month4")]
		public global::System.Decimal? month4
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month4"); }
			set { this.SetPropertyValue("month4", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the fourth month in the fiscal year. (Attribute for month4_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month4_base")]
		public global::System.Decimal? month4_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month4_base"); }
			private set { this.SetPropertyValue("month4_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the fifth month in the fiscal year. (Attribute for month5)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month5")]
		public global::System.Decimal? month5
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month5"); }
			set { this.SetPropertyValue("month5", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the fifth month in the fiscal year. (Attribute for month5_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month5_base")]
		public global::System.Decimal? month5_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month5_base"); }
			private set { this.SetPropertyValue("month5_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the sixth month in the fiscal year. (Attribute for month6)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month6")]
		public global::System.Decimal? month6
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month6"); }
			set { this.SetPropertyValue("month6", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the sixth month in the fiscal year. (Attribute for month6_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month6_base")]
		public global::System.Decimal? month6_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month6_base"); }
			private set { this.SetPropertyValue("month6_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the seventh month in the fiscal year. (Attribute for month7)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month7")]
		public global::System.Decimal? month7
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month7"); }
			set { this.SetPropertyValue("month7", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the seventh month in the fiscal year. (Attribute for month7_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month7_base")]
		public global::System.Decimal? month7_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month7_base"); }
			private set { this.SetPropertyValue("month7_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the eighth month in the fiscal year. (Attribute for month8)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month8")]
		public global::System.Decimal? month8
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month8"); }
			set { this.SetPropertyValue("month8", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the eighth month in the fiscal year. (Attribute for month8_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month8_base")]
		public global::System.Decimal? month8_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month8_base"); }
			private set { this.SetPropertyValue("month8_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Sales quota for the ninth month in the fiscal year. (Attribute for month9)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month9")]
		public global::System.Decimal? month9
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month9"); }
			set { this.SetPropertyValue("month9", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Base currency equivalent of the sales quota for the ninth month in the fiscal year. (Attribute for month9_base)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("month9_base")]
		public global::System.Decimal? month9_base
		{
			get { return this.GetPropertyValue<global::System.Decimal?>("month9_base"); }
			private set { this.SetPropertyValue("month9_base", value, typeof(global::Microsoft.Crm.Sdk.CrmMoney)); }
		}

		/// <summary>
		/// Unique identifier of the associated salesperson. (Attribute for salespersonid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("salespersonid")]
		public global::System.Guid? salespersonid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("salespersonid"); }
			set { this.SetPropertyValue("salespersonid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the associated salesperson. (N:1 Association for lk_monthlyfiscalcalendar_salespersonid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("salespersonid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_monthlyfiscalcalendar_salespersonid")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_monthlyfiscalcalendar_salespersonid
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("salespersonid", "systemuserid"); }
			set { this.SetRelatedEntity("salespersonid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// For internal use only. (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the currency associated with the monthly fiscal calendar. (Attribute for transactioncurrencyid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("transactioncurrencyid")]
		public global::System.Guid? transactioncurrencyid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("transactioncurrencyid"); }
			set { this.SetPropertyValue("transactioncurrencyid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "transactioncurrency"); }
		}

		/// <summary>
		/// Unique identifier of the currency associated with the monthly fiscal calendar. (N:1 Association for transactioncurrency_monthlyfiscalcalendar)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("transactioncurrencyid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("transactioncurrency_monthlyfiscalcalendar")]
		public global::NoProblem.Core.DataModel.Xrm.transactioncurrency transactioncurrency_monthlyfiscalcalendar
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.transactioncurrency>("transactioncurrencyid", "transactioncurrencyid"); }
			set { this.SetRelatedEntity("transactioncurrencyid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier of the monthly fiscal calendar. (Attribute for userfiscalcalendarid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("userfiscalcalendarid", true)]
		public global::System.Guid userfiscalcalendarid
		{
			get { return this.GetPropertyValue<global::System.Guid>("userfiscalcalendarid"); }
			set { this.SetPropertyValue("userfiscalcalendarid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Time zone code that was in use when the record was created. (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (1:N Association for MonthlyFiscalCalendar_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("MonthlyFiscalCalendar_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> MonthlyFiscalCalendar_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("userfiscalcalendarid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for MonthlyFiscalCalendar_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("MonthlyFiscalCalendar_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> MonthlyFiscalCalendar_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("userfiscalcalendarid", "regardingobjectid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for businessunitiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitiddsc")]
		public global::System.Int32? businessunitiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("businessunitiddsc"); }
			private set { this.SetPropertyValue("businessunitiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for businessunitidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("businessunitidname")]
		public global::System.String businessunitidname
		{
			get { return this.GetPropertyValue<global::System.String>("businessunitidname"); }
			private set { this.SetPropertyValue("businessunitidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for createdbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyyominame")]
		public global::System.String createdbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyyominame"); }
			private set { this.SetPropertyValue("createdbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyyominame")]
		public global::System.String modifiedbyyominame
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyyominame"); }
			private set { this.SetPropertyValue("modifiedbyyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for salespersoniddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("salespersoniddsc")]
		public global::System.Int32? salespersoniddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("salespersoniddsc"); }
			private set { this.SetPropertyValue("salespersoniddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for salespersonidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("salespersonidname")]
		public global::System.String salespersonidname
		{
			get { return this.GetPropertyValue<global::System.String>("salespersonidname"); }
			private set { this.SetPropertyValue("salespersonidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for salespersonidyominame)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("salespersonidyominame")]
		public global::System.String salespersonidyominame
		{
			get { return this.GetPropertyValue<global::System.String>("salespersonidyominame"); }
			private set { this.SetPropertyValue("salespersonidyominame", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for transactioncurrencyiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("transactioncurrencyiddsc")]
		public global::System.Int32? transactioncurrencyiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("transactioncurrencyiddsc"); }
			private set { this.SetPropertyValue("transactioncurrencyiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for transactioncurrencyidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("transactioncurrencyidname")]
		public global::System.String transactioncurrencyidname
		{
			get { return this.GetPropertyValue<global::System.String>("transactioncurrencyidname"); }
			private set { this.SetPropertyValue("transactioncurrencyidname", value, typeof(global::System.String)); }
		}

		*/

		#endregion
	}
}
