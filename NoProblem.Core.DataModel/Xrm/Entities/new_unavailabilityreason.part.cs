﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (new_unavailabilityreason)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("new_unavailabilityreasonid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("new_unavailabilityreason", "new_unavailabilityreasons")]
	public partial class new_unavailabilityreason : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "new_unavailabilityreason";
		private const string _primaryKeyLogicalName = "new_unavailabilityreasonid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public new_unavailabilityreason()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public new_unavailabilityreason(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public new_unavailabilityreason(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public new_unavailabilityreason(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (N:1 Association for lk_new_unavailabilityreason_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_unavailabilityreason_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_unavailabilityreason_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Sequence number of the import that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (N:1 Association for lk_new_unavailabilityreason_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_unavailabilityreason_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_unavailabilityreason_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for new_disabled)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_disabled")]
		public global::System.Boolean? new_disabled
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_disabled"); }
			set { this.SetPropertyValue("new_disabled", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// Unique identifier for Unavailability Reason Group associated with Unavailability Reason. (Attribute for new_groupid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_groupid")]
		public global::System.Guid? new_groupid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_groupid"); }
			set { this.SetPropertyValue("new_groupid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_unavailabilityreasongroup"); }
		}

		/// <summary>
		/// Unique identifier for Unavailability Reason Group associated with Unavailability Reason. (N:1 Association for new_group_unavailabilityreason)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_groupid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_group_unavailabilityreason")]
		public global::NoProblem.Core.DataModel.Xrm.new_unavailabilityreasongroup new_group_unavailabilityreason
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_unavailabilityreasongroup>("new_groupid", "new_unavailabilityreasongroupid"); }
			set { this.SetRelatedEntity("new_groupid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_id)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_id")]
		public global::System.String new_id
		{
			get { return this.GetPropertyValue<global::System.String>("new_id"); }
			set { this.SetPropertyValue("new_id", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_isdailybudget)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_isdailybudget")]
		public global::System.Boolean? new_isdailybudget
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_isdailybudget"); }
			set { this.SetPropertyValue("new_isdailybudget", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for new_isoverrefundpercent)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_isoverrefundpercent")]
		public global::System.Boolean? new_isoverrefundpercent
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_isoverrefundpercent"); }
			set { this.SetPropertyValue("new_isoverrefundpercent", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for new_ispricingended)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_ispricingended")]
		public global::System.Boolean? new_ispricingended
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_ispricingended"); }
			set { this.SetPropertyValue("new_ispricingended", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for new_issystemreason)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_issystemreason")]
		public global::System.Boolean? new_issystemreason
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_issystemreason"); }
			set { this.SetPropertyValue("new_issystemreason", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		/// The name of the custom entity. (Attribute for new_name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_name")]
		public global::System.String new_name
		{
			get { return this.GetPropertyValue<global::System.String>("new_name"); }
			set { this.SetPropertyValue("new_name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for entity instances (Attribute for new_unavailabilityreasonid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_unavailabilityreasonid", true)]
		public global::System.Guid new_unavailabilityreasonid
		{
			get { return this.GetPropertyValue<global::System.Guid>("new_unavailabilityreasonid"); }
			set { this.SetPropertyValue("new_unavailabilityreasonid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier for the organization (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier for the organization (N:1 Association for organization_new_unavailabilityreason)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_new_unavailabilityreason")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_new_unavailabilityreason
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Status of the Unavailability Reason (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the Unavailability Reason (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the Unavailability Reason (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		///  (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for new_new_unavailabilityreason_account)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_new_unavailabilityreason_account", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.account> new_new_unavailabilityreason_account
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.account>("new_unavailabilityreasonid", "new_unavailabilityreasonid"); }
		}

		/// <summary>
		///  (1:N Association for new_new_unavailabilityreason_new_accountexper)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_new_unavailabilityreason_new_accountexper", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_accountexpertise> new_new_unavailabilityreason_new_accountexper
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_accountexpertise>("new_unavailabilityreasonid", "new_unavailabilityreasonid"); }
		}

		/// <summary>
		///  (1:N Association for new_unavailabilityreason_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_unavailabilityreason_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> new_unavailabilityreason_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("new_unavailabilityreasonid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_unavailabilityreason_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_unavailabilityreason_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> new_unavailabilityreason_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("new_unavailabilityreasonid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_unavailabilityreason_DuplicateBaseRecord)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_unavailabilityreason_DuplicateBaseRecord", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.duplicaterecord> new_unavailabilityreason_DuplicateBaseRecord
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.duplicaterecord>("new_unavailabilityreasonid", "baserecordid"); }
		}

		/// <summary>
		///  (1:N Association for new_unavailabilityreason_DuplicateMatchingRecord)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_unavailabilityreason_DuplicateMatchingRecord", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.duplicaterecord> new_unavailabilityreason_DuplicateMatchingRecord
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.duplicaterecord>("new_unavailabilityreasonid", "duplicaterecordid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_disabledname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_disabledname")]
		public global::System.Object new_disabledname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_disabledname"); }
			private set { this.SetPropertyValue("new_disabledname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_groupiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_groupiddsc")]
		public global::System.Int32? new_groupiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_groupiddsc"); }
			private set { this.SetPropertyValue("new_groupiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_groupidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_groupidname")]
		public global::System.String new_groupidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_groupidname"); }
			private set { this.SetPropertyValue("new_groupidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_isdailybudgetname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_isdailybudgetname")]
		public global::System.Object new_isdailybudgetname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_isdailybudgetname"); }
			private set { this.SetPropertyValue("new_isdailybudgetname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_isoverrefundpercentname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_isoverrefundpercentname")]
		public global::System.Object new_isoverrefundpercentname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_isoverrefundpercentname"); }
			private set { this.SetPropertyValue("new_isoverrefundpercentname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_ispricingendedname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_ispricingendedname")]
		public global::System.Object new_ispricingendedname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_ispricingendedname"); }
			private set { this.SetPropertyValue("new_ispricingendedname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_issystemreasonname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_issystemreasonname")]
		public global::System.Object new_issystemreasonname
		{
			get { return this.GetPropertyValue<global::System.Object>("new_issystemreasonname"); }
			private set { this.SetPropertyValue("new_issystemreasonname", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for organizationiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationiddsc")]
		public global::System.Int32? organizationiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("organizationiddsc"); }
			private set { this.SetPropertyValue("organizationiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for organizationidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationidname")]
		public global::System.String organizationidname
		{
			get { return this.GetPropertyValue<global::System.String>("organizationidname"); }
			private set { this.SetPropertyValue("organizationidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
