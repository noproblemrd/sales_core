﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generation tool version: 4.0.0.0
// Generation date: 1/21/2016 9:11:25 AM

namespace NoProblem.Core.DataModel.Xrm
{
	/// <summary>
	///  (new_zipcode)
	/// </summary>
	[global::System.Data.Services.Common.DataServiceKey("new_zipcodeid")]
	[global::Microsoft.Xrm.Client.Linq.CrmEntity("new_zipcode", "new_zipcodes")]
	public partial class new_zipcode : global::Microsoft.Xrm.Client.CrmEntity
	{
		private const string _logicalName = "new_zipcode";
		private const string _primaryKeyLogicalName = "new_zipcodeid";

		/// <summary>
		/// Default constructor.
		/// </summary>
		public new_zipcode()
			: base(_logicalName, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="context">The source context.</param>
		public new_zipcode(global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(_logicalName, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		public new_zipcode(global::Microsoft.Xrm.Client.ICrmEntity entity)
			: base(entity, _primaryKeyLogicalName)
		{
		}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="entity">The source entity.</param>
		/// <param name="context">The source context.</param>
		public new_zipcode(global::Microsoft.Xrm.Client.ICrmEntity entity, global::Microsoft.Xrm.Client.Data.Services.ICrmDataContext context)
			: base(entity, _primaryKeyLogicalName, context)
		{
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (Attribute for createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdby")]
		public global::System.Guid? createdby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("createdby"); }
			private set { this.SetPropertyValue("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who created the record. (N:1 Association for lk_new_zipcode_createdby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("createdby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_zipcode_createdby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_zipcode_createdby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("createdby", "systemuserid"); }
			private set { this.SetRelatedEntity("createdby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was created. (Attribute for createdon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdon")]
		public global::System.DateTime? createdon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("createdon"); }
			private set { this.SetPropertyValue("createdon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		///  (Attribute for deletionstatecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("deletionstatecode")]
		public global::System.Int32? deletionstatecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("deletionstatecode"); }
			private set { this.SetPropertyValue("deletionstatecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Sequence number of the import that created this record. (Attribute for importsequencenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("importsequencenumber")]
		public global::System.Int32? importsequencenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("importsequencenumber"); }
			set { this.SetPropertyValue("importsequencenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (Attribute for modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedby")]
		public global::System.Guid? modifiedby
		{
			get { return this.GetPropertyValue<global::System.Guid?>("modifiedby"); }
			private set { this.SetPropertyValue("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "systemuser"); }
		}

		/// <summary>
		/// Unique identifier of the user who modified the record. (N:1 Association for lk_new_zipcode_modifiedby)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("modifiedby")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("lk_new_zipcode_modifiedby")]
		public global::NoProblem.Core.DataModel.Xrm.systemuser lk_new_zipcode_modifiedby
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.systemuser>("modifiedby", "systemuserid"); }
			private set { this.SetRelatedEntity("modifiedby", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time when the record was modified. (Attribute for modifiedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedon")]
		public global::System.DateTime? modifiedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("modifiedon"); }
			private set { this.SetPropertyValue("modifiedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Unique identifier for city associated with Zip Code. (Attribute for new_cityid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_cityid")]
		public global::System.Guid? new_cityid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_cityid"); }
			set { this.SetPropertyValue("new_cityid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_city"); }
		}

		/// <summary>
		/// Unique identifier for city associated with Zip Code. (N:1 Association for new_new_city_new_zipcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_cityid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_new_city_new_zipcode")]
		public global::NoProblem.Core.DataModel.Xrm.new_city new_new_city_new_zipcode
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_city>("new_cityid", "new_cityid"); }
			set { this.SetRelatedEntity("new_cityid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_daylightsavingtime)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_daylightsavingtime")]
		public global::System.Boolean? new_daylightsavingtime
		{
			get { return this.GetPropertyValue<global::System.Boolean?>("new_daylightsavingtime"); }
			set { this.SetPropertyValue("new_daylightsavingtime", value, typeof(global::Microsoft.Crm.Sdk.CrmBoolean)); }
		}

		/// <summary>
		///  (Attribute for new_englishcityname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_englishcityname")]
		public global::System.String new_englishcityname
		{
			get { return this.GetPropertyValue<global::System.String>("new_englishcityname"); }
			set { this.SetPropertyValue("new_englishcityname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_fromhousenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_fromhousenumber")]
		public global::System.Int32? new_fromhousenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_fromhousenumber"); }
			set { this.SetPropertyValue("new_fromhousenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_latitude)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_latitude")]
		public global::System.Double? new_latitude
		{
			get { return this.GetPropertyValue<global::System.Double?>("new_latitude"); }
			set { this.SetPropertyValue("new_latitude", value, typeof(global::Microsoft.Crm.Sdk.CrmFloat)); }
		}

		/// <summary>
		///  (Attribute for new_localcityname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_localcityname")]
		public global::System.String new_localcityname
		{
			get { return this.GetPropertyValue<global::System.String>("new_localcityname"); }
			set { this.SetPropertyValue("new_localcityname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_longitude)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_longitude")]
		public global::System.Double? new_longitude
		{
			get { return this.GetPropertyValue<global::System.Double?>("new_longitude"); }
			set { this.SetPropertyValue("new_longitude", value, typeof(global::Microsoft.Crm.Sdk.CrmFloat)); }
		}

		/// <summary>
		/// The name of the custom entity. (Attribute for new_name)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_name")]
		public global::System.String new_name
		{
			get { return this.GetPropertyValue<global::System.String>("new_name"); }
			set { this.SetPropertyValue("new_name", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Unique identifier for Region associated with Zip Code. (Attribute for new_regionid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_regionid")]
		public global::System.Guid? new_regionid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_regionid"); }
			set { this.SetPropertyValue("new_regionid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_region"); }
		}

		/// <summary>
		/// Unique identifier for Region associated with Zip Code. (N:1 Association for new_new_region_new_zipcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_regionid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_new_region_new_zipcode")]
		public global::NoProblem.Core.DataModel.Xrm.new_region new_new_region_new_zipcode
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_region>("new_regionid", "new_regionid"); }
			set { this.SetRelatedEntity("new_regionid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Unique identifier for Street associated with Zip Code. (Attribute for new_streetid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_streetid")]
		public global::System.Guid? new_streetid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("new_streetid"); }
			set { this.SetPropertyValue("new_streetid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "new_street"); }
		}

		/// <summary>
		/// Unique identifier for Street associated with Zip Code. (N:1 Association for new_new_street_new_zipcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("new_streetid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("new_new_street_new_zipcode")]
		public global::NoProblem.Core.DataModel.Xrm.new_street new_new_street_new_zipcode
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.new_street>("new_streetid", "new_streetid"); }
			set { this.SetRelatedEntity("new_streetid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		///  (Attribute for new_timezone)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_timezone")]
		public global::System.Int32? new_timezone
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_timezone"); }
			set { this.SetPropertyValue("new_timezone", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_tohousenumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_tohousenumber")]
		public global::System.Int32? new_tohousenumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_tohousenumber"); }
			set { this.SetPropertyValue("new_tohousenumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		/// Unique identifier for entity instances (Attribute for new_zipcodeid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_zipcodeid", true)]
		public global::System.Guid new_zipcodeid
		{
			get { return this.GetPropertyValue<global::System.Guid>("new_zipcodeid"); }
			set { this.SetPropertyValue("new_zipcodeid", value, typeof(global::Microsoft.Crm.Sdk.Key)); }
		}

		/// <summary>
		/// Unique identifier for the organization (Attribute for organizationid)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationid")]
		public global::System.Guid? organizationid
		{
			get { return this.GetPropertyValue<global::System.Guid?>("organizationid"); }
			private set { this.SetPropertyValue("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup), "organization"); }
		}

		/// <summary>
		/// Unique identifier for the organization (N:1 Association for organization_new_zipcode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("organizationid")]
		[global::Microsoft.Xrm.Client.Linq.CrmAssociation("organization_new_zipcode")]
		public global::NoProblem.Core.DataModel.Xrm.organization organization_new_zipcode
		{
			get { return this.GetRelatedEntity<global::NoProblem.Core.DataModel.Xrm.organization>("organizationid", "organizationid"); }
			private set { this.SetRelatedEntity("organizationid", value, typeof(global::Microsoft.Crm.Sdk.Lookup)); }
		}

		/// <summary>
		/// Date and time that the record was migrated. (Attribute for overriddencreatedon)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("overriddencreatedon")]
		public global::System.DateTime? overriddencreatedon
		{
			get { return this.GetPropertyValue<global::System.DateTime?>("overriddencreatedon"); }
			set { this.SetPropertyValue("overriddencreatedon", value, typeof(global::Microsoft.Crm.Sdk.CrmDateTime)); }
		}

		/// <summary>
		/// Status of the Zip Code (Attribute for statecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecode")]
		public global::System.String statecode
		{
			get { return this.GetPropertyValue<global::System.String>("statecode"); }
			private set { this.SetPropertyValue("statecode", value, typeof(global::System.String)); }
		}

		/// <summary>
		/// Reason for the status of the Zip Code (Attribute for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscode", "statuscodeLabel")]
		public global::System.Int32? statuscode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("statuscode"); }
			set { this.SetPropertyValue("statuscode", value, typeof(global::Microsoft.Crm.Sdk.Status)); }
		}

		/// <summary>
		/// Reason for the status of the Zip Code (Label for statuscode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmNamed("statuscode")]
		public global::System.String statuscodeLabel
		{
			get { return this.GetPropertyLabel("statuscode"); }
		}

		/// <summary>
		///  (Attribute for timezoneruleversionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("timezoneruleversionnumber")]
		public global::System.Int32? timezoneruleversionnumber
		{
			get { return this.GetPropertyValue<global::System.Int32?>("timezoneruleversionnumber"); }
			set { this.SetPropertyValue("timezoneruleversionnumber", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for utcconversiontimezonecode)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("utcconversiontimezonecode")]
		public global::System.Int32? utcconversiontimezonecode
		{
			get { return this.GetPropertyValue<global::System.Int32?>("utcconversiontimezonecode"); }
			set { this.SetPropertyValue("utcconversiontimezonecode", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for versionnumber)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("versionnumber")]
		public global::System.String versionnumber
		{
			get { return this.GetPropertyValue<global::System.String>("versionnumber"); }
			private set { this.SetPropertyValue("versionnumber", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (1:N Association for new_new_zipcode_incident)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_new_zipcode_incident", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.incident> new_new_zipcode_incident
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.incident>("new_zipcodeid", "new_zipcodeid"); }
		}

		/// <summary>
		///  (1:N Association for new_new_zipcode_new_servicearea)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_new_zipcode_new_servicearea", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.new_servicearea> new_new_zipcode_new_servicearea
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.new_servicearea>("new_zipcodeid", "new_zipcodeid"); }
		}

		/// <summary>
		///  (1:N Association for new_zipcode_AsyncOperations)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_zipcode_AsyncOperations", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.asyncoperation> new_zipcode_AsyncOperations
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.asyncoperation>("new_zipcodeid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_zipcode_BulkDeleteFailures)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_zipcode_BulkDeleteFailures", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure> new_zipcode_BulkDeleteFailures
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.bulkdeletefailure>("new_zipcodeid", "regardingobjectid"); }
		}

		/// <summary>
		///  (1:N Association for new_zipcode_DuplicateBaseRecord)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_zipcode_DuplicateBaseRecord", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.duplicaterecord> new_zipcode_DuplicateBaseRecord
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.duplicaterecord>("new_zipcodeid", "baserecordid"); }
		}

		/// <summary>
		///  (1:N Association for new_zipcode_DuplicateMatchingRecord)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmAssociationSet("new_zipcode_DuplicateMatchingRecord", true)]
		public global::System.Collections.Generic.IEnumerable<global::NoProblem.Core.DataModel.Xrm.duplicaterecord> new_zipcode_DuplicateMatchingRecord
		{
			get { return this.GetRelatedEntities<global::NoProblem.Core.DataModel.Xrm.duplicaterecord>("new_zipcodeid", "duplicaterecordid"); }
		}

		#region System Attribute Members

		/*

		/// <summary>
		///  (Attribute for createdbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbydsc")]
		public global::System.Int32? createdbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("createdbydsc"); }
			private set { this.SetPropertyValue("createdbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for createdbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("createdbyname")]
		public global::System.String createdbyname
		{
			get { return this.GetPropertyValue<global::System.String>("createdbyname"); }
			private set { this.SetPropertyValue("createdbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for modifiedbydsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbydsc")]
		public global::System.Int32? modifiedbydsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("modifiedbydsc"); }
			private set { this.SetPropertyValue("modifiedbydsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for modifiedbyname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("modifiedbyname")]
		public global::System.String modifiedbyname
		{
			get { return this.GetPropertyValue<global::System.String>("modifiedbyname"); }
			private set { this.SetPropertyValue("modifiedbyname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_cityiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_cityiddsc")]
		public global::System.Int32? new_cityiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_cityiddsc"); }
			private set { this.SetPropertyValue("new_cityiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_cityidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_cityidname")]
		public global::System.String new_cityidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_cityidname"); }
			private set { this.SetPropertyValue("new_cityidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_daylightsavingtimename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_daylightsavingtimename")]
		public global::System.Object new_daylightsavingtimename
		{
			get { return this.GetPropertyValue<global::System.Object>("new_daylightsavingtimename"); }
			private set { this.SetPropertyValue("new_daylightsavingtimename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for new_regioniddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_regioniddsc")]
		public global::System.Int32? new_regioniddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_regioniddsc"); }
			private set { this.SetPropertyValue("new_regioniddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_regionidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_regionidname")]
		public global::System.String new_regionidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_regionidname"); }
			private set { this.SetPropertyValue("new_regionidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for new_streetiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_streetiddsc")]
		public global::System.Int32? new_streetiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("new_streetiddsc"); }
			private set { this.SetPropertyValue("new_streetiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for new_streetidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("new_streetidname")]
		public global::System.String new_streetidname
		{
			get { return this.GetPropertyValue<global::System.String>("new_streetidname"); }
			private set { this.SetPropertyValue("new_streetidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for organizationiddsc)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationiddsc")]
		public global::System.Int32? organizationiddsc
		{
			get { return this.GetPropertyValue<global::System.Int32?>("organizationiddsc"); }
			private set { this.SetPropertyValue("organizationiddsc", value, typeof(global::Microsoft.Crm.Sdk.CrmNumber)); }
		}

		/// <summary>
		///  (Attribute for organizationidname)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("organizationidname")]
		public global::System.String organizationidname
		{
			get { return this.GetPropertyValue<global::System.String>("organizationidname"); }
			private set { this.SetPropertyValue("organizationidname", value, typeof(global::System.String)); }
		}

		/// <summary>
		///  (Attribute for statecodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statecodename")]
		public global::System.Object statecodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statecodename"); }
			private set { this.SetPropertyValue("statecodename", value, typeof(global::System.Object)); }
		}

		/// <summary>
		///  (Attribute for statuscodename)
		/// </summary>
		[global::Microsoft.Xrm.Client.Linq.CrmProperty("statuscodename")]
		public global::System.Object statuscodename
		{
			get { return this.GetPropertyValue<global::System.Object>("statuscodename"); }
			private set { this.SetPropertyValue("statuscodename", value, typeof(global::System.Object)); }
		}

		*/

		#endregion
	}
}
