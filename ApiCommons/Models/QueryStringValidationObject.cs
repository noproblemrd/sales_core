﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiCommons.Models
{
    public class QueryStringValidationObject
    {
        public QueryStringValidationObject(IQueryStringParameterValidationAttribute queryStringValidationAttribute, object value)
        {
            QueryStringValidationAttribute = queryStringValidationAttribute;
            Value = value;
        }

        public IQueryStringParameterValidationAttribute QueryStringValidationAttribute { get; private set; }
        public object Value { get; private set; }
    }
}