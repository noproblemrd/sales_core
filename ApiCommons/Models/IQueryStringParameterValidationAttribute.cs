﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApiCommons.Models
{
    public interface IQueryStringParameterValidationAttribute
    {
        bool IsValueValid(object value);

        string GetErrorMessage();
    }
}
