﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using log4net;
using System.Reflection;

namespace ApiCommons.Controllers
{
    public class ExceptionHandler
    {
        private static readonly ILog logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public static void HandleException(Exception exc)
        {
            if (exc is HttpResponseException)
            {
                throw exc;
            }
            logger.Error("Exception!.", exc);
            Exception currentExc = exc;
            ExceptionWrapper currentWrapper = new ExceptionWrapper();
            ExceptionWrapper mainWrapper = currentWrapper;
            while (true)
            {
                currentWrapper.Message = currentExc.Message;
                currentWrapper.StackTrace = currentExc.StackTrace;
                currentExc = currentExc.InnerException;
                if (currentExc != null)
                {
                    currentWrapper.InnerException = new ExceptionWrapper();
                    currentWrapper = currentWrapper.InnerException;
                }
                else
                {
                    break;
                }
            }
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(mainWrapper)) });

        }

        public class ExceptionWrapper
        {
            public string Message { get; set; }
            public string StackTrace { get; set; }
            public ExceptionWrapper InnerException { get; set; }
        }
    }
}