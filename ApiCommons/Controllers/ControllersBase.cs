﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;
using System.Net.Http;
using System.Net;
using System.Web.Http;

namespace ApiCommons.Controllers
{
    public abstract class ControllersBase : ApiController
    {
        private const string ERROR_JSON = @"{""errors"":""|ERRORS_PLACEHOLDER|""}";
        private const string ERRORS_PLACEHOLDER = "|ERRORS_PLACEHOLDER|";

        public void ValidateModel(object request, ModelStateDictionary ModelState)
        {
            List<string> errors = GetModelErrors(request, ModelState);
            if (errors != null)
            {
                ThrowValidationErrorsException(errors);
            }
        }

        protected List<string> GetModelErrors(object request, ModelStateDictionary ModelState)
        {
            if (request == null)
            {
                return new List<string> { "Invalid request" };
            }
            else if (!ModelState.IsValid)
            {
                var errors = ModelState
                        .Where(e => e.Value.Errors.Count > 0)
                        .Select(e => new
                        {
                            e.Value.Errors.First().ErrorMessage,
                            e.Value.Errors.First().Exception
                        }
                        ).Where(e => !string.IsNullOrEmpty(e.ErrorMessage) || e.Exception != null);
                return errors.Select(x => !String.IsNullOrEmpty(x.ErrorMessage) ? x.ErrorMessage : x.Exception.Message).ToList();
            }
            return null;
        }

        protected List<string> queryStringParamtersErrors;

        protected void ValidateQueryStringParameters(params Models.QueryStringValidationObject[] parameters)
        {
            queryStringParamtersErrors = new List<string>();
            foreach (var item in parameters)
            {
                if (!item.QueryStringValidationAttribute.IsValueValid(item.Value))
                {
                    queryStringParamtersErrors.Add(item.QueryStringValidationAttribute.GetErrorMessage());
                }
            }
            if (queryStringParamtersErrors.Count > 0)
            {
                ThrowValidationErrorsException(queryStringParamtersErrors);
            }
        }

        private void ThrowValidationErrorsException(List<string> errors)
        {
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ERROR_JSON.Replace(ERRORS_PLACEHOLDER, String.Join(" ", errors))) });
        }
    }
}