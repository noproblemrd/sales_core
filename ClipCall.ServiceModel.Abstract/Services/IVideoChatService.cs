﻿using System.ServiceModel;

//using ClipCall.ServiceModel.Abstract.DataContracts.Request.Chat;
using System;
using ClipCall.DomainModel.Entities.VideoInvitation.Response;
using ClipCall.DomainModel.Entities.VideoInvitation.Request;


namespace ClipCall.ServiceModel.Abstract.Services
{

    [ServiceContract]
    public interface IVideoChatService
    {
       // [OperationContract]
    //    VideoSession CreateVideoSession();

        [OperationContract]
        CreateVideoChatInvitationResponse CreateVideoChatInvitation(CreateInvitationRequest request);

        [OperationContract]
        InvitationCheckInResponse InvitationCheckIn(VideoChatRequestBase request);

        [OperationContract]
        void SetFavoriteServiceProvider(VideoChatCustomerBaseRequest request);

        [OperationContract]
        CreateVideoChatConversationResponse CreateVideoChatCall(VideoChatCustomerBaseRequest request);

        
        [OperationContract]
        void OpentokArchiveStatusChanges(OpentokArchiveStatusChangesRequest request);

        [OperationContract]
        void OnSentTextMessage(VideoChatSupplierBaseRequest request);

        [OperationContract]
        void OnSupplierAnswer(VideoChatSupplierBaseRequest request);

        [OperationContract]
        GetFavoriteServiceProviderResponse GetFavoriteServiceProvider(InvitationFavoriteRequest request);
       
    }
}
