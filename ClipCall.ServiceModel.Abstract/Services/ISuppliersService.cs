﻿using System.Collections.Generic;
using System.ServiceModel;
using ClipCall.ServiceModel.Abstract.DataContracts.Request;

namespace ClipCall.ServiceModel.Abstract.Services
{
    [ServiceContract]
    public interface ISuppliersService
    {
        [OperationContract]
        void ConnectToCustomer(ConnecToCustomerRequestData request);

        [OperationContract]
        SupplierLoginResponseData Login(SupplierLoginRequestData request);

        [OperationContract]
        void ReportLead(LeadReportRequestData request);

        [OperationContract]
        List<LeadReportStatusData> GetLeadReportStatuses();
    }
}
