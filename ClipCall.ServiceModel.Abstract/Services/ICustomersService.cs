﻿using System.ServiceModel;
using ClipCall.ServiceModel.Abstract.DataContracts.Request;
using ClipCall.ServiceModel.Abstract.DataContracts.Response;


namespace ClipCall.ServiceModel.Abstract.Services
{
    [ServiceContract]
    public interface ICustomersService
    {
        [OperationContract]
        LeadDetailsResponseData GetLeadDetails(LeadDetailsRequestData request);
    }
}
