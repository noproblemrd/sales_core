﻿namespace ClipCall.ServiceModel.Abstract.DataContracts.Request
{
    public class SupplierLoginRequestData
    {
        public string PhoneNumber { get; set; }
        public string Uid { get; set; }
        public string DeviceOs { get; set; }
        public string DeviceName { get; set; }
        public string OsVersion { get; set; }
        public string NpAppVersion { get; set; }
    }
}