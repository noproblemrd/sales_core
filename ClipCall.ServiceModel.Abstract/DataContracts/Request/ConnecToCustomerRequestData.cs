﻿using System;

namespace ClipCall.ServiceModel.Abstract.DataContracts.Request
{
    public class ConnecToCustomerRequestData
    {
        public bool ShouldRecordCall { get; set; }
        public Guid SupplierId { get; set; }
        public Guid IncidentAccountId { get; set; }
    }
}
