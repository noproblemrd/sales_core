﻿using System;

namespace ClipCall.ServiceModel.Abstract.DataContracts.Request
{
    public class LeadDetailsRequestData
    {
        public Guid LeadId { get; set; }
        public Guid CustomerId { get; set; }
    }
}