﻿using System;

namespace ClipCall.ServiceModel.Abstract.DataContracts.Request
{
    public class LeadReportRequestData
    {
        public Guid SupplierId { get; set; }
        public Guid IncidentAccountId { get; set; }
        public string Comment { get; set; }
        public int ReasonCode { get; set; }
    }

    public class LeadReportStatusData
    {
        public int ReasonCode { get; set; }
        public string Name { get; set; }
        public Guid Id { get; set; }
    }
}