﻿using System;
using Castle.Core.Logging;
using Castle.DynamicProxy;
using Newtonsoft.Json;

namespace NoProblem.Core.Interfaces.Extensibility.Windsor.Interceptor
{
    public class LoggerInterceptor : IInterceptor
    {
        private readonly ILoggerFactory factory;

        public LoggerInterceptor(ILoggerFactory factory)
        {
            this.factory = factory;
        }
        public void Intercept(IInvocation invocation)
        {
            ILogger logger = factory.Create(invocation.TargetType);
            if (logger.IsDebugEnabled)
            {
                logger.DebugFormat("{0}({1})", invocation.Method.Name, JsonConvert.SerializeObject(invocation.Arguments));                
            }

            try
            {
                invocation.Proceed();
                if (invocation.Method.ReturnType == typeof (void))
                    return;

                if (logger.IsDebugEnabled)
                {
                    logger.DebugFormat("{0} return value: {1}", invocation.Method.Name,
                        JsonConvert.SerializeObject(invocation.ReturnValue));
                }
            }

            catch (Exception e)
            {
                logger.ErrorFormat("{0} failed with exception message: {1}" + invocation.Method.Name, e.Message);
                logger.Error("stack trace: " + e.StackTrace);
                throw e;
            }

            
            
        }
    }
}
