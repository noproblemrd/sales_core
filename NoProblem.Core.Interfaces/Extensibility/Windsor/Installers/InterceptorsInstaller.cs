﻿using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using NoProblem.Core.Interfaces.Extensibility.Windsor.Interceptor;

namespace NoProblem.Core.Interfaces.Extensibility.Windsor.Installers
{
    public class InterceptorsInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IInterceptor>()
                    .ImplementedBy<LoggerInterceptor>()
                    .Named(typeof(LoggerInterceptor).FullName));
        }
    }
}