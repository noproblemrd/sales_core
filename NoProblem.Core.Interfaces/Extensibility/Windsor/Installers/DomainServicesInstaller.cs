﻿using System.Configuration;
using System.Web.Configuration;
using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using NoProblem.Core.BusinessLogic;
using NoProblem.Core.BusinessLogic.ClipCall.Chat;
using NoProblem.Core.BusinessLogic.ClipCall.Customers;
using NoProblem.Core.BusinessLogic.ClipCall.Leads;
using NoProblem.Core.BusinessLogic.ClipCall.Suppliers;
using NoProblem.Core.BusinessLogic.PublisherPortal;
using NoProblem.Core.DataAccessLayer;
using NoProblem.Core.DataAccessLayer.Bases;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Xrm;
using NoProblem.Core.Interfaces.Extensibility.Windsor.Interceptor;
using OpenTokSDK;
using Contact = NoProblem.Core.DataAccessLayer.Contact;

namespace NoProblem.Core.Interfaces.Extensibility.Windsor.Installers
{
    public class DomainServicesInstaller : IWindsorInstaller
    {

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {


            int apiKey = int.Parse(WebConfigurationManager.AppSettings["openTokApiKey"]);
            string secret = WebConfigurationManager.AppSettings["openTokSecret"];
            container.Register(Component.For<OpenTok>().UsingFactoryMethod(() => new OpenTok(apiKey, secret)));


            container.Register(Component.For<VideoChatManager>());


            container.Register(Component.For<SuppliersManager>()
                .Interceptors(InterceptorReference.ForType<LoggerInterceptor>()).First);

            container.Register(Component.For<CustomerManager>()
                .Interceptors(InterceptorReference.ForType<LoggerInterceptor>()).First);


            container.Register(Component.For<Repository>());
            

            container.Register(Component.For<LeadsManager>()
                .Interceptors(InterceptorReference.ForType<LoggerInterceptor>()).First);

            container.Register(Component.For<Notifications>());



            container.Register(Component.For<IncidentAccount>());
            container.Register(Component.For<Contact>());
            container.Register(Component.For<AccountRepository>());

            container.Register(Component.For<ConsumersManager>()
                .Interceptors(InterceptorReference.ForType<LoggerInterceptor>()).First);
            container.Register(
                Component.For<DataContext>().UsingFactoryMethod( (kernel,creationContext) =>
                    XrmDataContext.Create()
                    )
                );

            container.Register(
                Component.For<DataContext>()
                    .DependsOn(Dependency.OnValue("name",ConfigurationManager.ConnectionStrings["XrmDataContext"])));
        }
    }
}