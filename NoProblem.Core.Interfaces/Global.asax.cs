﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using System.Threading;
using System.Web;
using AutoMapper;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using ClipCall.ServiceModel.Abstract;
using ClipCall.ServiceModel.Abstract.Services;
using ClipCall.ServiceModel.Implementation;
using Effect.Crm.Logs;
using NoProblem.Core.Interfaces.AutoMapper;
using NoProblem.Core.Interfaces.Extensibility.Windsor.Installers;

namespace NoProblem.Core.Interfaces
{
    public class Global : System.Web.HttpApplication
    {
        private static List<object> timersHolder = new List<object>();

        private IWindsorContainer container;

        protected void Application_Start(object sender, EventArgs e)
        {
            LogUtils.MyHandle.WriteToLog("Application Started");

            container = new WindsorContainer();
            container.Install(new LoggerInstaller());
            container.Install(new DomainServicesInstaller());
            container.Install(new InterceptorsInstaller());

            container.AddFacility<WcfFacility>(f => f.CloseTimeout = TimeSpan.Zero);

            container.Register(Component.For<ICustomersService>().ImplementedBy<CustomersService>()
                    .AsWcfService(new DefaultServiceModel().Hosted()));

            container.Register(Component.For<ISuppliersService>().ImplementedBy<SuppliersService>()
                    .AsWcfService(new DefaultServiceModel().Hosted()));


            container.Register(Component.For<IVideoChatService>().ImplementedBy<VideoChatService>()
                    .AsWcfService(new DefaultServiceModel().Hosted()));

            Mapper.Initialize(cfg => cfg.AddProfile<CustomerProfile>());
               

            //initialize fone api router instance:
            var foneApiRouterInstance = BusinessLogic.FoneApiBL.FoneApiRouter.Instance;

            BusinessLogic.Scheduler.JobScheduler.Start();

            string host = System.Net.Dns.GetHostName();
            LogUtils.MyHandle.WriteToLog("host: " + host);
            string secondaryServer = ConfigurationManager.AppSettings["SecondaryServer"].ToLower();
            //if (host.ToLower() != "c70415wpapp02")
            if (host.ToLower() != secondaryServer)
            {
                LogUtils.MyHandle.WriteToLog("setting timers in host: " + host);
                SetWaitingForInvitationsTimer();
                SetGiveMoreInvitationsTimer();
                SetTimersForExpiredCalls();
                SetBalanceFixerTimer();
                SetFinancialDashboardTimer();
                SetCallForReportViewCreatorTimer();
                SetCaseWaitingFroStepTimer();
                SetDapazUserSyncTimer();
           //     SetRealTimeRoutingErrorPrevention(); Remove at 06-01-2015
                StartServiceRequestJob();
            }

            SetTimerForAutoResubmission();
            StartCoverAreaUpdater();
            StartPendingCallChargesChecker();
            StartPendingDesktopAppChargesChecker();
         //   StartEmailReportsReader();
            StartInjectionReportsApiReader();
            
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception lastError = Server.GetLastError();
            if (lastError is HttpException)
            {
                HttpException lastErrorWrapper = Server.GetLastError() as HttpException;
                if (lastErrorWrapper.InnerException != null)
                    lastError = lastErrorWrapper.InnerException;
            }
            string lastErrorTypeName = lastError.GetType().ToString();
            string lastErrorMessage = lastError.Message;
            string lastErrorStackTrace = lastError.StackTrace;

            string toPrint = string.Format(@"
An Unhandled Exception Has Occurred!
URL: {0}
User: {1}
Exception Type: {2}
Message: {3}
Stack Trace: {4}",
                Request.RawUrl,
                User.Identity.Name,
                lastErrorTypeName,
                lastErrorMessage,
                lastErrorStackTrace);

            LogUtils.MyHandle.WriteToLog("UNHANDLED EXCEPTION!!! Application_Error:" + toPrint);

            const string ToAddress = "unhandledexceptiongroup@onecall.co.il";
            const string FromAddress = "unhandledexception@onecall.co.il";
            const string Subject = "An Unhandled Exception Has Occurred!";

            // Create the MailMessage object
            MailMessage mm = new MailMessage(FromAddress, ToAddress);
            mm.Subject = Subject;
            mm.IsBodyHtml = true;
            mm.Priority = MailPriority.High;
            mm.Body = string.Format(@"
<html>
<body>
  <h1>An Error Has Occurred!</h1>
  <table cellpadding=""5"" cellspacing=""0"" border=""1"">
  <tr>
  <tdtext-align: right;font-weight: bold"">URL:</td>
  <td>{0}</td>
  </tr>
  <tr>
  <tdtext-align: right;font-weight: bold"">User:</td>
  <td>{1}</td>
  </tr>
  <tr>
  <tdtext-align: right;font-weight: bold"">Exception Type:</td>
  <td>{2}</td>
  </tr>
  <tr>
  <tdtext-align: right;font-weight: bold"">Message:</td>
  <td>{3}</td>
  </tr>
  <tr>
  <tdtext-align: right;font-weight: bold"">Stack Trace:</td>
  <td>{4}</td>
  </tr> 
  </table>
</body>
</html>",
                Request.RawUrl,
                User.Identity.Name,
                lastErrorTypeName,
                lastErrorMessage,
                lastErrorStackTrace.Replace(Environment.NewLine, "<br />"));


            if (lastError is HttpException)
            {
                // Attach the Yellow Screen of Death for this error   
                string YSODmarkup = ((HttpException)lastError).GetHtmlErrorMessage();
                if (!string.IsNullOrEmpty(YSODmarkup))
                {
                    Attachment YSOD = Attachment.CreateAttachmentFromString(YSODmarkup, "YSOD.htm");
                    mm.Attachments.Add(YSOD);
                }
            }

            // Send the email
            SmtpClient smtp = new SmtpClient("localhost");
            smtp.Send(mm);
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            LogUtils.MyHandle.WriteToLog("Application Ending...");
            try
            {
                //BusinessLogic.Dashboard.ExposureCreator exposureCreator = new NoProblem.Core.BusinessLogic.Dashboard.ExposureCreator();
                //exposureCreator.Flush();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.Application_End");
            }
            LogUtils.MyHandle.WriteToLog("Application Ended.");
        }

        private void SetWaitingForInvitationsTimer()
        {
            try
            {
                Timer waitingForInviteTimer = new Timer(
                        new TimerCallback(BusinessLogic.Invitations.InvitationsManager.CheckWaitingForInvites),
                        null,
                        GetTimeUntilNextHour(14),
                        new TimeSpan(12, 0, 0));
                //0,
                // 240000);

                timersHolder.Add(waitingForInviteTimer);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetWaitingForInvitationsTimer");
            }
        }

        private void SetGiveMoreInvitationsTimer()
        {
            try
            {
                Timer giveMoreInvitationsTimer = new Timer(
                        new TimerCallback(BusinessLogic.Invitations.InvitationsManager.GiveMoreInvites),
                        null,
                        GetTimeUntilNextDayOfWeek(DayOfWeek.Saturday, 14),
                        new TimeSpan(7, 0, 0, 0, 0));

                timersHolder.Add(giveMoreInvitationsTimer);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetGiveMoreInvitationsTimer");
            }
        }

        private TimeSpan GetTimeUntilNextHour(int hour)
        {
            var currentTime = DateTime.Now;
            var desiredTime = new DateTime(currentTime.Year,
                currentTime.Month, currentTime.Day, hour, 0, 0);
            var timePeriod = currentTime.Hour >= hour ?
                (desiredTime.AddDays(1) - currentTime) :
                (desiredTime - currentTime);
            return timePeriod;
        }

        private TimeSpan GetTimeUntilNextDayOfWeek(DayOfWeek dayOfWeek, int hour)
        {
            TimeSpan retVal;
            DateTime currentTime = DateTime.Now;
            var nearest = currentTime.AddDays(6 - ((currentTime.DayOfWeek == dayOfWeek) ? 6 : (int)currentTime.DayOfWeek));
            if (nearest.Date == currentTime.Date)
            {
                var desiredTime = new DateTime(currentTime.Year,
                    currentTime.Month, currentTime.Day, hour, 0, 0);
                retVal = currentTime.Hour >= hour ?
                    (desiredTime.AddDays(7) - currentTime) :
                    (desiredTime - currentTime);
            }
            else
            {
                retVal = (new DateTime(nearest.Year, nearest.Month, nearest.Day, hour, 0, 0) - currentTime);
            }
            return retVal;
        }

        private void SetTimersForExpiredCalls()
        {
            try
            {

                BusinessLogic.SupplierBL.ExpiredIvr.ExpiredIvrManager manager = new NoProblem.Core.BusinessLogic.SupplierBL.ExpiredIvr.ExpiredIvrManager(null);
                manager.SetTimersOnAppStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetTimersForExpiredCalls");
            }
        }

        //private void SetDapazPpaSyncTimer()
        //{
        //    try
        //    {
        //        BusinessLogic.Dapaz.PpaSyncer.SetTimerOnAppStart();
        //    }
        //    catch (Exception exc)
        //    {
        //        LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetDapazPpaSyncTimer");
        //    }
        //}

        private void SetBalanceFixerTimer()
        {
            try
            {
                BusinessLogic.Utils.BalanceFixer.SetTimerOnAppStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetBalanceFixerTimer");
            }
        }

        private void SetCaseWaitingFroStepTimer()
        {
            try
            {
                BusinessLogic.ServiceRequest.WaitForStepTimer.SetTimerOnAppStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetCaseWaitingFroStepTimer");
            }
        }

        private void SetFinancialDashboardTimer()
        {
            try
            {
                BusinessLogic.Dashboard.FinancialDashboard.DataCreator.SetTimerAtAppStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetFinancialDashboardTimer");
            }
        }

        private void SetCallForReportViewCreatorTimer()
        {
            try
            {
                BusinessLogic.Dapaz.Reports.CallViewCreator.SetTimerOnAppStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetCallForReportViewCreatorTimer");
            }
        }

        private void SetDapazUserSyncTimer()
        {
            try
            {
                BusinessLogic.Dapaz.UsersSync.UsersSyncManager.SetTimerAtAppStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetDapazUserSyncTimer");
            }
        }

        private void SetRealTimeRoutingErrorPrevention()
        {
            try
            {
                BusinessLogic.RealTimeRoutingErrorPrevention.SetTimerOnAppStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetRealTimeRoutingErrorPrevention");
            }
        }

        private void SetTimerForAutoResubmission()
        {
            try
            {
                BusinessLogic.ServiceRequest.AutoResubmission.Resubmitter.OnAppStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.SetTimerForAutoResubmission");
            }
        }

        private void StartCoverAreaUpdater()
        {
            try
            {
                BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreasUpdater.OnApplicationStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.StartCoverAreaUpdater");
            }
        }
        private void StartServiceRequestJob()
        {
            try
            {
                NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestJobChecker.OnApplicationStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.StartServiceRequestJob");
            }
        }
        private void StartPendingCallChargesChecker()
        {
            try
            {
                BusinessLogic.Balance.PendingCallChargesChecker.OnAppStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.StartPendingCallChargesChecker");
            }
        }

        private void StartPendingDesktopAppChargesChecker()
        {
            try
            {
                BusinessLogic.ConsumerDesktop.PendingChargesChecker.OnAppStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.StartPendingDesktopAppChargesChecker");
            }
        }

        private void StartEmailReportsReader()
        {
            try
            {
                BusinessLogic.InjectionsDataReaders.EmailReportsReader.Scheduler.OnApplicationStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.StartEmailReportsReader");
            }
        }

        private void StartInjectionReportsApiReader()
        {
            try
            {
                BusinessLogic.InjectionsDataReaders.ApiAndFtpScheduler.OnApplicationStart();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Global.asax.StartInjectionReportsApiReader");
            }
        }
    }
}