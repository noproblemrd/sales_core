﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Services;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.PublisherPortal;
using NoProblem.Core.BusinessLogic.SupplierPricing;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.AdvertiserPortal;
using NoProblem.Core.DataModel.PublisherPortal;
using NoProblem.Core.DataModel.Regions;
using NoProblem.Core.DataModel.SupplierPricing;


namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for Supplier
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Supplier : NpServiceBase
    {
        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.LogOnResponse> SwitchToAdvertiserMode(Guid customerId)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.LogOnResponse> result = new Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.LogOnResponse>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.SwitchToAdvertiserModeManager manager = new BusinessLogic.SupplierBL.Registration2014.SwitchToAdvertiserModeManager(null);
                result.Value = manager.SwitchToAdvertiserMode(customerId);

            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SwitchToAdvertiserMode with Guid {0}", customerId);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.SupplierStatusContainer> GetSupplierStatus(Guid supplierId)
        {
            Result<DataModel.SupplierModel.SupplierStatusContainer> result = new Result<NoProblem.Core.DataModel.SupplierModel.SupplierStatusContainer>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSupplierStatus started. supplierId = {0}", supplierId);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.GetSupplierStatus(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetSupplierStatus failed with exception. supplierId = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Tables.TableRowData>> GetAllInactivityReasons()
        {
            Result<List<DataModel.Tables.TableRowData>> result = new Result<List<DataModel.Tables.TableRowData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllInactivityReasons started.");
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.GetAllInactivityReasons();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetAllInactivityReasons failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.DeactivateSupplierResponse> DeactivateSupplier(Guid supplierId, Guid inactivityReasonId, Guid userId)
        {
            Result<DataModel.SupplierModel.DeactivateSupplierResponse> result = new Result<DataModel.SupplierModel.DeactivateSupplierResponse>();
            try
            {
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.DeactivateSupplier(supplierId, inactivityReasonId, userId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "DeactivateSupplier failed with exception. supplierId = {0}. inactivityReasonId = {1}.", supplierId, inactivityReasonId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.SupplierStatus> ActivateSupplier(Guid supplierId, Guid userId)
        {
            Result<DataModel.SupplierModel.SupplierStatus> result = new Result<NoProblem.Core.DataModel.SupplierModel.SupplierStatus>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ActivateSupplier started. supplierId = {0}", supplierId);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.ActivateSupplier(supplierId, userId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "ActivateSupplier failed with exception. supplierId = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.SearchSuppliersResponse> SearchSuppliers(DataModel.SupplierModel.SearchSuppliersRequest request)
        {
            Result<DataModel.SupplierModel.SearchSuppliersResponse> result =
                new Result<DataModel.SupplierModel.SearchSuppliersResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SearchSuppliers started. searchValue = {0}. PageNumber = {1}. PageSize = {2}", request.SearchValue, request.PageNumber, request.PageSize);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.SearchSuppliers(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SearchSuppliers failed with exception. searchValue = {0}. PageNumber = {1}. PageSize = {2}", request.SearchValue, request.PageNumber, request.PageSize);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Expertise.UpdateSupplierPricesResponse> UpdateSupplierPrices(DataModel.Expertise.UpdateSupplierPricesRequest request)
        {
            Result<DataModel.Expertise.UpdateSupplierPricesResponse> result = new Result<DataModel.Expertise.UpdateSupplierPricesResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateSupplierPrices started. request: supplierId = {0}", request.SupplierId.ToString());
                BusinessLogic.Expertises.ExpertiseManager manager = new NoProblem.Core.BusinessLogic.Expertises.ExpertiseManager(null);
                result.Value = manager.UpdateSupplierPrices(request);
                if (request.AuditOn)
                {
                    BusinessLogic.Audit.AuditManager auditManager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                    auditManager.CreateAudit(request.AuditEntries);
                }
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "UpdateSupplierPrices failed with exception. request: supplierId = {0}", request.SupplierId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.MiniSiteData> GetMiniSiteData(Guid supplierId)
        {
            Result<DataModel.SupplierModel.MiniSiteData> result = new Result<DataModel.SupplierModel.MiniSiteData>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetMiniSiteData failed with exception. supplierId = {0}.", supplierId.ToString());
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.GetMiniSiteData(supplierId);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetMiniSiteData failed with exception. supplierId = {0}.", supplierId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Expertise.GetSupplierPricesResponse> GetSupplierPrices(DataModel.Expertise.GetSupplierPricesRequest request)
        {
            Result<DataModel.Expertise.GetSupplierPricesResponse> result = new Result<DataModel.Expertise.GetSupplierPricesResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSupplierPrices started. request: SupplierId = {0}.", request.SupplierId.ToString());
                BusinessLogic.Expertises.ExpertiseManager manager = new NoProblem.Core.BusinessLogic.Expertises.ExpertiseManager(null);
                result.Value = manager.GetSupplierPrices(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetSupplierPrices failed with exception. request: SupplierId = {0}.", request.SupplierId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<int> GetRankingInExpertise(DataModel.Expertise.GetRankingInExpertiseRequest request)
        {
            Result<int> result = new Result<int>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetRankingInExpertise started. request: ExpertiseIs = {0}, Price = {1}.", request.ExpertiseId.ToString(), request.Price.ToString());
                BusinessLogic.Expertises.ExpertiseManager manager = new NoProblem.Core.BusinessLogic.Expertises.ExpertiseManager(null);
                result.Value = manager.GetRankingInExpertise(request.ExpertiseId, request.Price, request.SupplierId);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetRankingInExpertise failed with exception. request: ExpertiseIs = {0}, Price = {1}.", request.ExpertiseId.ToString(), request.Price.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Regions.GetSupplierRegionsResponse> GetSupplierRegionsInTree(DataModel.Regions.GetSupplierRegionsRequest request)
        {
            Result<DataModel.Regions.GetSupplierRegionsResponse> result = new Result<NoProblem.Core.DataModel.Regions.GetSupplierRegionsResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSupplierRegionsInTree started. request: SupplierId = {0}, ParentRegionId = {1}.", request.SupplierId.ToString(), request.ParentRegionId.ToString());
                BusinessLogic.Regions.RegionsManager manager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(null);
                DataModel.Regions.GetSupplierRegionsResponse response = manager.GetSupplierRegionsInTree(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetSupplierRegionsInTree failed with exception. request: SupplierId = {0}, ParentRegionId = {1}.", request.SupplierId.ToString(), request.ParentRegionId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Regions.GetSupplierRegionsSearchResponse> GetSupplierRegionsSearchInTree(DataModel.Regions.GetSupplierRegionsSearchRequest request)
        {
            Result<DataModel.Regions.GetSupplierRegionsSearchResponse> result = new Result<NoProblem.Core.DataModel.Regions.GetSupplierRegionsSearchResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSupplierRegionsSearchInTree started. request: SupplierId = {0}, RegionName = {1}.", request.SupplierId.ToString(), request.RegionName);
                BusinessLogic.Regions.RegionsManager manager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(null);
                DataModel.Regions.GetSupplierRegionsSearchResponse response = manager.GetSupplierRegionsSearchInTree(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetSupplierRegionsSearchInTree failed with exception. request: SupplierId = {0}, RegionName = {1}.", request.SupplierId.ToString(), request.RegionName);
            }
            return result;
        }

        [WebMethod]
        public Result UpdateSupplierRegionsInTree(DataModel.Regions.UpdateSupplierRegionsRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateSupplierRegionsInTree started. request: SupplierId = {0}.", request.SupplierId.ToString());
                BusinessLogic.Regions.RegionsManager manager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(null);
                manager.UpdateSupplierRegionsInTree(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "UpdateSupplierRegionsInTree failed with exception. request: SupplierId = {0}", request.SupplierId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<GetRegionsInLevelByAncestorResponse> GetRegionsInLevelByAncestor(GetRegionsInLevelByAncestorRequest request)
        {
            Result<GetRegionsInLevelByAncestorResponse> result = new Result<GetRegionsInLevelByAncestorResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetRegionsInLevelByAncestor started. request: ancestorId = {0}, level = {1}", request.AncestorId, request.Level);
                BusinessLogic.Regions.RegionsManager manager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(null);
                GetRegionsInLevelByAncestorResponse response = manager.GetRegionsInLevelByAncestor(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetRegionsInLevelByAncestor failed with exception. request: ancestorId = {0}, level = {1}", request.AncestorId, request.Level);
            }
            return result;
        }

        [WebMethod]
        public string SupplierSignUp(string email, string cellphone, string originCode)
        {
            LogUtils.MyHandle.WriteToLog(2, "SupplierSignUp started. Email: {0}, Cellphone: {1}", email, cellphone);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.SupplierSignUp(email, cellphone, originCode);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SupplierSignUp Failed with Exception");
                retVal = "<SupplierSignUp><Error>Failed</Error></SupplierSignUp>";
            }

            return retVal;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.SupplierSignUpResponse> SupplierSignUpNew(DataModel.SupplierModel.SupplierSignUpRequest request)
        {
            Result<DataModel.SupplierModel.SupplierSignUpResponse> result = new Result<NoProblem.Core.DataModel.SupplierModel.SupplierSignUpResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SupplierSignUpNew started. MobilePhone = {0}", request.MobilePhone);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.SupplierSignUp(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in SupplierSignUpNew. MobilePhone = {0}", request.MobilePhone);
            }
            return result;
        }

        [WebMethod]
        public Result<bool> SendSupplierSignUpNotification(Guid supplierId)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SendSupplierSignUpNotification started. supplierId = {0}", supplierId);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.SendSignupNotification(supplierId);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in SendSupplierSignUpNotification. supplierId = {0}", supplierId);
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.CheckEmailInRegistrationRequestResponse> CheckEmailInRegistrationRequest(string email)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.CheckEmailInRegistrationRequestResponse> result =
                new Result<NoProblem.Core.DataModel.SupplierModel.CheckEmailInRegistrationRequestResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CheckEmailInRegistrationRequest started. email = {0}", email);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.CheckEmailInRegistrationRequest(email);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckEmailInRegistrationRequest. email = {0}", email);
            }
            return result;
        }

        [WebMethod]
        public Result ChangePhoneInRegistration(Guid supplierId)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ChangePhoneInRegistration started. supplierId = {0}", supplierId);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                manager.ChangePhoneInRegistration(supplierId);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in ChangePhoneInRegistration. supplierId = {0}", supplierId);
            }
            return result;
        }

        [WebMethod]
        public Result<HashSet<NoProblem.Core.DataModel.Expertise.CategoryGroupContainer>> GetAllCategoryGroups()
        {
            Result<HashSet<NoProblem.Core.DataModel.Expertise.CategoryGroupContainer>> result =
                new Result<HashSet<NoProblem.Core.DataModel.Expertise.CategoryGroupContainer>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllCategoryGroups started");
                ExpertiseManager manager = new ExpertiseManager(null);
                result.Value = manager.GetAllCategoryGroups();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllCategoryGroups");
            }
            return result;
        }

        [WebMethod]
        public Result<List<GuidStringPair>> GetAllCategories()
        {
            Result<List<GuidStringPair>> result = new Result<List<GuidStringPair>>();
            try
            {
        //        LogUtils.MyHandle.WriteToLog(8, "GetAllCategories started");
                ExpertiseManager manager = new ExpertiseManager(null);
                result.Value = manager.GetAllCategories();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllCategories");
            }
            return result;
        }

        [WebMethod]
        public Result<List<GuidStringPair>> GetCategoryGroup(Guid categoryId)
        {
            Result<List<GuidStringPair>> result = new Result<List<GuidStringPair>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetCategoryGroup started. categoryId = {0}", categoryId);
                ExpertiseManager manager = new ExpertiseManager(null);
                result.Value = manager.GetCategoryGroup(categoryId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCategoryGroup. categoryId = {0}", categoryId);
            }
            return result;
        }

        [WebMethod(MessageName = "GetCategoryGroupByName")]
        public Result<List<GuidStringPair>> GetCategoryGroup(string categoryName)
        {
            Result<List<GuidStringPair>> result = new Result<List<GuidStringPair>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetCategoryGroup started. categoryName = {0}", categoryName);
                ExpertiseManager manager = new ExpertiseManager(null);
                result.Value = manager.GetCategoryGroup(categoryName);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCategoryGroup. categoryName = {0}", categoryName);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SeoData.SEOLeadsForHeading> SEOLeadsForHeading(Guid headingId)
        {
            Result<DataModel.SeoData.SEOLeadsForHeading> result = new Result<NoProblem.Core.DataModel.SeoData.SEOLeadsForHeading>();
            try
            {
                BusinessLogic.SEODataServices.SEOHeadingDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOHeadingDataRetriever(null);
                result.Value = retriever.GetLeadsForHeading(headingId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOLeadsForHeading. headingId = {0}", headingId);
            }
            return result;
        }

        [WebMethod(MessageName = "SEOLeadsForHeadingByName")]
        public Result<DataModel.SeoData.SEOLeadsForHeading> SEOLeadsForHeading(string headingName)
        {
            Result<DataModel.SeoData.SEOLeadsForHeading> result = new Result<NoProblem.Core.DataModel.SeoData.SEOLeadsForHeading>();
            try
            {
                BusinessLogic.SEODataServices.SEOHeadingDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOHeadingDataRetriever(null);
                result.Value = retriever.GetLeadsForHeading(headingName);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOLeadsForHeading. headingName = {0}", headingName);
            }
            return result;
        }

        [WebMethod]
        public Result<List<string>> SEOGetPopularHeadingsInRegion(string state)
        {
            Result<List<string>> result = new Result<List<string>>();
            try
            {
                BusinessLogic.SEODataServices.SEOGeneralDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOGeneralDataRetriever(null);
                result.Value = retriever.GetPopularHeadingsInRegion(state);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOGetPopularHeadingsInRegion. state = {0}", state);
            }
            return result;
        }

        [WebMethod(MessageName = "SEOGetPopularHeadingsInRegionCityLevel")]
        public Result<List<string>> SEOGetPopularHeadingsInRegion(string state, string city)
        {
            Result<List<string>> result = new Result<List<string>>();
            try
            {
                BusinessLogic.SEODataServices.SEOGeneralDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOGeneralDataRetriever(null);
                result.Value = retriever.GetPopularHeadingsInRegion(state, city);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOGetPopularHeadingsInRegion. state = {0}. city = {1}", state, city);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SeoData.SEOLeadsAndHeadingForGroup> SEOGetLeadsAndHeadingsForCategoryGroup(string categoryGroupName)
        {
            Result<DataModel.SeoData.SEOLeadsAndHeadingForGroup> result = new Result<NoProblem.Core.DataModel.SeoData.SEOLeadsAndHeadingForGroup>();
            try
            {
                BusinessLogic.SEODataServices.SEOHeadingDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOHeadingDataRetriever(null);
                result.Value = retriever.GetLeadsAndHeadingsForCategoryGroup(categoryGroupName);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOGetLeadsAndHeadingsForCategoryGroup. categoryGroupName = {0}", categoryGroupName);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SeoData.SEOLeadsAndSupplierForHeadingAndRegion> SEOGetLeadsAndSuppliers(string headingName, string state)
        {
            Result<DataModel.SeoData.SEOLeadsAndSupplierForHeadingAndRegion> result = new Result<NoProblem.Core.DataModel.SeoData.SEOLeadsAndSupplierForHeadingAndRegion>();
            /*
            try
            {
                BusinessLogic.SEODataServices.SEOHeadingDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOHeadingDataRetriever(null);
                result.Value = retriever.GetLeadsAndSuppliers(headingName, state);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOGetLeadsAndSuppliers. headingName = {0}, state = {1}", headingName, state);
            }
             * */
            return result;
        }

        [WebMethod(MessageName = "SEOGetLeadsAndSuppliersWithStateAndCity")]
        public Result<DataModel.SeoData.SEOLeadsAndSupplierForHeadingAndRegion> SEOGetLeadsAndSuppliers(string headingName, string state, string city)
        {
            Result<DataModel.SeoData.SEOLeadsAndSupplierForHeadingAndRegion> result = new Result<NoProblem.Core.DataModel.SeoData.SEOLeadsAndSupplierForHeadingAndRegion>();
            /*
            try
            {
                BusinessLogic.SEODataServices.SEOHeadingDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOHeadingDataRetriever(null);
                result.Value = retriever.GetLeadsAndSuppliers(headingName, state, city);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOGetLeadsAndSuppliersWithStateAndCity. headingName = {0}, state = {1}, city = {2}", headingName, state, city);
            }
             * */
            return result;
        }

        [WebMethod]
        public Result<DataModel.SeoData.SEOLeadsAndSupplierForHeadingAndRegion> SEOGetLeadsAndSuppliersWithZipcode(string headingName, string zipcode)
        {
            Result<DataModel.SeoData.SEOLeadsAndSupplierForHeadingAndRegion> result = new Result<NoProblem.Core.DataModel.SeoData.SEOLeadsAndSupplierForHeadingAndRegion>();
            try
            {
                BusinessLogic.SEODataServices.SEOHeadingDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOHeadingDataRetriever(null);
                result.Value = retriever.GetLeadsAndSuppliersWithZipCode(headingName, zipcode);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOGetLeadsAndSuppliersWithZipcode. headingName = {0}, zipcode = {1}", headingName, zipcode);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SeoData.SEOSupplierPageData> SEOGetSupplierPageData(NoProblem.Core.DataModel.SeoData.SEOGetSupplierPageDataRequest request)
        {
            Result<DataModel.SeoData.SEOSupplierPageData> result = new Result<NoProblem.Core.DataModel.SeoData.SEOSupplierPageData>();
            try
            {

                BusinessLogic.SEODataServices.SEOSupplierDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOSupplierDataRetriever(null);
                result.Value = retriever.GetSupplierPageData(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOGetSupplierPageData. supplierId = {0}, zipcode = {1}, state = {2}, city = {3}", request.SupplierId, request.Zipcode, request.State, request.City);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.SeoData.SEOStateData>> SEOGetAllStates()
        {
            Result<List<DataModel.SeoData.SEOStateData>> result = new Result<List<NoProblem.Core.DataModel.SeoData.SEOStateData>>();
            try
            {
                BusinessLogic.SEODataServices.SEOGeneralDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOGeneralDataRetriever(null);
                result.Value = retriever.GetAllStates();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOGetAllStates.");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.SeoData.SEOPopularCityData>> SEOGetPopularCities()
        {
            Result<List<DataModel.SeoData.SEOPopularCityData>> result = new Result<List<NoProblem.Core.DataModel.SeoData.SEOPopularCityData>>();
            try
            {
                BusinessLogic.SEODataServices.SEOGeneralDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOGeneralDataRetriever(null);
                result.Value = retriever.GetPopularCities();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOGetPopularCities.");
            }
            return result;
        }

        [WebMethod]
        public Result<List<string>> SEOGetPopularCitiesInState(string state)
        {
            Result<List<string>> result = new Result<List<string>>();
            try
            {
                BusinessLogic.SEODataServices.SEOGeneralDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOGeneralDataRetriever(null);
                result.Value = retriever.GetPopularCitiesInState(state);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SEOGetPopularCitiesInState. state = {0}", state);
            }
            return result;
        }

        [WebMethod]
        public Result<List<string>> SEOGetAllCitiesInState(string state)
        {
            Result<List<string>> result = new Result<List<string>>();
            try
            {
                BusinessLogic.SEODataServices.SEOGeneralDataRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOGeneralDataRetriever(null);
                result.Value = retriever.GetAllCitiesInState(state);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllCitiesInState. state = {0}", state);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Case.CustomerPageCaseData>> GetLastCases(int howMany)
        {
            Result<List<DataModel.Case.CustomerPageCaseData>> result = new Result<List<NoProblem.Core.DataModel.Case.CustomerPageCaseData>>();
            try
            {
                BusinessLogic.SEODataServices.SEOLastCasesRetriever retriever = new NoProblem.Core.BusinessLogic.SEODataServices.SEOLastCasesRetriever();
                result.Value = retriever.GetLastCases(howMany);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetLastCases");
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.ApprovePasswordResponse> ApprovePassword(DataModel.SupplierModel.ApprovePasswordRequest request)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.ApprovePasswordResponse> result =
                new Result<NoProblem.Core.DataModel.SupplierModel.ApprovePasswordResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ApprovePassword started. supplierId = {0}, password = {1}", request.SupplierId, request.Password);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.ApprovePassword(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ApprovePassword. supplierId = {0}, password = {1}", request.SupplierId, request.Password);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<UserLoginResponse> AdvertiserLogin(string phone, string password)
        {
            Result<UserLoginResponse> result = new Result<UserLoginResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AdvertiserLogin started. Phone = {0}, Password ={1}", phone, password);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.AdvertiserLogin(phone, password);
                LogUtils.MyHandle.WriteToLog(9, "_AdvertiserLogin ended. Result = {0}", result.Value.UserLoginStatus);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in AdvertiserLogin. Phone = {0}, Password ={1}", phone, password);
            }
            return result;
        }

        [WebMethod]
        public Result<bool> IsAdvertiserExists(string phone)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "IsAdvertiserExists started. Phone = {0}", phone);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.IsAdvertiserExists(phone);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in IsAdvertiserExists. Phone = {0}", phone);
            }
            return result;
        }

        [WebMethod]
        public Result SendPasswordReminder(string phone, bool useEmail)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SendPasswordReminder started. Phone = {0}, useEamil = {1}", phone, useEmail);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                manager.SendPasswordReminder(phone, useEmail);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SendPasswordReminder. Phone = {0}, useEamil = {1}", phone, useEmail);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<UserLoginResponse> UserLogin(string Email, string Password)
        {
            LogUtils.MyHandle.WriteToLog(7, "UserLogin started. Email: {0}", Email);
            Result<UserLoginResponse> result = new Result<UserLoginResponse>();
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                result.Value = supplierLogic.UserLogin(Email, Password);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "UserLogin Failed with Exception. Email: {0}", Email);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(ex.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<UserLoginResponse> UserLoginForAffiliates(string Email, string Password)
        {
            LogUtils.MyHandle.WriteToLog(7, "UserLoginForAffiliates started. Email: {0}", Email);
            Result<UserLoginResponse> result = new Result<UserLoginResponse>();
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                result.Value = supplierLogic.UserLoginForAffiliates(Email, Password);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "UserLoginForAffiliates Failed with Exception. Email: {0}", Email);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(ex.Message);
            }
            return result;
        }

        [WebMethod]
        public string ChangePassword(string SupplierId, string OldPassword, string NewPassword)
        {
            LogUtils.MyHandle.WriteToLog(1, "ChangePassword started. SupplierId: {0}", SupplierId);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.ChangePassword(SupplierId, OldPassword, NewPassword);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "ChangePassword Failed with Exception");
                retVal = "<ChangePassword><Error>Failed</Error></ChangePassword>";
            }
            return retVal;
        }

        [WebMethod]
        public string ChangePasswordByEmail(string email, string oldPassword, string newPassword)
        {
            LogUtils.MyHandle.WriteToLog(1, "ChangePasswordByEmail started. email= {0}", email);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.ChangePasswordByEmail(email, oldPassword, newPassword);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "ChangePasswordByEmail Failed with Exception. eamil= {0}", email);
                retVal = "<ChangePassword><Error>Failed</Error></ChangePassword>";
            }
            return retVal;
        }

        [WebMethod]
        public Result<ResetPasswordResponse> ResetPassword(ResetPasswordRequest request)
        {
            LogUtils.MyHandle.WriteToLog(1, "ResetPassword started. phoneNumber: {0}", request.PhoneNumber);
            Result<ResetPasswordResponse> result = new Result<ResetPasswordResponse>();
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                ResetPasswordResponse response = supplierLogic.ResetPassword(request);
                result.Value = response;
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "ResetPassword failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(ex.Message);
            }

            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.UpsertAdvertiserResponse> UpsertAdvertiser(DataModel.SupplierModel.UpsertAdvertiserRequest request)
        {
            Result<DataModel.SupplierModel.UpsertAdvertiserResponse> result = new Result<NoProblem.Core.DataModel.SupplierModel.UpsertAdvertiserResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(7, "UpsertAdvertiser started");
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.UpsertAdvertiser(request);
                if (request.AuditOn)
                {
                    NoProblem.Core.BusinessLogic.Audit.AuditManager auditManager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                    auditManager.CreateAudit(request.AuditEntries);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpsertAdvertiser failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        /// <summary>
        /// To be used from API. Saves the xml after validating it, it is later used to create a new advertiser.
        /// </summary>
        /// <param name="supplierXml">
        /// <Advertiser Method="Create or Update" Id="the advertiser's id if it is update">
        ///   <Name>Al3</Name>
        ///   <Phone>0543388911</Phone>
        ///   <Headings>
        ///     <Heading Price="5">1</Heading>
        ///   </Headings>
        ///   <Regions>
        ///     <Region Level="1">62</Region>
        ///   </Regions>
        ///   <Availability>
        ///     <Day FromHour="9" ToHour="18">Sunday</Day>
        ///     <Day FromHour="10" ToHour="19">Monday</Day>
        ///   </Availability>
        ///   <Balance>100</Balance> (Balance not accepted in update)
        /// </Advertiser>
        /// </param>
        /// <param name="callBackUrl">Url to POST after the request has been processed</param>
        /// <returns>ResultOfGuid where the guid is a unique id of the request</returns>
        [WebMethod]
        public Result<Guid> UpsertAdvertiserApi(string supplierXml, string callBackUrl, string customParam)
        {
            Result<Guid> result = null;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpsertAdvertiserApi started");
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result = manager.UpsertAdvertiserApi(supplierXml, callBackUrl, customParam);
            }
            catch (Exception exc)
            {
                if (result == null)
                {
                    result = new Result<Guid>();
                }
                LogUtils.MyHandle.HandleException(exc, "Exception in UpsertAdvertiserApi");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        [Obsolete("Please use method UpsertAdvertiser instead")]
        public string UpsertSupplier(UpsertSupplierRequest request)
        {
            /*
             * <InternetSupplier SiteId="" SupplierId="">
             *	<Email></Email>
             *  <FirstName></FirstName>
             *  <LastName></LastName>
             *  <ContactPhone></ContactPhone>
             *  <Company></Company>
             *  <CompanyPhone></CompanyPhone>
             *  <Fax></Fax>
             *  <Country></Country>
             *  <StateName></StateName>
             *  <CityName></CityName>
             *  <StreetName></StreetName>
             *  <HouseNum></HouseNum>
             *  <Zipcode></Zipcode>
             *  <CompanyId></CompanyId>
             *  <CompanySite></CompanySite>
             * </InternetSupplier> 
             */

            LogUtils.MyHandle.WriteToLog(7, "UpsertSupplier started. Request: {0}", request.Request);
            string retVal = null;

            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.UpsertSupplier(request.Request);
                if (request.AuditOn)
                {
                    NoProblem.Core.BusinessLogic.Audit.AuditManager auditManager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                    auditManager.CreateAudit(request.AuditEntries);
                }
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "UpsertSupplier Failed with exception");
                retVal = "<InternetSupplier><Error>Failed</Error></InternetSupplier>";
            }

            return retVal;
        }

        [WebMethod]
        //[Obsolete("Use Reports.MyCallsReport istead")]
        public string GetSuppliersIncidents(string publisherId, string SupplierId, DateTime DateFrom, DateTime DateTo, string IncidentNumber, IncidentWinStatus Status)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetSuppliersIncidents - publisherId: {0},SupplierId: {1}, DateFrom: {2}, DateTo: {3}, IncidentNumber: {4}, Status: {5}", publisherId, SupplierId, DateFrom, DateTo, IncidentNumber, Status);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetSuppliersIncidents(publisherId, SupplierId, DateFrom, DateTo, IncidentNumber, Status);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSuppliersIncidents Failed with exception");
                retVal = "<Incidents><Error>" + ex.Message + "</Error></Incidents>";
            }

            return retVal;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.SupplierIncidentData> GetSupplierIncidentData(Guid callId)
        {
            Result<DataModel.SupplierModel.SupplierIncidentData> result = new Result<NoProblem.Core.DataModel.SupplierModel.SupplierIncidentData>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSupplierIncidentData. callId = {0}", callId);
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.GetSupplierIncidentData(callId);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "GetSupplierIncidentData failed with exception. callId = {0}", callId);
            }
            return result;
        }

        [WebMethod]
        public Result UpdateJobDone(DataModel.SupplierModel.UpdateJobDoneRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateJobDone started, request: callId = {0}, isJobDone = {1}", request.CallId.ToString(), (request.IsJobDone.HasValue ? request.IsJobDone.Value.ToString() : "Null"));
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                manager.UpdateJobDone(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "UpdateJobDone failed with exception, request: callId = {0}, isJobDone = {1}", request.CallId.ToString(), (request.IsJobDone.HasValue ? request.IsJobDone.Value.ToString() : "Null"));
            }
            return result;
        }

        [WebMethod]
        public string CreateSupplierExpertise(CreateSupplierExpertiseRequest request)
        {
            /* Request Formation
             * <SupplierExpertise SiteId="" SupplierId="">
             *	<PrimaryExpertise ID="" Certificate="">
             *	   <SecondaryExpertise>ID</SecondaryExpertise>
             *	   <SecondaryExpertise>ID</SecondaryExpertise>
             *  </PrimaryExpertise>
             *	<PrimaryExpertise ID="">
             *	   <SecondaryExpertise>ID</SecondaryExpertise>
             *  </PrimaryExpertise>
             * </SupplierExpertise>
             */

            string retVal = null;

            LogUtils.MyHandle.WriteToLog(2, "CreateSupplierExpertise started. Request: {0}", request.Request);

            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.CreateSupplierExpertise(request.Request, request.IsFromAAR, request.UserId);
                if (request.AuditOn)
                {
                    BusinessLogic.Audit.AuditManager auditManager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                    auditManager.CreateAudit(request.AuditEntries);
                }
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "CreateSupplierExpertise Failed with Exception");
                retVal = "<SupplierExpertise><Error>Failed</Error></SupplierExpertise>";
            }

            return retVal;
        }

        [WebMethod]
        public string SetAvailability(SetAvailabilityRequest request)
        {
            /*
             * <SupplierAvailability SiteId="" SupplierId="" >
             *    <Availability>
             *	      <Day Name="sunday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *	      </Day>
             *	      <Day Name="monday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *	      </Day>
             *       <Day Name="tuesday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *       </Day>
             *       <Day Name="wednesday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *       </Day>
             *       <Day Name="thursday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *       </Day>
             *       <Day Name="friday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *       </Day>
             *       <Day Name="saturday">
             *		      <FromTime>14:00</FromTime>
             *    		<TillTime>17:00</TillTime>
             *       </Day>
             *    </Availability>
             *    <Unavailability>
             *       <StartDate>05/26/2010</StartDate>
             *       <EndDate></EndDate>
             *       <ReasonId></ReasonId>
             *    </Unavailability>
             * </SupplierAvailability>
             */
            LogUtils.MyHandle.WriteToLog(2, "SetAvailability started. Request: {0}", request.Request);
            string retVal = null;

            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.SetSupplierAvailability(request.Request, request.IsFromAAR);
                if (request.IsFromNewAdvertiserPortal)
                {
                    BusinessLogic.AdvertiserDashboards.GetStartedTasksManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTasksManager(null);
                    manager.CrossOutGetStartedTask(request.SupplierId, NoProblem.Core.DataModel.AdvertiserDashboards.GetStartedTaskType.work_hours);
                }
                if (request.AuditOn)
                {
                    BusinessLogic.Audit.AuditManager auditManager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                    auditManager.CreateAudit(request.AuditEntries);
                }
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SetAvailability Failed with exception");
                retVal = "<SupplierAvailability><Error>Failed</Error></SupplierAvailability>";
            }

            return retVal;
        }

        [WebMethod]
        public string GetSupplierAvailability(string SiteId, string SupplierId)
        {
            /*
             * <SupplierAvailability SupplierId="">
             *	<Day Name="sunday">
             *		<FromTime>14:00</FromTime>
             *		<TillTime>17:00</TillTime>
             *	</Day>
             *	<Day Name="monday">
             *		<FromTime>14:00</FromTime>
             *		<TillTime>17:00</TillTime>
             *	</Day>
             *  <Day Name="tuesday">
             *		<FromTime>14:00</FromTime>
             *		<TillTime>17:00</TillTime>
             *  </Day>
             *  <Day Name="wednesday">
             *		<FromTime>14:00</FromTime>
             *		<TillTime>17:00</TillTime>
             *  </Day>
             *  <Day Name="thursday">
             *		<FromTime>14:00</FromTime>
             *		<TillTime>17:00</TillTime>
             *  </Day>
             *  <Day Name="friday">
             *		<FromTime>14:00</FromTime>
             *		<TillTime>17:00</TillTime>
             *  </Day>
             *  <Day Name="saturday">
             *		<FromTime>14:00</FromTime>
             *		<TillTime>17:00</TillTime>
             *  </Day>
             * </SupplierAvailability>
             */

            LogUtils.MyHandle.WriteToLog(8, "GetSupplierAvailability started. SiteId: {0}, SupplierId: {1}", SiteId, SupplierId);
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                return supplierLogic.GetSupplierAvailability(SupplierId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSupplierAvailability Failed.");
                return "<SupplierAvailability><Error>Failed</Error></SupplierAvailability>";
            }
        }

        [WebMethod]
        public string GetPricingMethods(string SiteId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetPricingMethods started. SiteId: {0}", SiteId);
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                return supplierLogic.GetPricingMethods();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetPricingMethods failed with exception");
                return "<PricingMethods><Error>Failed</Error></PricingMethods>";
            }
        }

        [WebMethod]
        public string GetPricingPackages(string SiteId, string PricingMethodId)
        {
            /*
               <SupplierPackages>
                  <SupplierPackage>
                     <Name>Silver</Name>
                     <PaidAmount>200</PaidAmount>
                     <FreeAmount>30</FreeAmount>
                  </SupplierPackage>
                  <SupplierPackage>
                     <Name>Gold</Name>
                     <PaidAmount>500</PaidAmount>
                     <FreeAmount>200</FreeAmount>
                  </SupplierPackage>
               </SupplierPackages>
             */
            LogUtils.MyHandle.WriteToLog(8, "GetPricingPackages started. SiteId: {0}, PricingMethodId: {1}", SiteId, PricingMethodId);
            try
            {
                BusinessLogic.PublisherPortal.PricingPackagesManager logic = new NoProblem.Core.BusinessLogic.PublisherPortal.PricingPackagesManager(null);
                return logic.GetActivePricingPackages(PricingMethodId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetPricingPackages Failed.");
                return "<SupplierPackages><Error>Failed</Error></SupplierPackages>";
            }
        }

        [WebMethod]
        public Result<CreateSupplierPricingResponse> CreateSupplierPricing(CreateSupplierPricingRequest request)
        {
            Result<CreateSupplierPricingResponse> result = new Result<CreateSupplierPricingResponse>();
            LogUtils.MyHandle.WriteToLog(8, "CreateSupplierPricing started. SupplierId = {0}, PaymentMethod = {1}, Bonus = {2}, ChargeCompany = {3}, Amount = {4}, RechargeAmount = {5}, transactionId = {6}, voucher = {7}, UpdateRechargeFields = {8}, RechargeTypeCode = {9}",
                request.SupplierId.ToString(), request.PaymentMethod, request.BonusAmount, request.ChargingCompany, request.PaymentAmount, request.RechargeAmount, request.TransactionId, request.VoucherNumber, request.UpdateRechargeFields, request.RechargeTypeCode);
            try
            {
                SupplierPricingManager manager = new SupplierPricingManager(null);
                CreateSupplierPricingResponse response = manager.CreateSupplierPricing(request);
                result.Value = response;
                if (request.AuditOn && result.Value.DepositStatus == eDepositStatus.OK)
                {
                    BusinessLogic.Audit.AuditManager auditManager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                    auditManager.CreateAudit(request.AuditEntries);
                }
                LogUtils.MyHandle.WriteToLog(8, "CreateSupplierPricing returns: DesositStatus = {0}, SuppPricingId = {1}", response.DepositStatus, response.PricingId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "CreateSupplierPricing failed with exception. SupplierId = {0}", request.SupplierId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(ex.Message);
                result.Messages.Add(ex.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result UpdateExemptionOfMontlyPayment(UpdateExemptionOfMonthlyPaymentRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierPricing.SupplierPricingManager manager = new SupplierPricingManager(null);
                manager.UpdateExemptionOfMonthlyPayment(request);
                if (request.AuditOn)
                {
                    BusinessLogic.Audit.AuditManager auditManager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                    auditManager.CreateAudit(request.AuditEntries);
                }
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateExemptionOfMontlyPaymentRequest");
            }
            return result;
        }

        [WebMethod]
        public Result<List<string>> GetActivePaymentMethods()
        {
            Result<List<string>> result = new Result<List<string>>();
            try
            {
                BusinessLogic.SupplierPricing.SupplierPricingManager manager = new SupplierPricingManager(null);
                result.Value = manager.GetActivePaymentMethods();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<GetSupplierChargingDataResponse> GetSupplierChargingData(GetSupplierChargingDataRequest request)
        {
            Result<GetSupplierChargingDataResponse> result = new Result<GetSupplierChargingDataResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSupplierCharginData started. supplierId = {0}", request.SupplierId.ToString());
                SupplierPricingManager manager = new SupplierPricingManager(null);
                GetSupplierChargingDataResponse response = manager.GetSupplierChargingData(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetSupplierCharginData failed with exception. supplierId = {0}", request.SupplierId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<ChargingDataForHeadingPaymentPage> GetSupplierChargingDataForHeadingPaymentPage(Guid supplierId)
        {
            Result<ChargingDataForHeadingPaymentPage> result = new Result<ChargingDataForHeadingPaymentPage>();
            try
            {
                SupplierPricingManager manager = new SupplierPricingManager(null);
                result.Value = manager.GetSupplierChargingDataForHeadingPaymentPage(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "GetSupplierChargingDataForHeadingPaymentPage failed with exception. supplierId = {0}", supplierId);
            }
            return result;
        }

        [WebMethod]
        public Result<GetSupplierPricingHistoryResponse> GetSupplierPricingHistory(GetSupplierPricingHistoryRequest request)
        {
            Result<GetSupplierPricingHistoryResponse> result = new Result<GetSupplierPricingHistoryResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSupplierPricingHistory started. supplierId = {0}", request.SupplierId);
                SupplierPricingManager manager = new SupplierPricingManager(null);
                result.Value = manager.GetSupplierPricingHistory(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetSupplierPricingHistory failed with exception. supplierId = {0}", request.SupplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<GetPricingDataResponse> GetPricingData(Guid supplierPricingId)
        {
            Result<GetPricingDataResponse> result = new Result<GetPricingDataResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetPricingData started. supplierPricingId = {0}", supplierPricingId);
                SupplierPricingManager manager = new SupplierPricingManager(null);
                result.Value = manager.GetPricingData(supplierPricingId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetPricingData failed with exception. supplierPricingId = {0}", supplierPricingId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result UpdateAutoRechargeOptions(UpdateAutoRechargeOptionsRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateAutoRechargeOptions started. supplierId = {0}, rechargeAmount = {1}, isAutoRecharge = {2}", request.SupplierId, request.RechargeAmount, request.IsAutoRecharge);
                SupplierPricingManager manager = new SupplierPricingManager(null);
                manager.UpdateAutoRechargeOptions(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpdateAutoRechargeOptions failed with exception. supplierId = {0}, rechargeAmount = {1}, isAutoRecharge = {2}", request.SupplierId, request.RechargeAmount, request.IsAutoRecharge);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result UpdateInvoiceSendOptions(UpdateInvoiceSendOptionsRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateInvoiceSendOptions started. supplierId = {0}, invoiceSendAddress = {1}, invoiceSendOption = {2}", request.SupplierId, request.InvoiceSendAddress, request.InvoiceSendOption);
                SupplierPricingManager manager = new SupplierPricingManager(null);
                manager.UpdateInvoiceSendOptions(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpdateInvoiceSendOptions failed with exception. supplierId = {0}, invoiceSendAddress = {1}, invoiceSendOption = {2}", request.SupplierId, request.InvoiceSendAddress, request.InvoiceSendOption);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result CreateTransactionLog(CreateTransactionLogRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CreateTransactionLog started.  request: supplierId = {0}, supplierPricingId = {1}. ", request.SupplierId.ToString(), request.PricingId.ToString());
                SupplierPricingManager manager = new SupplierPricingManager(null);
                manager.CreateTransactionLog(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "CreateTransactionLog failed with exception. request: supplierId = {0}, supplierPricingId = {1}. ", request.SupplierId.ToString(), request.PricingId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result UpdateInvoicePdfLink(UpdateInvoicePdfLinkRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateInvoicePdfLink started. request: supplierPricingId = {0}, pdfLink = {1}.", request.PricingId.ToString(), request.InvoicePdfLink);
                SupplierPricingManager manager = new SupplierPricingManager(null);
                manager.UpdateInvoiceData(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpdateInvoicePdfLink failed with exception. request: supplierPricingId = {0}, pdfLink = {1}.", request.PricingId.ToString(), request.InvoicePdfLink);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public string GetAccountExpertiseIncidentAmount(string SiteId, string AccountId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetAccountExpertiseIncidentAmount started. SiteId: {0}, AccountId: {1}", SiteId, AccountId);
            string retVal = null;

            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetAccountExpertiseIncidentAmount(AccountId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetAccountExpertiseIncidentAmount Failed");
                retVal = "<IncidentAmount><Error>Failed</Error></IncidentAmount>";
            }

            return retVal;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.AdvertiserGeneralInfo> GetSupplierGeneralInfo(Guid supplierId)
        {
            Result<DataModel.SupplierModel.AdvertiserGeneralInfo> result = new Result<NoProblem.Core.DataModel.SupplierModel.AdvertiserGeneralInfo>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSupplierGeneralInfo started. supplierId = {0}", supplierId.ToString());
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.GetSupplierGeneralInfo(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetSupplierGeneralInfo failed with exception. supplierId = {0}", supplierId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public string GetSupplierDetails(string SiteId, string SupplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetSupplierDetails started. SiteId: {0}, SupplierId: {1}", SiteId, SupplierId);
            string retVal = "";

            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetSupplierDetails(SupplierId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSupplierDetails Failed");
                retVal = "<Supplier><Error>Failed</Error></Supplier>";
            }

            return retVal;
        }

        [WebMethod]
        public string GetUnavailabilityReasons()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetUnavailabilityReasons started.");
            string retVal = "";
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                retVal = publisherUtils.GetUnavailabilityReasons();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetUnavailabilityReasons Failed with Exception");
                retVal = "<UnavailabilityResons><Error>Failed</Error></UnavailabilityResons>";
            }
            return retVal;
        }

        [WebMethod]
        public string DeleteUnavailabilityReasons(string Request)
        {
            LogUtils.MyHandle.WriteToLog(2, "DeleteUnavailabilityReasons started. Request: {0}", Request);
            string retVal = "";
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                retVal = publisherUtils.DeleteUnavailabilityReasons(Request);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "DeleteUnavailabilityReasons Failed with Exception");
                retVal = "<UnavailabilityResons><Error>Failed</Error></UnavailabilityResons>";
            }
            return retVal;
        }

        [WebMethod]
        public string GetSupplierExpertise(string SiteId, string SupplierId)
        {
            /*
             <SupplierExpertise>
               <PrimaryExpertise Name="" ID="" Certificate="">
                     <SecondaryExpertise ID="">Name</SecondaryExpertise>
               </PrimaryExpertise>
               <PrimaryExpertise Name="" ID="" Certificate="">
                     <SecondaryExpertise ID="">Name</SecondaryExpertise>
                     <SecondaryExpertise ID="">Name</SecondaryExpertise>
               </PrimaryExpertise>
             </SupplierExpertise>
             */

            LogUtils.MyHandle.WriteToLog(8, "GetSupplierExpertise started. SiteId: {0}, SupplierId: {1}", SiteId, SupplierId);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetSupplierExpertise(SiteId, SupplierId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSupplierExpertise Failed");
                retVal = "<SupplierExpertise><Error>Failed</Error></SupplierExpertise>";
            }
            return retVal;
        }

        [WebMethod]
        public string GetSupplierComments(string SupplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetSupplierComments started. SupplierId: {0}", SupplierId);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetSupplierComments(SupplierId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSupplierComments Failed with exception");
                retVal = "<SupplierComments><Error>Failed</Error></SupplierComments>";
            }

            return retVal;
        }

        [WebMethod]
        public string UpsertSupplierComment(string Request)
        {
            /*
             * <SupplierComment ID="">
             * 
             *    <SupplierId></SupplierId>
             *    <Text>Some Text</Text>
             * </SupplierComment>
             */

            LogUtils.MyHandle.WriteToLog(8, "UpsertSupplierComment started. Request: {0}", Request);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.UpsertSupplierComment(Request);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "UpsertSupplierComment Failed with exception");
                retVal = "<SupplierComment><Error>Failed</Error></SupplierComment>";
            }

            return retVal;
        }

        [WebMethod]
        public string GetWorkArea(string SiteId, string SupplierId)
        {
            /*
     <WorkArea SiteId="" SupplierId="">
         <Details>
            <Radius></Radius>
            <Latitude></Latitude>
            <Longitude></Longitude>
         </Details>
         <Countries>
            <Country Name="">
               <City>Haifa</City>
               <City>Tel-Aviv</City>
            </Country>
         </Countries>
     </WorkArea>
    */

            LogUtils.MyHandle.WriteToLog(8, "GetWorkArea started. SiteId: {0}, SupplierId: {1}", SiteId, SupplierId);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetWorkArea(SupplierId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetWorkArea Failed with exception");
                retVal = "<WorkArea><Error>Failed</Error></WorkArea>";
            }
            LogUtils.MyHandle.WriteToLog(8, "GetWorkAreas returned = {0}", retVal);

            return retVal;
        }

        [WebMethod]
        public Result<List<DataModel.SqlHelper.RegionMinData>> GetMoreMapData(decimal longitude, decimal latitude)
        {
            Result<List<DataModel.SqlHelper.RegionMinData>> result = new Result<List<NoProblem.Core.DataModel.SqlHelper.RegionMinData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetMoreMapData started. logitude = {0}, latitude = {1}", longitude, latitude);
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                result.Value = supplierLogic.GetMoreMapData(longitude, latitude);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetMoreMapData. longitude = {0}, latitude = {1}", longitude, latitude);
            }
            return result;
        }

        [WebMethod]
        public string GetRegions(int RegionLevel)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetRegions started. RegionLevel: {0}", RegionLevel);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetRegions(RegionLevel);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetCities Failed with exception");
                retVal = "<Areas><Error>Failed</Error></Areas>";
            }

            return retVal;
        }

        [WebMethod]
        public string SetSupplierWorkAreas(string SupplierId, string XmlAreas, string fullAddress, string name, string email, string userIdentity)
        {
            LogUtils.MyHandle.WriteToLog(8, "SetSupplierWorkAreas started. SupplierId: {0}, XmlAreas: {1}", SupplierId, XmlAreas);

            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                if (!supplierLogic.SetSupplierWorkAreas(new Guid(SupplierId), XmlAreas, fullAddress, name, email, true, userIdentity, false))
                {
                    retVal = "<SetSupplierWorkAreas><Status>EmailDuplicate</Status></SetSupplierWorkAreas>";
                }
                else
                {
                    retVal = "<SetSupplierWorkAreas><Status>Success</Status></SetSupplierWorkAreas>";
                }
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SetSupplierWorkAreas Failed with exception");
                retVal = "<SetSupplierWorkAreas><Error>Failed</Error></SetSupplierWorkAreas>";
            }
            return retVal;
        }

        [WebMethod]
        public string GetCitiesByCoordinate(Guid supplierId, decimal radius, decimal longitude, decimal latitude)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetCitiesByCoordinate started. supplierId: {0}, radius: {1}, longitude: {2}, latitude: {3}", supplierId, radius, longitude, latitude);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetCitiesByCoordinate(supplierId, radius, longitude, latitude);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetCitiesByCoordinate Failed with exception");
                retVal = "<GetCitiesByCoordinate><Error>Failed</Error></GetCitiesByCoordinate>";
            }
            LogUtils.MyHandle.WriteToLog(8, "GetCititesByCoordinate returned = {0}", retVal);
            return retVal;
        }

        [WebMethod]
        public string SetCitiesByCoordinate(Guid supplierId, decimal radius, decimal longitude, decimal latitude, string fullAddress, string name, string email, Guid regionId, bool allRegions, bool updateNameAndEmail, string userIdentity, bool isFromAdvRegistration)
        {
            LogUtils.MyHandle.WriteToLog(8, "SetCitiesByCoordinate started. supplierId = {0}, radius = {1}, longitude = {2}, latitude = {3}, fullAddress = {4}, name = {5}, email = {6}, regionId = {7}, allRegions = {8}", supplierId, radius, longitude, latitude, fullAddress, name, email, regionId, allRegions);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                bool invitationApproved = false;
                if (!supplierLogic.SetCitiesByCoordinate(supplierId, radius, longitude, latitude, fullAddress, name, email, regionId, allRegions, updateNameAndEmail, userIdentity, isFromAdvRegistration, ref invitationApproved))
                {
                    retVal = "<SetCitiesByCoordinate><Status>EmailDuplicate</Status></SetCitiesByCoordinate>";
                }
                else
                {
                    if (invitationApproved)
                    {
                        retVal = "<SetCitiesByCoordinate><Status>Success</Status><invitationApproved>True</invitationApproved></SetCitiesByCoordinate>";
                    }
                    else
                    {
                        retVal = "<SetCitiesByCoordinate><Status>Success</Status><invitationApproved>False</invitationApproved></SetCitiesByCoordinate>";
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "Exception in SetCitiesByCoordinate. supplierId = {0}, radius = {1}, longitude = {2}, latitude = {3}, fullAddress = {4}, name = {5}, email = {6}, regionId = {7}, allRegions = {8}", supplierId, radius, longitude, latitude, fullAddress, name, email, regionId, allRegions);
                retVal = "<SetCitiesByCoordinate><Error>Failed</Error></SetCitiesByCoordinate>";
            }

            return retVal;
        }

        [WebMethod]
        public string HandleAdvertiserRegions(string SiteId, int RegionLevel, string AdvertiserId, string Prefix, string SelectedRegions)
        {
            LogUtils.MyHandle.WriteToLog(8, "HandleAdvertiserRegions started. SiteId: {0},RegionLevel: {1}, SupplierId: {2}, Prefix: {3}, SelectedRegions: {4}", SiteId, RegionLevel, AdvertiserId, Prefix, SelectedRegions);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.HandleAdvertiserRegions(RegionLevel, new Guid(AdvertiserId), Prefix, SelectedRegions);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "HandleAdvertiserRegions Failed with exception");
                retVal = "<Regions Status='Failed' />";
            }

            LogUtils.MyHandle.WriteToLog(8, "HandleAdvertiserRegions ended with retval = {0}", retVal);
            return retVal;
        }

        [WebMethod]
        public string GetAllTimeZones()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetAllTimeZones started.");
            string retVal = "";

            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetAllTimeZones();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSumIncomingFromSupplierReport Failed with exception");
                retVal = "<GetSumIncomingFromSupplierReport><Error>Failed</Error></GetSumIncomingFromSupplierReport>";
            }

            return retVal;
        }

        [WebMethod]
        public Result<GetSuppliersDirectNumbersResponse> GetSuppliersDirectNumbers(GetSuppliersDirectNumbersRequest request)
        {
            Result<GetSuppliersDirectNumbersResponse> result = new Result<GetSuppliersDirectNumbersResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSuppliersDirectNumbers started. Request: SupplierId = {0}", request.SupplierId.ToString());
                PublisherPortalManager manager = new PublisherPortalManager(null, null);
                GetSuppliersDirectNumbersResponse response = manager.GetSuppliersDirectNumbers(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetSuppliersDirectNumbers failed with exception. Request: SupplierId = {0}", request.SupplierId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result UpdateDailyBudget(Guid supplierId, int? budget)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateDailyBudget started.");
                BusinessLogic.DailyBudgetManager manager = new NoProblem.Core.BusinessLogic.DailyBudgetManager(null);
                manager.UpdateDailyBudget(supplierId, budget);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateDailyBudget. SupplierId: {0}, DailyBudget: {1}.", supplierId, budget);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<int?> GetDailyBudget(Guid supplierId)
        {
            Result<int?> result = new Result<int?>();
            try
            {
                BusinessLogic.DailyBudgetManager manager = new NoProblem.Core.BusinessLogic.DailyBudgetManager(null);
                result.Value = manager.GetDailybudget(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetDailyBudget. SupplierId: {0}.", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result TurnOnRecordings(Guid supplierId)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                manager.TurnOnRecordings(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in TurnOnRecordings. SupplierId: {0}", supplierId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result TurnOffRecordings(Guid supplierId)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                manager.TurnOffRecordings(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in TurnOffRecordings. SupplierId: {0}", supplierId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result MakeSupplierAvailable(Guid supplierId)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "MakeSupplierAvailable started.");
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                result.Value = manager.MakeAvailable(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in MakeSupplierAvailable. SupplierId: {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.GetMyLeadsResponse> GetMyLeadsList(DataModel.AdvertiserDashboards.GetMyLeadsListRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetMyLeadsList started. SupplierId = {0}, Period = {1}, SortBy = {2}, SortDescending = {3}", request.SupplierId, request.Period, request.SortBy, request.SortDescending);
            Result<DataModel.AdvertiserDashboards.GetMyLeadsResponse> result = new Result<DataModel.AdvertiserDashboards.GetMyLeadsResponse>();
            try
            {
                BusinessLogic.AdvertiserDashboards.MyLeadsListCreator creator = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.MyLeadsListCreator(null);
                result.Value = creator.GetMyLeadsList(request.SupplierId, request.Period, request.SortBy, request.SortDescending);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetMyLeadsList. SupplierId = {0}, Period = {1}, SortBy = {2}, SortDescending = {3}", request.SupplierId, request.Period, request.SortBy, request.SortDescending);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<int> GetAnsweringRate(Guid supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetAnsweringRate started. SupplierId = {0}", supplierId);
            Result<int> result = new Result<int>();
            try
            {
                BusinessLogic.AdvertiserDashboards.AnsweringRateCalculator calculator = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.AnsweringRateCalculator();
                result.Value = calculator.GetAnsweringRate(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAnsweringRate. SupplierId = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.PhoneAvailabilityData> GetPhoneAvailabilityData(Guid supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetPhoneAvailabilityData started. SupplierId = {0}", supplierId);
            Result<DataModel.AdvertiserDashboards.PhoneAvailabilityData> result = new Result<NoProblem.Core.DataModel.AdvertiserDashboards.PhoneAvailabilityData>();
            try
            {
                BusinessLogic.AdvertiserDashboards.PhoneAvailabilityCalculator calculator = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.PhoneAvailabilityCalculator(null);
                result.Value = calculator.GetPhoneAvailabilityData(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetPhoneAvailabilityData. SupplierId = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.GetStartedTasksStatusResponse> RetrieveGetStartedTasksStatus(Guid supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "RetrieveGetStartedTasksStatus started. supplierId = {0}", supplierId);
            Result<DataModel.AdvertiserDashboards.GetStartedTasksStatusResponse> result = new Result<DataModel.AdvertiserDashboards.GetStartedTasksStatusResponse>();
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTasksManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTasksManager(null);
                result.Value = manager.RetrieveGetStartedTasksStatus(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RetrieveGetStartedTasksStatus. supplierId = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.AdvertiserDashboards.CrossOutGetStartedTaskResponse> CrossOutGetStartedTask(Guid supplierId, NoProblem.Core.DataModel.AdvertiserDashboards.GetStartedTaskType taskType)
        {
            LogUtils.MyHandle.WriteToLog(8, "CrossOutGetStartedTask started. supplierId = {0}, taskType = {1}", supplierId, taskType);
            Result<NoProblem.Core.DataModel.AdvertiserDashboards.CrossOutGetStartedTaskResponse> result = new Result<NoProblem.Core.DataModel.AdvertiserDashboards.CrossOutGetStartedTaskResponse>();
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTasksManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTasksManager(null);
                result.Value = manager.CrossOutGetStartedTask(supplierId, taskType);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CrossOutGetStartedTask. supplierId = {0}, taskType = {1}", supplierId, taskType);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.Invitations.CreateInvitationResponse> InviteYourFriend(NoProblem.Core.DataModel.AdvertiserDashboards.Invitations.CreateInvitationRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, "InviteYourFriend started. supplierId = {0}, friendEmail = {1}, personalMessage = {2}", request.SupplierId, request.InvitedEmail, request.PersonalMessage);
            Result<DataModel.AdvertiserDashboards.Invitations.CreateInvitationResponse> result = new Result<NoProblem.Core.DataModel.AdvertiserDashboards.Invitations.CreateInvitationResponse>();
            try
            {
                BusinessLogic.Invitations.InvitationsManager manager = new NoProblem.Core.BusinessLogic.Invitations.InvitationsManager(null);
                result.Value = manager.CreateInvitation(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in InviteYourFriend. supplierId = {0}, friendEmail = {1}, personalMessage = {2}", request.SupplierId, request.InvitedEmail, request.PersonalMessage);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.BillingOverview.GetRechargeDataResponse> GetRechargeData(Guid supplierId)
        {
            Result<DataModel.AdvertiserDashboards.BillingOverview.GetRechargeDataResponse> result = new Result<NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview.GetRechargeDataResponse>();
            try
            {
                BusinessLogic.AdvertiserDashboards.BillingOverviewManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.BillingOverviewManager(null);
                result.Value = manager.GetRechargeData(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetRechargeData");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.AdvertiserDashboards.BillingOverview.TransactionRow>> GetTransactions(Guid supplierId, bool lastSix)
        {
            Result<List<DataModel.AdvertiserDashboards.BillingOverview.TransactionRow>> result = new Result<List<NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview.TransactionRow>>();
            try
            {
                BusinessLogic.AdvertiserDashboards.BillingOverviewManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.BillingOverviewManager(null);
                result.Value = manager.GetTransactions(supplierId, lastSix);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetLastTransactions. supplierId = {0}", supplierId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.AdvertiserDashboards.BillingOverview.TransactionRow2014>> GetTransactions_Registration2014(Guid supplierId)
        {
            Result<List<DataModel.AdvertiserDashboards.BillingOverview.TransactionRow2014>> result = new Result<List<NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview.TransactionRow2014>>();
            try
            {
                BusinessLogic.AdvertiserDashboards.BillingOverviewManager2014 manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.BillingOverviewManager2014(null);
                result.Value = manager.GetTransactions(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetTransactions_Registration2014. supplierId = {0}", supplierId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.BillingOverview.GetAccountActivityResponse> GetAccountActivity(Guid supplierId)
        {
            Result<DataModel.AdvertiserDashboards.BillingOverview.GetAccountActivityResponse> result = new Result<NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview.GetAccountActivityResponse>();
            try
            {
                BusinessLogic.AdvertiserDashboards.BillingOverviewManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.BillingOverviewManager(null);
                result.Value = manager.GetAccountActivity(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAccountActivity. supplierId = {0}", supplierId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.BillingOverview.GetAccountActivityResponse2014> GetAccountActivity_Registration2014(Guid supplierId)
        {
            Result<DataModel.AdvertiserDashboards.BillingOverview.GetAccountActivityResponse2014> result = new Result<NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview.GetAccountActivityResponse2014>();
            try
            {
                BusinessLogic.AdvertiserDashboards.BillingOverviewManager2014 manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.BillingOverviewManager2014(null);
                result.Value = manager.GetAccountActivity(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAccountActivity_Registration2014. supplierId = {0}", supplierId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result RequestMoreInvites(Guid supplierId)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.Invitations.InvitationsManager manager = new NoProblem.Core.BusinessLogic.Invitations.InvitationsManager(null);
                manager.RequestMoreInvites(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RequestMoreInvites");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.Invitations.InvitationData> GetInvitationsData(Guid supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetInvitationsData started. supplierId = {0}", supplierId);
            Result<DataModel.AdvertiserDashboards.Invitations.InvitationData> result = new Result<DataModel.AdvertiserDashboards.Invitations.InvitationData>();
            try
            {
                BusinessLogic.Invitations.InvitationsManager manager = new NoProblem.Core.BusinessLogic.Invitations.InvitationsManager(null);
                result.Value = manager.GetInvitationsData(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetInvitationsData. supplierId = {0}", supplierId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<bool> GetPasswordByPhone(Guid supplierId)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.SupplierBL.PasswordCallManager manager = new NoProblem.Core.BusinessLogic.SupplierBL.PasswordCallManager(null);
                result.Value = manager.GetPasswordByPhone(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetPasswordByPhone. supplierId = {0}", supplierId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<bool> VerifyEmail(string verification, string verify)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.Invitations.InvitationsManager manager = new NoProblem.Core.BusinessLogic.Invitations.InvitationsManager(null);
                result.Value = manager.VerifyEmail(verification, verify);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in VerifyEmail");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.CompletenessData> GetCompletenessData(Guid supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetCompletenessData started. supplierId = {0}", supplierId);
            Result<DataModel.AdvertiserDashboards.CompletenessData> result = new Result<DataModel.AdvertiserDashboards.CompletenessData>();
            try
            {
                BusinessLogic.AdvertiserDashboards.AccountCompletenessManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.AccountCompletenessManager();
                result.Value = manager.GetCompletenessData(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCompletenessData. supplierId = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result HonorCodePlusOne(Guid supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "HonorCodePlusOne started. supplierId = {0}", supplierId);
            Result result = new Result();
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTasksManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTasksManager(null);
                manager.HonorCodePlusOne(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in HonorCodePlusOne. supplierId = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.HonorCodeReadingStatus> GetHonorCodeReadingStatus(Guid supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetHonorCodeReadingStatus started. supplierId = {0}", supplierId);
            Result<DataModel.AdvertiserDashboards.HonorCodeReadingStatus> result = new Result<DataModel.AdvertiserDashboards.HonorCodeReadingStatus>();
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTasksManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTasksManager(null);
                result.Value = manager.GetHonorCodeReadingStatus(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetHonorCodeReadingStatus. supplierId = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<Guid> StartGetStartedTestCall(Guid supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "StartGetStartedCallTest started. supplierId = {0}", supplierId);
            Result<Guid> result = new Result<Guid>();
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager(null);
                result.Value = manager.StartTestCall(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in StartGetStartedCallTest. supplierId = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.AdvertiserDashboards.LiveTestCallStatus> GetLiveTestCallStatus(Guid testCallId)
        {
            LogUtils.MyHandle.WriteToLog(10, "GetLiveTestCallStatus started. id = {0}", testCallId);
            Result<DataModel.AdvertiserDashboards.LiveTestCallStatus> result = new Result<NoProblem.Core.DataModel.AdvertiserDashboards.LiveTestCallStatus>();
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager(null);
                result.Value = manager.GetLiveTestCallStatus(testCallId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetLiveTestCallStatus. id = {0}", testCallId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result TestCallClosedInReport(Guid supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "TestCallClosedInReport started. supplierid = {0}", supplierId);
            Result result = new Result();
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager(null);
                manager.TestCallClosedInReport(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in TestCallClosedInReport. supplierid = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.AdvertiserDashboards.AdvDashboardLeadData> GetTestCallForReport(Guid supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetTestCallForReport started. supplierid = {0}", supplierId);
            Result<NoProblem.Core.DataModel.AdvertiserDashboards.AdvDashboardLeadData> result = new Result<NoProblem.Core.DataModel.AdvertiserDashboards.AdvDashboardLeadData>();
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager(null);
                result.Value = manager.GetTestCallForReport(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetTestCallForReport. supplierid = {0}", supplierId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result SetRegistrationHit(NoProblem.Core.DataModel.SupplierModel.Registration2014.LandedRegistraionPageRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, "SetRegistrationHit");
            Result result = new Result();
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager(null);
                manager.SetRegistrationHit(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SetRegistrationHit. Date = {0}", request.date.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        #region Registration 2014
        
        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.LogOnResponse> CreateUserNormal_Registration2014(NoProblem.Core.DataModel.SupplierModel.Registration2014.NormalLogOnRequest request)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.LogOnResponse> result = new Result<DataModel.SupplierModel.Registration2014.LogOnResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.NormalLogOnManager manager = new BusinessLogic.SupplierBL.Registration2014.NormalLogOnManager(null);
                result.Value = manager.CreateUser(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateUserNormal_Registration2014");
            }
            return result;
        }
         
        
        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.LogOnResponse> LogInNormal_Registration2014(NoProblem.Core.DataModel.SupplierModel.Registration2014.NormalLogOnRequest request)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.LogOnResponse> result = new Result<DataModel.SupplierModel.Registration2014.LogOnResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.NormalLogOnManager manager = new BusinessLogic.SupplierBL.Registration2014.NormalLogOnManager(null);
                result.Value = manager.LogIn(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in LogInNormal_Registration2014");
            }
            return result;
        }
        
        /*
        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.LogOnResponse> SocialMediaLogOn_Registration2014(NoProblem.Core.DataModel.SupplierModel.Registration2014.SocialMediaLogOnRequest request)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.LogOnResponse> result = new Result<DataModel.SupplierModel.Registration2014.LogOnResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.SocialMediaLogOnManager manager = new BusinessLogic.SupplierBL.Registration2014.SocialMediaLogOnManager(null);
                result.Value = manager.SocialMediaLogOn(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SocialMediaLogOn_Registration2014");
            }
            return result;
        }
        */
        [WebMethod]
        public Result<int> EmailVerified_Registration2014(Guid supplierId)
        {
            Result<int> result = new Result<int>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.EmailVerificator verificator = new BusinessLogic.SupplierBL.Registration2014.EmailVerificator();
                result.Value = verificator.EmailVerified(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in EmailVerified_Registration2014");
            }
            return result;
        }

        [WebMethod]
        public Result<bool> ResendEmailVerificationEmail_Registration2014(Guid supplierId)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.EmailVerificator verificator = new BusinessLogic.SupplierBL.Registration2014.EmailVerificator();
                result.Value = verificator.ResendEmailVerificationEmail(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in ResendEmailVerificationEmail_Registration2014");
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.ChoosePlanResponse> ChoosePlan_Registration2014(NoProblem.Core.DataModel.SupplierModel.Registration2014.ChoosePlanRequest request)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.ChoosePlanResponse> result = new Result<DataModel.SupplierModel.Registration2014.ChoosePlanResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.ChoosePlanManager manager = new BusinessLogic.SupplierBL.Registration2014.ChoosePlanManager(null);
                result.Value = manager.ChoosePlan(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in ChoosePlan_Registration2014");
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.EnterBusinessDetailsResponse> EnterBusinessDetails_Registration2014(NoProblem.Core.DataModel.SupplierModel.Registration2014.EnterBusinessDetailsRequest request)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.EnterBusinessDetailsResponse> result = new Result<DataModel.SupplierModel.Registration2014.EnterBusinessDetailsResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.EnterBusinessDetailsManager manager = new BusinessLogic.SupplierBL.Registration2014.EnterBusinessDetailsManager(null);
                result.Value = manager.EnterBusinessDetails(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in EnterBusinessDetails_Registration2014");
            }
            return result;
        }
        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.EnterBusinessDetailsResponse> EnterBusinessDetailsV2(NoProblem.Core.DataModel.SupplierModel.Registration2014.EnterBusinessDetailsRequest request)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.EnterBusinessDetailsResponse> result = new Result<DataModel.SupplierModel.Registration2014.EnterBusinessDetailsResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.EnterBusinessDetailsManager manager = new BusinessLogic.SupplierBL.Registration2014.EnterBusinessDetailsManager(null);
                result.Value = manager.EnterBusinessDetailsV2(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in EnterBusinessDetails_Registration2014");
            }
            return result;
        }
        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.GetBusinessDetailsResponse> GetBusinessDetails_Registration2014(Guid supplierId)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.GetBusinessDetailsResponse> result =
                new Result<DataModel.SupplierModel.Registration2014.GetBusinessDetailsResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.EnterBusinessDetailsManager manager = new BusinessLogic.SupplierBL.Registration2014.EnterBusinessDetailsManager(null);
                result.Value = manager.GetBusinessDetails(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetBusinessDetails_Registration2014");
            }
            return result;
        }

        [WebMethod]
        public Result<string> GetCoverAreas_Registration2014(NoProblem.Core.DataModel.SupplierModel.Registration2014.GetCoverAreaRequest request)
        {
            Result<string> result = new Result<string>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager manager = new BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager(null);
                NoProblem.Core.DataModel.SupplierModel.Registration2014.GetCoverAreaResponse value = manager.GetCoverArea(request);
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(NoProblem.Core.DataModel.SupplierModel.Registration2014.GetCoverAreaResponse));
                System.IO.StringWriter textWriter = new System.IO.StringWriter();
                ser.Serialize(textWriter, value);
                string xmlValue = textWriter.ToString();
                result.Value = xmlValue;
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCoverAreas_Registration2014");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.GetCoverAreaResponse> GetCoverAreas_Registration2014_obj(NoProblem.Core.DataModel.SupplierModel.Registration2014.GetCoverAreaRequest request)
        {
            Result<DataModel.SupplierModel.Registration2014.GetCoverAreaResponse> result = new Result<DataModel.SupplierModel.Registration2014.GetCoverAreaResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager manager = new BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager(null);
                NoProblem.Core.DataModel.SupplierModel.Registration2014.GetCoverAreaResponse value = manager.GetCoverArea(request);
                result.Value = value;
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCoverAreas_Registration2014_obj");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.GetCoverAreaResponse> GetDefaultCoverAreas(DataModel.SupplierModel.Registration2014.GetDefaultCoverAreasRequest request)
        {
            Result<DataModel.SupplierModel.Registration2014.GetCoverAreaResponse> result =
                new Result<DataModel.SupplierModel.Registration2014.GetCoverAreaResponse>();
            try
            {
                NoProblem.Core.DataModel.SupplierModel.Registration2014.GetCoverAreaResponse response = new NoProblem.Core.DataModel.SupplierModel.Registration2014.GetCoverAreaResponse();
                var defaultAreas = NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager.CreateDefaultCoverAreaObject(request.Longitude, request.Latitude, null);
                response.AreasList.Add(defaultAreas);
                result.Value = response;
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetDefaultCoverAreas");
            }
            return result;
        }

        [WebMethod]
        public Result<int> GetNumberOfZipcodes(Guid regionId, decimal latitude, decimal longitude, decimal radius)
        {
            Result<int> result = new Result<int>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager manager = new BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager(null);
                int resultValue;
                if (regionId == Guid.Empty)
                {
                    resultValue = manager.GetNumberOfZipcodes(latitude, longitude, radius);
                }
                else
                {
                    resultValue = manager.GetNumberOfZipcodes(regionId);
                }
                result.Value = resultValue;
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetNumberOfZipcodes");
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.SetCoverAreasResponse> SetCoverAreas__Registration2014(NoProblem.Core.DataModel.SupplierModel.Registration2014.SetCoverAreasRequest request)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.SetCoverAreasResponse> result =
                new Result<DataModel.SupplierModel.Registration2014.SetCoverAreasResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager manager = new BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager(null);
                result.Value = manager.SetCoverArea(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SetCoverAreas__Registration2014");
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.GetCoverAreaResponse> SetBusinessAddress__Registration2014(NoProblem.Core.DataModel.SupplierModel.Registration2014.SetBussinessAddressRequest request)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.GetCoverAreaResponse> result =
                new Result<DataModel.SupplierModel.Registration2014.GetCoverAreaResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager manager = new BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreaManager(null);
                result.Value = manager.SetBussinessAddress(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SetBusinessAddress__Registration2014");
            }
            return result;
        }

        [WebMethod]
        public Result<string> Checkout_GetPlan_Registration2014(Guid supplierId)
        {
            Result<string> result = new Result<string>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.CheckoutManager manager = new BusinessLogic.SupplierBL.Registration2014.CheckoutManager(null);
                result.Value = manager.GetPlan(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Checkout_GetPlan_Registration2014");
            }
            return result;
        }

        [WebMethod]
        public Result SetWorkingHours_Registration2014(NoProblem.Core.DataModel.SupplierModel.Registration2014.WorkingHours.SetWorkingHoursRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.WorkingHours.WorkingHoursManager manager = new BusinessLogic.SupplierBL.Registration2014.WorkingHours.WorkingHoursManager(null);
                manager.SetWorkingHours(request);
                BusinessLogic.AdvertiserDashboards.GetStartedTasksManager getStarted = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTasksManager(manager.XrmDataContext);
                getStarted.CrossOutGetStartedTask(request.SupplierId, NoProblem.Core.DataModel.AdvertiserDashboards.GetStartedTaskType.work_hours);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SetWorkingHours_Registration2014");
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.WorkingHours.GetWorkingHoursResponse> GetWorkingHours_Registration2014(Guid supplierId)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.WorkingHours.GetWorkingHoursResponse> result =
                new Result<DataModel.SupplierModel.Registration2014.WorkingHours.GetWorkingHoursResponse>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.WorkingHours.WorkingHoursManager manager = new BusinessLogic.SupplierBL.Registration2014.WorkingHours.WorkingHoursManager(null);
                result.Value = manager.GetWorkingHours(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetWorkingHours_Registration2014");
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.GetBusinessProfileResponse> GetBusinessProfile(Guid supplierId)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Registration2014.GetBusinessProfileResponse> result =
                new Result<DataModel.SupplierModel.Registration2014.GetBusinessProfileResponse>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.BusinessProfileManager manager = new BusinessLogic.SupplierBL.Registration2014.BusinessProfileManager(null);
                result.Value = manager.GetBusinessProfile(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetBusinessProfile");
            }
            return result;
        }

        [WebMethod]
        public Result SetBusinessProfileData(NoProblem.Core.DataModel.SupplierModel.Registration2014.SetBusinessProfileDataRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.BusinessProfileManager manager = new BusinessLogic.SupplierBL.Registration2014.BusinessProfileManager(null);
                manager.SetBusinessProfileData(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SetBusinessProfileData");
            }
            return result;
        }

        [WebMethod]
        public Result ChangeSubscriptionPlan(NoProblem.Core.DataModel.SupplierModel.Registration2014.ChangeSubscriptionPlanRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.SubscriptionPlanChanger changer = new BusinessLogic.SupplierBL.Registration2014.SubscriptionPlanChanger(null);
                changer.ChangeSubscriptionPlan(request.SupplierId, request.NewSubscriptionPlan);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in ChangeSubscriptionPlan");
            }
            return result;
        }

        [WebMethod]
        public Result<bool> DeleteMyAccount(DataModel.SupplierModel.Registration2014.DeleteAccountRequest request)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.AccountDeleter deleter = new BusinessLogic.SupplierBL.Registration2014.AccountDeleter(null);
                result.Value = deleter.DeleteAccount(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in DeleteMyAccount");
            }
            return result;
        }

        [WebMethod]
        public Result SendInvitationEmails(DataModel.SupplierModel.Registration2014.SendInvitationEmailsRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.FriendEmailInviter inviter = new BusinessLogic.SupplierBL.Registration2014.FriendEmailInviter(null);
                inviter.SendInvitationEmails(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SendInvitationEmails");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.GetPaymentMethodDataResponse> GetPaymentMethodData(Guid supplierId)
        {
            var result = new Result<DataModel.SupplierModel.Registration2014.GetPaymentMethodDataResponse>();
            try
            {
                var manager = new BusinessLogic.SupplierBL.Registration2014.PaymentMethodManager(null);
                result.Value = manager.GetPaymentMethodData(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetPaymentMethodData. SupplierId = {0}", supplierId);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.SubscriptionCostAndBonus> GetSubscriptionPlanCost()
        {
            Result<DataModel.SupplierModel.Registration2014.SubscriptionCostAndBonus> result = new Result<DataModel.SupplierModel.Registration2014.SubscriptionCostAndBonus>();
            try
            {
                result.Value = NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.ChoosePlanManager.GetSubscriptionPlanCostAndRegistrationBonus();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetSubscriptionPlanCost");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod(MessageName = "GetSubscriptionPlanCostForSupplier")]
        public Result<decimal> GetSubscriptionPlanCost(Guid supplierId)
        {
            Result<decimal> result = new Result<decimal>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.ChoosePlanManager manager = new BusinessLogic.SupplierBL.Registration2014.ChoosePlanManager(null);
                result.Value = manager.GetSubscriptionPlanCost(supplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetSubscriptionPlanCost");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result CheckoutButtonClicked_Registration2014(Guid supplierId)
        {
            Result result = new Result();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CheckoutManager manager = new BusinessLogic.SupplierBL.Registration2014.CheckoutManager(null);
                manager.CheckoutButtonClicked(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckoutButtonClicked_Registration2014");
            }
            return result;
        }

        #endregion

        #region Forgot password flow

        [WebMethod]
        public Result<bool> SendResetEmail(string email)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.SupplierBL.ForgotPasswordFlowManager manager = new BusinessLogic.SupplierBL.ForgotPasswordFlowManager(null);
                result.Value = manager.SendResetEmail(email);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SendResetEmail. email = {0}", email);
            }
            return result;
        }

        [WebMethod]
        public Result<bool> CheckResetPasswordToken(string token)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.SupplierBL.ForgotPasswordFlowManager manager = new BusinessLogic.SupplierBL.ForgotPasswordFlowManager(null);
                result.Value = manager.CheckResetPasswordToken(token);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckResetPasswordToken. token = {0}", token);
            }
            return result;
        }

        [WebMethod]
        public Result NewPasswordEntered(string password, string token)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierBL.ForgotPasswordFlowManager manager = new BusinessLogic.SupplierBL.ForgotPasswordFlowManager(null);
                manager.NewPasswordEntered(password, token);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in NewPasswordEntered. password = {0}. token = {1}", password, token);
            }
            return result;
        }

        #endregion

        #region Legacy 2014

        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.Legacy.PhoneLogInResponse> PhoneLogIn_Legacy2014(DataModel.SupplierModel.Registration2014.Legacy.PhoneLogInRequest request)
        {
            Result<DataModel.SupplierModel.Registration2014.Legacy.PhoneLogInResponse> result = new Result<DataModel.SupplierModel.Registration2014.Legacy.PhoneLogInResponse>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.Legacy.PhoneLogInManager manager = new BusinessLogic.SupplierBL.Registration2014.Legacy.PhoneLogInManager(null);
                result.Value = manager.PhoneLogIn(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PhoneLogIn_Legacy2014. Phone = {0}. Pass = {1}", request.Phone, request.Password);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.Legacy.EmailVerifiyLegacyPreLoadResponse> EmailVerifiyLegacyPreLoad_Legacy2014(Guid supplierId)
        {
            Result<DataModel.SupplierModel.Registration2014.Legacy.EmailVerifiyLegacyPreLoadResponse> result = new Result<DataModel.SupplierModel.Registration2014.Legacy.EmailVerifiyLegacyPreLoadResponse>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.Legacy.EmailVerificationManager manager = new BusinessLogic.SupplierBL.Registration2014.Legacy.EmailVerificationManager(null);
                result.Value = manager.EmailVerifiyLegacyPreLoad(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in EmailVerifiyLegacyPreLoad_Legacy2014. SupplierId = {0}", supplierId);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.Legacy.SendVerificationEmailResponse> SendVerificationEmail_Legacy2014(Guid supplierId, string email)
        {
            Result<DataModel.SupplierModel.Registration2014.Legacy.SendVerificationEmailResponse> result =
                new Result<DataModel.SupplierModel.Registration2014.Legacy.SendVerificationEmailResponse>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.Legacy.EmailVerificationManager manager = new BusinessLogic.SupplierBL.Registration2014.Legacy.EmailVerificationManager(null);
                result.Value = manager.SendEmail(supplierId, email);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SendVerificationEmail_Legacy2014. SupplierId = {0}. Email = {1}", supplierId, email);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.Legacy.EmailVerifiedResponse> EmailVerified_Legacy2014(Guid supplierId)
        {
            Result<DataModel.SupplierModel.Registration2014.Legacy.EmailVerifiedResponse> result =
                new Result<DataModel.SupplierModel.Registration2014.Legacy.EmailVerifiedResponse>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.Legacy.EmailVerificationManager manager = new BusinessLogic.SupplierBL.Registration2014.Legacy.EmailVerificationManager(null);
                result.Value = manager.EmailVerified(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in EmailVerified_Legacy2014. SupplierId = {0}", supplierId);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.Legacy.UpdatePaymentMethodResponse> UpdatePaymentMethodOnLoad_Legacy2014(Guid supplierId)
        {
            Result<DataModel.SupplierModel.Registration2014.Legacy.UpdatePaymentMethodResponse> result = new Result<DataModel.SupplierModel.Registration2014.Legacy.UpdatePaymentMethodResponse>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.Legacy.UpdatePaymentMethodManager manager = new BusinessLogic.SupplierBL.Registration2014.Legacy.UpdatePaymentMethodManager(null);
                result.Value = manager.UpdatePaymentMethodOnLoad(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdatePaymentMethod_Legacy2014. SupplierId = {0}", supplierId);
            }
            return result;
        }

        #endregion

        [WebMethod]
        public Result<DataModel.SupplierModel.Mobile.LeadsListResponse> GetLeadsListForMobile(DataModel.SupplierModel.Mobile.LeadsListRequest request)
        {
            Result<DataModel.SupplierModel.Mobile.LeadsListResponse> result = new Result<DataModel.SupplierModel.Mobile.LeadsListResponse>();
            try
            {
                BusinessLogic.SupplierBL.MobileQueries m = new BusinessLogic.SupplierBL.MobileQueries();
                result.Value = m.GetSupplierLeads(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetLeadsListForMobile");
            }
            return result;
        }
        /*
        [WebMethod]
        public Result<DataModel.SupplierModel.Mobile.LeadsListResponse> GetLeadsListForMobileV2(DataModel.SupplierModel.Mobile.LeadsListRequestV2 request)
        {
            Result<DataModel.SupplierModel.Mobile.LeadsListResponse> result = new Result<DataModel.SupplierModel.Mobile.LeadsListResponse>();
            try
            {
                BusinessLogic.SupplierBL.MobileQueries m = new BusinessLogic.SupplierBL.MobileQueries();
                result.Value = m.GetSupplierLeadsV2(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetLeadsListForMobile");
            }
            return result;
        }
        */
        [WebMethod]
        public Result<DataModel.SupplierModel.Mobile.GetLeadForMobileResponse> GetLeadForMobile(DataModel.SupplierModel.Mobile.GetLeadForMobileRequest request)
        {
            Result<DataModel.SupplierModel.Mobile.GetLeadForMobileResponse> result = new Result<DataModel.SupplierModel.Mobile.GetLeadForMobileResponse>();
            try
            {
                BusinessLogic.SupplierBL.MobileQueries m = new BusinessLogic.SupplierBL.MobileQueries();
                result.Value = m.GetLead(request.SupplierId, request.IncidentAccountId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetLeadForMobile");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.Mobile.SetBidForLeadResponse> SetBidForLead(DataModel.SupplierModel.Mobile.SetBidForLeadRequest request)
        {
            Result<DataModel.SupplierModel.Mobile.SetBidForLeadResponse> result = new Result<DataModel.SupplierModel.Mobile.SetBidForLeadResponse>();
            try
            {
                BusinessLogic.ServiceRequest.BiddingManager manager = new BusinessLogic.ServiceRequest.BiddingManager(null);
                DataModel.SupplierModel.Mobile.SetBidForLeadResponse response = manager.SetBidForLead(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SetBidForLead");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.SupplierModel.Mobile.GetUserAlertsResponse> GetUserAlerts(Guid supplierId)
        {
            Result<DataModel.SupplierModel.Mobile.GetUserAlertsResponse> result =
                new Result<DataModel.SupplierModel.Mobile.GetUserAlertsResponse>();
            try
            {
                BusinessLogic.SupplierBL.UserAlertsManager m = new BusinessLogic.SupplierBL.UserAlertsManager(null);
                result.Value = m.GetUserAlerts(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetUserAlerts. supplierid = {0}", supplierId);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.SupplierModel.Recording>> GetRecordingsForLead(Guid supplierId, Guid incidentAccountId)
        {
            Result<List<DataModel.SupplierModel.Recording>> result = new Result<List<DataModel.SupplierModel.Recording>>();
            try
            {
                BusinessLogic.SupplierBL.CallRecordingsManager manager = new BusinessLogic.SupplierBL.CallRecordingsManager(null);
                result.Value = manager.GetRecordingsForLead(supplierId, incidentAccountId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetRecordingsForLead. supplierId = {0}. incidentAccountId = {1}", supplierId, incidentAccountId);
            }
            return result;
        }

        [WebMethod]
        public Result<string> GetCallRecordingsFilesLocation(Guid supplierId, Guid incidentAccountId)
        {
            Result<string> result = new Result<string>();
            try
            {
                BusinessLogic.SupplierBL.CallRecordingsManager manager = new BusinessLogic.SupplierBL.CallRecordingsManager(null);
                result.Value = manager.GetFilesLocation(supplierId, incidentAccountId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCallRecordingsFilesLocation. supplierId = {0}. incidentAccountId = {1}", supplierId, incidentAccountId);
            }
            return result;
        }

        [WebMethod]
        public Result<bool> PostNewCallRecording(Guid supplierId, Guid incidentAccountId, string file)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.SupplierBL.CallRecordingsManager manager = new BusinessLogic.SupplierBL.CallRecordingsManager(null);
                result.Value = manager.PostNewCallRecording(supplierId, incidentAccountId, file);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PostNewCallRecording. supplierId = {0}. incidentAccountId = {1}. file = {2}", supplierId, incidentAccountId, file);
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.BulkEmails.UnsubscribeFromBulkEmailsResponse> UnsubscribeFromBulkEmails(DataModel.BulkEmails.UnsubscribeFromBulkEmailsRequest request)
        {
            Result<NoProblem.Core.DataModel.BulkEmails.UnsubscribeFromBulkEmailsResponse> result = new Result<NoProblem.Core.DataModel.BulkEmails.UnsubscribeFromBulkEmailsResponse>();
            try
            {
                result.Value = BusinessLogic.BulkEmails.Unsubscribber.Unsubscribe(request.SupplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception inUnsubscribeFromBulkEmails. supplierId = {0}.", request.SupplierId);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.APIs.MobileApi.Settings> GetMobileSettings(Guid supplierId)
        {
            Result<DataModel.APIs.MobileApi.Settings> result = new Result<DataModel.APIs.MobileApi.Settings>();
            try
            {
                BusinessLogic.APIs.MobileAPI.SettingsManager settingsManager = new BusinessLogic.APIs.MobileAPI.SettingsManager();
                result.Value = settingsManager.GetSettings(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetMobileSettings. SupplierId = {0}", supplierId);
            }
            return result;
        }

        [WebMethod]
        public Result SetMobileSettings(Guid supplierId, DataModel.APIs.MobileApi.Settings settings)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.APIs.MobileAPI.SettingsManager settingsManager = new BusinessLogic.APIs.MobileAPI.SettingsManager();
                settingsManager.SetSettings(supplierId, settings);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SetMobileSettings. SupplierId = {0}", supplierId);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.APIs.MobileApi.LiveBid>> GetLiveBidRequest(Guid supplierId)
        {
            Result<List<DataModel.APIs.MobileApi.LiveBid>> result = new Result<List<DataModel.APIs.MobileApi.LiveBid>>();
            try
            {
                BusinessLogic.ServiceRequest.BiddingManager manager = new BusinessLogic.ServiceRequest.BiddingManager(null);
                result.Value = manager.GetLiveBidRequest(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetLiveBid. supplierId = {0}", supplierId);
            }
            return result;
        }

       

        [WebMethod]
        public Result<bool> CreatePaymentMethod(DataModel.APIs.MobileApi.CreatePaymentMethodRequest request)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.PaymentMethodManager manager = new BusinessLogic.SupplierBL.Registration2014.PaymentMethodManager(null);
                result.Value = manager.CreatePaymentMethod(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CreatePaymentMethod");
            }
            return result;
        }

        [WebMethod]
        public Result<bool> ConnectAndRecord(Guid incidentAccountId, Guid supplierId)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.APIs.MobileAPI.ConnectAndRecord connectAndRecord = new BusinessLogic.APIs.MobileAPI.ConnectAndRecord(incidentAccountId, supplierId);
                result.Value = connectAndRecord.InitiateCall();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in ConnectAndRecord");
            }
            return result;
        }

        [WebMethod]
        public Result LogOutFromMobile(Guid supplierID)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.APIs.MobileAPI.Devices.DevicesManager manager =
                    new BusinessLogic.APIs.MobileAPI.Devices.DevicesManager(null);
                manager.Logout(supplierID);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in LogOutFromMobile");
            }
            return result;
        }

        [WebMethod]
        public Result UpdateLeadAddressBySupplier(string newAddress, Guid incidentAccountId)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierBL.SupplierLeadEditor editor =
                    new BusinessLogic.SupplierBL.SupplierLeadEditor(null);
                editor.UpdateLeadAddress(newAddress, incidentAccountId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateLeadAddressBySupplier");
            }
            return result;
        }

        [WebMethod]
        public Result UpdateLeadCustomerNameBySupplier(string newName, Guid incidentAccountId)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierBL.SupplierLeadEditor editor =
                    new BusinessLogic.SupplierBL.SupplierLeadEditor(null);
                editor.UpdateLeadCustomerName(newName, incidentAccountId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateLeadCustomerNameBySupplier");
            }
            return result;
        }

        [WebMethod]
        public Result UpdateBusinessDescription(NoProblem.Core.DataModel.SupplierModel.Mobile.UpdateBusinessDescriptionRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.BusinessProfileManager manager =
                   new BusinessLogic.SupplierBL.Registration2014.BusinessProfileManager(null);
                manager.UpdateBusinessDescription(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateBusinessDescription for supplier with id {0}", request.supplierId);
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.SupplierModel.Mobile.BusinessDescriptionResponse> GetBusinessDescription(Guid supplierId)
        {
            Result<NoProblem.Core.DataModel.SupplierModel.Mobile.BusinessDescriptionResponse> result = new Result<NoProblem.Core.DataModel.SupplierModel.Mobile.BusinessDescriptionResponse>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.BusinessProfileManager manager =
                    new BusinessLogic.SupplierBL.Registration2014.BusinessProfileManager(null);
                result.Value = manager.GetBusinessDescription(supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetBusinessDescription for supplier with id {0}", supplierId);
            }
            return result;
        }
        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoResponse>
            UpdateSupplierLogo(DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoUploadRequest uploadRequest)
        {
            Result<DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoResponse> result = new Result<DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.LogoManager logoManager = new NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.LogoManager();
                result.Value = logoManager.UpdateSupplierLogo(uploadRequest);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateMobileDevice. customerId = {0}, ImageUrl={1}", uploadRequest.SupplierId, uploadRequest.ImageUrl);
            }
            return result;
        }
        [WebMethod]
        public Result<DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoResponse>
            GetCustomertLogo(Guid supplierId)
        {
            Result<DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoResponse> result = new Result<DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.LogoManager logoManager = new NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.LogoManager();
                result.Value = new DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoResponse() { ImageUrl = logoManager.GetLogoImageUrl(supplierId) };
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCustomertLogo. customerId = {0}", supplierId);
            }
            return result;
        }
    }
}
