﻿using System;
using System.ComponentModel;
using System.Web.Services;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.PublisherPortal;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.PublisherPortal;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using NoProblem.Core.BusinessLogic.APIs.MobileAPI.Devices;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for Customer
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class CustomersService : NpServiceBase
    {
        [WebMethod]
        public Result<NoProblem.Core.DataModel.Expertise.DidYouMeanResponse> DidYouMean(string searchTerm)
        {
            Result<NoProblem.Core.DataModel.Expertise.DidYouMeanResponse> result =
                new Result<DataModel.Expertise.DidYouMeanResponse>();
            try
            {
                BusinessLogic.CustomerBL.DidYouMeanManager manager = new BusinessLogic.CustomerBL.DidYouMeanManager(null);
                result.Value = manager.DidYouMean(searchTerm);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in DidYouMean with searchTerm '{0}'", searchTerm);
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.Consumer.CreateNewVideoLeadResponse> CraeteNewVideoLead(DataModel.Consumer.VideoRequests.NewVideoRequest request)
        {
            Result<NoProblem.Core.DataModel.Consumer.CreateNewVideoLeadResponse> result = new Result<NoProblem.Core.DataModel.Consumer.CreateNewVideoLeadResponse>();
            try
            {
                BusinessLogic.ServiceRequest.VideoRequests.VideoRequestsManager manager = new BusinessLogic.ServiceRequest.VideoRequests.VideoRequestsManager(null);
                result.Value = manager.CreateNewVideoRequest(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CraeteNewVideoLead");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Consumer.CustomerLogOnResponse> CustomerLogOn(DataModel.Consumer.CustomerLogOnRequest request)
        {
            Result<DataModel.Consumer.CustomerLogOnResponse> result = new Result<DataModel.Consumer.CustomerLogOnResponse>();
            try
            {
                BusinessLogic.CustomerBL.LogOnManager manager = new BusinessLogic.CustomerBL.LogOnManager(null);
                result.Value = manager.LogOn(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CustomerLogOn");
            }
            return result;
        }
        [WebMethod]
        public Result<DataModel.Consumer.CustomerLogOnResponse> CustomerChangePhoneNumber(DataModel.Consumer.CustomerChangePhoneNumberRequest request)
        {
            Result<DataModel.Consumer.CustomerLogOnResponse> result = new Result<DataModel.Consumer.CustomerLogOnResponse>();
            try
            {
                BusinessLogic.CustomerBL.LogOnManager manager = new BusinessLogic.CustomerBL.LogOnManager(null);
                result.Value = manager.ChangePhoneNumber(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CustomerChangePhoneNumber customerId:{0}, PhineNumber{1}", request.customerId, request.PhoneNumber);
            }
            return result;
        }

        [WebMethod]
        public Result<bool> SendEmailAfterCallAndRecordDesktopApp(Guid callId, string email)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.OrganicDesktopApp.EmailSenderAfterCallAndRecord sender = new BusinessLogic.OrganicDesktopApp.EmailSenderAfterCallAndRecord(null);
                result.Value = sender.SendEmail(callId, email);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SendEmailAfterCallAndRecordDesktopApp");
            }
            return result;
        }

        [WebMethod]
        public Result CustomerDisallowesRecording(Guid incidentId, Guid supplierId)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.ConsumerDesktop.ConsumerManager manager = new BusinessLogic.ConsumerDesktop.ConsumerManager(null);
                manager.CustomerDisallowesRecording(incidentId, supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CustomerDisallowesRecording");
            }
            return result;
        }

        [WebMethod]
        public Result<bool> CustomerCallRecorderPayemnt(NoProblem.Core.DataModel.Consumer.DesktopApp.CustomerPaymentInDesktopApp payment)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.ConsumerDesktop.PaymentsManager manager = new BusinessLogic.ConsumerDesktop.PaymentsManager(null);
                result.Value = manager.AddPayment(payment);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CustomerCallRecorderPayemnt");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Consumer.Tips.TipBasicData>> GetTipsForIncident(Guid incidentId)
        {
            Result<List<DataModel.Consumer.Tips.TipBasicData>> result =
                new Result<List<DataModel.Consumer.Tips.TipBasicData>>();
            try
            {
                BusinessLogic.ServiceRequest.CustomerTips.CustomerTipsManager manager = new BusinessLogic.ServiceRequest.CustomerTips.CustomerTipsManager(null);
                result.Value = manager.GetTipsForIncidentId(incidentId).ToList();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetTipsForIncident");
            }
            return result;
        }

        [WebMethod]
        public Result SetCustomerTipFeedback(DataModel.Consumer.Tips.TipFeedback feedback)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.ServiceRequest.CustomerTips.CustomerTipsManager manager = new BusinessLogic.ServiceRequest.CustomerTips.CustomerTipsManager(null);
                manager.SetCustomerTipFeedback(feedback);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SetCustomerTipFeedback");
            }
            return result;
        }

        [WebMethod]
        public Result SendEmailToCustomerAfterCase(DataModel.Consumer.SendEmailToCustomerAfterCaseRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.ServiceRequest.CustomerAfterCaseService service = new BusinessLogic.ServiceRequest.CustomerAfterCaseService(null);
                service.SendAfterCaseEmail(request.IncidentId, request.Email);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SendEmailToCustomerAfterCase");
            }
            return result;
        }

        [WebMethod]
        public Result StartDesktopAppCase(Guid incidentId)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.ServiceRequest.ServiceRequestManager manager = new BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                manager.StartDesktopAppCase(incidentId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in StartDesktopAppCase");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Pusher.CaseFlow.CaseAllDataContainer> GetCaseDataForDesktopApp(Guid incidentId)
        {
            Result<DataModel.Pusher.CaseFlow.CaseAllDataContainer> result = new Result<DataModel.Pusher.CaseFlow.CaseAllDataContainer>();
            try
            {
                BusinessLogic.ServiceRequest.ServiceRequestManager manager = new BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                result.Value = manager.GetCaseDataForDesktopApp(incidentId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCaseDataForDesktopApp");
            }
            return result;
        }

        [WebMethod]
        public Result ContinueDesktopAppCase(Guid incidentId, bool wantsToContinue)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.ServiceRequest.ServiceRequestManager manager = new BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                manager.ContinueDesktopAppCase(incidentId, wantsToContinue);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in ContinueDesktopAppCase");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Expertise.ExpertiseInSpecialCategoryContainer>> GetSpecialCategoryGroup(string connectivityName)
        {
            Result<List<DataModel.Expertise.ExpertiseInSpecialCategoryContainer>> result = new Result<List<NoProblem.Core.DataModel.Expertise.ExpertiseInSpecialCategoryContainer>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSpecialCategoryGroup started. connectivityName = {0}", connectivityName);
                ExpertiseManager manager = new ExpertiseManager(null);
                result.Value = manager.GetSpecialCategoryGroup(connectivityName);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetSpecialCategoryGroup. connectivityName = {0}", connectivityName);
            }
            return result;
        }

        [WebMethod]
        public Result HandleAddOnAction(NoProblem.Core.DataModel.Consumer.AddOnActionRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.AddOns.AddOnsManager manager = new NoProblem.Core.BusinessLogic.AddOns.AddOnsManager(null);
                manager.HandleAddOnAction(request.OriginId, request.MacAddresses, request.IP, request.IsInstallation, request.AppType, request.IsDuplicate, request.SubId, request.Country, request.IsUpdater);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in HandleAddOnAction");
            }
            return result;
        }


        [WebMethod]
        public Result ReportInstallation(NoProblem.Core.DataModel.Consumer.ReportInstallationRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.AddOns.AddOnsManager manager = new NoProblem.Core.BusinessLogic.AddOns.AddOnsManager(null);
                manager.ReportGhostInstallation(request);

            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in HandleAddOnAction");
            }
            return result;
        }



        [WebMethod]
        public Result DeskAppLog(NoProblem.Core.DataModel.Consumer.DeskAppLogRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.AddOns.AddOnsManager manager = new NoProblem.Core.BusinessLogic.AddOns.AddOnsManager(null);
                manager.DeskAppLog(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in DeskAppLog");
            }
            return result;
        }

        [WebMethod]
        public Result WidgetRemovalLog(DataModel.Dashboard.Convertion.CreateWidgetRemovalLogRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "WidgetRemovalLog started");
                BusinessLogic.Dashboard.ConvertionsManager manager = new NoProblem.Core.BusinessLogic.Dashboard.ConvertionsManager(null);
                manager.CreateWidgetRemovalLog(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in WidgetRemovalLog");
            }
            return result;
        }

        [WebMethod]
        public Result DescriptionValidationLog(DataModel.DescriptionValidationLogRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DescriptionValidationLog started. QualityCheckName = {0}", request.QualityCheckName);
                BusinessLogic.ServiceRequest.DescriptionValidationLogger logger = new NoProblem.Core.BusinessLogic.ServiceRequest.DescriptionValidationLogger(null);
                logger.LogValidation(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in DescriptionValidationLog");
            }
            return result;
        }

        [WebMethod]
        public Result CreateExposure(DataModel.Dashboard.CreateExposureRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CreateExposure started.");
                BusinessLogic.Dashboard.ConvertionsManager manager = new NoProblem.Core.BusinessLogic.Dashboard.ConvertionsManager(null);
                manager.CreateExposure(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "CreateExposure failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            LogUtils.MyHandle.WriteToLog(9, "_CreateExposure ended.");
            return result;
        }

        [WebMethod]
        public Result<DataModel.Consumer.NonPpcServiceResponse> CreateNonPpcService(DataModel.Consumer.NonPpcServiceRequest request)
        {
            Result<DataModel.Consumer.NonPpcServiceResponse> result = new Result<NoProblem.Core.DataModel.Consumer.NonPpcServiceResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CreateNonPpcService started. AdvertiserName = {0}, AdvertiserPhoneNumber = {1}, ContactPhoneNumber = {2}", request.AdvertiserName, request.AdvertiserPhoneNumber, request.ContactPhoneNumber);
                BusinessLogic.ServiceRequest.NonPpcServiceManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.NonPpcServiceManager(null);
                result.Value = manager.CreateNonPpcService(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "CreateNonPpcService AdvertiserName = {0}, AdvertiserPhoneNumber = {1}, ContactPhoneNumber = {2}", request.AdvertiserName, request.AdvertiserPhoneNumber, request.ContactPhoneNumber);
            }
            LogUtils.MyHandle.WriteToLog(9, "_CreateNonPpcService ended. AdvertiserName = {0}, AdvertiserPhoneNumber = {1}, ContactPhoneNumber = {2}", request.AdvertiserName, request.AdvertiserPhoneNumber, request.ContactPhoneNumber);
            return result;
        }

        [WebMethod]
        public Result<DataModel.Consumer.SupplierServiceResponse> CreateSupplierSpecificService(DataModel.Consumer.SupplierServiceRequest request)
        {
            Result<DataModel.Consumer.SupplierServiceResponse> result = new Result<NoProblem.Core.DataModel.Consumer.SupplierServiceResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CreateSupplierSpecificService started. SupplierId = {0}", request.SupplierId.ToString());
                BusinessLogic.ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                DataModel.Consumer.SupplierServiceResponse response = manager.CreateService(request) as DataModel.Consumer.SupplierServiceResponse;
                result.Value = response;
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "CreateSupplierSpecificService failed with exception. SupplierId = {0}", request.SupplierId.ToString());
            }
            LogUtils.MyHandle.WriteToLog(9, "_CreateSupplierSpecificService ended. SupplierId = {0}", request.SupplierId.ToString());
            return result;
        }

        [WebMethod]
        public Result<DataModel.Consumer.ScheduledServiceResponse> CreateScheduledServiceWithZipCodeUsa(DataModel.Consumer.ScheduledServiceRequest request)
        {
            Result<DataModel.Consumer.ScheduledServiceResponse> result = new Result<NoProblem.Core.DataModel.Consumer.ScheduledServiceResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, @"CreateScheduledServiceWithZipCodeUsa started. consumerPhoneNumber = {0}, expertiseCode = {1}, expertiseType = {2}, NumOfSuppliers = {3}, PreferedCallTime = {4}, 
                        regionCode = {5}, regionLevel = {6}, originId = {7}, description = {8}, siteId = {9}",
                    request.ContactPhoneNumber, request.ExpertiseCode, request.ExpertiseType, request.NumOfSuppliers, request.PrefferedCallTime, request.RegionCode,
                    request.RegionLevel, request.OriginId, request.RequestDescription, request.SiteId);
                BusinessLogic.ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                DataModel.Consumer.ScheduledServiceResponse response = manager.CreateServiceWithZipcodeUSA(request) as DataModel.Consumer.ScheduledServiceResponse;
                result.Value = response;
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, @"CreateScheduledServiceWithZipCodeUsa failed with exception. consumerPhoneNumber = {0}, expertiseCode = {1}, expertiseType = {2}, NumOfSuppliers = {3}, PreferedCallTime = {4}, 
                        regionCode = {5}, regionLevel = {6}, originId = {7}, description = {8}, siteId = {9}",
                    request.ContactPhoneNumber, request.ExpertiseCode, request.ExpertiseType, request.NumOfSuppliers, request.PrefferedCallTime, request.RegionCode,
                    request.RegionLevel, request.OriginId, request.RequestDescription, request.SiteId);
            }
            LogUtils.MyHandle.WriteToLog(9, @"CreateScheduledServiceWithZipCodeUsa ended. consumerPhoneNumber = {0}, expertiseCode = {1}, expertiseType = {2}, NumOfSuppliers = {3}, PreferedCallTime = {4}, 
                        regionCode = {5}, regionLevel = {6}, originId = {7}, description = {8}, siteId = {9}",
                    request.ContactPhoneNumber, request.ExpertiseCode, request.ExpertiseType, request.NumOfSuppliers, request.PrefferedCallTime, request.RegionCode,
                    request.RegionLevel, request.OriginId, request.RequestDescription, request.SiteId);
            return result;
        }

        [WebMethod]
        public Result<DataModel.Consumer.ScheduledServiceResponse> CreateScheduledService(DataModel.Consumer.ScheduledServiceRequest request)
        {
            Result<DataModel.Consumer.ScheduledServiceResponse> result = new Result<NoProblem.Core.DataModel.Consumer.ScheduledServiceResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, @"CreateFutureService started. consumerPhoneNumber = {0}, expertiseCode = {1}, expertiseType = {2}, NumOfSuppliers = {3}, PreferedCallTime = {4}, 
                        regionCode = {5}, regionLevel = {6}, originId = {7}, description = {8}, siteId = {9}",
                    request.ContactPhoneNumber, request.ExpertiseCode, request.ExpertiseType, request.NumOfSuppliers, request.PrefferedCallTime, request.RegionCode,
                    request.RegionLevel, request.OriginId, request.RequestDescription, request.SiteId);
                BusinessLogic.ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                DataModel.Consumer.ScheduledServiceResponse response = manager.CreateService(request) as DataModel.Consumer.ScheduledServiceResponse;
                result.Value = response;
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, @"CreateFutureService failed with exception. consumerPhoneNumber = {0}, expertiseCode = {1}, expertiseType = {2}, NumOfSuppliers = {3}, PreferedCallTime = {4}, 
                        regionCode = {5}, regionLevel = {6}, originId = {7}, description = {8}, siteId = {9}",
                    request.ContactPhoneNumber, request.ExpertiseCode, request.ExpertiseType, request.NumOfSuppliers, request.PrefferedCallTime, request.RegionCode,
                    request.RegionLevel, request.OriginId, request.RequestDescription, request.SiteId);
            }
            LogUtils.MyHandle.WriteToLog(9, @"_CreateFutureService ended. consumerPhoneNumber = {0}, expertiseCode = {1}, expertiseType = {2}, NumOfSuppliers = {3}, PreferedCallTime = {4}, 
                        regionCode = {5}, regionLevel = {6}, originId = {7}, description = {8}, siteId = {9}",
                    request.ContactPhoneNumber, request.ExpertiseCode, request.ExpertiseType, request.NumOfSuppliers, request.PrefferedCallTime, request.RegionCode,
                    request.RegionLevel, request.OriginId, request.RequestDescription, request.SiteId);
            return result;
        }

        [WebMethod]
        public string SearchSiteSuppliers(SearchRequest Request)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(8, "SearchSiteSuppliers started - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SiteId"] == "np_israel" && Request.RegionLevel == 2)
                {
                    Request.RegionLevel = 3;
                }
                NoProblem.Core.BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                System.Xml.Linq.XElement doc = supplierLogic.SearchSiteSuppliers(Request.SiteId, Request.ExpertiseCode, Request.ExpertiseLevel, Request.RegionCode, Request.RegionLevel, Request.OriginId, Request.MaxResults, Request.UpsaleId, Request.PageNumber, Request.PageSize, false, false, false, null);
                retVal = doc.ToString();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SearchSiteSuppliers Failed with exception");
                retVal = "<Suppliers><Error>Failed</Error></Suppliers>";
            }
            LogUtils.MyHandle.WriteToLog(9, "_SearchSiteSuppliers ended - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            return retVal;
        }

        [WebMethod]
        public string SearchSuppliersDapazPoc(string headingCodeDapaz, string mekomonCode, Guid originId)
        {
            string retVal = null;
            try
            {
                NoProblem.Core.BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                System.Xml.Linq.XElement doc = supplierLogic.SearchSiteSuppliersDapazPoc(headingCodeDapaz, mekomonCode, originId);
                retVal = doc.ToString();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SearchSuppliersDapazPoc Failed with exception");
                retVal = "<Suppliers><Error>Failed</Error></Suppliers>";
            }
            return retVal;
        }

        [WebMethod]
        public Result<List<DataModel.Expertise.ExpertiseWithIsActive>> GetHeadingsForDondil()
        {
            Result<List<DataModel.Expertise.ExpertiseWithIsActive>> result = new Result<List<NoProblem.Core.DataModel.Expertise.ExpertiseWithIsActive>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetHeadingsForDondil started");
                BusinessLogic.Expertises.ExpertiseManager manager = new NoProblem.Core.BusinessLogic.Expertises.ExpertiseManager(null);
                result.Value = manager.GetHeadingsWithSuppliers();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetHeadingsForDondil");
            }
            return result;
        }

        [WebMethod]
        public string SearchSiteSuppliersExpanded(SearchRequest Request)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(8, "SearchSiteSuppliersExpanded started - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SiteId"] == "np_israel" && Request.RegionLevel == 2)
                {
                    Request.RegionLevel = 3;
                }
                NoProblem.Core.BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.SearchSiteSuppliersExpanded(Request.SiteId, Request.ExpertiseCode, Request.ExpertiseLevel, Request.RegionCode, Request.RegionLevel, Request.OriginId, Request.MaxResults, Request.UpsaleId, Request.PageNumber, Request.PageSize, false, false, false, false);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SearchSiteSuppliersExpanded Failed with exception");
                retVal = "<Suppliers><Error>Failed</Error></Suppliers>";
            }
            LogUtils.MyHandle.WriteToLog(9, "_SearchSiteSuppliersExpanded ended - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            return retVal;
        }

        [WebMethod]
        public string SearchSiteSuppliersExpandedForDondil(SearchRequest request)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(8, "SearchSiteSuppliersExpandedForDondil started - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               request.ExpertiseCode, request.ExpertiseLevel, request.MaxResults, request.RegionCode, request.RegionLevel, request.SiteId, request.OriginId, request.UpsaleId);
            try
            {
                NoProblem.Core.BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.SearchSiteSuppliersExpanded(request.SiteId, request.ExpertiseCode, request.ExpertiseLevel, request.RegionCode, request.RegionLevel, request.OriginId, request.MaxResults, request.UpsaleId, request.PageNumber, request.PageSize, true, false, true, true);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SearchSiteSuppliersExpandedForDondil Failed with exception");
                retVal = "<Suppliers><Error>Failed</Error></Suppliers>";
            }
            LogUtils.MyHandle.WriteToLog(9, "SearchSiteSuppliersExpandedForDondil ended - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               request.ExpertiseCode, request.ExpertiseLevel, request.MaxResults, request.RegionCode, request.RegionLevel, request.SiteId, request.OriginId, request.UpsaleId);
            return retVal;
        }

        [WebMethod]
        public string SearchSiteSuppliersWithExposure(SearchRequestWithExposure Request)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(8, "SearchSiteSuppliersWithExposure started - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SiteId"] == "np_israel" && Request.RegionLevel == 2)
                {
                    Request.RegionLevel = 3;
                }
                NoProblem.Core.BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                System.Xml.Linq.XElement doc = supplierLogic.SearchSiteSuppliersWithExposure(Request.SiteId, Request.ExpertiseCode, Request.ExpertiseLevel, Request.RegionCode, Request.RegionLevel, Request.OriginId, Request.MaxResults, Request.UpsaleId, Request.PageNumber, Request.PageSize, false, false,
                    Request.ControlName, Request.Domain, Request.PageName, Request.PlaceInWebSite, Request.SessionId, Request.Url);
                retVal = doc.ToString();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SearchSiteSuppliersWithExposure Failed with exception");
                retVal = "<Suppliers><Error>Failed</Error></Suppliers>";
            }
            LogUtils.MyHandle.WriteToLog(9, "_SearchSiteSuppliersWithExposure ended - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            return retVal;
        }

        [WebMethod]
        public string SearchSiteSuppliersExpandedWithExposureWithZipcodeUSA(SearchRequestWithExposure Request)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(8, "SearchSiteSuppliersExpandedWitExposure started - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            try
            {
                NoProblem.Core.BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.SearchSiteSuppliersExpandedWithExposureWithZipcodeUSA(Request.SiteId, Request.ExpertiseCode, Request.ExpertiseLevel, Request.RegionCode, Request.RegionLevel, Request.OriginId, Request.MaxResults, Request.UpsaleId, Request.PageNumber, Request.PageSize, false, false,
                    Request.ControlName, Request.Domain, Request.PageName, Request.PlaceInWebSite, Request.SessionId, Request.Url);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SearchSiteSuppliersExpandedWitExposure Failed with exception");
                retVal = "<Suppliers><Error>Failed</Error></Suppliers>";
            }
            LogUtils.MyHandle.WriteToLog(9, "_SearchSiteSuppliersExpandedWitExposure ended - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            return retVal;
        }

        [WebMethod]
        public string SearchSiteSuppliersExpandedWithExposure(SearchRequestWithExposure Request)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(8, "SearchSiteSuppliersExpandedWitExposure started - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SiteId"] == "np_israel" && Request.RegionLevel == 2)
                {
                    Request.RegionLevel = 3;
                }
                NoProblem.Core.BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.SearchSiteSuppliersExpandedWithExposure(Request.SiteId, Request.ExpertiseCode, Request.ExpertiseLevel, Request.RegionCode, Request.RegionLevel, Request.OriginId, Request.MaxResults, Request.UpsaleId, Request.PageNumber, Request.PageSize, false, false,
                    Request.ControlName, Request.Domain, Request.PageName, Request.PlaceInWebSite, Request.SessionId, Request.Url);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SearchSiteSuppliersExpandedWitExposure Failed with exception");
                retVal = "<Suppliers><Error>Failed</Error></Suppliers>";
            }
            LogUtils.MyHandle.WriteToLog(9, "_SearchSiteSuppliersExpandedWitExposure ended - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            return retVal;
        }

        [WebMethod]
        public string SearchAvailableAndUnavailableSuppliers(SearchRequestWithExposure Request)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(8, "SearchAvailableandUnalvailablesuppliers started - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["SiteId"] == "np_israel" && Request.RegionLevel == 2)
                {
                    Request.RegionLevel = 3;
                }
                NoProblem.Core.BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                string suppliers = supplierLogic.SearchSiteSuppliersExpandedWithExposure(Request.SiteId, Request.ExpertiseCode, Request.ExpertiseLevel, Request.RegionCode, Request.RegionLevel, Request.OriginId, Request.MaxResults, Request.UpsaleId, Request.PageNumber, Request.PageSize, true, true,
                    Request.ControlName, Request.Domain, Request.PageName, Request.PlaceInWebSite, Request.SessionId, Request.Url);
                return suppliers;
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SearchAvailableandUnalvailablesuppliers Failed with exception");
                retVal = "<Suppliers><Error>Failed</Error></Suppliers>";
            }
            LogUtils.MyHandle.WriteToLog(9, "SearchAvailableandUnalvailablesuppliers ended - ExpertiseCode: {0}, ExpertiseLevel: {1}, MaxResults: {2}, RegionCode: {3}, RegionLevel: {4}, SiteId: {5}, OriginId: {6}, IncidentId: {7}",
               Request.ExpertiseCode, Request.ExpertiseLevel, Request.MaxResults, Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.OriginId, Request.UpsaleId);
            return retVal;
        }

        [WebMethod]
        public Result<List<DataModel.SupplierModel.ExpertiseData>> GetSuppliersCount(string expertiseName, string areaCode, int areaLevel)
        {
            Result<List<DataModel.SupplierModel.ExpertiseData>> result = new Result<List<NoProblem.Core.DataModel.SupplierModel.ExpertiseData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSuppliersCount started");
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                result.Value = supplierLogic.GetSupplierCount(expertiseName, areaCode, areaLevel);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetSuppliersCount failed with exception.");
            }
            LogUtils.MyHandle.WriteToLog(9, "_GetSuppliersCount ended");
            return result;
        }

        [WebMethod]
        public string GetSuppliersStatus(string incidentId)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(9, "GetSuppliersStatus started - incidentId: {0}", incidentId);
            if (String.IsNullOrEmpty(incidentId))
            {
                LogUtils.MyHandle.WriteToLog(10, "GetSuppliersStatus - incidentId is null or empty");
                return "<Result><GetSuppliersStatus><Error>Failed</Error></GetSuppliersStatus></Result>";
            }
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                retVal = publisherUtils.GetSuppliersStatus(incidentId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSuppliersStatus Failed with Exception");
                retVal = "<Result><GetSuppliersStatus><Error>Failed</Error></GetSuppliersStatus></Result>";
            }
            LogUtils.MyHandle.WriteToLog(9, "_GetSuppliersStatus ended - Response: {0}", retVal);
            return retVal;
        }

        [WebMethod]
        public ServiceResponse CreateServiceRequestWithZipcodeUSA(ServiceRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, @"CreateServiceRequest started - ContactFullName: {0}, ContactPhoneNumber: {1}, 
           ExpertiseCode: {2}, ExpertiseType: {3}, NumOfSuppliers: {4}, OriginId: {5}, PrefferedCallTime: {6}, 
           RequestDescription: {7}, ServiceAreaCode: {8}, ServiceAreaType: {9}, SiteId: {10}, UpsaleId: {11}",
               request.ContactFullName, request.ContactPhoneNumber, request.ExpertiseCode, request.ExpertiseType,
               request.NumOfSuppliers, request.OriginId, request.PrefferedCallTime, request.RequestDescription,
               request.RegionCode, request.RegionLevel, request.SiteId, request.UpsaleId);
            ServiceResponse response = null;
            try
            {
                BusinessLogic.ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                response = manager.CreateServiceWithZipcodeUSA(request) as ServiceResponse;
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, @"CreateServiceRequest failed with exception.
                    - ContactFullName: {0}, ContactPhoneNumber: {1}, 
           ExpertiseCode: {2}, ExpertiseType: {3}, NumOfSuppliers: {4}, OriginId: {5}, PrefferedCallTime: {6}, 
           RequestDescription: {7}, ServiceAreaCode: {8}, ServiceAreaType: {9}, SiteId: {10}, UpsaleId: {11}",
               request.ContactFullName, request.ContactPhoneNumber, request.ExpertiseCode, request.ExpertiseType,
               request.NumOfSuppliers, request.OriginId, request.PrefferedCallTime, request.RequestDescription,
               request.RegionCode, request.RegionLevel, request.SiteId, request.UpsaleId);
                response = new ServiceResponse();
                response.Status = StatusCode.UnexpectedError;
                response.Message = "Unexpected Exception Ocurred.";
            }
            LogUtils.MyHandle.WriteToLog(9, @"_CreateServiceRequest ended - ContactFullName: {0}, ContactPhoneNumber: {1}, 
           ExpertiseCode: {2}, ExpertiseType: {3}, NumOfSuppliers: {4}, OriginId: {5}, PrefferedCallTime: {6}, 
           RequestDescription: {7}, ServiceAreaCode: {8}, ServiceAreaType: {9}, SiteId: {10}, UpsaleId: {11}",
               request.ContactFullName, request.ContactPhoneNumber, request.ExpertiseCode, request.ExpertiseType,
               request.NumOfSuppliers, request.OriginId, request.PrefferedCallTime, request.RequestDescription,
               request.RegionCode, request.RegionLevel, request.SiteId, request.UpsaleId);
            return response;
        }

        [WebMethod]
        public ServiceResponse CreateServiceRequest(ServiceRequest Request)
        {
            LogUtils.MyHandle.WriteToLog(8, @"CreateServiceRequest started - ContactFullName: {0}, ContactPhoneNumber: {1}, 
           ExpertiseCode: {2}, ExpertiseType: {3}, NumOfSuppliers: {4}, OriginId: {5}, PrefferedCallTime: {6}, 
           RequestDescription: {7}, ServiceAreaCode: {8}, ServiceAreaType: {9}, SiteId: {10}, UpsaleId: {11}, UpsaleRequestId: {12}",
               Request.ContactFullName, Request.ContactPhoneNumber, Request.ExpertiseCode, Request.ExpertiseType,
               Request.NumOfSuppliers, Request.OriginId, Request.PrefferedCallTime, Request.RequestDescription,
               Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.UpsaleId, Request.UpsaleRequestId);
            ServiceResponse response = null;
            try
            {
                BusinessLogic.ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                response = manager.CreateService(Request) as ServiceResponse;
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, @"CreateServiceRequest failed with exception.
                    - ContactFullName: {0}, ContactPhoneNumber: {1}, 
           ExpertiseCode: {2}, ExpertiseType: {3}, NumOfSuppliers: {4}, OriginId: {5}, PrefferedCallTime: {6}, 
           RequestDescription: {7}, ServiceAreaCode: {8}, ServiceAreaType: {9}, SiteId: {10}, UpsaleId: {11}",
               Request.ContactFullName, Request.ContactPhoneNumber, Request.ExpertiseCode, Request.ExpertiseType,
               Request.NumOfSuppliers, Request.OriginId, Request.PrefferedCallTime, Request.RequestDescription,
               Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.UpsaleId);
                response = new ServiceResponse();
                response.Status = StatusCode.UnexpectedError;
                response.Message = "Unexpected Exception Ocurred.";
            }
            LogUtils.MyHandle.WriteToLog(9, @"_CreateServiceRequest ended - ContactFullName: {0}, ContactPhoneNumber: {1}, 
           ExpertiseCode: {2}, ExpertiseType: {3}, NumOfSuppliers: {4}, OriginId: {5}, PrefferedCallTime: {6}, 
           RequestDescription: {7}, ServiceAreaCode: {8}, ServiceAreaType: {9}, SiteId: {10}, UpsaleId: {11}",
               Request.ContactFullName, Request.ContactPhoneNumber, Request.ExpertiseCode, Request.ExpertiseType,
               Request.NumOfSuppliers, Request.OriginId, Request.PrefferedCallTime, Request.RequestDescription,
               Request.RegionCode, Request.RegionLevel, Request.SiteId, Request.UpsaleId);
            return response;
        }

        [WebMethod]
        public Result<SearchConsumersResponse> SearchConsumers(SearchConsumersRequest request)
        {
            Result<SearchConsumersResponse> result = new Result<SearchConsumersResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SearchConsumers started. Request: SearchParameter = {0}", request.SearchParameter);
                ConsumersManager manager = new ConsumersManager(null);
                SearchConsumersResponse response = manager.SearchConsumers(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SearchConsumers failed with exception. SearchParameter = {0}", request.SearchParameter);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Reports.PagedList<ConsumerMinimalData>> SearchConsumersPaged(SearchConsumersRequestPaged request)
        {
            Result<DataModel.Reports.PagedList<ConsumerMinimalData>> result = new Result<DataModel.Reports.PagedList<ConsumerMinimalData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SearchConsumers started. Request: SearchParameter = {0}", request.SearchParameter);
                ConsumersManager manager = new ConsumersManager(null);
                SearchConsumersResponse response = manager.SearchConsumers(request);
                DataModel.Reports.PagedList<ConsumerMinimalData> pagesList =
                    new NoProblem.Core.DataModel.Reports.PagedList<ConsumerMinimalData>(response.Consumers, request.PageSize, request.CurrentPage);
                result.Value = pagesList;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SearchConsumers failed with exception. SearchParameter = {0}", request.SearchParameter);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<GetConsumerInfoResponse> GetConsumerInfo(GetConsumerInfoRequest request)
        {
            Result<GetConsumerInfoResponse> result = new Result<GetConsumerInfoResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetConsumerInfo started. Request: ContactId = {0}", request.ContactId.ToString());
                ConsumersManager manager = new ConsumersManager(null);
                GetConsumerInfoResponse response = manager.GetConsumerInfo(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetConsumerInfo failed with exception. ContactId = {0}", request.ContactId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result UpdateConsumerInfo(UpdateConsumerInfoRequest request)
        {
            Result result = new Result();
            try
            {
                ConsumersManager manager = new ConsumersManager(null);
                manager.UpdateConsumerInfo(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpdateConsumerInfo failed with exception. ContactId = {0}", request.ContactId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }



        [WebMethod]
        public Result<GetConsumerIncidentsResponse> GetCustomerIncidents(GetCustomerLeadsRequest request)
        {
            Result<GetConsumerIncidentsResponse> result = new Result<GetConsumerIncidentsResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetConsumerIncidents started. request: ContactId = {0}", request.CustomerId.ToString());
                ConsumersManager manager = new ConsumersManager(null);
                GetConsumerIncidentsResponse response = manager.GetConsumerIncidents(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetConsumerIncidents failed with exception. request: ContactId = {0}", request.CustomerId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<GetCustomerLeadResponse> GetCustomerLead(GetCustomerLeadRequest request)
        {
            Result<GetCustomerLeadResponse> result = new Result<GetCustomerLeadResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetConsumerIncidents started. request: ContactId = {0}", request.CustomerId.ToString());
                ConsumersManager manager = new ConsumersManager(null);
                ConsumerIncidentData lead = manager.GetCustomerLead(request.CustomerId, request.LeadId);
                var response = new GetCustomerLeadResponse
                {
                    Lead = lead
                };
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetConsumerIncidents failed with exception. request: ContactId = {0}", request.CustomerId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }



        [WebMethod]
        public Result<RelaunchLeadResponse> RelaunchIncident(RelaunchLeadRequest request)
        {
            Result<RelaunchLeadResponse> result = new Result<RelaunchLeadResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "RelaunchIncident started. request: lead id = {0}", request.LeadId.ToString());
                ConsumersManager manager = new ConsumersManager(null);
                RelaunchLeadResponse response = manager.RelaunchLead(request, BusinessLogic.ServiceRequest.VideoRequests.TypeExecuteUser.Customer);                
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "RelaunchIncident failed with exception. request: ContactId = {0}", request.LeadId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }
        [WebMethod]
        public Result<RelaunchLeadResponse> RelaunchIncidentByPublisher(Guid incidentId)
        {
            Result<RelaunchLeadResponse> result = new Result<RelaunchLeadResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ReviveIncident started. request: lead id = {0}", incidentId.ToString());
                ConsumersManager manager = new ConsumersManager(null);
                RelaunchLeadResponse response = manager.RelaunchLead(incidentId, BusinessLogic.ServiceRequest.VideoRequests.TypeExecuteUser.Publisher);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "RelaunchIncidentByPublisher failed with exception. request: ContactId = {0}", incidentId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

 
        [WebMethod]
        public Result<GetConsumerIncidentDataResponse> GetConsumerIncidentData(GetConsumerIncidentDataRequest request)
        {
            Result<GetConsumerIncidentDataResponse> result = new Result<GetConsumerIncidentDataResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetConsumerIncidentData started. request: IncidentId = {0}", request.LeadId.ToString());
                ConsumersManager manager = new ConsumersManager(null);
                GetConsumerIncidentDataResponse response = manager.GetConsumerIncidentData(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetConsumerIncidentData failed with exception. request: IncidentId = {0}", request.LeadId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }     

        [WebMethod]
        public Result<GetConsumerNotesResponse> GetConsumerNotes(GetConsumerNotesRequest request)
        {
            Result<GetConsumerNotesResponse> result = new Result<GetConsumerNotesResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetConsumerNotes started. request: ContactId = {0}", request.ContactId.ToString());
                ConsumersManager manager = new ConsumersManager(null);
                GetConsumerNotesResponse response = manager.GetConsumerNotes(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetConsumerNotes failed with exception. request: ContactId = {0}", request.ContactId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result CreateConsumerNote(CreateConsumerNoteRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetConsumerNotes started. request: ContactId = {0}, UserId = {1}, Description = {2}", request.ContactId.ToString(), request.UserId.ToString(), request.Description);
                ConsumersManager manager = new ConsumersManager(null);
                manager.CreateConsumerNote(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetConsumerNotes failed with exception. request: ContactId = {0}, UserId = {1}, Description = {2}", request.ContactId.ToString(), request.UserId.ToString(), request.Description);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<System.Collections.Generic.List<DataModel.Regions.RegionCodeNamePair>> GetRegionsByParentCode(DataModel.Regions.Requests.GetRegionsByParentCodeRequest request)
        {
            Result<System.Collections.Generic.List<DataModel.Regions.RegionCodeNamePair>> result = new Result<System.Collections.Generic.List<DataModel.Regions.RegionCodeNamePair>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetRegionsByParentCode started. request: ParentCode = {0}", request.ParentCode);
                BusinessLogic.Regions.RegionsManager manager = new BusinessLogic.Regions.RegionsManager(null);
                result.Value = manager.GetRegionsByParentCode(request.ParentCode);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetRegionsByParentCode failed with exception. request: ParentCode = {0}", request.ParentCode);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Reviews.InsertReviewResponse> InsertReview(DataModel.Reviews.InsertReviewRequest request)
        {
            Result<DataModel.Reviews.InsertReviewResponse> result = new Result<NoProblem.Core.DataModel.Reviews.InsertReviewResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "InsertReview started. request: Name = {0}, Phone = {1}, SupplierId = {2}, IsLike = {3}, Description = {4}",
                    request.Name, request.Phone, request.SupplierId.ToString(), request.IsLike.ToString(), request.Description);
                BusinessLogic.Reviews.ReviewsManager manager = new NoProblem.Core.BusinessLogic.Reviews.ReviewsManager(null);
                var insertionStatus = manager.InsertReview(request.Description, request.Phone, request.Name, request.IsLike, request.SupplierId);
                result.Value = new NoProblem.Core.DataModel.Reviews.InsertReviewResponse()
                {
                    InsertionStatus = insertionStatus
                };
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "InsertReview failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Xrm.new_channel.ChannelCode> GetChannelForHeading(string headingCode, int headingLevel)
        {
            Result<DataModel.Xrm.new_channel.ChannelCode> result = new Result<NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode>();
            result.Value = NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Call;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetChannelForHeading started. HeadingCode = {0}, HeadingLevel = {1}", headingCode, headingLevel);
                BusinessLogic.Expertises.ExpertiseManager manager = new BusinessLogic.Expertises.ExpertiseManager(null);
                result.Value = manager.GetChannelForHeading(headingCode, headingLevel);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetChannelForHeading failed with exception. HeadingCode = {0}, HeadingLevel = {1}", headingCode, headingLevel);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<bool> IsHeadingForAuction(string headingCode, int headingLevel)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetServiceTypeForHeading started. HeadingCode = {0}, HeadingLevel = {1}", headingCode, headingLevel);
                BusinessLogic.Expertises.ExpertiseManager manager = new BusinessLogic.Expertises.ExpertiseManager(null);
                result.Value = manager.IsHeadingForAuction(headingCode, headingLevel);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetServiceTypeForHeading failed with exception. HeadingCode = {0}, HeadingLevel = {1}", headingCode, headingLevel);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.DynamicDIDs.AllocationResponse> AllocateDID(DataModel.DynamicDIDs.AllocationRequest request)
        {
            Result<DataModel.DynamicDIDs.AllocationResponse> result = new Result<NoProblem.Core.DataModel.DynamicDIDs.AllocationResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AllocateDID started. request: HeadingCode = {0}, OriginId = {1}, RealPhone = {2}, RegionCode = {3}, Url = {4}, WebSite = {5}"
                    , request.HeadingCode, request.OriginId, request.RealPhone, request.RegionCode, request.Url, request.WebSite);
                BusinessLogic.DynamicDIDs.AllocationManager manager = new NoProblem.Core.BusinessLogic.DynamicDIDs.AllocationManager(null);
                result.Value = manager.Allocate(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AllocateDID. RealPhone:{0}, OriginId:{1}, HeadingCode:{2}, RegionCode:{3}, Url:{4}, WebSite:{5}",
                    request.RealPhone, request.OriginId, request.HeadingCode, request.RegionCode, request.Url, request.WebSite);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.DynamicDIDs.AllocationResponse>> AllocateMultipleDIDs(List<DataModel.DynamicDIDs.AllocationRequest> request)
        {
            Result<List<DataModel.DynamicDIDs.AllocationResponse>> result = new Result<List<DataModel.DynamicDIDs.AllocationResponse>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AllocateMultipleDIDs started");
                BusinessLogic.DynamicDIDs.AllocationManager manager = new NoProblem.Core.BusinessLogic.DynamicDIDs.AllocationManager(null);
                result.Value = manager.AllocateMultiple(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AllocateMultipleDIDs");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<string> GetIvrWidgetNumber(Guid originId, string headingCode)
        {
            Result<string> result = new Result<string>();
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "GetIvrWidgetNumber started. originId={0}, headingCode={1}", originId, headingCode);
                BusinessLogic.IvrWidget.IvrWidgetNumberRetriever retriever = new NoProblem.Core.BusinessLogic.IvrWidget.IvrWidgetNumberRetriever(null);
                result.Value = retriever.GetIvrWidgetNumber(originId, headingCode);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetIvrWidgetNumber. originId = {0}. headingCode = {1}", originId, headingCode);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<int> GetTotalCaseCount()
        {
            Result<int> result = new Result<int>();
            try
            {
                LogUtils.MyHandle.WriteToLog(10, "GetTotalCaseCount started");
                BusinessLogic.CustomerPageDataManager manager = new NoProblem.Core.BusinessLogic.CustomerPageDataManager();
                result.Value = manager.GetTotalCaseCount();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetTotalCaseCount");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Case.CustomerPageCaseData>> GetLastThreeCases(Guid headingId)
        {
            Result<List<DataModel.Case.CustomerPageCaseData>> result = new Result<List<DataModel.Case.CustomerPageCaseData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(10, "GetLastThreeCases started");
                BusinessLogic.CustomerPageDataManager manager = new NoProblem.Core.BusinessLogic.CustomerPageDataManager();
                result.Value = manager.GetLastThreeCases(headingId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetLastThreeCases");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<bool> CheckUsaZipCode(string zipCode)
        {
            Result<bool> result = new Result<bool>();
            LogUtils.MyHandle.WriteToLog(8, "CheckUsaZipCode started");
            try
            {
                BusinessLogic.CustomerPageDataManager manager = new NoProblem.Core.BusinessLogic.CustomerPageDataManager();
                result.Value = manager.CheckUsaZipCode(zipCode);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckUsaZipCode");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result LogFlavorUsability(NoProblem.Core.DataModel.Audit.LogFlavorUsabilityRequest request)
        {
            Result result = new Result();
            LogUtils.MyHandle.WriteToLog(8, "LogFlavorUsability started");
            try
            {
                BusinessLogic.Audit.AuditManager manager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                manager.LogFlavorUsability(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in LogFlavorUsability");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<string> PerionGetDID(string perionSessionId, Guid ExpertiseId)
        {
            Result<string> result = new Result<string>();
            try
            {
                BusinessLogic.Dialer.PerionDDID.PerionDDIDManager m = new BusinessLogic.Dialer.PerionDDID.PerionDDIDManager();
                result.Value = m.GetNumber(perionSessionId, ExpertiseId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in perionSessionId. sessionid = {0}", perionSessionId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<CloseLeadResponse> CloseLeadRequest(CloseLeadRequest request)
        {
            var result = new Result<CloseLeadResponse>();
            try
            {
                ConsumersManager manager = new ConsumersManager(null);
                CloseLeadResponse response = manager.CloseLead(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CancelFutureCalls. leadId = {0}", request.LeadId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<GetCustomerAdvertiserAboutResponse> GetCustomerAdvertiserAbout(GetCustomerAdvertiserAboutRequest request)
        {
            var result = new Result<GetCustomerAdvertiserAboutResponse>();
            try
            {
                ConsumersManager manager = new ConsumersManager(null);
                GetCustomerAdvertiserAboutResponse response = manager.GetCustomerAdvertiserAbout(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCustomerAdvertiser. customerId = {0}", request.CustomerId);
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<GetAdvertiserCallHistoryResponse> GetAdvertiserCallHistory(GetAdvertiserCallHistoryRequest request)
        {
            var result = new Result<GetAdvertiserCallHistoryResponse>();
            try
            {
                ConsumersManager manager = new ConsumersManager(null);
                GetAdvertiserCallHistoryResponse response = manager.GetAdvertiserCallHistory(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAdvertiserCallHistory. customerId = {0}", request.CustomerId);
                AddFailureToResult(result, exc);
            }
            return result;
        }
        [WebMethod]
        public Result AddLove(NoProblem.Core.DataModel.Consumer.LoveRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.CustomerBL.LoveManager manager = new BusinessLogic.CustomerBL.LoveManager(null);
                manager.AddLove(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in AddLove with AccountId: '{0}', ContactId: '{1}'", request.SupplierId, request.ContactId);
            }
            return result;
        }

        [WebMethod]
        public Result UpdateCustomerProfile(UpdateCustomerProfileRequest request)
        {
            Result result = new Result();
            try
            {
                ConsumersManager manager = new ConsumersManager(null);
                manager.UpdateCustomerProfile(request);
                result.Type = Result.eResultType.Success;                
                return result;
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateCustomerProfile with customerId: '{0}'", request.CustomerId);
            }
            return result;
        }

        [WebMethod]
        public Result<CustomerProfileData> GetCustomerProfile(Guid customerId)
        {
            var result = new Result<CustomerProfileData>();
            try
            {
                ConsumersManager manager = new ConsumersManager(null);
                CustomerProfileData profile = manager.GetCustomerProfile(customerId);
                result.Type = Result.eResultType.Success;
                result.Value = profile;
                return result;
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCustomerProfile with customerId: '{0}'", customerId);
            }
            return result;
        }

        [WebMethod]
        public Result UpdateCustomerSettings(UpdateCustomerSettingsRequest request)
        {
            Result result = new Result();
            try
            {
                ConsumersManager manager = new ConsumersManager(null);
                manager.UpdateCustomerSettings(request);
                result.Type = Result.eResultType.Success;
                return result;
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateCustomerProfile with customerId: '{0}'", request.CustomerId);
            }
            return result;
        }

        [WebMethod]
        public Result<CustomerSettingsData> GetCustomerSettings(Guid customerId)
        {
            var result = new Result<CustomerSettingsData>();
            try
            {
                ConsumersManager manager = new ConsumersManager(null);
                CustomerSettingsData settings = manager.GetCustomerSettings(customerId);
                result.Type = Result.eResultType.Success;
                result.Value = settings;
                return result;
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateCustomerProfile with customerId: '{0}'", customerId);
            }
            return result;
        }

        /*
        [WebMethod]
        public Result<NoProblem.Core.DataModel.Consumer.LoveResponse> GetLoves(NoProblem.Core.DataModel.Consumer.LoveRequest request)
        {
            Result<NoProblem.Core.DataModel.Consumer.LoveResponse> result = new Result<NoProblem.Core.DataModel.Consumer.LoveResponse>();
            try
            {
                BusinessLogic.CustomerBL.LoveManager manager = new BusinessLogic.CustomerBL.LoveManager(null);
                result.Value = manager.GetLoves(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetLoves with AccountId: '{0}', ContactId: '{1}'", request.SupplierId, request.ContactId);
            }
            return result;
        }
         * */
        [WebMethod]
        public Result<bool> ConnectAndRecord(Guid incidentAccountId, Guid CustomerId)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                var connectAndRecord = new BusinessLogic.APIs.MobileAPI.ConnectAndRecordCustomer(incidentAccountId, CustomerId);
                result.Value = connectAndRecord.InitiateCall();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in ConnectAndRecord(Customer)");
            }
            return result;
        }

        [WebMethod]
        public Result LogOut(Guid customerId, Guid deviceId)
        {
            Result result = new Result();
            try
            {
                var manager = new DevicesManager(null);
                manager.CustomerLogout(customerId, deviceId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Logout");
            }
            return result;
        }
        [WebMethod]
        public Result<string> UpdateMobileDevice(Guid customerId, DataModel.SupplierModel.Registration2014.Mobile.MobileLogOnData deviceData)
        {
            Result<string> result = new Result<string>();
            try
            {
                BusinessLogic.APIs.MobileAPI.Devices.DevicesManager deviceManager = new BusinessLogic.APIs.MobileAPI.Devices.DevicesManager(null);
                string token = deviceManager.DeviceLogin(customerId, deviceData.DeviceName, deviceData.DeviceUId, deviceData.DeviceOS, 
                    deviceData.DeviceOSVersion, deviceData.NpAppVersion, deviceData.isDebug);
                result.Value = token;
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateMobileDevice. customerId = {0}", customerId);
            }
            return result;
        }
        [WebMethod]
        public Result<List<ClipCall.DomainModel.Entities.VideoInvitation.Response.GetFavoriteServiceProviderResponse>> GetFavoriteServiceProvider(NoProblem.Core.DataModel.PublisherPortal.Consumers.Requests.GetFavoriteServiceProviderRequest request)
        {
            Result<List<ClipCall.DomainModel.Entities.VideoInvitation.Response.GetFavoriteServiceProviderResponse>> result =
                new Result<List<ClipCall.DomainModel.Entities.VideoInvitation.Response.GetFavoriteServiceProviderResponse>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetFavoriteServiceProvider started. request: ContactId = {0}, ExpertiseId = {1}", request.CustomerId, request.ExpertiseId);
                ConsumersManager manager = new ConsumersManager(null);
                List<ClipCall.DomainModel.Entities.VideoInvitation.Response.GetFavoriteServiceProviderResponse> favorites = manager.GetFavoriteServiceProvider(request.CustomerId, request.ExpertiseId);

                result.Value = favorites;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetFavoriteServiceProvider failed with exception. request: ContactId = {0}, ExpertiseId = {1}", request.CustomerId, request.ExpertiseId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }
       
    }
}
