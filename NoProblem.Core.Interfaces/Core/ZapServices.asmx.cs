﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Dapaz.DapazAccountsOnlineSync;
using Effect.Crm.Logs;
using System.Text;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for ZapServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ZapServices : NpServiceBase
    {
        [WebMethod]
        public Result ZapAccountOnlineSync(AccountForOnlineSync_Zap syncRequest)
        {
            Result result = new Result();
            string requestString = GetLogRequestString(syncRequest);
            try
            {
                LogUtils.MyHandle.WriteToLog(10, requestString);
                BusinessLogic.Dapaz.DapazAccountsOnlineSync.DapazAccountsOnlineSyncManager manager = new NoProblem.Core.BusinessLogic.Dapaz.DapazAccountsOnlineSync.DapazAccountsOnlineSyncManager(null);
                manager.HandleSyncRequest(syncRequest);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in ZapAccountOnlineSync.\n\r" + requestString);
            }
            return result;
        }

        private string GetLogRequestString(AccountForOnlineSync_Zap syncRequest)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("ZapAccountOnlineSync received:");
            builder.Append("CustomerNo = ").AppendLine(syncRequest.CustomerNo);
            builder.Append("AccountName = ").AppendLine(syncRequest.AccountName);
            builder.Append("IsActive = ").AppendLine(syncRequest.IsActive.ToString());
            builder.Append("BusinessId = ").AppendLine(syncRequest.BusinessId);
            builder.Append("AccountInvoiceName = ").AppendLine(syncRequest.AccountInvoiceName);
            builder.Append("AccountPrimaryCityName = ").AppendLine(syncRequest.AccountPrimaryCityName);
            builder.Append("AccountPrimaryStreetName = ").AppendLine(syncRequest.AccountPrimaryStreetName);
            builder.Append("AccountPrimaryHouseNum = ").AppendLine(syncRequest.AccountPrimaryHouseNum);
            builder.Append("AccountPrimaryZipcode = ").AppendLine(syncRequest.AccountPrimaryZipcode);
            builder.Append("Email = ").AppendLine(syncRequest.Email);
            builder.Append("AccountContactFirstName = ").AppendLine(syncRequest.AccountContactFirstName);
            builder.Append("AccountContactLastName = ").AppendLine(syncRequest.AccountContactLastName);
            builder.Append("AccountPrimaryLocal = ").AppendLine(syncRequest.AccountPrimaryLocal);
            builder.Append("PhoneNumberRelatedForSims = ").AppendLine(syncRequest.PhoneNumberRelatedForSims);
            builder.Append("PhoneNumberRelatedForSMS = ").AppendLine(syncRequest.PhoneNumberRelatedForSMS);
            builder.Append("AccountFaxNum = ").AppendLine(syncRequest.AccountFaxNum);
            builder.Append("AccountPhoneNum2 = ").AppendLine(syncRequest.AccountPhoneNum2);
            builder.Append("AccountInternetSite = ").AppendLine(syncRequest.AccountInternetSite);
            builder.Append("FinanceStatus = ").AppendLine(syncRequest.FinanceStatus);
            builder.Append("PrimaryLocalAreaCode = ").AppendLine(syncRequest.PrimaryLocalAreaCode);
            builder.Append("PrimaryHeadingCode = ").AppendLine(syncRequest.PrimaryHeadingCode);
            for (int i = 0; i < syncRequest.ListOfNoProblemKeys.Count; i++)
            {
                builder.Append("KeyForAccountOnlineSync_Zap ").Append(i.ToString()).AppendLine(":");
                builder.Append("\tAccountMedia = ").AppendLine(syncRequest.ListOfNoProblemKeys[i].AccountMedia);
                builder.Append("\tAccountSegment = ").AppendLine(syncRequest.ListOfNoProblemKeys[i].AccountSegment);
                builder.Append("\tAccountKeyStatus = ").AppendLine(syncRequest.ListOfNoProblemKeys[i].AccountKeyStatus);
                builder.AppendLine("\tAccountSubLocals:");
                foreach (var item in syncRequest.ListOfNoProblemKeys[i].AccountSubLocals)
                {
                    builder.Append("\t\tLocal = ").AppendLine(item);
                }
            }
            return builder.ToString();
        }
    }
}
