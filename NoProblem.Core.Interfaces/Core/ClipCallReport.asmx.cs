﻿using Effect.Crm.Logs;
using NoProblem.Core.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for ClipCallReport
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ClipCallReport : System.Web.Services.WebService
    {

        [WebMethod]
        public Result<NoProblem.Core.DataModel.ClipCall.Request.IncidentData>
            GetClipCallIncidentData(Guid incidentId)
        {
            Result<NoProblem.Core.DataModel.ClipCall.Request.IncidentData> result =
                 new Result<DataModel.ClipCall.Request.IncidentData>();
            try
            {
                NoProblem.Core.BusinessLogic.ClipCall.Leads.IncidentManager manager = new BusinessLogic.ClipCall.Leads.IncidentManager(incidentId);
                result.Value = manager.GetIncidentData();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetClipCallIncidentData: IncidentId:{0}", incidentId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.ClipCall.Request.CloseIncidentResponse> CloseClipCallIncident(Guid incidentId)
        {
            Result<NoProblem.Core.DataModel.ClipCall.Request.CloseIncidentResponse> result = new Result<DataModel.ClipCall.Request.CloseIncidentResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.PublisherPortal.PublisherPortalManager manager =
                    new BusinessLogic.PublisherPortal.PublisherPortalManager(null, null);
                result.Value = manager.CloseLead(incidentId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CloseClipCallIncident: IncidentId:{0}", incidentId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        
        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.ClipCall.Request.RelaunchSearchSuppliersResponse>> 
            SearchClipCallSuppliers(NoProblem.Core.DataModel.ClipCall.Request.RelaunchSearchSuppliersRequest request)
        {
            Result<List<NoProblem.Core.DataModel.ClipCall.Request.RelaunchSearchSuppliersResponse>> result =
                new Result<List<DataModel.ClipCall.Request.RelaunchSearchSuppliersResponse>>();
            try
            {
                NoProblem.Core.BusinessLogic.PublisherPortal.PublisherPortalManager manager =
                   new BusinessLogic.PublisherPortal.PublisherPortalManager(null, null);
                result.Value = manager.SearchClipCallSuppliers(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SearchClipCallSuppliers: IncidentId:{0}, ExpertiseId: {1}, RegionName: {2}, RegionLevel: {3}",
                    request.IncidentId, request.CategoryId, request.RegionName, request.RegionLevel);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }

            return result;
        }
        
            [WebMethod]
        public Result<NoProblem.Core.DataModel.ClipCall.Request.RelaunchClipCallIncidentResponse>
            RelaunchCustomIncident(NoProblem.Core.DataModel.ClipCall.Request.RelaunchClipCallIncidentRequest request)
        {
            Result<NoProblem.Core.DataModel.ClipCall.Request.RelaunchClipCallIncidentResponse> result =
                new Result<DataModel.ClipCall.Request.RelaunchClipCallIncidentResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.PublisherPortal.PublisherPortalManager manager =
                   new BusinessLogic.PublisherPortal.PublisherPortalManager(null, null);
                result.Value = manager.RelaunchClipCallIncident(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RelaunchCustomIncident: IncidentId:{0}, ExpertiseId: {1}, RegionName: {2}, Description: {3}",
                    request.IncidentId, request.expertiseId, request.regionName, request.desc);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }

            return result;
        }
            [WebMethod]
            public Result<bool>
                SendPushNotification(NoProblem.Core.BusinessLogic.OneCallUtilities.NotificationServiceManagerRequest pushRequest)
            {
                Result<bool> result =
                    new Result<bool>();
                try
                {
                    NoProblem.Core.BusinessLogic.OneCallUtilities.NotificationServiceManager manager =
                       new BusinessLogic.OneCallUtilities.NotificationServiceManager(pushRequest);
                    result.Value = manager.SendPushNotification();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in SendPushNotification: DeviceId:{0}, PushType: {1}, OS: {2}",
                        pushRequest.DeviceId, pushRequest.PushType.ToString(), pushRequest.OS);
                    result.Type = Result.eResultType.Failure;
                    result.Messages.Add(exc.Message);
                    result.Messages.Add(exc.StackTrace);
                }

                return result;
            }
         
    }
}
