﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CookComputing.XmlRpc;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Asterisk;

namespace NoProblem.Core.Interfaces.Core.Asterisk
{
    public class TtsStatusHandler : XmlRpcService
    {
        [XmlRpcMethod("tts.status")]
        public void TtsStatus(XmlRpcStruct myStruct)
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(8,
                      "TtsStatusHandler.TtsStatus started. internal_id: {0}, status: {1}, digit: {2}",
                      myStruct[TtsStatusContainer.PropertyNames.INTERNAL_ID], myStruct[TtsStatusContainer.PropertyNames.STATUS], myStruct[TtsStatusContainer.PropertyNames.DIGIT]);
                TtsStatusContainer statusContainer = CreateStatusContiner(myStruct);
                BusinessLogic.AsteriskBL.AsteriskHandler hander = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskHandler(null);
                hander.TtsStatus(statusContainer);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                      "Exception in TtsStatusHandler.TtsStatus. internal_id: {0}, status: {1}, digit: {2}",
                      myStruct[TtsStatusContainer.PropertyNames.INTERNAL_ID], myStruct[TtsStatusContainer.PropertyNames.STATUS], myStruct[TtsStatusContainer.PropertyNames.DIGIT]);
            }
        }

        private TtsStatusContainer CreateStatusContiner(XmlRpcStruct myStruct)
        {
            TtsStatusContainer statusContainer = new TtsStatusContainer();
            statusContainer.Digit = (string)myStruct[TtsStatusContainer.PropertyNames.DIGIT];
            statusContainer.Internal_Id = (string)myStruct[TtsStatusContainer.PropertyNames.INTERNAL_ID];
            statusContainer.Status = (string)myStruct[TtsStatusContainer.PropertyNames.STATUS];
            return statusContainer;
        }
    }
}
