﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Asterisk;
using System.Globalization;

namespace NoProblem.Core.Interfaces.Core.Asterisk
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class NonPpcC2cStatusHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "NonPpcC2cStatusHandler.ProcessRequest started. QueryString: {0}", PrintQueryString(context.Request.QueryString));
                C2cStatusContainer statusContainer = CreateStatusContainer(context);
                BusinessLogic.AsteriskBL.AsteriskHandler handler = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskHandler(null);
                handler.NonPpcC2cStatus(statusContainer);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in NonPpcC2cStatusHandler.ProcessRequest, QueryString: {0}", PrintQueryString(context.Request.QueryString));
            }
        }

        private C2cStatusContainer CreateStatusContainer(HttpContext context)
        {
            C2cStatusContainer statusContainer = new C2cStatusContainer();
            var queryString = context.Request.QueryString;
            statusContainer.Session = queryString[C2cStatusContainer.PropertyNames.SESSION];
            statusContainer.Side = queryString[C2cStatusContainer.PropertyNames.SIDE];
            statusContainer.Status = queryString[C2cStatusContainer.PropertyNames.STATUS];
            string timeStr = queryString[C2cStatusContainer.PropertyNames.TIME];
            statusContainer.Time = DateTime.ParseExact(timeStr, C2cStatusContainer.TIME_FORMAT, CultureInfo.InvariantCulture);
            statusContainer.Type = queryString[C2cStatusContainer.PropertyNames.TYPE];
            return statusContainer;
        }

        private string PrintQueryString(System.Collections.Specialized.NameValueCollection nameValueCollection)
        {
            System.Text.StringBuilder b = new System.Text.StringBuilder();
            foreach (String key in nameValueCollection)
            {
                b.Append(key + ": " + nameValueCollection[key] + ", ");
            }
            return b.ToString();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
