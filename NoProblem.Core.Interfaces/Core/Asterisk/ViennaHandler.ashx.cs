﻿using System;
using CookComputing.XmlRpc;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Asterisk;

namespace NoProblem.Core.Interfaces.Core.Asterisk
{
    public class ViennaHandler : XmlRpcService
    {
        [XmlRpcMethod("CallFW.vn_query")]
        public XmlRpcStruct VnQuery(XmlRpcStruct request)
        {
            XmlRpcStruct retVal = null;
            try
            {
                VnQueryRequest container = CreateVnQueryRequest(request);
                LogUtils.MyHandle.WriteToLog(8,
                      "ViennaHandler.VnQuery started. ivr_session: {0}, ivr_callerid: {1}, ivr_inbound: {2}, source_url: {3}",
                      container.IvrSession,
                      container.IvrCallerId,
                      container.IvrInbound,
                      container.SourceUrl);
                BusinessLogic.AsteriskBL.AsteriskHandler hander = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskHandler(null);
                VnQueryResponse response = hander.VnQuery(container);
                retVal = ConvertVnQueryResponseToXmlRpc(response);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                      "Exception in ViennaHandler.VnQuery. ivr_session: {0}, ivr_callerid: {1}, ivr_inbound: {2}, source_url: {3}",
                      request[VnQueryRequest.PropertyNames.IVR_SESSION],
                      request[VnQueryRequest.PropertyNames.IVR_CALLERID],
                      request[VnQueryRequest.PropertyNames.IVR_INBOUND],
                      request[VnQueryRequest.PropertyNames.SOURCE_URL]);
            }
            if (retVal == null)
                retVal = new XmlRpcStruct();
            return retVal;
        }        

        [XmlRpcMethod("CallFW.vn_status")]
        public void VnStatus(XmlRpcStruct request)
        {
            try
            {
                VnStatusRequest container = CreateVnStatusRequest(request);
                LogUtils.MyHandle.WriteToLog(8,
                      "ViennaHandler.VnStatus started. ivr_session: {0}, ivr_status: {1}, digit: {2}, isWaitForDigit: {3}",
                      container.IvrSession,
                      container.IvrStatus,
                      container.Digit,
                      container.IsWaitForDigit);
                BusinessLogic.AsteriskBL.AsteriskHandler hander = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskHandler(null);
                hander.VnStatus(container);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                      "Exception in ViennaHandler.VnStatus. ivr_session: {0}, ivr_status: {1}, digit: {2}, isWaitForDigit: {3}",
                      request[VnStatusRequest.PropertyNames.IVR_SESSION],
                      request[VnStatusRequest.PropertyNames.IVR_STATUS],
                      request[VnStatusRequest.PropertyNames.DIGIT],
                      request[VnStatusRequest.PropertyNames.IS_WAIT_FOR_DIGIT]);
            }
        }

        private XmlRpcStruct ConvertVnQueryResponseToXmlRpc(VnQueryResponse response)
        {
            CookComputing.XmlRpc.XmlRpcStruct retVal = new CookComputing.XmlRpc.XmlRpcStruct();
            retVal.Add(VnQueryResponse.PropertyNames.ERRORCODE, response.ErrorCode);
            retVal.Add(VnQueryResponse.PropertyNames.IVR_TRUNK, response.IvrTrunk);
            retVal.Add(VnQueryResponse.PropertyNames.IVR_DISPLAY_CLID, response.IvrDisplayClid);
            retVal.Add(VnQueryResponse.PropertyNames.IVR_CALLER_ANNOUNCE, response.IvrCallerAnnounce);
            retVal.Add(VnQueryResponse.PropertyNames.IVR_ADVERTISER_ANNOUNCE, response.IvrAdvertiserAnnounce);
            retVal.Add(VnQueryResponse.PropertyNames.IVR_FIND_ALTERNATIVE_ADVERTISER, response.IvrFindAlternativeAdvertiser);
            retVal.Add(VnQueryResponse.PropertyNames.IVR_NO_ALTERNATIVE_FOUND, response.IvrNoAlternativeFound);
            retVal.Add(VnQueryResponse.PropertyNames.IVR_TECH, response.IvrTech);
            retVal.Add(VnQueryResponse.PropertyNames.INC2_SESSION, response.Inc2Session);
            retVal.Add(VnQueryResponse.PropertyNames.IVR_TARGET, response.IvrTarget);
            retVal.Add(VnQueryResponse.PropertyNames.ERRORMSG, response.ErrorMsg);
            retVal.Add(VnQueryResponse.PropertyNames.RECORDING, response.Recording);
            retVal.Add(VnQueryResponse.PropertyNames.RETURNADDRESS, response.ReturnAddress);
            retVal.Add(VnQueryResponse.PropertyNames.IS_WAIT_FOR_DIGIT, response.IsWaitForDigit);
            return retVal;
        }

        private VnStatusRequest CreateVnStatusRequest(XmlRpcStruct myStruct)
        {
            VnStatusRequest retVal = new VnStatusRequest();
            retVal.IvrSession = (string)myStruct[VnStatusRequest.PropertyNames.IVR_SESSION];
            retVal.IvrStatus = (string)myStruct[VnStatusRequest.PropertyNames.IVR_STATUS];
            retVal.Digit = (string)myStruct[VnStatusRequest.PropertyNames.DIGIT];
            retVal.IsWaitForDigit = (string)myStruct[VnStatusRequest.PropertyNames.IS_WAIT_FOR_DIGIT];
            return retVal;
        }

        private VnQueryRequest CreateVnQueryRequest(XmlRpcStruct myStruct)
        {
            VnQueryRequest retVal = new VnQueryRequest();
            retVal.IvrSession = (string)myStruct[VnQueryRequest.PropertyNames.IVR_SESSION];
            retVal.IvrCallerId = (string)myStruct[VnQueryRequest.PropertyNames.IVR_CALLERID];
            retVal.IvrInbound = (string)myStruct[VnQueryRequest.PropertyNames.IVR_INBOUND];
            retVal.SourceUrl = (string)myStruct[VnQueryRequest.PropertyNames.SOURCE_URL];
            return retVal;
        }
    }
}
