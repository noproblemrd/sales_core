﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.Interfaces.Core
{
    public abstract class NpServiceBase : System.Web.Services.WebService
    {
        protected void AddFailureToResult(Result result, Exception exc)
        {
            result.Type = Result.eResultType.Failure;
            result.Messages.Add(exc.Message);
            result.Messages.Add(exc.StackTrace);
        }
    }
}
