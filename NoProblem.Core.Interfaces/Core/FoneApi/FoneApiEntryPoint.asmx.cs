﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using FoneApiWrapper.CallBacks;
using Effect.Crm.Logs;
using System.Text;

namespace NoProblem.Core.Interfaces.Core.FoneApi
{
    /// <summary>
    /// Summary description for FoneApiEntryPoint
    /// </summary>
    [WebService(Namespace = "http://noproblemppc.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FoneApiEntryPoint : System.Web.Services.WebService
    {
        private const string FAPI = "fapi";
        private const string NPP = "npp";
        private const string APP_JSON = "application/json";

        [WebMethod]
        public void CallBack()
        {
            BusinessLogic.FoneApiBL.FapiArgs args = new NoProblem.Core.BusinessLogic.FoneApiBL.FapiArgs();
            StringBuilder logBuilder = new StringBuilder("Foneapi event:\n");
            foreach (var item in Context.Request.Params.AllKeys.Where(x => x.StartsWith(FAPI) || x.StartsWith(NPP)))
            {
                args.Add(item, Context.Request.Params[item]);
                logBuilder.Append(item).Append("=").AppendLine(args[item]);
            }
            LogUtils.MyHandle.WriteToLog(9, logBuilder.ToString());
            try
            {
                CallBackResponse response = BusinessLogic.FoneApiBL.FoneApiRouter.Instance.Route(args);
                Context.Response.ContentType = APP_JSON;
                if (response != null)
                {
                    string json = response.ToString();
                    LogUtils.MyHandle.WriteToLog(9, "Foneapi json to send: " + json);
                    Context.Response.Write(json);
                }
                else
                {
                    LogUtils.MyHandle.WriteToLog(9, "Foneapi json to send: NULL");
                }
            }
            catch (Exception exc)
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("Exception in FoneApiEntryPoint.CallBack, args:");
                foreach (var item in args)
                {
                    builder.Append(item.Key).Append("=").AppendLine(item.Value);
                }
                LogUtils.MyHandle.HandleException(exc, builder.ToString());
                Context.Response.ContentType = APP_JSON;
                builder.Append("Exception message: ").AppendLine(exc.Message);
                builder.Append("Exception stack trace: ").AppendLine(exc.StackTrace);
                Context.Response.Write(builder.ToString());
                Context.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                Context.Response.Status = "500 InternalServerError";
            }
        }
    }
}
