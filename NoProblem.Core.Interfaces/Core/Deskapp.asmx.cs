﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using NoProblem.Core.DataModel;
using Effect.Crm.Logs;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for Deskapp
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Deskapp : System.Web.Services.WebService
    {
        [WebMethod]
        public Result<DataModel.Deskapp.ConsumerSignUpDesktopResponse> ConsumerSignUpDesktop(DataModel.Deskapp.ConsumerSignUpDesktopRequest request)
        {
            Result<DataModel.Deskapp.ConsumerSignUpDesktopResponse> result = new Result<NoProblem.Core.DataModel.Deskapp.ConsumerSignUpDesktopResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ConsumerSignUpDesktop started. MobilePhone = {0}", request.phone);
                BusinessLogic.ConsumerDesktop.ConsumerManager manager = new NoProblem.Core.BusinessLogic.ConsumerDesktop.ConsumerManager(null);
                result.Value = manager.ConsumerSignUpDesktop(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in SupplierSignUpNew. MobilePhone = {0}", request.phone);
            }
            return result;
        }
        [WebMethod]
        public Result<Guid> ConsumerRegisterPincode(DataModel.Deskapp.ConsumerRegisterPincodeRequest request)
        {
            Result<Guid> result = new Result<Guid>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ConsumerRegisterPincode started. MobilePhone = {0}, PinCode = {1}", request.phone, request.PinCode);
                BusinessLogic.ConsumerDesktop.ConsumerManager manager = new NoProblem.Core.BusinessLogic.ConsumerDesktop.ConsumerManager(null);
                result.Value = manager.ConsumerRegisterPincode(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in SupplierSignUpNew. MobilePhone = {0}", request.phone);
            }
            return result;
        }
        [WebMethod]
        public Result<NoProblem.Core.DataModel.Deskapp.CallRecordResponse> RecordCall(NoProblem.Core.DataModel.Deskapp.CallRecordRequest request)
        {
            Result<NoProblem.Core.DataModel.Deskapp.CallRecordResponse> result = new Result<NoProblem.Core.DataModel.Deskapp.CallRecordResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "RecordCall started.  ContactId = {0},  BobPhone = {1},", request.ContactId.ToString()
                    , request.BobPhone);
                BusinessLogic.OrganicDesktopApp.CallAndRecordManager m = new BusinessLogic.OrganicDesktopApp.CallAndRecordManager(null);
                result.Value = m.StartCall(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in RecordCall. ContactId = {0},  BobPhone = {1},", request.ContactId.ToString()
                    , request.BobPhone);
            }
            return result;
            
        }
        [WebMethod]
        public Result<int> GetSecondRecordLeft(Guid ContactId)
        {
            Result<int> result = new Result<int>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSecondRecordLeft.  ContactId = {0}", ContactId.ToString());
                BusinessLogic.ConsumerDesktop.ConsumerManager manager = new NoProblem.Core.BusinessLogic.ConsumerDesktop.ConsumerManager(null);
                result.Value = manager.GetSecondRecordLeft(ContactId);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetSecondRecordLeft. ContactId = {0},", ContactId.ToString());
            }
            return result;

        }
    }
}
