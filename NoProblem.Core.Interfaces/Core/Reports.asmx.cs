﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.Interfaces.Core.Reports
//  File: Reports.asmx.cs
//  Description: Web entry point for report's requests
//  Author: Alain Adler
//  Company: Onecall
//  Createdon: 31/10/10
//
/////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Services;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Reports;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Reports;
using NoProblem.Core.DataModel.Reports.Request;
using NoProblem.Core.DataModel.Reports.Response;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary> 
    /// Summary description for Reports
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Reports : NpServiceBase
    {
        [WebMethod]
        public Result<DML.Dashboard.FinancialDashboard.FinancialDashboardFiltersContainer> GetFinancialDashboardFilters()
        {
            Result<DML.Dashboard.FinancialDashboard.FinancialDashboardFiltersContainer> result = new Result<NoProblem.Core.DataModel.Dashboard.FinancialDashboard.FinancialDashboardFiltersContainer>();
            try
            {
                BusinessLogic.Dashboard.FinancialDashboard.ReportFiltersCreator filterCreator = new NoProblem.Core.BusinessLogic.Dashboard.FinancialDashboard.ReportFiltersCreator(null);
                result.Value = filterCreator.GetFilters();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Dashboard.FinancialDashboard.FinancialDashboardResponse> FinancialDashboard(DataModel.Dashboard.FinancialDashboard.FinancialDashboardRequest request)
        {
            Result<DataModel.Dashboard.FinancialDashboard.FinancialDashboardResponse> result = new Result<NoProblem.Core.DataModel.Dashboard.FinancialDashboard.FinancialDashboardResponse>();
            try
            {
                BusinessLogic.Dashboard.FinancialDashboard.ReportCreator creator = new NoProblem.Core.BusinessLogic.Dashboard.FinancialDashboard.ReportCreator(null);
                result.Value = creator.CreateReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Reports.asmx.FinancialDashboard");
                AddFailureToResult(result, exc);
            }
            return result;
        }
        [WebMethod]
        public Result<DateTime> FinancialDashboardLastUpdate()
        {
            Result<DateTime> result = new Result<DateTime>();
            try
            {
                BusinessLogic.Dashboard.FinancialDashboard.ReportCreator creator = new NoProblem.Core.BusinessLogic.Dashboard.FinancialDashboard.ReportCreator(null);
                result.Value = creator.GetFinancialLastUpdate();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Reports.asmx.FinancialDashboardLastUpdate");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Reports.KeywordReportResponse> KeywordReport(DataModel.Reports.KeywordReportRequest request)
        {
            Result<DataModel.Reports.KeywordReportResponse> result = new Result<KeywordReportResponse>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.KeywordReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in KeywordReport");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<PreDefinedPriceReportResponse> PreDefinedPriceReport(PreDefinedPriceReportRequest request)
        {
            Result<PreDefinedPriceReportResponse> result = new Result<PreDefinedPriceReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "PreDefinedPriceReport started. Heading = {0}, from = {1}, to = {2}", request.HeadingId.ToString(), request.FromDate.ToString(), request.ToDate.ToString());
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.PreDefinedPriceReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "PreDefinedPriceReport failed with exception. Heading = {0}, from = {1}, to = {2}", request.HeadingId.ToString(), request.FromDate.ToString(), request.ToDate.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<RequestsReportResponse> RequestsReport(RequestsReportRequest request)
        {
            Result<RequestsReportResponse> result = new Result<RequestsReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "RequestsReport started. request: from= {0}, to= {1}, caseNumber = {2}, ExpertiseId = {3}.", request.FromDate.ToString(), request.ToDate.ToString(), request.CaseNumber, request.ExpertiseId.ToString());
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.RequestsReport(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "RequestsReport failed with exception. request: from= {0}, to= {1}, caseNumber = {2}, ExpertiseId = {3}.", request.FromDate.ToString(), request.ToDate.ToString(), request.CaseNumber, request.ExpertiseId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.DescriptionValidationBasicReportResponse> DescriptionValidationBasicReport(DML.Reports.DescriptionValidationReportRequest request)
        {
            Result<DML.Reports.DescriptionValidationBasicReportResponse> result = new Result<DescriptionValidationBasicReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DescriptionValidationBasicReport started");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.DescriptionValidationBasicReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DescriptionValidationBasicReport");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.DescriptionValidationGroupBySessionResponse> DescriptionValidationGroupBySessionReport(DML.Reports.DescriptionValidationReportRequest request)
        {
            Result<DML.Reports.DescriptionValidationGroupBySessionResponse> result = new Result<DescriptionValidationGroupBySessionResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DescriptionValidationGroupBySessionReport started");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.DescriptionValidationGroupBySessionReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DescriptionValidationGroupBySessionReport");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Reports.DescriptionValidationReportRow>> DescriptionValidationSessionDrillDown(string session)
        {
            Result<List<DataModel.Reports.DescriptionValidationReportRow>> result = new Result<List<DataModel.Reports.DescriptionValidationReportRow>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DescriptionValidationSessionDrillDown started");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.DescriptionValidationSessionDrillDown(session);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DescriptionValidationSessionDrillDown");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<GetValidationReportDropDownListsResponse> GetValidationReportDropDownLists()
        {
            Result<GetValidationReportDropDownListsResponse> result = new Result<GetValidationReportDropDownListsResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetValidationReportDropDownLists started");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.GetValidationReportDropDownLists();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetValidationReportDropDownLists");
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.ExternalSupplierReportResponse> ExternalSupplierReport(DML.PagingRequest request)
        {
            Result<DML.Reports.ExternalSupplierReportResponse> result = new Result<ExternalSupplierReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ExternalSupplierReport started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.ExternalSupplierReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "ExternalSupplierReport failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.FullUrlExposureReportResponse> FullUrlExposureReport(DML.Reports.FullUrlExposureReportRequest request)
        {
            Result<DML.Reports.FullUrlExposureReportResponse> result = new Result<FullUrlExposureReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "FullUrlExposureReport started. From={0}, To={1}, OriginId={2}", request.FromDate, request.ToDate, request.OriginId);
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.FullUrlExposuresReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "FullUrlExposureReport failed with exception. From={0}, To={1}, OriginId={2}", request.FromDate, request.ToDate, request.OriginId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        #region Root Url Exposures Methods

        [WebMethod]
        public Result<DML.Reports.RootUrlExposuresReportResponse> RootUrlExposureReport(DML.Reports.RootUrlExposuresReportRequest request)
        {
            Result<DML.Reports.RootUrlExposuresReportResponse> result = new Result<RootUrlExposuresReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "RootUrlExposureReport started. From={0}, To={1}, OriginId={2}", request.FromDate, request.ToDate, request.OriginId);
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.RootUrlExposuresReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "RootUrlExposureReport failed with exception. From={0}, To={1}, OriginId={2}", request.FromDate, request.ToDate, request.OriginId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.RootUrlExposuresReportResponse> RootUrlExposureKeywordDrillDown(DML.Reports.RootUrlExposuresKeywordDrillDownRequest request)
        {
            Result<DML.Reports.RootUrlExposuresReportResponse> result = new Result<RootUrlExposuresReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "RootUrlExposureKeywordDrillDown started. From={0}, To={1}, OriginId={2}, RootUrl={3}", request.FromDate, request.ToDate, request.OriginId, request.RootUrl);
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.RootUrlExposuresKeywordDrillDown(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "RootUrlExposureKeywordDrillDown failed with exception. From={0}, To={1}, OriginId={2}, RootUrl={3}", request.FromDate, request.ToDate, request.OriginId, request.RootUrl);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.RootUrlExposuresReportResponse> RootUrlExposureFullUrlDrillDown(DML.Reports.RootUrlExposuresFullUrlRequest request)
        {
            Result<DML.Reports.RootUrlExposuresReportResponse> result = new Result<RootUrlExposuresReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "RootUrlExposureFullUrlDrillDown started. From={0}, To={1}, OriginId={2}, RootUrl={3}, Keyword={4}", request.FromDate, request.ToDate, request.OriginId, request.RootUrl, request.Keyword);
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.RootUrlExposuresFullUrlDrillDown(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "RootUrlExposureFullUrlDrillDown failed with exception. From={0}, To={1}, OriginId={2}, RootUrl={3}, Keyword={4}", request.FromDate, request.ToDate, request.OriginId, request.RootUrl, request.Keyword);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        #endregion

        [WebMethod]
        public Result<DML.Dashboard.AarDashboardReportResponse> AarDashboard(DML.Dashboard.AarDashboardReportRequest request)
        {
            Result<DML.Dashboard.AarDashboardReportResponse> result = new Result<NoProblem.Core.DataModel.Dashboard.AarDashboardReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AarDashboard started.");
                BusinessLogic.Dashboard.AarDashboardReportCreator creator = new NoProblem.Core.BusinessLogic.Dashboard.AarDashboardReportCreator(null);
                result.Value = creator.AarDashboardReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "AarDashboard failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.SupplierMoneyReportResponse> SupplierMoneyReport(string supplierNumber)
        {
            Result<DML.Reports.SupplierMoneyReportResponse> result = new Result<DML.Reports.SupplierMoneyReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SupplierMoneyReport started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.SupplierMoneyReport(supplierNumber);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SupplierMoneyReport failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DML.Reports.SupplierMoneyReportResponse>> SupplierMoneyReportToExcel()
        {
            Result<List<DML.Reports.SupplierMoneyReportResponse>> result = new Result<List<DML.Reports.SupplierMoneyReportResponse>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SupplierMoneyReportToExcel started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.SupplierMoneyReportToExcel();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SupplierMoneyReportToExcel failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DML.Reports.AarRequestsReportRow>> AarRequestsReport(DML.Reports.AarRequestsReportsRequest request)
        {
            Result<List<DML.Reports.AarRequestsReportRow>> result = new Result<List<DML.Reports.AarRequestsReportRow>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AarRequestsReport started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.AarRequestsReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "AarRequestsReport failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DML.Reports.AarRequestReportDrillDownRow>> AarRequestsReportDrillDown(DML.Reports.AarRequestsReportsRequest request)
        {
            Result<List<DML.Reports.AarRequestReportDrillDownRow>> result = new Result<List<DML.Reports.AarRequestReportDrillDownRow>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AarRequestsReportDrillDown started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.AarRequestReportDrillDown(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "AarRequestsReportDrillDown failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DML.Reports.SalesmanDeposits>> SalesmenReport(DML.Reports.SalesmenReportRequest request)
        {
            Result<List<DML.Reports.SalesmanDeposits>> result = new Result<List<DML.Reports.SalesmanDeposits>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SalesmenReport started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.SalesmenReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SalesmenReport failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<VoucherData>> VoucherReport()
        {
            Result<List<VoucherData>> result = new Result<List<VoucherData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "VoucherReport started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.VoucherReport();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "VoucherReport failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<ConversionReportResponse> ConversionReport(ConversionReportRequest request)
        {
            Result<ConversionReportResponse> result =
                new Result<ConversionReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ConversionReport started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.ConversionReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "ConversionReport failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<ConversionReportResponse> ConversionReport2(ConversionReportRequest request)
        {
            Result<ConversionReportResponse> result =
                new Result<ConversionReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ConversionReport started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.ConversionReport2(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "ConversionReport failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        /*
        [WebMethod]
        public Result<List<ConversionDrillDownRow>> ConversionDrillDown(ConversionDrillDownRequest request)
        {
            Result<List<ConversionDrillDownRow>> result = new Result<List<ConversionDrillDownRow>>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.ConversionDrillDown(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "ConversionDrillDown failed with exception.");
            }
            return result;
        }
         * */
        [WebMethod]
        public Result<List<ConversionDrillDownRow>> ConversionDrillDown2(ConversionDrillDownRequest request)
        {
            Result<List<ConversionDrillDownRow>> result = new Result<List<ConversionDrillDownRow>>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.ConversionDrillDown2(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "ConversionDrillDown failed with exception.");
            }
            return result;
        }
        [WebMethod]
        public Result<List<ConversionUsersReportResponse>> ConversionReportUsers(ConversionReportUsersRequest request)
        {
            Result<List<ConversionUsersReportResponse>> result =
                new Result<List<ConversionUsersReportResponse>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ConversionReportUsers started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.ConversionReportUsers(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "ConversionReportUsers failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<List<ConversionUsersReportExposureResponse>> ConversionReportUsersExposures(ConversionReportUsersRequest request)
        {
            Result<List<ConversionUsersReportExposureResponse>> result =
                new Result<List<ConversionUsersReportExposureResponse>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "ConversionReportUsersExposures started.");
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.ConversionReportUsersExposures(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "ConversionReportUsersExposures failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<RefundReportResponse> RefundsReport(RefundReportRequest request)
        {
            Result<RefundReportResponse> result = new Result<RefundReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "RefundsReport started. Request: from = {0}, to = {1}, headingId = {2}", request.FromDate, request.ToDate, request.HeadingId);
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.RefundReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "RefundsReport failed with exception. Request: from = {0}, to = {1}, headingId = {2}", request.FromDate, request.ToDate, request.HeadingId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Dashboard.DashboardReportResponse> DashboardReport(DML.Dashboard.DashboardReportRequest request)
        {
            Result<DML.Dashboard.DashboardReportResponse> result = new Result<NoProblem.Core.DataModel.Dashboard.DashboardReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DashboardReport started. request: from = {0}, to = {1}, compareToPast = {2}, interval = {3}, onlyGraphs = {4}, pastFrom = {5}, pastTo = {6}, showMe = {7}, showObjectives = {8}.",
                    request.FromDate.ToString(), request.ToDate.ToString(), request.CompareToPast, request.Interval, request.OnlyGraphs, request.PastFromDate.ToString(), request.PastToDate.ToString(), request.ShowMe, request.ShowObjectives);
                BusinessLogic.Dashboard.DashboardReportCreator creator = new NoProblem.Core.BusinessLogic.Dashboard.DashboardReportCreator(null);
                result.Value = creator.DashboardReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "DashboardReport failed with exception.");
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                result.Type = Result.eResultType.Failure;
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reviews.GetReviewsResponse> GetReviews(DML.Reviews.GetReviewsRequest request)
        {
            Result<DML.Reviews.GetReviewsResponse> result = new Result<NoProblem.Core.DataModel.Reviews.GetReviewsResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetReviews started. request: fromDate = {0}, toDate = {1}, status = {2}, supplierId = {3}",
                    request.FromDate.HasValue ? request.FromDate.Value.ToString() : "null",
                    request.ToDate.HasValue ? request.ToDate.Value.ToString() : "null",
                    request.ReviewStatus.HasValue ? request.ReviewStatus.Value.ToString() : "null",
                    request.SupplierId.HasValue ? request.SupplierId.Value.ToString() : "null");
                BusinessLogic.Reviews.ReviewsManager manager = new NoProblem.Core.BusinessLogic.Reviews.ReviewsManager(null);
                List<DML.Reviews.ReviewData> dataList = manager.GetReviews(request.FromDate, request.ToDate, request.ReviewStatus, request.SupplierId);
                result.Value = new NoProblem.Core.DataModel.Reviews.GetReviewsResponse()
                {
                    Reviews = dataList
                };
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetReviews failed with exception.");
            }
            return result;
        }

        [WebMethod]
        public Result<BalanceReportResponse> BalanceReport(BalanceReportRequest request)
        {
            Result<BalanceReportResponse> result = new Result<BalanceReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "BalanceReport started. request: from= {0}, to= {1}, supplierId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.SupplierId.ToString());
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.BalanceReport(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "BalanceReport failed with exception. request: from= {0}, to= {1}, supplierId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.SupplierId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<GetExpertiseReportResponse> GetExpertiseReport(GetExpertiseReportRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetExpertiseReport started - request.ExpertiseId = {0}, request.ExpertiseLevel = {1}", request.ExpertiseId, request.ExpertiseLevel);
            Result<GetExpertiseReportResponse> result = new Result<GetExpertiseReportResponse>();
            try
            {
                bool valid = true;
                if (request.ExpertiseId != Guid.Empty && request.ExpertiseLevel != 1 && request.ExpertiseLevel != 2)
                {
                    result.Type = Result.eResultType.Failure;
                    result.Messages.Add("If ExpertiseId is not Guid.Empty, ExpertiseLevel must be 1 or 2");
                    valid = false;
                }
                if (valid)
                {
                    ReportsManager manager = new ReportsManager(null);
                    GetExpertiseReportResponse response = manager.GetExpertiseReport(request);
                    result.Value = response;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetExpertiseReport failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<NonPpcCallsReportResponse> NonPpcCallsReport(NonPpcCallsReportRequest request)
        {
            Result<NonPpcCallsReportResponse> result = new Result<NonPpcCallsReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "NonPpcCallsReport started. request: from= {0}, to= {1}, groupby= {2}", request.FromDate.ToString(), request.ToDate.ToString(), request.GroupBy.ToString());
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.NonPpcCallsReport(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "NonPpcCallsReport failed with exception. request: from= {0}, to= {1}, groupby= {2}", request.FromDate.ToString(), request.ToDate.ToString(), request.GroupBy.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Audit.AuditReportResponse> AuditReport(DataModel.Audit.AuditReportRequest request)
        {
            Result<DataModel.Audit.AuditReportResponse> result = new Result<NoProblem.Core.DataModel.Audit.AuditReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AuditReport started. request: fromDate = {0}, toDate = {1}, pageId = {2}", request.FromDate.ToString(), request.ToDate.ToString(), request.PageId);
                BusinessLogic.Audit.AuditManager auditManager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                result.Value = auditManager.AuditReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "AuditReport failed with exception. request: fromDate = {0}, toDate = {1}, pageId = {2}", request.FromDate.ToString(), request.ToDate.ToString(), request.PageId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Reports.InvalidRequestsReportResponse> InvalidRequestsReport(DataModel.Reports.InvalidRequestsReportRequest request)
        {
            Result<DataModel.Reports.InvalidRequestsReportResponse> result = new Result<InvalidRequestsReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "InvalidRequestsReport started. request: fromDate = {0}, toDate = {1}, status = {2}, badWord = {3}, phone = {4}", request.FromDate.ToString(), request.ToDate.ToString(), request.InvalidStatus, request.BadWord, request.PhoneNumber);
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.InvalidRequestsReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "InvalidRequestsReport failed with exception. request: fromDate = {0}, toDate = {1}, status = {2}, badWord = {3}, phone = {4}", request.FromDate.ToString(), request.ToDate.ToString(), request.InvalidStatus, request.BadWord, request.PhoneNumber);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Reports.AdvertisersReportResponse> AdvertisersReport(DataModel.Reports.AdvertisersReportRequest request)
        {
            Result<DataModel.Reports.AdvertisersReportResponse> result = new Result<AdvertisersReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AdvertisersReport started. request: fromDate = {0}, toDate = {1}, status = {2}, maxBalance = {3}", request.FromDate.ToString(), request.ToDate.ToString(), request.SupplierStatus, request.MaxBalance);
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.AdvertisersReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "AdvertisersReport failed with exception. request: fromDate = {0}, toDate = {1}, status = {2}, maxBalance = {3}", request.FromDate.ToString(), request.ToDate.ToString(), request.SupplierStatus, request.MaxBalance);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Upsale.GetAllOpenUpsalesResponse> GetAllOpenUpsales(DML.Upsale.GetAllOpenUpsalesRequest request)
        {
            Result<DML.Upsale.GetAllOpenUpsalesResponse> result = new Result<NoProblem.Core.DataModel.Upsale.GetAllOpenUpsalesResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllOpenUpsales Started.");
                BusinessLogic.Reports.ReportsManager manager = new NoProblem.Core.BusinessLogic.Reports.ReportsManager(null);
                result.Value = manager.GetAllOpenUpsales(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetAllOpenUpsales failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            LogUtils.MyHandle.WriteToLog(9, "_GetAllOpenUpsales Ended.");
            return result;
        }

        [WebMethod]
        public Result<DML.Upsale.UpsaleDataExpanded> GetUpsaleData(Guid upsaleId)
        {
            Result<DML.Upsale.UpsaleDataExpanded> result = new Result<NoProblem.Core.DataModel.Upsale.UpsaleDataExpanded>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetUpsaleData Started.");
                BusinessLogic.Reports.ReportsManager manager = new NoProblem.Core.BusinessLogic.Reports.ReportsManager(null);
                result.Value = manager.GetUpsaleData(upsaleId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetUpsaleData failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DepositReportResponse> GetDepositsReport(DepositReportRequest Request)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetDepositsReport started.");
            Result<DepositReportResponse> result = new Result<DepositReportResponse>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new NoProblem.Core.BusinessLogic.Reports.ReportsManager(null);
                result.Value = manager.GetDepositsReport(Request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetDepositsReport Failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<System.Collections.Generic.List<System.Collections.Generic.List<string>>> WizardReport(DataModel.ReportsWizard.WizardReportRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, "WizardReport started.");
            Result<System.Collections.Generic.List<System.Collections.Generic.List<string>>> result = new Result<System.Collections.Generic.List<System.Collections.Generic.List<string>>>();
            try
            {
                BusinessLogic.ReportsWizard.WizardManager manager = new NoProblem.Core.BusinessLogic.ReportsWizard.WizardManager(null);
                result.Value = manager.CreateReport(request);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "WizardReport Failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(ex.Message);
            }
            return result;
        }

        #region Calls Reports

        [WebMethod]
        public Result<DML.Reports.Response.Calls.CallsReportResponse> CallsReport(DML.Reports.CallsReport.CallsReportRequest request)
        {
            Result<DML.Reports.Response.Calls.CallsReportResponse> result = new Result<DML.Reports.Response.Calls.CallsReportResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CallsReport (no group by) started. request: from= {0}, to= {1}, ExpertiseId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.ExpertiseId.ToString());
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.CallsReportNoGroupBy(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "CallsReport (no group by) failed with exception. request: from= {0}, to= {1}, ExpertiseId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.ExpertiseId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.Response.Calls.CallsReportRevenuePerHeadingResponse> CallsReportRevenuePerHeading(DML.Reports.CallsReport.CallsReportRequest request)
        {
            Result<DML.Reports.Response.Calls.CallsReportRevenuePerHeadingResponse> result = new Result<DML.Reports.Response.Calls.CallsReportRevenuePerHeadingResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CallsReportRevenuePerHeading started. request: from= {0}, to= {1}, ExpertiseId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.ExpertiseId.ToString());
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.CallsReportRevenuePerHeading(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "CallsReportRevenuePerHeading failed with exception. request: from= {0}, to= {1}, ExpertiseId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.ExpertiseId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.Response.Calls.CallsReportRevenuePerDateResponse> CallsReportRevenuePerDate(DML.Reports.CallsReport.CallsReportRequest request)
        {
            Result<DML.Reports.Response.Calls.CallsReportRevenuePerDateResponse> result = new Result<DML.Reports.Response.Calls.CallsReportRevenuePerDateResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CallsReportRevenuePerDate started. request: from= {0}, to= {1}, ExpertiseId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.ExpertiseId.ToString());
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.CallsReportRevenuePerDate(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "CallsReportRevenuePerDate failed with exception. request: from= {0}, to= {1}, ExpertiseId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.ExpertiseId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.Response.Calls.CallsReportCountPerDateResponse> CallsReportCountPerDate(DML.Reports.CallsReport.CallsReportRequest request)
        {
            Result<DML.Reports.Response.Calls.CallsReportCountPerDateResponse> result = new Result<DML.Reports.Response.Calls.CallsReportCountPerDateResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CallsReportCountPerDate started. request: from= {0}, to= {1}, ExpertiseId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.ExpertiseId.ToString());
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.CallsReportCountPerDate(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "CallsReportCountPerDate failed with exception. request: from= {0}, to= {1}, ExpertiseId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.ExpertiseId.ToString());
            }
            return result;
        }

        [WebMethod]
        public Result<DML.Reports.Response.Calls.CallsReportCountPerHeadingResponse> CallsReportCountPerHeading(DML.Reports.CallsReport.CallsReportRequest request)
        {
            Result<DML.Reports.Response.Calls.CallsReportCountPerHeadingResponse> result = new Result<DML.Reports.Response.Calls.CallsReportCountPerHeadingResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CallsReportCountPerHeading started. request: from= {0}, to= {1}, ExpertiseId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.ExpertiseId.ToString());
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.CallsReportCountPerHeading(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "CallsReportCountPerHeading failed with exception. request: from= {0}, to= {1}, ExpertiseId = {2}.", request.FromDate.ToString(), request.ToDate.ToString(), request.ExpertiseId.ToString());
            }
            return result;
        }

        #endregion

        [WebMethod]
        public Result<MyCallsReportResponse> MyCallsReport(MyCallsReportRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, "MyCallsReport started.");
            Result<MyCallsReportResponse> result = new Result<MyCallsReportResponse>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.MyCallsReport(request);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "MyCallsReport Failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(ex.Message);
                result.Messages.Add(ex.StackTrace);
            }
            LogUtils.MyHandle.WriteToLog(9, "_MyCallsReport ended.");
            return result;
        }

        [WebMethod]
        public Result<List<OptOutAdvancedReportRow>> OptOutAdvancedReport(OptOutReportsRequest request)
        {
            Result<List<OptOutAdvancedReportRow>> result = new Result<List<OptOutAdvancedReportRow>>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.OptOutAdvancedReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in OptOutAdvancedReport");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<StringIntPair>> OptOutRegularReport(OptOutReportsRequest request)
        {
            Result<List<StringIntPair>> result = new Result<List<StringIntPair>>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.OptOutRegualrReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in OptOutRegularReport");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        /*
        [WebMethod]
        public Result<AddOnInstallationReportResponse> AddOnInstallationReport(AddOnInstallationReportRequest request)
        {
            Result<AddOnInstallationReportResponse> result = new Result<AddOnInstallationReportResponse>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.AddOnInstallationReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AddOnInstallationReport");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
         * */
        [WebMethod]
        public Result<DistributionInstallationReportResponse> DistributionInstallationReport(AddOnInstallationReportRequest request)
        {
            Result<DistributionInstallationReportResponse> result = new Result<DistributionInstallationReportResponse>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.DistributionInstallationReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DistributionInstallationReport");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<DistributionInstallationReportResponseDrillDown> DistributionInstallationReportDrillDown(AddOnInstallationReportRequest request)
        {
            Result<DistributionInstallationReportResponseDrillDown> result = new Result<DistributionInstallationReportResponseDrillDown>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.DistributionInstallationReportDrillDown(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DistributionInstallationReportDrillDown");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        /*
        [WebMethod]
        public string CreateC2cReport(string command, string date, string format, string fields)
        {
            try
            {
                BusinessLogic.ServiceRequest.NonPpcServiceManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.NonPpcServiceManager(null);
                return manager.CreateC2cReport(command, date, format, fields);
            }
            catch (Exception exc)
            {
                return "Unexpected exception was thrown. \n" + exc.Message + "\n " + exc.StackTrace;
            }
        }
         * */
        [WebMethod]
        public Result<List<DML.Reports.Response.C2CData>> CreateC2cReport(DateTime date)
        {
            Result<List<DML.Reports.Response.C2CData>> result = new Result<List<NoProblem.Core.DataModel.Reports.Response.C2CData>>();
            try
            {
                BusinessLogic.ServiceRequest.NonPpcServiceManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.NonPpcServiceManager(null);
                result.Value = manager.CreateC2cReport(date);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateC2cReport");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        //[WebMethod]
        //public Result<DataModel.Dapaz.PpaSync.PpaSyncData> PpaSync()
        //{
        //    Result<DataModel.Dapaz.PpaSync.PpaSyncData> result = new Result<NoProblem.Core.DataModel.Dapaz.PpaSync.PpaSyncData>();
        //    try
        //    {
        //        System.Threading.Thread t1 = new System.Threading.Thread(delegate()
        //        {
        //            try
        //            {
        //                BusinessLogic.Dapaz.PpaSyncer syncer = new NoProblem.Core.BusinessLogic.Dapaz.PpaSyncer(null);
        //                //result.Value = syncer.Create();
        //                syncer.CreateSave();
        //            }
        //            catch (Exception exc)
        //            {
        //                LogUtils.MyHandle.HandleException(exc, "Ppa async");
        //            }
        //        });
        //        t1.Start();

        //    }
        //    catch (Exception exc)
        //    {
        //        LogUtils.MyHandle.HandleException(exc, "Exception in PpaSync.");
        //        AddFailureToResult(result, exc);
        //    }
        //    return result;
        //}

        [WebMethod]
        public Result<List<DataModel.Dapaz.SimsReports.SimsCallData>> CreateSimsReport(DateTime date)
        {
            Result<List<DataModel.Dapaz.SimsReports.SimsCallData>> result = new Result<List<DataModel.Dapaz.SimsReports.SimsCallData>>();
            try
            {
                BusinessLogic.Dapaz.SimsReportCreator reportCreator = new NoProblem.Core.BusinessLogic.Dapaz.SimsReportCreator(null);
                result.Value = reportCreator.CreateSimsReport(date);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateSimsReport. date = {0}", date.ToString());
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Dapaz.Reports.DapazReportsSupplierContainer>> DapazReportFindSupplier(string searchValue)
        {
            Result<List<DataModel.Dapaz.Reports.DapazReportsSupplierContainer>> result = new Result<List<NoProblem.Core.DataModel.Dapaz.Reports.DapazReportsSupplierContainer>>();
            try
            {
                BusinessLogic.Dapaz.Reports.SupplierFinder finder = new NoProblem.Core.BusinessLogic.Dapaz.Reports.SupplierFinder(null);
                result.Value = finder.FindSupplier(searchValue);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in DapazReportFindSupplier. SearchValue = {0}", searchValue);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Dapaz.Reports.DapazCallReportResponse> DapazCallReport(DataModel.Dapaz.Reports.DapazCallsReportRequest request)
        {
            Result<DataModel.Dapaz.Reports.DapazCallReportResponse> result = new Result<NoProblem.Core.DataModel.Dapaz.Reports.DapazCallReportResponse>();
            try
            {
                BusinessLogic.Dapaz.Reports.CallsReportCreator creator = new NoProblem.Core.BusinessLogic.Dapaz.Reports.CallsReportCreator(null);
                result.Value = creator.CreateReport(request.FromDate, request.ToDate, request.SupplierId, request.CallStatus, request.DapazStatus);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in DapazCallReport");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Dapaz.Reports.DapazGroupedCallsResponse> DapazGroupedCallsReport(DataModel.Dapaz.Reports.DapazCallsReportRequest request)
        {
            Result<DataModel.Dapaz.Reports.DapazGroupedCallsResponse> result = new Result<DataModel.Dapaz.Reports.DapazGroupedCallsResponse>();
            try
            {
                BusinessLogic.Dapaz.Reports.CallsGroupedReportCreator creator = new NoProblem.Core.BusinessLogic.Dapaz.Reports.CallsGroupedReportCreator(null);
                result.Value = creator.CreateReport(request.FromDate, request.ToDate, request.SupplierId, request.CallStatus, request.GroupBy, request.DapazStatus);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in DapazGroupedCallsReport");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Dapaz.Reports.DapazGroupedByCallerIdReportResponse> DapazCallsByCallerIdReport(DataModel.Dapaz.Reports.DapazCallsReportRequest request)
        {
            Result<DataModel.Dapaz.Reports.DapazGroupedByCallerIdReportResponse> result = new Result<DataModel.Dapaz.Reports.DapazGroupedByCallerIdReportResponse>();
            try
            {
                BusinessLogic.Dapaz.Reports.CallsByCallerIdReportCreator creator = new NoProblem.Core.BusinessLogic.Dapaz.Reports.CallsByCallerIdReportCreator(null);
                result.Value = creator.CreateReport(request.FromDate, request.ToDate, request.SupplierId, request.CallStatus, request.DapazStatus);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in DapazCallsByCallerIdReport");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Dapaz.PotentialStats.PotentialStatRow>> ZgPotentialStatsReport()
        {
            Result<List<DataModel.Dapaz.PotentialStats.PotentialStatRow>> result = new Result<List<NoProblem.Core.DataModel.Dapaz.PotentialStats.PotentialStatRow>>();
            try
            {
                BusinessLogic.Dapaz.PotentialStats.PotentialStatsReport reportCreator = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsReport(null);
                result.Value = reportCreator.CreateReport();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in ZgPotentialStatsReport");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Reports.BrokersLeadsDataForReportResponse>> BrokersLeadsReport(BrokersLeadsReportRequest request)
        {
            Result<List<DataModel.Reports.BrokersLeadsDataForReportResponse>> result = new Result<List<BrokersLeadsDataForReportResponse>>();
            try
            {
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.BrokersLeadsReport(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in BrokersLeadsReport");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Reports.BrokerSummaryRow>> BrokersSummaryReport(BrokersSummaryReportRequest request)
        {
            Result<List<DataModel.Reports.BrokerSummaryRow>> result = new Result<List<BrokerSummaryRow>>();
            try
            {
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.BrokersSummaryReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in BrokersSummaryReport");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<ProfitableReportResponse> ProfitableReport(ProfitableReportRequest request)
        {
            Result<ProfitableReportResponse> result = new Result<ProfitableReportResponse>();
            try
            {
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.ProfitableReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ProfitableReport");
                AddFailureToResult(result, exc);
            }
            return result;
        }
        [WebMethod]
        public Result<DateTime> LastUpdateProfitableReport()
        {
            Result<DateTime> result = new Result<DateTime>();
            try
            {
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.LastUpdateProfitableReport();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ProfitableReport");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DistributorBillingReportIndividualRow>> DistributorBillingReportIndividual(DistributorBillingReportRequest request)
        {
            Result<List<DistributorBillingReportIndividualRow>> result = new Result<List<DistributorBillingReportIndividualRow>>();
            try
            {
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.DistributorBillingReportIndividual(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in DistributorBillingReportIndividual");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DistributorsReportSummaryRow>> DistributorBillingReportSummary(DistributorBillingReportRequest request)
        {
            Result<List<DistributorsReportSummaryRow>> result = new Result<List<DistributorsReportSummaryRow>>();
            try
            {
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.DistributorBillingReportSummary(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in DistributorBillingReportSummary");
            }
            return result;
        }

        [WebMethod]
        public Result<AutoRerouteReportResponse> AutoRerouteReport(DateTime fromDate, DateTime toDate)
        {
            Result<AutoRerouteReportResponse> result = new Result<AutoRerouteReportResponse>();
            try
            {
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.AutoRerouteReport(fromDate, toDate);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in AutoRerouteReport");
            }
            return result;
        }

        [WebMethod]
        public Result<List<PpaControlReportRow>> PpaControlReport(DateTime fromDate, DateTime toDate)
        {
            Result<List<PpaControlReportRow>> result = new Result<List<PpaControlReportRow>>();
            try
            {
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.PpaControlReport(fromDate, toDate);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PpaControlReport");
            }
            return result;
        }

        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.Reports.Response.PplReportData>> PplReport(NoProblem.Core.DataModel.Reports.Request.PplReportRequest request)
        {
            Result<List<NoProblem.Core.DataModel.Reports.Response.PplReportData>> result = new Result<List<NoProblem.Core.DataModel.Reports.Response.PplReportData>>();
            try
            {
                ReportsManager manager = new ReportsManager(null);
                result.Value = manager.PplReport(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PplReport");
            }
            return result;
        }

        [WebMethod]
        public Result<AdvertiserRegistrationResponse> AdvertiserRegistrationReport(AdvertiserRegistrationRequest request)
        {
            Result<AdvertiserRegistrationResponse> result = new Result<AdvertiserRegistrationResponse>();
            try
            {

                LogUtils.MyHandle.WriteToLog(8, "AdvertiserRegistrationReport started. request: fromDate = {0}, toDate = {1}, CameFrom = {2}, Plan = {3}", request.From.ToString(), request.To.ToString(), request.CameFrom.ToString(), request.Plan.ToString());

                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.AdvertiserRegistrationReport(request);

            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "AdvertiserRegistrationReport failed with exception. request: fromDate = {0}, toDate = {1}, CameFrom = {2}, Plan = {3}", request.From.ToString(), request.To.ToString(), request.CameFrom.ToString(), request.Plan.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs>> GetDistributionOriginsWithDAUs()
        {
            Result<List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs>> result = new Result<List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs>>();
            try
            {
                BusinessLogic.Dashboard.FinancialDashboard.ReportFiltersCreator filterCreator = new NoProblem.Core.BusinessLogic.Dashboard.FinancialDashboard.ReportFiltersCreator(null);
                result.Value = filterCreator.GetDistributionOriginsWithDaus();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetDistributionOriginsWithDAUs failed with exception.");
                AddFailureToResult(result, exc);
            }
            return result;
        }
        [WebMethod]
        public Result<List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs>> GetDistributionOrigins()
        {
            Result<List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs>> result = new Result<List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs>>();
            try
            {
                BusinessLogic.Dashboard.FinancialDashboard.ReportFiltersCreator filterCreator = new NoProblem.Core.BusinessLogic.Dashboard.FinancialDashboard.ReportFiltersCreator(null);
                result.Value = filterCreator.GetDistributionOrigins();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetDistributionOrigins failed with exception.");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DistributionsInstallationsByIpRow>> DistributaionInstallationsByIpReport(DistributionsInstallationsByIpRowRequest request)
        {
            Result<List<DistributionsInstallationsByIpRow>> result =
                new Result<List<DistributionsInstallationsByIpRow>>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.DistributaionInstallationsByIpReport(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "DistributaionInstallationsByIpReport failed with exception.");
            }
            return result;
        }
        [WebMethod]
        public Result<NoProblem.Core.DataModel.Reports.Response.InjectionReportResponse> InjectionReport(InjectionReportRequest request)
        {
            Result<NoProblem.Core.DataModel.Reports.Response.InjectionReportResponse> result = new Result<NoProblem.Core.DataModel.Reports.Response.InjectionReportResponse>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.InjectionReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in InjectionReport");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<List<InjectionReportDrillDownResponse>> InjectionReportDrillDown(InjectionReportRequest request)
        {
            Result<List<InjectionReportDrillDownResponse>> result = new Result<List<InjectionReportDrillDownResponse>>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.InjectionReportDrillDown(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in InjectionReportDrillDown");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<List<AdEngineReportResponse>> AdEngineReport(AdEngineReportRequest request)
        {
            Result<List<AdEngineReportResponse>> result = new Result<List<AdEngineReportResponse>>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.AdEngineReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "AdEngineReport failed with exception.");
                AddFailureToResult(result, exc);
            }
            return result;
        }
        [WebMethod]
        public Result<List<HeartBeatInjectionResponse>> HeartBeatInjectionReport(HeartBeatInjectionRequest request)
        {
            Result<List<HeartBeatInjectionResponse>> result = new Result<List<HeartBeatInjectionResponse>>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.HeartBeatInjectionReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "HeartBeatInjectionReport failed with exception.");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<List<NP_DistributionReportResponse>> NP_DistributionReport(NP_DistributionReportRequest request)
        {
            Result<List<NP_DistributionReportResponse>> result = new Result<List<NP_DistributionReportResponse>>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.NP_DistributionReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in NP_DistributionReport: from: {0}, to: {1}", request._from, request._to);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<List<NP_DistributionReportOriginResponse>> NP_DistributioReportByOrigin(NP_DistributionReportOriginRequest request)
        {
            Result<List<NP_DistributionReportOriginResponse>> result = new Result<List<NP_DistributionReportOriginResponse>>();
            try
            {
                BusinessLogic.Reports.ReportsManager manager = new ReportsManager(null);
                result.Value = manager.NP_DistributionOriginReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in NP_DistributionReport: from: {0}, to: {1}", request._from, request._to);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<List<NoProblem.Core.BusinessLogic.AAR.AarReportManager.AarCallCenterResponse>> 
            AarCallCenterReport(NoProblem.Core.BusinessLogic.AAR.AarReportManager.AarCallCenterRequest request)
        {
            Result<List<NoProblem.Core.BusinessLogic.AAR.AarReportManager.AarCallCenterResponse>> result =
                new Result<List<NoProblem.Core.BusinessLogic.AAR.AarReportManager.AarCallCenterResponse>>();
            try
            {
                NoProblem.Core.BusinessLogic.AAR.AarReportManager manager = new BusinessLogic.AAR.AarReportManager();
                result.Value = manager.CreateAarCallCenterReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AarCallCenterReport: from: {0}, to: {1}", request.From, request.To);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.ClipCall.Request.RequestReportResponse>>
            ClipCallRequestReport(NoProblem.Core.DataModel.ClipCall.Request.RequestReportRequest request)
        {
            Result<List<NoProblem.Core.DataModel.ClipCall.Request.RequestReportResponse>> result =
                new Result<List<NoProblem.Core.DataModel.ClipCall.Request.RequestReportResponse>>();
            try
            {
                NoProblem.Core.BusinessLogic.ClipCall.RequestReportManager manager = new BusinessLogic.ClipCall.RequestReportManager();
                result.Value = manager.RequestReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ClipCallRequestReport: from: {0}, to: {1}", request.from, request.to);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<NoProblem.Core.DataModel.ClipCall.Request.ReviewRejectConfirmResponse>
            RejectConfirmReviewIncident(NoProblem.Core.DataModel.ClipCall.Request.ReviewRejectConfirmRequest request)
        {
            Result<NoProblem.Core.DataModel.ClipCall.Request.ReviewRejectConfirmResponse> result =
                new Result<NoProblem.Core.DataModel.ClipCall.Request.ReviewRejectConfirmResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.ClipCall.RequestReportManager manager = new BusinessLogic.ClipCall.RequestReportManager();
                result.Value = manager.RejectConfirmReviewIncident(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RejectConfirmReviewIncident: IncidentId: {0}, ToCinfirmed: {1}", request.incidentId, request.toConfirm);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.ClipCall.FunnelSummary.FunnelSummaryResponse>>
            FunnelSummaryReport(NoProblem.Core.DataModel.ClipCall.FunnelSummary.FunnelSummaryRequest request)
        {
            Result<List<NoProblem.Core.DataModel.ClipCall.FunnelSummary.FunnelSummaryResponse>> result =
                new Result<List<NoProblem.Core.DataModel.ClipCall.FunnelSummary.FunnelSummaryResponse>>();
            try
            {
                NoProblem.Core.BusinessLogic.ClipCall.RequestReportManager manager = new BusinessLogic.ClipCall.RequestReportManager();
                result.Value = manager.FunnelSummaryReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in FunnelSummaryReport: from: {0}, to: {1}", request.From, request.To);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.ClipCall.SmsCampaignReport.SmsCampaignReportResponse>>
            SmsCampaignReport(NoProblem.Core.DataModel.ClipCall.SmsCampaignReport.SmsCampaignReportRequest request)
        {
            Result<List<NoProblem.Core.DataModel.ClipCall.SmsCampaignReport.SmsCampaignReportResponse>> result =
                new Result<List<NoProblem.Core.DataModel.ClipCall.SmsCampaignReport.SmsCampaignReportResponse>>();
            try
            {
            //    NoProblem.Core.BusinessLogic.SmsEngine.SmsManagerConsumers manager = new BusinessLogic.SmsEngine.SmsManagerConsumers();
                result.Value = NoProblem.Core.BusinessLogic.SmsEngine.SmsManagerConsumers.SmsCampaignReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SmsCampaignReport: from: {0}, to: {1}", request.From, request.To);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.ClipCall.Request.AddAarSupplierToLeadResponse>
            AddAarSupplierToLead(NoProblem.Core.DataModel.ClipCall.Request.AddAarSupplierToLeadRequest request)
        {
            NoProblem.Core.BusinessLogic.ClipCall.RequestReportManager rrm = new BusinessLogic.ClipCall.RequestReportManager();
            Result<NoProblem.Core.DataModel.ClipCall.Request.AddAarSupplierToLeadResponse> result =
                new Result<NoProblem.Core.DataModel.ClipCall.Request.AddAarSupplierToLeadResponse>();
            try
            {
                //    NoProblem.Core.BusinessLogic.SmsEngine.SmsManagerConsumers manager = new BusinessLogic.SmsEngine.SmsManagerConsumers();
                result.Value = rrm.AddAarSupplierToLead(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, string.Format("Exception in AddAarSupplierToLead: IncidentId:{0}, SupplierName:{1}, phone:{2}", request.incidentId, request.name, request.phoneNumber));
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.ClipCall.Advertiser.AdvertiserReportResponse>>
            ClipCallAdvertiserReport(NoProblem.Core.DataModel.ClipCall.Advertiser.AdvertiserReportRequest request)
        {
            Result<List<NoProblem.Core.DataModel.ClipCall.Advertiser.AdvertiserReportResponse>> result =
                new Result<List<NoProblem.Core.DataModel.ClipCall.Advertiser.AdvertiserReportResponse>>();
            try
            {
                NoProblem.Core.BusinessLogic.ClipCall.Suppliers.AdvertiserReportManager manager = new BusinessLogic.ClipCall.Suppliers.AdvertiserReportManager();
                result.Value = manager.AdvertiserReport(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ClipCallAdvertiserReport: from: {0}, to: {1}, stepInRegistration:{2}", request.From, request.To, request.stepInRegistration.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
       
       
    }
}
