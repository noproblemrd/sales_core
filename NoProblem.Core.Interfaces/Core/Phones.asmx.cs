﻿using System;
using System.ComponentModel;
using System.Web.Services;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel;
using System.Linq;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for DirectNumbers
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class Phones : NpServiceBase
    {
        [WebMethod]
        public DNQueryResult DNQuery(string DNQueryId, string DirectPhoneNumber, string consumerPhoneNumber)
        {
            LogUtils.MyHandle.WriteToLog(8, "DNQuery Started. DNQueryId: {0}, DirectPhoneNumber: {1}, consumerPhoneNumber: {2}", DNQueryId, DirectPhoneNumber, consumerPhoneNumber);
            DNQueryResult response = null;
            try
            {
                NoProblem.Core.BusinessLogic.Phones phonesLogic = new NoProblem.Core.BusinessLogic.Phones(null, null);
                response = phonesLogic.GetPhoneNumber(DNQueryId, DirectPhoneNumber, consumerPhoneNumber);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "Exception in DNQuery. DNQueryId: {0}, DirectPhoneNumber : {1}, consumerPhoneNumber: {2}", DNQueryId, DirectPhoneNumber, consumerPhoneNumber);
                response = new DNQueryResult();
            }
            LogUtils.MyHandle.WriteToLog(9, "_DNQuery ended. DNQueryId: {0}, DirectPhoneNumber: {1}, consumerPhoneNumber: {2}, retVal.TruePhoneNumber.FirstOrDefault = {4}", DNQueryId, DirectPhoneNumber, consumerPhoneNumber, response.TruePhoneNumber.FirstOrDefault());
            return response;
        }

        [WebMethod]
        public string AuctionCompleted(AuctionResult Result)
        {
            LogUtils.MyHandle.WriteToLog(8, "AuctionCompleted started. ServiceRequestId: {0}, Providers Count: {1}", Result.ServiceRequestId, Result.Providers.Length);
            string retVal = "0";
            try
            {
                NoProblem.Core.BusinessLogic.Dialer.CallManager callManager = new NoProblem.Core.BusinessLogic.Dialer.CallManager(null, null);
                callManager.AuctionCompleted(Result);
            }
            catch (Exception ex)
            {
                retVal = "1";
                LogUtils.MyHandle.HandleException(ex, "AuctionCompleted failed with exception");
            }
            LogUtils.MyHandle.WriteToLog(9, "_AuctionCompleted ended. ServiceRequestId: {0}, Providers Count: {1}. retVal = {2}", Result.ServiceRequestId, Result.Providers.Length, retVal);
            return retVal;
        }

        [WebMethod]
        public string CallStatus(
            string ServiceRequestUniqueId,
            string ProviderUniqueId,
            int StatusCode/*, 
            string session_id, 
            int siperrorcode, 
            string sipmessage*/
                               )
        {
            LogUtils.MyHandle.WriteToLog(8, "CallStatus started. serviceRequestUniqueId: {0}, providerUniqueId: {1}, StatusCode: {2}, astrixSessionId: {3}, siperrorcode: {4}, sipmessage: {5}", ServiceRequestUniqueId, ProviderUniqueId, StatusCode/*, session_id, siperrorcode, sipmessage*/);
            string retVal = "";
            try
            {
                BusinessLogic.Dialer.PhoneManager manager = new NoProblem.Core.BusinessLogic.Dialer.PhoneManager(null);
                retVal = manager.CallStatus(ServiceRequestUniqueId, ProviderUniqueId, StatusCode);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "CallStatus failed with exception");
            }
            LogUtils.MyHandle.WriteToLog(9, "_CallStatus ended. serviceRequestUniqueId: {0}, providerUniqueId: {1}, StatusCode: {2}. retVal = {3}", ServiceRequestUniqueId, ProviderUniqueId, StatusCode, retVal);
            return retVal;
        }

        [WebMethod]
        public string ConsumerContactedSucc(string ServiceRequestUniqueId, string ProviderUniqueId, string CallId, string TimeStamp, string Duration)
        {
            LogUtils.MyHandle.WriteToLog(8, "ConsumerContactedSucc started. ServiceRequestUniqueId: {0}, ProviderUniqueId: {1}, CallId: {2}, Duration: {3}", ServiceRequestUniqueId, ProviderUniqueId, CallId, Duration);
            string retVal = "0,Success";
            try
            {
                BusinessLogic.Dialer.PhoneManager manager = new NoProblem.Core.BusinessLogic.Dialer.PhoneManager(null);
                manager.ConsumerContactedSucc(ServiceRequestUniqueId, ProviderUniqueId, CallId, TimeStamp, Duration);
            }
            catch (Exception exc)
            {
                retVal = "1, error";
                LogUtils.MyHandle.HandleException(exc, "ConsumerContactedSucc failed with exception. requestId = {0}", ServiceRequestUniqueId);
            }
            LogUtils.MyHandle.WriteToLog(9, "_ConsumerContactedSucc ended. ServiceRequestUniqueId: {0}, ProviderUniqueId: {1}, CallId: {2}, Duration: {3}. retVal = {4}", ServiceRequestUniqueId, ProviderUniqueId, CallId, Duration, retVal);
            return retVal;
        }

        [WebMethod]
        public string ServiceRequestCompleted(string serviceRequestUniqueId, int reasonCode, string cdrxml)
        {
            LogUtils.MyHandle.WriteToLog(8, "ServiceRequestCompleted started. serviceRequestUniqueId: {0}, reasonCode: {1}, cdrxml: {2}", serviceRequestUniqueId, reasonCode, cdrxml);
            string retVal = string.Empty;
            try
            {
                BusinessLogic.Dialer.PhoneManager manager = new NoProblem.Core.BusinessLogic.Dialer.PhoneManager(null);
                manager.ServiceRequestCompleted(serviceRequestUniqueId, reasonCode, cdrxml);
                retVal = "0,Success";
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "ServiceRequestCompleted failed with exception. serviceRequestUniqueId = {0}, reasonCode = {1}", serviceRequestUniqueId, reasonCode);
                retVal = "2," + ex.Message;
            }
            LogUtils.MyHandle.WriteToLog(9, "_ServiceRequestCompleted ended. serviceRequestUniqueId: {0}, reasonCode: {1}, cdrxml: {2}. retVal = {3}", serviceRequestUniqueId, reasonCode, cdrxml, retVal);
            return retVal;
        }

        [WebMethod]
        public string ProviderDnContacted(
            string DNQueryId,
            string DirectPhoneNumber,
            string ConsumerPhoneNumber,
            int ReasonCode,
            string CallId,
            string TimeStamp,
            string Duration,
            string TruePhoneNumber)
        {
            LogUtils.MyHandle.WriteToLog(8, "ProviderDnContacted started. DNQueryId: {0}, DirectPhoneNumber: {1}, ConsumerPhoneNumber: {2}, ReasonCode: {3}, CallId: {4}, TimeStamp: {5}, Duration: {6}, TruePhoneNumber: {7}",
                DNQueryId, DirectPhoneNumber, ConsumerPhoneNumber, ReasonCode, CallId, TimeStamp, Duration, TruePhoneNumber);
            string retVal = string.Empty;
            try
            {
                NoProblem.Core.BusinessLogic.Phones handler = new NoProblem.Core.BusinessLogic.Phones(null, null);
                retVal = handler.DirectNumberContactedAsync(ConsumerPhoneNumber, DirectPhoneNumber, ReasonCode, CallId, int.Parse(Duration), DNQueryId, null, false, null, null, null, null);
            }
            catch (Exception ex)
            {
                retVal = "2," + ex.Message;
            }
            LogUtils.MyHandle.WriteToLog(9, "_ProviderDnContacted ended. DNQueryId: {0}, DirectPhoneNumber: {1}, ConsumerPhoneNumber: {2}, ReasonCode: {3}, CallId: {4}, TimeStamp: {5}, Duration: {6}, TruePhoneNumber: {7}, retVal = {8}",
                DNQueryId, DirectPhoneNumber, ConsumerPhoneNumber, ReasonCode, CallId, TimeStamp, Duration, TruePhoneNumber, retVal);
            return retVal;
        }

        [WebMethod]
        public string UpdateRecordingFileLocation(string CallId, string FileLocation)
        {
            LogUtils.MyHandle.WriteToLog(8, "UpdateRecordingFileLocation started. CallId: {0}, FileLocation: {1}", CallId, FileLocation);
            string retVal = string.Empty;
            try
            {
                NoProblem.Core.BusinessLogic.Phones handler = new NoProblem.Core.BusinessLogic.Phones(null, null);
                retVal = handler.UpdateRecordingFileLocation(CallId, FileLocation);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "UpdateRecordingFileLocation Failed with Exception");
                retVal = "1,UpdateRecordingFileLocation failed with exception: " + ex.Message;
            }
            LogUtils.MyHandle.WriteToLog(9, "_UpdateRecordingFileLocation ended. CallId: {0}, FileLocation: {1}. retVal = {2}", CallId, FileLocation, retVal);
            return retVal;
        }
    }
}
