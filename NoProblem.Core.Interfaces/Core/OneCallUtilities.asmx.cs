﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Web.Services;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.PushNotifications;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.OnecallUtilities;
using NoProblem.Core.DataModel.SupplierModel;
using System.Collections.Generic;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for OneCallUtilities
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class OneCallUtilities : NpServiceBase
    {
        [WebMethod]
        public bool SendTestPushNotificationIOS(DataModel.OnecallUtilities.SendTestPushRequest request)
        {
            //var sender = new BusinessLogic.PushNotifications.iOS.APNSServer(true);
            var sender = new BusinessLogic.PushNotifications.iOS.APNSServer(false);
            var dict = request.Data.ToDictionary(item => item.Key, item => item.Value);
            var pushRequest = new PushRequest
            {
                DeviceId = request.DeviceId,
                Data = dict,
                PushType = (ePushTypes) Enum.Parse(typeof (ePushTypes), request.PushType)
            };
            var result = sender.Send(pushRequest);
            return result.IsSuccess;
        }

        [WebMethod]
        public string SyncAllDapaz(string password)
        {
            NoProblem.Core.BusinessLogic.Dapaz.PpaOnlineSync.SyncAllManager manager = new BusinessLogic.Dapaz.PpaOnlineSync.SyncAllManager(password);
            manager.Start();
            return "Job started. Check logs for more info.";
        }

        [WebMethod]
        public void UpdateCoverAreas()
        {
            BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreasUpdater updater = new BusinessLogic.SupplierBL.Registration2014.CoverAreas.CoverAreasUpdater(null);
            updater.UpdateAreas();
        }

        //[WebMethod]
        //public Result<LoadAdvertisersResponse> LoadAdvertisers(LoadAdvertisersRequest request)
        //{
        //    LogUtils.MyHandle.WriteToLog("LoadAdvertisers started.");
        //    Result<LoadAdvertisersResponse> result = new Result<LoadAdvertisersResponse>();
        //    try
        //    {
        //        BusinessLogic.OneCallUtilities.OneCallUtilities utils = new BusinessLogic.OneCallUtilities.OneCallUtilities(null);
        //        utils.LoadAdvertisers(request);
        //    }
        //    catch (Exception exc)
        //    {
        //        result.Type = Result.eResultType.Failure;
        //        result.Messages.Add(exc.Message);
        //        LogUtils.MyHandle.HandleException(exc, "LoadAdvertisers failed with exception");
        //    }
        //    LogUtils.MyHandle.WriteToLog("LoadAdvertisers ended.");
        //    return result;
        //}       

        [WebMethod]
        public string test()
        {
            BusinessLogic.OneCallUtilities.OneCallUtilities o = new BusinessLogic.OneCallUtilities.OneCallUtilities(null);
            return o.stamTest();

        }

        [WebMethod]
        public string ResubmissionTest(string incidentid)
        {
            BusinessLogic.ServiceRequest.AutoResubmission.ResubmissionScheduler sch = new BusinessLogic.ServiceRequest.AutoResubmission.ResubmissionScheduler(
                new Guid(incidentid));
            sch.Start();
            return "ok";
        }

        [WebMethod]
        public void CheckRealTimeRoutingErrorPrevention()
        {
            BusinessLogic.RealTimeRoutingErrorPrevention p = new NoProblem.Core.BusinessLogic.RealTimeRoutingErrorPrevention();
            p.Check();
        }

        [WebMethod]
        public void StartDapazUsersSync()
        {
            BusinessLogic.Dapaz.UsersSync.UsersSyncManager.StartSyncNow();
        }

        [WebMethod]
        public void ImportZapAccounts(int top)
        {
            BusinessLogic.Dapaz.DapazAccountsOnlineSync.FirstTimeSyncManager manager =
                new NoProblem.Core.BusinessLogic.Dapaz.DapazAccountsOnlineSync.FirstTimeSyncManager();
            manager.Sync(top);
        }

        [WebMethod]
        public string ClearCache()
        {
            BusinessLogic.OneCallUtilities.OneCallUtilities utilities = new NoProblem.Core.BusinessLogic.OneCallUtilities.OneCallUtilities(null);
            utilities.ClearCache();
            return "Done";
        }

        [WebMethod]
        public string InitiateC2c(string originatorPhone, string targetPhone, string description, bool rejectEnabled)
        {
            BusinessLogic.ServiceRequest.JonaManager jona = new NoProblem.Core.BusinessLogic.ServiceRequest.JonaManager(null);
            return jona.InitiateCall(originatorPhone, targetPhone, description, rejectEnabled);
        }

        [WebMethod]
        public string InitiateTts(string phone, string description, bool rejectEnabled)
        {
            BusinessLogic.ServiceRequest.JonaManager jona = new NoProblem.Core.BusinessLogic.ServiceRequest.JonaManager(null);
            return jona.InitiateTts(phone, description, rejectEnabled);
        }

        [WebMethod]
        public string LoadRegionsFromXml(string xml)
        {
            BusinessLogic.Regions.RegionsManager manager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(null);
            manager.LoadRegoinFromXml(xml);
            return "done";
        }

        [WebMethod]
        public MiniSiteSupplierData GetMiniSiteSupplierData(string supplier)
        {
            try
            {
                if (string.IsNullOrEmpty(supplier))
                {
                    throw new ArgumentException("Argument cannot be null nor empty", "supplier");
                }
                return new BusinessLogic.SupplierManager(null).GetMiniSiteSupplierData(supplier);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetMiniSiteSupplierData. supplier = {0}", supplier);
            }
            return new MiniSiteSupplierData();
        }

        [WebMethod]
        public MiniSiteSupplierData GetGeneralSupplierData(Guid supplierId, string originCode)
        {
            try
            {
                return new BusinessLogic.SupplierManager(null).GetMiniSiteSupplierData(supplierId, originCode);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetGeneralSupplierData");
            }
            return new MiniSiteSupplierData();
        }

        [WebMethod]
        public string SendSmsTest(string supplierId)
        {
            BusinessLogic.Notifications notifier = new NoProblem.Core.BusinessLogic.Notifications(null);
            bool ans = notifier.NotifySupplier(enumSupplierNotificationTypes.SUPPLIER_SIGNUP, new Guid(supplierId), supplierId
                , "account", supplierId
                , "account");
            return ans.ToString();
        }

        [WebMethod]
        public string GiveFreeDirectNumbersToSuppliers(string originId)
        {
            Guid originIsG = new Guid(originId);
            BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
            manager.GiveDirectNumbersToSuppliers(originIsG);
            return "Done";
        }

        [WebMethod]
        public void GiveDirectNumbersInAllOrigins()
        {
            try
            {
                //LogUtils.MyHandle.WriteToLog(9, "GiveDirectNumbersInAllOrigins started.");
                //BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                //manager.GiveDirectNumbersToSuppliers();
                //LogUtils.MyHandle.WriteToLog(9, "GiveDirectNumbersInAllOrigins ended.");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GiveDirectNumbersInAllOrigins");
            }
        }

        [WebMethod]
        public Result UpdateSupplierBizId(UpdateSupplierBizIdRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.DataExport.DataExportManager manager = new NoProblem.Core.BusinessLogic.DataExport.DataExportManager(null);
                manager.UpdateSupplierBizId(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "UpdateSupplierBizId failed with exception");
            }
            return result;
        }

        [WebMethod]
        public void ExportData()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog("ExportData started");
                BusinessLogic.DataExport.DataExportManager man = new NoProblem.Core.BusinessLogic.DataExport.DataExportManager(null);
                man.Export();
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "ExportData failed with exception");
            }
        }

        [WebMethod]
        public Result SendScheduledServices()
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "SendScheduledServices started.");
                Thread tr = new Thread(delegate()
                {
                    try
                    {
                        BusinessLogic.ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                        manager.SendScheduledServices();
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "SendScheduledServices inner async part failed with exception");
                    }
                });
                tr.Start();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SendScheduledServices failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result CreateAudit(DataModel.Audit.AuditRequest request)
        {
            Result result = new Result();
            try
            {
                if (request.AuditOn)
                {
                    BusinessLogic.Audit.AuditManager auditManager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                    auditManager.CreateAudit(request.AuditEntries);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "CreateAudit failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public void SendDelayedNotifications()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(10, "SendDelayedNotifications started.");
                BusinessLogic.ServiceRequest.ServiceRequestManager man = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                man.SendDelayedNotifications();
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SendDelayedNotifications failed with exception");
            }
        }

        [WebMethod]
        public void CreateDashboardData()
        {
            try
            {
                BusinessLogic.Dashboard.DashboardDataCreator dataCreator = new NoProblem.Core.BusinessLogic.Dashboard.DashboardDataCreator(null);
                dataCreator.CreateData();
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "CreateDashboardData failed with exception");
            }
        }

        [WebMethod]
        public void RecalculateDashboardData()
        {
            try
            {
                BusinessLogic.Dashboard.DashboardDataCreator dataCreator = new NoProblem.Core.BusinessLogic.Dashboard.DashboardDataCreator(null);
                dataCreator.RecalculateData();
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "RecalculateDashboardData failed with exception");
            }
        }

        [WebMethod]
        public void DoMonthlyDeposits()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog("DoMonthlyDeposits started");
                BusinessLogic.SupplierPricing.SupplierPricingManager manager =
                    new NoProblem.Core.BusinessLogic.SupplierPricing.SupplierPricingManager(null);
                manager.DoMonthlyDeposits();
                LogUtils.MyHandle.WriteToLog("DoMonthlyDeposits ended");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DoMonthlyDeposits");
            }
        }

        [WebMethod]
        public void CampaignStarter()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "CampaignStarter started");
                BusinessLogic.Campaigns.CampaignsManager man = new NoProblem.Core.BusinessLogic.Campaigns.CampaignsManager(null);
                man.InitiateCampaignsDailyActivities();
                LogUtils.MyHandle.WriteToLog(9, "CampaignStarter ended");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CampaignStarter");
            }
        }

        [WebMethod]
        public void WeeklyLostCallsNotification()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "WeeklyLostCallsNotification started");
                BusinessLogic.Campaigns.LosersCampaignManager manager = new NoProblem.Core.BusinessLogic.Campaigns.LosersCampaignManager(null);
                manager.StartLoserCampaign();
                LogUtils.MyHandle.WriteToLog(9, "WeeklyLostCallsNotification ended");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WeeklyLostCallsNotification");
            }
        }

        [WebMethod]
        public void AutoUpsaleStarter()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "AutoUpsaleStarter started");
                BusinessLogic.ServiceRequest.AutoUpsaleManager auManager = new NoProblem.Core.BusinessLogic.ServiceRequest.AutoUpsaleManager(null);
                auManager.StartAutoUpsales();
                LogUtils.MyHandle.WriteToLog(9, "AutoUpsaleStarter ended");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AutoUpsaleStarter");
            }
        }

        [WebMethod]
        public void DateLocalfixer()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "DateLocalfixer started");
                BusinessLogic.Utils.DateLocalFixer fixer = new NoProblem.Core.BusinessLogic.Utils.DateLocalFixer();
                fixer.FillLocalDates();
                LogUtils.MyHandle.WriteToLog(9, "DateLocalfixer ended");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DateLocalfixer");
            }
        }

        [WebMethod]
        public void CreateSupplierFromAPI()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "CreateSupplierFromAPI started");
                BusinessLogic.SupplierManager manager = new NoProblem.Core.BusinessLogic.SupplierManager(null);
                manager.CreateSavedSuppliers();
                LogUtils.MyHandle.WriteToLog(9, "CreateSupplierFromAPI ended");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateSupplierFromAPI");
            }
        }

        [WebMethod]
        public void DoAarMachineCheck()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "DoAarMachineCheck started");
                //BusinessLogic.AAR.AarManager manager = new NoProblem.Core.BusinessLogic.AAR.AarManager(null);
                //manager.DoMachineTests();
                LogUtils.MyHandle.WriteToLog(9, "DoAarMachineCheck ended");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DoAarMachineCheck");
            }
        }

        [WebMethod]
        public void OnDayLightSavingClockChange()
        {
            try
            {
                BusinessLogic.SupplierBL.Registration2014.WorkingHours.WorkingHoursManager manager =
                    new BusinessLogic.SupplierBL.Registration2014.WorkingHours.WorkingHoursManager(null);
                manager.OnDayLightSavingClockChange();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in OnDayLightSavingClockChange");
            }
        }

        [WebMethod]
        public void CheckPendingRecharges()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "CheckPendingRecharges started");
                BusinessLogic.SupplierPricing.PendingRechargesChecker checker = new NoProblem.Core.BusinessLogic.SupplierPricing.PendingRechargesChecker(null);
                checker.CheckAllPending();
                LogUtils.MyHandle.WriteToLog(9, "CheckPendingRecharges ended");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckPendingRecharges");
            }
        }

        [WebMethod]
        public void CleanDynamicDIDAllocations()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "CleanDynamicDIDAllocatoins started");
                BusinessLogic.DynamicDIDs.AllocationManager manager = new NoProblem.Core.BusinessLogic.DynamicDIDs.AllocationManager(null);
                manager.CleanAllocations();
                LogUtils.MyHandle.WriteToLog(9, "CleanDynamicDIDAllocatoins ended");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CleanDynamicDIDAllocatoins");
            }
        }

        [WebMethod]
        public void LoadScrapedAdvertisers()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(10, "LoadScrapedAdvertisers started");
                //BusinessLogic.AAR.AutoScraper scraper = new NoProblem.Core.BusinessLogic.AAR.AutoScraper(null);
                //scraper.LoadScrapedAdvertisers();
                LogUtils.MyHandle.WriteToLog(10, "LoadScrapedAdvertisers ended");
            }
            catch (ThreadAbortException threadAbortException)
            {
                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                Thread.ResetAbort();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in LoadScrapedAdvertisers");
            }
        }

        [WebMethod]
        public void CalculateFinancialDashboard(int daysBack, bool doMissing)
        {
            Thread tr = new Thread(delegate()
            {
                try
                {
                    BusinessLogic.Dashboard.FinancialDashboard.DataCreator creator = new NoProblem.Core.BusinessLogic.Dashboard.FinancialDashboard.DataCreator(null);
                    creator.CalculatePastDays(daysBack, doMissing);
                    LogUtils.MyHandle.WriteToLog("CalculateFinancialDashboard finished");
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in CalculateFinancialDashboard");
                }
            });
            tr.Start();
        }
    }
}
