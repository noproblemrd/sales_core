﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Services;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic;
using NoProblem.Core.BusinessLogic.PublisherPortal;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.AdvertiserRankings;
using NoProblem.Core.DataModel.PublisherPortal;
using NoProblem.Core.DataModel.PublisherPortal.BadWords;
using NoProblem.Core.DataModel.Refunds;
using System.Threading;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for Site
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Site : NpServiceBase
    {
        [WebMethod]
        public Result<List<DataModel.Expertise.KeywordWithCategory>> GetAllKeywordsWithCategory()
        {
            Result<List<DataModel.Expertise.KeywordWithCategory>> result = new Result<List<DataModel.Expertise.KeywordWithCategory>>();
            try
            {
                BusinessLogic.Expertises.ExpertiseManager manager = new BusinessLogic.Expertises.ExpertiseManager(null);
                result.Value = manager.GetAllKeywordsWithCategories();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllKeywordsWithCategory");
            }
            return result;
        }

        [WebMethod]
        public void DoMonthlyDepositsTestFirstOfTheMonth()
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "DoMonthlyDepositsTestFirstOfTheMonth started");
                string siteId = System.Configuration.ConfigurationManager.AppSettings.Get("SiteId").ToLower();
                if (siteId == "sales" || siteId == "np_israel")
                {
                    throw new Exception("Cannot run this test in production environments");
                }
                BusinessLogic.SupplierPricing.SupplierPricingManager manager =
                    new NoProblem.Core.BusinessLogic.SupplierPricing.SupplierPricingManager(null);
                manager.DoMonthlyDeposits(true);
                LogUtils.MyHandle.WriteToLog(9, "DoMonthlyDepositsTestFirstOfTheMonth ended");
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DoMonthlyDepositsTestFirstOfTheMonth");
            }
        }

        [WebMethod]
        public Result StopRequestManually(NoProblem.Core.DataModel.Case.StopRequestManuallyRequest request)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.ServiceRequest.RequestStopper manager = new NoProblem.Core.BusinessLogic.ServiceRequest.RequestStopper(null);
                manager.StopRequestManually(request.CaseId, request.UserId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in StopRequestManually. caseId:{0}. userId:{1}", request.CaseId, request.UserId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<bool> IsPublisherUser(Guid userId)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "IsPublisherUser started. userId = {0}", userId);
                SupplierManager manager = new SupplierManager(null);
                result.Value = manager.IsPublisherUser(userId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IsPublisherUser. userId = {0}", userId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }
        [WebMethod]
        public Result<string> GetPublisherUserName(Guid userId)
        {
            Result<string> result = new Result<string>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetPublisherUserName started. userId = {0}", userId);
                SupplierManager manager = new SupplierManager(null);
                result.Value = manager.GetPublisherUserName(userId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetPublisherUserName. userId = {0}", userId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<string> GetChargingErrorUserEmail()
        {
            Result<string> result = new Result<string>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetChargingErrorUserEmail started");
                BusinessLogic.SupplierPricing.SupplierPricingManager manager = new NoProblem.Core.BusinessLogic.SupplierPricing.SupplierPricingManager(null);
                result.Value = manager.GetChargingErrorUserEmail();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetChargingErrorUserEmail");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result UpsertTableData(DataModel.Tables.UpsertTableDataRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpsertTableData started");
                BusinessLogic.Tables.TablesManager manager = new NoProblem.Core.BusinessLogic.Tables.TablesManager(null);
                manager.UpsertTableData(request.Type, request.Guid, request.Name);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpsertTableData failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result DeleteTablesData(DataModel.Tables.eTableType type, List<Guid> ids)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DeleteTablesDate started");
                BusinessLogic.Tables.TablesManager manager = new NoProblem.Core.BusinessLogic.Tables.TablesManager(null);
                manager.DeleteTableData(type, ids);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "DeleteTablesDate failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result UpsertUnavailabilityReason(DataModel.Tables.UpsertUnavailabilityReasonRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpsertUnavailabilityReason started");
                BusinessLogic.Tables.TablesManager manager = new NoProblem.Core.BusinessLogic.Tables.TablesManager(null);
                manager.UpsertUnavailabilityReason(request.Guid, request.Name, request.IsSystem, request.GroupId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpsertUnavailabilityReason failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result DeleteUnavailabilityReasons(List<Guid> reasonIds)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DeleteUnavailabilityReasons started");
                BusinessLogic.Tables.TablesManager manager = new NoProblem.Core.BusinessLogic.Tables.TablesManager(null);
                manager.DeleteUnavailabilityReasons(reasonIds);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "DeleteUnavailabilityReasons failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Tables.TableRowData>> GetTableData(DataModel.Tables.eTableType type)
        {
            Result<List<DataModel.Tables.TableRowData>> result = new Result<List<DataModel.Tables.TableRowData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetTableData started");
                BusinessLogic.Tables.TablesManager manager = new NoProblem.Core.BusinessLogic.Tables.TablesManager(null);
                result.Value = manager.GetTableData(type);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetTableData failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.Dashboard.Convertion.ExposuresPicklistsContainer> GetExposuresPicklists()
        {
            Result<NoProblem.Core.DataModel.Dashboard.Convertion.ExposuresPicklistsContainer> result = new Result<NoProblem.Core.DataModel.Dashboard.Convertion.ExposuresPicklistsContainer>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetExposuresPicklists started");
                BusinessLogic.Dashboard.ConvertionsManager manager = new NoProblem.Core.BusinessLogic.Dashboard.ConvertionsManager(null);
                result.Value = manager.GetExposuresPicklists();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetExposuresPicklists failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<Guid> CreateNote(DataModel.Notes.NoteData note, DataModel.Notes.NoteType type)
        {
            Result<Guid> result = new Result<Guid>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CreateNote started");
                BusinessLogic.Notes.NotesManager manager = new NoProblem.Core.BusinessLogic.Notes.NotesManager(null);
                result.Value = manager.CreateNote(note, type);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "CreateNote failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Notes.NoteData>> GetNotes(Guid caseId, DataModel.Notes.NoteType type)
        {
            Result<List<DataModel.Notes.NoteData>> result = new Result<List<DataModel.Notes.NoteData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetNotes started. caseId = {0}", caseId);
                BusinessLogic.Notes.NotesManager manager = new NoProblem.Core.BusinessLogic.Notes.NotesManager(null);
                result.Value = manager.GetNotes(caseId, type);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetNotes failed with exception. caseId = {0}", caseId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Case.CaseData> GetCaseData(Guid caseId)
        {
            Result<DataModel.Case.CaseData> result = new Result<NoProblem.Core.DataModel.Case.CaseData>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetCaseData started. caseId = {0}", caseId);
                BusinessLogic.ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(null);
                result.Value = manager.GetCaseData(caseId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetCaseData failed with exception. caseId = {0}", caseId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result DeleteObjectives(List<Guid> objectives)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DeleteObjectives started");
                BusinessLogic.Dashboard.ObjectivesManager manager = new NoProblem.Core.BusinessLogic.Dashboard.ObjectivesManager(null);
                manager.DeleteObjectives(objectives);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "DeleteObjectives failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Dashboard.CreateObjectiveResponse> CreateObjective(DataModel.Dashboard.CreateObjectiveRequest request)
        {
            Result<DataModel.Dashboard.CreateObjectiveResponse> result = new Result<NoProblem.Core.DataModel.Dashboard.CreateObjectiveResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CreateObjective started. request: From = {0}, To = {1}, Amount = {2}, Criteria = {3}, ObjectiveId = {4}, IsApproved = {5}.",
                    request.FromDate.ToString(), request.ToDate.ToString(), request.Amount.ToString(), request.Criteria.ToString(), request.ObjectiveId.ToString(), request.IsApproved.ToString());
                BusinessLogic.Dashboard.ObjectivesManager manager = new NoProblem.Core.BusinessLogic.Dashboard.ObjectivesManager(null);
                result.Value = manager.CreateObjective(request);
                LogUtils.MyHandle.WriteToLog(8, "CreateObjective ending. response: NeedsApproval = {0}.", result.Value.NeedsApproval.ToString());
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "CreateObjective failed with exception. request: From = {0}, To = {1}, Amount = {2}, Criteria = {3}, ObjectiveId = {4}, IsApproved = {5}.",
                    request.FromDate.ToString(), request.ToDate.ToString(), request.Amount.ToString(), request.Criteria.ToString(), request.ObjectiveId.ToString(), request.IsApproved.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Dashboard.ObjectiveData>> GetAllObjectives()
        {
            Result<List<DataModel.Dashboard.ObjectiveData>> result = new Result<List<DataModel.Dashboard.ObjectiveData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllObjectives started.");
                BusinessLogic.Dashboard.ObjectivesManager manager = new NoProblem.Core.BusinessLogic.Dashboard.ObjectivesManager(null);
                result.Value = manager.GetAllObjectives();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetAllObjectives failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<string>> GetAllSuppliersNames()
        {
            Result<List<string>> result = new Result<List<string>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllSuppliers started");
                PublisherPortalManager manager = new PublisherPortalManager(null, null);
                result.Value = manager.GetAllSuppliersNames();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetAllSuppliers failed with exception.");
            }
            return result;
        }

        [WebMethod]
        public Result<List<List<GuidStringPair>>> GetAllPayStatusAndReasons()
        {
            Result<List<List<GuidStringPair>>> result = new Result<List<List<GuidStringPair>>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllPayStatusAndReasons started");
                NoProblem.Core.BusinessLogic.Origins.AffiliatesManager manaer = new NoProblem.Core.BusinessLogic.Origins.AffiliatesManager(null);
                result.Value = manaer.GetAllPayStatusAndReasons();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "GetAllPayStatusAndReasons failed with exception.");
            }
            return result;
        }

        [WebMethod]
        public Result UpdateAffiliatePayStatus(UpdateAffiliatePayStatusRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateAffiliatePayStatus started. caseId = {0}, payStatusId = {1}, psyReasonId = {2}",
                    request.CaseId, request.PayStatusId, request.PayReasonId);
                NoProblem.Core.BusinessLogic.Origins.AffiliatesManager manager = new NoProblem.Core.BusinessLogic.Origins.AffiliatesManager(null);
                manager.UpdateAffiliatePayStatus(request.CaseId, request.PayStatusId, request.PayReasonId);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "UpdateAffiliatePayStatus failed with exception. caseId = {0}, payStatusId = {1}, psyReasonId = {2}",
                    request.CaseId, request.PayStatusId, request.PayReasonId);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.PublisherPortal.GuidStringPair>> GetAllSupplierNamesAndIds()
        {
            Result<List<DataModel.PublisherPortal.GuidStringPair>> result = new Result<List<DataModel.PublisherPortal.GuidStringPair>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllSupplierNamesAndIds started");
                PublisherPortalManager manager = new PublisherPortalManager(null, null);
                result.Value = manager.GetAllSupplierNamesAndIds();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetAllSupplierNamesAndIds failed with exception.");
            }
            return result;
        }

        [WebMethod]
        public Result<List<GuidUserNamePair>> GetUserNames(GetUserNamesRequest request)
        {
            Result<List<GuidUserNamePair>> result = new Result<List<GuidUserNamePair>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetUserNames started");
                PublisherPortalManager manager = new PublisherPortalManager(null, null);
                result.Value = manager.GetUserNames(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetUserNames failed with exception.");
            }
            return result;
        }

        [WebMethod]
        public Result<CreateBadWordResponse> CreateBadWord(CreateBadWordRequest request)
        {
            Result<CreateBadWordResponse> result = new Result<CreateBadWordResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CreateBadWord started. Word = {0}", request.Word);
                BadWordManager manager = new BadWordManager(null);
                CreateBadWordResponse response = manager.CreateBadWord(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "CreateBadWord failed with exception. Word = {0}", request.Word);
            }
            return result;
        }

        [WebMethod]
        public Result DeleteBadWord(DeleteBadWordRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DeleteBadWord started. BadWordId = {0}", request.BadWordId);
                BadWordManager manager = new BadWordManager(null);
                manager.DeleteBadWord(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "DeleteBadWord failed with exception. BadWordId = {0}", request.BadWordId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<GetAllBadWordsResponse> GetAllBadWords()
        {
            Result<GetAllBadWordsResponse> result = new Result<GetAllBadWordsResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllBadWords started.");
                BadWordManager manager = new BadWordManager(null);
                GetAllBadWordsResponse response = manager.GetAllBadWords();
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetAllBadWords failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<NoProblem.Core.DataModel.PublisherPortal.MultipleExpertiseCreator.InsertMultipleExpertisesResponse> InsertMultipleExpertises(NoProblem.Core.DataModel.PublisherPortal.MultipleExpertiseCreator.InsertMultipleExpertisesRequest request)
        {
            Result<NoProblem.Core.DataModel.PublisherPortal.MultipleExpertiseCreator.InsertMultipleExpertisesResponse> result = new Result<NoProblem.Core.DataModel.PublisherPortal.MultipleExpertiseCreator.InsertMultipleExpertisesResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "InsertMultipleExpertises started");
                ExpertiseManager manager = new ExpertiseManager(null);
                NoProblem.Core.DataModel.PublisherPortal.MultipleExpertiseCreator.InsertMultipleExpertisesResponse response = manager.InsertMultipleExpertises(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "InsertMultipleExpertises failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<GetAllPricingPackagesResponse> GetAllPricingPackages()
        {
            Result<GetAllPricingPackagesResponse> result = new Result<GetAllPricingPackagesResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllPricingPackages started.");
                PricingPackagesManager manager = new PricingPackagesManager(null);
                GetAllPricingPackagesResponse response = manager.GetAllPricingPackages(DataModel.Xrm.new_pricingpackage.PricingPackageSupplierType.All);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetAllPricingPackages failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<CreateNewPricingPackageResponse> CreateNewPricingPackage(CreateNewPricingPackageRequest request)
        {
            Result<CreateNewPricingPackageResponse> result = new Result<CreateNewPricingPackageResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "CreateNewPricingPackage started. request: Name = {0}, FromAmount = {1}, BonusAmount = {2}", request.Name, request.FromAmount, request.BonusPercent);
                PricingPackagesManager manager = new PricingPackagesManager(null);
                CreateNewPricingPackageResponse response = manager.CreateNewPricingPackage(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "CreateNewPricingPackage failed with exception. request: Name = {0}, FromAmount = {1}, BonusAmount = {2}", request.Name, request.FromAmount, request.BonusPercent);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result UpdatePricingPackage(UpdatePricingPackageRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdatePricingPackage started. request: id = {0} ", request.PricingPackageId.ToString());
                PricingPackagesManager manager = new PricingPackagesManager(null);
                manager.UpdatePricingPackage(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpdatePricingPackage started. request: id = {0} ", request.PricingPackageId.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result SetPricingPackageState(SetPricingPackageStateRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SetPricingPackageState started. request: id = {0}, IsActive = {1} ", request.PricingPackageId.ToString(), request.IsActive.ToString());
                PricingPackagesManager manager = new PricingPackagesManager(null);
                manager.SetPricingPackageState(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SetPricingPackageState started. request: id = {0}, IsActive = {1} ", request.PricingPackageId.ToString(), request.IsActive.ToString());
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<SetBlackListStatusResponse> SetBlackListStatus(SetBlackListStatusRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, "SetBlackListStatus stated. PhoneNUmber = {0}, IncidentId = {1}, BlackListStatusToSet = {2}", string.IsNullOrEmpty(request.PhoneNumber) == false ? request.PhoneNumber : string.Empty, request.IncidentId.ToString(), request.BlackListStatusToSet.ToString());
            Result<SetBlackListStatusResponse> result = new Result<SetBlackListStatusResponse>();
            try
            {
                bool valid = true;
                if (string.IsNullOrEmpty(request.PhoneNumber) && request.IncidentId == Guid.Empty)
                {
                    result.Messages.Add("PhoneNumber or IncidentId must be provided.");
                    result.Type = Result.eResultType.Failure;
                    valid = false;
                }
                if (valid)
                {
                    ConsumersManager consumerManager = new ConsumersManager(null);
                    SetBlackListStatusResponse response = consumerManager.SetBlackListStatus(request);
                    result.Value = response;
                }
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "SetBlackListStatus failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result<SearchBlackListResponse> SearchBlackList(SearchBlackListRequest request)
        {
            Result<SearchBlackListResponse> result = new Result<SearchBlackListResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "SearchBlackList started. PhoneNumber = {0}", request.PhoneNumber);
                ConsumersManager manager = new ConsumersManager(null);
                SearchBlackListResponse response = manager.SearchBlackList(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SearchBlackList failed with exception. PhoneNumber = {0}", request.PhoneNumber);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<List<GuidStringPair>> GetAllOrigins(bool getSystemOrigins)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetAllOrigins Started.");
            Result<List<GuidStringPair>> result = new Result<List<GuidStringPair>>();
            try
            {
                NoProblem.Core.BusinessLogic.Origins.AffiliatesManager manager = new NoProblem.Core.BusinessLogic.Origins.AffiliatesManager(null);
                result.Value = manager.GetAllOrigins(getSystemOrigins);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetAllOrigins failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result<Guid> GetDefaultOrigin()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetDefaultOrigin Started.");
            Result<Guid> result = new Result<Guid>();
            try
            {
                NoProblem.Core.BusinessLogic.Origins.AffiliatesManager manager = new NoProblem.Core.BusinessLogic.Origins.AffiliatesManager(null);
                result.Value = manager.GetDefaultOrigin();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetDefaultOrigin");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.PublisherPortal.AffiliateUserData>> GetAllAffiliateUsers()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetAllAffiliateUsers Started.");
            Result<List<DataModel.PublisherPortal.AffiliateUserData>> result = new Result<List<DataModel.PublisherPortal.AffiliateUserData>>();
            try
            {
                NoProblem.Core.BusinessLogic.Origins.AffiliatesManager manager = new NoProblem.Core.BusinessLogic.Origins.AffiliatesManager(null);
                result.Value = manager.GetAllAffiliateUsers();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetAllAffiliateUsers failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result<GetAllPublishersResponse> GetAllPublishers()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetAllPublishers Started.");
            Result<GetAllPublishersResponse> result = new Result<GetAllPublishersResponse>();
            try
            {
                PublisherPortalManager ppManager = new PublisherPortalManager(null, null);
                result.Value = ppManager.GetAllPublishers();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetAllPublishers failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result<bool> IsCanHearAllRecordings(Guid userId)
        {
            LogUtils.MyHandle.WriteToLog(8, "IsCanHearAllRecordings Started.");
            Result<bool> result = new Result<bool>();
            try
            {
                PublisherPortalManager ppManager = new PublisherPortalManager(null, null);
                result.Value = ppManager.IsCanHearAllRecordings(userId);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "IsCanHearAllRecordings failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result DeletePublishers(DeletePublishersRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, "DeletePublishers Started.");
            Result result = new Result();
            try
            {
                PublisherPortalManager ppManager = new PublisherPortalManager(null, null);
                ppManager.DeletePublishers(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "DeletePublishers failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result DeleteExpertises(DeleteExpertisesRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, "DeleteExpertises Started.");
            Result result = new Result();
            try
            {
                ExpertiseManager manager = new ExpertiseManager(null);
                manager.DeleteExpertises(request);

            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "DeleteExpertises failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result<GetRefundsReportResponse> GetRefundsReport(GetRefundsReportRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8,
                "GetRefundsReport Started - RefundStatus = {0}, FromDate = {1}, ToDate = {2}",
                (request.RefundStatus.HasValue ? request.RefundStatus.Value.ToString() : ""), request.FromDate.ToString(), request.ToDate.ToString());
            Result<GetRefundsReportResponse> result = new Result<GetRefundsReportResponse>();
            try
            {
                BusinessLogic.Refunds.RefundsManager manager = new NoProblem.Core.BusinessLogic.Refunds.RefundsManager(null);
                GetRefundsReportResponse response = manager.GetRefundsReport(request);
                result.Value = response;
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetRefundsReport failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result<bool> RefundFromMobile(DataModel.Refunds.RefundFromMobileRequest request)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.Refunds.RefundsManager manager =
                    new BusinessLogic.Refunds.RefundsManager(null);
                result.Value = manager.RefundFromMobile(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in RefundFromMobile");
            }
            return result;
        }

        [WebMethod]
        public Result<UpsertRefundResponse> UpsertRefund(UpsertRefundRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8,
                "UpsertRefund started - supplierId = {0}, incidentId = {1}, refundReasonCode = {2}, refundNote = {3}, refundOwner = {4}, refundStatus = {5}",
                request.SupplierId, request.IncidentId, request.RefundReasonCode, request.RefundNote, request.RefundOwnerId, request.RefundStatus.ToString());
            Result<UpsertRefundResponse> result = new Result<UpsertRefundResponse>();
            try
            {
                bool valid = true;
                if (request.IncidentId == Guid.Empty)
                {
                    result.Messages.Add("IncidentId cannot be Guid.Empty, please provide a valid incident's Guid");
                    valid = false;
                }
                if (request.SupplierId == Guid.Empty)
                {
                    result.Messages.Add("SupplierId cannot be Guid.Empty, please provide a valid supplier's Guid");
                    valid = false;
                }
                if (valid)
                {
                    BusinessLogic.Refunds.RefundsManager manager = new NoProblem.Core.BusinessLogic.Refunds.RefundsManager(null);
                    result.Value = manager.UpsertRefund(request);
                }
                else
                {
                    result.Type = Result.eResultType.Failure;
                }
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "UpsertRefund failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result<GetAllRefundReasonsResponse> GetAllRefundReasons()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetAllRefundReasons started");
            Result<GetAllRefundReasonsResponse> result = new Result<GetAllRefundReasonsResponse>();
            try
            {
                BusinessLogic.Refunds.RefundsManager manager = new NoProblem.Core.BusinessLogic.Refunds.RefundsManager(null);
                GetAllRefundReasonsResponse response = manager.GetAllRefundReasons();
                result.Value = response;
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetAllRefundReasons failed with exception");
            }
            return result;
        }

        [WebMethod]
        public string GetAllExpertises(string siteId, string supplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetAllExpertises - supplierId: {0}", supplierId.ToString());
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetAllExpertises(supplierId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetAllExpertises failed with exception");
                retVal = "<Expertise><Error>Failed</Error></Expertise>";
            }
            return retVal;
        }

        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.Expertise.ExpertiseWithIsActive>> GetAllExpertisesWithIsActive()
        {
            Result<List<NoProblem.Core.DataModel.Expertise.ExpertiseWithIsActive>> result =
                new Result<List<NoProblem.Core.DataModel.Expertise.ExpertiseWithIsActive>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllExpertisesWithIsActive started.");
                BusinessLogic.Expertises.ExpertiseManager manager = new NoProblem.Core.BusinessLogic.Expertises.ExpertiseManager(null);
                result.Value = manager.GetAllExpertisesWithHasSuppliers();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetAllExpertisesWithIsActive failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result SetRankingParametersValues(SetAdvertiserRankingsRequest request)
        {
            LogUtils.MyHandle.WriteToLog(8, "SetRankingParametersValues started.");
            Result result = new Result();
            try
            {
                AdvertiserRankingsManager manager = new AdvertiserRankingsManager(null);
                manager.SetRankingParametersValues(request);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "SetRankingParametersValues failed with exception");
            }
            return result;
        }

        [WebMethod]
        public Result<GetRankingParametersResponse> GetRankingParameters()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetRankingParameters started");
            Result<GetRankingParametersResponse> result = new Result<GetRankingParametersResponse>();
            try
            {
                AdvertiserRankingsManager manager = new AdvertiserRankingsManager(null);
                GetRankingParametersResponse response = manager.GetRankingParameters();
                result.Value = response;
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                LogUtils.MyHandle.HandleException(exc, "GetRankingParameters failed with exception");
            }
            return result;
        }

        [WebMethod]
        public string GetExpertise(string SiteId, string PrimaryExpertisePreFix)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetExpertise - SiteId: {0}, PrimaryExpertisePreFix: {1}", SiteId, PrimaryExpertisePreFix);
            string retVal = "";
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                retVal = publisherUtils.GetExpertise(SiteId, PrimaryExpertisePreFix);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetExpertise failed with exception");
                retVal = "<Expertise><Error>Failed</Error></Expertise>";
            }

            return retVal;
        }

        [WebMethod]
        public string GetPrimaryExpertise(string SiteId, string PrimaryExpertisePreFix)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetPrimaryExpertise - SiteId: {0}, PrimaryExpertisePreFix: {1}", SiteId, PrimaryExpertisePreFix);
            string retVal = "";
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                retVal = publisherUtils.GetPrimaryExpertise(SiteId, PrimaryExpertisePreFix);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetPrimaryExpertise failed with exception");
                retVal = "<PrimaryExpertise><Error>Failed</Error></PrimaryExpertise>";
            }

            return retVal;
        }

        [WebMethod]
        public string GetSecondaryExpertise(string SiteId, string PrimaryExpertiseId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetSecondaryExpertise - SiteId: {0}, PrimaryExpertiseId: {1}", SiteId, PrimaryExpertiseId);
            string retVal = "";
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                retVal = publisherUtils.GetSecondaryExpertise(SiteId, PrimaryExpertiseId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSecondaryExpertise failed with exception");
                retVal = "<PrimaryExpertise><Error>Failed</Error></PrimaryExpertise>";
            }

            return retVal;
        }

        [WebMethod]
        public string UpsertPrimaryExpertise(string Request)
        {
            LogUtils.MyHandle.WriteToLog(8, "UpsertPrimaryExpertise - Request: {0}", Request);
            string retVal = "";
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                retVal = publisherUtils.UpsertPrimaryExpertise(Request);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "UpsertPrimaryExpertise failed with exception");
                retVal = "<PrimaryExpertiseUpsert><Error>Failed</Error></PrimaryExpertiseUpsert>";
            }

            return retVal;
        }

        [WebMethod]
        public string GetIncidentPackages()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetIncidentPackages Started");
            string retVal = "";
            try
            {
                BusinessLogic.ServiceRequest.Incident incidentLogic = new NoProblem.Core.BusinessLogic.ServiceRequest.Incident(null, null);
                retVal = incidentLogic.GetIncidentPackages();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetIncidentPackages Failed with Exception");
                retVal = "<IncidentPackages><Error>Failed</Error></IncidentPackages>";
            }

            return retVal;
        }

        [WebMethod]
        public string GetConfigurationSettings()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetConfigurationSettings Started");
            string retVal = "";
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                retVal = publisherUtils.GetConfigurationSettings();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetConfigurationSettings Failed with Exception");
                retVal = "<Configurations><Error>Failed</Error></Configurations>";
            }
            LogUtils.MyHandle.WriteToLog(9, "_GetConfigurationSettings Ended");
            return retVal;
        }

        [WebMethod]
        public string GetSupplierLogs(string SiteId, string SupplierId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetSupplierLogs - SiteId: {0}, SupplierId: {1}", SiteId, SupplierId);
            string retVal = "";
            try
            {
                BusinessLogic.Utils.EntityUtils entityUtils = new NoProblem.Core.BusinessLogic.Utils.EntityUtils(null);
                retVal = entityUtils.GetEntityLogs("account", SupplierId);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSupplierLogs");
                retVal = "";
            }
            return retVal;
        }

        [WebMethod]
        public string UpdateConfigurationSettings(string key, string value)
        {
            LogUtils.MyHandle.WriteToLog(8, "UpdateConfigurationSettings - key: {0}, value: {1}", key, value);
            string retVal = String.Empty;
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                publisherUtils.UpdateConfigurationSettings(key, value);
                retVal = "<UpdateConfigurationSettings><Status>Success</Status></UpdateConfigurationSettings>";
                return retVal;
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "UpdateConfigurationSettings Failed with exception");
                return "<UpdateConfigurationSettings><Error>Failed</Error></UpdateConfigurationSettings>";
            }
        }

        [WebMethod]
        public Result UpdateManyConfigurationSettings(List<StringPair> configurationSettingsList)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateManyConfigurationSettings started");
                PublisherPortalManager manager = new PublisherPortalManager(null, null);
                manager.UpdateConfigurationSettings(configurationSettingsList);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpdateManyConfigurationSettings failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        #region TimeZone functions

        [WebMethod]
        public string GetAllTimeZones()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetAllTimeZones started.");
            string retVal = "";

            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetAllTimeZones();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSumIncomingFromSupplierReport Failed with exception");
                retVal = "<GetSumIncomingFromSupplierReport><Error>Failed</Error></GetSumIncomingFromSupplierReport>";
            }

            return retVal;
        }

        [WebMethod]
        public string SetUserTimeZone(string siteId, int timeZoneCode)
        {
            LogUtils.MyHandle.WriteToLog(8, "SetUserTimeZone: Started. UserId: {0},TimeZoneCode: {1}.", siteId, timeZoneCode);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                supplierLogic.SetUserTimeZone(timeZoneCode);
                retVal = "<SetUserTimeZone><Status>Success</Status></SetUserTimeZone>";
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SetUserTimeZone Failed with exception");
                retVal = "<SetUserTimeZone><Error>Failed</Error></SetUserTimeZone>";
            }

            return retVal;
        }

        [WebMethod]
        public string GetUserTimeZone(string UserId)
        {
            LogUtils.MyHandle.WriteToLog(8, "GetUserTimeZone Started. UserId: {0}.", UserId);
            string retVal = "";
            try
            {
                BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(null, null);
                retVal = supplierLogic.GetUserTimeZone(new Guid(UserId));
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetUserTimeZone Failed with exception");
                retVal = "<GetUserTimeZone><Error>Failed</Error></GetUserTimeZone>";
            }
            return retVal;
        }

        #endregion

        [WebMethod]
        public Result UpdateUpsale(NoProblem.Core.DataModel.Upsale.UpdateUpsaleRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateUpsale Started.");
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                publisherUtils.UpdateUpsale(request.UpsaleId, request.UserId, request.Status, request.StatusReasonId, request.TimeToCall);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpdateUpsale failed with exception ");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public string GetUpsaleLoseReasons()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetUpsaleLoseReasons Started.");
            string retVal = "";
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                retVal = publisherUtils.GetUpsaleLoseReasons();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetUpsaleLoseReasons failed with exception ");
                retVal = "<UpsaleLoseReasons><Error>Failed</Error></UpsaleLoseReasons>";
            }

            return retVal;
        }

        [WebMethod]
        public Result<SupplierActivityStatusResponse> GetSupplierActivityStatus()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetSupplierActivityStatus Started.");
            Result<SupplierActivityStatusResponse> retVal = new Result<SupplierActivityStatusResponse>();
            try
            {
                PublisherPortalManager publisherUtils = new PublisherPortalManager(null, null);
                retVal = publisherUtils.GetSupplierActivityStatus();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetSupplierActivityStatus failed with exception ");
                retVal.Type = Result.eResultType.Failure;
                retVal.Messages.Add(ex.Message);
            }

            return retVal;
        }

        [WebMethod]
        public Result UpdateReviewStatus(DataModel.Reviews.UpdateReviewStatusRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateReviewStatus started. request: ReviewId = {0}, Status = {1}.",
                    request.ReviewId.ToString(), request.ReviewStatus.ToString());
                BusinessLogic.Reviews.ReviewsManager manager = new NoProblem.Core.BusinessLogic.Reviews.ReviewsManager(null);
                manager.UpdateReviewStatus(request.ReviewId, request.ReviewStatus);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpdateReviewStatus failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        #region HelpDesk Methods

        [WebMethod]
        public Result<List<DataModel.PublisherPortal.GuidStringPair>> GetAllHelpDeskTypes()
        {
            Result<List<DataModel.PublisherPortal.GuidStringPair>> result =
                new Result<List<DataModel.PublisherPortal.GuidStringPair>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllHelpDeskTypes started.");
                BusinessLogic.HelpDesk.HelpDeskManager manager =
                    new NoProblem.Core.BusinessLogic.HelpDesk.HelpDeskManager(null);
                result.Value = manager.GetAllHpTypes();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetAllHelpDeskTypes started.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.HelpDesk.SeverityAndStatusContainer> GetAllHelpDeskStatusesAndSeverities(Guid helpDeskTypeId)
        {
            Result<DataModel.HelpDesk.SeverityAndStatusContainer> result =
                new Result<DataModel.HelpDesk.SeverityAndStatusContainer>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllHelpDeskStatusesAndSeverities started. typeId = {0}", helpDeskTypeId);
                BusinessLogic.HelpDesk.HelpDeskManager manager =
                    new NoProblem.Core.BusinessLogic.HelpDesk.HelpDeskManager(null);
                result.Value = manager.GetAllStatusesAndSeverities(helpDeskTypeId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetAllHelpDeskStatusesAndSeverities failed with exception. typeId = {0}", helpDeskTypeId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<Guid> UpsertHelpDeskEntry(DataModel.HelpDesk.HdEntryData data)
        {
            Result<Guid> result = new Result<Guid>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpsertHelpDeskEntry started. data: regardingObjectId = {0}, intitiatorId = {1}, createdById = {2}, description = {3}, followUp = {4}, Id = {5}, ownerId = {6}, severityId = {7}, statusId = {8}, title = {9}, typeId = {10}",
                    data.RegardingObjectId, data.InitiatorId, data.CreatedById, data.Description, data.FollowUp, data.Id, data.OwnerId, data.SeverityId, data.StatusId, data.Title, data.TypeId);
                BusinessLogic.HelpDesk.HelpDeskManager manager = new BusinessLogic.HelpDesk.HelpDeskManager(null);
                result.Value = manager.UpsertEntry(data);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpsertHelpDeskEntry failed with exception. data: regardingObjectId = {0}, intitiatorId = {1}, createdById = {2}, description = {3}, followUp = {4}, Id = {5}, ownerId = {6}, severityId = {7}, statusId = {8}, title = {9}, typeId = {10}",
                    data.RegardingObjectId, data.InitiatorId, data.CreatedById, data.Description, data.FollowUp, data.Id, data.OwnerId, data.SeverityId, data.StatusId, data.Title, data.TypeId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.HelpDesk.HdEntryData>> GetHelpDeskEntries(DataModel.HelpDesk.GetHdEntriesRequest request)
        {
            Result<List<DataModel.HelpDesk.HdEntryData>> result = new Result<List<DataModel.HelpDesk.HdEntryData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetHelpDeskEntries started. reques: statusId = {0}, initiatorId = {1}, fromDate = {2}, toDate = {3}, ownerId = {4}, ticketNumber = {5}",
                    request.StatusId.HasValue ? request.StatusId.Value.ToString() : "", request.InitiatorId.HasValue ? request.InitiatorId.Value.ToString() : "",
                    request.FromDate.HasValue ? request.FromDate.Value.ToString() : "", request.ToDate.HasValue ? request.ToDate.Value.ToString() : "",
                    request.OwnerId.HasValue ? request.OwnerId.Value.ToString() : "", request.TicketNumber);
                BusinessLogic.HelpDesk.HelpDeskManager manager = new NoProblem.Core.BusinessLogic.HelpDesk.HelpDeskManager(null);
                result.Value = manager.GetEntries(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetHelpDeskEntries failed with exception. reques: statusId = {0}, customerId = {1}, fromDate = {2}, toDate = {3}, ownerId = {4}, ticketNumber = {5}",
                    request.StatusId.HasValue ? request.StatusId.Value.ToString() : "", request.InitiatorId.HasValue ? request.InitiatorId.Value.ToString() : "",
                    request.FromDate.HasValue ? request.FromDate.Value.ToString() : "", request.ToDate.HasValue ? request.ToDate.Value.ToString() : "",
                    request.OwnerId.HasValue ? request.OwnerId.Value.ToString() : "", request.TicketNumber);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }


        [WebMethod]
        public Result<List<DataModel.HelpDesk.InitiatorsSearchData>> GetInitiators(DataModel.HelpDesk.GetInitiatorsRequest request)
        {
            Result<List<DataModel.HelpDesk.InitiatorsSearchData>> result = new Result<List<DataModel.HelpDesk.InitiatorsSearchData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetInitiators started. request: SearchInput = {0}, HelpDeskTypeId = {1}", request.SearchInput, request.HelpDeskTypeId);
                BusinessLogic.HelpDesk.HelpDeskManager manager = new NoProblem.Core.BusinessLogic.HelpDesk.HelpDeskManager(null);
                result.Value = manager.SearchInitiators(request.SearchInput, request.HelpDeskTypeId, request.InitiatorId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetInitiators failed with exception. request: SearchInput = {0}, HelpDeskTypeId = {1}", request.SearchInput, request.HelpDeskTypeId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.HelpDesk.RegardingObjectSearchData>> GetRegardingObjects(DataModel.HelpDesk.GetRegardingObjectsRequest request)
        {
            Result<List<DataModel.HelpDesk.RegardingObjectSearchData>> result = new Result<List<NoProblem.Core.DataModel.HelpDesk.RegardingObjectSearchData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetRegardingObjects started. request: SearchInput = {0}, InitiatorId = {1}, HelpDeskTypeId = {2}, RegardingObjectId = {3}", request.SearchInput, request.InitiatorId, request.HelpDeskTypeId, request.RegardingObjectId);
                BusinessLogic.HelpDesk.HelpDeskManager manager = new NoProblem.Core.BusinessLogic.HelpDesk.HelpDeskManager(null);
                result.Value = manager.SearchRegardingObjects(request.SearchInput, request.InitiatorId, request.HelpDeskTypeId, request.RegardingObjectId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetRegardingObjects failed with exception. request: SearchInput = {0}, InitiatorId = {1}, HelpDeskTypeId = {2}, RegardingObjectId = {3}", request.SearchInput, request.InitiatorId, request.HelpDeskTypeId, request.RegardingObjectId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result DeleteHelpDeskEntries(List<Guid> HelpDeskEntriesIds)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DeleteHelpDeskEntries started.");
                BusinessLogic.HelpDesk.HelpDeskManager manager = new NoProblem.Core.BusinessLogic.HelpDesk.HelpDeskManager(null);
                manager.DeleteHelpDeskEntries(HelpDeskEntriesIds);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "DeleteHelpDeskEntries failed with exception.");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        #endregion

        #region Business Closure Methods

        [WebMethod]
        public Result<Guid> UpsertClosureDate(DataModel.BusinessClosures.UpsertClosureDateRequest request)
        {
            Result<Guid> result = new Result<Guid>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpsertClosureDate started. Id = {0}, Name = {1}, From = {2}, To = {3}", request.ClosureDateId, request.Name, request.FromDate, request.ToDate);
                BusinessLogic.BusinessClosures.ClosuresManager manager =
                    new NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager(null);
                result.Value = manager.UpsertClosureDate(request.ClosureDateId, request.Name, request.FromDate, request.ToDate);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpsertClosureDate. Id = {0}, Name = {1}, From = {2}, To = {3}", request.ClosureDateId, request.Name, request.FromDate, request.ToDate);
            }
            return result;
        }

        [WebMethod]
        public Result DeleteClosureDates(List<Guid> ids)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "DeleteClosureDates started.");
                BusinessLogic.BusinessClosures.ClosuresManager manager =
                    new NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager(null);
                manager.DeleteClosureDates(ids);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in DeleteClosureDates.");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.BusinessClosures.ClosureDateData>> GetClosureDates()
        {
            Result<List<DataModel.BusinessClosures.ClosureDateData>> result = new Result<List<DataModel.BusinessClosures.ClosureDateData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetClosureDates started.");
                BusinessLogic.BusinessClosures.ClosuresManager manager =
                    new NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager(null);
                result.Value = manager.GetClosureDates();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetClosureDates.");
            }
            return result;
        }

        [WebMethod]
        public Result UpdateMessagingTimes(List<DataModel.BusinessClosures.MessagingTimeData> messagingTimes)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "UpdateMessagingTimes started.");
                BusinessLogic.BusinessClosures.ClosuresManager manager =
                    new NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager(null);
                manager.UpdateMessagingTimes(messagingTimes);
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateMessagingTimes.");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.BusinessClosures.MessagingTimeData>> GetMessagingTimes()
        {
            Result<List<DataModel.BusinessClosures.MessagingTimeData>> result =
                new Result<List<DataModel.BusinessClosures.MessagingTimeData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetMessagingTimes started.");
                BusinessLogic.BusinessClosures.ClosuresManager manager =
                    new NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager(null);
                result.Value = manager.GetMessagingTimes();
            }
            catch (Exception exc)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetMessagingTimes.");
            }
            return result;
        }

        #endregion

        #region Wibiya Methods

        [WebMethod]
        public Result<DataModel.Origins.WibiyaSignUpStatus> WibiyaSignUp(DataModel.Origins.WibiyaSignUpRequest request)
        {
            Result<DataModel.Origins.WibiyaSignUpStatus> result = new Result<NoProblem.Core.DataModel.Origins.WibiyaSignUpStatus>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "WibiyaSignUp started. FirstName={0}, LastName={1}, Email={2}, WebSiteUrl={3}, BusinessName={4}, WibiyaToken={5}, SiteId={6}", request.FirstName, request.LastName, request.Email, request.WebSiteUrl, request.BusinessName, request.WibiyaToken, request.SiteId);
                BusinessLogic.Origins.WibiyaManager manager = new NoProblem.Core.BusinessLogic.Origins.WibiyaManager(null);
                result.Value = manager.SignUp(request.FirstName, request.LastName, request.Email, request.WebSiteUrl, request.BusinessName, request.WibiyaToken, request.SiteId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaSignUp. FirstName={0}, LastName={1}, Email={2}, WebSiteUrl={3}, BusinessName={4}, WibiyaToken={5}, SiteId={6}", request.FirstName, request.LastName, request.Email, request.WebSiteUrl, request.BusinessName, request.WibiyaToken, request.SiteId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Origins.WibiyaLoginResponse> WibiyaLogin(DataModel.Origins.WibiyaLoginRequest request)
        {
            Result<DataModel.Origins.WibiyaLoginResponse> result = new Result<DataModel.Origins.WibiyaLoginResponse>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "WibiyaLogin started. email = {0}, password = {1}", request.Email, request.Password);
                BusinessLogic.Origins.WibiyaManager manager = new NoProblem.Core.BusinessLogic.Origins.WibiyaManager(null);
                result.Value = manager.Login(request.Email, request.Password, request.UpdateXmlInWibiya, request.WibiyaToken, request.SiteId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaLogin. email = {0}, password = {1}", request.Email, request.Password);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result WibiyaSaveHeadings(DataModel.Origins.WibiyaSaveHeadingsRequest request)
        {
            Result result = new Result();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "WibiyaSaveHeadings started. originId = {0}.", request.OriginId);
                BusinessLogic.Origins.WibiyaManager manager = new NoProblem.Core.BusinessLogic.Origins.WibiyaManager(null);
                manager.SaveHeadings(request.OriginId, request.Headings);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaSaveHeadings. originId = {0}.", request.OriginId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<Guid>> WibiyaGetHeadingsForConfiguration(Guid originId)
        {
            Result<List<Guid>> result = new Result<List<Guid>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "WibiyaGetHeadingsForConfiguration started. originId = {0}.", originId);
                BusinessLogic.Origins.WibiyaManager manager = new NoProblem.Core.BusinessLogic.Origins.WibiyaManager(null);
                result.Value = manager.GetHeadingsForConfiguration(originId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaGetHeadingsForConfiguration. originId = {0}.", originId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<Guid>> WibiyaGetHeadingsForWidget(Guid originId)
        {
            Result<List<Guid>> result = new Result<List<Guid>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "WibiyaGetHeadingsForWidget started. originId = {0}.", originId);
                BusinessLogic.Origins.WibiyaManager manager = new NoProblem.Core.BusinessLogic.Origins.WibiyaManager(null);
                result.Value = manager.GetHeadingsForWidget(originId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaGetHeadingsForWidget. originId = {0}.", originId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<Guid>> WibiyaGetDefaultHeadings()
        {
            Result<List<Guid>> result = new Result<List<Guid>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "WibiyaGetDefaultHeadings started.");
                BusinessLogic.Origins.WibiyaManager manager = new NoProblem.Core.BusinessLogic.Origins.WibiyaManager(null);
                result.Value = manager.GetDefaultHeadings();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaGetDefaultHeadings");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Origins.WibiyaIncomingFundsData> WibiyaGetIncomingFundsData(Guid originId)
        {
            Result<DataModel.Origins.WibiyaIncomingFundsData> result = new Result<NoProblem.Core.DataModel.Origins.WibiyaIncomingFundsData>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "WibiyaGetIncomingFundsData started. originId = {0}.", originId);
                BusinessLogic.Origins.WibiyaManager manager = new NoProblem.Core.BusinessLogic.Origins.WibiyaManager(null);
                result.Value = manager.GetIncomingFundsData(originId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaGetIncomingFundsData. originId = {0}.", originId);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<Guid> WibiyaDoCashOut(DataModel.Origins.WibiyaDoCashOutRequest request)
        {
            Result<Guid> result = new Result<Guid>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "WibiyaDoCashOut started. originId = {0}. payPalEmail = {1}, currency = {2}, rememberPayPalEmail = {3}",
                    request.OriginId, request.PayPalEmail, request.Currency, request.RememberPayPalEmail);
                BusinessLogic.Origins.WibiyaManager manager = new NoProblem.Core.BusinessLogic.Origins.WibiyaManager(null);
                result.Value = manager.DoCashOut(request.OriginId, request.PayPalEmail, request.RememberPayPalEmail, request.Currency);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaDoCashOut. originId = {0}. payPalEmail = {1}, currency = {2}, rememberPayPalEmail = {3}",
                    request.OriginId, request.PayPalEmail, request.Currency, request.RememberPayPalEmail);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        #endregion

        [WebMethod]
        public Result<List<string>> AutoCompleter(string str,
            NoProblem.Core.BusinessLogic.AutoCompleter.eAutoCompleteFromTable table,
            NoProblem.Core.BusinessLogic.AutoCompleter.eAtuoCompleteField field)
        {
            Result<List<string>> result = new Result<List<string>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(9, "AutoCompleter started. str = {0}, table = {1}, field = {2}", str, table, field);
                BusinessLogic.AutoCompleter completer = new AutoCompleter(null);
                result.Value = completer.AutoComplete(str, table, field);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AutoCompleter. str = {0}, table = {1}, field = {2}", str, table, field);
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.SliderData.ExpertiseData>> GetAllExpertiseSliderData()
        {
            Result<List<DataModel.SliderData.ExpertiseData>> result = new Result<List<NoProblem.Core.DataModel.SliderData.ExpertiseData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllExpertiseSliderData started");
                SliderDataManager manager = new SliderDataManager(null);
                result.Value = manager.GetAllExpertiseSliderData();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllExpertiseSliderData");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public
            Result<DataModel.SliderData.KeywordsContainer>
            //Result<List<DataModel.SliderData.KeywordHeadingIdPair>>
            GetAllSliderKeywords()
        {
            Result<DataModel.SliderData.KeywordsContainer> result = new Result<DataModel.SliderData.KeywordsContainer>();
            //Result<List<DataModel.SliderData.KeywordHeadingIdPair>> result =
            //    new Result<List<DataModel.SliderData.KeywordHeadingIdPair>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllSliderKeywords started");
                SliderDataManager manager = new SliderDataManager(null);
                result.Value = manager.GetAllSliderKeywords();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllSliderKeywords");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<List<StringBoolPair>> GetAllBadDomains()
        {
            Result<List<StringBoolPair>> result = new Result<List<StringBoolPair>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetAllBadDomains started");
                SliderDataManager manager = new SliderDataManager(null);
                result.Value = manager.GetAllBadDomains();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllBadDomains");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<HashSet<DataModel.SliderData.SpecialCategoryGroup>> GetAllSpecialCategroyGroups()
        {
            Result<HashSet<DataModel.SliderData.SpecialCategoryGroup>> result = new Result<HashSet<NoProblem.Core.DataModel.SliderData.SpecialCategoryGroup>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSpecialCategroyGroups started");
                SliderDataManager manager = new SliderDataManager(null);
                result.Value = manager.GetAllSpecialCategroyGroups();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetSpecialCategroyGroups");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<HashSet<DataModel.SliderData.CategoryGroupForDisplay>> GetCategoryGroupsForDisplay()
        {
            Result<HashSet<DataModel.SliderData.CategoryGroupForDisplay>> result = new Result<HashSet<NoProblem.Core.DataModel.SliderData.CategoryGroupForDisplay>>();
            try
            {
                SliderDataManager manager = new SliderDataManager(null);
                result.Value = manager.GetCategoryGroupsForDisplay();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetCategoryGroupsForDisplay");
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.Consumer.Containers.TipData>> GetAllTips()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetAllTips started");
            Result<List<DataModel.Consumer.Containers.TipData>> result = new Result<List<NoProblem.Core.DataModel.Consumer.Containers.TipData>>();
            try
            {
                BusinessLogic.CustomerPageDataManager manager = new CustomerPageDataManager();
                result.Value = manager.GetAllTips();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllTips");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
                result.Messages.Add(exc.StackTrace);
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Dapaz.GoldenNumberAdvertisers.CreateGoldenNumberAdvertiserResponse> CreateGoldenNumberAdvertiser(DataModel.Dapaz.GoldenNumberAdvertisers.CreateGoldenNumberAdvertiserRequest request)
        {
            Result<DataModel.Dapaz.GoldenNumberAdvertisers.CreateGoldenNumberAdvertiserResponse> result = new Result<NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers.CreateGoldenNumberAdvertiserResponse>();
            try
            {
                BusinessLogic.Dapaz.GoldenNumberAdvertiserManager manager = new NoProblem.Core.BusinessLogic.Dapaz.GoldenNumberAdvertiserManager(null);
                result.Value = manager.CreateGoldenNumberAdvertiser(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateGoldenNumberAdvertiser");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Dapaz.GoldenNumberAdvertisers.UpdateGoldenNumberAdvertiserResponse> UpdateGoldenNumberAdvertiser(DataModel.Dapaz.GoldenNumberAdvertisers.UpdateGoldenNumberAdvertiserRequest request)
        {
            Result<DataModel.Dapaz.GoldenNumberAdvertisers.UpdateGoldenNumberAdvertiserResponse> result = new Result<NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers.UpdateGoldenNumberAdvertiserResponse>();
            try
            {
                BusinessLogic.Dapaz.GoldenNumberAdvertiserManager manager = new NoProblem.Core.BusinessLogic.Dapaz.GoldenNumberAdvertiserManager(null);
                result.Value = manager.UpdateGoldenNumberAdvertiser(request);
                if (request.AuditOn)
                {
                    NoProblem.Core.BusinessLogic.Audit.AuditManager auditManager = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                    auditManager.CreateAudit(request.AuditEntries);
                }
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateGoldenNumberAdvertiser");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Dapaz.GoldenNumberAdvertisers.RetrieveGoldenNumberAdvertiserResponse> RetrieveGoldenNumberAdvertiser(DataModel.Dapaz.GoldenNumberAdvertisers.RetrieveGoldenNumberAdvertiserRequest request)
        {
            Result<DataModel.Dapaz.GoldenNumberAdvertisers.RetrieveGoldenNumberAdvertiserResponse> result = new Result<NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers.RetrieveGoldenNumberAdvertiserResponse>();
            try
            {
                BusinessLogic.Dapaz.GoldenNumberAdvertiserManager manager = new NoProblem.Core.BusinessLogic.Dapaz.GoldenNumberAdvertiserManager(null);
                result.Value = manager.RetrieveGoldenNumberAdvertiser(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in RetrieveGoldenNumberAdvertiser");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Dapaz.GoldenNumberAdvertisers.GetAvailableVirtualNumbersResponse> GetAvailableVirtualNumbers()
        {
            Result<DataModel.Dapaz.GoldenNumberAdvertisers.GetAvailableVirtualNumbersResponse> result = new Result<DataModel.Dapaz.GoldenNumberAdvertisers.GetAvailableVirtualNumbersResponse>();
            try
            {
                BusinessLogic.Dapaz.GoldenNumberAdvertiserManager manager = new NoProblem.Core.BusinessLogic.Dapaz.GoldenNumberAdvertiserManager(null);
                result.Value = manager.GetAvailableVirtualNumbers();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAvailableVirtualNumbers");
            }
            return result;
        }

        [WebMethod]
        public Result<string> GetPathToRecordingByRecordId(string recordId)
        {
            Result<string> result = new Result<string>();
            try
            {
                BusinessLogic.SupplierManager manager = new SupplierManager(null);
                result.Value = manager.GetRecordingPathByRecordid(recordId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetPathToRecordingByRecordId. recordId = {0}", recordId);
            }
            return result;
        }

        [WebMethod]
        public Result InsertDomainBlackList(List<DataModel.DomainBlackList.DomainBlackListData> dataList, string callBackUrl)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.DomainBlackList.DomainBlackListManager manager = new NoProblem.Core.BusinessLogic.DomainBlackList.DomainBlackListManager(null);
                manager.Insert(dataList, callBackUrl);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in InsertDomainBlackList");
                AddFailureToResult(result, exc);
            }
            return result;
        }

        [WebMethod]
        public Result<string[][]> GetAllBlackList()
        {
            Result<string[][]> result = new Result<string[][]>();
            try
            {
                BusinessLogic.DomainBlackList.DomainBlackListManager manager = new NoProblem.Core.BusinessLogic.DomainBlackList.DomainBlackListManager(null);
                result.Value = manager.GetAllBlackList();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllBlackList");
            }
            return result;
        }
        [WebMethod]
        public Result<List<GuidStringPair>> GetOriginBroughtByName()
        {
            Result<List<GuidStringPair>> result = new Result<List<GuidStringPair>>();
            try
            {
                NoProblem.Core.BusinessLogic.Origins.AffiliatesManager manaer = new NoProblem.Core.BusinessLogic.Origins.AffiliatesManager(null);
                result.Value = manaer.GetOriginBroughtByName();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetOriginBroughtByName");
            }
            return result;
        }
        [WebMethod]
        public Result<List<GuidStringPair>> GetAllBrokers()
        {
            Result<List<GuidStringPair>> result = new Result<List<GuidStringPair>>();
            try
            {
                BusinessLogic.LeadBuyers.LeadBuyersUtils utils = new NoProblem.Core.BusinessLogic.LeadBuyers.LeadBuyersUtils(null);
                result.Value = utils.GetAllBrokers();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllBrokers");
            }
            return result;
        }
        [WebMethod]
        public Result<List<Guid>> GetNoProblemSliderOrigins()
        {
            Result<List<Guid>> result = new Result<List<Guid>>();
            try
            {
                NoProblem.Core.BusinessLogic.Origins.AffiliatesManager manaer = new NoProblem.Core.BusinessLogic.Origins.AffiliatesManager(null);
                result.Value = manaer.GetNoProblemSliderOrigins();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetOriginBroughtByName");
            }
            return result;
        }
        [WebMethod]
        public Result<List<Guid>> GetDistributionOrigins()
        {
            Result<List<Guid>> result = new Result<List<Guid>>();
            try
            {
                NoProblem.Core.BusinessLogic.Origins.AffiliatesManager manaer = new NoProblem.Core.BusinessLogic.Origins.AffiliatesManager(null);
                result.Value = manaer.GetDistributionOrigins();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetOriginBroughtByName");
            }
            return result;
        }
        /*
        [WebMethod]
        public Result<HashSet<DataModel.Expertise.Campaigns.CampaignData>> GetAllCampaignsData()
        {
            Result<HashSet<DataModel.Expertise.Campaigns.CampaignData>> result = new Result<HashSet<DataModel.Expertise.Campaigns.CampaignData>>();
            try
            {
                BusinessLogic.Expertises.CampaignsDataRetriever campaigns = new NoProblem.Core.BusinessLogic.Expertises.CampaignsDataRetriever();
                result.Value = campaigns.GetAllCampaignsData();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllCampaignsData");
            }
            return result;
        }
        */
        [WebMethod]
        public Result<List<DataModel.Expertise.Campaigns.HeadingIdAndIsAvailable>> DidSliderCheckAvailability(List<Guid> headingIds)
        {
            Result<List<DataModel.Expertise.Campaigns.HeadingIdAndIsAvailable>> result = new Result<List<NoProblem.Core.DataModel.Expertise.Campaigns.HeadingIdAndIsAvailable>>();
            try
            {
                BusinessLogic.Expertises.DidSliderCampaignActivityChecker checker = new NoProblem.Core.BusinessLogic.Expertises.DidSliderCampaignActivityChecker();
                result.Value = checker.CheckAvailability(headingIds);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in DidSliderCheckAvailability");
            }
            return result;
        }

        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.Expertise.Campaigns.EmailAndIsAvailable>> CheckSpecialAdvertisersAvailability(List<string> advertiserEmails)
        {
            Result<List<NoProblem.Core.DataModel.Expertise.Campaigns.EmailAndIsAvailable>> result = new Result<List<DataModel.Expertise.Campaigns.EmailAndIsAvailable>>();
            try
            {
                BusinessLogic.Expertises.DidSliderCampaignActivityChecker checker = new NoProblem.Core.BusinessLogic.Expertises.DidSliderCampaignActivityChecker();
                result.Value = checker.CheckSpecialAdvertisersAvailability(advertiserEmails);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckSpecialAdvertisersAvailability");
            }
            return result;
        }

        [WebMethod]
        public Result<bool> SendToMyMobile(NoProblem.Core.DataModel.SeoData.SendToMyMobileRequest request)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.SEODataServices.SEOServicesProvider provider = new NoProblem.Core.BusinessLogic.SEODataServices.SEOServicesProvider(null);
                result.Value = provider.SendToMyMobile(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SendToMyMobile. phone:{0}, supplierId:{1}, url:{2}", request.Phone, request.SupplierId, request.Url);
            }
            return result;
        }

        [WebMethod]
        public Result<string> ShortenSupplierPageUrl(string longUrl, Guid supplierId)
        {
            Result<string> result = new Result<string>();
            try
            {
                BusinessLogic.SEODataServices.SEOServicesProvider provider = new NoProblem.Core.BusinessLogic.SEODataServices.SEOServicesProvider(null);
                result.Value = provider.ShortenSupplierPageUrl(longUrl, supplierId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in ShortenUrl. longUlr = {0}. supplierId = {1}", longUrl, supplierId);
            }
            return result;
        }
        [WebMethod]
        public Result<List<DataModel.PublisherPortal.GuidStringPair>> GetToolbarsId(Guid OriginId)
        {
            Result<List<DataModel.PublisherPortal.GuidStringPair>> result = new Result<List<DataModel.PublisherPortal.GuidStringPair>>();

            try
            {
                NoProblem.Core.BusinessLogic.Origins.ToolbarManager provider = new NoProblem.Core.BusinessLogic.Origins.ToolbarManager(null);
                result.Value = provider.GetToolbarid(OriginId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetToolbarsId. OriginId = {0}", OriginId);
            }
            return result;
        }
        [WebMethod]
        public Result<HashSet<DataModel.Expertise.Campaigns.CampaignData>> GetAllCampaignsData()
        {
            Result<HashSet<DataModel.Expertise.Campaigns.CampaignData>> result = new Result<HashSet<DataModel.Expertise.Campaigns.CampaignData>>();
            try
            {
                BusinessLogic.Expertises.CampaignsDataRetriever campaigns = new NoProblem.Core.BusinessLogic.Expertises.CampaignsDataRetriever();
                result.Value = campaigns.GetAllCampaignsData();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllCampaignsData");
            }
            return result;
        }
        [WebMethod]
        public Result<bool> ClearPhoneMobileApp(string phone)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                PublisherPortalManager ppm = new PublisherPortalManager(null, null);
                result.Value = ppm.ClearPhoneMobileApp(phone);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in ClearPhoneMobileApp, phone:{0}", phone);
            }
            return result;
        }
        [WebMethod]
        public Result<DataModel.Expertise.Campaigns.PhoneDidSliderCampaignData> GetPhoneDidData(Guid ExpertiseId)
        {
            Result<DataModel.Expertise.Campaigns.PhoneDidSliderCampaignData> result = new Result<DataModel.Expertise.Campaigns.PhoneDidSliderCampaignData>();
            try
            {
                BusinessLogic.Expertises.CampaignsDataRetriever campaigns = new NoProblem.Core.BusinessLogic.Expertises.CampaignsDataRetriever();
                result.Value = campaigns.GetPhoneDidData(ExpertiseId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetPhoneDidData");
            }
            return result;
        }
        [WebMethod]
        public Result<List<DataModel.Expertise.Campaigns.PhonedidOutsideUsResponse>> GetPhoneDidOutsideUS()
        {
            Result<List<DataModel.Expertise.Campaigns.PhonedidOutsideUsResponse>> result = new Result<List<DataModel.Expertise.Campaigns.PhonedidOutsideUsResponse>>();
            try
            {
                BusinessLogic.Expertises.CampaignsDataRetriever campaigns = new NoProblem.Core.BusinessLogic.Expertises.CampaignsDataRetriever();
                result.Value = campaigns.GetPhoneDidOutsideUS();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetPhoneDidData");
            }
            return result;
        }
        [WebMethod]
        public Result<List<Guid>> GetOriginsBlockCampaigns()
        {
            Result<List<Guid>> result = new Result<List<Guid>>();
            try
            {
                BusinessLogic.Expertises.CampaignsDataRetriever campaigns = new NoProblem.Core.BusinessLogic.Expertises.CampaignsDataRetriever();
                result.Value = campaigns.GetOriginsBlockCampaigns();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllCampaignsData");
            }
            return result;
        }
        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.Regions.RegionData>> GetRegionsOfParent(NoProblem.Core.DataModel.Regions.RegionRequest request)
        {
            Result<List<NoProblem.Core.DataModel.Regions.RegionData>> result = new Result<List<NoProblem.Core.DataModel.Regions.RegionData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetRegionsOfParent");
                NoProblem.Core.BusinessLogic.Regions.RegionsManager manager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(null);
                List<NoProblem.Core.DataModel.Regions.RegionData> response = manager.GetAllRegionByParentAndLevel(request.level, request.RegionParentId);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetRegionsOfParent failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }
        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.Regions.RegionData>> GetRegionsOfLevel(int level)
        {
            Result<List<NoProblem.Core.DataModel.Regions.RegionData>> result = new Result<List<NoProblem.Core.DataModel.Regions.RegionData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetRegionsOfLevel");
                NoProblem.Core.BusinessLogic.Regions.RegionsManager manager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(null);
                List<NoProblem.Core.DataModel.Regions.RegionData> response = manager.GetAllRegionByParentAndLevel(level);
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetRegionsOfLevel failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }
        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.Injection.InjectionData>> GetInjectionData()
        {
            Result<List<NoProblem.Core.DataModel.Injection.InjectionData>> result = new Result<List<NoProblem.Core.DataModel.Injection.InjectionData>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetInjectionData");
                NoProblem.Core.BusinessLogic.InjectionsDataReaders.InjectionManager _injection = new BusinessLogic.InjectionsDataReaders.InjectionManager();
                List<NoProblem.Core.DataModel.Injection.InjectionData> response = _injection.GetInjectionData();
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetInjectionData failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }
        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.Injection.InjectionCampaign>> GetInjectionCampaign()
        {
            Result<List<NoProblem.Core.DataModel.Injection.InjectionCampaign>> result = new Result<List<NoProblem.Core.DataModel.Injection.InjectionCampaign>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetInjectionCampaign");
                NoProblem.Core.BusinessLogic.InjectionsDataReaders.InjectionManager _injection = new BusinessLogic.InjectionsDataReaders.InjectionManager();
                List<NoProblem.Core.DataModel.Injection.InjectionCampaign> response = _injection.GetInjectionCampaign();
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetInjectionCampaign failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }
        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.Injection.InjectionCampaignParasite>> GetInjectionParasiteCampaign()
        {
            Result<List<NoProblem.Core.DataModel.Injection.InjectionCampaignParasite>> result = new Result<List<NoProblem.Core.DataModel.Injection.InjectionCampaignParasite>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetInjectionParasiteCampaign");
                NoProblem.Core.BusinessLogic.InjectionsDataReaders.InjectionManager _injection = new BusinessLogic.InjectionsDataReaders.InjectionManager();
                List<NoProblem.Core.DataModel.Injection.InjectionCampaignParasite> response = _injection.GetInjectionParasiteCampaign();
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetInjectionParasiteCampaign failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }
         [WebMethod]
        public Result<List<Guid>> GetParasiteOriginBlackList()
        {
            Result<List<Guid>> result = new Result<List<Guid>>();
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "GetParasiteOriginBlackList");
                NoProblem.Core.BusinessLogic.InjectionsDataReaders.InjectionManager _injection = new BusinessLogic.InjectionsDataReaders.InjectionManager();
                List<Guid> response = _injection.GetParasiteOriginBlackList();
                result.Value = response;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "GetParasiteOriginBlackList failed with exception");
                result.Type = Result.eResultType.Failure;
                result.Messages.Add(exc.Message);
            }
            return result;
        }

        [WebMethod]
        public Result<List<DataModel.AdEngine.AdEngineCampaignData>> GetAllAdEngineCampaigns()
        {
            Result<List<DataModel.AdEngine.AdEngineCampaignData>> result =
                new Result<List<DataModel.AdEngine.AdEngineCampaignData>>();
            try
            {
                BusinessLogic.AdEngine.AdEngineManager manager = new BusinessLogic.AdEngine.AdEngineManager();
                result.Value = manager.GetAllCampaigns();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in GetAllAdEngineCampaigns");
            }
            return result;
        }

        #region Injections Cockpit

        [WebMethod]
        public Result<List<DataModel.Origins.InjectionsCockpit.InjectionWithDAUs>> GetInjectionsAndDAUs()
        {
            Result<List<DataModel.Origins.InjectionsCockpit.InjectionWithDAUs>> result = new Result<List<DataModel.Origins.InjectionsCockpit.InjectionWithDAUs>>();
            try
            {
                BusinessLogic.Origins.InjectionsCockpit.InjectionsCockpitLoader loader =
                    new BusinessLogic.Origins.InjectionsCockpit.InjectionsCockpitLoader();
                result.Value = loader.GetInjectionsAndDAUs();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.GetInjectionsAndDAUs");
            }
            return result;
        }

        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.Injection.InjectionOriginData>> GetOriginsByInjection(Guid injectionId)
        {
            Result<List<NoProblem.Core.DataModel.Injection.InjectionOriginData>> result = new Result<List<NoProblem.Core.DataModel.Injection.InjectionOriginData>>();
            try
            {
                BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager manager =
                    new BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager(null);
                result.Value = manager.GetOriginsByInjection(injectionId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.GetOriginsByInjection");
            }
            return result;
        }

        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.Injection.InjectionOriginData>> GetInjectionsByOrigin(Guid originId)
        {
            Result<List<NoProblem.Core.DataModel.Injection.InjectionOriginData>> result = new Result<List<NoProblem.Core.DataModel.Injection.InjectionOriginData>>();
            try
            {
                BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager manager =
                    new BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager(null);
                result.Value = manager.GetInjectionsByOrigin(originId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.GetInjectionsByOrigin");
            }
            return result;
        }

        [WebMethod]
        public Result UpdateConnectionsByOrigin(Guid originId, List<NoProblem.Core.DataModel.Injection.InjectionOriginData> injectionIds)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager manager =
                    new BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager(null);
                manager.UpdateConnectionsByOrigin(originId, injectionIds);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.UpdateConnectionsByOrigin");
            }
            return result;
        }

        [WebMethod]
        public Result UpdateConnectionsByInjection(Guid injectionId, List<NoProblem.Core.DataModel.Injection.InjectionOriginData> originIds)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager manager =
                    new BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager(null);
                manager.UpdateConnectionsByInjection(injectionId, originIds);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.UpdateConnectionsByInjection");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Injection.InjectionData> GetInjectionDetails(Guid injectionId)
        {
            Result<DataModel.Injection.InjectionData> result = new Result<DataModel.Injection.InjectionData>();
            try
            {
                BusinessLogic.Origins.InjectionsCockpit.InjectionsManager manager =
                    new BusinessLogic.Origins.InjectionsCockpit.InjectionsManager(null);
                result.Value = manager.GetInjectionDetails(injectionId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.GetInjectionDetails");
            }
            return result;
        }

        [WebMethod]
        public Result<Guid> UpsertInjectionDetails(DataModel.Injection.InjectionData data)
        {
            Result<Guid> result = new Result<Guid>();
            try
            {
                BusinessLogic.Origins.InjectionsCockpit.InjectionsManager manager =
                    new BusinessLogic.Origins.InjectionsCockpit.InjectionsManager(null);
                result.Value = manager.UpsertInjectionDetails(data);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.UpsertInjectionDetails");
            }
            return result;
        }

        [WebMethod]
        public Result<DataModel.Origins.InjectionsCockpit.OriginData> GetOriginDetails(Guid originId)
        {
            Result<DataModel.Origins.InjectionsCockpit.OriginData> result =
                new Result<DataModel.Origins.InjectionsCockpit.OriginData>();
            try
            {
                BusinessLogic.Origins.InjectionsCockpit.OriginsManager manager =
                    new BusinessLogic.Origins.InjectionsCockpit.OriginsManager(null);
                result.Value = manager.GetOriginDetails(originId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.GetOriginDetails");
            }
            return result;
        }

        [WebMethod]
        public Result<Guid> UpsertOriginDetails(DataModel.Origins.InjectionsCockpit.OriginData data)
        {
            Result<Guid> result = new Result<Guid>();
            try
            {
                BusinessLogic.Origins.InjectionsCockpit.OriginsManager manager =
                    new BusinessLogic.Origins.InjectionsCockpit.OriginsManager(null);
                result.Value = manager.UpsertOriginDetails(data);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.UpsertOriginDetails");
            }
            return result;
        }

        [WebMethod]
        public Result<List<Guid>> GetOriginsWithAdEngineEnabled()
        {
            Result<List<Guid>> result = new Result<List<Guid>>();
            try
            {
                BusinessLogic.AdEngine.AdEngineManager manager =
                    new BusinessLogic.AdEngine.AdEngineManager();
                result.Value = manager.GetOriginsWithAdEngineEnabled();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.GetOriginsWithAdEngineEnabled");
            }
            return result;
        }
        [WebMethod]
        public Result<List<NoProblem.Core.DataModel.ClipCall.Request.eRequestsStatus>> GetMobileAppStatuses()
        {
            Result<List<NoProblem.Core.DataModel.ClipCall.Request.eRequestsStatus>> result =
                new Result<List<NoProblem.Core.DataModel.ClipCall.Request.eRequestsStatus>>();
            try
            {
                NoProblem.Core.BusinessLogic.ClipCall.RequestReportManager manager =
                    new NoProblem.Core.BusinessLogic.ClipCall.RequestReportManager();
                result.Value = manager.GetRequestStatuses();
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in Site.GetOriginsWithAdEngineEnabled");
            }
            return result;
        }

        #endregion
    }
}
