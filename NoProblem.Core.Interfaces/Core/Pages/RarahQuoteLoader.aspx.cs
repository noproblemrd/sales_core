﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NoProblem.Core.Interfaces.Core.Pages
{
    public partial class RarahQuoteLoader : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LoadButton_Click(object sender, EventArgs e)
        {
            if (!FileUpload1.HasFile)
                return;
            if (!FileUpload1.FileName.EndsWith(".csv"))
                return;
            LoadButton.Enabled = false;
            using (var csvStream = FileUpload1.PostedFile.InputStream)
            {
                BusinessLogic.Dapaz.RarahQuote.Loader.LoadRarahimQuotes(csvStream);
            }
        }

        protected void PricesButton_Click(object sender, EventArgs e)
        {
            if (!FileUpload2.HasFile)
                return;
            if (!FileUpload2.FileName.EndsWith(".csv"))
                return;
            PricesButton.Enabled = false;
            using (var csvStream = FileUpload2.PostedFile.InputStream)
            {
                BusinessLogic.Dapaz.RarahQuote.PriceUpdater.UpdatePrices(csvStream);
            }
        }

    }
}
