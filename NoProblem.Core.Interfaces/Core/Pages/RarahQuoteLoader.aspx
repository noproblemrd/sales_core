﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RarahQuoteLoader.aspx.cs" Inherits="NoProblem.Core.Interfaces.Core.Pages.RarahQuoteLoader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h3>Update prices</h3>
        File with prices to load:<asp:FileUpload ID="FileUpload2" runat="server" />
        <br />
        <asp:Button ID="PricesButton" runat="server" Text="Load Prices" 
            onclick="PricesButton_Click" />
    </div>
    <div>
        <h3>Load</h3>
        File to load:<asp:FileUpload ID="FileUpload1" runat="server" />
        <br />
        <asp:Button ID="LoadButton" runat="server" Text="Load Rarahim Quotes" 
            onclick="LoadButton_Click" />
    </div>
    </form>
</body>
</html>
