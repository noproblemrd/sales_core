﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GoldenNumbersLoaderAndExporter.aspx.cs"
    Inherits="NoProblem.Core.Interfaces.Core.Pages.GoldenNumbersLoaderAndExporter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h3>Load</h3>
        File to load:<asp:FileUpload ID="FileUpload1" runat="server" />
        <br />
        <asp:Button ID="LoadButton" runat="server" Text="Load" 
            onclick="LoadButton_Click" />
    </div>
    <div>
        <h3>Export</h3>
        <asp:Button ID="ExportButton" runat="server" Text="Export" 
            onclick="ExportButton_Click" />
    </div>
    </form>
</body>
</html>
