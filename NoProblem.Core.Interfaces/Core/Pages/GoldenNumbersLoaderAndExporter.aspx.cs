﻿using System;
using System.IO;

namespace NoProblem.Core.Interfaces.Core.Pages
{
    public partial class GoldenNumbersLoaderAndExporter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LoadButton_Click(object sender, EventArgs e)
        {
            if (!FileUpload1.HasFile)
                return;
            if (!FileUpload1.FileName.EndsWith(".csv"))
                return;
            LoadButton.Enabled = false;
            using (var csvStream = FileUpload1.PostedFile.InputStream)
            {
                BusinessLogic.Dapaz.GoldenNumberAdvertiserManager.LoadAdvertisers(csvStream);
            }
        }

        protected void ExportButton_Click(object sender, EventArgs e)
        {
            BusinessLogic.Dapaz.GoldenNumberAdvertiserManager manager = new NoProblem.Core.BusinessLogic.Dapaz.GoldenNumberAdvertiserManager(null);
            byte[] bytes = manager.ExportAdvertisers();

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "text/csv";//"application/octet-stream"; // Change this type as necessary
            Response.AddHeader("Content-Length", bytes.Length.ToString());
            Response.AddHeader("content-disposition", String.Format("inline; filename={0}", "export" + DateTime.Now.Ticks + ".csv"));
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());
            Response.BinaryWrite(bytes);
            Response.Flush();
        }
    }
}
