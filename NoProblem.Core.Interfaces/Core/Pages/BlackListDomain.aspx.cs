﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using NoProblem.Core.BusinessLogic.Utils;
using NoProblem.Core.DataModel.SupplierModel;
using DML = NoProblem.Core.DataModel;



namespace NoProblem.Core.Interfaces.Core.Pages
{
    public partial class BlackListDomain : System.Web.UI.Page
    {
        private DML.Xrm.DataContext _xrmDataContext;
        public DML.Xrm.DataContext XrmDataContext
        {
            get
            {
                if (_xrmDataContext == null)
                {
                    _xrmDataContext = DataModel.XrmDataContext.Create();
                }
                return _xrmDataContext;
            }
            private set { _xrmDataContext = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Load_Click(object sender, EventArgs e)
        {
            if (!fu.HasFile)
                return;
            Stream myStream;
            Int32 fileLen;
           
            // Get the length of the file.
            fileLen = fu.PostedFile.ContentLength;
                       

            // Create a byte array to hold the contents of the file.
            Byte[] Input = new Byte[fileLen];

            // Initialize the stream to read the uploaded file.
            myStream = fu.FileContent;

            // Read the file into the byte array.
            myStream.Read(Input, 0, fileLen);

            // Copy the byte array to a string.
            string str = Encoding.UTF8.GetString(Input);
            char separet = (char)10;
            string[] domains = str.Split(new char[] { separet }, StringSplitOptions.RemoveEmptyEntries );
            NoProblem.Core.BusinessLogic.DomainBlackList.BadDomainManager bdm = new NoProblem.Core.BusinessLogic.DomainBlackList.BadDomainManager(null);
            lbl_Inserted.Text = bdm.InsertBadDomain(domains).ToString();
            div_result.Visible = true;
            /*
            foreach (string domain in domains)
            {
                
                if (!string.IsNullOrEmpty(domain))
                    bdm.InsertBadDomain(domain);
            //    XrmDataContext.AddObject(baddomain.
            }
             * */
            
        }
    }
}
