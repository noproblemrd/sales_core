﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Effect.Crm.Logs;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for TwilioEntryPoint
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TwilioEntryPoint : NpServiceBase
    {
        #region IVR Widget Methods

        [WebMethod]
        public string IvrWidgetConsultation(string callSid, string from, string to, string callStatus)
        {
            string retVal = string.Empty;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "IvrWidgetConsultation started. callSid = {0}, from = {1}, to = {2}, callStatus = {3}",
                    callSid, from, to, callStatus);
                BusinessLogic.TwilioBL.TwilioHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioHandler(null);
                retVal = handler.IvrWidgetConsultation(callSid, from, to, callStatus);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IvrWidgetConsultation. callSid = {0}, from = {1}, to = {2}, callStatus = {3}",
                    callSid, from, to, callStatus);
            }
            return retVal;
        }

        [WebMethod]
        public string IvrWidgetPhoneGet(string digits, string callSid, string ivrWidgetCallId, int tryCount)
        {
            string retVal = string.Empty;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "IvrWidgetPhoneGet started. callSid = {0}, ivrWidgetCallId = {1}, digits = {2}, tryCount = {3}",
                    callSid, ivrWidgetCallId, digits, tryCount);
                BusinessLogic.TwilioBL.TwilioHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioHandler(null);
                retVal = handler.IvrWidgetPhoneGet(digits, callSid, ivrWidgetCallId, tryCount);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IvrWidgetPhoneGet. callSid = {0}, ivrWidgetCallId = {1}, digits = {2}, tryCount = {3}",
                    callSid, ivrWidgetCallId, digits, tryCount);
            }
            return retVal;
        }

        [WebMethod]
        public string IvrWidgetPhoneConfirm(string digits, string callSid, string ivrWidgetCallId, int tryCount)
        {
            string retVal = string.Empty;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "IvrWidgetPhoneConfirm started. callSid = {0}, ivrWidgetCallId = {1}, digits = {2}, tryCount = {3}",
                    callSid, ivrWidgetCallId, digits, tryCount);
                BusinessLogic.TwilioBL.TwilioHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioHandler(null);
                retVal = handler.IvrWidgetPhoneConfirm(digits, callSid, ivrWidgetCallId, tryCount);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IvrWidgetPhoneConfirm. callSid = {0}, ivrWidgetCallId = {1}, digits = {2}, tryCount = {3}",
                    callSid, ivrWidgetCallId, digits, tryCount);
            }
            return retVal;
        }

        [WebMethod]
        public string IvrWidgetAreaGet(string digits, string callSid, string ivrWidgetCallId, int tryCount)
        {
            string retVal = string.Empty;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "IvrWidgetAreaGet started. callSid = {0}, ivrWidgetCallId = {1}, digits = {2}, tryCount = {3}",
                    callSid, ivrWidgetCallId, digits, tryCount);
                BusinessLogic.TwilioBL.TwilioHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioHandler(null);
                retVal = handler.IvrWidgetAreaGet(digits, callSid, ivrWidgetCallId, tryCount);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IvrWidgetAreaGet. callSid = {0}, ivrWidgetCallId = {1}, digits = {2}, tryCount = {3}",
                    callSid, ivrWidgetCallId, digits, tryCount);
            }
            return retVal;
        }

        [WebMethod]
        public string IvrWidgetAreaConfirm(string digits, string callSid, string ivrWidgetCallId, int tryCount)
        {
            string retVal = string.Empty;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "IvrWidgetAreaConfirm started. callSid = {0}, ivrWidgetCallId = {1}, digits = {2}, tryCount = {3}",
                    callSid, ivrWidgetCallId, digits, tryCount);
                BusinessLogic.TwilioBL.TwilioHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioHandler(null);
                retVal = handler.IvrWidgetAreaConfirm(digits, callSid, ivrWidgetCallId, tryCount);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IvrWidgetAreaConfirm. callSid = {0}, ivrWidgetCallId = {1}, digits = {2}, tryCount = {3}",
                    callSid, ivrWidgetCallId, digits, tryCount);
            }
            return retVal;
        }

        [WebMethod]
        public void IvrWidgetStatusCallback(string callDuration, string callSid, string callStatus)
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "IvrWidgetStatusCallback started. callDuration = {0}, callSid = {1}, callStatus = {2}",
                    callDuration, callSid, callStatus);
                BusinessLogic.TwilioBL.TwilioHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioHandler(null);
                handler.IvrWidgetStatusCallback(callDuration, callSid, callStatus);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IvrWidgetStatusCallback. callDuration = {0}, callSid = {1}, callStatus = {2}",
                    callDuration, callSid, callStatus);
            }
        }

        [WebMethod]
        public string IvrWidgetFallBack(
            string callSid, string from, string to,
            string callStatus, string errorCode, string errorUrl)
        {
            string retVal = string.Empty;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "IvrWidgetFallBack started. callSid = {0}, from = {1}, to = {2}, callStatus = {3}, errorCode = {4}, errorUrl = {5}",
                    callSid, from, to, callStatus, errorCode, errorUrl);
                BusinessLogic.TwilioBL.TwilioHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioHandler(null);
                retVal = handler.IvrWidgetFallBack(callSid, from, to, callStatus, errorCode, errorUrl);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IvrWidgetFallBack. callSid = {0}, from = {1}, to = {2}, callStatus = {3}, errorCode = {4}, errorUrl = {5}",
                    callSid, from, to, callStatus, errorCode, errorUrl);
            }
            return retVal;
        }

        #endregion

        #region Auto Upsale Call Back Web Methods

        [WebMethod]
        public string AutoUpsaleCallBackConsultation(
            string callSid, string accountSid, string from, string to,
            string callStatus, string apiVersion, string direction, string id, string headingIvrName
            )
        {
            string retVal = string.Empty;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AutoUpsaleCallBackConsultation started. Params: callSid= {0}, accountSid= {1}, from= {2}, to= {3}, callStatus= {4}, apiVersion= {5}, direction= {6}, id= {7}, headingIvrName = {8}",
                    callSid, accountSid, from, to, callStatus, apiVersion, direction, id, headingIvrName);
                BusinessLogic.TwilioBL.TwilioAutoUpsaleCallBackHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioAutoUpsaleCallBackHandler(null);
                retVal = handler.MainConsultation(callSid, accountSid, from, to, callStatus, apiVersion, direction, id, headingIvrName);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AutoUpsaleCallBackConsultation. Params: callSid= {0}, accountSid= {1}, from= {2}, to= {3}, callStatus= {4}, apiVersion= {5}, direction= {6}, id= {7}, headingIvrName = {8}",
                    callSid, accountSid, from, to, callStatus, apiVersion, direction, id, headingIvrName);
            }
            LogUtils.MyHandle.WriteToLog(9, "_AutoUpsaleCallBackConsultation ended. Params: callSid= {0}, accountSid= {1}, from= {2}, to= {3}, callStatus= {4}, apiVersion= {5}, direction= {6}, id= {7}, headingIvrName = {8}, retVal = {9}",
                   callSid, accountSid, from, to, callStatus, apiVersion, direction, id, headingIvrName, retVal);
            return retVal;
        }

        [WebMethod]
        public string AutoUpsaleCallBackMainClickHandler
            (string digits, string callSid, string id, string headingIvrName)
        {
            string retVal = string.Empty;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AutoUpsaleCallBackMainClickHandler started. digits = {0}, callSid = {1}, id = {2}, headingIvrName = {3}", digits, callSid, id, headingIvrName);
                BusinessLogic.TwilioBL.TwilioAutoUpsaleCallBackHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioAutoUpsaleCallBackHandler(null);
                retVal = handler.MainClickHandler(digits, callSid, id, headingIvrName);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AutoUpsaleCallBackMainClickHandler. digits = {0}, callSid = {1}, id = {2}, headingIvrName = {3}", digits, callSid, id, headingIvrName);
            }
            LogUtils.MyHandle.WriteToLog(9, "_AutoUpsaleCallBackMainClickHandler ended. digits = {0}, callSid = {1}, id = {2}, headingIvrName = {3}, retVal = {4}", digits, callSid, id, headingIvrName, retVal);
            return retVal;
        }

        [WebMethod]
        public string AutoUpsaleCallBackSecondaryClickHandler
            (string digits, string callSid, string id, string headingIvrName)
        {
            string retVal = string.Empty;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AutoUpsaleCallBackSecondaryClickHandler started. digits = {0}, callSid = {1}, id = {2}, headingIvrName = {3}", digits, callSid, id, headingIvrName);
                BusinessLogic.TwilioBL.TwilioAutoUpsaleCallBackHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioAutoUpsaleCallBackHandler(null);
                retVal = handler.SecodaryClickHandler(digits, callSid, id, headingIvrName);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AutoUpsaleCallBackSecondaryClickHandler. digits = {0}, callSid = {1}, id = {2}, headingIvrName = {3}", digits, callSid, id, headingIvrName);
            }
            LogUtils.MyHandle.WriteToLog(9, "_AutoUpsaleCallBackSecondaryClickHandler ended. digits = {0}, callSid = {1}, id = {2}, headingIvrName = {3}, retVal = {4}", digits, callSid, id, headingIvrName, retVal);
            return retVal;
        }

        [WebMethod]
        public string AutoUpsaleCallBackFallback
            (string callSid, string accountSid, string from, string to,
            string callStatus, string apiVersion, string direction, string id,
            string errorCode, string errorUrl)
        {
            string retVal = string.Empty;
            try
            {
                LogUtils.MyHandle.WriteToLog(8, @"AutoUpsaleCallBackFallback started. Params: callSid= {0}, accountSid= {1}, from= {2}, to= {3}, callStatus= {4}, apiVersion= {5}, direction= {6}, id= {7}, errorCode= {8}, errorUrl= {9}",
                    callSid, accountSid, from, to, callStatus, apiVersion, direction, id, errorCode, errorUrl);
                BusinessLogic.TwilioBL.TwilioAutoUpsaleCallBackHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioAutoUpsaleCallBackHandler(null);
                retVal = handler.Fallback(callSid, accountSid, from, to, callStatus, apiVersion, direction, id, errorCode, errorUrl);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AutoUpsaleCallBackFallback. Params: callSid= {0}, accountSid= {1}, from= {2}, to= {3}, callStatus= {4}, apiVersion= {5}, direction= {6}, id= {7}, errorCode= {8}, errorUrl= {9}",
                    callSid, accountSid, from, to, callStatus, apiVersion, direction, id, errorCode, errorUrl);
            }
            return retVal;
        }

        [WebMethod]
        public void AutoUpsaleCallBackStatusCallback(
             string callDuration, string callSid, string accountSid, string from, string to,
             string callStatus, string apiVersion, string direction, string id
             )
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "AutoUpsaleCallBackStatusCallback started. Params: callDuration = {8}, callSid= {0}, accountSid= {1}, from= {2}, to= {3}, callStatus= {4}, apiVersion= {5}, direction= {6}, id= {7}",
                    callSid, accountSid, from, to, callStatus, apiVersion, direction, id, callDuration);
                BusinessLogic.TwilioBL.TwilioAutoUpsaleCallBackHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioAutoUpsaleCallBackHandler(null);
                handler.StatusCallBack(callDuration, callSid, accountSid, from, to, callStatus, apiVersion, direction, id);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AutoUpsaleCallBackStatusCallback. Params: callDuration = {8}, callSid= {0}, accountSid= {1}, from= {2}, to= {3}, callStatus= {4}, apiVersion= {5}, direction= {6}, id= {7}",
                    callSid, accountSid, from, to, callStatus, apiVersion, direction, id, callDuration);
            }
        }

        #endregion

        #region Expired Call

        [WebMethod]
        public string ExpiredCallAnswered(string callId, string callStatus, bool again)
        {
            string retVal = string.Empty;
            LogUtils.MyHandle.WriteToLog(8, "ExpiredCallAnswered started.");
            try
            {
                BusinessLogic.SupplierBL.ExpiredIvr.ExpiredIvrCallHandler handler = new NoProblem.Core.BusinessLogic.SupplierBL.ExpiredIvr.ExpiredIvrCallHandler(null);
                retVal = handler.ExpiredCallAnswered(callId, callStatus, again);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ExpiredCallAnswered");
            }
            return retVal;
        }

        [WebMethod]
        public string ExpiredCallClickHandler(string callId, string digits)
        {
            string retVal = string.Empty;
            LogUtils.MyHandle.WriteToLog(8, "ExpiredCallClickHandler started.");
            try
            {
                BusinessLogic.SupplierBL.ExpiredIvr.ExpiredIvrCallHandler handler = new NoProblem.Core.BusinessLogic.SupplierBL.ExpiredIvr.ExpiredIvrCallHandler(null);
                retVal = handler.ExpiredCallClickHandler(callId, digits);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ExpiredCallClickHandler");
            }
            return retVal;
        }

        [WebMethod]
        public void ExpiredCallCallback(string callStatus, string callId)
        {
            LogUtils.MyHandle.WriteToLog(8, "ExpiredCallCallback started.");
            try
            {
                BusinessLogic.SupplierBL.ExpiredIvr.ExpiredIvrCallHandler handler = new NoProblem.Core.BusinessLogic.SupplierBL.ExpiredIvr.ExpiredIvrCallHandler(null);
                handler.ExpiredCallCallback(callStatus, callId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ExpiredCallCallback");
            }
        }

        [WebMethod]
        public void ExpiredRepresentativeCallback(string recording, string callId)
        {
            LogUtils.MyHandle.WriteToLog(8, "ExpiredRepresentativeCallback started.");
            try
            {
                BusinessLogic.SupplierBL.ExpiredIvr.ExpiredIvrCallHandler handler = new NoProblem.Core.BusinessLogic.SupplierBL.ExpiredIvr.ExpiredIvrCallHandler(null);
                handler.ExpiredRepresentativeCallback(recording, callId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ExpiredRepresentativeCallback");
            }
        }

        #endregion

        #region Password Call

        [WebMethod]
        public string PasswordCallAnswered(string id)
        {
            string retVal = string.Empty;
            LogUtils.MyHandle.WriteToLog(8, "PasswordCallAnswered started.");
            try
            {
                BusinessLogic.SupplierBL.PasswordCallManager manager = new NoProblem.Core.BusinessLogic.SupplierBL.PasswordCallManager(null);
                retVal = manager.CallAnswered(id);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in PasswordCallAnswered");
            }
            return retVal;
        }

        [WebMethod]
        public string PasswordCallClickHandler(string digits, string callSid, string id)
        {
            string retVal = string.Empty;
            LogUtils.MyHandle.WriteToLog(8, "PasswordCallClickHandler started.");
            try
            {
                BusinessLogic.SupplierBL.PasswordCallManager manager = new NoProblem.Core.BusinessLogic.SupplierBL.PasswordCallManager(null);
                retVal = manager.ClickHandler(id, digits);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in PasswordCallClickHandler");
            }
            return retVal;
        }

        #endregion

        #region Get Started Test Call Methods

        [WebMethod]
        public string GetStartedCallTestFakeConsumer()
        {
            LogUtils.MyHandle.WriteToLog(8, "GetStartedCallTestFakeConsumer started.");
            string retVal = null;
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager handler = new BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager(null);
                retVal = handler.GetStartedCallTestFakeCustomer();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GetStartedCallTestFakeConsumer");
            }
            return retVal;

        }

        [WebMethod]
        public string TestCallAnswered(
            string callSid, string accountSid, string from, string to,
            string callStatus, string direction, string testCallId)
        {
            string retVal = string.Empty;
            LogUtils.MyHandle.WriteToLog(8, "TestCallAnswered started.");
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager(null);
                retVal = manager.TestCallAnswered(callSid, accountSid, from, to, callStatus, direction, testCallId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in TestCallAnswered");
            }
            return retVal;
        }

        [WebMethod]
        public string TestCallClickHandler(string digits, string callSid, string testCallId)
        {
            string retVal = string.Empty;
            LogUtils.MyHandle.WriteToLog(8, "TestCallClickHandler started.");
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager(null);
                retVal = manager.ClickHandler(digits, callSid, testCallId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in TestCallClickHandler");
            }
            return retVal;
        }

        [WebMethod]
        public void TestCallCallback(string callDuration, string callSid, string accountSid, string from, string to,
           string callStatus, string testCallId)
        {
            LogUtils.MyHandle.WriteToLog(8, "TestCallCallback started.");
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager(null);
                manager.TestCallCallback(callDuration, callSid, accountSid, from, to, callStatus, testCallId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in TestCallCallback");
            }
        }

        [WebMethod]
        public void TestCallConsumerCallback(
            string dialCallStatus, string dialCallSid, string dialCallDuration, string recordingUrl, string testCallId)
        {
            LogUtils.MyHandle.WriteToLog(8, "TestCallConsumerCallback started.");
            try
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTestCallManager(null);
                manager.TestCallConsumerCallback(dialCallStatus, dialCallSid, dialCallDuration, recordingUrl, testCallId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in TestCallConsumerCallback");
            }
        }

        #endregion
        #region Receive SMS
        [WebMethod]
        public void GetSmsCallBack(string FromPhone, string message)
        {
            try
            {
                NoProblem.Core.BusinessLogic.AAR.AarManager handler = new BusinessLogic.AAR.AarManager(null);
                handler.GetSmsCallBack(FromPhone, message);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                    "Exception in GetSmsCallBack. FromPhone = {0}, message = {1}",
                    FromPhone, message);
            }
        }
        [WebMethod]
        public void sf_GetSmsCallBack(string FromPhone, string message)
        {
            try
            {
                NoProblem.Core.BusinessLogic.SmsEngine.SmsUtility.GetSmsCallBack(FromPhone, message);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                    "Exception in sf_GetSmsCallBack. FromPhone = {0}, message = {1}",
                    FromPhone, message);
            }
        }
        #endregion
        [WebMethod]
        public string LeadBuyersCustomerCallAnswered(string callSid, string accountSid, string from, string to,
            string callStatus, string apiVersion, string direction, string answeredBy, string ids)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(8,
                "LeadBuyersCustomerCallAnswered started. callSid = {0}, accountSid = {1}, from = {2}, to = {3}, callStatus = {4}, apiVersion = {5}, direction = {6}, answeredBy = {7}, ids = {8}",
                callSid, accountSid, from, to, callStatus, apiVersion, direction, answeredBy, ids);
            try
            {
                BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler handler = new NoProblem.Core.BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler(null);
                retVal = handler.LeadBuyersCustomerCallAnswered(callSid, accountSid, from, to, callStatus, apiVersion, direction, answeredBy, ids);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                    "Exception in LeadBuyersCustomerCallAnswered. callSid = {0}, accountSid = {1}, from = {2}, to = {3}, callStatus = {4}, apiVersion = {5}, direction = {6}, answeredBy = {7}, ids = {8}",
                    callSid, accountSid, from, to, callStatus, apiVersion, direction, answeredBy, ids);
            }
            return retVal;
        }

        [WebMethod]
        public string LeadBuyersCallClickHandler(string ids, string digits, string callSid)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(8,
                "LeadBuyersCallClickHandler started. callSid = {0}, ids = {1}, digits = {2}",
                callSid, ids, digits);
            try
            {
                BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler handler = new NoProblem.Core.BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler(null);
                retVal = handler.LeadBuyersCallClickHandler(ids, digits, callSid);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                    "Exception in LeadBuyersCallClickHandler. callSid = {0}, ids = {1}, digits = {2}",
                callSid, ids, digits);
            }
            return retVal;
        }

        [WebMethod]
        public void LeadBuyersCustomerCallCallback(string callDuration, string callSid, string accountSid, string from, string to,
            string callStatus, string apiVersion, string direction, string ids)
        {
            LogUtils.MyHandle.WriteToLog(8,
                "LeadBuyersCustomerCallCallback started. callDuration = {0}, callSid = {1}, accountSid = {2}, from = {3}, to = {4}, callStatus = {5}, apiVersion = {6}, direction = {7}, ids = {8}",
                callDuration, callSid, accountSid, from, to, callStatus, apiVersion, direction, ids);
            try
            {
                BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler handler = new NoProblem.Core.BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler(null);
                handler.LeadBuyersCustomerCallCallback(callDuration, callSid, accountSid, from, to, callStatus, apiVersion, direction, ids);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                "Exception in LeadBuyersCustomerCallCallback. callDuration = {0}, callSid = {1}, accountSid = {2}, from = {3}, to = {4}, callStatus = {5}, apiVersion = {6}, direction = {7}, ids = {8}",
                callDuration, callSid, accountSid, from, to, callStatus, apiVersion, direction, ids);
            }
        }

        [WebMethod]
        public void LeadBuyersCallConferenceCallBack(string callSid, string recordingUrl, string ids)
        {
            LogUtils.MyHandle.WriteToLog(8,
                "LeadBuyersCallConferenceCallBack started. callSid = {0}, recordingUrl = {1}, ids = {2}",
                callSid, recordingUrl, ids);
            try
            {
                BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler handler = new NoProblem.Core.BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler(null);
                handler.LeadBuyersCallConferenceCallBack(callSid, recordingUrl, ids);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                    "Exception in LeadBuyersCallConferenceCallBack. callSid = {0}, recordingUrl = {1}, ids = {2}",
                    callSid, recordingUrl, ids);
            }
        }

        [WebMethod]
        public string LeadBuyersBrokerCallAnswered(string callSid, string accountSid, string from, string to,
            string callStatus, string apiVersion, string direction, string ids)
        {
            string retVal = null;
            LogUtils.MyHandle.WriteToLog(8,
                "LeadBuyersBrokerCallAnswered started. callSid = {0}, accountSid = {1}, from = {2}, to = {3}, callStatus = {4}, apiVersion = {5}, direction = {6}, ids = {7}",
                    callSid, accountSid, from, to, callStatus, apiVersion, direction, ids);
            try
            {
                BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler handler = new NoProblem.Core.BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler(null);
                retVal = handler.LeadBuyersBrokerCallAnswered(callSid, accountSid, from, to, callStatus, apiVersion, direction, ids);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                    "Exception in LeadBuyersBrokerCallAnswered. callSid = {0}, accountSid = {1}, from = {2}, to = {3}, callStatus = {4}, apiVersion = {5}, direction = {6}, ids = {7}",
                    callSid, accountSid, from, to, callStatus, apiVersion, direction, ids);
            }
            return retVal;
        }

        [WebMethod]
        public void LeadBuyersBrokerCallCallback(string callDuration, string callSid, string accountSid, string from, string to,
            string callStatus, string apiVersion, string direction, string ids)
        {
            LogUtils.MyHandle.WriteToLog(8,
                "LeadBuyersBrokerCallCallback started. callDuration = {0}, callSid = {1}, accountSid = {2}, from = {3}, to = {4}, callStatus = {5}, apiVersion = {6}, direction = {7}, ids = {8}",
                callDuration, callSid, accountSid, from, to, callStatus, apiVersion, direction, ids);
            try
            {
                BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler handler = new NoProblem.Core.BusinessLogic.LeadBuyers.LeadBuyersPhoneHandler(null);
                handler.LeadBuyersBrokerCallCallback(callDuration, callSid, accountSid, from, to, callStatus, apiVersion, direction, ids);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,
                "Exception in LeadBuyersBrokerCallCallback. callDuration = {0}, callSid = {1}, accountSid = {2}, from = {3}, to = {4}, callStatus = {5}, apiVersion = {6}, direction = {7}, ids = {8}",
                callDuration, callSid, accountSid, from, to, callStatus, apiVersion, direction, ids);
            }
        }
        [WebMethod]
        public string IvrGetTheApp(string fromPhone, string toPhone)
        {
           // LogUtils.MyHandle.WriteToLog(8, "IvrGetTheApp started.");
            string retVal = null;
            try
            {
                NoProblem.Core.BusinessLogic.SmsEngine.SmsIvrPhoneCampaign sipc = new BusinessLogic.SmsEngine.SmsIvrPhoneCampaign(fromPhone, toPhone);
                retVal = sipc.Execute();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IvrGetTheApp");
            }
            return retVal;

        }
        #region Vienna

        [WebMethod]
        public string ViennaConsultation(string callSid, string from, string to, string callStatus)
        {
            string retVal = string.Empty;
            try
            {
                BusinessLogic.TwilioBL.IncomingCalls.IncomingCallsHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.IncomingCalls.IncomingCallsHandler();
                retVal = handler.Consultation(callSid, from, to, callStatus);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ViennaConsultation. callSid = {0}, from = {1}, to = {2}, callStatus = {3}",
                    callSid, from, to, callStatus);
            }
            return retVal;
        }

        [WebMethod]
        public void ViennaDialStatusCallback(
            string dialCallStatus, string dialCallSid, string dialCallDuration, string recordingUrl, bool isUnmapped,
                    string incomingCallSid, string customerPhone, string virtualNumber, string advertiserPhone)
        {
            try
            {
                BusinessLogic.TwilioBL.IncomingCalls.IncomingCallsHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.IncomingCalls.IncomingCallsHandler();
                handler.DialStatusCallBack(
                     dialCallStatus, dialCallSid, dialCallDuration, recordingUrl, isUnmapped,
                     incomingCallSid, customerPhone, virtualNumber, advertiserPhone);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ViennaStatusCallback. callDuration = {0}, callSid = {1}, callStatus = {2}",
                   dialCallDuration, dialCallSid, dialCallStatus);
            }
        }

        [WebMethod]
        public void ViennaIncomingCallStatusCallback(
            string callDuration, string callSid, string callStatus, string from, string to
            )
        {
            try
            {
                BusinessLogic.TwilioBL.IncomingCalls.IncomingCallsHandler handler = new NoProblem.Core.BusinessLogic.TwilioBL.IncomingCalls.IncomingCallsHandler();
                handler.IncomingCallStatusCallBack(callDuration, callSid, callStatus, from, to);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ViennaIncomingCallStatusCallback. callDuration = {0}, callSid = {1}, callStatus = {2}",
                   callDuration, callSid, callStatus);
            }
        }

        #endregion
    }
}
