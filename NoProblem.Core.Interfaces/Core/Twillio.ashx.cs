﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for Twillio
    /// </summary>
    public class Twillio : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
          //  string body = GetDocumentContents(context.Request);
            string phone = context.Request.Params["From"];
            string message = context.Request.Params["Body"];
            NoProblem.Core.BusinessLogic.AAR.AarManager handler = new BusinessLogic.AAR.AarManager(null);
            handler.GetSmsCallBack(phone, message);
            context.Response.ContentType = "text/plain";
     //       context.Response.Write("Hello World");
        }
        private string GetDocumentContents(HttpRequest Request)
        {
            string documentContents;
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, System.Text.Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }
            return documentContents;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}