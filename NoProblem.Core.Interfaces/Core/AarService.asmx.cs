﻿using NoProblem.Core.BusinessLogic.AAR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace NoProblem.Core.Interfaces.Core
{
    /// <summary>
    /// Summary description for AarService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AarService : System.Web.Services.WebService
    {

        [WebMethod]
        public string ExecuteAarTextMessage(int count, Guid IncidentId, string RecordName)
        {
            NoProblem.Core.BusinessLogic.AAR.AarManager am = new BusinessLogic.AAR.AarManager(null);
//            System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(delegate(object state)
//            {
            int suppliers = -1;
            try
            {
                suppliers = am.CreateTextMessagesForServiceRequest(IncidentId, count, RecordName);
            } 
            catch(AarException aexc)
            {
                return aexc.Message;
                
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception in CreateTextMessagesForServiceRequest, IncidentId=" + IncidentId);
                return exc.Message;
            }
            return "Excute on " + suppliers + " suppliers!";
//            }));
        }
       
        [WebMethod]
        public void UpdateCameToLandingPage(NoProblem.Core.BusinessLogic.AAR.AarLandingPageRequestData _AarLandingPageRequestData)
        {
            NoProblem.Core.BusinessLogic.AAR.AarManager am = new BusinessLogic.AAR.AarManager(null);
            am.UpdateCameToLandingPage(_AarLandingPageRequestData);
        }

        [WebMethod]
        public AarCallIncidentData GetAarCallIncidentData(Guid aarCallId)
        {
            NoProblem.Core.BusinessLogic.AAR.AarManager am = new BusinessLogic.AAR.AarManager(null);
            AarCallIncidentData incidentData = am.GetSupplierByCallId(aarCallId);
            return incidentData;
            
        }

        [WebMethod]
        public ConnectSupplierToCustomerResponse ConnectSupplierToCustomer(Guid callId, Guid incidentId)
        {
            AarManager aarManager = new AarManager(null);
            return aarManager.ConnectSupplierToCustomer(callId, incidentId);
        }

        [WebMethod]
        public void FireEvent(string eventName,Guid callId, Guid incidentId)
        {
            AarManager aarManager = new AarManager(null);
            aarManager.FireEvent(eventName, callId, incidentId);
        }

        [WebMethod]
        public void ReportLead(Guid callId, Guid incidentId, int reportType, string commentText)
        {
            var aarManager = new AarManager(null);
            aarManager.ReportLead(callId, incidentId, reportType, commentText);
        }
        [WebMethod]
        public void LandingOnGetAppLink(Guid callId)
        {
            AarManager aarManager = new AarManager(null);
            aarManager.LandingOnGetAppLink(callId);
        }
        [WebMethod]
        public List<NoProblem.Core.BusinessLogic.SmsEngine.CampaignData> GetSmsCampaigns()
        {
            
            NoProblem.Core.BusinessLogic.SmsEngine.CampaignManager cm = new BusinessLogic.SmsEngine.CampaignManager();
            List<NoProblem.Core.BusinessLogic.SmsEngine.CampaignData> list = cm.GetCampaignDetails();
            return list;
        }
        [WebMethod]
        public List<NoProblem.Core.BusinessLogic.SmsEngine.CampaignTaskData> GetSmsCampaignTasks()
        {

            NoProblem.Core.BusinessLogic.SmsEngine.CampaignManager cm = new BusinessLogic.SmsEngine.CampaignManager();
            List<NoProblem.Core.BusinessLogic.SmsEngine.CampaignTaskData> list = cm.GetCampaignTaskDetails();
            return list;
        }
        [WebMethod]
        public NoProblem.Core.BusinessLogic.SmsEngine.CampaignManager.eCampaignCreateResponse SetCampaign(string CampaignName, string text)
        {
            NoProblem.Core.BusinessLogic.SmsEngine.CampaignManager cm = new BusinessLogic.SmsEngine.CampaignManager();
            BusinessLogic.SmsEngine.CampaignManager.eCampaignCreateResponse response = 
                cm.CreateCampaign(new BusinessLogic.SmsEngine.CampaignData() { name = CampaignName, text = text });            
            return response;
        }
        [WebMethod]
        public NoProblem.Core.BusinessLogic.SmsEngine.SmsManagerBase.SmsManagerResponse CreateBulkSms(string CsvFilePath, int CampaignId)
        {
            Context.Server.ScriptTimeout = 600;
            NoProblem.Core.BusinessLogic.SmsEngine.SmsManagerConsumers smc = new BusinessLogic.SmsEngine.SmsManagerConsumers(CsvFilePath, CampaignId);
            NoProblem.Core.BusinessLogic.SmsEngine.SmsManagerBase.SmsManagerResponse response = new BusinessLogic.SmsEngine.SmsManagerBase.SmsManagerResponse();
            try
            {                
                response.status = smc.Execute();
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in CreateBulkSms CsvFilePath:{0}, CampaignId:{1}", CsvFilePath, CampaignId));
                response.status = BusinessLogic.SmsEngine.SmsManagerBase.eSmsStatus.SERVER_GENERAL_EXCEPTION;
            }
            return response;
        }
         [WebMethod]
        public NoProblem.Core.BusinessLogic.SmsEngine.SmsManagerBase.SmsManagerResponse ReExecuteTaskSms(int BulkId)
        {
            NoProblem.Core.BusinessLogic.SmsEngine.SmsManagerConsumers smc = new BusinessLogic.SmsEngine.SmsManagerConsumers(BulkId);
            NoProblem.Core.BusinessLogic.SmsEngine.SmsManagerBase.SmsManagerResponse response = new BusinessLogic.SmsEngine.SmsManagerBase.SmsManagerResponse();
            try
            {
                response.status = smc.ReExecuteTask();
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in ReExecuteTaskSms BulkId:{0}", BulkId));
                response.status = BusinessLogic.SmsEngine.SmsManagerBase.eSmsStatus.SERVER_GENERAL_EXCEPTION;
            }
            return response;
        }


    }
}
