﻿using Effect.Crm.Logs;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.APIs.SellersApi.PingPost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace NoProblem.Core.Interfaces.Core.APIs.SellersApi
{
    /// <summary>
    /// Summary description for PingPost
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class PingPost : NpServiceBase
    {
        [WebMethod]
        public Result<PingResponse> Ping(PingRequest request)
        {
            var result = new Result<PingResponse>();
            try
            {
                BusinessLogic.APIs.SellersApi.PingPost.PingManager pp = new BusinessLogic.APIs.SellersApi.PingPost.PingManager();
                result.Value = pp.Ping(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PingPost.Ping, leadid = {0}, sellerId = {1}", request.LeadId, request.SellerId);
                if (exc.InnerException != null)
                {
                    LogUtils.MyHandle.HandleException(exc.InnerException, "InnerException for PingPost.Ping");
                }
            }
            return result;
        }

        [WebMethod]
        public Result<PostResponse> Post(PostRequest request)
        {
            var result = new Result<PostResponse>();
            try
            {
                BusinessLogic.APIs.SellersApi.PingPost.PostManager pp = new BusinessLogic.APIs.SellersApi.PingPost.PostManager();
                result.Value = pp.Post(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PingPost.Post");
            }
            return result;
        }

        [WebMethod]
        public Result<bool> IsNpPingIdValidForPost(string npPingId)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                result.Value = BusinessLogic.APIs.SellersApi.PingPost.PingIdValidator.IsNpPingIdValidForPost(npPingId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PingPost.IsNpPingIdValidForPost");
            }
            return result;
        }
    }
}
