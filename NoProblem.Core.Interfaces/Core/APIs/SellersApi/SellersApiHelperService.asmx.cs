﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.Interfaces.Core.APIs.SellersApi
{
    /// <summary>
    /// Summary description for SellerApiHelperService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SellersApiHelperService : System.Web.Services.WebService
    {
        [WebMethod]
        public Result<bool> VerifySellerId(string sellerId)
        {            
            var result = new Result<bool>();
            result.Value = BusinessLogic.APIs.SellersApi.SellersApiHelper.VerifySellerId(sellerId);
            return result;
        }

        [WebMethod]
        public Result<bool> VerifyCategory(string categoryCode)
        {
            var result = new Result<bool>();
            BusinessLogic.Expertises.ExpertiseManager manager = new BusinessLogic.Expertises.ExpertiseManager(null);
            result.Value = manager.VerifyCategoryCodeExists(categoryCode);
            return result;
        }
    }
}
