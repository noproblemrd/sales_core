﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using NoProblem.Core.DataModel.APIs.SellersApi.PingPhone;
using NoProblem.Core.DataModel;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.APIs.SellersApi.Soleo;

namespace NoProblem.Core.Interfaces.Core.APIs.SellersApi
{
    /// <summary>
    /// Summary description for PingPhone
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class PingPhone : NpServiceBase
    {

        [WebMethod]
        public Result<Ping1Response> Ping1(Ping1Request request)
        {
            var result = new Result<Ping1Response>();
            try
            {
                BusinessLogic.APIs.SellersApi.PingPhone.Ping1Manager pp = new BusinessLogic.APIs.SellersApi.PingPhone.Ping1Manager(null);
                result.Value = pp.Ping1(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PingPhone.Ping1");
            }
            return result;
        }

        [WebMethod]
        public Result<Ping2Response> Ping2(Ping2Request request)
        {
            var result = new Result<Ping2Response>();
            try
            {
                BusinessLogic.APIs.SellersApi.PingPhone.Ping2Manager pp = new BusinessLogic.APIs.SellersApi.PingPhone.Ping2Manager(null);
                result.Value = pp.Ping2(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PingPhone.Ping2");
            }
            return result;
        }

        [WebMethod]
        public Result<bool> IsNpPingIdValidForPing2(Guid npPingId)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.APIs.SellersApi.PingPhone.Ping2Manager pp = new BusinessLogic.APIs.SellersApi.PingPhone.Ping2Manager(null);
                result.Value = pp.IsNpPingIdValidForPing2(npPingId);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PingPhone.IsNpPingIdValidForPing2");
            }
            return result;
        }
         [WebMethod]
        public Result<SoleoPingResponse> SoleoPing(SoleoPingRequest request)
        {
            Result<SoleoPingResponse> result = new Result<SoleoPingResponse>();
            try
            {
                NoProblem.Core.BusinessLogic.APIs.SellersApi.Soleo.SoleoManager manager = new BusinessLogic.APIs.SellersApi.Soleo.SoleoManager();
                result.Value = manager.Get(request);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SoleoPing");
            }
            return result;
        }
    }
}
