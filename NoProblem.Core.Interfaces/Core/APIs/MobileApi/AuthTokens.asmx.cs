﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using NoProblem.Core.DataModel;
using Effect.Crm.Logs;

namespace NoProblem.Core.Interfaces.Core.APIs.MobileApi
{
    /// <summary>
    /// Summary description for AuthTokens
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AuthTokens : NpServiceBase
    {

        [WebMethod]
        public Result<bool> VerifyToken(string token, Guid id, bool isSupplier)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.APIs.MobileAPI.Devices.AuthTokensManager manager = new BusinessLogic.APIs.MobileAPI.Devices.AuthTokensManager();
                bool isVerified = manager.VerifyToken(token, id, isSupplier);
                result.Value = isVerified;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in VerifyToken");
                AddFailureToResult(result, exc);
            }
            return result;
        }
    }
}
