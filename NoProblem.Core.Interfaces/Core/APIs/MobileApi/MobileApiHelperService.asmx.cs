﻿using Effect.Crm.Logs;
using NoProblem.Core.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace NoProblem.Core.Interfaces.Core.APIs.MobileApi
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class MobileApiHelperService : NpServiceBase
    {
        private const string VerificationCodeMessage = "Your verification code is: {0}";

        [WebMethod]
        public Result<bool> SendPhoneValidationSms(string phone, string code)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                result.Value = BusinessLogic.APIs.MobileAPI.PhoneValidator.SendPhoneValidationSms(phone, code);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SendPhoneValidationSms");
            }
            return result;
        }

        [WebMethod]
        public Result<bool> SendApplicationPhoneValidationSms(string phone, string code)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                string smsBodyText = string.Format(VerificationCodeMessage, code);
                result.Value = BusinessLogic.APIs.MobileAPI.PhoneValidator.SendPhoneValidationSms(phone, smsBodyText);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in SendApplicationPhoneValidationSms");
            }
            return result;
        }



        [WebMethod]
        public Result<bool> PhoneVerificationCall(string phone, string pin)
        {
            Result<bool> result = new Result<bool>();
            try
            {
                BusinessLogic.SupplierBL.Registration2014.Mobile.PhoneVerificationCallManager manager =
                    new BusinessLogic.SupplierBL.Registration2014.Mobile.PhoneVerificationCallManager();
                result.Value = manager.MakeCall(phone, pin);
            }
            catch (Exception exc)
            {
                AddFailureToResult(result, exc);
                LogUtils.MyHandle.HandleException(exc, "Exception in PhoneVerificationCall");
            }
            return result;
        }

        [WebMethod]
        public Result SendFeedback(DataModel.APIs.MobileApi.Feedback feedback)
        {
            Result result = new Result();
            try
            {
                BusinessLogic.APIs.MobileAPI.FeedbackManager manager = new BusinessLogic.APIs.MobileAPI.FeedbackManager(null);
                manager.CreateFeedback(feedback);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SendFeedback");
                AddFailureToResult(result, exc);
            }
            return result;
        }
    }
}
