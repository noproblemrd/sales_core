﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CookComputing.XmlRpc;

namespace NoProblem.Core.Interfaces.Core.IncomingCallsGlobal
{
    /// <summary>
    /// Summary description for IncomingCallsGlobalService
    /// </summary>
    [WebService(Namespace = "http://noproblemppc.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class IncomingCallsGlobalService : System.Web.Services.WebService
    {
        private const string FAPI_EVENT = "fapi_event";

        [WebMethod]
        public void IncomingCall()
        {
            if (string.IsNullOrEmpty(Context.Request.Params[FAPI_EVENT]))
            {
                Asterisk.ViennaHandler handler = new NoProblem.Core.Interfaces.Core.Asterisk.ViennaHandler();
                XmlRpcHttpRequest httpReq = new XmlRpcHttpRequest(Context.Request);
                XmlRpcHttpResponse httpResp = new XmlRpcHttpResponse(Context.Response);
                handler.HandleHttpRequest(httpReq, httpResp);
            }
            else
            {
                FoneApi.FoneApiEntryPoint handler = new NoProblem.Core.Interfaces.Core.FoneApi.FoneApiEntryPoint();
                handler.CallBack();
            }
        }
    }
}
