﻿using AutoMapper;
using ClipCall.ServiceModel.Abstract.DataContracts.Response;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.Interfaces.AutoMapper
{
    public class CustomerProfile : Profile
    {
        protected override void Configure()
        {

            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();
            CreateMap<ConsumerIncidentData, LeadDetailsResponseData>()
                .ForMember(d => d.Id, o => o.MapFrom(s => s.IncidentId))
                .ForMember(d => d.SuppliersCount, o => o.MapFrom(s => s.OrderedProviders))
                .ForMember(d => d.ZipCode, o => o.MapFrom(s => s.RegionName));
        }

        public override string ProfileName
        {
            get { return this.GetType().Name; }
        }
    }        
}