﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class CriminalLawAttorneyCaseData : DalBase<DataModel.Xrm.new_criminallawattorneycasedata>
    {
        public CriminalLawAttorneyCaseData(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_criminallawattorneycasedata");
        }

        public override NoProblem.Core.DataModel.Xrm.new_criminallawattorneycasedata Retrieve(Guid id)
        {
            return XrmDataContext.new_criminallawattorneycasedatas.FirstOrDefault(x => x.new_criminallawattorneycasedataid == id);
        }
    }
}
