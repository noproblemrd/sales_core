﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class Availability : DalBase<DataModel.Xrm.new_availability>
    {
        private const string TIME_FORMAT = "HH:mm:ss";

        public Availability(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_availability");
        }

        public override IQueryable<DataModel.Xrm.new_availability> All
        {
            get
            {
                return XrmDataContext.new_availabilitynew_availabilities;
            }
        }

        public override NoProblem.Core.DataModel.Xrm.new_availability Retrieve(Guid id)
        {
            return XrmDataContext.new_availabilitynew_availabilities.FirstOrDefault(x => x.new_availabilityid == id);
        }

        public void Create(Guid supplierId, DateTime fromTimeUtc, DateTime toTimeUtc, DayOfWeek dayOfWeek)
        {
            DataModel.Xrm.new_availability entity = new DataModel.Xrm.new_availability();
            entity.new_day = ((int)dayOfWeek) + 1;//picklist in CRM is from 1 to 7 and enum 'DayOfWeek' is from 0 to 6.
            entity.new_from = fromTimeUtc.ToString(TIME_FORMAT);
            entity.new_to = toTimeUtc.ToString(TIME_FORMAT);
            entity.new_accountid = supplierId;
            base.Create(entity);
        }

        public bool IsInMessagingTime(DateTime localDate)
        {
            string query =
            @"select count(*)
            from new_availability with (nolock) 
            where deletionstatecode = 0
            and new_ismessagingtime = 1
            and new_day = @day
            and new_from <= CONVERT(char(8),@date,108)
            and new_to >= CONVERT(char(8),@date,108)";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@day", ((int)localDate.DayOfWeek) + 1));
            parameters.Add(new KeyValuePair<string, object>("@date", localDate));
            int count = (int)ExecuteScalar(query, parameters);
            bool retVal = false;
            if (count > 0)
            {
                retVal = true;
            }
            return retVal;
            
        }

        public List<DataModel.BusinessClosures.MessagingTimeData> GetMessagingTimes()
        {
            string query =
                @"select new_from, new_to, new_day
            from new_availability with (nolock) 
            where deletionstatecode = 0
            and new_ismessagingtime = 1";
            List<DataModel.BusinessClosures.MessagingTimeData> retVal = new List<NoProblem.Core.DataModel.BusinessClosures.MessagingTimeData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.BusinessClosures.MessagingTimeData data = new NoProblem.Core.DataModel.BusinessClosures.MessagingTimeData();
                            data.ToHour = (string)reader["new_to"];
                            data.FromHour = (string)reader["new_from"];
                            data.DayOfWeek = (DataModel.Xrm.new_availability.DayOfWeek)((int)reader["new_day"]);
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public void DeleteAllMessagingTimes()
        {
            string query =
                @"select new_availabilityid
            from new_availability with (nolock) 
            where deletionstatecode = 0
            and new_ismessagingtime = 1";
            List<Guid> ids = new List<Guid>();           
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {                            
                            Guid id = (Guid)reader["new_availabilityid"];
                            ids.Add(id);
                        }
                    }
                }
            }
            foreach (var id in ids)
            {
                DataModel.Xrm.new_availability availability = new NoProblem.Core.DataModel.Xrm.new_availability(XrmDataContext);
                availability.new_availabilityid = id;
                Delete(availability);
            }
            XrmDataContext.SaveChanges();
        }

        public IEnumerable<Guid> GetByAccountId(Guid supplierId)
        {
            string query =
                @"select new_availabilityid from new_availability with(nolock)
                    where deletionstatecode = 0 and new_accountid = @supplierId";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@supplierId", supplierId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
