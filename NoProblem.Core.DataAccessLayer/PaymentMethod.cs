﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class PaymentMethod : DalBase<DML.Xrm.new_paymentmethod>
    {
        #region Ctor
        public PaymentMethod(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_paymentmethod");
        }
        #endregion

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public override NoProblem.Core.DataModel.Xrm.new_paymentmethod Retrieve(Guid id)
        {
            DML.Xrm.new_paymentmethod paymentMethod = XrmDataContext.new_paymentmethods.FirstOrDefault(x => x.new_paymentmethodid == id);
            return paymentMethod;
        }

        public DML.Xrm.new_paymentmethod GetDefaultPaymentMethod()
        {
            DML.Xrm.new_paymentmethod method = XrmDataContext.new_paymentmethods.SingleOrDefault(x => x.new_isdefaultpaymentmethod == true);
            if (method == null)
            {
                throw new Exception("No default payment method has been declared in the system.");
            }
            return method;
        }

        public NoProblem.Core.DataModel.Xrm.new_paymentmethod GetPaymentMethodByCode(NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod paymentMethod)
        {
            return CacheManager.Instance.Get<DML.Xrm.new_paymentmethod>(cachePrefix + ".GetPaymentMethodByCode", paymentMethod.ToString(), GetPaymentMethodByCodePrivate, CacheManager.ONE_WEEK);
        }

        private NoProblem.Core.DataModel.Xrm.new_paymentmethod GetPaymentMethodByCodePrivate(string paymentMethodStr)
        {
            DML.Xrm.new_paymentmethod.ePaymentMethod paymentMethod = (DML.Xrm.new_paymentmethod.ePaymentMethod)Enum.Parse(typeof(DML.Xrm.new_paymentmethod.ePaymentMethod), paymentMethodStr);
            IEnumerable<DML.Xrm.new_paymentmethod> methods = from p in XrmDataContext.new_paymentmethods
                                                             where
                                                             p.new_paymentmethodcode.HasValue
                                                             &&
                                                             p.new_paymentmethodcode.Value == (int)paymentMethod
                                                             select
                                                             p;
            DML.Xrm.new_paymentmethod method = methods.FirstOrDefault();
            if (method == null)
            {
                throw new Exception(string.Format("Payment method with code '{0}', has not been declared in the system.", paymentMethod));
            }
            return method;
        }
    }
}
