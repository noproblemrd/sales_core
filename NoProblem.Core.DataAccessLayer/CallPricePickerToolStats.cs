﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class CallPricePickerToolStats : DalBase<DataModel.Xrm.new_callpricepickertoolstats>
    {
        public CallPricePickerToolStats(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_callpricepickertoolstats");
        }

        public override NoProblem.Core.DataModel.Xrm.new_callpricepickertoolstats Retrieve(Guid id)
        {
            return XrmDataContext.new_callpricepickertoolstatses.FirstOrDefault(x => x.new_callpricepickertoolstatsid == id);
        }
    }
}
