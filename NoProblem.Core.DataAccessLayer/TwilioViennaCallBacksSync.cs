﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class TwilioViennaCallBacksSync : Generic
    {
        public void Insert(string callSid)
        {
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@callSid", callSid);
            ExecuteNonQuery(insertQuery, parameters);
        }

        public bool FindAndDelete(string callSid)
        {
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@callSid", callSid);
            object scalar = ExecuteScalar(findAndDeleteQuery, parameters);
            return (bool)scalar;
        }

        private const string insertQuery =
            @"
                insert into tbl_twilioViennaCallBacksSync (callSid) values (@callSid)
            ";

        private const string findAndDeleteQuery =
            @"
                declare @found bit = 0
                select top 1 @found = 1 from tbl_twilioViennaCallBacksSync where callSid = @callSid
                delete tbl_twilioViennaCallBacksSync where callSid = @callSid
                select @found
            ";
    }
}
