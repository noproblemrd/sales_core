﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AccountMedia : DalBase<DataModel.Xrm.new_accountmedia>
    {
        public AccountMedia(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_accountmedia");
        }

        public override IQueryable<DataModel.Xrm.new_accountmedia> All
        {
            get
            {
                return XrmDataContext.new_accountmedias;
            }
        }

        public override DataModel.Xrm.new_accountmedia Retrieve(Guid id)
        {
            return XrmDataContext.new_accountmedias.FirstOrDefault(x => x.new_accountmediaid == id);
        }
    }
}
