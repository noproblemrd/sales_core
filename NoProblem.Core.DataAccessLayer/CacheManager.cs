﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;

namespace NoProblem.Core.DataAccessLayer
{
    public sealed class CacheManager
    {
        #region Fields

        private static readonly object cacheSync = new object();
        private const string dot = ".";
        private static volatile CacheManager _instance;
        private static readonly object instanceSync = new object();

        #endregion

        public const int ONE_MINUTE = 1;
        public const int ONE_HOUR = ONE_MINUTE * 60;
        public const int ONE_DAY = ONE_HOUR * 24;
        public const int ONE_WEEK = ONE_DAY * 7;
        public const int ONE_YEAR = ONE_DAY * 365;

        public static CacheManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (instanceSync)
                    {
                        if (_instance == null)
                        {
                            _instance = new CacheManager();
                        }
                    }
                }
                return _instance;
            }
        }

        #region Ctor

        private CacheManager()
        {
        }

        #endregion

        #region Public Methods
        
        public void ClearCache()
        {
            List<string> keyList = new List<string>();
            IDictionaryEnumerator CacheEnum = HttpRuntime.Cache.GetEnumerator();
            while (CacheEnum.MoveNext())
            {
                keyList.Add(CacheEnum.Key.ToString());
            }
            foreach (string key in keyList)
            {
                HttpRuntime.Cache.Remove(key);
            }
        }

        public T Get<T>(string prefix, string key, Func<T> getItemMethod) where T : class
        {
            return Get<T>(prefix, key, getItemMethod, ONE_HOUR);
        }

        public T Get<T>(string prefix, string key, Func<T> getItemMethod, int minutesToCacheExpire) where T : class
        {
            string cacheId = CreateCacheId(prefix, key);
            T retVal = HttpRuntime.Cache.Get(cacheId) as T;
            if (retVal == null)
            {
                lock (cacheSync)
                {
                    retVal = HttpRuntime.Cache.Get(cacheId) as T;
                    if (retVal == null)
                    {
                        retVal = getItemMethod();
                        if (retVal != null)
                        {
                            HttpRuntime.Cache.Insert(cacheId, retVal, null, DateTime.Now.AddMinutes(minutesToCacheExpire), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
                        }
                    }
                }
            }
            return retVal;
        }

        public T Get<T>(string prefix, string key, Func<string, T> getItemMethod) where T : class
        {
            return Get<T>(prefix, key, getItemMethod, ONE_HOUR);
        }

        public T Get<T>(string prefix, string key, Func<string, T> getItemMethod, int minutesToCacheExpire) where T : class
        {
            string cacheId = CreateCacheId(prefix, key);
            T retVal = HttpRuntime.Cache.Get(cacheId) as T;
            if (retVal == null)
            {
                lock (cacheSync)
                {
                    retVal = HttpRuntime.Cache.Get(cacheId) as T;
                    if (retVal == null)
                    {
                        retVal = getItemMethod(key);
                        if (retVal != null)
                        {
                            HttpRuntime.Cache.Insert(cacheId, retVal, null, DateTime.Now.AddMinutes(minutesToCacheExpire), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
                        }
                    }
                }
            }
            return retVal;
        }

        public void Insert(string prefix, string key, object value)
        {
            Insert(prefix, key, value, ONE_HOUR);
        }

        public void Insert(string prefix, string key, object value, int minutesToCacheExpire)
        {
            string cacheId = CreateCacheId(prefix, key);
            HttpRuntime.Cache.Insert(cacheId, value, null, DateTime.Now.AddMinutes(minutesToCacheExpire), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
        }

        public void ClearCache(string prefix, string key)
        {
            string cacheId = CreateCacheId(prefix, key);
            HttpRuntime.Cache.Remove(cacheId);
        }

        #endregion

        #region Private Methods

        private static string CreateCacheId(string prefix, string key)
        {
            return prefix + dot + key;
        }

        #endregion
    }
}
