﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class UserSettings : DalBase<DataModel.Xrm.usersettings>
    {
        public UserSettings(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("usersettings");
        }

        [Obsolete("This method always returns null. Settings cannot be retrieved by id.")]
        public override NoProblem.Core.DataModel.Xrm.usersettings Retrieve(Guid id)
        {
            return null;
        }

        public string GetUsersDefaultCurrencySymbol(Guid systemuserId)
        {
            string queryString = "select currencysymbol from usersettingsbase with (nolock) where systemuserid = @userId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@userId", systemuserId));
            string retVal = string.Empty;
            object scalar = ExecuteScalar(queryString, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        public string GetUsersDefaultCurrencyCode(Guid systemuserId)
        {
            string queryString = 
                @"select isocurrencycode 
                from usersettingsbase us with (nolock) 
                join transactioncurrency tc with(nolock) on tc.transactioncurrencyid = us.transactioncurrencyid
                where systemuserid = @userId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@userId", systemuserId));
            string retVal = string.Empty;
            object scalar = ExecuteScalar(queryString, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }
    }
}
