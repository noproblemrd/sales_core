﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class CallRecording : DalBase<DataModel.Xrm.new_callrecording>
    {
        public CallRecording(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_callrecording");
        }

        public override IQueryable<DataModel.Xrm.new_callrecording> All
        {
            get
            {
                return XrmDataContext.new_callrecordings;
            }
        }

        public override DataModel.Xrm.new_callrecording Retrieve(Guid id)
        {
            return XrmDataContext.new_callrecordings.FirstOrDefault(x => x.new_callrecordingid == id);
        }
    }
}
