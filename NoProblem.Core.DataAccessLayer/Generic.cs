﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.GenericUpsert
//  File: GenericUpsert.cs
//  Description: DAL for genericly updating entity entries.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using DML = NoProblem.Core.DataModel;
using System.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System;
using System.Data;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class Generic
    {
        protected static readonly string connectionStringProd = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";

        protected static readonly string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
        //       protected static readonly string connectionString = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";
        public static string GetConnectionString
        {
            get { return connectionString; }
        }
        public static string GetConnectionStringProd
        {
            get { return connectionStringProd; }
        }
        //   protected static readonly string connectionStringProduction = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";
        public Guid AddRecordToCRM(Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity entity, DataModel.Xrm.systemuser currentUser)
        {
            Guid rslt = AddRecordToCRM(entity, currentUser, DateTime.Now);
            return rslt;
        }

        public Guid AddRecordToCRM(Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity entity, DataModel.Xrm.systemuser currentUser, DateTime createdOn)
        {
            Guid rslt = Guid.NewGuid();
            string sqlString = "insert into " + entity.Name + "base(" + entity.Name + "id, statecode, statuscode, deletionstatecode, owninguser, owningbusinessunit, createdon, createdby, modifiedon, modifiedby)";
            sqlString += " values (N'{0}', 0, 1, 0, N'{1}', N'{2}', convert(datetime, N'{3}', 103), N'{4}', convert(datetime, N'{3}', 103), N'{4}')";
            //Effect.Crm.DB.DataBaseUtils.ExecuteSqlString(sqlString, rslt, currentUser.systemuserid, currentUser.businessunitid, createdOn.ToUniversalTime().ToString("dd/MM/yyyy HH:mm:ss"), currentUser.systemuserid);
            ExecuteNonQuery(String.Format(sqlString, rslt, currentUser.systemuserid, currentUser.businessunitid, createdOn.ToUniversalTime().ToString("dd/MM/yyyy HH:mm:ss"), currentUser.systemuserid));
            sqlString = "insert into " + entity.Name + "extensionbase(" + entity.Name + "id";
            string values = "N'" + Effect.Crm.Convertor.ConvertorUtils.ToSql(rslt.ToString()) + "'";
            for (int i = 0; i < entity.Properties.Length; i++)
            {
                Effect.Crm.SDKProviders.CrmServiceSdk.Property property = entity.Properties[i];
                sqlString += ", " + property.Name;
                string displayValue;
                if (property.GetType().Name == "CrmDecimalProperty")
                {
                    displayValue = ((Effect.Crm.SDKProviders.CrmServiceSdk.CrmDecimalProperty)property).Value.Value.ToString();
                }
                else
                {
                    displayValue = Effect.Crm.Convertor.ConvertorCRMParams.GetValueFromProperty(property);
                }
                values += ", " + (displayValue == "" ? "null" : "N'" + Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue) + "'");
            }
            sqlString += ") values(" + values + ")";
            //Effect.Crm.DB.DataBaseUtils.ExecuteSqlString(sqlString);
            ExecuteNonQuery(sqlString);
            return rslt;
        }

        public void CreateSqlInsertString(Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity entity, DataModel.Xrm.systemuser currentUser, DateTime createdOn, StringBuilder builder)
        {
            Guid rslt = Guid.NewGuid();
            string sqlString = "insert into " + entity.Name + "base(" + entity.Name + "id, statecode, statuscode, deletionstatecode, owninguser, owningbusinessunit, createdon, createdby, modifiedon, modifiedby)";
            sqlString += " values (N'{0}', 0, 1, 0, N'{1}', N'{2}', convert(datetime, N'{3}', 103), N'{4}', convert(datetime, N'{3}', 103), N'{4}')";
            //Effect.Crm.DB.DataBaseUtils.ExecuteSqlString(sqlString, rslt, currentUser.systemuserid, currentUser.businessunitid, createdOn.ToUniversalTime().ToString("dd/MM/yyyy HH:mm:ss"), currentUser.systemuserid);
            builder.AppendLine(String.Format(sqlString, rslt, currentUser.systemuserid, currentUser.businessunitid, createdOn.ToUniversalTime().ToString("dd/MM/yyyy HH:mm:ss"), currentUser.systemuserid));
            sqlString = "insert into " + entity.Name + "extensionbase(" + entity.Name + "id";
            string values = "N'" + Effect.Crm.Convertor.ConvertorUtils.ToSql(rslt.ToString()) + "'";
            for (int i = 0; i < entity.Properties.Length; i++)
            {
                Effect.Crm.SDKProviders.CrmServiceSdk.Property property = entity.Properties[i];
                sqlString += ", " + property.Name;
                string displayValue;
                if (property.GetType().Name == "CrmDecimalProperty")
                {
                    displayValue = ((Effect.Crm.SDKProviders.CrmServiceSdk.CrmDecimalProperty)property).Value.Value.ToString();
                }
                else
                {
                    displayValue = Effect.Crm.Convertor.ConvertorCRMParams.GetValueFromProperty(property);
                }
                values += ", " + (displayValue == "" ? "null" : "N'" + Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue) + "'");
            }
            sqlString += ") values(" + values + ")";
            builder.AppendLine(sqlString);
        }

        public void BulkCopy(string destinationTable, DataTable dataToCopy, List<SqlBulkCopyColumnMapping> mapping)
        {
            SqlBulkCopy bulk = new SqlBulkCopy(connectionString);
            bulk.DestinationTableName = destinationTable;
            foreach (var item in mapping)
            {
                bulk.ColumnMappings.Add(item);
            }
            bulk.WriteToServer(dataToCopy);
        }

        public object ExecuteScalar(string query, List<KeyValuePair<string, object>> parameters, SqlConnection connection)
        {
            object retVal = null;
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                foreach (var pair in parameters)
                {
                    command.Parameters.AddWithValue(pair.Key, pair.Value);
                }
                retVal = command.ExecuteScalar();
            }
            return retVal;
        }

        public object ExecuteScalar(string query, List<KeyValuePair<string, object>> parameters)
        {
            object retVal = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                retVal = ExecuteScalar(query, parameters, connection);
            }
            return retVal;
        }

        public object ExecuteScalar(string query, SqlConnection connection)
        {
            return ExecuteScalar(query, new SqlParametersList(), connection);
        }

        public object ExecuteScalar(string query)
        {
            return ExecuteScalar(query, new List<KeyValuePair<string, object>>());
        }

        public int ExecuteNonQuery(string query)
        {
            int retVal;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.CommandTimeout = 240;
                    retVal = command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return retVal;
        }

        public int ExecuteNonQuery(string query, List<KeyValuePair<string, object>> parameters)
        {
            int retVal;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    foreach (var pair in parameters)
                    {
                        command.Parameters.AddWithValue(pair.Key, pair.Value);
                    }
                    retVal = command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return retVal;
        }

        public Reader ExecuteReader(string query)
        {
            return ExecuteReader(query, new SqlParametersList());
        }

        public Reader ExecuteReader(string query, SqlConnection connection)
        {
            return ExecuteReader(query, new SqlParametersList(), connection, System.Data.CommandType.Text);
        }

        public Reader ExecuteReader(string query, List<KeyValuePair<string, object>> parameters, SqlConnection connection, System.Data.CommandType commandType)
        {
            Reader retVal = new Reader();
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.CommandType = commandType;
                command.CommandTimeout = 240;
                foreach (var pair in parameters)
                {
                    command.Parameters.AddWithValue(pair.Key, pair.Value);
                }
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Dictionary<string, object> row = new Dictionary<string, object>();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            row.Add(reader.GetName(i), reader[i]);
                        }
                        retVal.Add(row);
                    }
                }
            }
            return retVal;
        }

        public Reader ExecuteReader(string query, List<KeyValuePair<string, object>> parameters)
        {
            return ExecuteReader(query, parameters, System.Data.CommandType.Text);
        }

        public Reader ExecuteReader(string query, List<KeyValuePair<string, object>> parameters, System.Data.CommandType commandType)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                return ExecuteReader(query, parameters, conn, commandType);
            }
        }

        public SqlConnection GetSqlConnectionString()
        {
            return new SqlConnection(connectionString);
        }

        public class SqlParametersList : List<KeyValuePair<string, object>>
        {
            public void Add(string paramName, object paramValue)
            {
                KeyValuePair<string, object> pair = new KeyValuePair<string, object>(paramName, paramValue);
                base.Add(pair);
            }
        }
    }

}
