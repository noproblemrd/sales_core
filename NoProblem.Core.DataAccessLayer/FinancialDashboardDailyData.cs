﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class FinancialDashboardDailyData : DalBase<DataModel.Xrm.new_financialdashboarddailydata>
    {
        public FinancialDashboardDailyData(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_financialdashboarddailydata");
        }

        public override NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata Retrieve(Guid id)
        {
            return XrmDataContext.new_financialdashboarddailydatas.FirstOrDefault(x => x.new_financialdashboarddailydataid == id);
        }
    }
}
