﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class Upsale : DalBase<DataModel.Xrm.new_upsale>
    {
        private const string tableName = "new_upsale";

        #region Ctor
        public Upsale(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache(tableName);
        }
        #endregion

        public override void Create(NoProblem.Core.DataModel.Xrm.new_upsale entity)
        {
            Autonumber.AutonumberManager autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
            string next = autoNumber.GetNextNumber(tableName);
            entity.new_name = next;
            base.Create(entity);
        }

        public override NoProblem.Core.DataModel.Xrm.new_upsale Retrieve(Guid id)
        {
            DataModel.Xrm.new_upsale upsale = XrmDataContext.new_upsales.FirstOrDefault(x => x.new_upsaleid == id);
            return upsale;
        }

        public DataModel.Xrm.new_upsale GetUpsaleByIncidentId(Guid incidentId)
        {
            var incident = XrmDataContext.incidents.First(x => x.incidentid == incidentId);
            if (incident.new_upsaleid.HasValue)
            {
                var upsale = Retrieve(incident.new_upsaleid.Value);
                return upsale;
            }
            return null;
        }

        public Guid FindUpsaleGroup(Guid contactId, Guid expertiseId, Guid originId, out DateTime lastCaseInUpsale)
        {
            string query =
                @"select top 1
                up.new_upsaleid, inc.createdon
                from new_upsale up with(nolock)
                join incident inc with(nolock) on inc.new_upsaleid = up.new_upsaleid
                where up.deletionstatecode = 0 and inc.deletionstatecode = 0
                and up.new_originid = @originId
                and up.new_primaryexpertiseid = @expertiseId
                and up.new_contactid = @contactId
                order by inc.createdon desc";
            Guid retVal;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@originId", originId);
                    command.Parameters.AddWithValue("@expertiseId", expertiseId);
                    command.Parameters.AddWithValue("@contactId", contactId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            lastCaseInUpsale = (DateTime)reader["createdon"];
                            retVal = (Guid)reader["new_upsaleid"];
                        }
                        else
                        {
                            lastCaseInUpsale = new DateTime();
                            retVal = Guid.Empty;
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid FindOpenUpsale(Guid consumerId, Guid primaryExptertiseId)
        {
            string query =
                @"select top 1
                new_upsaleid
                from new_upsale with(nolock)
                where deletionstatecode = 0
                and new_primaryexpertiseid = @expertiseId
                and new_contactid = @consumerId
                and statuscode = @open
                order by createdon desc";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@expertiseId", primaryExptertiseId));
            parameters.Add(new KeyValuePair<string, object>("@consumerId", consumerId));
            parameters.Add(new KeyValuePair<string, object>("@open", (int)DataModel.Xrm.new_upsale.UpsaleStatus.Open));
            Guid retVal = Guid.Empty;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        public List<NoProblem.Core.DataModel.Upsale.UpsaleData> GetAllOpenUpsales(DateTime fromDate, DateTime toDate, Guid expertiseId, int currentPage, int pageSize,  out int totalRows, bool useLocalHours)
        {
            totalRows = 0;
            List<NoProblem.Core.DataModel.Upsale.UpsaleData> retVal = new List<NoProblem.Core.DataModel.Upsale.UpsaleData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    string query =
                @"WITH PagerTbl AS
                (                
                SELECT ROW_NUMBER() OVER(ORDER BY up.new_timetocall) AS RowNum,
                up.new_upsaleid, up.new_name, up.new_primaryexpertiseidname,
                co.mobilephone, up.new_contactid, up.new_createdonlocal, up.statuscode,
                up.new_timetocall
                ,up.new_Numofrequestedadvertisers
                ,up.new_Numofconnectedadvertisers
                ,up.new_upsalelosereasonid
                ,up.new_upsalependingreasonid
                ,pe.new_code
                from new_upsale up with(nolock)
                join new_primaryexpertise pe with(nolock) on up.new_primaryexpertiseid = pe.new_primaryexpertiseid
                join contact co with(nolock) on co.contactid = up.new_contactid
                where up.deletionstatecode = 0 and up.new_createdonlocal is not null and up.new_contactid is not null and up.statuscode = 1 ";
                    if (useLocalHours)
                    {
                        query += @" and up.new_createdonlocal between @from and @to ";
                    }
                    else
                    {
                        query += @" and up.createdon between @from and @to ";
                    }
                    if (expertiseId != Guid.Empty)
                    {
                        query += " and up.new_primaryexpertiseid = @expertiseId";
                        command.Parameters.AddWithValue("@expertiseId", expertiseId);
                    }
                    query += @"
                )
                    SELECT * , (select max(RowNum) from PagerTbl) as totalRows
                      FROM PagerTbl
                     WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
                                      AND @PageNum * @PageSize)
                            or @PageSize = -1)
                     ORDER BY RowNum";
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = query;
                    command.Parameters.AddWithValue("@from", fromDate);
                    command.Parameters.AddWithValue("@to", toDate);
                    command.Parameters.AddWithValue("@PageNum", currentPage);
                    command.Parameters.AddWithValue("@PageSize", pageSize);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            NoProblem.Core.DataModel.Upsale.UpsaleData data = new NoProblem.Core.DataModel.Upsale.UpsaleData();
                            data.ConsumerId = (Guid)reader["new_contactid"];
                            data.ConsumerPhone = (string)reader["mobilephone"];
                            data.ExpertiseName = reader["new_primaryexpertiseidname"] != DBNull.Value ? (string)reader["new_primaryexpertiseidname"] : string.Empty;
                            data.NumOfConnectedAdvertisers = reader["new_Numofconnectedadvertisers"] != DBNull.Value ? (int)reader["new_Numofconnectedadvertisers"] : 0;
                            data.NumOfRequestedAdvertisers = reader["new_Numofrequestedadvertisers"] != DBNull.Value ? (int)reader["new_Numofrequestedadvertisers"] : 0;
                            data.UpsaleStatus = (NoProblem.Core.DataModel.Xrm.new_upsale.UpsaleStatus)((int)reader["statuscode"]);
                            if (data.UpsaleStatus == NoProblem.Core.DataModel.Xrm.new_upsale.UpsaleStatus.Lost)
                            {
                                data.StatusReasonId = reader["new_upsalelosereasonid"] != DBNull.Value ? (Guid)reader["new_upsalelosereasonid"] : Guid.Empty;
                            }
                            else if (data.UpsaleStatus == NoProblem.Core.DataModel.Xrm.new_upsale.UpsaleStatus.Open)
                            {
                                data.StatusReasonId = reader["new_upsalependingreasonid"] != DBNull.Value ? (Guid)reader["new_upsalependingreasonid"] : Guid.Empty;
                            }
                            data.UpsaleId = (Guid)reader["new_upsaleid"];
                            data.UpsaleNumber = reader["new_name"] != DBNull.Value ? (string)reader["new_name"] : string.Empty;
                            data.CreatedOn = (DateTime)reader["new_createdonlocal"];
                            data.TimeToCall = reader["new_timetocall"] != DBNull.Value ? (DateTime)reader["new_timetocall"] : data.CreatedOn;
                            data.ExpertiseCode = (string)reader["new_code"];
                            if (totalRows == 0)
                            {
                                totalRows = unchecked((int)((long)reader["totalRows"]));
                            }
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public NoProblem.Core.DataModel.Upsale.UpsaleDataExpanded GetUpsaleData(Guid upsaleId)
        {
            NoProblem.Core.DataModel.Upsale.UpsaleDataExpanded retVal = new NoProblem.Core.DataModel.Upsale.UpsaleDataExpanded();
            string query =
                @"select up.new_name, up.new_primaryexpertiseidname,
                co.mobilephone, up.new_contactid, up.createdon, up.statuscode as upsaleStatus,
                up.new_timetocall
                ,up.new_Numofrequestedadvertisers
                ,up.new_Numofconnectedadvertisers
                ,up.new_upsalelosereasonid
                ,up.new_upsalependingreasonid
                ,ia.new_accountid, ia.new_accountidname
                ,inc.incidentid, inc.ticketnumber
                ,ia.statuscode as callStatus
                ,ia.createdon as callTime
                ,pe.new_code
                ,inc.new_regionidname
                ,inc.CaseTypeCode
                from new_upsale up with(nolock)
                join contact co with(nolock) on co.contactid = up.new_contactid
                join new_primaryexpertise pe with(nolock) on up.new_primaryexpertiseid = pe.new_primaryexpertiseid
                left join incident inc with(nolock) on inc.new_upsaleid = up.new_upsaleid
                left join new_incidentaccount ia with(nolock) on ia.new_incidentid = inc.incidentid and ia.deletionstatecode = 0
                where up.deletionstatecode = 0
                and up.new_upsaleid = @id
                order by ia.createdon";
            retVal.ConnectedSuppliers = new List<NoProblem.Core.DataModel.Upsale.ConnectedSupplierData>();
            retVal.Regions = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", upsaleId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            retVal.ConsumerId = (Guid)reader["new_contactid"];
                            retVal.ConsumerPhone = (string)reader["mobilephone"];
                            retVal.CreatedOn = (DateTime)reader["createdon"];
                            retVal.ExpertiseName = (string)reader["new_primaryexpertiseidname"];
                            retVal.NumOfConnectedAdvertisers = reader["new_Numofconnectedadvertisers"] != DBNull.Value ? (int)reader["new_Numofconnectedadvertisers"] : 0;
                            retVal.NumOfRequestedAdvertisers = reader["new_Numofrequestedadvertisers"] != DBNull.Value ? (int)reader["new_Numofrequestedadvertisers"] : 0;
                            retVal.UpsaleStatus = (DataModel.Xrm.new_upsale.UpsaleStatus)((int)reader["upsaleStatus"]);
                            if (retVal.UpsaleStatus == NoProblem.Core.DataModel.Xrm.new_upsale.UpsaleStatus.Open)
                            {
                                retVal.StatusReasonId = reader["new_upsalependingreasonid"] != DBNull.Value ? (Guid)reader["new_upsalependingreasonid"] : Guid.Empty;
                            }
                            else if (retVal.UpsaleStatus == NoProblem.Core.DataModel.Xrm.new_upsale.UpsaleStatus.Lost)
                            {
                                retVal.StatusReasonId = reader["new_upsalelosereasonid"] != DBNull.Value ? (Guid)reader["new_upsalelosereasonid"] : Guid.Empty;
                            }
                            retVal.TimeToCall = reader["new_timetocall"] != DBNull.Value ? (DateTime)reader["new_timetocall"] : DateTime.MinValue;
                            retVal.UpsaleId = upsaleId;
                            retVal.UpsaleNumber = (string)reader["new_name"];
                            retVal.ExpertiseCode = (string)reader["new_code"];
                            AddCallData(retVal, reader);
                        }
                        while (reader.Read())
                        {
                            AddCallData(retVal, reader);
                        }
                    }
                }
            }
            return retVal;
        }

        private void AddCallData(NoProblem.Core.DataModel.Upsale.UpsaleDataExpanded retVal, SqlDataReader reader)
        {
            Guid accountId = reader["new_accountid"] != DBNull.Value ? (Guid)reader["new_accountid"] : Guid.Empty;
            string regionName = reader["new_regionidname"] != DBNull.Value ? (string)reader["new_regionidname"] : string.Empty;
            AddRegionNameToList(retVal.Regions, regionName);
            if (accountId != Guid.Empty)
            {
                DataModel.Xrm.new_incidentaccount.Status iaStatus = (DataModel.Xrm.new_incidentaccount.Status)((int)reader["callStatus"]);
                if (iaStatus == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.INVITED_BY_SMS
                    || iaStatus == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSE
                    || iaStatus == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS)
                {
                    DataModel.Upsale.ConnectedSupplierData data = new NoProblem.Core.DataModel.Upsale.ConnectedSupplierData();
                    if (iaStatus == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS
                        || iaStatus == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSE)
                    {
                        data.CallStatus = NoProblem.Core.DataModel.Case.EndStatus.Won;
                    }
                    else
                    {
                        data.CallStatus = NoProblem.Core.DataModel.Case.EndStatus.Invited;
                    }
                    data.CaseNumber = (string)reader["ticketnumber"];
                    data.CaseId = (Guid)reader["incidentid"];
                    data.SupplierId = (Guid)reader["new_accountid"];
                    data.SupplierName = (string)reader["new_accountidname"];
                    data.CallTime = (DateTime)reader["callTime"];
                    data.RegionName = regionName;
                    data.CaseType = (DataModel.Xrm.incident.CaseTypeCode)reader["CaseTypeCode"];
                    retVal.ConnectedSuppliers.Add(data);
                }
            }
        }

        private void AddRegionNameToList(List<string> regionsList, string newRegion)
        {
            if (string.IsNullOrEmpty(newRegion) == false)
            {
                if (regionsList.Contains(newRegion) == false)
                {
                    regionsList.Add(newRegion);
                }
            }
        }

        public Guid GetLastCaseOriginIdAndIsUseSameInUpsale(Guid upsaleId, out bool isUseSameInUpsale, ref Guid incidentId)
        {
            isUseSameInUpsale = false;
            string query = @"
            select top 1 inc.new_originid, org.new_usesameinupsale, inc.incidentid
            from incident inc with(nolock)
            join new_upsale ups with(nolock) on ups.new_upsaleid = inc.new_upsaleid
            join new_origin org with(nolock) on org.new_originid = inc.new_originid
            where ups.new_upsaleid = @upsaleId
            order by inc.createdon desc
            ";
            Guid retVal = Guid.Empty;
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_upsale>.SqlParametersList();
            parameters.Add("@upsaleId", upsaleId);
            var reader = ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                retVal = (Guid)row["new_originid"];
                isUseSameInUpsale = row["new_usesameinupsale"] != DBNull.Value ? (bool)row["new_usesameinupsale"] : false;
                incidentId = (Guid)row["incidentid"];
                break;
            }
            return retVal;
        }
    }
}
