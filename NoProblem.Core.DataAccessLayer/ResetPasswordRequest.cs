﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class ResetPasswordRequest : DalBase<DataModel.Xrm.new_resetpasswordrequest>
    {
        public ResetPasswordRequest(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_resetpasswordrequest");
        }

        public override IQueryable<DataModel.Xrm.new_resetpasswordrequest> All
        {
            get
            {
                return XrmDataContext.new_resetpasswordrequests;
            }
        }

        public override DataModel.Xrm.new_resetpasswordrequest Retrieve(Guid id)
        {
            return XrmDataContext.new_resetpasswordrequests.FirstOrDefault(x => x.new_resetpasswordrequestid == id);
        }
    }
}
