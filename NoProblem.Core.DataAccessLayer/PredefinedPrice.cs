﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class PredefinedPrice : DalBase<DataModel.Xrm.new_predefinedprice>
    {
        public PredefinedPrice(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_predefinedprice");
        }

        public override NoProblem.Core.DataModel.Xrm.new_predefinedprice Retrieve(Guid id)
        {
            return XrmDataContext.new_predefinedprices.FirstOrDefault(x => x.new_predefinedpriceid == id);
        }

        public Dictionary<string, decimal> CalculatePreDefinedPrices()
        {
            Dictionary<string, decimal> dic = new Dictionary<string, decimal>();
            string query =
                @"select avg(aex.new_incidentprice),pe.new_code
                    from
                    new_accountexpertise aex with(nolock)
                    join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = aex.new_primaryexpertiseid
                    join account acc with(nolock) on acc.accountid = aex.new_accountid
                    where
                    pe.deletionstatecode = 0 and aex.deletionstatecode = 0 and acc.deletionstatecode = 0
                    and aex.statecode = 0
                    and aex.new_incidentprice is not null
                    and acc.new_status = 1
                    and (acc.New_securitylevel is null or acc.New_securitylevel = 0)
                    group by pe.new_code";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dic.Add((string)reader[1], (decimal)reader[0]);
                        }
                    }
                }
            }
            return dic;
        }

        public int UpdatePreDefined(string expertiseCode, decimal avgPredefinedPrice, string dateString)
        {
            string query =
                @"update new_predefinedprice 
                    set new_avgpredefinedprice = @value
                    where 
                    deletionstatecode = 0
                    and new_expertisecode = @expertiseCode
                    and new_date = @date";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@value", avgPredefinedPrice));
            parameters.Add(new KeyValuePair<string, object>("@expertiseCode", expertiseCode));
            parameters.Add(new KeyValuePair<string, object>("@date", dateString));
            int rowsAffected = ExecuteNonQuery(query, parameters);
            return rowsAffected;
        }

        public bool IsExistsInDB(string dateString)
        {
            bool retVal = true;
            string query = @"select new_date from new_predefinedprice with(nolock) where new_date = @dateString and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@dateString", dateString));
            object selectRetValue = ExecuteScalar(query, parameters);
            if (selectRetValue == null || selectRetValue == DBNull.Value)
            {
                retVal = false;
            }
            return retVal;
        }

        public void GetPreDefinedPrice(List<NoProblem.Core.DataModel.Reports.PreDefinedPriceData> dataList, string code)
        {
            string query =
                @"select new_avgpredefinedprice
                from new_predefinedprice with(nolock)
                where
                new_expertiseCode = @expCode 
                and  convert(varchar,createdon,111) =  convert(varchar,@date,111)
                and deletionstatecode = 0";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                foreach (var item in dataList)
                {
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@expCode", code);
                        command.Parameters.AddWithValue("date", item.Date);
                        object scalar = command.ExecuteScalar();
                        if (scalar != null && scalar != DBNull.Value)
                        {
                            decimal preDef = new decimal((double)scalar);
                            item.PreDefinedPrice = preDef;
                        }
                        else
                        {
                            item.PreDefinedPrice = 0;
                        }
                    }
                }
            }
        }
    }
}
