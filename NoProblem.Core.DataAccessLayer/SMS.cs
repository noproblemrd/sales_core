﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class SMS : DalBase<DML.Xrm.new_sms>
    {
        #region Ctor
        public SMS(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_sms");
        } 
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_sms Retrieve(Guid id)
        {
            DML.Xrm.new_sms sms = XrmDataContext.new_smses.FirstOrDefault(x => x.new_smsid == id);
            return sms;
        }
    }
}
