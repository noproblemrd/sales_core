﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class CaseWaitForStep : DalBase<DataModel.Xrm.new_casewaitforstep>
    {
        public CaseWaitForStep(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_casewaitforstep");
        }

        public override NoProblem.Core.DataModel.Xrm.new_casewaitforstep Retrieve(Guid id)
        {
            return XrmDataContext.new_casewaitforsteps.FirstOrDefault(x => x.new_casewaitforstepid == id);
        }

        public bool FindAndMarkCaseWait(Guid headingId, Guid regionId, Guid contactId)
        {
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_casewaitforstep>.SqlParametersList();
            parameters.Add("@contactId", contactId);
            parameters.Add("@headingId", headingId);
            parameters.Add("@regionId", regionId);
            int rowsAffected = ExecuteNonQuery(queryForFindAndMarkCaseWait, parameters);
            return rowsAffected > 0;
        }

        private const string queryForFindAndMarkCaseWait =
            @"update new_casewaitforstepextensionbase
set new_done = 1
, new_senton = getdate()
, new_senttype = 2
where isnull(new_done, 0) = 0
and new_contactid = @contactId
and new_primaryexpertiseid = @headingId
and new_regionid = @regionId";
    }
}
