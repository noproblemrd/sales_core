﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.PrimaryExpertise
//  File: PrimaryExpertise.cs
//  Description: DAL for PrimaryExpertise Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using DML = NoProblem.Core.DataModel;
using System.Collections.Generic;
using System.Data.SqlClient;
using Effect.Crm.Convertor;
using System.Data;

namespace NoProblem.Core.DataAccessLayer
{
    public class PrimaryExpertise : DalBase<DML.Xrm.new_primaryexpertise>
    {
        #region Ctor
        public PrimaryExpertise(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_primaryexpertise");
        }
        #endregion

        #region Fields

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        #endregion

        public override IQueryable<DML.Xrm.new_primaryexpertise> All
        {
            get
            {
                return XrmDataContext.new_primaryexpertises;
            }
        }

        public override NoProblem.Core.DataModel.Xrm.new_primaryexpertise Retrieve(Guid id)
        {
            DML.Xrm.new_primaryexpertise primary = XrmDataContext.new_primaryexpertises.FirstOrDefault(x => x.new_primaryexpertiseid == id);
            return primary;
        }

        public DML.Xrm.new_primaryexpertise GetPrimaryExpertiseByCode(string code)
        {
            DML.Xrm.new_primaryexpertise primaryExpertise = XrmDataContext.new_primaryexpertises.FirstOrDefault(x => x.new_code == code);
            return primaryExpertise;
        }

        public NoProblem.Core.DataModel.Xrm.new_primaryexpertise GetPrimaryExpertiseByName(string name)
        {
            DML.Xrm.new_primaryexpertise primaryExpertise = XrmDataContext.new_primaryexpertises.FirstOrDefault(x => x.new_name == name);
            return primaryExpertise;
        }

        public string GetPrimaryExpertiseCodeByEnglishName(string englishName)
        {
            return CacheManager.Instance.Get<string>(cachePrefix + ".GetPrimaryExpertiseCodeByEnglishName", englishName, GetPrimaryExpertiseCodeByEnglishNameMethod, int.MaxValue);
        }

        private string GetPrimaryExpertiseCodeByEnglishNameMethod(string englishName)
        {
            string query = "select top 1 new_code from new_primaryexpertiseextensionbase with(nolock) where new_englishname = @englishName";
            string retVal = string.Empty;
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_primaryexpertise>.SqlParametersList();
            parameters.Add("@englishName", englishName);
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        public NoProblem.Core.DataModel.Xrm.new_primaryexpertise GetPrimaryExpertiseByEnglishName(string englishName)
        {
            DML.Xrm.new_primaryexpertise primaryExpertise = XrmDataContext.new_primaryexpertises.FirstOrDefault(x => x.new_englishname == englishName);
            return primaryExpertise;
        }

        public List<DML.Expertise.ExpertiseWithIsActive> GetAllActive()
        {
            return CacheManager.Instance.Get<List<DML.Expertise.ExpertiseWithIsActive>>(cachePrefix, "GetAllActive", GetAllActiveCached, CacheManager.ONE_DAY);
        }

        public List<DML.Expertise.ExpertiseWithIsActive> GetAllActiveCached()
        {
            string query = @"select new_code,new_name,new_primaryexpertiseid
                    from
                    new_primaryexpertise with(nolock)
                    where
                    statecode = 0
                    and
                    deletionstatecode = 0
                    order by new_name";
            List<DML.Expertise.ExpertiseWithIsActive> dataList = new List<NoProblem.Core.DataModel.Expertise.ExpertiseWithIsActive>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.Expertise.ExpertiseWithIsActive data = new NoProblem.Core.DataModel.Expertise.ExpertiseWithIsActive();
                            data.Code = ConvertorUtils.ToString(reader["new_code"], string.Empty);
                            data.Name = ConvertorUtils.ToString(reader["new_name"], string.Empty);
                            data.Id = (Guid)reader["new_primaryexpertiseid"];
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_primaryexpertise> GetPrimaryExpertiseByPartOfName(string expertiseName)
        {
            IEnumerable<NoProblem.Core.DataModel.Xrm.new_primaryexpertise> retVal =
                from item in XrmDataContext.new_primaryexpertises
                where
                item.new_name.Contains(expertiseName)
                select
                item;
            return retVal;
        }

        public List<DML.Reports.ExpertiseRow> GetAllExpertiseReport()
        {
            #region query
            string query = @"
                    
    CREATE TABLE #TEST
    (
        RowId int Not Null IDENTITY(1,1) PRIMARY KEY,
        name nvarchar(100) Not Null,
        Inactive int,
        Candidate int,
        Available int,
        NotAvailable int,
        total int,
        IsPrimary bit,
        Id uniqueidentifier
    )

    DECLARE @primaryexpertiseId uniqueidentifier
    DECLARE @primaryexpertiseName nvarchar(100)
    

    DECLARE PrimExpr CURSOR FOR
    SELECT New_primaryexpertiseId, New_name 
    FROM dbo.New_primaryexpertise with (nolock)

    OPEN PrimExpr	
    FETCH PrimExpr INTO @primaryexpertiseId,
                    @primaryexpertiseName
                    



    WHILE @@Fetch_Status = 0
    BEGIN


        DECLARE @Inactive int
        SELECT @Inactive = COUNT(*)
        FROM dbo.New_accountexpertise ae with (nolock),
            dbo.Account a with (nolock)
        WHERE ae.New_accountId = a.AccountId AND
                ae.New_primaryexpertiseId = @primaryexpertiseId AND
                a.New_status = 3

        
        DECLARE @Candidate int
        SELECT @Candidate = COUNT(*)
        FROM dbo.New_accountexpertise ae with (nolock),
            dbo.Account a with (nolock)
        WHERE ae.New_accountId = a.AccountId AND
                ae.New_primaryexpertiseId = @primaryexpertiseId AND
                a.New_status = 2


        DECLARE @AllApproved int
        SELECT @AllApproved = COUNT(*)
        FROM dbo.New_accountexpertise ae with (nolock),
            dbo.Account a with (nolock)
        WHERE ae.New_accountId = a.AccountId AND
                ae.New_primaryexpertiseId = @primaryexpertiseId AND
                a.New_status = 1 AND
                ae.statecode = 0

        DECLARE @Approved int
        SELECT @Approved = COUNT(*)
        FROM dbo.New_accountexpertise ae with (nolock),
            dbo.Account a with (nolock)
        WHERE ae.New_accountId = a.AccountId AND
                ae.New_primaryexpertiseId = @primaryexpertiseId AND
                a.New_status = 1 AND
                a.New_cashbalance >= ae.New_IncidentPrice AND
                (a.new_unavailabilityreasonid IS NULL OR
                ISNULL(a.new_unavailableto, DATEADD(day, 2, GETDATE())) < GETDATE())
                AND ae.statecode = 0

        DECLARE @NoApproved int
        SET @NoApproved = @AllApproved - @Approved
    
        DECLARE @Total int
        SET @Total = @AllApproved +@Candidate +@Inactive


        INSERT INTO #TEST
            (name, Inactive, Candidate,
            Available, NotAvailable, total, IsPrimary ,Id)
        VALUES (@primaryexpertiseName, @Inactive,
                @Candidate, @Approved, @NoApproved,	 
                @Total, 1, @primaryexpertiseId)
        
        DECLARE @SecondaryexpertiseId uniqueidentifier
        DECLARE @SecondaryexpertiseName nvarchar(100)

        DECLARE SecondExpr CURSOR FOR
        SELECT  New_secondaryexpertiseId, New_name
        FROM dbo.New_secondaryexpertise with (nolock)
        WHERE New_primaryexpertiseId = @primaryexpertiseId

        OPEN SecondExpr	
        FETCH SecondExpr INTO @SecondaryexpertiseId,
                        @SecondaryexpertiseName

        WHILE @@Fetch_Status = 0
        BEGIN
            DECLARE @Inactive2 int
            SELECT @Inactive2 = COUNT(*)
            FROM dbo.New_accountexpertise ae with (nolock),
                dbo.Account a with (nolock),
                dbo.new_secondaryexpertise_new_accountexpertiseBase a_ae with (nolock)
            WHERE ae.New_accountId = a.AccountId AND
                    ae.New_accountexpertiseId = a_ae.new_accountexpertiseid AND
                    a_ae.new_secondaryexpertiseid = @SecondaryexpertiseId AND
                    a.New_status = 3
                    AND ae.statecode = 0

            
            DECLARE @Candidate2 int
            SELECT @Candidate2 = COUNT(*)
            FROM dbo.New_accountexpertise ae with (nolock),
                dbo.Account a with (nolock),
                dbo.new_secondaryexpertise_new_accountexpertiseBase a_ae with (nolock)
            WHERE ae.New_accountId = a.AccountId AND
                    ae.New_accountexpertiseId = a_ae.new_accountexpertiseid AND
                    a_ae.new_secondaryexpertiseid = @SecondaryexpertiseId AND
                    a.New_status = 2 AND ae.statecode = 0


            DECLARE @AllApproved2 int
            SELECT @AllApproved2 = COUNT(*)
            FROM dbo.New_accountexpertise ae with (nolock),
                dbo.Account a with (nolock),
                dbo.new_secondaryexpertise_new_accountexpertiseBase a_ae with (nolock)
            WHERE ae.New_accountId = a.AccountId AND
                    ae.New_accountexpertiseId = a_ae.new_accountexpertiseid AND
                    a_ae.new_secondaryexpertiseid = @SecondaryexpertiseId AND
                    a.New_status = 1 AND ae.statecode = 0

            DECLARE @Approved2 int
            SELECT @Approved2 = COUNT(*)
            FROM dbo.New_accountexpertise ae with (nolock),
                dbo.Account a with (nolock),
                dbo.new_secondaryexpertise_new_accountexpertiseBase a_ae with (nolock)
            WHERE ae.New_accountId = a.AccountId AND
                    ae.New_accountexpertiseId = a_ae.new_accountexpertiseid AND
                    a_ae.new_secondaryexpertiseid = @SecondaryexpertiseId AND
                    a.New_status = 1 AND
                    a.New_cashbalance >= ae.New_IncidentPrice AND
                    (a.new_unavailabilityreasonid IS NULL OR
                    ISNULL(a.new_unavailableto, DATEADD(day, 2, GETDATE())) < GETDATE())
                    AND ae.statecode = 0

            DECLARE @NoApproved2 int
            SET @NoApproved2 = @AllApproved2 - @Approved2
        
            DECLARE @Total2 int
            SET @Total2 = @AllApproved2 +@Candidate2 +@Inactive2


            INSERT INTO #TEST
                (name, Inactive, Candidate,
                Available, NotAvailable, total, IsPrimary, Id)
            VALUES (@SecondaryexpertiseName, @Inactive2,
                    @Candidate2, @Approved2, @NoApproved2,	 
                    @Total2, 0, @SecondaryexpertiseId)
        

            FETCH SecondExpr INTO @SecondaryexpertiseId,
                        @SecondaryexpertiseName
        END
        CLOSE SecondExpr

        DEALLOCATE SecondExpr

        FETCH PrimExpr INTO @primaryexpertiseId,
                    @primaryexpertiseName       

    END
    CLOSE PrimExpr

    DEALLOCATE PrimExpr

    select * from #TEST

    DROP TABLE #TEST
            ";
            #endregion
            List<DML.Reports.ExpertiseRow> retVal = new List<NoProblem.Core.DataModel.Reports.ExpertiseRow>();
            using (SqlCommand command = new SqlCommand(query))
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    command.Connection = connection;
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.Reports.ExpertiseRow dataRow = new NoProblem.Core.DataModel.Reports.ExpertiseRow();
                            dataRow.Available = (int)reader["Available"];
                            dataRow.Candidate = (int)reader["Candidate"];
                            dataRow.Id = (Guid)reader["Id"];
                            dataRow.Inactive = (int)reader["Inactive"];
                            dataRow.IsPrimary = (bool)reader["IsPrimary"];
                            dataRow.Name = (string)reader["name"];
                            dataRow.NotAvailable = (int)reader["NotAvailable"];
                            dataRow.Total = (int)reader["total"];
                            retVal.Add(dataRow);
                        }
                    }
                }
            }
            return retVal;
        }

        public NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode GetChannelCodeFromPrimaryExpertise(string HeadingCode)
        {
            string query = string.Format(
                @"select ch.new_code from new_channel ch with (nolock) join new_primaryexpertise pe with (nolock) on ch.new_channelid = pe.new_channelid
                    where pe.new_code = N'{0}' and pe.deletionstatecode = 0 and ch.deletionstatecode = 0", HeadingCode);
            object code;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    code = command.ExecuteScalar();
                }
            }
            DataModel.Xrm.new_channel.ChannelCode retVal = NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Call;
            if (code != null)
            {
                retVal = (DataModel.Xrm.new_channel.ChannelCode)code;
            }
            return retVal;
        }

        private string GetPrimaryExpertiseIdByCodeMethod(string code)
        {
            Guid retVal = Guid.Empty;
            string query = @"select new_primaryexpertiseid from new_primaryexpertise with (nolock) where new_code = @code and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@code", code));
            object guidObj = ExecuteScalar(query, parameters);
            if (guidObj != null && guidObj != DBNull.Value)
            {
                retVal = (Guid)guidObj;
            }
            return retVal.ToString();
        }

        public Guid GetPrimaryExpertiseIdByCode(string code)
        {
            string id = DataAccessLayer.CacheManager.Instance.Get<string>(cachePrefix + ".GetPrimaryExpertiseIdBycode", code, GetPrimaryExpertiseIdByCodeMethod, CacheManager.ONE_WEEK);
            return new Guid(id);
        }

        public Guid GetPrimaryExpertiseIdByEnglishName(string englishName)
        {
            Guid retVal = Guid.Empty;
            string query = @"select new_primaryexpertiseid from new_primaryexpertise with (nolock) where new_englishname = @englishname and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@englishname", englishName));
            object guidObj = ExecuteScalar(query, parameters);
            if (guidObj != null && guidObj != DBNull.Value)
            {
                retVal = (Guid)guidObj;
            }
            return retVal;
        }

        public string GetPrimaryExpertiseNameByCode(string code)
        {
            string retVal = string.Empty;
            string query = @"select new_name from new_primaryexpertise with (nolock) where new_code = @code and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@code", code));
            object nameObj = ExecuteScalar(query, parameters);
            if (nameObj != null && nameObj != DBNull.Value)
            {
                retVal = (string)nameObj;
            }
            return retVal;
        }

        public string GetPrimaryExpertiseNameById(Guid id)
        {
            return CacheManager.Instance.Get<string>(cachePrefix + ".GetPrimaryExpertiseNameById", id.ToString(), GetPrimaryExpertiseNameByIdCached, CacheManager.ONE_YEAR);
        }

        public string GetPrimaryExpertiseNameByIdCached(string id)
        {
            string retVal = string.Empty;
            string query = @"select new_name from new_primaryexpertise with (nolock) where new_primaryexpertiseid = @id and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", new Guid(id)));
            object nameObj = ExecuteScalar(query, parameters);
            if (nameObj != null && nameObj != DBNull.Value)
            {
                retVal = (string)nameObj;
            }
            return retVal;
        }

        public IEnumerable<Guid> GetSecondariesIds(Guid primaryId)
        {
            string query = @"
                select new_secondaryexpertiseid from new_secondaryexpertise with (nolock) where new_primaryexpertiseid = '" + primaryId.ToString() + "' and deletionstatecode = 0";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Guid id = (Guid)reader[0];
                            retVal.Add(id);
                        }
                    }
                }
            }
            return retVal;
        }

        public int GetNotificationDelay(Guid expertiseId)
        {
            string query = "select new_notificationdelay from new_primaryexpertise with (nolock) where new_primaryexpertiseid = '" + expertiseId.ToString() + "' and deletionstatecode = 0";
            object obj = ExecuteScalar(query);
            int retVal = 0;
            if (obj != null && obj != DBNull.Value)
            {
                retVal = (int)obj;
            }
            return retVal;
        }

        public decimal GetMinimumPrice(Guid expertiseId)
        {
            string query = "select new_minprice from new_primaryexpertise with(nolock) where new_primaryexpertiseid = @id";
            List<KeyValuePair<string, object>> paramters = new List<KeyValuePair<string, object>>();
            paramters.Add(new KeyValuePair<string, object>("@id", expertiseId));
            object obj = ExecuteScalar(query, paramters);
            decimal retVal = 1;
            if (obj != null && obj != DBNull.Value)
            {
                retVal = (decimal)obj;
            }
            return retVal;
        }

        public decimal GetDefaultPrice(Guid expertiseId)
        {
            string query = "select new_defaultprice from new_primaryexpertise with(nolock) where new_primaryexpertiseid = @id";
            List<KeyValuePair<string, object>> paramters = new List<KeyValuePair<string, object>>();
            paramters.Add(new KeyValuePair<string, object>("@id", expertiseId));
            object obj = ExecuteScalar(query, paramters);
            decimal retVal = 1;
            if (obj != null && obj != DBNull.Value)
            {
                retVal = (decimal)obj;
            }
            return retVal;
        }

        public string GetCode(Guid id)
        {
            return CacheManager.Instance.Get<string>(cachePrefix + ".GetCode", id.ToString(), GetCodeCachable, CacheManager.ONE_WEEK);
        }

        private string GetCodeCachable(string id)
        {
            string query = "select new_code from new_primaryexpertise with(nolock) where new_primaryexpertiseid = @id and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameter = new List<KeyValuePair<string, object>>();
            parameter.Add(new KeyValuePair<string, object>("@id", new Guid(id)));
            return (string)ExecuteScalar(query, parameter);
        }

        public bool IsAuction(string headingCode)
        {
            string query = "select new_isauction from new_primaryexpertise with(nolock) where new_code = @code and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameter = new List<KeyValuePair<string, object>>();
            parameter.Add(new KeyValuePair<string, object>("@code", headingCode));
            bool retVal = true;
            object scalar = ExecuteScalar(query, parameter);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (bool)scalar;
            }
            return retVal;
        }

        public bool IsExists(string code)
        {
            string query = @"select top 1 1
                from new_primaryexpertise with (nolock) 
                where new_code = @code
                and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@code", code));
            object scalar = ExecuteScalar(query, parameters);
            return scalar != null;
        }

        public int GetDailyCapacityCount(Guid expertiseId)
        {
            string query =
                @"select new_dailycapacitycount from new_primaryexpertise with(nolock)
                where deletionstatecode = 0 and new_primaryexpertiseid = @id";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", expertiseId));
            int retVal = -1;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (int)scalar;
            }
            return retVal;
        }

        /// <summary>
        /// Returns headings with suppliers (Trial, Approved).
        /// </summary>
        /// <returns></returns>
        public List<NoProblem.Core.DataModel.Expertise.ExpertiseWithIsActive> GetHeadingsWithSuppliers()
        {
            string query =
                @"select distinct pe.new_name, pe.new_primaryexpertiseid, pe.new_code
from
new_accountexpertise ae
join account acc on ae.new_accountid = acc.accountid
join new_primaryexpertise pe on ae.new_primaryexpertiseid = pe.new_primaryexpertiseid
where 
ae.deletionstatecode = 0
and ae.statecode = 0
and acc.deletionstatecode = 0
and pe.deletionstatecode = 0
and (acc.new_status = 1 or acc.new_status = 4)";
            List<DataModel.Expertise.ExpertiseWithIsActive> retVal = new List<NoProblem.Core.DataModel.Expertise.ExpertiseWithIsActive>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Expertise.ExpertiseWithIsActive data = new NoProblem.Core.DataModel.Expertise.ExpertiseWithIsActive();
                            data.Code = (string)reader["new_code"];
                            data.Name = (string)reader["new_name"];
                            data.Id = (Guid)reader["new_primaryexpertiseid"];
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public string GetIvrWidgetName(Guid headingId)
        {
            string query =
                @"select new_ivrwidgetname from new_primaryexpertise with(nolock)
                where deletionstatecode = 0 and new_primaryexpertiseID = @id";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", headingId));
            string retVal = string.Empty;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        public string GetIvrPluralNameFromIncidentId(Guid incidentId)
        {
            string query =
                @"select top 1 new_ivrpluralname from new_primaryexpertise with(nolock)
where deletionstatecode = 0
and new_primaryexpertiseid = 
(select new_primaryexpertiseid from incident with(nolock) where deletionstatecode = 0 and incidentid = @incidentId)";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@incidentId", incidentId));
            var scalar = ExecuteScalar(query, parameters);
            string retVal = null;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        public string GetIvrName(string incidentAccountId)
        {
            string query =
                @"select top 1 new_ivrname from  new_primaryexpertise with(nolock)
                    where deletionstatecode = 0
                    and new_primaryexpertiseid = 
                    (select top 1 new_primaryexpertiseid from incident with(nolock) where deletionstatecode = 0 and incidentid = 
                        (select top 1 new_incidentid from new_incidentaccount with(nolock) where deletionstatecode = 0 and new_incidentaccountid = @id))";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", incidentAccountId));
            var scalar = ExecuteScalar(query, parameters);
            string retVal = null;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        public List<DataModel.SliderData.ExpertiseData> GetAllExpertiseSliderData()
        {
            return CacheManager.Instance.Get<List<DataModel.SliderData.ExpertiseData>>(cachePrefix, "GetAllExpertiseSliderData", GetAllExpertiseSliderDataMethod, 240);
        }

        private List<DataModel.SliderData.ExpertiseData> GetAllExpertiseSliderDataMethod()
        {
            var retVal = new List<NoProblem.Core.DataModel.SliderData.ExpertiseData>();

            string query =
                @"
                select new_primaryexpertiseid, new_code, new_name, new_ivrpluralname, new_singularname, new_watermark, 
                    new_imageback, new_speaktobuttonname, new_slogan, New_MobileDeviceTitle, 
                    New_MobileDeviceDescription, New_InluceGoogleAddon, new_intext
                from new_primaryexpertise with(nolock)
                where deletionstatecode = 0";

            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                DataModel.SliderData.ExpertiseData data = new NoProblem.Core.DataModel.SliderData.ExpertiseData();
                data.Code = Convert.ToString(row["new_code"]);
                data.Id = (Guid)row["new_primaryexpertiseid"];
                data.ImageBack = Convert.ToString(row["new_imageback"]);
                data.Name = Convert.ToString(row["new_name"]);
                data.Plural = Convert.ToString(row["new_ivrpluralname"]);
                data.Singular = Convert.ToString(row["new_singularname"]);
                data.Watermark = Convert.ToString(row["new_watermark"]);
                data.SpeakToButtonName = Convert.ToString(row["new_speaktobuttonname"]);
                data.Slogan = Convert.ToString(row["new_slogan"]);
                data.MobileDeviceTitel = (row["New_MobileDeviceTitle"] == DBNull.Value) ? string.Empty : (string)row["New_MobileDeviceTitle"];
                data.MobileDeviceDescription = (row["New_MobileDeviceDescription"] == DBNull.Value) ? string.Empty : (string)row["New_MobileDeviceDescription"];
                data.InluceGoogleAddon = (row["New_InluceGoogleAddon"] == DBNull.Value) ? false : (bool)row["New_InluceGoogleAddon"];
                data.RelatedExpertises = GetRelatedExpertisesFromCache(data.Id);
                data.Intext = row["new_intext"] != DBNull.Value ? (bool)row["new_intext"] : false;
                retVal.Add(data);
            }

            return retVal;
        }

        private List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData> GetRelatedExpertisesFromCache(Guid headingId)
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData>>(cachePrefix + ".GetRelatedExpertisesFromCache", headingId.ToString(), GetRelatedExpertises, 1440);
        }

        private List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData> GetRelatedExpertises(string headingId)
        {
            List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData> retVal = new List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData>();
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@headingId", new Guid(headingId));
            var reader = ExecuteReader(queryGetRelatedExpertises, parameters);
            foreach (var row in reader)
            {
                DataModel.SliderData.RelatedExpertiseData data = new NoProblem.Core.DataModel.SliderData.RelatedExpertiseData();
                data.Code = Convert.ToString(row["New_code"]);
                data.Name = Convert.ToString(row["New_name"]);
                retVal.Add(data);
            }
            if (retVal.Count < 3)
            {
                List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData> mostPopularOverall = GetMostPopularOverall();
                if (retVal.Count < 3)
                {
                    for (int i = 0; i < mostPopularOverall.Count; i++)
                    {
                        if (!retVal.Contains(mostPopularOverall[i]))
                        {
                            retVal.Add(mostPopularOverall[i]);
                            if (retVal.Count >= 3)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return retVal;
        }

        public List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData> GetMostPopularOverall()
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData>>(cachePrefix, "GetMostPopularOverall", GetMostPopularOverallMethod, 1440);
        }

        private List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData> GetMostPopularOverallMethod()
        {
            List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData> retVal = new List<NoProblem.Core.DataModel.SliderData.RelatedExpertiseData>();
            var reader = ExecuteReader(queryGetMostPopularExpertises);
            foreach (var row in reader)
            {
                DataModel.SliderData.RelatedExpertiseData data = new NoProblem.Core.DataModel.SliderData.RelatedExpertiseData();
                data.Code = Convert.ToString(row["New_code"]);
                data.Name = Convert.ToString(row["New_name"]);
                retVal.Add(data);
            }
            return retVal;
        }

        private const string queryGetRelatedExpertises =
                @"
                select distinct top 3 SUM(cnt) sum, New_name, New_code from
                tbl_popular_headings_by_category_group
                where
                new_categorygroupid in (
                select x.new_categorygroupid
                from new_primaryexpertise_categorygroup x with(nolock)
                where
                x.new_primaryexpertiseid = @headingId
                )
                and new_primaryexpertiseid != @headingId
                group by New_name, New_code
                order by SUM(cnt) desc
                ";

        private const string queryGetMostPopularExpertises =
            @"
            select distinct top 3 sum(cnt) sum, New_name, New_code from
            tbl_popular_headings_by_category_group
            group by New_name, New_code
            order by SUM(cnt) desc
            ";

        public Guid GetGenericHeadingId()
        {
            string guidStr = CacheManager.Instance.Get<string>(cachePrefix, "GetGenericHeadingId", GetGenericHeadingIdMethod, 1440);
            Guid retVal = Guid.Empty;
            if (!string.IsNullOrEmpty(guidStr))
            {
                retVal = new Guid(guidStr);
            }
            return retVal;
        }

        private string GetGenericHeadingIdMethod()
        {
            string query = "select new_primaryexpertiseid from new_primaryexpertise where deletionstatecode = 0 and new_isgenericheading = 1";
            object scalar = ExecuteScalar(query);
            string retVal = string.Empty;
            if (scalar != null)
            {
                retVal = ((Guid)scalar).ToString();
            }
            return retVal;
        }
    }
}
