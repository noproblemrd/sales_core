﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class PaydayLoanData : DalBase<DataModel.Xrm.new_paydayloandata>
    {
        public PaydayLoanData(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_paydayloandata");
        }

        public override NoProblem.Core.DataModel.Xrm.new_paydayloandata Retrieve(Guid id)
        {
            return XrmDataContext.new_paydayloandatas.FirstOrDefault(x => x.new_paydayloandataid == id);
        }
    }
}
