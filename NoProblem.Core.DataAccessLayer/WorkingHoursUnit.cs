﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.DataAccessLayer
{
    public class WorkingHoursUnit : DalBase<DataModel.Xrm.new_workinghoursunit>
    {
        public WorkingHoursUnit(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_workinghoursunit");
        }

        public override IQueryable<new_workinghoursunit> All
        {
            get
            {
                return XrmDataContext.new_workinghoursunits;
            }
        }

        public override DataModel.Xrm.new_workinghoursunit Retrieve(Guid id)
        {
            return XrmDataContext.new_workinghoursunits.FirstOrDefault(x => x.new_workinghoursunitid == id);
        }

        public void Create(DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit workingHoursUnit, Guid supplierId)
        {
            new_workinghoursunit unit = new new_workinghoursunit();
            unit.new_accountid = supplierId;
            unit.new_allday = workingHoursUnit.AllDay;
            unit.new_fromhour = workingHoursUnit.FromHour;
            unit.new_tohour = workingHoursUnit.ToHour;
            unit.new_fromminute = workingHoursUnit.FromMinute;
            unit.new_tominute = workingHoursUnit.ToMinute;
            unit.new_order = workingHoursUnit.Order;
            foreach (var day in workingHoursUnit.DaysOfWeek)
            {
                switch (day)
                {
                    case DayOfWeek.Sunday:
                        unit.new_sunday = true;
                        break;
                    case DayOfWeek.Monday:
                        unit.new_monday = true;
                        break;
                    case DayOfWeek.Tuesday:
                        unit.new_tuesday = true;
                        break;
                    case DayOfWeek.Wednesday:
                        unit.new_wednesday = true;
                        break;
                    case DayOfWeek.Thursday:
                        unit.new_thursday = true;
                        break;
                    case DayOfWeek.Friday:
                        unit.new_friday = true;
                        break;
                    case DayOfWeek.Saturday:
                        unit.new_saturday = true;
                        break;
                }
            }
            base.Create(unit);
        }
    }
}
