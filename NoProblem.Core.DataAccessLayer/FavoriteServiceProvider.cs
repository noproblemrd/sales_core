﻿using ClipCall.DomainModel.Entities.VideoInvitation.Response;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class FavoriteServiceProvider : DalBase<DML.Xrm.new_favoriteserviceprovider>
    {
        #region Ctor
        public FavoriteServiceProvider(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_favoriteserviceprovider");
        }
        #endregion
        public override NoProblem.Core.DataModel.Xrm.new_favoriteserviceprovider Retrieve(Guid id)
        {
            DML.Xrm.new_favoriteserviceprovider favoriteserviceprovider = XrmDataContext.new_favoriteserviceproviders.FirstOrDefault(x => x.new_favoriteserviceproviderid == id);
            return favoriteserviceprovider;
        }
        public void AddFavoriteServiceProvider(Guid customerId, Guid supplierId)
        {
      //      DataModel.Xrm.DataContext _xrmDataContext = DataModel.XrmDataContext.Create();
     //       NoProblem.Core.DataAccessLayer.FavoriteServiceProvider favoriteDal = new DataAccessLayer.FavoriteServiceProvider(_xrmDataContext);
            bool? hasFavorite = HasFavoriteServiceProviderId(customerId, supplierId);
            if (!hasFavorite.HasValue || hasFavorite.Value)
                return;
            _AddFavoriteServiceProvider(customerId, supplierId);
        }
        public void AddFavoriteServiceProviderByInvitation(Guid customerId, Guid invitationId)
        {
            //      DataModel.Xrm.DataContext _xrmDataContext = DataModel.XrmDataContext.Create();
            //       NoProblem.Core.DataAccessLayer.FavoriteServiceProvider favoriteDal = new DataAccessLayer.FavoriteServiceProvider(_xrmDataContext);
       //     bool? hasFavorite = HasFavoriteServiceProviderId(customerId);
     //       if (!hasFavorite.HasValue || hasFavorite.Value)
     //           return;
            VideoChatInvitation invitation = new VideoChatInvitation();
            Guid supplierId = invitation.GetSupplierId(invitationId);
            if (supplierId == Guid.Empty)
                return;
            AddFavoriteServiceProvider(customerId, supplierId);
            /*
            bool? hasFavorite = HasFavoriteServiceProviderId(customerId, supplierId);
            if (!hasFavorite.HasValue || hasFavorite.Value)
                return;
            _AddFavoriteServiceProvider(customerId, supplierId);
             * */
        }
        private void _AddFavoriteServiceProvider(Guid customerId, Guid accountId)
        {
            NoProblem.Core.DataModel.Xrm.new_favoriteserviceprovider favorite = new DataModel.Xrm.new_favoriteserviceprovider();
            favorite.new_contactid = customerId;
            favorite.new_accountid = accountId;
            this.Create(favorite);
            this.XrmDataContext.SaveChanges();
        }
        private bool? HasFavoriteServiceProviderId(Guid customerId, Guid supplierId)
        {
            bool? result = null;
            string command = @"
SELECT COUNT(*) 
FROM dbo.New_favoriteserviceprovider WITH(NOLOCK) 
WHERE DeletionStateCode = 0 and New_contactId = @contactId and New_accountId = @accountId";
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command,conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@contactId", customerId);
                        cmd.Parameters.AddWithValue("@accountId", supplierId);
                        int num = (int)cmd.ExecuteScalar();
                        result = (num > 0);
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed GetFavoriteServiceProviderId sql query - customerId = {0}", customerId);
                return null;
            }
            return result;
        }
        public bool IsFavoriteSupplier(Guid customerId, Guid supplierId)
        {
            bool result;
            string command = @"
SELECT COUNT(*) 
FROM dbo.New_favoriteserviceprovider F WITH(NOLOCK) 
	INNER JOIN dbo.AccountExtensionBase A WITH(NOLOCK) on F.New_accountId = A.AccountId
WHERE F.New_contactId = @contactId and F.New_accountId = @supplierId and F.DeletionStateCode = 0
	and A.New_status in (1,4)";
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@contactId", customerId);
                        int num = (int)cmd.ExecuteScalar();
                        result = (num > 0);
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed IsFavoriteSupplier sql query - customerId = {0}, supplierId = {1}", customerId, supplierId);
                return false;
            }
            return result;
        }
        public List<GetFavoriteServiceProviderResponse> GetFavoriteServiceProvider(Guid customerId, Guid expertiseId)
        {
            List<GetFavoriteServiceProviderResponse> list = new List<GetFavoriteServiceProviderResponse>();
            if (expertiseId == Guid.Empty)
                return list;
            string command = @"
SELECT A.Name, A.New_businessImageUrl, A.AccountId
FROM dbo.New_favoriteserviceprovider F WITH(NOLOCK)
	INNER JOIN dbo.Account A WITH(NOLOCK) on F.New_accountId = A.AccountId
	INNER JOIN dbo.New_accountexpertise E WITH(NOLOCK) on E.new_accountid = A.AccountId
WHERE F.New_contactId = @customerId and E.new_primaryexpertiseid = @expertiseId
    and A.New_status in(1,4) and E.DeletionStateCode = 0
ORDER BY F.CreatedOn";
            try
            {
                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {

                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@customerId", customerId);
                        cmd.Parameters.AddWithValue("@expertiseId", expertiseId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            GetFavoriteServiceProviderResponse favoriteData = new GetFavoriteServiceProviderResponse();
                            favoriteData.AccountId = (Guid)reader["AccountId"];
                            if (reader["Name"] != DBNull.Value)
                                favoriteData.AccountName = (string)reader["Name"];
                            if (reader["New_businessImageUrl"] != DBNull.Value)
                                favoriteData.ImageUrl = (string)reader["New_businessImageUrl"];
                            list.Add(favoriteData);
                        }
                        conn.Close();
                    }
                }

            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "GetFavoriteServiceProvider sql query failed with exception. request: ContactId = {0}, ExpertiseId = {1}", customerId, expertiseId);
            }
            return list;
        }
        public class AddFavoriteServiceData
        {
            public Guid customerId { get; set; }
            public Guid invitationId { get; set; }
        }
    }
}
