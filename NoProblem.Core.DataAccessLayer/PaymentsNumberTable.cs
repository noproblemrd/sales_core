﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.SupplierPricing;

namespace NoProblem.Core.DataAccessLayer
{
    public class PaymentsNumberTable : DalBase<DataModel.Xrm.new_paymentsnumbertable>
    {
        public PaymentsNumberTable(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_paymentsnumbertable");
        }

        public override NoProblem.Core.DataModel.Xrm.new_paymentsnumbertable Retrieve(Guid id)
        {
            return XrmDataContext.new_paymentsnumbertables.FirstOrDefault(x => x.new_paymentsnumbertableid == id);
        }

        public List<PaymentsKeyValuePair> GetAll()
        {
            string query =
                @"select new_from, new_payments
                from new_paymentsnumbertable with(nolock)
                where deletionstatecode = 0
                order by new_from";
            List<PaymentsKeyValuePair> retVal = new List<PaymentsKeyValuePair>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PaymentsKeyValuePair pair = new PaymentsKeyValuePair();
                            pair.FromPrice = (int)reader[0];
                            pair.NumberOfPayments = (int)reader[1];
                            retVal.Add(pair);
                        }
                    }
                }
            }            
            return retVal;
        }
    }
}
