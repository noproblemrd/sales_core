﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class ClosureDate : DalBase<DataModel.Xrm.new_closuredate>
    {
        public ClosureDate(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_closuredate");
        }

        public override NoProblem.Core.DataModel.Xrm.new_closuredate Retrieve(Guid id)
        {
            return XrmDataContext.new_closuredates.FirstOrDefault(x => x.new_closuredateid == id);
        }

        public bool IsInClosure(DateTime localDate)
        {
            string query =
                @"select count(*)
                from new_closuredate with(nolock)
                where deletionstatecode = 0
                and @date between new_from and new_to";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@date", localDate));
            int count = (int)ExecuteScalar(query, parameters);
            bool retVal = false;
            if (count > 0)
            {
                retVal = true;
            }
            return retVal;
        }

        public List<DateTime> GetAllClosureToDate(DateTime localDate)
        {
            string query =
                @"select new_to
                from new_closuredate with(nolock)
                where deletionstatecode = 0
                and @date between new_from and new_to ";
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@date", localDate);
            var reader = ExecuteReader(query, parameters);
            List<DateTime> retVal = new List<DateTime>();
            foreach (var row in reader)
            {
                DateTime to = (DateTime)row["new_to"];
                retVal.Add(to);
            }
            return retVal;
        }

        public List<NoProblem.Core.DataModel.BusinessClosures.ClosureDateData> GetClosureDates()
        {
            string query =
                @"select new_from, new_to, new_name, new_closuredateid
                from new_closuredate with(nolock)
                where deletionstatecode = 0";
            List<NoProblem.Core.DataModel.BusinessClosures.ClosureDateData> retVal =
                new List<NoProblem.Core.DataModel.BusinessClosures.ClosureDateData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.BusinessClosures.ClosureDateData data = new NoProblem.Core.DataModel.BusinessClosures.ClosureDateData();
                            data.ClosureDateId = (Guid)reader["new_closuredateid"];
                            data.FromDate = (DateTime)reader["new_from"];
                            data.ToDate = (DateTime)reader["new_to"];
                            data.Name = (string)reader["new_name"];
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
