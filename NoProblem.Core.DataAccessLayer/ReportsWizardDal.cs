﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class ReportsWizardDal
    {
        protected readonly string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];

        public DataTable ExecuteSql(string sqlQuery)
        {
            DataTable dt = new DataTable();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                {                    
                    using (SqlDataAdapter ada = new SqlDataAdapter(command))
                    {
                        connection.Open();                        
                        ada.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }
}
