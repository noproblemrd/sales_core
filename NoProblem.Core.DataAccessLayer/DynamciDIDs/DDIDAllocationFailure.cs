﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class DDIDAllocationFailure : DalBase<DataModel.Xrm.new_ddidallocatoinfailure>
    {
        public DDIDAllocationFailure(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_ddidallocatoinfailure");
        }

        public override NoProblem.Core.DataModel.Xrm.new_ddidallocatoinfailure Retrieve(Guid id)
        {
            return XrmDataContext.new_ddidallocatoinfailures.FirstOrDefault(x => x.new_ddidallocatoinfailureid == id);
        }
    }
}
