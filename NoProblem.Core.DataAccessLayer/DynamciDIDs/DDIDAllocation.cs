﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class DDIDAllocation : DalBase<DataModel.Xrm.new_ddidallocation>
    {
        public DDIDAllocation(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_ddidallocation");
        }

        public override NoProblem.Core.DataModel.Xrm.new_ddidallocation Retrieve(Guid id)
        {
            return XrmDataContext.new_ddidallocations.FirstOrDefault(x => x.new_ddidallocationid == id);
        }

        public NoProblem.Core.DataModel.Xrm.new_ddidallocation FindExistingAllocation(string realPhone, Guid headingId, Guid webSiteId)
        {
            var allocation =
                XrmDataContext.new_ddidallocations.FirstOrDefault(x =>
                x.new_realphone == realPhone
                && x.new_primaryexpertiseid == headingId
                && x.new_websiteid == webSiteId);
            return allocation;
        }

        public void CleanAllocations()
        {
            string nonQuery =
                @"update new_ddidallocation set deletionstatecode = 2 where deletionstatecode = 0 and new_expireson < getdate() and new_lockcount <= 0;
                update new_dynamicdid set new_allocated = 0 where deletionstatecode = 0 and new_allocated = 1
                and new_dynamicdidid not in
                ( select new_dynamicdidid from new_ddidallocation where deletionstatecode = 0 );";
            ExecuteNonQuery(nonQuery);
        }

        public DataModel.DynamicDIDs.DynamicAllocationContainer FindRealNumberFromAllocation(string directPhoneNumber, bool isDnQuery)
        {
            DataModel.DynamicDIDs.DynamicAllocationContainer retVal = new NoProblem.Core.DataModel.DynamicDIDs.DynamicAllocationContainer();
            string query =
                @"declare @allocationId uniqueidentifier
                declare @realPhone nvarchar(20)
                declare @dynamicDidId uniqueidentifier
                declare @headingId uniqueidentifier
                declare @websiteId uniqueidentifier

                select @realPhone = al.new_realphone
                , @allocationId = al.new_ddidallocationid
                , @dynamicDidId = al.new_dynamicdidid
                , @headingId = al.new_primaryexpertiseid
                , @websiteId = al.new_websiteid
                from new_ddidallocation al with(nolock)
                join new_dynamicdid did with(nolock) on al.new_dynamicdidid = did.new_dynamicdidid
                where al.deletionstatecode = 0 and did.deletionstatecode = 0
                and did.new_mappingnumber = @number;

                update new_ddidallocation
                set new_lockcount = new_lockcount ";
                query += isDnQuery ? "+" : "-";
                query += @" 1
                where new_ddidallocationid = @allocationId

                select @realPhone as 'realPhone'
                , @allocationId as 'allocationId'
                , @dynamicDidId as 'dynamicDidId'
                , @headingId as 'headingId'
                , @websiteId as 'websiteId'";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@number", directPhoneNumber));
            var reader = ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                if (row["allocationId"] != DBNull.Value && row["realPhone"] != DBNull.Value)
                {
                    retVal.AllocationId = (Guid)row["allocationId"];
                    retVal.RealPhone = (string)row["realPhone"];
                    retVal.HeadingId = (Guid)row["headingId"];
                    retVal.IsFound = true;
                    retVal.DynamicDidId = (Guid)row["dynamicDidId"];
                    retVal.WebsiteId = (Guid)row["websiteId"];
                }                
                break;
            }
            if (retVal.IsFound == false)
            {
                FindLastExposureData(directPhoneNumber, retVal, parameters);
            }
            return retVal;
        }

        private void FindLastExposureData(string directPhoneNumber, DataModel.DynamicDIDs.DynamicAllocationContainer container, List<KeyValuePair<string, object>> parameters)
        {
            string exposureQuery =
                @"select top 1 ex.new_realphone
                    ,ex.new_ddidexposureid
                    ,ex.new_dynamicdidid
                    ,ex.new_primaryexpertiseid
                    ,ex.new_websiteid
                    from new_ddidexposure ex with(nolock)
                    join new_dynamicdid did with(nolock) on ex.new_dynamicdidid = did.new_dynamicdidid
                    where ex.deletionstatecode = 0 and did.deletionstatecode = 0
                    and did.new_mappingnumber = @number
                    order by ex.createdon desc";
            var exposureQueryReader = ExecuteReader(exposureQuery, parameters);
            foreach (var row in exposureQueryReader)
            {
                if (row["new_ddidexposureid"] != DBNull.Value && row["new_realphone"] != DBNull.Value)
                {
                    container.AllocationId = (Guid)row["new_ddidexposureid"];
                    container.RealPhone = (string)row["new_realphone"];
                    container.HeadingId = (Guid)row["new_primaryexpertiseid"];
                    container.IsFound = true;
                    container.DynamicDidId = (Guid)row["new_dynamicdidid"];
                    container.WebsiteId = (Guid)row["new_websiteid"];
                }
                break;
            }
        }
    }
}
