﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class DDIDExposure : DalBase<DataModel.Xrm.new_ddidexposure>
    {
        public DDIDExposure(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_ddidexposure");
        }

        public override NoProblem.Core.DataModel.Xrm.new_ddidexposure Retrieve(Guid id)
        {
            return XrmDataContext.new_ddidexposures.FirstOrDefault(x => x.new_ddidexposureid == id);
        }
    }
}
