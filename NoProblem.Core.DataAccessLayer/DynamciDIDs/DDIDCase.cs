﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class DDIDCase : DalBase<DataModel.Xrm.new_ddidcase>
    {
        public DDIDCase(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_ddidcase");
        }

        public override NoProblem.Core.DataModel.Xrm.new_ddidcase Retrieve(Guid id)
        {
            return XrmDataContext.new_ddidcases.FirstOrDefault(x => x.new_ddidcaseid == id);
        }

        public bool UpdateRecordFileLocation(string callId, string fileLocation)
        {
            string query =
                @"update new_ddidcase set new_recordfilelocation = @fileLocation
                where new_callid = @callId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@callId", callId));
            parameters.Add(new KeyValuePair<string, object>("@fileLocation", fileLocation));
            int affectedRows = ExecuteNonQuery(query, parameters);
            return affectedRows > 0;
        }
    }
}
