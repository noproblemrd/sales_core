﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class DynamicDID : DalBase<DataModel.Xrm.new_dynamicdid>
    {
        public DynamicDID(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_dynamicdid");
        }

        public override NoProblem.Core.DataModel.Xrm.new_dynamicdid Retrieve(Guid id)
        {
            return XrmDataContext.new_dynamicdids.FirstOrDefault(x => x.new_dynamicdidid == id);
        }

        public Guid FindFreeDynamicDID(Guid headingId)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                return FindFreeDynamicDID(headingId, conn);
            }
        }

        private Guid FindFreeDynamicDID(Guid headingId, SqlConnection conn)
        {
            string query =
                @"select new_dynamicdidid
                from new_dynamicdid
                where deletionstatecode = 0 and new_primaryexpertiseid = @headingId
                and (new_allocated is null or new_allocated = 0)
                and new_dynamicdidid not in
                ( select new_dynamicdidid from new_ddidallocation where deletionstatecode = 0 )";
            string nonQuery =
                @"update new_dynamicdid 
                 set new_allocated = 1 
                 where new_dynamicdidid = @id and (new_allocated is null or new_allocated = 0)";
            Guid retVal = Guid.Empty;
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@headingId", headingId);
                using (SqlDataReader reader = command.ExecuteReader(System.Data.CommandBehavior.SingleRow))
                {
                    if (reader.Read())
                    {
                        retVal = (Guid)reader[0];
                    }
                }
            }
            if (retVal != Guid.Empty)
            {
                using (SqlCommand command = new SqlCommand(nonQuery, conn))
                {
                    command.Parameters.AddWithValue("@id", retVal);
                    int rowsAffected = command.ExecuteNonQuery();
                    if (rowsAffected == 0)
                    {
                        retVal = FindFreeDynamicDID(headingId, conn);
                    }
                }
            }
            return retVal;
        }

        public Guid GetHeadingIdFromNumber(string directPhoneNumber)
        {
            Guid retVal = Guid.Empty;
            string query = 
                @"select new_primaryexpertiseid
                from new_dynamicdid with(nolock)
                where deletionstatecode = 0
                and new_mappingnumber = @number";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string,object>>();
            parameters.Add(new KeyValuePair<string,object>("@number", directPhoneNumber));
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }
    }
}
