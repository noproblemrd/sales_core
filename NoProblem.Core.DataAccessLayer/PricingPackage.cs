﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class PricingPackage : DalBase<DML.Xrm.new_pricingpackage>
    {
        private const string tableName = "new_pricingpackage";

        #region Ctor
        public PricingPackage(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache(tableName);
        } 
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_pricingpackage Retrieve(Guid id)
        {
            DML.Xrm.new_pricingpackage package = XrmDataContext.new_pricingpackages.FirstOrDefault(x => x.new_pricingpackageid == id);
            return package;
        }

        public override void Create(DML.Xrm.new_pricingpackage pricingPackage)
        {
            Autonumber.AutonumberManager autoNumber = new Autonumber.AutonumberManager();
            pricingPackage.new_number = autoNumber.GetNextNumber(tableName);
            base.Create(pricingPackage);
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_pricingpackage> PricingPackagesByState(string state)
        {
            IEnumerable<DML.Xrm.new_pricingpackage> retVal = from pack in XrmDataContext.new_pricingpackages
                                                             where
                                                             pack.statecode == state
                                                             select
                                                             pack;
            return retVal;
        }

        public string GetActivePricingPackages(string PricingMethodId)
        {
            string query = "SELECT new_name,new_pricingunitamount,new_freepricingunitamount FROM new_pricingpackage with (nolock) WHERE new_pricingmethodid = '{0}' AND deletionstatecode = 0 AND statecode = 0";

            XElement xmlDoc = new XElement("SupplierPackages");
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(string.Format(query, PricingMethodId), connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader getPackages = command.ExecuteReader())
                    {
                        while (getPackages.Read())
                        {
                            xmlDoc.Add(new XElement("SupplierPackage",
                                new XElement("Name", getPackages["new_name"].ToString()),
                                new XElement("PaidAmount", getPackages["new_pricingunitamount"].ToString()),
                                new XElement("FreeAmount", getPackages["new_freepricingunitamount"].ToString())));
                        }
                    }
                }
            }
            List<XElement> packages = new List<XElement>(xmlDoc.Elements());
            packages.Sort(packageXElementComparison);
            xmlDoc.RemoveAll();
            xmlDoc.Add(packages);
            return xmlDoc.ToString();
        }

        public Dictionary<int, int> GetActivePricingPackages()
        {
            string query = "SELECT new_pricingunitamount,new_freepricingunitamount FROM new_pricingpackage with (nolock) WHERE deletionstatecode = 0 AND statecode = 0";
            Dictionary<int, int> retVal = new Dictionary<int, int>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((int)reader[0], (int)reader[1]);
                        }
                    }
                }
            }
            return retVal;
        }

        private int packageXElementComparison(XElement x, XElement y)
        {
            int retVal = 0;
            int xValue = int.Parse(x.Element("PaidAmount").Value);
            int yValue = int.Parse(y.Element("PaidAmount").Value);
            if (xValue > yValue)
            {
                retVal = 1;
            }
            else if (xValue < yValue)
            {
                retVal = -1;
            }
            return retVal;
        }
    }
}
