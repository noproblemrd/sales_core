﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SliderData;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class SlikerKeyword : DalBase<DataModel.Xrm.new_sliderkeyword>
    {
        #region Ctor
        public SlikerKeyword(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_sliderkeyword");
        }
        #endregion

        #region Fields

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_sliderkeyword Retrieve(Guid id)
        {
            return XrmDataContext.new_sliderkeywords.FirstOrDefault(x => x.new_sliderkeywordid == id);
        }

        public KeywordsContainer
            //List<DataModel.SliderData.KeywordHeadingIdPair>
            GetAllSliderKeywords()
        {
            return CacheManager.Instance.Get<KeywordsContainer>(cachePrefix, "GetAllSliderKeywords", GetAllSliderKeywordsMethod, CacheManager.ONE_DAY);
            //return CacheManager.Get<List<DataModel.SliderData.KeywordHeadingIdPair>>(cachePrefix, "GetAllSliderKeywords", GetAllSliderKeywordsMethod);
        }

        public Dictionary<string, Guid> GetAllKeywordsIds()
        {
            return CacheManager.Instance.Get<Dictionary<string, Guid>>(cachePrefix, "GetAllKeywordsIds", GetAllKeywordsIdsMethod, 240);
        }
        public Guid GetPrimaryExpertiseByKeyword(string keyword)
        {
            Guid expertiseId = Guid.Empty;
            string command = @"
select new_primaryexpertiseid
from new_sliderkeyword with(nolock)
where deletionstatecode = 0 and new_keyword = @keyword";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        cmd.CommandTimeout = 60;
                        conn.Open();
                        cmd.Parameters.AddWithValue("@keyword", keyword);
                        object o = cmd.ExecuteScalar();
                        if (o != null && o != DBNull.Value)
                            expertiseId = (Guid)o;
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed GetPrimaryExpertiseByKeyword. keyword = {0}", keyword);
            }
            return expertiseId;
        }
        private Dictionary<string, Guid> GetAllKeywordsIdsMethod()
        {
            Dictionary<string, Guid> retVal = new Dictionary<string, Guid>();
            string query =
                @"select new_keyword, new_sliderkeywordid from new_sliderkeyword with(nolock)
                where deletionstatecode = 0";
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                string keyword = Convert.ToString(row["new_keyword"]).ToLower();
                if (!retVal.ContainsKey(keyword))
                {
                    retVal.Add(keyword, (Guid)row["new_sliderkeywordid"]);
                }
            }
            return retVal;
        }

        private KeywordsContainer
            //List<DataModel.SliderData.KeywordHeadingIdPair>
            GetAllSliderKeywordsMethod()
        {
            KeywordsContainer retVal = new KeywordsContainer();

            //List<DataModel.SliderData.KeywordHeadingIdPair> retVal =
            //    new List<KeywordHeadingIdPair>();

            string query =
                @"select new_keyword, new_primaryexpertiseid, new_priority, new_isinwhitelist
                from new_sliderkeyword with(nolock)
                where deletionstatecode = 0 and statecode = 0";
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                DataModel.SliderData.KeywordHeadingIdPair data = new NoProblem.Core.DataModel.SliderData.KeywordHeadingIdPair();
                data.Keyword = Convert.ToString(row["new_keyword"]);
                if (!string.IsNullOrEmpty(data.Keyword) && row["new_primaryexpertiseid"] != DBNull.Value)
                {
                    data.HeadingId = (Guid)row["new_primaryexpertiseid"];
                    data.Priority = row["new_priority"] != DBNull.Value ? (int)row["new_priority"] : 2;
                    data.IsInWhiteList = row["new_isinwhitelist"] != DBNull.Value ? (bool)row["new_isinwhitelist"] : true;
                    if (data.Priority != 1 && data.Priority != 2)
                    {
                        data.Priority = 2;
                    }
                    retVal.Keywords.Add(data);
                    //retVal.Add(data);
                }
            }

            string notKeyQuery =
                @"select new_keyword, new_primaryexpertiseid
                from new_notkeyword with(nolock)
                where deletionstatecode = 0 and statecode = 0";
            var notKeyReader = ExecuteReader(notKeyQuery);
            foreach (var row in notKeyReader)
            {
                DataModel.SliderData.KeywordHeadingIdPair data = new NoProblem.Core.DataModel.SliderData.KeywordHeadingIdPair();
                data.Keyword = Convert.ToString(row["new_keyword"]);
                if (!string.IsNullOrEmpty(data.Keyword) && row["new_primaryexpertiseid"] != DBNull.Value)
                {
                    data.HeadingId = (Guid)row["new_primaryexpertiseid"];
                    retVal.NotKeywords.Add(data);
                }
            }

            return retVal;
        }
    }
}
