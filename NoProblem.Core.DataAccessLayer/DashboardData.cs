﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Effect.Crm.Convertor;

namespace NoProblem.Core.DataAccessLayer
{
    public class DashboardData : DalBase<DataModel.Xrm.new_dashboarddata>
    {
        #region Ctor
        public DashboardData(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_dashboarddata");
        }
        #endregion

        #region Public Methods

        public override NoProblem.Core.DataModel.Xrm.new_dashboarddata Retrieve(Guid id)
        {
            return XrmDataContext.new_dashboarddatas.FirstOrDefault(x => x.new_dashboarddataid == id);
        }

        public void CreateDirectSql(NoProblem.Core.DataModel.Xrm.new_dashboarddata entity, DataModel.Xrm.systemuser currentUser, int year, int month, int day)
        {
            Guid rslt = Guid.NewGuid();
            StringBuilder sqlString = new StringBuilder("insert into new_dashboarddatabase(new_dashboarddataid, statecode, statuscode, deletionstatecode, owninguser, owningbusinessunit, createdon, createdby, modifiedon, modifiedby)");
            sqlString.Append(" values (N'{0}', 0, 1, 0, N'{1}', N'{2}', convert(datetime, N'{3}', 103), N'{4}', convert(datetime, N'{3}', 103), N'{4}')");
            //Effect.Crm.DB.DataBaseUtils.ExecuteSqlString(sqlString.ToString(), rslt, currentUser.systemuserid, currentUser.businessunitid, new DateTime(year, month, day).ToUniversalTime().ToString("dd/MM/yyyy HH:mm:ss"), currentUser.systemuserid);
            ExecuteNonQuery(String.Format(sqlString.ToString(), rslt, currentUser.systemuserid, currentUser.businessunitid, new DateTime(year, month, day).ToUniversalTime().ToString("dd/MM/yyyy HH:mm:ss"), currentUser.systemuserid));
            sqlString = new StringBuilder("insert into new_dashboarddataextensionbase(new_dashboarddataid");
            StringBuilder values = new StringBuilder("N'");
            values.Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(rslt.ToString()));
            values.Append("'");

            sqlString.Append(", new_date");
            string displayValue = entity.new_date;
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_soldcallsperrequest");
            displayValue = entity.new_soldcallsperrequest.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_avgdefaultprice");
            displayValue = entity.new_avgdefaultprice.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofadvertisers");
            displayValue = entity.new_amountofadvertisers.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofdeposits");
            displayValue = entity.new_amountofdeposits.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_revenuedeposits");
            displayValue = entity.new_revenuedeposits.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_avgdeposit");
            displayValue = entity.new_avgdeposit.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofrecurringdeposits");
            displayValue = entity.new_amountofrecurringdeposits.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_revenuerecurringdeposits");
            displayValue = entity.new_revenuerecurringdeposits.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_avgrecurringdeposits");
            displayValue = entity.new_avgrecurringdeposits.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofcalls");
            displayValue = entity.new_amountofcalls.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_revenuecalls");
            displayValue = entity.new_revenuecalls.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_avgpricepercall");
            displayValue = entity.new_avgpricepercall.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofcallsrealmoney");
            displayValue = entity.new_amountofcallsrealmoney.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_rrevenuecallsrealmoney");
            displayValue = entity.new_rrevenuecallsrealmoney.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_avgpricepercallrealmoney");
            displayValue = entity.new_avgpricepercallrealmoney.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelcallamount");
            displayValue = entity.new_channelcallamount.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelcallrevenue");
            displayValue = entity.new_channelcallrevenue.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelcallavgprice");
            displayValue = entity.new_channelcallavgprice.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelsmsamount");
            displayValue = entity.new_channelsmsamount.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelsmsrevenue");
            displayValue = entity.new_channelsmsrevenue.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelsmsavgprice");
            displayValue = entity.new_channelsmsavgprice.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelemailamount");
            displayValue = entity.new_channelemailamount.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelemailrevenue");
            displayValue = entity.new_channelemailrevenue.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelemailavgprice");
            displayValue = entity.new_channelemailavgprice.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelfaxamount");
            displayValue = entity.new_channelfaxamount.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelfaxrevenue");
            displayValue = entity.new_channelfaxrevenue.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_channelfaxavgprice");
            displayValue = entity.new_channelfaxavgprice.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_directcallamount");
            displayValue = entity.new_directcallamount.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_directcallrevenue");
            displayValue = entity.new_directcallrevenue.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_directcallavgprice");
            displayValue = entity.new_directcallavgprice.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_conversionratedn");
            displayValue = entity.new_conversionratedn.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_conversionratesr");
            displayValue = entity.new_conversionratesr.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofrequests");
            displayValue = entity.new_amountofrequests.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_moneyrefunded");
            displayValue = entity.new_moneyrefunded.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofrefundedcalls");
            displayValue = entity.new_amountofrefundedcalls.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_percentageofrefunds");
            displayValue = entity.new_percentageofrefunds.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_outofmoneycount");
            displayValue = entity.new_outofmoneycount.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_balancesum");
            displayValue = entity.new_balancesum.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofwoncalls");
            displayValue = entity.new_amountofwoncalls.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_exposureslisting");
            displayValue = entity.new_exposureslisting.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_exposureswidget");
            displayValue = entity.new_exposureswidget.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_requestslisting");
            displayValue = entity.new_requestslisting.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_requestswidget");
            displayValue = entity.new_requestswidget.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_availablesupplierscount");
            displayValue = entity.new_availablesupplierscount.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_customerserviceunavailabilitycount");
            displayValue = entity.new_customerserviceunavailabilitycount.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_tempunavailabilitycount");
            displayValue = entity.new_tempunavailabilitycount.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_candidatescount");
            displayValue = entity.new_candidatescount.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_virtualmoneydeposits");
            displayValue = entity.new_virtualmoneydeposits.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_virtualmoneyrecurringdeposits");
            displayValue = entity.new_virtualmoneyrecurringdeposits.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofaccessibles");
            displayValue = entity.new_amountofaccessibles.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofdonotcalllist");
            displayValue = entity.new_amountofdonotcalllist.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofexpired");
            displayValue = entity.new_amountofexpired.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountoffreshlyimported");
            displayValue = entity.new_amountoffreshlyimported.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofinregistration");
            displayValue = entity.new_amountofinregistration.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofintrial");
            displayValue = entity.new_amountofintrial.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofmachine");
            displayValue = entity.new_amountofmachine.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofremoveme");
            displayValue = entity.new_amountofremoveme.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofwebenteredmobile");
            displayValue = entity.new_amountofwebenteredmobile.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofwebenteredcode");
            displayValue = entity.new_amountofwebenteredcode.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofwebenteredheading");
            displayValue = entity.new_amountofwebenteredheading.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofwebfinished");
            displayValue = entity.new_amountofwebfinished.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofaarenteredmobile");
            displayValue = entity.new_amountofaarenteredmobile.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofaarenteredcode");
            displayValue = entity.new_amountofaarenteredcode.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofaarenteredheading");
            displayValue = entity.new_amountofaarenteredheading.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofaarfinished");
            displayValue = entity.new_amountofaarfinished.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_reroutecalls");
            displayValue = entity.new_reroutecalls.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_reroutecallsunanswered");
            displayValue = entity.new_reroutecallsunanswered.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_reroutecallsrerouted");
            displayValue = entity.new_reroutecallsrerouted.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_amountofwebregistrants");
            displayValue = entity.new_amountofwebregistrants.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_payingadvertisers");
            displayValue = entity.new_payingadvertisers.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_autoupsalecallbacksconfirmed");
            displayValue = entity.new_autoupsalecallbacksconfirmed.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_autoupsalecallbacks");
            displayValue = entity.new_autoupsalecallbacks.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_rejectedcalls");
            displayValue = entity.new_rejectedcalls.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_numberofattemptsaar");
            displayValue = entity.new_numberofattemptsaar.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_conversionratesrnoupsales");
            displayValue = entity.new_conversionratesrnoupsales.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(", new_requestswidgetnoupsales");
            displayValue = entity.new_requestswidgetnoupsales.Value.ToString();
            values.Append(", N'").Append(Effect.Crm.Convertor.ConvertorUtils.ToSql(displayValue)).Append("'");

            sqlString.Append(") values(").Append(values.ToString()).Append(")");
            //Effect.Crm.DB.DataBaseUtils.ExecuteSqlString(sqlString.ToString());
            ExecuteNonQuery(sqlString.ToString());
        }

        public bool IsExistsInDB(string dateString)
        {
            bool retVal = true;
            string query = @"select new_date from new_dashboarddata with(nolock) where new_date = @dateString and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@dateString", dateString));
            object selectRetValue = ExecuteScalar(query, parameters);
            if (selectRetValue == null || selectRetValue == DBNull.Value)
            {
                retVal = false;
            }
            return retVal;
        }

        public NoProblem.Core.DataModel.Xrm.new_dashboarddata GetDashboardData(DateTime fromDate, DateTime toDate)
        {
            DataModel.Xrm.new_dashboarddata data = new NoProblem.Core.DataModel.Xrm.new_dashboarddata();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                DoCallsQuery(fromDate, toDate, data, connection);
                DoDepositsQuery(fromDate, toDate, data, connection);
                DoRecurringDepositQuery(fromDate, toDate, data, connection);
                DoAdvertisersQuery(ref fromDate, ref toDate, data, connection);
                DoAverageDefaultPriceQuery(data, connection);
                DoSoldCallsQuery(fromDate, toDate, data, connection);
                DoChannelQuery(fromDate, toDate, data, connection, DataModel.Xrm.new_channel.ChannelCode.Call);
                DoChannelQuery(fromDate, toDate, data, connection, DataModel.Xrm.new_channel.ChannelCode.Email);
                DoChannelQuery(fromDate, toDate, data, connection, DataModel.Xrm.new_channel.ChannelCode.Fax);
                DoChannelQuery(fromDate, toDate, data, connection, DataModel.Xrm.new_channel.ChannelCode.SMS);
                DoDirectCallQuery(fromDate, toDate, data, connection);
                DoConversion(fromDate, toDate, data, connection);
                DoRefundQuery(fromDate, toDate, data, connection);
                DoBalanceQuery(data, connection);
                DoVirtualDepositQueries(fromDate, toDate, data, connection);
                DoAllTrialStatusesQuery(data, connection);
                DoRerouteQueries(data, connection, fromDate, toDate);
                DoPayingAdvertisersQuery(data, connection);
                DoAarAttemptsQuery(fromDate, toDate, data, connection);
                DoRejectedCallsQuery(fromDate, toDate, data, connection);
                DoAutoUpsaleCallBacksQuery(fromDate, toDate, data, connection);
            }

            return data;
        }

        public void UpdateDirectSql(NoProblem.Core.DataModel.Xrm.new_dashboarddata data, string dateString, bool isRecalculation)
        {
            string query =
                @"update new_dashboarddata set 
                    new_soldcallsperrequest = @new_soldcallsperrequest,";
            if (isRecalculation == false)
            {
                query += @"new_avgdefaultprice = @new_avgdefaultprice,
                    new_amountofadvertisers = @new_amountofadvertisers,
                    new_outofmoneycount = @new_outofmoneycount,
                    new_balancesum = @new_balancesum,
                    new_availablesupplierscount = @new_availablesupplierscount,
                    new_customerserviceunavailabilitycount = @new_customerserviceunavailabilitycount,
                    new_tempunavailabilitycount = @new_tempunavailabilitycount,
                    new_candidatescount = @new_candidatescount, 
                    new_amountofaccessibles = @new_amountofaccessibles,
                    new_amountofdonotcalllist = @new_amountofdonotcalllist,
                    new_amountofexpired = @new_amountofexpired,
                    new_amountoffreshlyimported = @new_amountoffreshlyimported,
                    new_amountofinregistration = @new_amountofinregistration,
                    new_amountofintrial = @new_amountofintrial,
                    new_amountofmachine = @new_amountofmachine,
                    new_amountofremoveme = @new_amountofremoveme,
                    new_amountofwebenteredmobile = @new_amountofwebenteredmobile,
                    new_amountofwebenteredcode = @new_amountofwebenteredcode,
                    new_amountofwebenteredheading = @new_amountofwebenteredheading,
                    new_amountofwebfinished = @new_amountofwebfinished,
                    new_amountofaarenteredmobile = @new_amountofaarenteredmobile,
                    new_amountofaarenteredcode = @new_amountofaarenteredcode,
                    new_amountofaarenteredheading = @new_amountofaarenteredheading,
                    new_amountofaarfinished = @new_amountofaarfinished,
                    new_amountofwebregistrants = @new_amountofwebregistrants,
                    new_payingadvertisers = @new_payingadvertisers,
                ";
            }
            query +=
                    @"new_amountofdeposits = @new_amountofdeposits,
                    new_revenuedeposits = @new_revenuedeposits,
                    new_avgdeposit = @new_avgdeposit,
                    new_amountofrecurringdeposits = @new_amountofrecurringdeposits,
                    new_revenuerecurringdeposits = @new_revenuerecurringdeposits,
                    new_avgrecurringdeposits = @new_avgrecurringdeposits,
                    new_amountofcalls = @new_amountofcalls,
                    new_revenuecalls = @new_revenuecalls,
                    new_avgpricepercall = @new_avgpricepercall,
                    new_amountofcallsrealmoney = @new_amountofcallsrealmoney,
                    new_rrevenuecallsrealmoney = @new_rrevenuecallsrealmoney,
                    new_avgpricepercallrealmoney = @new_avgpricepercallrealmoney,
                    new_channelcallamount = @new_channelcallamount,
                    new_channelcallrevenue = @new_channelcallrevenue,
                    new_channelcallavgprice = @new_channelcallavgprice,
                    new_channelemailamount = @new_channelemailamount,
                    new_channelemailrevenue = @new_channelemailrevenue,
                    new_channelemailavgprice = @new_channelemailavgprice,
                    new_channelfaxamount = @new_channelfaxamount,
                    new_channelfaxrevenue = @new_channelfaxrevenue,
                    new_channelfaxavgprice = @new_channelfaxavgprice,
                    new_channelsmsamount = @new_channelsmsamount,
                    new_channelsmsrevenue = @new_channelsmsrevenue,
                    new_channelsmsavgprice = @new_channelsmsavgprice,
                    new_directcallamount = @new_directcallamount,
                    new_directcallrevenue = @new_directcallrevenue,
                    new_directcallavgprice = @new_directcallavgprice,
                    new_conversionratedn = @new_conversionratedn,
                    new_conversionratesr = @new_conversionratesr,
                    new_amountofrequests = @new_amountofrequests,
                    new_moneyrefunded = @new_moneyrefunded,
                    new_amountofrefundedcalls = @new_amountofrefundedcalls,
                    new_percentageofrefunds = @new_percentageofrefunds,
                    new_amountofwoncalls = @new_amountofwoncalls,
                    new_exposureswidget = @new_exposureswidget,
                    new_exposureslisting = @new_exposureslisting,
                    new_requestswidget = @new_requestswidget,
                    new_requestslisting = @new_requestslisting,
                    new_virtualmoneydeposits = @new_virtualmoneydeposits,
                    new_virtualmoneyrecurringdeposits = @new_virtualmoneyrecurringdeposits,
                    new_reroutecalls = @new_reroutecalls,
                    new_reroutecallsunanswered = @new_reroutecallsunanswered,
                    new_reroutecallsrerouted = @new_reroutecallsrerouted,
                    new_autoupsalecallbacksconfirmed = @new_autoupsalecallbacksconfirmed,
                    new_autoupsalecallbacks = @new_autoupsalecallbacks,
                    new_rejectedcalls = @new_rejectedcalls,
                    new_numberofattemptsaar = @new_numberofattemptsaar,
                    new_conversionratesrnoupsales = @new_conversionratesrnoupsales,
                    new_requestswidgetnoupsales = @new_requestswidgetnoupsales
                where new_date = @new_date";

            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@new_soldcallsperrequest", data.new_soldcallsperrequest.Value));
            if (isRecalculation == false)
            {
                parameters.Add(new KeyValuePair<string, object>("@new_avgdefaultprice", data.new_avgdefaultprice.Value));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofadvertisers", data.new_amountofadvertisers.Value));
                parameters.Add(new KeyValuePair<string, object>("@new_outofmoneycount", data.new_outofmoneycount.Value));
                parameters.Add(new KeyValuePair<string, object>("@new_balancesum", data.new_balancesum.Value));
                parameters.Add(new KeyValuePair<string, object>("@new_availablesupplierscount", data.new_availablesupplierscount));
                parameters.Add(new KeyValuePair<string, object>("@new_customerserviceunavailabilitycount", data.new_customerserviceunavailabilitycount));
                parameters.Add(new KeyValuePair<string, object>("@new_tempunavailabilitycount", data.new_tempunavailabilitycount));
                parameters.Add(new KeyValuePair<string, object>("@new_candidatescount", data.new_candidatescount));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofaccessibles", data.new_amountofaccessibles));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofdonotcalllist", data.new_amountofdonotcalllist));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofexpired", data.new_amountofexpired));
                parameters.Add(new KeyValuePair<string, object>("@new_amountoffreshlyimported", data.new_amountoffreshlyimported));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofinregistration", data.new_amountofinregistration));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofintrial", data.new_amountofintrial));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofmachine", data.new_amountofmachine));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofremoveme", data.new_amountofremoveme));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofwebenteredmobile", data.new_amountofwebenteredmobile));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofwebenteredcode", data.new_amountofwebenteredcode));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofwebenteredheading", data.new_amountofwebenteredheading));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofwebfinished", data.new_amountofwebfinished));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofaarenteredmobile", data.new_amountofaarenteredmobile));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofaarenteredcode", data.new_amountofaarenteredcode));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofaarenteredheading", data.new_amountofaarenteredheading));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofaarfinished", data.new_amountofaarfinished));
                parameters.Add(new KeyValuePair<string, object>("@new_amountofwebregistrants", data.new_amountofwebregistrants));
                parameters.Add(new KeyValuePair<string, object>("@new_payingadvertisers", data.new_payingadvertisers));
            }
            parameters.Add(new KeyValuePair<string, object>("@new_amountofdeposits", data.new_amountofdeposits.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_revenuedeposits", data.new_revenuedeposits.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_avgdeposit", data.new_avgdeposit.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_amountofrecurringdeposits", data.new_amountofrecurringdeposits.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_revenuerecurringdeposits", data.new_revenuerecurringdeposits.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_avgrecurringdeposits", data.new_avgrecurringdeposits.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_amountofcalls", data.new_amountofcalls.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_revenuecalls", data.new_revenuecalls.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_avgpricepercall", data.new_avgpricepercall.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_amountofcallsrealmoney", data.new_amountofcallsrealmoney.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_rrevenuecallsrealmoney", data.new_rrevenuecallsrealmoney.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_avgpricepercallrealmoney", data.new_avgpricepercallrealmoney.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelcallamount", data.new_channelcallamount.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelcallrevenue", data.new_channelcallrevenue.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelcallavgprice", data.new_channelcallavgprice.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelemailamount", data.new_channelemailamount.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelemailrevenue", data.new_channelemailrevenue.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelemailavgprice", data.new_channelemailavgprice.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelfaxamount", data.new_channelfaxamount.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelfaxrevenue", data.new_channelfaxrevenue.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelfaxavgprice", data.new_channelfaxavgprice.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelsmsamount", data.new_channelsmsamount.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelsmsrevenue", data.new_channelsmsrevenue.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_channelsmsavgprice", data.new_channelsmsavgprice.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_directcallamount", data.new_directcallamount.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_directcallrevenue", data.new_directcallrevenue.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_directcallavgprice", data.new_directcallavgprice.Value));
            parameters.Add(new KeyValuePair<string, object>("@new_conversionratedn", data.new_conversionratedn));
            parameters.Add(new KeyValuePair<string, object>("@new_conversionratesr", data.new_conversionratesr));
            parameters.Add(new KeyValuePair<string, object>("@new_amountofrequests", data.new_amountofrequests));
            parameters.Add(new KeyValuePair<string, object>("@new_moneyrefunded", data.new_moneyrefunded));
            parameters.Add(new KeyValuePair<string, object>("@new_amountofrefundedcalls", data.new_amountofrefundedcalls));
            parameters.Add(new KeyValuePair<string, object>("@new_percentageofrefunds", data.new_percentageofrefunds));
            parameters.Add(new KeyValuePair<string, object>("@new_amountofwoncalls", data.new_amountofwoncalls));
            parameters.Add(new KeyValuePair<string, object>("@new_exposureswidget", data.new_exposureswidget));
            parameters.Add(new KeyValuePair<string, object>("@new_exposureslisting", data.new_exposureslisting));
            parameters.Add(new KeyValuePair<string, object>("@new_requestswidget", data.new_requestswidget));
            parameters.Add(new KeyValuePair<string, object>("@new_requestslisting", data.new_requestslisting));
            parameters.Add(new KeyValuePair<string, object>("@new_virtualmoneydeposits", data.new_virtualmoneydeposits));
            parameters.Add(new KeyValuePair<string, object>("@new_virtualmoneyrecurringdeposits", data.new_virtualmoneyrecurringdeposits));
            parameters.Add(new KeyValuePair<string, object>("@new_reroutecalls", data.new_reroutecalls));
            parameters.Add(new KeyValuePair<string, object>("@new_reroutecallsunanswered", data.new_reroutecallsunanswered));
            parameters.Add(new KeyValuePair<string, object>("@new_reroutecallsrerouted", data.new_reroutecallsrerouted));
            parameters.Add(new KeyValuePair<string, object>("@new_autoupsalecallbacksconfirmed", data.new_autoupsalecallbacksconfirmed));
            parameters.Add(new KeyValuePair<string, object>("@new_autoupsalecallbacks", data.new_autoupsalecallbacks));
            parameters.Add(new KeyValuePair<string, object>("@new_rejectedcalls", data.new_rejectedcalls));
            parameters.Add(new KeyValuePair<string, object>("@new_numberofattemptsaar", data.new_numberofattemptsaar));
            parameters.Add(new KeyValuePair<string, object>("@new_conversionratesrnoupsales", data.new_conversionratesrnoupsales));
            parameters.Add(new KeyValuePair<string, object>("@new_requestswidgetnoupsales", data.new_requestswidgetnoupsales));
            parameters.Add(new KeyValuePair<string, object>("@new_date", dateString));

            ExecuteNonQuery(query, parameters);
        }

        public List<DataModel.Dashboard.DayData> GetRangeData(DateTime fromDate, DateTime toDate)
        {
            string query =
                @"select
                new_soldcallsperrequest,
                new_avgdefaultprice,
                new_amountofadvertisers,
                new_amountofdeposits ,
                new_revenuedeposits,
                new_avgdeposit,
                new_amountofcalls ,
                new_revenuecalls ,
                new_avgpricepercall,
                new_amountofcallsrealmoney ,
                new_rrevenuecallsrealmoney ,
                new_avgpricepercallrealmoney,
                new_conversionratedn,
                new_conversionratesr,
                new_amountofrequests,
                new_moneyrefunded,
                new_percentageofrefunds,
                new_amountofrefundedcalls,
                new_amountofrecurringdeposits,
                new_revenuerecurringdeposits,
                new_avgrecurringdeposits,
                new_balancesum,
                new_outofmoneycount,
                new_exposureswidget,
                new_exposureslisting,
                new_requestswidget,
                new_requestslisting,
                new_amountofwoncalls,
                new_availablesupplierscount,
                new_customerserviceunavailabilitycount,
                new_tempunavailabilitycount,
                new_candidatescount,
                new_virtualmoneydeposits,
                new_virtualmoneyrecurringdeposits,
                new_amountofaccessibles,
                new_amountofdonotcalllist,
                new_amountoffreshlyimported,
                new_amountofintrial,
                new_amountofmachine,
                new_amountofexpired,
                new_amountofinregistration,
                new_amountofremoveme,
                new_reroutecalls,
                new_reroutecallsunanswered,
                new_reroutecallsrerouted,
                new_amountofwebregistrants,
                new_payingadvertisers,
                new_autoupsalecallbacksconfirmed,
                new_autoupsalecallbacks,
                new_rejectedcalls,
                new_numberofattemptsaar,
                new_conversionratesrnoupsales,
                new_requestswidgetnoupsales,
                createdon
                from
                new_dashboarddata with(nolock)
                where
                createdon between @from and @to and deletionstatecode = 0";
            List<DataModel.Dashboard.DayData> dataList = new List<NoProblem.Core.DataModel.Dashboard.DayData>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@from", fromDate);
                    command.Parameters.AddWithValue("@to", toDate);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Dashboard.DayData data = new NoProblem.Core.DataModel.Dashboard.DayData();
                            data.AmountOfAdvertisers = ConvertorUtils.ToDecimal(reader["new_amountofadvertisers"]);
                            data.AmountOfCalls = ConvertorUtils.ToDecimal(reader["new_amountofcalls"]);
                            data.AmountOfCallsRealMoney = ConvertorUtils.ToDecimal(reader["new_amountofcallsrealmoney"]);
                            data.AmountOfDeposits = ConvertorUtils.ToDecimal(reader["new_amountofdeposits"]);
                            data.AveragePreDefinedPrice = ConvertorUtils.ToDecimal(reader["new_avgdefaultprice"]);
                            data.AverageDeposits = ConvertorUtils.ToDecimal(reader["new_avgdeposit"]);
                            data.AveragePricePerCall = ConvertorUtils.ToDecimal(reader["new_avgpricepercall"]);
                            data.AvgPricePerCallRealMoney = ConvertorUtils.ToDecimal(reader["new_avgpricepercallrealmoney"]);
                            data.CreatedOn = (DateTime)reader["createdon"];
                            data.RevenueFromCalls = ConvertorUtils.ToDecimal(reader["new_revenuecalls"]);
                            data.RevenueCallsRealMoney = ConvertorUtils.ToDecimal(reader["new_rrevenuecallsrealmoney"]);
                            data.RevenueFromDeposits = ConvertorUtils.ToDecimal(reader["new_revenuedeposits"]);
                            data.SoldCallsPerRequets = ConvertorUtils.ToDecimal(reader["new_soldcallsperrequest"]);
                            data.ConversionRateDN = ConvertorUtils.ToDecimal(reader["new_conversionratedn"]);
                            data.ConversionRateSR = ConvertorUtils.ToDecimal(reader["new_conversionratesr"]);
                            data.AmountOfRequests = ConvertorUtils.ToDecimal(reader["new_amountofrequests"]);
                            data.AmountOfRefundedCalls = ConvertorUtils.ToDecimal(reader["new_amountofrefundedcalls"]);
                            data.PercentageOfRefunds = ConvertorUtils.ToDecimal(reader["new_percentageofrefunds"]);
                            data.MoneyRefunded = ConvertorUtils.ToDecimal(reader["new_moneyrefunded"]);
                            data.AmountOfRecurringDeposits = ConvertorUtils.ToDecimal(reader["new_amountofrecurringdeposits"]);
                            data.RevenueFromRecurringDeposits = ConvertorUtils.ToDecimal(reader["new_revenuerecurringdeposits"]);
                            data.AverageRecurringDeposits = ConvertorUtils.ToDecimal(reader["new_avgrecurringdeposits"]);
                            data.BalanceSum = ConvertorUtils.ToDecimal(reader["new_balancesum"]);
                            data.OutOfMoneyCount = ConvertorUtils.ToDecimal(reader["new_outofmoneycount"]);
                            data.RequestsListing = ConvertorUtils.ToDecimal(reader["new_requestslisting"]);
                            data.RequestsWidget = ConvertorUtils.ToDecimal(reader["new_requestswidget"]);
                            data.ExposuresListing = ConvertorUtils.ToDecimal(reader["new_exposureslisting"]);
                            data.ExposuresWidget = ConvertorUtils.ToDecimal(reader["new_exposureswidget"]);
                            data.AmountOfWonCalls = ConvertorUtils.ToDecimal(reader["new_amountofwoncalls"]);
                            data.AvailableAdvertisersCount = ConvertorUtils.ToDecimal(reader["new_availablesupplierscount"]);
                            data.CandidatesCount = ConvertorUtils.ToDecimal(reader["new_candidatescount"]);
                            data.CustomerServiceUnavailableCount = ConvertorUtils.ToDecimal(reader["new_customerserviceunavailabilitycount"]);
                            data.TemporaryUnavailableCount = ConvertorUtils.ToDecimal(reader["new_tempunavailabilitycount"]);
                            data.VirtualMoneyDeposits = ConvertorUtils.ToDecimal(reader["new_virtualmoneydeposits"]);
                            data.VirtualMoneyRecurringDeposits = ConvertorUtils.ToDecimal(reader["new_virtualmoneyrecurringdeposits"]);
                            data.AmountOfAccessibles = ConvertorUtils.ToDecimal(reader["new_amountofaccessibles"]);
                            data.AmountOfDoNotCallList = ConvertorUtils.ToDecimal(reader["new_amountofdonotcalllist"]);
                            data.AmountOfFreshlyImported = ConvertorUtils.ToDecimal(reader["new_amountoffreshlyimported"]);
                            data.AmountOfITCs = ConvertorUtils.ToDecimal(reader["new_amountofintrial"]);
                            data.AmountOfMachines = ConvertorUtils.ToDecimal(reader["new_amountofmachine"]);
                            data.AmountOfExpired = ConvertorUtils.ToDecimal(reader["new_amountofexpired"]);
                            data.AmountOfInRegistration = ConvertorUtils.ToDecimal(reader["new_amountofinregistration"]);
                            data.AmountOfRemoveMe = ConvertorUtils.ToDecimal(reader["new_amountofremoveme"]);
                            data.RerouteCallsCount = ConvertorUtils.ToDecimal(reader["new_reroutecalls"]);
                            data.RerouteCallsUnansweredCount = ConvertorUtils.ToDecimal(reader["new_reroutecallsunanswered"]);
                            data.RerouteCallsReroutedCount = ConvertorUtils.ToDecimal(reader["new_reroutecallsrerouted"]);
                            data.AmountOfWebRegistrants = ConvertorUtils.ToDecimal(reader["new_amountofwebregistrants"]);
                            data.PayingAdvertisers = ConvertorUtils.ToDecimal(reader["new_payingadvertisers"]);
                            data.AutoUpsaleCallbacksConfirmed = ConvertorUtils.ToDecimal(reader["new_autoupsalecallbacksconfirmed"]);
                            data.AutoUpsaleCallbacks = ConvertorUtils.ToDecimal(reader["new_autoupsalecallbacks"]);
                            data.RejectedCalls = ConvertorUtils.ToDecimal(reader["new_rejectedcalls"]);
                            data.NumberOfAttemptsAar = ConvertorUtils.ToDecimal(reader["new_numberofattemptsaar"]);
                            data.ConversionRateSrNoUpsales = ConvertorUtils.ToDecimal(reader["new_conversionratesrnoupsales"]);
                            data.RequestsWidgetNoUpsales = ConvertorUtils.ToDecimal(reader["new_requestswidgetnoupsales"]);
                            dataList.Add(data);
                        }
                    }
                }
            }

            TimeSpan span = toDate - fromDate;
            int days = span.Days + 1;
            DateTime current = fromDate.Date;
            for (int i = 0; i < days; i++)
            {
                var item = dataList.FirstOrDefault(x => x.CreatedOn.Date == current);
                if (item == null)
                {
                    DataModel.Dashboard.DayData data = new NoProblem.Core.DataModel.Dashboard.DayData();
                    data.CreatedOn = current;
                    dataList.Add(data);
                }
                current = current.AddDays(1);
            }

            return dataList;
        }

        #endregion

        #region Private Methods

        private void DoRerouteQueries(DataModel.Xrm.new_dashboarddata data, SqlConnection connection, DateTime fromDate, DateTime toDate)
        {
            string allCallsToReraoutesQuery =
                @"select count(*) as 'calls to reroute suppliers'
                    from
                    incident inc with(nolock)
                    where
                    inc.new_israrahcase = 1
                    and inc.deletionstatecode = 0
                    and inc.createdon between @from and @to";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate));
            int allCallsToReroutes = (int)ExecuteScalar(allCallsToReraoutesQuery, parameters, connection);

            string unansweredAndReroutesQuery =
                    @"select inc.incidentid, count(ia.new_incidentaccountid) as count
                    into #tmpCases
                    from
                    incident inc with(nolock)
                    join new_incidentaccount ia with(nolock) on inc.incidentid = ia.new_incidentid
                    where
                    inc.new_israrahcase = 1
                    and inc.deletionstatecode = 0
                    and ia.deletionstatecode = 0
                    and inc.createdon between @from and @to
                    group by inc.incidentid

                    select count(*) as count, 'unanswered calls' as name
                    from #tmpCases
                    join new_incidentaccount ia with(nolock) on incidentid = ia.new_incidentid
                    where
                    (count = 1 and ia.statuscode = 16)
                    or (count = 2)

                    union 

                    select count(*) as count, 'reroutes' as name
                    from #tmpCases
                    where count = 2

                    drop table #tmpCases";

            var reader = ExecuteReader(unansweredAndReroutesQuery, parameters, connection, System.Data.CommandType.Text);
            int unansweredCalls = 0;
            int reroutes = 0;
            foreach (var row in reader)
            {
                string name = (string)row["name"];
                int count = (int)row["count"];
                if (name == "unanswered calls")
                {
                    unansweredCalls = count;
                }
                else
                {
                    reroutes = count;
                }
            }

            data.new_reroutecalls = allCallsToReroutes;
            data.new_reroutecallsunanswered = unansweredCalls;
            data.new_reroutecallsrerouted = reroutes;
        }

        private void DoPayingAdvertisersQuery(DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query =
                @"declare @voucherMethodId uniqueidentifier
                select @voucherMethodId = new_paymentmethodid from new_paymentmethod with(nolock) where new_paymentmethodcode = 5

                select count(distinct new_accountid) from new_supplierpricing with(nolock)
                where deletionstatecode = 0
                and new_paymentmethodid != @voucherMethodId";
            // get how many advertisers have made a real payment (payment that is not from voucher). non-regarding status/availability or anything else.

            var scalar = ExecuteScalar(query, connection);
            int amount = 0;
            if (scalar != null && scalar != DBNull.Value)
            {
                amount = (int)scalar;
            }
            data.new_payingadvertisers = amount;
        }

        private void DoAllTrialStatusesQuery(DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query =//Trial (status = 4) account in different status reasons
                @"select count(*)
                from account with(nolock)
                where deletionstatecode = 0
                and new_status = 4
                and new_trialstatusreason = @trialstatusreason
                and (new_securitylevel is null or new_securitylevel = 0)
                and new_affiliateoriginid is null";

            data.new_amountofaccessibles = GetCount(query, (int)NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.Accessible, connection, "@trialstatusreason");
            data.new_amountofdonotcalllist = GetCount(query, (int)NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.DoNotCallList, connection, "@trialstatusreason");
            data.new_amountofexpired = GetCount(query, (int)NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.Expired, connection, "@trialstatusreason");
            data.new_amountoffreshlyimported = GetCount(query, (int)NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.FreshlyImported, connection, "@trialstatusreason");
            data.new_amountofinregistration = GetCount(query, (int)NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.InRegistration, connection, "@trialstatusreason");
            data.new_amountofintrial = GetCount(query, (int)NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.InTrial, connection, "@trialstatusreason");
            data.new_amountofmachine = GetCount(query, (int)NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.Machine, connection, "@trialstatusreason");
            data.new_amountofremoveme = GetCount(query, (int)NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.RemoveMe, connection, "@trialstatusreason");

            query =
             @"select count(*)
                from account with(nolock)
                where deletionstatecode = 0
                and (new_isfromaar = 0 or new_isfromaar is null)
                and new_stageintrialregistration = @stage
                and (new_securitylevel is null or new_securitylevel = 0)
                and new_affiliateoriginid is null";

            data.new_amountofwebenteredmobile = GetCount(query, (int)DataModel.Xrm.account.StageInTrialRegistration.EnteredMobile, connection, "@stage");
            data.new_amountofwebenteredcode = GetCount(query, (int)DataModel.Xrm.account.StageInTrialRegistration.EnteredCode, connection, "@stage");
            data.new_amountofwebenteredheading = GetCount(query, (int)DataModel.Xrm.account.StageInTrialRegistration.EnteredHeading, connection, "@stage");
            data.new_amountofwebfinished = GetCount(query, (int)DataModel.Xrm.account.StageInTrialRegistration.EnteredLocation, connection, "@stage");

            query =
             @"select count(*)
                from account with(nolock)
                where deletionstatecode = 0
                and new_isfromaar = 1
                and new_stageintrialregistration = @stage
                and (new_securitylevel is null or new_securitylevel = 0)
                and new_affiliateoriginid is null";

            data.new_amountofaarenteredmobile = GetCount(query, (int)DataModel.Xrm.account.StageInTrialRegistration.EnteredMobile, connection, "@stage");
            data.new_amountofaarenteredcode = GetCount(query, (int)DataModel.Xrm.account.StageInTrialRegistration.EnteredCode, connection, "@stage");
            data.new_amountofaarenteredheading = GetCount(query, (int)DataModel.Xrm.account.StageInTrialRegistration.EnteredHeading, connection, "@stage");
            data.new_amountofaarfinished = GetCount(query, (int)DataModel.Xrm.account.StageInTrialRegistration.EnteredLocation, connection, "@stage");

            query =
                @"declare @voucherMethodId uniqueidentifier
                select @voucherMethodId = New_paymentmethodid
                from New_paymentmethod with(nolock) where
                DeletionStateCode = 0 and New_paymentmethod.New_paymentmethodcode = 5

                select count(*)
                from account with(nolock)
                where deletionstatecode = 0
                and new_stageintrialregistration = @stage
                and (new_securitylevel is null or new_securitylevel = 0)
                and new_affiliateoriginid is null
                 and new_status = 1
                 
                 and 
                 (
                 select COUNT(*) from New_supplierpricing with(nolock) 
                 where new_paymentmethodid != @voucherMethodId and DeletionStateCode = 0
                 and new_accountid = AccountId
                 ) = 0";
            data.new_amountofwebregistrants = GetCount(query, (int)DataModel.Xrm.account.StageInTrialRegistration.EnteredLocation, connection, "@stage");
        }

        private int GetCount(string query, int paramerValue, SqlConnection connection, string parameterName)
        {
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>(parameterName, paramerValue));
            int count = (int)ExecuteScalar(query, parameters, connection);
            return count;
        }

        private void DoBalanceQuery(DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query =
                @"select sum(new_cashbalance)
                from account with(nolock)
                where deletionstatecode = 0
                and new_status = 1";// 1 is approved
            object scalar = ExecuteScalar(query, connection);
            decimal sum = 0;
            if (scalar != null && scalar != DBNull.Value)
            {
                sum = (decimal)ExecuteScalar(query);
            }
            data.new_balancesum = sum;
        }

        private void DoRefundQuery(DateTime fromDate, DateTime toDate, NoProblem.Core.DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query =
                   @"select sum(new_potentialincidentprice) as moneyRefunded, count(*) as refunds
                    ,(  select count(*)
                        from new_incidentaccount with(nolock)
                        where deletionstatecode = 0 and createdon between @from and @to
                        and (statuscode = @close or statuscode = @closeDirect or statuscode = @invited)
                    ) as wonCalls
                    from new_incidentaccount with(nolock)
                    where new_refundstatus = @approved and createdon between @from and @to and deletionstatecode = 0";
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                command.Parameters.AddWithValue("@close", (int)DataModel.Xrm.new_incidentaccount.Status.CLOSE);
                command.Parameters.AddWithValue("@closeDirect", (int)DataModel.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS);
                command.Parameters.AddWithValue("@invited", (int)DataModel.Xrm.new_incidentaccount.Status.INVITED_BY_SMS);
                command.Parameters.AddWithValue("@approved", (int)DataModel.Xrm.new_incidentaccount.RefundSatus.Approved);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_moneyrefunded = reader["moneyRefunded"] != DBNull.Value ? (decimal)reader["moneyRefunded"] : 0;
                        data.new_amountofrefundedcalls = (int)reader["refunds"];
                        int wonCalls = (int)reader["wonCalls"];
                        data.new_amountofwoncalls = wonCalls;
                        data.new_percentageofrefunds = wonCalls == 0 ? 0 : (decimal)(((double)data.new_amountofrefundedcalls) / ((double)wonCalls) * 100);
                    }
                    else
                    {
                        data.new_moneyrefunded = 0;
                        data.new_amountofrefundedcalls = 0;
                        data.new_percentageofrefunds = 0;
                        data.new_amountofwoncalls = 0;
                    }
                }
            }
        }

        private void DoConversion(DateTime fromDate, DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query = @"
                declare @DNexpo decimal
                declare @SRexpo decimal
                declare @inciDN decimal
                declare @inciSR decimal
                declare @inciSRnoUpsale decimal
                declare @upsaleOriginId uniqueidentifier
                declare @autoUpsaleOriginId uniqueidentifier

                set @upsaleOriginId = (select top 1 new_originid
                from new_origin
                where
                deletionstatecode = 0
                and new_isupsaleorigin = 1)

                set @autoUpsaleOriginId = (select top 1 new_originid
                from new_origin
                where
                deletionstatecode = 0
                and new_isautoupsaleorigin = 1)

                set @DNexpo = (select 
                Count(new_exposureid)
                from
                new_exposure with(nolock)
                where new_type = 1
                and deletionstatecode = 0
                and isnull(new_iswelcomescreenevent, 0) = 0
                and 
                new_date between @from and @to)

                set @SRexpo = (select 
                Count(new_exposureid)
                from
                new_exposure with(nolock)
                where new_type = 2
                and deletionstatecode = 0
                and isnull(new_iswelcomescreenevent, 0) = 0
                and 
                new_date between @from and @to)

                set @inciSR = (select count(incidentid)
                from incident with(nolock)
                where
                ((ISNULL(new_isdirectnumber, 0) = 0) 
                and 
                (isnull(new_isfromlisting, 0) = 0))
                and 
                deletionstatecode = 0
                and new_createdonlocal between @from and @to)

                set @inciSRnoUpsale = (select count(incidentid)
                from incident with(nolock)
                where
                ((ISNULL( new_isdirectnumber, 0) = 0)
                and
                (isnull(new_isfromlisting,0) = 0))
                and
                (new_originid != @upsaleOriginId)
                and
                (new_originid != @autoUpsaleOriginId)
                and
                deletionstatecode = 0
                and new_createdonlocal between @from and @to)

                set @inciDN = (select count(incidentid)
                from incident with(nolock)
                where
                (new_isdirectnumber = 1 or new_isfromlisting = 1)
                and ISNULL(new_isgoldennumbercall, 0) = 0
                and
                deletionstatecode = 0
                and new_createdonlocal between @from and @to)

                select 
                case when @DNexpo = 0 then 0 else @inciDN / @DNexpo * 100 end as conversionDN,
                case when @SRexpo = 0 then 0 else @inciSR / @SRexpo * 100 end as conversionSR,
                @DNexpo as exposuresListing, @SRexpo as exposuresWidget, @inciDN as requestsListing, @inciSR as requestsWidget, @inciSRnoUpsale as requestsWidgetNoUpsales,
                case when @SRexpo = 0 then 0 else @inciSRnoUpsale / @SRexpo * 100 end as conversionSRnoUpsales
                ";


            //using (SqlCommand command = new SqlCommand(query, connection))
            //{
            //    command.Parameters.AddWithValue("@from", fromDate);
            //    command.Parameters.AddWithValue("@to", toDate);
            //    command.Parameters.AddWithValue("@listing", (int)DataModel.Xrm.new_exposure.ExposureType.Listing);
            //    command.Parameters.AddWithValue("@widget", (int)DataModel.Xrm.new_exposure.ExposureType.Widget);

            //    using (SqlDataReader reader = command.ExecuteReader())
            //    {
            //        if (reader.Read())
            //        {
            //            data.new_conversionratedn = (decimal)reader[0];
            //            data.new_conversionratesr = (decimal)reader[1];
            //            data.new_exposureslisting = (int)(decimal)reader[2];
            //            data.new_exposureswidget = (int)(decimal)reader[3];
            //            data.new_requestslisting = (int)(decimal)reader[4];
            //            data.new_requestswidget = (int)(decimal)reader[5];
            //            data.new_requestswidgetnoupsales = (int)(decimal)reader[6];
            //            data.new_conversionratesrnoupsales = (decimal)reader[7];
            //        }
            //        else
            //        {
            //            data.new_conversionratedn = 0;
            //            data.new_conversionratesr = 0;
            //            data.new_exposureslisting = 0;
            //            data.new_exposureswidget = 0;
            //            data.new_requestslisting = 0;
            //            data.new_requestswidget = 0;
            //            data.new_requestswidgetnoupsales = 0;
            //            data.new_conversionratesrnoupsales = 0;
            //        }
            //    }

            //}
            data.new_conversionratedn = 0;
            data.new_conversionratesr = 0;
            data.new_exposureslisting = 0;
            data.new_exposureswidget = 0;
            data.new_requestslisting = 0;
            data.new_requestswidget = 0;
            data.new_requestswidgetnoupsales = 0;
            data.new_conversionratesrnoupsales = 0;
        }

        private void DoSoldCallsQuery(DateTime fromDate, DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query = @"
                    select count(incidentid)
                    from incident with(nolock)
                    where
                    CreatedOn between @from and @to and deletionstatecode = 0
                    and (statuscode != @blackList and  statuscode != @badWord)
                    and ISNULL(new_isgoldennumbercall, 0) = 0
                    and isnull(new_isstoppedmanually, 0) = 0";

            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                command.Parameters.AddWithValue("@blackList", (int)DataModel.Xrm.incident.Status.BLACKLIST);
                command.Parameters.AddWithValue("@badWord", (int)DataModel.Xrm.incident.Status.BADWORD);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        int incidentCount = ConvertorUtils.ToInt(reader[0]);
                        data.new_amountofrequests = incidentCount;
                        if (incidentCount != 0)
                        {
                            data.new_soldcallsperrequest = (decimal)data.new_amountofcalls / (decimal)incidentCount;
                        }
                        else
                        {
                            data.new_soldcallsperrequest = 0;
                        }
                    }
                    else
                    {
                        data.new_soldcallsperrequest = 0;
                    }
                }
            }
        }

        private void DoAverageDefaultPriceQuery(DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            //avg of default price of all active accountexpertises of approved suppliers (without publisher users).
            string query = @"
                    select avg(ae.new_incidentprice) 'AverageDefaultPrice'
                    from
                    new_accountexpertise ae with(nolock)
                    join Account ac with(nolock) on ac.AccountId = ae.new_accountid
                    where 
                    ae.statecode = 0 
                    and ae.deletionstatecode = 0
                    and ac.DeletionStateCode = 0
                    and ac.New_status = 1
                    and isnull(ac.New_securitylevel, 0) = 0
                    and ae.new_incidentprice is not null
                    and ac.new_dapazstatus is null";

            using (SqlCommand command = new SqlCommand(query, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_avgdefaultprice = ConvertorUtils.ToDecimal(reader[0]);
                    }
                    else
                    {
                        data.new_avgdefaultprice = 0;
                    }
                }
            }
        }

        private void DoAdvertisersQuery(ref DateTime fromDate, ref DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query = @"
                    select count(accountid) 'NumberOfAdvertisers'
                    from
                    account with(nolock)
                    where
                    new_status = 1 and deletionstatecode = 0
                    and isnull(New_securitylevel, 0) = 0
                    and new_affiliateoriginid is null
                    and new_dapazstatus is null";

            using (SqlCommand command = new SqlCommand(query, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_amountofadvertisers = ConvertorUtils.ToInt(reader[0]);
                    }
                    else
                    {
                        data.new_amountofadvertisers = 0;
                    }
                }
            }

            string availableQuery =
                @"select count(accountid) 'NumberOfAdvertisers'
from
account with(nolock)
where
new_status = 1 and deletionstatecode = 0
and isnull(New_securitylevel, 0) = 0
and new_affiliateoriginid is null
and new_dapazstatus is null
and
(New_unavailabilityreasonId is null or
(New_unavailableto is not null and New_unavailableto < GETUTCDATE())
or ((new_unavailablefrom is not null and New_unavailablefrom > GETUTCDATE())))";

            using (SqlCommand command = new SqlCommand(availableQuery, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_availablesupplierscount = ConvertorUtils.ToInt(reader[0]);
                    }
                    else
                    {
                        data.new_availablesupplierscount = 0;
                    }
                }
            }

            string unavailabilityGroupQuery =
                @"select 
count(accountid) 'NumberOfAdvertisers'
from account acc with(nolock)
join new_unavailabilityreason ur with(nolock) on ur.new_unavailabilityreasonid = acc.new_unavailabilityreasonid
join new_unavailabilityreasongroup grp with(nolock) on grp.new_unavailabilityreasongroupid = ur.new_groupid
where
new_status = 1 and acc.deletionstatecode = 0
and new_affiliateoriginid is null
and new_dapazstatus is null
and isnull(New_securitylevel, 0) = 0
and
not(
(acc.New_unavailabilityreasonId is null or
(New_unavailableto is not null and New_unavailableto < GETUTCDATE())
or ((new_unavailablefrom is not null and New_unavailablefrom > GETUTCDATE())))
)
and grp.new_code = @groupCode
and ur.deletionstatecode = 0 and grp.deletionstatecode = 0";

            using (SqlCommand command = new SqlCommand(unavailabilityGroupQuery, connection))
            {
                command.Parameters.AddWithValue("@groupCode", (int)DataModel.Xrm.new_unavailabilityreasongroup.Code.OutOfMoney);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_outofmoneycount = ConvertorUtils.ToInt(reader[0]);
                    }
                    else
                    {
                        data.new_outofmoneycount = 0;
                    }
                }
            }

            using (SqlCommand command = new SqlCommand(unavailabilityGroupQuery, connection))
            {
                command.Parameters.AddWithValue("@groupCode", (int)DataModel.Xrm.new_unavailabilityreasongroup.Code.CustomerService);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_customerserviceunavailabilitycount = ConvertorUtils.ToInt(reader[0]);
                    }
                    else
                    {
                        data.new_customerserviceunavailabilitycount = 0;
                    }
                }
            }

            using (SqlCommand command = new SqlCommand(unavailabilityGroupQuery, connection))
            {
                command.Parameters.AddWithValue("@groupCode", (int)DataModel.Xrm.new_unavailabilityreasongroup.Code.TemporaryUnavailability);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_tempunavailabilitycount = ConvertorUtils.ToInt(reader[0]);
                    }
                    else
                    {
                        data.new_tempunavailabilitycount = 0;
                    }
                }
            }

            string candidateQuery = @"
                    select count(accountid) 'NumberOfAdvertisers'
                    from
                    account with(nolock)
                    where
                    new_status = 2 and deletionstatecode = 0
                    and new_dapazstatus is null
                    and isnull(New_securitylevel, 0) = 0
                    and new_affiliateoriginid is null";

            using (SqlCommand command = new SqlCommand(candidateQuery, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_candidatescount = ConvertorUtils.ToInt(reader[0]);
                    }
                    else
                    {
                        data.new_candidatescount = 0;
                    }
                }
            }
        }

        private void DoDepositsQuery(DateTime fromDate, DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query = @"
                    select 
                    count(new_supplierpricingid) 'AmountOfDeposits'
                    , sum(new_total) 'RevenueFromDeposits'
                    , sum(new_total)/count(new_supplierpricingid) 'AverageDeposit'
                    from
                    new_supplierpricing with(nolock)
                    WHERE
                    CreatedOn between @from and @to and deletionstatecode = 0 and new_voucherid is null";

            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_amountofdeposits = ConvertorUtils.ToInt(reader[0]);
                        data.new_revenuedeposits = ConvertorUtils.ToDecimal(reader[1]);
                        data.new_avgdeposit = ConvertorUtils.ToDecimal(reader[2]);
                    }
                    else
                    {
                        data.new_amountofdeposits = 0;
                        data.new_revenuedeposits = 0;
                        data.new_avgdeposit = 0;
                    }
                }
            }
        }

        private void DoVirtualDepositQueries(DateTime fromDate, DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string queryTotal = @"
                    select
                    sum(new_total) 'total'
                    from
                    new_supplierpricing with(nolock)
                    WHERE
                    CreatedOn between @from and @to and deletionstatecode = 0 and new_voucherid is not null";

            string queryBonus = @"
                    select
                    sum(new_credit) 'bonus'
                    ,sum(new_extrabonus) 'extra'
                    from
                    new_supplierpricing with(nolock)
                    WHERE
                    CreatedOn between @from and @to and deletionstatecode = 0";

            string queryRecurringTotal = @"
                    select
                    sum(new_total) 'totalRecurring'
                    from
                    new_supplierpricing with(nolock)
                    WHERE
                    CreatedOn between @from and @to and deletionstatecode = 0 and new_voucherid is not null and new_isfirstdeposit = 0";

            string queryRecurringBonus = @"
                    select
                    sum(new_credit) 'bonusRecurring'
                    ,sum(new_extrabonus) 'extraRecurring'
                    from
                    new_supplierpricing with(nolock)
                    WHERE
                    CreatedOn between @from and @to and deletionstatecode = 0 and new_isfirstdeposit = 0";

            decimal total = 0;
            using (SqlCommand command = new SqlCommand(queryTotal, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        total += reader["total"] != DBNull.Value ? ConvertorUtils.ToDecimal(reader["total"]) : 0;
                    }
                }
            }

            using (SqlCommand command = new SqlCommand(queryBonus, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        total += reader["bonus"] != DBNull.Value ? ConvertorUtils.ToDecimal(reader["bonus"]) : 0;
                        total += reader["extra"] != DBNull.Value ? ConvertorUtils.ToDecimal(reader["extra"]) : 0;
                    }
                }
            }

            decimal recurring = 0;
            using (SqlCommand command = new SqlCommand(queryRecurringTotal, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        recurring += reader["totalRecurring"] != DBNull.Value ? ConvertorUtils.ToDecimal(reader["totalRecurring"]) : 0;
                    }
                }
            }

            using (SqlCommand command = new SqlCommand(queryRecurringBonus, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        recurring += reader["bonusRecurring"] != DBNull.Value ? ConvertorUtils.ToDecimal(reader["bonusRecurring"]) : 0;
                        recurring += reader["extraRecurring"] != DBNull.Value ? ConvertorUtils.ToDecimal(reader["extraRecurring"]) : 0;
                    }
                }
            }

            data.new_virtualmoneydeposits = total;
            data.new_virtualmoneyrecurringdeposits = recurring;
        }

        private void DoRecurringDepositQuery(DateTime fromDate, DateTime toDate, NoProblem.Core.DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query = @"
                    select 
                    count(new_supplierpricingid) 'AmountOfDeposits'
                    , sum(new_total) 'RevenueFromDeposits'
                    , sum(new_total)/count(new_supplierpricingid) 'AverageDeposit' 
                    from
                    new_supplierpricing with(nolock)
                    WHERE
                    CreatedOn between @from and @to and deletionstatecode = 0 and new_isfirstdeposit = 0 and new_voucherid is null";

            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_amountofrecurringdeposits = ConvertorUtils.ToInt(reader[0]);
                        data.new_revenuerecurringdeposits = ConvertorUtils.ToDecimal(reader[1]);
                        data.new_avgrecurringdeposits = ConvertorUtils.ToDecimal(reader[2]);
                    }
                    else
                    {
                        data.new_amountofrecurringdeposits = 0;
                        data.new_revenuerecurringdeposits = 0;
                        data.new_avgrecurringdeposits = 0;
                    }
                }
            }
        }

        private void DoCallsQuery(DateTime fromDate, DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query = @"
                    create table #tmpSupps (accountid uniqueidentifier)

                    insert into #tmpSupps
                    select distinct Accountid 
                    from Account acc with(nolock)
                    join New_supplierpricing sp with(nolock) on sp.new_accountid = acc.AccountId
                    where acc.New_IsLeadBuyer = 1
                    or sp.new_voucherid is null

                    declare @calls int
                    declare @revenue real
                    declare @avg real
                    declare @callsreal int
                    declare @revenuereal real
                    declare @avgreal real

                    select
                    @calls = COUNT(new_incidentid)-- as 'Calls'
                    , @revenue = SUM(new_potentialincidentprice)-- as 'RevenueFromCalls'
                    , @avg = SUM(new_potentialincidentprice)/COUNT(new_incidentid)-- as 'AveragePricePerCall'
                    from
                    New_incidentaccount with(nolock)
                    WHERE
                    CreatedOn between @from and @to
                    and
                    New_tocharge = 1 and deletionstatecode = 0
                    and ISNULL(new_isgoldennumbercall, 0) = 0


                    select
                    @callsreal = COUNT(new_incidentid)-- as 'Calls'
                    , @revenuereal = SUM(new_potentialincidentprice)-- as 'RevenueFromCalls'
                    , @avgreal = SUM(new_potentialincidentprice)/COUNT(new_incidentid)-- as 'AveragePricePerCall'
                    from
                    New_incidentaccount with(nolock)
                    WHERE
                    CreatedOn between @from and @to
                    and ISNULL(new_isgoldennumbercall, 0) = 0
                    and
                    New_tocharge = 1 and deletionstatecode = 0
                       and new_accountid in (
                       select * from #tmpSupps
                       )            
                                        
                    drop table #tmpSupps

                    select @calls as 'Calls', @revenue as 'RevenueFromCalls', @avg as 'AveragePricePerCall', @callsreal as 'CallsRealMoney', @revenuereal as 'RevenueFromCallsRealMoney', @avgreal as 'AveragePricePerCallRealMoney'";

            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_amountofcalls = ConvertorUtils.ToInt(reader[0]);
                        data.new_revenuecalls = ConvertorUtils.ToDecimal(reader[1]);
                        data.new_avgpricepercall = ConvertorUtils.ToDecimal(reader[2]);
                        data.new_amountofcallsrealmoney = ConvertorUtils.ToInt(reader[3]);
                        data.new_rrevenuecallsrealmoney = ConvertorUtils.ToDecimal(reader[4]);
                        data.new_avgpricepercallrealmoney = ConvertorUtils.ToDecimal(reader[5]);
                    }
                    else
                    {
                        data.new_amountofcalls = 0;
                        data.new_revenuecalls = 0;
                        data.new_avgpricepercall = 0;
                        data.new_amountofcallsrealmoney = 0;
                        data.new_rrevenuecallsrealmoney = 0;
                        data.new_avgpricepercallrealmoney = 0;
                    }
                }
            }
        }

        private void DoChannelQuery(DateTime fromDate, DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection, DataModel.Xrm.new_channel.ChannelCode channelCode)
        {
            string query = @"
                    select
                    COUNT(new_incidentid) as 'Calls'
                    , SUM(new_potentialincidentprice) as 'RevenueFromCalls'
                    , SUM(new_potentialincidentprice)/COUNT(new_incidentid) as 'AveragePricePerCall'
                    from
                    New_incidentaccount ia with(nolock)
                    join New_channel with(nolock) on new_channel.New_ChannelId = ia.New_ChannelId
                    WHERE
                    ia.CreatedOn between @from and @to
                    and
                    New_tocharge = 1 and ia.deletionstatecode = 0 and New_Channel.DeletionStateCode = 0 and ISNULL(new_isgoldennumbercall, 0) = 0
                    and New_channel.New_Code = " + ((int)channelCode).ToString();

            int amount = 0;
            decimal revenue = 0;
            decimal average = 0;
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        amount = ConvertorUtils.ToInt(reader[0]);
                        revenue = ConvertorUtils.ToDecimal(reader[1]);
                        average = ConvertorUtils.ToDecimal(reader[2]);
                    }
                }
            }
            switch (channelCode)
            {
                case NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Call:
                    data.new_channelcallamount = amount;
                    data.new_channelcallrevenue = revenue;
                    data.new_channelcallavgprice = average;
                    break;
                case NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Email:
                    data.new_channelemailamount = amount;
                    data.new_channelemailrevenue = revenue;
                    data.new_channelemailavgprice = average;
                    break;
                case NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Fax:
                    data.new_channelfaxamount = amount;
                    data.new_channelfaxrevenue = revenue;
                    data.new_channelfaxavgprice = average;
                    break;
                case NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.SMS:
                    data.new_channelsmsamount = amount;
                    data.new_channelsmsrevenue = revenue;
                    data.new_channelsmsavgprice = average;
                    break;
            }
        }

        private void DoDirectCallQuery(DateTime fromDate, DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query = @"
                    select
                    COUNT(new_incidentid) as 'Calls'
                    , SUM(new_potentialincidentprice) as 'RevenueFromCalls'
                    , SUM(new_potentialincidentprice)/COUNT(new_incidentid) as 'AveragePricePerCall'
                    from
                    New_incidentaccount ia with(nolock)
                    WHERE
                    ia.CreatedOn between @from and @to
                    and
                    New_tocharge = 1 and ia.deletionstatecode = 0
                    and (New_isdirectnumber = 1)
                    and ISNULL(new_isgoldennumbercall, 0) = 0";

            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                command.Parameters.AddWithValue("@direct", (int)DataModel.Xrm.incident.CaseTypeCode.DirectNumber);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        data.new_directcallamount = ConvertorUtils.ToInt(reader[0]);
                        data.new_directcallrevenue = ConvertorUtils.ToDecimal(reader[1]);
                        data.new_directcallavgprice = ConvertorUtils.ToDecimal(reader[2]);
                    }
                    else
                    {
                        data.new_directcallamount = 0;
                        data.new_directcallrevenue = 0;
                        data.new_directcallavgprice = 0;
                    }
                }
            }
        }

        private void DoAarAttemptsQuery(DateTime fromDate, DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query =
                @"select count(*)
                from new_incidentaccount with(nolock)
                where deletionstatecode = 0
                and createdon between @from and @to
                and new_isaarcall = 1";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate));
            var scalar = ExecuteScalar(query, parameters, connection);
            if (scalar != null && scalar != DBNull.Value)
            {
                data.new_numberofattemptsaar = (int)scalar;
            }
            else
            {
                data.new_numberofattemptsaar = 0;
            }
        }

        private void DoRejectedCallsQuery(DateTime fromDate, DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query =
                @"select count(*)
                from new_incidentaccount with(nolock)
                where
                deletionstatecode = 0
                and new_isrejected = 1
                and createdon between @from and @to";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate));
            var scalar = ExecuteScalar(query, parameters, connection);
            if (scalar != null && scalar != DBNull.Value)
            {
                data.new_rejectedcalls = (int)scalar;
            }
            else
            {
                data.new_rejectedcalls = 0;
            }
        }

        private void DoAutoUpsaleCallBacksQuery(DateTime fromDate, DateTime toDate, DataModel.Xrm.new_dashboarddata data, SqlConnection connection)
        {
            string query =
                @"select count(distinct new_upsaleid)
                from new_autoupsalecallback with(nolock)
                where deletionstatecode = 0
                and createdon between @from and @to";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate));
            var scalar = ExecuteScalar(query, parameters, connection);
            if (scalar != null && scalar != DBNull.Value)
            {
                data.new_autoupsalecallbacks = (int)scalar;
            }
            else
            {
                data.new_autoupsalecallbacks = 0;
            }

            string queryConfirmed =
                @"select count(distinct new_upsaleid)
                from new_autoupsalecallback with(nolock)
                where deletionstatecode = 0
                and createdon between @from and @to
                and new_confirmed = 1";
            var scalarConfirmed = ExecuteScalar(queryConfirmed, parameters, connection);
            if (scalarConfirmed != null && scalarConfirmed != DBNull.Value)
            {
                data.new_autoupsalecallbacksconfirmed = (int)scalarConfirmed;
            }
            else
            {
                data.new_autoupsalecallbacksconfirmed = 0;
            }
        }

        #endregion
    }
}
