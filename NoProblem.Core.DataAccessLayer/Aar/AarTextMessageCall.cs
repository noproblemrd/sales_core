﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.DataAccessLayer.Aar
{
    public class AarTextMessageCall
    {
        public Guid IncidentId {get;set;}
        public string CustomerPhone{get;set;}
        public Guid YelpSupplierId{get;set;}
        public string SupplierPhone{get;set;}
        public AarRecord aarRecord{get;set;}
        public Guid AarIncidentId { get; set; }
        public Timer timer { get; set; }
        public AarTextMessageCall(Guid IncidentId, string CustomerPhone, Guid YelpSupplierId,
            string SupplierPhone, AarRecord aarRecord, Guid AarIncidentId)
        {
            this.IncidentId = IncidentId;
            this.CustomerPhone = CustomerPhone;
            this.YelpSupplierId = YelpSupplierId;
            this.SupplierPhone = SupplierPhone;
            this.aarRecord = aarRecord;
            this.AarIncidentId = AarIncidentId;
            this.timer = timer;
        }
    }
}
