﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer.Aar
{
    public class AarRecord
    {
        public int Id { get; private set; }
        public string AarRecordName { get; private set; }
        public string NumberFolder { get; private set; }
      //  public string AarRecordFile { get; private set; }
        public Dictionary<int, AarRecordData> AarRecordFiles { get; private set; }
     //   public int Duration { get; private set; }
   //     public int step { get; private set; }
        public int RedailTimes { get; private set; }
        public int RedailIntervalMinutes { get; private set; }
        private AarRecord() 
        {
            AarRecordFiles = new Dictionary<int, AarRecordData>();
        }
        /*
        const string GetAarRecordByNameQuery = @"
SELECT Id, RecordName, FileRecord, Duration, Step 
FROM dbo.AarRecord
WHERE RecordName = @RecordName";
         * */
        const string GetAarPromptQuery = @"
SELECT Id, Numbers
FROM dbo.AarPrompt	
WHERE Name = @RecordName";
        const string GetAarRecordByNameQuery = @"
SELECT R.FileRecord, R.Duration, R.Step 
FROM dbo.AarPrompt P
	inner join dbo.AarPromptAarRecord PR on P.Id = PR.AarPromptId
	inner join dbo.AarRecord R on R.Id = PR.AarRecordId
WHERE P.Name = @RecordName
ORDER BY R.Step";
        public static AarRecord GetAarRecord(string RecordName)
        {
            AarRecord ar = new AarRecord();
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(GetAarPromptQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@RecordName", RecordName);
                    SqlDataReader reader = cmd.ExecuteReader();                    
                    if (reader.Read())
                    {
                        ar.AarRecordName = RecordName;
                        ar.Id = (int)reader["Id"];
                        ar.NumberFolder = reader["Numbers"] == DBNull.Value ? string.Empty : (string)reader["Numbers"];
                    }
                    else
                        return null;
                    reader.Dispose();
                    cmd.Dispose();
                }
                using (SqlCommand cmd = new SqlCommand(GetAarRecordByNameQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@RecordName", RecordName);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return null;
                    
                    while (reader.Read())
                    {
                        AarRecordData ard = new AarRecordData();
                        ard.step = (int)reader["Step"];
                        ard.Duration = (int)reader["Duration"];
                        ard.FileName = (string)reader["FileRecord"];
                        ar.AarRecordFiles.Add(ard.step, ard);
                    }
                    cmd.Dispose();
                }
                conn.Close();
            }
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            ar.RedailTimes = int.Parse(configDal.GetConfigurationSettingValueFromDB(NoProblem.Core.DataModel.ConfigurationKeys.AAR_VIDEOLEAD_READAIL_TIMES));
            ar.RedailIntervalMinutes = int.Parse(configDal.GetConfigurationSettingValueFromDB(NoProblem.Core.DataModel.ConfigurationKeys.AAR_VIDEOLEAD_READAIL_INTERVAL_MINUTE));
            return ar;
        }
        /*
        const string GetAarRecordByIdQuery = @"
SELECT INC.New_Prompt, INC.New_RedailTimes, INC.New_RedialIntervalMinutes, REC.Duration, REC.FileRecord, REC.Step
FROM dbo.New_aarincidentExtensionBase INC WITH(NOLOCK)
	INNER JOIN dbo.AarRecord REC WITH(NOLOCK) on INC.New_AarRecordId = REC.Id
WHERE INC.New_aarincidentId = @AarIncidentId";
         */
        const string GetAarPromptByIdQuery = @"
SELECT INC.New_Prompt, INC.New_RedailTimes, INC.New_RedialIntervalMinutes, P.Numbers
FROM dbo.New_aarincidentExtensionBase INC WITH(NOLOCK)	
	INNER JOIN dbo.AarPrompt P on INC.New_AarPromptId = P.Id
WHERE INC.New_aarincidentId = @AarIncidentId";
        const string GetAarPromptRecordsByIdQuery = @"
SELECT REC.Duration, REC.FileRecord, REC.Step
FROM dbo.New_aarincidentExtensionBase INC WITH(NOLOCK)
	INNER JOIN dbo.AarPromptAarRecord AP WITH(NOLOCK) on INC.New_AarPromptId = AP.AarPromptId
	INNER JOIN dbo.AarRecord REC WITH(NOLOCK) on REC.Id = AP.AarRecordId
WHERE INC.New_aarincidentId = @AarIncidentId";
        public AarRecord(Guid AarIncidentId)
        {
            this.AarRecordFiles = new Dictionary<int, AarRecordData>();
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(GetAarPromptByIdQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@AarIncidentId", AarIncidentId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        this.AarRecordName = (string)reader["New_Prompt"];
                        this.NumberFolder = reader["Numbers"] == DBNull.Value ? string.Empty : (string)reader["Numbers"];
                        this.RedailIntervalMinutes = (int)reader["New_RedialIntervalMinutes"];
                        this.RedailTimes = (int)reader["New_RedailTimes"];
                    }
                    reader.Dispose();
                    cmd.Dispose();
                }
                using (SqlCommand cmd = new SqlCommand(GetAarPromptRecordsByIdQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@AarIncidentId", AarIncidentId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        AarRecordData ard = new AarRecordData();
                        ard.step = (int)reader["Step"];
                        ard.Duration = (int)reader["Duration"];
                        ard.FileName = (string)reader["FileRecord"];
                        this.AarRecordFiles.Add(ard.step, ard);
                    }
                }
                conn.Close();
            }
        }
    }
    public class AarRecordData
    {
        public string FileName { get; set; }
        public int Duration { get; set; }
        public int step { get; set; }
    }
}
