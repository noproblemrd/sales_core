﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer.Aar
{
    public class AarIcidentDal : DalBase<DataModel.Xrm.new_aarincident>
    {
        public AarIcidentDal(DataModel.Xrm.DataContext xrmDataContext) :
            base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_aarincident");
        }
        public override DataModel.Xrm.new_aarincident Retrieve(Guid id)
        {
            return XrmDataContext.new_aarincidents.FirstOrDefault(x => x.new_aarincidentid == id);
        }
    }
}
