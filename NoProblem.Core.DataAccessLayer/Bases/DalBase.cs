﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace NoProblem.Core.DataAccessLayer
{
    public abstract class DalBase<T> : Generic where T : Microsoft.Xrm.Client.ICrmEntity
    {
        #region Properties

        public NoProblem.Core.DataModel.Xrm.DataContext XrmDataContext { get; set; }

        #endregion

        #region Fields
        protected const string DATETIMEFORMATSQL = "yyyy-MM-dd HH:mm:ss";
        #endregion

        #region Ctor
        public DalBase(NoProblem.Core.DataModel.Xrm.DataContext xrmDataContext)
        {
            if (xrmDataContext != null)
            {
                XrmDataContext = xrmDataContext;
            }
            else
            {
                XrmDataContext = DataModel.XrmDataContext.Create();
            }
        }
        #endregion

        public virtual IQueryable<T> All
        {
            get { throw new NotImplementedException(); }
        }

        #region Consts
        private const string ACTIVE_FOR_SET = "active";
        private const string INACTIVE_FOR_SET = "inactive";
        public const string ACTIVE = "Active";
        #endregion

        #region Public Methods

        #region Abstract Methods

        public abstract T Retrieve(Guid id);

        #endregion

        #region Virtual Methods

        public virtual void Create(T entity)
        {
            XrmDataContext.AddObject(entity.LogicalName, entity);
        }

        public virtual void Update(T entity)
        {
            XrmDataContext.UpdateObject(entity);
        }

        public virtual void Delete(T entity)
        {
            XrmDataContext.DeleteObject(entity);
        }

        public virtual void SetState(T entity, bool isActive)
        {
            SetState(entity.LogicalName, entity.Id.Value, isActive);
        }

        public virtual void SetState(string entityLogicalName, Guid entityId, bool isActive)
        {
            string stateToSet = string.Empty;
            if (isActive)
            {
                stateToSet = ACTIVE_FOR_SET;
            }
            else
            {
                stateToSet = INACTIVE_FOR_SET;
            }
            Microsoft.Crm.SdkTypeProxy.SetStateDynamicEntityRequest request = new Microsoft.Crm.SdkTypeProxy.SetStateDynamicEntityRequest();
            request.Entity = new Microsoft.Crm.Sdk.Moniker(entityLogicalName, entityId);
            request.Status = -1;
            request.State = stateToSet;
            using (var service = XrmDataContext.CreateService())
            {
                service.Execute(request);
            }
        }

        #endregion

        public Guid AddRecordToCRM(Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity entity)
        {
            SystemUser systemUserDal = new SystemUser(XrmDataContext);
            DataModel.Xrm.systemuser currentUser = systemUserDal.GetCurrentUser();
            Guid rslt = AddRecordToCRM(entity, currentUser);
            return rslt;
        }

        #endregion
    }    
}
