﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public abstract class MongoDalBase<T> where T : DataModel.MongoModel
    {
        public abstract string CollectionName { get; }

        public T Retrieve(string id)
        {
            return Retrieve(new MongoDB.Bson.ObjectId(id));
        }

        public T Retrieve(MongoDB.Bson.ObjectId objectId)
        {
            var collection = MongoDBHelper.Instance.GetCollection(CollectionName);
            var query = MongoDB.Driver.Builders.Query<T>.Where(x => x._id == objectId);
            return collection.FindOneAs<T>(query);
        }

        /// <summary>
        /// if _id is null it will insert and if _id has value it will udpate.
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public MongoDB.Bson.ObjectId Save(T document)
        {
            var collection = MongoDBHelper.Instance.GetCollection(CollectionName);
            collection.Save<T>(document);
            return document._id;
        }

        public MongoDB.Driver.MongoCursor Find(MongoDB.Driver.IMongoQuery query)
        {
            var collection = MongoDBHelper.Instance.GetCollection(CollectionName);
            return collection.FindAs<T>(query);
        }

        public T FindOne(MongoDB.Driver.IMongoQuery query)
        {
            var collection = MongoDBHelper.Instance.GetCollection(CollectionName);
            return collection.FindOneAs<T>(query);
        }
    }
}
