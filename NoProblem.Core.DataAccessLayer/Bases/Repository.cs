﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NoProblem.Core.DataModel.ClipCall;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.DataAccessLayer.Bases
{
    public class Repository
    {
        private readonly DataContext dbContext;

        public Repository(DataContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public virtual T GetById<T>(Guid id) where T : IIdentifiable
        {
            return this.dbContext.GetEntities<T>().SingleOrDefault(x => x.Id == id);
        }

        public virtual T GetSingleByCriteria<T>(Expression<Func<T, bool>> criteria)
        {
            T result = this.GetByCriteria(criteria).SingleOrDefault();
            return result;
        }

        public virtual List<T> GetAll<T>()
        {
            return this.dbContext.GetEntities<T>().ToList();
        }

        public virtual List<T> GetByCriteria<T>(Expression<Func<T, bool>> criteria)
        {
            return this.dbContext.GetEntities<T>().Where(criteria).ToList();
        }


        public virtual void Create<T>(T entity) where T : Microsoft.Xrm.Client.ICrmEntity
        {
            this.dbContext.AddObject(entity.LogicalName, entity);
            this.dbContext.SaveChanges();
        }

        public virtual void Update<T>(T entity)
        {
            this.dbContext.UpdateObject(entity);
            this.dbContext.SaveChanges();
        }

        public virtual void Delete<T>(T entity)
        {
            this.dbContext.DeleteObject(entity);
            this.dbContext.SaveChanges();
        }
    }
}