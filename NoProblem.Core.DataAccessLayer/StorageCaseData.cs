﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class StorageCaseData : DalBase<DataModel.Xrm.new_storagecasedata>
    {
        public StorageCaseData(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_storagecasedata");
        }

        public override DataModel.Xrm.new_storagecasedata Retrieve(Guid id)
        {
            return XrmDataContext.new_storagecasedatas.FirstOrDefault(x => x.new_storagecasedataid == id);
        }
    }
}
