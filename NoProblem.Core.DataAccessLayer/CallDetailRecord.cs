﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.DataAccessLayer
{
    public class CallDetailRecord : DalBase<DataModel.Xrm.new_calldetailrecord>
    {
        public CallDetailRecord(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_calldetailrecord");
        }

        public override NoProblem.Core.DataModel.Xrm.new_calldetailrecord Retrieve(Guid id)
        {
            DataModel.Xrm.new_calldetailrecord retVal = XrmDataContext.new_calldetailrecords.FirstOrDefault(x => x.new_calldetailrecordid == id);
            return retVal;
        }

        public override IQueryable<new_calldetailrecord> All
        {
            get
            {
                return XrmDataContext.new_calldetailrecords;
            }
        }
    }
}
