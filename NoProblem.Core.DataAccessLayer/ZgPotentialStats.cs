﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class ZgPotentialStats : DalBase<DataModel.Xrm.new_zgpotentialstats>
    {
        public ZgPotentialStats(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_zgpotentialstats");
        }

        public override NoProblem.Core.DataModel.Xrm.new_zgpotentialstats Retrieve(Guid id)
        {
            return XrmDataContext.new_zgpotentialstatses.FirstOrDefault(x => x.new_zgpotentialstatsid == id);
        }

        public bool ExistsNonRetrieved(Guid supplierId)
        {
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_zgpotentialstats>.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            object scalar = ExecuteScalar(existsNonRetrievedQuery, parameters);
            return scalar != null;
        }

        private const string existsNonRetrievedQuery =
            @"select top 1 1 from new_zgpotentialstats with(nolock) where deletionstatecode = 0 and new_retrievedon is null and new_accountid = @supplierId";
    }
}
