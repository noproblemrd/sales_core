﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class SocialSecurityDisabilityCaseData : DalBase<DataModel.Xrm.new_socialsecuritydisabilitycasedata>
    {
        public SocialSecurityDisabilityCaseData(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {

        }

        public override DataModel.Xrm.new_socialsecuritydisabilitycasedata Retrieve(Guid id)
        {
            return XrmDataContext.new_socialsecuritydisabilitycasedatas.FirstOrDefault(x => x.new_socialsecuritydisabilitycasedataid == id);
        }
    }
}
