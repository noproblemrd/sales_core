﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.SupplierPricing;

namespace NoProblem.Core.DataAccessLayer
{
    public class PendingSupplierPricing : DalBase<DataModel.Xrm.new_pendingsupplierpricing>
    {
        public PendingSupplierPricing(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_pendingsupplierpricing");
        }

        public override NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing Retrieve(Guid id)
        {
            return XrmDataContext.new_pendingsupplierpricings.FirstOrDefault(x => x.new_pendingsupplierpricingid == id);
        }

        public List<PendingSupplierPricingDataContainer> GetAllPending()
        {
            var retVal = new List<PendingSupplierPricingDataContainer>();
            string query =
                @"select 
                new_pendingsupplierpricingid, new_accountid, new_transactionid, new_total
                from
                new_pendingsupplierpricing with(nolock)
                where
                deletionstatecode = 0
                and new_orderstatus = 2";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            try
                            {
                                Guid pendingId = (Guid)reader["new_pendingsupplierpricingid"];
                                Guid supplierId = (Guid)reader["new_accountid"];
                                string transactionId = (string)reader["new_transactionid"];
                                int total = reader["new_total"] != DBNull.Value ? (int)reader["new_total"] : 0;
                                PendingSupplierPricingDataContainer data = new PendingSupplierPricingDataContainer();
                                data.PendingId = pendingId;
                                data.SupplierId = supplierId;
                                data.transactionId = transactionId;
                                data.Total = Convert.ToDecimal(total);
                                retVal.Add(data);
                            }
                            catch
                            {
                                continue;
                            }
                        }
                    }
                }
            }
            return retVal;
        }        
    }
}
