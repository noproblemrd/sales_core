﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class RarahDnTempCase : DalBase<DataModel.Xrm.new_rarahdntempcase>
    {
        public RarahDnTempCase(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_rarahdntempcase");
        }

        public override NoProblem.Core.DataModel.Xrm.new_rarahdntempcase Retrieve(Guid id)
        {
            return XrmDataContext.new_rarahdntempcases.FirstOrDefault(x => x.new_rarahdntempcaseid == id);
        }

        public NoProblem.Core.DataModel.Xrm.new_rarahdntempcase GetRarahTempCaseByDnQueryId(string dnQueryId)
        {
            return XrmDataContext.new_rarahdntempcases.FirstOrDefault(x => x.new_dnqueryid == dnQueryId);
        }
    }
}
