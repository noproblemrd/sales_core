﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class AccountExpertise : DalBase<DataModel.Xrm.new_accountexpertise>
    {
        #region Ctor
        public AccountExpertise(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_accountexpertise");
        }
        #endregion

        public override IQueryable<DataModel.Xrm.new_accountexpertise> All
        {
            get
            {
                return XrmDataContext.new_accountexpertises;
            }
        }

        public override NoProblem.Core.DataModel.Xrm.new_accountexpertise Retrieve(Guid id)
        {
            DataModel.Xrm.new_accountexpertise accExp = XrmDataContext.new_accountexpertises.FirstOrDefault(x => x.new_accountexpertiseid == id);
            return accExp;
        }

        public NoProblem.Core.DataModel.Xrm.new_accountexpertise GetByAccountAndPrimaryExpertise(Guid accountId, Guid primaryExpertiseId)
        {
            DataModel.Xrm.new_accountexpertise retVal = XrmDataContext.new_accountexpertises.FirstOrDefault(
                x => x.new_accountid.HasValue
                    && x.new_accountid.Value == accountId
                    && x.new_primaryexpertiseid.HasValue
                    && x.new_primaryexpertiseid.Value == primaryExpertiseId);
            return retVal;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_accountexpertise> GetActiveByAccount(Guid accountId)
        {
            IEnumerable<DataModel.Xrm.new_accountexpertise> retVal = from item in XrmDataContext.new_accountexpertises
                                                                     where
                                                                     item.new_accountid.HasValue
                                                                     &&
                                                                     item.new_primaryexpertiseid.HasValue
                                                                     &&
                                                                     item.new_accountid.Value == accountId
                                                                     &&
                                                                     item.statecode == ACTIVE
                                                                     select
                                                                     item;
            return retVal;
        }

        /// <summary>
        /// Get all the account's accountexpertise (including inactive).
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public List<Guid> GetAllIdsByAccount(Guid supplierId)
        {
            string query =
                @"select new_accountexpertiseid from new_accountexpertise with(nolock)
                    where deletionstatecode = 0 and new_accountid = @supplierId";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@supplierId", supplierId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        /// <summary>
        /// Get all the account's accountexpertise (including inactive).
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public IEnumerable<DataModel.Xrm.new_accountexpertise> GetAllByAccount(Guid supplierId)
        {
            return XrmDataContext.new_accountexpertises.Where(x => x.new_accountid == supplierId);
        }

        public List<string> GetActiveHeadingNamesForAccount(Guid accountId)
        {
            List<string> retVal = new List<string>();
            string query = @"select new_primaryexpertiseidname from new_accountexpertise with(nolock)
                    where new_accountid = @id and deletionstatecode = 0 and statecode = 0";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", accountId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((string)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public bool IsActiveWithPrimaryExpertise(Guid primaryExpertiseId)
        {
            string query = @"select top 1 1
                from
                new_accountexpertise ae with(nolock)
                join account ac with(nolock) on accountid = new_accountid
                where
                new_accountid is not null
                and
                ae.new_incidentprice is not null
                and
                new_primaryexpertiseid = @id
                and
                ae.statecode = 0
                and
                ae.deletionstatecode = 0 and ac.deletionstatecode = 0
                and ae.new_incidentprice <= new_cashbalance";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", primaryExpertiseId));
            object scalar = ExecuteScalar(query, parameters);
            bool retVal = false;
            if (scalar != null && scalar != DBNull.Value && (int)scalar == 1)
            {
                retVal = true;
            }
            return retVal;
        }

        public IEnumerable<Guid> GetAllSecondariesIds(Guid accountExpertiseId)
        {
            string query = @"
                select new_secondaryexpertiseid
                from
                new_secondaryexpertise_new_accountexpertise with (nolock)
                where
                new_accountexpertiseid = '" + accountExpertiseId.ToString() + "'";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Guid id = (Guid)reader[0];
                            retVal.Add(id);
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetIdByAccountAndPrimary(Guid accountId, Guid primaryId)
        {
            string query = @"select new_accountexpertiseid from new_accountexpertise with (nolock) where
                            new_accountid = '" + accountId.ToString() + "' and new_primaryexpertiseid = '"
                            + primaryId.ToString() + "' and deletionstatecode = 0 and statecode = 0";
            object guidObj = ExecuteScalar(query);
            Guid retVal = Guid.Empty;
            if (guidObj != null && guidObj != DBNull.Value)
            {
                retVal = (Guid)guidObj;
            }
            return retVal;
        }      

        public decimal GetIncidentPrice(Guid accountId, Guid expertiseId)
        {
            string query = "SELECT new_incidentprice FROM new_accountexpertise with (nolock) WHERE new_accountid = @accountId and new_primaryexpertiseid = @expertiseId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@accountId", accountId));
            parameters.Add(new KeyValuePair<string, object>("@expertiseId", expertiseId));
            object scalar = ExecuteScalar(query, parameters);
            decimal retVal = 0;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (decimal)scalar;
            }
            return retVal;
        }

        public int GetRanking(Guid expertiseId, Guid supplierId, decimal price)
        {
            string query =
                @"select (count(new_accountexpertiseid) + 1) as rank
                from new_accountexpertise ae with(nolock)
                join account ac with(nolock) on ac.accountid = ae.new_accountid
                where ac.deletionstatecode = 0 and ae.deletionstatecode = 0
                and ae.statecode = 0
                and (ac.new_status = 1 or (ac.new_status = 4 and ac.new_trialstatusreason = 4))
                and ae.new_incidentprice >= @price
                and ae.new_accountid != @supplierId
                and ae.new_primaryexpertiseid = @expertiseId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@price", price));
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            parameters.Add(new KeyValuePair<string, object>("@expertiseId", expertiseId));
            int rank = (int)ExecuteScalar(query, parameters);
            return rank;
        }

        public void GetSupplierMinAndMaxPrices(Guid supplierId, out decimal maxPrice, out decimal minPrice)
        {
            IEnumerable<DataModel.Xrm.new_accountexpertise> accExps = GetActiveByAccount(supplierId);
            if (accExps.Count() == 0)
            {
                maxPrice = decimal.MaxValue;
                minPrice = decimal.MinValue;
            }
            else
            {
                maxPrice = 0;
                minPrice = decimal.MaxValue;
                foreach (var accEx in accExps)
                {
                    if (accEx.new_incidentprice.HasValue && accEx.new_incidentprice.Value > maxPrice)
                    {
                        maxPrice = accEx.new_incidentprice.Value;
                    }
                    if (accEx.new_incidentprice.HasValue && accEx.new_incidentprice.Value < minPrice)
                    {
                        minPrice = accEx.new_incidentprice.Value;
                    }
                }
            }
        }
    }
}
