﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AutoUpsaleCallBackSettings : DalBase<DataModel.Xrm.new_autoupsalecallbacksettings>
    {
        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public AutoUpsaleCallBackSettings(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_autoupsalecallbacksettings");
        }

        public override NoProblem.Core.DataModel.Xrm.new_autoupsalecallbacksettings Retrieve(Guid id)
        {
            return XrmDataContext.new_autoupsalecallbacksettingses.FirstOrDefault(x => x.new_autoupsalecallbacksettingsid == id);
        }

        public string GetConfigurationSettingValue(string key)
        {
            return CacheManager.Instance.Get<string>(cachePrefix, key, GetConfigurationSettingValueMethod);
        }

        private string GetConfigurationSettingValueMethod(string key)
        {
            string query =
                @"select new_value
                    from
                    new_autoupsalecallbacksettings with (nolock)
                    where
                    new_key = @key and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string,object>>();
            parameters.Add(new KeyValuePair<string,object>("@key", key));
            var scalar  = ExecuteScalar(query, parameters);
            string retVal = string.Empty;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }
    }
}
