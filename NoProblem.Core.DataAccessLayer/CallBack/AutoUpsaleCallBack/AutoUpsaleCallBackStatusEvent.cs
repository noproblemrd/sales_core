﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AutoUpsaleCallBackStatusEvent: DalBase<DataModel.Xrm.new_autoupsalecallbackstatusevent>
    {
        public AutoUpsaleCallBackStatusEvent(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_autoupsalecallbackstatusevent");
        }

        public override NoProblem.Core.DataModel.Xrm.new_autoupsalecallbackstatusevent Retrieve(Guid id)
        {
            return XrmDataContext.new_autoupsalecallbackstatusevents.FirstOrDefault(x => x.new_autoupsalecallbackstatuseventid == id);
        }
    }
}
