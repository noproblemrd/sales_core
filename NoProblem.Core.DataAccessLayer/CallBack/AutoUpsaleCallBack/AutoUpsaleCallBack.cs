﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AutoUpsaleCallBack : DalBase<DataModel.Xrm.new_autoupsalecallback>
    {
        public AutoUpsaleCallBack(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_autoupsalecallback");
        }

        public override NoProblem.Core.DataModel.Xrm.new_autoupsalecallback Retrieve(Guid id)
        {
            return XrmDataContext.new_autoupsalecallbacks.FirstOrDefault(x => x.new_autoupsalecallbackid == id);
        }
    }
}
