﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class IvrWidgetConfiguration :DalBase<DataModel.Xrm.new_ivrwidgetconfiguration>
    {
        public IvrWidgetConfiguration(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_ivrwidgetconfiguration");
        }

        public override NoProblem.Core.DataModel.Xrm.new_ivrwidgetconfiguration Retrieve(Guid id)
        {
            return XrmDataContext.new_ivrwidgetconfigurations.FirstOrDefault(x => x.new_ivrwidgetconfigurationid == id);
        }

        public string GetConfigurationText(string property)
        {
            string query =
                @"select top 1 {0} from new_ivrwidgetconfiguration with(nolock)
                where deletionstatecode = 0";
            query = string.Format(query, property);
            string retVal = string.Empty;
            object scalar = ExecuteScalar(query);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }
    }
}
