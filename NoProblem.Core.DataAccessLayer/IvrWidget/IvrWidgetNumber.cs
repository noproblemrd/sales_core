﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class IvrWidgetNumber : DalBase<DataModel.Xrm.new_ivrwidgetnumber>
    {
        public IvrWidgetNumber(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_ivrwidgetnumber");
        }

        public override NoProblem.Core.DataModel.Xrm.new_ivrwidgetnumber Retrieve(Guid id)
        {
            return XrmDataContext.new_ivrwidgetnumbers.FirstOrDefault(x => x.new_ivrwidgetnumberid == id);
        }

        public NoProblem.Core.DataModel.Xrm.new_ivrwidgetnumber FindNumber(string number)
        {
            return XrmDataContext.new_ivrwidgetnumbers.FirstOrDefault(x => x.new_number == number);
        }

        public string GetIvrWidgetNumber(Guid originId, string headingCode)
        {
            string query =
                @"select new_number 
                    from new_ivrwidgetnumber nu with(nolock)
                    join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = nu.new_primaryexpertiseid
                    where nu.deletionstatecode = 0 and pe.deletionstatecode = 0
                    and pe.new_code = @code";
            //and nu.new_originid = @originId";//by alain and shaip on 18.7.12, we want the ivr widget to appear no matter who the origin is test A-B test it.
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@code", headingCode));
            //parameters.Add(new KeyValuePair<string, object>("@originId", originId));//by alain and shaip on 18.7.12, we want the ivr widget to appear no matter who the origin is test A-B test it.
            string retVal = null;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }
    }
}
