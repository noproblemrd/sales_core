﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class IvrWidgetCall : DalBase<DataModel.Xrm.new_ivrwidgetcall>
    {
        public IvrWidgetCall(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_ivrwidgetcall");
        }

        public override NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall Retrieve(Guid id)
        {
            return XrmDataContext.new_ivrwidgetcalls.FirstOrDefault(x => x.new_ivrwidgetcallid == id);
        }

        public Guid GetCallIdByDialerCallId(string callSid)
        {
            string query = @"select new_ivrwidgetcallid from new_ivrwidgetcall with(nolock)
                        where deletionstatecode = 0 and new_dialercallid = @callSid";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@callSid", callSid));
            Guid retVal = Guid.Empty;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }
    }
}
