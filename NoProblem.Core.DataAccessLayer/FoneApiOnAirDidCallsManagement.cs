﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class FoneApiOnAirDidCallsManagement : Generic
    {
        public string FindAndDelete(string incomingCallId)
        {
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@icomingCallId", incomingCallId);
            object scalar = ExecuteScalar(findDeleteQuery, parameters);
            string retVal = null;
            if (scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        public void InsertCall(string incomingCallId, string outgoingCallId)
        {
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@icomingCallId", incomingCallId);
            parameters.Add("@outgoingCallId", outgoingCallId);
            ExecuteNonQuery(insertQuery, parameters);
        }

        public bool Find(string incomingCallId)
        {
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@icomingCallId", incomingCallId);
            object scalar = ExecuteScalar(findQuery, parameters);
            return scalar != null;
        }

        public void UpdateOutgoingCallId(string incomingCallId, string outgoingCallId)
        {
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@icomingCallId", incomingCallId);
            parameters.Add("@outgoingCallId", outgoingCallId);
            ExecuteNonQuery(updateQuery, parameters);
        }

        private const string updateQuery =
            @"update tbl_foneApiOnAirDidCallsManagement set outgoingCallId = @outgoingCallId where incomingCallId = @icomingCallId";

        private const string findQuery =
            @"select top 1 1 from tbl_foneApiOnAirDidCallsManagement where incomingCallId = @icomingCallId";

        private const string insertQuery = 
            @"
                insert into tbl_foneApiOnAirDidCallsManagement (incomingCallId, outgoingCallId) 
                values (@icomingCallId, @outgoingCallId)
            ";

        private const string findDeleteQuery =
            @"
                declare @outgoingCallId nvarchar(50) = NULL
                select top 1 @outgoingCallId = outgoingCallId from tbl_foneApiOnAirDidCallsManagement where incomingCallId = @icomingCallId
                delete tbl_foneApiOnAirDidCallsManagement where incomingCallId = @icomingCallId
                select @outgoingCallId
            "; 
    }
}
