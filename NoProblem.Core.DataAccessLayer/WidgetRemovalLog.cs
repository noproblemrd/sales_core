﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class WidgetRemovalLog : DalBase<DataModel.Xrm.new_widgetremovallog>
    {
        public WidgetRemovalLog(DataModel.Xrm.DataContext xrmDataContext) :
            base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_widgetremovallog");
        }

        public override NoProblem.Core.DataModel.Xrm.new_widgetremovallog Retrieve(Guid id)
        {
            return XrmDataContext.new_widgetremovallogs.FirstOrDefault(x => x.new_widgetremovallogid == id);
        }
    }
}
