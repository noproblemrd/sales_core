﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using System.Data.SqlClient;
using ClipCall.DomainModel.Entities.VideoInvitation.Response;


namespace NoProblem.Core.DataAccessLayer
{
    public class VideoChatInvitation //: DalBase<DML.Xrm.new_videochatinvitation>
    {
        const string PUSH_TEXT = @"Show ""{0}"" what you need done";
        /*
         #region Ctor
        public VideoChatInvitation(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_videochatinvitation");
        }
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_videochatinvitation Retrieve(Guid id)
        {
            DML.Xrm.new_videochatinvitation incidentAccount = XrmDataContext.new_videochatinvitations.FirstOrDefault(x => x.new_videochatinvitationid == id);
            return incidentAccount;
        }

        public override IQueryable<DML.Xrm.new_videochatinvitation> All
        {
            get
            {
                return XrmDataContext.new_videochatinvitations;
            }
        }

        public override void Create(NoProblem.Core.DataModel.Xrm.new_videochatinvitation entity)
        {
            base.Create(entity);
        }
         * */
        public bool IsExistsAndAvailable(Guid invitationId)
        {
            string command = @"
SELECT COUNT(*)
FROM dbo.New_videochatinvitation with(nolock)
WHERE DeletionStateCode = 0 and New_videochatinvitationId = @invitationId and New_ContactId is null";
            bool result;
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@invitationId", invitationId);
                        int c = (int)cmd.ExecuteScalar();
                        result = c > 0;
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed VideoChatInvitation - IsExistsAndAvailable invitationId = {0}", invitationId);
                return false;
            }
            return result;
        }
       
        /*
        public void UpdateVideoChatInvitation(Guid invitationId, int apiKey, string sessionId, string token)
        {
            string command = @"
UPDATE dbo.New_videochatinvitationExtensionBase
SET New_sessionId = @sesseionId,
	New_apiKey = @apiKey, New_token = @token
WHERE New_videochatinvitationId = @videochatinvitationId";
            using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
            {

                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                  //  cmd.Parameters.AddWithValue("@contactId", customerId);
                    cmd.Parameters.AddWithValue("@sesseionId", sessionId);
                    cmd.Parameters.AddWithValue("@apiKey", apiKey);
                    cmd.Parameters.AddWithValue("@token", token);
                    cmd.Parameters.AddWithValue("@videochatinvitationId", invitationId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
         * */
        public void UpdateVideoChatInvitationOnCheckedIn(Guid invitationId)
        {
            string command = @"
UPDATE dbo.New_videochatinvitationExtensionBase
SET new_ischeckedin = 1
WHERE New_videochatinvitationId = @videochatinvitationId";
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@videochatinvitationId", invitationId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed VideoChatInvitation - UpdateVideoChatInvitationOnCheckedIn invitationId = {0}", invitationId);
            }
        }
        
        public void UpdateVideoChatInvitationArchiveId(Guid invitationId, Guid archiveId, Guid supplierId)
        {
            if (archiveId == Guid.Empty)
                return;
            string command = @"
UPDATE dbo.New_videochatinvitationExtensionBase
SET New_archiveId = @archiveId
WHERE New_videochatinvitationId = @videochatinvitationId and New_AccountId = @supplierId";
            using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
            {

                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@archiveId", archiveId.ToString());                   
                    cmd.Parameters.AddWithValue("@videochatinvitationId", invitationId);
                    cmd.Parameters.AddWithValue("@supplierId", supplierId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
         
        public void UpdateVideoChatInvitation(Guid invitationId, Guid incidentAccountId)//, Guid archiveId)
        {
            string command = @"
UPDATE dbo.New_videochatinvitationExtensionBase
SET New_IncidentAccountId = @incidentAccountId, New_joinAt = GETDATE()
WHERE New_videochatinvitationId = @videochatinvitationId";
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@incidentAccountId", incidentAccountId);
                        cmd.Parameters.AddWithValue("@videochatinvitationId", invitationId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed VideoChatInvitation - UpdateVideoChatInvitation invitationId = {0}, incidentAccountId = {1}", invitationId, incidentAccountId);
            }
        }
        public void OnSentTextMessage(Guid invitationId)
        {
            string command = @"
UPDATE dbo.New_videochatinvitationExtensionBase
SET new_sentsms = 1
WHERE New_videochatinvitationId = @invitationId";
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@invitationId", invitationId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed VideoChatInvitation - OnSentTextMessage invitationId = {0}", invitationId);
            }

        }
        /*
        public void UpdateVideoChatInvitation(Guid invitationId, string videoUrl, int duration)
        {
            string command = @"
UPDATE dbo.New_videochatinvitationExtensionBase
SET New_videoUrl = @videoUrl, New_duration = @duration
WHERE New_videochatinvitationId = @invitationId

UPDATE inc
SET inc.New_VideoDuration = @duration, inc.New_videoUrl = @videoUrl
FROM dbo.New_videochatinvitationExtensionBase vi
	inner join dbo.New_incidentaccountExtensionBase ia on vi.New_IncidentAccountId = ia.New_incidentaccountId
	inner join dbo.IncidentExtensionBase inc on inc.IncidentId = ia.new_incidentid";
            using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
            {

                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@invitationId", invitationId);
                    cmd.Parameters.AddWithValue("@videoUrl", videoUrl);
                    cmd.Parameters.AddWithValue("@duration", duration);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
        */
        public void UpdateVideoChatInvitationOnCreateCall(Guid invitationId, Guid customerId, string sessionId, int apiKey, string token, Guid archiveId)
        {
            string command = @"
UPDATE dbo.New_videochatinvitationExtensionBase 
SET New_ContactId = @contactId, New_sessionId = @sessionId,
	New_apiKey = @apiKey, New_token=  @token, New_archiveId = @archiveId	
WHERE New_videochatinvitationId = @invitationId";
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@invitationId", invitationId);
                        cmd.Parameters.AddWithValue("@contactId", customerId);
                        cmd.Parameters.AddWithValue("@sessionId", sessionId);
                        cmd.Parameters.AddWithValue("@apiKey", apiKey);
                        cmd.Parameters.AddWithValue("@token", token);
                        if (archiveId == Guid.Empty)
                            cmd.Parameters.AddWithValue("@archiveId", DBNull.Value);
                        else
                            cmd.Parameters.AddWithValue("@archiveId", archiveId.ToString());
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed VideoChatInvitation - UpdateVideoChatInvitationOnCreateCall invitationId = {0}, customerId = {1}", invitationId, customerId);
            }
        }
        public void UpdateVideoChatInvitation(string sessionId, Guid archiveId, string videoUrl, int duration)
        {
            string command = @"EXEC [dbo].[UpdateVideoCahtInvitationOnStatusChanged] @sessionId, @archiveId, @videoUrl, @duration";
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@sessionId", sessionId);
                        cmd.Parameters.AddWithValue("@archiveId", archiveId.ToString());
                        cmd.Parameters.AddWithValue("@videoUrl", videoUrl);
                        cmd.Parameters.AddWithValue("@duration", duration);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed UpdateVideoChatInvitation - UpdateVideoChatInvitation sessionId = {0}, archiveId = {1}, " +
                "videoUrl = {2}, duration = {3}", sessionId, archiveId, videoUrl, duration);

            }
        }
        public string GetArchiveId(string sessionId, Guid Id, bool isSupplier, out Guid invitationId)
        {
            string command = @"
SELECT New_archiveId, New_videochatinvitationId
FROM dbo.New_videochatinvitationExtensionBase
WHERE New_sessionId = @sessionId and
    {WHERE}";
            string whereSupplier = " New_AccountId = @id";
            string whereCustomer = " New_ContactId = @id";
            string query = command.Replace("{WHERE}", (isSupplier ?  whereSupplier : whereCustomer));
            string result = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@id", Id);
                        cmd.Parameters.AddWithValue("@sessionId", sessionId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            invitationId = (Guid)reader["New_videochatinvitationId"];
                            result = (string)reader["New_archiveId"];
                        }
                        else
                            invitationId = Guid.Empty;
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed VideoChatInvitation - GetArchiveId sessionId = {0}, Id = {1}", sessionId, Id);
                result = null;
                invitationId = Guid.Empty;
            }
            return result;
        }
        public VideoSession GetVideoChatSession(Guid invitationId)
        {
            string command = @"
SELECT New_sessionId, New_apiKey, New_token
FROM dbo.New_videochatinvitationExtensionBase
WHERE New_videochatinvitationId = @invitationId";
            VideoSession vs =null;
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@invitationId", invitationId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            vs = new VideoSession();
                            vs.SessionId = reader["New_sessionId"] == DBNull.Value ? null : (string)reader["New_sessionId"];
                            vs.ApiKey = reader["New_apiKey"] == DBNull.Value ? -1 : (int)reader["New_apiKey"];
                            vs.Token = reader["New_token"] == DBNull.Value ? null : (string)reader["New_token"];
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed VideoChatInvitation - GetVideoChatSession invitationId = {0}", invitationId);
            }
            return vs;
        }
        public InvitationCheckInResponse GetSupplierDetails(Guid invitationId)
        {
            InvitationCheckInResponse supplierDetails = new InvitationCheckInResponse();

            string command = @"
SELECT a.New_businessImageUrl, a.Name, i.new_invitationtype, category.New_primaryexpertiseId, category.New_name CategoryName
FROM dbo.New_videochatinvitationExtensionBase i WITH(NOLOCK)
	inner join dbo.Account a WITH(NOLOCK) on i.New_AccountId = a.AccountId
	OUTER apply
	(
		SELECT TOP 1 c.new_primaryexpertiseid, c.New_name
		FROM dbo.New_accountexpertise ce WITH(NOLOCK)
			inner join dbo.New_primaryexpertiseExtensionBase c WITH(NOLOCK) on ce.new_primaryexpertiseid = c.New_primaryexpertiseId
		WHERE ce.new_accountid = a.AccountId and ce.DeletionStateCode = 0
	) category
WHERE i.New_videochatinvitationId = @invitationId";
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@invitationId", invitationId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            supplierDetails.supplierName = reader["Name"] == DBNull.Value ? null : (string)reader["Name"];
                            supplierDetails.supplierImage = reader["New_businessImageUrl"] == DBNull.Value ? null : (string)reader["New_businessImageUrl"];
                            supplierDetails.invitationType = reader["new_invitationtype"] == DBNull.Value ?
                                ClipCall.DomainModel.Entities.VideoInvitation.Request.CreateInvitationRequest.eInvitationType.VIDEO_CHAT :
                                (ClipCall.DomainModel.Entities.VideoInvitation.Request.CreateInvitationRequest.eInvitationType)((int)reader["new_invitationtype"]);
                            supplierDetails.checkInStatus = InvitationCheckInResponse.eInvitationCheckInStatus.success;
                            supplierDetails.text = string.Format(PUSH_TEXT, supplierDetails.supplierName);
                            if (reader["New_primaryexpertiseId"] != DBNull.Value)
                                supplierDetails.categoryId = (Guid)reader["New_primaryexpertiseId"];
                            if (reader["CategoryName"] != DBNull.Value)
                                supplierDetails.categoryName = (string)reader["CategoryName"];
                        }
                        else
                            supplierDetails.checkInStatus = InvitationCheckInResponse.eInvitationCheckInStatus.failedToFindSupplier;
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed VideoChatInvitation - GetSupplierDetails invitationId = {0}", invitationId);
            }
            return supplierDetails;

        }
        public VideoChatPushSupplierData GetInvitationDetailsForPushSupplier(Guid invitationId, Guid customerId)
        {
            string command = @"
SELECT A.New_AccountId, C.MobilePhone, C.New_profileImageUrl
FROM
(
	SELECT New_AccountId 
	FROM dbo.New_videochatinvitationExtensionBase WITH(NOLOCK)	
	WHERE New_videochatinvitationId = @invitationId
) A
INNER JOIN
(
	SELECT MobilePhone, new_profileimageurl
	FROM dbo.Contact WITH(NOLOCK)
	WHERE ContactId = @contactId
) C on 1=1";
            VideoChatPushSupplierData data = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@invitationId", invitationId);
                        cmd.Parameters.AddWithValue("@contactId", customerId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            data = new VideoChatPushSupplierData();
                            data.supplierId = (Guid)reader["New_AccountId"];
                            data.customerPhone = (string)reader["MobilePhone"];
                            data.customerImage = reader["new_profileimageurl"] == DBNull.Value ? "" : (string)reader["new_profileimageurl"];
                        }

                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed VideoChatInvitation - GetInvitationDetailsForPushSupplier invitationId = {0}, customerId = {1}", invitationId, customerId);
            }
            return data;
        }
        public GetFavoriteServiceProviderResponse GetFavoriteServiceProvider(Guid invitationId, Guid expertiseId)
        {
            GetFavoriteServiceProviderResponse response = null;
            string command = @"
SELECT A.Name, A.New_businessImageUrl, A.AccountId
FROM dbo.New_videochatinvitationExtensionBase I WITH(NOLOCK)
	INNER JOIN dbo.Account A WITH(NOLOCK) on I.New_AccountId = A.AccountId
	INNER JOIN dbo.New_accountexpertise E WITH(NOLOCK) on E.new_accountid = A.AccountId
WHERE I.New_videochatinvitationId = @invitationId and E.new_primaryexpertiseid = @expertiseId and E.DeletionStateCode = 0";
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@invitationId", invitationId);
                        cmd.Parameters.AddWithValue("@expertiseId", expertiseId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            response = new GetFavoriteServiceProviderResponse();
                            response.AccountId = (Guid)reader["AccountId"];
                            if (reader["Name"] != DBNull.Value)
                                response.AccountName = (string)reader["Name"];
                            if (reader["New_businessImageUrl"] != DBNull.Value)
                                response.ImageUrl = (string)reader["New_businessImageUrl"];
                        }

                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed GetFavoriteServiceProvider sql query -  invitationId = {0}, expertiseiD = {1}", invitationId, expertiseId);
            }
            return response;
        }
        public Guid GetSupplierId(Guid invitationId)
        {
            Guid supplierId = Guid.Empty;

            string command = @"
SELECT New_AccountId
FROM dbo.New_videochatinvitationExtensionBase
WHERE New_videochatinvitationId = @invitationId";
            try
            {
                using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@invitationId", invitationId);
                        object o = cmd.ExecuteScalar();
                        if (o != null || o != DBNull.Value)
                            supplierId = (Guid)o;
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed GetSupplierId sql query -  invitationId = {0}", invitationId);
            }
            return supplierId;
        }
        
    }
}
