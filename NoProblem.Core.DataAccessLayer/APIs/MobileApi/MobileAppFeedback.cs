﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer.APIs.MobileApi
{
    public class MobileAppFeedback : DalBase<DataModel.Xrm.new_mobileappfeedback>
    {
        public MobileAppFeedback(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_mobileappfeedback");
        }

        public override DataModel.Xrm.new_mobileappfeedback Retrieve(Guid id)
        {
            return XrmDataContext.new_mobileappfeedbacks.FirstOrDefault(x => x.new_mobileappfeedbackid == id);
        }

        public override IQueryable<DataModel.Xrm.new_mobileappfeedback> All
        {
            get
            {
                return XrmDataContext.new_mobileappfeedbacks;
            }
        }
    }
}
