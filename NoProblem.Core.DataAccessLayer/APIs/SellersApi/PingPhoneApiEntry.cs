﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer.APIs.SellersApi
{
    public class PingPhoneApiEntry : DalBase<DataModel.Xrm.new_pingphoneapientry>
    {
        public PingPhoneApiEntry(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_pingphoneapientry");
        }

        public override IQueryable<DataModel.Xrm.new_pingphoneapientry> All
        {
            get
            {
                return XrmDataContext.new_pingphoneapientrynew_pingphoneapientries;
            }
        }

        public override DataModel.Xrm.new_pingphoneapientry Retrieve(Guid id)
        {
            return XrmDataContext.new_pingphoneapientrynew_pingphoneapientries.FirstOrDefault(x => x.new_pingphoneapientryid == id);
        }
    }
}
