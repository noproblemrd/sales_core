﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer.APIs.SellersApi
{
    public class PingPhoneApiFailedIncomingMap : DalBase<DataModel.Xrm.new_pingphoneapifailedincomingmap>
    {
        public PingPhoneApiFailedIncomingMap(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_pingphoneapifailedincomingmap");
        }

        public override IQueryable<DataModel.Xrm.new_pingphoneapifailedincomingmap> All
        {
            get
            {
                return XrmDataContext.new_pingphoneapifailedincomingmaps;
            }
        }

        public override DataModel.Xrm.new_pingphoneapifailedincomingmap Retrieve(Guid id)
        {
            return XrmDataContext.new_pingphoneapifailedincomingmaps.FirstOrDefault(x => x.new_pingphoneapifailedincomingmapid == id);
        }
    }
}
