﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class VnRequest : DalBase<DataModel.Xrm.new_vnrequest>
    {
        public VnRequest(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_vnrequest");
        }
        
        public override NoProblem.Core.DataModel.Xrm.new_vnrequest Retrieve(Guid id)
        {
            return XrmDataContext.new_vnrequests.FirstOrDefault(x => x.new_vnrequestid == id);
        }

        public DataModel.Xrm.new_vnrequest GetVnRequestByIvrSession(string ivrSession)
        {
            return XrmDataContext.new_vnrequests.FirstOrDefault(x => x.new_ivrsession == ivrSession);
        }

        public int GetNumberOfCalls(string phone, DateTime fromDate)
        {
            string query =
                @"select count(*)
                    from
                    new_vnrequest with(nolock)
                    where createdon > @date
                    and deletionstatecode = 0
                    and new_ivrcallerid = @phone";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@phone", phone));
            parameters.Add(new KeyValuePair<string, object>("@date", fromDate));
            var scalar = ExecuteScalar(query, parameters);
            int retVal = 0;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (int)scalar;
            }
            return retVal;
        }
    }
}
