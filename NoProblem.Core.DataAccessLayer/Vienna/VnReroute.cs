﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class VnReroute : DalBase<DataModel.Xrm.new_vnreroute>
    {
        public VnReroute(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_vnreroute");
        }

        public override NoProblem.Core.DataModel.Xrm.new_vnreroute Retrieve(Guid id)
        {
            return XrmDataContext.new_vnreroutes.FirstOrDefault(x => x.new_vnrerouteid == id);
        }
    }
}
