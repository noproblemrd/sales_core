﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class VnStatus : DalBase<DataModel.Xrm.new_vnstatus>
    {
        public VnStatus(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_vnstatus");
        }

        public override NoProblem.Core.DataModel.Xrm.new_vnstatus Retrieve(Guid id)
        {
            return XrmDataContext.new_vnstatuses.FirstOrDefault(x => x.new_vnstatusid == id);
        }

        public void UpdateIncomingCallDuration(string callId, int duration)
        {
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_vnstatus>.SqlParametersList();
            parameters.Add("@duration", duration);
            parameters.Add("@callId", callId);
            ExecuteNonQuery(nonQueryForUpdateIncomingCallDuration, parameters);
        }

        private const string nonQueryForUpdateIncomingCallDuration =
            @"update new_vnstatusextensionbase
set new_customercallduration = @duration
where new_ivrsession = @callId";
    }
}
