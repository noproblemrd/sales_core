﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class WibiyaCashOut : DalBase<DataModel.Xrm.new_wibiyacashout>
    {
        public WibiyaCashOut(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_wibiyacashout");
        }

        public override NoProblem.Core.DataModel.Xrm.new_wibiyacashout Retrieve(Guid id)
        {
            return XrmDataContext.new_wibiyacashouts.FirstOrDefault(x => x.new_wibiyacashoutid == id);
        }

        public DateTime? GetLastCashOutData(Guid originId, out string payPalEmail, out bool rememberPayPayAccount)
        {
            payPalEmail = null;
            rememberPayPayAccount = false;
            DateTime? retVal = null;
            string query =
                @"select top 1 createdon, new_paypalemailaddress, new_rememberpaypalaccount
                from new_wibiyacashout with(nolock)
                where deletionstatecode = 0 and new_originid = @originId
                order by createdon desc";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@originId", originId));
            var reader = ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                retVal = (DateTime)row["createdon"];
                payPalEmail = (string)row["new_paypalemailaddress"];
                rememberPayPayAccount = (bool)row["new_rememberpaypalaccount"];
            }
            return retVal;
        }
    }
}
