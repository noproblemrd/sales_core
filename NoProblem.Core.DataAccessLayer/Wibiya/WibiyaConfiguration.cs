﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class WibiyaConfiguration : DalBase<DataModel.Xrm.new_wibiyaconfiguration>
    {
        public WibiyaConfiguration(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_wibiyaconfiguration");
        }

        public override NoProblem.Core.DataModel.Xrm.new_wibiyaconfiguration Retrieve(Guid id)
        {
            return XrmDataContext.new_wibiyaconfigurations.FirstOrDefault(x => x.new_wibiyaconfigurationid == id);
        }

        public DataModel.Xrm.new_wibiyaconfiguration GetConfiguration()
        {
            return XrmDataContext.new_wibiyaconfigurations.FirstOrDefault();
        }
    }
}
