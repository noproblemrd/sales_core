﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class SystemUser : DalBase<DataModel.Xrm.systemuser>
    {
        #region Ctor
        public SystemUser(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("systemuser");
        } 
        #endregion

        public override NoProblem.Core.DataModel.Xrm.systemuser Retrieve(Guid id)
        {
            DataModel.Xrm.systemuser user = XrmDataContext.systemusers.FirstOrDefault(x => x.systemuserid == id);
            return user;
        }

        public DataModel.Xrm.systemuser GetCurrentUser()
        {
            Microsoft.Crm.SdkTypeProxy.WhoAmIRequest request = new Microsoft.Crm.SdkTypeProxy.WhoAmIRequest();
            Guid userId;
            using (var service = XrmDataContext.CreateService())
            {
                Microsoft.Crm.SdkTypeProxy.WhoAmIResponse response = service.Execute(request) as Microsoft.Crm.SdkTypeProxy.WhoAmIResponse;
                userId = response.UserId;
            }
            DataModel.Xrm.systemuser user = Retrieve(userId);
            return user;
        }
    }
}
