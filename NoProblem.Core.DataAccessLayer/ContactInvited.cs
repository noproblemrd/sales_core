﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class ContactInvited : DalBase<DataModel.Xrm.new_contactinvited>
    {
        public ContactInvited(DataModel.Xrm.DataContext datacontext):base(datacontext)
        {
            DataModel.XrmDataContext.ClearCache("new_contactinvited");
        }
        public override DataModel.Xrm.new_contactinvited Retrieve(Guid id)
        {
            return XrmDataContext.new_contactinviteds.FirstOrDefault(x => x.new_contactinvitedid == id);
        }
        public override IQueryable<DataModel.Xrm.new_contactinvited> All
        {
            get
            {
                return XrmDataContext.new_contactinviteds;
            }
        }
    }
}
