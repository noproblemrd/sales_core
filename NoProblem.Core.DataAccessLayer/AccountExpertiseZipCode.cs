﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AccountExpertiseZipCode : DalBase<DataModel.Xrm.new_accountexpertisezipcode>
    {
        public AccountExpertiseZipCode(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_accountexpertisezipcode");
        }

        public override NoProblem.Core.DataModel.Xrm.new_accountexpertisezipcode Retrieve(Guid id)
        {
            return XrmDataContext.new_accountexpertisezipcodes.FirstOrDefault(x => x.new_accountexpertisezipcodeid == id);
        }
    }
}
