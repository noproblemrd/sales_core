﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.ConfigurationSettings
//  File: ConfigurationSettings.cs
//  Description: DAL for ConfigurationSettings Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class ConfigurationSettings : DalBase<DML.Xrm.new_configurationsetting>
    {
        #region Ctor
        public ConfigurationSettings(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_configurationsetting");
        } 
        #endregion

        #region Fields

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        #endregion

        #region Public Methods

        public override NoProblem.Core.DataModel.Xrm.new_configurationsetting Retrieve(System.Guid id)
        {
            DML.Xrm.new_configurationsetting setting = XrmDataContext.new_configurationsettings.FirstOrDefault(x => x.new_configurationsettingid == id);
            return setting;
        }

        

        public string GetConfigurationSettingValue(string key)
        {
            return CacheManager.Instance.Get<string>(cachePrefix, key, GetConfigurationSettingValueMethod);
        }
        public string GetConfigurationSettingValueFromDB(string key)
        {
            return GetConfigurationSettingValueMethod(key);
        }
        public bool SetConfigurationSettingValue(string key, string value)
        {
            string query =
                @"update new_configurationsetting
                    set new_value = @value
                    where
                    new_key = @key and deletionstatecode = 0 and new_value <> @value";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@value", value));
            parameters.Add(new KeyValuePair<string, object>("@key", key));
            int rowsAffected = ExecuteNonQuery(query, parameters);
            bool affected = rowsAffected > 0;
            if (affected)
            {
                CacheManager.Instance.Insert(cachePrefix, key, value);
            }
            return affected;
        }

        public string GetPublisherTimeZoneStandardName()
        {
            return CacheManager.Instance.Get<string>(cachePrefix, "GetPublisherTimeZoneStandardName", GetPublisherTimeZoneStandardNameMethod, CacheManager.ONE_DAY);
        }

        public string GetSupplierTimeZoneStandardName(Guid supplierId)
        {
            return GetSupplierTimeZoneStandardNameMethod(supplierId);
        }

        public string GetTimeZoneStandardName(int timeZoneCode)
        {
            return CacheManager.Instance.Get<string>(cachePrefix, "GetTimeZoneStandardName." + timeZoneCode, GetTimeZoneStandardNameMethod, CacheManager.ONE_YEAR);
        }

        #endregion

        #region Private Methods

        private string GetTimeZoneStandardNameMethod(string timeZoneCodeKey)
        {
            string timeZoneCode = timeZoneCodeKey.Substring("GetTimeZoneStandardName.".Length);
            string query =
                @"select top 1
                StandardName
                from
                TimeZoneDefinition d WITH(NOLOCK)
                where 
                d.TimeZoneCode = @code";
            DataAccessLayer.ConfigurationSettings.SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_configurationsetting>.SqlParametersList();
            parameters.Add("@code", timeZoneCode);
            return (string)ExecuteScalar(query, parameters);
        }

        private string GetPublisherTimeZoneStandardNameMethod()
        {
            string query =
                @"select top 1
                StandardName
                from
                TimeZoneDefinition d with(nolock)
                where 
                d.TimeZoneCode =
                (select new_value from new_configurationsetting WITH(NOLOCK) where new_key = 'PublisherTimeZone')
                ";
            return (string)ExecuteScalar(query);
        }

        private string GetSupplierTimeZoneStandardNameMethod(Guid supplierId)
        {
            string query =
                @"select top 1
                StandardName
                from
                TimeZoneDefinition d WITH(NOLOCK)
                where 
                d.TimeZoneCode = 
                isnull(
                (select address1_utcoffset from account WITH(NOLOCK) where accountid = @supplierId)
                , (select new_value from new_configurationsetting WITH(NOLOCK) where new_key = 'PublisherTimeZone'))";
            DataAccessLayer.ConfigurationSettings.SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_configurationsetting>.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            return (string)ExecuteScalar(query, parameters); 
        }

        private string GetConfigurationSettingValueMethod(string key)
        {
            string query = string.Format(
                @"select new_value
                    from
                    new_configurationsetting with (nolock)
                    where
                    new_key = N'{0}' and deletionstatecode = 0", key);
            object valueObject;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    valueObject = command.ExecuteScalar();
                }
            }
            string retVal = string.Empty;
            if (valueObject != null && valueObject != DBNull.Value)
            {
                retVal = (string)valueObject;
            }
            return retVal;
        }

        #endregion
    }
}
