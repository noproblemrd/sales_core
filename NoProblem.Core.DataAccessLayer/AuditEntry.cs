﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AuditEntry : DalBase<DataModel.Xrm.new_auditentry>
    {
        #region Ctor
        public AuditEntry(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_auditentry");
        }
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_auditentry Retrieve(Guid id)
        {
            DataModel.Xrm.new_auditentry audit = XrmDataContext.new_auditentrynew_auditentries.FirstOrDefault(x => x.new_auditentryid == id);
            return audit;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_auditentry> GetAuditEntriesByDatesAndSupplier(DateTime fromDate, DateTime toDate, Guid supplierId)
        {
            IEnumerable<DataModel.Xrm.new_auditentry> retVal = from audit in XrmDataContext.new_auditentrynew_auditentries
                                                               where
                                                               audit.new_advertiserid.HasValue
                                                               &&
                                                               audit.new_advertiserid.Value == supplierId
                                                               orderby
                                                               audit.createdon descending
                                                               select
                                                               audit;
            retVal = from audit in retVal
                     where
                     audit.createdon.Value.CompareTo(fromDate) >= 0
                     &&
                     audit.createdon.Value.CompareTo(toDate) <= 0
                     select audit;
            return retVal;
        }
    }
}
