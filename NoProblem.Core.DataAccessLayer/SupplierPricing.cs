﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.SupplierPricing
//  File: SupplierPricing.cs
//  Description: DAL for SupplierPricing Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using NoProblem.Core.DataModel.Xrm;
using DML = NoProblem.Core.DataModel;
using System.Collections.Generic;
using NoProblem.Core.DataAccessLayer.Autonumber;
using System.Data.SqlClient;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class SupplierPricing : DalBase<DML.Xrm.new_supplierpricing>
    {
        private const string DateFormat = "{0:yyyy-MM-dd HH:mm:ss}";
        private const string tableName = "new_supplierpricing";

        #region Ctor
        public SupplierPricing(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache(tableName);
        }
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_supplierpricing Retrieve(Guid id)
        {
            DML.Xrm.new_supplierpricing supplierPricing = XrmDataContext.new_supplierpricings.FirstOrDefault(x => x.new_supplierpricingid == id);
            return supplierPricing;
        }

        public override IQueryable<new_supplierpricing> All
        {
            get { return XrmDataContext.new_supplierpricings; }
        }

        public override void Create(NoProblem.Core.DataModel.Xrm.new_supplierpricing entity)
        {
            Autonumber.AutonumberManager autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
            string next = autoNumber.GetNextNumber(tableName);
            entity.new_name = next;
            base.Create(entity);
        }

        public NoProblem.Core.DataModel.Xrm.new_supplierpricing GetLastSupplierPricing(Guid supplierId)
        {
            IEnumerable<DML.Xrm.new_supplierpricing> thisSupplierPricings =
                from p in XrmDataContext.new_supplierpricings
                where
                p.new_accountid == supplierId
                select
                p;
            DML.Xrm.new_supplierpricing lastSupplierPricing = null;
            foreach (DML.Xrm.new_supplierpricing pricing in thisSupplierPricings)
            {
                if (lastSupplierPricing == null)
                {
                    lastSupplierPricing = pricing;
                }
                else if (lastSupplierPricing.createdon.Value.CompareTo(pricing.createdon.Value) < 0)
                {
                    lastSupplierPricing = pricing;
                }
            }
            return lastSupplierPricing;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_supplierpricing> GetSupplierPricingsByDates(DateTime fromDate, DateTime toDate)
        {
            IEnumerable<DML.Xrm.new_supplierpricing> retVal = from pri in XrmDataContext.new_supplierpricings
                                                              where
                                                              pri.new_accountid.HasValue
                                                              &&
                                                              pri.createdon.Value >= fromDate
                                                              &&
                                                              pri.createdon.Value < toDate
                                                              select pri;
            return retVal;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_supplierpricing> GetAllSupplierPricings()
        {
            IEnumerable<DML.Xrm.new_supplierpricing> retVal = from pri in XrmDataContext.new_supplierpricings
                                                              where
                                                              pri.new_accountid.HasValue
                                                              select pri;
            return retVal;
        }

        public NoProblem.Core.DataModel.Xrm.new_supplierpricing GetByTransactionIdAndChargingCompany(string transactionId, string chargingCompany)
        {
            IEnumerable<DML.Xrm.new_supplierpricing> pricings = from pric in XrmDataContext.new_supplierpricings
                                                                where
                                                                pric.new_transactionid == transactionId
                                                                &&
                                                                pric.new_chargingcompany == chargingCompany
                                                                select
                                                                pric;
            return pricings.FirstOrDefault();
        }

        public bool IsFirstDeposit(Guid supplierId)
        {
            string query =
                @"select top 1 1 from new_supplierpricing with(nolock)
                    where deletionstatecode = 0 and new_accountid = @supplierId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            object scalar = ExecuteScalar(query, parameters);
            bool retVal = true;
            if (scalar != null)
            {
                retVal = false;
            }
            return retVal;
        }

        public List<DataModel.Reports.DepositData> DepositsReportQuery(DateTime fromDate, DateTime toDate, Guid expertiseId, DML.Reports.DepositReportRequest.eDepositType type, DML.Reports.DepositReportRequest.eDepositCreatedBy createdBy)
        {
            List<DataModel.Reports.DepositData> dataList = new List<NoProblem.Core.DataModel.Reports.DepositData>();
            using (SqlCommand command = new SqlCommand())
            {
                StringBuilder query = new StringBuilder(
                    @"select sp.new_total, sp.new_credit, sp.new_extrabonus, new_voucherid, sp.createdon 
                from new_supplierpricing sp with(nolock)  ");
                if (expertiseId != Guid.Empty)
                {
                    query.Append(@"join new_accountexpertise ae with(nolock) on ae.new_accountid = sp.new_accountid ");
                }
                if (createdBy == NoProblem.Core.DataModel.Reports.DepositReportRequest.eDepositCreatedBy.Advertiser
                    || createdBy == NoProblem.Core.DataModel.Reports.DepositReportRequest.eDepositCreatedBy.Publisher)
                {
                    query.Append(@"join account acc with(nolock) on acc.accountid = sp.new_userid ");
                }
                query.Append(@"where sp.deletionstatecode = 0
                and sp.createdon between @from and @to ");
                if (expertiseId != Guid.Empty)
                {
                    query.Append(@"and ae.deletionstatecode = 0 and ae.statecode = 0
                    and ae.new_primaryexpertiseid = @expertiseId ");
                    command.Parameters.AddWithValue("@expertiseId", expertiseId);
                }
                if (type != DML.Reports.DepositReportRequest.eDepositType.All)
                {
                    query.Append(@"and sp.new_isfirstdeposit = @type ");
                    command.Parameters.AddWithValue("@type", (int)type);
                }
                if (createdBy == NoProblem.Core.DataModel.Reports.DepositReportRequest.eDepositCreatedBy.Advertiser)
                {
                    query.Append(@"and (acc.new_securitylevel is null or acc.new_securitylevel = 0) ");
                }
                else if (createdBy == NoProblem.Core.DataModel.Reports.DepositReportRequest.eDepositCreatedBy.Publisher)
                {
                    query.Append(@"and acc.new_securitylevel > 0 ");
                }
                else if (createdBy == NoProblem.Core.DataModel.Reports.DepositReportRequest.eDepositCreatedBy.Auto)
                {
                    query.Append(@"and sp.new_isautorecharge = 1 ");
                }
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = query.ToString();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Reports.DepositData data = new NoProblem.Core.DataModel.Reports.DepositData();
                            bool isVoucher = reader["new_voucherid"] != DBNull.Value;
                            if (isVoucher)
                            {
                                data.Amount = 0;
                                data.VirtualMoney = (int)reader["new_total"];
                            }
                            else
                            {
                                data.Amount = (int)reader["new_total"];
                                data.VirtualMoney = 0;
                            }
                            int credit = reader["new_credit"] != DBNull.Value ? (int)reader["new_credit"] : 0;
                            int extraBonus = reader["new_extrabonus"] != DBNull.Value ? (int)reader["new_extrabonus"] : 0;
                            data.Credit = credit + extraBonus;
                            data.CreatedOn = (DateTime)reader["createdon"];
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }

        public void UpdateInvoiceData(Guid supplierPricingId, string invoicePdfLink, string invoiceNumber, string invoiceNumberRefund)
        {
            if (invoiceNumber == null)
                invoiceNumber = string.Empty;
            if (invoiceNumberRefund == null)
                invoiceNumberRefund = string.Empty;
            if (invoicePdfLink == null)
                invoicePdfLink = string.Empty;
            string sqlString =
            @"update new_supplierpricing
            set new_invoicepdflink = @link,
            new_invoicenumber = @invoiceNumber,
            new_invoicenumberrefund = @invoiceNumberRefund
            where new_supplierpricingid = @id";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@link", invoicePdfLink));
            parameters.Add(new KeyValuePair<string, object>("@invoiceNumber", invoiceNumber));
            parameters.Add(new KeyValuePair<string, object>("@invoiceNumberRefund", invoiceNumberRefund));
            parameters.Add(new KeyValuePair<string, object>("@id", supplierPricingId));
            ExecuteNonQuery(sqlString, parameters);
        }

        public List<NoProblem.Core.DataModel.SupplierPricing.PricingRowData> GetSupplierPricingHistory(Guid supplierId, string pricingNumber, int pageNumber, int pageSize, out int totalRows)
        {
            totalRows = 0;
            string query =
                @"WITH PagerTbl AS
                (                
                SELECT ROW_NUMBER() OVER(ORDER BY sp.createdon desc) AS RowNum,
                sp.new_name, sp.createdon, sp.new_total, sp.new_credit, sp.new_extrabonus, pm.new_paymentmethodcode, sp.new_supplierpricingid
                ,sp.new_invoicenumber, sp.new_invoicenumberrefund, sp.new_invoicepdflink
                from
                new_supplierpricing sp with(nolock)
                join new_paymentmethod pm with(nolock) on pm.new_paymentmethodid = sp.new_paymentmethodid
                where
                sp.deletionstatecode = 0
                and sp.new_accountid = @supplierId";
            if (string.IsNullOrEmpty(pricingNumber) == false)
            {
                query += " and sp.new_name like @pricingNumber ";
            }
            query += @" )
                select *, (select max(RowNum) from PagerTbl) as totalRows
                from  PagerTbl
                where 
                    ((RowNum BETWEEN (@pageNum - 1) * @pageSize + 1 AND @pageNum * @pageSize)
                    or @pageSize = -1)
                ORDER BY RowNum";
            List<NoProblem.Core.DataModel.SupplierPricing.PricingRowData> retVal = new List<NoProblem.Core.DataModel.SupplierPricing.PricingRowData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Parameters.AddWithValue("@supplierId", supplierId);
                    command.Parameters.AddWithValue("@pageNum", pageNumber);
                    command.Parameters.AddWithValue("@pageSize", pageSize);
                    if (string.IsNullOrEmpty(pricingNumber) == false)
                    {
                        command.Parameters.AddWithValue("@pricingNumber", "%" + pricingNumber + "%");
                    }
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.SupplierPricing.PricingRowData data = new NoProblem.Core.DataModel.SupplierPricing.PricingRowData();
                            data.Amount = (int)reader["new_total"];
                            int bonus = reader["new_credit"] != DBNull.Value ? (int)reader["new_credit"] : 0;
                            int extraBonus = reader["new_extrabonus"] != DBNull.Value ? (int)reader["new_extrabonus"] : 0;
                            data.Total = data.Amount + bonus + extraBonus;
                            data.CreatedOn = (DateTime)reader["createdon"];
                            data.Number = reader["new_name"] != DBNull.Value ? (string)reader["new_name"] : "***";
                            data.PaymentMethod = (DataModel.Xrm.new_paymentmethod.ePaymentMethod)((int)reader["new_paymentmethodcode"]);
                            data.InvoiceNumber = reader["new_invoicenumber"] != DBNull.Value ? (string)reader["new_invoicenumber"] : string.Empty;
                            data.InvoiceNumberRefund = reader["new_invoicenumberrefund"] != DBNull.Value ? (string)reader["new_invoicenumberrefund"] : string.Empty;
                            data.InvoicePdfLink = reader["new_invoicepdflink"] != DBNull.Value ? (string)reader["new_invoicepdflink"] : string.Empty;
                            data.SupplierPricingId = (Guid)reader["new_supplierpricingid"];
                            retVal.Add(data);
                            if (totalRows == 0)
                            {
                                totalRows = unchecked((int)((long)reader["totalRows"]));
                            }
                        }
                    }
                }
            }
            return retVal;
        }

        //public CreditCardInfo FindCreditCardInfo(Guid supplierId)
        //{
        //    string ccToken, cvv2, ccOwnerId, ccOwnerName, last4digits, chargingCompanyAccountNumber, chargingCompanyContractId, ccType, ccExpDate, billingAddress, ccOwnerPhone, ccOwnerEmail;
        //    DateTime tokenDate;
        //    bool found = FindCreditCardInfo(supplierId, out ccToken, out cvv2, out ccOwnerId, out ccOwnerName, out last4digits, out tokenDate, out chargingCompanyContractId, out chargingCompanyAccountNumber, out ccType, out ccExpDate, out billingAddress, out ccOwnerEmail, out ccOwnerPhone);
        //    if (!found)
        //        return null;
        //    CreditCardInfo retVal = new CreditCardInfo()
        //    {
        //        billingAddress = billingAddress,
        //        ccExpDate = ccExpDate,
        //        ccOwnerId = ccOwnerId,
        //        ccOwnerName = ccOwnerName,
        //        tokenDate = tokenDate,
        //        ccToken = ccToken,
        //        ccType = ccType,
        //        chargingCompanyAccountNumber = chargingCompanyAccountNumber,
        //        chargingCompanyContractId = chargingCompanyContractId,
        //        cvv2 = cvv2,
        //        last4digits = last4digits,
        //        ccOwnerEmail = ccOwnerEmail,
        //        ccOwnerPhone = ccOwnerPhone,
        //        BillingCity = 
        //    };
        //    return retVal;
        //}

        public class CreditCardInfo
        {
            public string ccToken { get; set; }
            public string cvv2 { get; set; }
            public string ccOwnerId { get; set; }
            public string ccOwnerName { get; set; }
            public string last4digits { get; set; }
            public DateTime tokenDate { get; set; }
            public string chargingCompanyContractId { get; set; }
            public string chargingCompanyAccountNumber { get; set; }
            public string ccType { get; set; }
            public string ccExpDate { get; set; }
            public string billingAddress { get; set; }
            public string ccOwnerEmail { get; set; }
            public string ccOwnerPhone { get; set; }
            public string BillingCity { get; set; }
            public string BillingState { get; set; }
            public string BillingZip { get; set; }
        }


        public CreditCardInfo FindCreditCardInfo(Guid supplierId)
        {
            /*
            var supplierPricingRespository = new SupplierPricing(XrmDataContext);

            //(new_chargingcompanyaccountnumber is not null and new_chargingcompanycontractid is not null and new_creditcardtype is not null and New_Last4DigitsCC is not null
            new_supplierpricing pricing = supplierPricingRespository.All.OrderByDescending(x => x.createdon).FirstOrDefault(x => (x.new_creditcardtoken != null) || (x.new_chargingcompanycontractid != null && x.new_creditcardtype != null && x.new_last4digitscc != null));
            if (pricing == null)
                return null;
            var creditCard = new CreditCardInfo
            {
                BillingCity = pricing.new_billingaddress,
                BillingState = pricing.new_billingstate,
                BillingZip = pricing.new_billingzip,
                billingAddress = pricing.new_billingaddress,
                ccExpDate = pricing.new_creditcardexpdate,
                ccOwnerEmail = pricing.new_billingemail,
                ccOwnerId = pricing.ownerid.Value.ToString(),
                ccOwnerName = pricing.new_creditcardownername,
                ccOwnerPhone = pricing.new_billingphone,
                ccToken = pricing.new_creditcardtoken,
                ccType = pricing.new_creditcardtype,
                chargingCompanyAccountNumber = pricing.new_chargingcompanyaccountnumber,
                chargingCompanyContractId = pricing.new_chargingcompanycontractid,
                cvv2 = pricing.new_cvv2,
                last4digits = pricing.new_last4digitscc,
                tokenDate = pricing.createdon.Value
            };
            */
            CreditCardInfo creditCard = null;
            string command = @"
SELECT TOP 1 new_billingaddress, new_billingstate, new_billingzip, new_billingcity, new_creditcardexpdate,
new_billingemail, ownerid, new_creditcardownername, new_billingphone, new_creditcardtoken, new_creditcardtype,
new_chargingcompanyaccountnumber, new_chargingcompanycontractid, new_cvv2, new_last4digitscc, createdon
FROM [dbo].[New_supplierpricing]
WHERE new_accountid = @accountId
ORDER BY CreatedOn desc";
            using(SqlConnection conn = new SqlConnection(Generic.connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(command,conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@accountId", supplierId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if(reader.Read())
                    {
                        creditCard = new CreditCardInfo();
                        creditCard.BillingCity = reader["new_billingcity"] == DBNull.Value ? string.Empty : (string)reader["new_billingcity"];
                        creditCard.BillingState =  reader["new_billingstate"] == DBNull.Value ? string.Empty : (string)reader["new_billingstate"];
                        creditCard.BillingZip =  reader["new_billingzip"] == DBNull.Value ? string.Empty : (string)reader["new_billingzip"];
                        creditCard.billingAddress =  reader["new_billingaddress"] == DBNull.Value ? string.Empty : (string)reader["new_billingaddress"];
                        creditCard.ccExpDate = reader["new_creditcardexpdate"] == DBNull.Value ? string.Empty : (string)reader["new_creditcardexpdate"];
                        creditCard.ccOwnerEmail = reader["new_billingemail"] == DBNull.Value ? string.Empty : (string)reader["new_billingemail"];
                        creditCard.ccOwnerId = ((Guid)reader["ownerid"]).ToString();
                        creditCard.ccOwnerName = reader["new_creditcardownername"] == DBNull.Value ? string.Empty : (string)reader["new_creditcardownername"];
                        creditCard.ccOwnerPhone = reader["new_billingphone"] == DBNull.Value ? string.Empty : (string)reader["new_billingphone"];
                        creditCard.ccToken = reader["new_creditcardtoken"] == DBNull.Value ? string.Empty : (string)reader["new_creditcardtoken"];
                        creditCard.ccType = reader["new_creditcardtype"] == DBNull.Value ? string.Empty : (string)reader["new_creditcardtype"];
                        creditCard.chargingCompanyAccountNumber = reader["new_chargingcompanyaccountnumber"] == DBNull.Value ? string.Empty : (string)reader["new_chargingcompanyaccountnumber"];
                        creditCard.chargingCompanyContractId = reader["new_chargingcompanycontractid"] == DBNull.Value ? string.Empty : (string)reader["new_chargingcompanycontractid"];
                        creditCard.cvv2 = reader["new_cvv2"] == DBNull.Value ? string.Empty : (string)reader["new_cvv2"];
                        creditCard.last4digits = reader["new_last4digitscc"] == DBNull.Value ? string.Empty : (string)reader["new_last4digitscc"];
                        creditCard.tokenDate = (DateTime)reader["createdon"];
                    }
                    conn.Close();
                }
            }
            return creditCard;

        }

        public bool FindCreditCardInfo(Guid supplierId, out string ccToken, out string cvv2, out string ccOwnerId, out string ccOwnerName, out string last4digits, out DateTime tokenDate, out string chargingCompanyContractId, out string chargingCompanyAccountNumber, out string ccType, out string ccExpDate, out string billingAddress)
        {
            string ccOwnerPhone, ccOwnerEmail;
            return FindCreditCardInfo(supplierId, out ccToken, out cvv2, out ccOwnerId, out ccOwnerName, out last4digits, out tokenDate, out chargingCompanyContractId, out chargingCompanyAccountNumber, out ccType, out ccExpDate, out billingAddress, out ccOwnerEmail, out ccOwnerPhone);
        }

        public bool FindCreditCardInfo(Guid supplierId, out string ccToken, out string cvv2, out string ccOwnerId, out string ccOwnerName, out string last4digits, out DateTime tokenDate, out string chargingCompanyContractId, out string chargingCompanyAccountNumber, out string ccType, out string ccExpDate, out string billingAddress, out string ccOwnerEmail, out string ccOwnerPhone)
        {
            bool retVal = false;
            ccToken = string.Empty;
            cvv2 = string.Empty;
            ccOwnerId = string.Empty;
            ccOwnerName = string.Empty;
            last4digits = string.Empty;
            tokenDate = DateTime.Now;
            chargingCompanyAccountNumber = string.Empty;
            chargingCompanyContractId = string.Empty;
            ccType = string.Empty;
            ccExpDate = string.Empty;
            billingAddress = string.Empty;
            ccOwnerEmail = string.Empty;
            ccOwnerPhone = string.Empty;
            string query =
                @"select top 1
                createdon,
                New_Last4DigitsCC,
                New_CreditCardOwnerId,
                New_CreditCardOwnerName,
                New_CreditCardToken,
                New_CVV2,
                new_chargingcompanyaccountnumber,
                new_chargingcompanycontractid,
                new_creditcardtype,
                new_creditcardexpdate,
                new_billingaddress,
                New_BillingEmail, 
                New_BillingPhone,
                new_billingState,
                new_billingCity
                new_billingZip
                from
                New_supplierpricing with(nolock)
                where DeletionStateCode = 0
                and (New_CreditCardToken is not null or (new_chargingcompanyaccountnumber is not null and new_chargingcompanycontractid is not null and new_creditcardtype is not null and New_Last4DigitsCC is not null))
                and new_accountid = @accountId
                order by CreatedOn desc";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@accountId", supplierId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            retVal = true;
                            ccToken = reader["New_CreditCardToken"] != DBNull.Value ? (string)reader["New_CreditCardToken"] : string.Empty;
                            cvv2 = reader["New_CVV2"] != DBNull.Value ? (string)reader["New_CVV2"] : string.Empty;
                            ccOwnerId = reader["New_CreditCardOwnerId"] != DBNull.Value ? (string)reader["New_CreditCardOwnerId"] : string.Empty;
                            ccOwnerName = reader["New_CreditCardOwnerName"] != DBNull.Value ? (string)reader["New_CreditCardOwnerName"] : string.Empty;
                            last4digits = reader["New_Last4DigitsCC"] != DBNull.Value ? (string)reader["New_Last4DigitsCC"] : string.Empty;
                            tokenDate = (DateTime)reader["createdon"];
                            chargingCompanyAccountNumber = reader["new_chargingcompanyaccountnumber"] != DBNull.Value ? (string)reader["new_chargingcompanyaccountnumber"] : string.Empty;
                            chargingCompanyContractId = reader["new_chargingcompanycontractid"] != DBNull.Value ? (string)reader["new_chargingcompanycontractid"] : string.Empty;
                            ccType = reader["new_creditcardtype"] != DBNull.Value ? (string)reader["new_creditcardtype"] : string.Empty;
                            ccExpDate = Convert.ToString(reader["new_creditcardexpdate"]);
                            billingAddress = Convert.ToString(reader["new_billingaddress"]);
                            ccOwnerPhone = Convert.ToString(reader["New_BillingPhone"]);
                            ccOwnerEmail = Convert.ToString(reader["New_BillingEmail"]);
                        }
                    }
                }
            }
            return retVal;
        }

        public NoProblem.Core.DataModel.SupplierPricing.GetPricingDataResponse GetPricingData(Guid supplierPricingId)
        {
            string query =
                @"select top 1
                sp.new_supplierpricingid,
                sp.new_total,
                sp.new_name,
                sp.new_bonustype,
                sp.new_credit,
                sp.new_extrabonus,
                sp.new_extrabonusreasonid,
                pm.new_paymentmethodcode,
                sp.new_voucherreasonid,
                sp.new_voucheridname,
                sp.new_useridname,
                sp.createdon,
                sp.new_bank,
                sp.new_bankbranch,
                sp.new_bankaccount,
                acc.new_cashbalance,
                sp.new_numberofpayments,
                sp.new_invoicenumber,
                sp.new_invoicenumberrefund,
                sp.new_invoicepdflink,
                sp.new_checkdate,
                sp.new_checknumber,
                sp.new_transferdate
                from New_supplierpricing sp with(nolock)
                join new_paymentmethod pm with(nolock) on pm.new_paymentmethodid = sp.new_paymentmethodid
                join account acc with(nolock) on acc.accountid = sp.new_accountid
                where sp.new_supplierpricingid = @spId";
            DataModel.SupplierPricing.GetPricingDataResponse response = new NoProblem.Core.DataModel.SupplierPricing.GetPricingDataResponse();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@spId", supplierPricingId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            response.Amount = (int)reader["new_total"];
                            response.Bank = reader["new_bank"] != DBNull.Value ? (string)reader["new_bank"] : string.Empty;
                            response.BankAccount = reader["new_bankaccount"] != DBNull.Value ? (string)reader["new_bankaccount"] : string.Empty;
                            response.BankBranch = reader["new_bankbranch"] != DBNull.Value ? (string)reader["new_bankbranch"] : string.Empty;
                            response.CreatedByUserName = reader["new_useridname"] != DBNull.Value ? (string)reader["new_useridname"] : string.Empty;
                            response.CreatedOn = (DateTime)reader["createdon"];
                            response.ExtraBonus = reader["new_extrabonus"] != DBNull.Value ? (int)reader["new_extrabonus"] : 0;
                            response.ExtraBonusReasonId = reader["new_extrabonusreasonid"] != DBNull.Value ? (Guid)reader["new_extrabonusreasonid"] : Guid.Empty;
                            response.Number = reader["new_name"] != DBNull.Value ? (string)reader["new_name"] : "***";
                            response.Bonus = reader["new_credit"] != DBNull.Value ? (int)reader["new_credit"] : 0;
                            response.BonusType = reader["new_bonustype"] != DBNull.Value ? (DML.Xrm.new_supplierpricing.BonusType)((int)reader["new_bonustype"]) : DML.Xrm.new_supplierpricing.BonusType.Package;
                            response.PaymentMethod = (DataModel.Xrm.new_paymentmethod.ePaymentMethod)((int)reader["new_paymentmethodcode"]);
                            response.SupplierPricingId = supplierPricingId;
                            response.VoucherReasonId = reader["new_voucherreasonid"] != DBNull.Value ? (Guid)reader["new_voucherreasonid"] : Guid.Empty;
                            response.VoucherNumber = reader["new_voucheridname"] != DBNull.Value ? (string)reader["new_voucheridname"] : string.Empty;
                            response.SupplierBalance = (decimal)reader["new_cashbalance"];
                            response.NumberOfPayments = reader["new_numberofpayments"] != DBNull.Value ? (int)reader["new_numberofpayments"] : -1;
                            response.InvoiceNumber = reader["new_invoicenumber"] != DBNull.Value ? (string)reader["new_invoicenumber"] : string.Empty;
                            response.InvoiceNumberRefund = reader["new_invoicenumberrefund"] != DBNull.Value ? (string)reader["new_invoicenumberrefund"] : string.Empty;
                            response.InvoicePdfLink = reader["new_invoicepdflink"] != DBNull.Value ? (string)reader["new_invoicepdflink"] : string.Empty;
                            if (reader["new_transferdate"] != DBNull.Value)
                            {
                                response.TransferDate = (DateTime)reader["new_transferdate"];
                            }
                            if (reader["new_checkdate"] != DBNull.Value)
                            {
                                response.CheckDate = (DateTime)reader["new_checkdate"];
                            }
                            response.CheckNumber = reader["new_checknumber"] != DBNull.Value ? (string)reader["new_checknumber"] : string.Empty;
                        }
                    }
                }
            }
            return response;
        }

        /// <summary>
        /// Check if the invoice belongs to the supplier and also if it is a positive deposit invoice.
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public bool IsInvoiceNumberOfSupplier(string invoiceNumber, Guid supplierId)
        {
            string query =
                @"select top 1 new_invoicenumber from
                    new_supplierpricing with(nolock)
                    where deletionstatecode = 0
                    and new_accountid = @supplierId
                    and new_invoicenumber = @invoiceNumber
                    and new_total > 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            parameters.Add(new KeyValuePair<string, object>("@invoiceNumber", invoiceNumber));
            object scalar = ExecuteScalar(query, parameters);
            bool retVal = false;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = true;
            }
            return retVal;
        }

        public List<NoProblem.Core.DataModel.Reports.SalesmanDepositData> GetDepositsByAccountManager(Guid salesmanId, DateTime from, DateTime to)
        {
            List<DML.Reports.SalesmanDepositData> retVal = new List<NoProblem.Core.DataModel.Reports.SalesmanDepositData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    string query =
                        @"select sp.CreatedOn as 'DepositCreatedOn', sp.new_total
                , sp.New_IsFirstDeposit, acc.Name, acc.AccountNumber
                , acc.New_ApprovedOn, acc.new_bizid, new_rechargetypecode
                , sp.new_paymentmethodidname
                , isnull(forCalls, 0) as forCalls
                , New_total - isnull(forCalls, 0) as notForCalls
                from New_supplierpricing sp with(nolock)
                join Account acc with(nolock) on acc.AccountId = sp.new_accountid
                left join (
					select sum(new_CurrentPaymentAmount) forCalls, new_supplierpricingid
					from New_paymentcomponent
					where CreatedOn between @from and @to
					and New_Type !=  1
					and DeletionStateCode = 0
					group by new_supplierpricingid
                ) compsForCalls on compsForCalls.new_supplierpricingid = sp.New_supplierpricingId
                where sp.DeletionStateCode = 0 and acc.DeletionStateCode = 0                
                and sp.createdon between @from and @to
                and sp.new_voucherid is null ";
                    if (salesmanId != Guid.Empty)
                    {
                        query += @" and acc.new_managerid = @managerId ";
                        command.Parameters.AddWithValue("@managerId", salesmanId);
                    }
                    else
                    {
                        query += @" and acc.new_managerid is null ";
                    }

                    command.Parameters.AddWithValue("@from", from);
                    command.Parameters.AddWithValue("@to", to);
                    command.CommandText = query;
                    command.Connection = conn;
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.Reports.SalesmanDepositData data = new NoProblem.Core.DataModel.Reports.SalesmanDepositData();
                            data.DepositCreatedOn = (DateTime)reader["DepositCreatedOn"];
                            data.MoneyAmount = (int)reader["new_total"];
                            data.IsFirstDeposit = reader["New_IsFirstDeposit"] != DBNull.Value ? (bool)reader["New_IsFirstDeposit"] : false;
                            data.AccountName = reader["name"] != DBNull.Value ? (string)reader["name"] : "";
                            data.AccountNumber = reader["AccountNumber"] != DBNull.Value ? (string)reader["AccountNumber"] : "";
                            data.AccountApprovedOn = reader["New_ApprovedOn"] != DBNull.Value ? new DateTime?((DateTime)reader["New_ApprovedOn"]) : null;
                            data.BizId = Convert.ToString(reader["new_bizid"]);
                            data.RechargeType = GetRechargeType(reader["new_rechargetypecode"]);
                            data.PaymentMethod = Convert.ToString(reader["new_paymentmethodidname"]);
                            data.AmountForCalls = Convert.ToDecimal(reader["forCalls"]);
                            data.AmountNotForCalls = Convert.ToDecimal(reader["notForCalls"]);
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        private string GetRechargeType(object p)
        {
            string retVal = null;
            if (p != DBNull.Value)
            {
                retVal = ((DataModel.Xrm.account.RechargeTypeCode)((int)p)).ToString();
            }
            return retVal;
        }

        public void GetAllSuppliersMoneySummaryReportData(out int realMoneyDeposited, out int bonusMoneyDeposited, out int realUsed, out int bonusUsed, out int realLeft, out int bonusLeft)
        {
            realMoneyDeposited = 0;
            bonusMoneyDeposited = 0;
            realUsed = 0;
            bonusUsed = 0;
            realLeft = 0;
            bonusLeft = 0;
            string query =
                @"
declare @realDepositTotal int;
declare @bonusDepositTotal int;
declare @realUsedTotal int;
declare @bonusUsedTotal int;

set @realDepositTotal = 0;
set @bonusDepositTotal = 0;
set @realUsedTotal = 0;
set @bonusUsedTotal = 0;

select accountid 
into #tmpAccount
from account with(nolock)
where deletionstatecode = 0
and (new_securitylevel is null or new_securitylevel = 0) 
and new_affiliateoriginid is null

DECLARE AccountIterator CURSOR FOR
select accountid
from #tmpAccount

DECLARE @accountId uniqueidentifier

OPEN AccountIterator
FETCH AccountIterator INTO @accountId
WHILE @@FETCH_Status = 0
BEGIN

declare @realMoney int
declare @bonusMoney int

select @realMoney = sum(case when new_total is null then 0 else New_total end)
, @bonusMoney = sum(
case when new_credit is null then 0 else new_credit end +
case when new_extrabonus is null then 0 else new_extrabonus end)
from
new_supplierpricing with(nolock)
where deletionstatecode = 0
and new_accountid = @accountId
and new_voucherid is null

declare @voucherMoney int

select @voucherMoney = sum(
case when new_total is null then 0 else New_total end + 
case when new_credit is null then 0 else new_credit end + 
case when new_extrabonus is null then 0 else new_extrabonus end)
from
new_supplierpricing with(nolock)
where deletionstatecode = 0
and new_accountid = @accountId
and new_voucherid is not null

declare @usedMoney int

select @usedMoney = sum(case when  new_potentialincidentprice is null then 0 else new_potentialincidentprice end) from
new_incidentaccount with(nolock)
where deletionstatecode = 0
and new_tocharge = 1
and new_accountid = @accountId

if @realMoney is null
begin set @realMoney = 0 end
if @bonusMoney is null
begin set @bonusMoney = 0 end
if @voucherMoney is null
begin set @voucherMoney = 0 end
if @usedMoney is null
begin set @usedMoney = 0 end

declare @allBonus int
set @allBonus = @bonusMoney + @voucherMoney;

declare @usedReal int;
declare @usedBonus int;
declare @dif int;
SET @dif = @realMoney - @usedMoney;

if @dif < 0
BEGIN
	SET @usedReal = @realMoney;
	SET @usedBonus = @dif * -1;
END
ELSE
BEGIN
	SET @usedReal = @usedMoney;
	SET @usedBonus = 0;
END

set @realDepositTotal = @realDepositTotal + @realMoney;
set @bonusDepositTotal = @bonusDepositTotal + @allBonus;
set @realUsedTotal = @realUsedTotal + @usedReal;
set @bonusUsedTotal = @bonusUsedTotal + @usedBonus;

FETCH AccountIterator INTO @accountId
END
CLOSE AccountIterator
DEALLOCATE AccountIterator

drop table #tmpAccount

select 
@realDepositTotal as 'realDepositTotal'
, @bonusDepositTotal as 'bonusDepositTotal'
, @realUsedTotal as 'realUsedTotal'
, @bonusUsedTotal as 'bonusUsedTotal'
, @realDepositTotal - @realUsedTotal as 'realLeft'
, @bonusDepositTotal - @bonusUsedTotal as 'bonusLeft'";
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                realMoneyDeposited = (int)row["realDepositTotal"];
                bonusMoneyDeposited = (int)row["bonusDepositTotal"];
                realUsed = (int)row["realUsedTotal"];
                bonusUsed = (int)row["bonusUsedTotal"];
                realLeft = (int)row["realLeft"];
                bonusLeft = (int)row["bonusLeft"];
            }
        }

        public void GetSupplierMoneyReportData(string supplierNumber, out int realMoneyDeposited, out int bonusMoneyDeposited, out int usedMoney, out Guid supplierId, out string supplierName)
        {
            realMoneyDeposited = 0;
            bonusMoneyDeposited = 0;
            usedMoney = 0;
            supplierId = Guid.Empty;
            supplierName = null;
            string query =
                @"
declare @realMoney int
declare @bonusMoney int
declare @supplierId uniqueidentifier
declare @supplierName nvarchar(100)

select @supplierId = accountid, @supplierName = name
from account with(nolock)
where deletionstatecode = 0
and accountnumber = @supplierNumber

select @realMoney = sum(case when new_total is null then 0 else New_total end), 
@bonusMoney = sum(
case when new_credit IS NULL then 0 else new_credit end
+ 
case when new_extrabonus is null then 0 else new_extrabonus end)
from
new_supplierpricing with(nolock)
where deletionstatecode = 0
and new_accountid = @supplierId
and new_voucherid is null

declare @voucherMoney int

select @voucherMoney = sum(
case when new_total is null then 0 else New_total end + 
case when new_credit IS NULL then 0 else new_credit end + 
case when new_extrabonus is null then 0 else new_extrabonus end)
from
new_supplierpricing with(nolock)
where deletionstatecode = 0
and new_accountid = @supplierId
and new_voucherid is not null

declare @usedMoney int

select @usedMoney = sum(case when new_potentialincidentprice is null then 0 else new_potentialincidentprice end) from
new_incidentaccount with(nolock)
where deletionstatecode = 0
and new_tocharge = 1
and new_accountid = @supplierId

if @realMoney is null
begin set @realMoney = 0 end
if @bonusMoney is null
begin set @bonusMoney = 0 end
if @voucherMoney is null
begin set @voucherMoney = 0 end
if @usedMoney is null
begin set @usedMoney = 0 end

declare @allBonus int
set @allBonus = @bonusMoney + @voucherMoney;

select 
@realMoney as 'real deposited'
,  @allBonus as 'bonus deposited'
, @usedMoney as 'used'
, @supplierId as 'supplierId', @supplierName as 'supplierName'
";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierNumber", supplierNumber));
            var reader = ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                realMoneyDeposited = (int)row["real deposited"];
                bonusMoneyDeposited = (int)row["bonus deposited"];
                usedMoney = (int)row["used"];
                supplierName = row["supplierName"] != DBNull.Value ? (string)row["supplierName"] : string.Empty;
                supplierId = row["supplierId"] != DBNull.Value ? (Guid)row["supplierId"] : Guid.Empty;
            }
        }

        public List<NoProblem.Core.DataModel.Reports.SupplierMoneyReportResponse> GetSupplierMoneyDataForExcel()
        {
            string query =
                @"create table #results
(realDeposit int, bonusDeposit int, realUsed int, bonusUsed int, realLeft int, bonusLeft int, supplierId uniqueidentifier, supplierName nvarchar(64), supplierNumber nvarchar(16))

select accountid, accountNumber, name
into #tmpAccount
from account with(nolock)
where deletionstatecode = 0
and (new_securitylevel is null or new_securitylevel = 0) 
and new_affiliateoriginid is null

DECLARE AccountIterator CURSOR FOR
select accountid, accountNumber, name
from #tmpAccount

DECLARE @accountId uniqueidentifier
DECLARE @accountNumber nvarchar(16)
DECLARE @accountName nvarchar(64)

OPEN AccountIterator
FETCH AccountIterator INTO @accountId, @accountNumber, @accountName
WHILE @@FETCH_Status = 0
BEGIN

declare @realMoney int
declare @bonusMoney int

select @realMoney = sum(case when new_total is null then 0 else New_total end)
, @bonusMoney = sum(
case when new_credit is null then 0 else new_credit end + 
case when new_extrabonus is null then 0 else new_extrabonus end)
from
new_supplierpricing with(nolock)
where deletionstatecode = 0
and new_accountid = @accountId
and new_voucherid is null

declare @voucherMoney int

select @voucherMoney = sum(
case when new_total is null then 0 else New_total end + 
case when new_credit is null then 0 else new_credit end +  
case when new_extrabonus is null then 0 else new_extrabonus end)
from
new_supplierpricing with(nolock)
where deletionstatecode = 0
and new_accountid = @accountId
and new_voucherid is not null

declare @usedMoney int

select @usedMoney = sum(case when new_potentialincidentprice IS null then 0 else new_potentialincidentprice end) from
new_incidentaccount with(nolock)
where deletionstatecode = 0
and new_tocharge = 1
and new_accountid = @accountId

if @realMoney is null
begin set @realMoney = 0 end
if @bonusMoney is null
begin set @bonusMoney = 0 end
if @voucherMoney is null
begin set @voucherMoney = 0 end
if @usedMoney is null
begin set @usedMoney = 0 end

declare @allBonus int
set @allBonus = @bonusMoney + @voucherMoney;

declare @usedReal int;
declare @usedBonus int;
declare @dif int;
SET @dif = @realMoney - @usedMoney;

if @dif < 0
BEGIN
	SET @usedReal = @realMoney;
	SET @usedBonus = @dif * -1;
END
ELSE
BEGIN
	SET @usedReal = @usedMoney;
	SET @usedBonus = 0;
END

INSERT INTO #results (realDeposit, bonusDeposit, realUsed, bonusUsed, realLeft, bonusLeft, supplierId, supplierName, supplierNumber)
VALUES (@realMoney, @allBonus, @usedReal, @usedBonus, @realMoney - @usedReal, @allBonus - @usedBonus, @accountId, @accountName, @accountNumber)

FETCH AccountIterator INTO @accountId, @accountNumber, @accountName
END
CLOSE AccountIterator
DEALLOCATE AccountIterator
drop table #tmpAccount

select * 
from #results
drop table #results";
            List<NoProblem.Core.DataModel.Reports.SupplierMoneyReportResponse> dataList = new List<NoProblem.Core.DataModel.Reports.SupplierMoneyReportResponse>();
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                NoProblem.Core.DataModel.Reports.SupplierMoneyReportResponse data = new NoProblem.Core.DataModel.Reports.SupplierMoneyReportResponse();
                data.DepositBonus = (int)row["bonusDeposit"];
                data.DepositReal = (int)row["realDeposit"];
                data.LeftBonus = (int)row["bonusLeft"];
                data.LeftReal = (int)row["realLeft"];
                data.UsedBonus = (int)row["bonusUsed"];
                data.UsedReal = (int)row["realUsed"];
                data.SupplierNumber = row["supplierNumber"] != DBNull.Value ? (string)row["supplierNumber"] : string.Empty;
                data.SupplierName = row["supplierName"] != DBNull.Value ? (string)row["supplierName"] : string.Empty;
                data.SupplierId = (Guid)row["supplierId"];
                dataList.Add(data);
            }
            return dataList;
        }

        public bool HasRealMoneyButThis(Guid supplierId, Guid supplierPricingId)
        {
            string query =
                @"select count(*) from new_supplierpricing with(nolock)
                where deletionstatecode = 0 and new_supplierpricingid != @this
                and new_accountid = @supplierId and new_voucherid is null";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@this", supplierPricingId));
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            int count = (int)ExecuteScalar(query, parameters);
            return count > 0;
        }
    }
}
