﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class HdEntry : DalBase<DataModel.Xrm.new_helpdeskentry>
    {
        private const string tableName = "new_helpdeskentry";

        #region Ctor
        public HdEntry(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache(tableName);
        }
        #endregion

        #region Public Methods

        public override NoProblem.Core.DataModel.Xrm.new_helpdeskentry Retrieve(Guid id)
        {
            return XrmDataContext.new_helpdeskentrynew_helpdeskentries.FirstOrDefault(x => x.new_helpdeskentryid == id);
        }

        public override void Create(NoProblem.Core.DataModel.Xrm.new_helpdeskentry entity)
        {
            Autonumber.AutonumberManager autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
            string next = autoNumber.GetNextNumber(tableName);
            entity.new_name = next;
            base.Create(entity);
        }

        public List<NoProblem.Core.DataModel.HelpDesk.HdEntryData> GetEntries(
            Guid hpTypeId,
            Guid? regardingObjectId,
            DateTime? fromDate,
            DateTime? toDate,
            Guid? statusId,
            Guid? initiatorId,
            DataModel.Xrm.new_helpdesktype.InitiatorType initiatorType,
            Guid? ownerId,
            string ticketNumber,
            DataModel.Xrm.new_helpdesktype.RegardingObjectType regardingObjectType,
            string secondaryRegardingObjectName,
            DateTime? followUp)
        {
            List<DataModel.HelpDesk.HdEntryData> retVal = new List<DataModel.HelpDesk.HdEntryData>();
            using (SqlCommand command = new SqlCommand())
            {
                StringBuilder select = new StringBuilder();
                select.Append(@"select new_helpdeskentryid, new_helpdeskentry.new_name, new_initiatorid, new_title, new_description, new_regardingobjectid
                            ,new_helpdeskstatusid, new_helpdeskseverityid, new_followup, new_ownerid, new_createdbyid, new_helpdeskentry.createdon ");
                StringBuilder from = new StringBuilder();
                from.Append(@" from new_helpdeskentry with(nolock) ");
                StringBuilder where = new StringBuilder();
                where.Append(" where new_helpdeskentry.deletionstatecode = 0 ");
                HandleRegardingObjectType(regardingObjectType, select, from);
                HandleInitiatorType(initiatorType, select, from);
                AddFilters(hpTypeId, regardingObjectId, fromDate, toDate, statusId, initiatorId, ownerId, ticketNumber, followUp, secondaryRegardingObjectName, regardingObjectType, command, where);
                where.Append(" order by new_helpdeskentry.createdon desc ");
                command.CommandText = select.ToString() + from.ToString() + where.ToString();
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    command.Connection = connection;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.HelpDesk.HdEntryData data = CreateDataFromReader(reader);
                            data.TypeId = hpTypeId;
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        #endregion

        #region Private Methods

        private DataModel.HelpDesk.HdEntryData CreateDataFromReader(SqlDataReader reader)
        {
            DataModel.HelpDesk.HdEntryData data = new NoProblem.Core.DataModel.HelpDesk.HdEntryData();
            object regardingObj = reader["new_regardingobjectid"];
            if (regardingObj != null && regardingObj != DBNull.Value)
            {
                data.RegardingObjectId = new Guid((string)regardingObj);
                data.RegardingObjectName = (string)reader["regardingObjectName"];
            }
            object secondaryRergardingObj = reader["SecondaryRegardingObjectName"];
            if (secondaryRergardingObj != null && secondaryRergardingObj != DBNull.Value)
            {
                data.SecondaryRegardingObject = (string)secondaryRergardingObj;
            }
            object secondaryRergardingIdObj = reader["SecondaryRegardingObjectId"];
            if (secondaryRergardingIdObj != null && secondaryRergardingIdObj != DBNull.Value)
            {
                data.SecondaryRegardingObjectId = (Guid)secondaryRergardingIdObj;
            }
            data.InitiatorId = new Guid((string)reader["new_initiatorid"]);
            data.InitiatorName = (string)reader["initiatorName"];
            data.CreatedById = (Guid)reader["new_createdbyid"];
            data.CreatedOn = (DateTime)reader["createdon"];
            data.Description = reader["new_description"] != DBNull.Value ? (string)reader["new_description"] : string.Empty;
            object followUbObj = reader["new_followup"];
            if (followUbObj != null && followUbObj != DBNull.Value)
            {
                data.FollowUp = (DateTime)followUbObj;
            }
            data.Id = (Guid)reader["new_helpdeskentryid"];
            data.OwnerId = (Guid)reader["new_ownerid"];
            data.SeverityId = (Guid)reader["new_helpdeskseverityid"];
            data.StatusId = (Guid)reader["new_helpdeskstatusid"];
            data.TicketNumber = (string)reader["new_name"];
            data.Title = reader["new_title"] != DBNull.Value ? (string)reader["new_title"] : string.Empty;
            return data;
        }

        private void AddFilters(Guid hpTypeId, Guid? regardingObjectId, DateTime? fromDate, DateTime? toDate, Guid? statusId, Guid? initiatorId, Guid? ownerId, string ticketNumber, DateTime? followUp, string secondaryRegardingObjectName, DataModel.Xrm.new_helpdesktype.RegardingObjectType regardingObjectType, SqlCommand command, StringBuilder where)
        {
            where.Append(" and new_helpdesktypeid = @typeId ");
            command.Parameters.AddWithValue("@typeId", hpTypeId);
            if (fromDate.HasValue)
            {
                where.Append(" and new_helpdeskentry.createdon >= @from ");
                command.Parameters.AddWithValue("@from", fromDate.Value);
            }
            if (toDate.HasValue)
            {
                where.Append(" and new_helpdeskentry.createdon <= @to ");
                command.Parameters.AddWithValue("@to", toDate.Value);
            }
            if (statusId.HasValue)
            {
                where.Append(" and new_helpdeskstatusid = @statusId ");
                command.Parameters.AddWithValue("@statusId", statusId.Value);
            }
            if (initiatorId.HasValue)
            {
                where.Append(" and new_initiatorid = @initiatorId ");
                command.Parameters.AddWithValue("@initiatorId", initiatorId.Value.ToString());
            }
            if (ownerId.HasValue)
            {
                where.Append(" and new_ownerid = @ownerId ");
                command.Parameters.AddWithValue("@ownerId", ownerId.Value);
            }
            if (string.IsNullOrEmpty(ticketNumber) == false)
            {
                where.Append(" and new_helpdeskentry.new_name = @ticket ");
                command.Parameters.AddWithValue("@ticket", ticketNumber);
            }
            if (followUp.HasValue)
            {
                where.Append(" and new_helpdeskentry.new_followup <= @followUp ");
                command.Parameters.AddWithValue("@followUp", followUp.Value);
            }
            if (string.IsNullOrEmpty(secondaryRegardingObjectName) == false)
            {
                if (regardingObjectType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.RegardingObjectType.Call)
                {
                    where.Append(" and new_incidentaccount.new_accountidname = @secondaryObjName ");
                    command.Parameters.AddWithValue("@secondaryObjName", secondaryRegardingObjectName);
                }
                else if (regardingObjectType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.RegardingObjectType.Case)
                {

                }
            }
            if (regardingObjectId.HasValue && regardingObjectId.Value != Guid.Empty)
            {
                if (regardingObjectType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.RegardingObjectType.Call)
                {
                    where.Append(" and new_helpdeskentry.new_regardingobjectid = @regardingObjId ");
                    command.Parameters.AddWithValue("@regardingObjId", regardingObjectId.Value.ToString());
                }
                else if (regardingObjectType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.RegardingObjectType.Case)
                {

                }
            }
        }

        private void HandleInitiatorType(DataModel.Xrm.new_helpdesktype.InitiatorType initiatorType, StringBuilder select, StringBuilder from)
        {
            if (initiatorType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.InitiatorType.Consumer)
            {
                select.Append(" ,firstname as initiatorName ");
                from.Append(" join contact with(nolock) on new_initiatorid = convert(varchar(38) , contact.contactid) ");
            }
            else if (initiatorType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.InitiatorType.Supplier)
            {
                select.Append(" ,name as initiatorName ");
                from.Append(" join account with(nolock) on new_initiatorid = convert(varchar(38) , account.accountid) ");
            }
        }

        private void HandleRegardingObjectType(DataModel.Xrm.new_helpdesktype.RegardingObjectType regardingObjectType, StringBuilder select, StringBuilder from)
        {
            if (regardingObjectType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.RegardingObjectType.Call)
            {
                select.Append(@" ,ticketnumber as regardingObjectName, new_incidentaccount.new_accountidname as SecondaryRegardingObjectName
                                ,new_incidentaccount.new_accountid as SecondaryRegardingObjectId");
                from.Append(@" join new_incidentaccount with(nolock) on new_regardingobjectid = convert(varchar(38) , new_incidentaccountid) 
                                    join incident with(nolock) on new_incidentid = incidentid");
            }
            else if (regardingObjectType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.RegardingObjectType.Case)
            {
                select.Append(" ,ticketnumber as regardingObjectName, ticketnumber as SecondaryRegardingObjectName, incidentid as SecondaryRegardingObjectId");
                from.Append(@" join incident with(nolock) on new_regardingobjectid = convert(varchar(38) , incidentid)");
            }
        }

        #endregion
    }
}
