﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class HdType : DalBase<DataModel.Xrm.new_helpdesktype>
    {
        public HdType(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_helpdesktype");
        }

        public override NoProblem.Core.DataModel.Xrm.new_helpdesktype Retrieve(Guid id)
        {
            return XrmDataContext.new_helpdesktypes.FirstOrDefault(x => x.new_helpdesktypeid == id);
        }

        public List<DataModel.PublisherPortal.GuidStringPair> GetAll()
        {
            string query =
                @"select new_name, new_helpdesktypeid
                    from new_helpdesktype with(nolock)
                    where deletionstatecode = 0";
            List<DataModel.PublisherPortal.GuidStringPair> dataList =
                new List<DataModel.PublisherPortal.GuidStringPair>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.PublisherPortal.GuidStringPair data
                                = new DataModel.PublisherPortal.GuidStringPair();
                            data.Id = (Guid)reader[1];
                            data.Name = (string)reader[0];
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }

        public NoProblem.Core.DataModel.HelpDesk.HdTypeData GetTypeData(Guid typeId)
        {
            string query =
                @"SELECT
                new_regardingobjecttype,
                new_initiatortype
                from
                new_helpdesktype with(nolock)
                where
                deletionstatecode = 0
                and
                new_helpdesktypeid = @typeId";
            DataModel.HelpDesk.HdTypeData data = new NoProblem.Core.DataModel.HelpDesk.HdTypeData();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@typeId", typeId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            data.InitiatorType = (DataModel.Xrm.new_helpdesktype.InitiatorType)((int)reader[1]);
                            data.RegardingObjectType = (DataModel.Xrm.new_helpdesktype.RegardingObjectType)((int)reader[0]);
                        }
                    }
                }
            }
            return data;
        }
    }
}
