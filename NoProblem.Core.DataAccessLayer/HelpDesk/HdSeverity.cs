﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class HdSeverity : DalBase<DataModel.Xrm.new_helpdeskseverity>
    {
        public HdSeverity(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_helpdeskseverity");
        }

        public override NoProblem.Core.DataModel.Xrm.new_helpdeskseverity Retrieve(Guid id)
        {
            return XrmDataContext.new_helpdeskseveritynew_helpdeskseverities.FirstOrDefault(x => x.new_helpdeskseverityid == id);
        }

        public List<NoProblem.Core.DataModel.HelpDesk.HdSeverityData> GetAll(Guid hdTypeId)
        {
            string query =
                @"select new_name, new_helpdeskseverityid, new_id, new_disabled
                    from new_helpdeskseverity with(nolock)
                    where deletionstatecode = 0
                    and new_helpdesktypeid = @typeId";
            List<NoProblem.Core.DataModel.HelpDesk.HdSeverityData> dataList = new List<NoProblem.Core.DataModel.HelpDesk.HdSeverityData>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@typeId", hdTypeId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.HelpDesk.HdSeverityData data = new NoProblem.Core.DataModel.HelpDesk.HdSeverityData();
                            data.Id = (Guid)reader[1];
                            data.Name = (string)reader[0];
                            data.TypeId = hdTypeId;
                            data.IdSmall = (string)reader[2];
                            data.Inactive = reader[3] == DBNull.Value ? false : (bool)reader[3];
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }
    }
}
