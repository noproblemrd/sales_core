﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace NoProblem.Core.DataAccessLayer
{
    public class HdNote : DalBase<DataModel.Xrm.new_helpdesknote>
    {
        #region Ctor
        public HdNote(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_helpdesknote");
        } 
        #endregion

        #region Public Methods

        public override NoProblem.Core.DataModel.Xrm.new_helpdesknote Retrieve(Guid id)
        {
            return XrmDataContext.new_helpdesknotes.FirstOrDefault(x => x.new_helpdesknoteid == id);
        }

        public List<DataModel.Notes.NoteData> GetNotesByEntryId(Guid hdEntryId)
        {
            string query =
                @"select new_description, new_file, new_userid, new_useridname, createdon
                from new_helpdesknote with(nolock)
                where deletionstatecode = 0
                and new_helpdeskentryid = @entryId
                order by createdon desc";
            List<DataModel.Notes.NoteData> dataList = new List<NoProblem.Core.DataModel.Notes.NoteData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@entryId", hdEntryId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Notes.NoteData data = new NoProblem.Core.DataModel.Notes.NoteData();
                            data.Description = Effect.Crm.Convertor.ConvertorUtils.ToString(reader[0], string.Empty);
                            data.File = Effect.Crm.Convertor.ConvertorUtils.ToString(reader[1], string.Empty);
                            data.CreatedById = (Guid)reader[2];
                            data.CreatedOn = (DateTime)reader[4];
                            data.CreatedByName = (string)reader[3];
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        } 

        #endregion
    }
}
