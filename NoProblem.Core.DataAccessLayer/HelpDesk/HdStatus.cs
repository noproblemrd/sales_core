﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class HdStatus : DalBase<DataModel.Xrm.new_helpdeskstatus>
    {
        public HdStatus(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_helpdeskstatus");
        }

        public override NoProblem.Core.DataModel.Xrm.new_helpdeskstatus Retrieve(Guid id)
        {
            return XrmDataContext.new_helpdeskstatuses.FirstOrDefault(x => x.new_helpdeskstatusid == id);
        }

        public List<NoProblem.Core.DataModel.HelpDesk.HdStatusData> GetAll(Guid hpTypeId)
        {
            string query =
                @"select new_name, new_helpdeskstatusid, new_id, new_disabled
                    from new_helpdeskstatus with(nolock)
                    where deletionstatecode = 0
                    and new_helpdesktypeid = @typeId";
            List<NoProblem.Core.DataModel.HelpDesk.HdStatusData> dataList = new List<NoProblem.Core.DataModel.HelpDesk.HdStatusData>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@typeId", hpTypeId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.HelpDesk.HdStatusData data = new NoProblem.Core.DataModel.HelpDesk.HdStatusData();
                            data.Id = (Guid)reader[1];
                            data.Name = (string)reader[0];
                            data.TypeId = hpTypeId;
                            data.IdSmall = (string)reader[2];
                            data.Inactive = reader[3] == DBNull.Value ? false : (bool)reader[3];
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }
    }
}
