﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace NoProblem.Core.DataAccessLayer
{
    public class Voucher : DalBase<DataModel.Xrm.new_voucher>
    {
        public Voucher(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_voucher");
        }

        public override NoProblem.Core.DataModel.Xrm.new_voucher Retrieve(Guid id)
        {
            return XrmDataContext.new_vouchers.FirstOrDefault(x => x.new_voucherid == id);
        }

        public Guid GetVoucher(string voucherNumber, out int voucherBalance)
        {
            string query =
                @"select new_voucherid ,new_currentbalance from new_voucher with(nolock)
                    where deletionstatecode = 0 and new_number = @number";
            Guid retVal = Guid.Empty;
            voucherBalance = 0;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@number", voucherNumber);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            retVal = (Guid)reader["new_voucherid"];
                            voucherBalance = reader["new_currentbalance"] != DBNull.Value ? (int)reader["new_currentbalance"] : 0;
                        }
                    }
                }
            }
            return retVal;
        }

        public void DecreaseVoucherBalance(Guid voucherId, int amount)
        {
            string query =
                @"update new_voucher set new_currentbalance = (new_currentbalance - @amount)
                    where new_voucherid = @id";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@amount", amount));
            parameters.Add(new KeyValuePair<string, object>("@id", voucherId));
            ExecuteNonQuery(query, parameters);
        }

        public List<NoProblem.Core.DataModel.Reports.VoucherData> GetAllVouchers()
        {
            string query =
                @"select vo.new_number, vo.new_currentbalance, vo.new_originalbalance, vo.createdon
                ,(select count(sp.new_supplierpricingid) from new_supplierpricing sp with(nolock) where sp.deletionstatecode = 0 and sp.new_voucherid = vo.new_voucherid and sp.new_total > 0) as depositsCount
                from new_voucher vo with(nolock)
                where vo.deletionstatecode = 0
                order by vo.createdon desc";
            //sp.new_total > 0 because we don't want to count refund deposits.
            List<DataModel.Reports.VoucherData> dataList = new List<NoProblem.Core.DataModel.Reports.VoucherData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Reports.VoucherData data = new NoProblem.Core.DataModel.Reports.VoucherData();
                            data.AmountOfDeposits = (int)reader["depositsCount"];
                            data.CreatedOn = (DateTime)reader["createdon"];
                            data.CurrentBalance = (int)reader["new_currentbalance"];
                            data.Number = (string)reader["new_number"];
                            data.OriginalBalance = (int)reader["new_originalbalance"];
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }
    }
}
