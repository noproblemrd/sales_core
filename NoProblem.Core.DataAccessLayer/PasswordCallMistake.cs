﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class PasswordCallMistake : DalBase<DataModel.Xrm.new_passwordcallmistake>
    {
        public PasswordCallMistake(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_passwordcallmistake");
        }

        public override NoProblem.Core.DataModel.Xrm.new_passwordcallmistake Retrieve(Guid id)
        {
            return XrmDataContext.new_passwordcallmistakes.FirstOrDefault(x => x.new_passwordcallmistakeid == id);
        }
    }
}
