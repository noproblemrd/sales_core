﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.Region
//  File: Region.cs
//  Description: DAL for Region Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using DML = NoProblem.Core.DataModel;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.Regions;

namespace NoProblem.Core.DataAccessLayer
{
    public class Region : DalBase<DML.Xrm.new_region>
    {
        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        #region Ctor
        public Region(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_region");
        }
        #endregion

        #region Public Methods

        public override IQueryable<DML.Xrm.new_region> All
        {
            get
            {
                return XrmDataContext.new_regions;
            }
        }

        public override NoProblem.Core.DataModel.Xrm.new_region Retrieve(Guid id)
        {
            DML.Xrm.new_region region = XrmDataContext.new_regions.FirstOrDefault(x => x.new_regionid == id);
            return region;
        }

        public IEnumerable<DML.Xrm.new_region> GetBrothers(Guid regionId)
        {
            DML.Xrm.new_region region = Retrieve(regionId);
            return GetBrothers(region);
        }

        public IEnumerable<DML.Xrm.new_region> GetBrothers(DML.Xrm.new_region region)
        {
            if (region.new_level == 1)
            {
                return new List<DML.Xrm.new_region>();
            }
            IEnumerable<DML.Xrm.new_region> brothers = from r in XrmDataContext.new_regions
                                                       where
                                                       r.new_parentregionid.Value == region.new_parentregionid
                                                       select r;
            return brothers;
        }

        public List<Guid> GetBrothersIds(Guid regionId)
        {
            List<Guid> retVal = new List<Guid>();
            string query =
                @"select new_regionid from new_region with(nolock) where deletionstatecode = 0
                and new_parentregionid = ( select top 1 new_parentregionid from new_region with(nolock) where deletionstatecode = 0 and new_regionid = @id )";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", regionId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public List<DML.Xrm.new_region> GetRegionsById(List<Guid> regionsIds)
        {
            List<DML.Xrm.new_region> retVal = new List<DML.Xrm.new_region>();
            foreach (Guid id in regionsIds)
            {
                DML.Xrm.new_region region = Retrieve(id);
                if (region != null)
                {
                    retVal.Add(region);
                }
            }
            return retVal;
        }

        public DML.SqlHelper.RegionMinData GetRegionMinDataByName(string name)
        {
            return CacheManager.Instance.Get<DML.SqlHelper.RegionMinData>(cachePrefix + ".GetRegionMinDataByName", name, GetRegionMinDataByNameMethod, int.MaxValue);
        }

        private DML.SqlHelper.RegionMinData GetRegionMinDataByNameMethod(string name)
        {
            string query = @"select new_regionid,new_parentregionid,new_level,new_name
                            from new_region with (nolock) where new_name = @name and deletionstatecode = 0";
            DML.SqlHelper.RegionMinData retVal = new NoProblem.Core.DataModel.SqlHelper.RegionMinData();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@name", name);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Level = (int)reader[2];
                            retVal.Id = (Guid)reader[0];
                            retVal.ParentId = Guid.Empty;
                            retVal.Name = (string)reader["new_name"];
                            object guidObj = reader[1];
                            if (guidObj != null && guidObj != DBNull.Value)
                            {
                                retVal.ParentId = (Guid)guidObj;
                            }
                        }
                    }
                }
            }
            return retVal;
        }

        public DML.SqlHelper.RegionMinData GetRegionMinDataByCode(string code)
        {
            return CacheManager.Instance.Get<DML.SqlHelper.RegionMinData>(cachePrefix + ".", code, GetRegionMinDataByCodeMethod, int.MaxValue);
        }

        private DML.SqlHelper.RegionMinData GetRegionMinDataByCodeMethod(string code)
        {
            string query = @"select new_regionid,new_parentregionid,new_level,new_name
                            from new_region with (nolock) where new_code = @code and deletionstatecode = 0";
            DML.SqlHelper.RegionMinData retVal = new NoProblem.Core.DataModel.SqlHelper.RegionMinData();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@code", code);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Level = (int)reader[2];
                            retVal.Id = (Guid)reader[0];
                            retVal.ParentId = Guid.Empty;
                            retVal.Name = (string)reader["new_name"];
                            object guidObj = reader[1];
                            if (guidObj != null && guidObj != DBNull.Value)
                            {
                                retVal.ParentId = (Guid)guidObj;
                            }
                        }
                    }
                }
            }
            return retVal;
        }

        public DML.SqlHelper.RegionMinData GetRegionMinDataById(Guid regionId)
        {
            return CacheManager.Instance.Get<DML.SqlHelper.RegionMinData>(cachePrefix + ".GetRegionMinDataById", regionId.ToString(), GetRegionMinDataByIdMethod, CacheManager.ONE_YEAR);
        }

        private DML.SqlHelper.RegionMinData GetRegionMinDataByIdMethod(string regionIdStr)
        {
            string query = @"select new_regionid,new_parentregionid,new_level,new_code,new_name,new_timezonecode,new_englishname
                            from new_region with (nolock) where new_regionid = '" + regionIdStr + "' and deletionstatecode = 0";
            DML.SqlHelper.RegionMinData retVal = new NoProblem.Core.DataModel.SqlHelper.RegionMinData();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Level = (int)reader[2];
                            retVal.Id = (Guid)reader[0];
                            retVal.ParentId = Guid.Empty;
                            object guidObj = reader[1];
                            if (guidObj != null && guidObj != DBNull.Value)
                            {
                                retVal.ParentId = (Guid)guidObj;
                            }
                            retVal.Code = (string)reader[3];
                            string regionName = (retVal.Level == 2) ? (string)reader[4] + " county" : (string)reader[4];//if is county
                            retVal.Name = regionName;// (string)reader[4];
                            retVal.TimeZoneCode = reader[5] != DBNull.Value ? (int)reader[5] : -1;
                            retVal.EnglishName = Convert.ToString(reader[6]);
                        }
                    }
                }
            }
            return retVal;
        }

        private DML.Xrm.new_region GetRegionByCodeMethod(string code)
        {
            DML.Xrm.new_region region = XrmDataContext.new_regions.FirstOrDefault(x => x.new_code == code);
            return region;
        }

        public DML.Xrm.new_region GetRegionByCode(string code)
        {
            return CacheManager.Instance.Get<DML.Xrm.new_region>(cachePrefix + ".GetRegionByCode", code, GetRegionByCodeMethod, CacheManager.ONE_WEEK);
        }

        public IEnumerable<DML.Xrm.new_region> GetChildren(Guid regionId)
        {
            IEnumerable<DML.Xrm.new_region> retVal = from r in XrmDataContext.new_regions
                                                     where
                                                     r.new_parentregionid.HasValue
                                                     &&
                                                     r.new_parentregionid.Value == regionId
                                                     select
                                                     r;
            return retVal;
        }

        public List<Guid> GetChildrenIds(Guid regionId)
        {
            List<Guid> retVal = new List<Guid>();
            string query =
                @"select new_regionid from new_region with(nolock) where deletionstatecode = 0 and new_parentregionid = @id";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", regionId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_region> GetRegionsByLevel(int level)
        {
            IEnumerable<DML.Xrm.new_region> retVal = XrmDataContext.new_regions.Where(x => x.new_level == level);
            return retVal;
        }

        public IEnumerable<Guid> GetRegionIdsByLevel(int level)
        {
            var regions = GetRegionsByLevel(level);
            var retVal = from reg in regions
                         select reg.new_regionid;
            return retVal;
        }

        public IEnumerable<DML.Xrm.new_region> GetParents(NoProblem.Core.DataModel.Xrm.new_region region)
        {
            List<DML.Xrm.new_region> parents = new List<NoProblem.Core.DataModel.Xrm.new_region>();
            FindParents(parents, region);
            return parents;
        }

        public List<DML.SqlHelper.RegionMinData> GetParents(DML.SqlHelper.RegionMinData region)
        {
            List<DML.SqlHelper.RegionMinData> parents = new List<DML.SqlHelper.RegionMinData>();
            FindParents(parents, region);
            return parents;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_region> SearchRegions(string regionName)
        {
            IEnumerable<DML.Xrm.new_region> retVal =
                from reg in XrmDataContext.new_regions
                where
                reg.new_name.StartsWith(regionName)
                select
                reg;
            return retVal;
        }

        public NoProblem.Core.DataModel.Xrm.new_region GetRegionByName(string regionName)
        {
            return CacheManager.Instance.Get<NoProblem.Core.DataModel.Xrm.new_region>(cachePrefix + ".GetRegionByName", regionName, GetRegionByNameCached, CacheManager.ONE_WEEK);
        }

        private NoProblem.Core.DataModel.Xrm.new_region GetRegionByNameCached(string regionName)
        {
            DML.Xrm.new_region region = XrmDataContext.new_regions.FirstOrDefault(x => x.new_name == regionName);
            return region;
        }

        public Guid GetRegionIdByName(string regionName)
        {
            string id = CacheManager.Instance.Get<string>(cachePrefix + ".GetRegionIdByName", regionName, GetRegionIdByNameMethod, int.MaxValue);
            return new Guid(id);
        }

        private string GetRegionIdByNameMethod(string regionName)
        {
            string query =
                @"select new_regionid from new_region with(nolock) where 
                deletionstatecode = 0 and new_name = @name";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@name", regionName));
            Guid retVal = Guid.Empty;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal.ToString();
        }

        public string GetCodeByNameAndLevel(string regionName, int level)
        {
            string query = @"select new_code from new_region with(nolock) where deletionstatecode = 0 and new_name = @name and new_level = @level";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@name", regionName));
            parameters.Add(new KeyValuePair<string, object>("@level", level));
            object scalar = ExecuteScalar(query, parameters);
            string code = null;
            if (scalar != null && scalar != DBNull.Value)
            {
                code = (string)scalar;
            }
            return code;
        }

        public Guid GetRegionIdByCode(string code)
        {
            var query = from r in XrmDataContext.new_regions
                        where
                        r.new_code == code
                        select
                        r.new_regionid;
            return query.FirstOrDefault();
        }

        public IEnumerable<Guid> GetLevel1RegionIdsForAccount(Guid supplierId)
        {
            string query =
                @"select new_regionaccountexpertise.new_regionid from new_regionaccountexpertise with(nolock)
                join new_region with(nolock) on new_region.new_regionid = new_regionaccountexpertise.new_regionid
                where new_regionaccountexpertise.new_accountid = @id and new_regionaccountexpertise.deletionstatecode = 0
                and new_region.deletionstatecode = 0
                and new_region.new_level = 1
                and (new_regionaccountexpertise.new_regionstate = @working 
                or new_regionaccountexpertise.new_regionstate = @partial)";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", supplierId);
                    command.Parameters.AddWithValue("@working", (int)DML.Xrm.new_regionaccountexpertise.RegionState.Working);
                    command.Parameters.AddWithValue("@partial", (int)DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public List<string> GetLevel1RegionNamesForAccount(Guid accountId)
        {
            string query =
                @"select new_regionidname from new_regionaccountexpertise with(nolock)
                join new_region with(nolock) on new_region.new_regionid = new_regionaccountexpertise.new_regionid
                where new_regionaccountexpertise.new_accountid = @id and new_regionaccountexpertise.deletionstatecode = 0
                and new_region.deletionstatecode = 0
                and new_region.new_level = 1
                and (new_regionaccountexpertise.new_regionstate = @working 
                or new_regionaccountexpertise.new_regionstate = @partial)";
            List<string> retVal = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", accountId);
                    command.Parameters.AddWithValue("@working", (int)DML.Xrm.new_regionaccountexpertise.RegionState.Working);
                    command.Parameters.AddWithValue("@partial", (int)DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((string)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public bool IsExists(string code)
        {
            string query = @"select top 1 1
                from new_region with (nolock) 
                where new_code = @code
                and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@code", code));
            object scalar = ExecuteScalar(query, parameters);
            return scalar != null;
        }

        public Guid GetLevel1Parent(Guid regionId)
        {
            string id = CacheManager.Instance.Get<string>(cachePrefix + ".GetLevel1Parent", regionId.ToString(), GetLevel1ParentMethod, int.MaxValue);
            return new Guid(id);
        }

        private string GetLevel1ParentMethod(string regionIdStr)
        {
            Guid regionId = new Guid(regionIdStr);
            string query =
                @"WITH RegionHirarchy(RegionLevel,RegionId,ParentRegionId)
                AS
                (
                    SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
                    FROM new_region with(nolock)
                    WHERE new_region.new_regionid = @regionId
                    UNION ALL
                    SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
                    FROM new_region with(nolock)
                    INNER JOIN RegionHirarchy ON RegionHirarchy.ParentRegionId = new_regionid
                )
                select RegionId from RegionHirarchy
                where RegionLevel = 1";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@regionId", regionId));
            object scalar = ExecuteScalar(query, parameters);
            Guid retVal = Guid.Empty;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal.ToString();
        }

        public Guid GetLevel1Parent(string regionCode, int regionLevel)
        {
            string query =
                @"WITH RegionHirarchy(RegionLevel,RegionId,ParentRegionId)
                AS
                (
                    SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
                    FROM new_region with(nolock)
                    WHERE new_region.new_level = @RegionLevel AND new_code = @RegionCode
                    UNION ALL
                    SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
                    FROM new_region with(nolock)
                    INNER JOIN RegionHirarchy ON RegionHirarchy.ParentRegionId = new_regionid
                )
                select RegionId from RegionHirarchy
                where RegionLevel = 1";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@RegionLevel", regionLevel));
            parameters.Add(new KeyValuePair<string, object>("@RegionCode", regionCode));
            object scalar = ExecuteScalar(query, parameters);
            Guid retVal = Guid.Empty;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        public Guid GetParentId(Guid regionId)
        {
            string idStr = DataAccessLayer.CacheManager.Instance.Get<string>(cachePrefix + ".GetParentId", regionId.ToString(), GetParentIdMethod, CacheManager.ONE_YEAR);
            return new Guid(idStr);
        }
        public string GetCityState(Guid regionId)
        {
            string fullName = string.Empty;
            string command = "SELECT New_englishname FROM dbo.New_regionExtensionBase WITH(NOLOCK) WHERE New_regionId = @regionId";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@regionId", regionId);
                    fullName = (string)cmd.ExecuteScalar();
                    conn.Close();
                }
            }
            return BuildCityAndStateString(fullName);
        }
        private string BuildCityAndStateString(string fullNameWithParents)
        {
            string[] splitted = fullNameWithParents.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (splitted.Length == 4)
            {
                return splitted[1] + ", " + splitted[3];
            }
            return "****";
        }
        public Guid GetRegionIdByNameAndLevel(string name, int level)
        {
            Guid regionId = Guid.Empty;
            string command = "SELECT New_regionId FROM dbo.New_region WITH(NOLOCK) WHERE DeletionStateCode = 0 and New_name = @name and New_Level = @level";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@name", name);
                    cmd.Parameters.AddWithValue("@level", level);
                    object o = cmd.ExecuteScalar();
                    if (o != null || o != DBNull.Value)
                        regionId = (Guid)o;
                    conn.Close();
                }
            }
            return regionId;
        }
        private string GetParentIdMethod(string regionId)
        {
            string query =
                @"select new_parentregionid from new_region with(nolock)
                where deletionstatecode = 0 and new_regionid = @regionId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@regionId", new Guid(regionId)));
            Guid retVal = Guid.Empty;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal.ToString();
        }

        #endregion

        #region Private Methods

        private void FindParents(List<DML.SqlHelper.RegionMinData> parents, DML.SqlHelper.RegionMinData region)
        {
            if (region.ParentId != Guid.Empty)
            {
                DML.SqlHelper.RegionMinData dad = GetRegionMinDataById(region.ParentId);
                parents.Add(dad);
                FindParents(parents, dad);
            }
        }

        private void FindParents(List<DML.Xrm.new_region> parents, DML.Xrm.new_region region)
        {
            if (region.new_parentregionid.HasValue)
            {
                DML.Xrm.new_region dad = Retrieve(region.new_parentregionid.Value);
                parents.Add(dad);
                FindParents(parents, dad);
            }
        }

        #endregion

        public Guid GetRegionByNameWithParent(string name, Guid parentId)
        {
            string query;
            if (parentId != Guid.Empty)
            {
                query =
                    @"select top 1 new_regionid from new_region with(nolock)
                where deletionstatecode = 0 and new_parentregionid = @parentId and new_name = @name";
            }
            else
            {
                query =
                    @"select top 1 new_regionid from new_region with(nolock)
                where deletionstatecode = 0 and new_parentregionid is null and new_name = @name";
            }
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            if (parentId != Guid.Empty)
            {
                parameters.Add(new KeyValuePair<string, object>("@parentId", parentId));
            }
            parameters.Add(new KeyValuePair<string, object>("@name", name));
            Guid retVal = Guid.Empty;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        public Dictionary<string, int> GetRegionsByZipCode(string zipcode)
        {
            Dictionary<string, int> retVal = new Dictionary<string, int>();
            string query = @"SELECT new_region.new_code, new_region.new_level
	                FROM new_zipcode
	                INNER JOIN new_region ON new_region.new_regionid = new_zipcode.new_regionid
	                WHERE new_zipcode.new_name = @ZipCode";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@ZipCode", zipcode);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string code = (string)reader["new_code"];
                            int level = (int)reader["new_level"];
                            retVal.Add(code, level);
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetRegionIdByCityAndState(string state, string city)
        {
            string id = CacheManager.Instance.Get<string>(cachePrefix + ".GetRegionIdByCityAndState", state + ";" + city, GetRegionIdByCityAndState, int.MaxValue);
            return new Guid(id);
        }

        private string GetRegionIdByCityAndState(string stateAndCity)
        {
            string[] split = stateAndCity.Split(';');
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_region>.SqlParametersList();
            parameters.Add("@state", split[0]);
            parameters.Add("@city", split[1]);
            string query = @"
select top 1 r3.new_regionid 
from new_regionextensionbase r3 with(nolock) 
join New_regionExtensionBase r2 with(Nolock) on r3.new_parentregionid = r2.New_regionId
join New_regionExtensionBase r1 with(nolock) on r2.new_parentregionid = r1.New_regionId
where r3.new_name = @city and r3.New_Level = 3
and r1.New_englishname = @state
";
            Guid retVal = Guid.Empty;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal.ToString();
        }

        public string GetStateAbbreviation(string state)
        {
            return CacheManager.Instance.Get<string>(cachePrefix + ".GetStateAbbreviation", state, GetStateAbbreviationMethod, int.MaxValue);
        }
        public int GetTimeZoneCode(string zipcode)
        {
            string command = @"
select r1.New_TimeZoneCode
from dbo.New_region r WITH(NOLOCK)
	inner join dbo.Region_Hierarchy  rh WITH(NOLOCK) on r.New_regionId = rh.Lvl4
	inner join dbo.New_region r1 WITH(NOLOCK) on r1.New_regionId = rh.Lvl1
where r.DeletionStateCode = 0 and r.New_name = @zipcode and r.New_Level = 4";
            int result = -1;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@zipcode", zipcode);
                    object o = cmd.ExecuteScalar();
                    if (o != null && o != DBNull.Value)
                        result = (int)o;
                    conn.Close();
                }
            }
            return result;
        }
        public int GetTimeZoneCode(Guid regionId)
        {
            string command = @"
select  r.New_TimeZoneCode
from dbo.Region_Hierarchy  rh WITH(NOLOCK) 
	inner join dbo.New_region r WITH(NOLOCK) on r.New_regionId = rh.Lvl1
where rh.Lvl4 = @regionId";
            int result = -1;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@regionId", regionId);
                    object o = cmd.ExecuteScalar();
                    if (o != null && o != DBNull.Value)
                        result = (int)o;
                    conn.Close();
                }
            }
            return result;
        }
        private string GetStateAbbreviationMethod(string state)
        {
            string retVal = null;
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.Region.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_region>.SqlParametersList();
            parameters.Add("@state", state);
            object scalar = dal.ExecuteScalar(queryGetStateAbbreviation, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        private const string queryGetStateAbbreviation =
            @"select new_name stateAbb from new_region with(nolock) where new_englishname = @state";

        public ZipCodeData GetZipCodeData(string zipCode)
        {
            return CacheManager.Instance.Get<ZipCodeData>(cachePrefix + ".GetZipCodeData", zipCode, GetZipCodeDataMethod, CacheManager.ONE_YEAR);
        }

        private ZipCodeData GetZipCodeDataMethod(string zipCode)
        {
            ZipCodeData retVal = new ZipCodeData();
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_region>.SqlParametersList();
            parameters.Add("@zipcode", zipCode);
            var areasReader = ExecuteReader(queryForGetZipCodeData, parameters);
            if (areasReader.Count > 0)
            {
                var row = areasReader.First();
                retVal.City = Convert.ToString(row["cityName"]);
                retVal.StateAbbreviation = Convert.ToString(row["stateAbb"]);
                retVal.State = Convert.ToString(row["stateName"]);
                retVal.Id = (Guid)row["id"];
            }
            retVal.ZipCode = zipCode;
            return retVal;
        }

        private const string queryForGetZipCodeData =
    @"
select rgCity.new_name cityName
, rgState.new_name stateAbb
, rgState.new_englishname stateName
, rgZip.new_regionid id
from new_regionextensionbase rgZip with(nolock)
join new_regionextensionbase rgCity with(nolock) on rgZip.new_parentregionid = rgCity.new_regionid
join new_regionextensionbase rgCounty with(nolock) on rgCity.new_parentregionid = rgCounty.new_regionid
join new_regionextensionbase rgState with(nolock) on rgCounty.new_parentregionid = rgState.new_regionid
where rgZip.new_name = @zipcode and rgZip.new_level = 4
";
    }
}
