﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.IncidentAccount
//  File: IncidentAccount.cs
//  Description: DAL for IncidentAccount Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Effect.Crm.Convertor;
using NoProblem.Core.DataModel.Reports;
using DML = NoProblem.Core.DataModel;


namespace NoProblem.Core.DataAccessLayer
{
    public class IncidentAccount : DalBase<DML.Xrm.new_incidentaccount>
    {
        #region Ctor
        public IncidentAccount(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_incidentaccount");
        }
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_incidentaccount Retrieve(Guid id)
        {
            DML.Xrm.new_incidentaccount incidentAccount = XrmDataContext.new_incidentaccounts.FirstOrDefault(x => x.new_incidentaccountid == id);
            return incidentAccount;
        }

        public override IQueryable<DML.Xrm.new_incidentaccount> All
        {
            get
            {
                return XrmDataContext.new_incidentaccounts;
            }
        }

        public override void Create(NoProblem.Core.DataModel.Xrm.new_incidentaccount entity)
        {
            entity.new_advertiserfaults = 0;
            base.Create(entity);
        }

        public DML.Xrm.new_incidentaccount GetIncidentAccountByRecordId(string recordId)
        {
            DML.Xrm.new_incidentaccount incidentAccount = XrmDataContext.new_incidentaccounts.FirstOrDefault(x => x.new_recordid == recordId);
            return incidentAccount;
        }

        public DML.Xrm.new_incidentaccount GetIncidentAccountByIncidentAndAccount(Guid incidentId, Guid accountId)
        {
            DML.Xrm.new_incidentaccount retVal = XrmDataContext.new_incidentaccounts.FirstOrDefault(
                x => x.new_accountid == accountId && x.new_incidentid == incidentId);
            return retVal;
        }

        public List<DataModel.Refunds.RefundData> GetIncidentAccountsByRefundDatesAndRefundStatus(DateTime fromDate, DateTime toDate, DML.Xrm.new_incidentaccount.RefundSatus? refundStatus, int pageSize, int pageNumber, out int totalRows)
        {
            totalRows = 0;
            string query =
@"WITH PagerTbl AS
                (                
                SELECT ROW_NUMBER() OVER(ORDER BY ia.new_refundrequestdate) AS RowNum, 
ia.new_incidentid
,inc.ticketnumber
,ia.new_accountid
,acc.name
,refr.new_name
,refr.new_code
,ia.new_refundrequestdate
,ia.new_refundrequestdatelocal
,inc.new_primaryexpertiseidname
,ia.new_refundnote
,ia.new_refundnoteforadvertiser
,ia.new_recordfilelocation
,ia.new_refundstatus
,ia.new_potentialincidentprice
,ct.IncidentId as caseTicketIncidentId
, (
			case when
				(select cast(count(*) AS REAL)
                from new_incidentaccount with(nolock)
                where (statuscode = 10 or statuscode = 12 or statuscode = 13)
                and deletionstatecode  = 0
                and new_accountid = ia.new_accountid)--connected
				= 0 
				then 0
				else(	
				(select cast(count(*) AS REAL)
                from new_incidentaccount with(nolock)
                where (statuscode = 10 or statuscode = 12 or statuscode = 13)
                and new_refundstatus is not null
                and deletionstatecode  = 0
                and new_accountid = ia.new_accountid) --refund requested
				/ 
				(select cast(count(*) AS REAL)
                from new_incidentaccount with(nolock)
                where (statuscode = 10 or statuscode = 12 or statuscode = 13)
                and deletionstatecode  = 0
                and new_accountid = ia.new_accountid) --connected
				* 100)
				end
) as RefundRequestPercentage
,(
			case when 
				(select cast(count(*) AS REAL)
                from new_incidentaccount with(nolock)
                where (statuscode = 10 or statuscode = 12 or statuscode = 13)
                and new_refundstatus is not null
                and deletionstatecode  = 0
                and new_accountid = ia.new_accountid) --refund requested
			= 0 
			then 0
			else (
				(select cast(count(*) AS REAL)
                from new_incidentaccount with(nolock)
                where (statuscode = 10 or statuscode = 12 or statuscode = 13)
                and new_refundstatus = 3
                and deletionstatecode  = 0
                and new_accountid = ia.new_accountid) --refund approved
				/
				(select cast(count(*) AS REAL)
                from new_incidentaccount with(nolock)
                where (statuscode = 10 or statuscode = 12 or statuscode = 13)
                and new_refundstatus is not null
                and deletionstatecode  = 0
                and new_accountid = ia.new_accountid) --refund requested
				* 100
				) end
) as RefundsApprovedPercentage
from
new_incidentaccount ia with(nolock)
join incident inc with(nolock) on ia.new_incidentid = inc.incidentid
join account acc with(nolock) on ia.new_accountid = acc.accountid
left join new_refundreason refr with(nolock) on ia.new_refundreasonid = refr.new_refundreasonid
left join CaseTickets ct on ct.IncidentId = inc.incidentid
where
ia.new_refundstatus is not null
and ia.new_refundrequestdate is not null
and ia.new_refundrequestdate between @from and @to
and ia.deletionstatecode = 0
and inc.deletionstatecode = 0
and acc.deletionstatecode = 0 ";

            List<DataModel.Refunds.RefundData> retVal = new List<NoProblem.Core.DataModel.Refunds.RefundData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    if (refundStatus.HasValue)
                    {
                        query += " and ia.new_refundstatus = @refundStatus ";
                        command.Parameters.AddWithValue("@refundStatus", (int)refundStatus.Value);
                    }
                    query += @" )
                    SELECT * , (select max(RowNum) from PagerTbl) as totalRows
                      FROM PagerTbl
                     WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
                                      AND @PageNum * @PageSize)
                            or @PageSize = -1)
                     ORDER BY RowNum";
                    command.Parameters.AddWithValue("@from", fromDate);
                    command.Parameters.AddWithValue("@to", toDate);
                    command.Parameters.AddWithValue("@PageNum", pageNumber);
                    command.Parameters.AddWithValue("@PageSize", pageSize);
                    command.CommandText = query;
                    command.Connection = conn;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Refunds.RefundData data = new NoProblem.Core.DataModel.Refunds.RefundData();
                            if (reader["caseTicketIncidentId"] != DBNull.Value)
                            {
                                data.CaseId = (Guid)reader["new_incidentid"];
                            }
                            data.ExpertiseName = reader["new_primaryexpertiseidname"] != DBNull.Value ? (string)reader["new_primaryexpertiseidname"] : string.Empty;
                            data.Price = reader["new_potentialincidentprice"] != DBNull.Value ? (decimal)reader["new_potentialincidentprice"] : 0;
                            data.RecordFileLocation = reader["new_recordfilelocation"] != DBNull.Value ? (string)reader["new_recordfilelocation"] : string.Empty;
                            data.RefundNote = reader["new_refundnote"] != DBNull.Value ? (string)reader["new_refundnote"] : string.Empty;
                            data.RefundNoteForAdvertiser = reader["new_refundnoteforadvertiser"] != DBNull.Value ? (string)reader["new_refundnoteforadvertiser"] : string.Empty;
                            data.RefundReasonCode = reader["new_code"] != DBNull.Value ? ((int)reader["new_code"]).ToString() : string.Empty;
                            data.RefundReasonName = reader["new_name"] != DBNull.Value ? (string)reader["new_name"] : string.Empty;
                            data.RefundRequestDate = reader["new_refundrequestdatelocal"] != DBNull.Value ? (DateTime)reader["new_refundrequestdatelocal"] : (DateTime)reader["new_refundrequestdate"];
                            data.RefundRequestPercentage = (float)reader["RefundRequestPercentage"];
                            data.RefundsApprovedPercentage = (float)reader["RefundsApprovedPercentage"];
                            data.RefundStatus = Enum.GetName(typeof(DML.Xrm.new_incidentaccount.RefundSatus), (int)reader["new_refundstatus"]);
                            data.SupplierId = (Guid)reader["new_accountid"];
                            data.SupplierName = (string)reader["name"];
                            data.TicketNumber = (string)reader["ticketnumber"];
                            if (totalRows == 0)
                            {
                                totalRows = unchecked((int)((long)reader["totalRows"]));
                            }
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public IEnumerable<DML.Xrm.new_incidentaccount> GetIncidentAccountsByIncidentId(Guid incidentId)
        {
            IEnumerable<DML.Xrm.new_incidentaccount> retVal = from inciAcc in XrmDataContext.new_incidentaccounts
                                                              where
                                                              inciAcc.new_incidentid.HasValue
                                                              &&
                                                              inciAcc.new_incidentid.Value == incidentId
                                                              orderby
                                                              inciAcc.new_potentialincidentprice descending
                                                              select
                                                              inciAcc;

            return retVal;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_incidentaccount> GetIncidentAccountByAccount(NoProblem.Core.DataModel.Xrm.account account)
        {
            IEnumerable<DML.Xrm.new_incidentaccount> retVal = from i in XrmDataContext.new_incidentaccounts
                                                              where
                                                              i.new_accountid == account.accountid
                                                              select
                                                              i;
            return retVal;
        }

        public List<DML.Reports.CallData> GetIncidentAccountsForCallsReport(DateTime fromDate, DateTime toDate, Guid expertiseId, DML.Reports.CallsReport.CallType callType, Guid userId, Guid supplierOriginId)
        {
            List<DML.Reports.CallData> dataList = new List<NoProblem.Core.DataModel.Reports.CallData>();
            using (SqlCommand command = new SqlCommand())
            {
                string query = @"
                        declare @publisherTimeZoneCode int
                        select @publisherTimeZoneCode = new_value from new_configurationsetting with(nolock) where new_key = 'PublisherTimeZone'

                        select 
                        case when ia.new_potentialincidentprice is null then 0 else ia.new_potentialincidentprice end as new_potentialincidentprice
                        ,ia.new_recordfilelocation
                        ,ia.new_accountidname
                        ,ia.new_accountid
                        ,sup.accountnumber
                        ,inc.new_primaryexpertiseidname
                        ,case when ia.new_predefinedprice is null then ae.new_incidentprice else ia.new_predefinedprice end as new_predefinedprice
                        --,dbo.fn_UTCToLocalTimeByTimeZoneCode(ia.createdon, @publisherTimeZoneCode) as createdon
                        ,ia.createdon as createdon
                        ,inc.ticketnumber
                        ,case when acc.new_canhearallrecordings = 1 then 1 else 
                            (case when ia.new_refundstatus is null then 0 else 1 end) end as isAllowed                        
						,blr.new_balanceafteraction
                        ,(select top 1 new_duration from new_calldetailrecord cdr with(nolock) where cdr.deletionstatecode = 0 and (cdr.new_type in (3, 4)) and cdr.new_incidentaccountid = ia.new_incidentaccountid) as new_duration
                        from 
                        new_incidentaccount ia with(nolock)
                        join incident inc with(nolock) on inc.incidentid = ia.new_incidentid
                        join new_accountexpertise ae with(nolock) on (ae.new_accountid = ia.new_accountid and ae.new_primaryexpertiseid = inc.new_primaryexpertiseid)
                        join account sup with(nolock) on ia.new_accountid = sup.accountid
                        left join account acc with(nolock) on @userId = acc.accountid
                        
						left join new_balancerow blr with(nolock) on blr.new_incidentaccountid = ia.new_incidentaccountid

                        where
                        ia.createdon between @from and @to
                        and
                        ia.new_tocharge = 1
                        and
                        inc.new_primaryexpertiseid is not null
                        and
                        ia.deletionstatecode = 0 and ae.deletionstatecode = 0 and inc.deletionstatecode = 0
                        and isnull(sup.new_dapazstatus, 1) = 1
                        ";

                if (expertiseId != Guid.Empty)
                {
                    query += @" and inc.new_primaryexpertiseid = @expertiseId";
                    command.Parameters.AddWithValue("@expertiseId", expertiseId);
                }
                if (callType == NoProblem.Core.DataModel.Reports.CallsReport.CallType.DirectNumber)
                {
                    query += @" and (ia.new_isdirectnumber = 1 or inc.casetypecode = @type)";
                    command.Parameters.AddWithValue("@type", (int)DML.Xrm.incident.CaseTypeCode.DirectNumber);
                }
                else if (callType == NoProblem.Core.DataModel.Reports.CallsReport.CallType.SelfService)
                {
                    query += @" and (inc.casetypecode = @type or inc.casetypecode = @type2)";
                    command.Parameters.AddWithValue("@type", (int)DML.Xrm.incident.CaseTypeCode.Request);
                    command.Parameters.AddWithValue("@type2", (int)DML.Xrm.incident.CaseTypeCode.ScheduledRequest);
                }
                else if (callType == NoProblem.Core.DataModel.Reports.CallsReport.CallType.Click2)
                {
                    query += @" and (inc.casetypecode = @type)";
                    command.Parameters.AddWithValue("@type", (int)DML.Xrm.incident.CaseTypeCode.SupplierSpecified);
                }
                if (supplierOriginId != Guid.Empty)
                {
                    query += @" and inc.new_originid = @supplierOriginId ";
                    command.Parameters.AddWithValue("@supplierOriginId", supplierOriginId);
                }
                query += " order by ia.createdon desc ";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    command.Connection = connection;
                    command.CommandText = query;
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@from", fromDate);
                    command.Parameters.AddWithValue("@to", toDate);
                    command.Parameters.AddWithValue("@close", (int)DML.Xrm.new_incidentaccount.Status.CLOSE);
                    command.Parameters.AddWithValue("@sms", (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS);
                    command.Parameters.AddWithValue("@direct", (int)DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS);
                    command.Parameters.AddWithValue("@userId", userId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.Reports.CallData data = new NoProblem.Core.DataModel.Reports.CallData();
                            data.CaseNumber = ConvertorUtils.ToString(reader["ticketnumber"], string.Empty);
                            data.Date = (DateTime)reader["createdon"];
                            data.DefaultPrice = ConvertorUtils.ToDecimal(reader["new_predefinedprice"]);
                            data.Expertise = ConvertorUtils.ToString(reader["new_primaryexpertiseidname"], string.Empty);
                            data.Price = ConvertorUtils.ToDecimal(reader["new_potentialincidentprice"]);
                            data.Recording = ConvertorUtils.ToString(reader["new_recordfilelocation"], string.Empty);
                            data.Supplier = ConvertorUtils.ToString(reader["new_accountidname"], string.Empty);
                            data.IsAllowedRecording = ConvertorUtils.ToBoolean(reader["isAllowed"]);
                            data.SupplierNumber = ConvertorUtils.ToString(reader["accountnumber"], string.Empty);
                            data.SupplierId = (Guid)reader["new_accountid"];
                            data.Duration = reader["new_duration"] != DBNull.Value ? (int)reader["new_duration"] : 0;
                            data.Balance = reader["new_balanceafteraction"] != DBNull.Value ? (decimal)reader["new_balanceafteraction"] : 0;
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_incidentaccount> GetAllCalls()
        {
            IEnumerable<DML.Xrm.new_incidentaccount> retVal = from incAc in XrmDataContext.new_incidentaccounts
                                                              where
                                                              incAc.new_accountid.HasValue
                                                              &&
                                                              incAc.new_incidentid.HasValue
                                                              &&
                                                              (
                                                              incAc.statuscode == (int)DML.Xrm.new_incidentaccount.Status.CLOSE
                                                              || incAc.statuscode == (int)DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS
                                                              || incAc.statuscode == (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS
                                                              )
                                                              select
                                                              incAc;
            return retVal;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_incidentaccount> GetRefundsByRefundStatus(NoProblem.Core.DataModel.Xrm.new_incidentaccount.RefundSatus refundSatus)
        {
            int status = (int)refundSatus;
            IEnumerable<DML.Xrm.new_incidentaccount> retVal = from inac in XrmDataContext.new_incidentaccounts
                                                              where
                                                              inac.new_refundstatus.HasValue
                                                              &&
                                                              inac.new_refundstatus.Value == status
                                                              select inac;
            return retVal;
        }

        public DateTime GetLastWinDate(Guid supplierId)
        {
            DateTime retVal = DateTime.MinValue;
            StringBuilder query = new StringBuilder(
                @"select top 1 createdon
                from
                new_incidentaccount with (nolock)
                where
                deletionstatecode = 0
                and
                new_accountid = '");
            query.Append(supplierId.ToString());
            query.Append(
                @"'
                and
                (
                statuscode = ");
            query.Append(((int)DML.Xrm.new_incidentaccount.Status.CLOSE));
            query.Append(" or statuscode = ");
            query.Append(((int)DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS));
            query.Append(" or statuscode = ");
            query.Append(((int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS));
            query.Append(@") order by createdon desc");

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query.ToString(), connection))
                {
                    command.Connection.Open();
                    object scalar = command.ExecuteScalar();
                    if (scalar != null && scalar != DBNull.Value)
                    {
                        retVal = (DateTime)scalar;
                    }
                }
            }
            return retVal;
        }

        public void AddToSumSurvey(Guid incidentAccountId, bool isRemoveOne)
        {
            string query;
            if (isRemoveOne == false)
            {
                query = @"update account 
                set new_sumsurvey = case when new_sumsurvey is null then 1 else new_sumsurvey + 1 end
                where accountid = 
                (select new_accountid from new_incidentaccount with(nolock)
                where new_incidentaccountid = @id)";
            }
            else
            {
                query = @"update account 
                set new_sumsurvey = case when new_sumsurvey = 0 then 0 else new_sumsurvey - 1 end
                where accountid = 
                (select new_accountid from new_incidentaccount with(nolock)
                where new_incidentaccountid = @id)";
            }
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", incidentAccountId));
            ExecuteNonQuery(query, parameters);
        }

        public List<PreDefinedPriceData> GetAvgPricePayedInDates(DateTime fromDate, DateTime toDate, Guid headingId)
        {
            string query = @"
                select avg( new_potentialincidentprice ), convert(varchar,ia.createdon,111)
                from
                new_incidentaccount ia with(nolock)
                join incident inc with(nolock) on inc.incidentid = ia.new_incidentid
                where
                inc.new_primaryexpertiseid = @expId
                and ia.deletionstatecode = 0 and inc.deletionstatecode = 0
                and ia.new_tocharge = 1
                and ia.new_potentialincidentprice is not null
                and ia.createdon between @from and @to
                group by convert(varchar,ia.createdon,111)
                order by convert(varchar,ia.createdon,111)";
            List<PreDefinedPriceData> dataList = new List<PreDefinedPriceData>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@expId", headingId);
                    command.Parameters.AddWithValue("@from", fromDate);
                    command.Parameters.AddWithValue("@to", toDate);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PreDefinedPriceData data = new PreDefinedPriceData();
                            data.ActualPrice = (decimal)reader[0];
                            DateTime date = DateTime.Parse((string)reader[1]);
                            data.Date = date;
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }

        public List<DataModel.HelpDesk.RegardingObjectSearchData> SearchCallsFast(string input, Guid contactId, Guid incidentAccountId)
        {
            string query;
            if (incidentAccountId == Guid.Empty)
            {
                query =
                    @"select 
                top 100 
                ia.new_incidentaccountid, inc.ticketnumber, ia.new_accountidname, inc.new_createdonlocal, ia.createdon from
                new_incidentaccount ia with(nolock)
                join incident inc with(nolock) on inc.incidentid = ia.new_incidentid
                where
                ia.deletionstatecode = 0
                and inc.deletionstatecode = 0
                and 
                (
                ia.new_accountidname like @input
                or inc.ticketnumber like @input
                or inc.description like @input
                )
                and
                inc.customerid = @id
                and inc.ticketnumber is not null and ia.new_accountidname is not null
                order by createdon desc";
            }
            else
            {
                query =
                    @"select ia.new_incidentaccountid, inc.ticketnumber, ia.new_accountidname, inc.new_createdonlocal, ia.createdon
                    from new_incidentaccount ia with(nolock)
                    join incident inc with(nolock) on inc.incidentid = ia.new_incidentid
                    where
                    ia.deletionstatecode = 0
                    and inc.deletionstatecode = 0
                    and ia.new_incidentaccountid = @id";
            }
            List<DataModel.HelpDesk.RegardingObjectSearchData> dataList = new List<NoProblem.Core.DataModel.HelpDesk.RegardingObjectSearchData>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    if (incidentAccountId == Guid.Empty)
                    {
                        command.Parameters.AddWithValue("@input", "%" + input + "%");
                        command.Parameters.AddWithValue("@id", contactId);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@id", incidentAccountId);
                    }
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.HelpDesk.RegardingObjectSearchData data = new NoProblem.Core.DataModel.HelpDesk.RegardingObjectSearchData();
                            data.Id = (Guid)reader["new_incidentaccountid"];
                            data.Field1 = (string)reader["ticketnumber"];
                            data.Field2 = (string)reader["new_accountidname"];
                            object localObj = reader["new_createdonlocal"];
                            if (localObj != null && localObj != DBNull.Value)
                            {
                                data.Field3 = ((DateTime)localObj).ToShortDateString();
                            }
                            else
                            {
                                data.Field3 = ((DateTime)reader["createdon"]).ToShortDateString();
                            }
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }

        public NoProblem.Core.DataModel.SupplierModel.SupplierIncidentDataExpanded GetSupplierIncidentData(Guid callId)
        {
            string query =
                @"select
                ia.new_isjobdone
                ,inc.description
                ,ia.new_potentialincidentprice
                ,ia.new_refundnote
                ,ia.new_refundnoteforadvertiser
                ,inc.new_winningprice
                ,case when ia.new_tocharge = 1 then inc.new_telephone1 else '' end as ConsumerPhone
                ,inc.customerid
                ,ia.new_refundstatus
                ,ia.statuscode
                ,rr.new_code
                ,inc.incidentid
                ,inc.ticketnumber
                ,ia.new_tocharge
                ,acc.new_isrefundblocked
                ,acc.accountid
                ,ia.createdon
                ,ia.new_recordfilelocation
                ,acc.new_recordcalls
                from
                new_incidentaccount ia with(nolock)
                join incident inc with(nolock) on inc.incidentid = ia.new_incidentid
                join account acc with(nolock) on acc.accountid = ia.new_accountid
                left join new_refundreason rr with(nolock) on rr.new_refundreasonid = ia.new_refundreasonid
                where
                ia.deletionstatecode = 0 and inc.deletionstatecode = 0
                and
                ia.new_incidentaccountid = @id";
            DataModel.SupplierModel.SupplierIncidentDataExpanded data = new NoProblem.Core.DataModel.SupplierModel.SupplierIncidentDataExpanded();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", callId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            data.CustomerId = (Guid)reader["customerid"];
                            data.CustomerPhoneNumber = (string)reader["ConsumerPhone"];
                            data.Description = reader["description"] == DBNull.Value ? string.Empty : (string)reader["description"];
                            data.IsJobDone = reader["new_isjobdone"] == DBNull.Value ? new bool?() : (bool)reader["new_isjobdone"];
                            data.Price = (decimal)reader["new_potentialincidentprice"];
                            data.RefundNote = reader["new_refundnote"] == DBNull.Value ? string.Empty : (string)reader["new_refundnote"];
                            data.RefundNoteForAdvertiser = reader["new_refundnoteforadvertiser"] == DBNull.Value ? string.Empty : (string)reader["new_refundnoteforadvertiser"];
                            data.WinningPrice = reader["new_winningprice"] == DBNull.Value ? new decimal() : (decimal)reader["new_winningprice"];
                            data.ToCharge = (bool)reader["new_tocharge"];
                            object refundStatusObj = reader["new_refundstatus"];
                            if (refundStatusObj != DBNull.Value)
                            {
                                int refundStatusInt = (int)refundStatusObj;
                                if (refundStatusInt != 0)
                                {
                                    data.RefundStatus = (DML.Xrm.new_incidentaccount.RefundSatus)refundStatusInt;
                                }
                            }
                            DML.Xrm.new_incidentaccount.Status status = (DML.Xrm.new_incidentaccount.Status)((int)reader["statuscode"]);
                            if (status == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSE
                                || status == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS
                                || status == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.INVITED_BY_SMS)
                            {
                                data.Won = true;
                            }
                            else
                            {
                                data.Won = false;
                            }
                            data.RefundReason = reader["new_code"] != DBNull.Value ? (int)reader["new_code"] : new int?();
                            data.IncidentId = (Guid)reader["incidentid"];
                            data.TicketNumber = (string)reader["ticketnumber"];
                            data.CreatedOn = (DateTime)reader["createdon"];
                            data.IsRefundBlocked = reader["new_isrefundblocked"] != DBNull.Value ? (bool)reader["new_isrefundblocked"] : false;
                            data.HasRecording = reader["new_recordfilelocation"] != DBNull.Value && string.IsNullOrEmpty((string)reader["new_recordfilelocation"]) == false;
                            //data.IsCallChannel = (int)reader["IsCallChannel"] == 1;
                            //data.IsInvitedBySMS = status == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.INVITED_BY_SMS;
                            data.RecordsCalls = reader["new_recordcalls"] != DBNull.Value ? (bool)reader["new_recordcalls"] : false;
                            data.SupplierId = (Guid)reader["accountid"];
                        }
                    }
                }
            }
            return data;
        }

        public List<DataModel.Reports.RefundReportRow> RefundReportQuery(DateTime fromDate, DateTime toDate, Guid headingId, int pageSize, int pageNumber, out int totalRows)
        {
            totalRows = 0;
            List<DataModel.Reports.RefundReportRow> retVal =
                new List<DataModel.Reports.RefundReportRow>();
            using (SqlCommand command = new SqlCommand())
            {
                string query =
                    @"                    
WITH PagerTbl AS
                (                
                SELECT ROW_NUMBER() OVER(ORDER BY dateadd(day, datediff(day, 0, ia.new_createdonlocal),0)) AS RowNum,
dateadd(day, datediff(day, 0, ia.new_createdonlocal),0) as date
, count(*) soldCalls
, count(ia.new_refundstatus) refundRequests
, sum(case when ia.new_refundstatus = @approved then 1 else 0 end) approved
, case when count(*) = 0 then 0 else cast(sum(case when ia.new_refundstatus = 3 then 1 else 0 end) as real) / cast(count(*) AS REAL) * 100 end as approvedFromCalls
, case when count(*) = 0 then 0 else cast(count(ia.new_refundstatus) as real) / cast(count(*) AS REAL) * 100 end as requestedFromCalls
, case when count(ia.new_refundstatus) = 0 then 0 else cast(sum(case when ia.new_refundstatus = 3 then 1 else 0 end) as real) / cast(count(ia.new_refundstatus) as real) * 100 end as approvedFromRequested
from
new_incidentaccount ia with(nolock)
join incident inc with(nolock) on inc.incidentid = ia.new_incidentid
where
ia.deletionstatecode = 0 and inc.deletionstatecode = 0
and ia.new_createdonlocal between @from and @to
and (ia.statuscode = @closed or ia.statuscode = @directClosed or ia.statuscode = @invited) 
and not(ia.new_refundstatus is null and ia.new_tocharge = 0)
";
                if (headingId != Guid.Empty)
                {
                    query += " and inc.new_primaryexpertiseid = @headingId ";
                    command.Parameters.AddWithValue("@headingId", headingId);
                }
                query += @"
    GROUP BY dateadd(day, datediff(day, 0, ia.new_createdonlocal),0)
)
SELECT * , (select max(RowNum) from PagerTbl) as totalRows
                      FROM PagerTbl
                     WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
                                      AND @PageNum * @PageSize)
                            or @PageSize = -1)
                     ORDER BY RowNum
";
                command.Parameters.AddWithValue("@closed", (int)DML.Xrm.new_incidentaccount.Status.CLOSE);
                command.Parameters.AddWithValue("@directClosed", (int)DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS);
                command.Parameters.AddWithValue("@invited", (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS);
                command.Parameters.AddWithValue("@approved", (int)DML.Xrm.new_incidentaccount.RefundSatus.Approved);
                command.Parameters.AddWithValue("@from", fromDate);
                command.Parameters.AddWithValue("@to", toDate);
                command.Parameters.AddWithValue("@PageNum", pageNumber);
                command.Parameters.AddWithValue("@PageSize", pageSize);
                command.CommandText = query;
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    command.Connection = conn;
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Reports.RefundReportRow data = new RefundReportRow();
                            data.Date = (DateTime)reader["date"];
                            data.CallsWon = (int)reader["soldCalls"];
                            data.AmountOfRefundRequests = (int)reader["refundRequests"];
                            data.AmountOfApprovedRequests = (int)reader["approved"];
                            data.PercentApprovedFromCalls = (float)reader["approvedFromCalls"];
                            data.PercentApprovedFromRequests = (float)reader["approvedFromRequested"];
                            data.PercentRefundRequestsOfCalls = (float)reader["requestedFromCalls"];
                            if (totalRows == 0)
                            {
                                totalRows = unchecked((int)((long)reader["totalRows"]));
                            }
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public void UpdateDtmfPressed(Guid incidentAccountId, string digits)
        {
            string query =
                @"update new_incidentaccount
                set new_dtmfpressed = @digits
                where new_incidentaccountid = @callId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@digits", digits));
            parameters.Add(new KeyValuePair<string, object>("@callId", incidentAccountId));
            ExecuteNonQuery(query, parameters);
        }

        public string GetConsumerPhone(Guid incidentAccountId)
        {
            string query =
                @"select co.mobilephone
                from contact co with(nolock)
                join incident inc with(nolock) on co.contactid = inc.customerid
                join new_incidentaccount ia with(nolock) on inc.incidentid = ia.new_incidentid
                where
                ia.new_incidentaccountid = @callId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@callId", incidentAccountId));
            string phone = (string)ExecuteScalar(query, parameters);
            return phone;
        }

        public void UpdateCallToInConversation(Guid incidentAccountId)
        {
            string query =
                @"update new_incidentaccount
                set statuscode = 15
                where new_incidentaccountid = @callId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@callId", incidentAccountId));
            ExecuteNonQuery(query, parameters);
        }

        public void UpdateRecordFileLocation(Guid incidentAccountId, string recordingUrl, int duration)
        {
            string query =
                @"update new_incidentaccount
                set new_recordfilelocation = @url, new_recordduration = @duration
                where new_incidentaccountid = @id";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@url", recordingUrl));
            parameters.Add(new KeyValuePair<string, object>("@id", incidentAccountId));
            parameters.Add(new KeyValuePair<string, object>("@duration", duration));
            ExecuteNonQuery(query, parameters);
        }
        public void UpdateRecordFileLocation(Guid incidentAccountId, string recordingUrl, int duration, string endStatus)
        {
            string query =
                @"update New_incidentaccountExtensionBase
set new_recordfilelocation = @url, new_recordduration = @duration,
	New_firstCallStatus = @endCallStatus
where new_incidentaccountid = @id";
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@id", incidentAccountId);
                        if(!string.IsNullOrEmpty(recordingUrl))
                            cmd.Parameters.AddWithValue("@url", recordingUrl);
                        else
                            cmd.Parameters.AddWithValue("@url", DBNull.Value);
                        if (duration > 0)
                            cmd.Parameters.AddWithValue("@duration", duration);
                        else
                            cmd.Parameters.AddWithValue("@duration", DBNull.Value);
                        cmd.Parameters.AddWithValue("@endCallStatus", endStatus);
                        cmd.ExecuteNonQuery();

                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {

            }
                 /*
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@url", recordingUrl));
            parameters.Add(new KeyValuePair<string, object>("@id", incidentAccountId));
            parameters.Add(new KeyValuePair<string, object>("@duration", duration));
            ExecuteNonQuery(query, parameters);
                  * */
        }


        public int GetNumberOfSupplierInBatch(Guid headingId)
        {
            string query =
                @"select
case when sum(Case new_dtmfpressed when '1' then 1 else 0 end) > 0 then 
(
CAST(
cast(sum(Case new_dtmfpressed when '1' then 0 else 1 end) as real)
/ cast(sum(Case new_dtmfpressed when '1' then 1 else 0 end) as real) as int)
  ) 
  else
  -1 end as 'Call to get a one'
from
New_incidentaccount ia
join Incident inc on inc.IncidentId = ia.new_incidentid
where ia.DeletionStateCode = 0
and inc.DeletionStateCode = 0
and inc.new_primaryexpertiseid = @headingId
and ia.New_IsAARCall = 1";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@headingId", headingId));
            int retVal = (int)ExecuteScalar(query, parameters);
            return retVal;
        }

        public string GetTicketNumber(Guid incidentAccountId)
        {
            string query =
                @"select ticketnumber from new_incidentaccount with(nolock)
                join incident with(nolock) on incidentid = new_incidentid
                where new_incidentaccount.new_incidentaccountid = @id";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", incidentAccountId));
            object scalar = ExecuteScalar(query, parameters);
            string retVal = null;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        public List<Guid> GetNonDoneIncidentAccount(Guid incidentId)
        {
            string query =
                @"select new_incidentaccountid
                from new_incidentaccount ia with(nolock)
                where 
                ia.new_incidentid = @incidentId
                and ia.statuscode != 16 and ia.statuscode != 10 and ia.statuscode != 12";//16 is lost, 10 is close, 12 is sms
            List<KeyValuePair<string, object>> paramters = new List<KeyValuePair<string, object>>();
            paramters.Add(new KeyValuePair<string, object>("@incidentId", incidentId));
            var reader = ExecuteReader(query, paramters);
            List<Guid> retVal = new List<Guid>();
            foreach (var row in reader)
            {
                retVal.Add((Guid)row["new_incidentaccountid"]);
            }
            return retVal;
        }
        public List<Guid> GetNonDoneMobileIncidentAccount(Guid incidentId)
        {
            string query =
                @"select new_incidentaccountid
                from new_incidentaccount ia with(nolock)
                where 
                ia.new_incidentid = @incidentId
                and ia.statuscode in(1,14)";//1 MATCH, 14 IN_AUCTION
            List<KeyValuePair<string, object>> paramters = new List<KeyValuePair<string, object>>();
            paramters.Add(new KeyValuePair<string, object>("@incidentId", incidentId));
            var reader = ExecuteReader(query, paramters);
            List<Guid> retVal = new List<Guid>();
            foreach (var row in reader)
            {
                retVal.Add((Guid)row["new_incidentaccountid"]);
            }
            return retVal;
        }
      

        public List<Guid> GetNonDoneAndNotAcceptingNormalAdvertisers(Guid incidentId)
        {
            string query =
                @"select new_incidentaccountid
                from new_incidentaccount ia with(nolock)
                join account acc with(nolock) on acc.accountid = ia.new_accountid
                where 
                ia.new_incidentid = @incidentId
                and ia.new_leadaccepted = 0 
                and acc.new_isleadbuyer = 0
                and ia.statuscode != 16 and ia.statuscode != 10 and ia.statuscode != 12";//16 is lost, 10 is close, 12 is sms
            List<KeyValuePair<string, object>> paramters = new List<KeyValuePair<string, object>>();
            paramters.Add(new KeyValuePair<string, object>("@incidentId", incidentId));
            var reader = ExecuteReader(query, paramters);
            List<Guid> retVal = new List<Guid>();
            foreach (var row in reader)
            {
                retVal.Add((Guid)row["new_incidentaccountid"]);
            }
            return retVal;
        }

        public bool ClosedAtLeastOnce(Guid incidentId)
        {
            string query =
                @"select top 1 1
                from new_incidentaccount ia with(nolock)
                where 
                ia.new_incidentid = @incidentId
                and (ia.statuscode = 10 or ia.statuscode = 12)";//10 is close, 12 is sms
            List<KeyValuePair<string, object>> paramters = new List<KeyValuePair<string, object>>();
            paramters.Add(new KeyValuePair<string, object>("@incidentId", incidentId));
            object scalar = ExecuteScalar(query, paramters);
            return scalar != null;
        }
        public void SetClosedOn(Guid IncidentAccountId)
        {
            string command =
@"UPDATE [dbo].[New_incidentaccountExtensionBase]
SET New_ClosedOn = GETDATE()
WHERE New_incidentaccountId = @IncidentAccountId";
            using(SqlConnection conn = new SqlConnection(Generic.connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@IncidentAccountId", IncidentAccountId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
        #region Mobile
        public List<Guid> GetIncidentAccountWaitingAfterBiddingMobile(Guid incidentId)
        {
            List<Guid> retVal = new List<Guid>();
            string query =
                @"SELECT new_incidentaccountid
FROM new_incidentaccount ia with(nolock)
where ia.new_incidentid = @incidentId
and ia.statuscode = @InAuction and ia.New_BidDone = 1";
            using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", incidentId);
                    cmd.Parameters.AddWithValue("@InAuction", (int)DML.Xrm.new_incidentaccount.Status.IN_AUCTION);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        retVal.Add((Guid)reader["new_incidentaccountid"]);
                    }
                    conn.Close();
                }
            }

            return retVal;
        }
        /*
        public bool IsMobileIncidentInConversation(Guid IncidentAccountId)
        {
        }
         * */
        //DML.Xrm.new_incidentaccount
        public void SetMobileCallProcess(Guid IncidentAccountId, DML.Xrm.new_incidentaccount.CallProcess StatusCallProcess)
        {
            string command = @"UPDATE dbo.New_incidentaccountExtensionBase
SET New_callProcess = @callProcess
	{WHERE}
WHERE New_incidentaccountId = @IncidentAccountId";
            string where = ", New_mobileInConversationDate = @date";
            using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@IncidentAccountId", IncidentAccountId);
                    cmd.Parameters.AddWithValue("@callProcess", (int)StatusCallProcess);
                    string query;
                    if (StatusCallProcess == DML.Xrm.new_incidentaccount.CallProcess.InConversation)
                    {
                        query = command.Replace("{WHERE}", where);
                        cmd.Parameters.AddWithValue("@date", DateTime.UtcNow);
                    }
                    else
                        query = command.Replace("{WHERE}", string.Empty);
                    cmd.Connection = conn;
                    cmd.CommandText = query;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
        public void SetCallTracing(Guid incidentAccountId, DML.Xrm.new_incidentaccount.CallTracing callTracing)
        {
             string command = @"
UPDATE dbo.New_incidentaccountExtensionBase
SET New_callTracing = @callTracing
WHERE New_incidentaccountId = @incidentAccountId";
             try
             {
                 using (SqlConnection conn = new SqlConnection(Generic.GetConnectionString))
                 {
                     using (SqlCommand cmd = new SqlCommand(command, conn))
                     {
                         conn.Open();
                         cmd.Parameters.AddWithValue("@incidentAccountId", incidentAccountId);
                         cmd.Parameters.AddWithValue("@callTracing", (int)callTracing);
                         cmd.ExecuteNonQuery();
                         conn.Close();
                     }
                 }
             }
             catch (Exception exc)
             {
                 Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed IncidentAccount - SetCallTracing incidentAccountId = {0}, callTracing = {1}", incidentAccountId, callTracing);
             }
        }
        
        #endregion
    }
}
