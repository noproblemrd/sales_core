﻿using System;

namespace NoProblem.Core.DataAccessLayer.Autonumber
{
    public class AutonumberManager : Generic
    {
        public string GetNextNumber(string sequenceName)
        {
            string tableName = GetTableName(sequenceName);
            string query = String.Format(queryTemplate, tableName);
            decimal nextNumber = (decimal)ExecuteScalar(query);
            return nextNumber.ToString();
        }

        private string GetTableName(string sequenceName)
        {
            return String.Format(tableNameTemplate, sequenceName);
        }

        private const string tableNameTemplate = "sequence_{0}";

        private const string queryTemplate =
            @"
                insert into dbo.{0} default values;
                select SCOPE_IDENTITY() option (maxdop 1)
            ";

        //protected readonly string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];

//        public int GetNextNumber(string entityName)
//        {
//            return GetNextNumber(entityName, 1);
//        }

//        private int GetNextNumber(string entityName, int tries)
//        {
//            int nextValue = 0;
//            //try
//            //{
//            //                string getNextNumberQuery = @"
//            //                SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
//            //                BEGIN TRAN
//            //                declare @id int
//            //                SELECT @id = new_nextvalue
//            //                         FROM new_autonumberextensionbase WITH (ROWLOCK)
//            //                         WHERE new_entityname = '{0}'
//            //                UPDATE new_autonumberextensionbase SET new_nextvalue = @id + 1 
//            //                        where
//            //                        new_entityname = '{0}'
//            //                select @id as new_nextvalue
//            //                COMMIT TRAN";

//            string getNextNumberQuery = @"
//                SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
//                BEGIN TRAN
//                declare @id int
//				UPDATE new_autonumberextensionbase SET new_nextvalue = 
//				(select new_nextvalue from new_autonumberextensionbase where new_entityname = '{0}') + 1 
//                        where
//                        new_entityname = '{0}'
//                SELECT @id = new_nextvalue
//                         FROM new_autonumberextensionbase
//                         WHERE new_entityname = '{0}'                
//                select @id as new_nextvalue
//                COMMIT TRAN";

//            using (SqlConnection connection = new SqlConnection(connectionString))
//            {
//                using (SqlCommand command = new SqlCommand(string.Format(getNextNumberQuery, entityName), connection))
//                {
//                    command.Connection.Open();
//                    using (SqlDataReader getNextNumber = command.ExecuteReader())
//                    {
//                        if (getNextNumber.Read())
//                        {
//                            if (getNextNumber["new_nextvalue"] != DBNull.Value)
//                                nextValue = int.Parse(getNextNumber["new_nextvalue"].ToString());
//                        }
//                    }
//                }
//            }
//            //}
//            //catch (Exception exc)
//            //{
//            //    if (exc.Message.Contains("deadlocked") && tries < 5)
//            //    {
//            //        nextValue = GetNextNumber(entityName, tries + 1);
//            //    }
//            //    else
//            //    {
//            //        throw;
//            //    }
//            //}
//            return nextValue;
//        }

        
    }
}
