﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer.Yelp
{
    public class YelpAARCall : DalBase<DataModel.Xrm.new_yelpaarcall>
    {
        public YelpAARCall(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_yelpaarcall");
        }

        public override DataModel.Xrm.new_yelpaarcall Retrieve(Guid id)
        {
            return XrmDataContext.new_yelpaarcalls.FirstOrDefault(x => x.new_yelpaarcallid == id);
        }

        public override IQueryable<DataModel.Xrm.new_yelpaarcall> All
        {
            get
            {
                return XrmDataContext.new_yelpaarcalls;
            }
        }
    }
}
