﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer.Yelp
{
    public class YelpSearchResult : DalBase<DataModel.Xrm.new_yelpsearchresult>
    {
        public YelpSearchResult(DataModel.Xrm.DataContext xrmDataContext) :
            base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_yelpsearchresult");
        }
        public override DataModel.Xrm.new_yelpsearchresult Retrieve(Guid id)
        {
            return XrmDataContext.new_yelpsearchresults.FirstOrDefault(x => x.new_yelpsearchresultid == id);
        }
    }
}
