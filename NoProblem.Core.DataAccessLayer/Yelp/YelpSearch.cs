﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer.Yelp
{
    public class YelpSearch : DalBase<DataModel.Xrm.new_yelpsearch>
    {
        public YelpSearch(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_yelpsearch");
        }

        public override DataModel.Xrm.new_yelpsearch Retrieve(Guid id)
        {
            return XrmDataContext.new_yelpsearches.FirstOrDefault(x => x.new_yelpsearchid == id);
        }

        public override IQueryable<DataModel.Xrm.new_yelpsearch> All
        {
            get
            {
                return XrmDataContext.new_yelpsearches;
            }
        }
    }
}
