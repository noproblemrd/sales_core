﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer.Yelp
{
    public class YelpSupplier : DalBase<DataModel.Xrm.new_yelpsupplier>
    {
        public YelpSupplier(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_yelpsupplier");
        }

        public override DataModel.Xrm.new_yelpsupplier Retrieve(Guid id)
        {
            return XrmDataContext.new_yelpsuppliers.FirstOrDefault(x => x.new_yelpsupplierid == id);
        }

        public override IQueryable<DataModel.Xrm.new_yelpsupplier> All
        {
            get
            {
                return XrmDataContext.new_yelpsuppliers;
            }
        }
    }
}
