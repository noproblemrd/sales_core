﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class UnmappedDnQuery : DalBase<DataModel.Xrm.new_unmappeddnquery>
    {
        public UnmappedDnQuery(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public override NoProblem.Core.DataModel.Xrm.new_unmappeddnquery Retrieve(Guid id)
        {
            return XrmDataContext.new_unmappeddnquerynew_unmappeddnqueries.FirstOrDefault(x => x.new_unmappeddnqueryid == id);
        }
    }
}
