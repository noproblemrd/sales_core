﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.AdvertiserDashboards;

namespace NoProblem.Core.DataAccessLayer
{
    public class GetStartedTestCall : DalBase<DataModel.Xrm.new_getstartedtestcall>
    {
        public GetStartedTestCall(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_getstartedtestcall");
        }

        public override NoProblem.Core.DataModel.Xrm.new_getstartedtestcall Retrieve(Guid id)
        {
            return XrmDataContext.new_getstartedtestcalls.FirstOrDefault(x => x.new_getstartedtestcallid == id);
        }

        public GetStartedTestCallLeadData GetDoneTestCallOfSupplier(Guid supplierId)
        {
            GetStartedTestCallLeadData data = null;
            string query =@"
            declare @timeZone int
select top 1 @timeZone = address1_utcoffset from account with(nolock) where accountid = @supplierId

select 
top 1 
new_getstartedtestcallid
, rg1.new_name rg1
, rg2.new_name rg2
, rg3.new_name rg3
, rg4.new_name rg4
, pe.new_ivrname
, @timeZone as timezone
, tc.new_recordingurl
, tc.createdon


from new_getstartedtestcall tc with(nolock)
join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = tc.new_primaryexpertiseid
join New_region rg1 with(nolock) on tc.new_regionid = rg1.New_regionId
left join New_region rg2 with(nolock) on rg1.new_parentregionid = rg2.new_regionid
left join New_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionid
left join New_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionid

where
tc.deletionstatecode = 0
and tc.new_accountid = @supplierId
and tc.new_taskdone = 1
and (tc.new_closedfromreport is null or tc.new_closedfromreport = 0)

order by tc.createdon desc
    ";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            var reader = ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                data = new GetStartedTestCallLeadData();
                data.HeadingIvrName = Convert.ToString(row["new_ivrname"]);
                data.Id = (Guid)row["new_getstartedtestcallid"];
                data.RecordingUrl = Convert.ToString(row["new_recordingurl"]);
                List<string> regions = new List<string>();
                for (int i = 1; i <= 4; i++)
                {
                    string rg = Convert.ToString(row["rg" + i]);
                    if (!string.IsNullOrEmpty(rg))
                    {
                        regions.Add(rg);
                    }
                    else
                    {
                        break;
                    }
                }
                data.RegionsHirarchy = regions;
                data.TimeZoneCode = (int)row["timezone"];
                data.CreatedOn = (DateTime)row["createdon"];
                break;
            }
            return data;
        }

        public void CloseTestCallFromReport(Guid supplierId)
        {
            var calls = XrmDataContext.new_getstartedtestcalls.Where(
                x => x.new_accountid == supplierId
                    && x.new_taskdone == true);
            foreach (var item in calls)
            {
                item.new_closedfromreport = true;
                Update(item);
            }
            XrmDataContext.SaveChanges();
        }
    }
}
