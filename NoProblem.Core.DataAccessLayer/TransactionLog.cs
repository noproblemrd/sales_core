﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class TransactionLog : DalBase<DataModel.Xrm.new_transactionlog>
    {
        #region Ctor
        public TransactionLog(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_transactionlog");
        }  
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_transactionlog Retrieve(Guid id)
        {
            DataModel.Xrm.new_transactionlog log = XrmDataContext.new_transactionlogs.FirstOrDefault(x => x.new_transactionlogid == id);
            return log;
        }
    }
}
