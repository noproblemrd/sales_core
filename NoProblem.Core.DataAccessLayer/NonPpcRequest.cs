﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class NonPpcRequest : DalBase<DataModel.Xrm.new_nonppcrequest>
    {
        #region Ctor
        public NonPpcRequest(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_nonppcrequest");
        } 
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_nonppcrequest Retrieve(Guid id)
        {
            DataModel.Xrm.new_nonppcrequest request = XrmDataContext.new_nonppcrequests.FirstOrDefault(x => x.new_nonppcrequestid == id);
            return request;
        }

        public NoProblem.Core.DataModel.Xrm.new_nonppcrequest GetNonPpcRequestByRecordId(string callId)
        {
            DataModel.Xrm.new_nonppcrequest request = XrmDataContext.new_nonppcrequests.FirstOrDefault(x => x.new_recordid == callId);
            return request;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_nonppcrequest> GetNonPpcRequestsByDates(DateTime fromDate, DateTime toDate)
        {
            IEnumerable<NoProblem.Core.DataModel.Xrm.new_nonppcrequest> retVal = XrmDataContext.new_nonppcrequests;
            retVal = from item in retVal
                     where
                     item.createdon.Value.CompareTo(fromDate) > 0
                     &&
                     item.createdon.Value.CompareTo(toDate) < 0
                     orderby item.createdby.Value descending
                     select
                     item;
            return retVal;
        }
    }
}
