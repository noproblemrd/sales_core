﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class ToolbarId : DalBase<DataModel.Xrm.new_toolbarid>
    {
        public ToolbarId(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_toolbarid");
        }

        public override NoProblem.Core.DataModel.Xrm.new_toolbarid Retrieve(Guid id)
        {
            return XrmDataContext.new_toolbarids.FirstOrDefault(x => x.new_toolbaridid == id);
        }

        public Guid GetToolbarIdByName(string name, Guid originId)
        {
            string query =
                @"select new_toolbaridid from new_toolbarid with(nolock)
                    where new_name = @name and new_originid = @originId and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@name", name));
            parameters.Add(new KeyValuePair<string, object>("@originId", originId));
            object scalar = ExecuteScalar(query, parameters);
            Guid retVal = Guid.Empty;
            if (scalar != null)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }
        public List<DataModel.PublisherPortal.GuidStringPair> GetToolbaridByOrigin(Guid OriginId)
        {
            List<DataModel.PublisherPortal.GuidStringPair> list = new List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>();
            string query = "select New_toolbaridId, New_name " +
                            "from dbo.New_toolbarid with(nolock) " +
                            "where new_originid = @OriginId " + 
                            "order by New_name ";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@OriginId", OriginId));
            List<Dictionary<string, object>> reader = ExecuteReader(query, parameters);
            foreach (Dictionary<string, object> dic in reader)
            {
                Guid tbi = (Guid)dic["New_toolbaridId"];
                string tbName = (dic["New_name"] == DBNull.Value) ? string.Empty : (string)dic["New_name"];
                list.Add(new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair(tbName, tbi));
            }
            return list;
        }
    }
}
