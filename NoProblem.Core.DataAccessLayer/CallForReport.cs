﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class CallForReport : DalBase<DataModel.Xrm.new_callforreport>
    {
        public CallForReport(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_callforreport");
        }

        public override NoProblem.Core.DataModel.Xrm.new_callforreport Retrieve(Guid id)
        {
            return XrmDataContext.new_callforreports.FirstOrDefault(x => x.new_callforreportid == id);
        }
    }
}
