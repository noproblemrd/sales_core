﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class Injection : DalBase<DataModel.Xrm.new_injection>
    {
        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public Injection(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_injection");
        }

        public override DataModel.Xrm.new_injection Retrieve(Guid id)
        {
            return XrmDataContext.new_injections.FirstOrDefault(x => x.new_injectionid == id);
        }

        public override IQueryable<DataModel.Xrm.new_injection> All
        {
            get
            {
                return XrmDataContext.new_injections;
            }
        }

        public Guid GetIdByName(string name)
        {
            string idStr = CacheManager.Instance.Get<string>(cachePrefix + ".GetIdByName", name, GetIdByNameMethod, CacheManager.ONE_DAY);
            return new Guid(idStr);
        }

        private string GetIdByNameMethod(string name)
        {
            var query = from x in All
                        where x.new_name == name
                        select x.new_injectionid;
            Guid id = query.FirstOrDefault();
            return id.ToString();
        }
    }
}
