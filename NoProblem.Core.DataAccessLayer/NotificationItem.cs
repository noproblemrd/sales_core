﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class NotificationItem : DalBase<DataModel.Xrm.new_notificationitem>
    {
        public NotificationItem(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_notificationitem");
        }

        public override NoProblem.Core.DataModel.Xrm.new_notificationitem Retrieve(Guid id)
        {
            DataModel.Xrm.new_notificationitem item = XrmDataContext.new_notificationitems.FirstOrDefault(x => x.new_notificationitemid == id);
            return item;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_notificationitem> GetItemsToSend()
        {
            IEnumerable<DML.Xrm.new_notificationitem> retVal =
                from item in XrmDataContext.new_notificationitems
                where
                item.new_timetosend.Value < DateTime.Now
                select
                item;
            return retVal;
        }
    }
}
