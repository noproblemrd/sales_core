﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class PingPostTransaction : MongoDalBase<DataModel.APIs.SellersApi.PingPost.PingPostTransaction>
    {
        private const string COLLECTION_NAME = "PingPostTransactions";

        public override string CollectionName
        {
            get { return COLLECTION_NAME; }
        }
    }
}
