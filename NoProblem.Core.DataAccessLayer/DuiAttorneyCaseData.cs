﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class DuiAttorneyCaseData : DalBase<DataModel.Xrm.new_duiattorneycasedata>
    {
        public DuiAttorneyCaseData(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_duiattorneycasedata");
        }

        public override NoProblem.Core.DataModel.Xrm.new_duiattorneycasedata Retrieve(Guid id)
        {
            return XrmDataContext.new_duiattorneycasedatas.FirstOrDefault(x => x.new_duiattorneycasedataid == id);
        }
    }
}
