﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class Invitation : DalBase<DataModel.Xrm.new_invitation>
    {
        public Invitation(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_invitation");
        }

        public override NoProblem.Core.DataModel.Xrm.new_invitation Retrieve(Guid id)
        {
            return XrmDataContext.new_invitations.FirstOrDefault(x => x.new_invitationid == id);
        }

//        public int GetNotLostInvitationCount(Guid supplierId)
//        {
//            string query =
//                @"select count(*) from new_invitation with(nolock)
//                where deletionstatecode = 0
//                and new_accountregisteredid = @supplierId
//                and statuscode != @lost";
//            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
//            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
//            parameters.Add(new KeyValuePair<string, object>("@lost", (int)DataModel.Xrm.new_invitation.InvitationStatus.Lost));
//            int invitationCount = (int)ExecuteScalar(query, parameters);
//            return invitationCount;
//        }

        public bool IsAlreadyInvited(string invitedEmail)
        {
            string query =
                @"select top 1 new_invitationid
                from new_invitation with(nolock)
                where deletionstatecode = 0
                and new_invitedemail = @email
                and statuscode != @lost";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@email", invitedEmail));
            parameters.Add(new KeyValuePair<string, object>("@lost", (int)DataModel.Xrm.new_invitation.InvitationStatus.Lost));
            object scalar = ExecuteScalar(query, parameters);
            bool exists = false;
            if (scalar != null && scalar != DBNull.Value)
            {
                exists = true;
            }
            return exists;
        }

        public DataModel.Xrm.new_invitation FindLinkedInvitation(Guid supplierId)
        {
            int linked = (int)DataModel.Xrm.new_invitation.InvitationStatus.Linked;
            return XrmDataContext.new_invitations.FirstOrDefault(
                x => x.new_accountinvitedid.Value == supplierId
                    && x.statuscode == linked);
        }

        public DataModel.Xrm.new_invitation FindPendingInvitationByEmail(string email)
        {
            int pending = (int)DataModel.Xrm.new_invitation.InvitationStatus.Pending;
            return XrmDataContext.new_invitations.FirstOrDefault(
                x => x.statuscode == pending
                    && x.new_invitedemail == email);
        }

        public DataModel.Xrm.new_invitation FindInvitationbyReferralCode(string code)
        {
            return XrmDataContext.new_invitations.FirstOrDefault(
                x => x.new_referralcode == code);
        }

        public NoProblem.Core.DataModel.Xrm.new_invitation FindInviteeSuccessInvitation(Guid inviteeId)
        {
            int success = (int)DataModel.Xrm.new_invitation.InvitationStatus.Success;
            return XrmDataContext.new_invitations.FirstOrDefault(
                x => x.statuscode == success
                    && x.new_accountinvitedid == inviteeId);
        }
    }
}
