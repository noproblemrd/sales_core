﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class Origin : DalBase<DML.Xrm.new_origin>
    {
        #region Ctor
        public Origin(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_origin");
        }
        #endregion

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public override IQueryable<DML.Xrm.new_origin> All
        {
            get
            {
                return XrmDataContext.new_origins;
            }
        }

        public override NoProblem.Core.DataModel.Xrm.new_origin Retrieve(Guid id)
        {
            DML.Xrm.new_origin origin = XrmDataContext.new_origins.FirstOrDefault(x => x.new_originid == id);
            return origin;
        }

        public bool IsExists(Guid originId)
        {
            string query = @"select top 1 1
                from new_origin with(nolock)
                 where new_originid = @id
                and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", originId));
            object scalar = ExecuteScalar(query, parameters);
            return scalar != null;
        }

        public Guid GetFirstOriginId()
        {
            string query = @"select top 1 new_originId from 
                new_origin with(nolock)
                where deletionstatecode = 0";
            object originObj = ExecuteScalar(query);
            return (Guid)originObj;
        }

        public Guid GetOriginIdByCode(string code)
        {
            string query = @"select new_originid from new_origin with(nolock) where new_code = @code and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@code", code));
            object scalar = ExecuteScalar(query, parameters);
            Guid retVal = Guid.Empty;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        public IEnumerable<Guid> GetAllDirectNumbersOrigins()
        {
            string query =
                @"select new_originid from new_origin with(nolock)
                    where deletionstatecode = 0 and new_isdirectnumber = 1";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public IEnumerable<Guid> GetAccountExpertisesThatNeedDirectNumber(Guid supplierId, Guid originId, Guid regionId)
        {
            string query =
               @"SELECT new_accountexpertiseid
                FROM new_accountexpertise with(nolock)
                WHERE deletionstatecode = 0 AND statecode = 0 
                AND new_accountid = @supplierId
                And new_accountexpertiseid not in(
                select new_accountexpertiseid
                from new_directnumber with(nolock)
                where deletionstatecode = 0 and new_accountexpertiseid is not null
                and new_originid = @originId";
            if (regionId != Guid.Empty)
            {
                query += " and new_regionid = @regionId ";
            }
            query += " )";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@supplierId", supplierId);
                    command.Parameters.AddWithValue("@originId", originId);
                    if (regionId != Guid.Empty)
                    {
                        command.Parameters.AddWithValue("@regionId", regionId);
                    }
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public IEnumerable<Guid> GetAccountExpertisesThatNeedDirectNumber(Guid supplierId, Guid originId)
        {
            return GetAccountExpertisesThatNeedDirectNumber(supplierId, originId, Guid.Empty);
        }

        public IEnumerable<Guid> GetAllAccountExpertisesThatNeedDirectNumber(Guid originId)
        {
            string query =
               @"SELECT new_accountexpertiseid
FROM new_accountexpertise ae with(nolock)
join account acc with(nolock) on acc.accountid = ae.new_accountid
WHERE ae.deletionstatecode = 0 AND ae.statecode = 0 and acc.deletionstatecode = 0
and (acc.new_disableautodirectnumbers is null or acc.new_disableautodirectnumbers = 0)
and acc.new_status = 1 and 
(
	acc.New_unavailabilityreasonId is null
    or (acc.New_unavailableto is not null and acc.New_unavailableto < GETUTCDATE())
    or (acc.new_unavailablefrom is not null and acc.New_unavailablefrom > GETUTCDATE())
)
and isnull(New_DapazStatus, 1) in (1,4)
And new_accountexpertiseid not in(
select new_accountexpertiseid
from new_directnumber with(nolock)
where deletionstatecode = 0 and new_accountexpertiseid is not null
and new_originid = @originId
)";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@originId", originId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public IEnumerable<Guid> GetAllAccountExpertisesThatNeedDirectNumber(Guid originId, Guid regionId)
        {
            string query =
               @"SELECT new_accountexpertiseid, new_accountidname, new_primaryexpertiseidname
FROM new_accountexpertise ae with(nolock)
join account acc with(nolock) on acc.accountid = ae.new_accountid
WHERE ae.deletionstatecode = 0 AND ae.statecode = 0 and acc.deletionstatecode = 0
and (acc.new_disableautodirectnumbers is null or acc.new_disableautodirectnumbers = 0)
and isnull(New_DapazStatus, 1) in (1,4)
and acc.new_status = 1 and 
(
	acc.New_unavailabilityreasonId is null
    or (acc.New_unavailableto is not null and acc.New_unavailableto < GETUTCDATE())
    or (acc.new_unavailablefrom is not null and acc.New_unavailablefrom > GETUTCDATE())
)
and @regionId in
(
	select new_regionaccountexpertise.new_regionid from new_regionaccountexpertise with(nolock)
                join new_region with(nolock) on new_region.new_regionid = new_regionaccountexpertise.new_regionid
                where new_regionaccountexpertise.new_accountid = acc.accountid and new_regionaccountexpertise.deletionstatecode = 0
                and new_region.deletionstatecode = 0
                and new_region.new_level = 1
                and (new_regionaccountexpertise.new_regionstate = @working
                or new_regionaccountexpertise.new_regionstate = @partial)
)
And new_accountexpertiseid not in(
select new_accountexpertiseid
from new_directnumber with(nolock)
where deletionstatecode = 0 and new_accountexpertiseid is not null
and new_originid = @originId
and new_regionid = @regionId
)";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@originId", originId);
                    command.Parameters.AddWithValue("@regionId", regionId);
                    command.Parameters.AddWithValue("@working", (int)DML.Xrm.new_regionaccountexpertise.RegionState.Working);
                    command.Parameters.AddWithValue("@partial", (int)DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetUpsaleOriginId()
        {
            string query =
                @"select top 1 new_originid
                from new_origin with(nolock)
                where deletionstatecode = 0
                and new_isupsaleorigin = 1";
            object scalar = ExecuteScalar(query);
            if (scalar == null)
            {
                throw new Exception("Upsale Origin is not configured in the system. Please configure it.");
            }
            return (Guid)scalar;
        }

        public Guid GetAutoUpsaleOriginId()
        {
            string query =
                @"select top 1 new_originid
                from new_origin with(nolock)
                where deletionstatecode = 0
                and new_isautoupsaleorigin = 1";
            object scalar = ExecuteScalar(query);
            if (scalar == null)
            {
                throw new Exception("Auto Upsale Origin is not configured in the system. Please configure it.");
            }
            return (Guid)scalar;
        }

        public Guid GetDefaultOriginId()
        {
            string query =
                @"select top 1 new_originid
                from new_origin with(nolock)
                where deletionstatecode = 0
                and new_isdefaultorigin = 1";
            object scalar = ExecuteScalar(query);
            Guid retVal = Guid.Empty;
            if (scalar != null)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        private List<DataModel.PublisherPortal.OriginMinDataHolder> GetAllOriginsMethod()
        {
            List<DataModel.PublisherPortal.OriginMinDataHolder> retVal = new List<NoProblem.Core.DataModel.PublisherPortal.OriginMinDataHolder>();
            string query =
                @"select new_name, new_originid, new_isupsaleorigin, new_isautoupsaleorigin, new_parentoriginid
                from new_origin                
                where deletionstatecode = 0";
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                DML.PublisherPortal.OriginMinDataHolder holder = new NoProblem.Core.DataModel.PublisherPortal.OriginMinDataHolder();
                holder.Id = (Guid)row["new_originid"];
                holder.Name = Convert.ToString(row["new_name"]);
                holder.ParentOriginId = row["new_parentoriginid"] != DBNull.Value ? (Guid)row["new_parentoriginid"] : Guid.Empty;
                holder.IsUpsaleOrigin = row["new_isupsaleorigin"] != DBNull.Value ? (bool)row["new_isupsaleorigin"] : false;
                holder.IsAutoUpsaleOrigin = row["new_isautoupsaleorigin"] != DBNull.Value ? (bool)row["new_isautoupsaleorigin"] : false;
                retVal.Add(holder);
            }
            return retVal;
        }

        public IEnumerable<DataModel.PublisherPortal.GuidStringPair> GetAllOrigins(bool getSystemOrigins, bool getWibiyaOrigins)
        {
            var lstAll = CacheManager.Instance.Get<List<DataModel.PublisherPortal.OriginMinDataHolder>>(cachePrefix, "GetAllOrigins", GetAllOriginsMethod, CacheManager.ONE_HOUR);
            IEnumerable<DataModel.PublisherPortal.OriginMinDataHolder> enumerable = lstAll;
            if (!getWibiyaOrigins)
            {
                enumerable = from x in enumerable
                             where x.ParentOriginId == Guid.Empty
                             select x;
            }
            if (!getSystemOrigins)
            {
                enumerable = from x in enumerable
                             where !x.IsUpsaleOrigin
                             & !x.IsAutoUpsaleOrigin
                             select x;
            }
            var retVal = from x in enumerable
                         orderby x.Name
                         select new DML.PublisherPortal.GuidStringPair(x.Name, x.Id);
            return retVal;
        }

        public Guid GetOriginIdByEmail(string email)
        {
            string query =
                @"select new_originid from new_origin with(nolock)
                    where deletionstatecode = 0 and new_email = @email";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@email", email));
            Guid retVal = Guid.Empty;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        public Guid GetOriginIdInLogin(string email, string password, out string firstName)
        {
            string query =
                @"select top 1 new_originid, new_firstname from new_origin with(nolock)
                 where deletionstatecode = 0 and new_email = @email and new_password = @password";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@email", email));
            parameters.Add(new KeyValuePair<string, object>("@password", password));
            Guid retVal = Guid.Empty;
            firstName = string.Empty;
            var reader = ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                retVal = (Guid)row["new_originid"];
                firstName = row["new_firstname"] != DBNull.Value ? (string)row["new_firstname"] : string.Empty;
            }
            return retVal;
        }

        public int GetBillableRequestsNumberFromDate(Guid originId, DateTime? fromDate)
        {
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            string query =
                @"declare @yesPayId uniqueidentifier

                select top 1 @yesPayId = new_affiliatepaystatusid 
                from new_affiliatepaystatus with(nolock) 
                where deletionstatecode = 0 
                and (new_disabled = 0 or new_disabled is null)
                and new_istopay = 1

                select count(*) 
                from incident with(nolock)
                where deletionstatecode = 0
                and new_originid = @originId
                and new_affiliatepaystatusid = @yesPayId";
            if (fromDate.HasValue)
            {
                query += " and createdon > @fromDate ";
                parameters.Add(new KeyValuePair<string, object>("@fromDate", fromDate.Value));
            }
            parameters.Add(new KeyValuePair<string, object>("@originId", originId));
            int retVal = (int)ExecuteScalar(query, parameters);
            return retVal;
        }

        public Guid GetDefaultOrigin()
        {
            string guidStr = CacheManager.Instance.Get<string>(cachePrefix, "GetDefaultOrigin", GetDefaultOriginMethod, 1440);
            if (string.IsNullOrEmpty(guidStr))
                return Guid.Empty;
            return new Guid(guidStr);
        }

        private string GetDefaultOriginMethod()
        {
            string query =
                @"select new_originid from new_origin with(nolock)
                    where deletionstatecode = 0 and new_isdefaultorigin = 1";
            string retVal = null;
            object scalar = ExecuteScalar(query);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = ((Guid)scalar).ToString();
            }
            return retVal;
        }

        public Guid GetMainDapazOriginId()
        {
            string guidStr = CacheManager.Instance.Get<string>(cachePrefix, "GetMainDapazOriginId", GetMainDapazOriginIdMethod, 1440);
            if (string.IsNullOrEmpty(guidStr))
                return Guid.Empty;
            return new Guid(guidStr);
        }

        private string GetMainDapazOriginIdMethod()
        {
            string query =
                @"select new_originid from new_origin with(nolock)
                    where deletionstatecode = 0 and new_isdapazmainorigin = 1";
            string retVal = null;
            object scalar = ExecuteScalar(query);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = ((Guid)scalar).ToString();
            }
            return retVal;
        }

        public IEnumerable<Guid> GetBuyersOfThisSeller(Guid originId)
        {
            List<Guid> retVal = new List<Guid>();
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@originId", originId);
            var reader = ExecuteReader(getBuyersFromSellerQuery, parameters);
            foreach (var row in reader)
            {
                retVal.Add((Guid)row["accountid"]);
            }
            return retVal;
        }

        private const string getBuyersFromSellerQuery =
            @"select accountid
from new_seller_buyerbase
where new_originid = @originId";
    }
}
