﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class HealthInsuranceCaseData : DalBase<DataModel.Xrm.new_healthinsurancecasedata>
    {
        public HealthInsuranceCaseData(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_healthinsurancecasedata");
        }

        public override DataModel.Xrm.new_healthinsurancecasedata Retrieve(Guid id)
        {
            return XrmDataContext.new_healthinsurancecasedatas.FirstOrDefault(x => x.new_healthinsurancecasedataid == id);
        }
    }
}
