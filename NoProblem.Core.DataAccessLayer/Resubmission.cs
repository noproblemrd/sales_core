﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class Resubmission : DalBase<DataModel.Xrm.new_resubmission>
    {
        public Resubmission(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_resubmission");
        }

        public override DataModel.Xrm.new_resubmission Retrieve(Guid id)
        {
            return XrmDataContext.new_resubmissions.FirstOrDefault(x => x.new_resubmissionid == id);
        }

        public DateTime? GetLastScheduledResubmissionForSupplier(Guid supplierId)
        {
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            object scalar = ExecuteScalar(lastScheduledResumbissionQuery, parameters);
            DateTime? retVal = null;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (DateTime)scalar;
            }
            return retVal;
        }

        private const string lastScheduledResumbissionQuery =
            @"select max(new_scheduleddate) date
                from new_resubmission
                where new_accountid = @supplierId
                and new_done = 0
                and deletionstatecode = 0";
    }
}
