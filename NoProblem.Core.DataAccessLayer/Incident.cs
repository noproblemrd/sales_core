﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.Incident
//  File: Incident.cs
//  Description: DAL for Incident Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using System.Collections.Generic;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel;
using System.Data.SqlClient;
using System.Text;
using NoProblem.Core.DataModel.Reports;
using System.Data;

namespace NoProblem.Core.DataAccessLayer
{
	public class Incident : DalBase<DML.Xrm.incident>
	{
		private const string tableName = "incident";

		#region Ctor
		public Incident(DML.Xrm.DataContext xrmDataContext)
			: base(xrmDataContext)
		{
			DataModel.XrmDataContext.ClearCache(tableName);
		}
		#endregion

		public override IQueryable<DML.Xrm.incident> All
		{
			get
			{
				return XrmDataContext.incidents;
			}
		}

		#region Public Methods

		public IEnumerable<DML.Xrm.incident> GetIncidentsByContact(Guid contactId)
		{
			IEnumerable<DML.Xrm.incident> retVal = from inci in XrmDataContext.incidents
												   where
												   inci.customerid != null && inci.customerid.Value == contactId
												   select
												   inci;
			return retVal;
		}

		public override void Create(NoProblem.Core.DataModel.Xrm.incident entity)
		{
			Autonumber.AutonumberManager autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
			string next = autoNumber.GetNextNumber(tableName);
			entity.ticketnumber = next;
			entity.new_customerfaults = 0;
			base.Create(entity);
		}

		public override NoProblem.Core.DataModel.Xrm.incident Retrieve(Guid id)
		{
			return XrmDataContext.incidents.FirstOrDefault(x => x.incidentid == id);
		}

		public IEnumerable<DML.Xrm.incident> GetScheduledIncidentsToSend()
		{
			int newStatus = (int)DML.Xrm.incident.Status.NEW;
			int scheduledRequestCaseType = (int)DML.Xrm.incident.CaseTypeCode.ScheduledRequest;
			IEnumerable<DML.Xrm.incident> retVal = from inci in XrmDataContext.incidents
												   where
												   inci.statuscode.HasValue
												   &&
												   inci.statuscode.Value == newStatus
												   &&
												   inci.casetypecode.HasValue
												   &&
												   inci.casetypecode.Value == scheduledRequestCaseType
												   &&
												   inci.customersatisfactioncode != 3
												   select
												   inci;
			return retVal;
		}

		public DML.Xrm.incident FindExistingIncident(string expertiseCode, int expertiseLevel, string areaCode, int areaLevel, Guid contactid, Guid originId)
		{
			Guid retValId = Guid.Empty;
			DateTime incidentCreatedOn = DateTime.MinValue;
			string query = @"SELECT incidentid, incident.createdon FROM incident with(nolock) ";

			// Check for the appropriate expertise
			if (expertiseLevel == (int)enumExpertiseType.Primary)
			{
				query += "inner join new_primaryexpertise ON new_primaryexpertise.new_code = '{0}' AND new_primaryexpertise.new_primaryexpertiseid = incident.new_primaryexpertiseid ";
			}
			else
			{
				query += "inner join new_secondaryexpertise ON new_secondaryexpertise.new_code = '{0}' AND new_secondaryexpertise.new_secondaryexpertiseid = incident.new_secondaryexpertiseid ";
			}

			// Check for the appropriate area level
			if (areaLevel == (int)enumServiceAreaType.ZipCode)
			{
				query += "inner join new_zipcode ON new_zipcode.new_code = '{1}' AND new_zipcode.new_zipcodeid = incident.new_zipcodeid ";
			}
			else
			{
				query += "inner join new_region ON new_region.new_code = '{1}' AND new_region.new_regionid = incident.new_regionid ";
			}

			query += @"WHERE incident.statecode = 0 AND incident.deletionstatecode = 0 AND incident.contactid = '{2}'
						AND incident.new_originid = '{3}'"; // AND GETDATE() BETWEEN incident.createdon AND DATEADD(dd,{3},incident.createdon)";

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand(string.Format(query, expertiseCode, areaCode, contactid, originId), connection))
				{
					command.Connection.Open();
					using (SqlDataReader incidentId = command.ExecuteReader())
					{
						while (incidentId.Read())
						{
							Guid tempIncidentId = (Guid)incidentId["incidentid"];
							DateTime tempCreatedOn = (DateTime)incidentId["createdon"];
							if (tempCreatedOn > incidentCreatedOn)
							{
								incidentCreatedOn = tempCreatedOn;
								retValId = tempIncidentId;
							}
						}
					}
				}
			}
			DML.Xrm.incident retVal = null;
			if (retValId != Guid.Empty)
			{
				retVal = Retrieve(retValId);
			}
			return retVal;
		}

		public IEnumerable<DML.Xrm.incident> GetIncidentsByStatus(DML.Xrm.incident.Status status, params DML.Xrm.incident.Status[] statuses)
		{
			IEnumerable<DML.Xrm.incident> retVal = from inci in XrmDataContext.incidents
												   where
												   inci.statuscode == (int)status
												   select inci;
			return retVal;
		}

		public IEnumerable<NoProblem.Core.DataModel.Xrm.incident> GetIncidentsWithInvalidStatus()
		{
			IEnumerable<DML.Xrm.incident> retVal = from inci in XrmDataContext.incidents
												   where
												   inci.statuscode == (int)DML.Xrm.incident.Status.BADWORD
												   ||
												   inci.statuscode == (int)DML.Xrm.incident.Status.BLACKLIST
												   ||
												   inci.statuscode == (int)DML.Xrm.incident.Status.InvalidConsumerPhone
												   select inci;
			return retVal;
		}

		public IEnumerable<NoProblem.Core.DataModel.Xrm.incident> GetIncidentsByDates(DateTime fromDate, DateTime toDate)
		{
			IEnumerable<DML.Xrm.incident> retVal = from inci in XrmDataContext.incidents
												   where
												   inci.createdon.Value >= fromDate
												   &&
												   inci.createdon.Value <= toDate
												   orderby inci.createdon.Value
												   select
												   inci;
			return retVal;
		}

		public IEnumerable<NoProblem.Core.DataModel.Xrm.incident> GetValidFinishedIncidents()
		{
			IEnumerable<DML.Xrm.incident> retVal = from inci in XrmDataContext.incidents
												   where
												   (inci.statuscode == (int)DML.Xrm.incident.Status.WORK_DONE
												   ||
												   inci.statuscode == (int)DML.Xrm.incident.Status.CUSTOMER_NOT_AVAILABLE_DIALER
												   )
												   select inci;
			return retVal;
		}

		public Guid GetLastIncidentId(Guid cutomerId, Guid primaryExpertiseId, ref DateTime createdOnLastIncident)
		{
			string query = @"select top 1 incidentid, createdon
							from
							incident with (nolock)
							where
							customerid = '{0}'
							and
							new_primaryexpertiseid = '{1}'
							order by createdon desc";
			Guid retVal = Guid.Empty;
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand(string.Format(query, cutomerId.ToString(), primaryExpertiseId.ToString()), connection))
				{
					command.Connection.Open();
					using (SqlDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							retVal = (Guid)reader[0];
							createdOnLastIncident = (DateTime)reader[1];
						}
					}
				}
			}
			return retVal;
		}

		public List<DML.Reports.IncidentData> GetRequestsReport(DateTime? fromDate, DateTime? toDate, Guid expertiseId, string caseNumber, 
			DML.Reports.RequestsReportRequest.CaseType caseType, int? suppliersProvided, Guid affiliateOriginId, Guid caseOriginId, 
			int pageNumber, int pageSize, out int totalRows, out int notToPayCount, string url, bool onlyForUpsales, Guid ToolbarId,
			int? NumberOfUpsales, RequestsReportRequest.eCallDurationType WithMaxCallDuration, bool OnlyRejectByEmailOrName, int SliderType)
		{
			totalRows = 0;
			notToPayCount = 0;
			List<DML.Reports.IncidentData> dataList = new List<NoProblem.Core.DataModel.Reports.IncidentData>();
			using (SqlCommand command = new SqlCommand())
			{
				StringBuilder query = new StringBuilder();
				query.Append(
					@"
WITH PagerTbl AS
				(                
				
				SELECT ROW_NUMBER() OVER(ORDER BY inc.createdon desc) AS RowNum,
 inc.incidentid ,inc.ticketnumber ,con.ContactId ,con.FullName ContactFullName, inc_eb.new_telephone1 ,pexp.new_name ExpertiseName
 ,reg.New_englishname RegionName,inc.createdon, inc_eb.new_createdonlocal, inc_eb.new_originid
					,(select count(*) 
						from dbo.New_incidentaccountBase iab with(nolock)
						inner join dbo.New_incidentaccountExtensionBase iaeb with(nolock) on iab.New_incidentaccountId = iaeb.New_incidentaccountId
						where statuscode in (@closed, @closedDirect, @invited) and deletionstatecode = 0
						and new_incidentid = inc.incidentid) as AdvertisersProvided
					,case when isnull(new_requiredaccountno, 0) = 0 then 1 else new_requiredaccountno end as RequestedAdvertisers
					,(select sum(new_potentialincidentprice) 
						from dbo.New_incidentaccountBase iab with(nolock)
						inner join dbo.New_incidentaccountExtensionBase iaeb with(nolock) on iab.New_incidentaccountId = iaeb.New_incidentaccountId
						where new_tocharge = 1 and deletionstatecode = 0 and new_incidentid = inc.incidentid) as Revenue
					,ps.new_istopay
					,sr.new_name as payReasonName
					,inc_eb.new_showaffiliate
					,inc.statuscode
					,inc_eb.new_isstoppedmanually
					,inc_eb.new_isresearch
					,ct.IncidentId CaseTiketId ");
				  //  ,ct.IncidentId as caseTicketIncidentId 
				if (WithMaxCallDuration == RequestsReportRequest.eCallDurationType.With)
					query.Append(@"
					,isnull((select max(cdr.New_Duration)
						from dbo.New_incidentaccount ia with(nolock)
						inner Join 
						dbo.New_calldetailrecord cdr with(nolock)
						On ia.New_incidentaccountId =cdr.new_incidentaccountid
						where ia.new_incidentid = inc.IncidentId), 0) MaxCallDuration");
				if (NumberOfUpsales.HasValue)
					query.Append(@"
						,(select COUNT(*)
						from incident incIn with(nolock)
						where  inc.IncidentId = incIn.new_upsaleincidentid) as Upsales");
				query.Append(@"
					from dbo.IncidentBase inc with(nolock)
					inner join dbo.IncidentExtensionBase inc_eb  with(nolock) on inc.IncidentId = inc_eb.IncidentId
					inner join dbo.ContactBase con with(nolock) on inc.ContactId = con.ContactId
					inner join dbo.New_primaryexpertiseExtensionBase pexp with(nolock) on inc_eb.new_primaryexpertiseid = pexp.New_primaryexpertiseId
					left join  dbo.New_regionExtensionBase reg with(nolock) on inc_eb.new_regionid = reg.New_regionId
					left join dbo.CaseTickets ct on ct.IncidentId = inc.incidentid");
				  //  from incident inc with(nolock) ");
				if (onlyForUpsales)
				{
					query.Append(@" left join new_upsale up with(nolock) on up.new_upsaleid = inc_eb.new_upsaleid ");
				}
				if (caseOriginId != Guid.Empty)
				{
					query.Append(@" join new_origin org with(nolock) on inc_eb.new_originid = org.new_originid ");
				}
				//inner join CaseTickets ct on ct.IncidentId = inc.incidentid
				query.Append(@" 
				inner join dbo.New_affiliatepaystatusExtensionBase ps with(nolock) on ps.new_affiliatepaystatusid = inc_eb.new_affiliatepaystatusid
				inner join dbo.New_affiliatepaystatusreasonExtensionBase sr with(nolock) on sr.new_affiliatepaystatusreasonid = inc_eb.new_affiliatepaystatusreasonid                
				where ISNULL(inc_eb.new_isgoldennumbercall, 0) = 0 and inc.deletionstatecode = 0 and inc.statuscode != @duplicate and inc.statuscode != @blacklist and inc.statuscode != @badword and inc_eb.new_createdonlocal is not null ");
				command.Parameters.AddWithValue("@closed", (int)DML.Xrm.new_incidentaccount.Status.CLOSE);
				command.Parameters.AddWithValue("@closedDirect", (int)DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS);
				command.Parameters.AddWithValue("@invited", (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS);
				command.Parameters.AddWithValue("@badword", (int)DML.Xrm.incident.Status.BADWORD);
				command.Parameters.AddWithValue("@blacklist", (int)DML.Xrm.incident.Status.BLACKLIST);
				command.Parameters.AddWithValue("@duplicate", (int)DML.Xrm.incident.Status.DUPLICATE);
				if (string.IsNullOrEmpty(caseNumber) == false)
				{
					query.Append(" and inc.ticketnumber like @caseNumber ");
					command.Parameters.AddWithValue("@caseNumber", "%" + caseNumber + "%");
				}
				else
				{
					if (NumberOfUpsales.HasValue && NumberOfUpsales.Value > -1)
					{
						query.Append(" and (select COUNT(*) from dbo.IncidentExtensionBase incIn with(nolock) " +
							"where  inc.IncidentId = incIn.new_upsaleincidentid) = @NumberOfUpsales ");
						command.Parameters.AddWithValue("@NumberOfUpsales", NumberOfUpsales.Value);
					}
					if (fromDate.HasValue && toDate.HasValue)
					{
						query.Append(" and inc_eb.new_createdonlocal between @from and @to ");
						command.Parameters.AddWithValue("@from", fromDate.Value);
						command.Parameters.AddWithValue("@to", toDate.Value);
					}
					if (ToolbarId != Guid.Empty)
					{
						query.Append(" and inc_eb.New_toolbaridId = @ToolbarId ");
						command.Parameters.AddWithValue("@ToolbarId", ToolbarId);
					}
					if (expertiseId != Guid.Empty)
					{
						query.Append(" and inc_eb.new_primaryexpertiseid = @expertiseid ");
						command.Parameters.AddWithValue("@expertiseid", expertiseId);
					}
					if (caseType != NoProblem.Core.DataModel.Reports.RequestsReportRequest.CaseType.All)
					{
						if (caseType == NoProblem.Core.DataModel.Reports.RequestsReportRequest.CaseType.Click2)
						{
							query.Append(" and inc.casetypecode = @caseType ");
							command.Parameters.AddWithValue("@caseType", DML.Xrm.incident.CaseTypeCode.SupplierSpecified);
						}
						else if (caseType == NoProblem.Core.DataModel.Reports.RequestsReportRequest.CaseType.ServiceRequest)
						{
							query.Append(" and (inc.casetypecode = @normal  or inc.casetypecode = @scheduled) ");
							command.Parameters.AddWithValue("@normal", DML.Xrm.incident.CaseTypeCode.Request);
							command.Parameters.AddWithValue("@scheduled", DML.Xrm.incident.CaseTypeCode.ScheduledRequest);
						}
						else
						{
							query.Append(" and (inc.casetypecode = @caseType or inc_eb.new_isdirectnumber = 1) ");
							command.Parameters.AddWithValue("@caseType", DML.Xrm.incident.CaseTypeCode.DirectNumber);
						}
					}
					if (WithMaxCallDuration == RequestsReportRequest.eCallDurationType.Zero)
					{
						query.Append(@" and                            
							isnull((select max(cdr.New_Duration)
							from dbo.New_incidentaccountExtensionBase ia with(nolock)
							inner Join 
							dbo.New_calldetailrecordExtensionBase cdr with(nolock)
							On ia.New_incidentaccountId =cdr.new_incidentaccountid
							where ia.new_incidentid = inc.IncidentId), 0) = 0 ");
/*
from dbo.New_incidentaccount ia with(nolock)
						inner Join 
						dbo.New_calldetailrecord cdr with(nolock)
						On ia.New_incidentaccountId =cdr.new_incidentaccountid
						where ia.new_incidentid = inc.IncidentId), 0) MaxCallDuration");
						*/
					}
					if (suppliersProvided.HasValue)
					{
						query.Append(@" and (select count(*) from new_incidentaccount with(nolock)
						where (statuscode = @closed or statuscode = @closedDirect or statuscode = @invited) and deletionstatecode = 0
						and new_incidentid = inc.incidentid) = @provided ");
						command.Parameters.AddWithValue("@provided", suppliersProvided);
					}
					if (affiliateOriginId != Guid.Empty)
					{
						query.Append(@" and inc_eb.new_originid = @affiliateOriginId 
										and (inc_eb.new_showaffiliate is null or inc_eb.new_showaffiliate = 1)");
						command.Parameters.AddWithValue("@affiliateOriginId", affiliateOriginId);
					}
					else if (caseOriginId != Guid.Empty)
					{
						query.Append(@" and (inc_eb.new_originid = @caseOriginId or org.new_parentoriginid = @caseOriginId) ");
						command.Parameters.AddWithValue("@caseOriginId", caseOriginId);
					}
					if (!string.IsNullOrEmpty(url))
					{
						query.Append(@" and inc_eb.new_url = @url ");
						command.Parameters.AddWithValue("@url", url);
					}
					if (onlyForUpsales)
					{
						query.Append(@" and up.statuscode = 1 ");
						query.Append(@" and isnull(inc_eb.New_IsStoppedManually, 0) = 0 ");
					}
					if (OnlyRejectByEmailOrName)
					{
						query.Append(@"and EXISTS ((select * 
						from dbo.New_incidentaccountExtensionBase iaeb  with(nolock)
						where iaeb.new_incidentid = inc.IncidentId
							and	ISNULL(iaeb.New_rejectedreason, '') not in ('Probable voicemail', '')
						 and (iaeb.New_rejectedreason like '%Email%' or iaeb.New_rejectedreason like '%Name%')
						 
						))");
					}
                    if(SliderType > 0)
                    {
                        query.Append(@" and inc_eb.New_Type = @SliderType ");
                        command.Parameters.AddWithValue("@SliderType", SliderType);
                    }
				}
				query.Append(@"
				)
					SELECT * , (select max(RowNum) from PagerTbl) as totalRows
					, (select count(*) from PagerTbl where new_istopay = 0) as notToPayCount
					  FROM PagerTbl
					 WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
									  AND @PageNum * @PageSize)
							or @PageSize = -1)
					 ORDER BY RowNum

				option (recompile)");
				command.Parameters.AddWithValue("@PageNum", pageNumber);
				command.Parameters.AddWithValue("@PageSize", pageSize);
				using (SqlConnection connection = new SqlConnection(connectionString))
				{
					connection.Open();
					command.Connection = connection;
					command.CommandTimeout = 300;
					command.CommandText = query.ToString();
					using (SqlDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							DML.Reports.IncidentData data = new NoProblem.Core.DataModel.Reports.IncidentData();
							if (reader["CaseTiketId"] != DBNull.Value)
							{
								data.CaseId = (Guid)reader["incidentid"];
							}
							data.CaseNumber = (string)reader["ticketnumber"];
							data.ConsumerId = (Guid)reader["ContactId"];
							string customerName = Convert.ToString(reader["ContactFullName"]);
							string customerPhone = Convert.ToString(reader["new_telephone1"]);
							string customerToShow;
							if (string.IsNullOrEmpty(customerName) || customerName == customerPhone)
							{
								customerToShow = customerPhone;
							}
							else
							{
								customerToShow = customerName + ": " + customerPhone;
							}
							data.ConsumerName = customerToShow;
							//data.ConsumerName = reader["customeridname"] != DBNull.Value ? (string)reader["customeridname"] : "Anonymous";
							data.CreatedOn = reader["new_createdonlocal"] != DBNull.Value ? (DateTime)reader["new_createdonlocal"] : (DateTime)reader["createdon"];
							data.ExpertiseName = reader["ExpertiseName"] != DBNull.Value ? (string)reader["ExpertiseName"] : "***";
							data.NumberOfAdvertisersProvided = (int)reader["AdvertisersProvided"];
							data.NumberOfRequestedAdvertisers = (int)reader["RequestedAdvertisers"];
							data.RegionName = reader["RegionName"] != DBNull.Value ? (string)reader["RegionName"] : string.Empty;
							data.Revenue = reader["Revenue"] != DBNull.Value ? (decimal)reader["Revenue"] : 0;
							data.OriginId = reader["new_originid"] != DBNull.Value ? (Guid)reader["new_originid"] : Guid.Empty;
							data.IsToPay = reader["new_istopay"] != DBNull.Value ? (bool)reader["new_istopay"] : true;
							data.PayStatusReason = reader["payReasonName"] != DBNull.Value ? (string)reader["payReasonName"] : string.Empty;
							data.ShowAffiliate = reader["new_showaffiliate"] != DBNull.Value ? (bool)reader["new_showaffiliate"] : true;
							data.Upsales = NumberOfUpsales.HasValue ? (int)reader["Upsales"] : 0;
							data.MaxCallDuration = WithMaxCallDuration == RequestsReportRequest.eCallDurationType.With ? (int)reader["MaxCallDuration"] : 0;
							bool isCancelledManually = reader["new_isstoppedmanually"] != DBNull.Value ? (bool)reader["new_isstoppedmanually"] : false;
							if (isCancelledManually)
							{
								data.RequestStatus = "Stopped Manually";
							}
							else
							{
								bool isResearch = reader["new_isresearch"] != DBNull.Value ? (bool)reader["new_isresearch"] : false;
								if (isResearch)
								{
									data.RequestStatus = "Research";
								}
								else
								{
									DML.Xrm.incident.Status status = (DML.Xrm.incident.Status)((int)reader["statuscode"]);
									data.RequestStatus = status.ToString();
								}
							}
							dataList.Add(data);
							if (totalRows == 0)
							{
								totalRows = unchecked((int)((long)reader["totalRows"]));
								notToPayCount = (int)reader["notToPayCount"];
							}
						}
					}
				}
			}
			return dataList;
		}

		private const string queryForCaseTicket =
            @"select Title, Description, WinningPrice, ProvidedAdvertisers, HeadingName, 
			HeadingCode, RegionLevel, RegionName, OriginName, ConsumerId, ConsumerName,
			ct.CreatedOn as CreatedOnCase, RequestedAdvertisers, Revenue, ConsumerPhone,
			ct.ConsumerEmail
			,TicketNumber
			, inc.PayStatusId, inc.PayReasonId
			, UpsaleId, ExposureUrl, ExposureControlName,
			Keyword, ct.IncidentId, Type, IsBlockedContact, IsDirectNumber, SupplierId,
			PredefinedPrice, ActualPrice, Status, cal.CreatedOn as CreatedOnCall,
			Duration, AarCandidateStatus, IsAnsweringMachine, DtmfPressed, IvrFlavor,
			IsRejected, Recording, RejectReason, SupplierName, new_tocharge, new_recordfilelocation,
			new_isstoppedmanually, upsalestatuscode, isdirectnumber, cal.Id as callId
			,ParentRequestId, ParentRequestTicket, cal.BillingRate, cal.LeadBuyerName
from CaseTickets ct
left join CallsForCaseTickets cal on ct.id = cal.caseticketid
left join (select new_incidentaccountid, new_tocharge, new_recordfilelocation
from
new_incidentaccount with(nolock)) as rcal on rcal.new_incidentaccountid = cal.incidentaccountid
join (
select incidentid, new_isstoppedmanually, new_affiliatepaystatusid as PayStatusId, new_affiliatepaystatusreasonid as PayReasonId
from incident with(nolock)
) as inc on inc.incidentid = ct.incidentid
left join (
select statuscode as upsalestatuscode, new_upsaleid from new_upsale with(nolock)
) as ups on ups.new_upsaleid = ct.upsaleid
where
ct.incidentid = @caseId";

		private const string queryForGetConnectedCases =
			@"select incident.ticketnumber, ct.incidentid from incident with(nolock)
				left join CaseTickets ct on incident.incidentid = ct.incidentid
			   where (new_upsaleincidentid = @parentIncidentId
				or new_upsaleincidentid = @caseId)
				and incident.incidentid != @caseId
				order by incident.createdon desc";

		public NoProblem.Core.DataModel.Case.CaseData GetCaseData(Guid caseId)
		{
			DML.Case.CaseData caseData = new NoProblem.Core.DataModel.Case.CaseData();

			SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.incident>.SqlParametersList();
			parameters.Add("@caseId", caseId);
			var reader = ExecuteReader(queryForCaseTicket, parameters);
			if (reader.Count > 0)
			{
				var firstRow = reader.First();
				caseData.CanBeStopped = firstRow["new_isstoppedmanually"] != DBNull.Value ? !(bool)firstRow["new_isstoppedmanually"] : true;
				caseData.ConsumerId = (Guid)firstRow["ConsumerId"];
				caseData.ConsumerName = Convert.ToString(firstRow["ConsumerName"]);
				caseData.CostumerPhone = Convert.ToString(firstRow["ConsumerPhone"]);
				caseData.ConsumerEmail = (firstRow["ConsumerEmail"] == DBNull.Value) ? string.Empty : (string)firstRow["ConsumerEmail"];
				caseData.CreatedOn = (DateTime)firstRow["CreatedOnCase"];
				caseData.Description = Convert.ToString(firstRow["Description"]);
				caseData.ExposureControlName = Convert.ToString(firstRow["ExposureControlName"]);
				caseData.ExposureUrl = Convert.ToString(firstRow["ExposureUrl"]);
				caseData.HeadingCode = Convert.ToString(firstRow["HeadingCode"]);
				caseData.HeadingName = Convert.ToString(firstRow["HeadingName"]);
				caseData.IsStoppedManually = !caseData.CanBeStopped;
				caseData.Keyword = Convert.ToString(firstRow["Keyword"]);
				caseData.OriginName = Convert.ToString(firstRow["OriginName"]);
				caseData.PayReasonId = firstRow["PayReasonId"] != DBNull.Value ? (Guid)firstRow["PayReasonId"] : Guid.Empty;
				caseData.PayStatusId = firstRow["PayStatusId"] != DBNull.Value ? (Guid)firstRow["PayStatusId"] : Guid.Empty;
				caseData.ProvidedAdvertisers = firstRow["ProvidedAdvertisers"] != DBNull.Value ? (int)firstRow["ProvidedAdvertisers"] : 0;
				caseData.RegionLevel = firstRow["RegionLevel"] != DBNull.Value ? (int)firstRow["RegionLevel"] : 4;
				caseData.RegionName = Convert.ToString(firstRow["RegionName"]);
				caseData.RequestedAdvertisers = firstRow["RequestedAdvertisers"] != DBNull.Value ? (int)firstRow["RequestedAdvertisers"] : 0;
				caseData.Revenue = firstRow["Revenue"] != DBNull.Value ? (decimal)firstRow["Revenue"] : 0;
				caseData.TicketNumber = Convert.ToString(firstRow["TicketNumber"]);
				caseData.Title = Convert.ToString(firstRow["Title"]);
				caseData.Type = GetCaseType((DML.Xrm.incident.CaseTypeCode)firstRow["Type"], firstRow["isdirectnumber"] == DBNull.Value ? false : (bool)firstRow["isdirectnumber"]);
				caseData.UpsaleId = firstRow["UpsaleId"] != DBNull.Value ? (Guid)firstRow["UpsaleId"] : Guid.Empty;
				caseData.WinningPrice = firstRow["WinningPrice"] != DBNull.Value ? (decimal)firstRow["WinningPrice"] : 0;
				bool hasParent = firstRow["ParentRequestId"] != DBNull.Value;
				if (hasParent)
				{
					caseData.ParentRequestId = (Guid)firstRow["ParentRequestId"];
					caseData.ParentRequestTicket = Convert.ToString(firstRow["ParentRequestTicket"]);
				}
				DecideOnUpsaleButton(caseData, firstRow);
				foreach (var row in reader)
				{
					if (row["callId"] != DBNull.Value)
					{
						var callData = new NoProblem.Core.DataModel.Case.CallData();
						callData.AarCandidateStatus = Convert.ToString(row["AarCandidateStatus"]);
						callData.ActualPrice = (decimal)row["ActualPrice"];
						callData.CreatedOn = (DateTime)row["CreatedOnCall"];
						callData.DtmfPressed = Convert.ToString(row["DtmfPressed"]);
						callData.Duration = Convert.ToString(row["Duration"]);
						callData.IsAnsweringMachine = row["IsAnsweringMachine"] != DBNull.Value ? (bool)row["IsAnsweringMachine"] : false;
						callData.IsCharged = (bool)row["new_tocharge"];
						callData.IsRejected = row["IsRejected"] != DBNull.Value ? (bool)row["IsRejected"] : false;
						callData.IvrFlavor = Convert.ToString(row["IvrFlavor"]);
						callData.PredefinedPrice = row["PredefinedPrice"] != DBNull.Value ? (decimal)row["PredefinedPrice"] : callData.ActualPrice;
						callData.Recording = Convert.ToString(row["new_recordfilelocation"]);
						callData.RejectReason = Convert.ToString(row["RejectReason"]);
						callData.Status = (DataModel.Case.EndStatus)Enum.Parse(typeof(DataModel.Case.EndStatus), Convert.ToString(row["Status"]));
						callData.SupplierId = (Guid)row["SupplierId"];
						callData.SupplierName = Convert.ToString(row["SupplierName"]);
                        callData.BillingRate = (row["BillingRate"] == DBNull.Value) ? 0m : (decimal)row["BillingRate"];
                        callData.LeadBuyerName = (row["LeadBuyerName"] == DBNull.Value) ? string.Empty : (string)row["LeadBuyerName"];
                        callData.SetQualityRate();
						caseData.Calls.Add(callData);
                        //, cal.BillingRate, cal.LeadBuyerName
					}
				}
				parameters.Add("@parentIncidentId", caseData.ParentRequestId);
				reader = ExecuteReader(queryForGetConnectedCases, parameters);
				foreach (var row in reader)
				{
					DML.PublisherPortal.GuidStringPair pair = new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair();
					pair.Id = row["incidentid"] != DBNull.Value ? (Guid)row["incidentid"] : Guid.Empty;
					pair.Name = Convert.ToString(row["ticketnumber"]);
					caseData.ConnectedRequests.Add(pair);
				}
			}
			return caseData;

			//            string caseQuery = @"select
			//inc.ticketnumber, inc.title, inc.description, inc.new_primaryexpertiseidname, inc.new_regionidname, inc.new_keyword
			//, inc.new_originidname, inc.customerid, inc.customeridname, inc.casetypecode, inc.createdon,inc.new_winningprice,inc.new_isdirectnumber
			//,rg.new_level,pe.new_code, ct.mobilephone, inc.new_affiliatepaystatusid, inc.new_affiliatepaystatusreasonid
			//    ,(select count(*) from new_incidentaccount with(nolock)
			//        where (statuscode = @closed or statuscode = @closedDirect or statuscode = @invited) and deletionstatecode = 0
			//        and new_incidentid = inc.incidentid) as AdvertisersProvided
			//    ,case when (new_requiredaccountno = 0 or new_requiredaccountno is null) then 1 else new_requiredaccountno end as RequestedAdvertisers
			//    ,(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
			//        where new_tocharge = 1 and deletionstatecode = 0 and new_incidentid = inc.incidentid) as Revenue
			//,up.statuscode as upsaleStatus
			//,up.new_upsaleid
			//,ct.new_isanonymouscontact as isBlockedContact
			//,inc.new_url
			//,inc.new_controlname
			//,inc.new_isstoppedmanually
			//from incident inc with(nolock)
			//join contact ct with(nolock) on ct.contactid = inc.customerid
			//left join new_upsale up with(nolock) on up.new_upsaleid = inc.new_upsaleid
			//left join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = inc.new_primaryexpertiseid
			//left join new_region rg with(nolock) on inc.new_regionid = rg.new_regionid
			//where inc.deletionstatecode = 0 and inc.incidentid = @caseId";

			//            string callsQuery = @"select
			//                    ia.new_accountid,ia.new_accountidname,ia.statuscode,ia.new_predefinedprice,ia.new_potentialincidentprice,ia.new_tocharge, ia.new_createdonlocal
			//                    ,ivrt.new_ivrtypecode, ia.new_dtmfpressed, ia.new_isansweredbymachine, ia.new_isrejected
			//                    ,( select top 1 new_duration from new_calldetailrecord with(nolock) where new_incidentaccountid = ia.new_incidentaccountid and new_type = 2 ) as new_durationadvertiser
			//                    ,( select top 1 new_duration from new_calldetailrecord with(nolock) where new_incidentaccountid = ia.new_incidentaccountid and new_type = 3 ) as new_durationconsumer
			//                    ,ivrf.new_name as flavor
			//                    ,ia.new_recordfilelocation
			//                    ,ia.new_rejectedreason
			//                    from new_incidentaccount ia with(nolock)
			//                    left join new_ivrflavor ivrf with(nolock) on ivrf.new_ivrflavorid = ia.new_ivrflavorid
			//                    left join new_ivrtype ivrt with(nolock) on ivrt.new_ivrtypeid = ivrf.new_ivrtypeid
			//	                where ia.deletionstatecode = 0 and ia.new_incidentid = @caseId and ia.new_createdonlocal is not null
			//                    order by ia.new_createdonlocal";

			//            DataAccessLayer.AffiliatePayStatusReason payReasonDal = new AffiliatePayStatusReason(XrmDataContext);
			//            Guid defaultPayStatusId;
			//            Guid defaultPayReasonId = payReasonDal.GetDefault(out defaultPayStatusId);

			//            List<DML.Case.CallData> calls = new List<NoProblem.Core.DataModel.Case.CallData>();
			//            DML.Case.CaseData caseData = new NoProblem.Core.DataModel.Case.CaseData();
			//            using (SqlConnection conn = new SqlConnection(connectionString))
			//            {
			//                conn.Open();
			//                using (SqlCommand callsCommand = new SqlCommand(callsQuery, conn))
			//                {
			//                    callsCommand.Parameters.AddWithValue("@caseId", caseId);
			//                    using (SqlDataReader reader = callsCommand.ExecuteReader())
			//                    {
			//                        while (reader.Read())
			//                        {
			//                            DML.Case.CallData call = new NoProblem.Core.DataModel.Case.CallData();
			//                            call.ActualPrice = (decimal)reader["new_potentialincidentprice"];
			//                            call.IsCharged = (bool)reader["new_tocharge"];
			//                            call.PredefinedPrice = reader["new_predefinedprice"] != DBNull.Value ? (decimal)reader["new_predefinedprice"] : call.ActualPrice;
			//                            call.Status = GetEndStatus((DML.Xrm.new_incidentaccount.Status)reader["statuscode"]);
			//                            call.SupplierId = (Guid)reader["new_accountid"];
			//                            call.SupplierName = (string)reader["new_accountidname"];
			//                            if (reader["new_ivrtypecode"] != DBNull.Value)
			//                            {
			//                                call.AarCandidateStatus = ((DML.Xrm.new_ivrtype.IvrTypeCode)((int)reader["new_ivrtypecode"])).ToString();
			//                            }
			//                            call.DtmfPressed = reader["new_dtmfpressed"] != DBNull.Value ? (string)reader["new_dtmfpressed"] : string.Empty;
			//                            call.CreatedOn = (DateTime)reader["new_createdonlocal"];
			//                            if (reader["new_durationadvertiser"] != DBNull.Value)
			//                            {
			//                                call.Duration = ((int)reader["new_durationadvertiser"]).ToString();
			//                                if (reader["new_durationconsumer"] != DBNull.Value)
			//                                {
			//                                    call.Duration += " / " + ((int)reader["new_durationconsumer"]).ToString();
			//                                }
			//                            }
			//                            if (reader["new_isansweredbymachine"] != DBNull.Value)
			//                            {
			//                                call.IsAnsweringMachine = (bool)reader["new_isansweredbymachine"];
			//                            }
			//                            call.IvrFlavor = Convert.ToString(reader["flavor"]);
			//                            call.IsRejected = reader["new_isrejected"] != DBNull.Value ? (bool)reader["new_isrejected"] : false;
			//                            call.Recording = Convert.ToString(reader["new_recordfilelocation"]);
			//                            call.RejectReason = Convert.ToString(reader["new_rejectedreason"]);
			//                            calls.Add(call);
			//                        }
			//                    }
			//                }
			//                var winningCalls = calls.Where(x => x.Status == DML.Case.EndStatus.Won || x.Status == DML.Case.EndStatus.Invited);
			//                decimal winPrice = winningCalls.Count() > 0 ? calls.Max(x => x.ActualPrice) : 0;
			//                caseData.Calls = calls;
			//                using (SqlCommand command = new SqlCommand(caseQuery, conn))
			//                {
			//                    command.Parameters.AddWithValue("@closed", DML.Xrm.new_incidentaccount.Status.CLOSE);
			//                    command.Parameters.AddWithValue("@closedDirect", DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS);
			//                    command.Parameters.AddWithValue("@invited", DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS);
			//                    command.Parameters.AddWithValue("@caseId", caseId);
			//                    using (SqlDataReader reader = command.ExecuteReader())
			//                    {
			//                        while (reader.Read())
			//                        {
			//                            caseData.ConsumerId = (Guid)reader["customerid"];
			//                            caseData.ConsumerName = reader["customeridname"] != DBNull.Value ? (string)reader["customeridname"] : "Anonymous";
			//                            caseData.CreatedOn = (DateTime)reader["createdon"];
			//                            caseData.Description = reader["description"] == DBNull.Value ? string.Empty : (string)reader["description"];
			//                            caseData.HeadingName = reader["new_primaryexpertiseidname"] != DBNull.Value ? (string)reader["new_primaryexpertiseidname"] : "***";
			//                            caseData.OriginName = (string)reader["new_originidname"];
			//                            caseData.ProvidedAdvertisers = (int)reader["AdvertisersProvided"];
			//                            caseData.RegionName = reader["new_regionidname"] == DBNull.Value ? string.Empty : (string)reader["new_regionidname"];
			//                            caseData.RequestedAdvertisers = (int)reader["RequestedAdvertisers"];
			//                            caseData.Revenue = reader["Revenue"] == DBNull.Value ? 0 : (decimal)reader["Revenue"];
			//                            caseData.Title = reader["title"] == DBNull.Value ? string.Empty : (string)reader["title"];
			//                            caseData.Type = GetCaseType((DML.Xrm.incident.CaseTypeCode)reader["casetypecode"], reader["new_isdirectnumber"] == DBNull.Value ? false : (bool)reader["new_isdirectnumber"]);
			//                            caseData.WinningPrice = winPrice;
			//                            caseData.RegionLevel = reader["new_level"] != DBNull.Value ? (int)reader["new_level"] : 0;
			//                            caseData.HeadingCode = reader["new_code"] != DBNull.Value ? (string)reader["new_code"] : string.Empty;
			//                            caseData.CostumerPhone = reader["mobilephone"] != DBNull.Value ? (string)reader["mobilephone"] : string.Empty;
			//                            caseData.TicketNumber = reader["ticketnumber"] != DBNull.Value ? (string)reader["ticketnumber"] : string.Empty;
			//                            caseData.PayStatusId = reader["new_affiliatepaystatusid"] != DBNull.Value ? (Guid)reader["new_affiliatepaystatusid"] : defaultPayStatusId;
			//                            caseData.PayReasonId = reader["new_affiliatepaystatusreasonid"] != DBNull.Value ? (Guid)reader["new_affiliatepaystatusreasonid"] : defaultPayReasonId;
			//                            caseData.UpsaleId = reader["new_upsaleid"] != DBNull.Value ? (Guid)reader["new_upsaleId"] : Guid.Empty;
			//                            bool isBlockedContact = reader["isBlockedContact"] != DBNull.Value ? (bool)reader["isBlockedContact"] : false;
			//                            if (caseData.UpsaleId != Guid.Empty && isBlockedContact == false)
			//                            {
			//                                caseData.ShowUpsaleButton = reader["upsaleStatus"] != DBNull.Value ? (int)reader["upsaleStatus"] == 1 : false;
			//                            }
			//                            else
			//                            {
			//                                caseData.ShowUpsaleButton = false;
			//                            }
			//                            if (reader["new_controlname"] != DBNull.Value)
			//                            {
			//                                caseData.ExposureControlName = (string)reader["new_controlname"];
			//                            }
			//                            if (reader["new_url"] != DBNull.Value)
			//                            {
			//                                caseData.ExposureUrl = (string)reader["new_url"];
			//                            }
			//                            caseData.Keyword = reader["new_keyword"] != DBNull.Value ? (string)reader["new_keyword"] : string.Empty;
			//                            caseData.IsStoppedManually = reader["new_isstoppedmanually"] != DBNull.Value ? (bool)reader["new_isstoppedmanually"] : false;
			//                            caseData.CanBeStopped = !caseData.IsStoppedManually;
			//                        }
			//                    }
			//                }
			//            }
			//            return caseData;
		}

		private void DecideOnUpsaleButton(DML.Case.CaseData caseData, Dictionary<string, object> firstRow)
		{
			DataAccessLayer.ConfigurationSettings config = new ConfigurationSettings(XrmDataContext);
			if (config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.UNLIMITED_UPSALES_ENABLED) == 1.ToString())
			{
				int hours;
				if (!int.TryParse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.UNLIMITED_UPSALES_HOURS), out hours))
				{
					hours = 72;
				}
				caseData.ShowUpsaleButton = caseData.CreatedOn.AddHours(hours) > DateTime.Now;
			}
			else
			{
				caseData.ShowUpsaleButton = firstRow["upsalestatuscode"] != DBNull.Value ? (int)firstRow["upsalestatuscode"] == 1 : false;
			}
		}

		public List<DML.Reports.ConversionReportContainerPerDate> ExposureQuery(
		   DateTime fromDate,
		   DateTime toDate,
		   Guid expertiseId,
		   string controlName,
		   string domain,
		   Guid originId,
		   bool? upsales,
		   out DateTime requestsLastUpdate)
		{
			List<DML.Reports.ConversionReportContainerPerDate> retVal = new List<NoProblem.Core.DataModel.Reports.ConversionReportContainerPerDate>();
			SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.incident>.SqlParametersList();
			parameters.Add("@from", fromDate);
			parameters.Add("@to", toDate);
			StringBuilder builder = new StringBuilder();
			builder.Append(
				@"
				declare @lastUpdate datetime
				select @lastUpdate = Max(lastupdate) from incident_PerDay_ConversionReport

				select sum(Cnt) sum, sum(sumCallCount) callSum, CreatedOn, @lastUpdate as lastUpdate
				from incident_PerDay_ConversionReport
				where CreatedOn between @from and @to ");
			if (expertiseId != Guid.Empty)
			{
				builder.Append(" and new_primaryexpertiseid = @headingId ");
				parameters.Add("@headingId", expertiseId);
			}
			if (!string.IsNullOrEmpty(controlName))
			{
				builder.Append(" and New_ControlName = @flavor ");
				parameters.Add("@flavor", controlName);
			}
			if (!string.IsNullOrEmpty(domain))
			{
				builder.Append(" and New_Domain = @domain ");
				parameters.Add("@domain", domain);
			}
			if (originId != Guid.Empty)
			{
				builder.Append(" and new_originid = @originId ");
				parameters.Add("@originId", originId);
			}
			if (upsales.HasValue)
			{
				builder.Append(" and new_isupsale = ");
				if (upsales.Value)
				{
					builder.Append("1 ");
				}
				else
				{
					builder.Append("0 ");
				}
			}
			builder.Append(" group by CreatedOn");

			var reader = ExecuteReader(builder.ToString(), parameters);
			if (reader.Count > 0)
			{
				requestsLastUpdate = (DateTime)reader.First()["lastUpdate"];
			}
			else if (!upsales.HasValue)
			{
				string lastUpdateQuery = @"select Max(lastupdate) from incident_PerDay_ConversionReport";
				object scalar = ExecuteScalar(lastUpdateQuery);
				if (scalar != null && scalar != DBNull.Value)
				{
					requestsLastUpdate = (DateTime)scalar;
				}
				else
				{
					requestsLastUpdate = DateTime.MinValue.AddDays(1);
				}
			}
			else
			{
				requestsLastUpdate = DateTime.MinValue.AddDays(1);
			}
			foreach (var row in reader)
			{
				DML.Reports.ConversionReportContainerPerDate pair =
					new DML.Reports.ConversionReportContainerPerDate(
						(DateTime)row["CreatedOn"], (int)row["sum"], (int)row["callSum"]
					   );
				retVal.Add(pair);
			}
			return retVal;
		}

		public bool CheckIsDuplicate(Guid contactId, Guid expertiseId, Guid regionId, Guid originId, int seconds)
		{
			string query =
				@"select count(*)
				from incident with(nolock)
				where deletionstatecode = 0
				and createdon > @createdOn
				and new_primaryexpertiseid = @expertiseId
				and customerid = @contactId
				and new_regionid = @regionId
				and new_originid = @originId";
			DateTime createdOn = DateTime.Now.AddSeconds(seconds * -1);
			List<KeyValuePair<string, object>> parameters =
				new List<KeyValuePair<string, object>>();
			parameters.Add(new KeyValuePair<string, object>("@createdOn", createdOn));
			parameters.Add(new KeyValuePair<string, object>("@expertiseId", expertiseId));
			parameters.Add(new KeyValuePair<string, object>("@contactId", contactId));
			parameters.Add(new KeyValuePair<string, object>("@regionId", regionId));
			parameters.Add(new KeyValuePair<string, object>("@originId", originId));
			int count = (int)ExecuteScalar(query, parameters);
			bool retVal = count > 0;
			return retVal;
		}

		public List<Guid> GetIncidentsIdsByUpsale(Guid upsaleId)
		{
			string query =
				@"select incidentid from incident with(nolock)
				where deletionstatecode = 0 and new_upsaleid = @id";
			List<Guid> retVal = new List<Guid>();
			using (SqlConnection conn = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand(query, conn))
				{
					command.Connection.Open();
					command.Parameters.AddWithValue("@id", upsaleId);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							retVal.Add((Guid)reader[0]);
						}
					}
				}
			}
			return retVal;
		}

		public List<Guid> GetIncidentsByTimePass(Guid expertiseId, Guid contactId, DateTime fromDate, Guid thisIncidentId)
		{
			List<Guid> retVal = new List<Guid>();
			using (SqlConnection conn = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand())
				{
					string query =
				@"select
				incidentid
				from incident with(nolock)
				where
				deletionstatecode = 0
				and new_primaryexpertiseid = @expertiseId
				and customerid = @contactId
				and createdon >= @from";
					if (thisIncidentId != Guid.Empty)
					{
						query += " and incidentid != @incidentId ";
						command.Parameters.AddWithValue("@incidentId", thisIncidentId);
					}
					command.Connection = conn;
					command.CommandText = query;
					command.Connection.Open();
					command.Parameters.AddWithValue("@expertiseId", expertiseId);
					command.Parameters.AddWithValue("@contactId", contactId);
					command.Parameters.AddWithValue("@from", fromDate);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							retVal.Add((Guid)reader[0]);
						}
					}
				}
			}
			return retVal;
		}

		public bool WasChargedInTimePass(Guid expertiseId, Guid contactId, Guid supplierId, DateTime fromDate, Guid thisIncidentId)
		{
			bool retVal = false;
			using (SqlConnection conn = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand())
				{
					string query =
				@"select count(inc.incidentid) 
				from incident inc with(nolock)
				join new_incidentaccount ia with(nolock) on ia.new_incidentid = inc.incidentid
				where
				inc.deletionstatecode = 0 and ia.deletionstatecode = 0
				and inc.new_primaryexpertiseid = @expertiseId
				and inc.customerid = @contactId
				and inc.createdon >= @from
				and ia.new_accountid = @supplierId
				and ia.new_tocharge = 1";
					if (thisIncidentId != Guid.Empty)
					{
						query += " and incidentid != @incidentId ";
						command.Parameters.AddWithValue("@incidentId", thisIncidentId);
					}
					command.Connection = conn;
					command.CommandText = query;
					command.Connection.Open();
					command.Parameters.AddWithValue("@expertiseId", expertiseId);
					command.Parameters.AddWithValue("@contactId", contactId);
					command.Parameters.AddWithValue("@from", fromDate);
					command.Parameters.AddWithValue("@supplierId", supplierId);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							int scalarInt = (int)reader[0];
							if (scalarInt > 0)
							{
								retVal = true;
							}
						}
					}
				}
			}
			return retVal;
		}

		public bool IsExistsInInterval(Guid expertiseId, Guid contactId, Guid originId, DateTime fromDate, Guid regionId)
		{
			bool retVal = false;
			using (SqlConnection conn = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand())
				{
					string query =
				@"select count(inc.incidentid) 
				from incident inc with(nolock)
				where
				inc.deletionstatecode = 0
				and inc.new_primaryexpertiseid = @expertiseId
				and inc.customerid = @contactId
				and inc.createdon >= @from
				and inc.new_originid = @originId";
					if (regionId != Guid.Empty)
					{
						query += " and inc.new_regionid = @regionId";
						command.Parameters.AddWithValue("@regionId", regionId);
					}
					command.Connection = conn;
					command.CommandText = query;
					command.Connection.Open();
					command.Parameters.AddWithValue("@expertiseId", expertiseId);
					command.Parameters.AddWithValue("@from", fromDate);
					command.Parameters.AddWithValue("@contactId", contactId);
					command.Parameters.AddWithValue("@originId", originId);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							int scalarInt = (int)reader[0];
							if (scalarInt > 0)
							{
								retVal = true;
							}
						}
					}
				}
			}
			return retVal;
		}

		public List<Guid> GetIncidentsByUpsaleId(Guid upsaleId)
		{
			string query =
				@"select incidentid
				from incident with(nolock)
				where deletionstatecode = 0 
				and new_upsaleid = @upsaleId";
			List<Guid> retVal = new List<Guid>();
			using (SqlConnection conn = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand(query, conn))
				{
					command.Connection.Open();
					command.Parameters.AddWithValue("@upsaleId", upsaleId);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							retVal.Add((Guid)reader[0]);
						}
					}
				}
			}
			return retVal;
		}

		public List<Guid> GetRegionsOfSuppliersLostIncidents(Guid supplierId)
		{
			string query =
				@"select inc.new_regionid
				from incident inc with(nolock)
				where inc.deletionstatecode = 0
				and (inc.casetypecode = 200000 or inc.casetypecode = 3)
				and (statuscode = 1 or statuscode = 200003)
				and inc.createdon between dateadd(wk, -1, getdate()) and getdate()
				and inc.new_regionid is not null
				and 
				(
					(select count(new_incidentaccountid)
					from new_incidentaccount
					where deletionstatecode = 0
					and new_incidentid = inc.incidentid
					and (statuscode = 10 or statuscode = 12 or statuscode = 13)
					and new_accountid = @supplierId) = 0
				)
				and inc.new_primaryexpertiseid
				in
				(
					select new_primaryexpertiseid
					from new_accountexpertise
					where new_accountid = @supplierId
					and statecode = 0
					and deletionstatecode = 0
				)";
			List<Guid> retVal = new List<Guid>();
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand(query, connection))
				{
					command.Connection.Open();
					command.Parameters.AddWithValue("@supplierId", supplierId);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							retVal.Add((Guid)reader[0]);
						}
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// This function sums all the incident accounts that are connected to the passed incident 
		/// and thier status code is one of the supplied status codes
		/// </summary>
		/// <param name="IncidentId">The parent incident of the incident accounts</param>
		/// <param name="incidentAccountStatusCodes">The status codes to consider when suming the incident accounts</param>
		/// <returns>The amount of incident account</returns>
		public int GetIncidentAccountSum(Guid incidentId, params DML.Xrm.new_incidentaccount.Status[] incidentAccountStatusCodes)
		{
			int retVal = 0;
			string statusCodes = "";
			foreach (DML.Xrm.new_incidentaccount.Status incidentAccountStatusCode in incidentAccountStatusCodes)
			{
				statusCodes += statusCodes == "" ? "" : ",";
				statusCodes += ((int)incidentAccountStatusCode).ToString();
			}
			string query = string.Format("SELECT COUNT(new_incidentaccountid) AS NumOfAccounts FROM new_incidentaccount with(nolock) WHERE new_incidentid = '{0}' AND statuscode in ({1})", incidentId.ToString(), statusCodes);
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand(query, connection))
				{
					command.Connection.Open();
					using (SqlDataReader incidentAccountSum = command.ExecuteReader())
					{
						if (incidentAccountSum.Read())
							retVal = (int)incidentAccountSum["NumOfAccounts"];
					}
				}
			}
			return retVal;
		}

		#endregion

		#region Private Methods

		private NoProblem.Core.DataModel.Case.CaseType GetCaseType(NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode caseType, bool isDirectNumber)
		{
			if (caseType == NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode.DirectNumber || isDirectNumber)
			{
				return NoProblem.Core.DataModel.Case.CaseType.DirectNumber;
			}
			if (caseType == NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode.SupplierSpecified)
			{
				return NoProblem.Core.DataModel.Case.CaseType.Click2;
			}
			return NoProblem.Core.DataModel.Case.CaseType.SelfService;
		}

		private NoProblem.Core.DataModel.Case.EndStatus GetEndStatus(NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status status)
		{
			if (status == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSE
				|| status == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS)
			{
				return NoProblem.Core.DataModel.Case.EndStatus.Won;
			}
			if (status == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.INVITED_BY_SMS)
			{
				return NoProblem.Core.DataModel.Case.EndStatus.Invited;
			}
			if (status == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.LOST)
			{
				return NoProblem.Core.DataModel.Case.EndStatus.Lost;
			}
			return NoProblem.Core.DataModel.Case.EndStatus.InProcess;
		}

		#endregion

		public List<DML.PublisherPortal.GuidStringPair> FindHeadingsOfAarCases(DateTime fromDate, DateTime toDate)
		{
			string query =
				@"select distinct new_primaryexpertiseid, new_primaryexpertiseidname
				  from incident with(nolock)
				  where deletionstatecode = 0
				  and new_usedaar = 1
				  and createdon between @from and @to";
			List<DML.PublisherPortal.GuidStringPair> retVal =
				new List<DML.PublisherPortal.GuidStringPair>();
			using (SqlConnection conn = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand(query, conn))
				{
					command.Connection.Open();
					command.Parameters.AddWithValue("@from", fromDate);
					command.Parameters.AddWithValue("@to", toDate);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							DML.PublisherPortal.GuidStringPair pair =
								new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair();
							pair.Id = (Guid)reader[0];
							pair.Name = (string)reader[1];
							retVal.Add(pair);
						}
					}
				}
			}
			return retVal;
		}

		public List<DataModel.Reports.AarRequestReportDrillDownRow> GetAarRequestReportDrillDown(DateTime fromDate, DateTime toDate, Guid headingId)
		{
			string query =
				@"create table #tmpResult (ticketNumber nvarchar(10), caseCreatedOn datetime, isFullfiled bit
, AvgAttemptsAcsPress1 real, AvgAttemptsItcsPress1 real, AvgAttemptsItcsLastPress1 real
, AvgMinutesAcsPress1 real, AvgMinutesItcsPress1 real, AvgMinutesItcsLastPress1 real
, TotalAttemptsAcs int, TotalAttemptsItcs int, TotalAttemptsItcsLast int
, MinutesToFillfilment real, CallsToFullfilment real)

select
incidentid, createdon, new_ordered_providers, new_requiredaccountno, TicketNumber
into #tmpIncident
from
incident with(nolock)
where
deletionstatecode = 0
and new_usedaar = 1 
and new_primaryexpertiseid = @headingId             
and createdon between @from and @to
order by createdon

DECLARE @numberOfCalls INT
DECLARE @averageTimeTo1AC REAL
DECLARE @averageAttempsTo1AC REAL
DECLARE @averageTimeTo1ITC REAL
DECLARE @averageAttempsTo1ITC REAL
DECLARE @averageTimeTo1ITClast REAL
DECLARE @averageAttempsTo1ITClast REAL
DECLARE @numberOf1sAC INT
DECLARE @numberOf1sITC INT
DECLARE @numberOf1sITClast INT
DECLARE @noFullfilmentCount INT
DECLARE @fullfilmentCaseCount INT
DECLARE @averageFullfilmentTime REAL
DECLARE @averageFullfilmentCalls REAL
DECLARE @totalAcAttemptCount INT
DECLARE @totalItcAttemptCount INT
DECLARE @totalItcLastAttemptCount INT

SET @numberOfCalls = 0;
SET @averageTimeTo1AC = 0;
SET @averageAttempsTo1AC = 0;
SET @averageTimeTo1ITC = 0;
SET @averageAttempsTo1ITC = 0;
SET @averageTimeTo1ITClast = 0;
SET @averageAttempsTo1ITClast = 0;
SET @numberOf1sAC = 0;
SET @numberOf1sITC = 0;
SET @numberOf1sITClast = 0;
SET @noFullfilmentCount = 0;
SET @fullfilmentCaseCount = 0;
SET @averageFullfilmentTime = 0;
SET @averageFullfilmentCalls = 0;
SET @totalAcAttemptCount = 0;
SET @totalItcAttemptCount = 0;
SET @totalItcLastAttemptCount = 0;

DECLARE CaseIterator CURSOR FOR

select
incidentid, createdon, new_ordered_providers, new_requiredaccountno, TicketNumber
from
#tmpIncident

DECLARE @id UNIQUEIDENTIFIER
DECLARE @caseCreatedOn DATETIME
DECLARE @ordered INT
DECLARE @required INT
DECLARE @ticketNumber nvarchar(10)
OPEN CaseIterator       
FETCH CaseIterator INTO @id, @caseCreatedOn, @ordered, @required, @ticketNumber
WHILE @@FETCH_Status = 0
BEGIN

select 
ia.createdon, ivrt.new_ivrtypecode, ia.new_dtmfpressed
into #tmpCalls
from new_incidentaccount ia with(nolock)
join new_ivrflavor ivrf with(nolock) on ivrf.new_ivrflavorid = ia.new_ivrflavorid
join new_ivrtype ivrt with(nolock) on ivrt.new_ivrtypeid = ivrf.new_ivrtypeid
where ia.deletionstatecode = 0
and ia.new_incidentid = @id
and ia.new_isaarcall = 1
order by ia.createdon

DECLARE @tmpCallCount INT;
SET @tmpCallCount = 0;

DECLARE CallIterator CURSOR FOR

select 
createdon, new_ivrtypecode, new_dtmfpressed
from #tmpCalls

DECLARE @tempCreatedOn DATETIME;
SET @tempCreatedOn = @caseCreatedOn;
DECLARE @caseNumberOfCalls INT;
SET @caseNumberOfCalls = 0;

DECLARE @callCreatedOn DATETIME;
DECLARE @ivrTypeCode INT;
DECLARE @dtmfPressed NVARCHAR;
OPEN CallIterator
FETCH CallIterator INTO @callCreatedOn, @ivrTypeCode, @dtmfPressed
WHILE @@FETCH_Status = 0
BEGIN

	SET @numberOfCalls = @numberOfCalls + 1;
	SET @tmpCallCount = @tmpCallCount + 1;
	SET @caseNumberOfCalls = @caseNumberOfCalls + 1;

	IF @ivrTypeCode = 1--ITC
	BEGIN
		SET @totalItcAttemptCount = @totalItcAttemptCount + 1;
		IF @dtmfPressed = '1'
		BEGIN
			SET @averageAttempsTo1ITC = @averageAttempsTo1ITC + @tmpCallCount;
			SET @numberOf1sITC = @numberOf1sITC + 1;
			SET @averageTimeTo1ITC = @averageTimeTo1ITC + DATEDIFF(second, @tempCreatedOn, @callCreatedOn)
		END
	END
	ELSE IF @ivrTypeCode = 2--ITCLAST
	BEGIN
		SET @totalItcLastAttemptCount = @totalItcLastAttemptCount + 1;
		IF @dtmfPressed = '1'
		BEGIN
			SET @averageAttempsTo1ITClast = @averageAttempsTo1ITClast + @tmpCallCount;
			SET @numberOf1sITClast = @numberOf1sITClast + 1;
			SET @averageTimeTo1ITClast = @averageTimeTo1ITClast + DATEDIFF(second, @tempCreatedOn, @callCreatedOn)
		END
	END
	ELSE--AC
	BEGIN
		SET @totalAcAttemptCount = @totalAcAttemptCount + 1;
		IF @dtmfPressed = '1'
		BEGIN
			SET @averageAttempsTo1AC = @averageAttempsTo1AC + @tmpCallCount;
			SET @numberOf1sAC = @numberOf1sAC + 1;
			SET @averageTimeTo1AC = @averageTimeTo1AC + DATEDIFF(second, @tempCreatedOn, @callCreatedOn)
		END
	END

	IF @dtmfPressed = '1'
	BEGIN
		SET @tmpCallCount = 0;
		SET @tempCreatedOn = @callCreatedOn;
	END

	FETCH CallIterator INTO @callCreatedOn, @ivrTypeCode, @dtmfPressed
END
CLOSE CallIterator
DEALLOCATE CallIterator

drop table #tmpCalls;

IF @callCreatedOn is not null
BEGIN
IF @ordered >= @required
BEGIN
	SET @averageFullfilmentCalls = @averageFullfilmentCalls + @caseNumberOfCalls;
	SET @averageFullfilmentTime = @averageFullfilmentTime + DATEDIFF(second, @caseCreatedOn, @callCreatedOn);
	SET @fullfilmentCaseCount = @fullfilmentCaseCount + 1;
END
ELSE
	SET @noFullfilmentCount = @noFullfilmentCount + 1;
END

insert into #tmpResult(ticketNumber, caseCreatedOn, isFullfiled
, AvgAttemptsAcsPress1, AvgAttemptsItcsPress1, AvgAttemptsItcsLastPress1
, AvgMinutesAcsPress1, AvgMinutesItcsPress1, AvgMinutesItcsLastPress1
, totalAttemptsAcs, totalAttemptsItcs, totalAttemptsItcsLast
, MinutesToFillfilment, CallsToFullfilment)
values (@ticketNumber
, @caseCreatedOn
,case when @ordered >= @required then 1 else 0 end
,case @numberOf1sAC when 0 then -1 else @averageAttempsTo1AC / @numberOf1sAC end
,case @numberOf1sITC when 0 then -1 else @averageAttempsTo1ITC / @numberOf1sITC end
, case @numberOf1sITClast when 0 then -1 else @averageAttempsTo1ITClast / @numberOf1sITClast end
, case @numberOf1sAC when 0 then -1 else @averageTimeTo1AC / @numberOf1sAC / 60 end
, case @numberOf1sITC when 0 then -1 else @averageTimeTo1ITC / @numberOf1sITC / 60 end
, case @numberOf1sITClast when 0 then -1 else @averageTimeTo1ITClast / @numberOf1sITClast / 60 end
, @totalAcAttemptCount,@totalItcAttemptCount, @totalItcLastAttemptCount
, case when @ordered >= @required then @averageFullfilmentTime / 60 else -1 end
, case when @ordered >= @required then @averageFullfilmentCalls else -1 end
)

set @numberOf1sAC = 0;
set @averageAttempsTo1AC = 0;
set @numberOf1sITC = 0;
set @averageAttempsTo1ITC = 0;
set @numberOf1sITClast = 0;
set @averageAttempsTo1ITClast = 0;
set @averageTimeTo1AC = 0;
set @averageTimeTo1ITC = 0;
set @averageTimeTo1ITClast = 0;
set @totalAcAttemptCount = 0;
set @totalItcAttemptCount = 0;
set @totalItcLastAttemptCount = 0;
set @averageFullfilmentCalls = 0;
set @averageFullfilmentTime = 0;

FETCH CaseIterator INTO @id, @caseCreatedOn, @ordered, @required, @ticketNumber
END
CLOSE CaseIterator
DEALLOCATE CaseIterator

drop table #tmpIncident;
select * from #tmpResult
drop table #tmpResult";
			List<DataModel.Reports.AarRequestReportDrillDownRow> retVal = new List<NoProblem.Core.DataModel.Reports.AarRequestReportDrillDownRow>();
			using (SqlConnection conn = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand(query, conn))
				{
					command.Connection.Open();
					command.Parameters.AddWithValue("@headingId", headingId);
					command.Parameters.AddWithValue("@from", fromDate);
					command.Parameters.AddWithValue("@to", toDate);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							DataModel.Reports.AarRequestReportDrillDownRow row = new NoProblem.Core.DataModel.Reports.AarRequestReportDrillDownRow();
							row.AvgAttemptsAcsPress1 = (float)reader["AvgAttemptsAcsPress1"];
							row.AvgAttemptsItcsLastPress1 = (float)reader["AvgAttemptsItcsLastPress1"];
							row.AvgAttemptsItcsPress1 = (float)reader["AvgAttemptsItcsPress1"];
							row.AvgMinutesAcsPress1 = (float)reader["AvgMinutesAcsPress1"];
							row.AvgMinutesItcsLastPress1 = (float)reader["AvgMinutesItcsLastPress1"];
							row.AvgMinutesItcsPress1 = (float)reader["AvgMinutesItcsPress1"];
							row.CallsToFullfilment = (float)reader["CallsToFullfilment"];
							row.CaseNumber = (string)reader["TicketNumber"];
							row.IsFullfiled = (bool)reader["IsFullfiled"];
							row.MinutesToFillfilment = (float)reader["MinutesToFillfilment"];
							row.totalAttemptsAcs = (int)reader["totalAttemptsAcs"];
							row.totalAttemptsItcs = (int)reader["totalAttemptsItcs"];
							row.totalAttemptsItcsLast = (int)reader["totalAttemptsItcsLast"];
							retVal.Add(row);
						}
					}
				}
			}
			return retVal;
		}

		public NoProblem.Core.DataModel.Reports.AarRequestsReportRow GetAarRequestsReport(DateTime fromDate, DateTime toDate, Guid headingId)
		{
			string query =
				@"
select
incidentid, createdon, new_ordered_providers, new_requiredaccountno
into #tmpIncident
from
incident with(nolock)
where
deletionstatecode = 0
and new_usedaar = 1 ";
			if (headingId != Guid.Empty)
			{
				query += " and new_primaryexpertiseid = @headingId ";
			}
			query += @" and createdon between @from and @to
order by createdon

DECLARE @numberOfCalls INT
DECLARE @averageTimeTo1AC REAL
DECLARE @averageAttempsTo1AC REAL
DECLARE @averageTimeTo1ITC REAL
DECLARE @averageAttempsTo1ITC REAL
DECLARE @averageTimeTo1ITClast REAL
DECLARE @averageAttempsTo1ITClast REAL
DECLARE @numberOf1sAC INT
DECLARE @numberOf1sITC INT
DECLARE @numberOf1sITClast INT
DECLARE @noFullfilmentCount INT
DECLARE @fullfilmentCaseCount INT
DECLARE @averageFullfilmentTime REAL
DECLARE @averageFullfilmentCalls REAL
DECLARE @totalAcAttemptCount INT
DECLARE @totalItcAttemptCount INT
DECLARE @totalItcLastAttemptCount INT

SET @numberOfCalls = 0;
SET @averageTimeTo1AC = 0;
SET @averageAttempsTo1AC = 0;
SET @averageTimeTo1ITC = 0;
SET @averageAttempsTo1ITC = 0;
SET @averageTimeTo1ITClast = 0;
SET @averageAttempsTo1ITClast = 0;
SET @numberOf1sAC = 0;
SET @numberOf1sITC = 0;
SET @numberOf1sITClast = 0;
SET @noFullfilmentCount = 0;
SET @fullfilmentCaseCount = 0;
SET @averageFullfilmentTime = 0;
SET @averageFullfilmentCalls = 0;
SET @totalAcAttemptCount = 0;
SET @totalItcAttemptCount = 0;
SET @totalItcLastAttemptCount = 0;

DECLARE CaseIterator CURSOR FOR

select
incidentid, createdon, new_ordered_providers, new_requiredaccountno
from
#tmpIncident

DECLARE @id UNIQUEIDENTIFIER
DECLARE @caseCreatedOn DATETIME
DECLARE @ordered INT
DECLARE @required INT
OPEN CaseIterator       
FETCH CaseIterator INTO @id, @caseCreatedOn, @ordered, @required
WHILE @@FETCH_Status = 0
BEGIN

select 
ia.createdon, ivrt.new_ivrtypecode, ia.new_dtmfpressed
into #tmpCalls
from new_incidentaccount ia with(nolock)
join new_ivrflavor ivrf with(nolock) on ivrf.new_ivrflavorid = ia.new_ivrflavorid
join new_ivrtype ivrt with(nolock) on ivrt.new_ivrtypeid = ivrf.new_ivrtypeid
where ia.deletionstatecode = 0
and ia.new_incidentid = @id
and ia.new_isaarcall = 1
order by ia.createdon

DECLARE @tmpCallCount INT;
SET @tmpCallCount = 0;

DECLARE CallIterator CURSOR FOR

select 
createdon, new_ivrtypecode, new_dtmfpressed
from #tmpCalls

DECLARE @tempCreatedOn DATETIME;
SET @tempCreatedOn = @caseCreatedOn;
DECLARE @caseNumberOfCalls INT;
SET @caseNumberOfCalls = 0;

DECLARE @callCreatedOn DATETIME;
DECLARE @ivrTypeCode INT;
DECLARE @dtmfPressed NVARCHAR;
OPEN CallIterator
FETCH CallIterator INTO @callCreatedOn, @ivrTypeCode, @dtmfPressed
WHILE @@FETCH_Status = 0
BEGIN

	SET @numberOfCalls = @numberOfCalls + 1;
	SET @tmpCallCount = @tmpCallCount + 1;
	SET @caseNumberOfCalls = @caseNumberOfCalls + 1;

	IF @ivrTypeCode = 1--ITC
	BEGIN
		SET @totalItcAttemptCount = @totalItcAttemptCount + 1;
		IF @dtmfPressed = '1'
		BEGIN
			SET @averageAttempsTo1ITC = @averageAttempsTo1ITC + @tmpCallCount;
			SET @numberOf1sITC = @numberOf1sITC + 1;
			SET @averageTimeTo1ITC = @averageTimeTo1ITC + DATEDIFF(second, @tempCreatedOn, @callCreatedOn)
		END
	END
	ELSE IF @ivrTypeCode = 2--ITCLAST
	BEGIN
		SET @totalItcLastAttemptCount = @totalItcLastAttemptCount + 1;
		IF @dtmfPressed = '1'
		BEGIN
			SET @averageAttempsTo1ITClast = @averageAttempsTo1ITClast + @tmpCallCount;
			SET @numberOf1sITClast = @numberOf1sITClast + 1;
			SET @averageTimeTo1ITClast = @averageTimeTo1ITClast + DATEDIFF(second, @tempCreatedOn, @callCreatedOn)
		END
	END
	ELSE--AC
	BEGIN
		SET @totalAcAttemptCount = @totalAcAttemptCount + 1;
		IF @dtmfPressed = '1'
		BEGIN
			SET @averageAttempsTo1AC = @averageAttempsTo1AC + @tmpCallCount;
			SET @numberOf1sAC = @numberOf1sAC + 1;
			SET @averageTimeTo1AC = @averageTimeTo1AC + DATEDIFF(second, @tempCreatedOn, @callCreatedOn)
		END
	END

	IF @dtmfPressed = '1'
	BEGIN
		SET @tmpCallCount = 0;
		SET @tempCreatedOn = @callCreatedOn;
	END

	FETCH CallIterator INTO @callCreatedOn, @ivrTypeCode, @dtmfPressed
END
CLOSE CallIterator
DEALLOCATE CallIterator

drop table #tmpCalls;

IF @callCreatedOn is not null
BEGIN
IF @ordered >= @required
BEGIN
	SET @averageFullfilmentCalls = @averageFullfilmentCalls + @caseNumberOfCalls;
	SET @averageFullfilmentTime = @averageFullfilmentTime + DATEDIFF(second, @caseCreatedOn, @callCreatedOn);
	SET @fullfilmentCaseCount = @fullfilmentCaseCount + 1;
END
ELSE
	SET @noFullfilmentCount = @noFullfilmentCount + 1;
END

FETCH CaseIterator INTO @id, @caseCreatedOn, @ordered, @required
END
CLOSE CaseIterator
DEALLOCATE CaseIterator

select 
--@numberOfCalls as 'number of call'
--, @averageAttempsTo1AC as 'acs attempts sum'
--, @numberOf1sAC as 'number of acs'
 case @numberOf1sAC when 0 then -1 else @averageAttempsTo1AC / @numberOf1sAC end as 'Avg attempts ACs'
--, @averageAttempsTo1ITC as 'itc attempts sum'
--, @numberOf1sITC as 'number of itc'
, case @numberOf1sITC when 0 then -1 else @averageAttempsTo1ITC / @numberOf1sITC end as 'Avg attempts ITC'
--, @averageAttempsTo1ITClast as 'itc last attempts sum'
--, @numberOf1sITClast as 'number of itc last'
, case @numberOf1sITClast when 0 then -1 else @averageAttempsTo1ITClast / @numberOf1sITClast end as 'Avg attempts ITC last'
--, @averageTimeTo1AC as 'seconds sum ac'
, case @numberOf1sAC when 0 then -1 else @averageTimeTo1AC / @numberOf1sAC / 60 end as 'Avg minutes AC'
--, @averageTimeTo1ITC as 'seconds sum itc'
, case @numberOf1sITC when 0 then -1 else @averageTimeTo1ITC / @numberOf1sITC / 60 end as 'Avg minutes ITC'
--,@averageTimeTo1ITClast as 'seconds sum itc last'
, case @numberOf1sITClast when 0 then -1 else @averageTimeTo1ITClast / @numberOf1sITClast / 60 end as 'Avg minutes ITC last'
--,@averageFullfilmentCalls as 'calls to fullfilment sum'
--,@averageFullfilmentTime as 'seconds to fullfilment sum'
, @noFullfilmentCount as 'no fullfilment count'
, @fullfilmentCaseCount as 'fullfilment count'
, case @fullfilmentCaseCount when 0 then -1 else @averageFullfilmentCalls / @fullfilmentCaseCount end as 'Avg calls to fullfilment'
, case @fullfilmentCaseCount when 0 then -1 else @averageFullfilmentTime / @fullfilmentCaseCount / 60 end as 'Avg minutes to fullfilment'
,@totalAcAttemptCount as 'total ac attempts'
, @totalItcAttemptCount as 'total itc attempts'
, @totalItcLastAttemptCount as 'total itc last attempts'

drop table #tmpIncident;
";

			DML.Reports.AarRequestsReportRow row = new NoProblem.Core.DataModel.Reports.AarRequestsReportRow();
			using (SqlConnection conn = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand(query, conn))
				{
					command.Connection.Open();
					if (headingId != Guid.Empty)
					{
						command.Parameters.AddWithValue("@headingId", headingId);
					}
					command.Parameters.AddWithValue("@from", fromDate);
					command.Parameters.AddWithValue("@to", toDate);
					using (SqlDataReader reader = command.ExecuteReader(System.Data.CommandBehavior.SingleRow))
					{
						if (reader.Read())
						{
							row.AvgAttemptsForAcPress1 = (float)reader["Avg attempts ACs"];
							row.AvgAttemptsForItcPress1 = (float)reader["Avg attempts ITC"];
							row.AvgAttemptsForItcLastPress1 = (float)reader["Avg attempts ITC last"];
							row.AvgAttemptsForFullfilment = (float)reader["Avg calls to fullfilment"];
							row.AvgMinutesForAcPress1 = (float)reader["Avg minutes AC"];
							row.AvgMinutesForFullfilment = (float)reader["Avg minutes to fullfilment"];
							row.AvgMinutesForItcLastPress1 = (float)reader["Avg minutes ITC last"];
							row.AvgMinutesForItcPress1 = (float)reader["Avg minutes ITC"];
							row.NumberOfAttemptsToAcs = (int)reader["total ac attempts"];
							row.NumberOfAttemptsToItc = (int)reader["total itc attempts"];
							row.NumberOfAttemptsToItcLast = (int)reader["total itc last attempts"];
							row.NumberOfFullfiledCases = (int)reader["fullfilment count"];
							row.NumberOfNotFullfiledCases = (int)reader["no fullfilment count"];
						}
					}
				}
			}
			return row;
		}

		public bool HasBillableCalls(Guid incidentId)
		{
			SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.incident>.SqlParametersList();
			parameters.Add("@id", incidentId);
			object scalar = ExecuteScalar(queryForHasBillableCalls, parameters);
			return scalar != null;
		}

		private const string queryForHasBillableCalls = 
			@"select 1 
			from new_incidentaccountextensionbase with(nolock) 
			where 
			new_incidentid = @id
			and new_tocharge = 1";

        
        #region mobile
        internal bool IsMobileIncidentOpen(Guid IncidentId, out DML.Xrm.incident.Status IncidentStatus)
        {
            string command = @"[dbo].[IsMobileIncidentOpen]";
            bool _isOpen, _toClosedAllIncidentAccount;
            //and new_requiredaccountno < new_ordered_providers";//new, waiting, after bidding
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@incidentId", IncidentId);
                    SqlParameter isOpen = new SqlParameter("@isOpen", SqlDbType.Bit);
                    isOpen.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(isOpen);
                    SqlParameter toClosedAllIncidentAccount = new SqlParameter("@toClosedAllIncidentAccount", SqlDbType.Bit);
                    toClosedAllIncidentAccount.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(toClosedAllIncidentAccount);
                    conn.Open();
                    object o = cmd.ExecuteScalar();
                    IncidentStatus = (o == DBNull.Value) ? DML.Xrm.incident.Status.WORK_DONE : (DML.Xrm.incident.Status)((int)o);
                    _isOpen = (bool)cmd.Parameters["@isOpen"].Value;
                    _toClosedAllIncidentAccount = (bool)cmd.Parameters["@toClosedAllIncidentAccount"].Value;
                    conn.Close();
                }
            }           
            return _isOpen;
        }
        public int GetNextMobileCallOrder(Guid IncidentId)
        {
            string query = @"SELECT [dbo].[GetNextMobileCallOrder] (@IncidentId)";
            int result;
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@IncidentId", IncidentId);
                    result = (int)cmd.ExecuteScalar();
                    conn.Close();
                }
            }
            return result;
        }
        public bool IsMobileIncidentInConversation(Guid IncidentId)
        {
            string query = @"SELECT [dbo].[IsMobileInConversation] (@IncidentId)";
            bool result;
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", IncidentId);
                    result = (bool)cmd.ExecuteScalar();
                    conn.Close();
                }
            }
            return result;
        }
        public DML.Xrm.new_incidentaccount GetNextToCall(DataAccessLayer.IncidentAccount incidentAccountDal, Guid IncidentId)
        {
            string query = @"SELECT [dbo].[GetNextMobileToCall] (@IncidentId)";
            object obj = null;
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", IncidentId);
                    obj = cmd.ExecuteScalar();
                    conn.Close();
                }
            }
            if (obj == null || obj == DBNull.Value)
                return null;
            return incidentAccountDal.Retrieve((Guid)obj);
        }
       
        #endregion
    }
}
