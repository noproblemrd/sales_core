﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class DirectNumber : DalBase<DataModel.Xrm.new_directnumber>
    {
        public static object lockObject = new object();

        #region Ctor
        public DirectNumber(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_directnumber");
        }
        #endregion

        private const string cachePrefix = "NoProblem.Core.DataAccessLayer.DirectNumber";

        #region Public Methods

        public override IQueryable<DataModel.Xrm.new_directnumber> All
        {
            get
            {
                return XrmDataContext.new_directnumbers;
            }
        }

        public override NoProblem.Core.DataModel.Xrm.new_directnumber Retrieve(Guid id)
        {
            DataModel.Xrm.new_directnumber number = XrmDataContext.new_directnumbers.SingleOrDefault(x => x.new_directnumberid == id);
            return number;
        }

        public NoProblem.Core.DataModel.Xrm.new_directnumber GetDirectNumberByNumber(string number)
        {
            DataModel.Xrm.new_directnumber directNumber = XrmDataContext.new_directnumbers.FirstOrDefault(x => x.new_number == number);
            return directNumber;
        }

        public NoProblem.Core.DataModel.Xrm.new_directnumber GetDirectNumberByAccountExpertise(Guid accountExpertiseId)
        {
            DataModel.Xrm.new_directnumber directNumber = XrmDataContext.new_directnumbers.FirstOrDefault(x => x.new_accountexpertiseid == accountExpertiseId);
            return directNumber;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_directnumber> GetAllDirectNumbersOfAccountExpertise(Guid accountExpertiseId)
        {
            var dns = XrmDataContext.new_directnumbers.Where(x => x.new_accountexpertiseid == accountExpertiseId);
            return dns;
        }

        public NoProblem.Core.DataModel.Xrm.new_directnumber GetDirectNumberByNumberIfFree(string number)
        {
            //var directNumberList =
            //    (from d in XrmDataContext.new_directnumbers
            //     where (d.new_number == number
            //        && !d.new_accountexpertiseid.HasValue
            //         && (d.new_frozendate == null ||
            //             d.new_frozendate < DateTime.Now.AddMonths(-2))
            //         )
            //     select d);
            //DataModel.Xrm.new_directnumber directNumber = directNumberList.FirstOrDefault();
            //return directNumber;
            string query = @"select top 1 new_directnumberid from new_directnumber with(nolock)
                where deletionstatecode = 0
                and new_number = @number
                and isnull(new_iscallerid, 0) = 0
                and isnull(new_frozendate, '2000-01-01 00:00:00.000') < dateadd(month, -2, getdate())
                and new_accountexpertiseid is null";
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_directnumber>.SqlParametersList();
            parameters.Add("@number", number);
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null)
            {
                return Retrieve((Guid)scalar);
            }
            else
            {
                return null;
            }
        }        

        public List<Guid> GetFreeDirectNumbersInRegion(Guid regionId)
        {
            string query =
                @"select new_directnumberid from new_directnumber with(nolock)
                where deletionstatecode = 0
                and new_accountexpertiseid is null
                and isnull(new_iscallerid, 0) = 0
                and isnull(new_frozendate, '2000-01-01 00:00:00.000') < dateadd(month, -2, getdate())
                and new_regionid = @regionId";
            List<Guid> freeNumbers = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Parameters.AddWithValue("@regionId", regionId);
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            freeNumbers.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return freeNumbers;
        }

        public List<Guid> GetFreeDirectNumbers()
        {
            string query =
                @"select new_directnumberid from new_directnumber with(nolock)
                where deletionstatecode = 0
                and isnull(new_iscallerid, 0) = 0
                and new_accountexpertiseid is null
                and isnull(new_frozendate, '2000-01-01 00:00:00.000') < dateadd(month, -2, getdate())";
            List<Guid> freeNumbers = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            freeNumbers.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return freeNumbers;
        }

        public List<string> GetFreeDirectNumbersNumbers()
        {
            List<string> retVal = new List<string>();
            string query =
                @"select top 408 new_number from new_directnumber with(nolock)
                where deletionstatecode = 0
                and isnull(new_iscallerid, 0) = 0
                and isnull(new_frozendate, '2000-01-01 00:00:00.000') < dateadd(month, -2, getdate())
                and new_accountexpertiseid is null";
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                retVal.Add(Convert.ToString(row["new_number"]));
            }
            return retVal;
        }

        public void DetachDirectNumbersFromAccountExpertise(Guid accountExpertiseId, bool freeze)
        {
            var dns = XrmDataContext.new_directnumbers.Where(x => x.new_accountexpertiseid == accountExpertiseId);
            foreach (var dn in dns)
            {
                dn.new_accountexpertiseid = null;
                dn.new_originid = null;
                if (freeze)
                    dn.new_frozendate = DateTime.Now;
                Update(dn);
            }
            XrmDataContext.SaveChanges();
        }

        public NoProblem.Core.DataModel.Xrm.new_directnumber GetOneFreeDirectNumber()
        {
            //var directNumberList =
            //    (from d in XrmDataContext.new_directnumbers
            //     where (!d.new_accountexpertiseid.HasValue
            //         && (d.new_frozendate == null ||
            //             d.new_frozendate < DateTime.Now.AddMonths(-2))
            //         )
            //     select d);
            //DataModel.Xrm.new_directnumber directNumber = directNumberList.FirstOrDefault();
            //return directNumber;
            string query = @"select top 1 new_directnumberid from new_directnumber with(nolock)
                where deletionstatecode = 0
                and isnull(new_iscallerid, 0) = 0
                and isnull(new_frozendate, '2000-01-01 00:00:00.000') < dateadd(month, -2, getdate())
                and new_accountexpertiseid is null";
            object scalar = ExecuteScalar(query);
            if (scalar != null)
            {
                return Retrieve((Guid)scalar);
            }
            else
            {
                return null;
            }
        }

        public bool IsDirectNumber(string phone)
        {
            if (String.IsNullOrEmpty(phone))
                return false;
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@phone", phone);
            object scalar = ExecuteScalar(IsDirectNumberQuery, parameters);
            return scalar != null;
        }

        private const string IsDirectNumberQuery =
            @"
                select 1 
                from New_directnumber with(nolock)
                where New_number = @phone
            ";

        #endregion

    }
}
