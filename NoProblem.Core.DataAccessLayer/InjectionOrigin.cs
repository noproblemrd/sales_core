﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class InjectionOrigin : DalBase<DataModel.Xrm.new_injectionorigin>
    {
        public InjectionOrigin(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_injectionorigin");
        }

        public override IQueryable<DataModel.Xrm.new_injectionorigin> All
        {
            get
            {
                return XrmDataContext.new_injectionorigins;
            }
        }

        public override DataModel.Xrm.new_injectionorigin Retrieve(Guid id)
        {
            return XrmDataContext.new_injectionorigins.FirstOrDefault(x => x.new_injectionid == id);
        }
    }
}
