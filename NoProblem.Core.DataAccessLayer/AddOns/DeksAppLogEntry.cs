﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class DeksAppLogEntry : DalBase<DataModel.Xrm.new_deskapplogentry>
    {
        public DeksAppLogEntry(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_deskapplogentry");
        }

        public override IQueryable<DataModel.Xrm.new_deskapplogentry> All
        {
            get
            {
                return XrmDataContext.new_deskapplogentrynew_deskapplogentries;
            }
        }

        public override DataModel.Xrm.new_deskapplogentry Retrieve(Guid id)
        {
            return XrmDataContext.new_deskapplogentrynew_deskapplogentries.FirstOrDefault(x => x.new_deskapplogentryid == id);
        }
    }
}
