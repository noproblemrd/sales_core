﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AddOnActionByUpdater : DalBase<DataModel.Xrm.new_addonactionbyupdater>
    {
        public AddOnActionByUpdater(DataModel.Xrm.DataContext xrmDatacontext)
            : base(xrmDatacontext)
        {
            DataModel.XrmDataContext.ClearCache("new_addonactionbyupdater");
        }

        public override NoProblem.Core.DataModel.Xrm.new_addonactionbyupdater Retrieve(Guid id)
        {
            return XrmDataContext.new_addonactionbyupdaters.FirstOrDefault(x => x.new_addonactionbyupdaterid == id);
        }
    }
}
