﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AddOnUser : DalBase<DataModel.Xrm.new_addonuser>
    {
        public AddOnUser(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_addonuser");
        }

        public override IQueryable<DataModel.Xrm.new_addonuser> All
        {
            get
            {
                return XrmDataContext.new_addonusers;
            }
        }

        public override NoProblem.Core.DataModel.Xrm.new_addonuser Retrieve(Guid id)
        {
            return XrmDataContext.new_addonusers.FirstOrDefault(x => x.new_addonuserid == id);
        }
    }
}
