﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AddOnAction : DalBase<DataModel.Xrm.new_addonaction>
    {
        public AddOnAction(DataModel.Xrm.DataContext xrmDatacontext)
            : base(xrmDatacontext)
        {
            DataModel.XrmDataContext.ClearCache("new_addonaction");
        }

        public override NoProblem.Core.DataModel.Xrm.new_addonaction Retrieve(Guid id)
        {
            return XrmDataContext.new_addonactions.FirstOrDefault(x => x.new_addonactionid == id);
        }
    }
}
