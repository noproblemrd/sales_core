﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class UserInstalledProgram : DalBase<DataModel.Xrm.new_userinstalledprogram>
    {
        public UserInstalledProgram(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_userinstalledprogram");
        }

        public override IQueryable<DataModel.Xrm.new_userinstalledprogram> All
        {
            get
            {
                return XrmDataContext.new_userinstalledprograms;
            }
        }

        public override DataModel.Xrm.new_userinstalledprogram Retrieve(Guid id)
        {
            return XrmDataContext.new_userinstalledprograms.FirstOrDefault(x => x.new_userinstalledprogramid == id);
        }
    }
}
