﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AddOnMACAddress : DalBase<DataModel.Xrm.new_addonmacaddress>
    {
        public AddOnMACAddress(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_addonmacaddress");
        }

        public override IQueryable<DataModel.Xrm.new_addonmacaddress> All
        {
            get
            {
                return XrmDataContext.new_addonmacaddresses;
            }
        }

        public override NoProblem.Core.DataModel.Xrm.new_addonmacaddress Retrieve(Guid id)
        {
            return XrmDataContext.new_addonmacaddresses.FirstOrDefault(x => x.new_addonmacaddressid == id);
        }

        public NoProblem.Core.DataModel.Xrm.new_addonmacaddress FindMacAddressEntry(string address)
        {
            return XrmDataContext.new_addonmacaddresses.FirstOrDefault(x => x.new_name == address);
        }
    }
}
