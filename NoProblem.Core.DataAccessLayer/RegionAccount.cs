﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.RegionAccount
//  File: RegionAccount.cs
//  Description: DAL for RegionAccount Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class RegionAccount : DalBase<DML.Xrm.new_regionaccountexpertise>
    {
        #region Ctor
        public RegionAccount(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_regionaccountexpertise");
        }
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise Retrieve(Guid id)
        {
            DML.Xrm.new_regionaccountexpertise regAcc = XrmDataContext.new_regionaccountexpertises.FirstOrDefault(x => x.new_regionaccountexpertiseid == id);
            return regAcc;
        }

        public DML.Xrm.new_regionaccountexpertise GetByRegionIdAndAccountId(Guid regionId, Guid accountId)
        {
            DML.Xrm.new_regionaccountexpertise rae = (from rea in XrmDataContext.new_regionaccountexpertises
                                                      where
                                                      rea.statecode == ACTIVE
                                                      &&
                                                      rea.new_accountid.Value == accountId
                                                      &&
                                                      rea.new_regionid.Value == regionId
                                                      select
                                                      rea).SingleOrDefault();
            return rae;
        }

        public DML.Regions.RegionAccountMinData GetIdByRegionIdAndAccountId(Guid regionId, Guid accountId)
        {
            DML.Regions.RegionAccountMinData retVal = null;
            string query =
                @"select top 1 new_regionaccountexpertiseid, new_regionstate from new_regionaccountexpertise with(nolock)
                    where deletionstatecode = 0 and statecode = 0 and new_accountid = @supplierId and new_regionid = @regionId";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@supplierId", accountId);
                    command.Parameters.AddWithValue("@regionId", regionId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            retVal = new NoProblem.Core.DataModel.Regions.RegionAccountMinData();
                            retVal.Id = (Guid)reader["new_regionaccountexpertiseid"];
                            retVal.RegionState = (DML.Xrm.new_regionaccountexpertise.RegionState)((int)reader["new_regionstate"]);
                            retVal.SupplierId = accountId;
                            retVal.RegionId = regionId;
                        }
                    }
                }
            }
            return retVal;
        }

        public List<DML.Regions.RegionAccountMinData> GetByAccountIdAndLevel(Guid accountId, int level)
        {
            List<DML.Regions.RegionAccountMinData> retVal = new List<NoProblem.Core.DataModel.Regions.RegionAccountMinData>();
            string query =
                @"select ra.new_regionid, ra.new_regionaccountexpertiseid, ra.new_regionstate
                from new_regionaccountexpertise ra with(nolock)
                join new_region rg with(nolock) on ra.new_regionid = rg.new_regionid
                where ra.deletionstatecode = 0 and rg.deletionstatecode = 0 and ra.new_accountid = @supplierId and rg.new_level = @level";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@supplierId", accountId);
                    command.Parameters.AddWithValue("@level", level);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.Regions.RegionAccountMinData data = new NoProblem.Core.DataModel.Regions.RegionAccountMinData();
                            data.Id = (Guid)reader["new_regionaccountexpertiseid"];
                            data.RegionId = (Guid)reader["new_regionid"];
                            data.RegionState = (DML.Xrm.new_regionaccountexpertise.RegionState)((int)reader["new_regionstate"]);
                            data.SupplierId = accountId;
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
            //IEnumerable<DML.Xrm.new_regionaccountexpertise> AllAccountsRegionAccounts = from rae in XrmDataContext.new_regionaccountexpertises
            //                                                                            where
            //                                                                            rae.new_accountid.Value == accountId
            //                                                                            select
            //                                                                            rae;
            //List<DML.Xrm.new_regionaccountexpertise> retVal = new List<DML.Xrm.new_regionaccountexpertise>();
            //foreach (DML.Xrm.new_regionaccountexpertise reAcc in AllAccountsRegionAccounts)
            //{
            //    DML.Xrm.new_region region = (from reg in XrmDataContext.new_regions
            //                                 where
            //                                 reg.new_regionid == reAcc.new_regionid.Value
            //                                 &&
            //                                 reg.new_level.Value == level
            //                                 select reg).SingleOrDefault();
            //    if (region != null)
            //    {
            //        retVal.Add(reAcc);
            //    }
            //}
            //return retVal;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise> GetForTreeUpdate(Guid supplierId, IEnumerable<DML.Regions.RegionDataForUpdate> cleanData)
        {
            FixRegionAccountDataFromMap(supplierId);

            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            builder.Append("select new_regionaccountexpertiseid from new_regionaccountexpertise with (nolock) where deletionstatecode = 0 and new_accountid = '");
            builder.Append(supplierId.ToString());
            builder.Append("'");
            foreach (DML.Regions.RegionDataForUpdate clean in cleanData)
            {
                builder.Append(" and (new_parent");
                builder.Append(clean.Level.ToString());
                builder.Append("id != '");
                builder.Append(clean.RegionId.ToString());
                builder.Append("' ");
                builder.Append("or new_parent");
                builder.Append(clean.Level.ToString());
                builder.Append("id is null)");
            }
            List<NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise> retVals = new List<NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(builder.ToString(), connection))
                {
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Guid id = new Guid(reader["new_regionaccountexpertiseid"].ToString());
                            DML.Xrm.new_regionaccountexpertise regAcc = Retrieve(id);
                            retVals.Add(regAcc);
                        }
                    }
                }
            }
            return retVals;
        }

        private void FixRegionAccountDataFromMap(Guid supplierId)
        {
            string preQuery = "select new_regionaccountexpertiseid, new_regionid from new_regionaccountexpertise with (nolock) where deletionstatecode = 0 and new_parent1id is null and new_accountid = @supplierId";
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            Reader preReader = ExecuteReader(preQuery, parameters);
            Region regDal = new Region(XrmDataContext);
            foreach (var row in preReader)
            {
                var regData = regDal.GetRegionMinDataById((Guid)row["new_regionid"]);
                var parents = regDal.GetParents(regData);
                DataModel.Xrm.new_regionaccountexpertise ra = new DML.Xrm.new_regionaccountexpertise(XrmDataContext);
                ra.new_regionaccountexpertiseid = (Guid)row["new_regionaccountexpertiseid"];
                foreach (var parent in parents)
                {
                    switch (parent.Level)
                    {
                        case 1:
                            ra.new_parent1id = parent.Id;
                            break;
                        case 2:
                            ra.new_parent2id = parent.Id;
                            break;
                        case 3:
                            ra.new_parent3id = parent.Id;
                            break;
                        case 4:
                            ra.new_parent4id = parent.Id;
                            break;
                    }
                }
                switch (regData.Level)
                {
                    case 1:
                        ra.new_parent1id = regData.Id;
                        break;
                    case 2:
                        ra.new_parent2id = regData.Id;
                        break;
                    case 3:
                        ra.new_parent3id = regData.Id;
                        break;
                    case 4:
                        ra.new_parent4id = regData.Id;
                        break;
                }
                Update(ra);
                XrmDataContext.SaveChanges();
            }
        }

        public IEnumerable<DataModel.SqlHelper.RegionAccountMinData> GetRegionAccountMinDataByAccountId(Guid supplierId, Guid regionId, string parentsParam, SqlConnection conn)
        {
            List<DataModel.SqlHelper.RegionAccountMinData> retVal = new List<DataModel.SqlHelper.RegionAccountMinData>();
            string query = @"select new_regionid,new_regionstate from new_regionaccountexpertise with (nolock) where new_accountid = @supplierId and deletionstatecode = 0
                            and ( new_regionid = @regionId or new_regionid in ({0}) )";
                        
            query = string.Format(query, parentsParam);
            using (SqlCommand command = new SqlCommand(query, conn))
            {
                command.Parameters.AddWithValue("@supplierId", supplierId);
                command.Parameters.AddWithValue("@regionId", regionId);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        object regIdObj = reader["new_regionid"];
                        object stateObj = reader["new_regionstate"];
                        if (regIdObj != null && regIdObj != DBNull.Value && stateObj != null && stateObj != DBNull.Value)
                        {
                            DataModel.SqlHelper.RegionAccountMinData data = new NoProblem.Core.DataModel.SqlHelper.RegionAccountMinData();
                            data.RegionId = (Guid)regIdObj;
                            data.RegionState = (DataModel.Xrm.new_regionaccountexpertise.RegionState)((int)stateObj);
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public List<DataModel.Regions.RegionAccountMinData> GetMinDataByAccountId(Guid supplierId)
        {
            List<DML.Regions.RegionAccountMinData> retVal = new List<NoProblem.Core.DataModel.Regions.RegionAccountMinData>();
            string query =
                @"select ra.new_regionid, ra.new_regionstate, ra.new_regionaccountexpertiseid, rg.new_level
                from new_regionaccountexpertise ra with(nolock)
                join new_region rg with(nolock) on ra.new_regionid = rg.new_regionid
                where ra.new_accountid = @supplierId and ra.deletionstatecode = 0 and rg.deletionstatecode = 0";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@supplierId", supplierId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.Regions.RegionAccountMinData data = new NoProblem.Core.DataModel.Regions.RegionAccountMinData();
                            data.SupplierId = supplierId;
                            data.Id = (Guid)reader["new_regionaccountexpertiseid"];
                            data.Level = (int)reader["new_level"];
                            data.RegionId = (Guid)reader["new_regionid"];
                            data.RegionState = (DML.Xrm.new_regionaccountexpertise.RegionState)((int)reader["new_regionstate"]);
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public IEnumerable<Guid> GetByAccountId(Guid supplierId)
        {
            string query =
                @"select new_regionaccountexpertiseid from new_regionaccountexpertise with(nolock)
                    where deletionstatecode = 0 and new_accountid = @supplierId";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@supplierId", supplierId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
