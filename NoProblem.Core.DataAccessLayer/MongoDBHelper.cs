﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using System.Configuration;

namespace NoProblem.Core.DataAccessLayer
{
    public class MongoDBHelper
    {        
        private MongoClient client;
        private MongoServer server;
        private MongoDatabase database;
        private static object syncRoot = new object();
        private static volatile MongoDBHelper instance;
        private const string connectionStringName = "mongodbConnectionString";

        private MongoDBHelper()
        {
            MongoUrl mongoUrl = new MongoDB.Driver.MongoUrl(ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString);
            client = new MongoDB.Driver.MongoClient(mongoUrl);
            server = client.GetServer();
            database = server.GetDatabase(mongoUrl.DatabaseName);
        }

        public static MongoDBHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new MongoDBHelper();
                        }
                    }
                }
                return instance;
            }
        }

        public MongoCollection GetCollection(string collectionName)
        {
            return database.GetCollection(collectionName);
        }
    }
}
