﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class MandrillEmailSubjects : DalBase<DataModel.Xrm.new_emailsubject>
    {
        #region Ctor
        public MandrillEmailSubjects(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_emailsubject");
        } 
        #endregion

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public override NoProblem.Core.DataModel.Xrm.new_emailsubject Retrieve(Guid id)
        {
            return XrmDataContext.new_emailsubjects.FirstOrDefault(x => x.new_emailsubjectid == id);
        }

        public string GetEmailSubject(string templateName)
        {
            return CacheManager.Instance.Get<string>(cachePrefix, templateName, GetEmailSubjectMethod);
        }

        private string GetEmailSubjectMethod(string templateName)
        {
            string query = 
                @"select new_subject
                    from
                    new_emailsubject with (nolock)
                    where
                    new_name = @name and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@name", templateName));
            object scalar = ExecuteScalar(query, parameters);
            string retVal = string.Empty;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }
    }
}
