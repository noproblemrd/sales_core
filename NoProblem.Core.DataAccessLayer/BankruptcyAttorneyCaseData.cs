﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class BankruptcyAttorneyCaseData : DalBase<DataModel.Xrm.new_bankruptcyattorneycasedata>
    {
        public BankruptcyAttorneyCaseData(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_bankruptcyattorneycasedata");
        }

        public override NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata Retrieve(Guid id)
        {
            return XrmDataContext.new_bankruptcyattorneycasedatas.FirstOrDefault(x => x.new_bankruptcyattorneycasedataid == id);
        }
    }
}
