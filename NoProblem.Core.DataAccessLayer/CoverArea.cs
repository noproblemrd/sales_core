﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.DataAccessLayer
{
    public class CoverArea : DalBase<DataModel.Xrm.new_coverarea>
    {
        public CoverArea(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_coverarea");
        }

        public override IQueryable<new_coverarea> All
        {
            get
            {
                return XrmDataContext.new_coverareas;
            }
        }

        public override DataModel.Xrm.new_coverarea Retrieve(Guid id)
        {
            return XrmDataContext.new_coverareas.FirstOrDefault(x => x.new_coverareaid == id);
        }
    }
}
