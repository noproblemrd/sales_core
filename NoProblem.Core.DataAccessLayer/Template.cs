﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class Template : DalBase<DataModel.Xrm.template>
    {
        public Template(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("template");
        }

        public override NoProblem.Core.DataModel.Xrm.template Retrieve(Guid id)
        {
            DataModel.Xrm.template template = XrmDataContext.templates.FirstOrDefault(x => x.templateid == id);
            return template;
        }

        public DataModel.Xrm.template GetTemplateByTitle(string title)
        {
            DataModel.Xrm.template template = XrmDataContext.templates.FirstOrDefault(x => x.title == title);
            return template;
        }
    }
}
