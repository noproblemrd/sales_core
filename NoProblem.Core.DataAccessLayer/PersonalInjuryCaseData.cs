﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class PersonalInjuryCaseData : DalBase<DataModel.Xrm.new_personalinjurycasedata>
    {
        public PersonalInjuryCaseData(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_personalinjurycasedata");
        }

        public override NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata Retrieve(Guid id)
        {
            return XrmDataContext.new_personalinjurycasedatas.FirstOrDefault(x => x.new_personalinjurycasedataid == id);
        }
    }
}
