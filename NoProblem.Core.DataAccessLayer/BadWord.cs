﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class BadWord : DalBase<DataModel.Xrm.new_badword>
    {
        #region Ctor
        public BadWord(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_badword");
        } 
        #endregion

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public override NoProblem.Core.DataModel.Xrm.new_badword Retrieve(Guid id)
        {
            DataModel.Xrm.new_badword badWord = XrmDataContext.new_badwords.FirstOrDefault(x => x.new_badwordid == id);
            return badWord;
        }

        public DML.Xrm.new_badword GetBadWordByName(string name)
        {
            DML.Xrm.new_badword badWord = XrmDataContext.new_badwords.FirstOrDefault(x => x.new_name == name);
            return badWord;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_badword> GetAllBadWords()
        {
            return CacheManager.Instance.Get<IEnumerable<NoProblem.Core.DataModel.Xrm.new_badword>>(cachePrefix, "AllBadWords", GetAllBadWordsMethod);
        }

        private IEnumerable<NoProblem.Core.DataModel.Xrm.new_badword> GetAllBadWordsMethod()
        {
            return XrmDataContext.new_badwords;
        }
    }
}
