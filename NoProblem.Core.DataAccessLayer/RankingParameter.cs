﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class RankingParameter : DalBase<DML.Xrm.new_rankingparameter>
    {
        #region Ctor
        public RankingParameter(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_rankingparameter");
        } 
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_rankingparameter Retrieve(Guid id)
        {
            DML.Xrm.new_rankingparameter param = XrmDataContext.new_rankingparameters.FirstOrDefault(x => x.new_rankingparameterid == id);
            return param;
        }

        public DML.Xrm.new_rankingparameter GetRankingParameterByCode(int code)
        {
            DML.Xrm.new_rankingparameter param = XrmDataContext.new_rankingparameters.FirstOrDefault(x => x.new_code == code);
            return param;
        }
    }
}
