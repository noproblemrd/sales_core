﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class CustomerTipAdveritserResponse : DalBase<DataModel.Xrm.new_customertipadvertiserresponse>
    {
        public CustomerTipAdveritserResponse(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_customertipadvertiserresponse");
        }

        public override DataModel.Xrm.new_customertipadvertiserresponse Retrieve(Guid id)
        {
            return XrmDataContext.new_customertipadvertiserresponses.FirstOrDefault(x => x.new_customertipadvertiserresponseid == id);
        }

        public override IQueryable<DataModel.Xrm.new_customertipadvertiserresponse> All
        {
            get
            {
                return XrmDataContext.new_customertipadvertiserresponses;
            }
        }
    }
}
