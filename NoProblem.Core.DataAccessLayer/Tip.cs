﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class Tip : DalBase<DataModel.Xrm.new_tip>
    {
        public Tip(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_tip");
        }

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public override NoProblem.Core.DataModel.Xrm.new_tip Retrieve(Guid id)
        {
            return XrmDataContext.new_tips.FirstOrDefault(x => x.new_tipid == id);
        }

        public List<NoProblem.Core.DataModel.Consumer.Containers.TipData> GetAll()
        {
            return CacheManager.Instance.Get<List<NoProblem.Core.DataModel.Consumer.Containers.TipData>>(cachePrefix, "GetAll", GetAllMethod);
        }

        private List<NoProblem.Core.DataModel.Consumer.Containers.TipData> GetAllMethod()
        {
            string query =
                @"select new_title, new_subtitle, new_primaryexpertiseid, new_order
                from new_tip with(nolock) where deletionstatecode = 0
                order by new_primaryexpertiseid, new_order";
            List<NoProblem.Core.DataModel.Consumer.Containers.TipData> dataList = new List<NoProblem.Core.DataModel.Consumer.Containers.TipData>();
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                DataModel.Consumer.Containers.TipData data = new NoProblem.Core.DataModel.Consumer.Containers.TipData();
                data.HeadingId = row["new_primaryexpertiseid"] != DBNull.Value ? (Guid)row["new_primaryexpertiseid"] : Guid.Empty;
                data.Title = Convert.ToString(row["new_title"]);
                data.Subtitle = Convert.ToString(row["new_subtitle"]);
                object order = row["new_order"];
                if (order != DBNull.Value)
                {
                    data.Order = (int)order;
                }
                dataList.Add(data);
            }
            return dataList;
        }
    }
}
