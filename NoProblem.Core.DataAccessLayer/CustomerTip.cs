﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class CustomerTip : DalBase<DataModel.Xrm.new_customertip>
    {
        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public CustomerTip(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_customertip");
        }

        public override DataModel.Xrm.new_customertip Retrieve(Guid id)
        {
            return XrmDataContext.new_customertips.FirstOrDefault(x => x.new_customertipid == id);
        }

        public override IQueryable<DataModel.Xrm.new_customertip> All
        {
            get
            {
                return XrmDataContext.new_customertips;
            }
        }

        public IEnumerable<DataModel.Consumer.Tips.TipBasicData> GetByCategoryId(Guid categoryId)
        {
            var all = GetAll();
            return all.Where(x => x.CategoryId == categoryId);
        }

        public List<DataModel.Consumer.Tips.TipBasicData> GetAll()
        {
            return CacheManager.Instance.Get<List<DataModel.Consumer.Tips.TipBasicData>>(cachePrefix, "All_Basic_Tip_Data", GetAllMethod, CacheManager.ONE_DAY);
        }

        private List<DataModel.Consumer.Tips.TipBasicData> GetAllMethod()
        {
            var query =
                from tip in All
                select new DataModel.Consumer.Tips.TipBasicData
                {
                    Answer = tip.new_answer,
                    Question = tip.new_question,
                    CategoryId = tip.new_primaryexpertiseid.Value,
                    TipId = tip.new_customertipid
                };
            return query.ToList();
        }
    }
}
