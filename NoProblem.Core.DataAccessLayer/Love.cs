﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class Love : DalBase<DataModel.Xrm.new_love>
    {
        public Love(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_love");
        }
        public override NoProblem.Core.DataModel.Xrm.new_love Retrieve(Guid id)
        {
            return XrmDataContext.new_loves.FirstOrDefault(x => x.new_loveid == id);
        }

    }
}
