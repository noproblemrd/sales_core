﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class SupplierPricingNote : DalBase<DataModel.Xrm.new_supplierpricingnote>
    {
        #region Ctor
        public SupplierPricingNote(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_supplierpricingnote");
        } 
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_supplierpricingnote Retrieve(Guid id)
        {
            return XrmDataContext.new_supplierpricingnotes.FirstOrDefault(x => x.new_supplierpricingnoteid == id);
        }

        public List<NoProblem.Core.DataModel.Notes.NoteData> GetNotesByEntryId(Guid regardingObjectId)
        {
            string query =
                @"select new_userid, new_useridname, new_description, createdon, new_file
                    from new_supplierpricingnote with(nolock)
                    where new_supplierpricingid = @id and deletionstatecode = 0
                    order by createdon";
            List<NoProblem.Core.DataModel.Notes.NoteData> notes = new List<NoProblem.Core.DataModel.Notes.NoteData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", regardingObjectId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            NoProblem.Core.DataModel.Notes.NoteData data = new NoProblem.Core.DataModel.Notes.NoteData();
                            data.RegargingObjectId = regardingObjectId;
                            data.CreatedOn = (DateTime)reader["createdon"];
                            data.Description = (string)reader["new_description"];
                            data.File = reader["new_file"] == DBNull.Value ? string.Empty : (string)reader["new_file"];
                            data.CreatedById = (Guid)reader["new_userid"];
                            data.CreatedByName = (string)reader["new_useridname"];
                            notes.Add(data);
                        }
                    }
                }
            }
            return notes;
        }
    }
}
