﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class CaseNote : DalBase<DataModel.Xrm.new_casenote>
    {
        public CaseNote(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_casenote");
        }

        public override NoProblem.Core.DataModel.Xrm.new_casenote Retrieve(Guid id)
        {
            return XrmDataContext.new_casenotes.FirstOrDefault(x => x.new_casenoteid == id);
        }

        public List<NoProblem.Core.DataModel.Notes.NoteData> GetNotesByCaseId(Guid caseId)
        {
            List<NoProblem.Core.DataModel.Notes.NoteData> notes = new List<NoProblem.Core.DataModel.Notes.NoteData>();
            string query =
                @"select new_userid, new_useridname, new_description, createdon, new_file
                    from new_casenote with(nolock)
                    where new_incidentid = @caseId and deletionstatecode = 0
                    order by createdon";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@caseId", caseId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            NoProblem.Core.DataModel.Notes.NoteData data = new NoProblem.Core.DataModel.Notes.NoteData();
                            data.RegargingObjectId = caseId;
                            data.CreatedOn = (DateTime)reader["createdon"];
                            data.Description = (string)reader["new_description"];
                            data.File = reader["new_file"] == DBNull.Value ? string.Empty : (string)reader["new_file"];
                            data.CreatedById = (Guid)reader["new_userid"];
                            data.CreatedByName = (string)reader["new_useridname"];
                            notes.Add(data);
                        }
                    }
                }
            }
            return notes;
        }
    }
}
