﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class ConsumerNote : DalBase<DML.Xrm.new_consumernote>
    {
        #region Ctor
        public ConsumerNote(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_consumernote");
        } 
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_consumernote Retrieve(Guid id)
        {
            DML.Xrm.new_consumernote note = XrmDataContext.new_consumernotes.FirstOrDefault(x => x.new_consumernoteid == id);
            return note;
        }

        public IEnumerable<DML.Xrm.new_consumernote> GetNotesByContactId(Guid contactId)
        {
            IEnumerable<DML.Xrm.new_consumernote> retVal = from note in XrmDataContext.new_consumernotes
                                                       where
                                                       note.new_contactid.HasValue
                                                       &&
                                                       note.new_contactid.Value == contactId
                                                       select
                                                       note;
            return retVal;
        }
    }
}
