﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class IvrFlavor : DalBase<DataModel.Xrm.new_ivrflavor>
    {
        #region Ctor
        public IvrFlavor(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_ivrflavor");
        }
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_ivrflavor Retrieve(Guid id)
        {
            return XrmDataContext.new_ivrflavors.FirstOrDefault(x => x.new_ivrflavorid == id);
        }

        public void GetIvrMenuDataForCall(Guid incidentAccountId, out string intro, out string menu, out bool isManVoice, out bool isAudioFiles, out string headingCode, out int leadsLeft, out string description)
        {
            intro = null;
            menu = null;
            isManVoice = false;
            isAudioFiles = false;
            headingCode = null;
            leadsLeft = 0;
            description = null;
            string query =
                @"select inc.description, new_intro, new_menu, new_usemanvoice, pe.new_ivrname, cast(cast((acc.new_cashbalance / ae.new_incidentprice) as float) as int) as leadsLeft, ae.new_accountexpertiseid, new_isaudiofiles, pe.new_code
                from new_incidentaccount ia with(nolock)
                join new_ivrflavor ivr with(nolock) on ia.new_ivrflavorid = ivr.new_ivrflavorid
				join incident inc with(nolock) on inc.incidentid = ia.new_incidentid
				join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = inc.new_primaryexpertiseid
                join account acc with(nolock) on acc.accountid = ia.new_accountid
                join new_accountexpertise ae with(nolock) on (ae.new_accountid = ia.new_accountid and ae.new_primaryexpertiseid = inc.new_primaryexpertiseid)
                where
                ia.deletionstatecode = 0 and ivr.deletionstatecode = 0 and acc.deletionstatecode = 0 and ae.deletionstatecode = 0
				and ae.statecode = 0
                and ia.new_incidentaccountid = @callId";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@callId", incidentAccountId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            isAudioFiles = reader["new_isaudiofiles"] != DBNull.Value ? (bool)reader["new_isaudiofiles"] : false;
                            intro = reader["new_intro"] != DBNull.Value ? (string)reader["new_intro"] : string.Empty;
                            menu = (string)reader["new_menu"];
                            leadsLeft = reader["leadsLeft"] != DBNull.Value ? (int)reader["leadsLeft"] : 0;
                            description = reader["description"] != DBNull.Value ? (string)reader["description"] : string.Empty;
                            if (!isAudioFiles)
                            {
                                string ivrName = reader["new_ivrname"] != DBNull.Value ? (string)reader["new_ivrname"] : string.Empty;
                                isManVoice = (bool)reader["new_usemanvoice"];
                                intro = string.Format(intro, ivrName, leadsLeft, description);
                                menu = string.Format(menu, leadsLeft, description);
                            }
                            else
                            {
                                headingCode = Convert.ToString(reader["new_code"]);
                            }
                        }
                    }
                }
            }
        }

        public void GetIvrMenuDataForCallByFlavorId(Guid selectedFlavorId, out string intro, out string menu, out bool isManVoice)
        {
            intro = null;
            menu = null;
            isManVoice = false;
            string query =
                @"select new_intro, new_menu, new_usemanvoice
                from new_ivrflavor ivr with(nolock)
                where
                ivr.deletionstatecode = 0
                and ivr.new_ivrflavorid = @flavorId";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@flavorId", selectedFlavorId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            intro = Convert.ToString(reader["new_intro"]);
                            menu = Convert.ToString(reader["new_menu"]);
                            isManVoice = reader["new_usemanvoice"] != DBNull.Value ? (bool)reader["new_usemanvoice"] : false;
                        }
                    }
                }
            }
        }

        public void GetIvrMenuDataForCallFromIncomingCall(Guid callId, out string intro, out string menu, out bool isManVoice)
        {
            intro = null;
            menu = null;
            isManVoice = false;
            string query =
                @"select new_intro, new_menu, new_usemanvoice
                from new_ivrflavor ivr with(nolock)
                join new_aarincomingcall aic with(nolock) on aic.new_ivrflavorid = ivr.new_ivrflavorid
                where
                ivr.deletionstatecode = 0 and aic.deletionstatecode = 0
                and aic.new_aarincomingcallid = @callId";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@callId", callId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            intro = (string)reader["new_intro"];
                            menu = (string)reader["new_menu"];
                            isManVoice = (bool)reader["new_usemanvoice"];
                        }
                    }
                }
            }
        }

        public bool GetIvrPressedDataForCall(Guid incidentAccountId, string digits, out string txt, out bool isManVoice, out bool isAudioFiles)
        {
            txt = null;
            isManVoice = false;
            isAudioFiles = false;
            string query;
            if (digits == "1" || digits == "2" || digits == "3" || digits == "4" || digits == "9")
            {
                query =
                    @"if(
                (select ty.new_{0}enabled
                from new_ivrtype ty
                where ty.deletionstatecode = 0
                and ty.new_ivrtypeid = (
                select top 1 fl.new_ivrtypeid from
                new_ivrflavor fl 
                join new_incidentaccount ia on fl.new_ivrflavorid = ia.new_ivrflavorid
                where new_incidentaccountid = @callId)) = 1
                )
                begin
                select
                fl.new_pressed{1}, fl.new_usemanvoice, 1, new_isaudiofiles
                from
                new_ivrflavor fl
                join new_incidentaccount ia on fl.new_ivrflavorid = ia.new_ivrflavorid
                where ia.new_incidentaccountid = @callId
                end
                else
                begin
                select 
                fl.new_presseddisablednumber, fl.new_usemanvoice, 0, new_isaudiofiles
                from
                new_ivrflavor fl
                join new_incidentaccount ia on fl.new_ivrflavorid = ia.new_ivrflavorid              
                where ia.new_incidentaccountid = @callId
                end";
            }
            else
            {
                query =
                @"select 
                fl.new_presseddisablednumber, fl.new_usemanvoice, 0, new_isaudiofiles
                from
                new_ivrflavor fl
                join new_incidentaccount ia on fl.new_ivrflavorid = ia.new_ivrflavorid
                where ia.new_incidentaccountid = @callId";
            }
            string queryReady = string.Empty;
            switch (digits)
            {
                case "1":
                    queryReady = string.Format(query, "one", "1");
                    break;
                case "2":
                    queryReady = string.Format(query, "two", "2");
                    break;
                case "3":
                    queryReady = string.Format(query, "three", "3");
                    break;
                case "4":
                    queryReady = string.Format(query, "four", "4");
                    break;
                case "9":
                    queryReady = string.Format(query, "nine", "9");
                    break;
                default:
                    queryReady = query;
                    break;
            }
            bool retVal = false;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryReady, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@callId", incidentAccountId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            txt = reader[0] != DBNull.Value ? (string)reader[0] : string.Empty;
                            isManVoice = (bool)reader[1];
                            int enabled = (int)reader[2];
                            retVal = enabled == 1;
                            isAudioFiles = reader[3] != DBNull.Value ? (bool)reader[3] : false;
                        }
                    }
                }
            }
            return retVal;
        }

        public bool GetIvrPressedDataForIncomingCall(Guid callId, string digits, out string txt, out bool isManVoice)
        {
            txt = null;
            isManVoice = false;
            string query;
            if (digits == "2" || digits == "3" || digits == "4" || digits == "9")
            {
                query =
                    @"if(
                (select ty.new_{0}enabled
                from new_ivrtype ty
                where ty.deletionstatecode = 0
                and ty.new_ivrtypeid = (
                select top 1 fl.new_ivrtypeid from
                new_ivrflavor fl 
                join new_aarincomingcall aic on fl.new_ivrflavorid = aic.new_ivrflavorid
                where aic.new_aarincomingcallid = @callId)) = 1
                )
                begin
                select
                fl.new_pressed{1}, fl.new_usemanvoice, 1
                from
                new_ivrflavor fl
                join new_aarincomingcall aic on fl.new_ivrflavorid = aic.new_ivrflavorid
                where aic.new_aarincomingcallid = @callId
                end
                else
                begin
                select 
                fl.new_presseddisablednumber, fl.new_usemanvoice, 0
                from
                new_ivrflavor fl
                join new_aarincomingcall aic on fl.new_ivrflavorid = aic.new_ivrflavorid              
                where aic.new_aarincomingcallid = @callId
                end";
            }
            else
            {
                query =
                @"select 
                fl.new_presseddisablednumber, fl.new_usemanvoice, 0
                from
                new_ivrflavor fl
                join new_aarincomingcall aic on fl.new_ivrflavorid = aic.new_ivrflavorid
                where aic.new_aarincomingcallid = @callId";
            }
            string queryReady = string.Empty;
            switch (digits)
            {
                case "2":
                    queryReady = string.Format(query, "two", "2");
                    break;
                case "3":
                    queryReady = string.Format(query, "three", "3");
                    break;
                case "4":
                    queryReady = string.Format(query, "four", "4");
                    break;
                case "9":
                    queryReady = string.Format(query, "nine", "9");
                    break;
                default:
                    queryReady = query;
                    break;
            }
            bool retVal = false;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryReady, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@callId", callId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            txt = reader[0] != DBNull.Value ? (string)reader[0] : string.Empty;
                            isManVoice = (bool)reader[1];
                            int enabled = (int)reader[2];
                            retVal = enabled == 1;
                        }
                    }
                }
            }
            return retVal;
        }

        public List<Guid> GetEnabledFlavorsByIvrType(NoProblem.Core.DataModel.Xrm.new_ivrtype.IvrTypeCode ivrType, bool hasDescription)
        {
            string query =
                @"select fl.new_ivrflavorid 
                from new_ivrflavor fl with(nolock)
                join new_ivrtype ty with(nolock) on fl.new_ivrtypeid = ty.new_ivrtypeid
                where fl.deletionstatecode = 0 and ty.deletionstatecode = 0
                and fl.new_enabled = 1
                and ty.new_ivrtypecode = @typeCode
                and (@hasDescription = 1 or (fl.new_musthavedescription = 0 or fl.new_musthavedescription is null))";
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@typeCode", (int)ivrType);
                    command.Parameters.AddWithValue("@hasDescription", hasDescription);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
