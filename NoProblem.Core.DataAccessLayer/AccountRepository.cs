﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.Account
//  File: Account.cs
//  Description: DAL for Account Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Effect.Crm.Convertor;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Xrm;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class AccountRepository : DalBase<account>
    {
        static readonly int MOBILE_FREELEADS;
        static AccountRepository()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            MOBILE_FREELEADS = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_FREE_LEADS));
        }
        #region Ctor
        public AccountRepository(DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("account");
        }
        #endregion
        //test 2
        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public override IQueryable<account> All
        {
            get
            {
                return XrmDataContext.accounts;
            }
        }

        public override account Retrieve(Guid id)
        {
            DataModel.Xrm.account account = XrmDataContext.accounts.FirstOrDefault(x => x.accountid == id);
            return account;
        }

        public override void Create(account entity)
        {
            entity.new_cashbalance = 0;
            base.Create(entity);
        }

        public int GetStageInRegistration(Guid SupplierId)
        {
            int retVal = 0;
            string getStageQuery = "SELECT new_stageinregistration FROM account with (nolock) WHERE accountid = '{0}' and deletionstatecode = 0";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(string.Format(getStageQuery, SupplierId), connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader getStage = command.ExecuteReader())
                    {
                        if (getStage.Read())
                        {
                            if (getStage["new_stageinregistration"] != null && getStage["new_stageinregistration"] != DBNull.Value)
                                retVal = (int)getStage["new_stageinregistration"];
                        }
                    }
                }
            }
            return retVal;
        }

        public IEnumerable<account> GetAllPublishers()
        {
            int inactiveCode = (int)DataModel.Xrm.account.SupplierStatus.Inactive;
            IEnumerable<account> retVal = from ac in XrmDataContext.accounts
                                          where
                                          ac.new_securitylevel.HasValue
                                          &&
                                          ac.new_securitylevel.Value > 0
                                          &&
                                          ac.new_status.HasValue
                                          &&
                                          ac.new_status.Value != inactiveCode
                                          select
                                          ac;
            retVal = retVal.Where(x => x.new_isnoproblemuser.HasValue == false || x.new_isnoproblemuser.Value == false);
            retVal = retVal.Where(x => x.new_affiliateoriginid.HasValue == false);
            return retVal;
        }

        public account GetAccountByPhoneAndEmail(string phoneNumber, string email)
        {
            account account = XrmDataContext.accounts.FirstOrDefault(x =>
                x.telephone1 == phoneNumber && x.emailaddress1 == email);
            return account;
        }

        public void SetSupplierRegistrationStage(Guid supplierId, DataModel.Xrm.account.StageInRegistration stage)
        {
            string query = "update account set new_stageinregistration = {0} where accountid = '{1}' and (new_stageinregistration < {0} or new_stageinregistration is null)";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = new SqlCommand(String.Format(query, (int)stage, supplierId), conn))
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public void SetSupplierTrialRegistrationStage(Guid supplierId, account.StageInTrialRegistration stageInTrialRegistration)
        {
            string query = "update account set new_stageintrialregistration = {0} where accountid = '{1}' and (new_stageintrialregistration < {0} or new_stageintrialregistration is null)";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = new SqlCommand(String.Format(query, (int)stageInTrialRegistration, supplierId), conn))
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                }
            }
        }

        public IEnumerable<account> GetSuppliersByStatus(account.SupplierStatus supplierStatus)
        {
            IEnumerable<account> retVal = from acc in XrmDataContext.accounts
                                          where
                                          acc.new_status.Value == (int)supplierStatus
                                          select acc;
            retVal = from acc in retVal
                     where
                      acc.new_securitylevel.HasValue == false
                      ||
                      acc.new_securitylevel.Value == 0
                     select acc;

            return retVal;
        }

        public account GetSupplierByGovernmentId(string id)
        {
            DataModel.Xrm.account supplier = XrmDataContext.accounts.FirstOrDefault(x => x.new_accountgovernmentid == id);
            return supplier;
        }

        public IEnumerable<account> GetAllSuppliers()
        {
            IEnumerable<account> retVal = from acc in XrmDataContext.accounts select acc;
            retVal = from acc
                     in retVal
                     where acc.new_securitylevel.HasValue == false
                     && acc.new_affiliateoriginid.HasValue == false
                     && acc.new_status != (int)DML.Xrm.account.SupplierStatus.Trial
                     select acc;
            return retVal;
        }

        public IEnumerable<account> GetSuppliersThatCompletedRegistration()
        {
            IEnumerable<account> retVal = GetAllSuppliers();
            int paidStage = (int)account.StageInRegistration.PAID;
            retVal = from acc in retVal
                     where
                     acc.new_stageinregistration.HasValue
                     &&
                     acc.new_stageinregistration.Value == paidStage
                     select acc;
            return retVal;
        }

        public account GetByBizId(string bizId)
        {
            account retVal = XrmDataContext.accounts.FirstOrDefault(x => x.new_bizid == bizId);
            return retVal;
        }

        public string GetBizId(string supplierId)
        {
            return CacheManager.Instance.Get<string>(cachePrefix + ".GetBizId", supplierId, GetBizIdMethod, 1440);
        }

        private string GetBizIdMethod(string supplierId)
        {
            string query = "select top 1 new_bizid from accountextensionbase with(nolock) where accountid = @supplierId";
            string retVal = string.Empty;
            SqlParametersList parameters = new DalBase<account>.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        public account GetSupplierByEmail(string email)
        {
            account retVal = XrmDataContext.accounts.FirstOrDefault(
                x => x.emailaddress1 == email
                && !x.new_securitylevel.HasValue
                && !x.new_affiliateoriginid.HasValue);
            return retVal;
        }

        public bool IsEmailDuplicate(string email, Guid supplierId)
        {
            StringBuilder query = new StringBuilder(
                @"select count(*)
					from
					account with (nolock) where
					emailaddress1 = @email and
					deletionstatecode = 0
					and new_securitylevel is null
					and new_affiliateoriginid is null ");
            if (supplierId != Guid.Empty)
            {
                query.Append(" and accountid != '");
                query.Append(supplierId.ToString());
                query.Append("'");
            }
            bool retVal = false;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query.ToString(), connection))
                {
                    command.Parameters.AddWithValue("@email", email);
                    command.Connection.Open();
                    int count = (int)command.ExecuteScalar();
                    if (count > 0)
                    {
                        retVal = true;
                    }
                }
            }
            return retVal;
        }

        public bool IsPhoneDuplicate(string phone, Guid supplierId)
        {
            StringBuilder query = new StringBuilder(
                @"select count(*)
					from
					account with (nolock) where
					telephone1 = '");
            query.Append(phone);
            query.Append(@"'
					and
					deletionstatecode = 0");
            if (supplierId != Guid.Empty)
            {
                query.Append(" and accountid != '");
                query.Append(supplierId.ToString());
                query.Append("'");
            }
            bool retVal = false;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query.ToString(), connection))
                {
                    command.Connection.Open();
                    int count = (int)command.ExecuteScalar();
                    if (count > 0)
                    {
                        retVal = true;
                    }
                }
            }
            return retVal;
        }

        public List<NoProblem.Core.DataModel.SqlHelper.AccountExpandedFields> GetAccountsExpandedFields(List<string> supplierIds, bool withRealPhone)
        {
            string query = @"select address1_country,address1_stateorprovince, name
							,address1_city,address1_county,address1_line1,
							address1_line2,address1_postalcode,accountid,
							(select count(new_islike) from new_review with(nolock) where new_accountid = accountid and deletionstatecode = 0 and new_islike = 1 and new_status = 1) as likes
							,(select count(new_islike) from new_review with(nolock) where new_accountid = accountid and deletionstatecode = 0 and new_islike = 0 and new_status = 1) as dislikes
							,telephone1
							from
							account with (nolock)
							where
							accountid in ({0}) and deletionstatecode = 0";
            StringBuilder ids = new StringBuilder();
            int count = supplierIds.Count;
            int last = count - 1;
            for (int i = 0; i < count; i++)
            {
                ids.Append("'");
                ids.Append(supplierIds[i]);
                ids.Append("'");
                if (i < last)
                {
                    ids.Append(",");
                }
            }
            List<DataModel.SqlHelper.AccountExpandedFields> retVal = new List<NoProblem.Core.DataModel.SqlHelper.AccountExpandedFields>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(string.Format(query, ids.ToString()), connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.SqlHelper.AccountExpandedFields data = new NoProblem.Core.DataModel.SqlHelper.AccountExpandedFields();
                            data.City = ConvertorUtils.ToString(reader["address1_city"], string.Empty);
                            data.Country = ConvertorUtils.ToString(reader["address1_country"], string.Empty);
                            data.Dislikes = (int)reader["dislikes"];
                            data.District = ConvertorUtils.ToString(reader["address1_county"], string.Empty);
                            data.Likes = (int)reader["likes"];
                            data.State = ConvertorUtils.ToString(reader["address1_stateorprovince"], string.Empty);
                            data.Street = ConvertorUtils.ToString(reader["address1_line1"], string.Empty);
                            data.StreetNumber = ConvertorUtils.ToString(reader["address1_line2"], string.Empty);
                            data.SupplierId = (Guid)reader["accountid"];
                            data.ZipCode = ConvertorUtils.ToString(reader["address1_postalcode"], string.Empty);
                            data.Crm3Url = reader["name"] != DBNull.Value ? (string)reader["name"] : string.Empty;
                            if (withRealPhone)
                            {
                                data.RealPhone = reader["telephone1"] != DBNull.Value ? (string)reader["telephone1"] : "";
                            }
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public DataModel.SupplierModel.MiniSiteSupplierData GetMiniSiteSupplierData(Guid supplierId, string originCode, string supplier)
        {
            DML.SupplierModel.MiniSiteSupplierData retVal = new NoProblem.Core.DataModel.SupplierModel.MiniSiteSupplierData();
            retVal.Regions = new List<string>();
            retVal.Expertises = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string query;
                if (supplierId == Guid.Empty)
                {
                    query =
                        @"select 
					accountid
					,name
					,new_sumassistancerequests
					,new_sumsurvey
					,new_videourl
					,new_description
					,description
					,numberofemployees
					,(select count(new_islike) from new_review with(nolock) where new_accountid = accountid and deletionstatecode = 0 and new_islike = 1 and new_status = 1) as likes
					,(select count(new_islike) from new_review with(nolock) where new_accountid = accountid and deletionstatecode = 0 and new_islike = 0 and new_status = 1) as dislikes                    
					from
					account with(nolock)
					where
					deletionstatecode = 0
					and
					name = @supplier";
                }
                else
                {
                    query =
                        @"select 
					accountid
					,name
					,new_sumassistancerequests
					,new_sumsurvey
					,new_videourl
					,new_description
					,description
					,numberofemployees
					,(select count(new_islike) from new_review with(nolock) where new_accountid = accountid and deletionstatecode = 0 and new_islike = 1 and new_status = 1) as likes
					,(select count(new_islike) from new_review with(nolock) where new_accountid = accountid and deletionstatecode = 0 and new_islike = 0 and new_status = 1) as dislikes
					from
					account with(nolock)
					where
					deletionstatecode = 0
					and
					accountid = @supplier";
                }
                Guid accountid = Guid.Empty;
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    if (supplierId == Guid.Empty)
                    {
                        command.Parameters.AddWithValue("@supplier", supplier);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@supplier", supplierId);
                    }
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            accountid = (Guid)reader["accountid"];
                            retVal.AccountId = accountid;
                            retVal.Name = reader["name"] != DBNull.Value ? (string)reader["name"] : string.Empty;
                            retVal.CallsNumber = reader["new_sumassistancerequests"] != DBNull.Value ? (int)reader["new_sumassistancerequests"] : 0;
                            retVal.EmployeeNumber = reader["numberofemployees"] != DBNull.Value ? (int)reader["numberofemployees"] : 0;
                            retVal.LongDescription = reader["description"] != DBNull.Value ? (string)reader["description"] : string.Empty;
                            retVal.ShortDescription = reader["new_description"] != DBNull.Value ? (string)reader["new_description"] : string.Empty;
                            retVal.SurveyNumber = reader["new_sumsurvey"] != DBNull.Value ? (int)reader["new_sumsurvey"] : 0;
                            retVal.Video = reader["new_videourl"] != DBNull.Value ? (string)reader["new_videourl"] : string.Empty;
                            retVal.Likes = (int)reader["likes"];
                            retVal.Dislikes = (int)reader["dislikes"];
                        }
                    }
                }
                if (accountid != Guid.Empty)
                {
                    string query2 =
                        @"select re.new_name
from
New_region re with(nolock)
join New_regionaccountexpertise ra with(nolock) on re.New_regionId = ra.new_regionid
where
re.DeletionStateCode = 0 and ra.DeletionStateCode = 0
and re.New_Level = 1
and ra.new_accountid = @id";
                    using (SqlCommand command = new SqlCommand(query2, conn))
                    {
                        command.Parameters.AddWithValue("@id", accountid);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                retVal.Regions.Add((string)reader[0]);
                            }
                        }
                    }
                    string query3 =
                        @"select pe.new_name, ae.new_certificate
from New_primaryexpertise pe with(nolock)
join new_accountexpertise ae with(nolock) on pe.New_primaryexpertiseId = ae.new_primaryexpertiseid
where
pe.DeletionStateCode = 0 and ae.DeletionStateCode = 0
and ae.statecode = 0
and ae.new_accountid = @id";
                    using (SqlCommand command = new SqlCommand(query3, conn))
                    {
                        command.Parameters.AddWithValue("@id", accountid);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                retVal.Expertises.Add((string)reader[0]);
                                retVal.Certificate = reader[1] != DBNull.Value ? (bool)reader[1] : false;
                            }
                        }
                    }
                    string query4;
                    if (supplierId == Guid.Empty)
                    {
                        query4 =
                            @"select new_number
from New_directnumber dn with(nolock)
join new_accountexpertise ae with(nolock) on dn.new_accountexpertiseid = ae.New_accountexpertiseId
where
dn.DeletionStateCode = 0 and ae.DeletionStateCode = 0
and ae.new_accountid = @id
and dn.new_originid = 'A543E7BB-C606-E011-BADF-001517D1792A'";
                    }
                    else
                    {
                        query4 =
                            @"select new_number
from New_directnumber dn with(nolock)
join new_accountexpertise ae with(nolock) on dn.new_accountexpertiseid = ae.New_accountexpertiseId
where
dn.DeletionStateCode = 0 and ae.DeletionStateCode = 0
and ae.new_accountid = @id
and dn.new_originid = (select top 1 new_originid from new_origin with(nolock) where deletionstatecode = 0 and new_code = @code)";
                    }
                    using (SqlCommand command = new SqlCommand(query4, conn))
                    {
                        command.Parameters.AddWithValue("@id", accountid);
                        if (supplierId != Guid.Empty)
                        {
                            command.Parameters.AddWithValue("@code", originCode);
                        }
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                retVal.DirectNumber = (string)reader[0];
                                if (string.IsNullOrEmpty(retVal.DirectNumber) == false)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    string queryDn = @"declare @UtcNow datetime
			   set @UtcNow = GETUTCDATE()
			   declare @new_unavailableto datetime
			   declare @new_unavailablefrom datetime
			   declare @day int
			   declare @number varchar(20)
			   set @day = datepart (dw, @UtcNow)
			   set @number = '{0}'
			   SELECT telephone1,accountid,new_presentationphone,new_recordcalls,
			   CASE 
			   WHEN (account.new_unavailablefrom IS NULL AND account.new_unavailableto IS NULL) THEN 1
			   WHEN (@UtcNow > account.new_unavailableto) THEN 1
			   WHEN (@UtcNow < account.new_unavailablefrom) THEN 1
			   ELSE 0
			   END AS IsAvailable,
			   CASE 
				   WHEN new_availabilityid IS NULL THEN 0
				   ELSE 1
			   END AS IsWorkHours,
			   CASE
			   WHEN (new_accountexpertise.new_incidentprice <= account.new_availablecashbalance) THEN 1
			   ELSE 0
			   END AS HasAvailableMoney,
				CASE
				WHEN new_status = 1 THEN 1 ELSE 0
				END AS IsStatusApproved
			   FROM account with(nolock) 
			   INNER JOIN new_accountexpertise with(nolock) ON new_accountexpertise.new_accountid = account.accountid AND new_accountexpertise.deletionstatecode = 0 AND new_accountexpertise.statecode = 0
			   INNER JOIN new_directnumber with(nolock) ON new_directnumber.new_accountexpertiseid = new_accountexpertise.new_accountexpertiseid AND new_directnumber.deletionstatecode = 0
			   LEFT JOIN new_availability with (nolock) ON new_availability.new_accountid = account.accountid AND new_availability.deletionstatecode = 0 AND
				   new_availability.new_day = @day
				   AND convert(char(8), @UtcNow, 108) BETWEEN convert(char(8), new_availability.new_from, 108) AND convert(char(8), new_availability.new_to, 108)
			   WHERE new_mappingnumber = @number";
                    DML.DNQueryResult response = new DML.DNQueryResult();
                    response.IsAvailable = false;
                    if (string.IsNullOrEmpty(retVal.DirectNumber) == false)
                    {
                        using (SqlCommand command = new SqlCommand(string.Format(queryDn, retVal.DirectNumber), conn))
                        {
                            using (SqlDataReader getPhoneDetails = command.ExecuteReader())
                            {
                                if (getPhoneDetails.Read())
                                {
                                    if (ConvertorUtils.ToBoolean(getPhoneDetails["IsAvailable"]) &&
                                        ConvertorUtils.ToBoolean(getPhoneDetails["IsWorkHours"]) &&
                                        ConvertorUtils.ToBoolean(getPhoneDetails["HasAvailableMoney"]) &&
                                        ConvertorUtils.ToBoolean(getPhoneDetails["IsStatusApproved"]))
                                    {
                                        response.IsAvailable = true;
                                    }
                                }
                            }
                        }
                    }
                    if (!response.IsAvailable)
                    {
                        retVal.DirectNumber = string.Empty;
                    }
                }
            }
            return retVal;
        }

        public NoProblem.Core.DataModel.SupplierModel.MiniSiteData GetMiniSiteData(Guid supplierId)
        {
            string query = @"select address1_stateorprovince,name,description,new_videourl
							,address1_city,address1_line1,new_sumassistancerequests,
							address1_line2,address1_postalcode,numberofemployees,
							(select count(new_islike) from new_review with(nolock) where new_accountid = accountid and deletionstatecode = 0 and new_islike = 1) as likes
							,(select count(new_islike) from new_review with(nolock) where new_accountid = accountid and deletionstatecode = 0 and new_islike = 0) as dislikes
							from
							account with (nolock)
							where
							accountid = @supplierId and deletionstatecode = 0";
            DataModel.SupplierModel.MiniSiteData data = new NoProblem.Core.DataModel.SupplierModel.MiniSiteData();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@supplierId", supplierId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            data.CityName = ConvertorUtils.ToString(reader["address1_city"], string.Empty);
                            data.Dislikes = (int)reader["dislikes"];
                            data.Likes = (int)reader["likes"];
                            data.StateName = ConvertorUtils.ToString(reader["address1_stateorprovince"], string.Empty);
                            data.StreetName = ConvertorUtils.ToString(reader["address1_line1"], string.Empty);
                            data.HouseNum = ConvertorUtils.ToString(reader["address1_line2"], string.Empty);
                            data.Zipcode = ConvertorUtils.ToString(reader["address1_postalcode"], string.Empty);
                            data.LongDescription = ConvertorUtils.ToString(reader["description"], string.Empty);
                            data.Name = ConvertorUtils.ToString(reader["name"], string.Empty);
                            data.NumberOfEmployees = ConvertorUtils.ToInt(reader["numberofemployees"]);
                            data.SumAssistanceRequests = ConvertorUtils.ToInt(reader["new_sumassistancerequests"]);
                            data.VideoUrl = ConvertorUtils.ToString(reader["new_videourl"], string.Empty);
                        }
                    }
                }
            }
            return data;
        }

        public string GetEmail(Guid id)
        {
            var query = from acc in XrmDataContext.accounts
                        where acc.accountid == id
                        select acc.emailaddress1;
            return query.FirstOrDefault();
        }

        /// <summary>
        /// Getting all the account expertise as a dictionary where the primary expertise id is the key.
        /// </summary>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public Dictionary<string, ExpertiseContainer> GetAccountExpertise(Guid accountId)
        {
            Dictionary<string, ExpertiseContainer> accountExpertise = new Dictionary<string, ExpertiseContainer>();
            string getExpertiseQuery = @"SELECT new_accountexpertise.new_accountexpertiseid,new_accountexpertise.new_primaryexpertiseid,new_secondaryexpertiseid,new_accountexpertise.statecode,new_incidentprice
FROM new_accountexpertise with (nolock)
LEFT JOIN new_secondaryexpertise_new_accountexpertise seRelation with (nolock) ON seRelation.new_accountexpertiseid = new_accountexpertise.new_accountexpertiseid
WHERE new_accountid = '{0}' AND new_accountexpertise.deletionstatecode = 0 AND new_accountexpertise.new_primaryexpertiseid is not null";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(string.Format(getExpertiseQuery, accountId), connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader expertiseReader = command.ExecuteReader())
                    {
                        while (expertiseReader.Read())
                        {
                            string accountExpertiseId = expertiseReader["new_accountexpertiseid"].ToString();
                            string primaryExpertiseId = expertiseReader["new_primaryexpertiseid"].ToString().ToLower();
                            string secondaryExpertiseId = expertiseReader["new_secondaryexpertiseid"].ToString();
                            object statecode = expertiseReader["statecode"];
                            ExpertiseContainer tempExpertise = null;

                            if (accountExpertise.ContainsKey(primaryExpertiseId))
                            {
                                tempExpertise = accountExpertise[primaryExpertiseId] as ExpertiseContainer;
                            }
                            else
                            {
                                tempExpertise = new ExpertiseContainer();
                                tempExpertise.IncidentPrice = expertiseReader["new_incidentprice"] == DBNull.Value ? 0 : (decimal)expertiseReader["new_incidentprice"];
                                tempExpertise.IsActive = statecode == null || statecode.ToString() != "0" ? false : true;
                                tempExpertise.AllreadyHandled = false;
                                tempExpertise.AccountExpertiseId = new Guid(accountExpertiseId);
                                tempExpertise.PrimaryExpertiseId = new Guid(primaryExpertiseId);
                                if (tempExpertise.SecondaryExpertiseIds == null)
                                    tempExpertise.SecondaryExpertiseIds = new List<Guid>();
                            }
                            if (tempExpertise != null)
                            {
                                if (secondaryExpertiseId != null && secondaryExpertiseId != "")
                                    tempExpertise.SecondaryExpertiseIds.Add(new Guid(secondaryExpertiseId));

                                if (!accountExpertise.ContainsKey(primaryExpertiseId))
                                {
                                    accountExpertise.Add(primaryExpertiseId, tempExpertise);
                                }
                            }
                        }
                    }
                }
            }
            return accountExpertise;
        }

        public bool IsSupplierExists(string email)
        {
            string query = "SELECT 1 FROM account with (nolock) WHERE emailaddress1 = @email and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@email", email));
            object isExists = ExecuteScalar(query, parameters);
            bool retVal = false;
            if (isExists != null)
            {
                retVal = true;
            }
            return retVal;
        }

        /// <summary>
        /// Is unavailability reason = null or unavailableTo before today.
        /// Use only if you know the supplier is approved, else the answer has no value.
        /// </summary>
        /// <param name="supplierId"></param>
        public bool IsAvailable(Guid supplierId, out Guid unavailabilityReasonId)
        {
            unavailabilityReasonId = Guid.Empty;
            string query =
            @"select New_unavailabilityreasonId,
				case when 
				(New_unavailabilityreasonId is null or
				(New_unavailableto is not null and New_unavailableto < GETUTCDATE())
				or ((new_unavailablefrom is not null and New_unavailablefrom > GETUTCDATE())))
				then 1 else 0 end as isAvailable
				from account with(nolock)
				where accountid = @id";
            bool isAvailable = false;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", supplierId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            isAvailable = (int)reader[1] == 1;
                            if (!isAvailable)
                            {
                                unavailabilityReasonId = (Guid)reader[0];
                            }
                        }
                    }
                }
            }
            return isAvailable;
        }

        public Dictionary<DML.SupplierModel.SupplierSearchRow, DML.Xrm.account.SupplierStatus> SearchSuppliers(string searchValue)
        {
            return CacheManager.Instance.Get<Dictionary<DML.SupplierModel.SupplierSearchRow, DML.Xrm.account.SupplierStatus>>(
                cachePrefix + ".SearchSuppliers",
                searchValue,
                SearchSuppliersMethod,
                1
                );
        }

        private Dictionary<DML.SupplierModel.SupplierSearchRow, DML.Xrm.account.SupplierStatus> SearchSuppliersMethod(string searchValue)
        {
            string query =
                @"SELECT accountid, name, new_firstname, new_lastname, telephone1, emailaddress1, accountnumber, new_cashbalance
				,address1_city, address1_line1, address1_line2, address1_postalcode, telephone2, fax, websiteurl, numberofemployees, new_status, new_dapazstatus
	from dbo.Account with(nolock)
	where deletionstatecode = 0
				and new_securitylevel is null
				and
				name like @sv
union  
	  SELECT accountid, name, new_firstname, new_lastname, telephone1, emailaddress1, accountnumber, new_cashbalance
				,address1_city, address1_line1, address1_line2, address1_postalcode, telephone2, fax, websiteurl, numberofemployees, new_status, new_dapazstatus
	from dbo.Account with(nolock)
	where deletionstatecode = 0
				and new_securitylevel is null
				and
				new_firstname like @sv

union 
	  SELECT accountid, name, new_firstname, new_lastname, telephone1, emailaddress1, accountnumber, new_cashbalance
				,address1_city, address1_line1, address1_line2, address1_postalcode, telephone2, fax, websiteurl, numberofemployees, new_status, new_dapazstatus
	from dbo.Account with(nolock)
	where deletionstatecode = 0
				and new_securitylevel is null
				and new_lastname like @sv
				
union 
	  SELECT accountid, name, new_firstname, new_lastname, telephone1, emailaddress1, accountnumber, new_cashbalance
				,address1_city, address1_line1, address1_line2, address1_postalcode, telephone2, fax, websiteurl, numberofemployees, new_status, new_dapazstatus
	from dbo.Account with(nolock)
	where deletionstatecode = 0
				and new_securitylevel is null
				and
				telephone1 like @sv
union 
	  SELECT accountid, name, new_firstname, new_lastname, telephone1, emailaddress1, accountnumber, new_cashbalance
				,address1_city, address1_line1, address1_line2, address1_postalcode, telephone2, fax, websiteurl, numberofemployees, new_status, new_dapazstatus
	from dbo.Account with(nolock)
	where deletionstatecode = 0
				and new_securitylevel is null
				and emailaddress1 like @sv
union 
	  SELECT accountid, name, new_firstname, new_lastname, telephone1, emailaddress1, accountnumber, new_cashbalance
				,address1_city, address1_line1, address1_line2, address1_postalcode, telephone2, fax, websiteurl, numberofemployees, new_status, new_dapazstatus
	from dbo.Account with(nolock)
	where deletionstatecode = 0
				and new_securitylevel is null
				and accountnumber like @sv
union 
	  SELECT accountid, name, new_firstname, new_lastname, telephone1, emailaddress1, accountnumber, new_cashbalance
				,address1_city, address1_line1, address1_line2, address1_postalcode, telephone2, fax, websiteurl, numberofemployees, new_status, new_dapazstatus
	from dbo.Account with(nolock)
	where deletionstatecode = 0
				and new_securitylevel is null
				and new_bizid like @sv
union 
	  SELECT acc.accountid, acc.name, acc.new_firstname, acc.new_lastname, acc.telephone1, acc.emailaddress1, acc.accountnumber, acc.new_cashbalance
				,acc.address1_city, acc.address1_line1, acc.address1_line2, acc.address1_postalcode, acc.telephone2, acc.fax, acc.websiteurl, acc.numberofemployees, acc.new_status, acc.new_dapazstatus
	from new_directnumber dn with(nolock)
	join dbo.new_accountexpertise ae with(nolock) on ae.new_accountexpertiseid = dn.new_accountexpertiseid
	join dbo.account acc with(nolock) on acc.accountid = ae.new_accountid
	where acc.deletionstatecode = 0
				and acc.new_securitylevel is null
				and dn.new_number like @sv
  order by name";
            Dictionary<DML.SupplierModel.SupplierSearchRow, DML.Xrm.account.SupplierStatus> dataList =
                new Dictionary<DML.SupplierModel.SupplierSearchRow, DML.Xrm.account.SupplierStatus>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.CommandTimeout = 90;
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@sv", "%" + searchValue + "%");
                    //command.Parameters.AddWithValue("@PageNumber", request.PageNumber);
                    //command.Parameters.AddWithValue("@PageSize", request.PageSize);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.SupplierModel.SupplierSearchRow row = new NoProblem.Core.DataModel.SupplierModel.SupplierSearchRow();
                            row.SupplierId = (Guid)reader["accountid"];
                            row.Name = reader["name"] != DBNull.Value ? (string)reader["name"] : string.Empty;
                            row.FirstName = reader["new_firstname"] != DBNull.Value ? (string)reader["new_firstname"] : string.Empty;
                            row.LastName = reader["new_lastname"] != DBNull.Value ? (string)reader["new_lastname"] : string.Empty;
                            row.Telephone1 = reader["telephone1"] != DBNull.Value ? (string)reader["telephone1"] : string.Empty;
                            row.Email = reader["emailaddress1"] != DBNull.Value ? (string)reader["emailaddress1"] : string.Empty;
                            row.Number = reader["accountnumber"] != DBNull.Value ? (string)reader["accountnumber"] : string.Empty;
                            row.CashBalance = reader["new_cashbalance"] != DBNull.Value ? (decimal)reader["new_cashbalance"] : 0;
                            row.City = reader["address1_city"] != DBNull.Value ? (string)reader["address1_city"] : string.Empty;
                            row.Street = reader["address1_line1"] != DBNull.Value ? (string)reader["address1_line1"] : string.Empty;
                            row.HouseNumber = reader["address1_line2"] != DBNull.Value ? (string)reader["address1_line2"] : string.Empty;
                            row.HouseNumber = reader["address1_postalcode"] != DBNull.Value ? (string)reader["address1_postalcode"] : string.Empty;
                            row.Telephone2 = reader["telephone2"] != DBNull.Value ? (string)reader["telephone2"] : string.Empty;
                            row.Fax = reader["fax"] != DBNull.Value ? (string)reader["fax"] : string.Empty;
                            row.WebSite = reader["websiteurl"] != DBNull.Value ? (string)reader["websiteurl"] : string.Empty;
                            row.NumberOfEmployees = reader["numberofemployees"] != DBNull.Value ? (int)reader["numberofemployees"] : 0;
                            if (reader["new_dapazstatus"] != DBNull.Value)
                            {
                                row.IsGoldenNumberAdvertiser = (int)reader["new_dapazstatus"] == 2;
                                row.DapazStatus = (DataModel.Xrm.account.DapazStatus)((int)reader["new_dapazstatus"]);
                            }
                            else
                            {
                                row.DapazStatus = account.DapazStatus.PPA;
                                row.IsGoldenNumberAdvertiser = false;
                            }
                            dataList.Add(row, (DML.Xrm.account.SupplierStatus)reader["new_status"]);
                            //if (totalRows == 0)
                            //{
                            //    totalRows = (int)reader["totalRows"];
                            //}
                        }
                    }
                }
            }
            return dataList;
        }

        public List<NoProblem.Core.DataModel.SqlHelper.RankingNotificationSupplier> GetSuppliersForRankingNotification(Guid supplierId)
        {
            List<NoProblem.Core.DataModel.SqlHelper.RankingNotificationSupplier> retVal
                = new List<NoProblem.Core.DataModel.SqlHelper.RankingNotificationSupplier>();
            string query =
                @"select
				ae.new_incidentprice, ae.new_accountid, ae.new_primaryexpertiseid, acc.new_rankingnotification
				from new_accountexpertise ae with(nolock)
				join account acc with(nolock) on acc.accountid = ae.new_accountid
				where
				ae.statecode = 0 and ae.deletionstatecode = 0 and acc.deletionstatecode = 0 and acc.new_status = 1
				and ae.new_incidentprice is not null and ae.new_accountid is not null
				and acc.new_rankingnotification > 0 and ae.new_accountid != @supplierId
				and ae.new_primaryexpertiseid
				in(
				select new_primaryexpertiseid
				from new_accountexpertise with(nolock)
				where statecode = 0
				and deletionstatecode = 0
				and new_accountid = @supplierId)";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@supplierId", supplierId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.SqlHelper.RankingNotificationSupplier data = new NoProblem.Core.DataModel.SqlHelper.RankingNotificationSupplier();
                            data.ExpertiseId = (Guid)reader["new_primaryexpertiseid"];
                            data.IncidentPrice = (decimal)reader["new_incidentprice"];
                            data.RankingNotification = (int)reader["new_rankingnotification"];
                            data.SupplierId = (Guid)reader["new_accountid"];
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public account.SupplierStatus GetSupplierDataBaseStatus(Guid supplierId)
        {
            string query =
                @"select new_status from account with(nolock) where accountid = @id and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", supplierId));
            account.SupplierStatus retVal = account.SupplierStatus.Inactive;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (account.SupplierStatus)scalar;
            }
            return retVal;
        }

        public List<NoProblem.Core.DataModel.PublisherPortal.AffiliateUserData> GetAllAffiliateUsers()
        {
            string query =
                @"select accountid, accountnumber, name, emailaddress1, telephone1, new_password, new_firstname, new_affiliateoriginid, new_affiliateoriginidname
				from account with(nolock)
				where deletionstatecode = 0 and new_affiliateoriginid is not null
				and new_status = 1";
            List<NoProblem.Core.DataModel.PublisherPortal.AffiliateUserData> retVal =
                new List<NoProblem.Core.DataModel.PublisherPortal.AffiliateUserData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.PublisherPortal.AffiliateUserData data = new NoProblem.Core.DataModel.PublisherPortal.AffiliateUserData();
                            data.AccountId = (Guid)reader["accountid"];
                            data.AccountNumber = reader["accountnumber"] != DBNull.Value ? (string)reader["accountnumber"] : string.Empty;
                            data.Email = reader["emailaddress1"] != DBNull.Value ? (string)reader["emailaddress1"] : string.Empty;
                            data.ContactName = reader["new_firstname"] != DBNull.Value ? (string)reader["new_firstname"] : string.Empty;
                            data.Name = reader["name"] != DBNull.Value ? (string)reader["name"] : string.Empty;
                            data.OriginId = (Guid)reader["new_affiliateoriginid"];
                            data.OriginName = (string)reader["new_affiliateoriginidname"];
                            data.Password = reader["new_password"] != DBNull.Value ? (string)reader["new_password"] : string.Empty;
                            data.PhoneNumber = reader["telephone1"] != DBNull.Value ? (string)reader["telephone1"] : string.Empty;
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public bool IsCanHearAllRecordings(Guid userId)
        {
            string query =
                @"select 
				New_CanHearAllRecordings
				from 
				Account with(nolock)
				where deletionstatecode = 0 and accountid = @id";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", userId));
            object scalar = ExecuteScalar(query, parameters);
            bool retVal = false;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (bool)scalar;
            }
            return retVal;
        }

        public bool GetRechargeData(Guid supplierId, out int rechargeAmount, out account.RechargeTypeCode rechargeType, out int dayOfMonth, out int rechargeBonusPercent, out int numberOfPayments, out string userIP, out string userAgent, out string userAcceptLanguage, out string userRemoteHost, out decimal cashBalance, out string subscriptionPlan)
        {

            bool retVal = false;
            rechargeAmount = 0;
            rechargeType = account.RechargeTypeCode.BalanceLow;
            dayOfMonth = 0;
            rechargeBonusPercent = 0;
            numberOfPayments = 1;
            userIP = string.Empty;
            userAcceptLanguage = string.Empty;
            userRemoteHost = string.Empty;
            userAgent = string.Empty;
            cashBalance = 0;
            subscriptionPlan = null;
            string query =
                @"select 
				new_rechargeenabled
				,new_rechargeamount
				,new_rechargetypecode
				,new_rechargedayofmonth
				,new_rechargebonuspercent
				,new_numberofpayments
				,new_userip
				,new_useragent
				,new_userremotehost
				,new_useracceptlanguage
				,new_cashbalance
                ,new_subscriptionplan
				from account with(nolock)
				where deletionstatecode = 0
				and accountid = @accountId";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@accountId", supplierId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            retVal = reader["new_rechargeenabled"] != DBNull.Value ? (bool)reader["new_rechargeenabled"] : false;

                            rechargeType = reader["new_rechargetypecode"] != DBNull.Value ? (account.RechargeTypeCode)((int)reader["new_rechargetypecode"]) : account.RechargeTypeCode.BalanceLow;
                            rechargeAmount = reader["new_rechargeamount"] != DBNull.Value ? (int)reader["new_rechargeamount"] : 0;
                            dayOfMonth = reader["new_rechargedayofmonth"] != DBNull.Value ? (int)reader["new_rechargedayofmonth"] : 1;
                            rechargeBonusPercent = reader["new_rechargebonuspercent"] != DBNull.Value ? (int)reader["new_rechargebonuspercent"] : 0;
                            numberOfPayments = (reader["new_numberofpayments"] != DBNull.Value && (int)reader["new_numberofpayments"] > 0) ? (int)reader["new_numberofpayments"] : 1;
                            userIP = reader["new_userip"] != DBNull.Value ? (string)reader["new_userip"] : string.Empty;
                            userAgent = reader["new_useragent"] != DBNull.Value ? (string)reader["new_useragent"] : string.Empty;
                            userAcceptLanguage = reader["new_useracceptlanguage"] != DBNull.Value ? (string)reader["new_useracceptlanguage"] : string.Empty;
                            userRemoteHost = reader["new_userremotehost"] != DBNull.Value ? (string)reader["new_userremotehost"] : string.Empty;
                            cashBalance = reader["new_cashbalance"] != DBNull.Value ? (decimal)reader["new_cashbalance"] : 0;
                            subscriptionPlan = Convert.ToString(reader["new_subscriptionplan"]);

                        }
                    }
                }
            }
            return retVal;
        }

        public List<Guid> FindSuppliersForMonthlyCharge(int dayOfMonth, DateTime todayUtcStartOfDay)
        {
            string query =
                @"
select accountid
from account acc with(nolock)
where deletionstatecode = 0
and new_rechargeenabled = 1
and (
(new_rechargetypecode = 1 and new_rechargedayofmonth = @dayOfMonth)--monthly
or (1 = @dayOfMonth) --maxMonthly and for ppa fix monthly payment
)
and new_rechargeamount > 0
and ( 
		(	
		new_status = 1
		and isnull(new_dapazstatus, 1) = 1--PPA
		)
		or
		(
		new_status = 2
		and isnull(new_dapazstatus, 1) = 7--PPA HOLD
		)
	)
and new_isinactivewithmoney = 0
				";

            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@dayOfMonth", dayOfMonth);
                    command.Parameters.AddWithValue("@todayStartOfDay", todayUtcStartOfDay);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public HashSet<Guid> FindSuppliersForMonthlyCharge(List<int> daysOfMonth, DateTime todayUtcStartOfDay)
        {
            HashSet<Guid> idSet = new HashSet<Guid>();
            foreach (var day in daysOfMonth)
            {
                List<Guid> l = FindSuppliersForMonthlyCharge(day, todayUtcStartOfDay);
                foreach (var id in l)
                {
                    idSet.Add(id);
                }
            }
            return idSet;
        }

        public IEnumerable<Guid> GetSupplierPopulation(string populationSql, string dateTimeField, DateTime fromDate, DateTime toDate)
        {
            StringBuilder queryBuilder = new StringBuilder(
                @"select accountid from account with(nolock)
				where deletionstatecode = 0 and ");
            queryBuilder.Append(populationSql);
            queryBuilder.Append(" and ");
            queryBuilder.Append(dateTimeField);
            queryBuilder.Append(" between @from and @to");
            string query = queryBuilder.ToString();
            List<Guid> retVal = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@from", fromDate);
                    command.Parameters.AddWithValue("@to", toDate);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetAutoUpsaleUserId()
        {
            string query =
                @"select accountid from account with(nolock)
				where deletionstatecode = 0 and 
				new_isautoupsaleuser = 1";
            object scalar = ExecuteScalar(query);
            Guid retVal = Guid.Empty;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            else
            {
                throw new Exception("No auto upsale user defined in the system.");
            }
            return retVal;
        }

        public string GetAccountManagerEmail(Guid supplierId)
        {
            string query =
                @"select mana.emailaddress1
				from account acc with(nolock)
				join account mana with(nolock) on acc.new_managerid = mana.accountid
				where acc.deletionstatecode = 0
				and mana.deletionstatecode = 0
				and acc.accountid = @supplierId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            object scalar = ExecuteScalar(query, parameters);
            string retVal = string.Empty;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        public bool IsAutoDirectNumbersDisabled(Guid supplierId)
        {
            string query =
                @"select new_disableautodirectnumbers from account with(nolock)
					where accountid = @supplierId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            object scalar = ExecuteScalar(query, parameters);
            bool retVal = false;
            if (scalar != null
                && scalar != DBNull.Value)
            {
                retVal = (bool)scalar;
            }
            return retVal;
        }

        public int GetSecurityLevel(Guid userId)
        {
            string query =
                @"select new_securitylevel from account with(nolock)
				where accountid = @userId and
				deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@userId", userId));
            object scalar = ExecuteScalar(query, parameters);
            if (scalar == null)
            {
                throw new Exception(string.Format("account with userId {0} does not exists", userId));
            }
            int retVal = 0;
            if (scalar != DBNull.Value)
            {
                retVal = (int)scalar;
            }
            return retVal;
        }

        public bool IsOverDailyBugdet(Guid supplierId, DateTime utcStartDate, bool isTrialSupplier)
        {
            string query =
                @"select 
cast(case when count(*) >= 
(select new_dailybudget from account with(nolock) where accountid = @supplierId) 
then 1 else 0 end as bit)
from
new_incidentaccount with(nolock)
where
deletionstatecode = 0
and createdon >= @startDate
and new_accountid = @supplierId ";
            if (!isTrialSupplier)
            {
                query += " and (statuscode = @closed or statuscode = @closedDirect or statuscode = @invited)";
            }
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            parameters.Add(new KeyValuePair<string, object>("@startDate", utcStartDate));
            parameters.Add(new KeyValuePair<string, object>("@closed", (int)DataModel.Xrm.new_incidentaccount.Status.CLOSE));
            parameters.Add(new KeyValuePair<string, object>("@closedDirect", (int)DataModel.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS));
            parameters.Add(new KeyValuePair<string, object>("@invited", (int)DataModel.Xrm.new_incidentaccount.Status.INVITED_BY_SMS));
            bool isOver = (bool)ExecuteScalar(query, parameters);
            return isOver;
        }

        public double GetRefundRequestPercent(Guid supplierId, bool addOne)
        {
            string query = @"
			select case when
			(select cast(count(*) AS REAL)
			from new_incidentaccount with(nolock)
			where (statuscode = 10 or statuscode = 12 or statuscode = 13)
			and deletionstatecode  = 0
			and new_accountid = @supplierId)--connected
			= 0 
			then 0
			else(	
			(select cast(count(*) AS REAL) ";
            if (addOne)
            {
                query += " + 1 ";
            }
            query +=
            @" from new_incidentaccount with(nolock)
			where (statuscode = 10 or statuscode = 12 or statuscode = 13)
			and new_refundstatus is not null
			and deletionstatecode  = 0
			and new_accountid = @supplierId) --refund requested
			/ 
			(select cast(count(*) AS REAL)
			from new_incidentaccount with(nolock)
			where (statuscode = 10 or statuscode = 12 or statuscode = 13)
			and deletionstatecode  = 0
			and new_accountid = @supplierId) --connected
			* 100)
			end";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            object scalar = ExecuteScalar(query, parameters);
            float floatScalar = (float)scalar;
            decimal d = new decimal(floatScalar);
            return decimal.ToDouble(d);
        }

        public Dictionary<Guid, string> GetTrialSuppliersPhonesByTrialStatusReason(DML.Xrm.account.TrialStatusReasonCode trialStatusReason, int top)
        {
            Dictionary<Guid, string> retVal = new Dictionary<Guid, string>();

            string nyQuery =
                @"select distinct ";
            if (top > 0)
            {
                nyQuery += "top " + top;
            }
            nyQuery += @" acc.accountid, acc.telephone1 
				from account acc with(nolock)
				join New_regionaccountexpertise ra on ra.new_accountid = acc.AccountId
				where acc.deletionstatecode = 0 and ra.DeletionStateCode = 0
				and acc.new_status = 4
				and acc.new_trialstatusreason = 1
				and ra.new_regionid = 'C02675C1-BF20-E111-BD7E-001517D10F6E'";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(nyQuery, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@trial", (int)DML.Xrm.account.SupplierStatus.Trial);
                    command.Parameters.AddWithValue("@reason", (int)trialStatusReason);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Guid id = (Guid)reader["accountid"];
                            string phone = reader["telephone1"] != DBNull.Value ? (string)reader["telephone1"] : string.Empty;
                            if (phone != string.Empty)
                            {
                                retVal.Add(id, phone);
                            }
                        }
                    }
                }
            }
            if (retVal.Count() > 0)
            {
                return retVal;
            }

            string query =
                @"select ";
            if (top > 0)
            {
                query += "top " + top;
            }
            query += @" accountid, telephone1 
				from account with(nolock)
				where deletionstatecode = 0
				and new_status = @trial
				and new_trialstatusreason = @reason";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@trial", (int)DML.Xrm.account.SupplierStatus.Trial);
                    command.Parameters.AddWithValue("@reason", (int)trialStatusReason);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Guid id = (Guid)reader["accountid"];
                            string phone = reader["telephone1"] != DBNull.Value ? (string)reader["telephone1"] : string.Empty;
                            if (phone != string.Empty)
                            {
                                retVal.Add(id, phone);
                            }
                        }
                    }
                }
            }
            return retVal;
        }

        public account GetSupplierByPhone(string phone)
        {
            var supplier = XrmDataContext.accounts.FirstOrDefault(
                x => x.telephone1 == phone
                && !x.new_affiliateoriginid.HasValue
                && !x.new_securitylevel.HasValue);
            return supplier;
        }

        public Guid GetSupplierIdByPhone(string phone)
        {
            string query =
                @"select accountid from account with(nolock)
				where deletionstatecode = 0 and telephone1 = @phone";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@phone", phone));
            Guid retVal = Guid.Empty;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        public string GetPassword(Guid supplierId)
        {
            string query =
                @"select new_password
				from account with(nolock)
				where deletionstatecode = 0
				and accountid = @supplierId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            object scalar = ExecuteScalar(query, parameters);
            string retVal = null;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        public void MarkSupplierInCallAsRemoveMe(Guid incidentAccountId)
        {
            string query =
                @"update account
				set new_trialstatusreason = 5
				,new_trialstatusreasonmodifiedon = getdate()
				where
				accountid = (
				select top 1 new_accountid from new_incidentaccount with(nolock)
				where new_incidentaccountid = @callId)";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@callId", incidentAccountId));
            ExecuteNonQuery(query, parameters);
        }

        public void CheckDoNotCallList()
        {
            string query =
                @"update account set new_trialstatusreason = 8
				where deletionstatecode = 0
				and new_trialstatusreason = 1
				and new_status = 4
				and telephone1 in (
					select new_name from new_donotcalllist with(nolock)
					where deletionstatecode = 0
				)";
            ExecuteNonQuery(query);
        }

        public Guid FindAarSupplierByPhone(string from)
        {
            string query =
                @"select accountid
				from account with(nolock)
				where deletionstatecode = 0
				and new_status = 4
				and @from like '%' + telephone1 ";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@from", from));
            object scalar = ExecuteScalar(query, parameters);
            Guid retVal = Guid.Empty;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        public bool IsAvailableHours(Guid supplierId)
        {
            string query =
                @"select 1
				from new_availability with(nolock)
				where deletionstatecode = 0
				and new_day = datepart(dw,getdate())
				and new_accountid = @supplierId
				and (new_from <= CONVERT(char(8),getdate(),108) AND new_to >= CONVERT(char(8),getdate(),108))";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            object scalar = ExecuteScalar(query, parameters);
            bool retVal = false;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = true;
            }
            return retVal;
        }

        public List<Dictionary<string, object>> GetAdvertisersReport(DateTime fromDate, DateTime toDate, decimal? maxBalance,
            DML.Reports.SupplierStatus supplierStatus, bool? isFromAAR, bool? isWebRegistrant, NoProblem.Core.DataModel.Reports.eCompletedRegistrationStep CompletedRegistrationStep)//, int pageNumber, int pageSize)
        {
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            /*
            StringBuilder queryBuilder = new StringBuilder(
                @"
WITH PagerTbl AS
				(                
				SELECT ROW_NUMBER() OVER(ORDER BY acc.createdon desc) AS RowNum,
				acc.accountid, acc.accountnumber, acc.name, acc.new_firstname + ' ' + acc.new_lastname as 'contactName',UserInterfaceName,
				acc.telephone1, acc.emailaddress1, acc.new_stageinregistration, acc.new_stageintrialregistration,
				acc.createdon, acc.new_cashbalance, acc.new_isfromaar, acc.new_status, acc.new_trialstatusreason,
				(select case when acc.new_unavailabilityreasonid is null
					or acc.new_unavailableto < getdate()
					or acc.new_unavailablefrom > getdate() then 1 else 0 end) as 'Available'
				from account acc with(nolock)
				left join TimeZoneDefinition with (nolock) on TimeZoneCode = acc.address1_utcoffset
				where acc.deletionstatecode = 0 and (acc.new_securitylevel is null or acc.new_securitylevel = 0) and acc.new_affiliateoriginid is null"
                );
             * */
            StringBuilder queryBuilder = new StringBuilder(
                @"
                SELECT TOP 1000 
				acc.accountid, acc.accountnumber, acc.name, acc.new_firstname + ' ' + acc.new_lastname as 'contactName',UserInterfaceName,
				acc.telephone1, acc.emailaddress1, acc.new_stepinregistration2014, acc.new_stageintrialregistration,
				acc.createdon, acc.new_cashbalance, acc.new_isfromaar, acc.new_status, acc.new_trialstatusreason, acc.new_checkoutbuttonclicked, 
				(select case when acc.new_unavailabilityreasonid is null
					or acc.new_unavailableto < getdate()
					or acc.new_unavailablefrom > getdate() then 1 else 0 end) as 'Available'
				from account acc with(nolock)
				left join TimeZoneDefinition with (nolock) on TimeZoneCode = acc.address1_utcoffset
				where acc.deletionstatecode = 0 and (acc.new_securitylevel is null or acc.new_securitylevel = 0) and acc.new_affiliateoriginid is null"
                );
            AdvertiserReportDates(fromDate, toDate, parameters, queryBuilder);
            AdvertiserReportStatus(supplierStatus, CompletedRegistrationStep, parameters, queryBuilder);
            AdvertiserReportMaxBalance(maxBalance, parameters, queryBuilder);
            AdvertiserReportIsFromAAR(isFromAAR, queryBuilder);
            AdvertiserReportIsWebRegistrant(isWebRegistrant, queryBuilder);
            /*
            queryBuilder.Append(@" )
					SELECT * , (select max(RowNum) from PagerTbl) as totalRows
					  FROM PagerTbl
					 WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
									  AND @PageNum * @PageSize)
							or @PageSize = -1)
					 ORDER BY RowNum ");
             *  parameters.Add(new KeyValuePair<string, object>("@PageNum", pageNumber));
            parameters.Add(new KeyValuePair<string, object>("@PageSize", pageSize));
             */

            var reader = ExecuteReader(queryBuilder.ToString(), parameters);
            return reader;
        }

        private void AdvertiserReportIsFromAAR(bool? isFromAAR, StringBuilder queryBuilder)
        {
            if (isFromAAR.HasValue)
            {
                if (isFromAAR.Value)
                {
                    queryBuilder.Append(" and acc.new_isfromaar = 1 ");
                }
                else
                {
                    queryBuilder.Append(" and ( acc.new_isfromaar is null or acc.new_isfromaar = 0 ) ");
                }
            }
        }

        private void AdvertiserReportIsWebRegistrant(bool? isWebRegistrant, StringBuilder queryBuilder)
        {
            if (isWebRegistrant.HasValue)
            {
                if (isWebRegistrant.Value)
                {
                    queryBuilder.Append(" and acc.new_stageintrialregistration > 0 ");
                }
                else
                {
                    queryBuilder.Append(" and (acc.new_stageintrialregistration is null or acc.new_stageintrialregistration = 0) ");
                }
            }
        }

        private void AdvertiserReportMaxBalance(decimal? maxBalance, List<KeyValuePair<string, object>> parameters, StringBuilder queryBuilder)
        {
            if (maxBalance.HasValue)
            {
                queryBuilder.Append(" and ( acc.new_cashbalance is null or acc.new_cashbalance <= @maxBalance ) ");
                parameters.Add(new KeyValuePair<string, object>("@maxBalance", maxBalance.Value));
            }
        }

        private void AdvertiserReportStatus(DML.Reports.SupplierStatus supplierStatus, NoProblem.Core.DataModel.Reports.eCompletedRegistrationStep CompletedRegistrationStep,
            List<KeyValuePair<string, object>> parameters, StringBuilder queryBuilder)
        {
            if (supplierStatus != NoProblem.Core.DataModel.Reports.SupplierStatus.All)
            {
               
                int status = 0;
                if (supplierStatus == NoProblem.Core.DataModel.Reports.SupplierStatus.Available                   
                    || supplierStatus == NoProblem.Core.DataModel.Reports.SupplierStatus.Inactive
                    || supplierStatus == NoProblem.Core.DataModel.Reports.SupplierStatus.Unavailable)
                {
                    queryBuilder.Append(" and acc.new_status = @status ");
                    switch (supplierStatus)
                    {
                        case NoProblem.Core.DataModel.Reports.SupplierStatus.Available:
                            status = 1;
                            queryBuilder.Append(@" and
									(
										acc.new_unavailabilityreasonid is null
										or acc.new_unavailableto < getdate()
										or acc.new_unavailablefrom > getdate()
									) ");
                            break;
                        case NoProblem.Core.DataModel.Reports.SupplierStatus.Unavailable:
                            status = 1;
                            queryBuilder.Append(@" and
									(
										not(acc.new_unavailabilityreasonid is null
										or acc.new_unavailableto < getdate()
										or acc.new_unavailablefrom > getdate())
									) ");
                            break;                        
                        case NoProblem.Core.DataModel.Reports.SupplierStatus.Inactive:
                            status = 3;
                            break;
                       
                           
                    }
                }
                else if (supplierStatus == NoProblem.Core.DataModel.Reports.SupplierStatus.Candidate)
                {
                    status = 2;
                    queryBuilder.Append(" AND ISNULL(acc.new_legacyadvertiser2014, 0) = 0 AND ISNULL(acc.new_isleadbuyer, 0) = 0 ");
                    if (CompletedRegistrationStep == DML.Reports.eCompletedRegistrationStep.PaymentRegistered)
                    {
                        queryBuilder.Append(" and acc.new_status <> @status ");
                        queryBuilder.Append(" AND acc.new_stepinregistration2014 = 7 ");
                    }
                    else if (CompletedRegistrationStep == DML.Reports.eCompletedRegistrationStep.All)
                    {
                        queryBuilder.Append(" AND acc.new_stepinregistration2014 IS NOT NULL ");
                    }
                    else
                    {
                        queryBuilder.Append(" and acc.new_status = @status ");
                        if ((int)CompletedRegistrationStep < 6)
                            queryBuilder.Append(" and acc.new_stepinregistration2014 = " + (int)CompletedRegistrationStep + " ");
                        else if ((int)CompletedRegistrationStep == 6 || (int)CompletedRegistrationStep == 7)
                        {
                            queryBuilder.Append(" and acc.new_stepinregistration2014 = " + 6 + " ");
                            queryBuilder.Append(" and acc.new_checkoutbuttonclicked = " + ((int)CompletedRegistrationStep == 6 ? "0" : "1"));
                        }
                        else
                            queryBuilder.Append(" and acc.new_stepinregistration2014 = 7 ");
                        
                    }
                    
                }
                parameters.Add(new KeyValuePair<string, object>("@status", status));               
            }
        }

        private void AdvertiserReportDates(DateTime fromDate, DateTime toDate, List<KeyValuePair<string, object>> parameters, StringBuilder queryBuilder)
        {
            queryBuilder.Append(" and ( (acc.new_createdonoverride is null and acc.createdon between @from and @to ) or ( acc.new_createdonoverride between @from and @to ) ) ");
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate));
        }

        public bool IsPayingAdvertiser(Guid supplierId)
        {
            string query =
                @"select count(*) from new_supplierpricing with(nolock)
					where deletionstatecode = 0 
					and new_accountid = @supplierId
					and new_voucherid is null";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            int count = (int)ExecuteScalar(query, parameters);
            return count > 0;
        }

        public bool IsWaitingForInvitation(Guid supplierId)
        {
            var waitingForInvitation =
                 from x in XrmDataContext.accounts
                 where x.accountid == supplierId
                 select x.new_waitingforinvitation;
            bool retVal = false;
            bool? first = waitingForInvitation.FirstOrDefault();
            if (first != null)
            {
                retVal = first.Value;
            }
            return retVal;

        }

        public account GetSupplierByBizId(string bizId)
        {
            return XrmDataContext.accounts.FirstOrDefault(x => x.new_bizid == bizId);
            //string sql = "select * from account where new_bizid = @id";
        }

        public Guid GetSupplierIdByBizId(string bizId)
        {
            SqlParametersList parameters = new DalBase<account>.SqlParametersList();
            parameters.Add("@bizid", bizId);
            object scalar = ExecuteScalar(queryForGetIdByBizId, parameters);
            Guid retVal = Guid.Empty;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        private const string queryForGetIdByBizId =
            @"select top 1 accountid from account with(nolock)
			where deletionstatecode = 0
			and new_bizid = @bizid";

        public bool ExistsByBizId(string bizId)
        {
            SqlParametersList parameters = new DalBase<account>.SqlParametersList();
            parameters.Add("@bizid", bizId);
            object scalar = ExecuteScalar(queryForExistsByBizId, parameters);
            bool retVal = (scalar != null && scalar != DBNull.Value);
            return retVal;
        }

        private const string queryForExistsByBizId =
            @"select top 1 1 from account with(nolock)
			where deletionstatecode = 0
			and new_bizid = @bizid";

        public bool ExistsByBizIdExcludingThisId(Guid supplierId, string bizId)
        {
            SqlParametersList parameters = new DalBase<account>.SqlParametersList();
            parameters.Add("@bizid", bizId);
            parameters.Add("@supplierId", supplierId);
            object scalar = ExecuteScalar(queryForExistsByBizId, parameters);
            bool retVal = (scalar != null && scalar != DBNull.Value);
            return retVal;
        }

        public void CheckMobileAccountAfterRequest(Guid accountId)
        {
            string command = @"EXEC [dbo].[CheckMobileAccountAfterRequest] @AccountId, @freeLeads";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, connection))
                {
                    connection.Open();
                    cmd.Parameters.Add("@AccountId", accountId);
                    cmd.Parameters.Add("@freeLeads", MOBILE_FREELEADS);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }
        public bool IsActiveMobileAccount(Guid accountId)
        {
            string command = @"SELECT New_status
 FROM [dbo].[Account] WITH(NOLOCK)
 WHERE DeletionStateCode = 0
	and AccountId = @AccountId
	and New_IsMobile = 1 and New_havevideo = 1";
            NoProblem.Core.DataModel.Xrm.account.SupplierStatus supplierStatus = account.SupplierStatus.Inactive;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, connection))
                {
                    connection.Open();
                    cmd.Parameters.AddWithValue("@AccountId", accountId);
                    object o = cmd.ExecuteScalar();
                    if (o != null && o != DBNull.Value)
                       supplierStatus = (account.SupplierStatus)((int)o);
                    connection.Close();
                }
            }           
            return (supplierStatus == account.SupplierStatus.Trial || supplierStatus == account.SupplierStatus.Approved);
        }
        private const string queryForExistsByBizIdExcludingThisId =
            @"select top 1 1 from account with(nolock)
			where deletionstatecode = 0
			and new_bizid = @bizid
			and accountid != @supplierId";
        public bool HaveFreeMobileLead(Guid accountId)
        {
            string command = @"SELECT COUNT(*)
FROM dbo.New_incidentaccountExtensionBase
WHERE new_accountid = @accountId
	and New_tocharge = 1
	and new_potentialincidentprice = 0";
            int freeLeads;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, connection))
                {
                    connection.Open();
                    cmd.Parameters.AddWithValue("@accountId", accountId);
                    freeLeads = (int)cmd.ExecuteScalar();
                    connection.Close();
                }
            }
            return freeLeads < MOBILE_FREELEADS;
        }
        public bool IsAccountExistsAndActive(Guid accountId)
        {
            string command = @"
SELECT count(*) 
FROM dbo.AccountBase B with(nolock) 
	INNER JOIN dbo.AccountExtensionBase A with(nolock) on B.AccountId = A.AccountId
WHERE B.DeletionStateCode = 0 and B.AccountId = @accountId and New_status <> 3";//New_status <> Inactive
            bool result;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@accountId", accountId);
                    int c = (int)cmd.ExecuteScalar();
                    result = c > 0;
                    conn.Close();
                }
            }
            return result;
        }
        public bool IsAccountExistsAndActive(Guid accountId, out string supplierName)
        {
            string command = @"
SELECT name
FROM dbo.AccountBase B with(nolock) 
	INNER JOIN dbo.AccountExtensionBase A with(nolock) on B.AccountId = A.AccountId
WHERE B.DeletionStateCode = 0 and B.AccountId = @accountId and New_status <> 3";//New_status <> Inactive
            bool result;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@accountId", accountId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if(reader.Read())
                    {
                        result = true;
                        supplierName = reader["name"] == DBNull.Value ? string.Empty : (string)reader["name"];
                    }
                    else
                    {
                        result = false;
                        supplierName = null;
                    }
                    conn.Close();
                }
            }
            return result;
        }
    }
}
