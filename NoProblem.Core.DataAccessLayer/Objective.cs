﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class Objective : DalBase<DataModel.Xrm.new_objective>
    {
        #region Ctor
        public Objective(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_objective");
        } 
        #endregion

        #region Public Methods

        public override NoProblem.Core.DataModel.Xrm.new_objective Retrieve(Guid id)
        {
            return XrmDataContext.new_objectives.FirstOrDefault(x => x.new_objectiveid == id);
        }

        public List<NoProblem.Core.DataModel.Dashboard.ObjectiveData> GetAllObjectives()
        {
            string query =
                @"select 
                new_objectiveid
                ,new_fromdate
                ,new_todate
                ,new_amount
                ,new_criteria
                from new_objective with(nolock)
                where
                deletionstatecode  = 0
                order by new_criteria,new_fromdate";
            List<DataModel.Dashboard.ObjectiveData> retVal = new List<NoProblem.Core.DataModel.Dashboard.ObjectiveData>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Dashboard.ObjectiveData data = new NoProblem.Core.DataModel.Dashboard.ObjectiveData();
                            data.ObjectiveId = (Guid)reader[0];
                            data.FromDate = (DateTime)reader[1];
                            data.ToDate = (DateTime)reader[2];
                            data.Amount = (decimal)reader[3];
                            data.Criteria = (DataModel.Dashboard.Criteria)reader[4];
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public bool IsOverlaping(DateTime fromDate, DateTime toDate, Guid selfId, DataModel.Dashboard.Criteria criteria)
        {
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate.Date));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate.Date));
            parameters.Add(new KeyValuePair<string, object>("@criteria", (int)criteria));
            string query =
                @"select 1
                from
                new_objective with(nolock)
                where
                new_criteria = @criteria
                and
                new_fromdate <= @to
                and 
                new_todate >= @from
                and
                deletionstatecode = 0";
            if (selfId != Guid.Empty)
            {
                query += " and new_objectiveid != @id";
                parameters.Add(new KeyValuePair<string, object>("@id", selfId));
            }

            object scalar = ExecuteScalar(query, parameters);
            bool retVal = true;
            if (scalar == null || scalar == DBNull.Value)
            {
                retVal = false;
            }
            return retVal;
        }

        public int FixOldOverlapedObjectives(DateTime fromDate, DateTime toDate, DataModel.Dashboard.Criteria criteria)
        {
            int affectedRows = 0;
            bool deletedSome = DeleteOverlappedDays(fromDate, toDate, criteria);
            if (deletedSome)
            {
                //delete objective that has no day data pointing at him (was totally overlapped by the new one).
//                Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_objective",
//                    @"new_objectiveid in(
//                    select obj.new_objectiveid
//                    from
//                    new_objective obj with(nolock)
//                    left join new_objectivedaydata dd with(nolock) on obj.new_objectiveid = dd.new_objectiveid and dd.deletionstatecode = 0
//                    where 
//                    obj.deletionstatecode = 0
//                    and
//                    dd.new_objectivedaydataid is null)");

                string sqlDelete = @"update new_objective set deletionstatecode = 2 where new_objectiveid in (
                    select obj.new_objectiveid
                    from
                    new_objective obj with(nolock)
                    left join new_objectivedaydata dd with(nolock) on obj.new_objectiveid = dd.new_objectiveid and dd.deletionstatecode = 0
                    where 
                    obj.deletionstatecode = 0
                    and
                    dd.new_objectivedaydataid is null)";
                ExecuteNonQuery(sqlDelete);

                //fix remaining overlaped objective, fix start and end date.
                string query =
                    @"
                    DECLARE loop CURSOR FOR
                        select
                        new_objectiveid
                        from
                        new_objective with(nolock)
                        where
                        new_criteria = @criteria
                        and
                        new_fromdate <= @to
                        and 
                        new_todate >= @from
                        and
                        deletionstatecode = 0

                    DECLARE @new_objectiveid uniqueidentifier

                    OPEN loop       
                    FETCH loop INTO @new_objectiveid

                    WHILE @@Fetch_Status = 0
                    BEGIN
                          update new_objective
                                    set new_fromdate = (select min(new_date)
									                    from new_objectivedaydata dd with(nolock) 
									                    where dd.new_objectiveid = @new_objectiveid
                                                        and dd.deletionstatecode = 0)
				                    , new_todate = (select max(new_date)
									                    from new_objectivedaydata dd with(nolock) 
									                    where dd.new_objectiveid = @new_objectiveid
                                                        and dd.deletionstatecode = 0)
                        where new_objectiveid = @new_objectiveid 

	                FETCH loop INTO @new_objectiveid
                    END
                    CLOSE loop

                    DEALLOCATE loop";
                List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
                parameters.Add(new KeyValuePair<string, object>("@from", fromDate));
                parameters.Add(new KeyValuePair<string, object>("@to", toDate));
                parameters.Add(new KeyValuePair<string, object>("@criteria", (int)criteria));
                affectedRows = ExecuteNonQuery(query, parameters);
            }
            return affectedRows;
        }        

        public void Delete(Guid objectiveId)
        {
            string sql = "update new_objectivebase set deletionstatecode = 2 where new_objectiveid = @objectiveId";
            string sql2 = "update new_objectivedaydata set deletionstatecode = 2 where new_objectiveid = @objectiveId";
            SqlParametersList parameters = new SqlParametersList();
            parameters.Add("@objectiveId", objectiveId);
            ExecuteNonQuery(sql2, parameters);
            ExecuteNonQuery(sql, parameters);
            //Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_objectivedaydata",
            //    "new_objectiveid = '" + objectiveId.ToString() + "'");
            //Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_objective",
            //    "new_objectiveid = '" + objectiveId.ToString() + "'");
        }

        public bool IsCutsInTheMiddle(DataModel.Dashboard.Criteria criteria, DateTime newFrom, DateTime newTo, out DateTime existsFrom, out DateTime existsTo, out decimal existsAmount, out Guid existsId)
        {
            bool retVal = false;
            string query = @"
                select new_objectiveid, new_fromdate, new_todate, new_amount
                from
                new_objective with(nolock)
                where
                new_fromdate < @from
                and
                new_todate > @to
                and
                deletionstatecode = 0
                and
                new_criteria = @criteria";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@from", newFrom);
                    command.Parameters.AddWithValue("@to", newTo);
                    command.Parameters.AddWithValue("@criteria", (int)criteria);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            retVal = true;
                            existsAmount = (decimal)reader["new_amount"];
                            existsFrom = (DateTime)reader["new_fromdate"];
                            existsTo = (DateTime)reader["new_todate"];
                            existsId = (Guid)reader["new_objectiveid"];
                        }
                        else
                        {
                            existsAmount = default(decimal);
                            existsFrom = default(DateTime);
                            existsTo = default(DateTime);
                            existsId = default(Guid);
                        }
                    }
                }
            }
            return retVal;
        }

        #endregion

        #region Private Methods

        private bool DeleteOverlappedDays(DateTime fromDate, DateTime toDate, DataModel.Dashboard.Criteria criteria)
        {
            //delete all old overlaped day data:
            //first find them:
            string getDayDataToDeleteQuery =
                @"select new_objectivedaydataid
                from
                new_objectivedaydata with(nolock)
                join new_objective with(nolock) on new_objective.new_objectiveid = new_objectivedaydata.new_objectiveid
                where
                new_objective.new_criteria = @criteria
                and
                new_objectivedaydata.new_date between @from and @to
                and
                new_objective.deletionstatecode = 0 and new_objectivedaydata.deletionstatecode = 0";
            List<Guid> dayDataToDelete = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(getDayDataToDeleteQuery, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@criteria", (int)criteria);
                    command.Parameters.AddWithValue("@to", toDate);
                    command.Parameters.AddWithValue("@from", fromDate);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dayDataToDelete.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            StringBuilder guidString = new StringBuilder("(");
            for (int i = 0; i < dayDataToDelete.Count; i++)
            {
                guidString.Append("'");
                guidString.Append(dayDataToDelete[i].ToString());
                guidString.Append("'");
                if (i < dayDataToDelete.Count - 1)
                {
                    guidString.Append(",");
                }
            }
            guidString.Append(")");

            bool found = guidString.Length > 2;
            if (found)//if something was found.
            {
                //delete day data.
               // Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_objectivedaydata",
                //    "new_objectivedaydataid in " + guidString.ToString());

                string sql = "update new_objectivedaydata set deletionstatecode = 2 where new_objectivedaydataid in " + guidString.ToString();
                ExecuteNonQuery(sql);
            }
            return found;
        } 

        #endregion
    }
}
