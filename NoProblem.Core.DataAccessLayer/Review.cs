﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataAccessLayer.Autonumber;

namespace NoProblem.Core.DataAccessLayer
{
    public class Review : DalBase<DataModel.Xrm.new_review>
    {
        public const string tableName = "new_review";

        public Review(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache(tableName);
        }

        public override NoProblem.Core.DataModel.Xrm.new_review Retrieve(Guid id)
        {
            DML.Xrm.new_review retVal = XrmDataContext.new_reviews.FirstOrDefault(x => x.new_reviewid == id);
            return retVal;
        }

        public override void Create(NoProblem.Core.DataModel.Xrm.new_review review)
        {
            AutonumberManager autoNumber = new AutonumberManager();
            review.new_number = autoNumber.GetNextNumber(tableName);
            base.Create(review);
        }

        public IEnumerable<DML.Xrm.new_review> GetReviews(DateTime? fromDate, DateTime? toDate, DML.Xrm.new_review.ReviewStatus? status, Guid? supplierId)
        {
            IEnumerable<DML.Xrm.new_review> retVal = XrmDataContext.new_reviews;
            if (fromDate.HasValue && toDate.HasValue)
            {
                retVal = from review in retVal
                         where
                         review.createdon.Value >= fromDate.Value
                         &&
                         review.createdon.Value <= toDate.Value
                         select
                         review;
            }

            if (status.HasValue)
            {
                retVal = from review in retVal
                         where
                         review.new_status.HasValue
                         &&
                         review.new_status.Value == (int)status.Value
                         select
                         review;
            }

            if (supplierId.HasValue)
            {
                retVal = from review in retVal
                         where
                         review.new_accountid.HasValue
                         &&
                         review.new_accountid.Value == supplierId.Value
                         select
                         review;
            }

            return retVal;
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_review> GetBySupplierAndPhone(Guid supplierId, string phone)
        {
            IEnumerable<NoProblem.Core.DataModel.Xrm.new_review> retVal =
                from review in XrmDataContext.new_reviews
                where
                review.new_accountid.HasValue
                &&
                review.new_accountid.Value == supplierId
                &&
                review.new_phone == phone
                select
                review;
            return retVal;
        }
    }
}
