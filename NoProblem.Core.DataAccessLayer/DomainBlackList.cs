﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class DomainBlackList : DalBase<DataModel.Xrm.new_domainblacklist>
    {
        public DomainBlackList(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_domainblacklist");
        }

        public override NoProblem.Core.DataModel.Xrm.new_domainblacklist Retrieve(Guid id)
        {
            return XrmDataContext.new_domainblacklists.FirstOrDefault(x => x.new_domainblacklistid == id);
        }
    }
}
