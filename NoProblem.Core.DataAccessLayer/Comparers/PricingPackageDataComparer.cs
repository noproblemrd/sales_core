﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.DataAccessLayer
{
    public class PricingPackageDataComparer : IComparer<PricingPackageData>
    {
        #region IComparer<PricingPackageData> Members

        public int Compare(PricingPackageData x, PricingPackageData y)
        {
            int retVal;
            if (x.FromAmount > y.FromAmount)
            {
                retVal = 1;
            }
            else if (x.FromAmount < y.FromAmount)
            {
                retVal = -1;
            }
            else
            {
                retVal = 0;
            }
            return retVal;
        }

        #endregion
    }
}
