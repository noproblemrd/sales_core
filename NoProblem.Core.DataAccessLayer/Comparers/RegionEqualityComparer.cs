﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.RegionComparer
//  File: RegionComparer.cs
//  Description: EqualityComparer for new_region
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.DataAccessLayer
{
    public class RegionEqualityComparer : EqualityComparer<new_region>
    {
        public override bool Equals(new_region x, new_region y)
        {
            return (x.new_regionid == y.new_regionid);
        }

        public override int GetHashCode(new_region obj)
        {
            return obj.GetHashCode();
        }
    }
}
