﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AarIncomingCall : DalBase<DataModel.Xrm.new_aarincomingcall>
    {
        public AarIncomingCall(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_aarincomingcall");
        }

        public override NoProblem.Core.DataModel.Xrm.new_aarincomingcall Retrieve(Guid id)
        {
            return XrmDataContext.new_aarincomingcalls.FirstOrDefault(x => x.new_aarincomingcallid == id);
        }

        public NoProblem.Core.DataModel.Xrm.new_aarincomingcall GetCallByCallSid(string callSid)
        {
            return XrmDataContext.new_aarincomingcalls.FirstOrDefault(x => x.new_dialercallid == callSid);
        }

        public void UpdateDtmfPressed(Guid callId, string digits)
        {
            string query =
                @"update new_aarincomingcall
                set new_dtmfpressed = @digits
                where new_aarincomingcallid = @callId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@digits", digits));
            parameters.Add(new KeyValuePair<string, object>("@callId", callId));
            ExecuteNonQuery(query, parameters); 
        }

        public void UpdateRecordFileLocation(Guid callId, string recordingUrl)
        {
            string query =
                @"update new_aarincomingcall
                set new_recordfilelocation = @url
                where new_aarincomingcallid = @id";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@url", recordingUrl));
            parameters.Add(new KeyValuePair<string, object>("@id", callId));
            ExecuteNonQuery(query, parameters);
        }

        public void MarkSupplierInCallAsRemoveMe(Guid callId)
        {
            string query =
                @"update account
                set new_trialstatusreason = 5
                ,new_trialstatusreasonmodifiedon = getdate()
                where
                accountid = (
                select top 1 new_accountid from new_aarincomingcall with(nolock)
                where new_aarincomingcallid = @callId)";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@callId", callId));
            ExecuteNonQuery(query, parameters);
        }
    }
}
