﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class InjectionReport : DalBase<DataModel.Xrm.new_injectionreport>
    {
        public InjectionReport(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_injectionreport");
        }

        public override IQueryable<DataModel.Xrm.new_injectionreport> All
        {
            get
            {
                return XrmDataContext.new_injectionreports;
            }
        }

        public override DataModel.Xrm.new_injectionreport Retrieve(Guid id)
        {
            return All.FirstOrDefault(x => x.new_injectionreportid == id);
        }
    }
}
