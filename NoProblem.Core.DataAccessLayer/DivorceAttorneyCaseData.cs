﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class DivorceAttorneyCaseData : DalBase<DataModel.Xrm.new_divorceattorneycasedata>
    {
        public DivorceAttorneyCaseData(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_divorceattorneycasedata");
        }

        public override NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata Retrieve(Guid id)
        {
            return XrmDataContext.new_divorceattorneycasedatas.FirstOrDefault(x => x.new_divorceattorneycasedataid == id);
        }
    }
}
