﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class ObjectiveDayData : DalBase<DataModel.Xrm.new_objectivedaydata>
    {
        #region Ctor
        public ObjectiveDayData(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_objectivedaydata");
        } 
        #endregion

        #region Public Methods

        public override NoProblem.Core.DataModel.Xrm.new_objectivedaydata Retrieve(Guid id)
        {
            return XrmDataContext.new_objectivedaydatas.FirstOrDefault(x => x.new_objectivedaydataid == id);
        }

        public List<DML.Dashboard.DayData> GetRangeData(DateTime fromDate, DateTime toDate)
        {
            List<DML.Dashboard.DayData> retVal = new List<DML.Dashboard.DayData>();

            string query =
                @"select dd.new_date, dd.new_amount, obj.new_criteria
                    from
                    new_objectivedaydata dd with(nolock)
                    join new_objective obj with(nolock) on obj.new_objectiveid = dd.new_objectiveid
                    where
                    dd.new_date between @from and @to
                    and
                    dd.deletionstatecode = 0
                    and 
                    obj.deletionstatecode = 0";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@from", fromDate);
                    command.Parameters.AddWithValue("@to", toDate);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DateTime newDate = (DateTime)reader[0];
                            int createriaInt = (int)reader[2];
                            DML.Dashboard.Criteria criteria = (DML.Dashboard.Criteria)createriaInt;
                            PropertyInfo property = typeof(DML.Dashboard.DayData).GetProperty(criteria.ToString());
                            var existing = retVal.FirstOrDefault(x => x.CreatedOn == newDate);
                            if (existing == null)
                            {
                                DML.Dashboard.DayData data = new NoProblem.Core.DataModel.Dashboard.DayData();                                
                                property.SetValue(data, (decimal)reader[1], null);
                                data.CreatedOn = newDate;
                                retVal.Add(data);
                            }
                            else
                            {
                                property.SetValue(existing, (decimal)reader[1], null);
                            }
                        }
                    }
                }
            }

            TimeSpan span = toDate - fromDate;
            int days = span.Days + 1;
            DateTime current = fromDate.Date;
            for (int i = 0; i < days; i++)
            {
                var item = retVal.FirstOrDefault(x => x.CreatedOn.Date == current);
                if (item == null)
                {
                    DML.Dashboard.DayData data = new NoProblem.Core.DataModel.Dashboard.DayData();
                    data.CreatedOn = current;
                    retVal.Add(data);
                }
                current = current.AddDays(1);
            }

            return retVal;
        }

        #endregion
    }
}
