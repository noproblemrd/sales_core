﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class ExtraBonusReason : DalBase<DataModel.Xrm.new_extrabonusreason>, ITableDal
    {
        public ExtraBonusReason(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_extrabonusreason");
        }

        public override NoProblem.Core.DataModel.Xrm.new_extrabonusreason Retrieve(Guid id)
        {
            return XrmDataContext.new_extrabonusreasons.FirstOrDefault(x => x.new_extrabonusreasonid == id);
        }

        #region ITableDal Members

        public List<NoProblem.Core.DataModel.Tables.TableRowData> GetAll()
        {
            List<DataModel.Tables.TableRowData> retVal = new List<NoProblem.Core.DataModel.Tables.TableRowData>();
            string query =
                @"SELECT new_extrabonusreasonid, new_name, new_disabled, new_id FROM new_extrabonusreason with (nolock) WHERE deletionstatecode = 0";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Tables.TableRowData data = new NoProblem.Core.DataModel.Tables.TableRowData();
                            data.Name = (string)reader["new_name"];
                            data.Inactive = reader["new_disabled"] != DBNull.Value ? (bool)reader["new_disabled"] : false;
                            data.Guid = (Guid)reader["new_extrabonusreasonid"];
                            data.Id = (string)reader["new_id"];
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        #endregion
    }
}
