﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.RefundReason
//  File: RefundReason.cs
//  Description: DAL for RefundReason Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Linq;
using System.Collections.Generic;
using NoProblem.Core.DataModel.Refunds;
using NoProblem.Core.DataModel.Xrm;
using DML = NoProblem.Core.DataModel;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class RefundReason : DalBase<DML.Xrm.new_refundreason>, ITableDal
    {
        #region Ctor
        public RefundReason(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_refundreason");
        } 
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_refundreason Retrieve(Guid id)
        {
            DML.Xrm.new_refundreason reason = XrmDataContext.new_refundreasons.FirstOrDefault(x => x.new_refundreasonid == id);
            return reason;
        }

        public Guid GetRefundReasonIdByCode(int code)
        {
            string query =
                @"select new_refundreasonid from new_refundreason with(nolock)
                    where new_code = @code";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@code", code));
            Guid retVal = Guid.Empty;
            object scalar = ExecuteScalar(query, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        public List<RefundReasonData> GetAllActiveMobileReasons()
        {
            List<new_refundreason> refundReasons =
                XrmDataContext.new_refundreasons.Where(x => x.new_ismobile.Value && !x.new_disabled.Value).ToList();
            return refundReasons.Select(r => new RefundReasonData
            {
                Name = r.new_name,
                Code = r.new_code.Value,
                Guid = r.new_refundreasonid,
                Inactive = r.new_disabled.HasValue && r.new_disabled.Value,
            }).ToList();
        }

        public List<RefundReasonData> GetAll()
        {
            List<new_refundreason> refundReasons = XrmDataContext.new_refundreasons.ToList();
            return refundReasons.Select(r => new RefundReasonData
            {
                Name = r.new_name,
                Code = r.new_code.Value,
                Guid = r.new_refundreasonid,
                Inactive = r.new_disabled.HasValue && r.new_disabled.Value,
            }).ToList();
        }

        #region ITableDal Members

        List<NoProblem.Core.DataModel.Tables.TableRowData> ITableDal.GetAll()
        {
            string query =
                @"select new_disabled, new_code, new_name, new_refundreasonid from new_refundreason with(nolock)
                    where deletionstatecode = 0";
            List<DataModel.Tables.TableRowData> retVal = new List<NoProblem.Core.DataModel.Tables.TableRowData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Tables.TableRowData data = new NoProblem.Core.DataModel.Tables.TableRowData();
                            data.Id = ((int)reader["new_code"]).ToString();
                            data.Inactive = reader["new_disabled"] == DBNull.Value ? false : (bool)reader["new_disabled"];
                            data.Name = (string)reader["new_name"];
                            data.Guid = (Guid)reader["new_refundreasonid"];
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        #endregion
    }
}
