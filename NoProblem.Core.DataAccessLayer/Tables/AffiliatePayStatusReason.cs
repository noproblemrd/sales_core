﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class AffiliatePayStatusReason : DalBase<DataModel.Xrm.new_affiliatepaystatusreason>
    {
        public AffiliatePayStatusReason(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_affiliatepaystatusreason");
        }

        public override NoProblem.Core.DataModel.Xrm.new_affiliatepaystatusreason Retrieve(Guid id)
        {
            return XrmDataContext.new_affiliatepaystatusreasons.FirstOrDefault(x => x.new_affiliatepaystatusreasonid == id);
        }

        public Guid GetDefault(out Guid payStatusId)
        {
            string query =
                @"select new_affiliatepaystatusreasonid, new_affiliatepaystatusid
                from new_affiliatepaystatusreason with(nolock)
                where deletionstatecode = 0 and new_isdefault = 1";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetByUpsaleLoseReason(Guid upsaleReasonId, out Guid payStatusId)
        {
            string query =
                 @"select 
sr.new_affiliatepaystatusreasonid, sr.new_affiliatepaystatusid
from
new_upsalelosereason ur with(nolock)
join new_affiliatepaystatusreason sr with(nolock) on sr.new_affiliatepaystatusreasonid = ur.new_affiliatepaystatusreasonid
where
sr.deletionstatecode = 0 and ur.deletionstatecode = 0
and
ur.new_upsalelosereasonid = @loseReasonId";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@loseReasonId", upsaleReasonId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetByRefundReason(Guid refundReasonId, out Guid payStatusId)
        {
            string query =
                 @"select 
sr.new_affiliatepaystatusreasonid, sr.new_affiliatepaystatusid
from
new_refundreason rr with(nolock)
join new_affiliatepaystatusreason sr with(nolock) on sr.new_affiliatepaystatusreasonid = rr.new_affiliatepaystatusreasonid
where
sr.deletionstatecode = 0 and rr.deletionstatecode = 0
and
rr.new_refundreasonid = @refundReasonId";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@refundReasonId", refundReasonId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            if (retVal == Guid.Empty)
            {
                retVal = GetRefundDefaultReason(out payStatusId);
            }
            return retVal;
        }

        public Guid GetRefundDefaultReason(out Guid payStatusId)
        {
            string query =
                @"select new_affiliatepaystatusreasonid, new_affiliatepaystatusid
                from new_affiliatepaystatusreason with(nolock)
                where deletionstatecode = 0 and new_isrefunddefaultreason = 1";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            if (retVal == Guid.Empty)
            {
                throw new Exception("Affiliate pay status reason 'Refund Default Reason' was not fount in the system, please configure it.");
            }
            return retVal;
        }

        public Guid GetIntervalPayStatus(out Guid payStatusId)
        {
            string query =
                @"select new_affiliatepaystatusid, new_affiliatepaystatusreasonid
                    from new_affiliatepaystatusreason with(nolock)
                    where deletionstatecode = 0
                    and new_disabled = 0
                    and new_isintervalreason = 1";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetDuplicatePayStatus(out Guid payStatusId)
        {
            string query =
                @"select new_affiliatepaystatusid, new_affiliatepaystatusreasonid
                    from new_affiliatepaystatusreason with(nolock)
                    where deletionstatecode = 0
                    and new_disabled = 0
                    and new_isduplicatereason = 1";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetBadWordPayStatus(out Guid payStatusId)
        {
            string query =
                @"select new_affiliatepaystatusid, new_affiliatepaystatusreasonid
                    from new_affiliatepaystatusreason with(nolock)
                    where deletionstatecode = 0
                    and new_disabled = 0
                    and new_isbadwordreason = 1";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetBlackListPayStatus(out Guid payStatusId)
        {
            string query =
                @"select new_affiliatepaystatusid, new_affiliatepaystatusreasonid
                    from new_affiliatepaystatusreason with(nolock)
                    where deletionstatecode = 0
                    and new_disabled = 0
                    and new_isblacklistreason = 1";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetCallCenterReason(out Guid payStatusId)
        {
            string query =
                @"select new_affiliatepaystatusid, new_affiliatepaystatusreasonid
                    from new_affiliatepaystatusreason with(nolock)
                    where deletionstatecode = 0
                    and new_disabled = 0
                    and new_iscallcentercasereason = 1";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetStoppedManuallyPayStatus(out Guid payStatusId)
        {
            string query =
               @"select new_affiliatepaystatusid, new_affiliatepaystatusreasonid
                    from new_affiliatepaystatusreason with(nolock)
                    where deletionstatecode = 0
                    and new_disabled = 0
                    and new_isstoppedmanuallyreason = 1";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetNotForSalePayStatus(out Guid payStatusId)
        {
            string query =
               @"select new_affiliatepaystatusid, new_affiliatepaystatusreasonid
                    from new_affiliatepaystatusreason with(nolock)
                    where deletionstatecode = 0
                    and new_disabled = 0
                    and new_isnotforsalereason = 1";
            Guid retVal = Guid.Empty;
            payStatusId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            payStatusId = (Guid)reader["new_affiliatepaystatusid"];
                            retVal = (Guid)reader["new_affiliatepaystatusreasonid"];
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
