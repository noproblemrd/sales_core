﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class UnavailabilityReason : DalBase<DataModel.Xrm.new_unavailabilityreason>
    {
        public UnavailabilityReason(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_unavailabilityreason");
        }

        public override NoProblem.Core.DataModel.Xrm.new_unavailabilityreason Retrieve(Guid id)
        {
            DataModel.Xrm.new_unavailabilityreason retVal = XrmDataContext.new_unavailabilityreasons.FirstOrDefault(x => x.new_unavailabilityreasonid == id);
            return retVal;
        }

        public override void Create(NoProblem.Core.DataModel.Xrm.new_unavailabilityreason entity)
        {
            string id = GetNextId();
            entity.new_id = id;
            base.Create(entity);
        }

        public NoProblem.Core.DataModel.Xrm.new_unavailabilityreason GetPricingEndedReason()
        {
            DataModel.Xrm.new_unavailabilityreason reason = XrmDataContext.new_unavailabilityreasons.FirstOrDefault(
                x => x.new_ispricingended.HasValue && x.new_ispricingended.Value == true);
            if (reason == null)
            {
                throw new Exception("No 'Is Pricing Ended' unavailability reason found in the system. Please create it");
            }
            return reason;
        }

        public void UpdateDirect(Guid guid, string name, bool isSystem, Guid groupId)
        {
            string query =
                @"update new_unavailabilityreason 
                set new_name = @name, new_issystemreason = @isSystem
                ,new_groupid = @groupId
                where new_unavailabilityreasonid = @id ";
            if (isSystem == false)// if they are trying to remove the isSystem, then we must check this is not the "no balance" reason.
            {//this way we will not allow to update the 'no balance' reason to be not system reason.
                query += "and (new_ispricingended is null or new_ispricingended = 0)";
            }
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@name", name));
            parameters.Add(new KeyValuePair<string, object>("@isSystem", isSystem));
            parameters.Add(new KeyValuePair<string, object>("@id", guid));
            parameters.Add(new KeyValuePair<string, object>("@groupId", groupId));
            ExecuteNonQuery(query, parameters);
        }

        private string GetNextId()
        {
            string query =
                @"select max(cast(new_id as int)) + 1
                from new_unavailabilityreason with(nolock)
                where deletionstatecode = 0";
            return ((int)ExecuteScalar(query)).ToString();
        }

        public void Disable(List<Guid> lst)
        {
            if (lst.Count == 0) return;
            StringBuilder query = new StringBuilder(
                @"update new_unavailabilityreason
                set new_disabled = 1
                where new_unavailabilityreasonid
                in ( ");

            foreach (var id in lst)
            {
                query.Append("'").Append(id.ToString()).Append("',");
            }
            query.Remove(query.Length - 1, 1);
            query.Append(@" )
                and (new_ispricingended is null or new_ispricingended = 0)
                and (new_isdailybudget is null or new_isdailybudget = 0)
                and (new_isoverrefundpercent is null or new_isoverrefundpercent = 0)");
            ExecuteNonQuery(query.ToString());
        }

        public DataModel.Xrm.new_unavailabilityreason GetDailyBudgetReason()
        {
            DataModel.Xrm.new_unavailabilityreason reason = XrmDataContext.new_unavailabilityreasons.FirstOrDefault(
                x => x.new_isdailybudget.HasValue && x.new_isdailybudget.Value == true);
            if (reason == null)
            {
                throw new Exception("No 'Is Daily Budget' unavailability reason found in the system. Please create it");
            }
            return reason;
        }

        public DataModel.Xrm.new_unavailabilityreason GetOverRefundPercentReason()
        {
            DataModel.Xrm.new_unavailabilityreason reason = XrmDataContext.new_unavailabilityreasons.FirstOrDefault(
                x => x.new_isoverrefundpercent.HasValue && x.new_isoverrefundpercent.Value == true);
            if (reason == null)
            {
                throw new Exception("No 'Is Over Refund Percent' unavailability reason found in the system. Please create it");
            }
            return reason;
        }

        public Guid GetDefaultReasonId()
        {
            var query = from res in XrmDataContext.new_unavailabilityreasons
                        where res.new_id == "0"
                        select res.new_unavailabilityreasonid;
            return query.First();
        }
    }
}
