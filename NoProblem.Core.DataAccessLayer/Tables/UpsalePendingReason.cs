﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Tables;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class UpsalePendingReason : DalBase<DataModel.Xrm.new_upsalependingreason>, ITableDal
    {
        public UpsalePendingReason(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_upsalependingreason");
        }

        public override NoProblem.Core.DataModel.Xrm.new_upsalependingreason Retrieve(Guid id)
        {
            return XrmDataContext.new_upsalependingreasons.FirstOrDefault(x => x.new_upsalependingreasonid == id);
        }

        public List<TableRowData> GetAll()
        {
            List<DataModel.Tables.TableRowData> retVal = new List<NoProblem.Core.DataModel.Tables.TableRowData>();
            string query =
                @"SELECT new_upsalependingreasonid,new_name FROM new_upsalependingreason with (nolock) WHERE deletionstatecode = 0";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Tables.TableRowData data = new NoProblem.Core.DataModel.Tables.TableRowData();
                            data.Name = (string)reader["new_name"];
                            data.Inactive = false;
                            data.Guid = (Guid)reader["new_upsalependingreasonid"];
                            data.Id = string.Empty;
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
