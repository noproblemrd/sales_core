﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class UpsaleLoseReason : DalBase<DataModel.Xrm.new_upsalelosereason>, ITableDal
    {
        public UpsaleLoseReason(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_upsalelosereason");
        }

        public override NoProblem.Core.DataModel.Xrm.new_upsalelosereason Retrieve(Guid id)
        {
            return XrmDataContext.new_upsalelosereasons.FirstOrDefault(x => x.new_upsalelosereasonid == id);
        }

        public List<DataModel.Tables.TableRowData> GetAll()
        {
            List<DataModel.Tables.TableRowData> retVal = new List<NoProblem.Core.DataModel.Tables.TableRowData>();
            string query =
                @"SELECT new_upsalelosereasonid,new_name,new_id,new_disabled FROM new_upsalelosereason with (nolock) WHERE deletionstatecode = 0";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Tables.TableRowData data = new NoProblem.Core.DataModel.Tables.TableRowData();
                            data.Name = (string)reader["new_name"];
                            data.Inactive = reader["new_disabled"] == DBNull.Value ? false : (bool)reader["new_disabled"];
                            data.Guid = (Guid)reader["new_upsalelosereasonid"];
                            data.Id = (string)reader["new_id"];
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
