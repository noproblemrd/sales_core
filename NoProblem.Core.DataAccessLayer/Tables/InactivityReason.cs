﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class InactivityReason : DalBase<DataModel.Xrm.new_inactivityreason>, ITableDal
    {
        public InactivityReason(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_inactivityreason");
        }

        private const string cachePrefix = "NoProblem.Core.DataAccessLayer.InactivityReason";

        public override NoProblem.Core.DataModel.Xrm.new_inactivityreason Retrieve(Guid id)
        {
            return XrmDataContext.new_inactivityreasons.FirstOrDefault(x => x.new_inactivityreasonid == id);
        }

        public List<DataModel.Tables.TableRowData> GetAll()
        {
            string query =
                @"select new_inactivityreasonid, new_name, new_id, new_disabled
                from new_inactivityreason with(nolock)
                where deletionstatecode = 0";
            List<DataModel.Tables.TableRowData> retVal = new List<NoProblem.Core.DataModel.Tables.TableRowData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Tables.TableRowData data = new NoProblem.Core.DataModel.Tables.TableRowData();
                            data.Guid = (Guid)reader["new_inactivityreasonid"];
                            data.Name = (string)reader["new_name"];
                            data.Id = (string)reader["new_id"];
                            data.Inactive = reader["new_disabled"] == DBNull.Value ? false : (bool)reader["new_disabled"];
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetRequestedByApiReasonId()
        {
            string query =
                 @"select top 1 new_inactivityreasonid
                from new_inactivityreason with(nolock)
                where deletionstatecode = 0
                and new_isrequestedbyapireason = 1";
            object scalar = ExecuteScalar(query);
            if (scalar == null)
            {
                throw new Exception("'Requested by API' inactivity reason is not configured in the system. Please configure it.");
            }
            return (Guid)scalar;
        }

        public Guid GetReasonIdForInactiveWithMoney()
        {
            string idStr = CacheManager.Instance.Get<string>(cachePrefix, "GetReasonIdForInactiveWithMoney", GetReasonIdForInactiveWithMoneyMethod, 1440);
            return new Guid(idStr);
        }

        private string GetReasonIdForInactiveWithMoneyMethod()
        {
            object scalar = ExecuteScalar(queryForGetReasonIdForInactiveWithMoney);
            if (scalar == null)
            {
                return Guid.Empty.ToString();
            }
            return scalar.ToString();
        }

        private const string queryForGetReasonIdForInactiveWithMoney =
            @"select top 1 new_inactivityreasonid
                from new_inactivityreason with(nolock)
                where deletionstatecode = 0
                and new_isreasonforinactivewithmoney = 1";
    }
}
