﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class UnavailabilityReasonGroup : DalBase<DataModel.Xrm.new_unavailabilityreasongroup>
    {
        public UnavailabilityReasonGroup(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_unavailabilityreasongroup");
        }

        public override NoProblem.Core.DataModel.Xrm.new_unavailabilityreasongroup Retrieve(Guid id)
        {
            return XrmDataContext.new_unavailabilityreasongroups.FirstOrDefault(x => x.new_unavailabilityreasongroupid == id);
        }

        public Guid GetIdByCode(int code)
        {
            Guid retVal = Guid.Empty;
            var group = XrmDataContext.new_unavailabilityreasongroups.FirstOrDefault(x => x.new_code == code);
            if (group != null)
            {
                retVal = group.new_unavailabilityreasongroupid;
            }
            return retVal;
        }
    }
}
