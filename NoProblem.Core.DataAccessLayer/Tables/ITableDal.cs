﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Tables;

namespace NoProblem.Core.DataAccessLayer
{
    public interface ITableDal
    {
        List<TableRowData> GetAll();
    }
}
