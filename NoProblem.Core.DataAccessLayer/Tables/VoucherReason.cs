﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class VoucherReason : DalBase<DataModel.Xrm.new_voucherreason>, ITableDal
    {
        public VoucherReason(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_voucherreason");
        }
        
        public override NoProblem.Core.DataModel.Xrm.new_voucherreason Retrieve(Guid id)
        {
            return XrmDataContext.new_voucherreasons.FirstOrDefault(x => x.new_voucherreasonid == id);
        }

        public List<NoProblem.Core.DataModel.Tables.TableRowData> GetAll()
        {
            List<DataModel.Tables.TableRowData> retVal = new List<NoProblem.Core.DataModel.Tables.TableRowData>();
            string query =
                @"SELECT new_voucherreasonid, new_name, new_disabled, new_id FROM new_voucherreason with (nolock) WHERE deletionstatecode = 0";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Tables.TableRowData data = new NoProblem.Core.DataModel.Tables.TableRowData();
                            data.Name = (string)reader["new_name"];
                            data.Inactive = reader["new_disabled"] != DBNull.Value ? (bool)reader["new_disabled"] : false;
                            data.Guid = (Guid)reader["new_voucherreasonid"];
                            data.Id = (string)reader["new_id"];
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
