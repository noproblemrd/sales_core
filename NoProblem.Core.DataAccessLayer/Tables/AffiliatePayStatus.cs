﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.DataAccessLayer
{
    public class AffiliatePayStatus : DalBase<DataModel.Xrm.new_affiliatepaystatus>
    {
        public AffiliatePayStatus(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_affiliatepaystatus");
        }

        public override NoProblem.Core.DataModel.Xrm.new_affiliatepaystatus Retrieve(Guid id)
        {
            return XrmDataContext.new_affiliatepaystatuses.FirstOrDefault(x => x.new_affiliatepaystatusid == id);
        }

        public List<List<GuidStringPair>> GetAll()
        {
            string query =
                @"select ps.new_name as psName, ps.new_affiliatepaystatusid as psId, sr.new_name as srName, sr.new_affiliatepaystatusreasonid as srId
                from new_affiliatepaystatus ps with(nolock)
                join new_affiliatepaystatusreason sr with(nolock) on sr.new_affiliatepaystatusid = ps.new_affiliatepaystatusid
                where ps.deletionstatecode = 0 and sr.deletionstatecode = 0";
            List<List<GuidStringPair>> retVal = new List<List<GuidStringPair>>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string psName = (string)reader["psName"];
                            string srName = (string)reader["srName"];
                            Guid psId = (Guid)reader["psId"];
                            Guid srId = (Guid)reader["srId"];
                            bool found = false;
                            foreach (var item in retVal)
                            {
                                if (item.ElementAt(0).Name == psName)
                                {
                                    item.Add(new GuidStringPair(srName, srId));
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                List<GuidStringPair> newList = new List<GuidStringPair>();
                                newList.Add(new GuidStringPair(psName, psId));
                                newList.Add(new GuidStringPair(srName, srId));
                                retVal.Add(newList);
                            }
                        }
                    }
                }
            }
            return retVal;
        }

        public Guid GetToPayStatusId()
        {
            return GetPayStatusId(true);
        }

        public Guid GetNotToPayStatusId()
        {
            return GetPayStatusId(false);
        }

        private Guid GetPayStatusId(bool toPay)
        {
            string query;
            if (toPay)
            {
                query = queryForGetToPay;
            }
            else
            {
                query = queryForGetNotToPay;
            }
            object scalar = ExecuteScalar(query);
            Guid retVal = Guid.Empty;
            if (scalar != null)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        private const string queryForGetToPay = "select new_affiliatepaystatusid from new_affiliatepaystatus where new_istopay = 1";
        private const string queryForGetNotToPay = "select new_affiliatepaystatusid from new_affiliatepaystatus where isnull(new_istopay, 0) = 0";
    }
}
