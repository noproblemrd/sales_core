﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class Channel : DalBase<DataModel.Xrm.new_channel>
    {
        #region Ctor
        public Channel(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_channel");
        } 
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_channel Retrieve(Guid id)
        {
            DataModel.Xrm.new_channel channel = XrmDataContext.new_channels.FirstOrDefault(x => x.new_channelid == id);
            return channel;
        }

        public DataModel.Xrm.new_channel GetChannelByCode(DataModel.Xrm.new_channel.ChannelCode code)
        {
            DataModel.Xrm.new_channel channel = XrmDataContext.new_channels.FirstOrDefault(x => x.new_code.Value == (int)code);
            return channel;
        }

        public Guid GetChannelIdByCode(DataModel.Xrm.new_channel.ChannelCode code)
        {
            var retVal = (from c in XrmDataContext.new_channels
                          where
                          c.new_code.Value == (int)code
                          select c.new_channelid).FirstOrDefault();
            return retVal;
        }
    }
}
