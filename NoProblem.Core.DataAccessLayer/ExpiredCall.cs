﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class ExpiredCall : DalBase<DataModel.Xrm.new_expiredcall>
    {
        public ExpiredCall(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_expiredcall");
        }

        public override NoProblem.Core.DataModel.Xrm.new_expiredcall Retrieve(Guid id)
        {
            return XrmDataContext.new_expiredcalls.FirstOrDefault(x => x.new_expiredcallid == id);
        }
    }
}
