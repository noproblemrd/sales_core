﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class LeadCreditCardCharge : DalBase<DataModel.Xrm.new_leadcreditcardcharge>
    {
        public LeadCreditCardCharge(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_leadcreditcardcharge");
        }

        public override IQueryable<DataModel.Xrm.new_leadcreditcardcharge> All
        {
            get
            {
                return XrmDataContext.new_leadcreditcardcharges;
            }
        }

        public override DataModel.Xrm.new_leadcreditcardcharge Retrieve(Guid id)
        {
            return XrmDataContext.new_leadcreditcardcharges.FirstOrDefault(x => x.new_leadcreditcardchargeid == id);
        }
    }
}
