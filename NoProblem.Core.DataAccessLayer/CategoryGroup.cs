﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.Expertise;

namespace NoProblem.Core.DataAccessLayer
{
    public class CategoryGroup : DalBase<DataModel.Xrm.new_categorygroup>
    {
        public CategoryGroup(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_categorygroup");
        }

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public override NoProblem.Core.DataModel.Xrm.new_categorygroup Retrieve(Guid id)
        {
            return XrmDataContext.new_categorygroups.FirstOrDefault(x => x.new_categorygroupid == id);
        }

        public HashSet<CategoryGroupContainer> GetAllCategoryGroups()
        {
            HashSet<CategoryGroupContainer> retVal = new HashSet<CategoryGroupContainer>();
            string query =
                @"select x.new_primaryexpertiseid, pe.new_name as HeadingName, x.new_categorygroupid, cg.new_name as GroupName
from new_primaryexpertise_categorygroup x with(nolock)
join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = x.new_primaryexpertiseid
join new_categorygroup cg with(nolock) on cg.new_categorygroupid = x.new_categorygroupid
where
pe.deletionstatecode = 0
and cg.deletionstatecode = 0
order by GroupName";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Guid groupId = (Guid)reader["new_categorygroupid"];
                            string headName = (string)reader["HeadingName"];
                            Guid headId = (Guid)reader["new_primaryexpertiseid"];
                            var existing = retVal.FirstOrDefault(x => x.CategoryGroupId == groupId);
                            if (existing == null)
                            {
                                CategoryGroupContainer container = new CategoryGroupContainer();
                                container.CategoryGroupId = groupId;
                                container.Name = (string)reader["GroupName"];
                                container.Headings = new List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>();
                                container.Headings.Add(new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair(headName, headId));
                                retVal.Add(container);
                            }
                            else
                            {
                                existing.Headings.Add(new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair(headName, headId));
                            }
                        }
                    }
                }
            }
            return retVal;
        }

        public List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair> GetAllCategories()
        {
            List<DataModel.PublisherPortal.GuidStringPair> retVal = new List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>();
            string query =
                @"select distinct cg.new_categorygroupid, cg.new_name
                from new_categorygroup cg with(nolock)
                join new_primaryexpertise_categorygroup x with(nolock) on x.new_categorygroupid = cg.new_categorygroupid
                where cg.deletionstatecode = 0 and isnull(new_display, 1) = 1
                order by cg.new_name";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add(new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair()
                            {
                                Id = (Guid)reader["new_categorygroupid"],
                                Name = (string)reader["new_name"]
                            });
                        }
                    }
                }
            }
            return retVal;
        }

        public List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair> GetCategoryGroup(Guid categoryId)
        {
            return CacheManager.Instance.Get<List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>>(cachePrefix, categoryId.ToString(), GetCategoryGroupByIdMethod);
        }

        private List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair> GetCategoryGroupByIdMethod(string categoryId)
        {
            List<DataModel.PublisherPortal.GuidStringPair> retVal = new List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>();
            string query =
                @"select x.new_primaryexpertiseid, pe.new_name
                from new_primaryexpertise_categorygroup x with(nolock)
                join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = x.new_primaryexpertiseid
                join new_categorygroup cg with(nolock) on cg.new_categorygroupid = x.new_categorygroupid
                where
                pe.deletionstatecode = 0
                and cg.deletionstatecode = 0
                and cg.new_categorygroupid = @id
                order by pe.new_name";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", categoryId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add(new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair()
                            {
                                Id = (Guid)reader["new_primaryexpertiseid"],
                                Name = (string)reader["new_name"]
                            });
                        }
                    }
                }
            }
            return retVal;
        }

        public List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair> GetCategoryGroup(string categoryName)
        {
            return CacheManager.Instance.Get<List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>>(cachePrefix, categoryName, GetCategoryGroupByNameMethod);
        }

        private List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair> GetCategoryGroupByNameMethod(string categoryName)
        {
            List<DataModel.PublisherPortal.GuidStringPair> retVal = new List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>();
            string query =
                @"select x.new_primaryexpertiseid, pe.new_name
                from new_primaryexpertise_categorygroup x with(nolock)
                join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = x.new_primaryexpertiseid
                join new_categorygroup cg with(nolock) on cg.new_categorygroupid = x.new_categorygroupid
                where
                pe.deletionstatecode = 0
                and cg.deletionstatecode = 0
                and cg.new_name = @name
                order by pe.new_name";
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_categorygroup>.SqlParametersList();
            parameters.Add("@name", categoryName);
            var reader = ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                retVal.Add(new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair()
                {
                    Id = (Guid)row["new_primaryexpertiseid"],
                    Name = (string)row["new_name"]
                });
            }
            return retVal;
        }

        public List<ExpertiseInSpecialCategoryContainer> GetSpecialCategoryGroup(string connectivityName)
        {
            return CacheManager.Instance.Get<List<ExpertiseInSpecialCategoryContainer>>(cachePrefix, connectivityName, GetSpecialCategoryGroupMethod, CacheManager.ONE_DAY);
        }

        private List<ExpertiseInSpecialCategoryContainer> GetSpecialCategoryGroupMethod(string connectivityName)
        {
            List<ExpertiseInSpecialCategoryContainer> retVal = new List<ExpertiseInSpecialCategoryContainer>();
            string query =
                @"select pe.new_name, pe.new_code, pe.new_displayinslider, pe.new_primaryexpertiseid
                from new_primaryexpertise_categorygroup x with(nolock)
                join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = x.new_primaryexpertiseid
                join new_categorygroup cg with(nolock) on cg.new_categorygroupid = x.new_categorygroupid
                where
                pe.deletionstatecode = 0
                and cg.deletionstatecode = 0
                and cg.new_connectivityname = @name
                order by pe.new_name";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@name", connectivityName));
            var reader = ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                ExpertiseInSpecialCategoryContainer container = new ExpertiseInSpecialCategoryContainer();
                container.Code = Convert.ToString(row["new_code"]);
                container.Name = Convert.ToString(row["new_name"]);
                container.DisplayInSlider = row["new_displayinslider"] != DBNull.Value ? (bool)row["new_displayinslider"] : false;
                container.HeadingId = (Guid)row["new_primaryexpertiseid"];
                retVal.Add(container);
            }
            return retVal;
        }

        public HashSet<DataModel.SliderData.SpecialCategoryGroup> GetAllSpecialCategoryGroups()
        {
            return CacheManager.Instance.Get<HashSet<DataModel.SliderData.SpecialCategoryGroup>>(cachePrefix, "GetAllSpecialCategoryGroups", GetAllSpecialCategroyGroupsMethod, 240);
        }

        private HashSet<DataModel.SliderData.SpecialCategoryGroup> GetAllSpecialCategroyGroupsMethod()
        {
            string query =
                @"select pe.new_primaryexpertiseid, new_connectivityname
                from new_primaryexpertise_categorygroup x with(nolock)
                join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = x.new_primaryexpertiseid
                join new_categorygroup cg with(nolock) on cg.new_categorygroupid = x.new_categorygroupid
                where
                pe.deletionstatecode = 0
                and cg.deletionstatecode = 0
                and cg.new_connectivityname is not null
                order by new_connectivityname";

            HashSet<DataModel.SliderData.SpecialCategoryGroup> retVal = new HashSet<NoProblem.Core.DataModel.SliderData.SpecialCategoryGroup>();

            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                DataModel.SliderData.SpecialCategoryGroup data = new NoProblem.Core.DataModel.SliderData.SpecialCategoryGroup();
                data.ConnectivityName = Convert.ToString(row["new_connectivityname"]);
                if (retVal.Add(data))
                {
                    data.HeadingIds = new List<Guid>();
                    data.HeadingIds.Add((Guid)row["new_primaryexpertiseid"]);
                }
                else
                {
                    var existingData = retVal.First(x => x.ConnectivityName == data.ConnectivityName);
                    existingData.HeadingIds.Add((Guid)row["new_primaryexpertiseid"]);
                }
            }

            return retVal;
        }

        public HashSet<DataModel.SliderData.CategoryGroupForDisplay> GetCategoryGroupsForDisplay()
        {
            return CacheManager.Instance.Get<HashSet<DataModel.SliderData.CategoryGroupForDisplay>>(cachePrefix, "GetCategoryGroupsForDisplay", GetCategoryGroupsForDisplayMethod, 240);
        }

        private HashSet<DataModel.SliderData.CategoryGroupForDisplay> GetCategoryGroupsForDisplayMethod()
        {
            string query =
                @"select pe.new_code as HeadingCode, cg.new_name as GroupName
                from new_primaryexpertise_categorygroup x with(nolock)
                join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = x.new_primaryexpertiseid
                join new_categorygroup cg with(nolock) on cg.new_categorygroupid = x.new_categorygroupid
                where
                pe.deletionstatecode = 0
                and cg.deletionstatecode = 0
                and isnull(cg.new_display, 1) = 1
                order by GroupName";

            HashSet<DataModel.SliderData.CategoryGroupForDisplay> retVal = new HashSet<NoProblem.Core.DataModel.SliderData.CategoryGroupForDisplay>();

            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                DataModel.SliderData.CategoryGroupForDisplay data = new NoProblem.Core.DataModel.SliderData.CategoryGroupForDisplay();
                data.Name = Convert.ToString(row["GroupName"]);
                if (retVal.Add(data))
                {
                    data.HeadingCodes = new List<string>();
                    data.HeadingCodes.Add(Convert.ToString(row["HeadingCode"]));
                }
                else
                {
                    var existingData = retVal.First(x => x.Name == data.Name);
                    existingData.HeadingCodes.Add(Convert.ToString(row["HeadingCode"]));
                }
            }
            return retVal;
        }
    }
}
