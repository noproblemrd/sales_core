﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;


namespace NoProblem.Core.DataAccessLayer
{
    public class deskappprivatecallrecord : DalBase<DML.Xrm.new_deskappprivatecallrecord>
    {
         #region Ctor
        public deskappprivatecallrecord(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_deskappprivatecallrecord");
        }
        #endregion

        public override DML.Xrm.new_deskappprivatecallrecord Retrieve(Guid id)
        {
            DML.Xrm.new_deskappprivatecallrecord deskappprivatecallrecord = XrmDataContext.new_deskappprivatecallrecords.FirstOrDefault(x => x.new_deskappprivatecallrecordid == id);
            return deskappprivatecallrecord;
        }
        public override IQueryable<DML.Xrm.new_deskappprivatecallrecord> All
        {
            get
            {
                return XrmDataContext.new_deskappprivatecallrecords;
            }
        }
    }
}
