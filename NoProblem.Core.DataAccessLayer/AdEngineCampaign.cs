﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class AdEngineCampaign : DalBase<DataModel.Xrm.new_adenginecampaign>
    {
        private const string CACHE_PREFIX = "dal_AdEngineCampaign";

        public AdEngineCampaign(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_adenginecampaign");
        }

        public override DataModel.Xrm.new_adenginecampaign Retrieve(Guid id)
        {
            return XrmDataContext.new_adenginecampaigns.FirstOrDefault(x => x.new_adenginecampaignid == id);
        }

        public override IQueryable<DataModel.Xrm.new_adenginecampaign> All
        {
            get
            {
                return XrmDataContext.new_adenginecampaigns;
            }
        }

        public List<DataModel.AdEngine.AdEngineCampaignData> GetAll()
        {
            return CacheManager.Instance.Get<List<DataModel.AdEngine.AdEngineCampaignData>>(CACHE_PREFIX, "GetAll", GetAllActually, CacheManager.ONE_DAY);
        }

        private List<DataModel.AdEngine.AdEngineCampaignData> GetAllActually()
        {
            List<DataModel.AdEngine.AdEngineCampaignData> retVal = new List<DataModel.AdEngine.AdEngineCampaignData>();
            foreach (var campaign in All.Select(x => new
            {               
                Name = x.new_name,
                URL = x.new_url,
                Id = x.new_adenginecampaignid,
                Height = x.new_height,
                Width = x.new_width,
                statuscode = x.statuscode
            }))
            {
                if (!campaign.statuscode.HasValue || campaign.statuscode.Value != 1)
                    continue;
                DataModel.AdEngine.AdEngineCampaignData data = new DataModel.AdEngine.AdEngineCampaignData();
                data.Id = campaign.Id;
                data.URL = campaign.URL;
                data.Name = campaign.Name;
                data.Width = campaign.Width.Value;
                data.Height = campaign.Height.Value;
                retVal.Add(data);
            }
            return retVal;
        }
    }
}
