﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class HomeSecurityCaseData : DalBase<DataModel.Xrm.new_homesecuritycasedata>
    {
        public HomeSecurityCaseData(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {

        }

        public override DataModel.Xrm.new_homesecuritycasedata Retrieve(Guid id)
        {
            return XrmDataContext.new_homesecuritycasedatas.FirstOrDefault(x => x.new_homesecuritycasedataid == id);
        }
    }
}
