﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class BlackListCheckPassed : DalBase<DataModel.Xrm.new_blacklistcheckpassed>
    {
        public BlackListCheckPassed(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_blacklistcheckpassed");
        }

        public override NoProblem.Core.DataModel.Xrm.new_blacklistcheckpassed Retrieve(Guid id)
        {
            return XrmDataContext.new_blacklistcheckpasseds.FirstOrDefault(x => x.new_blacklistcheckpassedid == id);
        }
    }
}
