﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NoProblem.Core.DataAccessLayer
{
    public class AsteriskConfiguration : DalBase<DataModel.Xrm.new_asteriskconfiguration>
    {
        #region Ctor
        public AsteriskConfiguration(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_asteriskconfiguration");
        } 
        #endregion

        #region Fields

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString(); 

        #endregion

        #region Public Methods

        public override NoProblem.Core.DataModel.Xrm.new_asteriskconfiguration Retrieve(Guid id)
        {
            return XrmDataContext.new_asteriskconfigurations.FirstOrDefault(x => x.new_asteriskconfigurationid == id);
        }

        public string GetConfigurationValue(string key)
        {
            return CacheManager.Instance.Get<string>(cachePrefix, key, GetConfigurationValueMethod);
        }

        #endregion

        #region Private Methods

        private string GetConfigurationValueMethod(string key)
        {
            string query =
                @"select new_value
                    from
                    new_asteriskconfiguration with (nolock)
                    where
                    new_key = @key and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@key", key));
            object scalar = ExecuteScalar(query, parameters);            
            string retVal = string.Empty;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (string)scalar;
            }
            return retVal;
        }

        #endregion
    }
}
