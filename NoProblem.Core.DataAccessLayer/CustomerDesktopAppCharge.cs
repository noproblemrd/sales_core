﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class CustomerDesktopAppCharge : DalBase<DataModel.Xrm.new_customerdesktopappcharge>
    {
        public CustomerDesktopAppCharge(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_customerdesktopappcharge");
        }

        public override DataModel.Xrm.new_customerdesktopappcharge Retrieve(Guid id)
        {
            return XrmDataContext.new_customerdesktopappcharges.FirstOrDefault(x => x.new_customerdesktopappchargeid == id);
        }

        public override IQueryable<DataModel.Xrm.new_customerdesktopappcharge> All
        {
            get
            {
                return XrmDataContext.new_customerdesktopappcharges;
            }
        }
    }
}
