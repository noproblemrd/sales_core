﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class PaymentComponent : DalBase<DataModel.Xrm.new_paymentcomponent>
    {
        public PaymentComponent(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public override NoProblem.Core.DataModel.Xrm.new_paymentcomponent Retrieve(Guid id)
        {
            return XrmDataContext.new_paymentcomponents.FirstOrDefault(x => x.new_paymentcomponentid == id);
        }

        public DateTime? GetLastPaymentDateOfMonthlyFee(Guid supplierId)
        {
            DateTime? retVal = null;
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_paymentcomponent>.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            object scalar = ExecuteScalar(isPayedThisMonthQuery, parameters);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (DateTime)scalar;
            }
            return retVal;
        }

        private const string isPayedThisMonthQuery =
            @"select top 1 pc.createdon
from new_paymentcomponent pc
join new_supplierpricing sp on sp.new_supplierpricingid = pc.new_supplierpricingid
where sp.new_accountid = @supplierId
and pc.new_type = 1
order by pc.createdon desc";

    }
}
