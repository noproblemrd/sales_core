﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class IncidentPrivateServiceProvider : DalBase<DML.Xrm.new_incidentprivateserviceprovider>
    {
        #region Ctor
        public IncidentPrivateServiceProvider(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_incidentprivateserviceprovider");
        }
        #endregion

        public void AddIncidentPrivateServiceProvider(Guid incidentId, Guid[] supplierIds)
        {
            foreach(Guid supplierId in supplierIds)
            {
                DML.Xrm.new_incidentprivateserviceprovider ipsp = new DML.Xrm.new_incidentprivateserviceprovider();
                ipsp.new_incidentid = incidentId;
                ipsp.new_accountid = supplierId;
                this.Create(ipsp);
            }
            this.XrmDataContext.SaveChanges();
        }
        public List<Guid> GetPrivateSuppliers(Guid incidentId)
        {
            List<Guid> listResult = new List<Guid>();
            string command = @"
SELECT New_accountId
FROM dbo.New_incidentprivateserviceproviderExtensionBase
WHERE New_incidentId = @incidentId";
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@incidentId", incidentId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while(reader.Read())
                        {
                            listResult.Add((Guid)reader["New_accountId"]);
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed GetPrivateSuppliers sql query - incidentId = {0}", incidentId);
                return null;
            }
            return listResult;
        }

        public override DML.Xrm.new_incidentprivateserviceprovider Retrieve(Guid id)
        {
            return XrmDataContext.new_incidentprivateserviceproviders.FirstOrDefault(x => x.new_incidentprivateserviceproviderid == id);
        }
    }
}
