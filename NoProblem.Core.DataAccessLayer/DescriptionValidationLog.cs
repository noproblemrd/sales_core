﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class DescriptionValidationLog : DalBase<DataModel.Xrm.new_descriptionvalidationlog>
    {
        public DescriptionValidationLog(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_descriptionvalidationlog");
        }

        public override NoProblem.Core.DataModel.Xrm.new_descriptionvalidationlog Retrieve(Guid id)
        {
            return XrmDataContext.new_descriptionvalidationlogs.FirstOrDefault(x => x.new_descriptionvalidationlogid == id);
        }
    }
}
