﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class WebSite : DalBase<DataModel.Xrm.new_website>
    {
        public WebSite(DataModel.Xrm.DataContext xrmDataContext):
            base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_website");
        }

        public override NoProblem.Core.DataModel.Xrm.new_website Retrieve(Guid id)
        {
            return XrmDataContext.new_websites.FirstOrDefault(x => x.new_websiteid == id);
        }

        public Guid GetWebSiteByName(string name, Guid originId)
        {
            string query =
                @"select new_websiteid from new_website with(nolock)
                    where new_name = @name and new_originid = @originId and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@name", name));
            parameters.Add(new KeyValuePair<string, object>("@originId", originId));
            object scalar = ExecuteScalar(query, parameters);
            Guid retVal = Guid.Empty;
            if (scalar != null)
            {
                retVal = (Guid)scalar;
            }
            return retVal;
        }

        public HashSet<NoProblem.Core.DataModel.PublisherPortal.OriginsWebSitesData> GetAllWebSites()
        {
            string query =
                @"select new_websiteid, new_name, new_originid, or.new_originidname
                from new_website with(nolock)
                where deletionstatecode = 0";
            HashSet<NoProblem.Core.DataModel.PublisherPortal.OriginsWebSitesData> retVal =
                new HashSet<NoProblem.Core.DataModel.PublisherPortal.OriginsWebSitesData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
