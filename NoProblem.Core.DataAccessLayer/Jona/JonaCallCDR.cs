﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class JonaCallCDR : DalBase<DataModel.Xrm.new_jonacallcdr>
    {
        public JonaCallCDR(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_jonacallcdr");
        }

        public override NoProblem.Core.DataModel.Xrm.new_jonacallcdr Retrieve(Guid id)
        {
            return XrmDataContext.new_jonacallcdrs.FirstOrDefault(x => x.new_jonacallcdrid == id);
        }

        public NoProblem.Core.DataModel.Xrm.new_jonacallcdr GetByAsteriskCallId(string callId)
        {
            return XrmDataContext.new_jonacallcdrs.FirstOrDefault(x => x.new_callid == callId);
        }
    }
}
