﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class JonaCall : DalBase<DataModel.Xrm.new_jonacall>
    {
        public JonaCall(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_jonacall");
        }

        public override NoProblem.Core.DataModel.Xrm.new_jonacall Retrieve(Guid id)
        {
            return XrmDataContext.new_jonacalls.FirstOrDefault(x => x.new_jonacallid == id);
        }

        public NoProblem.Core.DataModel.Xrm.new_jonacall GetCallByCallId(string asteriskCallId)
        {
            return XrmDataContext.new_jonacalls.First(x => x.new_callid == asteriskCallId);
        }

        public NoProblem.Core.DataModel.Xrm.new_jonacall GetCallByTtsCallId(string asteriskCallId)
        {
            return XrmDataContext.new_jonacalls.First(x => x.new_ttscallid == asteriskCallId);
        }
    }
}
