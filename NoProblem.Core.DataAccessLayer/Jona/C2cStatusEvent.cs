﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class C2cStatusEvent : DalBase<DataModel.Xrm.new_c2cstatusevent>
    {
        public C2cStatusEvent(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_c2cstatusevent");
        }

        public override NoProblem.Core.DataModel.Xrm.new_c2cstatusevent Retrieve(Guid id)
        {
            return XrmDataContext.new_c2cstatusevents.FirstOrDefault<DataModel.Xrm.new_c2cstatusevent>(x => x.new_c2cstatuseventid == id);
        }
    }
}
