﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class JonaManagement : DalBase<DataModel.Xrm.new_jonamanagement>
    {
        public JonaManagement(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_jonamanagement");
        }

        public override NoProblem.Core.DataModel.Xrm.new_jonamanagement Retrieve(Guid id)
        {
            return XrmDataContext.new_jonamanagements.FirstOrDefault(x => x.new_jonamanagementid == id);
        }
    }
}
