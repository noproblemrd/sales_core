﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class TtsStatusEvent : DalBase<DataModel.Xrm.new_ttsstatusevent>
    {
        public TtsStatusEvent(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_ttsstatusevent");
        }

        public override NoProblem.Core.DataModel.Xrm.new_ttsstatusevent Retrieve(Guid id)
        {
            return XrmDataContext.new_ttsstatusevents.FirstOrDefault<DataModel.Xrm.new_ttsstatusevent>(x => x.new_ttsstatuseventid == id);
        }
    }
}
