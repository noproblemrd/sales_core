﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.DataAccessLayer
{
    public class Exposure : DalBase<DataModel.Xrm.new_exposure>
    {
        #region Ctor
        public Exposure(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_exposure");
        }
        #endregion

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public override NoProblem.Core.DataModel.Xrm.new_exposure Retrieve(Guid id)
        {
            return XrmDataContext.new_exposures.FirstOrDefault(x => x.new_exposureid == id);
        }

        public List<DML.Reports.ConversionReportContainerPerDate> GetExposureCount(
            DateTime fromDate,
            DateTime toDate,
            Guid expertiseId,
            string controlName,
            string domain,
            Guid originId,
            out DateTime exposuresLastUpdate)
        {            
            List<DML.Reports.ConversionReportContainerPerDate> retVal = new List<DML.Reports.ConversionReportContainerPerDate>();
            SqlParametersList parameters = new DalBase<NoProblem.Core.DataModel.Xrm.new_exposure>.SqlParametersList();
            parameters.Add("@from", fromDate);
            parameters.Add("@to", toDate);
            StringBuilder builder = new StringBuilder();
            builder.Append(
                @"
                declare @lastUpdate datetime
                select @lastUpdate = max(lastupdate) from New_exposure_PerDay_ConversionReport

                select sum(Cnt) sum, new_date ,@lastUpdate as lastUpdate
                from New_exposure_PerDay_ConversionReport where 
                new_date between @from and @to "
                );
            if (expertiseId != Guid.Empty)
            {
                builder.Append(" and new_primaryexpertiseid = @headingId ");
                parameters.Add("@headingId", expertiseId);
            }
            if (!string.IsNullOrEmpty(controlName))
            {
                builder.Append(" and New_ControlName = @flavor ");
                parameters.Add("@flavor", controlName);
            }
            if (!string.IsNullOrEmpty(domain))
            {
                builder.Append(" and New_Domain = @domain ");
                parameters.Add("@domain", domain);
            }
            if (originId != Guid.Empty)
            {
                builder.Append(" and new_originid = @originId ");
                parameters.Add("@originId", originId);
            }
            builder.Append(" group by new_date");
            var reader = ExecuteReader(builder.ToString(), parameters);
            if (reader.Count > 0)
            {
                exposuresLastUpdate = (DateTime)reader.First()["lastUpdate"];
            }
            else
            {
                string lastUpdateQuery = "select max(lastupdate) from New_exposure_PerDay_ConversionReport";
                object scalar = ExecuteScalar(lastUpdateQuery);
                if (scalar != null && scalar != DBNull.Value)
                {
                    exposuresLastUpdate = (DateTime)scalar;
                }
                else
                {
                    exposuresLastUpdate = DateTime.MinValue.AddDays(1);
                }
            }
            foreach (var row in reader)
            {
                DML.Reports.ConversionReportContainerPerDate pair =
                    new DML.Reports.ConversionReportContainerPerDate(
                        (DateTime)row["new_date"], (int)row["sum"]
                       );
                retVal.Add(pair);
            }
            return retVal;
        }

        public List<string> GetExposuresPlaces()
        {
            string query =
                @"select distinct top 20 new_place
                from new_exposure with(nolock)
                where deletionstatecode = 0 and new_place is not null";
            List<string> retVal = new List<string>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader[0] != DBNull.Value)
                            {
                                retVal.Add((string)reader[0]);
                            }
                        }
                    }
                }
            }
            return retVal;
        }

        public List<string> GetExposuresControlNames()
        {
            return CacheManager.Instance.Get<List<string>>(cachePrefix, GetExposuresControlNames_cacheKey, GetExposuresControlNamesMethod, 480);
        }

        private const string GetExposuresControlNames_cacheKey = "GetExposuresControlNames";

        private List<string> GetExposuresControlNamesMethod()
        {
            string query = "EXEC [dbo].[GetFlavorsControls]";
                /*
                @"select distinct new_controlname from New_exposure_PerDay_ConversionReport
                    where new_controlname is not null and new_controlname != ''
                    order by new_controlname";
                 * */
            List<string> retVal = new List<string>();            
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                retVal.Add((string)row["Name"]);
            }
            return retVal;
        }

        public List<string> GetExposuresPageNames()
        {
            string query =
                @"select distinct top 20 new_pagename
                from new_exposure with(nolock)
                where deletionstatecode = 0 and new_pagename is not null";
            List<string> retVal = new List<string>();
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                retVal.Add((string)row["new_pagename"]);
            }
            return retVal;
        }

        public List<string> GetExposuresUrls()
        {
            string query =
                @"select distinct top 20 new_url
                from new_exposure with(nolock)
                where deletionstatecode = 0 and new_url is not null";
            List<string> retVal = new List<string>();
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                retVal.Add((string)row["new_url"]);
            }
            return retVal;
        }

        public int GetExposuresCountFromOriginFromDate(Guid originId, DateTime? fromDate)
        {
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            string query =
                @"select count(*)
                from new_exposure with(nolock) where deletionstatecode = 0
                and new_originid = @originId";
            if (fromDate.HasValue)
            {
                query += " and createdon > @fromDate ";
                parameters.Add(new KeyValuePair<string, object>("@fromDate", fromDate.Value));
            }
            parameters.Add(new KeyValuePair<string, object>("@originId", originId));
            int retVal = (int)ExecuteScalar(query, parameters);
            return retVal;
        }
    }
}
