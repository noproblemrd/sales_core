﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class SupplierInsertData : DalBase<DataModel.Xrm.new_supplierinsertdata>
    {
        public SupplierInsertData(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_supplierinsertdata");
        }

        public override NoProblem.Core.DataModel.Xrm.new_supplierinsertdata Retrieve(Guid id)
        {
            return XrmDataContext.new_supplierinsertdatas.FirstOrDefault(x => x.new_supplierinsertdataid == id);
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_supplierinsertdata> GetNewSuppliersToCreate()
        {
            return XrmDataContext.new_supplierinsertdatas.Where(x => x.statuscode == 1);
        }
    }
}
