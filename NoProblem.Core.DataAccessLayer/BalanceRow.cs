﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class BalanceRow : DalBase<DataModel.Xrm.new_balancerow>
    {
        #region Ctor
        public BalanceRow(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_balancerow");
        }
        #endregion

        #region Public Methods

        public override IQueryable<DML.Xrm.new_balancerow> All
        {
            get
            {
                return XrmDataContext.new_balancerows;
            }
        }

        public override NoProblem.Core.DataModel.Xrm.new_balancerow Retrieve(Guid id)
        {
            DML.Xrm.new_balancerow row = XrmDataContext.new_balancerows.FirstOrDefault(x => x.new_balancerowid == id);
            return row;
        }

        public IEnumerable<DML.Reports.BalanceRowData> GetBalanceReport(Guid supplierId, DateTime from, DateTime to)
        {
            string iaQuery =
                @"select new_amount,new_action,new_balanceafteraction,br.createdon
                from new_balancerow br with(nolock)
                join new_incidentaccount ia with(nolock) on ia.new_incidentaccountid = br.new_incidentaccountid
                where br.deletionstatecode = 0 and ia.deletionstatecode = 0
                and ia.new_accountid = @id
                and br.createdon between @from and @to
and
               (select count(*) from new_balancerow with(nolock)
				where
				new_incidentaccountid = ia.new_incidentaccountid
				and
				deletionstatecode = 0 and new_action = 4) = 0";
            //The last 'and' removes all rows of calls that are not charged because of interval.
            string spQuery =
                @"select new_amount,new_action,new_balanceafteraction,br.createdon
                from new_balancerow br with(nolock)
                join new_supplierpricing sp with(nolock) on sp.new_supplierpricingid = br.new_supplierpricingid
                where br.deletionstatecode = 0 and sp.deletionstatecode = 0
                and sp.new_accountid = @id
                and br.createdon between @from and @to";
            List<DML.Reports.BalanceRowData> iaList = new List<DML.Reports.BalanceRowData>();
            List<DML.Reports.BalanceRowData> spList = new List<DML.Reports.BalanceRowData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand iaCommand = new SqlCommand(iaQuery, conn))
                {
                    AddParametersToCommand(supplierId, from, to, iaCommand);
                    using (SqlDataReader reader = iaCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.Reports.BalanceRowData data = CreateDataEntry(reader);
                            iaList.Add(data);
                        }
                    }
                }
                using (SqlCommand spCommand = new SqlCommand(spQuery, conn))
                {
                    AddParametersToCommand(supplierId, from, to, spCommand);
                    using (SqlDataReader reader = spCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.Reports.BalanceRowData data = CreateDataEntry(reader);
                            spList.Add(data);
                        }
                    }
                }
            }
            List<DML.Reports.BalanceRowData> retVal = new List<NoProblem.Core.DataModel.Reports.BalanceRowData>(iaList);
            retVal.AddRange(spList);
            return retVal;
        }

        #endregion

        private void AddParametersToCommand(Guid supplierId, DateTime from, DateTime to, SqlCommand spCommand)
        {
            spCommand.Parameters.AddWithValue("@id", supplierId);
            spCommand.Parameters.AddWithValue("@from", from);
            spCommand.Parameters.AddWithValue("@to", to);
        }

        private DML.Reports.BalanceRowData CreateDataEntry(SqlDataReader reader)
        {
            DML.Reports.BalanceRowData data = new NoProblem.Core.DataModel.Reports.BalanceRowData();
            data.Action = reader["new_action"] != DBNull.Value ? (DML.Xrm.new_balancerow.Action)((int)reader["new_action"]) : DML.Xrm.new_balancerow.Action.Call;
            data.Amount = reader["new_amount"] != DBNull.Value ? (decimal)reader["new_amount"] : 0;
            data.Balance = reader["new_balanceafteraction"] != DBNull.Value ? (decimal)reader["new_balanceafteraction"] : 0;
            data.CreatedOn = (DateTime)reader["createdon"];
            return data;
        } 
      
    }
}
