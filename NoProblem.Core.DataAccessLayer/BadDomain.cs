﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.DataAccessLayer
{
    public class BadDomain : DalBase<DataModel.Xrm.new_baddomain>
    {
        #region Ctor
        public BadDomain(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_baddomain");
        } 
        #endregion

        #region Fields

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_baddomain Retrieve(Guid id)
        {
            return XrmDataContext.new_baddomains.FirstOrDefault(x => x.new_baddomainid == id);
        }

        public List<StringBoolPair> GetAllBadDomains()
        {
            return CacheManager.Instance.Get<List<StringBoolPair>>(cachePrefix, "GetAllBadDomains", GetAllBadDomainsMethod);
        }

        private List<StringBoolPair> GetAllBadDomainsMethod()
        {
            string query =
                @"select bd.New_name, bd.New_BlockInClient
                    from dbo.New_baddomainExtensionBase bd
	                    inner join dbo.New_baddomainBase b
	                    on b.New_baddomainId = bd.New_baddomainId
                    where b.DeletionStateCode = 0";
            List<StringBoolPair> retVal = new List<StringBoolPair>();
            var reader = ExecuteReader(query);
            foreach (var row in reader)
            {
                string data = (row["New_name"] == DBNull.Value) ? string.Empty : (string)row["New_name"];
                
                if (!string.IsNullOrEmpty(data))
                {
                    bool BlockInClient = (row["New_BlockInClient"] == DBNull.Value) ? false : (bool)row["New_BlockInClient"];
                    StringBoolPair sbp = new StringBoolPair(data, BlockInClient);
                    if (retVal.Contains(sbp))
                        continue;                    
                    retVal.Add(sbp);
                }
            }
            
            return retVal;
        }
    }
}
