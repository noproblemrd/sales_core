﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class CampaignActivity : DalBase<DataModel.Xrm.new_campaignactivity>
    {
        public CampaignActivity(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_campaignactivity");
        }

        public override NoProblem.Core.DataModel.Xrm.new_campaignactivity Retrieve(Guid id)
        {
            return XrmDataContext.new_campaignactivitynew_campaignactivities.FirstOrDefault(x => x.new_campaignactivityid == id);
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_campaignactivity> GetAllActivitiesFromCampaigns(IEnumerable<Guid> campaigns)
        {
            StringBuilder query = new StringBuilder(
                @"select new_campaignactivityid, new_interval
                from new_campaignactivity with(nolock)
                where deletionstatecode = 0
                and new_campaignid in 
                ( ");
            int count = campaigns.Count();
            for (int i = 0; i < count; i++)
            {
                query.Append("'").Append(campaigns.ElementAt(i)).Append("'");
                if (i < count - 1)
                {
                    query.Append(",");
                }
            }
            query.Append(" )");
            List<DataModel.Xrm.new_campaignactivity> retVal = new List<NoProblem.Core.DataModel.Xrm.new_campaignactivity>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query.ToString(), conn))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DataModel.Xrm.new_campaignactivity activity = new NoProblem.Core.DataModel.Xrm.new_campaignactivity();
                            activity.new_campaignactivityid = (Guid)reader["new_campaignactivityid"];
                            activity.new_interval = (int)reader["new_interval"];
                            retVal.Add(activity);
                        }
                    }
                }
            }
            return retVal;
        }


    }
}
