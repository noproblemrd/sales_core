﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class Campaign : DalBase<DataModel.Xrm.new_campaign>
    {
        public Campaign(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_campaign");
        }

        public override NoProblem.Core.DataModel.Xrm.new_campaign Retrieve(Guid id)
        {
            return XrmDataContext.new_campaigns.FirstOrDefault(x => x.new_campaignid == id);
        }

        public IEnumerable<Guid> GetActiveAfterTimeCampaigns(DateTime localTime)
        {
            List<Guid> retVal = new List<Guid>();
            string query =
                @"select new_campaignid
                from new_campaign with(nolock)
                where deletionstatecode = 0
                and statuscode = 1
                and new_time <= CONVERT(char(8),@time,108)
                and new_time >= CONVERT(char(8),@timeMinusHour,108)";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@time", localTime);
                    command.Parameters.AddWithValue("@timeMinusHour", localTime.AddMinutes(-30));
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add((Guid)reader[0]);
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
