﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class CampaignEvent : DalBase<DataModel.Xrm.new_campaignevent>
    {
        public CampaignEvent(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_campaignevent");
        }

        public override NoProblem.Core.DataModel.Xrm.new_campaignevent Retrieve(Guid id)
        {
            return XrmDataContext.new_campaignevents.FirstOrDefault(x => x.new_campaigneventid == id);
        }

        public IEnumerable<NoProblem.Core.DataModel.Xrm.new_campaignevent> GetAllOpenEvents(IEnumerable<DataModel.Xrm.new_campaignactivity> activities)
        {
            int openCode = (int)DataModel.Xrm.new_campaignevent.CampaignEventStatusCode.Open;
            var events = from eve in XrmDataContext.new_campaignevents
                where eve.statuscode == openCode
                select eve;
            List<DataModel.Xrm.new_campaignevent> retVal = new List<NoProblem.Core.DataModel.Xrm.new_campaignevent>();
            foreach (var eve in events)
            {
                foreach (var acti in activities)
                {
                    if (acti.new_campaignactivityid == eve.new_campaignactivityid.Value)
                    {
                        retVal.Add(eve);
                        break;
                    }
                }
            }
            return retVal;
        }

        public bool EventExists(Guid activityId, DateTime fromDate, DateTime toDate)
        {
            var existing = XrmDataContext.new_campaignevents.FirstOrDefault(
                x => x.createdon.Value >= fromDate
                    && x.createdon.Value <= toDate
                    && x.new_campaignactivityid.Value == activityId);

            return existing != null;
        }
    }
}
