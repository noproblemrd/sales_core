﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class MobileDevice : DalBase<DataModel.Xrm.new_mobiledevice>
    {
        public MobileDevice(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_mobiledevice");
        }

        public override IQueryable<DataModel.Xrm.new_mobiledevice> All
        {
            get
            {
                return XrmDataContext.new_mobiledevices;
            }
        }

        public override DataModel.Xrm.new_mobiledevice Retrieve(Guid id)
        {
            return XrmDataContext.new_mobiledevices.FirstOrDefault(x => x.new_mobiledeviceid == id);
        }
        public DataModel.Xrm.new_mobiledevice GetActiveMobileDeviceByAccount(Guid accountId)
        {
            IEnumerable<DataModel.Xrm.new_mobiledevice> devices = (from m in All
                                                                   where m.new_accountid == accountId
                                                                   && m.new_isauthorized == true
                                                                   && m.new_isactive == true
                                                                   select m
                                                      );
            if (devices.Count() == 0)
                return null;
            return devices.FirstOrDefault();
        }
        public DataModel.Xrm.new_mobiledevice GetActiveMobileDeviceByContact(Guid contactId)
        {
            IEnumerable<DataModel.Xrm.new_mobiledevice> devices = (from m in All
                                                                   where m.new_contactid == contactId
                                                                   && m.new_isauthorized == true
                                                                   && m.new_isactive == true
                                                                   select m
                                                      );
            if (devices.Count() == 0)
                return null;
            return devices.FirstOrDefault();
        }
    }
}
