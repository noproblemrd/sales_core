﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class Coordinate : DalBase<DataModel.Xrm.new_coordinate>
    {
        public Coordinate(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_coordinate");
        }

        public override DataModel.Xrm.new_coordinate Retrieve(Guid id)
        {
            return XrmDataContext.new_coordinates.FirstOrDefault(x => x.new_coordinateid == id);
        }

        public override IQueryable<DataModel.Xrm.new_coordinate> All
        {
            get
            {
                return XrmDataContext.new_coordinates;
            }
        }
    }
}
