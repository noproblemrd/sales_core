﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.SecondaryExpertise
//  File: SecondaryExpertise.cs
//  Description: DAL for SecondaryExpertise Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using DML = NoProblem.Core.DataModel;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class SecondaryExpertise : DalBase<DML.Xrm.new_secondaryexpertise>
    {
        #region Ctor
        public SecondaryExpertise(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_secondaryexpertise");
        } 
        #endregion

        public override NoProblem.Core.DataModel.Xrm.new_secondaryexpertise Retrieve(Guid id)
        {
            DML.Xrm.new_secondaryexpertise secondaryExpertise = XrmDataContext.new_secondaryexpertises.FirstOrDefault(x => x.new_secondaryexpertiseid == id);
            return secondaryExpertise;
        }

        public DML.Xrm.new_secondaryexpertise GetSecondaryExpertiseByCode(string code)
        {
            DML.Xrm.new_secondaryexpertise secondaryExpertise = XrmDataContext.new_secondaryexpertises.FirstOrDefault(x => x.new_code == code);
            return secondaryExpertise;
        }

        public System.Collections.Generic.IEnumerable<NoProblem.Core.DataModel.Xrm.new_secondaryexpertise> GetSecondaryExpertiseByPrimary(Guid primaryExpertiseId)
        {
            IEnumerable<DML.Xrm.new_secondaryexpertise> retVal = XrmDataContext.new_secondaryexpertises.Where(
                x => x.new_primaryexpertiseid.HasValue
                    &&
                    x.new_primaryexpertiseid.Value == primaryExpertiseId);
            return retVal;
        }

        public DataModel.Xrm.new_channel.ChannelCode GetChannelCodeFromSecondaryExpertise(string HeadingCode)
        {
            string query = string.Format(
                @"select ch.new_code from 
                    new_channel ch with (nolock)
                    join new_primaryexpertise pe with (nolock) on ch.new_channelid = pe.new_channelid
                    join new_secondaryexpertise se with (nolock) on se.new_primaryexpertiseid = pe.new_primaryexpertiseid
                    where se.new_code = N'{0}' and se.deletionstatecode = 0 and pe.deletionstatecode = 0 and ch.deletionstatecode = 0", HeadingCode);
            object code;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    code = command.ExecuteScalar();
                }
            }
            DataModel.Xrm.new_channel.ChannelCode retVal = NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Call;
            if (code != null)
            {
                retVal = (DataModel.Xrm.new_channel.ChannelCode)code;
            }
            return retVal;
        }

        public NoProblem.Core.DataModel.SqlHelper.SecondaryExpertiseMinData GetSecondaryMinDataByCode(string code)
        {
            string query = @"select new_secondaryexpertiseid, new_primaryexpertiseid from
                        new_secondaryexpertise with (nolock) where new_code = N'" + code + "' and deletionstatecode = 0";
            DML.SqlHelper.SecondaryExpertiseMinData data = new NoProblem.Core.DataModel.SqlHelper.SecondaryExpertiseMinData(); ;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            data.Id = (Guid)reader["new_secondaryexpertiseid"];
                            data.PrimaryExpertiseId = (Guid)reader["new_primaryexpertiseid"];
                        }
                    }
                }
            }
            return data;
        }

        public string GetSecondaryExpertiseNameByCode(string code)
        {
            string retVal = string.Empty;
            string query = @"select new_name from new_secondaryexpertise with (nolock) where new_code = @code and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@code", code));
            object nameObj = ExecuteScalar(query, parameters);
            if (nameObj != null && nameObj != DBNull.Value)
            {
                retVal = (string)nameObj;
            }
            return retVal;
        }

        public bool IsAuction(string headingCode)
        {
            string query = @"select new_isauction from new_primaryexpertise pe with(nolock)
            join new_secondaryexpertise se with(nolock) on pe.new_primaryexpertiseid = se.new_primaryexpertiseid
             where se.new_code = @code and pe.deletionstatecode = 0 and se.deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameter = new List<KeyValuePair<string, object>>();
            parameter.Add(new KeyValuePair<string, object>("@code", headingCode));
            bool retVal = true;
            object scalar = ExecuteScalar(query, parameter);
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (bool)scalar;
            }
            return retVal;
        }

        public bool IsExists(string code)
        {
            string query = @"select count(*) 
                from new_secondaryexpertise with (nolock) 
                where new_code = @code
                and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@code", code));
            int count = (int)ExecuteScalar(query, parameters);
            return count > 0;
        }
    }
}
