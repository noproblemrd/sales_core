﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.PricingMethod
//  File: PricingMethod.cs
//  Description: DAL for PricingMethod Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using DML = NoProblem.Core.DataModel;
using System.IO;
using System.Xml;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class PricingMethod : DalBase<DML.Xrm.new_pricingmethod>
    {
        #region Ctor
        public PricingMethod(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_pricingmethod");
        } 
        #endregion

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public override NoProblem.Core.DataModel.Xrm.new_pricingmethod Retrieve(Guid id)
        {
            DML.Xrm.new_pricingmethod method = XrmDataContext.new_pricingmethods.FirstOrDefault(x => x.new_pricingmethodid == id);
            return method;
        }

        public DML.Xrm.new_pricingmethod GetDefaultPricingMethod()
        {
            return DataAccessLayer.CacheManager.Instance.Get<DML.Xrm.new_pricingmethod>(cachePrefix, "GetDefaultPricingMethod", GetDefaultPricingMethodPrivate, CacheManager.ONE_WEEK);
        }

        private DML.Xrm.new_pricingmethod GetDefaultPricingMethodPrivate()
        {
            DML.Xrm.new_pricingmethod method = XrmDataContext.new_pricingmethods.SingleOrDefault(x => x.new_isdefaultpricingmethod == true);
            if (method == null)
            {
                throw new Exception("No default pricing method has been declared in the system.");
            }
            return method;
        }

        public string GetPricingMethods()
        {
            string query = "SELECT new_name,new_pricingmethodid FROM new_pricingmethod with (nolock) WHERE deletionstatecode = 0 AND statecode = 0 AND new_isdisplayed = 1";
            StringWriter xmlText = new StringWriter();
            XmlTextWriter xmlConstructor = new XmlTextWriter(xmlText);
            xmlConstructor.WriteStartElement("PricingMethods");
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader getMethods = command.ExecuteReader())
                    {
                        while (getMethods.Read())
                        {
                            xmlConstructor.WriteStartElement("PricingMethod");
                            xmlConstructor.WriteElementString("Name", getMethods["new_name"].ToString());
                            xmlConstructor.WriteElementString("ID", getMethods["new_pricingmethodid"].ToString());
                            xmlConstructor.WriteEndElement();
                        }
                    }
                }
            }
            xmlConstructor.WriteEndElement(); // SupplierPackages
            return xmlText.ToString();
        }
    }
}
