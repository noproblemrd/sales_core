﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class IvrType : DalBase<DataModel.Xrm.new_ivrtype>
    {
        public IvrType(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_ivrtype");
        }

        public override NoProblem.Core.DataModel.Xrm.new_ivrtype Retrieve(Guid id)
        {
            return XrmDataContext.new_ivrtypes.FirstOrDefault(x => x.new_ivrtypeid == id);
        }
    }
}
