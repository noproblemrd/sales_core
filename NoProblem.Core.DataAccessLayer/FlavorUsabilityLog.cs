﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.DataAccessLayer
{
    public class FlavorUsabilityLog : DalBase<DataModel.Xrm.new_flavorusabilitylog>
    {
        public FlavorUsabilityLog(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache("new_flavorusabilitylog");
        }

        public override NoProblem.Core.DataModel.Xrm.new_flavorusabilitylog Retrieve(Guid id)
        {
            return XrmDataContext.new_flavorusabilitylogs.FirstOrDefault(x => x.new_flavorusabilitylogid == id);
        }
    }
}
