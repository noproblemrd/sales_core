﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: NoProblem.Core.DataAccessLayer.Contact
//  File: Contact.cs
//  Description: DAL for Contact Entity
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using System.Collections.Generic;
using NoProblem.Core.DataAccessLayer.Autonumber;
using DML = NoProblem.Core.DataModel;
using System.Data.SqlClient;

namespace NoProblem.Core.DataAccessLayer
{
    public class Contact : DalBase<DML.Xrm.contact>
    {
        private const string tableName = "contact";

        #region Ctor
        public Contact(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataModel.XrmDataContext.ClearCache(tableName);
        }
        #endregion

        public override NoProblem.Core.DataModel.Xrm.contact Retrieve(Guid id)
        {
            DML.Xrm.contact contact = XrmDataContext.contacts.FirstOrDefault(x => x.contactid == id);
            return contact;
        }

        public override IQueryable<DML.Xrm.contact> All
        {
            get
            {
                return XrmDataContext.contacts;
            }
        }

        public Guid SearchContactByOneOfHisPhones(string phone)
        {
            Guid retVal = Guid.Empty;
            string query = "SELECT contactid FROM contact with(nolock) WHERE deletionstatecode = 0 AND (mobilephone = '{0}' OR telephone1 = '{0}' or telephone2 = '{0}')";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(string.Format(query, phone), connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader contactid = command.ExecuteReader())
                    {
                        if (contactid.Read())
                            retVal = (Guid)contactid["contactid"];
                    }
                }
            }
            return retVal;
        }

        public int GetNumberOfIncidentByContact(Guid contactId)
        {
            string query = @"select count(incidentid) from incident with(nolock)
                            where customerid = @contactId and deletionstatecode = 0";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@contactId", contactId));
            int retVal = (int)ExecuteScalar(query, parameters);
            return retVal;
        }

        public List<DML.Consumer.Containers.ConsumerData> FindConsumersFast(string input, Guid consumerId)
        {
            string query;
            if (consumerId != Guid.Empty)
            {
                query =
                    @"select contactid, firstname, lastname, mobilephone from
                    contactbase with(nolock)
                    where
                    deletionstatecode = 0
                    and 
                    (
                        contactid = @id
                    )";
            }
            else
            {
                query =
                    @"select top 100 contactid, firstname, lastname, mobilephone from
                    contactbase with(nolock)
                    where
                    deletionstatecode = 0
                    and 
                    (
                        firstname like @input or lastname like @input or mobilephone like @input
                    )";
            }
            List<DML.Consumer.Containers.ConsumerData> dataList = new List<NoProblem.Core.DataModel.Consumer.Containers.ConsumerData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    if (consumerId == Guid.Empty)
                    {
                        command.Parameters.AddWithValue("@input", "%" + input + "%");
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@id", consumerId);
                    }
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.Consumer.Containers.ConsumerData data = new NoProblem.Core.DataModel.Consumer.Containers.ConsumerData();
                            data.Id = (Guid)reader[0];
                            object firstObj = reader[1];
                            if (firstObj != null && firstObj != DBNull.Value)
                            {
                                data.FirstName = (string)firstObj;
                            }
                            object lastObj = reader[2];
                            if (lastObj != null && lastObj != DBNull.Value)
                            {
                                data.LastName = (string)lastObj;
                            }
                            object mobileObj = reader[3];
                            if (mobileObj != null && mobileObj != DBNull.Value)
                            {
                                data.Phone = (string)mobileObj;
                            }
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }

        public IEnumerable<string> SearchBlackListByMobilePhone(string phone)
        {
            IEnumerable<string> retVal = from cont in XrmDataContext.contacts
                                         where
                                         cont.new_blaklist.HasValue
                                         &&
                                         cont.new_blaklist.Value == true
                                         &&
                                         cont.mobilephone.Contains(phone)
                                         select
                                         cont.mobilephone;
            return retVal;
        }

        /// <summary>
        /// Gets the contact that has the given phoneNumber as his mobile phone.
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public DML.Xrm.contact GetContactByPhoneNumber(string phoneNumber)
        {
            DML.Xrm.contact contact = XrmDataContext.contacts.FirstOrDefault(x => x.mobilephone == phoneNumber);
            return contact;
        }

        public DML.SqlHelper.ContactMinData GetClipCallContactMinDataByPhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
                return null;
            string query = @"
select contactid, mobilephone, new_blaklist
from contact with (nolock) 
where DeletionStateCode = 0 and StatusCode = 1 and New_isMobileApp = 1 and mobilephone = @phone";
            DML.SqlHelper.ContactMinData data = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@phone", phoneNumber);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            data = new NoProblem.Core.DataModel.SqlHelper.ContactMinData();
                            data.Id = (Guid)reader["contactid"];
                            data.IsBlackList = reader["new_blaklist"] == DBNull.Value ? false : (bool)reader["new_blaklist"];
                            data.MobilePhone = phoneNumber;
                        }
                    }
                }
            }
            return data;
        }
        public DML.SqlHelper.ContactMinData GetContactMinDataByPhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
                return null;
            string query = @"
select contactid, mobilephone, new_blaklist
from contact with (nolock) 
where DeletionStateCode = 0 and StatusCode = 1 and ISNULL(New_isMobileApp, 0) = 0 and mobilephone = @phone";
            DML.SqlHelper.ContactMinData data = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@phone", phoneNumber);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            data = new NoProblem.Core.DataModel.SqlHelper.ContactMinData();
                            data.Id = (Guid)reader["contactid"];
                            data.IsBlackList = reader["new_blaklist"] == DBNull.Value ? false : (bool)reader["new_blaklist"];
                            data.MobilePhone = phoneNumber;
                        }
                    }
                }
            }
            return data;
        }
        public List<DML.SqlHelper.ContactMinData> GetContactMinDataByPhoneNumberExeptSupplier(string phoneNumber, Guid supplierId)
        {
            if (string.IsNullOrEmpty(phoneNumber))
                return null;
            List<DML.SqlHelper.ContactMinData> list = new List<DML.SqlHelper.ContactMinData>();
            string query = @"
select contactid, mobilephone, new_blaklist, new_accountid
from contact with (nolock) 
where DeletionStateCode = 0 and StatusCode = 1 and New_isMobileApp = 1 
    and mobilephone = @phone and contactid <> @contactId";
     //       DML.SqlHelper.ContactMinData data = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@phone", phoneNumber);
                    command.Parameters.AddWithValue("@contactId", supplierId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.SqlHelper.ContactMinData data = new NoProblem.Core.DataModel.SqlHelper.ContactMinData();
                            data.Id = (Guid)reader["contactid"];
                            data.IsBlackList = reader["new_blaklist"] == DBNull.Value ? false : (bool)reader["new_blaklist"];
                            data.MobilePhone = phoneNumber;
                            if (reader["new_accountid"] != DBNull.Value)
                                data.AccountId = (Guid)reader["new_accountid"];
                            list.Add(data);
                        }
                    }
                }
            }
            return list;
        }

        public DML.SqlHelper.ContactMinData GetContactMinDataById(Guid id)
        {
            string query = @"select contactid, mobilephone, new_blaklist, new_accountid
                            from contact with (nolock) where contactid = @id";
            DML.SqlHelper.ContactMinData data = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            data = new NoProblem.Core.DataModel.SqlHelper.ContactMinData();
                            data.Id = (Guid)reader["contactid"];
                            data.IsBlackList = reader["new_blaklist"] == DBNull.Value ? false : (bool)reader["new_blaklist"];
                            data.MobilePhone = Convert.ToString(reader["mobilephone"]);
                            if (reader["new_accountid"] != DBNull.Value)
                                data.AccountId = (Guid)reader["new_accountid"];
                        }
                    }
                }
            }
            return data;
        }

        public DML.Xrm.contact CreateAnonymousContact(string phoneNumber, string name, string email, string address, bool isFemale, DateTime? birthDate, 
            string height, string weight, string country, int? heightFeet, int? heightInches, bool isMobileApp)
        {
            return CreateAnonymousContact(phoneNumber, name, email, address, isFemale, birthDate, height, weight, country, false, heightFeet, heightInches, isMobileApp);
        }

        /// <summary>
        /// Create a new contact with the given phoneNumber as his mobile phone.
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public DML.Xrm.contact CreateAnonymousContact(string phoneNumber, string name, string email, string address, bool isFemale, DateTime? birthDate, 
            string height, string weight, string country, bool isFromApi, int? heightFeet, int? heightInches, bool isMobileApp)
        {
            DML.Xrm.contact contact = new DML.Xrm.contact();
            if(!string.IsNullOrEmpty(phoneNumber))
                contact.mobilephone = phoneNumber;
            if (!string.IsNullOrEmpty(name))
            {
                name = name.Trim();
                int indexOfSpace = name.IndexOf(' ');
                if (indexOfSpace == -1)
                {
                    contact.firstname = name;
                }
                else
                {
                    contact.firstname = name.Substring(0, indexOfSpace).Trim();
                    contact.lastname = name.Substring(indexOfSpace, name.Length - indexOfSpace).Trim();
                }
            }
            if (!string.IsNullOrEmpty(address))
            {
                contact.address1_line1 = address;
            }
            if (!string.IsNullOrEmpty(email))
            {
                contact.emailaddress1 = email;
            }
            if (birthDate.HasValue)
            {
                contact.birthdate = birthDate.Value;
            }
            if (!String.IsNullOrEmpty(weight))
            {
                contact.new_weight = weight;
            }
            if (!String.IsNullOrEmpty(height))
            {
                contact.new_height = height;
            }
            if (heightFeet.HasValue)
            {
                contact.new_heightfeet = heightFeet;
            }
            if (heightInches.HasValue)
            {
                contact.new_heightinches = heightInches;
            }
            if (!String.IsNullOrEmpty(country))
            {
                contact.new_country = country;
            }
            contact.new_isfromapi = isFromApi;
            contact.new_ismobileapp = isMobileApp;
            contact.gendercode = isFemale ? (int)DataModel.Xrm.contact.GenderCode.FEMALE : (int)NoProblem.Core.DataModel.Xrm.contact.GenderCode.MALE;
            Create(contact);
            XrmDataContext.SaveChanges();
            return contact;
        }

        public override void Create(DML.Xrm.contact contact)
        {
            AutonumberManager autoNumber = new AutonumberManager();
            contact.new_consumernumber = autoNumber.GetNextNumber(tableName);
            base.Create(contact);
        }

        public List<DML.PublisherPortal.ConsumerMinimalData> SearchContacts(string searchParameter)
        {
            string query =
                @"select co.contactid, co.new_consumernumber, co.emailaddress1, co.firstname, co.lastname, co.mobilephone
                ,(select count(incidentid) from incident with(nolock) where deletionstatecode = 0 and customerid = co.contactid) incidentCount
                from contact co with(nolock)
                where co.deletionstatecode = 0
                and 
                (co.mobilephone like @search
                or co.emailaddress1 like @search
                or co.telephone1 like @search
                or co.fullname like @search
                )";
            List<DML.PublisherPortal.ConsumerMinimalData> dataList = new List<NoProblem.Core.DataModel.PublisherPortal.ConsumerMinimalData>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@search", "%" + searchParameter + "%");
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DML.PublisherPortal.ConsumerMinimalData data = new NoProblem.Core.DataModel.PublisherPortal.ConsumerMinimalData();
                            data.ConsumerNumber = reader["new_consumernumber"] != DBNull.Value ? (string)reader["new_consumernumber"] : string.Empty;
                            data.ContactId = (Guid)reader["contactid"];
                            data.Email = reader["emailaddress1"] != DBNull.Value ? (string)reader["emailaddress1"] : string.Empty;
                            data.FirstName = reader["firstname"] != DBNull.Value ? (string)reader["firstname"] : string.Empty;
                            data.LastName = reader["lastname"] != DBNull.Value ? (string)reader["lastname"] : string.Empty;
                            data.MobilePhone = reader["mobilephone"] != DBNull.Value ? (string)reader["mobilephone"] : string.Empty;
                            data.NumberOfRequests = (int)reader["incidentCount"];
                            dataList.Add(data);
                        }
                    }
                }
            }
            return dataList;
        }

        /// <summary>
        /// This function retrives the contact used for blocked numbers.
        /// </summary>
        /// <returns></returns>
        public Guid GetBlockedNumberContactId()
        {
            Guid id = Guid.Empty;
            string query = "select contactid from contact with (nolock) where new_isanonymouscontact = 1 and deletionstatecode = 0";
            object obj = ExecuteScalar(query);
            if (obj != null && obj != DBNull.Value)
            {
                id = (Guid)obj;
            }
            else
            {
                DML.Xrm.contact contact = CreateBlockedNumberContact();
                id = contact.contactid;
            }
            return id;
        }

        private DML.Xrm.contact CreateBlockedNumberContact()
        {
            DML.Xrm.contact contact = new NoProblem.Core.DataModel.Xrm.contact();
            contact.firstname = "Blocked Number";
            contact.new_isanonymouscontact = true;
            Create(contact);
            XrmDataContext.SaveChanges();
            return contact;
        }
        public bool HasPhoneNumber(Guid customerId)
        {
            string command = "SELECT MobilePhone FROM dbo.ContactBase WITH(NOLOCK) WHERE ContactId = @customerId";
            object o;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@customerId", customerId);
                    o = cmd.ExecuteScalar();
                    conn.Close();
                }
            }
            if (o == null || o == DBNull.Value)
                return false;
            string phone = (string)o;
            return !string.IsNullOrEmpty(phone);
            
        }
    }
}
