﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SellersApi
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            FileInfo log4NetConfigFile = new FileInfo(Server.MapPath(ConfigurationManager.AppSettings.Get("log4net.Config")));
            log4net.Config.XmlConfigurator.ConfigureAndWatch(log4NetConfigFile);
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var config = new HttpConfiguration();

            config.Services.Add(typeof(IExceptionLogger), new SellersApi.Extensibility.WebApi.Exceptions.MarketplaceExceptionLog()); 
        }
        
    }
}