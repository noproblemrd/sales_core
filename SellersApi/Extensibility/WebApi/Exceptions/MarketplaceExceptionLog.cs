﻿using log4net;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ExceptionHandling;
//using Castle.Core.Logging;

namespace SellersApi.Extensibility.WebApi.Exceptions
{
    public class MarketplaceExceptionLog : ExceptionLogger
    {
    //    public ILogger Logger { get; set; }
        public ILog log { get; set; }
        public override void Log(ExceptionLoggerContext context)
        {
            log.Error("error", context.Exception);
            
        }

        public override bool ShouldLog(ExceptionLoggerContext context)
        {
            return true;
        }
    }
}