﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SellersApi.CoreConnectors;

namespace SellersApi.Models
{
    public class ZipValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string zip = value as string;
            if (String.IsNullOrEmpty(zip))
            {
                return false;
            }
            GeneralConnector connector = new GeneralConnector();
            return connector.ZipExists(zip);
        }
    }
}