﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SellersApi.Models
{
    public class SellerIdValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string sellerId = value as string;
            CoreConnectors.GeneralConnector connector = new CoreConnectors.GeneralConnector();
            return connector.VerifySellerId(sellerId);
        }
    }
}