﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SellersApi.Models
{
    public class TestFieldValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int test = (int)value;
            if (test >= 0 && test < 3)
                return ValidationResult.Success;
            return new ValidationResult("The field test must be 0, 1 or 2.");
        }
    }
}