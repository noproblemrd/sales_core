﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace SellersApi.Models
{
    public class PhoneValidationAttribute : ValidationAttribute
    {
        private const string PHONE_REGEX = "^[0,2-9]\\d{9}$";

        public override bool IsValid(object value)
        {
            string phone = value as string;
            if (String.IsNullOrEmpty(phone))
                return false;
            return Regex.IsMatch(phone, PHONE_REGEX);
        }
    }
}