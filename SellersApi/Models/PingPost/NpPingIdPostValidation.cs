﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SellersApi.Models.PingPost
{
    public class NpPingIdPostValidation : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string npPingId = value as string;
            if (String.IsNullOrEmpty(npPingId))
                return false;
            CoreConnectors.PingPostConnector connector = new CoreConnectors.PingPostConnector();
            return connector.IsPingIdValidForPost(npPingId);
        }
    }
}