﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SellersApi.Models.PingPost.V1
{
    public class PingRequest
    {
        [Required]
        [CategoryValidation]
        public string category { get; set; }

        [Required]
        [ZipValidation]
        public string zip { get; set; }

        [Required]
        public string lead_id { get; set; }

        [Required]
        [SellerIdValidation]
        public string seller_id { get; set; }

        [TestFieldValidation]
        public int test { get; set; }

        public string ip { get; set; }

        public string description { get; set; }
    }

    public class PingResponse
    {
        public bool interested { get; set; }
        public string np_ping_id { get; set; }
        public decimal price { get; set; }
    }
}