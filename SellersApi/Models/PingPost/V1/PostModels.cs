﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SellersApi.Models.PingPost.V1
{
    public class PostRequest
    {
        [Required]
        [PhoneValidation]
        public string phone { get; set; }

        public string street_address { get; set; }

        public string email { get; set; }

        public string first_name { get; set; }

        public string last_name { get; set; }

        [Required]
        [NpPingIdPostValidation]
        public string np_ping_id { get; set; }

        [Required]
        [SellerIdValidation]
        public string seller_id { get; set; }

        public string ip { get; set; }

        public string description { get; set; }

        public string additional_info { get; set; }
    }

    public class PostResponse
    {
        public bool success { get; set; }
        public IEnumerable<string> messages { get; set; }
    }
}