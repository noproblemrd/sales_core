﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace SellersApi.Models
{
    public class UrlNotRequiredValidationAttribute : ValidationAttribute
    {
        private const string URL_REGEX = @"^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$";

        public override bool IsValid(object value)
        {
            string url = value as string;
            if (String.IsNullOrEmpty(url))
                return true;
            return Regex.IsMatch(url, URL_REGEX);
        }
    }
}