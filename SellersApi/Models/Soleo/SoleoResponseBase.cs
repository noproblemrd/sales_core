﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SellersApi.Models.Soleo
{
    [DataContract]
    public abstract class SoleoResponseBase
    {
        public virtual string ToXml()
        {
            /*
            var stringwriter = new System.IO.StringWriter();
            var serializer = new System.Xml.Serialization.XmlSerializer(this.GetType());
            serializer.Serialize(stringwriter, this);
            return stringwriter.ToString();
            */
            using (MemoryStream memStm = new MemoryStream())
            {
                var serializer = new DataContractSerializer(this.GetType());
                serializer.WriteObject(memStm, this);

                memStm.Seek(0, SeekOrigin.Begin);

                using (var streamReader = new StreamReader(memStm))
                {
                    string result = streamReader.ReadToEnd();
                    return result;
                }
            }

        }
    }
}