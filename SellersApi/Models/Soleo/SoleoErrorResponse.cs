﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SellersApi.Models.Soleo
{
    [DataContract(Name = "results")]
    public class SoleoErrorResponse : SoleoResponseBase
    {
        public SoleoErrorResponse():base()
        {
            properties = new ErrorProperties();
        }
        [DataMember]
        public ErrorProperties properties { get; set; }
    }
    [DataContract()]
    public class ErrorProperties
    {
        [DataMember]
        public string error { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public string searchid { get; set; }
    }
    /*
     * <result>
<properties>
<error></error>
<message></message>
<searchid>123</searchid>
</properties>
</result>
     */
}