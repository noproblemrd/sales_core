﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SellersApi.Models.Soleo
{
    [DataContract(Name = "results")]
    public class SoleoResponse : SoleoResponseBase
    {
        public SoleoResponse(bool isSuccess) : base()
        {
            properties = new SoleoProperties(isSuccess);
            listings = new List<SoleoListing>();
        }
        [DataMember]
        public SoleoProperties properties { get; set; }
        [DataMember]
        public List<SoleoListing> listings { get; set; }

        
    }
     [DataContract(Name = "properties")]
    public class SoleoProperties
    {
         const string SUCCESS = "success";
         const string FAILED = "failed";
         public SoleoProperties(bool isSuccess)
         {
             result = isSuccess ? SUCCESS : FAILED;
         }
         /*
         <result>success</result>
<searchid>11213a55</searchid>
<count>2</count>
          * */
         [DataMember]
         public string result { get; set; }
          [DataMember]
         public string searchid { get; set; }
          [DataMember]
         public int count { get; set; }
    }
     [DataContract(Name = "listing")]
    public class SoleoListing
    {
         const string _type = "pay-per-call";
         public SoleoListing()
         {
             type = _type;
         }
         [DataMember]
         public string name { get; set; }
         [DataMember]
         public string phone { get; set; }
         [DataMember]
         public string listingid { get; set; }
         [DataMember]
         public string type { get; set; }
         [DataMember]
         public string description { get; set; }
         [DataMember]
         public string address1 { get; set; }
         [DataMember]
         public string city { get; set; }
         [DataMember]
         public string state { get; set; }
         [DataMember]
         public string zip { get; set; }
         [DataMember]
         public string latitude { get; set; }
         [DataMember]
         public string longtitude { get; set; }
         [DataMember]
         public decimal price { get; set; }
         [DataMember]
         public string recordedname { get; set; }
         [DataMember]
         public int duration { get; set; }
    }
}