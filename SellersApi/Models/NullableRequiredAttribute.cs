﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SellersApi.Models
{
    public class NullableRequiredAttribute : ValidationAttribute
    {
        public bool AllowEmptyStrings { get; set; }

        public NullableRequiredAttribute()
            : base("The field {0} is required.")
        {
            AllowEmptyStrings = false;
        }

        public override bool IsValid(object value)
        {
            if (value == null)
                return false;

            if (!this.AllowEmptyStrings)
            {
                string str = value as string;
                if (str != null)
                    return (str.Trim().Length != 0);
            }

            return true;
        }
    }
}