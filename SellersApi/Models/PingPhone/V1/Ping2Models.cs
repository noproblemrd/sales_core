﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace SellersApi.Models.PingPhone.V1
{
    public static class Ping2Consts
    {
        public const string OK = "OK";
        public const string ERROR = "ERROR";
    }

    public class Ping2Request
    {
        [Required]
        [PhoneValidation]
        public string caller_id { get; set; }

        [NullableRequired]
        public bool? is_caller_id_real { get; set; }

        [Required]
        public string lead_id { get; set; }

        [Required]
        [NpPingIdPing2Validation]
        public string np_ping_id { get; set; }

        [UrlNotRequiredValidation]
        public string callback_url { get; set; }

        [Required]
        [SellerIdValidation]
        public string seller_id { get; set; }
    }

    public class Ping2Response
    {
        public string status { get; set; }
        public List<string> messages { get; set; }
        public string np_phone { get; set; }
    }
}