﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SellersApi.Models.PingPhone.V1
{
    public class TestPhoneRequiredAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int test = ((Ping1Request)validationContext.ObjectInstance).test;
            if (test == 1)
            {
                string phone = value as string;
                if (String.IsNullOrEmpty(phone))
                    return new ValidationResult("test_provider_phone is required if field test is 1.");
                PhoneValidationAttribute phoneValidation = new PhoneValidationAttribute();
                try
                {
                    phoneValidation.Validate(phone, "test_provider_phone");
                }
                catch (ValidationException vExc)
                {
                    return new ValidationResult(vExc.ValidationResult.ErrorMessage);
                }
            }
            return ValidationResult.Success;
        }
    }
}