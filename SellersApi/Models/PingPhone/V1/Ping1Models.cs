﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SellersApi.Models.PingPhone.V1
{
    public static class Ping1Consts
    {
        public const string OK = "OK";
        public const string NOT_INTERESTED = "NOT_INTERESTED";
        public const string ERROR = "ERROR";
    }

    public class Ping1Request
    {
        [Required]
        [CategoryValidation]
        public string category { get; set; }

        [Required]
        [ZipValidation]
        public string zip { get; set; }

        [Required]
        public string lead_id { get; set; }

        [Required]
        [SellerIdValidation]
        public string seller_id { get; set; }

        [TestFieldValidation]
        public int test { get; set; }

        [TestPhoneRequired]        
        public string test_provider_phone { get; set; }
    }

    public class Ping1Response
    {
        public string status { get; set; }
        public List<string> messages { get; set; }
        public string np_ping_id { get; set; }
        public decimal price { get; set; }
        public int billable_call_duration_seconds { get; set; }
    }   
}