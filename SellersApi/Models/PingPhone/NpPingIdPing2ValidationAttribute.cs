﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SellersApi.Models.PingPhone
{
    public class NpPingIdPing2ValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string npPingId = value as string;
            if (String.IsNullOrEmpty(npPingId))
                return false;
            Guid id;
            if (!Guid.TryParse(npPingId, out id))
            {
                return false;
            }
            CoreConnectors.PingPhoneConnector connector = new CoreConnectors.PingPhoneConnector();
            return connector.IsPingIdValidForPing2(id);
        }
    }
}