﻿using log4net;
using SellersApi.Models.Soleo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiCommons.Controllers;

namespace SellersApi.Controllers.Soleo.V1
{
    public class SoleoController : ApiController
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SoleoController));
        // GET /soleo/ping
        [HttpGet]
        public HttpResponseMessage Ping([FromUri]SoleoGet request)
        {
            SoleoResponse s;
            PingPhoneCoreService.PingPhone connector = SellersApi.CoreConnectors.ClientsFactory.GetClient<PingPhoneCoreService.PingPhone>();
            PingPhoneCoreService.SoleoPingRequest soleoPingRequest = new PingPhoneCoreService.SoleoPingRequest()
            {
                category = request.category,
                zip = request.location,
                count = request.count,
                sellerId = request.sellerId,
                requesterid = request.requesterid,
                 callerId=request.callerId,
                isTest = request.isTest
            };
            PingPhoneCoreService.ResultOfSoleoPingResponse result = null;
            try
            {
                result = connector.SoleoPing(soleoPingRequest);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                s = new SoleoResponse(false);
                return new HttpResponseMessage() { Content = new StringContent(s.ToXml(), System.Text.Encoding.UTF8, "application/xml") };
            }
            if(result.Type == PingPhoneCoreService.eResultType.Failure)
            {
                s = new SoleoResponse(false);
                return new HttpResponseMessage() { Content = new StringContent(s.ToXml(), System.Text.Encoding.UTF8, "application/xml") };
            }
            PingPhoneCoreService.SoleoPingResponse data = result.Value;
            s = new SoleoResponse(true);
            s.properties.count = data.listings.Length;
            s.properties.searchid = data.searchid;
            foreach(PingPhoneCoreService.SoleoDataResponse row in data.listings)
            {
                SoleoListing sl = new SoleoListing();
                sl.duration = row.duration;
                sl.description = row.description;
                sl.price = row.price;
                sl.phone = row.phone;
                sl.name = row.name;
                s.listings.Add(sl);
            }
            return new HttpResponseMessage() { Content = new StringContent(s.ToXml(), System.Text.Encoding.UTF8, "application/xml") };
        }
    }
}
