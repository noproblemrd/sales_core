﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;

namespace SellersApi.Controllers
{
    public class ControllersHelper
    {
        public static List<string> GetModelErrors(object request, ModelStateDictionary ModelState)
        {
            if (request == null)
            {
                return new List<string> { "Invalid request" };
            }
            else if(!ModelState.IsValid)
            {
                var errors = ModelState
                        .Where(e => e.Value.Errors.Count > 0)
                        .Select(e =>
                            e.Value.Errors.First().ErrorMessage
                        ).Where(e => !string.IsNullOrEmpty(e));
                return errors.ToList();
            }
            return null;
        }
    }
}