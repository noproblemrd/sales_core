﻿using ApiCommons.Controllers;
using log4net;
using SellersApi.Models.PingPost.V1;
using System;
using System.Web.Http;

namespace SellersApi.Controllers.V1
{
    public class PingPostController : ControllersBase
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PingPostController));
        CoreConnectors.PingPostConnector connector;

        public PingPostController()
        {
            connector = new CoreConnectors.PingPostConnector();
        }

        [HttpPost]
        public PingResponse Ping(PingRequest request)
        {
            logger.Info("New ping: " + Newtonsoft.Json.JsonConvert.SerializeObject(request));
            ValidateModel(request, ModelState);
            try
            {
                if (request.test == 1)
                {
                    return new PingResponse()
                    {
                        interested = false
                    };
                }
                PingResponse result = connector.Ping(request);
                return result;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }

        [HttpPost]
        public PostResponse Post(PostRequest request)
        {
            logger.Info("New post: " + Newtonsoft.Json.JsonConvert.SerializeObject(request));
            ValidateModel(request, ModelState);
            try
            {
                PostResponse result = connector.Post(request);
                return result;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
    }
}