﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SellersApi.Models.PingPhone.V1;

namespace SellersApi.Controllers.V1
{
    public class PingPhoneController : ApiController
    {
        // POST api/v1/pingphone/ping1
        [HttpPost]
        public Ping1Response Ping1([FromBody]Ping1Request request)
        {
            List<string> errors = ControllersHelper.GetModelErrors(request, ModelState);
            if (errors != null)
            {
                return new Ping1Response()
                {
                    messages = errors,
                    status = Ping1Consts.ERROR
                };
            }
            if (request.test == 2)
            {
                return new Ping1Response()
                {
                    status = Ping1Consts.NOT_INTERESTED
                };
            }

            CoreConnectors.PingPhoneConnector connector = new CoreConnectors.PingPhoneConnector();
            var result = connector.SendPing1ToCore(new PingPhoneCoreService.Ping1Request()
            {
                Category = request.category,
                Zip = request.zip,
                LeadId = request.lead_id,
                SellerId = request.seller_id,
                Test = request.test,
                TestProviderPhone = request.test_provider_phone
            });

            if (result.Type == PingPhoneCoreService.eResultType.Failure)
            {
                return new Ping1Response()
                {
                    messages = new List<string>() { "Server Error." },
                    status = Ping1Consts.ERROR
                };
            }

            if (result.Value.IsValid)
            {
                return new Ping1Response()
                {
                    status = result.Value.Interested ? Ping1Consts.OK : Ping1Consts.NOT_INTERESTED,
                    np_ping_id = result.Value.NpPingId,
                    price = result.Value.Price,
                    billable_call_duration_seconds = result.Value.BillingCallDurationSeconds
                };
            }
            else
            {
                return new Ping1Response()
                {
                    status = Ping1Consts.ERROR,
                    messages = new List<string>() { result.Value.ValidationError }
                };
            }
        }

        // POST api/v1/pingphone/ping2
        [HttpPost]
        public Ping2Response Ping2([FromBody]Ping2Request request)
        {
            List<string> errors = ControllersHelper.GetModelErrors(request, ModelState);
            if (errors != null)
            {
                return new Ping2Response()
                {
                    messages = errors,
                    status = Ping1Consts.ERROR
                };
            }
            CoreConnectors.PingPhoneConnector connector = new CoreConnectors.PingPhoneConnector();
            var result = connector.SendPing2ToCore(new PingPhoneCoreService.Ping2Request()
            {
                CallbackUrl = request.callback_url,
                CallerId = request.caller_id,
                IsCallerIdReal = request.is_caller_id_real.Value,
                LeadId = request.lead_id,
                NpPingId = request.np_ping_id,
                SellerId = request.seller_id
            });

            if (result.Type == PingPhoneCoreService.eResultType.Failure)
            {
                return new Ping2Response()
                {
                    messages = new List<string>() { "Server Error." },
                    status = Ping1Consts.ERROR
                };
            }

            if (result.Value.IsValid)
            {
                return new Ping2Response()
                {
                    status = Models.PingPhone.V1.Ping2Consts.OK,
                    np_phone = result.Value.NpPhone
                };
            }
            else
            {
                return new Ping2Response()
                {
                    status = Ping2Consts.ERROR,
                    messages = new List<string>() { result.Value.ValidationError }
                };
            }

            return new Ping2Response();
        }
    }
}
