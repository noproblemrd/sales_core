﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SellersApi.CoreConnectors
{
    public class PingPostConnector
    {
        private PingPostCoreService.PingPost client;

        public PingPostConnector()
        {
            client = ClientsFactory.GetClient<PingPostCoreService.PingPost>();
        }

        internal bool IsPingIdValidForPost(string id)
        {
            var coreResult = client.IsNpPingIdValidForPost(id);
            if (coreResult.Type == PingPostCoreService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        internal Models.PingPost.V1.PingResponse Ping(Models.PingPost.V1.PingRequest request)
        {
            var coreResult = client.Ping(
                new PingPostCoreService.PingRequest()
                {
                    CategoryCode = request.category,
                    LeadId = request.lead_id,
                    SellerId = new Guid(request.seller_id),
                    Test = request.test,
                    Zip = request.zip,
                    Description = request.description,
                    IP = request.ip
                });
            if (coreResult.Type == PingPostCoreService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            Models.PingPost.V1.PingResponse response = new Models.PingPost.V1.PingResponse();
            response.interested = coreResult.Value.Interested;
            response.np_ping_id = coreResult.Value.NpPingId;
            response.price = coreResult.Value.Price;
            return response;
        }

        internal Models.PingPost.V1.PostResponse Post(Models.PingPost.V1.PostRequest request)
        {
            var coreResult = client.Post(
                new PingPostCoreService.PostRequest()
                {
                    Email = request.email,
                    FirstName = request.first_name,
                    LastName = request.last_name,
                    NpPingId = request.np_ping_id,
                    Phone = request.phone,
                    SellerId = new Guid(request.seller_id),
                    StreetAddress = request.street_address,
                    IP = request.ip,
                    Description = request.description,
                    AdditionalInfo = request.additional_info
                });
            if (coreResult.Type == PingPostCoreService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            Models.PingPost.V1.PostResponse resposne = new Models.PingPost.V1.PostResponse();
            resposne.success = coreResult.Value.Success;
            resposne.messages = coreResult.Value.Messages;
            return resposne;
        }
    }
}