﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace SellersApi.CoreConnectors
{
    public class ClientsFactory
    {
        private readonly static string coreServicesUrl = ConfigurationManager.AppSettings.Get("CoreServicesUrlBase");

        private const string CUSTOMER = "/Core/CustomersService.asmx";
        private const string PING_PHONE = "/Core/APIs/SellersApi/PingPhone.asmx";
        private const string PING_POST = "/Core/APIs/SellersApi/PingPost.asmx";
        private const string SELLER_API_HELPER = "/Core/APIs/SellersApi/SellersApiHelperService.asmx";

        public static T GetClient<T>() where T : System.Web.Services.Protocols.SoapHttpClientProtocol, new()
        {
            T client = new T();
            Type typeOfT = typeof(T);
            if (typeOfT == typeof(CustomerService.CustomersService))
            {
                client.Url = coreServicesUrl + CUSTOMER;
            }
            else if (typeOfT == typeof(PingPhoneCoreService.PingPhone))
            {
                client.Url = coreServicesUrl + PING_PHONE;
            }
            else if (typeOfT == typeof(SellersApiHelperCoreService.SellersApiHelperService))
            {
                client.Url = coreServicesUrl + SELLER_API_HELPER;
            }
            else if (typeOfT == typeof(PingPostCoreService.PingPost))
            {
                client.Url = coreServicesUrl + PING_POST;
            }
            return client;
        }
    }
}