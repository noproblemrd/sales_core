﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace SellersApi.CoreConnectors
{
    public class GeneralConnector
    {
        public bool ZipExists(string zip)
        {
            CustomerService.CustomersService client = ClientsFactory.GetClient<CustomerService.CustomersService>();
            var response = client.CheckUsaZipCode(zip);
            return response.Value;
        }

        public bool VerifySellerId(string sellerId)
        {
            SellersApiHelperCoreService.SellersApiHelperService client = ClientsFactory.GetClient<SellersApiHelperCoreService.SellersApiHelperService>();
            var response = client.VerifySellerId(sellerId);
            return response.Value;
        }

        public bool VerifyCategory(string category)
        {
            SellersApiHelperCoreService.SellersApiHelperService client = ClientsFactory.GetClient<SellersApiHelperCoreService.SellersApiHelperService>();
            var response = client.VerifyCategory(category);
            return response.Value;
        }
    }
}