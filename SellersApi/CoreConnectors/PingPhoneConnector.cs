﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace SellersApi.CoreConnectors
{
    public class PingPhoneConnector
    {
        private PingPhoneCoreService.PingPhone client;

        public PingPhoneConnector()
        {
            client = ClientsFactory.GetClient<PingPhoneCoreService.PingPhone>();
        }

        public PingPhoneCoreService.ResultOfPing1Response SendPing1ToCore(PingPhoneCoreService.Ping1Request request)
        {
            var result = client.Ping1(request);
            return result;
        }

        public PingPhoneCoreService.ResultOfPing2Response SendPing2ToCore(PingPhoneCoreService.Ping2Request request)
        {
            var result = client.Ping2(request);
            return result;
        }

        public bool IsPingIdValidForPing2(Guid npPingId)
        {
            var result = client.IsNpPingIdValidForPing2(npPingId);
            return result.Value;
        }
    }
}