﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Shouldly;
using TechTalk.SpecFlow;

namespace ClipCall.Specifications
{
    [Binding]
    public class ReportLeadSpecSteps
    {

        private readonly List<int> numbers = new List<int>();

        [Given(@"I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(int p0)
        {
            numbers.Add(p0);
        }
        
        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            int sum = numbers.Sum();
            numbers.Clear();
            numbers.Add(sum);
        }

        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int p0)
        {
            int sum = numbers.First();
            numbers.Clear();
            sum.ShouldBe(p0);
        }
    }
}
