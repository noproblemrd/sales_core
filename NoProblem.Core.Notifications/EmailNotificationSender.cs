using System;
using System.Web.Services.Protocols;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using Effect.Crm.Methods;
using Effect.Crm.Logs;

namespace NoProblem.Core.Notifications
{
    public class EmailNotificationSender : NotificationSender
    {
        public EmailNotificationSender(CrmService crmService)
            : base(crmService)
        {
        }

        private Guid CreateCrmEmailObject(EmailNotificationObject NotificationDetails)
        {
            try
            {
                email eml = new email();
                activityparty[] partiesFrom = new activityparty[1];               
                //From
                activityparty partyFrom = new activityparty();
                partyFrom.participationtypemask = new Picklist();
                partyFrom.participationtypemask.Value = 0;
                partyFrom.partyid = new Lookup();
                partyFrom.partyid.type = "systemuser";
                partyFrom.partyid.Value = NotificationDetails.From;
                partiesFrom[0] = partyFrom;
                activityparty[] partiesTo = new activityparty[NotificationDetails.To.Length];
                //To
                int toIndex = 0;
                foreach (Guid toUser in NotificationDetails.To)
                {
                    activityparty partyTo = new activityparty();
                    partyTo.participationtypemask = new Picklist();
                    partyTo.participationtypemask.Value = 0;
                    partyTo.partyid = new Lookup();
                    partyTo.partyid.Value = toUser;
                    partyTo.partyid.type = NotificationDetails.ToEntityName;
                    partiesTo[toIndex] = partyTo;
                    toIndex++;
                }
                eml.mimetype = "text/html";
                eml.from = partiesFrom;
                eml.to = partiesTo;
                //Set subject of email message
                eml.subject = NotificationDetails.Subject;
                eml.regardingobjectid = new Lookup();
                eml.regardingobjectid.type = NotificationDetails.RegardingObjectType;
                eml.regardingobjectid.Value = NotificationDetails.RegardingObjectID;
                eml.description = NotificationDetails.BodyText;
                // Assign the email to specify owner
                if (NotificationDetails.EmailOwner != Guid.Empty)
                {
                    eml.ownerid = new Owner();
                    eml.ownerid.type = "systemuser";
                    eml.ownerid.Value = NotificationDetails.EmailOwner;
                }

                if (NotificationDetails.ScheduledEnd != null)
                {
                    eml.scheduledend = new CrmDateTime();
                    eml.scheduledend.Value = NotificationDetails.ScheduledEnd;
                }
                return CrmService.Create(eml);
            }
            catch (SoapException ex)
            {
                LogUtils.MyHandle.HandleException(ex, "Exception (SoapException) in CreateCrmEmailObject");
                throw new Exception(ex.Detail.InnerText);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "Exception in CreateCrmEmailObject");
                throw new Exception(ex.Message);
            }
        }

        public override bool Send(NotificationObect NotificationDetails)
        {
            try
            {
                SendEmailRequest req = new SendEmailRequest();
                req.IssueSend = true;
                req.EmailId = CreateCrmEmailObject((EmailNotificationObject)NotificationDetails);
                req.TrackingToken = String.Empty;
                SendEmailResponse resp = (SendEmailResponse)CrmService.Execute(req);
                return true;
            }
            catch (SoapException ex)
            {
                throw new Exception(ex.Detail.InnerText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override void FillNotificationObject(string TemplateText, NotificationObect NotificationDetails)
        {
            try
            {
                ((EmailNotificationObject)NotificationDetails).BodyText = getDescriptionFromTemplate(TemplateText);
                ((EmailNotificationObject)NotificationDetails).Subject = getSubjectFromTemplate(TemplateText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
