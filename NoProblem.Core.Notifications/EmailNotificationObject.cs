using System;
using Effect.Crm.SDKProviders.CrmServiceSdk;

namespace NoProblem.Core.Notifications
{
    /// <summary>
    /// This class inherits the NotificationObect abstract class,
    /// and adds information that needs to be saved for handling email messages in the CRM enviroment
    /// </summary>
    public class EmailNotificationObject : NotificationObect
    {
        #region Properties

        /// <summary>
        /// The sending user's ID
        /// </summary>
        public Guid From { get; set; }

        /// <summary>
        /// A list of IDs to send the message to
        /// </summary>
        public Guid[] To { get; set; }

        /// <summary>
        /// The type of the Entities who will recive the message
        /// </summary>
        public string ToEntityName { get; set; }

        /// <summary>
        /// The ID of the Owner
        /// </summary>
        public Guid EmailOwner { get; set; }

        /// <summary>
        /// The subject of the message
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// The time at which the message should have been sent
        /// </summary>
        public string ScheduledEnd { get; set; }

        #endregion
    }
}
