using System;

namespace NoProblem.Core.Notifications
{
    /// <summary>
    /// This class inherits the NotificationObect abstract class,
    /// and adds information that need to be saved in order to work with the teleclal SMS provider
    /// </summary>
    public class SMSNotificationObject : NotificationObect
    {
        #region Properties

        public string SMSAccount { get; set; }

        /// <summary>
        /// The user for the teleclal system
        /// </summary>
        public string SMSUser { get; set; }

        /// <summary>
        /// The password for the teleclal user
        /// </summary>
        public string SMSPassword { get; set; }

        /// <summary>
        /// The number which will be presented on the contact's display
        /// </summary>
        public string FromPhoneNumber { get; set; }

        /// <summary>
        /// Holds the phone number to send the sms to, if the number entered is not a mobile phone number,
        /// the ToPhoneNumber will hold the string.Empty value.
        /// Accepteble values are 05########
        /// </summary>
        public string ToPhoneNumber { get; set; }

        /// <summary>
        /// Unique Id for response purpose
        /// </summary>
        public string UniqueId { get; set; }

        /// <summary>
        /// Unique Id for response purpose
        /// </summary>
        public Guid SmsSupplierId { get; set; }

        /// <summary>
        /// Unique Id for response purpose
        /// </summary>
        public string MessageId { get; set; }

        public SMSWorkingMethods WorkingMethod { get; set; }

        public SMSSendData SendDataMethod { get; set; }

        public string Encoding { get; set; }

        public string URL { get; set; }

        public string DataTemplate { get; set; }

        public int NumOfTries { get; set; }

        public string PhoneCountry { get; set; }

        public string PhoneOriginal { get; set; }

        #endregion
    }
}
