using System;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using System.Configuration;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using Effect.Crm.Logs;
using NoProblem.Core.Notifications.SmsSuppliers;

namespace NoProblem.Core.Notifications
{
    /// <summary>
    /// This class implements the sending of an sms message through the teleclal sms supplier
    /// </summary>
    public class SMSNotificationSender : NotificationSender
    {

        public delegate void onSendingSms(NotificationObect NotificationDetails);
        public event onSendingSms OnSendingSms;

        public delegate void onSmsSent(NotificationObect NotificationDetails);
        public event onSmsSent OnSmsSent;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NotificationDetails"></param>
        /// <returns>True or False to indicate another sending try</returns>
        public delegate bool onSmsSendFailed(NotificationObect NotificationDetails);
        public event onSmsSendFailed OnSmsSendFailed;

        public SMSNotificationSender(CrmService crmService)
            : base(crmService)
        {
        }

        public override bool Send(NotificationObect NotificationDetails)
        {
            if (OnSendingSms != null)
                OnSendingSms(NotificationDetails);

            bool sendOk = false;
            bool sendSms = true;
            SMSNotificationObject smsNotification = NotificationDetails as SMSNotificationObject;

            while (sendSms)
            {
                SmsSupplierHandler supplierHandler = Factory.GetSmsSupplierHandler(smsNotification.Name);
                smsNotification.NumOfTries++;
                sendOk = supplierHandler.Send(smsNotification);

                if (sendOk)
                    sendSms = false;

                // the SMS sending failed
                if (!sendOk && OnSmsSendFailed != null)
                    sendSms = OnSmsSendFailed(NotificationDetails);

            }

            #region OldSendCode
            //if(smsNotification.SmsSupplierId == new Guid(SmsSupplier_SHAMIR))
            //{
            //   for (int i = 0 ; i < 3 ; i++)
            //   {
            //      if(sendOk)break;
            //      if (!sendOk){smsNotification.SmsSupplierId = new Guid(SmsSupplier_SHAMIR); sendOk = SendShamir(NotificationDetails);}
            //      if (!sendOk){smsNotification.SmsSupplierId = new Guid(SmsSupplier_CELLACT); sendOk = SendCallAct(NotificationDetails);}
            //   }
            //}
            //else if(smsNotification.SmsSupplierId == new Guid(SmsSupplier_CELLACT))
            //{
            //   for (int i = 0 ; i < 3 ; i++)
            //   {
            //      if(sendOk)break;
            //      if (!sendOk){smsNotification.SmsSupplierId = new Guid(SmsSupplier_CELLACT); sendOk = SendCallAct(NotificationDetails);}
            //      if (!sendOk){smsNotification.SmsSupplierId = new Guid(SmsSupplier_SHAMIR); sendOk = SendShamir(NotificationDetails);}
            //   }
            //}
            #endregion

            if (sendOk && OnSmsSent != null)
                OnSmsSent(NotificationDetails);
            return sendOk;
        }

        public override void FillNotificationObject(string TemplateText, NotificationObect NotificationDetails)
        {
            ((SMSNotificationObject)NotificationDetails).BodyText = getDescriptionFromTemplate(TemplateText);
        }
    }
}
