using System;
using System.Configuration;
using System.Net;
using System.Xml;
using System.Web.Services.Protocols;

using CrmServiceSdk = Effect.Crm.SDKProviders.CrmServiceSdk;
using Effect.Crm.SDKProviders.MetadataServiceSdk;
using Effect.Crm.Methods;
using Effect.Crm.SDKProviders;
using Effect.Crm.Logs;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using System.Collections;
using Effect.Crm.Convertor;

namespace NoProblem.Core.Notifications
{
    public abstract class NotificationSender
    {
        public NotificationSender(CrmService crmService)
        {
            if (crmService != null)
            {
                CrmService = crmService;
            }
            else
            {
                CrmService = ProviderUtils.GetCrmService();
            }
        }

        #region Constants

        public const string EFFECT_OBJECT_PREFIX = "{Effect:";
        public const string EFFECT_OBJECT_SUFFIX = "}";

        #endregion

        #region Memmbers

        private MetadataService m_pMeta;
        //private string m_CrmServerAddress;
        private string m_CrmUserName;
        private string m_CrmUserPassword;
        private string m_CrmUserDomain;

        #endregion

        #region Properties

        public CrmService CrmService { get; set; }

        public MetadataService pMeta
        {
            get
            {
                if (m_pMeta == null)
                    m_pMeta = ProviderUtils.GetMetadataService();


                return m_pMeta;
            }
        }

        public string CrmUserName
        {
            get
            {
                if (m_CrmUserName == null && ConfigurationManager.AppSettings["serviceUserName"] != "")
                    m_CrmUserName = ConfigurationManager.AppSettings["serviceUserName"];

                return m_CrmUserName;
            }
            set
            {
                m_CrmUserName = value;
            }
        }

        public string CrmUserPassword
        {
            get
            {
                if (m_CrmUserPassword == null && ConfigurationManager.AppSettings["servicePassword"] != "")
                    m_CrmUserPassword = ConfigurationManager.AppSettings["servicePassword"];

                return m_CrmUserPassword;
            }
            set
            {
                m_CrmUserPassword = value;
            }
        }
        public string CrmUserDomain
        {
            get
            {
                if (m_CrmUserDomain == null && ConfigurationManager.AppSettings["serviceDomain"] != "")
                    m_CrmUserDomain = ConfigurationManager.AppSettings["serviceDomain"];

                return m_CrmUserDomain;
            }
            set
            {
                m_CrmUserDomain = value;
            }
        }

        #endregion


        /// <summary>
        /// This function sends a template by instantiate it with the TemplateObject 
        /// and then replacing all the EffectObjects in the template with thier matching values and send it as a text.
        /// </summary>
        /// <param name="TemplateName">The Name of the template to send</param>
        /// <param name="NotificationDetails"></param>
        /// <param name="TemplateObjectID">The ID of the Entity by wich to instantiate the Template</param>
        /// <param name="TemplateObjectType">The Type of the Entity by wich to instantiate the Template</param>
        public bool SendEffectTemplate(string templateName, NotificationObect notificationDetails, Guid templateObjectID, string templateObjectType)
        {
            LogUtils.MyHandle.WriteToLog(9, "SendEffectTemplate(string TemplateName={0},NotificationObect NotificationDetails={1},Guid TemplateObjectID={2},EntityName TemplateObjectType={3})", templateName, notificationDetails.ToString(), templateObjectID.ToString(), templateObjectType);
            if (!string.IsNullOrEmpty(templateName))
            {
                string templateText = InstantiateTemplate(templateName, templateObjectID, templateObjectType);
                templateText = System.Web.HttpUtility.HtmlDecode(templateText);

                if (notificationDetails.RegardingObjectID != Guid.Empty)
                {
                    CrmServiceSdk.BusinessEntity[] entities = MethodsUtils.FindEntities(CrmService, notificationDetails.RegardingObjectType.ToString(), new string[] { notificationDetails.RegardingObjectType + "id" }, new object[] { notificationDetails.RegardingObjectID }, "", "", false);

                    if (entities == null || entities.Length == 0)
                        return false;

                    templateText = replaceEffectObjectsWithValues(templateText, entities[0]);
                }

                FillNotificationObject(templateText, notificationDetails);
            }

            return Send(notificationDetails);
        }

        public abstract bool Send(NotificationObect NotificationDetails);

        public static string getDescriptionFromTemplate(string template)
        {
            XmlDocument outputDoc = new XmlDocument();
            outputDoc.LoadXml(template);
            string description = outputDoc.SelectSingleNode("template/body").InnerText;

            return description;
        }

        public static string getSubjectFromTemplate(string template)
        {
            XmlDocument outputDoc = new XmlDocument();
            outputDoc.LoadXml(template);

            string subject = outputDoc.SelectSingleNode("template/subject").InnerText;

            return subject;
        }

        public string InstantiateTemplate(string templateName, Guid templateObjectID, string templateObjectType)
        {
            string subjectText = "";
            string descriptionText = "";

            CrmServiceSdk.BusinessEntity[] ent = MethodsUtils.FindEntities(CrmService, templateObjectType, new string[] { templateObjectType + "id" }, new object[] { templateObjectID }, "", "", false);

            if (ent == null || ent.Length == 0)
                return string.Empty;

            CrmServiceSdk.BusinessEntity[] templs = MethodsUtils.FindEntities(CrmService, "template", new string[] { "title" }, new object[] { templateName }, "", "", false);

            if (templs == null || templs.Length == 0)
                return string.Empty;

            CrmServiceSdk.template tmpl = (CrmServiceSdk.template)templs[0];
            string tmplId = tmpl.templateid.Value.ToString();
            if (tmplId.IndexOf("{") < 0)
            {
                tmplId = "{" + tmplId + "}";
            }
            LogUtils.MyHandle.WriteToLog(9, "InstantiateTemplate- templateid: {0}", tmplId);

            InstantiateTemplateRequest request = new InstantiateTemplateRequest();
            request.ObjectId = templateObjectID;
            request.ObjectType = templateObjectType;
            request.ReturnDynamicEntities = true;
            request.TemplateId = tmpl.templateid.Value;

            InstantiateTemplateResponse response = CrmService.Execute(request) as InstantiateTemplateResponse;

            if (response.BusinessEntityCollection.BusinessEntities.Length > 0)
            {
                DynamicEntity email = response.BusinessEntityCollection.BusinessEntities[0] as DynamicEntity;

                if (email != null)
                {
                    object subject = ConvertorCRMParams.GetDynamicEntityPropertyValue(email, "subject");
                    object description = ConvertorCRMParams.GetDynamicEntityPropertyValue(email, "description");

                    subjectText = subject != null ? subject.ToString() : "";
                    descriptionText = description != null ? description.ToString() : "";
                }
            }
            return string.Format("<template><subject><![CDATA[{0}]]></subject><body><![CDATA[{1}]]></body></template>", subjectText, descriptionText);
        }

        public string replaceEffectObjectsWithValues(string templateBody, CrmServiceSdk.BusinessEntity regardingObject)
        {
            string processedTemplate = templateBody;

            int startIndex = 0;
            int endIndex = 0;
            string[] templateObject;

            do
            {
                startIndex = processedTemplate.IndexOf(EFFECT_OBJECT_PREFIX, 0);

                if (startIndex < 0)
                    continue;

                endIndex = processedTemplate.IndexOf(EFFECT_OBJECT_SUFFIX, startIndex);

                templateObject = processedTemplate.Substring(startIndex + EFFECT_OBJECT_PREFIX.Length, endIndex - startIndex - EFFECT_OBJECT_PREFIX.Length).Split('.');

                string entityName = templateObject[0];

                object propertyValue = null;

                propertyValue = GetPropertyValue(regardingObject, templateObject);
                processedTemplate = processedTemplate.Remove(startIndex, endIndex - startIndex + 1);

                if (propertyValue != null)
                {
                    processedTemplate = processedTemplate.Insert(startIndex, propertyValue.ToString());
                }

            } while (startIndex > 0);

            return processedTemplate;
        }

        /// <summary>
        /// The function goes through the path and finds the value of the last property.
        /// The function supports 
        /// </summary>
        /// <param name="entity">The Entity from which to obtain the properties</param>
        /// <param name="propertyPath">An array of property names</param>
        /// <returns>The Value of the property</returns>
        public object GetPropertyValue(CrmServiceSdk.BusinessEntity entity, string[] propertyPath)
        {
            object tempEntity = entity;
            object tempValue = null;
            object tempType = null;

            if (tempEntity == null)
                return null;

            try
            {
                for (int i = 1; i < propertyPath.Length; i++)
                {
                    System.Reflection.MemberInfo[] props = tempEntity.GetType().GetMember(propertyPath[i]);

                    if (props == null || props.Length == 0)
                        return null;

                    System.Reflection.MemberInfo prop = props[0] as System.Reflection.MemberInfo;
                    if (prop.MemberType == System.Reflection.MemberTypes.Property)
                        tempValue = ((System.Reflection.PropertyInfo)prop).GetValue(tempEntity, null);
                    else if (prop.MemberType == System.Reflection.MemberTypes.Field)
                        tempValue = ((System.Reflection.FieldInfo)prop).GetValue(tempEntity);
                    else
                        break;

                    CrmServiceSdk.CrmDateTime tempDate = tempValue as CrmServiceSdk.CrmDateTime;
                    if (tempDate != null)
                        return DateTime.Parse(tempDate.Value).ToString("dd/MM/yyyy HH:mm");

                    System.Reflection.MemberInfo[] propTypes = tempValue.GetType().GetMember("type");

                    if (propTypes != null && propTypes.Length > 0 && propTypes[0] != null)
                    {
                        System.Reflection.MemberInfo propType = propTypes[0] as System.Reflection.MemberInfo;
                        if (propType.MemberType == System.Reflection.MemberTypes.Property)
                            tempType = ((System.Reflection.PropertyInfo)propType).GetValue(tempValue, null);
                        else if (propType.MemberType == System.Reflection.MemberTypes.Field)
                            tempType = ((System.Reflection.FieldInfo)propType).GetValue(tempValue);
                    }

                    System.Reflection.PropertyInfo propValue = tempValue.GetType().GetProperty("Value");
                    if (propValue == null)
                        return tempValue;
                    else
                        tempValue = propValue.GetValue(tempValue, null);

                    if (tempValue.GetType() == typeof(Guid))
                    {
                        tempEntity = CrmService.Retrieve(tempType.ToString(), (Guid)tempValue, new CrmServiceSdk.AllColumns());
                    }
                    else
                    {
                        return tempValue;
                    }
                }
            }
            catch
            {
                return null;
            }

            return tempValue;
        }

        public abstract void FillNotificationObject(string TemplateText, NotificationObect NotificationDetails);
    }
}
