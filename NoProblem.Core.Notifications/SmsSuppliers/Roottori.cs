﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace NoProblem.Core.Notifications.SmsSuppliers
{
    /// <summary>
    /// Finland / Fonecta
    /// </summary>
    internal class Roottori : SmsSupplierHandler
    {
        internal override bool Send(SMSNotificationObject smsNotification)
        {
            bool retVal = false;
            string message = StripTags(smsNotification.BodyText);
            message = System.Web.HttpUtility.UrlEncode(message, Encoding.GetEncoding(smsNotification.Encoding));
            string phone = System.Web.HttpUtility.UrlEncode(smsNotification.ToPhoneNumber);
            string data = string.Format(smsNotification.DataTemplate, phone, message, smsNotification.FromPhoneNumber, smsNotification.UniqueId);
            string response = PostDataToURL(smsNotification, smsNotification.URL, data);
            retVal = ProccessResponse(smsNotification, response);
            return retVal;
        }

        private string PostDataToURL(SMSNotificationObject notificationObject, string szUrl, string szData)
        {
            //Setup the web request
            string szResult = string.Empty;
            szUrl += "?" + szData;
            WebRequest Request = WebRequest.Create(szUrl);
            Request.Timeout = 30000;
            Request.Method = "GET";
            Request.ContentType = "application/x-www-form-urlencoded";
            try
            {
                //Create the Response object
                using (WebResponse Response = Request.GetResponse())
                {
                    //Create the reader for the response                    
                    using (StreamReader sr = new StreamReader(Response.GetResponseStream(), Encoding.UTF8))
                    {
                        szResult = sr.ReadToEnd();
                    }
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                LogUtils.MyHandle.WriteToLog(1, "Thread was almost aborted in PostDataToURL but it was kept alive by force");
                System.Threading.Thread.ResetAbort();
            }
            return szResult;
        }

        private bool ProccessResponse(NotificationObect notificationDetails, string response)
        {
            LogUtils.MyHandle.WriteToLog(9, "ProccessResponse: Response = {0}", response);
            bool smsSent = false;
            XDocument doc = XDocument.Parse(response);
            XElement root = doc.Element("delivery-report");
            XElement failed = root.Element("failed");
            if (failed == null)
            {
                smsSent = true;
            }
            return smsSent;
        }
    }
}
