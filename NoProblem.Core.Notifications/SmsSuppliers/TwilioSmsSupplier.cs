﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.Notifications.SmsSuppliers
{
    internal class TwilioSmsSupplier : SmsSupplierHandler
    {

        private static string accountSid = System.Configuration.ConfigurationManager.AppSettings["TwilioAccountSid"];
        private static string authToken = System.Configuration.ConfigurationManager.AppSettings["TwilioAuthToken"];
        private Twilio.TwilioRestClient restClient;

        public TwilioSmsSupplier()
        {
            restClient = new Twilio.TwilioRestClient(accountSid, authToken);
        }

        internal override bool Send(SMSNotificationObject notificationDetails)
        {
            string message = StripTags(notificationDetails.BodyText);
            var sms = restClient.SendSmsMessage(
                notificationDetails.FromPhoneNumber,
                notificationDetails.ToPhoneNumber,
                message);
            bool retVal = false;
            if (sms != null && sms.Sid != null && sms.Status != "failed")
            {
                retVal = true;
            }
            return retVal;
        }
    }
}
