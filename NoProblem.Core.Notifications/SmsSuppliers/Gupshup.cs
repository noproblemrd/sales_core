﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Net;
using System.IO;

namespace NoProblem.Core.Notifications.SmsSuppliers
{
    /// <summary>
    /// India / Boonza
    /// </summary>
    internal class Gupshup : SmsSupplierHandler
    {
        internal override bool Send(SMSNotificationObject smsNotification)
        {
            bool retVal = false;

            string message = StripTags(smsNotification.BodyText);
            message = System.Web.HttpUtility.UrlEncode(message);
            string phone = System.Web.HttpUtility.UrlEncode(smsNotification.ToPhoneNumber);

            string data = string.Format(smsNotification.DataTemplate, phone, message, smsNotification.FromPhoneNumber, smsNotification.UniqueId);

            string response = PostDataToURL(smsNotification, smsNotification.URL, data);

            retVal = ProccessResponse(smsNotification, response);

            return retVal;
        }

        private bool ProccessResponse(NotificationObect notificationDetails, string response)
        {
            LogUtils.MyHandle.WriteToLog(9, "ProccessResponse: Response = {0}", response);
            bool smsSent = false;

            if (response.StartsWith("success"))
            {
                smsSent = true;
            }

            return smsSent;
        }

        private string PostDataToURL(SMSNotificationObject notificationObject, string szUrl, string szData)
        {
            //Setup the web request
            string szResult = string.Empty;
            WebRequest Request = WebRequest.Create(szUrl);
            Request.Timeout = 30000;
            Request.Method = "POST";
            Request.ContentType = "application/x-www-form-urlencoded";
            //Set the POST data in a buffer
            byte[] PostBuffer;
            try
            {
                // replacing " " with "+" according to Http post RPC
                szData = szData.Replace(" ", "+");
                //Specify the length of the buffer
                PostBuffer = Encoding.UTF8.GetBytes(szData);
                Request.ContentLength = PostBuffer.Length;
                //Open up a request stream
                using (Stream RequestStream = Request.GetRequestStream())
                {
                    //Write the POST data
                    RequestStream.Write(PostBuffer, 0, PostBuffer.Length);
                }
                //Create the Response object
                using (WebResponse Response = Request.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(Response.GetResponseStream(), Encoding.UTF8))
                    {
                        szResult = sr.ReadToEnd();
                    }
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                LogUtils.MyHandle.WriteToLog(1, "Thread was almost aborted in PostDataToURL but it was kept alive by force");
                System.Threading.Thread.ResetAbort();
            }
            return szResult;
        }


    }
}
