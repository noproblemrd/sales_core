﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.Notifications.SmsSuppliers
{
    internal abstract class SmsSupplierHandler
    { 
        #region Sms Suppliers Names
        protected const string ZumiSupplier1 = "ZumiSmsSupplier1";
        protected const string Shamir = "Shamir";
        protected const string Cellact = "Cellact";
        protected const string m_time = "m-time";
        #endregion

        internal abstract bool Send(SMSNotificationObject notificationDetails);

        /// <summary>
        /// This function goes all over the supplied text string and removes the tags from it
        /// </summary>
        /// <param name="text">A text containing tags</param>
        /// <returns>The string without the tags</returns>
        protected string StripTags(string text)
        {
            try
            {
                string tagprefix = "<";
                string tagsuffix = ">";
                string strippedText = text;

                int startindex = strippedText.IndexOf(tagprefix, 0);
                int tempStartIndex = 0;
                int endindex = 0;

                while (startindex != -1)
                {
                    tempStartIndex = strippedText.IndexOf(tagprefix, startindex);
                    endindex = strippedText.IndexOf(tagsuffix, startindex);

                    if (tempStartIndex != -1 && tempStartIndex < endindex)
                        startindex = tempStartIndex;

                    if (endindex == -1)
                        break;

                    strippedText = strippedText.Remove(startindex, endindex - startindex + 1);
                    endindex -= endindex - startindex;

                    startindex = strippedText.IndexOf(tagprefix, endindex);
                }

                return strippedText;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
