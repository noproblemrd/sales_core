﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Effect.Crm.Logs;
using System.Xml;

namespace NoProblem.Core.Notifications.SmsSuppliers
{
    internal class Standard : SmsSupplierHandler
    {
        internal override bool Send(SMSNotificationObject smsNotification)
        {
            bool retVal = false;
            if (smsNotification.WorkingMethod == SMSWorkingMethods.WebPage)
            {
                string message = StripTags(smsNotification.BodyText);
                if (smsNotification.Name == Shamir)
                {
                    message = System.Security.SecurityElement.Escape(message);
                }
                if (smsNotification.SendDataMethod == SMSSendData.Encoded)
                {
                    Byte[] messageBytes = Encoding.GetEncoding(smsNotification.Encoding).GetBytes(message);
                    message = BitConverter.ToString(messageBytes).Replace("-", "");
                }
                else
                {
                    message = System.Web.HttpUtility.UrlEncode(message);
                }
                string phone = System.Web.HttpUtility.UrlEncode(smsNotification.ToPhoneNumber);

                string data = string.Format(smsNotification.DataTemplate, phone, message, smsNotification.FromPhoneNumber, smsNotification.UniqueId);

                string response = PostDataToURL(smsNotification, smsNotification.URL, data);

                retVal = ProccessResponse(smsNotification, response);
            }
            return retVal;
        }

        private bool ProccessResponse(NotificationObect NotificationDetails, string Response)
        {
            LogUtils.MyHandle.WriteToLog(9, "ProccessResponse: Response = {0}", Response);
            bool smsSent = false;

            if (NotificationDetails.Name == ZumiSupplier1)//temp for Zumi.
            {
                return true;
            }

            XmlDocument output = new XmlDocument();
            output.LoadXml(Response);

            smsSent = ProcessShamirResponse(output);

            if (!smsSent)
            {
                smsSent = ProcessCellactResponse(output);
                if (smsSent)
                {
                    ((SMSNotificationObject)NotificationDetails).MessageId = output.GetElementsByTagName("SESSION")[0].InnerText;
                }
                else
                {
                    smsSent = ProcessMTimeResponse(output);
                    if (smsSent)
                    {
                        XmlNode messageID = output.SelectSingleNode("/SubmitResponse/MessageID");
                        ((SMSNotificationObject)NotificationDetails).MessageId = messageID.InnerText;
                    }
                }
            }

            return smsSent;
        }

        private bool ProcessShamirResponse(XmlDocument Response)
        {
            XmlNode messageNode = Response.SelectSingleNode("//Result");

            if (messageNode != null && messageNode.HasChildNodes)
            {
                LogUtils.MyHandle.WriteToLog(9, "The response was from shamir");
                // the sms is from shamir
                string statusCode = messageNode.SelectSingleNode("Status").InnerText;
                string Description = messageNode.SelectSingleNode("Description").InnerText;

                LogUtils.MyHandle.WriteToLog(9, "SMS Sending Return statusCode: {0} ,Description: {1}", statusCode, Description);

                if (statusCode == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        private bool ProcessMTimeResponse(XmlDocument Response)
        {


            // the sms is from m-time
            #region m-time response example
            /*<?xml version="1.0" encoding="UTF-8"?>
<SubmitResponse>
<Response>0</Response>
<ResponseMessage>Success</ResponseMessage>
<MessageID>23220246</MessageID>
<Info>
<Receiver>13581735108</Receiver>
<Encoding>UNIC</Encoding>
<Status>Queued for delivery</Status>
</Info>
</SubmitResponse>*/
            #endregion

            XmlNode responseNode = Response.SelectSingleNode("/SubmitResponse/Response");

            if (responseNode != null && responseNode.InnerText == "0")
            {

                return true;
            }
            return false;
        }

        private bool ProcessCellactResponse(XmlDocument Response)
        {
            XmlNode resultNode = Response.SelectSingleNode("//RESULT");

            if (resultNode != null)
            {
                if (resultNode.InnerText.ToLower() == "true")
                {
                    LogUtils.MyHandle.WriteToLog(9, "The response was from Cellact");
                    // the sms is from Cellact

                    return true;
                }
            }

            return false;
        }

        private string PostDataToURL(SMSNotificationObject notificationObject, string szUrl, string szData)
        {
            //Setup the web request
            string szResult = string.Empty;
            WebRequest Request = WebRequest.Create(szUrl);
            Request.Timeout = 30000;
            Request.Method = "POST";
            Request.ContentType = "application/x-www-form-urlencoded";
            //Set the POST data in a buffer
            byte[] PostBuffer;
            try
            {
                // replacing " " with "+" according to Http post RPC
                szData = szData.Replace(" ", "+");
                //Specify the length of the buffer
                if (!string.IsNullOrEmpty(notificationObject.Encoding))
                {
                    PostBuffer = Encoding.GetEncoding(notificationObject.Encoding).GetBytes(szData);
                }
                else
                {
                    PostBuffer = Encoding.UTF8.GetBytes(szData);
                }
                Request.ContentLength = PostBuffer.Length;
                //Open up a request stream
                using (Stream RequestStream = Request.GetRequestStream())
                {
                    //Write the POST data
                    RequestStream.Write(PostBuffer, 0, PostBuffer.Length);
                }
                //Create the Response object
                using (WebResponse Response = Request.GetResponse())
                {
                    //Create the reader for the response
                    if (!string.IsNullOrEmpty(notificationObject.Encoding))
                    {
                        using (StreamReader sr = new StreamReader(Response.GetResponseStream(), Encoding.GetEncoding(notificationObject.Encoding)))
                        {
                            szResult = sr.ReadToEnd();
                        }
                    }
                    else
                    {
                        using (StreamReader sr = new StreamReader(Response.GetResponseStream(), Encoding.UTF8))
                        {
                            szResult = sr.ReadToEnd();
                        }
                    }
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                LogUtils.MyHandle.WriteToLog(1, "Thread was almost aborted in PostDataToURL but it was kept alive by force");
                System.Threading.Thread.ResetAbort();
            }
            return szResult;
        }
             
    }
}
