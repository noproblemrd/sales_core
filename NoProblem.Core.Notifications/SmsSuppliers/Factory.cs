﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.Notifications.SmsSuppliers
{
    internal class Factory
    {
        internal static SmsSupplierHandler GetSmsSupplierHandler(string name)
        {
            switch (name)
            {
                case "Roottori":
                    return new Roottori();
                case "Gupshup":
                    return new Gupshup();
                case "ZumiSmsSupplier1":
                    return new ZumiSmsSupplier1();
                case "Twilio":
                    return new TwilioSmsSupplier();
                default:
                    return new Standard();
            }
        }
    }
}
