﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoProblem.Core.Notifications
{

    public enum SMSWorkingMethods
    {
        WebPage = 1,
        WebService,
        StoredProcedure
    }

    public enum SMSSendData
    {
        PlainText = 1,
        Encoded
    }
}
