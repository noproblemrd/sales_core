using System;
using Effect.Crm.SDKProviders.CrmServiceSdk;

namespace NoProblem.Core.Notifications
{
	/// <summary>
	/// An abstract class that holds information regarding to the notification, 
	/// all new notification method should also implement a derivetion of this class
	/// </summary>
	public abstract class NotificationObect
	{
		#region Properties

        public string Name { get; set; }

		/// <summary>
		/// The object to which the notification is connected
		/// </summary>
        public Guid RegardingObjectID { get; set; }
        
		/// <summary>
		/// The EntityName of the RegaringObject
		/// </summary>
        public string RegardingObjectType { get; set; }
        
		/// <summary>
		/// The message to be send
		/// </summary>
        public string BodyText { get; set; }		

		#endregion
	}
}
