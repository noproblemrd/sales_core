﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WhatSearchPoc.Models
{
    public class SearchModel
    {
        public string What { get; set; }
    }

    public class SearchResultModel
    {
        public string SearchTerm { get; set; }
        public List<string> Categories { get; set; }
        public List<string> CategoriesBySliderKeywords { get; set; }
        public List<string> GoogleCategories { get; set; }
        public List<string> YelpCategories { get; set; }
    }
}