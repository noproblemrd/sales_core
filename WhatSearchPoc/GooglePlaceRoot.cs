﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace WhatSearchPoc
{
    
    [DataContract(Name = "location")]
    public class GooglePlaceLocation
    {
        [DataMember]
        public double lat { get; set; }
        [DataMember]
        public double lng { get; set; }
    }
    [DataContract(Name = "geometry")]
    public class GooglePlaceGeometry
    {
        [DataMember]
        public GooglePlaceLocation location { get; set; }
    }
    [DataContract(Name = "opening_hours")]
    public class GooglePlaceOpeningHours
    {
        [DataMember]
        public bool open_now { get; set; }
        [DataMember]
        public List<object> weekday_text { get; set; }
    }
    [DataContract(Name = "photos")]
    public class GooglePlacePhoto
    {
        [DataMember]
        public int height { get; set; }
        [DataMember]
        public List<object> html_attributions { get; set; }
        [DataMember]
        public string photo_reference { get; set; }
        [DataMember]
        public int width { get; set; }
    }
    [DataContract(Name = "results")]
    public class GooglePlaceResult
    {
        [DataMember]
        public string formatted_address { get; set; }
        [DataMember]
        public GooglePlaceGeometry geometry { get; set; }
        [DataMember]
        public string icon { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public GooglePlaceOpeningHours opening_hours { get; set; }
        [DataMember]
        public List<GooglePlacePhoto> photos { get; set; }
        [DataMember]
        public string place_id { get; set; }
        [DataMember]
        public double rating { get; set; }
        [DataMember]
        public string reference { get; set; }
        [DataMember]
        public List<string> types { get; set; }

        public string details { get; set; }
    }
    [DataContract]
    internal class GooglePlaceResultJson
    {
        [DataMember]
        public List<object> html_attributions { get; set; }
        [DataMember]
        public string next_page_token { get; set; }
        [DataMember]
        public List<GooglePlaceResult> results { get; set; }
        [DataMember]
        public string status { get; set; }
    }
    
}