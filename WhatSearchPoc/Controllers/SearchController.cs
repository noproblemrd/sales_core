﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace WhatSearchPoc.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        public ActionResult Index()
        {
            return View();
        }

        // POST: Search/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                var what = collection.GetValue("What").AttemptedValue;



                Models.SearchResultModel resutls = new Models.SearchResultModel();
                resutls.Categories = QueryNpCategories(what, true);
                resutls.CategoriesBySliderKeywords = QueryNpCategories(what, false);
                resutls.GoogleCategories = QueryGoogle(what);
                resutls.YelpCategories = QueryYelp(what);
                resutls.SearchTerm = what;
                return View("SearchResults", resutls);
            }
            catch (Exception exc)
            {
                return View();
            }
        }

        private List<string> QueryYelp(string what)
        {
            var retval = new List<string>();
            Dictionary<string, int> count = new Dictionary<string, int>();
            YelpSharp.Yelp yelp = new YelpSharp.Yelp(new YelpSharp.Options()
            {
                AccessToken = "nPtMOw9paxwO60m2WMhtnbaHNMk2I5_b",
                AccessTokenSecret = "4FGtBgnPuJJ22S6VfuXuwfM9A38",
                ConsumerKey = "vrH3Clgic3uD6iyyt2c4Pg",
                ConsumerSecret = "cYoeUs9S4YjZQ5rx1t3Oz1oXt7Y"
            });
            string _city = "los angeles";
            var ans = yelp.Search(what, _city);
            if (ans != null && ans.businesses != null)
            {
                foreach (var item in ans.businesses)
                {
                    if (item != null && item.categories != null)
                        foreach (var t2 in item.categories)
                        {
                            if (t2 != null)
                                foreach (var t in t2)
                                {
                                    string cat = Convert.ToString(t);
                                    if (count.ContainsKey(cat))
                                    {
                                        count[cat] = count[cat] + 1;
                                    }
                                    else
                                    {
                                        count.Add(cat, 1);
                                    }
                                }
                        }
                }
            }
            foreach (var item in count.OrderByDescending(x => x.Value))
            {
                retval.Add(item.Key + " x" + item.Value);
            }
            return retval;
        }
        const string GoogleKey = "AIzaSyDd8dcimbypMHG0zBgpIuSna2LxCbeSU9Q";
        private List<string> QueryGoogle(string what)
        {
            
            var retval = new List<string>();
            Dictionary<string, int> count = new Dictionary<string, int>();
            RestSharp.RestClient client = new RestSharp.RestClient("https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + HttpUtility.UrlEncode(what) + "&key=AIzaSyDd8dcimbypMHG0zBgpIuSna2LxCbeSU9Q");
            //AIzaSyDJQ_-mhV9NHoI-q9kOS23Soqx97e9rq5E
            RestSharp.RestRequest request = new RestSharp.RestRequest();
            var response = client.Execute(request);
            GooglePlaceResultJson opr;
            using (var ms = new MemoryStream(response.RawBytes))
            {
                var serializer = new DataContractJsonSerializer(typeof(GooglePlaceResultJson));
                opr = (GooglePlaceResultJson)serializer.ReadObject(ms);
            }
            dynamic json = Newtonsoft.Json.JsonConvert.DeserializeObject(response.Content);
            foreach (var item in json.results)
            {
                foreach (var t in item.types)
                {
                    string cat = Convert.ToString(t);
                    if (count.ContainsKey(cat))
                    {
                        count[cat] = count[cat] + 1;
                    }
                    else
                    {
                        count.Add(cat, 1);
                    }
                }
            }
            foreach (var item in count.OrderByDescending(x => x.Value))
            {
                retval.Add(item.Key + " x" + item.Value);
            }
            return retval;
        }

        private List<string> QueryNpCategories(string what, bool cats)
        {
            var retval = new List<string>();

            using (SqlConnection con = new SqlConnection(dbString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(cats ? queryCats : queryKeywords, con))
                {
                    command.Parameters.AddWithValue("@what", "%" + what + "%");
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            retval.Add(Convert.ToString(reader[0]));
                    }
                }
            }

            return retval;
        }

        private const string dbString = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";

        private const string queryKeywords =
            @"
select distinct pe.new_name from new_primaryexpertise pe with(nolock)
join new_sliderkeyword sk with(nolock) on pe.new_primaryexpertiseid = sk.new_primaryexpertiseid
where new_keyword like @what
";

        private const string queryCats =
            @"
select new_name from new_primaryexpertise with(nolock)
where new_name like @what
";
    }
}
