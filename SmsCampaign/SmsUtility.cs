﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SmsCampaign
{
    public class SmsUtility
    {
        public static void GetSmsCallBack(string FromPhone, string message)
        {

            string phone = GetCleanPhoneFromTwillio(FromPhone);
            //     Guid YelpSupplierId = Guid.Empty;
            //     LogUtils.MyHandle.WriteToLog("GetSmsCallBack From: {0}, Fix Phone: {1}, Message: {2}", FromPhone,phone, message);
            if (!string.IsNullOrEmpty(phone))
            {
                string command = "EXEC [dbo].[sf_SetDoNotCallMeBySMS] @phone, @OriginalPhone, @SetDoNotCallMe, @Message";
                using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationSetting.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@phone", phone);
                        cmd.Parameters.AddWithValue("@OriginalPhone", FromPhone);
                        cmd.Parameters.AddWithValue("@SetDoNotCallMe", (message.ToLower().StartsWith("remove")));
                        cmd.Parameters.AddWithValue("@Message", message);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            //  InsertCallbackSMS(FromPhone, message, YelpSupplierId);
        }
        private static string GetCleanPhoneFromTwillio(string phone)
        {
            if (string.IsNullOrEmpty(phone))
                return null;
            //+17076907398
            if (phone.Length > 10)
                phone = phone.Substring(phone.Length - 10, 10);
            string result = string.Empty;
            foreach (char c in phone)
            {
                int i = (int)c;
                if (i > 47 && i < 58)
                    result += c;
            }


            return result;
        }
    }
}
