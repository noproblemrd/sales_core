﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace SmsCampaign
{
    public class CampaignManager
    {
        public enum eCampaignCreateResponse
        {
            SUCCESS,
            INVALID_TEXT,
            INVALID_NAME
        }
        public eCampaignCreateResponse CreateCampaign(CampaignData cd)
        {
            if (string.IsNullOrEmpty(cd.text) || cd.text.Length > 159)
                return eCampaignCreateResponse.INVALID_TEXT;
            if(string.IsNullOrEmpty(cd.name))
                return eCampaignCreateResponse.INVALID_NAME;
            string command = "insert into dbo.sf_Campaign (Text, Name) VALUES (@text, @name)";
            using(SqlConnection conn = new SqlConnection(ConfigurationSetting.ConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand(command,conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@text", cd.text);
                    cmd.Parameters.AddWithValue("@name", cd.name);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            return eCampaignCreateResponse.SUCCESS;
        }
        public CampaignData GetCampaignDetails(int CampaignId)
        {
            string command = "SELECT Text, Name FROM dbo.sf_Campaign WHERE Id = @CampaignId";
            CampaignData cd = null;
            using (SqlConnection conn = new SqlConnection(ConfigurationSetting.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@CampaignId", CampaignId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if(reader.Read())
                    {
                        cd = new CampaignData();
                        cd.name = (string)reader["Name"];
                        cd.text = (string)reader["Text"];
                    }
                    conn.Close();
                }
            }
            return cd;
        }
        public List<CampaignData> GetCampaignDetails()
        {
            string command = "SELECT Id, Text, Name FROM dbo.sf_Campaign";
            List<CampaignData> list = new List<CampaignData>();
            using (SqlConnection conn = new SqlConnection(ConfigurationSetting.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        CampaignData cd = new CampaignData();
                        cd.name = (string)reader["Name"];
                        cd.text = (string)reader["Text"];
                        cd.Id = (int)reader["Id"];
                        list.Add(cd);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        public void DeleteCampaign(int CampaignId)
        {
            string command = "DELETE FROM dbo.sf_Campaign WHERE Id = @CampaignId";
            using (SqlConnection conn = new SqlConnection(ConfigurationSetting.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@CampaignId", CampaignId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
    }
    public class CampaignData
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string text { get; set; }
    }
}