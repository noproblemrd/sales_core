﻿using Microsoft.VisualBasic.FileIO;
using NoProblem.Core.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace SmsCampaign
{
    public abstract class SmsManagerBase
    {
        /*
        //SMS_PHONE_NUMBER_CAMPAIGN
        readonly static string FROM_PHONE;// = "8888660885";
        static SmsManagerBase()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
        //    MOBILE_BID_TIMEOUT = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_BID_TIMEOUT));
            FROM_PHONE = configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.SMS_PHONE_NUMBER_CAMPAIGN);           
        }
         * */

        string CsvPath;
        string Message;
        protected virtual string GetMessage
        {
            get { return Message; }
        }
        public SmsManagerBase(string CsvPath, string message)
        {
            this.CsvPath = CsvPath;
            this.Message = message;
        }
        public virtual eSmsStatus Execute()
        {
            if (string.IsNullOrEmpty(Message) || Message.Length > 159)
                return eSmsStatus.MESSGAE_INVALID;
            if (!File.Exists(CsvPath))
                return eSmsStatus.FILE_NOT_FOUND;
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
            {
                using (TextFieldParser parser = new TextFieldParser(CsvPath))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string[] fields = parser.ReadFields();
                        foreach (string field in fields)
                        {
                            if (!CheckPhoneNumber(field))
                                continue;
                            if (SendSms(field))
                                InsertToDB(field);
                        }
                    }
                }
            }));
            return eSmsStatus.IN_PROCESS;
            
        }
        abstract protected void InsertToDB(string phone);
        protected virtual bool SendSms(string phone)
        {
           Notifications noti = new Notifications(null);
#if DEBUG
            if ("ISRAEL" == ConfigurationSetting.DefaultCountry && !phone.StartsWith("0"))
                return false;
#endif
            return noti.SendSMSTemplate(this.Message, phone, ConfigurationSetting.DefaultCountry, ConfigurationSetting.PhoneNumber);
        }
        protected bool CheckPhoneNumber(string phone)
        {
            Regex reg = new Regex("^[0-9]{10}$");
            return reg.IsMatch(phone);
        }
        public enum eSmsStatus
        {
            IN_PROCESS,
            FILE_NOT_FOUND,
            MESSGAE_INVALID
        }
        public class SmsManagerResponse
        {
            public eSmsStatus status { get; set; }
        }
       
        
    }
}
