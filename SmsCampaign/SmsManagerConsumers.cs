﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace SmsCampaign
{
    public class SmsManagerConsumers : SmsManagerBase
    {
        public SmsManagerConsumers(string CsvPath, string message):base(CsvPath, message)
        {

        }
       
        protected override void InsertToDB(string phone)
        {
            string command = "EXEC [dbo].[sf_InsertSMS] @phone, @Message";
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationSetting.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@phone", phone);
                    cmd.Parameters.AddWithValue("@Message", GetMessage);
                    conn.Close();
                }
            }
        }
    }
}
