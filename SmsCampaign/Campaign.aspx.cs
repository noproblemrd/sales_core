﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace SmsCampaign
{
    public partial class Campaign : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadCampains();
        }
        void LoadCampains()
        {
            CampaignManager cm = new CampaignManager();
            List<CampaignData> list = cm.GetCampaignDetails();
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Text", typeof(string));
            foreach(CampaignData cd in list)
            {
                DataRow row = dt.NewRow();
                row["Id"] = cd.Id;
                row["Name"] = cd.name;
                row["Text"] = cd.text;
                dt.Rows.Add("row");
            }
            _GridView.DataSource = dt;
            _GridView.DataBind();
        }
        protected void btn_Click(object sender, EventArgs e)
        {
            CampaignManager cm = new CampaignManager();
            CampaignManager.eCampaignCreateResponse result = cm.CreateCampaign(new CampaignData() { text = txt_message.Text, name = txt_CampaignName.Text });
            lbl_message.Text = result.ToString().Replace("_", " ");
        }

        protected void _GridView_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            
        }
    }
}