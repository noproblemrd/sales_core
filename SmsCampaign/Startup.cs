﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SmsCampaign.Startup))]
namespace SmsCampaign
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
