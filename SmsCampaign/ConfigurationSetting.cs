﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace SmsCampaign
{
    public class ConfigurationSetting
    {
        const string SMS_PHONE_NUMBER_CAMPAIGN = "SMS_PHONE_NUMBER_CAMPAIGN";
        const string DEFAULT_COUNTRY = "DEFAULT_COUNTRY";
        public static readonly string ConnectionString;
        public static readonly string PhoneNumber;
        public static readonly string DefaultCountry;
        static ConfigurationSetting()
        {
            ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string command = @"
select New_value
from dbo.New_configurationsettingExtensionBase
where New_key = @key";            
            using(SqlConnection conn = new SqlConnection(ConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@key", SMS_PHONE_NUMBER_CAMPAIGN);
                    PhoneNumber = (string)cmd.ExecuteScalar();
                    conn.Close();
                    cmd.Dispose();
                }
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@key", DEFAULT_COUNTRY);
                    DefaultCountry = (string)cmd.ExecuteScalar();
                    conn.Close();
                }
            }
        }
    }
}
