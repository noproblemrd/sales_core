﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Xml;
using System.Configuration;
using System.Xml.Linq;

namespace NoProblem.Core.Daemon
{
    public partial class Daemon : ServiceBase
    {
        private List<TimerHolder> holders;
        private string urlBase;
        private string configFilePath;

        public Daemon()
        {
            InitializeComponent();
            this.ServiceName = "NoProblemCoreDaemon";
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;
            ConfigureEventLogger();
        }

        protected override void OnStart(string[] args)
        {
            daemonEventLog.WriteEntry("NoProblemCoreDaemon starting...", EventLogEntryType.Information);
            configFilePath = ConfigurationManager.AppSettings["configFilePath"];
            holders = new List<TimerHolder>();
            ConfigurationContainer configs = ReadConfigs();
            urlBase = configs.UrlBase;
            foreach (var job in configs.Jobs)
            {
                StartJobExecutionTimer(job);
            }
        }

        protected override void OnStop()
        {
            daemonEventLog.WriteEntry("NoProblemCoreDaemon stopping...", EventLogEntryType.Information);
            foreach (var item in holders)
            {
                item.Timer.Dispose();
            }
        }

        private void ConfigureEventLogger()
        {
            if (!System.Diagnostics.EventLog.SourceExists("NoProblemCoreDaemonSource"))
            {
                System.Diagnostics.EventLog.CreateEventSource("NoProblemCoreDaemonSource", "NoProblemCoreDaemonLog");
            }
            daemonEventLog.Source = "NoProblemCoreDaemonSource";
            daemonEventLog.Log = "NoProblemCoreDaemonLog";
        }

        private ConfigurationContainer ReadConfigs()
        {
            ConfigurationContainer retVal = new ConfigurationContainer();
            using (XmlTextReader xmlReader = new XmlTextReader(configFilePath))
            {
                XDocument xDoc = XDocument.Load(xmlReader);
                XElement root = xDoc.Element("DaemonConfig");
                XElement jobs = root.Element("Jobs");
                foreach (XElement jobElement in jobs.Elements("Job"))
                {
                    DaemonJob job = new DaemonJob();
                    job.Method = jobElement.Attribute("Method").Value;
                    var execution = jobElement.Element("Execution");
                    string execType = execution.Attribute("Type").Value;
                    job.ExecutionType = (DaemonJob.ExecutionTypeCodes)Enum.Parse(typeof(DaemonJob.ExecutionTypeCodes), execType);
                    job.ExecutionValue = int.Parse(execution.Value);
                    foreach (var env in jobElement.Element("Environments").Elements("Env"))
                    {
                        job.Environments.Add(env.Value);
                    }
                    retVal.Jobs.Add(job);
                }
                XElement general = root.Element("General");
                retVal.UrlBase = general.Element("UrlBase").Value;
            }
            return retVal;
        }

        private void StartJobExecutionTimer(DaemonJob job)
        {
            JobExecuter executer = new JobExecuter(job, urlBase, daemonEventLog);

            TimerCallback callBack = new TimerCallback(executer.ExecuteJob);
            TimerHolder holder = new TimerHolder();
            holder.Callback = callBack;
            holder.JobExecuter = executer;
            Timer t;
            long intervalMiliSecs = job.ExecutionValue * 60 * 1000;
            if (job.ExecutionType == DaemonJob.ExecutionTypeCodes.Interval)
            {
                t = new Timer(callBack, null, 0, intervalMiliSecs);
            }
            else
            {
                // Figure how much time until the hour
                DateTime now = DateTime.Now;
                DateTime hour = DateTime.Today.AddHours(job.ExecutionValue);
                // If it's already past the hour, wait until that hour tomorrow
                if (now > hour)
                {
                    hour = hour.AddDays(1);
                }
                var spanToHour = hour - now;
                t = new Timer(callBack, null, spanToHour, new TimeSpan(1, 0, 0, 0));
            }
            holder.Timer = t;
            holders.Add(holder);
        }
    }
}
