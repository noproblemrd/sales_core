﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.Daemon
{
    internal class ConfigurationContainer
    {
        public List<DaemonJob> Jobs { get; private set; }
        public string UrlBase { get; set; }

        public ConfigurationContainer()
        {
            Jobs = new List<DaemonJob>();
        }
    }
}
