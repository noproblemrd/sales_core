﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration.Install;
using System.ComponentModel;
using System.ServiceProcess;

namespace NoProblem.Core.Daemon
{
    [RunInstaller(true)]
    public class DaemonInstaller : Installer
    {
        public DaemonInstaller()
        {
            var processInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();

            processInstaller.Account = ServiceAccount.User;
            serviceInstaller.DisplayName = "NoProblemCoreDaemon";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.ServiceName = "NoProblemCoreDaemon";

            this.Installers.Add(processInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
