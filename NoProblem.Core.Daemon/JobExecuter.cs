﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace NoProblem.Core.Daemon
{
    internal class JobExecuter
    {
        private System.Reflection.MethodInfo method;
        private DaemonJob job;
        private string urlBase;
        private EventLog daemonEventLog;

        internal JobExecuter(DaemonJob job, string urlBase, EventLog log)
        {
            this.job = job;
            this.urlBase = urlBase;
            method = typeof(OneCallUtilities.OneCallUtilities).GetMethod(job.Method);
            daemonEventLog = log;
        }

        internal void ExecuteJob(object obj)
        {
            daemonEventLog.WriteEntry(string.Format("Executing Job {0}", method), EventLogEntryType.Information);
            foreach (string env in job.Environments)
            {
                string url = string.Format(urlBase, env);
                Thread t1 = new Thread(new ParameterizedThreadStart(Send));
                t1.Start(url);
            }
        }

        private void Send(object url)
        {
            try
            {
                OneCallUtilities.OneCallUtilities service = new OneCallUtilities.OneCallUtilities();
                service.Url = (string)url;
                method.Invoke(service, null);
            }
            catch (Exception exc)
            {
                daemonEventLog.WriteEntry(string.Format("Exception in JobExecuter.Send async method. Message:{0} StackTrace:{1} InnerExceptionMessage:{2} InnerExceptionStackTrace:{3}", exc.Message, exc.StackTrace, exc.InnerException != null ? exc.InnerException.Message : "null", exc.InnerException != null ? exc.InnerException.StackTrace : "null"), EventLogEntryType.Error);
            }
        }
    }
}
