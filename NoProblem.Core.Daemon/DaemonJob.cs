﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.Daemon
{
    internal class DaemonJob
    {
        public int ExecutionValue { get; set; }
        public ExecutionTypeCodes ExecutionType { get; set; }
        public string Method { get; set; }
        public List<string> Environments { get; set; }

        public DaemonJob()
        {
            Environments = new List<string>();
            ExecutionType = ExecutionTypeCodes.Interval;
        }

        internal enum ExecutionTypeCodes
        {
            Interval, Daily
        }
    }
}
