﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.Daemon
{
    internal class TimerHolder
    {
        public Timer Timer { get; set; }
        public TimerCallback Callback { get; set; }
        public JobExecuter JobExecuter { get; set; }
    }
}
