﻿using System.Collections.Generic;
using System.Linq;
using ClipCall.ServiceModel.Abstract.DataContracts.Request;
using ClipCall.ServiceModel.Abstract.Services;
using NoProblem.Core.BusinessLogic.ClipCall.Customers;
using NoProblem.Core.BusinessLogic.ClipCall.Leads;
using NoProblem.Core.BusinessLogic.ClipCall.Suppliers;
using NoProblem.Core.DataModel.ClipCall;
using NoProblem.Core.DataModel.Xrm;

namespace ClipCall.ServiceModel.Implementation
{
    public class SuppliersService : ISuppliersService
    {
        private readonly SuppliersManager suppliersManager;
        private readonly CustomerManager customerManager;
        private readonly LeadsManager leadsManager;


        public SuppliersService(SuppliersManager suppliersManager, CustomerManager customerManager, LeadsManager leadsManager)
        {
            this.suppliersManager = suppliersManager;
            this.customerManager = customerManager;
            this.leadsManager = leadsManager;
        }

        public virtual void ConnectToCustomer(ConnecToCustomerRequestData request)
        {
            suppliersManager.ConnectToCustomer(request.SupplierId,
                                               request.IncidentAccountId,
                                               request.ShouldRecordCall);           
        }

        public virtual SupplierLoginResponseData Login(SupplierLoginRequestData request)
        {
            //TODO implement

            //contact customer = customerManager.FindCustomerByPhoneNumber(request.PhoneNumber);
            //if (customer == null)
            //{
            //    customerManager.CreateCustomer(request.PhoneNumber);
            //    customer = customerManager.FindCustomerByPhoneNumber(request.PhoneNumber);
            //}

            


            suppliersManager.Login(string.Empty);
            return null;
        }

        public virtual List<LeadReportStatusData> GetLeadReportStatuses()
        {
            var statuses =  this.leadsManager.GetLeadReportStatuses();
            return statuses.Select(s => new LeadReportStatusData
            {
                Name = s.Name,
                Id = s.Guid,
                ReasonCode = s.Code
            }).ToList();
        }

        public virtual void ReportLead(LeadReportRequestData request)
        {
            var leadReport = new LeadReport
            {
               Comment = request.Comment,
               SupplierId = request.SupplierId,
               IncidentAccountId = request.IncidentAccountId,
               ReasonCode = request.ReasonCode
            };
            leadsManager.ReportLead(leadReport);
        }
    }
}