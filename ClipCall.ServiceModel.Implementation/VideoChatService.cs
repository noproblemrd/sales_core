﻿using Castle.Core.Logging;
using ClipCall.DomainModel.Entities.VideoInvitation.Request;
using ClipCall.DomainModel.Entities.VideoInvitation.Response;
using ClipCall.ServiceModel.Abstract.Services;
using NoProblem.Core.BusinessLogic.ClipCall.Chat;
using System;

namespace ClipCall.ServiceModel.Implementation
{    
    public class VideoChatService : IVideoChatService
    {
        public  ILogger Logger { get; set; }

        private readonly VideoChatManager videoChatManager;

        public VideoChatService(VideoChatManager videoChatManager)
        {
            this.videoChatManager = videoChatManager;
        }


        public CreateVideoChatInvitationResponse CreateVideoChatInvitation(CreateInvitationRequest request)
        {
            CreateVideoChatInvitationResponse response = videoChatManager.CreateVideoChatInvitation(request.SupplierId, request.InvitationType);
            return response;            
        }

        public InvitationCheckInResponse InvitationCheckIn(VideoChatRequestBase request)
        {
            InvitationCheckInResponse response = videoChatManager.InvitationCheckIn(request.invitationId);
            return response;            
        }
        public void SetFavoriteServiceProvider(VideoChatCustomerBaseRequest request)
        {
            videoChatManager.SetFavoriteServiceProvider(request.invitationId, request.CustomerId);            
        }

        public CreateVideoChatConversationResponse CreateVideoChatCall(VideoChatCustomerBaseRequest request)
        {
            CreateVideoChatConversationResponse response = videoChatManager.CreateVideoChatCall(request.CustomerId, request.invitationId);
            return response;   
        }
        public void OnSentTextMessage(VideoChatSupplierBaseRequest request)
        {
            videoChatManager.OnSentTextMessage(request.SupplierId, request.invitationId);
        }
        public void OnSupplierAnswer(VideoChatSupplierBaseRequest request)
        {
            videoChatManager.OnSupplierAnswer(request.SupplierId, request.invitationId);
        }      
        public void OpentokArchiveStatusChanges(OpentokArchiveStatusChangesRequest request)
        {
            videoChatManager.OpentokArchiveStatusChanges(request.id, request.partnerId, request.sessionId, request.url, request.duration, request.status);
        }
        public GetFavoriteServiceProviderResponse GetFavoriteServiceProvider(InvitationFavoriteRequest request)
        {
            return videoChatManager.GetFavoriteServiceProvider(request.invitationId, request.ExpertiseId);
        }
    }
}
