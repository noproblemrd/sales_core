﻿using AutoMapper;
using Castle.Core.Logging;
using ClipCall.ServiceModel.Abstract.DataContracts.Request;
using ClipCall.ServiceModel.Abstract.DataContracts.Response;
using ClipCall.ServiceModel.Abstract.Services;
using NoProblem.Core.BusinessLogic.PublisherPortal;
using NoProblem.Core.DataModel.PublisherPortal;

namespace ClipCall.ServiceModel.Implementation
{    
    public class CustomersService : ICustomersService
    {
        public  ILogger Logger { get; set; }

        private readonly ConsumersManager customerManager;

        public CustomersService(ConsumersManager customerManager)
        {
            this.customerManager = customerManager;
        }


        public LeadDetailsResponseData GetLeadDetails(LeadDetailsRequestData request)
        {
            ConsumerIncidentData consumerIncidentData = customerManager.GetCustomerLead(request.CustomerId, request.LeadId);
            LeadDetailsResponseData leadDetails = Mapper.Map<ConsumerIncidentData, LeadDetailsResponseData>(consumerIncidentData);
            return leadDetails;
        }
    }
}
