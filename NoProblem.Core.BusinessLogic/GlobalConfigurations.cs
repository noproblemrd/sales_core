﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace NoProblem.Core.BusinessLogic
{
    static class GlobalConfigurations
    {
        private static DataAccessLayer.ConfigurationSettings configDal = new DataAccessLayer.ConfigurationSettings(null);
        private const string SITE_ID_CONFIG_NAME = "SiteId";
        private const int DEFAULT_BONUS_VALUE = 30;

        public static bool IsUsaSystem
        {
            get
            {
                return configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.USA_SYSTEM) == 1.ToString();
            }
        }

        public static string DefaultCountry
        {
            get
            {
                return configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.DEFAULT_COUNTRY);
            }
        }

        public static bool IsProductionUsa()
        {
            return ConfigurationManager.AppSettings.Get(SITE_ID_CONFIG_NAME) == DataModel.Constants.SiteIds.SALES;
        }

        private const string IS_DEV = "isDev";
        private static bool? isDev = null;
        public static bool IsDevEnvironment
        {
            get
            {
                if (!isDev.HasValue)
                {
                    isDev = ConfigurationManager.AppSettings.Get(IS_DEV) == 1.ToString();
                }
                return isDev.Value;
            }
        }

        public static int GetRegistrationBonusAmount()
        {
            string bonusStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.REGISTRATION_BONUS);
            int retVal;
            if (!int.TryParse(bonusStr, out retVal))
            {
                retVal = DEFAULT_BONUS_VALUE;
            }
            return retVal;
        }
    }
}
