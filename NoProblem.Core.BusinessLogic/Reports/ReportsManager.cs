﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: Effect.Onecall.BusinessLogic.Reports.ReportsManager
//  File: ReportsManager.cs
//  Description: Creates reports. Entry point for report's requests.
//  Author: Alain Adler
//  Company: Onecall
//  Createdon: 31/10/10
//
/////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.Reports;
using NoProblem.Core.DataModel.Reports.CallsReport;
using NoProblem.Core.DataModel.Reports.Request;
using NoProblem.Core.DataModel.Reports.Response;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Reports
{
    public class ReportsManager : XrmUserBase
    {
        #region Ctor

        public ReportsManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        #endregion

        #region Public methods

        public DataModel.Reports.KeywordReportResponse KeywordReport(DataModel.Reports.KeywordReportRequest request)
        {
            KeywordReportCreator creator = new KeywordReportCreator(XrmDataContext);
            return creator.Create(request.FromDate, request.ToDate, request.HeadingId, request.Keyword, request.IncludeUpsales);
        }

        public DataModel.Reports.DescriptionValidationGroupBySessionResponse DescriptionValidationGroupBySessionReport(DataModel.Reports.DescriptionValidationReportRequest request)
        {
            DescriptionValidationReportCreator creator = new DescriptionValidationReportCreator(XrmDataContext);
            return creator.CreateGroupBySession(request.FromDate, request.ToDate, request.QualityCheckName, request.PageSize, request.PageNumber, request.Passed, request.Flavor, request.ZipCodeCheckName, request.Event, request.Step, request.Control);
        }

        public List<DataModel.Reports.DescriptionValidationReportRow> DescriptionValidationSessionDrillDown(string session)
        {
            DescriptionValidationReportCreator creator = new DescriptionValidationReportCreator(XrmDataContext);
            return creator.SessionDrillDown(session);
        }

        public DataModel.Reports.DescriptionValidationBasicReportResponse DescriptionValidationBasicReport(DataModel.Reports.DescriptionValidationReportRequest request)
        {
            DescriptionValidationReportCreator creator = new DescriptionValidationReportCreator(XrmDataContext);
            return creator.CreateBasicReport(request.FromDate, request.ToDate, request.QualityCheckName, request.PageSize, request.PageNumber, request.Passed, request.Flavor, request.ZipCodeCheckName, request.Event, request.Step, request.Control);
        }

        public GetValidationReportDropDownListsResponse GetValidationReportDropDownLists()
        {
            DescriptionValidationReportCreator creator = new DescriptionValidationReportCreator(XrmDataContext);
            return creator.GetDropDownLists();
        }

        public DataModel.Reports.ExternalSupplierReportResponse ExternalSupplierReport(DataModel.PagingRequest request)
        {
            ExternalSupplierReportCreator creator = new ExternalSupplierReportCreator(XrmDataContext);
            return creator.Create(request.PageSize, request.PageNumber);
        }

        public DataModel.Reports.FullUrlExposureReportResponse FullUrlExposuresReport(DataModel.Reports.FullUrlExposureReportRequest request)
        {
            FullUrlExposuresReportCreator creator = new FullUrlExposuresReportCreator(XrmDataContext);
            return creator.Create(request.FromDate, request.ToDate, request.OriginId, request.PageSize, request.PageNumber);
        }

        public DataModel.Reports.RootUrlExposuresReportResponse RootUrlExposuresReport(DataModel.Reports.RootUrlExposuresReportRequest request)
        {
            RootUrlExposuresReportCreator creator = new RootUrlExposuresReportCreator(XrmDataContext);
            return creator.Create(request);
        }

        public DataModel.Reports.RootUrlExposuresReportResponse RootUrlExposuresKeywordDrillDown(DataModel.Reports.RootUrlExposuresKeywordDrillDownRequest request)
        {
            RootUrlExposuresReportCreator creator = new RootUrlExposuresReportCreator(XrmDataContext);
            return creator.KeywordDrillDown(request.FromDate, request.ToDate, request.RootUrl, request.OriginId, request.PageSize, request.PageNumber);
        }

        public DataModel.Reports.RootUrlExposuresReportResponse RootUrlExposuresFullUrlDrillDown(DataModel.Reports.RootUrlExposuresFullUrlRequest request)
        {
            RootUrlExposuresReportCreator creator = new RootUrlExposuresReportCreator(XrmDataContext);
            return creator.FullUrlDrillDown(request.FromDate, request.ToDate, request.RootUrl, request.OriginId, request.Keyword, request.PageSize, request.PageNumber);
        }

        public SupplierMoneyReportResponse SupplierMoneyReport(string supplierNumber)
        {
            SupplierMoneyReportCreator creator = new SupplierMoneyReportCreator(XrmDataContext);
            return creator.Create(supplierNumber);
        }

        public List<SupplierMoneyReportResponse> SupplierMoneyReportToExcel()
        {
            SupplierMoneyReportCreator creator = new SupplierMoneyReportCreator(XrmDataContext);
            return creator.CreateExcel();
        }

        public List<AarRequestsReportRow> AarRequestsReport(DML.Reports.AarRequestsReportsRequest request)
        {
            AarRequestsReportsCreator creator = new AarRequestsReportsCreator(XrmDataContext);
            return creator.Create(request.FromDate, request.ToDate, request.HeadingId);
        }

        public List<AarRequestReportDrillDownRow> AarRequestReportDrillDown(DML.Reports.AarRequestsReportsRequest request)
        {
            AarRequestsReportsCreator creator = new AarRequestsReportsCreator(XrmDataContext);
            return creator.CreateDrillDown(request.FromDate, request.ToDate, request.HeadingId);
        }

        public List<DML.Reports.SalesmanDeposits> SalesmenReport(DML.Reports.SalesmenReportRequest request)
        {
            SalesmenReportCreator creator = new SalesmenReportCreator(XrmDataContext);
            return creator.Create(request.FromDate, request.ToDate);
        }

        public List<DML.Reports.VoucherData> VoucherReport()
        {
            VouchersReportCreator creator = new VouchersReportCreator(XrmDataContext);
            return creator.Create();
        }

        public ConversionReportResponse ConversionReport(ConversionReportRequest request)
        {
            ConversionReportCreator creator = new ConversionReportCreator(XrmDataContext);
            var response = creator.Create(
                request.FromDate,
                request.ToDate,
                request.ExpertiseId,
                request.ControlName,
                request.Domain,
                request.OriginId);
            return response;
        }

        public ConversionReportResponse ConversionReport2(ConversionReportRequest request)
        {
            ConversionReportCreator creator = new ConversionReportCreator(XrmDataContext);
            var response = creator.Create2(
                request.FromDate,
                request.ToDate,
                request.ExpertiseId,
                request.ControlName,
                request.OriginId,
                request.Interval,
                request.interval_time_report,
                request.CountryId,
                request.SliderType);
            return response;
        }
        
        public List<ConversionDrillDownRow> ConversionDrillDown2(ConversionDrillDownRequest request)
        {
            ConversionReportCreator creator = new ConversionReportCreator(XrmDataContext);
            return creator.DrillDown2(request.from, request.to, request.ExpertiseId, request.ControlName, request.Interval, request.CountryId, request.SliderType);
        }

        public List<ConversionUsersReportResponse> ConversionReportUsers(ConversionReportUsersRequest request)
        {
            ConversionReportCreator creator = new ConversionReportCreator(XrmDataContext);
            return creator.CreateConversionUsersReport(request);
        }

        public List<ConversionUsersReportExposureResponse> ConversionReportUsersExposures(ConversionReportUsersRequest request)
        {
            ConversionReportCreator creator = new ConversionReportCreator(XrmDataContext);
            return creator.CreateConversionUsersReportExposures(request);
        }

        public RefundReportResponse RefundReport(RefundReportRequest request)
        {
            RefundReportCreator creator = new RefundReportCreator(XrmDataContext);
            return creator.Create(request.FromDate, request.ToDate, request.HeadingId, request.PageSize, request.PageNumber);
        }

        #region Calls Reports

        public DML.Reports.Response.Calls.CallsReportResponse CallsReportNoGroupBy(CallsReportRequest request)
        {
            CallsReportCreator creator = new CallsReportCreator(XrmDataContext);
            return creator.CreateNoGroupBy(request.FromDate, request.ToDate, request.ExpertiseId, request.CallType, request.UserId, request.SupplierOriginId, request.PageSize, request.PageNumber);
        }

        public DML.Reports.Response.Calls.CallsReportRevenuePerDateResponse CallsReportRevenuePerDate(CallsReportRequest request)
        {
            CallsReportCreator creator = new CallsReportCreator(XrmDataContext);
            return creator.CreateRevenuePerDate(request.FromDate, request.ToDate, request.ExpertiseId, request.CallType, request.SupplierOriginId, request.PageSize, request.PageNumber);
        }

        public DML.Reports.Response.Calls.CallsReportRevenuePerHeadingResponse CallsReportRevenuePerHeading(CallsReportRequest request)
        {
            CallsReportCreator creator = new CallsReportCreator(XrmDataContext);
            return creator.CreateRevenuePerHeading(request.FromDate, request.ToDate, request.ExpertiseId, request.CallType, request.SupplierOriginId, request.PageSize, request.PageNumber);
        }

        public DML.Reports.Response.Calls.CallsReportCountPerDateResponse CallsReportCountPerDate(CallsReportRequest request)
        {
            CallsReportCreator creator = new CallsReportCreator(XrmDataContext);
            return creator.CreateCountPerDate(request.FromDate, request.ToDate, request.ExpertiseId, request.CallType, request.SupplierOriginId, request.PageSize, request.PageNumber);
        }

        public DML.Reports.Response.Calls.CallsReportCountPerHeadingResponse CallsReportCountPerHeading(CallsReportRequest request)
        {
            CallsReportCreator creator = new CallsReportCreator(XrmDataContext);
            return creator.CreateCountPerHeading(request.FromDate, request.ToDate, request.ExpertiseId, request.CallType, request.SupplierOriginId, request.PageSize, request.PageNumber);
        }

        #endregion

        #region Requests Report

        public RequestsReportResponse RequestsReport(RequestsReportRequest request)
        {
            RequestsReportCreator creator = new RequestsReportCreator(XrmDataContext);
            return creator.Create(request.FromDate, request.ToDate, request.ExpertiseId, request.CaseNumber, request.Type,
                request.AffiliateOrigin, request.CaseOrigin, request.SuppliersProvided, request.PageNumber, request.PageSize,
                request.Url, request.OnlyForUpsale, request.ToolbarId, request.NumberOfUpsales, request.WithMaxCallDuration, 
                request.OnlyRejectByEmailOrName, request.SliderType);
        }

        #endregion

        public BalanceReportResponse BalanceReport(BalanceReportRequest request)
        {
            BalanceReportCreator creator = new BalanceReportCreator(XrmDataContext);
            return creator.Create(request.SupplierId, request.FromDate, request.ToDate);
        }

        /// <summary>
        /// Creates a report for the expertise given. Or all expertises if none given. Calculates how many advertisors
        /// work on the expertise and their status.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetExpertiseReportResponse GetExpertiseReport(GetExpertiseReportRequest request)
        {
            GetExpertiseReportResponse response = new GetExpertiseReportResponse();
            ExpertiseReportsCreator creator = new ExpertiseReportsCreator(XrmDataContext);
            List<DML.Reports.ExpertiseRow> report;
            if (request.ExpertiseId != Guid.Empty)
            {
                report = creator.CreateSpecificExpertiseReport(request.ExpertiseId, request.ExpertiseLevel);
            }
            else
            {
                report = creator.CreateAllExpertisesReport();
            }
            response.Report = report;
            return response;
        }

        public NonPpcCallsReportResponse NonPpcCallsReport(NonPpcCallsReportRequest request)
        {
            NonPpcCallsReportResponse response = new NonPpcCallsReportResponse();
            NonPpcCallsReportsCreator creator = new NonPpcCallsReportsCreator(XrmDataContext);
            List<DML.Reports.NonPpcCaseData> dataList = null;
            if (request.GroupBy == NonPpcCallsReportRequest.eGroupBy.None)
            {
                dataList = creator.NonPpcCallsReport(request.FromDate, request.ToDate, request.AdvertiserId);
            }
            else if (request.GroupBy == NonPpcCallsReportRequest.eGroupBy.Dates)
            {
                dataList = creator.NonPpcDatesCallsReport(request.FromDate, request.ToDate, request.AdvertiserId);
            }
            else
            {
                dataList = creator.NonPpcAdvertiserCallsReport(request.FromDate, request.ToDate, request.AdvertiserId);
            }
            response.Cases = dataList;
            return response;
        }

        public InvalidRequestsReportResponse InvalidRequestsReport(InvalidRequestsReportRequest request)
        {
            InvalidRequestsReportCreator creator = new InvalidRequestsReportCreator(XrmDataContext);
            return creator.Create(request);
        }

        public AdvertisersReportResponse AdvertisersReport(AdvertisersReportRequest request)
        {
            AdvertisersReportCreator creator = new AdvertisersReportCreator(XrmDataContext);
            return creator.Create(request);
        }

        public DepositReportResponse GetDepositsReport(DepositReportRequest request)
        {
            DepositsReportCreator creator = new DepositsReportCreator(XrmDataContext);
            return creator.Create(request.FromDate, request.ToDate, request.ExpertiseId, request.DepositType, request.CreatedBy, request.PageSize, request.PageNumber);
        }

        public DML.Upsale.GetAllOpenUpsalesResponse GetAllOpenUpsales(DML.Upsale.GetAllOpenUpsalesRequest request)
        {
            UpsaleReportCreator creator = new UpsaleReportCreator(XrmDataContext);
            return creator.GetAllOpenUpsales(request.ExpertiseId, request.FromDate, request.ToDate, request.CurrentPage, request.PageSize);
        }

        public DML.Upsale.UpsaleDataExpanded GetUpsaleData(Guid upsaleId)
        {
            UpsaleReportCreator creator = new UpsaleReportCreator(XrmDataContext);
            return creator.GetUpsaleData(upsaleId);
        }

        public PreDefinedPriceReportResponse PreDefinedPriceReport(PreDefinedPriceReportRequest request)
        {
            PreDefinedPriceReportCreator creator = new PreDefinedPriceReportCreator(XrmDataContext);
            return creator.Create(request);
        }

        public MyCallsReportResponse MyCallsReport(MyCallsReportRequest request)
        {
            throw new NotImplementedException();
        }

        public List<DataModel.Reports.OptOutAdvancedReportRow> OptOutAdvancedReport(OptOutReportsRequest request)
        {
            OptOutReportsCreator creator = new OptOutReportsCreator(XrmDataContext);
            return creator.CreateAdvancedReport(request.FromDate, request.ToDate, request.OriginId);
        }

        public List<DataModel.Reports.StringIntPair> OptOutRegualrReport(OptOutReportsRequest request)
        {
            OptOutReportsCreator creator = new OptOutReportsCreator(XrmDataContext);
            return creator.CreateRegularReport(request.FromDate, request.ToDate, request.OriginId);
        }

        public DistributionInstallationReportResponse DistributionInstallationReport(AddOnInstallationReportRequest request)
        {
            NoProblem.Core.BusinessLogic.Reports.Creators.DistributionInstallationReportCreator creator = new NoProblem.Core.BusinessLogic.Reports.Creators.DistributionInstallationReportCreator(XrmDataContext);
            return creator.Create(request);
        }
        public DistributionInstallationReportResponseDrillDown DistributionInstallationReportDrillDown(AddOnInstallationReportRequest request)
        {
            NoProblem.Core.BusinessLogic.Reports.Creators.DistributionInstallationReportCreator creator = new NoProblem.Core.BusinessLogic.Reports.Creators.DistributionInstallationReportCreator(XrmDataContext);
            return creator.CreateDrillDown(request);
        }

        public List<BrokersLeadsDataForReportResponse> BrokersLeadsReport(BrokersLeadsReportRequest request)
        {
            BrokersLeadsReportCreator creator = new BrokersLeadsReportCreator(XrmDataContext);
            return creator.Create(request.BrokerId, request.fromDate, request.toDate, request.IncludeNonBilledCalls);
        }

        public List<BrokerSummaryRow> BrokersSummaryReport(BrokersSummaryReportRequest request)
        {
            BrokersSummaryReportCreator creator = new BrokersSummaryReportCreator(XrmDataContext);
            return creator.Create(request.From, request.To, request.CalculateBillingRateByDates);
        }

        public ProfitableReportResponse ProfitableReport(ProfitableReportRequest request)
        {
            ProfitableReportCreator creator = new ProfitableReportCreator(XrmDataContext);
            return creator.Create(request);
        }
        public DateTime LastUpdateProfitableReport()
        {
            ProfitableReportCreator creator = new ProfitableReportCreator(XrmDataContext);
            return creator.GetLastUpdate();
        }

        public List<DistributorBillingReportIndividualRow> DistributorBillingReportIndividual(DistributorBillingReportRequest request)
        {
            DistributorBillingReportCreator creator = new DistributorBillingReportCreator(XrmDataContext);
            return creator.CreateIndividualDistributorBillingReport(request.OriginId, request.Year, request.Month);
        }

        public List<DistributorsReportSummaryRow> DistributorBillingReportSummary(DistributorBillingReportRequest request)
        {
            DistributorBillingReportCreator creator = new DistributorBillingReportCreator(XrmDataContext);
            return creator.CreateSummaryDistributorsBillingReport(request.Year, request.Month);
        }

        public AutoRerouteReportResponse AutoRerouteReport(DateTime fromDate, DateTime toDate)
        {
            AutoRerouteReportCreator creator = new AutoRerouteReportCreator(XrmDataContext);
            return creator.Create(fromDate, toDate);
        }

        public List<PpaControlReportRow> PpaControlReport(DateTime fromDate, DateTime toDate)
        {
            PpaControlReportCreator creator = new PpaControlReportCreator(XrmDataContext);
            return creator.Create(fromDate, toDate);
        }

        public List<NoProblem.Core.DataModel.Reports.Response.PplReportData> PplReport(NoProblem.Core.DataModel.Reports.Request.PplReportRequest request)
        {
            List<NoProblem.Core.DataModel.Reports.Response.PplReportData> result = new List<NoProblem.Core.DataModel.Reports.Response.PplReportData>();
            if (!string.IsNullOrEmpty(request.ZipCode))
            {
                object o;
                string _command = @"SELECT New_regionId
                                    FROM dbo.New_regionExtensionBase
                                    WHERE New_name = @RegionName";
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(_command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@RegionName", request.ZipCode);
                        o = cmd.ExecuteScalar();
                        conn.Close();
                    }
                }
                if (o is Guid)
                    request.RegionId = (Guid)o;
                if (request.RegionId == Guid.Empty)
                    return result;
            }


            string command;
            if (request.RegionId != Guid.Empty)
                command = "EXEC [dbo].[PPL_Report4.0] @ExpertiseId, @region";
            else
                /*
                command = @"SELECT accb.AccountId, AccExp.New_IncidentPrice Price, acc.Name, accb.New_LeadBuyerName
			                FROM dbo.AccountExtensionBase accb WITH(NOLOCK)
				                INNER JOIN dbo.New_accountexpertiseExtensionBase AccExp WITH(NOLOCK)
					                on accb.AccountId = AccExp.new_accountid			
				                INNER JOIN dbo.AccountBase acc WITH(NOLOCK)
					                on acc.AccountId = accb.AccountId
			                WHERE accb.New_status = 1 
				                AND AccExp.new_primaryexpertiseid = @ExpertiseId
                            ORDER BY AccExp.New_IncidentPrice DESC";
                 * */
                command = @"DECLARE @PaymentMethodVoucherId uniqueidentifier
                            SELECT @PaymentMethodVoucherId = New_paymentmethodId
                            FROM dbo.New_paymentmethodExtensionBase WITH(NOLOCK)
                            WHERE New_name = 'Voucher'
	
                            SELECT accb.AccountId, AccExp.New_IncidentPrice Price, acc.Name, accb.New_LeadBuyerName
                            FROM dbo.AccountExtensionBase accb WITH(NOLOCK)
                                INNER JOIN dbo.New_accountexpertiseExtensionBase AccExp WITH(NOLOCK)
                                    on accb.AccountId = AccExp.new_accountid			
                                INNER JOIN dbo.New_accountexpertiseBase AccExpB 
									on AccExp.New_accountexpertiseId = AccExpB.New_accountexpertiseId
                                INNER JOIN dbo.AccountBase acc WITH(NOLOCK)
                                    on acc.AccountId = accb.AccountId
                            WHERE accb.New_status = 1 
                                AND AccExpB.statecode = 0
                                AND AccExp.new_primaryexpertiseid = @ExpertiseId
                                AND (ISNULL(accb.New_IsLeadBuyer, 0) = 1 
		                            OR EXISTS(
			                            SELECT *
			                            FROM dbo.New_supplierpricingExtensionBase SEB WITH(NOLOCK)
			                            WHERE SEB.new_accountid = accb.AccountId
				                            AND  SEB.new_paymentmethodid <> @PaymentMethodVoucherId
		                            ))
                            ORDER BY AccExp.New_IncidentPrice DESC";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.CommandTimeout = 300;
                    conn.Open();
                    cmd.Parameters.AddWithValue("@ExpertiseId", request.HeadingId);
                    if (request.RegionId != Guid.Empty)
                        cmd.Parameters.AddWithValue("@region", request.RegionId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        PplReportData prd = new PplReportData();
                        prd.AccountId = (Guid)reader["AccountId"];
                        prd.AccountName = (string)reader["Name"];
                        prd.Price = (decimal)reader["Price"];
                        prd.LeadBuyerInterface = reader["New_LeadBuyerName"] == DBNull.Value ? string.Empty : (string)reader["New_LeadBuyerName"];
                        result.Add(prd);
                    }
                    conn.Close();
                }
            }
            return result;
        }

        public AdvertiserRegistrationResponse AdvertiserRegistrationReport(AdvertiserRegistrationRequest request)
        {
            AdvertiserRegistrationReportCreator creator = new AdvertiserRegistrationReportCreator(XrmDataContext);
            return creator.AdvertiserRegistrationReport(request);
        }

        public List<DistributionsInstallationsByIpRow> DistributaionInstallationsByIpReport(DistributionsInstallationsByIpRowRequest request)
        {
            DistributaionInstallationsByIpReportCreator creator = new DistributaionInstallationsByIpReportCreator();
            return creator.Create(request);
        }
        public NoProblem.Core.DataModel.Reports.Response.InjectionReportResponse InjectionReport(InjectionReportRequest request)
        {
            NoProblem.Core.BusinessLogic.Reports.Creators.InjectionReportCreator creator = new NoProblem.Core.BusinessLogic.Reports.Creators.InjectionReportCreator(XrmDataContext);
            return creator.Create(request);
        }
        public List<InjectionReportDrillDownResponse> InjectionReportDrillDown(InjectionReportRequest request)
        {
            NoProblem.Core.BusinessLogic.Reports.Creators.InjectionReportCreator creator = new NoProblem.Core.BusinessLogic.Reports.Creators.InjectionReportCreator(XrmDataContext);
            return creator.CreateDrillDown(request);
        }
        public List<AdEngineReportResponse> AdEngineReport(AdEngineReportRequest request)
        {
            NoProblem.Core.BusinessLogic.Reports.Creators.AdEngineReportCreator creator = new NoProblem.Core.BusinessLogic.Reports.Creators.AdEngineReportCreator(XrmDataContext);
            return creator.AdEngineReport(request);
        }
        public List<HeartBeatInjectionResponse> HeartBeatInjectionReport(HeartBeatInjectionRequest request)
        {
            NoProblem.Core.BusinessLogic.Reports.Creators.HeartBeatInjectionCreator creator = new NoProblem.Core.BusinessLogic.Reports.Creators.HeartBeatInjectionCreator(XrmDataContext);
            return creator.HeartBeatInjectionReport(request);
        }

        public List<NP_DistributionReportResponse> NP_DistributionReport(NP_DistributionReportRequest request)
        {
            NoProblem.Core.BusinessLogic.Reports.Creators.NP_DistributionReportCreator creator = new NoProblem.Core.BusinessLogic.Reports.Creators.NP_DistributionReportCreator(XrmDataContext);
            return creator.CreateDistributionReport(request);
        }
        public List<NP_DistributionReportOriginResponse> NP_DistributionOriginReport(NP_DistributionReportOriginRequest request)
        {
            NoProblem.Core.BusinessLogic.Reports.Creators.NP_DistributionReportCreator creator = new NoProblem.Core.BusinessLogic.Reports.Creators.NP_DistributionReportCreator(XrmDataContext);
            return creator.CreateDistributionOriginReport(request);
        }
        #endregion

    }
}
