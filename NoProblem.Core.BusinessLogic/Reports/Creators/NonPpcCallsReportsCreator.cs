﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class NonPpcCallsReportsCreator : XrmUserBase
    {
        #region Ctor
        internal NonPpcCallsReportsCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal List<DML.Reports.NonPpcCaseData> NonPpcCallsReport(DateTime fromDate, DateTime toDate, string advertiserId)
        {
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            DataAccessLayer.NonPpcRequest nonPpcDal = new NoProblem.Core.DataAccessLayer.NonPpcRequest(XrmDataContext);
            IEnumerable<DML.Xrm.new_nonppcrequest> requests = nonPpcDal.GetNonPpcRequestsByDates(fromDate, toDate);
            requests = FilterByAdvertiserId(requests, advertiserId);
            requests = FilterByStatus(requests, DML.Xrm.new_nonppcrequest.Status.Close);
            List<DML.Reports.NonPpcCaseData> dataList = new List<NoProblem.Core.DataModel.Reports.NonPpcCaseData>();
            foreach (var item in requests)
            {
                DML.Reports.NonPpcCaseData data = new NoProblem.Core.DataModel.Reports.NonPpcCaseData();
                data.ConsumerPhone = item.new_consumerphone;
                data.CreatedOn = FixDateToLocal(item.createdon.Value);
                data.SupplierId = item.new_advertiserid;
                data.SupplierName = item.new_advertisername;
                data.SupplierPhone = item.new_advertiserphone1;
                data.SupplierPhone2 = item.new_advertiserphone2;
                dataList.Add(data);
            }
            return dataList;
        }        

        internal List<DML.Reports.NonPpcCaseData> NonPpcAdvertiserCallsReport(DateTime fromDate, DateTime toDate, string advertiserId)
        {
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            DataAccessLayer.NonPpcRequest nonPpcDal = new NoProblem.Core.DataAccessLayer.NonPpcRequest(XrmDataContext);
            IEnumerable<DML.Xrm.new_nonppcrequest> requests = nonPpcDal.GetNonPpcRequestsByDates(fromDate, toDate);
            requests = FilterByAdvertiserId(requests, advertiserId);
            requests = FilterByStatus(requests, DML.Xrm.new_nonppcrequest.Status.Close);
            List<DML.Reports.NonPpcCaseData> dataList = new List<NoProblem.Core.DataModel.Reports.NonPpcCaseData>();
            foreach (var item in requests)
            {
                var existing = dataList.FirstOrDefault(x => x.SupplierId == item.new_advertiserid);
                if (existing == null)
                {
                    DML.Reports.NonPpcCaseData data = new NoProblem.Core.DataModel.Reports.NonPpcCaseData();
                    data.SupplierId = item.new_advertiserid;
                    data.SupplierName = item.new_advertisername;
                    data.SupplierPhone = item.new_advertiserphone1;
                    data.SupplierPhone2 = item.new_advertiserphone2;
                    data.NumberOfCalls = 1;
                    dataList.Add(data);
                }
                else
                {
                    existing.NumberOfCalls++;
                }
            }
            return dataList;
        }

        internal List<DML.Reports.NonPpcCaseData> NonPpcDatesCallsReport(DateTime fromDate, DateTime toDate, string advertiserId)
        {
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            DataAccessLayer.NonPpcRequest nonPpcDal = new NoProblem.Core.DataAccessLayer.NonPpcRequest(XrmDataContext);
            IEnumerable<DML.Xrm.new_nonppcrequest> requests = nonPpcDal.GetNonPpcRequestsByDates(fromDate, toDate);
            requests = FilterByAdvertiserId(requests, advertiserId);
            requests = FilterByStatus(requests, DML.Xrm.new_nonppcrequest.Status.Close);
            List<DML.Reports.NonPpcCaseData> dataList = new List<NoProblem.Core.DataModel.Reports.NonPpcCaseData>();
            foreach (var item in requests)
            {
                DateTime dateFixed = FixDateToLocal(item.createdon.Value).Date;
                var existing = dataList.FirstOrDefault(x => x.CreatedOn == dateFixed);
                if (existing == null)
                {
                    DML.Reports.NonPpcCaseData data = new NoProblem.Core.DataModel.Reports.NonPpcCaseData();
                    data.CreatedOn = dateFixed;
                    data.NumberOfCalls = 1;
                    dataList.Add(data);
                }
                else
                {
                    existing.NumberOfCalls++;
                }
            }
            return dataList;
        }

        #endregion

        #region Private Methods

        private IEnumerable<NoProblem.Core.DataModel.Xrm.new_nonppcrequest> FilterByAdvertiserId(IEnumerable<NoProblem.Core.DataModel.Xrm.new_nonppcrequest> requests, string advertiserId)
        {
            IEnumerable<NoProblem.Core.DataModel.Xrm.new_nonppcrequest> retVal = requests;
            if (string.IsNullOrEmpty(advertiserId) == false)
            {
                retVal = requests.Where(x => x.new_advertiserid == advertiserId);
            }
            return retVal;
        }

        private IEnumerable<NoProblem.Core.DataModel.Xrm.new_nonppcrequest> FilterByStatus(IEnumerable<NoProblem.Core.DataModel.Xrm.new_nonppcrequest> requests, NoProblem.Core.DataModel.Xrm.new_nonppcrequest.Status status)
        {
            IEnumerable<NoProblem.Core.DataModel.Xrm.new_nonppcrequest> retVal = requests.Where(x => x.statuscode.Value == (int)status);
            return retVal;
        }

        #endregion
    }
}
