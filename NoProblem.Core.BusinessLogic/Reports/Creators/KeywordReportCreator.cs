﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class KeywordReportCreator : XrmUserBase
    {
        protected static readonly string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
        protected static readonly string connectionStringProduction = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";
        public KeywordReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public DataModel.Reports.KeywordReportResponse Create(DateTime fromDate, DateTime toDate, Guid headingId, string keyword, bool IncludeUpsales)
        {
            DataAccessLayer.SlikerKeyword.SqlParametersList paramaters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_sliderkeyword>.SqlParametersList();
            paramaters.Add("@from", fromDate.Date);
            paramaters.Add("@to", toDate.Date);
            StringBuilder builder = new StringBuilder(queryProd);
            if (headingId != Guid.Empty)
            {
                builder.Append(whereHeadingId);
                paramaters.Add("@headingId", headingId);
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                builder.Append(whereKeyword);
                paramaters.Add("@keyword", "%" + keyword + "%");
            }
            if (!IncludeUpsales)
            {
                builder.Append(whereNotIncludeUpsales);
            }
            builder.Append(groupBy);
       //     DataAccessLayer.SlikerKeyword dal = new NoProblem.Core.DataAccessLayer.SlikerKeyword(XrmDataContext);
    //        var reader = dal.ExecuteReader(builder.ToString(), paramaters);
            DataModel.Reports.KeywordReportResponse response = new NoProblem.Core.DataModel.Reports.KeywordReportResponse();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new System.Data.SqlClient.SqlCommand(builder.ToString(), conn))
                {
                    conn.Open();
                    foreach(KeyValuePair<string, object> kvp in paramaters)
                    {
                        cmd.Parameters.AddWithValue(kvp.Key, kvp.Value);
                    }
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        DataModel.Reports.KeywordReportRow data = new NoProblem.Core.DataModel.Reports.KeywordReportRow();
                        data.Keyword = Convert.ToString(reader["new_keyword"]);
                        data.Heading = Convert.ToString(reader["heading"]);
                        data.Requests = (int)reader["reqCnt"];
                        data.Hits = (int)reader["Visitors"];
                        data.Exposures = (int)reader["expCnt"];
                        data.OptOutRest = (int)reader["optoutRestCnt"];
                        data.OptOutForever = (int)reader["optoutForeverCnt"];
                        data.NetActualRevenue = (decimal)reader["NetActualRevenueMoney"];
                        data.ActualRevenue = (decimal)reader["ActualRevenueMoney"];
                        data.PotentialRevenue = (decimal)reader["PotentialRevenueMoney"];
                        response.DataList.Add(data);
                    }
                    conn.Close();
                }
            }
            string ConnString = ConfigurationManager.AppSettings["updateConnectionString"];
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string command = @"select LastUpdate
                                    from dbo.LastUpdate
                                    where TableName = 'keywords_report_aggregation'";
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    response.Adjusted = (DateTime)cmd.ExecuteScalar();
                }
                conn.Close();
            }
            return response;
        }

        private const string query =
            @"
SELECT new_keyword
      ,K.new_primaryexpertiseid
      ,PE.new_name as heading
      ,sum(reqCnt) reqCnt
      ,sum(expCnt) expCnt
      ,sum(optoutRestCnt) optoutRestCnt
      ,sum(optoutForeverCnt) optoutForeverCnt
      ,sum(ActualRevenueMoney) ActualRevenueMoney
      ,sum(NetActualRevenueMoney) NetActualRevenueMoney
      ,sum(PotentialRevenueMoney) PotentialRevenueMoney
    ,sum(Visitors) Visitors
  FROM 
    tbl_keywords_report_aggregation K
    join new_primaryexpertise PE on PE.new_primaryexpertiseid = K.new_primaryexpertiseid
    where
        dateValue between @from and @to
";
        private const string queryProd =
            @"
SELECT new_keyword
      ,K.new_primaryexpertiseid
      ,PE.new_name as heading
      ,sum(reqCnt) reqCnt
      ,sum(expCnt) expCnt
      ,sum(optoutRestCnt) optoutRestCnt
      ,sum(optoutForeverCnt) optoutForeverCnt
      ,sum(ActualRevenueMoney) ActualRevenueMoney
      ,sum(NetActualRevenueMoney) NetActualRevenueMoney
      ,sum(PotentialRevenueMoney) PotentialRevenueMoney
    ,sum(Visitors) Visitors
  FROM 
    dbo.keywords_report_aggregation K
    left join new_primaryexpertise PE on PE.new_primaryexpertiseid = K.new_primaryexpertiseid
    where
        dateValue between @from and @to
";

        private const string groupBy =
            @"
    group by 
        new_keyword
        ,K.new_primaryexpertiseid
        ,PE.new_name
";

        private const string whereHeadingId =
            @"
    and K.new_primaryexpertiseid = @headingId
";

        private const string whereKeyword =
            @"
    and new_keyword like @keyword
";
        private const string whereNotIncludeUpsales =
            @"
    and K.isupsale = 0
";
    }
}
/*
SELECT new_keyword
      ,K.new_primaryexpertiseid
      ,PE.new_name as heading
      ,sum(reqCnt) reqCnt
      ,sum(expCnt) expCnt
      ,sum(optoutRestCnt) optoutRestCnt
      ,sum(optoutForeverCnt) optoutForeverCnt
      ,sum(ActualRevenueMoney) ActualRevenueMoney
      ,sum(NetActualRevenueMoney) NetActualRevenueMoney
      ,sum(PotentialRevenueMoney) PotentialRevenueMoney
    ,sum(Visitors) Visitors
  FROM 
    dbo.keywords_report_aggregation K
    join new_primaryexpertise PE on PE.new_primaryexpertiseid = K.new_primaryexpertiseid
    where
        dateValue between '2013-12-30' and '2014-01-01'
          group by 
        new_keyword
        ,K.new_primaryexpertiseid
        ,PE.new_name
*/