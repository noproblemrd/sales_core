﻿using System;
using System.Collections.Generic;
using NoProblem.Core.DataModel.Upsale;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class UpsaleReportCreator : XrmUserBase
    {
        #region Ctor
        internal UpsaleReportCreator(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        internal GetAllOpenUpsalesResponse GetAllOpenUpsales(Guid expertiseId, DateTime fromDate, DateTime toDate, int currentPage, int pageSize)
        {
            FixDatesToFullDays(ref fromDate, ref toDate);            
            DataAccessLayer.Upsale dal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
            int totalRows;
            List<DML.Upsale.UpsaleData> dataList = dal.GetAllOpenUpsales(fromDate, toDate, expertiseId, currentPage, pageSize, out totalRows, true);
            GetAllOpenUpsalesResponse response = new GetAllOpenUpsalesResponse();
            response.DataList = dataList;
            response.CurrentPage = currentPage;
            response.TotalRows = totalRows;
            response.TotalPages = (int)Math.Ceiling(((double)totalRows) / ((double)pageSize));
            return response;
        }

        internal DML.Upsale.UpsaleDataExpanded GetUpsaleData(Guid upsaleId)
        {
            DataAccessLayer.Upsale dal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
            DML.Upsale.UpsaleDataExpanded retVal = dal.GetUpsaleData(upsaleId);
            retVal.CreatedOn = FixDateToLocal(retVal.CreatedOn);
            if (retVal.TimeToCall == DateTime.MinValue)
            {
                retVal.TimeToCall = FixDateToLocal(DateTime.Now);
            }
            foreach (var item in retVal.ConnectedSuppliers)
            {
                item.CallTime = FixDateToLocal(item.CallTime);
            }
            return retVal;
        }
    }
}
