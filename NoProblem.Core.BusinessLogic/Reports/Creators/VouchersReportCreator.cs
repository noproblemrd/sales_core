﻿using System.Collections.Generic;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class VouchersReportCreator : XrmUserBase
    {
        internal VouchersReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        internal List<DataModel.Reports.VoucherData> Create()
        {
            DataAccessLayer.Voucher dal = new NoProblem.Core.DataAccessLayer.Voucher(XrmDataContext);
            List<DataModel.Reports.VoucherData> dataList = dal.GetAllVouchers();
            foreach (var item in dataList)
            {
                item.CreatedOn = FixDateToLocal(item.CreatedOn);
            }
            return dataList;
        }
    }
}
