﻿using NoProblem.Core.DataModel.Reports.Request;
using NoProblem.Core.DataModel.Reports.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.Reports.Creators
{
    internal class NP_DistributionReportCreator : XrmUserBase
    {
         #region Ctor
        internal NP_DistributionReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion
        #region Fields
        const string DistributionReportQuery = @"
SELECT
	ORIGIN.New_originId OriginId
	,ORIGIN.New_name OriginName
	,ISNULL(INS.Installations, 0) Installs
    ,ISNULL(UNIQUE_INSTALL.UniqueInstallations, 0) UniqueInstalls
    ,ISNULL(UNIQUE_INSTALL.EffectiveUniqueInstall, 0) EffectiveInstalls
	,ISNULL(UNIQUE_INSTALL.CostofInstallation, 0) Cost  
    
    FROM
    (
	SELECT OriginId
		,SUM(Installations) Installations            
    FROM [dbo].[InstallationReportAgg6.0] WITH(NOLOCK)		
    WHERE Date >= @from AND Date < @to  
    GROUP BY OriginId
) INS
FULL OUTER JOIN
(
	SELECT OriginId
		,COUNT(*) UniqueInstallations
		,CAST(SUM(Factor) as int) EffectiveUniqueInstall
		,SUM(CostofInstallation * Factor) CostofInstallation	
	FROM [dbo].[InstallationIpOrigin6.2]
	WHERE Date >= @from AND Date < @to  
       
	 GROUP BY OriginId
)UNIQUE_INSTALL
	on UNIQUE_INSTALL.OriginId = INS.OriginId
INNER JOIN
(
	SELECT oe.New_originId, oe.New_name
	FROM dbo.New_originExtensionBase oe WITH(NOLOCK)
		INNER JOIN dbo.New_originBase o WITH(NOLOCK)
			on oe.New_originId = o.New_originId
	WHERE oe.New_IsDistribution = 1
		AND o.deletionstatecode = 0
)ORIGIN
	on ORIGIN.New_originId = COALESCE(INS.OriginId, UNIQUE_INSTALL.OriginId)
ORDER BY ORIGIN.New_name";

        const string DistributionReportOriginQuery = @"
SELECT
		D.Date	
		,ISNULL(FULL_INS.Installs, 0) Installs
		,ISNULL(INS.Installs, 0) UniqueInstalls
		,ISNULL(INS.EffectiveInstalls, 0) EffectiveInstalls
		,INS.Cost
		,ISNULL(INS.Total, 0) as CalcTotal
		,ISNULL(INS.EffectiveInstalls * INS.Cost, 0) as FixTotal
	FROM
	(
		SELECT 
		[Date]		
		,COUNT(*) Installs
		,CAST(SUM(Factor) as int) EffectiveInstalls
		,AVG([CostofInstallation]) Cost
		,SUM([CostofInstallation] * Factor) Total
	  FROM [dbo].[InstallationIpOrigin6.2]
		
	  WHERE [Date] >= @from
			AND [Date] < @to
			AND [OriginId] = @OriginId
		GROUP BY [Date]
		
	)INS
	FULL OUTER JOIN
	(
		SELECT [Date], SUM(Installations) Installs            
		FROM [dbo].[InstallationReportAgg6.0] WITH(NOLOCK)		
		WHERE Date >= @from AND Date < @to  
			AND OriginId = @OriginId
		GROUP BY [Date]
	)FULL_INS
		on INS.Date = FULL_INS.Date
	FULL OUTER JOIN
	(	
		SELECT [Date] = DATEADD(Day,Number,@from) 
		FROM  master..spt_values 
		WHERE Type='P' AND
		 DATEADD(day,Number,@from) < @to
	)D on D.Date = COALESCE(INS.Date, FULL_INS.Date)
	ORDER BY [Date]";
        #endregion
        internal List<NP_DistributionReportResponse> CreateDistributionReport(NP_DistributionReportRequest request)
        {
            List<NP_DistributionReportResponse> list = new List<NP_DistributionReportResponse>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand(DistributionReportQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@from", request.From);
                    cmd.Parameters.AddWithValue("@to", request.To);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        NP_DistributionReportResponse row = new NP_DistributionReportResponse();
                        row.OriginId = (Guid)reader["OriginId"];
                        row.OriginName = (string)reader["OriginName"];
                        row.Installs = (int)reader["Installs"];
                        row.UniqueInstalls = (int)reader["UniqueInstalls"];
                        row.EffectiveInstalls = (int)reader["EffectiveInstalls"];
                        row.Cost = (decimal)reader["Cost"];

                        list.Add(row);
                    }
                    conn.Close();
                }
            }
            return list;
        }

        internal List<NP_DistributionReportOriginResponse> CreateDistributionOriginReport(NP_DistributionReportOriginRequest request)
        {
            List<NP_DistributionReportOriginResponse> list = new List<NP_DistributionReportOriginResponse>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(DistributionReportOriginQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@from", request.From);
                    cmd.Parameters.AddWithValue("@to", request.To);
                    cmd.Parameters.AddWithValue("@OriginId", request.OriginId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        NP_DistributionReportOriginResponse row = new NP_DistributionReportOriginResponse();
                        row.Date = (DateTime)reader["Date"];
                        row.Installs = (int)reader["Installs"];
                        row.UniqueInstalls = (int)reader["UniqueInstalls"];
                        row.EffectiveInstalls = (int)reader["EffectiveInstalls"];
                        row.CalcCost = (decimal)reader["CalcTotal"];
                        row.FixCost = (decimal)reader["FixTotal"];

                        list.Add(row);
                    }
                    conn.Close();
                }
            }
            return list;
        }
    }
}
