﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class BrokersLeadsReportCreator : XrmUserBase
    {
        public BrokersLeadsReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<BrokersLeadsDataForReportResponse> Create(Guid brokerId, DateTime fromDate, DateTime toDate, bool IncludeNonBilledCalls)
        {
            List<BrokersLeadsDataForReportResponse> response = new List<BrokersLeadsDataForReportResponse>();
            DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataAccessLayer.IncidentAccount.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_incidentaccount>.SqlParametersList();
            parameters.Add("@brokerId", brokerId);

            parameters.Add("@from", fromDate);
            parameters.Add("@to", toDate);
            string _query;
            if (!IncludeNonBilledCalls)
                _query = String.Format(query, " and i.New_tocharge = 1");
            else
                _query = String.Format(query, " ");
            var reader = dal.ExecuteReader(_query, parameters);
            foreach (var row in reader)
            {
                BrokersLeadsDataForReportResponse data = new BrokersLeadsDataForReportResponse();
                data.BrokersId = Convert.ToString(row["new_dialercallid"]);
                data.CreatedOn = (DateTime)row["new_createdonlocal"];
                data.HeadingName = Convert.ToString(row["new_primaryexpertiseidname"]);
                data.IncidentId = (Guid)row["incidentid"];
                data.IsMarkedAsBadManually = row["new_tocharge"] != DBNull.Value ? !(bool)row["new_tocharge"] : true;
                data.Notes = Convert.ToString(row["new_rejectedreason"]);
                data.Price = (decimal)row["new_potentialincidentprice"];
                data.RecordingLocation = Convert.ToString(row["new_recordfilelocation"]);
                data.Region = Convert.ToString(row["new_regionidname"]);
                data.TicketNumber = Convert.ToString(row["ticketnumber"]);
                data.LastUpdate = (DateTime)row["lastUpdate"];
                if (row["duration"] != DBNull.Value)
                    data.Duration = (int)row["duration"];

                data.Phone = Convert.ToString(row["new_telephone1"]);
                response.Add(data);
            }
            return response;
        }
        
        private const string query =
        @"
declare @lastUpdate datetime
select @lastUpdate = dbo.fn_UTCToLocalTimeByTimeZoneCode( MAX(lastUpdate), (select new_value from new_configurationsetting where new_key = 'PublisherTimeZone') ) from tbl_broker_calls

SELECT b.new_createdonlocal
      ,incidentid
      ,ticketnumber
      ,new_primaryexpertiseidname
      ,new_regionidname
      ,b.new_potentialincidentprice
      ,b.new_tocharge
      ,b.new_dialercallid
      ,b.new_recordfilelocation
      ,b.new_rejectedreason
      ,duration
      ,new_telephone1
      ,@lastUpdate lastUpdate
  FROM tbl_broker_calls b
	inner join dbo.New_incidentaccountExtensionBase i  on i.new_incidentid = b.incidentid and i.new_accountid = b.new_accountid
  where b.new_accountid= @brokerId	
  and b.new_createdonlocal between @from and @to  
  {0}

union

SELECT b.new_createdonlocal
      ,incidentid
      ,ticketnumber
      ,new_primaryexpertiseidname
      ,new_regionidname
      ,b.new_potentialincidentprice
      ,b.new_tocharge
      ,b.new_dialercallid
      ,b.new_recordfilelocation
      ,b.new_rejectedreason
      ,duration
      ,new_telephone1
      ,@lastUpdate lastUpdate
  FROM tbl_broker_calls b
	inner join dbo.New_incidentaccountExtensionBase i  on i.new_incidentid = b.incidentid
    inner join Account acc with(nolock) on acc.ParentAccountId = b.new_accountid
  where b.new_accountid= @brokerId	
  and b.new_createdonlocal between @from and @to  
  {0}
";
    }
}
