﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Reports;
using NoProblem.Core.DataModel.Reports.Response;
using NoProblem.Core.DataModel.Reports.Request;
using System.Configuration;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class ConversionReportCreator : XrmUserBase
    {
        #region Ctor
        internal ConversionReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Drill Down Report Methods
        /*
        internal List<ConversionDrillDownRow> DrillDown(DateTime date, Guid expertiseId, string controlName, string domain)
        {
            date = date.Date;
            DataAccessLayer.Exposure.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_exposure>.SqlParametersList();
            parameters.Add("@date", date);
            StringBuilder expoQuery = BuildDrillDownQuery(expertiseId, controlName, domain, parameters);
            DataAccessLayer.Exposure expoDal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
            var reader = expoDal.ExecuteReader(expoQuery.ToString(), parameters);
            List<ConversionDrillDownRow> retVal = new List<ConversionDrillDownRow>();
            foreach (var row in reader)
            {
                ConversionDrillDownRow data =  CreateDrillDownRowDataObject(row);
                retVal.Add(data);
            }
            return retVal;
        }
         * */
        internal List<ConversionDrillDownRow> DrillDown2(DateTime _from, DateTime _to, Guid expertiseId, string controlName, eConversionInterval Interval, 
            int CountryId, int SliderType)
        {
            //date = date.Date;
            List<ConversionDrillDownRow> retVal = new List<ConversionDrillDownRow>();
            
            _to = (Interval == eConversionInterval.HOUR) ? _to.AddHours(1) : _to.AddDays(1);
           string _connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
       //       string _connectionString = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                string _query = Interval == eConversionInterval.DAILY_AVERAGE ? DrillDownAverageQuery : qeuryDrillDown2;
                StringBuilder query = new StringBuilder(_query);
                
                string WhereConversionReport = string.Empty;
                string Whereincident = string.Empty;
                string WhereDAUS = string.Empty;
                string WhereREVENUE = string.Empty;
                string WhereInstalls = string.Empty;
                if (!string.IsNullOrEmpty(controlName))
                {
                    WhereConversionReport += " and ControlName = @ControlName ";
                    Whereincident += " and New_ControlName = @ControlName ";
                    WhereREVENUE += " and 1 = 0 ";
                    cmd.Parameters.AddWithValue("@ControlName", controlName);
                }
                if (expertiseId != Guid.Empty)
                {
                    WhereConversionReport += " and PrimaryExpertiseId = @expertiseId ";
                    Whereincident += " and new_primaryexpertiseid = @expertiseId ";
                    WhereREVENUE += " and new_primaryexpertiseid = @expertiseId ";
                    cmd.Parameters.AddWithValue("@expertiseId", expertiseId);
                }
                if (CountryId > -1)
                {
                    query.Insert(0, "DECLARE @country nvarchar(150) = [DynamicNoProblem].[dbo].[GetCountryIP2LOCATION_ByCountryId](@CountryId) " + Environment.NewLine);
                    WhereConversionReport += " and Country = @country ";
                    Whereincident += " and New_Country = @country ";
                    WhereDAUS += " and CountryId = @CountryId ";
                    WhereREVENUE += " and Country = @country ";
                    WhereInstalls += " and Country = @country ";
                    cmd.Parameters.AddWithValue("@CountryId", CountryId);
                }
                if(SliderType > 0)
                {
                    WhereConversionReport += " AND SliderType = @SliderType ";
                    Whereincident += " AND b.new_Type = @SliderType ";
                    WhereREVENUE += " AND SliderType = @SliderType ";
                    cmd.Parameters.AddWithValue("@SliderType", SliderType);
                }
                query.Replace("{WhereConversionReport}", WhereConversionReport);
                query.Replace("{Whereincident}", Whereincident);
                query.Replace("{WhereDAUS}", WhereDAUS);
                query.Replace("{WhereREVENUE}", WhereREVENUE);
                query.Replace("{WhereInstalls}", WhereInstalls);
                cmd.Parameters.AddWithValue("@from", _from);
                cmd.Parameters.AddWithValue("@to", _to);
                cmd.Connection = conn;
                cmd.CommandText = query.ToString();
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {                    
                    ConversionDrillDownRow cddr = new ConversionDrillDownRow();
                    cddr.Origin = (reader["Origin"] == DBNull.Value) ? "!!!NULL!!!" : (string)reader["Origin"];
                    cddr.DAUS = (int)reader["DAUS"];
                    cddr.ExposureCount = (int)reader["Exposures"];
                    cddr.ExposureHits = (int)reader["ExpHits"];
                    cddr.RequestCount = (int)reader["TotalRequests"];
                    cddr.CallCount = (int)reader["Calls"];
                    cddr.LogicHits = (int)reader["LogicHits"];
                    cddr.Revenue = (decimal)reader["Rev"];
                    cddr.Cost = (decimal)reader["Cost"];
                    cddr.NonUpsalesCount = (int)reader["Pure_Request"];
                    cddr.UpsalesCount = (int)reader["_Upsold"];
                    cddr.StoppedRequests = (int)reader["Stoped"];
                    cddr.UniqueInstalls = (int)reader["UniqueInstallations"];
                    cddr.SetValues();
                    retVal.Add(cddr);
                }
                conn.Close();
            }
            return retVal;
        }
        const string qeuryDrillDown2 = @"
select @to = dateadd(millisecond, -10, @to)

select
	 dbo.GetOriginNameByOriginId(coalesce(conversion.OriginId, DAUS.OriginId, REVENUE.new_originid, INSTALL.OriginId)) Origin
	 ,ISNULL(INSTALL.UniqueInstallations, 0) UniqueInstallations
	 ,ISNULL(conversion.Exposures, 0) Exposures
    ,ISNULL(conversion.TotalRequests, 0) TotalRequests
    ,ISNULL(conversion.Calls, 0) Calls
    ,ISNULL(conversion.Hits, 0) ExpHits
    ,ISNULL(DAUS.users, 0) DAUS
    ,ISNULL(DAUS.Hits, 0) LogicHits
    ,ISNULL(REVENUE.Revenue, 0) Rev
    ,ISNULL(REVENUE.Cost, 0) Cost
    ,ISNULL(PureRequests.requests, 0) Pure_Request
    ,ISNULL(upsales.upsolds, 0) _Upsold
    ,ISNULL(Stoped.cnt, 0) Stoped
	

from
(	
	SELECT       
      OriginId
      ,sum([Exposures]) Exposures
      ,sum([Requests]) TotalRequests
      ,sum([Calls]) Calls
     ,SUM([Hits]) Hits
  FROM [dbo].[ConversionReportMongo] 
  where Date between @from and @to  
    {WhereConversionReport}
  group by OriginId
  
) conversion 
FULL OUTER JOIN
(
    SELECT  OriginId, sum(Visitor) users, SUM(Hits) Hits
	FROM [dynamicNoProblem].[dbo].[Exposure]
	WHERE 
		 Date between @from and @to
        {WhereDAUS} 
	GROUP BY  OriginId       
   
)DAUS

    on conversion.OriginId = DAUS.OriginId
  left join
  (
	 select SUM(Requests) requests, OriginId
		from [dbo].[ConversionReportMongo] 
		where isnull(IsUpsale, 0) = 0
			and Date between @from and @to
            {WhereConversionReport}
		group by OriginId
  ) PureRequests
  on PureRequests.OriginId = COALESCE(conversion.OriginId, DAUS.OriginId) 
  left join
  (
	select SUM([Requests]) upsolds, OriginId
		from [dbo].[ConversionReportMongo] 
		where IsUpsale = 1
			and Date between @from and @to
            {WhereConversionReport}
		group by OriginId
  )upsales
  on upsales.OriginId = COALESCE(conversion.OriginId, DAUS.OriginId)
  left join
  (	
	select count(*) cnt, b.new_originid
	 from dbo.incidentextensionbase b with(nolock)
		inner join dbo.IncidentBase i on i.IncidentId = b.IncidentId
	where new_createdonlocal between @from and @to
	and new_isstoppedmanually = 1
        {Whereincident}
	group by b.new_originid
  ) Stoped
  on Stoped.new_originid = COALESCE(conversion.OriginId, DAUS.OriginId)
  full outer join
  (	
		select new_originid, sum(revenue) Revenue, SUM(cost) Cost
	    from dbo.tbl_profit_aggregation with(nolock)
		WHERE 
             Date between @from and @to 
            {WhereREVENUE}
		GROUP BY new_originid
        HAVING sum(revenue) > 0
  )REVENUE
  on REVENUE.new_originid = COALESCE(conversion.OriginId, DAUS.OriginId)
  full outer join
   (
/*
	    select OriginId, sum(UniqueInstallations) UniqueInstallations
	    from [dbo].[InstallationReportAgg5.0] with(nolock)
	    WHERE 
	        Date between @from and @to 
            {WhereInstalls}
	    GROUP BY OriginId
*/
        SELECT OriginId, COUNT(*) UniqueInstallations
		FROM [dbo].[InstallationIpOrigin6.2]
		WHERE Date between @from and @to 
             {WhereInstalls}
		GROUP BY OriginId
    )INSTALL
	on INSTALL.OriginId = COALESCE(conversion.OriginId, DAUS.OriginId, REVENUE.new_originid)
  order by Exposures desc
";
        /*
        private static ConversionDrillDownRow CreateDrillDownRowDataObject(Dictionary<string, object> row)
        {
            ConversionDrillDownRow data = new ConversionDrillDownRow();
            data.CallCount = (int)row["calls"];
            data.ExposureCount = (int)row["exposures"];
            data.RequestCount = (int)row["requests"];
            data.UpsalesCount = (int)row["upsales"];
            data.Origin = Convert.ToString(row["new_name"]);
            if (data.ExposureCount > 0)
            {
                data.CallsPercent = ((double)data.CallCount) / ((double)data.ExposureCount) * 100;
                data.RequestPercent = ((double)data.RequestCount) / ((double)data.ExposureCount) * 100;
            }
            return data;
        }
        */
        private static StringBuilder BuildDrillDownQuery(Guid expertiseId, string controlName, string domain, DataAccessLayer.Exposure.SqlParametersList parameters)
        {
            StringBuilder expoQuery = new StringBuilder(
                @"select 
                new_origin.new_name
                , isnull(exp.sum , 0) exposures
                , isnull(inc.sum , 0) requests
                , isnull(inc.callSum , 0) calls
                , isnull(ups.sum , 0) upsales
                from
                (select sum(Cnt) sum, new_originid
                from New_exposure_PerDay_ConversionReport 
                where
                new_date = @date ");
            if (!string.IsNullOrEmpty(domain))
            {
                expoQuery.Append(" and New_Domain = @domain ");
                parameters.Add("@domain", domain);
            }
            if (!string.IsNullOrEmpty(controlName))
            {
                expoQuery.Append(" and New_ControlName = @flavor ");
                parameters.Add("@flavor", controlName);
            }
            if (expertiseId != Guid.Empty)
            {
                expoQuery.Append(" and new_primaryexpertiseid = @headingId ");
                parameters.Add("@headingId", expertiseId);
            }
            expoQuery.Append(
                @" group by New_exposure_PerDay_ConversionReport.new_originid
                   ) exp
                   full outer join
                   (
                select sum(Cnt) sum, sum(sumCallCount) callSum, new_originid
                from incident_PerDay_ConversionReport                
                where incident_PerDay_ConversionReport.CreatedOn = @date ");
            if (!string.IsNullOrEmpty(domain))
            {
                expoQuery.Append(" and New_Domain = @domain ");
            }
            if (!string.IsNullOrEmpty(controlName))
            {
                expoQuery.Append(" and New_ControlName = @flavor ");
            }
            if (expertiseId != Guid.Empty)
            {
                expoQuery.Append(" and new_primaryexpertiseid = @headingId ");
            }
            expoQuery.Append(@" 
                group by incident_PerDay_ConversionReport.new_originid
                ) inc 
                on exp.new_originid = inc.new_originid                
                full outer join
                (
					 select sum(Cnt) sum, new_originid
                from incident_PerDay_ConversionReport              
                where incident_PerDay_ConversionReport.CreatedOn = @date 
                and new_isupsale = 1 ");
            if (!string.IsNullOrEmpty(domain))
            {
                expoQuery.Append(" and New_Domain = @domain ");
            }
            if (!string.IsNullOrEmpty(controlName))
            {
                expoQuery.Append(" and New_ControlName = @flavor ");
            }
            if (expertiseId != Guid.Empty)
            {
                expoQuery.Append(" and new_primaryexpertiseid = @headingId ");
            }
            expoQuery.Append(
                @"group by incident_PerDay_ConversionReport.new_originid
                ) ups
                on inc.new_originid = ups.new_originid
                inner join
                new_origin on new_origin.new_originid = isnull(exp.new_originid, isnull(inc.new_originid, ups.new_originid))");
            return expoQuery;
        }

        #endregion

        #region Conversion Regular Report Methods

        internal ConversionReportResponse Create(
            DateTime fromDate,
            DateTime toDate,
            Guid expertiseId,
            string controlName,
            string domain,
            Guid originId)
        {
            fromDate = fromDate.Date;
            toDate = toDate.Date;
            ConversionReportResponse response = new ConversionReportResponse();
            DataAccessLayer.Exposure expoDal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
            DateTime exposuresLastUpdate;
            var expoDates = expoDal.GetExposureCount(fromDate, toDate, expertiseId, controlName, domain, originId, out exposuresLastUpdate);
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DateTime requestsLastUpdate;
            var incidents = incidentDal.ExposureQuery(fromDate, toDate, expertiseId, controlName, domain, originId, null, out requestsLastUpdate);
            DateTime stam;
            var upsales = incidentDal.ExposureQuery(fromDate, toDate, expertiseId, controlName, domain, originId, true, out stam);
            var nonUpsales = incidentDal.ExposureQuery(fromDate, toDate, expertiseId, controlName, domain, originId, false, out stam);
            Dictionary<DateTime, int> stoppedRequests = CalculateStoppedRequests(fromDate, toDate, incidentDal, expertiseId, controlName, domain, originId);
            response.ExposuresLastUpdate = FixDateToLocal(exposuresLastUpdate);
            response.RequestsLastUpdate = FixDateToLocal(requestsLastUpdate);
            DateTime current = toDate;
            while (current >= fromDate)
            {
                DML.Reports.ConversionReportRow data = CalculateAndCreateRow(expoDates, incidents, upsales, nonUpsales, stoppedRequests, current);
                response.DataList.Add(data);
                current = current.AddDays(-1);
            }
            return response;
        }

        private Dictionary<DateTime, int> CalculateStoppedRequests(DateTime from, DateTime to, DataAccessLayer.Incident dal, 
           Guid expertiseId,
           string controlName,
           string domain,
           Guid originId)
        {
            DataAccessLayer.Incident.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.incident>.SqlParametersList();
            parameters.Add("@from", from);
            parameters.Add("@to", to);
            StringBuilder builder = new StringBuilder(string.Empty);
            if (expertiseId != Guid.Empty)
            {
                builder.Append(" and new_primaryexpertiseid = @headingId ");
                parameters.Add("@headingId", expertiseId);
            }
            if (!string.IsNullOrEmpty(controlName))
            {
                builder.Append(" and New_ControlName = @control ");
                parameters.Add("@control", controlName);
            }
            if (!string.IsNullOrEmpty(domain))
            {
                builder.Append(" and New_Domain = @domain ");
                parameters.Add("@domain", domain);
            }
            if (originId != Guid.Empty)
            {
                builder.Append(" and new_originid = @originId ");
                parameters.Add("@originId", originId);
            }
            string queryReady = string.Format(stoppedQuery, builder.ToString());
            var reader = dal.ExecuteReader(queryReady, parameters);
            Dictionary<DateTime, int> retVal = new Dictionary<DateTime, int>();
            foreach (var row in reader)
            {
                retVal.Add((DateTime)row["date"], (int)row["cnt"]);
            }
            return retVal;
        }

        private const string stoppedQuery =
            @"
select count(*) cnt, dateadd(d, 0, datediff(d, 0, new_createdonlocal)) date
 from incidentextensionbase with(nolock)
where new_createdonlocal between @from and @to
and new_isstoppedmanually = 1
{0}
group by dateadd(d, 0, datediff(d, 0, new_createdonlocal))
order by dateadd(d, 0, datediff(d, 0, new_createdonlocal)) desc 
";

        private DML.Reports.ConversionReportRow CalculateAndCreateRow(
            List<ConversionReportContainerPerDate> expoDates, 
            List<ConversionReportContainerPerDate> incidents, 
            List<ConversionReportContainerPerDate> upsales, 
            List<ConversionReportContainerPerDate> nonUpsales,
            Dictionary<DateTime, int> stoppedRequests, DateTime current)
        {
            DML.Reports.ConversionReportRow data = new NoProblem.Core.DataModel.Reports.ConversionReportRow();
            data.Date = current;
            var inci = incidents.FirstOrDefault(x => x.Date == current);
            if (inci != null)
            {
                data.RequestCount = inci.MainCount;
                data.CallCount = inci.SecondaryCount;
            }
            var ups = upsales.FirstOrDefault(x => x.Date == current);
            if (ups != null)
            {
                data.UpsalesCount = ups.MainCount;
            }
            var nonUps = nonUpsales.FirstOrDefault(x => x.Date == current);
            if (nonUps != null)
            {
                data.NonUpsalesCount = nonUps.MainCount;
            }
            var expo = expoDates.FirstOrDefault(x => x.Date == current);
            if (expo != null)
            {
                data.ExposureCount = expo.MainCount;
                if (data.ExposureCount > 0)
                {
                    data.CallsPercent = ((double)data.CallCount) / ((double)data.ExposureCount) * 100;
                    data.TotalRequests_Exposures = ((double)data.RequestCount) / ((double)data.ExposureCount) * 100;
                    data.OriginalRequests_Exposures = ((double)data.NonUpsalesCount) / ((double)data.ExposureCount) * 100;
                }
            }
            int stoppedCount;
            if (stoppedRequests.TryGetValue(current, out stoppedCount))
            {
                data.StoppedRequests = stoppedCount;
            }
            return data;
        }

        #endregion
        internal ConversionReportResponse Create2(
            DateTime fromDate,
            DateTime toDate,
            Guid expertiseId,
            string controlName,            
            Guid originId,
            eConversionInterval interval,
            IntervalTimeReport interval_time_report,
            int CountryId
            ,int SliderType)
        {
            ConversionReportResponse response = new ConversionReportResponse();
            DataAccessLayer.Exposure expoDal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
            
            
            
            response.DataList = new List<ConversionReportRow>();
           string _connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
    //        string _connectionString = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";
             using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandTimeout = 60;

                StringBuilder query = new StringBuilder(ConversionQueryMongo);
            //    query = ((interval == eConversionInterval.HOUR) ? query.Replace("{0}", "HOUR") : query.Replace("{0}", "DAY"));
                if (interval == eConversionInterval.DAILY_AVERAGE)
                {
                    query = query.Replace("{0}", eConversionInterval.DAY.ToString());
                    query = query.Replace("{1}", eConversionInterval.DAY.ToString());
                }
                else if (interval == eConversionInterval.HOUR)
                {
                    query = query.Replace("{0}", eConversionInterval.HOUR.ToString());
                    query = query.Replace("{1}", eConversionInterval.DAY.ToString());
                }
                else 
                {
                    query = query.Replace("{0}", interval.ToString());
                    query = query.Replace("{1}", interval.ToString());
                }
                 Dictionary<DateTime, DateTime> dic_datetime = interval_time_report == null ? new Dictionary<DateTime, DateTime>() :
                    interval_time_report.GetIntervalForQuery(fromDate, toDate);
                StringBuilder WhereConversionReport = new StringBuilder();
                StringBuilder Whereincident = new StringBuilder();
                StringBuilder Wherefinancial = new StringBuilder();
                StringBuilder WhereDAUS = new StringBuilder();
                StringBuilder WhereInstall = new StringBuilder();
                if (dic_datetime.Count == 0)
                {
                    query.Insert(0, "select @to = dateadd(millisecond, -10, @to) ");
                    WhereConversionReport.Append(" Date between @from and @to ");
                    Whereincident.Append(" new_createdonlocal between @from and @to ");
                    Wherefinancial.Append(" Date between dateadd(DAY, datediff(DAY, 0, @from), 0) and dateadd(DAY, datediff(DAY, 0, @to), 0) ");
                    WhereDAUS.Append(" Date between dateadd(DAY, datediff(DAY, 0, @from), 0) and dateadd(DAY, datediff(DAY, 0, @to), 0) ");
                    WhereInstall.Append(" Date between dateadd(DAY, datediff(DAY, 0, @from), 0) and dateadd(DAY, datediff(DAY, 0, @to), 0) ");
                    cmd.Parameters.AddWithValue("@from", fromDate);
                    cmd.Parameters.AddWithValue("@to", toDate);
                }
                else
                {
                    int i = 1;
                    foreach (KeyValuePair<DateTime, DateTime> kvp in dic_datetime)
                    {
                        WhereConversionReport.Append(((i == 1) ? "(" : " or") + " (Date between @from" + i + " and @to" + i + ") ");
                        WhereDAUS.Append(((i == 1) ? "(" : " or") + " (Date between dateadd(DAY, datediff(DAY, 0, @from" + i + "), 0) and dateadd(DAY, datediff(DAY, 0, @to" + i + "), 0)) ");
                        Whereincident.Append(((i == 1) ? "(" : " or") + " (new_createdonlocal between @from" + i + " and @to" + i + ") ");
                        Wherefinancial.Append(((i == 1) ? "(" : " or") + " (Date between dateadd(DAY, datediff(DAY, 0, @from" + i + "), 0) and dateadd(DAY, datediff(DAY, 0, @to" + i + "), 0)) ");
                        WhereInstall.Append(((i == 1) ? "(" : " or") + " (Date between dateadd(DAY, datediff(DAY, 0, @from" + i + "), 0) and dateadd(DAY, datediff(DAY, 0, @to" + i + "), 0)) ");
                        cmd.Parameters.AddWithValue("@from" + i, kvp.Key);
                        cmd.Parameters.AddWithValue("@to" + i, kvp.Value);
                        i++;
                    }
                    WhereConversionReport.Append(") ");
                    Whereincident.Append(") ");
                    Wherefinancial.Append(") ");
                    WhereDAUS.Append(") ");
                    WhereInstall.Append(") ");

                }
                if (originId != Guid.Empty)
                {
                    WhereConversionReport.Append(" and OriginId = @OriginId ");
                    WhereDAUS.Append(" and OriginId = @OriginId ");
                    Whereincident.Append(" and new_originid = @OriginId ");
                    Wherefinancial.Append(" and new_originid = @OriginId ");
                    WhereInstall.Append(" and OriginId = @OriginId ");
                    cmd.Parameters.AddWithValue("@OriginId", originId);
                }
                if (!string.IsNullOrEmpty(controlName))
                {
                    WhereConversionReport.Append(" and ControlName = @ControlName ");
                    Whereincident.Append(" and New_ControlName = @ControlName ");
                    cmd.Parameters.AddWithValue("@ControlName", controlName);
                }
                if (expertiseId != Guid.Empty)
                {
                    WhereConversionReport.Append(" and PrimaryExpertiseId = @expertiseId ");
                    Whereincident.Append(" and new_primaryexpertiseid = @expertiseId ");
                    Wherefinancial.Append(" and new_primaryexpertiseid = @expertiseId ");
                    cmd.Parameters.AddWithValue("@expertiseId", expertiseId);
                }
                if (CountryId > -1)
                {
                    string _country = @"DECLARE @country nvarchar(128)
                                        SELECT @country = [DynamicNoProblem].[dbo].[GetCountryIP2LOCATION_ByCountryId](@CountryId)" + Environment.NewLine;
                    query.Insert(0, _country);
                    /*
                    WhereConversionReport.Append(" and [dbo].[GetCountryIdByIP2LocationName](Country) = @CountryId ");
                    Whereincident.Append(" and [dbo].[GetCountryIdByIP2LocationName](New_Country) = @CountryId ");
                    WhereDAUS.Append(" and CountryId = @CountryId ");
                    Wherefinancial.Append(" and [dbo].[GetCountryIdByIP2LocationName](Country) = @CountryId ");
                     * */
                    WhereConversionReport.Append(" and Country = @country ");
                    Whereincident.Append(" and New_Country = @country ");
                    WhereDAUS.Append(" and CountryId = @CountryId ");
                    Wherefinancial.Append(" and Country = @country ");
                    WhereInstall.Append(" and Country = @country ");
                    cmd.Parameters.AddWithValue("@CountryId", CountryId);
                }
                 if(SliderType > 0)
                 {
                     WhereConversionReport.Append(" AND SliderType = @SliderType ");
                     Whereincident.Append(" AND b.New_Type = @SliderType ");
                     Wherefinancial.Append(" AND SliderType = @SliderType ");
                     cmd.Parameters.AddWithValue("@SliderType", SliderType);
                 }
                
                query.Replace("{WhereConversionReport}", WhereConversionReport.ToString());
                query.Replace("{Whereincident}", Whereincident.ToString());
                query.Replace("{WhereDAUS}", WhereDAUS.ToString());
                query.Replace("{Wherefinancial}", Wherefinancial.ToString());
                query.Replace("{InstallWhere}", WhereInstall.ToString());
                
                cmd.Connection = conn;
                cmd.CommandText = query.ToString();
                cmd.CommandTimeout = 60;
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    
                    ConversionReportRow crr = new ConversionReportRow();
                    crr.Date = (DateTime)reader["Date"];
                    crr.ExposureCount = (int)reader["Exposures"];
                    crr.ExposureHits = (int)reader["ExpHits"];
                    crr.RequestCount = (int)reader["TotalRequests"];
                    crr.CallCount = (int)reader["Calls"];
                    crr.DAUS = (int)reader["DAUS"];
                    crr.LogicHits = (int)reader["LogicHits"];
                    crr.Revenue = (decimal)reader["Rev"];
                    crr.Cost = (decimal)reader["Cost"];
                    crr.NonUpsalesCount = (int)reader["Pure_Request"];
                    crr.UpsalesCount = (int)reader["_Upsold"];
                    crr.StoppedRequests = (int)reader["Stoped"];
                    crr.UniqueInstalls = (int)reader["UniqueInstallations"];
                    crr.SetValues();
                    response.DataList.Add(crr);
                }
                reader.Dispose();
                cmd.Dispose();
                string new_command = "[dbo].[ConversionLastUpdate]";
                cmd = new SqlCommand(new_command, conn);
                DateTime exposuresLastUpdate = (DateTime)cmd.ExecuteScalar();
                conn.Close();
                response.ExposuresLastUpdate = exposuresLastUpdate;
                response.RequestsLastUpdate = exposuresLastUpdate;
                
           
            }

             return response;
        }
        internal List<ConversionUsersReportResponse> CreateConversionUsersReport(
            ConversionReportUsersRequest request)
        {
            string command = @"
select
	coalesce(DAUS.Date, REVENUE.New_Date, dateadd({0}, datediff({0}, 0, @from), 0)) Date
	,ORIGIN.New_name
	,ORIGIN.New_originId	
    ,ISNULL(DAUS.users, 0) DAUS
	,ISNULL(REVENUE.Revenue, 0) Rev
	
from
(
	 
		SELECT dateadd({0}, datediff({0}, 0, [Date]), 0) Date, sum(Visitor) users, OriginId
		FROM [dynamicNoProblem].[dbo].[Exposure]
		WHERE 
             OriginId in (select id from  dbo.fn_GetGuidsFromString(@origins))
            {DAUS WHERE}
		GROUP BY  dateadd({0}, datediff({0}, 0, [Date]), 0), OriginId
        
  )DAUS 
	--on dateadd({0}, datediff({0}, 0, conversion.Date), 0) = DAUS.Date
  full outer join
  (
		select dateadd({0}, datediff({0}, 0, [date]), 0) New_Date, sum(revenue) Revenue, new_originid OriginId
	    from dbo.tbl_profit_aggregation with(nolock)
		WHERE 
             new_originid in (select id from  dbo.fn_GetGuidsFromString(@origins)) 
            {REVENUE WHERE}
		GROUP BY dateadd({0}, datediff({0}, 0, [date]), 0), new_originid
  )REVENUE
	on DAUS.Date = REVENUE.New_Date and DAUS.OriginId = REVENUE.OriginId
 full outer JOIN
 (
	SELECT New_name, New_originId
	FROM dbo.New_originExtensionBase WITH(NOLOCK)
	WHERE new_originid in
		(select id from  dbo.fn_GetGuidsFromString(@origins)) 
 ) ORIGIN
	on ISNULL(DAUS.OriginId, REVENUE.OriginId) = ORIGIN.New_originId
  order by coalesce(DAUS.Date, REVENUE.New_Date, dateadd({0}, datediff({0}, 0, @from), 0)) desc, ORIGIN.New_name";
            StringBuilder query = new StringBuilder(command);

            if (request.Interval == eConversionInterval.HOUR)
                request.Interval = eConversionInterval.DAY;
            else if(request.Interval == eConversionInterval.DAILY_AVERAGE)
                request.Interval = eConversionInterval.DAY;

            List<ConversionUsersReportResponse> response = new List<ConversionUsersReportResponse>();
            string _connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
            //    string _connectionString = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    query = query.Replace("{0}", request.Interval.ToString());
                    Dictionary<DateTime, DateTime> dic_datetime = request.interval_time_report == null ? new Dictionary<DateTime, DateTime>() :
                            request.interval_time_report.GetIntervalForQuery(request.FromDate, request.ToDate);
                    StringBuilder DAUS_WHERE = new StringBuilder();
                    StringBuilder REVENUE_WHERE = new StringBuilder();
                    if (dic_datetime.Count == 0)
                    {
                        query.Insert(0, "select @to = dateadd(millisecond, -10, @to) ");
                        /*
                        WhereConversionReport.Append(" Date between @from and @to ");
                        Whereincident.Append(" new_createdonlocal between @from and @to ");
                        Wherefinancial.Append(" Date between dateadd(DAY, datediff(DAY, 0, @from), 0) and dateadd(DAY, datediff(DAY, 0, @to), 0) ");
                         * */
                        DAUS_WHERE.Append("and Date between dateadd(DAY, datediff(DAY, 0, @from), 0) and dateadd(DAY, datediff(DAY, 0, @to), 0) ");
                        REVENUE_WHERE.Append("and Date between dateadd(DAY, datediff(DAY, 0, @from), 0) and dateadd(DAY, datediff(DAY, 0, @to), 0) ");
                        cmd.Parameters.AddWithValue("@from", request.FromDate);
                        cmd.Parameters.AddWithValue("@to", request.ToDate);
                    }
                    else
                    {
                        int i = 1;
                        foreach (KeyValuePair<DateTime, DateTime> kvp in dic_datetime)
                        {
                            //  WhereConversionReport.Append(((i == 1) ? "(" : " or") + " (Date between @from" + i + " and @to" + i + ") ");
                            DAUS_WHERE.Append(((i == 1) ? "AND (" : " or") + " (Date between dateadd(DAY, datediff(DAY, 0, @from" + i + "), 0) and dateadd(DAY, datediff(DAY, 0, @to" + i + "), 0)) ");
                            REVENUE_WHERE.Append(((i == 1) ? "AND (" : " or") + " (Date between dateadd(DAY, datediff(DAY, 0, @from" + i + "), 0) and dateadd(DAY, datediff(DAY, 0, @to" + i + "), 0)) ");

                            //   Whereincident.Append(((i == 1) ? "(" : " or") + " (new_createdonlocal between @from" + i + " and @to" + i + ") ");
                            //     Wherefinancial.Append(((i == 1) ? "(" : " or") + " (Date between dateadd(DAY, datediff(DAY, 0, @from" + i + "), 0) and dateadd(DAY, datediff(DAY, 0, @to" + i + "), 0)) ");
                            cmd.Parameters.AddWithValue("@from" + i, kvp.Key);
                            cmd.Parameters.AddWithValue("@to" + i, kvp.Value);
                            i++;
                        }
                        cmd.Parameters.AddWithValue("@from", request.FromDate);
                        //         WhereConversionReport.Append(") ");
                        //         Whereincident.Append(") ");
                        REVENUE_WHERE.Append(") ");
                        DAUS_WHERE.Append(") ");

                    }
                    if (request.ExpertiseId != Guid.Empty)
                    {
                        REVENUE_WHERE.Append(" and new_primaryexpertiseid = @expertiseId ");
                        cmd.Parameters.AddWithValue("@expertiseId", request.ExpertiseId);
                    }
                    if (request.CountryId > -1)
                    {
                        query.Insert(0, "DECLARE @country nvarchar(150) = [DynamicNoProblem].[dbo].[GetCountryIP2LOCATION_ByCountryId](@CountryId) " + Environment.NewLine);
                        REVENUE_WHERE.Append(" and Country = @country ");
                        DAUS_WHERE.Append(" and CountryId = @CountryId ");
                        cmd.Parameters.AddWithValue("@CountryId", request.CountryId);
                    }
                    if(request.SliderType > 0)
                    {
                        REVENUE_WHERE.Append(" AND SliderType = @SliderType ");
                        cmd.Parameters.AddWithValue("@SliderType", request.SliderType);
                    }

                    query = query.Replace("{REVENUE WHERE}", REVENUE_WHERE.ToString());
                    query = query.Replace("{DAUS WHERE}", DAUS_WHERE.ToString());
                    cmd.Parameters.AddWithValue("@origins", request.OriginsForSQL);
                    cmd.CommandTimeout = 600;
                    cmd.CommandText = query.ToString();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ConversionUsersReportResponse curr = new ConversionUsersReportResponse();
                        curr.date = (DateTime)reader["Date"];
                        curr.DAUS = (int)reader["DAUS"];
                        curr.OriginId = (Guid)reader["New_originId"];
                        curr.OriginName = (string)reader["New_name"];
                        curr.Revenue = (decimal)reader["Rev"];
                        response.Add(curr);
                    }
                    conn.Close();
                }
            }
            return response;
        }
        internal List<ConversionUsersReportExposureResponse> CreateConversionUsersReportExposures(
            ConversionReportUsersRequest request)
        {
            string command = @"
select
    coalesce(DAUS.Date, EXPOSURE.Date) Date
	,ORIGIN.New_name
	,ORIGIN.New_originId	
    ,ISNULL(DAUS.users, 0) DAUS
	,ISNULL(EXPOSURE.Exposures, 0) Exposures
	,ISNULL(EXPOSURE.Requests, 0) Requests
from
(
	 
		SELECT dateadd({0}, datediff({0}, 0, [Date]), 0) Date, sum(Visitor) users, OriginId
		FROM [dynamicNoProblem].[dbo].[Exposure]
		WHERE 
             OriginId in (select id from  dbo.fn_GetGuidsFromString(@origins))
            {DAUS WHERE}
		GROUP BY  dateadd({0}, datediff({0}, 0, [Date]), 0), OriginId
        
  )DAUS 
	--on dateadd({0}, datediff({0}, 0, conversion.Date), 0) = DAUS.Date
  full outer join
  (
        SELECT dateadd({0}, datediff({0}, 0, [date]), 0) Date,	SUM(Exposures) Exposures, OriginId
                ,SUM(Requests) Requests
	    FROM dbo.ConversionReportMongo WITH(NOLOCK)
	    WHERE OriginId in (select id from  dbo.fn_GetGuidsFromString(@origins)) 		    
            {EXPOSURE WHERE}
	    GROUP BY dateadd({0}, datediff({0}, 0, [date]), 0), OriginId		
  )EXPOSURE
	on DAUS.Date = EXPOSURE.Date and DAUS.OriginId = EXPOSURE.OriginId
 full outer JOIN
 (
	SELECT New_name, New_originId
	FROM dbo.New_originExtensionBase WITH(NOLOCK)
	WHERE new_originid in
		(select id from  dbo.fn_GetGuidsFromString(@origins)) 
 ) ORIGIN
	on ISNULL(DAUS.OriginId, EXPOSURE.OriginId) = ORIGIN.New_originId
  order by coalesce(DAUS.Date, EXPOSURE.Date) desc, ORIGIN.New_name";
            StringBuilder query = new StringBuilder(command);

            if (request.Interval == eConversionInterval.HOUR)
                request.Interval = eConversionInterval.DAY;
            else if (request.Interval == eConversionInterval.DAILY_AVERAGE)
                request.Interval = eConversionInterval.DAY;

            List<ConversionUsersReportExposureResponse> response = new List<ConversionUsersReportExposureResponse>();
            string _connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
       //         string _connectionString = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    query = query.Replace("{0}", request.Interval.ToString());
                    Dictionary<DateTime, DateTime> dic_datetime = request.interval_time_report == null ? new Dictionary<DateTime, DateTime>() :
                            request.interval_time_report.GetIntervalForQuery(request.FromDate, request.ToDate);
                    StringBuilder DAUS_WHERE = new StringBuilder();
                    StringBuilder EXPOSUE_WHERE = new StringBuilder();
                    if (dic_datetime.Count == 0)
                    {
                        query.Insert(0, "select @to = dateadd(millisecond, -10, @to) ");
                        /*
                        WhereConversionReport.Append(" Date between @from and @to ");
                        Whereincident.Append(" new_createdonlocal between @from and @to ");
                        Wherefinancial.Append(" Date between dateadd(DAY, datediff(DAY, 0, @from), 0) and dateadd(DAY, datediff(DAY, 0, @to), 0) ");
                         * */
                        DAUS_WHERE.Append("and Date between dateadd(DAY, datediff(DAY, 0, @from), 0) and dateadd(DAY, datediff(DAY, 0, @to), 0) ");
                        EXPOSUE_WHERE.Append("and Date between dateadd(DAY, datediff(DAY, 0, @from), 0) and dateadd(DAY, datediff(DAY, 0, @to), 0) ");
                        cmd.Parameters.AddWithValue("@from", request.FromDate);
                        cmd.Parameters.AddWithValue("@to", request.ToDate);
                    }
                    else
                    {
                        int i = 1;
                        foreach (KeyValuePair<DateTime, DateTime> kvp in dic_datetime)
                        {
                            //  WhereConversionReport.Append(((i == 1) ? "(" : " or") + " (Date between @from" + i + " and @to" + i + ") ");
                            DAUS_WHERE.Append(((i == 1) ? "AND (" : " or") + " (Date between dateadd(DAY, datediff(DAY, 0, @from" + i + "), 0) and dateadd(DAY, datediff(DAY, 0, @to" + i + "), 0)) ");
                            EXPOSUE_WHERE.Append(((i == 1) ? "AND (" : " or") + " (Date between dateadd(DAY, datediff(DAY, 0, @from" + i + "), 0) and dateadd(DAY, datediff(DAY, 0, @to" + i + "), 0)) ");

                            //   Whereincident.Append(((i == 1) ? "(" : " or") + " (new_createdonlocal between @from" + i + " and @to" + i + ") ");
                            //     Wherefinancial.Append(((i == 1) ? "(" : " or") + " (Date between dateadd(DAY, datediff(DAY, 0, @from" + i + "), 0) and dateadd(DAY, datediff(DAY, 0, @to" + i + "), 0)) ");
                            cmd.Parameters.AddWithValue("@from" + i, kvp.Key);
                            cmd.Parameters.AddWithValue("@to" + i, kvp.Value);
                            i++;
                        }
                        cmd.Parameters.AddWithValue("@from", request.FromDate);
                        //         WhereConversionReport.Append(") ");
                        //         Whereincident.Append(") ");
                        EXPOSUE_WHERE.Append(") ");
                        DAUS_WHERE.Append(") ");

                    }
                    if (request.ExpertiseId != Guid.Empty)
                    {
                        EXPOSUE_WHERE.Append(" and PrimaryExpertiseId = @expertiseId ");
                        cmd.Parameters.AddWithValue("@expertiseId", request.ExpertiseId);
                    }
                    if (request.CountryId > -1)
                    {
                        query.Insert(0, "DECLARE @country nvarchar(150) = [DynamicNoProblem].[dbo].[GetCountryIP2LOCATION_ByCountryId](@CountryId) " + Environment.NewLine);
                        EXPOSUE_WHERE.Append(" and Country = @country ");
                        DAUS_WHERE.Append(" and CountryId = @CountryId ");
                        cmd.Parameters.AddWithValue("@CountryId", request.CountryId);
                    }
                    if(request.SliderType > 0)
                    {
                        EXPOSUE_WHERE.Append(" AND SliderType = @SliderType ");
                        cmd.Parameters.AddWithValue("@SliderType", request.SliderType);
                    }

                    query = query.Replace("{EXPOSURE WHERE}", EXPOSUE_WHERE.ToString());
                    query = query.Replace("{DAUS WHERE}", DAUS_WHERE.ToString());
                    cmd.Parameters.AddWithValue("@origins", request.OriginsForSQL);
                    cmd.CommandTimeout = 600;
                    cmd.CommandText = query.ToString();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ConversionUsersReportExposureResponse curr = new ConversionUsersReportExposureResponse();
                        curr.date = (reader["Date"] == DBNull.Value) ? DateTime.MinValue : (DateTime)reader["Date"];
                        curr.DAUS = (int)reader["DAUS"];
                        curr.OriginId = (Guid)reader["New_originId"];
                        curr.OriginName = (string)reader["New_name"];
                        curr.Exposure = (int)reader["Exposures"];
                        curr.Requests = (int)reader["Requests"];
                        curr.Set_EXP_KDAUS();
                        curr.Set_Request_KDAUS();
                        response.Add(curr);
                    }
                    conn.Close();
                }
            }
            return response;
        }
        private const string ConversionQueryMongo =
            @"select
	            coalesce(conversion.Date, DAUS.Date, REVENUE.New_Date, INSTALL.Date) Date
	            ,ISNULL(conversion.Exposures, 0) Exposures
	            ,ISNULL(conversion.TotalRequests, 0) TotalRequests
	            ,ISNULL(conversion.Calls, 0) Calls
                ,ISNULL(conversion.Hits, 0) ExpHits
                ,ISNULL(DAUS.users, 0) DAUS
                ,ISNULL(DAUS.Hits, 0) LogicHits
	            ,ISNULL(REVENUE.Revenue, 0) Rev
                ,ISNULL(REVENUE.Cost, 0) Cost
	            ,ISNULL(PureRequests.requests, 0) Pure_Request
	            ,ISNULL(upsales.upsolds, 0) _Upsold
                ,ISNULL(Stoped.cnt, 0) Stoped
                ,ISNULL(INSTALL.UniqueInstallations, 0) UniqueInstallations
            from
            (

            SELECT 
                  dateadd({0}, datediff({0}, 0, Date), 0) Date
      
                  ,sum([Exposures]) Exposures
                  ,sum([Requests]) TotalRequests
                  ,sum([Calls]) Calls
                  ,SUM([Hits]) Hits
     
              FROM [dbo].[ConversionReportMongo]  with(nolock)
              where 
                {WhereConversionReport}
              group by dateadd({0}, datediff({0}, 0, Date), 0)
  
              ) conversion
              left join
              (
	             select SUM(Requests) requests, dateadd({0}, datediff({0}, 0, Date), 0) dt
		            from [dbo].[ConversionReportMongo] with(nolock)
		            where isnull(IsUpsale, 0) = 0
			            and 
                    {WhereConversionReport}
		            group by dateadd({0}, datediff({0}, 0, Date), 0)	
              ) PureRequests
              on PureRequests.dt = conversion.Date 
              left join
              (
	            select SUM([Requests]) upsolds, dateadd({0}, datediff({0}, 0, Date), 0) dt
		            from  [dbo].[ConversionReportMongo] with(nolock)
		            where IsUpsale = 1
			            and 
                    {WhereConversionReport}
		            group by dateadd({0}, datediff({0}, 0, Date), 0)
              )upsales
              on upsales.dt = conversion.Date
              left join
              (
	
	            select count(*) cnt, dateadd({0}, datediff({0}, 0, new_createdonlocal), 0) dt
	             from dbo.incidentextensionbase b with(nolock)
		            inner join dbo.IncidentBase i with(nolock) on i.IncidentId = b.IncidentId
	            where 
	            new_isstoppedmanually = 1
                and
                    {Whereincident}
	            group by dateadd({0}, datediff({0}, 0, new_createdonlocal), 0)
              ) Stoped
              on Stoped.dt = conversion.Date
             full outer join
              (
		            SELECT dateadd({1}, datediff({1}, 0, [Date]), 0) Date, sum(Visitor) users, SUM(Hits) Hits
		            FROM [dynamicNoProblem].[dbo].[Exposure] with(nolock)
		            WHERE 
                        {WhereDAUS}
		            GROUP BY  dateadd({1}, datediff({1}, 0, [Date]), 0)
        
              )DAUS 
	            on dateadd({1}, datediff({1}, 0, conversion.Date), 0) = DAUS.Date
              full outer join
              (
		            select dateadd({1}, datediff({1}, 0, [date]), 0) New_Date, sum(revenue) Revenue, SUM(cost) Cost
	                from dbo.tbl_profit_aggregation with(nolock)
		            WHERE 
                        {Wherefinancial}
		            GROUP BY dateadd({1}, datediff({1}, 0, [date]), 0)
              )REVENUE
	            on COALESCE(dateadd({1}, datediff({1}, 0, conversion.Date), 0), DAUS.Date)  = REVENUE.New_Date
              full outer join
			(
                    select dateadd({1}, datediff({1}, 0, [Date]), 0) Date, COUNT(*) UniqueInstallations
	                from [dbo].[InstallationIpOrigin6.2] with(nolock)
		            WHERE 
                         {InstallWhere}
		            GROUP BY dateadd({1}, datediff({1}, 0, [Date]), 0)
					
			)INSTALL
				on COALESCE(dateadd({1}, datediff({1}, 0, conversion.Date), 0), DAUS.Date, REVENUE.New_Date) = INSTALL.Date
              order by Date desc                
                option(recompile)";
        /*
         * select dateadd({1}, datediff({1}, 0, [Date]), 0) Date, sum(UniqueInstallations) UniqueInstallations
	                from [dbo].[InstallationReportAgg5.0] with(nolock)
		            WHERE 
                        {InstallWhere}
		            GROUP BY dateadd({1}, datediff({1}, 0, [Date]), 0)
         * 
         * 
        private const string ConversionQueryMongo =
            @"
select
	coalesce(conversion.Date, DAUS.Date, REVENUE.New_Date) Date
	,ISNULL(conversion.Exposures, 0) Exposures
	,ISNULL(conversion.TotalRequests, 0) TotalRequests
	,ISNULL(conversion.Calls, 0) Calls
    ,ISNULL(DAUS.users, 0) DAUS
	,ISNULL(REVENUE.Revenue, 0) Rev
	,(CASE 
	  WHEN ISNULL(DAUS.users, 0) = 0 THEN 0
	  ELSE  CAST(ISNULL(conversion.Exposures, 0) as real) / (CAST(DAUS.users as real) / CAST(1000 as real))
	END) Exp_KDAUs
	,
	(CASE 
	  WHEN ISNULL(DAUS.users, 0) = 0 THEN 0
	  ELSE  CAST(ISNULL(conversion.TotalRequests, 0) as real) / (CAST(DAUS.users as real) / CAST(1000 as real))
	END)  Req_KDAUs
	,
	(CASE 
	  WHEN ISNULL(DAUS.users, 0) = 0 THEN 0
	  ELSE  CAST(ISNULL(REVENUE.Revenue, 0) as real) / (CAST(DAUS.users as real) / CAST(1000 as real))
	END) Revenue_KDAUs
	,isnull(PureRequests.requests, 0) Pure_Request
	,isnull(upsales.upsolds, 0) _Upsold
	,(CASE 
		  WHEN conversion.Exposures = 0 THEN 0
		  ELSE  (CAST(conversion.TotalRequests as real) / 
				CAST(conversion.Exposures as real) * 100)
				
		END) TotalRequests_Exposures

	,((CAST(isnull(PureRequests.requests, 0) as real) / cast(
		CASE 
		  WHEN conversion.Exposures = 0 THEN null
		  ELSE  conversion.Exposures
		END
		 as real)) * 100) OriginalRequests_Exposures
	,((CAST(conversion.Calls as real) / cast(
		CASE 
		  WHEN conversion.Exposures = 0 THEN null
		  ELSE  conversion.Exposures
		END
		 as real)) * 100) CallConversion
	,ISNULL(Stoped.cnt, 0) Stoped
	,((CAST(conversion.Calls as real) / CAST(	
		CASE 
		  WHEN conversion.TotalRequests - ISNULL(Stoped.cnt, 0)= 0 THEN null
		  ELSE  conversion.TotalRequests - ISNULL(Stoped.cnt, 0)
		END
		 as real)) * 100) SaleRate
	,(CASE 
	  WHEN ISNULL(DAUS.users, 0) = 0 THEN 0
	  ELSE  CAST(ISNULL(conversion.Hits, 0) as real) / (CAST(DAUS.users as real) / CAST(1000 as real))
	END) Hits_KDAUs
	,(CASE 
	  WHEN ISNULL(conversion.Exposures, 0) = 0 THEN 0
	  ELSE  CAST(ISNULL(conversion.Hits, 0) as real) / CAST(conversion.Exposures as real)
	END) Hits_Exposures
from
(

SELECT 
      dateadd({0}, datediff({0}, 0, Date), 0) Date
      
      ,sum([Exposures]) Exposures
      ,sum([Requests]) TotalRequests
      ,sum([Calls]) Calls
      ,SUM([Hits]) Hits
     
  FROM [dbo].[ConversionReportMongo] 
  where 
    {WhereConversionReport}
  group by dateadd({0}, datediff({0}, 0, Date), 0)
  
  ) conversion
  left join
  (
	 select SUM(Requests) requests, dateadd({0}, datediff({0}, 0, Date), 0) dt
		from [dbo].[ConversionReportMongo] 
		where isnull(IsUpsale, 0) = 0
			and 
        {WhereConversionReport}
		group by dateadd({0}, datediff({0}, 0, Date), 0)	
  ) PureRequests
  on PureRequests.dt = conversion.Date 
  left join
  (
	select SUM([Requests]) upsolds, dateadd({0}, datediff({0}, 0, Date), 0) dt
		from  [dbo].[ConversionReportMongo]
		where IsUpsale = 1
			and 
        {WhereConversionReport}
		group by dateadd({0}, datediff({0}, 0, Date), 0)
  )upsales
  on upsales.dt = conversion.Date
  left join
  (
	
	select count(*) cnt, dateadd({0}, datediff({0}, 0, new_createdonlocal), 0) dt
	 from dbo.incidentextensionbase b with(nolock)
		inner join dbo.IncidentBase i on i.IncidentId = b.IncidentId
	where 
	new_isstoppedmanually = 1
    and
        {Whereincident}
	group by dateadd({0}, datediff({0}, 0, new_createdonlocal), 0)
  ) Stoped
  on Stoped.dt = conversion.Date
 full outer join
  (
		SELECT dateadd({1}, datediff({1}, 0, [Date]), 0) Date, sum(Visitor) users
		FROM [dynamicNoProblem].[dbo].[Exposure]
		WHERE 
            {WhereDAUS}
		GROUP BY  dateadd({1}, datediff({1}, 0, [Date]), 0)
        
  )DAUS 
	on dateadd({1}, datediff({1}, 0, conversion.Date), 0) = DAUS.Date
  full outer join
  (
		select dateadd({1}, datediff({1}, 0, [date]), 0) New_Date, sum(revenue) Revenue
	    from dbo.tbl_profit_aggregation with(nolock)
		WHERE 
            {Wherefinancial}
		GROUP BY dateadd({1}, datediff({1}, 0, [date]), 0)
  )REVENUE
	on ISNULL(dateadd({1}, datediff({1}, 0, conversion.Date), 0), DAUS.Date)  = REVENUE.New_Date
  order by Date desc
";



        */

        string DrillDownAverageQuery = @"
            select @to = dateadd(millisecond, -10, @to)


create table #temp 
(
	OriginId uniqueidentifier
	,DAUS int
	,Exposures int
	,TotalRequests int
	,Calls int
	,LogicHits int
	,Pure_Request int
	,_Upsold int
	,Stoped int
    ,ExpHits int
	,Rev money
    ,Cost money
	,Date datetime
)

insert into #temp
	(OriginId, DAUS, Exposures, TotalRequests,
	Calls, LogicHits, Pure_Request, _Upsold, Stoped, ExpHits, Rev, Cost, Date)
select
	-- dbo.GetOriginNameByOriginId(coalesce(conversion.OriginId, DAUS.OriginId, REVENUE.new_originid)) Origin
	coalesce(conversion.OriginId, DAUS.OriginId, REVENUE.new_originid)
	,ISNULL(DAUS.users, 0)
	,ISNULL(conversion.Exposures, 0)
	,ISNULL(conversion.TotalRequests, 0)
	,ISNULL(conversion.Calls, 0)
	,ISNULL(DAUS.Hits, 0)
	,isnull(PureRequests.requests, 0) 
	,isnull(upsales.upsolds, 0) 
	,ISNULL(Stoped.cnt, 0) 
    ,ISNULL(conversion.Hits, 0) 
	,ISNULL(REVENUE.Revenue, 0)  
    ,ISNULL(REVENUE.Cost, 0) 
	,ISNULL(conversion.Date, DAUS.Date) 

from
(	
	SELECT       
      OriginId
      ,sum([Exposures]) Exposures
      ,sum([Requests]) TotalRequests
      ,sum([Calls]) Calls
      ,SUM([Hits]) Hits
      ,dateadd(day, 0, datediff(day, 0, Date)) Date
     
  FROM [dbo].[ConversionReportMongo] 
  where Date between @from and @to  
    {WhereConversionReport}    
  group by OriginId, dateadd(day, 0, datediff(day, 0, Date))
  
) conversion
FULL OUTER JOIN
(
    SELECT  OriginId, sum(Visitor) users, SUM(Hits) Hits
			,dateadd(day, 0, datediff(day, 0, Date)) Date
	FROM [dynamicNoProblem].[dbo].[Exposure]
	WHERE 
		 Date between @from and @to
            {WhereDAUS}
	GROUP BY  OriginId, dateadd(day, 0, datediff(day, 0, Date))      
   
)DAUS

    on conversion.OriginId = DAUS.OriginId and conversion.Date = DAUS.Date
  left join
  (
	 select sum(Requests) requests, OriginId
		from [dbo].[ConversionReportMongo] 
		where isnull(IsUpsale, 0) = 0
			and Date between @from and @to
                {WhereConversionReport}
		group by OriginId
  ) PureRequests
  on PureRequests.OriginId = ISNULL(conversion.OriginId, DAUS.OriginId) 
  left join
  (
	select sum([Requests]) upsolds, OriginId,
			dateadd(day, 0, datediff(day, 0, Date)) Date
		from [dbo].[ConversionReportMongo] 
		where IsUpsale = 1
			and Date between @from and @to
                {WhereConversionReport}
		group by OriginId, dateadd(day, 0, datediff(day, 0, Date))
  )upsales
  on upsales.OriginId = ISNULL(conversion.OriginId, DAUS.OriginId) and upsales.Date = ISNULL(conversion.Date, DAUS.Date)
  left join
  (	
	select count(*) cnt, b.new_originid, dateadd(day, 0, datediff(day, 0, b.New_CreatedOnLocal)) Date
	 from dbo.incidentextensionbase b with(nolock)
		inner join dbo.IncidentBase i on i.IncidentId = b.IncidentId
	where new_createdonlocal between @from and @to
	and new_isstoppedmanually = 1
        {Whereincident}        
	group by b.new_originid, dateadd(day, 0, datediff(day, 0, b.New_CreatedOnLocal))
  ) Stoped
  on Stoped.new_originid = ISNULL(conversion.OriginId, DAUS.OriginId) and Stoped.Date = ISNULL(conversion.Date, DAUS.Date)
  full outer join
  (	
		select new_originid, sum(revenue) Revenue, SUM(cost) Cost,
					dateadd(day, 0, datediff(day, 0, Date)) Date
	    from dbo.tbl_profit_aggregation with(nolock)
		WHERE 
             Date between @from and @to 
                 {WhereREVENUE}
		GROUP BY new_originid, dateadd(day, 0, datediff(day, 0, Date))
        HAVING sum(revenue) > 0
  )REVENUE
  on REVENUE.new_originid = ISNULL(conversion.OriginId, DAUS.OriginId) and REVENUE.Date = ISNULL(conversion.Date, DAUS.Date)
  

SELECT OriginId 
		,dbo.GetOriginNameByOriginId(OriginId) Origin
		,AVG(DAUS) DAUS
		,AVG(Exposures) Exposures
		,AVG(TotalRequests) TotalRequests
		,AVG(Calls) Calls
		,AVG(Pure_Request) Pure_Request
		,AVG(_Upsold) _Upsold
		,AVG(Stoped) Stoped
		,AVG(ExpHits) ExpHits
		,AVG(Rev) Rev
        ,AVG(Cost) Cost
		,AVG(LogicHits) LogicHits
FROM #temp
GROUP BY OriginId
ORDER BY Exposures  
  
  DROP TABLE #temp";
    }
}
/*
Total requests 	Requests 	Upsold 	Stopped 	Requests percent 	Requests conversion 	Calls 	Calls percent 	Sale rate
*/