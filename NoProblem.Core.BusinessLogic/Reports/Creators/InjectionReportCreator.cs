﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.Reports.Response;

namespace NoProblem.Core.BusinessLogic.Reports.Creators
{
    internal class InjectionReportCreator : XrmUserBase
    {
        #region Ctor
        internal InjectionReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        private const string queryInjectionReport = @"
CREATE TABLE #temp_origins
(
	ID uniqueidentifier NOT NULL
)

INSERT INTO #temp_origins(ID)
SELECT New_originId
FROM dbo.New_originExtensionBase
WHERE New_IsDistribution = 1

SELECT
	COALESCE (DAUS.Date, REVENUE.Date, TOTAL_DAUS.Date) Date
	,DAUS.Visitor
	,REVENUE.Revenue
	,TOTAL_DAUS.Total_Visitor
FROM
(
	SELECT 
      SUM(e.[Visitor]) Visitor
      ,DATEADD(DAY, DATEDIFF(DAY, 0, e.Date),0) Date
	FROM [DynamicNoProblem].[dbo].[Exposure] e  WITH(NOLOCK)
      {InnerJoin_DAUS}
	WHERE e.Date >= @from AND e.Date < @to		
        {WHERE_DAUS}
	GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, e.Date),0)
)DAUS
FULL OUTER JOIN
(
	SELECT DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0) Date             
            ,SUM(Revenue) Revenue
    FROM [dbo].[InstallationReportAgg6.0] WITH(NOLOCK)     
    WHERE Date >= @from AND Date < @to	       
         {WHERE_CRM}
    GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0) 
)REVENUE
	on DAUS.Date = REVENUE.Date
FULL OUTER JOIN
(
	SELECT 
      SUM(e.[Visitor]) Total_Visitor
      ,DATEADD(DAY, DATEDIFF(DAY, 0, e.Date),0) Date
	FROM [DynamicNoProblem].[dbo].[Exposure] e  WITH(NOLOCK)      
	WHERE e.Date >= @from AND e.Date < @to
         {WHERE_DAUS_Total}
		AND e.OriginId in (SELECT ID from #temp_origins)
	GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, e.Date),0)
)TOTAL_DAUS
	on TOTAL_DAUS.Date = COALESCE (DAUS.Date, REVENUE.Date)
ORDER BY Date desc
  
drop table #temp_origins";

        const string queryInjectionReportWithOrigins = @"
DECLARE @SelectOrigins table
(
	ID uniqueidentifier
)
CREATE TABLE #temp_origins
(
	ID uniqueidentifier NOT NULL
)
INSERT INTO #temp_origins(ID)
SELECT New_originId
FROM dbo.New_originExtensionBase
WHERE New_IsDistribution = 1

INSERT INTO @SelectOrigins(ID)
SELECT New_originId
FROM dbo.New_originExtensionBase
WHERE New_originId in (
	SELECT value
	FROM dbo.fn_ParseDelimitedGuid(@origins, ';'))

SELECT
	COALESCE (DAUS.Date, REVENUE.Date, TOTAL_DAUS.Date) Date
	,DAUS.Visitor
	,REVENUE.Revenue
	,TOTAL_DAUS.Total_Visitor
FROM
(
	SELECT 
      SUM(e.[Visitor]) Visitor
      ,DATEADD(DAY, DATEDIFF(DAY, 0, e.Date),0) Date
	FROM [DynamicNoProblem].[dbo].[Exposure] e  WITH(NOLOCK)
      {InnerJoin_DAUS}
	WHERE e.Date >= @from AND e.Date < @to		
        {WHERE_DAUS}
		AND e.OriginId in (SELECT ID from @SelectOrigins)
	GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, e.Date),0)
)DAUS
FULL OUTER JOIN
(
	SELECT DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0) Date             
            ,SUM(Revenue) Revenue
    FROM [dbo].[InstallationReportAgg6.0] WITH(NOLOCK)     
    WHERE Date >= @from AND Date < @to	       
        {WHERE_CRM}
        AND OriginId in (SELECT ID from @SelectOrigins)
    GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0) 
)REVENUE
	on DAUS.Date = REVENUE.Date
FULL OUTER JOIN
(
	SELECT 
      SUM(e.[Visitor]) Total_Visitor
      ,DATEADD(DAY, DATEDIFF(DAY, 0, e.Date),0) Date
	FROM [DynamicNoProblem].[dbo].[Exposure] e  WITH(NOLOCK)      
	WHERE e.Date >= @from AND e.Date < @to
         {WHERE_DAUS_Total}
		AND e.OriginId in (SELECT ID from #temp_origins)
	GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, e.Date),0)
)TOTAL_DAUS
	on TOTAL_DAUS.Date = COALESCE (DAUS.Date, REVENUE.Date)
ORDER BY Date desc
  
drop table #temp_origins";


        public NoProblem.Core.DataModel.Reports.Response.InjectionReportResponse Create(NoProblem.Core.DataModel.Reports.Request.InjectionReportRequest request)
        {
            DateTime fromDateFixed = request.FromDate;
            DateTime toDateFixed = request.ToDate.AddDays(1);
            //  FixDatesToUtcFullDays(ref fromDateFixed, ref toDateFixed);
            NoProblem.Core.DataModel.Reports.Response.InjectionReportResponse response = new NoProblem.Core.DataModel.Reports.Response.InjectionReportResponse();

            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    StringBuilder query;
                    string WhereCrm = string.Empty, WhereDaus = string.Empty, WhereDausTotal = string.Empty, InnerJoinDaus = string.Empty;

                    bool AllCampaign = true;
                    if (request.Origins != null && request.Origins.Length > 0)
                    {
                        query = new StringBuilder(queryInjectionReportWithOrigins);
                        string origins = "";
                        foreach (Guid _org in request.Origins)
                            origins += _org.ToString() + ";";
                        cmd.Parameters.AddWithValue("@origins", origins);
                        AllCampaign = false;
                    }
                    else
                    {
                        query = new StringBuilder(queryInjectionReport);
                    }
                    //      bool AllInjections = true;
                    if (request.InjectionId != Guid.Empty)
                    {
                        WhereCrm += " AND InjectionId = @InjectionId ";
                        WhereDaus += " AND di.InjectionId = @InjectionId ";
                        InnerJoinDaus = "inner join dbo.DausInjection di  WITH(NOLOCK) on  di.DausId = e.Id";
                        //            AllInjections = false;
                    }
                    else if (AllCampaign)
                        WhereDaus += " AND e.OriginId in (SELECT ID from #temp_origins) ";
                    else
                        WhereDaus += " AND e.OriginId in (SELECT ID from @SelectOrigins) ";

                    if (!string.IsNullOrEmpty(request.Country))
                    {
                        string _start = @"DECLARE @CountryId int
                                    SELECT @CountryId = DynamicNoProblem.dbo.GetCountryIdByIPName(@country)";
                        query.Insert(0, _start);
                        WhereDaus += " AND e.CountryId = @CountryId ";
                        WhereDausTotal += " AND e.CountryId = @CountryId ";
                        WhereCrm += " AND Country = @country ";
                        cmd.Parameters.AddWithValue("@country", request.Country);
                    }
                    query.Replace("{WHERE_DAUS}", WhereDaus);
                    query.Replace("{WHERE_CRM}", WhereCrm);
                    query.Replace("{WHERE_DAUS_Total}", WhereDausTotal);
                    query.Replace("{InnerJoin_DAUS}", InnerJoinDaus);
                    cmd.CommandText = query.ToString();
                    cmd.Parameters.AddWithValue("@from", fromDateFixed);
                    cmd.Parameters.AddWithValue("@to", toDateFixed);
                    cmd.Parameters.AddWithValue("@InjectionId", request.InjectionId);
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        InjectionReportResponseRow row = new InjectionReportResponseRow();
                        row.Date = (DateTime)reader["Date"];
                        int Total_Visitor = reader["Total_Visitor"] == DBNull.Value ? 0 : (int)reader["Total_Visitor"];
                        //          row.DAUS = (AllInjections && AllCampaign) ? Total_Visitor : (reader["Visitor"] == DBNull.Value ? 0 : (int)reader["Visitor"]);
                        row.DAUS = (reader["Visitor"] == DBNull.Value ? 0 : (int)reader["Visitor"]);
                        row.Revenue = reader["Revenue"] == DBNull.Value ? 0m : (decimal)reader["Revenue"];
                        row.PPI_DAU = Total_Visitor;
                        response.Add(row);
                    }
                    conn.Close();

                }
            }
            response.SetValues();
            return response;

        }
        const string QueryInjectionDrillDownWithOrigins = @"
DECLARE @SelectOrigins table
(
	ID uniqueidentifier
)
CREATE TABLE #temp_origins
(
	ID uniqueidentifier NOT NULL
)
INSERT INTO #temp_origins(ID)
SELECT New_originId
FROM dbo.New_originExtensionBase
WHERE New_IsDistribution = 1

INSERT INTO @SelectOrigins(ID)
SELECT New_originId
FROM dbo.New_originExtensionBase
WHERE New_originId in (
	SELECT value
	FROM dbo.fn_ParseDelimitedGuid(@origins, ';'))

DECLARE @TotalDaus int
SELECT 
      @TotalDaus = SUM(e.[Visitor])     
	FROM [DynamicNoProblem].[dbo].[Exposure] e WITH(NOLOCK)      
	WHERE Date >= @date AND Date < @to
	    {WHERE_DAUS_Total}
		AND OriginId in (SELECT ID from #temp_origins)
	

SELECT
	COALESCE (DAUS.OriginId, REVENUE.OriginId) OriginId
	,DAUS.Visitor
	,REVENUE.Revenue
	,@TotalDaus TotalDaus
	,ORIGIN.New_name Name
FROM
(
	SELECT 
      SUM(e.[Visitor]) Visitor      
      ,e.OriginId
	FROM [DynamicNoProblem].[dbo].[Exposure] e  WITH(NOLOCK)
      {InnerJoin_DAUS}
	WHERE e.Date >= @date AND e.Date < @to	
        AND e.OriginId IN (SELECT ID from @SelectOrigins)
		{WHERE_DAUS}
	GROUP BY e.OriginId
)DAUS
FULL OUTER JOIN
(
	SELECT            
            SUM(Revenue) Revenue
            ,OriginId
    FROM [dbo].[InstallationReportAgg6.0] WITH(NOLOCK)     
    WHERE Date >= @date AND Date < @to	        
        AND OriginId IN (SELECT ID from @SelectOrigins)
        {WHERE_CRM}
    GROUP BY OriginId 
)REVENUE
	on DAUS.OriginId = REVENUE.OriginId
LEFT JOIN
(
	SELECT New_originId, New_name
	FROM dbo.New_originExtensionBase
	
)ORIGIN
	on ORIGIN.New_originId = COALESCE(DAUS.OriginId, REVENUE.OriginId)

ORDER BY ORIGIN.New_name
  
drop table #temp_origins";

        const string QueryInjectionDrillDown = @"
SET ARITHABORT ON;

CREATE TABLE #temp_origins
(
	ID uniqueidentifier NOT NULL
)

INSERT INTO #temp_origins(ID)
SELECT New_originId
FROM dbo.New_originExtensionBase
WHERE New_IsDistribution = 1

DECLARE @TotalDaus int
SELECT 
      @TotalDaus = SUM(e.[Visitor])     
	FROM [DynamicNoProblem].[dbo].[Exposure] e WITH(NOLOCK)      
	WHERE Date >= @date AND Date < @to
	    {WHERE_DAUS_Total}
		AND OriginId in (SELECT ID from #temp_origins)
	

SELECT
	COALESCE (DAUS.OriginId, REVENUE.OriginId) OriginId
	,DAUS.Visitor
	,REVENUE.Revenue
	,@TotalDaus TotalDaus
	,ORIGIN.New_name Name
FROM
(
	SELECT 
      SUM(e.[Visitor]) Visitor      
      ,e.OriginId
	FROM [DynamicNoProblem].[dbo].[Exposure] e  WITH(NOLOCK)
        {InnerJoin_DAUS}
	WHERE e.Date >= @date AND e.Date < @to		
		{WHERE_DAUS}
	GROUP BY e.OriginId
)DAUS
FULL OUTER JOIN
(
	SELECT            
            SUM(Revenue) Revenue
            ,OriginId
    FROM [dbo].[InstallationReportAgg6.0] WITH(NOLOCK)     
    WHERE Date >= @date AND Date < @to	        
        {WHERE_CRM}
    GROUP BY OriginId 
)REVENUE
	on DAUS.OriginId = REVENUE.OriginId
LEFT JOIN
(
	SELECT New_originId, New_name
	FROM dbo.New_originExtensionBase
	
)ORIGIN
	on ORIGIN.New_originId = COALESCE(DAUS.OriginId, REVENUE.OriginId)

ORDER BY ORIGIN.New_name 

drop table #temp_origins";
        public List<InjectionReportDrillDownResponse> CreateDrillDown(NoProblem.Core.DataModel.Reports.Request.InjectionReportRequest request)
        {
            DateTime toDateFixed = request.ToDate.AddDays(1);
            List<InjectionReportDrillDownResponse> list = new List<InjectionReportDrillDownResponse>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    StringBuilder query;
                    string WhereCrm = string.Empty, WhereDaus = string.Empty, EhereDausTotal = string.Empty, InnerJoinDaus = string.Empty;
                    bool AllCampaign = true;
                    if (request.Origins != null && request.Origins.Length > 0)
                    {
                        query = new StringBuilder(QueryInjectionDrillDownWithOrigins);
                        string origins = "";
                        foreach (Guid _org in request.Origins)
                            origins += _org.ToString() + ";";
                        cmd.Parameters.AddWithValue("@origins", origins);
                        AllCampaign = false;
                    }
                    else
                        query = new StringBuilder(QueryInjectionDrillDown);
                    //bool AllInjections = true;
                    if (request.InjectionId != Guid.Empty)
                    {
                        WhereCrm += " AND InjectionId = @InjectionId ";
                        WhereDaus += " AND di.InjectionId = @InjectionId ";
                        InnerJoinDaus = "inner join dbo.DausInjection di  WITH(NOLOCK) on  di.DausId = e.Id";
                        //  AllInjections = false;
                    }
                    else if (AllCampaign)
                        WhereDaus += " AND e.OriginId in (SELECT ID from #temp_origins) ";
                    else
                        WhereDaus += " AND e.OriginId in (SELECT ID from @SelectOrigins) ";
                    if (!string.IsNullOrEmpty(request.Country))
                    {
                        string _start =
@"DECLARE @CountryId int
SELECT @CountryId = DynamicNoProblem.dbo.GetCountryIdByIPName(@country)
";
                        query.Insert(0, _start);
                        cmd.Parameters.AddWithValue("@country", request.Country);
                        WhereDaus += " AND e.CountryId = @CountryId ";
                        EhereDausTotal += " AND e.CountryId = @CountryId ";
                        WhereCrm += " AND Country = @country ";
                    }
                    query.Replace("{WHERE_DAUS}", WhereDaus);
                    query.Replace("{WHERE_CRM}", WhereCrm);
                    query.Replace("{WHERE_DAUS_Total}", EhereDausTotal);
                    query.Replace("{InnerJoin_DAUS}", InnerJoinDaus);
                    cmd.CommandText = query.ToString();
                    cmd.Parameters.AddWithValue("@date", request.FromDate);
                    cmd.Parameters.AddWithValue("@to", toDateFixed);
                    cmd.Parameters.AddWithValue("@InjectionId", request.InjectionId);
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        InjectionReportDrillDownResponse row = new InjectionReportDrillDownResponse();
                        row.OriginName = reader["Name"] == DBNull.Value ? "-UNKNOWN-" : (string)reader["Name"];
                        int Total_Visitor = reader["TotalDaus"] == DBNull.Value ? 0 : (int)reader["TotalDaus"];
                        //row.DAUS = (AllInjections) ? Total_Visitor : (reader["Visitor"] == DBNull.Value ? 0 : (int)reader["Visitor"]);
                        row.DAUS = (reader["Visitor"] == DBNull.Value ? 0 : (int)reader["Visitor"]);
                        row.Revenue = reader["Revenue"] == DBNull.Value ? 0m : (decimal)reader["Revenue"];
                        row.PPI_DAU = Total_Visitor;
                        row.SetValues();
                        list.Add(row);
                    }
                    conn.Close();

                }
            }
            return list;
        }


    }
}
