﻿using System;
using System.Collections.Generic;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class RequestsReportCreator : XrmUserBase
    {
        #region Ctor
        internal RequestsReportCreator(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal DML.Reports.RequestsReportResponse Create(DateTime? fromDate, DateTime? toDate, Guid expertiseId, string caseNumber, DML.Reports.RequestsReportRequest.CaseType caseType, Guid affiliateOrigin, 
            Guid caseOriginId, int? suppliersProvided, int pageNumber, int pageSize, string url, bool onlyForUpsales, Guid ToolbarId,
            int? NumberOfUpsales, RequestsReportRequest.eCallDurationType WithMaxCallDuration, bool OnlyRejectByEmailOrName, int SliderType)
        {
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DML.Reports.RequestsReportResponse response = new NoProblem.Core.DataModel.Reports.RequestsReportResponse();
            List<DML.Reports.IncidentData> dataList = null;
            int totalRows;
            int notToPayCount;
            if (string.IsNullOrEmpty(caseNumber) && fromDate.HasValue && toDate.HasValue)
            {
                DateTime fromDateFixed = fromDate.Value;
                DateTime toDateFixed = toDate.Value;
                FixDatesToFullDays(ref fromDateFixed, ref toDateFixed);
                dataList = dal.GetRequestsReport(fromDateFixed, toDateFixed, expertiseId, caseNumber, caseType, suppliersProvided, 
                    affiliateOrigin, caseOriginId, pageNumber, pageSize, out totalRows, out notToPayCount, url, onlyForUpsales,
                    ToolbarId, NumberOfUpsales, WithMaxCallDuration, OnlyRejectByEmailOrName, SliderType);
            }
            else
            {
                dataList = dal.GetRequestsReport(null, null, expertiseId, caseNumber, caseType, suppliersProvided, 
                    affiliateOrigin, caseOriginId, pageNumber, pageSize, out totalRows, out notToPayCount, url, onlyForUpsales,
                    ToolbarId, NumberOfUpsales, WithMaxCallDuration, OnlyRejectByEmailOrName, SliderType);
            }
            if (affiliateOrigin != Guid.Empty)
            {
                foreach (var item in dataList)
                {
                    item.CaseId = Guid.Empty;
                    item.ConsumerId = Guid.Empty;
                    item.NumberOfAdvertisersProvided = -1;
                    item.NumberOfRequestedAdvertisers = -1;
                    item.Revenue = -1;
                }
            }
            response.Incidents = dataList;
            response.TotalRows = totalRows;
            response.TotalPages = (int)Math.Ceiling(((double)totalRows) / ((double)pageSize));
            response.CurrentPage = pageNumber;
            response.NotToPay = notToPayCount;
            response.ToPay = totalRows - notToPayCount;
            return response;
        }

        #endregion
    }
}
