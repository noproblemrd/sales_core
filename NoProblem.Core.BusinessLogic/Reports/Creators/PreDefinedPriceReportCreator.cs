﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class PreDefinedPriceReportCreator : XrmUserBase
    {
        #region Ctor
        internal PreDefinedPriceReportCreator(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        internal PreDefinedPriceReportResponse Create(PreDefinedPriceReportRequest request)
        {
            PreDefinedPriceReportResponse response = new PreDefinedPriceReportResponse();
            
            DateTime fromDate = request.FromDate;
            DateTime toDate = request.ToDate;
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            List<PreDefinedPriceData> dataList = null;
            if (request.HeadingId != Guid.Empty)
            {
                DataAccessLayer.IncidentAccount incAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                dataList = incAccDal.GetAvgPricePayedInDates(fromDate, toDate, request.HeadingId);
                DataAccessLayer.PredefinedPrice preDefDal = new NoProblem.Core.DataAccessLayer.PredefinedPrice(XrmDataContext);
                DataAccessLayer.PrimaryExpertise expDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
                string code = expDal.GetCode(request.HeadingId);
                preDefDal.GetPreDefinedPrice(dataList, code);
            }
            else
            {
                dataList = new List<PreDefinedPriceData>();
                DataAccessLayer.DashboardData dal = new NoProblem.Core.DataAccessLayer.DashboardData(XrmDataContext);
                var dashData = dal.GetRangeData(fromDate, toDate);
                foreach (var item in dashData)
                {
                    if (item.AveragePricePerCall == 0)
                    {
                        continue;
                    }
                    PreDefinedPriceData data = new PreDefinedPriceData();
                    data.ActualPrice = item.AveragePricePerCall;
                    data.PreDefinedPrice = item.AveragePreDefinedPrice;
                    data.Date = item.CreatedOn;
                    dataList.Add(data);
                }
            }
            response.DataList = dataList.OrderBy(x => x.Date).ToList();
            return response;
        }
    }
}
