﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class SalesmenReportCreator : XrmUserBase
    {
        #region Ctor
        public SalesmenReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        internal List<DML.Reports.SalesmanDeposits> Create(DateTime fromDate, DateTime toDate)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var publishers = accountRepositoryDal.GetAllPublishers();
            DataAccessLayer.SupplierPricing spDal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            FixDatesToFullDays(ref fromDate, ref toDate);
            List<DML.Reports.SalesmanDeposits> allData = new List<NoProblem.Core.DataModel.Reports.SalesmanDeposits>();
            foreach (var user in publishers)
            {
                DoQueryForPublisher(fromDate, toDate, spDal, allData, user.accountid, user.name);
            }
            DoQueryForPublisher(fromDate, toDate, spDal, allData, Guid.Empty, "No Manager");
            return allData;
        }

        private void DoQueryForPublisher(DateTime fromDate, DateTime toDate, DataAccessLayer.SupplierPricing spDal, List<DML.Reports.SalesmanDeposits> allData, Guid userId, string userName)
        {
            List<DML.Reports.SalesmanDepositData> dataList = spDal.GetDepositsByAccountManager(userId, fromDate, toDate);
            if (dataList.Count > 0)
            {
                foreach (var item in dataList)
                {
                    item.DepositCreatedOn = FixDateToLocal(item.DepositCreatedOn);
                    if (item.AccountApprovedOn.HasValue)
                    {
                        item.AccountApprovedOn = FixDateToLocal(item.AccountApprovedOn.Value);
                    }
                    item.SalesmanName = userName;
                }
                DML.Reports.SalesmanDeposits container = new NoProblem.Core.DataModel.Reports.SalesmanDeposits();
                container.Deposits = dataList;
                container.SalesmanName = userName;
                allData.Add(container);
            }
        }
    }
}
