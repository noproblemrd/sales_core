﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class RefundReportCreator : XrmUserBase
    {
        #region Ctor
        internal RefundReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        internal NoProblem.Core.DataModel.Reports.RefundReportResponse Create(DateTime fromDate, DateTime toDate, Guid headingId, int pageSize, int pageNumber)
        {
            NoProblem.Core.DataModel.Reports.RefundReportResponse response = new NoProblem.Core.DataModel.Reports.RefundReportResponse();
            FixDatesToFullDays(ref fromDate, ref toDate);
            DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            int totalRows;
            var dataList = dal.RefundReportQuery(fromDate, toDate, headingId, pageSize, pageNumber, out totalRows);
            response.DataList = dataList;
            response.TotalRows = totalRows;
            response.CurrentPage = pageNumber;
            response.TotalPages = (int)Math.Ceiling(((double)totalRows) / ((double)pageSize));
            return response;   
        }
    }
}
