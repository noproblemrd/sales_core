﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class BrokersSummaryReportCreator : XrmUserBase
    {
        public BrokersSummaryReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<DataModel.Reports.BrokerSummaryRow> Create(DateTime from, DateTime to, bool calculateBillingRateByDates)
        {
            DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            //  Dictionary<Guid, decimal> acceptanceRateDict = GetAcceptanceRate(from, to, dal);
            FixDatesToUtcFullDaysNotIncludingToDate(ref from, ref to);
            DataAccessLayer.IncidentAccount.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_incidentaccount>.SqlParametersList();
            parameters.Add("@from", from);
            parameters.Add("@to", to);
            var reader = dal.ExecuteReader(query, parameters);
            Dictionary<Guid, double> billingRatePerDateDic = null;
            if (calculateBillingRateByDates)
            {
                billingRatePerDateDic = new Dictionary<Guid, double>();
                var billingRateReader = dal.ExecuteReader(queryBillingRateByDates, parameters);
                foreach (var row in billingRateReader)
                {
                    billingRatePerDateDic.Add((Guid)row["id"], row["brate"] != DBNull.Value ? (double)row["brate"] : 0);
                }
            }
            List<DataModel.Reports.BrokerSummaryRow> retVal = new List<NoProblem.Core.DataModel.Reports.BrokerSummaryRow>();
            foreach (var row in reader)
            {
                DataModel.Reports.BrokerSummaryRow data = new NoProblem.Core.DataModel.Reports.BrokerSummaryRow();
                data.Name = Convert.ToString(row["Name"]);
                data.Count = row["cnt"] != DBNull.Value ? (int)row["cnt"] : 0;
                data.Money = row["money"] != DBNull.Value ? (decimal)row["money"] : 0m;
                data.RefundCount = (row["refunds"] == DBNull.Value) ? -1 : (int)row["refunds"];
                data.RefundSum = (row["refunds_money"] == DBNull.Value) ? -1m : (decimal)row["refunds_money"];
                data.RevenueGross = row["GrossRevenue"] != DBNull.Value ? (decimal)row["GrossRevenue"] : 0m;
                data.BillingRate = (row["BillingRate"] == DBNull.Value) ? 0m : (decimal)row["BillingRate"];
                if (calculateBillingRateByDates)
                {
                    double bRate;
                    if (billingRatePerDateDic.TryGetValue((Guid)row["AccountId"], out bRate))
                    {
                        data.BillingRatePerDates = bRate;
                    }
                }
                data.SupplierId = (Guid)row["AccountId"];
                data.ParentAccountId = row["ParentAccountId"] != DBNull.Value ? (Guid)row["ParentAccountId"] : Guid.Empty;
                data.IsSubAccount = (bool)row["subaccount"];
                retVal.Add(data);
            }
            var subAccounts = retVal.Where(x => x.IsSubAccount);
            var rets = retVal.Except(subAccounts);
            foreach (var item in subAccounts)
            {
                var parent = rets.First(x => x.SupplierId == item.ParentAccountId);
                parent.BillingRate = 0;
                parent.BillingRatePerDates = 0;
                parent.Count += item.Count;
                parent.Money += item.Money;
                parent.Rank = 0;
                parent.RefundCount += item.RefundCount;
                parent.RefundSum += item.RefundSum;
                parent.RevenueGross += item.RevenueGross;
            }
            return rets.ToList();
        }

        private Dictionary<Guid, decimal> GetAcceptanceRate(DateTime from, DateTime to, DataAccessLayer.IncidentAccount dal)
        {
            Dictionary<Guid, decimal> dict = new Dictionary<Guid, decimal>();
            from = from.Date;
            to = to.Date.AddDays(-1);
            DataAccessLayer.IncidentAccount.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_incidentaccount>.SqlParametersList();
            parameters.Add("@from", from);
            parameters.Add("@to", to);
            var reader = dal.ExecuteReader(queryAcceptanceRate, parameters);
            foreach (var row in reader)
            {
                dict.Add((Guid)row["accountId"], (decimal)row["acceptanceRate"]);
            }
            return dict;
        }

        private const string queryAcceptanceRate =
           @"
			select accountId, (cast(sum(successCnt) as decimal) / cast(sum(allCnt) as decimal) * 100.0) as acceptanceRate from tbl_partners_acceptance_rate_aggregation
			where createdon between @from and @to
			group by accountid
			";
        /*
        private const string query =
            @"
            select acc.Name, COUNT(ia.New_incidentaccountId) cnt, SUM(isnull(ia.new_potentialincidentprice, 0)) money, acc.accountid, acc.New_BillingRate
            from New_incidentaccount ia with(nolock) 
            join account acc with(nolock) on ia.new_accountid = acc.AccountId
            where acc.New_IsLeadBuyer = 1
            and ia.New_tocharge = 1
            and ia.CreatedOn between @from and @to
            group by acc.Name, acc.accountid, acc.New_BillingRate
            order by acc.Name
            ";
         * */
        private const string query =
            @"            
			select
				ACCOUNT.AccountId
				,ACCOUNT.Name
				,ISNULL(ACCOUNT.New_BillingRate, 0) BillingRate
				,FINANCE.cnt
				,FINANCE.money
				,REFUND.refunds
				,REFUND.refunds_money
				,(ISNULL(FINANCE.money, 0) + ISNULL(REFUND.refunds_money, 0)) GrossRevenue	
                ,ACCOUNT.subaccount
                ,ACCOUNT.ParentAccountId
			from
			(
				SELECT acc.accountid, acc.Name, accb.New_BillingRate, CAST(0 AS BIT) as subaccount, null as ParentAccountId
                FROM dbo.AccountExtensionBase accb with(nolock)
	                INNER JOIN dbo.AccountBase acc with(nolock) on accb.AccountId = acc.AccountId                
                WHERE accb.New_IsLeadBuyer = 1
                union
                SELECT acc2.accountid, acc2.Name, accb2.New_BillingRate, CAST(1 AS BIT), acc2.ParentAccountId
                FROM dbo.AccountExtensionBase accb2 with(nolock)
	                INNER JOIN dbo.AccountBase acc2 with(nolock) on accb2.AccountId = acc2.AccountId                
	                inner join Account parent with(NOLOCK) on parent.AccountId = acc2.ParentAccountId
                WHERE parent.New_IsLeadBuyer = 1	
			)ACCOUNT
			LEFT JOIN
			(
				select COUNT(ia.New_incidentaccountId) cnt, SUM(isnull(ia.new_potentialincidentprice, 0)) money, ia.new_accountid--, acc.New_BillingRate
				from dbo.New_incidentaccountExtensionBase ia with(nolock) 
					inner join dbo.New_incidentaccountBase iab with(nolock) on ia.New_incidentaccountId = iab.New_incidentaccountId
				WHERE ia.New_tocharge = 1
				and iab.CreatedOn between @from and @to
				group by ia.new_accountid
	
			) FINANCE on ACCOUNT.AccountId = FINANCE.new_accountid           
			LEFT JOIN
			(
				select COUNT(ia.New_incidentaccountId) refunds, SUM(isnull(ia.new_potentialincidentprice, 0)) refunds_money, ia.new_accountid--, acc.New_BillingRate
				from dbo.New_incidentaccountExtensionBase ia with(nolock) 
					inner join dbo.New_incidentaccountBase iab with(nolock) on ia.New_incidentaccountId = iab.New_incidentaccountId
				WHERE ia.New_RefundStatus = 3
				and iab.CreatedOn between @from and @to
				group by ia.new_accountid
			)REFUND on REFUND.new_accountid = ACCOUNT.AccountId
			ORDER BY ACCOUNT.Name";

        private const string queryBillingRateByDates =
            @"
declare @supplierId uniqueidentifier

create table #tresults ( id uniqueidentifier, brate float)

DECLARE db_cursor CURSOR FOR  
select distinct accountid
from accountextensionbase with(nolock)
where New_IsLeadBuyer = 1

OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @supplierId

WHILE @@FETCH_STATUS = 0
BEGIN

	create table #tmpRateTable (cnt int, tocharge bit)

	insert into #tmpRateTable
	select COUNT(*) cnt, ia.New_tocharge
	from New_incidentaccount ia with(nolock)
	join IncidentExtensionBase inc with(nolock) on ia.new_incidentid = inc.incidentid
	where 
	ia.CreatedOn between @from and @to
	and ia.new_accountid = @supplierId
	and ia.new_referred = 1
	and ia.new_lostoncustomersfault = 0
	and ia.New_isrejected = 0
	group by ia.New_tocharge

	declare @billed int
	select @billed = COUNT(*)
	FROM New_incidentaccount ia with(nolock)
	where
	ia.CreatedOn between @from and @to
	and ia.new_accountid = @supplierId
	and ia.new_referred = 1
	and statuscode in (10, 12, 13)

	declare @billingRate float
	if @billed > 3
	begin	
		declare @good float	
		select @good = cnt from #tmpRateTable where tocharge = 1
		if @good is null or @good = 0
		begin
			set @billingRate = 0.01
		end
		else
		begin
			declare @total float
			select @total = SUM(cnt) from #tmpRateTable	
			set @billingRate = @good / @total
		end	
	end
	else
	begin
		set @billingRate = 1
	end

	drop table #tmpRateTable

	if @billingRate is null
	begin
		set @billingRate = 1
	end
	
	insert into #tresults ( id, brate ) values ( @supplierId, @billingRate )

	FETCH NEXT FROM db_cursor INTO @supplierId   
END   

CLOSE db_cursor   
DEALLOCATE db_cursor

select * from #tresults

drop table #tresults
";

    }
}
