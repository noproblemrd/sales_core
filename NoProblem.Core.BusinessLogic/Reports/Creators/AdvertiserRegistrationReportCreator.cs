﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using NoProblem.Core.DataModel.Reports.Request;
using NoProblem.Core.DataModel.Reports.Response;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class AdvertiserRegistrationReportCreator : XrmUserBase
    {
        public AdvertiserRegistrationReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private delegate AdvertiserRegistrationResponseCollection AsyncAdvertiserRegistrationResponse(AdvertiserRegistrationRequest request);

        public AdvertiserRegistrationResponse AdvertiserRegistrationReport(AdvertiserRegistrationRequest request)
        {
            request.SetToDate();
            AdvertiserRegistrationResponse _response = new AdvertiserRegistrationResponse();
            AsyncAdvertiserRegistrationResponse caller_current = new AsyncAdvertiserRegistrationResponse(_AdvertiserRegistrationReport);
            IAsyncResult result_current = null;
            AsyncAdvertiserRegistrationResponse caller_compare = new AsyncAdvertiserRegistrationResponse(_AdvertiserRegistrationReport);
            IAsyncResult result_compare = null;
            result_current = caller_current.BeginInvoke(request, null, null);
            if (request.CompareRequest != null)
            {
                result_compare = caller_compare.BeginInvoke(request.CompareRequest, null, null);
            }
            _response.RegiterUsers = _AdvertiserRegistrationReport_RegisterUsers(request);
            _response.CurrentData = caller_current.EndInvoke(result_current);
            if (result_compare != null)
                _response.CompareData = caller_compare.EndInvoke(result_compare);
            _response.SetValues(request.ShowStep1, (request.CompareRequest == null ? false : request.CompareRequest.ShowStep1), request.From, request.OriginalToDate);
            return _response;
        }

        private AdvertiserRegistrationResponseCollection _AdvertiserRegistrationReport(AdvertiserRegistrationRequest request)
        {
            AdvertiserRegistrationResponseCollection coll = new AdvertiserRegistrationResponseCollection();
            string query;// = RegisterQueryWithStep1;
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    //request.landingPage = eRegisterLandingPageType.C;
                    query = request.Plan == eRegisterPlan.All ? RegisterQuery : RegisterQuery_Plan;
                    string str_account, str_reg;
                    GetWhereSql_ToUseJoin(out str_account, out str_reg, request, cmd);
                    StringBuilder sb = new StringBuilder(query);
                    sb.Replace("{ACCOUNT_WHERE}", str_account);
                    sb.Replace("{REGISTRATION_WHERE}", str_reg);
                    //sb.Replace("{ShowStep1}", (request.ShowStep1 ? string.Empty : ((string.IsNullOrEmpty(_join)) ? "WHERE ISNULL(ACCOUNT.stepinregistration2014, 0) > 0" : "WHERE step > 0")));
                    sb.Replace("{ShowStep1}", (request.ShowStep1 ? string.Empty : "WHERE step > 0"));
                    cmd.Parameters.AddWithValue("@from", request.From);
                    cmd.Parameters.AddWithValue("@to", request.To);
                    cmd.Connection = conn;
                    cmd.CommandText = sb.ToString();
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        AdvertiserRegistrationResponseData _data = new AdvertiserRegistrationResponseData();
                        _data.Users = (int)reader["_count"];
                        _data.Step = (int)reader["step"];
                        coll.Add(_data);
                    }
                    conn.Close();
                }
            }
            return coll;
        }

        private string GetWhereSql_ToUseJoin(out string str_account, out string str_reg, AdvertiserRegistrationRequest request, SqlCommand cmd)
        {
            string plan_result = string.Empty;
            str_account = string.Empty;
            str_reg = string.Empty;
            switch (request.Plan)
            {
                case (eRegisterPlan.FeaturedListing):
                    cmd.Parameters.AddWithValue("@Plan", SubscriptionPlansContract.Plans.FEATURED_LISTING);
                    goto default;
                case (eRegisterPlan.LeadsBooster):
                    cmd.Parameters.AddWithValue("@Plan", SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING);
                    goto default;
                case (eRegisterPlan.All):
                    break;
                default:
                    plan_result = " AND acc.New_SubscriptionPlan = @Plan ";
                    break;
            }

            if (request.SalseManId.HasValue)
            {
                if (request.SalseManId.Value == Guid.Empty)
                {
                    str_account += " AND acc.New_ManagerId IS NULL ";
                }
                else
                {
                    str_account += " AND acc.new_managerid = @ManagerId ";
                    cmd.Parameters.AddWithValue("@ManagerId", request.SalseManId.Value);
                }
            }
            
            switch (request.CameFrom)
            {
                case (eRegisterCameFrom.Claim):
                    cmd.Parameters.AddWithValue("@Claim", eRegisterCameFrom.Claim.ToString());
                    goto default;
                case (eRegisterCameFrom.RecruitmentSlider):
                    cmd.Parameters.AddWithValue("@Claim", eRegisterCameFrom.RecruitmentSlider.ToString());
                    string _flavor = request.GetSliderFlavor;
                    if (_flavor != null)
                    {
                        str_reg += " AND reg.Flavor = @Flavor ";
                        cmd.Parameters.AddWithValue("@Flavor", _flavor);
                    }
                    goto default;
                case (eRegisterCameFrom.Slider):
                    cmd.Parameters.AddWithValue("@Claim", eRegisterCameFrom.Slider.ToString());
                    goto default;
                case (eRegisterCameFrom.Email):
                    cmd.Parameters.AddWithValue("@Claim", eRegisterCameFrom.Email.ToString());
                    string _flavorEmail = request.GetSliderFlavor;
                    if (_flavorEmail != null)
                    {
                        str_reg += " AND reg.Flavor = @Flavor ";
                        cmd.Parameters.AddWithValue("@Flavor", _flavorEmail);
                    }                    
                    goto default;

                case (eRegisterCameFrom.Other):
                    cmd.Parameters.AddWithValue("@Claim1", eRegisterCameFrom.Claim.ToString());
                    cmd.Parameters.AddWithValue("@Claim2", eRegisterCameFrom.Slider.ToString());
                    cmd.Parameters.AddWithValue("@Claim3", eRegisterCameFrom.RecruitmentSlider.ToString());
                    str_reg += " AND ISNULL(reg.UrlReferer, '') NOT IN(@Claim1, @Claim2, @Claim3) ";
                    break;
                case (eRegisterCameFrom.All):
                    break;
                default:
                    str_reg += " AND reg.UrlReferer = @Claim ";
                    break;

            }

            switch(request.landingPage)
            {
                case(eRegisterLandingPageType.All):
                    break;
                case (eRegisterLandingPageType.A):
                    cmd.Parameters.AddWithValue("@LandingPage", eRegisterLandingPageType.A.ToString());
                    goto default;
                case (eRegisterLandingPageType.B):
                    cmd.Parameters.AddWithValue("@LandingPage", eRegisterLandingPageType.B.ToString());
                    goto default;
                case (eRegisterLandingPageType.C):
                    cmd.Parameters.AddWithValue("@LandingPage", eRegisterLandingPageType.C.ToString());
                    goto default;
                case (eRegisterLandingPageType.D):
                    cmd.Parameters.AddWithValue("@LandingPage", eRegisterLandingPageType.D.ToString());
                    goto default;
                default:
                    str_reg += " AND reg.LandingpageType=@LandingPage ";                    
                    break;
            }

            return plan_result;
        }

        private List<DatetimeIntPair> _AdvertiserRegistrationReport_RegisterUsers(AdvertiserRegistrationRequest request)
        {
            List<DatetimeIntPair> list = new List<DatetimeIntPair>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    string str_account, str_reg, plan;
                    StringBuilder sb = new StringBuilder(RegisterUsersQuery);
                    plan = GetWhereSql_ToUseJoin(out str_account, out str_reg, request, cmd);
                    if (!string.IsNullOrEmpty(str_reg))
                        str_account += str_reg;
                    if (!string.IsNullOrEmpty(plan))
                        str_account += plan;
                    sb = sb.Replace("{ACCOUNT_WHERE}", str_account);
                    cmd.Parameters.AddWithValue("@from", request.From);
                    cmd.Parameters.AddWithValue("@to", request.To);
                    cmd.Connection = conn;
                    cmd.CommandText = sb.ToString();
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DatetimeIntPair _data = new DatetimeIntPair((DateTime)reader["date"], (int)reader["Users"]);
                        list.Add(_data);
                    }
                    conn.Close();
                }
            }
            return list;
        }

        private const string RegisterQuery = @"
CREATE TABLE #temp_id
                (	                
	                Id uniqueidentifier	
                )
                CREATE TABLE #temp
                (
	                step int,
	                [_count] int	                
                )
                
                INSERT INTO #temp_id
					(Id)
                SELECT reg.Id
				FROM dbo.RegistrationHits reg 
				WHERE reg.date	>= @from and reg.date < @to
				    {REGISTRATION_WHERE}

				INSERT INTO #temp
					(step, _count)
                SELECT 0, COUNT(*)
				FROM dbo.RegistrationHits reg 
				WHERE reg.date	>= @from and reg.date < @to
				    {REGISTRATION_WHERE}

                DECLARE @i int = 1
                WHILE(@i < 9)
                BEGIN
					INSERT INTO #temp
						(step, _count)
					SELECT @i, COUNT(*)
					FROM dbo.AccountExtensionBase acc
						INNER JOIN dbo.AccountBase a on acc.AccountId = a.AccountId
						INNER JOIN #temp_id t on CAST(acc.New_RegistrationHitsId as uniqueidentifier) = t.Id
					WHERE (CASE WHEN acc.new_stepinregistration2014 < 6 THEN acc.new_stepinregistration2014
	                    WHEN acc.new_stepinregistration2014 = 6 AND ISNULL(acc.New_CheckoutButtonClicked, 0) = 0 THEN 6
	                    ELSE acc.new_stepinregistration2014 + 1 
	                    END) >= @i
						AND acc.New_RegistrationHitsId IS NOT NULL	                
						AND ISNULL(acc.new_legacyadvertiser2014, 0) = 0
						AND ISNULL(acc.new_isleadbuyer, 0) = 0
					        {ACCOUNT_WHERE}

					set @i = @i + 1
                END
                
               SELECT step, _count
               FROM #temp              
                {ShowStep1}
               ORDER BY step
                 
               DROP TABLE #temp
               DROP TABLE #temp_id";

        private const string RegisterQuery_Plan = @"       
                 CREATE TABLE #temp_id
                (	                
	                Id uniqueidentifier	
                )
                CREATE TABLE #temp
                (
	                step int,
	                [_count] int	                
                )
                
                INSERT INTO #temp_id
					(Id)
                SELECT reg.Id
				FROM dbo.RegistrationHits reg 
				WHERE reg.date	>= @from and reg.date < @to
				    {REGISTRATION_WHERE}

				INSERT INTO #temp
					(step, _count)
                SELECT 0, COUNT(*)
				FROM dbo.RegistrationHits reg 
				WHERE reg.date	>= @from and reg.date < @to
				    {REGISTRATION_WHERE}

                DECLARE @i int = 1
                WHILE(@i < 3)
                BEGIN
					INSERT INTO #temp
						(step, _count)
					SELECT @i, COUNT(*)
					FROM dbo.AccountExtensionBase acc
						INNER JOIN dbo.AccountBase a on acc.AccountId = a.AccountId
						INNER JOIN #temp_id t on CAST(acc.New_RegistrationHitsId as uniqueidentifier) = t.Id
					WHERE (CASE WHEN acc.new_stepinregistration2014 < 6 THEN acc.new_stepinregistration2014
	                    WHEN acc.new_stepinregistration2014 = 6 AND ISNULL(acc.New_CheckoutButtonClicked, 0) = 0 THEN 6
	                    ELSE acc.new_stepinregistration2014 + 1 
	                    END) >= @i
						AND acc.New_RegistrationHitsId IS NOT NULL	                
						AND ISNULL(acc.new_legacyadvertiser2014, 0) = 0
						AND ISNULL(acc.new_isleadbuyer, 0) = 0
					        {ACCOUNT_WHERE}
					set @i = @i + 1
                END
				
				WHILE(@i < 9)
                BEGIN
					INSERT INTO #temp
						(step, _count)
					SELECT @i, COUNT(*)
					FROM dbo.AccountExtensionBase acc
						INNER JOIN dbo.AccountBase a on acc.AccountId = a.AccountId
						INNER JOIN #temp_id t on CAST(acc.New_RegistrationHitsId as uniqueidentifier) = t.Id
					WHERE (CASE WHEN acc.new_stepinregistration2014 < 6 THEN acc.new_stepinregistration2014
	                    WHEN acc.new_stepinregistration2014 = 6 AND ISNULL(acc.New_CheckoutButtonClicked, 0) = 0 THEN 6
	                    ELSE acc.new_stepinregistration2014 + 1 
	                    END) >= @i
						AND acc.New_RegistrationHitsId IS NOT NULL	                
						AND ISNULL(acc.new_legacyadvertiser2014, 0) = 0
						AND ISNULL(acc.new_isleadbuyer, 0) = 0
					    AND acc.New_SubscriptionPlan = @Plan 	
                            {ACCOUNT_WHERE}
							
					set @i = @i + 1
                END
                
               SELECT step, _count
               FROM #temp              
                {ShowStep1}
               ORDER BY step
                
               DROP TABLE #temp
               DROP TABLE #temp_id";

        private const string RegisterUsersQuery = @"
                    SELECT dateadd(DAY, 0, datediff(DAY, 0, reg.date)) date, 
						   COUNT(*) Users         
	                FROM dbo.AccountExtensionBase acc
		                INNER JOIN dbo.AccountBase a on acc.AccountId = a.AccountId
                        INNER JOIN dbo.RegistrationHits reg on reg.Id = CAST(acc.New_RegistrationHitsId as uniqueidentifier)
	                WHERE acc.new_stepinregistration2014 = 7
		                AND reg.date >= @from and reg.date < @to
                        AND ISNULL(acc.new_legacyadvertiser2014, 0) = 0
					    AND ISNULL(acc.new_isleadbuyer, 0) = 0
                            {ACCOUNT_WHERE}
                   GROUP BY dateadd(DAY, 0, datediff(DAY, 0, reg.date))
                    ORDER BY dateadd(DAY, 0, datediff(DAY, 0, reg.date))";
    }
}
