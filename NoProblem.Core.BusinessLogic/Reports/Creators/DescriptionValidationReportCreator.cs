﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class DescriptionValidationReportCreator : XrmUserBase
    {
        public DescriptionValidationReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public DataModel.Reports.GetValidationReportDropDownListsResponse GetDropDownLists()
        {
            DataModel.Reports.GetValidationReportDropDownListsResponse retVal = new NoProblem.Core.DataModel.Reports.GetValidationReportDropDownListsResponse();
            retVal.Flavors = GetListItems("new_flavor");
            retVal.Events = GetListItems("new_event");
            retVal.ZipCodeCheckNames = GetListItems("new_zipcodequalitycheckname");
            retVal.Steps = GetListItems("new_step");
            retVal.Controls = GetListItems("new_control");
            return retVal;
        }

        private List<string> GetListItems(string fieldName)
        {
            List<string> retVal = new List<string>();
            string query =
                "select distinct " + fieldName + " from new_descriptionvalidationlog with(nolock) where deletionstatecode = 0";
            DataAccessLayer.DescriptionValidationLog dal = new NoProblem.Core.DataAccessLayer.DescriptionValidationLog(XrmDataContext);
            var reader = dal.ExecuteReader(query);
            foreach (var row in reader)
            {
                if (row[fieldName] != DBNull.Value)
                    retVal.Add((string)row[fieldName]);
            }
            return retVal;
        }

        public DataModel.Reports.DescriptionValidationBasicReportResponse CreateBasicReport(
            DateTime fromDate,
            DateTime toDate,
            DataModel.enumQualityCheckName? qualityCheckName,
            int pageSize,
            int pageNumber,
            bool? passed,
            string flavor,
            string zipCodeCheckName,
            string eventt,
            string step,
            string control)
        {
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate));
            parameters.Add(new KeyValuePair<string, object>("@PageNum", pageNumber));
            parameters.Add(new KeyValuePair<string, object>("@PageSize", pageSize));
            string query =
                @"WITH PagerTbl AS
                ( 
                    select ROW_NUMBER() OVER(ORDER BY createdon desc) AS RowNum,
                    new_description, new_keyword, new_phone, 
                    new_heading, new_qualitycheckname, new_region, 
                    new_session, new_url, new_originidname, createdon,
                    new_flavor, new_zipcodequalitycheckname, new_event, new_step, new_control
                    from
                    new_descriptionvalidationlog with(nolock)
                    where deletionstatecode = 0
                    and createdon between @from and @to ";
            if (qualityCheckName.HasValue)
            {
                query += " and new_qualitycheckname = @qualityCheckName ";
                parameters.Add(new KeyValuePair<string, object>("@qualityCheckName", qualityCheckName.Value.ToString()));
            }
            if (passed.HasValue)
            {
                query +=
                        @" and (select count(*)                         
                        from incident inc with(nolock)
	                    where inc.deletionstatecode = 0 and inc.new_sessionid = new_session and inc.statuscode != 200014) ";
                if (passed.Value)
                {
                    query += " > 0 ";
                }
                else
                {
                    query += " = 0 ";
                }
            }
            if (!string.IsNullOrEmpty(flavor))
            {
                query += @" and new_flavor = @flavor ";
                parameters.Add(new KeyValuePair<string, object>("@flavor", flavor));
            }
            if (!string.IsNullOrEmpty(zipCodeCheckName))
            {
                query += @" and new_zipcodequalitycheckname = @zipCodeCheckName ";
                parameters.Add(new KeyValuePair<string, object>("@zipCodeCheckName", zipCodeCheckName));
            }
            if (!string.IsNullOrEmpty(eventt))
            {
                query += @" and new_event = @event ";
                parameters.Add(new KeyValuePair<string, object>("@event", eventt));
            }
            if (!string.IsNullOrEmpty(step))
            {
                query += @" and new_step = @step ";
                parameters.Add(new KeyValuePair<string, object>("@step", step));
            }
            if (!string.IsNullOrEmpty(control))
            {
                query += @" and new_control = @control ";
                parameters.Add(new KeyValuePair<string, object>("@control", control));
            }
            query += @")
                    SELECT * , (select max(RowNum) from PagerTbl) as totalRows
                    FROM PagerTbl
                    WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
                              AND @PageNum * @PageSize)
                    or @PageSize = -1)
                    ORDER BY RowNum";
            DataAccessLayer.DescriptionValidationLog dal = new NoProblem.Core.DataAccessLayer.DescriptionValidationLog(XrmDataContext);
            var reader = dal.ExecuteReader(query, parameters);
            DataModel.Reports.DescriptionValidationBasicReportResponse response = new NoProblem.Core.DataModel.Reports.DescriptionValidationBasicReportResponse();
            foreach (var row in reader)
            {
                DataModel.Reports.DescriptionValidationReportRow data = new NoProblem.Core.DataModel.Reports.DescriptionValidationReportRow();
                data.CreatedOn = FixDateToLocal((DateTime)row["createdon"]);
                data.Description = Convert.ToString(row["new_description"]);
                data.Heading = Convert.ToString(row["new_heading"]);
                data.Keyword = Convert.ToString(row["new_keyword"]);
                data.Origin = Convert.ToString(row["new_originidname"]);
                data.Phone = Convert.ToString(row["new_phone"]);
                data.QualityCheckName = Convert.ToString(row["new_qualitycheckname"]);
                data.Region = Convert.ToString(row["new_region"]);
                data.Session = Convert.ToString(row["new_session"]);
                data.Url = Convert.ToString(row["new_url"]);
                data.Flavor = Convert.ToString(row["new_flavor"]);
                data.ZipCodeCheckName = Convert.ToString(row["new_zipcodequalitycheckname"]);
                data.Event = Convert.ToString(row["new_event"]);
                data.Step = Convert.ToString(row["new_step"]);
                data.Control = Convert.ToString(row["new_control"]);
                response.Rows.Add(data);
                if (response.TotalRows == 0)
                {
                    response.TotalRows = unchecked((int)((long)row["totalRows"]));
                }
            }
            response.CurrentPage = pageNumber;
            response.TotalPages = (int)Math.Ceiling(((double)response.TotalRows) / ((double)pageSize));
            return response;
        }

        public DataModel.Reports.DescriptionValidationGroupBySessionResponse CreateGroupBySession(
            DateTime fromDate,
            DateTime toDate,
            DataModel.enumQualityCheckName? qualityCheckName,
            int pageSize,
            int pageNumber,
            bool? passed,
            string flavor,
            string zipCodeCheckName,
            string eventt,
            string step,
            string control)
        {
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@PageNum", pageNumber));
            parameters.Add(new KeyValuePair<string, object>("@PageSize", pageSize));
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate));
            string query =
                @"WITH PagerTbl AS
                ( 
                    select ROW_NUMBER() OVER(order by count(*) desc) AS RowNum,                    
                    new_session, count(*) as count,
(select top 1 new_heading from new_descriptionvalidationlog with(nolock)
where deletionstatecode = 0 and new_session = dvl.new_session
order by createdon desc
) as heading
,
(select top 1 new_description from new_descriptionvalidationlog with(nolock)
where deletionstatecode = 0 and new_session = dvl.new_session
order by createdon desc
) as lastDescription
,
(select  
cast(incidentid as nvarchar(36)) + ', ' + ticketnumber + '; ' as 'data()'  
from incident inc with(nolock)
	where inc.deletionstatecode = 0 and inc.new_sessionid = dvl.new_session and inc.statuscode != 200014
for xml path('')
) as cases
                    from
                    new_descriptionvalidationlog dvl with(nolock)
                    where deletionstatecode = 0 and new_session is not null 
                    and createdon between @from and @to ";
            if (qualityCheckName.HasValue)
            {
                query += " and new_qualitycheckname = @qualityCheckName ";
                parameters.Add(new KeyValuePair<string, object>("@qualityCheckName", qualityCheckName.Value.ToString()));
            }
            if (!string.IsNullOrEmpty(flavor))
            {
                query += @" and new_flavor = @flavor ";
                parameters.Add(new KeyValuePair<string, object>("@flavor", flavor));
            }
            if (!string.IsNullOrEmpty(zipCodeCheckName))
            {
                query += @" and new_zipcodequalitycheckname = @zipCodeCheckName ";
                parameters.Add(new KeyValuePair<string, object>("@zipCodeCheckName", zipCodeCheckName));
            }
            if (!string.IsNullOrEmpty(eventt))
            {
                query += @" and new_event = @event ";
                parameters.Add(new KeyValuePair<string, object>("@event", eventt));
            }
            if (passed.HasValue)
            {
                query +=
                        @" and (select count(*)                         
                        from incident inc with(nolock)
	                    where inc.deletionstatecode = 0 and inc.new_sessionid = new_session and inc.statuscode != 200014) ";
                if (passed.Value)
                {
                    query += " > 0 ";
                }
                else
                {
                    query += " = 0 ";
                }
            }
            if (!string.IsNullOrEmpty(step))
            {
                query += @" and new_step = @step ";
                parameters.Add(new KeyValuePair<string, object>("@step", step));
            }
            if (!string.IsNullOrEmpty(control))
            {
                query += @" and new_control = @control ";
                parameters.Add(new KeyValuePair<string, object>("@control", control));
            }
            query += @" group by new_session
                )
                    SELECT * , (select max(RowNum) from PagerTbl) as totalRows
                    FROM PagerTbl
                    WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
                              AND @PageNum * @PageSize)
                    or @PageSize = -1)
                    ORDER BY RowNum";
            DataAccessLayer.DescriptionValidationLog dal = new NoProblem.Core.DataAccessLayer.DescriptionValidationLog(XrmDataContext);
            var reader = dal.ExecuteReader(query, parameters);
            DataModel.Reports.DescriptionValidationGroupBySessionResponse response = new NoProblem.Core.DataModel.Reports.DescriptionValidationGroupBySessionResponse();
            foreach (var row in reader)
            {
                DataModel.Reports.DescriptionValidationGroupBySessionRow data = new NoProblem.Core.DataModel.Reports.DescriptionValidationGroupBySessionRow();
                data.Count = (int)row["count"];
                data.Session = (string)row["new_session"];
                data.Heading = row["heading"] != DBNull.Value ? (string)row["heading"] : string.Empty;
                data.LastDescription = row["lastDescription"] != DBNull.Value ? (string)row["lastDescription"] : string.Empty;
                if (row["cases"] != DBNull.Value)
                {
                    data.Cases = new List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>();
                    string casesStr = (string)row["cases"];
                    string[] pairSplit = casesStr.Split(';');
                    foreach (var item in pairSplit)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            string[] arr = item.Split(',');
                            if (arr.Length > 1 && !string.IsNullOrEmpty(arr[0]) && !string.IsNullOrEmpty(arr[1]))
                            {
                                var pair = new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair();
                                pair.Id = new Guid(arr[0]);
                                pair.Name = arr[1];
                                data.Cases.Add(pair);
                            }
                        }
                    }
                }
                response.Rows.Add(data);
                if (response.TotalRows == 0)
                {
                    response.TotalRows = unchecked((int)((long)row["totalRows"]));
                }
            }
            response.CurrentPage = pageNumber;
            response.TotalPages = (int)Math.Ceiling(((double)response.TotalRows) / ((double)pageSize));
            return response;
        }

        public List<DataModel.Reports.DescriptionValidationReportRow> SessionDrillDown(string session)
        {
            string query =
                @"select
                new_description, new_keyword, new_phone, 
                new_heading, new_qualitycheckname, new_region, 
                new_session, new_url, new_originidname, createdon,
                new_flavor, new_zipcodequalitycheckname, new_event, new_step, new_control
                from
                new_descriptionvalidationlog with(nolock)
                where deletionstatecode = 0
                and new_session = @session
                order by createdon";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@session", session));
            List<DataModel.Reports.DescriptionValidationReportRow> dataList = new List<NoProblem.Core.DataModel.Reports.DescriptionValidationReportRow>();
            DataAccessLayer.DescriptionValidationLog dal = new NoProblem.Core.DataAccessLayer.DescriptionValidationLog(XrmDataContext);
            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                DataModel.Reports.DescriptionValidationReportRow data = new NoProblem.Core.DataModel.Reports.DescriptionValidationReportRow();
                data.CreatedOn = FixDateToLocal((DateTime)row["createdon"]);
                data.Description = Convert.ToString(row["new_description"]);
                data.Heading = Convert.ToString(row["new_heading"]);
                data.Keyword = Convert.ToString(row["new_keyword"]);
                data.Origin = Convert.ToString(row["new_originidname"]);
                data.Phone = Convert.ToString(row["new_phone"]);
                data.QualityCheckName = Convert.ToString(row["new_qualitycheckname"]);
                data.Region = Convert.ToString(row["new_region"]);
                data.Session = Convert.ToString(row["new_session"]);
                data.Url = Convert.ToString(row["new_url"]);
                data.Flavor = Convert.ToString(row["new_flavor"]);
                data.ZipCodeCheckName = Convert.ToString(row["new_zipcodequalitycheckname"]);
                data.Event = Convert.ToString(row["new_event"]);
                data.Step = Convert.ToString(row["new_step"]);
                data.Control = Convert.ToString(row["new_control"]);
                dataList.Add(data);
            }
            return dataList;
        }
    }
}
