﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Reports;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class AarRequestsReportsCreator : XrmUserBase
    {
        internal AarRequestsReportsCreator(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {

        }

        internal List<AarRequestsReportRow> Create(DateTime fromDate, DateTime toDate, Guid headingId)
        {
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            List<AarRequestsReportRow> dataList = new List<AarRequestsReportRow>();
            if (headingId != Guid.Empty)
            {
                AarRequestsReportRow row = dal.GetAarRequestsReport(fromDate, toDate, headingId);
                row.HeadingId = headingId;
                dataList.Add(row);
            }
            else
            {
                List<GuidStringPair> headingIdsList = dal.FindHeadingsOfAarCases(fromDate, toDate);
                foreach (var item in headingIdsList)
                {
                    AarRequestsReportRow row = dal.GetAarRequestsReport(fromDate, toDate, item.Id);
                    row.HeadingName = item.Name;
                    row.HeadingId = item.Id;
                    dataList.Add(row);
                }
            }
            return dataList;
        }

        internal List<AarRequestReportDrillDownRow> CreateDrillDown(DateTime fromDate, DateTime toDate, Guid headingId)
        {
            if (headingId == Guid.Empty)
            {
                throw new ArgumentException("Argument cannot be empty", "HeadingId");
            }
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            var retVal = dal.GetAarRequestReportDrillDown(fromDate, toDate, headingId);
            return retVal;
        }
    }
}
