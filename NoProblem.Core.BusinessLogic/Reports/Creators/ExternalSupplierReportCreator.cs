﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class ExternalSupplierReportCreator : XrmUserBase
    {
        public ExternalSupplierReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {

        }

        private const string query =
            @"WITH PagerTbl AS
                (                
                SELECT ROW_NUMBER() OVER(ORDER BY accountnumber) AS RowNum,
            accountid, name, accountnumber, new_cashbalance, sum(isnull(New_total, 0)) total, sum(isnull(new_credit, 0)) credit, sum(isnull(new_extrabonus, 0)) extraBonus
            from
            Account ac
            join New_supplierpricing sp on sp.new_accountid = ac.AccountId
            where
            New_IsExternalSupplier = 1
            and ac.DeletionStateCode = 0
            and sp.DeletionStateCode = 0
            and sp.new_voucherid is not null
            group by accountid, name, accountnumber, new_cashbalance
                )
                    SELECT * , (select max(RowNum) from PagerTbl) as totalRows
                      FROM PagerTbl
                     WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
                                      AND @PageNum * @PageSize)
                            or @PageSize = -1)
                     ORDER BY RowNum";

        public DataModel.Reports.ExternalSupplierReportResponse Create(int pageSize, int pageNumber)
        {
            DataModel.Reports.ExternalSupplierReportResponse response = new NoProblem.Core.DataModel.Reports.ExternalSupplierReportResponse();
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@PageSize", pageSize));
            parameters.Add(new KeyValuePair<string, object>("@PageNum", pageNumber));
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                DataModel.Reports.ExternalSupplierReportRow data = new NoProblem.Core.DataModel.Reports.ExternalSupplierReportRow();
                data.Balance = row["new_cashbalance"] != DBNull.Value ? (decimal)row["new_cashbalance"] : 0;
                data.Name = row["name"] != DBNull.Value ? (string)row["name"] : string.Empty;
                data.Number = row["accountnumber"] != DBNull.Value ? (string)row["accountnumber"] : string.Empty;
                data.SupplierId = (Guid)row["accountid"];
                data.VoucherDepositSum = (int)row["total"];
                data.VoucherBonusDepositSum = (int)row["credit"];
                data.VoucherExtraBonusDepositSum = (int)row["extraBonus"];
                response.Rows.Add(data);
                if (response.TotalRows == 0)
                {
                    response.TotalRows = unchecked((int)((long)row["totalRows"]));
                }
            }
            response.CurrentPage = pageNumber;
            response.TotalPages = (int)Math.Ceiling(((double)response.TotalRows) / ((double)pageSize));
            return response;
        }
    }
}
