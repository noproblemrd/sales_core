﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class AutoRerouteReportCreator : XrmUserBase
    {
        public AutoRerouteReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public AutoRerouteReportResponse Create(DateTime fromDate, DateTime toDate)
        {
            long fromBinary = fromDate.ToBinary();
            long toBinary = toDate.ToBinary();
            string key = fromBinary.ToString() + ";" + toBinary.ToString();
            return DataAccessLayer.CacheManager.Instance.Get<AutoRerouteReportResponse>("AutoRerouteReportCreator", key, CreateMethod, 30);
        }

        private AutoRerouteReportResponse CreateMethod(string key)
        {
            string[] split = key.Split(';');
            long fromLong = long.Parse(split[0]);
            long toLong = long.Parse(split[1]);
            DateTime fromDate = DateTime.FromBinary(fromLong);
            DateTime toDate = DateTime.FromBinary(toLong);
            AutoRerouteReportResponse response = new AutoRerouteReportResponse();
            DataAccessLayer.VnRequest dal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
            DataAccessLayer.VnRequest.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_vnrequest>.SqlParametersList();
            parameters.Add("@from", fromDate.Date);
            parameters.Add("@to", toDate.Date);
            object scalar = dal.ExecuteScalar(querySuppliersCount, parameters);
            int numberOfSuppliers = 0;
            if (scalar != null && scalar != DBNull.Value)
            {
                numberOfSuppliers = (int)scalar;
            }
            response.NumberOfSuppliersInThisService = numberOfSuppliers;
            FixDatesToUtcFullDays(ref fromDate, ref toDate);//fix dates so they can be used with utf datetime columns
            parameters.Clear();
            parameters.Add("@from", fromDate);
            parameters.Add("@to", toDate);
            var callsReader = dal.ExecuteReader(queryCalls, parameters);
            foreach (var item in callsReader)
            {
                bool isSuccessfullCall = (bool)item["New_Successfulcall"];
                int count = (int)item["cnt"];
                if (isSuccessfullCall)
                    response.NumberOfReroutedCallsThatWereAnswered = count;
                else
                    response.NumberOfReroutedCallsThatWereNotAnswered = count;
            }
            object scalarReroutes = dal.ExecuteScalar(queryReroutes, parameters);
            int numberOfReroutes = 0;
            if (scalarReroutes != null && scalarReroutes != DBNull.Value)
            {
                numberOfReroutes = (int)scalarReroutes;
            }
            response.NumberOfReroutes = numberOfReroutes;
            response.PercentageOfAnsweredCallsFromReroutedCalls = GetPercentageOfAnsweredCallsFromReroutedCalls(response.NumberOfReroutedCallsThatWereAnswered, response.NumberOfReroutedCallsThatWereNotAnswered);
            return response;
        }

        private double GetPercentageOfAnsweredCallsFromReroutedCalls(int answeredCalls, int notAnsweredCalls)
        {
            double percentage = 0;
            int totalCalls = answeredCalls + notAnsweredCalls;
            if (totalCalls != 0)
            {
                percentage = (double)answeredCalls / (double)totalCalls * 100;
            }
            return percentage;
        }

        private const string querySuppliersCount = 
            @"select MAX(cnt) from tbl_autoreroute_advertisers 
             where date between @from and @to";

        private const string queryCalls =
            @"select COUNT(*) cnt, New_Successfulcall
            from 
            new_vnrequest
            where
            New_Usedrerouting = 1
            and CreatedOn between @from and @to
            group by New_Successfulcall";

        public const string queryReroutes =
            @"select COUNT(*) cnt
            from
            New_vnreroute
            where CreatedOn between @from and @to";
    }
}
