﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class BalanceReportCreator : XrmUserBase
    {
        #region Ctor
        internal BalanceReportCreator(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        internal DML.Reports.BalanceReportResponse Create(Guid supplierId, DateTime from, DateTime to)
        {
            DML.Reports.BalanceReportResponse response = new NoProblem.Core.DataModel.Reports.BalanceReportResponse();
            DataAccessLayer.BalanceRow rowDal = new NoProblem.Core.DataAccessLayer.BalanceRow(XrmDataContext);
            FixDatesToUtcFullDays(ref from, ref to);
            IEnumerable<DML.Reports.BalanceRowData> rows = rowDal.GetBalanceReport(supplierId, from, to);
            foreach (var item in rows)
            {
                item.CreatedOn = FixDateToLocal(item.CreatedOn);
            }
            var orderedRows = from item in rows
                       orderby item.CreatedOn descending
                       select
                       item;
            response.BalanceRows = orderedRows.ToList();
            return response;
        }
    }
}
