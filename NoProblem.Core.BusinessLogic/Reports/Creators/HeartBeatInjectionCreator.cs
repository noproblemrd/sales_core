﻿using NoProblem.Core.DataModel.Reports.Request;
using NoProblem.Core.DataModel.Reports.Response;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Reports.Creators
{

    delegate HeartBeatInjectionResponseCollection AsyncInjectionReport(HeartBeatInjectionRequest request);
    delegate Dictionary<DateTime, Dictionary<Guid, int>> AsyncDAUs(HeartBeatInjectionRequest request);

    public class HeartBeatInjectionCreator : XrmUserBase
    {
        #region Ctor

        public HeartBeatInjectionCreator(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        #endregion
        const string QueryDAUs = @"
DECLARE @Distribution TABLE
(
	ID uniqueidentifier NOT NULL
)

INSERT INTO @Distribution(ID)
SELECT New_originId
FROM dbo.New_originExtensionBase
WHERE New_IsDistribution = 1
  
  SELECT SUM(Visitor) DAUs,  DATEADD(day, DATEDIFF(day, 0, Date),0) date
  FROM DynamicNoProblem.dbo.Exposure ex
  WHERE Date >= @from AND Date < @to
	AND OriginId in(SELECT ID FROM @Distribution)
    {WHERE}
 GROUP BY dateadd(day, datediff(day, 0, Date),0)
 ORDER BY dateadd(day, datediff(day, 0, Date),0)";

        const string QueryDAUsInjections = @"
DECLARE @Distribution TABLE
(
	ID uniqueidentifier NOT NULL
)

INSERT INTO @Distribution(ID)
SELECT New_originId
FROM dbo.New_originExtensionBase
WHERE New_IsDistribution = 1
  
  SELECT SUM(ex.Visitor) DAUs,  DATEADD(day, DATEDIFF(day, 0, ex.Date),0) date, di.InjectionId
  FROM DynamicNoProblem.dbo.Exposure ex
	inner join dbo.DausInjection di on di.DausId = ex.Id
  WHERE ex.Date >= @from AND ex.Date < @to
	AND ex.OriginId in(SELECT ID FROM @Distribution)
        {WHERE}
	AND di.InjectionId in (
		SELECT Value FROM dbo.fn_ParseDelimitedGuid(@injections, ';'))
 GROUP BY dateadd(day, datediff(day, 0, ex.Date),0), di.InjectionId
 ORDER BY dateadd(day, datediff(day, 0, ex.Date),0), di.InjectionId
";
        const string QueryHeartBeat = @"
SELECT 	SUM(Hits) Hits, Date
  FROM [dbo].[HeartBeatInjection]
  WHERE Date >= @from and Date < @to
        {WHERE}
  GROUP BY Date
  ORDER BY Date";
        const string QueryLogicHeartBeat = @"
SELECT 	SUM(Hits) Hits, Date
  FROM [dbo].[HeartBeatLogic]
  WHERE Date >= @from and Date < @to
        {WHERE}
  GROUP BY Date
  ORDER BY Date";
        const string QueryHeartBeatInjections = @"
SELECT 	SUM(Hits) Hits, Date, InjectionId
  FROM [dbo].[HeartBeatInjection]
  WHERE Date >= @from and Date < @to
	and InjectionId in (
		SELECT Value FROM dbo.fn_ParseDelimitedGuid(@injections, ';'))
    {WHERE}
  GROUP BY Date, InjectionId
  ORDER BY Date, InjectionId";
        public HeartBeatInjectionResponseCollection HeartBeatInjectionReport(HeartBeatInjectionRequest request)
        {
            switch(request.HeartBeatChartType)
            {
                case(eHeartBeatChartType.Injection):
                    return InjectionReport(request);
                case(eHeartBeatChartType.Hits_KDAUs):
                    return Hits_UsersReport(request);
                case (eHeartBeatChartType.Logic):
                    return LogicReport(request);                    
                    
            }
            return new HeartBeatInjectionResponseCollection();
        }
        private HeartBeatInjectionResponseCollection InjectionReport(HeartBeatInjectionRequest request)
        {
            HeartBeatInjectionResponseCollection list = new HeartBeatInjectionResponseCollection();
            StringBuilder sb;// = new StringBuilder(QueryHeartBeat);
            StringBuilder sbWhere = new StringBuilder();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    bool WithInjection = false;
                    cmd.Connection = conn;
                    if (request.InjectionId == null || request.InjectionId.Length == 0)
                    {
                        sb = new StringBuilder(QueryHeartBeat);
                //        sbWhere.Append(" AND InjectionId = @InjectionId ");
                //        cmd.Parameters.AddWithValue("@InjectionId", request.InjectionId);
                    }
                    else
                    {
                        sb = new StringBuilder(QueryHeartBeatInjections);
                        cmd.Parameters.AddWithValue("@injections", request.GetInjectionsIds());
                        WithInjection = true;
                    }
                    if(request.CampaignId != Guid.Empty)
                    {
                        sbWhere.Append(" AND OriginId = @OriginId ");
                        cmd.Parameters.AddWithValue("@OriginId", request.CampaignId);
                    }
                    cmd.Parameters.AddWithValue("@from", request.From);
                    cmd.Parameters.AddWithValue("@to", request.To);
                    sb.Replace("{WHERE}", sbWhere.ToString());
                    cmd.CommandText = sb.ToString();
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        HeartBeatInjectionResponse row = new HeartBeatInjectionResponse();
                        row.Hits = (int)reader["Hits"];
                        row.date = (DateTime)reader["Date"];
                        if (WithInjection)
                            row.InjectionId = (Guid)reader["InjectionId"];
                        list.Add(row);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        private HeartBeatInjectionResponseCollection Hits_UsersReport(HeartBeatInjectionRequest request) 
        {
            AsyncInjectionReport air = new AsyncInjectionReport(InjectionReport);
            AsyncDAUs ad = new AsyncDAUs(DAUsReport);
            IAsyncResult result_injection = air.BeginInvoke(request, null, null);
            IAsyncResult result_DAUS = ad.BeginInvoke(request, null, null);
            HeartBeatInjectionResponseCollection list_injection = air.EndInvoke(result_injection);
            list_injection.DicDAUs = ad.EndInvoke(result_DAUS);
            list_injection.SetHitsDaus();
            return list_injection;
        }
        private HeartBeatInjectionResponseCollection LogicReport(HeartBeatInjectionRequest request) 
        {
            HeartBeatInjectionResponseCollection list = new HeartBeatInjectionResponseCollection();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    
                    StringBuilder sb = new StringBuilder(QueryLogicHeartBeat);
                    StringBuilder sbWhere = new StringBuilder();
                        //        sbWhere.Append(" AND InjectionId = @InjectionId ");
                        //        cmd.Parameters.AddWithValue("@InjectionId", request.InjectionId);
                    
                    if (request.CampaignId != Guid.Empty)
                    {
                        sbWhere.Append(" AND OriginId = @OriginId ");
                        cmd.Parameters.AddWithValue("@OriginId", request.CampaignId);
                    }
                    cmd.Parameters.AddWithValue("@from", request.From);
                    cmd.Parameters.AddWithValue("@to", request.To);
                    sb.Replace("{WHERE}", sbWhere.ToString());
                    cmd.CommandText = sb.ToString();
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        HeartBeatInjectionResponse row = new HeartBeatInjectionResponse();
                        row.Hits = (int)reader["Hits"];
                        row.date = (DateTime)reader["Date"];                        
                        list.Add(row);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        private Dictionary<DateTime, Dictionary<Guid, int>> DAUsReport(HeartBeatInjectionRequest request)
        {
            Dictionary<DateTime, Dictionary<Guid, int>> dic = new Dictionary<DateTime, Dictionary<Guid, int>>();
            StringBuilder sb;// = new StringBuilder(QueryHeartBeat);
            StringBuilder sbWhere = new StringBuilder();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    bool WithInjection = false;
                    cmd.Connection = conn;
                    if (request.InjectionId == null || request.InjectionId.Length == 0)
                    {
                        sb = new StringBuilder(QueryDAUs);
                        //        sbWhere.Append(" AND InjectionId = @InjectionId ");
                        //        cmd.Parameters.AddWithValue("@InjectionId", request.InjectionId);
                    }
                    else
                    {
                        
                        sb = new StringBuilder(QueryDAUsInjections);
                        cmd.Parameters.AddWithValue("@injections", request.GetInjectionsIds());
                        WithInjection = true;
                         
                    }
                    if (request.CampaignId != Guid.Empty)
                    {
                        sbWhere.Append(" AND ex.OriginId = @OriginId ");
                        cmd.Parameters.AddWithValue("@OriginId", request.CampaignId);
                    }
                    cmd.Parameters.AddWithValue("@from", request.From);
                    cmd.Parameters.AddWithValue("@to", request.To);
                    sb.Replace("{WHERE}", sbWhere.ToString());
                    cmd.CommandText = sb.ToString();
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DateTime dt = (DateTime)reader["date"];
                        if (!dic.ContainsKey(dt))
                            dic.Add(dt, new Dictionary<Guid, int>());
                        if (WithInjection)
                        {
                            dic[dt].Add((Guid)reader["InjectionId"], (int)reader["DAUs"]);
                        }
                        else
                            dic[dt].Add(Guid.Empty, (int)reader["DAUs"]);
                    }
                    conn.Close();
                }
            }
            return dic;
        }
    }
}
