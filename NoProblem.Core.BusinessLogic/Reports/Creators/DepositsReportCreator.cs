﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class DepositsReportCreator : XrmUserBase
    {
        #region Ctor
        internal DepositsReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        internal DataModel.Reports.DepositReportResponse Create(DateTime fromDate, DateTime toDate, Guid expertiseId, DataModel.Reports.DepositReportRequest.eDepositType type, DataModel.Reports.DepositReportRequest.eDepositCreatedBy createdBy, int pageSize, int pageNumber)
        {
            NoProblem.Core.DataModel.Reports.DepositReportResponse response = new NoProblem.Core.DataModel.Reports.DepositReportResponse();
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            DataAccessLayer.SupplierPricing dal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            List<DataModel.Reports.DepositData> deposits = dal.DepositsReportQuery(fromDate, toDate, expertiseId, type, createdBy);
            List<DataModel.Reports.DepositData> retVal = new List<NoProblem.Core.DataModel.Reports.DepositData>();

            foreach (var d in deposits)
            {
                DateTime dateFixed = FixDateToLocal(d.CreatedOn);
                var data = retVal.FirstOrDefault(x => x.CreatedOn.Date == dateFixed.Date);
                if (data == null)
                {
                    data = new NoProblem.Core.DataModel.Reports.DepositData();
                    data.CreatedOn = dateFixed.Date;
                    data.Amount = d.Amount;
                    data.VirtualMoney = d.VirtualMoney;
                    data.Credit = d.Credit;
                    retVal.Add(data);
                }
                else
                {
                    data.Amount += d.Amount;
                    data.Credit += d.Credit;
                    data.VirtualMoney += d.VirtualMoney;
                }
            }
            DataModel.Reports.DepositData totalRow = new NoProblem.Core.DataModel.Reports.DepositData();
            foreach (var item in retVal)
            {
                item.Total = item.Amount + item.Credit + item.VirtualMoney;
                totalRow.Amount += item.Amount;
                totalRow.Credit += item.Credit;
                totalRow.VirtualMoney += item.VirtualMoney;
                totalRow.Total += item.Total;
            }
            IEnumerable<DataModel.Reports.DepositData> orderedRetVal =
                (from item in retVal
                 orderby item.CreatedOn descending
                 select item).ToList();
            response.CurrentPage = 1;
            response.TotalPages = 1;
            response.TotalRows = orderedRetVal.Count();
            if (pageSize != -1)
            {
                orderedRetVal = orderedRetVal.Skip((pageNumber - 1) * pageSize).Take(pageSize);
                response.CurrentPage = pageNumber;
                response.TotalPages = (int)Math.Ceiling(((double)response.TotalRows) / ((double)pageSize));
            }
            response.DataList = orderedRetVal.ToList();
            response.TotalRow = totalRow;
            return response;
        }
    }
}
