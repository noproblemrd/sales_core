﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class AdvertisersReportCreator : XrmUserBase
    {
        #region Ctor
        internal AdvertisersReportCreator(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal DML.Reports.AdvertisersReportResponse Create(AdvertisersReportRequest _request)
        {
            DML.Reports.AdvertisersReportResponse response = new NoProblem.Core.DataModel.Reports.AdvertisersReportResponse();
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DateTime fromDate = _request.FromDate;
            DateTime toDate = _request.ToDate;
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            var rows = dal.GetAdvertisersReport(fromDate, toDate, _request.MaxBalance, _request.SupplierStatus, 
                _request.IsFromAAR, _request.IsWebRegistrant, _request.RegistrationStep);
            List<DML.Reports.SupplierData> dataList = new List<NoProblem.Core.DataModel.Reports.SupplierData>(rows.Count);
        //    int totalRows = 0;
            foreach (var row in rows)
            {
                DML.Reports.SupplierData data = new NoProblem.Core.DataModel.Reports.SupplierData();
                data.SupplierId = (Guid)row["accountid"];
                data.Balance = row["new_cashbalance"] != DBNull.Value ? (decimal)row["new_cashbalance"] : 0;
                data.ContactName = row["contactName"] != DBNull.Value ? (string)row["contactName"] : string.Empty;
                data.Email = row["emailaddress1"] != DBNull.Value ? (string)row["emailaddress1"] : string.Empty;
                data.Name = row["name"] != DBNull.Value ? (string)row["name"] : string.Empty; ;
                data.Number = row["accountnumber"] != DBNull.Value ? (string)row["accountnumber"] : string.Empty;
                data.Phone = row["telephone1"] != DBNull.Value ? (string)row["telephone1"] : string.Empty;                
                data.Status = GetSupplierStatus(row);
                data.CreatonOn = FixDateToLocal((DateTime)row["createdon"]);
                data.IsFromAar = row["new_isfromaar"] != DBNull.Value ? (bool)row["new_isfromaar"] : false;
                data.StageInTrialRegistration = row["new_stageintrialregistration"] != DBNull.Value ? (int)row["new_stageintrialregistration"] : 0;
                data.TimeZone = row["UserInterfaceName"] != DBNull.Value ? (string)row["UserInterfaceName"] : string.Empty;
                int _step = row["new_stepinregistration2014"] != DBNull.Value ? (int)row["new_stepinregistration2014"] : -1;
                bool checkoutbuttonclicked = (row["new_checkoutbuttonclicked"] == DBNull.Value ? false : (bool)row["new_checkoutbuttonclicked"]);
                if (_step == 6)
                    _step = checkoutbuttonclicked ? (_step + 1) : _step;
                else if(_step > 6)
                    _step++;
                if (_step == -1)
                    data.StageInRegistration = null;
                else
                    data.StageInRegistration = (eCompletedRegistrationStep)_step;
                dataList.Add(data);
                /*
                if (totalRows == 0)
                {
                    totalRows = unchecked((int)((long)row["totalRows"]));
                }
                */
            }
            response.Suppliers = dataList;
         //   response.CurrentPage = pageNumber;
        //    response.TotalRows = totalRows;
       //     response.TotalPages = (int)Math.Ceiling(((double)totalRows) / ((double)pageSize));
            return response;
        }

        #endregion

        #region Private Methods

        private NoProblem.Core.DataModel.Reports.SupplierStatus GetSupplierStatus(Dictionary<string, object> row)
        {
            DML.Xrm.account.SupplierStatus status = (DML.Xrm.account.SupplierStatus)((int)row["new_status"]);
            switch (status)
            {
                case NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Approved:
                    int isAvailable = (int)row["Available"];
                    if (isAvailable == 1)
                    {
                        return NoProblem.Core.DataModel.Reports.SupplierStatus.Available;
                    }
                    else
                    {
                        return NoProblem.Core.DataModel.Reports.SupplierStatus.Unavailable;
                    }
                case NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Candidate:
                    return NoProblem.Core.DataModel.Reports.SupplierStatus.Candidate;
                case NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Inactive:
                    return NoProblem.Core.DataModel.Reports.SupplierStatus.Inactive;
                case NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Trial:
                    if (row["new_trialstatusreason"] != DBNull.Value)
                    {
                        DML.Xrm.account.TrialStatusReasonCode trialStatus = (DML.Xrm.account.TrialStatusReasonCode)row["new_trialstatusreason"];
                        switch (trialStatus)
                        {
                            case NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.Accessible:
                                return NoProblem.Core.DataModel.Reports.SupplierStatus.AC;
                            case NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.DoNotCallList:
                                return NoProblem.Core.DataModel.Reports.SupplierStatus.DoNotCallList;
                            case NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.Expired:
                                return NoProblem.Core.DataModel.Reports.SupplierStatus.Expired;
                            case NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.FreshlyImported:
                                return NoProblem.Core.DataModel.Reports.SupplierStatus.FreshlyImported;
                            case NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.InRegistration:
                                return NoProblem.Core.DataModel.Reports.SupplierStatus.InRegistration;
                            case NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.InTrial:
                                return NoProblem.Core.DataModel.Reports.SupplierStatus.ITC;
                            case NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.Machine:
                                return NoProblem.Core.DataModel.Reports.SupplierStatus.Machine;
                            case NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.RemoveMe:
                                return NoProblem.Core.DataModel.Reports.SupplierStatus.RemoveMe;
                        }
                    }
                    return NoProblem.Core.DataModel.Reports.SupplierStatus.Inactive;
            }
            return NoProblem.Core.DataModel.Reports.SupplierStatus.Inactive;
        }

        #endregion
    }
}
