﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.Reports.Creators
{
    delegate List<DataModel.Reports.Response.DistributionInstallationReportRow> AsyncDistributionInstallationReport(DateTime fromDate, DateTime toDate, Guid[] origins, string Country);
    delegate List<DataModel.Reports.Response.InstallationReportInjectDetails> AsyncDistributionInstallationReport_Injections(DateTime fromDate, DateTime toDate, Guid[] origins, string Country);

    delegate List<DataModel.Reports.Response.DistributionInstallationReportRowDrillDown> AsyncDistributionInstallationReportDrillDown(DateTime fromDate, DateTime toDate, Guid[] origins, string Country);
    delegate List<DataModel.Reports.Response.InstallationReportTotalDetailsDrillDown> AsyncInstallationReportDrillDown_Total(DateTime toDate, Guid[] origins, string Country);


    internal class DistributionInstallationReportCreator : XrmUserBase
    {
        #region Ctor
        internal DistributionInstallationReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion
        #region Fields
        
        private const string queryGeneral_Origin = "EXEC [dbo].[InstallationReportGeneral_Origin6.1] @Origins, @date, @country";
        private const string queryGeneral = "EXEC [dbo].[InstallationReportGeneral6.1] @date, @country";
        /*
        private const string queryInstallationReportGeneral = @"
CREATE TABLE #temp_origins
(
	ID uniqueidentifier NOT NULL	
)

INSERT INTO #temp_origins(ID)
SELECT New_originId
FROM dbo.New_originExtensionBase
WHERE New_IsDistribution = 1
     {WHERE_ORIGIN}                                                                       

    SELECT
	COALESCE(INS.date, DAUS.Date) date
	,INS.Installations
    ,INS.UniqueInstallations
	,INS.Uninstallations
	,INS.Revenue
    ,INS.Cost
	,DAUS.Visitor
    FROM
    (
	SELECT DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0) date 
            ,SUM(Installations) Installations
            ,SUM(UniqueInstallations) UniqueInstallations
            ,SUM(Uninstallations) Uninstallations
            ,SUM(Revenue) Revenue
            ,SUM(CAST(Installations as decimal(23,10)) * CostOfInstallation) Cost
    FROM [dbo].[InstallationReportAgg5.0] WITH(NOLOCK)		
    WHERE Date >= @from AND Date < @to  
            {WHERE}          
    GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0)	
) INS
LEFT JOIN
(   
    SELECT SUM(CAST(Visitor as bigint)) Visitor, Date
    FROM [DynamicNoProblem].[dbo].[Exposure] WITH(NOLOCK)	
	WHERE Date >= @from AND Date < @to
		AND OriginId in (SELECT ID FROM #temp_origins)
        {WHERE_DAUS}
	GROUP BY Date
)DAUS on DAUS.Date = INS.date
ORDER BY date desc			

DROP TABLE #temp_origins";
        */
        private const string queryInstallationReportGeneral = @"
CREATE TABLE #temp_origins
(
	ID uniqueidentifier NOT NULL	
)

INSERT INTO #temp_origins(ID)
SELECT New_originId
FROM dbo.New_originExtensionBase
WHERE New_IsDistribution = 1
     {WHERE_ORIGIN}                                                                       

    SELECT
	COALESCE(INS.date, DAUS.Date) date
	,INS.Installations
    ,UNIQUE_INSTALL.UniqueInstallations
	,INS.Uninstallations
	,INS.Revenue
	,UNIQUE_INSTALL.CostofInstallation Cost  
	,DAUS.Visitor    
    ,UNIQUE_INSTALL.EffectiveUniqueInstall
    FROM
    (
	SELECT DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0) date 
            ,SUM(Installations) Installations            
            ,SUM(Uninstallations) Uninstallations
            ,SUM(Revenue) Revenue
            ,SUM(CAST(Installations as decimal(23,10)) * CostOfInstallation) Cost
    FROM [dbo].[InstallationReportAgg6.0] WITH(NOLOCK)		
    WHERE Date >= @from AND Date < @to  
            {WHERE}          
    GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0)	
) INS
FULL OUTER JOIN
(
	SELECT DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0) date 
		,COUNT(*) UniqueInstallations
		,CAST(SUM(Factor) as int) EffectiveUniqueInstall
		,SUM(CostofInstallation * Factor) CostofInstallation	
	FROM [dbo].[InstallationIpOrigin6.2]
	WHERE Date >= @from AND Date < @to  
        {WHERE}
	 GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0)
)UNIQUE_INSTALL
	on UNIQUE_INSTALL.date = INS.date
LEFT JOIN
(   
    SELECT SUM(CAST(Visitor as bigint)) Visitor, Date
    FROM [DynamicNoProblem].[dbo].[Exposure] WITH(NOLOCK)	
	WHERE Date >= @from AND Date < @to
		AND OriginId in (SELECT ID FROM #temp_origins)
        {WHERE_DAUS}
	GROUP BY Date
)DAUS on DAUS.Date = COALESCE(INS.date, UNIQUE_INSTALL.date)
ORDER BY date desc			

DROP TABLE #temp_origins";
        /*
        private const string queryInstallationReportGeneralDrillDown = @"
CREATE TABLE #temp_origins
(
	ID uniqueidentifier NOT NULL	
)
IF(@origins IS NULL OR LEN(@origins) = 0)
BEGIN
	INSERT INTO #temp_origins(ID)
	SELECT New_originId
	FROM dbo.New_originExtensionBase
	WHERE New_IsDistribution = 1
END
ELSE
BEGIN
	INSERT INTO #temp_origins(ID)
	SELECT Value
	FROM dbo.fn_ParseDelimitedGuid(@origins, ';')
END
                                                                            

    SELECT
    ORIGIN.New_originId OriginId	
    ,ORIGIN.New_name
	,ISNULL(INS.Installations, 0) Installations
    ,ISNULL(INS.UniqueInstallations, 0) UniqueInstallations
	,ISNULL(INS.Uninstallations, 0) Uninstallations
	,ISNULL(INS.Revenue, 0) Revenue
    ,ISNULL(INS.Cost, 0) Cost
	,ISNULL(DAUS.Visitor, 0) Visitor
    FROM
    (
	SELECT 
            SUM(Installations) Installations
            ,SUM(UniqueInstallations) UniqueInstallations
            ,SUM(Uninstallations) Uninstallations
            ,SUM(Revenue) Revenue
            ,SUM(CAST(Installations as decimal(23,10)) * CostOfInstallation) Cost
            ,OriginId
    FROM [dbo].[InstallationReportAgg5.0] WITH(NOLOCK)		
    WHERE Date >= @from AND Date < @to                
        {WHERE}                  
    GROUP BY  OriginId	
) INS
FULL OUTER JOIN
(   
    SELECT SUM(CAST(Visitor as bigint)) Visitor, OriginId
    FROM [DynamicNoProblem].[dbo].[Exposure] WITH(NOLOCK)	
	WHERE Date >= @from AND Date < @to		
        AND OriginId in (SELECT ID FROM #temp_origins) 
        {WHERE_DAUS}
	GROUP BY OriginId
)DAUS on DAUS.OriginId = INS.OriginId
INNER JOIN
(
	SELECT New_originId, New_name
	FROM dbo.New_originExtensionBase	
)ORIGIN on ORIGIN.New_originId = COALESCE(INS.OriginId, DAUS.OriginId)

ORDER BY New_name		

DROP TABLE #temp_origins";
         * */
        private const string queryInstallationReportGeneralDrillDown = @"
DECLARE @DefaultOrigin uniqueidentifier = '00000000-0000-0000-0000-000000000001'
CREATE TABLE #temp_origins
(
	ID uniqueidentifier NULL	
)
IF(@origins IS NULL OR LEN(@origins) = 0)
BEGIN
	INSERT INTO #temp_origins(ID)
	SELECT New_originId
	FROM dbo.New_originExtensionBase
	WHERE New_IsDistribution = 1

    INSERT INTO #temp_origins(ID)
	VALUES (@DefaultOrigin)
END
ELSE
BEGIN
	INSERT INTO #temp_origins(ID)
	SELECT Value
	FROM dbo.fn_ParseDelimitedGuid(@origins, ';')
END
                                                                            

    SELECT
    ORIGIN.New_originId OriginId	
    ,ORIGIN.New_name
	,ISNULL(INS.Installations, 0) Installations  
	,ISNULL(INS.Uninstallations, 0) Uninstallations
	,ISNULL(INS.Revenue, 0) Revenue 
	,ISNULL(DAUS.Visitor, 0) Visitor
	,ISNULL(UNIQUE_INSTALL.UniqueInstallations, 0) UniqueInstallations
    ,ISNULL(UNIQUE_INSTALL.EffectiveUniqueInstalls, 0) EffectiveUniqueInstalls
	,ISNULL(UNIQUE_INSTALL.Cost, 0) Cost
    FROM
    (
	SELECT 
            SUM(Installations) Installations            
            ,SUM(Uninstallations) Uninstallations
            ,SUM(Revenue) Revenue            
            ,OriginId
    FROM [dbo].[InstallationReportAgg6.0] WITH(NOLOCK)		
    WHERE Date >= @from AND Date < @to                
        AND ISNULL(OriginId, @DefaultOrigin) in (SELECT ID FROM #temp_origins)
        {WHERE}                  
    GROUP BY  OriginId	
) INS
LEFT JOIN
(
	SELECT COUNT(*) UniqueInstallations, CAST(SUM(Factor) as int) EffectiveUniqueInstalls, SUM(CostOfInstallation * Factor) Cost, OriginId
	FROM [dbo].[InstallationIpOrigin6.2]
	WHERE Date >= @from AND Date < @to 
        AND OriginId in (SELECT ID FROM #temp_origins) 
        {WHERE}
	GROUP BY  OriginId
)UNIQUE_INSTALL
	on ISNULL(UNIQUE_INSTALL.OriginId, @DefaultOrigin) = COALESCE(INS.OriginId, @DefaultOrigin)
FULL OUTER JOIN
(   
    SELECT SUM(CAST(Visitor as bigint)) Visitor, OriginId
    FROM [DynamicNoProblem].[dbo].[Exposure] WITH(NOLOCK)	
	WHERE Date >= @from AND Date < @to		
        AND OriginId in (SELECT ID FROM #temp_origins) 
        {WHERE_DAUS}
	GROUP BY OriginId
)DAUS on DAUS.OriginId = COALESCE(INS.OriginId, UNIQUE_INSTALL.OriginId)
LEFT JOIN
(
	SELECT New_originId, New_name
	FROM dbo.New_originExtensionBase	
)ORIGIN on ORIGIN.New_originId = COALESCE(INS.OriginId, DAUS.OriginId, UNIQUE_INSTALL.OriginId)

ORDER BY New_name		

DROP TABLE #temp_origins";
         
        private const string queryInstallationReportDrillDown_Injection = @"

IF(@origins IS NULL OR LEN(@origins) = 0)
BEGIN
	SELECT i.InjectionId			
			,inj.New_name
			,SUM(i.Revenue) Revenue
			,i.OriginId
	FROM [dbo].[InstallationReportAgg6.0] i WITH(NOLOCK)
		LEFT JOIN dbo.New_injectionExtensionBase inj ON inj.New_injectionId = i.InjectionId		
	WHERE i.Date >= @from AND i.Date < @to	
		AND i.InjectionId is not null		 	 
            {WHERE}       
	GROUP BY i.InjectionId, inj.New_name, i.OriginId
END
ELSE
BEGIN
	CREATE TABLE #temp_origins
	(
		ID uniqueidentifier NOT NULL	
	)
	INSERT INTO #temp_origins(ID)
	SELECT Value
	FROM dbo.fn_ParseDelimitedGuid(@origins, ';')
	
	SELECT i.InjectionId			
			,inj.New_name
			,SUM(i.Revenue) Revenue
			,i.OriginId
	FROM [dbo].[InstallationReportAgg6.0] i WITH(NOLOCK)
		LEFT JOIN dbo.New_injectionExtensionBase inj ON inj.New_injectionId = i.InjectionId		
	WHERE i.Date >= @from AND i.Date < @to	
		AND i.InjectionId is not null		
		AND i.OriginId in (SELECT ID FROM #temp_origins) 	        
            {WHERE}
	GROUP BY i.InjectionId, inj.New_name, i.OriginId
	    
	DROP TABLE #temp_origins
END";
        #endregion

        public DataModel.Reports.Response.DistributionInstallationReportResponse Create(NoProblem.Core.DataModel.Reports.AddOnInstallationReportRequest request)
        {
            DataModel.Reports.Response.DistributionInstallationReportResponse _response = new DataModel.Reports.Response.DistributionInstallationReportResponse();
            AsyncDistributionInstallationReport caller = new AsyncDistributionInstallationReport(this.InstallationReport);
            AsyncDistributionInstallationReport_Injections caller_injection = new AsyncDistributionInstallationReport_Injections(this.InstallationReport_Injections);
            IAsyncResult result = caller.BeginInvoke(request.FromDate, request.ToDate, request.Origins, request.Country, null, null);
            IAsyncResult result_injection = caller_injection.BeginInvoke(request.FromDate, request.ToDate, request.Origins, request.Country, null, null);
        //    DataModel.Reports.Response.InstallationReportGeneralDetails _details = InstallationReport(request.FromDate);
            _response.TotalDetails = InstallationReportTotals(request.Origins, request.FromDate, request.Country);
            _response.Data = caller.EndInvoke(result);
            List<DataModel.Reports.Response.InstallationReportInjectDetails> Injections_data = caller_injection.EndInvoke(result_injection);
            _response.SetValues(Injections_data, request.FromDate, request.ToDate);
            return _response;
        }
        private List<DataModel.Reports.Response.InstallationReportInjectDetails> InstallationReport_Injections(DateTime fromDate, DateTime toDate, Guid[] origins, string Country)
        {
            /*
            string query = @"
                    SELECT DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0) date 
				                ,InjectionId			
				                ,SUM(Revenue) Revenue
		                FROM dbo.InstallationReportAgg WITH(NOLOCK)
		                        {WHERE}
		                GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, [Date]),0), InjectionId";
             * */           
            string query = @"
                        SELECT DATEADD(DAY, DATEDIFF(DAY, 0, i.[Date]),0) date 
				                ,i.InjectionId			
				                ,inj.New_name
				                ,SUM(i.Revenue) Revenue
		                FROM [dbo].[InstallationReportAgg6.0] i WITH(NOLOCK)
			                LEFT JOIN dbo.New_injectionExtensionBase inj ON inj.New_injectionId = i.InjectionId
		                WHERE i.Date >= @from AND i.Date < @to	
                            AND i.InjectionId is not null		 
                                {WHERE}
		                GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, i.[Date]),0), i.InjectionId, inj.New_name";

            string _where = string.Empty;//, _where_origin = string.Empty, _where_daus = string.Empty;
            DateTime fromDateFixed = fromDate;
            DateTime toDateFixed = toDate.AddDays(1);
          //  FixDatesToUtcFullDays(ref fromDateFixed, ref toDateFixed);
            List<DataModel.Reports.Response.InstallationReportInjectDetails> response = new List<DataModel.Reports.Response.InstallationReportInjectDetails>();
            
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    if (origins != null && origins.Length > 0)
                    {
                        for (int i = 0; i < origins.Length; i++)
                        {
                            if (i == 0)
                                _where += " AND (";
                            else
                                _where += " OR ";
                            _where += "i.OriginId = @origin" + i + " ";
                            cmd.Parameters.AddWithValue("@origin" + i, origins[i]);
                        }
                        _where += ")";
                    }
                    if (!string.IsNullOrEmpty(Country))
                    {
                        _where += " AND i.Country = @country ";
                        cmd.Parameters.AddWithValue("@country", Country);
                    }
                    
                    query = query.Replace("{WHERE}", _where);
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@from", fromDateFixed);
                    cmd.Parameters.AddWithValue("@to", toDateFixed);
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DataModel.Reports.Response.InstallationReportInjectDetails data = new DataModel.Reports.Response.InstallationReportInjectDetails();
                        data.Date = (DateTime)reader["date"];
                        if (reader["InjectionId"] == DBNull.Value)
                            data.InjectionId = null;
                        else
                            data.InjectionId = (Guid)reader["InjectionId"];
                        
                        if (reader["New_name"] == DBNull.Value)
                            data.Name = null;
                        else
                            data.Name = (string)reader["New_name"];
                         
                        if (reader["Revenue"] == DBNull.Value)
                            data.Revenue = 0m;
                        else
                            data.Revenue = (decimal)reader["Revenue"];
                        response.Add(data);
                    }
                    conn.Close();
                }
            }
            return response;
                   
        }
        private List<DataModel.Reports.Response.DistributionInstallationReportRow> InstallationReport(DateTime fromDate, DateTime toDate, Guid[] origins, string Country)
        {
            DateTime fromDateFixed = fromDate;
            DateTime toDateFixed = toDate.AddDays(1);
          //  FixDatesToUtcFullDays(ref fromDateFixed, ref toDateFixed);
            List<DataModel.Reports.Response.DistributionInstallationReportRow> response = new List<DataModel.Reports.Response.DistributionInstallationReportRow>();
            
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    string query = queryInstallationReportGeneral;
                    string _where = string.Empty, _where_origin = string.Empty, _where_daus = string.Empty;
                    if (origins != null && origins.Length > 0)
                    {
                        for (int i = 0; i < origins.Length; i++)
                        {
                            if (i == 0)
                                _where += " AND (";
                            else
                                _where += " OR ";
                            _where += "OriginId = @origin" + i + " ";
                            cmd.Parameters.AddWithValue("@origin" + i, origins[i]);
                        }
                        _where += ")";
                        _where_origin = _where.Replace("OriginId", "New_originId");
                    }
                    if (!string.IsNullOrEmpty(Country))
                    {
                        _where += " AND Country = @country ";
                        _where_daus += " AND CountryId = [DynamicNoProblem].[dbo].[GetCountryIdByIPName](@country) ";
                        cmd.Parameters.AddWithValue("@country", Country);
                    }
                    query = query.Replace("{WHERE}", _where);
                    query = query.Replace("{WHERE_ORIGIN}", _where_origin);
                    query = query.Replace("{WHERE_DAUS}", _where_daus);
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@from", fromDateFixed);
                    cmd.Parameters.AddWithValue("@to", toDateFixed);
                    cmd.CommandTimeout = 120;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DataModel.Reports.Response.DistributionInstallationReportRow row = new DataModel.Reports.Response.DistributionInstallationReportRow();
                        row.Date = (DateTime)reader["Date"];
                        row.Installations = reader["Installations"] == DBNull.Value ? 0 : (int)reader["Installations"];
                        row.UniqeInstallations = reader["UniqueInstallations"] == DBNull.Value ? 0 : (int)reader["UniqueInstallations"];
                        row.Uninstallation = reader["Uninstallations"] == DBNull.Value ? 0 : (int)reader["Uninstallations"];
                        row.EffectiveUniqueInstalls = reader["EffectiveUniqueInstall"] == DBNull.Value ? 0 : (int)reader["EffectiveUniqueInstall"];
                        row.Revenue = reader["Revenue"] == DBNull.Value ? 0m : (decimal)reader["Revenue"];
                        row.DAUS = reader["Visitor"] == DBNull.Value ? 0L : (long)reader["Visitor"];
                        row.Cost = reader["Cost"] == DBNull.Value ? 0m : (decimal)reader["Cost"];
                        response.Add(row);
                    }
                    conn.Close();
                }
            }
            return response;
        }
       
        private DataModel.Reports.Response.InstallationReportTotalDetails InstallationReportTotals(Guid[] Origins, DateTime _date, string country)
        {
            string StrOrigins = GuidsToString(Origins, ';');
            DataModel.Reports.Response.InstallationReportTotalDetails data = null;
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                string _query = string.IsNullOrEmpty(StrOrigins) ? queryGeneral : queryGeneral_Origin;
                using (SqlCommand cmd = new SqlCommand(_query, conn))
                {
                    if (!string.IsNullOrEmpty(StrOrigins))
                        cmd.Parameters.AddWithValue("@Origins", StrOrigins);
                    cmd.Parameters.AddWithValue("@date", _date);
                    cmd.Parameters.AddWithValue("@country", (country == null) ? string.Empty : country);
                    conn.Open();
                    cmd.CommandTimeout = 120;
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        data = new DataModel.Reports.Response.InstallationReportTotalDetails();
                        data.TotalCost = reader["TotalCost"] == DBNull.Value ? 0 : (decimal)reader["TotalCost"];
                        data.TotalDAUs = reader["TotalDAUS"] == DBNull.Value ? 0 : (long)reader["TotalDAUS"];
                        data.TotalRevenue = reader["TotlaRevenue"] == DBNull.Value ? 0 : (decimal)reader["TotlaRevenue"];
                        data.TotalUniqueInstalls = reader["TotalUniqueInstallation"] == DBNull.Value ? 0 : (int)reader["TotalUniqueInstallation"];
                        data.TotalEffectiveUniqueInstalls = reader["TotalEffectiveUniqueIntalls"] == DBNull.Value ? 0 : (int)reader["TotalEffectiveUniqueIntalls"];
                    }
                    
                        
                    conn.Close();
                }
            }
            return data;
            /*
            SELECT @AvgInstallCost AvgInstallCost, @DAUs DAUs, @DAUs_SUM Sum_DAUs, @installs TotalInstalls
	,(ISNULL(@installs, 0)-ISNULL(@uninstall, 0)) Installs*/
        }
        string GuidsToString(Guid[] guids, char del)
        {
            if (guids == null)
                return null;
            string result = string.Empty;
            foreach (Guid _id in guids)
                result += _id.ToString() + del;
            return result;
        }
        /***************** DRILL DOWN  *************/
        /*
        const string queryInstallationReportDrillDown_Total = @"
        SELECT 			
			INSTALL.OriginId
			,INSTALL.TotalCost
			,INSTALL.TotalUniqueInstallation
			,INSTALL.TotlaRevenue
			,ISNULL(DAUS.DAUs, 0) DAUs
		FROM
		(
			SELECT SUM(UniqueInstallations) TotalUniqueInstallation, SUM(Revenue) TotlaRevenue
				,SUM(CAST(UniqueInstallations as decimal(23,10)) * CostOfInstallation) TotalCost
				,OriginId
			FROM [dbo].[InstallationReportAgg5.0] WITH(NOLOCK)
			WHERE Date < @Date
                {WHERE_INSTALL}
			GROUP BY OriginId
		)INSTALL
		LEFT JOIN
		(
			SELECT  SUM(CAST(Visitor as bigint)) DAUs, OriginId
			FROM [DynamicNoProblem].[dbo].[Exposure] WITH(NOLOCK)	
			WHERE Date < @Date	
                {WHERE_DAUS}
			GROUP BY OriginId
		)DAUS
			on INSTALL.OriginId = DAUS.OriginId";
         * */
        const string queryInstallationReportDrillDown_Total = @"
        SELECT 			
			COALESCE(INSTALL.OriginId, UNIQUE_INSTALL.OriginId, DAUS.OriginId) OriginId
			,ISNULL(UNIQUE_INSTALL.TotalCost, 0) TotalCost
			,ISNULL(UNIQUE_INSTALL.TotalUniqueInstallation, 0) TotalUniqueInstallation
			,ISNULL(UNIQUE_INSTALL.TotalEffectiveUniqueInstalls, 0) TotalEffectiveUniqueInstalls
			,ISNULL(INSTALL.TotlaRevenue, 0) TotlaRevenue
			,ISNULL(DAUS.DAUs, 0) DAUs
		FROM
		(
			SELECT SUM(Revenue) TotlaRevenue				
				,OriginId
			FROM [dbo].[InstallationReportAgg6.0] WITH(NOLOCK)
			WHERE Date < @Date
                {WHERE_INSTALL}
			GROUP BY OriginId
		)INSTALL
		LEFT JOIN
		(
			SELECT  COUNT(*) TotalUniqueInstallation
				,CAST(SUM(Factor) as int) TotalEffectiveUniqueInstalls
				,SUM(CostOfInstallation * Factor) TotalCost
				,OriginId
			FROM [dbo].[InstallationIpOrigin6.2]
			WHERE Date < @Date
                {WHERE_INSTALL}
			GROUP BY OriginId
		)UNIQUE_INSTALL
			on UNIQUE_INSTALL.OriginId = INSTALL.OriginId
		LEFT JOIN
		(
			SELECT  SUM(CAST(Visitor as bigint)) DAUs, OriginId
			FROM [DynamicNoProblem].[dbo].[Exposure] WITH(NOLOCK)	
			WHERE Date < @Date	
                {WHERE_DAUS}
			GROUP BY OriginId
		)DAUS
			on DAUS.OriginId = COALESCE(INSTALL.OriginId, INSTALL.OriginId)";
        public DataModel.Reports.Response.DistributionInstallationReportResponseDrillDown CreateDrillDown(NoProblem.Core.DataModel.Reports.AddOnInstallationReportRequest request)
        {
            DataModel.Reports.Response.DistributionInstallationReportResponseDrillDown _response = new DataModel.Reports.Response.DistributionInstallationReportResponseDrillDown();
            AsyncDistributionInstallationReportDrillDown caller = new AsyncDistributionInstallationReportDrillDown(this.InstallationReportDrillDown);
            AsyncInstallationReportDrillDown_Total caller_total = new AsyncInstallationReportDrillDown_Total(this.InstallationReportDrillDown_Total);
            IAsyncResult result = caller.BeginInvoke(request.FromDate, request.ToDate, request.Origins, request.Country, null, null);
            IAsyncResult result_total = caller_total.BeginInvoke(request.ToDate, request.Origins, request.Country, null, null);

            List<DataModel.Reports.Response.InstallationReportInjectDetails> Injections_data = InstallationReportDrillDown_Injections(request.FromDate, request.ToDate, request.Origins, request.Country);
           
            _response.Data = caller.EndInvoke(result);
            List<DataModel.Reports.Response.InstallationReportTotalDetailsDrillDown> list_total = caller_total.EndInvoke(result_total);
            
            _response.SetValues(Injections_data, list_total);
            return _response;
        }
        private List<DataModel.Reports.Response.DistributionInstallationReportRowDrillDown> InstallationReportDrillDown(DateTime fromDate, DateTime toDate, Guid[] origins, string Country)
        {
            DateTime fromDateFixed = fromDate;
            DateTime toDateFixed = toDate.AddDays(1);
            //  FixDatesToUtcFullDays(ref fromDateFixed, ref toDateFixed);
            List<DataModel.Reports.Response.DistributionInstallationReportRowDrillDown> response = new List<DataModel.Reports.Response.DistributionInstallationReportRowDrillDown>();

            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    StringBuilder query = new StringBuilder(queryInstallationReportGeneralDrillDown);
                    string _where = string.Empty, _where_daus = string.Empty, _origins = string.Empty;
                    if (origins != null && origins.Length > 0)
                    {                        
                        for (int i = 0; i < origins.Length; i++)
                        {
                            _origins += origins[i].ToString() + ";";
                        }
                       
                    }
                    if (!string.IsNullOrEmpty(Country))
                    {
                        _where += " AND Country = @country ";
                        _where_daus += " AND CountryId = [DynamicNoProblem].[dbo].[GetCountryIdByIPName](@country) ";
                        cmd.Parameters.AddWithValue("@country", Country);
                    }
                    query.Replace("{WHERE}", _where);
               //     query = query.Replace("{WHERE_ORIGIN}", _where_origin);
                    query.Replace("{WHERE_DAUS}", _where_daus);
                    cmd.CommandText = query.ToString();
                    cmd.Parameters.AddWithValue("@from", fromDateFixed);
                    cmd.Parameters.AddWithValue("@to", toDateFixed);
                    if(string.IsNullOrEmpty(_origins))
                        cmd.Parameters.AddWithValue("@origins", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@origins", _origins);
                    cmd.CommandTimeout = 120;
                    conn.Open();
                    /*
                    string PreCommand = "SET ANSI_NULLS OFF";
                    using (SqlCommand pre_cmd = new SqlCommand(PreCommand, conn))
                    {
                        pre_cmd.ExecuteNonQuery();
                    }
                     * */
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DataModel.Reports.Response.DistributionInstallationReportRowDrillDown row = new DataModel.Reports.Response.DistributionInstallationReportRowDrillDown();
                        row.OriginName = reader["New_name"] == DBNull.Value ? "-UNKNOWN-" : (string)reader["New_name"];
                        row.OriginId = reader["OriginId"] == DBNull.Value ? Guid.Empty : (Guid)reader["OriginId"];
                        row.Installations = reader["Installations"] == DBNull.Value ? 0 : (int)reader["Installations"];
                        row.UniqeInstallations = reader["UniqueInstallations"] == DBNull.Value ? 0 : (int)reader["UniqueInstallations"];
                        row.EffectiveUniqueInstalls = reader["EffectiveUniqueInstalls"] == DBNull.Value ? 0 : (int)reader["EffectiveUniqueInstalls"];
                        row.Uninstallation = reader["Uninstallations"] == DBNull.Value ? 0 : (int)reader["Uninstallations"];
                        row.Revenue = reader["Revenue"] == DBNull.Value ? 0m : (decimal)reader["Revenue"];
                        row.DAUS = reader["Visitor"] == DBNull.Value ? 0L : (long)reader["Visitor"];
                        row.Cost = (decimal)reader["Cost"];
                        response.Add(row);
                    }
                    conn.Close();
                }
            }
            return response;
        }
        private List<DataModel.Reports.Response.InstallationReportTotalDetailsDrillDown> InstallationReportDrillDown_Total(DateTime toDate, Guid[] origins, string Country)
        {
            StringBuilder query = new StringBuilder(queryInstallationReportDrillDown_Total);
            DateTime toDateFixed = toDate.AddDays(1);
            List<DataModel.Reports.Response.InstallationReportTotalDetailsDrillDown> response = new List<DataModel.Reports.Response.InstallationReportTotalDetailsDrillDown>();

             using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
             {
                 using (SqlCommand cmd = new SqlCommand())
                 {
                     string StartQuery = string.Empty, WhereInstall = string.Empty, WhereDAUs = string.Empty;
                     cmd.Connection = conn;
                     if (origins != null && origins.Length > 0)
                     {
                         string _origins = string.Empty;
                         for (int i = 0; i < origins.Length; i++)
                         {
                             _origins += origins[i].ToString() + ";";
                         }
                         StartQuery += @"
DECLARE @temp_origins TABLE
(
	ID uniqueidentifier
)
INSERT INTO @temp_origins(ID)
SELECT Value
FROM dbo.fn_ParseDelimitedGuid(@origins, ';')
";
                         
                         cmd.Parameters.AddWithValue("@origins", _origins);
                         WhereInstall += " AND OriginId in (SELECT ID FROM @temp_origins) ";
                     }
                     if (!string.IsNullOrEmpty(Country))
                     {
                         StartQuery += " DECLARE @CountryId int = dbo.GetCountryIdByIP2LocationName(@country) ";
                         WhereInstall+=" AND Country = @country ";
                         WhereDAUs += " AND CountryId = @CountryId ";
                         cmd.Parameters.AddWithValue("@country", Country);
                     }
                     query.Insert(0, StartQuery);
                     query.Replace("{WHERE_INSTALL}", WhereInstall);
                     query.Replace("{WHERE_DAUS}", WhereDAUs);
                     cmd.Parameters.AddWithValue("@Date", toDateFixed);
                     cmd.CommandText = query.ToString();
                     cmd.CommandTimeout = 120;
                     conn.Open();
                     SqlDataReader reader = cmd.ExecuteReader();
                     while (reader.Read())
                     {
                         DataModel.Reports.Response.InstallationReportTotalDetailsDrillDown row = new DataModel.Reports.Response.InstallationReportTotalDetailsDrillDown();
                         row.TotalCost = (decimal)reader["TotalCost"];
                         row.TotalDAUs = (long)reader["DAUs"];
                         row.TotalRevenue = (decimal)reader["TotlaRevenue"];
                         row.TotalUniqueInstalls = (int)reader["TotalUniqueInstallation"];
                         row.TotalEffectiveUniqueInstalls = (int)reader["TotalEffectiveUniqueInstalls"];
                         row.OriginId = reader["OriginId"] == DBNull.Value ? Guid.Empty : (Guid)reader["OriginId"];
                         response.Add(row);
                     }
                 }
                 
             }
             return response;
        }
        private List<DataModel.Reports.Response.InstallationReportInjectDetails> InstallationReportDrillDown_Injections(DateTime fromDate, DateTime toDate, Guid[] origins, string Country)
        {

            StringBuilder query = new StringBuilder(queryInstallationReportDrillDown_Injection);

            string _where = string.Empty, _origins = string.Empty;//, _where_origin = string.Empty, _where_daus = string.Empty;
            DateTime fromDateFixed = fromDate;
            DateTime toDateFixed = toDate.AddDays(1);
            //  FixDatesToUtcFullDays(ref fromDateFixed, ref toDateFixed);
            List<DataModel.Reports.Response.InstallationReportInjectDetails> response = new List<DataModel.Reports.Response.InstallationReportInjectDetails>();

            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    if (origins != null && origins.Length > 0)
                    {
                        for (int i = 0; i < origins.Length; i++)
                        {
                            _origins += origins[i].ToString() + ";";
                        }

                    }
                    if (!string.IsNullOrEmpty(Country))
                    {
                        _where += " AND i.Country = @country ";
                        cmd.Parameters.AddWithValue("@country", Country);
                    }

                    query.Replace("{WHERE}", _where);
                    cmd.CommandText = query.ToString();
                    cmd.Parameters.AddWithValue("@from", fromDateFixed);
                    cmd.Parameters.AddWithValue("@to", toDateFixed);
                    if (string.IsNullOrEmpty(_origins))
                        cmd.Parameters.AddWithValue("@origins", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@origins", _origins);
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DataModel.Reports.Response.InstallationReportInjectDetails data = new DataModel.Reports.Response.InstallationReportInjectDetails();
                        data.OriginId = reader["OriginId"] == DBNull.Value ? Guid.Empty : (Guid)reader["OriginId"];
                        if (reader["InjectionId"] == DBNull.Value)
                            data.InjectionId = null;
                        else
                            data.InjectionId = (Guid)reader["InjectionId"];

                        if (reader["New_name"] == DBNull.Value)
                            data.Name = null;
                        else
                            data.Name = (string)reader["New_name"];

                        if (reader["Revenue"] == DBNull.Value)
                            data.Revenue = 0m;
                        else
                            data.Revenue = (decimal)reader["Revenue"];
                        response.Add(data);
                    }
                    conn.Close();
                }
            }
            return response;

        }

       
    }
}
