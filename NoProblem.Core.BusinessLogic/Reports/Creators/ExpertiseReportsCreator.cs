﻿/////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: Effect.Onecall.BusinessLogic.Reports.ExpertiseReportsCreator
//  File: ExpertiseReportsCreator.cs
//  Description: Creates reports related to expertises
//  Author: Alain Adler
//  Company: Onecall
//  Createdon: 31/10/10
//
/////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class ExpertiseReportsCreator : XrmUserBase
    {
        #region Consts

        private const string ALLEXPERTISES = "AllExpertises";
        private const string SECONDARYEXPERTISE = "SecondaryExpertise";
        private const string PRIMARYEXPERTISE = "PrimaryExpertise";
        private const string GUID = "Guid";
        private const string EXPERTISENAME = "ExpertiseName";
        private const string TOTAL = "Total";
        private const string APPROVEDNOTAVAILABLE = "ApprovedNotAvailable";
        private const string APPROVEDAANDAVAILABLE = "ApprovedAndAvailable";
        private const string CANDIDATE = "Candidate";
        private const string INACTIVE = "Inactive";

        #endregion

        #region Ctor
        internal ExpertiseReportsCreator(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal List<DML.Reports.ExpertiseRow> CreateSpecificExpertiseReport(Guid expertiseId, int expertiseLevel)
        {
            DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            var expertisesData = expertiseDal.GetAllExpertiseReport();
            if (expertiseLevel == 2)
            {
                expertisesData = expertisesData.Where(x => x.Id == expertiseId).ToList();
            }
            else
            {
                DataAccessLayer.SecondaryExpertise secondaryDal = new NoProblem.Core.DataAccessLayer.SecondaryExpertise(XrmDataContext);
                IEnumerable<DML.Xrm.new_secondaryexpertise> secondaries = secondaryDal.GetSecondaryExpertiseByPrimary(expertiseId);
                List<DML.Reports.ExpertiseRow> retVal = new List<NoProblem.Core.DataModel.Reports.ExpertiseRow>();
                var primary = expertisesData.FirstOrDefault(x => x.Id == expertiseId);
                if (primary != null)
                {
                    retVal.Add(primary);
                }
                foreach (var second in secondaries)
                {
                    var dataRow = expertisesData.FirstOrDefault(x => x.Id == second.new_secondaryexpertiseid);
                    if (dataRow != null)
                    {
                        retVal.Add(dataRow);
                    }
                }
                expertisesData = retVal;
            }
            return expertisesData;
        }

        internal List<DML.Reports.ExpertiseRow> CreateAllExpertisesReport()
        {
            DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            var expertisesData = expertiseDal.GetAllExpertiseReport();
            return expertisesData;
        }

        #endregion
    }
}
