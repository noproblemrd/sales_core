﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Reports;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class ProfitableReportCreator : XrmUserBase
    {
        public ProfitableReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public ProfitableReportResponse Create(ProfitableReportRequest request)
        {
            ProfitableReportResponse response = new ProfitableReportResponse();
           // int Users = GetUsers(request.From, request.To);
            List<ProfitableReportRow> mainData = GetData(request.From, request.To, request.HeadingId, request.OriginId, 
                request.ProfitFilter, request.OnlyNonUpsales, request.Country, request.SliderType);
            response.MainData = mainData;
            response.MainProfitTotal = response.MainData.Sum(x => x.Profit);
            response.MainProfitPercentageTotal = CalcuateProfitPercentageTotal(mainData, response.MainProfitTotal);
            if (request.UseCompare)
            {
          //      int UsersCompare = GetUsers(request.FromCompare, request.ToCompare);
                List<ProfitableReportRow> compareData = GetData(request.FromCompare, request.ToCompare, request.HeadingIdCompare, 
                    request.OriginIdCompare, request.ProfitFilter, request.OnlyNonUpsalesCompare, request.CountryCompare, request.SliderTypeCompare);
                response.CompareData = compareData;
                response.CompareProfitTotal = response.CompareData.Sum(x => x.Profit);
                response.CompareProfitPercentageTotal = CalcuateProfitPercentageTotal(compareData, response.CompareProfitTotal);
            }
            return response;
        }
        public DateTime GetLastUpdate()
        {
            string command = "SELECT max([date]) FROM [dbo].[tbl_profit_aggregation]";
            string _connectionString = System.Configuration.ConfigurationManager.AppSettings["updateConnectionString"];
            DateTime dt = DateTime.MinValue;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    dt = (DateTime)cmd.ExecuteScalar();
                    conn.Close();
                }
            }
            return dt;
        }
        private decimal CalcuateProfitPercentageTotal(List<ProfitableReportRow> data, decimal totalProfit)
        {
            decimal retVal = 0;
            decimal totalRevenue = data.Sum(x => x.Revenue);
            if (totalRevenue != 0)
            {
                retVal = ((totalProfit * 100m) / totalRevenue);
            }
            return retVal;
        }

        private List<ProfitableReportRow> GetData(DateTime from, DateTime to, Guid headingId, Guid originId, 
            DataModel.Reports.ProfitableReportRequest.eProfitFilter profitFilter, bool onlyNonUpsales, int country, int SliderType)
        {
            List<ProfitableReportRow> data = new List<ProfitableReportRow>();
            DataAccessLayer.Incident.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.incident>.SqlParametersList();
            parameters.Add("@from", from.Date);
            parameters.Add("@to", to.Date);
            string query = BuildQuery(headingId, originId, parameters, onlyNonUpsales, country, SliderType);
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                ProfitableReportRow returnRow = new ProfitableReportRow();
                returnRow.HeadingId = (Guid)row["new_primaryexpertiseid"];
                returnRow.Requests = (int)row["requests"];
                returnRow.Cost = (decimal)row["cost"];
                returnRow.Revenue = (decimal)row["revenue"];
                returnRow.TotalRequest = (int)row["TotalRequest"];
                returnRow.Exposures = (int)row["Exposures"];
                /*
                returnRow.Exposure_KDAUs = (float)row["Exposure_KDAUs"];
                returnRow.Request_KDAUs = (float)row["Request_KDAUs"];
                returnRow.EPPL = (float)row["EPPL"];
                 * */
                returnRow.Profit = returnRow.Revenue - returnRow.Cost;
                returnRow.ProfitPercentage = returnRow.Revenue != 0 ? ((returnRow.Profit * 100m) / returnRow.Revenue) : 0;
                int users = (row["users"] == DBNull.Value) ? 0 : (int)row["users"];
                /*
                if (users != 0)
                    returnRow.DAUs_Request = ((double)returnRow.TotalRequest / ((double)users / 1000.00));
                else
                    returnRow.DAUs_Request = 0;
                 * returnRow.Exposure_Request = (returnRow.TotalRequest == 0) ? 0d :
                    (double)returnRow.Exposures / (double)returnRow.TotalRequest;
                 * */
                returnRow.Users = users;
                returnRow.SetValues();
                data.Add(returnRow);
            }
            if (profitFilter == ProfitableReportRequest.eProfitFilter.Losing)
            {
                data = data.Where(x => x.Profit <= 0).ToList<ProfitableReportRow>();
            }
            else if (profitFilter == ProfitableReportRequest.eProfitFilter.Profitable)
            {
                data = data.Where(x => x.Profit > 0).ToList<ProfitableReportRow>();
            }
            return data;
        }

        private string BuildQuery(Guid headingId, Guid originId, DataAccessLayer.Incident.SqlParametersList parameters, 
            bool onlyNonUpsales, int country, int SliderType)
        {
            StringBuilder queryBuilder = new StringBuilder();
            /*
            ,(CASE 
							WHEN (ISNULL(@users, 0)) = 0 THEN 0.0
							ELSE CAST(ISNULL(Exposure.Exposures	, 0) as real) / (CAST(@users as real) / CAST(1000 as real))
							END)
							Exposure_KDAUs
						,(CASE 
							WHEN (ISNULL(@users, 0)) = 0 THEN 0.0
							ELSE CAST(ISNULL(Request.TotalRequest	, 0) as real) / (CAST(@users as real) / CAST(1000 as real))
							END)
							Request_KDAUs
                         ,(CASE 
							WHEN (isnull(Request.requests, 0)) = 0 THEN 0.0
							ELSE CAST(isnull(Request.revenue, 0) as real) / CAST(Request.requests as real)
							END)
							EPPL
             */
            queryBuilder.Append(
                    @"
                    DECLARE @users int
                    SELECT @users = sum(Visitor)
							FROM [dynamicNoProblem].[dbo].[Exposure]
							WHERE [Date] between @from and @to 
                            {whereUsers}
                    select isnull(Request.requests, 0) requests
	                    ,isnull(Request.revenue, 0) revenue
	                    ,isnull(Request.cost, 0) cost
	                    ,isnull(Request.TotalRequest, 0) TotalRequest
	                    ,isnull(Exposure.Exposures	, 0) Exposures
	                    ,coalesce(Request.new_primaryexpertiseid, Exposure.PrimaryExpertiseId) new_primaryexpertiseid                         
                        ,@users users
                    from(
                        {query} 
                        {whereRequest} 
                        group by new_primaryexpertiseid
                    ) Request
                     full outer join
                     (
	                    select isnull(sum(Exposures), 0) Exposures, PrimaryExpertiseId
	                    from dbo.ConversionReportMongo
	                    where date >= @from and date < DATEADD (day , 1 , @to )
                        {whereExposure} 
	                    group by PrimaryExpertiseId
                        
                     ) Exposure on Exposure.PrimaryExpertiseId = Request.new_primaryexpertiseid
                     order by Request.revenue desc
                    "
                    );//having isnull(sum(Exposures), 0) > 0
            string _query;
            if (onlyNonUpsales)
            {
                _query = @"
                        SELECT 
	                        sum(isnull(reqCount, 0)) requests
                            ,sum(isnull(revenue, 0)) revenue
                            ,sum(isnull(cost, 0)) cost
                            ,sum(TotalRequest) TotalRequest
                            ,new_primaryexpertiseid
                            FROM tbl_profit_aggregation_non_upsales
                            where date between @from and @to 
                        ";
                
            }
            else
            {
                _query =
                    @"
                    SELECT 
	                    sum(isnull(reqCount, 0)) requests
                        ,sum(isnull(revenue, 0)) revenue
                        ,sum(isnull(cost, 0)) cost
                        ,sum(TotalRequest) TotalRequest
                        ,new_primaryexpertiseid
                        FROM tbl_profit_aggregation
                        where date between @from and @to ";
                    
            }
            string whereRequest = "", whereExposure = "", whereUsers = "";
            if (headingId != Guid.Empty)
            {
                whereRequest += " and new_primaryexpertiseid = @headingId ";
                whereExposure += " and PrimaryExpertiseId = @headingId ";
                parameters.Add("@headingId", headingId);
            }
            if (originId != Guid.Empty)
            {
                whereRequest += " and new_originid = @originId ";
                whereExposure += " and OriginId = @originId ";
                whereUsers += " and OriginId = @originId ";
                parameters.Add("@originId", originId);
            }
            if (country > -1)
            {
                queryBuilder.Insert(0, @"DECLARE @CountryName nvarchar(250)
	                        SELECT @CountryName = [DynamicNoProblem].[dbo].[GetCountryIP2LOCATION_ByCountryId] (@country) ");

                whereRequest += " and Country = @CountryName ";
                whereExposure += " and Country = @CountryName ";
                whereUsers += " and CountryId = @country ";
                parameters.Add("@country", country);
            }
            if(SliderType > -1)
            {
                whereRequest += " and SliderType = @SliderType ";
                whereExposure += " and SliderType = @SliderType ";
                parameters.Add("@SliderType", SliderType);
            }
            queryBuilder = queryBuilder.Replace("{query}", _query);
            queryBuilder = queryBuilder.Replace("{whereRequest}", whereRequest);
            queryBuilder = queryBuilder.Replace("{whereExposure}", whereExposure);
            queryBuilder = queryBuilder.Replace("{whereUsers}", whereUsers);
           // queryBuilder.Append(" group by new_primaryexpertiseid ");
           // queryBuilder.Append(" order by sum(isnull(revenue, 0)) desc ");
            return queryBuilder.ToString();
        }
        private int GetUsers(DateTime _from, DateTime _to)
        {
            List<KeyValuePair<string, object>> list = new List<KeyValuePair<string, object>>();
            list.Add(new KeyValuePair<string, object>("@from", _from));
            list.Add(new KeyValuePair<string, object>("@to", _to));
            string command = "EXEC dbo.GetDailyUsers @from, @to";
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            object obj = dal.ExecuteScalar(command, list);
            return (obj == DBNull.Value) ? 0 : (int)obj;
        }
    }
}
