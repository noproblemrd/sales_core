﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class InvalidRequestsReportCreator : XrmUserBase
    {
        #region Ctor
        internal InvalidRequestsReportCreator(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        internal InvalidRequestsReportResponse Create(InvalidRequestsReportRequest request)
        {
            InvalidRequestsReportResponse response = new InvalidRequestsReportResponse();
            DataAccessLayer.Incident incidentDAL = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            IEnumerable<DML.Xrm.incident> incidentList = FilterIncidentsByStatus(request, incidentDAL);
            incidentList = FilterIncidentsByFromDate(incidentList, request.FromDate);
            incidentList = FilterIncidentsByToDate(incidentList, request.ToDate);
            incidentList = FilterIncidentsByBadWord(incidentList, request.BadWord);
            incidentList = FilterIncidentsByConsumerPhone(incidentList, request.PhoneNumber);
            incidentList = from inci in incidentList orderby inci.createdon descending select inci;
            DataAccessLayer.Contact contactDAL = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            List<DML.Reports.InvalidRequestData> dataList = new List<InvalidRequestData>(incidentList.Count());
            foreach (DML.Xrm.incident inci in incidentList)
            {
                DML.Reports.InvalidRequestData data = new InvalidRequestData();
                data.CaseNumber = inci.ticketnumber;
                data.ConsumerPhoneNumber = GetConsumerPhoneNumber(inci, contactDAL);
                data.CreatedOn = FixDateToLocal(inci.createdon.Value);
                data.Description = inci.description;
                data.InvalidStatus = GetInvalidStatus(inci.statuscode.Value);
                data.Title = inci.title;
                data.CaseId = inci.incidentid;
                dataList.Add(data);
            }
            response.InvalidRequests = dataList;
            return response;
        }

        private InvalidRequestStatus GetInvalidStatus(int incidentStatus)
        {
            DML.Xrm.incident.Status status = (DML.Xrm.incident.Status)incidentStatus;
            DML.Reports.InvalidRequestStatus retVal;
            if (status == NoProblem.Core.DataModel.Xrm.incident.Status.InvalidConsumerPhone)
            {
                retVal = InvalidRequestStatus.InvalidPhoneNumber;
            }
            else if (status == NoProblem.Core.DataModel.Xrm.incident.Status.BADWORD)
            {
                retVal = InvalidRequestStatus.BadWord;
            }
            else
            {
                retVal = InvalidRequestStatus.BlackList;
            }
            return retVal;
        }

        private IEnumerable<NoProblem.Core.DataModel.Xrm.incident> FilterIncidentsByConsumerPhone(IEnumerable<NoProblem.Core.DataModel.Xrm.incident> incidentList, string phone)
        {
            IEnumerable<DML.Xrm.incident> retVal = incidentList;
            if (string.IsNullOrEmpty(phone) == false)
            {
                DataAccessLayer.Contact contactDAL = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
                retVal = from inci in incidentList
                         where
                         IsConsumerPhoneNumber(inci, phone, contactDAL)
                         select
                         inci;
            }
            return retVal;
        }

        private bool IsConsumerPhoneNumber(DML.Xrm.incident incident, string phone, DataAccessLayer.Contact contactDAL)
        {
            string consumerPhone = GetConsumerPhoneNumber(incident, contactDAL);
            return consumerPhone == phone;
        }

        private string GetConsumerPhoneNumber(DML.Xrm.incident incident, DataAccessLayer.Contact contactDAL)
        {
            DML.Xrm.contact consumer = contactDAL.Retrieve(incident.customerid.Value);
            return consumer.mobilephone;
        }

        private IEnumerable<NoProblem.Core.DataModel.Xrm.incident> FilterIncidentsByBadWord(IEnumerable<NoProblem.Core.DataModel.Xrm.incident> incidentList, string badWord)
        {
            IEnumerable<DML.Xrm.incident> retVal = incidentList;
            if (string.IsNullOrEmpty(badWord) == false)
            {
                retVal = from inci in incidentList
                         where
                         string.IsNullOrEmpty(inci.description) == false
                         &&
                         inci.description.Contains(badWord)
                         select inci;
            }
            return retVal;
        }

        private IEnumerable<NoProblem.Core.DataModel.Xrm.incident> FilterIncidentsByToDate(IEnumerable<NoProblem.Core.DataModel.Xrm.incident> incidentList, DateTime? toDate)
        {
            IEnumerable<DML.Xrm.incident> retVal = incidentList;
            if (toDate.HasValue)
            {
                DateTime toDateValue = toDate.Value.Date.AddDays(1);
                toDateValue = FixDateToUtcFromLocal(toDateValue);
                retVal = from inci in incidentList
                         where
                         inci.createdon.Value.CompareTo(toDateValue) <= 0
                         select inci;
            }
            return retVal;
        }

        private IEnumerable<NoProblem.Core.DataModel.Xrm.incident> FilterIncidentsByFromDate(IEnumerable<NoProblem.Core.DataModel.Xrm.incident> incidentList, DateTime? fromDate)
        {
            IEnumerable<DML.Xrm.incident> retVal = incidentList;
            if (fromDate.HasValue)
            {
                DateTime fromDateValue = fromDate.Value.Date;
                fromDateValue = FixDateToUtcFromLocal(fromDateValue);
                retVal = from inci in incidentList
                         where
                         inci.createdon.Value.CompareTo(fromDateValue) >= 0
                         select inci;
            }
            return retVal;
        }

        private static IEnumerable<DML.Xrm.incident> FilterIncidentsByStatus(InvalidRequestsReportRequest request, DataAccessLayer.Incident incidentDAL)
        {
            IEnumerable<DML.Xrm.incident> incidentList = null;
            if (request.InvalidStatus == InvalidRequestStatus.All)
            {
                incidentList = incidentDAL.GetIncidentsWithInvalidStatus();
            }
            else if (request.InvalidStatus == InvalidRequestStatus.BadWord)
            {
                incidentList = incidentDAL.GetIncidentsByStatus(DML.Xrm.incident.Status.BADWORD);
            }
            else if (request.InvalidStatus == InvalidRequestStatus.BlackList)
            {
                incidentList = incidentDAL.GetIncidentsByStatus(DML.Xrm.incident.Status.BLACKLIST);
            }
            else
            {
                incidentList = incidentDAL.GetIncidentsByStatus(DML.Xrm.incident.Status.InvalidConsumerPhone);
            }
            return incidentList;
        }


    }
}
