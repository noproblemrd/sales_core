﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Reports
{
	internal class PpaControlReportCreator : XrmUserBase
	{
		public PpaControlReportCreator(DataModel.Xrm.DataContext xrmDataContext)
			: base(xrmDataContext)
		{

		}

		public List<PpaControlReportRow> Create(DateTime from, DateTime to)
		{
			List<PpaControlReportRow> retVal = new List<PpaControlReportRow>();
			from = from.Date;
			to = to.Date.AddDays(1);
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
			parameters.Add("@from", from);
			parameters.Add("@to", to);
			var reader = dal.ExecuteReader(query, parameters);
			Guid current = Guid.Empty;
			foreach (var row in reader)
			{
				if ((Guid)row["AccountId"] != current)
				{
					if (current != Guid.Empty)
					{
						var totalRow = CreateTotalRow(current, retVal);
						retVal.Add(totalRow);
					}
					current = (Guid)row["AccountId"];
				}
				PpaControlReportRow data = CreateNormalDataRow(row);
				retVal.Add(data);
			}
			return retVal;
		}

		private PpaControlReportRow CreateTotalRow(Guid current, List<PpaControlReportRow> retVal)
		{
			PpaControlReportRow data = new PpaControlReportRow();
			var lst = retVal.Where(x => x.SupplierId == current);
			var first = lst.First();
			data.SupplierId = current;
			data.Balance = first.Balance;
			data.BizId = first.BizId;
			data.Calls = lst.Sum(x => x.Calls);
			data.Charged = lst.Sum(x => x.Charged);
			data.Heading = string.Empty;
			data.IsOldFromSaar = first.IsOldFromSaar;
			data.Name = first.Name;
			data.RechargeAmount = first.RechargeAmount;
			data.RechargeTypeCode = first.RechargeTypeCode;
			data.Revenue = lst.Sum(x => x.Revenue);
			data.IsTotalRow = true;
            data.AccountType = first.AccountType;
            data.Status = first.Status;
            data.PricePerCall = lst.Average(x => x.PricePerCall);
            data.Deposits = lst.Sum(x => x.Deposits);
            data.North = first.North;
            data.Sharon = first.Sharon;
            data.Center = first.Center;
            data.Jerusalem = first.Jerusalem;
            data.Shfela = first.Shfela;
            data.South = first.South;
			return data;
		}

		private PpaControlReportRow CreateNormalDataRow(Dictionary<string, object> row)
		{
			PpaControlReportRow data = new PpaControlReportRow();
			data.SupplierId = (Guid)row["AccountId"];
			data.Balance = (decimal)row["CashBalance"];
			data.BizId = Convert.ToString(row["BizId"]);
			data.Calls = (int)row["Calls"];
			data.Charged = (int)row["ChargedCalls"];
			data.Heading = Convert.ToString(row["HeadingName"]);
			data.IsOldFromSaar = row["isOld"] != DBNull.Value ? (bool)row["isOld"] : false;
			data.Name = Convert.ToString(row["Name"]);
			data.RechargeAmount = (int)row["RechargeAmount"];
			if (row["RechargeTypeCode"] != DBNull.Value)
			{
				data.RechargeTypeCode = (DataModel.Xrm.account.RechargeTypeCode)(int)row["RechargeTypeCode"];
			}
			else
			{
				data.RechargeTypeCode = null;
			}
			data.Revenue = (decimal)row["Revenue"];
			data.IsTotalRow = false;
            data.AccountType = Convert.ToString(row["AccountType"]);
            int status = (int)row["new_status"];
            string accountStatus;
            if (status == 3)
            {
                accountStatus = "INACTIVE";
            }
            else
            {
                 bool inactiveWithMoney = (bool)row["New_isinactivewithmoney"];
                 if (inactiveWithMoney)
                 {
                     accountStatus = "INACTIVE_WITH_MONEY";
                 }
                 else
                 {
                     bool isAvailable = (int)row["isAvailable"] == 1;
                     if (isAvailable)
                     {
                         accountStatus = "AVAILABLE";
                     }
                     else
                     {
                         accountStatus = "NOT_AVAILABLE";
                     }
                 }
            }
            data.Status = accountStatus;
            data.PricePerCall = (decimal)row["price"];
            data.Deposits = (int)row["deposits"];
            data.North = (int)row["north"] == 1;
            data.Sharon = (int)row["sharon"] == 1;
            data.Center = (int)row["center"] == 1;
            data.Jerusalem = (int)row["jerusalem"] == 1;
            data.Shfela = (int)row["shfela"] == 1;
            data.South = (int)row["south"] == 1;
			return data;
		}

		private const string query =
            @"
declare @now datetime = getdate()

select accE.AccountId, accE.New_BizId as 'BizId', accB.Name
, accE.New_IsExternalSupplier as 'isOld'
, CASE accE.New_RechargeEnabled when 1 then accE.New_RechargeTypeCode else NULL end as 'RechargeTypeCode'
, CASE accE.New_RechargeEnabled when 1 then isnull(accE.New_RechargeAmount, 0) else 0 end as 'RechargeAmount'
, isnull(accE.new_cashbalance, 0) as 'CashBalance'
, ae.new_primaryexpertiseidName as 'HeadingName'
, ISNULL(chargeTable.cnt, 0) + ISNULL(noChargeTable.cnt, 0) 'Calls'
, ISNULL(chargeTable.cnt, 0) 'ChargedCalls'
, ISNULL(chargeTable.revenue, 0) 'Revenue'
, case ISNULL(New_DapazStatus, 1) when 1 then 'PPA' else 'RARAH_QUOTE' end 'AccountType'
, CASE WHEN
	(accE.new_unavailablefrom > @now OR accE.new_unavailableto < @now OR
	  (accE.new_unavailablefrom IS NULL AND accE.new_unavailableto IS NULL))    
	THEN 1 ELSE 0 END as 'isAvailable'
, accE.new_status
, accE.New_isinactivewithmoney
, ae.new_incidentprice as 'price'
, isnull(deposits.total, 0) as 'deposits'
, regions.[1] as 'north'
, regions.[2] as 'sharon'
, regions.[3] as 'center'
, regions.[4] as 'jerusalem'
, regions.[5] as 'shfela'
, regions.[6] as 'south'
from AccountExtensionBase accE with(nolock)
join AccountBase accB with(nolock) on accE.AccountId = accB.AccountId
join new_accountexpertise ae with(nolock) on ae.new_accountid = accB.AccountId
left join(
	select COUNT(iaE.New_incidentaccountId) cnt, SUM(iaE.new_potentialincidentprice) revenue, iaE.new_accountid as 'AccountId', incE.new_primaryexpertiseid as 'HeadingId' 
	from New_incidentaccountExtensionBase iaE with(nolock)
	join IncidentExtensionBase incE with(nolock) on iaE.new_incidentid = incE.IncidentId
	where iaE.New_CreatedOnLocal between @from and @to
	and iaE.New_IsGoldenNumberCall = 0
	and iaE.New_tocharge = 1
	group by iaE.new_accountid, incE.new_primaryexpertiseid
) chargeTable on chargeTable.AccountId = accE.AccountId and chargeTable.HeadingId = ae.new_primaryexpertiseid
left join(
	select COUNT(iaE.New_incidentaccountId) cnt, iaE.new_accountid as 'AccountId', incE.new_primaryexpertiseid as 'HeadingId'
	from New_incidentaccountExtensionBase iaE with(nolock)
	join New_incidentaccountBase iaB with(nolock) on iaE.New_incidentaccountId = iaB.New_incidentaccountId
	join IncidentExtensionBase incE with(nolock) on iaE.new_incidentid = incE.IncidentId
	where iaE.New_CreatedOnLocal between @from and @to
	and iaE.New_IsGoldenNumberCall = 0
	and iaE.New_tocharge = 0
	and iaB.statuscode in (10, 12, 13)
	group by iaE.new_accountid, incE.new_primaryexpertiseid
) noChargeTable on noChargeTable.AccountId = accE.AccountId and noChargeTable.HeadingId = ae.new_primaryexpertiseid
left join(
	select SUM(new_total) total, new_accountid from 
	New_supplierpricing with(nolock)
	where CreatedOn between @from and @to
	group by new_accountid
)  deposits on deposits.new_accountid = accE.AccountId
join (
select new_accountid, [1], [2], [3], [4], [5], [6]
from
	(select new_code code, new_accountid
	from New_regionaccountexpertise rae with(nolock)
	join New_region reg with(nolock) on reg.New_regionId = rae.new_regionid
	where rae.DeletionStateCode = 0 and reg.New_Level = 1) sourceTable
	pivot ( count(code) for code in ([1], [2], [3], [4], [5], [6]) ) as pivotTable
) regions on regions.new_accountid = accE.AccountId
where
accE.new_status in (1, 3)
and (accE.new_dapazstatus = 1 or accE.new_dapazstatus is null or accE.New_DapazStatus = 4)
and ae.statecode = 0 and ae.DeletionStateCode = 0
order by accB.Name
";
	}
}
