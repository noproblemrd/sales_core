﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class FullUrlExposuresReportCreator : XrmUserBase
    {
        public FullUrlExposuresReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {

        }

        private const string query =
            @"
                WITH PagerTbl AS
                (                
                SELECT ROW_NUMBER() OVER(ORDER BY createdon desc) AS RowNum,
                    new_url, createdon
                    from
                    New_exposure with(nolock)
                    where DeletionStateCode = 0
                    and CreatedOn between @from and @to
                    and new_originid = @originId
                )
                    SELECT * , (select max(RowNum) from PagerTbl) as totalRows
                      FROM PagerTbl
                     WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
                                      AND @PageNum * @PageSize)
                            or @PageSize = -1)
                     ORDER BY RowNum";

        public DataModel.Reports.FullUrlExposureReportResponse Create(DateTime fromDate, DateTime toDate, Guid originId, int pageSize, int pageNumber)
        {
            DataModel.Reports.FullUrlExposureReportResponse response = new NoProblem.Core.DataModel.Reports.FullUrlExposureReportResponse();
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@originId", originId));
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate.Date));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate.Date.AddDays(1)));
            parameters.Add(new KeyValuePair<string, object>("@PageSize", pageSize));
            parameters.Add(new KeyValuePair<string, object>("@PageNum", pageNumber));
            DataAccessLayer.Exposure dal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                DataModel.Reports.FullUrlExposureReportRow data = new NoProblem.Core.DataModel.Reports.FullUrlExposureReportRow();
                data.CreatedOn = (DateTime)row["createdon"];
                data.Url = (string)row["new_url"];
                response.Rows.Add(data);
                if (response.TotalRows == 0)
                {
                    response.TotalRows = unchecked((int)((long)row["totalRows"]));
                }
            }
            response.CurrentPage = pageNumber;
            response.TotalPages = (int)Math.Ceiling(((double)response.TotalRows) / ((double)pageSize));
            return response;
        }
    }
}
