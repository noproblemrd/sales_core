﻿using NoProblem.Core.DataModel.Reports.Response;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Reports.Creators
{
    internal class AdEngineReportCreator : XrmUserBase
    {
         #region Ctor
        internal AdEngineReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        private const string queryAdEngineReport = @"
SELECT SUM([Hits]) Hits
	,DATEADD(DAY, DATEDIFF(DAY, 0, Date),0) Date      
FROM [dbo].[PopupExposureAgg]
WHERE Date >= @from and Date < @to
    {WHERE_PopupExposure}
GROUP BY DATEADD(DAY, DATEDIFF(DAY, 0, Date),0)
ORDER BY DATEADD(DAY, DATEDIFF(DAY, 0, Date),0) desc";
        public List<AdEngineReportResponse> AdEngineReport(AdEngineReportRequest request)
        {
            List<AdEngineReportResponse> response = new List<AdEngineReportResponse>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    StringBuilder query = new StringBuilder(queryAdEngineReport);
                    StringBuilder _where = new StringBuilder();
                    if(request.OriginId != Guid.Empty)
                    {
                        _where.Append(" and OriginId = @OriginId ");
                        cmd.Parameters.AddWithValue("@OriginId", request.OriginId);
                    }
                    if(request.AdEngineCampaign != Guid.Empty)
                    {
                        _where.Append(" and AdEngineCampaignId = @CampaignId ");
                        cmd.Parameters.AddWithValue("@CampaignId", request.AdEngineCampaign);
                    }
                    if (!string.IsNullOrEmpty(request.Country))
                    {
                        _where.Append(" and Country = @Country ");
                        cmd.Parameters.AddWithValue("@Country", request.Country);
                    }
                    cmd.Parameters.AddWithValue("@from", request.FromDateQuery);
                    cmd.Parameters.AddWithValue("@to", request.ToDateQuery);
                    query.Replace("{WHERE_PopupExposure}", _where.ToString());
                    cmd.Connection = conn;
                    cmd.CommandText = query.ToString();
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        AdEngineReportResponse aer = new AdEngineReportResponse();
                        aer.Date = (DateTime)reader["Date"];
                        aer.Hits = (int)reader["Hits"];
                        response.Add(aer);
                    }
                    conn.Close();

                }
            }
            return response;
        }

    }
}
