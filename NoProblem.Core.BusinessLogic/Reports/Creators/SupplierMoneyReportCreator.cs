﻿using System;
using System.Collections.Generic;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class SupplierMoneyReportCreator : XrmUserBase
    {
        #region Ctor
        internal SupplierMoneyReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal SupplierMoneyReportResponse Create(string supplierNumber)
        {
            SupplierMoneyReportResponse response = new SupplierMoneyReportResponse();
            DataAccessLayer.SupplierPricing dal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            int realMoneyDeposited;
            int bonusMoneyDeposited;
            if (!string.IsNullOrEmpty(supplierNumber))
            {
                GetSingleSupplierData(supplierNumber, response, dal, out realMoneyDeposited, out bonusMoneyDeposited);
            }
            else
            {
                GetAllSuppliersSummaryData(response, dal, out realMoneyDeposited, out bonusMoneyDeposited);
            }
            return response;
        }

        internal List<SupplierMoneyReportResponse> CreateExcel()
        {
            SupplierMoneyReportResponse response = new SupplierMoneyReportResponse();
            DataAccessLayer.SupplierPricing dal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            List<SupplierMoneyReportResponse> dataList = dal.GetSupplierMoneyDataForExcel();
            return dataList;
        } 

        #endregion

        #region Private Methods

        private void GetAllSuppliersSummaryData(SupplierMoneyReportResponse response, DataAccessLayer.SupplierPricing dal, out int realMoneyDeposited, out int bonusMoneyDeposited)
        {
            int realUsed, bonusUsed, realLeft, bonusLeft;
            dal.GetAllSuppliersMoneySummaryReportData(out realMoneyDeposited, out bonusMoneyDeposited, out realUsed, out bonusUsed, out realLeft, out bonusLeft);
            response.DepositBonus = bonusMoneyDeposited;
            response.DepositReal = realMoneyDeposited;
            response.LeftBonus = bonusLeft;
            response.LeftReal = realLeft;
            response.SupplierId = Guid.Empty;
            response.SupplierName = "All";
            response.SupplierNumber = string.Empty;
            response.UsedBonus = bonusUsed;
            response.UsedReal = realUsed;
        }

        private void GetSingleSupplierData(string supplierNumber, SupplierMoneyReportResponse response, DataAccessLayer.SupplierPricing dal, out int realMoneyDeposited, out int bonusMoneyDeposited)
        {
            int usedMoney;
            Guid supplierId;
            string supplierName;
            dal.GetSupplierMoneyReportData(supplierNumber, out realMoneyDeposited, out bonusMoneyDeposited, out usedMoney, out supplierId, out supplierName);
            if (supplierId != Guid.Empty)
            {
                response.SupplierId = supplierId;
                response.SupplierName = supplierName;
                response.SupplierNumber = supplierNumber;
                int usedReal;
                int usedBonus;
                int dif = realMoneyDeposited - usedMoney;
                if (dif < 0)
                {
                    usedReal = realMoneyDeposited;
                    usedBonus = dif * -1;
                }
                else
                {
                    usedReal = usedMoney;
                    usedBonus = 0;
                }
                int realLeft = realMoneyDeposited - usedReal;
                int bonusLeft = bonusMoneyDeposited - usedBonus;
                response.DepositBonus = bonusMoneyDeposited;
                response.DepositReal = realMoneyDeposited;
                response.LeftBonus = bonusLeft;
                response.LeftReal = realLeft;
                response.UsedBonus = usedBonus;
                response.UsedReal = usedReal;
            }
            else
            {
                response.SupplierName = "Supplier not found";
            }
        }

        #endregion
    }
}
