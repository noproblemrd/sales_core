﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Reports.Response;
using NoProblem.Core.DataModel.Reports.Request;

namespace NoProblem.Core.BusinessLogic.Reports
{
    class DistributaionInstallationsByIpReportCreator
    {
        DataAccessLayer.Generic dal;

        public DistributaionInstallationsByIpReportCreator()
        {
            dal = new DataAccessLayer.Generic();
        }

        public List<DistributionsInstallationsByIpRow> Create(DistributionsInstallationsByIpRowRequest request)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@originId", request.OriginId);
            parameters.Add("@from", request.From);
            parameters.Add("@to", request.To);
            var reader = dal.ExecuteReader(query, parameters);
            List<DistributionsInstallationsByIpRow> retVal = new List<DistributionsInstallationsByIpRow>();
            foreach (var row in reader)
            {
                DistributionsInstallationsByIpRow IpRow = new DistributionsInstallationsByIpRow();
                IpRow.Ip = (string)row["new_ip"];
                IpRow.UtcTime = (DateTime)row["CreatedOn"];
                IpRow.EasternStandardTime = (DateTime)row["EasternTime"];
                retVal.Add(IpRow);
            }
            return retVal;
        }
        /*
        private const string query =
            @"
select new_ip, count(*) count
from new_addonaction with(nolock)
where new_isinstallation = 1
and new_originid = @originId
group by new_ip
";
         * */
        private const string query = @"
DECLARE @timeZoneCode int, @from_et datetime, @to_et datetime
SELECT @timeZoneCode = new_value from new_configurationsetting where new_key = 'PublisherTimeZone'
DECLARE @diff int = DATEDIFF(hour, @from, dbo.fn_UTCToLocalTimeByTimeZoneCode(@from, @timeZoneCode))

SET @from_et = DATEADD(hour, @diff, @from)
SET @to_et = DATEADD(hour, @diff, @to)

SELECT a.new_ip, ab.CreatedOn, DATEADD(hour, @diff, ab.CreatedOn) EasternTime
FROM dbo.New_addonactionExtensionBase a with(nolock)
	INNER JOIN dbo.New_addonactionBase ab with(nolock) on a.New_addonactionId = ab.New_addonactionId
WHERE ab.CreatedOn >= @from_et
	and ab.CreatedOn < @to_et
	and a.new_isinstallation = 1
	and a.new_originid = @originId
ORDER BY ab.CreatedOn";
    }
}
