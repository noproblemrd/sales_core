﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class RootUrlExposuresReportCreator : XrmUserBase
    {
        #region Ctor
        public RootUrlExposuresReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Private Fields

        #endregion

        #region Internal Methods

        internal DataModel.Reports.RootUrlExposuresReportResponse Create(RootUrlExposuresReportRequest request)
        {
            DataModel.Reports.RootUrlExposuresReportResponse response = new NoProblem.Core.DataModel.Reports.RootUrlExposuresReportResponse();
            DataAccessLayer.Exposure dal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            if (request.OriginId != Guid.Empty)
            {
                parameters.Add(new KeyValuePair<string, object>("@originId", request.OriginId));
            }
            parameters.Add(new KeyValuePair<string, object>("@from", request.FromDate.Date));
            parameters.Add(new KeyValuePair<string, object>("@to", request.ToDate.Date.AddDays(1)));
            parameters.Add(new KeyValuePair<string, object>("@PageNum", request.PageNumber));
            parameters.Add(new KeyValuePair<string, object>("@PageSize", request.PageSize));

            string query =
    @"WITH PagerTbl AS
                (                
                SELECT ROW_NUMBER() OVER(ORDER BY count(*) desc) AS RowNum, 
case when  CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1) > 0 then
SUBSTRING(ex.new_url, 0, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1)) 
else ex.New_Url end as rootUrl, COUNT(*) as count
,
(
select COUNT(*) from 
Incident with(nolock) where DeletionStateCode = 0 and 
case when CHARINDEX('/', new_url, CHARINDEX('/', new_url, CHARINDEX('/', new_url)+1)+1) > 0 then
SUBSTRING(new_url, 0, CHARINDEX('/', new_url, CHARINDEX('/', new_url, CHARINDEX('/', new_url)+1)+1)) 
else New_Url end
=  case when  CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1) > 0 then
SUBSTRING(ex.new_url, 0, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1)) 
else ex.New_Url end
and CreatedOn between @from and @to
";
            if (Guid.Empty != request.OriginId)
            {
                query += " and new_originid = @originId ";
            }
            query += @" ) as countInc
from
New_exposure ex with(nolock)
where ex.DeletionStateCode = 0
and ex.CreatedOn between @from and @to
";
            if (Guid.Empty != request.OriginId)
            {
                query += " and ex.new_originid = @originId ";
            }
            if (!string.IsNullOrEmpty(request.FreeText))
            {
                query += @" and case when CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1) > 0 then
SUBSTRING(ex.new_url, 0, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1)) 
else ex.New_Url end like '%' + @freeText + '%' ";
                parameters.Add(new KeyValuePair<string, object>("@freeText", request.FreeText));
            }
            query += @" group by case when  CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1) > 0 then
SUBSTRING(ex.new_url, 0, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1)) 
else ex.New_Url end ";
            if (request.RootUrlExposuresCountType.HasValue && request.ExposuresCount > 0)
            {
                if (request.RootUrlExposuresCountType.Value == RootUrlExposuresReportRequest.eRootUrlExposuresCount.GreaterThan)
                {
                    query += @"having COUNT(*) > @expoCount";
                }
                else
                {
                    query += @"having COUNT(*) < @expoCount";
                }
                parameters.Add(new KeyValuePair<string, object>("@expoCount", request.ExposuresCount));
            }
            query += @"
 )
                    SELECT * , (select max(RowNum) from PagerTbl) as totalRows
                      FROM PagerTbl
                     WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
                                      AND @PageNum * @PageSize)
                            or @PageSize = -1)
                     ORDER BY RowNum
";

            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                DataModel.Reports.RootUrlExposuresReportRow data = new NoProblem.Core.DataModel.Reports.RootUrlExposuresReportRow();
                data.UrlRoot = (string)row["rootUrl"];
                data.Exposures = (int)row["count"];
                data.Requests = (int)row["countInc"];
                data.CTR = data.Exposures == 0 ? 0 : (double)data.Requests / (double)data.Exposures * 100;
                response.Rows.Add(data);
                if (response.TotalRows == 0)
                {
                    response.TotalRows = unchecked((int)((long)row["totalRows"]));
                }
            }
            response.CurrentPage = request.PageNumber;
            response.TotalPages = (int)Math.Ceiling(((double)response.TotalRows) / ((double)request.PageSize));
            return response;
        }

        internal RootUrlExposuresReportResponse KeywordDrillDown(DateTime fromDate, DateTime toDate, string rootUrl, Guid originId, int pageSize, int pageNumber)
        {
            RootUrlExposuresReportResponse response = new RootUrlExposuresReportResponse();
            DataAccessLayer.Exposure dal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            if (originId != Guid.Empty)
            {
                parameters.Add(new KeyValuePair<string, object>("@originId", originId));
            }
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate.Date));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate.Date.AddDays(1)));
            parameters.Add(new KeyValuePair<string, object>("@PageNum", pageNumber));
            parameters.Add(new KeyValuePair<string, object>("@PageSize", pageSize));
            parameters.Add(new KeyValuePair<string, object>("@rootUrl", rootUrl));
            string query =
                @"WITH PagerTbl AS
                (                
                SELECT ROW_NUMBER() OVER(ORDER BY count(*) desc) AS RowNum, 
new_keyword
, COUNT(*) as count
,
(
select COUNT(*) from 
Incident with(nolock) where DeletionStateCode = 0 and 
case when CHARINDEX('/', new_url, CHARINDEX('/', new_url, CHARINDEX('/', new_url)+1)+1) > 0 then
SUBSTRING(new_url, 0, CHARINDEX('/', new_url, CHARINDEX('/', new_url, CHARINDEX('/', new_url)+1)+1)) 
else New_Url end
= @rootUrl
and new_keyword = ex.new_keyword
and new_keyword is not null
and CreatedOn between @from and @to
";
            if (originId != Guid.Empty)
            {
                query += " and new_originid = @originId ";
            }
            query += @"
) as countInc
from
New_exposure ex with(nolock)
where ex.DeletionStateCode = 0
and ex.CreatedOn between @from and @to
";
            if (originId != Guid.Empty)
            {
                query += @" and ex.new_originid = @originId ";
            }
            query += @" and 
case when  CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1) > 0 then
SUBSTRING(ex.new_url, 0, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1)) 
else ex.New_Url end = @rootUrl
and new_keyword is not null
group by new_keyword
 )
                    SELECT * , (select max(RowNum) from PagerTbl) as totalRows
                      FROM PagerTbl
                     WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
                                      AND @PageNum * @PageSize)
                            or @PageSize = -1)
                     ORDER BY RowNum";

            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                DataModel.Reports.RootUrlExposuresReportRow data = new NoProblem.Core.DataModel.Reports.RootUrlExposuresReportRow();
                data.UrlRoot = (string)row["new_keyword"];
                data.Exposures = (int)row["count"];
                data.Requests = (int)row["countInc"];
                data.CTR = data.Exposures == 0 ? 0 : (double)data.Requests / (double)data.Exposures * 100;
                response.Rows.Add(data);
                if (response.TotalRows == 0)
                {
                    response.TotalRows = unchecked((int)((long)row["totalRows"]));
                }
            }
            response.CurrentPage = pageNumber;
            response.TotalPages = (int)Math.Ceiling(((double)response.TotalRows) / ((double)pageSize));

            return response;
        }

        internal RootUrlExposuresReportResponse FullUrlDrillDown(DateTime fromDate, DateTime toDate, string rootUrl, Guid originId, string keyword, int pageSize, int pageNumber)
        {
            RootUrlExposuresReportResponse response = new RootUrlExposuresReportResponse();
            DataAccessLayer.Exposure dal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            if (originId != Guid.Empty)
            {
                parameters.Add(new KeyValuePair<string, object>("@originId", originId));
            }
            parameters.Add(new KeyValuePair<string, object>("@from", fromDate.Date));
            parameters.Add(new KeyValuePair<string, object>("@to", toDate.Date.AddDays(1)));
            parameters.Add(new KeyValuePair<string, object>("@PageNum", pageNumber));
            parameters.Add(new KeyValuePair<string, object>("@PageSize", pageSize));
            parameters.Add(new KeyValuePair<string, object>("@rootUrl", rootUrl));
            parameters.Add(new KeyValuePair<string, object>("@keyword", keyword));
            string query =
            @"WITH PagerTbl AS
                (                
                SELECT ROW_NUMBER() OVER(ORDER BY count(*) desc) AS RowNum, 
new_url
, COUNT(*) as count
,
(
select COUNT(*) from 
Incident with(nolock) where DeletionStateCode = 0 and 
case when CHARINDEX('/', new_url, CHARINDEX('/', new_url, CHARINDEX('/', new_url)+1)+1) > 0 then
SUBSTRING(new_url, 0, CHARINDEX('/', new_url, CHARINDEX('/', new_url, CHARINDEX('/', new_url)+1)+1)) 
else New_Url end
= @rootUrl
and new_keyword = @keyword
and CreatedOn between @from and @to
";
            if (originId != Guid.Empty)
            {
                query += " and new_originid = @originId ";
            }
            query += @" ) as countInc
from
New_exposure ex with(nolock)
where ex.DeletionStateCode = 0
and ex.CreatedOn between @from and @to
";
            if (originId != Guid.Empty)
            {
                query += " and ex.new_originid = @originId ";
            }
            query += @" and 
case when  CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1) > 0 then
SUBSTRING(ex.new_url, 0, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url, CHARINDEX('/', ex.new_url)+1)+1)) 
else ex.New_Url end = @rootUrl
and new_keyword = @keyword
group by new_url
 )
                    SELECT * , (select max(RowNum) from PagerTbl) as totalRows
                      FROM PagerTbl
                     WHERE ((RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 
                                      AND @PageNum * @PageSize)
                            or @PageSize = -1)
                     ORDER BY RowNum";

            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                DataModel.Reports.RootUrlExposuresReportRow data = new NoProblem.Core.DataModel.Reports.RootUrlExposuresReportRow();
                data.UrlRoot = (string)row["new_url"];
                data.Exposures = (int)row["count"];
                data.Requests = (int)row["countInc"];
                data.CTR = data.Exposures == 0 ? 0 : (double)data.Requests / (double)data.Exposures * 100;
                response.Rows.Add(data);
                if (response.TotalRows == 0)
                {
                    response.TotalRows = unchecked((int)((long)row["totalRows"]));
                }
            }
            response.CurrentPage = pageNumber;
            response.TotalPages = (int)Math.Ceiling(((double)response.TotalRows) / ((double)pageSize));

            return response;
        }

        #endregion
    }
}
