﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class OptOutReportsCreator : XrmUserBase
    {
        public OptOutReportsCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            dal = new NoProblem.Core.DataAccessLayer.WidgetRemovalLog(XrmDataContext);
        }

        DataAccessLayer.WidgetRemovalLog dal;

        public List<DataModel.Reports.OptOutAdvancedReportRow> CreateAdvancedReport(DateTime fromDate, DateTime toDate, Guid originId)
        {
            List<DataModel.Reports.OptOutAdvancedReportRow> retVal = new List<NoProblem.Core.DataModel.Reports.OptOutAdvancedReportRow>();
            DateTime fromDateFixed = fromDate;
            DateTime toDateFixed = toDate;
            FixDatesToFullDays(ref fromDateFixed, ref toDateFixed);
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@from", fromDateFixed));
            parameters.Add(new KeyValuePair<string, object>("@to", toDateFixed));
            StringBuilder query = new StringBuilder(
                @"
create table #leadTmp (leadCount int, keyword nvarchar(100), controlname nvarchar(100))
create table #optOutTmp (optOutCount int, keyword nvarchar(100), controlname nvarchar(100))

insert into #leadTmp (leadCount, keyword, controlname)
select 
COUNT(*) as leadCount, New_Keyword, New_ControlName
from
Incident inc with(nolock) ");
            if (originId != Guid.Empty)
            {
                query.Append(@" join new_origin org with(nolock) on org.new_originid = inc.new_originid ");
            }
            query.Append(@" where
inc.DeletionStateCode = 0
and inc.createdon between @from and @to ");
            if (originId != Guid.Empty)
            {
                query.Append(" and (inc.new_originid = @originId or org.new_parentoriginid = @originId) ");
                parameters.Add(new KeyValuePair<string, object>("@originId", originId));
            }
            query.Append(
@" group by New_Keyword, New_ControlName

insert into #optOutTmp (optOutCount, keyword, controlname)
select 
COUNT(*) as optOutCount, new_keyword, New_ControlName
from new_widgetremovallog wrl with(nolock) ");
            if (originId != Guid.Empty)
            {
                query.Append(" join new_origin org with(nolock) on org.new_originid = wrl.new_originid ");
            }
            query.Append(@" where
wrl.deletionstatecode = 0
and wrl.createdon between @from and @to ");
            if (originId != Guid.Empty)
            {
                query.Append(" and (wrl.new_originid = @originId or org.new_parentoriginid = @originId) ");
            }
            query.Append(
            @" group by New_Keyword, New_ControlName

select 
case when #leadTmp.keyword IS null then #optOutTmp.keyword else #leadTmp.keyword end as keyword,
case when #leadTmp.controlname IS null then #optOutTmp.controlname else #leadTmp.controlname end as flavor,
isnull(#leadTmp.leadCount, 0) as leadCount,
ISNULL(#optOutTmp.optOutCount, 0) as optOutCount
from #leadTmp
full join #optOutTmp on #leadTmp.keyword = #optOutTmp.keyword and #leadTmp.controlname = #optOutTmp.controlname
order by leadCount desc

drop table #optOutTmp
drop table #leadTmp");

            var reader = dal.ExecuteReader(query.ToString(), parameters);
            foreach (var row in reader)
            {
                DataModel.Reports.OptOutAdvancedReportRow data = new NoProblem.Core.DataModel.Reports.OptOutAdvancedReportRow();
                data.Flavor = Convert.ToString(row["flavor"]);
                data.Keyword = Convert.ToString(row["keyword"]);
                data.LeadCount = (int)row["leadCount"];
                data.OptOutCount = (int)row["optOutCount"];
                retVal.Add(data);
            }
            return retVal;
        }

        public List<DataModel.Reports.StringIntPair> CreateRegularReport(DateTime fromDate, DateTime toDate, Guid originId)
        {
            DateTime fromDateFixed = fromDate;
            DateTime toDateFixed = toDate;
            FixDatesToUtcFullDays(ref fromDateFixed, ref toDateFixed);
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@from", fromDateFixed));
            parameters.Add(new KeyValuePair<string, object>("@to", toDateFixed));
            StringBuilder query = new StringBuilder(
                @"select 
                COUNT(*) as optOutCount, new_removeperiod
                from new_widgetremovallog with(nolock) ");
            if (originId != Guid.Empty)
            {
                query.Append(@" join new_origin org with(nolock) on org.new_originid = new_widgetremovallog.new_originid ");
            }
            query.Append(@" where
                new_widgetremovallog.deletionstatecode = 0 
                and new_widgetremovallog.createdon between @from and @to ");
            if (originId != Guid.Empty)
            {
                query.Append(" and (new_widgetremovallog.new_originid = @originId or org.new_parentoriginid = @originId) ");
                parameters.Add(new KeyValuePair<string, object>("@originId", originId));
            }
            query.Append(
                " group by new_removeperiod "
                            );
            List<DataModel.Reports.StringIntPair> retVal = new List<NoProblem.Core.DataModel.Reports.StringIntPair>();
            var reader = dal.ExecuteReader(query.ToString(), parameters);
            foreach (var row in reader)
            {
                DataModel.Reports.StringIntPair data = new NoProblem.Core.DataModel.Reports.StringIntPair();
                data.Str = Convert.ToString(row["new_removeperiod"]);
                data.Int = (int)row["optOutCount"];
                retVal.Add(data);
            }
            return retVal;
        }
    }
}
