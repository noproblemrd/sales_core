﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Reports.CallsReport;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class CallsReportCreator : XrmUserBase
    {
        #region Ctor
        internal CallsReportCreator(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal DML.Reports.Response.Calls.CallsReportResponse CreateNoGroupBy(DateTime fromDate, DateTime toDate, Guid expertiseId, CallType callType, Guid userId, Guid supplierOriginId, int pageSize, int pageNumber)
        {
            IEnumerable<DML.Reports.CallData> calls = GetCallsByParamsFiltersNoGrouping(ref fromDate, ref toDate, expertiseId, callType, userId, supplierOriginId);
            DML.Reports.Response.Calls.CallsReportResponse response = new NoProblem.Core.DataModel.Reports.Response.Calls.CallsReportResponse();
            calls = DoPaging(pageSize, pageNumber, calls, response);
            foreach (var c in calls)
            {
                c.Date = FixDateToLocal(c.Date);
            }
            response.Calls = calls.ToList();
            return response;
        }

        internal DML.Reports.Response.Calls.CallsReportRevenuePerDateResponse CreateRevenuePerDate(DateTime fromDate, DateTime toDate, Guid expertiseId, CallType callType, Guid supplierOriginId, int pageSize, int pageNumber)
        {
            List<DML.Reports.CallData> calls = GetCallsByParamsFilters(ref fromDate, ref toDate, expertiseId, callType, Guid.Empty, supplierOriginId);
            List<DML.Reports.CallDataRevenuePerDate> dataList = new List<NoProblem.Core.DataModel.Reports.CallDataRevenuePerDate>();
            foreach (var call in calls)
            {
                DateTime callDate = call.Date.Date;
                var existing = dataList.FirstOrDefault(x => x.Date == callDate);
                if (existing != null)
                {
                    existing.Revenue += call.Price;
                }
                else
                {
                    DML.Reports.CallDataRevenuePerDate data = new NoProblem.Core.DataModel.Reports.CallDataRevenuePerDate();
                    data.Date = callDate;
                    data.Revenue = call.Price;
                    dataList.Add(data);
                }
            }
            var orderedList = dataList.OrderByDescending(x => x.Date);
            DML.Reports.Response.Calls.CallsReportRevenuePerDateResponse response = new NoProblem.Core.DataModel.Reports.Response.Calls.CallsReportRevenuePerDateResponse();
            var pagesList = DoPaging(pageSize, pageNumber, orderedList, response);
            response.Calls = pagesList.ToList();
            response.Total = dataList.Sum(x => x.Revenue);
            return response;
        }

        internal DML.Reports.Response.Calls.CallsReportRevenuePerHeadingResponse CreateRevenuePerHeading(DateTime fromDate, DateTime toDate, Guid expertiseId, CallType callType, Guid supplierOriginId, int pageSize, int pageNumber)
        {
            List<DML.Reports.CallData> calls = GetCallsByParamsFilters(ref fromDate, ref toDate, expertiseId, callType, Guid.Empty, supplierOriginId);
            List<DML.Reports.CallDataRevenuePerHeading> dataList = new List<NoProblem.Core.DataModel.Reports.CallDataRevenuePerHeading>();
            foreach (var call in calls)
            {
                string expertiseName = call.Expertise;
                var existing = dataList.FirstOrDefault(x => x.Expertise == expertiseName);
                if (existing != null)
                {
                    existing.Revenue += call.Price;
                }
                else
                {
                    DML.Reports.CallDataRevenuePerHeading data = new NoProblem.Core.DataModel.Reports.CallDataRevenuePerHeading();
                    data.Expertise = expertiseName;
                    data.Revenue = call.Price;
                    dataList.Add(data);
                }
            }
            var orderedList = dataList.OrderBy(x => x.Expertise);
            DML.Reports.Response.Calls.CallsReportRevenuePerHeadingResponse response = new NoProblem.Core.DataModel.Reports.Response.Calls.CallsReportRevenuePerHeadingResponse();
            var pagesList = DoPaging(pageSize, pageNumber, orderedList, response);
            response.Calls = pagesList.ToList();
            response.Total = dataList.Sum(x => x.Revenue);
            return response;
        }

        internal DML.Reports.Response.Calls.CallsReportCountPerDateResponse CreateCountPerDate(DateTime fromDate, DateTime toDate, Guid expertiseId, CallType callType, Guid supplierOriginId, int pageSize, int pageNumber)
        {
            List<DML.Reports.CallData> calls = GetCallsByParamsFilters(ref fromDate, ref toDate, expertiseId, callType, Guid.Empty, supplierOriginId);
            List<DML.Reports.CallDataCountPerDate> dataList = new List<NoProblem.Core.DataModel.Reports.CallDataCountPerDate>();
            foreach (var call in calls)
            {
                DateTime callDate = call.Date.Date;
                var existing = dataList.FirstOrDefault(x => x.Date == callDate);
                if (existing != null)
                {
                    existing.Count++;
                }
                else
                {
                    DML.Reports.CallDataCountPerDate data = new NoProblem.Core.DataModel.Reports.CallDataCountPerDate();
                    data.Date = callDate;
                    data.Count = 1;
                    dataList.Add(data);
                }
            }
            var orderedList = dataList.OrderByDescending(x => x.Date);
            DML.Reports.Response.Calls.CallsReportCountPerDateResponse response = new NoProblem.Core.DataModel.Reports.Response.Calls.CallsReportCountPerDateResponse();
            var pagedList = DoPaging(pageSize, pageNumber, orderedList, response);
            response.Calls = pagedList.ToList();
            response.Total = dataList.Sum(x => x.Count);
            return response;
        }

        internal DML.Reports.Response.Calls.CallsReportCountPerHeadingResponse CreateCountPerHeading(DateTime fromDate, DateTime toDate, Guid expertiseId, CallType callType, Guid supplierOriginId, int pageSize, int pageNumber)
        {
            List<DML.Reports.CallData> calls = GetCallsByParamsFilters(ref fromDate, ref toDate, expertiseId, callType, Guid.Empty, supplierOriginId);
            List<DML.Reports.CallDataCountPerHeading> dataList = new List<NoProblem.Core.DataModel.Reports.CallDataCountPerHeading>();
            foreach (var call in calls)
            {
                string expertiseName = call.Expertise;
                var existing = dataList.FirstOrDefault(x => x.Expertise == expertiseName);
                if (existing != null)
                {
                    existing.Count++;
                }
                else
                {
                    DML.Reports.CallDataCountPerHeading data = new NoProblem.Core.DataModel.Reports.CallDataCountPerHeading();
                    data.Expertise = expertiseName;
                    data.Count = 1;
                    dataList.Add(data);
                }
            }
            var orderedList = dataList.OrderBy(x => x.Expertise);
            DML.Reports.Response.Calls.CallsReportCountPerHeadingResponse response = new NoProblem.Core.DataModel.Reports.Response.Calls.CallsReportCountPerHeadingResponse();
            var pagedList = DoPaging(pageSize, pageNumber, orderedList, response);
            response.Calls = pagedList.ToList();
            response.Total = dataList.Sum(x => x.Count);
            return response;
        }

        #endregion

        #region Private Methods

        private static IEnumerable<T> DoPaging<T>(int pageSize, int pageNumber, IEnumerable<T> calls, DML.PagingResponse response) where T : DataModel.Reports.CallDataContainers.CallDataBase
        {
            response.TotalRows = calls.Count();
            response.CurrentPage = 1;
            response.TotalPages = 1;
            if (pageSize != -1)
            {
                calls = calls.Skip((pageNumber - 1) * pageSize).Take(pageSize);
                response.CurrentPage = pageNumber;
                response.TotalPages = (int)Math.Ceiling(((double)response.TotalRows) / ((double)pageSize));
            }
            return calls;
        }

        private List<DML.Reports.CallData> GetCallsByParamsFilters(ref DateTime fromDate, ref DateTime toDate, Guid expertiseId, CallType callType, Guid userId, Guid supplierOriginId)
        {
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            DataAccessLayer.IncidentAccount inciAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            List<DML.Reports.CallData> calls = inciAccDal.GetIncidentAccountsForCallsReport(fromDate, toDate, expertiseId, callType, userId, supplierOriginId);
            foreach (var call in calls)
            {
                call.Date = FixDateToLocal(call.Date);
            }
            return calls;
        }

        private List<DML.Reports.CallData> GetCallsByParamsFiltersNoGrouping(ref DateTime fromDate, ref DateTime toDate, Guid expertiseId, CallType callType, Guid userId, Guid supplierOriginId)
        {
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            DataAccessLayer.IncidentAccount inciAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            List<DML.Reports.CallData> calls = inciAccDal.GetIncidentAccountsForCallsReport(fromDate, toDate, expertiseId, callType, userId, supplierOriginId);
            //foreach (var call in calls) taken care of this in the sql
            //{
            //    call.Date = FixDateToLocal(call.Date);
            //}
            return calls;
        }

        private decimal GetSuppliersDefaultPrice(Guid expertiseId, Guid supplierId, DataAccessLayer.AccountExpertise accountExpertiseDal)
        {
            DML.Xrm.new_accountexpertise accExp = accountExpertiseDal.GetByAccountAndPrimaryExpertise(supplierId, expertiseId);
            if (accExp != null)
            {
                return accExp.new_incidentprice.HasValue ? accExp.new_incidentprice.Value : 0;
            }
            return 0;
        }

        #endregion
    }
}
