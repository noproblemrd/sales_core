﻿using System;
using System.Collections.Generic;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Reports
{
    internal class DistributorBillingReportCreator : XrmUserBase
    {
        private const string cachePrefix = "DistributorBillingReportCreatorCache";

        public DistributorBillingReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<DistributorsReportSummaryRow> CreateSummaryDistributorsBillingReport(int year, int month)
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<DistributorsReportSummaryRow>>(cachePrefix, year + ";" + month, CreateSummaryDistributorsBillingReportMethod);
        }

        private List<DistributorsReportSummaryRow> CreateSummaryDistributorsBillingReportMethod(string cacheKey)
        {
            List<DistributorsReportSummaryRow> retVal = new List<DistributorsReportSummaryRow>();
            string[] split = cacheKey.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            int year = int.Parse(split[0]);
            int month = int.Parse(split[1]);
            DataAccessLayer.Incident.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.incident>.SqlParametersList();
            DateTime date = new DateTime(year, month, 1);
            parameters.Add("@date", date);
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var reader = dal.ExecuteReader(queryForDistributorsSummary, parameters);
            foreach (var row in reader)
            {
                DistributorsReportSummaryRow data = new DistributorsReportSummaryRow();
                data.OriginName = Convert.ToString(row["New_name"]);
                data.PaymentAmount = (decimal)row["cost"];
                data.RequestCount = (int)row["requestCount"];
                data.RevenueShare = (row["New_RevenueShare"] == DBNull.Value) ? 0m : (decimal)row["New_RevenueShare"];
                retVal.Add(data);
            }
            return retVal;
        }

        public List<DistributorBillingReportIndividualRow> CreateIndividualDistributorBillingReport(Guid originId, int year, int month)
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<DistributorBillingReportIndividualRow>>(cachePrefix, originId + ";" + year + ";" + month, CreateIndividualDistributorBillingReportMethod);
        }

        private List<DistributorBillingReportIndividualRow> CreateIndividualDistributorBillingReportMethod(string cacheKey)
        {
            List<DistributorBillingReportIndividualRow> retVal = new List<DistributorBillingReportIndividualRow>();
            string[] split = cacheKey.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            Guid originId = new Guid(split[0]);
            int year = int.Parse(split[1]);
            int month = int.Parse(split[2]);
            DataAccessLayer.Incident.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.incident>.SqlParametersList();
            parameters.Add("@originId", originId);
            DateTime date = new DateTime(year, month, 1);
            parameters.Add("@date", date);
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var reader = dal.ExecuteReader(queryForIndividualDistributor, parameters);
            foreach (var row in reader)
            {
                DistributorBillingReportIndividualRow data = new DistributorBillingReportIndividualRow();
                data.Consumer = Convert.ToString(row["customer"]);
                data.Date = (DateTime)row["new_createdonlocal"];
                data.Heading = Convert.ToString(row["new_primaryexpertiseidname"]);
                data.Payment = row["new_istopay"] != DBNull.Value ? (bool)row["new_istopay"] : true;
                data.PaymentReason = Convert.ToString(row["payReasonName"]);
                data.Region = Convert.ToString(row["new_regionidname"]);
                retVal.Add(data);
            }
            return retVal;
        }

        private const string queryForDistributorsSummary =
            @"
select sum(cnt) requestCount
, isnull(SUM((cnt * ISNULL(org.New_CostPerRequest, 0)) + ((revenue * 0.885 * 0.95) * isnull(org.New_RevenueShare, 0) / 100)), 0) cost
, org.New_name
,org.New_RevenueShare 
from
(
select
COUNT(inc.incidentid) cnt
,DATEADD(month, datediff(month, 0, inc.new_createdonlocal), 0) month
,inc.new_originid
,sum(tmptbl.revenue) revenue
from incident inc with(nolock) 
left join new_affiliatepaystatus ps with(nolock) on ps.new_affiliatepaystatusid = inc.new_affiliatepaystatusid
left join new_affiliatepaystatusreason sr with(nolock) on sr.new_affiliatepaystatusreasonid = inc.new_affiliatepaystatusreasonid
left join 
(
select SUM(new_potentialincidentprice) revenue, new_incidentid from 
New_incidentaccount
where DATEADD(month, datediff(month, 0, New_incidentaccount.new_createdonlocal), 0) = @date
and New_tocharge = 1
group by new_incidentid
) tmptbl on tmptbl.new_incidentid = inc.IncidentId
where 
ISNULL(inc.new_isgoldennumbercall, 0) = 0 
and inc.deletionstatecode = 0 
and inc.statuscode != 200016 
and inc.statuscode != 200013 
and inc.statuscode != 200014
and isnull(inc.new_showaffiliate, 1)= 1
and isnull(ps.New_IsToPay, 1) = 1

and DATEADD(month, datediff(month, 0, inc.new_createdonlocal), 0) = @date

group by inc.new_originid, DATEADD(month, datediff(month, 0, inc.new_createdonlocal), 0)
) innerTbl
join new_origin org on org.New_originId = innerTbl.new_originid
group by org.New_originId, org.New_name, month, org.New_RevenueShare
order by org.New_name
";

        private const string queryForIndividualDistributor =
            @"
select
(isnull(inc.customeridname, '') + ': ' + inc.new_telephone1) as customer
,inc.new_primaryexpertiseidname 
,inc.new_regionidname 
,inc.createdon
,inc.new_createdonlocal
,inc.new_originid
,ps.new_istopay
,sr.new_name as payReasonName
from incident inc with(nolock)               
left join new_affiliatepaystatus ps with(nolock) on ps.new_affiliatepaystatusid = inc.new_affiliatepaystatusid
left join new_affiliatepaystatusreason sr with(nolock) on sr.new_affiliatepaystatusreasonid = inc.new_affiliatepaystatusreasonid
where 
ISNULL(inc.new_isgoldennumbercall, 0) = 0 
and inc.deletionstatecode = 0 
and inc.statuscode != 200016 
and inc.statuscode != 200013 
and inc.statuscode != 200014                
and DATEADD(month, datediff(month, 0, inc.new_createdonlocal), 0) = @date        
and isnull(inc.new_showaffiliate, 1) = 1
and new_originid = @originId
order by CreatedOn desc
";
    }
}
