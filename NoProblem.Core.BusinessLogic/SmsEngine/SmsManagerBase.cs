﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.SmsEngine
{
    public abstract class SmsManagerBase  
    {
        //SMS_PHONE_NUMBER_CAMPAIGN
        readonly static string FROM_PHONE;// = "8888660885";
    //    const string TABLE_ENTITY = "dbo.sf_SmsEntity";
        static SmsManagerBase()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
        //    MOBILE_BID_TIMEOUT = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_BID_TIMEOUT));
            FROM_PHONE = configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.SMS_PHONE_NUMBER_CAMPAIGN);           
        }

        string CsvPath;
        int CampaignId;
        int BulkId;
        string Message;
        protected virtual int GetCampaignId
        {
            get { return CampaignId; }
        }
        protected virtual int GetBulkId
        {
            get { return BulkId; }
        }
        public SmsManagerBase(string CsvPath, int CampaignId)
        {
            this.CsvPath = CsvPath;
            this.CampaignId = CampaignId;
        }
        public SmsManagerBase(int BulkId)
        {
            this.BulkId = BulkId;
            string command = @"
SELECT C.Id CampaignId,  B.FileName
FROM dbo.sf_Campaign C WITH(NOLOCK)
	INNER JOIN dbo.sf_Bulk B WITH(NOLOCK) on C.Id = B.CampaignId
WHERE B.Id = @BulkId";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@BulkId", BulkId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if(reader.Read())
                    {
                        this.CsvPath = (string)reader["FileName"];
                        this.CampaignId = (int)reader["CampaignId"];
                    }
                    conn.Close();
                }
            }
        }
        public virtual eSmsStatus Execute()
        {
            
            CampaignManager cm = new CampaignManager();
            CampaignData cd = cm.GetCampaignDetails(CampaignId);
            if (cd == null)
                return eSmsStatus.CAMPAIGN_NOT_FOUND;
            this.Message = cd.text;
            if (!File.Exists(CsvPath))
                return eSmsStatus.FILE_NOT_FOUND;
            ThreadPool.QueueUserWorkItem(_execute);
            return eSmsStatus.IN_PROCESS;
            
        }
        private void _execute(object state)
        {
            string CommandCreateBulk = @"
INSERT INTO dbo.sf_Bulk (CampaignId, [FileName])
VALUES(@CampaignId, @FileName)	
SELECT SCOPE_IDENTITY()";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(CommandCreateBulk, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@CampaignId", CampaignId);
                    cmd.Parameters.AddWithValue("@FileName", CsvPath);
                    BulkId = Decimal.ToInt32((decimal)cmd.ExecuteScalar());
                    conn.Close();
                    cmd.Dispose();
                }
                conn.Dispose();
            }

            using (TextFieldParser parser = new TextFieldParser(CsvPath))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();
                    foreach (string field in fields)
                    {
                        if (!CheckPhoneNumber(field))
                            continue;
                        InsertToDB(field);
                    }
                }
            }

            Exec(null);
            
        }
        public virtual eSmsStatus ReExecuteTask()
        {

            CampaignManager cm = new CampaignManager();
            CampaignData cd = cm.GetCampaignDetails(CampaignId);
            if (cd == null)
                return eSmsStatus.CAMPAIGN_NOT_FOUND;
            this.Message = cd.text;           
            
            ThreadPool.QueueUserWorkItem(Exec);
            return eSmsStatus.IN_PROCESS;

        }
        private void Exec(object state)
        {
            List<string> PhoneList;
            string GetPhonesCommand = "EXEC [dbo].[sf_GetPhoneToSend] @BulkId, @CampaignId";
            bool IsFinish = false;
            while (!IsFinish)
            {
                PhoneList = new List<string>(10001);
                try
                {
                    using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand(GetPhonesCommand, conn))
                        {
                            cmd.Parameters.AddWithValue("@BulkId", BulkId);
                            cmd.Parameters.AddWithValue("@CampaignId", CampaignId);
                            SqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                PhoneList.Add((string)reader["Phone"]);
                            }
                            cmd.Dispose();
                        }
                        conn.Close();
                    }
                }
                catch (Exception exc)
                {
                    Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in procedure sf_GetPhoneToSend BulkId:{0}, CampaignId:{1}",
                        BulkId, CampaignId));
                }
                if (PhoneList.Count == 0)
                    IsFinish = true;
                foreach (string phone in PhoneList)
                {
                    if (SendSms(phone))
                        UpdateSentSms(phone);
                    else
                        UpdateFailedToSend(phone);
                }
                System.GC.Collect();
            }
        }

        private void UpdateFailedToSend(string phone)
        {
            string command = "EXEC [dbo].[sf_UpdateFaildSentSms] @phone, @BulkId, @CampaignId";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@phone", phone);
                        cmd.Parameters.AddWithValue("@BulkId", BulkId);
                        cmd.Parameters.AddWithValue("@CampaignId", CampaignId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in procedure sf_UpdateFaildSentSms phone:{0}, BulkId:{1}, CampaignId:{2}",
                    phone, BulkId, CampaignId));
            }
        }
        abstract protected void InsertToDB(string phone);
        protected virtual void UpdateSentSms(string phone)
        {
            string command = "EXEC [dbo].[sf_UpdateSentSms] @phone, @BulkId, @CampaignId";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@phone", phone);
                        cmd.Parameters.AddWithValue("@BulkId", BulkId);
                        cmd.Parameters.AddWithValue("@CampaignId", CampaignId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in procedure sf_UpdateSentSms phone:{0}, BulkId:{1}, CampaignId:{2}",
                    phone, BulkId, CampaignId));
            }
        }
        protected virtual bool SendSms(string phone)
        {
            Notifications noti = new Notifications(null);
#if DEBUG
            if ("ISRAEL" == GlobalConfigurations.DefaultCountry && !phone.StartsWith("0"))
                return false;
#endif
            return noti.SendSMSTemplate(this.Message, phone, GlobalConfigurations.DefaultCountry, FROM_PHONE);
        }
        protected bool CheckPhoneNumber(string phone)
        {
            Regex reg = new Regex("^[0-9]{10}$");
            return reg.IsMatch(phone);
        }
        public enum eSmsStatus
        {
            IN_PROCESS,
            FILE_NOT_FOUND,
            CAMPAIGN_NOT_FOUND,
            SERVER_GENERAL_EXCEPTION
        }
        public class SmsManagerResponse
        {
            public eSmsStatus status { get; set; }
        }
       
        
    }
    public enum eSmsCampaign
    {
        REGULAR = 1,
        IVR_1800 = 2
    }
}
