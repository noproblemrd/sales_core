﻿using NoProblem.Core.DataModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SmsEngine
{
    public class SmsIvrPhoneCampaign : Runnable
    {
#region static functions
        static Dictionary<string, string> PhoneLinkDictionary;
        static SmsIvrPhoneCampaign()
        {
            LoadePhoneLinkDictionary();
        }
        static void LoadePhoneLinkDictionary()
        {
            PhoneLinkDictionary = new Dictionary<string, string>();
            string command = "SELECT PhoneNumber, Link FROM dbo.sf_IvrCampaignPhoneLink";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            PhoneLinkDictionary.Add((string)reader["PhoneNumber"], (string)reader["Link"]);
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in LoadePhoneLinkDictionary SmsIvrPhoneCampaign"));
            }
        }
        public static void UpdatePhoneLinkDictionary()
        {
            LoadePhoneLinkDictionary();
        }
#endregion
        string fromPhone;
        string toPhone;
        public SmsIvrPhoneCampaign(string from, string to)
        {
            this.fromPhone = SmsUtility.GetCleanPhoneFromTwillio(from);
            this.toPhone = SmsUtility.GetCleanPhoneFromTwillio(to);
        }
        public string Execute()
        {
            
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            response.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + "smscampaign" + AudioFilesConsts.SLASH + "ClipCallVoicePromtpsSandi2" + AudioFilesConsts.MP3_FILE_EXTENSION);
            response.Pause(2);
            response.Hangup();
            this.Start();
            return response.ToString();
        }
        protected override void Run()
        {
            if (!SmsUtility.CheckPhoneNumber(fromPhone) || !SmsUtility.CheckPhoneNumber(toPhone))
                return;
            //int campaignId = -1, BulkId = -1, 
            int messageId = -1;
            bool DoNotSend = true;
            string message = null;
            bool _isSuccess = true;
            
            string command = "EXEC [dbo].[sf_Insert1800Sms] @fromPhone, @toPhone, @CampaignType";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@fromPhone", fromPhone);
                        cmd.Parameters.AddWithValue("@toPhone", toPhone);
                        cmd.Parameters.AddWithValue("@CampaignType", (int)(eSmsCampaign.IVR_1800));
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                  //          campaignId = (int)reader["CampaignId"];//, @BulkId BulkId, @DoNotSend DoNotSend	"]
                  //          BulkId = (int)reader["BulkId"];
                            DoNotSend = (bool)reader["DoNotSend"];
                            message = (string)reader["Message"];
                            messageId = (int)reader["MessageId"];
                        }
                        else
                            _isSuccess = false;
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in SmsIvrPhoneCampaign procedure sf_Insert1800Sms fromPhone:{0}, toPhone:{1}", fromPhone, toPhone));
                return;
            }
            if (!_isSuccess || DoNotSend)
                return;
    //        Effect.Crm.Logs.LogUtils.MyHandle.WriteToLog(8, string.Format("before send fromPhone:{0}, toPhone{1}", fromPhone, toPhone));
            message = string.Format(message, PhoneLinkDictionary[toPhone]);
            if (SendSms(message))
                UpdateSentSms(messageId);
            else
                UpdateFailedToSend(messageId);
            
        }
        protected bool SendSms(string message)
        {
            Notifications noti = new Notifications(null);
            /*
#if DEBUG
            if ("ISRAEL" == GlobalConfigurations.DefaultCountry && !fromPhone.StartsWith("0"))
                return false;
            if (toPhone.StartsWith("0"))
                toPhone = "2" + toPhone.Substring(1, toPhone.Length - 1);
#endif
             * 
             * */
            return noti.SendSMSTemplate(message, fromPhone, GlobalConfigurations.DefaultCountry, toPhone);
        }
        private void UpdateFailedToSend(int messageId)
        {
            string command = "EXEC [dbo].[sf_UpdateFaildSentSmsByMessageId] @MessageId";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@MessageId", messageId);                       
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in SmsIvrPhoneCampaign procedure sf_UpdateFaildSentSms phone:{0}, MessageId:{1}",
                    fromPhone, messageId));
            }
        }
        protected void UpdateSentSms(int messageId)
        {
            string command = "EXEC [dbo].[sf_UpdateSentSmsByMessageId] @MessageId";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@MessageId", messageId);                        
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in SmsIvrPhoneCampaign procedure sf_UpdateSentSms phone:{0}, MessageId:{1}",
                    fromPhone, messageId));
            }
        }
    }
}
