﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.SmsEngine
{
    public class CampaignManager
    {
        const string TO_REMOVE = " To remove reply remove";
        public enum eCampaignCreateResponse
        {
            SUCCESS,
            INVALID_TEXT,
            INVALID_NAME
        }
        public eCampaignCreateResponse CreateCampaign(CampaignData cd)
        {
          //  System.Threading.Thread.Sleep(300000);
         //   return eCampaignCreateResponse.SUCCESS;
            string _message = cd.text + TO_REMOVE;
            if (string.IsNullOrEmpty(_message) || _message.Length > 159)
                return eCampaignCreateResponse.INVALID_TEXT;
            if(string.IsNullOrEmpty(cd.name))
                return eCampaignCreateResponse.INVALID_NAME;
            string command = "insert into dbo.sf_Campaign (Text, Name) VALUES (@text, @name)";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand(command,conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@text", _message);
                    cmd.Parameters.AddWithValue("@name", cd.name);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            return eCampaignCreateResponse.SUCCESS;
        }
        public CampaignData GetCampaignDetails(int CampaignId)
        {
            string command = "SELECT Text, Name FROM dbo.sf_Campaign WHERE Id = @CampaignId";
            CampaignData cd = null;
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@CampaignId", CampaignId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if(reader.Read())
                    {
                        cd = new CampaignData();
                        cd.name = (string)reader["Name"];
                        cd.text = (string)reader["Text"];
                    }
                    conn.Close();
                }
            }
            return cd;
        }
        public List<CampaignTaskData> GetCampaignTaskDetails()
        {
            string command = @"
SELECT TOP 400 C.Name Campaign, B.CreatedOn, B.Id
FROM dbo.sf_Campaign C WITH(NOLOCK)
	INNER JOIN dbo.sf_Bulk B WITH(NOLOCK) on C.Id = B.CampaignId
ORDER BY B.CreatedOn desc";
            List<CampaignTaskData> list = new List<CampaignTaskData>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        CampaignTaskData cd = new CampaignTaskData();
                        cd.campaign = (string)reader["Campaign"];
                        cd.date = (DateTime)reader["CreatedOn"];
                        cd.Id = (int)reader["Id"];
                        list.Add(cd);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        public List<CampaignData> GetCampaignDetails()
        {
            string command = "SELECT Id, Text, Name FROM dbo.sf_Campaign";
            List<CampaignData> list = new List<CampaignData>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        CampaignData cd = new CampaignData();
                        cd.name = (string)reader["Name"];
                        cd.text = (string)reader["Text"];
                        cd.Id = (int)reader["Id"];
                        list.Add(cd);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        public void DeleteCampaign(int CampaignId)
        {
            string command = "DELETE FROM dbo.sf_Campaign WHERE Id = @CampaignId";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@CampaignId", CampaignId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
    }
    public class CampaignData
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string text { get; set; }
    }
    public class CampaignTaskData
    {
        public int Id { get; set; }
        public string campaign { get; set; }
        public DateTime date { get; set; }
    }
}