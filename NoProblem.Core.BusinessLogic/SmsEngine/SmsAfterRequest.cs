﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.SmsEngine
{
    public class SmsAfterRequest : Runnable
    {
        const string SMS_MESSAGE_SLIDER = "Following your online request for {0}, please install the ClipCall app to describe your need: {1}";
        const string SMS_MESSAGE_DIRECTNUMBER = "Following your online request, please install the ClipCall app to describe your need: {0}";
        const string SLIDER_LINK = "lets.clipcall.it/6f0b86e4";
        const string DIRECTNUMBER_LINK = "lets.clipcall.it/36e45f9a";
        readonly static string FROM_PHONE;
        static SmsAfterRequest()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            FROM_PHONE = configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.SMS_PHONE_NUMBER_CAMPAIGN);           
        }

        string phone;
        string category;
        Guid incidentId;
        bool isDirectNumber;
        public SmsAfterRequest(Guid incidentId)
        {
            this.incidentId = incidentId;
            isDirectNumber = true;
        }
        public SmsAfterRequest(string phone, string category, Guid incidentId)
        {
            this.phone = phone;
            this.category = category;
            this.incidentId = incidentId;
            isDirectNumber = false;
        }
        public void SendSms()
        {
            string command = "SELECT [dbo].[fn_sf_IsBlockNumber](@phone)";
            bool isBlockNumber = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@phone", phone);
                        isBlockNumber = (bool)cmd.ExecuteScalar();
                        conn.Close();
                        cmd.Dispose();
                    }
                    conn.Dispose();
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in SmsAfterRequest procedure fn_sf_IsBlockNumber phone:{0}", phone));
            }
            if (isBlockNumber)
                return;
            bool isSent = Send();
            command = @"EXEC [dbo].[sf_InsertRequestSMS] @phone, @incidentId, @IsSent";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@phone", phone);
                        cmd.Parameters.AddWithValue("@incidentId", incidentId);
                        cmd.Parameters.AddWithValue("@IsSent", isSent);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in SmsAfterRequest procedure sf_InsertRequestSMS phone:{0}", phone));
            }
        }
        private bool Send()
        {
            Notifications noti = new Notifications(null);
#if DEBUG
            if ("ISRAEL" == GlobalConfigurations.DefaultCountry && !phone.StartsWith("0"))
                return false;
#endif
            return noti.SendSMSTemplate(GetMessage, phone, GlobalConfigurations.DefaultCountry, FROM_PHONE);
        }
        string GetMessage
        {
            get
            {
                if (this.isDirectNumber)
                    return string.Format(SMS_MESSAGE_DIRECTNUMBER, DIRECTNUMBER_LINK);
                else
                    return string.Format(SMS_MESSAGE_SLIDER, category, SLIDER_LINK);
            }
        }
        
        protected override void Run()
        {
            DataAccessLayer.Incident incidentDAL = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            NoProblem.Core.DataModel.Xrm.incident _incident = incidentDAL.Retrieve(this.incidentId);
            this.phone = _incident.new_telephone1;
            this.category = _incident.new_new_primaryexpertise_incident.new_name;
            SendSms();
        }
         
    }
}
