﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.SmsEngine
{
    public class SmsManagerConsumers : SmsManagerBase
    {
        public SmsManagerConsumers(string CsvPath, int CampaignId)
            : base(CsvPath, CampaignId)
        {

        }
        public SmsManagerConsumers(int BulkId)
            : base(BulkId)
        {

        }
       
        protected override void InsertToDB(string phone)
        {
            string command = "EXEC [dbo].[sf_InsertSMS] @phone, @CampaignId, @BulkId";
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@phone", phone);
                    cmd.Parameters.AddWithValue("@CampaignId", GetCampaignId);
                    cmd.Parameters.AddWithValue("@BulkId", GetBulkId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
        public static List<NoProblem.Core.DataModel.ClipCall.SmsCampaignReport.SmsCampaignReportResponse> 
            SmsCampaignReport(NoProblem.Core.DataModel.ClipCall.SmsCampaignReport.SmsCampaignReportRequest request)
        {
            List<NoProblem.Core.DataModel.ClipCall.SmsCampaignReport.SmsCampaignReportResponse> list = new List<DataModel.ClipCall.SmsCampaignReport.SmsCampaignReportResponse>();
            string command = "EXEC [dbo].[sf_CampaignReport] @from, @to";
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@from", request.GetFrom);
                    cmd.Parameters.AddWithValue("@to", request.GetTo);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        NoProblem.Core.DataModel.ClipCall.SmsCampaignReport.SmsCampaignReportResponse row =
                            new DataModel.ClipCall.SmsCampaignReport.SmsCampaignReportResponse();
                        row.bulkId = (int)reader["BulkId"];
                        row.campaign = (string)reader["Campaign"];
                        row.date = (DateTime)reader["CreatedOn"];
                        row.didntTryToSent = (int)reader["DidntTryToSent"];
                        row.sent = (int)reader["Sent"];
                        row.TotalPhonesAttempts = (int)reader["TotalPhones"];
                        list.Add(row);
                    }
                    conn.Close();
                }
            }
            return list;
        }
    }
}
/*
T.CreatedOn
	,T.BulkId
	,T.Campaign
	,ISNULL(T.count, 0) TotalPhones
	,ISNULL(S.count, 0) 'Sent'
	,ISNULL(F.count, 0) DidntTryToSent*/