﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NoProblem.Core.BusinessLogic.SmsEngine
{
    public class SmsUtility
    {
       
        public static void GetSmsCallBack(string FromPhone, string message)
        {

            string phone = GetCleanPhoneFromTwillio(FromPhone);
            //     Guid YelpSupplierId = Guid.Empty;
            //     LogUtils.MyHandle.WriteToLog("GetSmsCallBack From: {0}, Fix Phone: {1}, Message: {2}", FromPhone,phone, message);
            if (!string.IsNullOrEmpty(phone))
            {
                string command = "EXEC [dbo].[sf_SetDoNotCallMeBySMS] @phone, @OriginalPhone, @SetDoNotCallMe, @Message";
                using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@phone", phone);
                        cmd.Parameters.AddWithValue("@OriginalPhone", FromPhone);
                        cmd.Parameters.AddWithValue("@SetDoNotCallMe", (message.ToLower().Contains("remove")));
                        cmd.Parameters.AddWithValue("@Message", message);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            //  InsertCallbackSMS(FromPhone, message, YelpSupplierId);
        }
        public static string GetCleanPhoneFromTwillio(string phone)
        {
            if (string.IsNullOrEmpty(phone))
                return null;
            //+17076907398
           
            string result = string.Empty;
            foreach (char c in phone)
            {
                int i = (int)c;
                if (i > 47 && i < 58)
                    result += c;
            }
            if (result.Length > 10)
                result = result.Substring(result.Length - 10, 10);

            return result;
        }
        public static bool CheckPhoneNumber(string phone)
        {
            Regex reg = new Regex("^[0-9]{10}$");
            return reg.IsMatch(phone);
        }
    }
}
