﻿using System.Linq;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.ClipCall.Customers
{
    public class CustomerManager
    {

        private readonly DataAccessLayer.Contact contactRepository;

        public CustomerManager(DataAccessLayer.Contact contactRepository)
        {
            this.contactRepository = contactRepository;
        }

        public virtual void CreateCustomer(string phoneNumber)
        {
            contactRepository.CreateAnonymousContact(phoneNumber, null, null, null, false, null, null, null, null, null, null, false);            
        }

        public virtual contact FindCustomerByPhoneNumber(string phoneNumber)
        {
            contact customer = contactRepository.All.SingleOrDefault(x => x.telephone1 == phoneNumber);
            return customer;
        }
    }
}
