﻿using NoProblem.Core.DataModel.ClipCall.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.Xrm;
using NoProblem.Core.BusinessLogic.ServiceRequest;
using NoProblem.Core.DataModel.ClipCall.FunnelSummary;

namespace NoProblem.Core.BusinessLogic.ClipCall
{
    public class RequestReportManager
    {
        public List<eRequestsStatus> GetRequestStatuses()
        {
            List<eRequestsStatus> list = new List<eRequestsStatus>();
            foreach(eRequestsStatus status in Enum.GetValues(typeof(eRequestsStatus)))
            {
                list.Add(status);
            }
            return list;
        }
        public List<RequestReportResponse> RequestReport(RequestReportRequest request)
        {
            string command = @"
SELECT TOP 500 inc.incidentid ,inc.ticketnumber ,con.ContactId ,con.FullName ContactFullName, inc_eb.new_telephone1 ,pexp.new_name ExpertiseName
		,reg.New_englishname RegionName,inc.createdon, inc_eb.new_createdonlocal, inc_eb.New_relaunchDate		
		,ISNULL(inc_eb.New_ordered_providers, 0) AdvertisersProvided
		,case when isnull(new_requiredaccountno, 0) = 0 then 1 else new_requiredaccountno end as RequestedAdvertisers
		,(select sum(new_potentialincidentprice) 
			from dbo.New_incidentaccountBase iab with(nolock)
			inner join dbo.New_incidentaccountExtensionBase iaeb with(nolock) on iab.New_incidentaccountId = iaeb.New_incidentaccountId
			where new_tocharge = 1 and deletionstatecode = 0 and new_incidentid = inc.incidentid) as Revenue	
		,inc.statuscode
		,COALESCE(inc_eb.New_reviewStatus, 1) ReviewStatus	
        ,(case when inc.CaseTypeCode = @videoChat then 'VIDEO_CHAT'
			when inc.statuscode in(200014,1,200002) then 'CLOSE'--BADWORD,WORK_DONE,NO_AVAILABLE_SUPPLIER);
			when inc.statuscode in(200022,200000,2) then 'LIVE'--NEW,WAITING,AFTER_AUCTION
			when inc.statuscode in(200023) then 'REVIEW'--IN_REVIEW
            when inc.StatusCode in (200024) then 'WAITING_FOR_PHONE_VALIDATION'
			end) CallStatus		
        ,inc_eb.New_videoUrl videoUrl
        ,inc.CaseTypeCode
         ,pexp.new_istestqa
from dbo.IncidentBase inc with(nolock)
		inner join dbo.IncidentExtensionBase inc_eb  with(nolock) on inc.IncidentId = inc_eb.IncidentId
		inner join dbo.ContactBase con with(nolock) on inc.ContactId = con.ContactId
		inner join dbo.New_primaryexpertiseExtensionBase pexp with(nolock) on inc_eb.new_primaryexpertiseid = pexp.New_primaryexpertiseId
		left join  dbo.New_regionExtensionBase reg with(nolock) on inc_eb.new_regionid = reg.New_regionId
		
where inc.deletionstatecode = 0 and inc.CaseTypeCode in(@videoType,@videoChat)
    {WHERE}
ORDER BY inc_eb.New_relaunchDate desc
option (recompile)";
            
            List<RequestReportResponse> listResponse = new List<RequestReportResponse>();
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        StringBuilder sb_query = new StringBuilder(command);
                        StringBuilder sb_where = new StringBuilder();
                        if (!string.IsNullOrEmpty(request.CaseNumber))
                        {
                            sb_where.Append(" and inc.TicketNumber = @caseNumber ");
                            cmd.Parameters.AddWithValue("@caseNumber", request.CaseNumber);
                        }
                        else
                        {
                            sb_where.Append(" and inc_eb.New_relaunchDate between @from and @to ");
                            cmd.Parameters.AddWithValue("@from", request.GetFrom);
                            cmd.Parameters.AddWithValue("@to", request.GetTo);
                            switch(request.requestStatus)
                            {
                                
                                case(eRequestsStatus.CLOSE):
                                    sb_where.Append(" and inc.statuscode in(@statusCode1, @statusCode2, @statusCode3) ");
                                    cmd.Parameters.AddWithValue("@statusCode1", (int)incident.Status.NO_AVAILABLE_SUPPLIER);
                                    cmd.Parameters.AddWithValue("@statusCode2", (int)incident.Status.WORK_DONE);
                                    cmd.Parameters.AddWithValue("@statusCode3", (int)incident.Status.BADWORD);
                                    break;
                                case (eRequestsStatus.REVIEW):
                                    /*
                                    sb_where.Append(" and inc.statuscode = @statusCode ");
                                    cmd.Parameters.AddWithValue("@statusCode", (int)incident.Status.IN_REVIEW);                                    
                                     */
                                    sb_where.Append(" and inc_eb.New_reviewStatus = @reviewStatus ");
                                    cmd.Parameters.AddWithValue("@reviewStatus", (int)incident.IncidentReviewStatus.WaitnigForReview);
                                    break;
                                case(eRequestsStatus.LIVE):
                                    sb_where.Append(" and inc.statuscode in(@statusCode1, @statusCode2, @statusCode3) ");
                                    cmd.Parameters.AddWithValue("@statusCode1", (int)incident.Status.AFTER_AUCTION);
                                    cmd.Parameters.AddWithValue("@statusCode2", (int)incident.Status.NEW);
                                    cmd.Parameters.AddWithValue("@statusCode3", (int)incident.Status.WAITING);                                   
                                    break;                    
                                case(eRequestsStatus.WAITING_FOR_PHONE_VALIDATION):
                                    sb_where.Append(" and inc.statuscode in(@statusCode) ");
                                    cmd.Parameters.AddWithValue("@statusCode", (int)incident.Status.WATING_FOR_PHONE_VALIDATION);
                                    break;
                            }
                            if(request.category != Guid.Empty)
                            {
                                sb_where.Append(" and pexp.New_primaryexpertiseId = @category ");
                                cmd.Parameters.AddWithValue("@category", request.category);
                            }
                            if(request.numberProvidedAdvertisers.HasValue)
                            {
                                sb_where.Append(" and ISNULL(inc_eb.New_ordered_providers, 0) = @numberProvidedAdvertisers ");
                                cmd.Parameters.AddWithValue("@numberProvidedAdvertisers", request.numberProvidedAdvertisers.Value);
                            }
                            if(request.aarInitiated.HasValue)
                            {
                                sb_where.Append(" and ISNULL(inc_eb.New_UsedAAR, 0) = @UsedAAR ");
                                cmd.Parameters.AddWithValue("@UsedAAR", request.aarInitiated.Value);
                            }
                        }
                        sb_query.Replace("{WHERE}", sb_where.ToString());
                        cmd.Parameters.AddWithValue("@videoType", (int)incident.CaseTypeCode.Video);
                        cmd.Parameters.AddWithValue("@videoChat", (int)incident.CaseTypeCode.VideoChat);
                        cmd.Connection = conn;
                        cmd.CommandText = sb_query.ToString();
                        conn.Open();
                        
                        SqlDataReader reader = cmd.ExecuteReader();
                        while(reader.Read())
                        {
                            RequestReportResponse rrr = new RequestReportResponse();
                            rrr.incidentId = (Guid)reader["incidentid"];
                            rrr.caseNumber = (string)reader["ticketnumber"];
                            rrr.contactId = (Guid)reader["ContactId"];
                            rrr.contactName = reader["ContactFullName"] == DBNull.Value ? string.Empty : (string)reader["ContactFullName"];
                            rrr.contactPhone = reader["new_telephone1"] == DBNull.Value ? string.Empty : (string)reader["new_telephone1"];
                            rrr.category = (string)reader["ExpertiseName"];
                            rrr.region = (string)reader["RegionName"];
                            rrr.date = (DateTime)reader["New_relaunchDate"];
                            rrr.advertisersProvided = (int)reader["AdvertisersProvided"];
                            rrr.requestedAdvertisers = (int)reader["RequestedAdvertisers"];
                            if (reader["Revenue"] != DBNull.Value)
                                rrr.revenue = (decimal)reader["Revenue"];
                            rrr.statuscode = (int)reader["statuscode"];
                            rrr.reviewStatus = (incident.IncidentReviewStatus)((int)reader["ReviewStatus"]);
                            rrr.caseStatus = (eRequestsStatus)(Enum.Parse(typeof(eRequestsStatus), (string)reader["CallStatus"]));
                            rrr.videoUrl = reader["videoUrl"] == DBNull.Value ? string.Empty : (string)reader["videoUrl"];
                            rrr.caseType = (incident.CaseTypeCode)((int)reader["CaseTypeCode"]);
                            rrr.isTestQa = reader["new_istestqa"] == DBNull.Value ? false : (bool)reader["new_istestqa"];
                            listResponse.Add(rrr);
                        }
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception in ClipCall RequestReport SQL");
            }
            return listResponse;

        }
        public ReviewRejectConfirmResponse RejectConfirmReviewIncident(ReviewRejectConfirmRequest request)
        {
            ServiceRequestManager srm = new ServiceRequestManager(null);
            return srm.ConfirmRejectIncidentReview(request.incidentId, request.toConfirm);
        }
        public List<FunnelSummaryResponse> FunnelSummaryReport(FunnelSummaryRequest request)
        {
            string command = "EXEC [dbo].[AarFunnelSummaryReport] @from, @to";
            List<FunnelSummaryResponse> list = new List<FunnelSummaryResponse>();
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        cmd.CommandTimeout = 90;
                        conn.Open();
                        cmd.Parameters.AddWithValue("@from", request.GetFrom);
                        cmd.Parameters.AddWithValue("@to", request.GetTo);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while(reader.Read())
                        {
                            FunnelSummaryResponse fsr = new FunnelSummaryResponse();
                            fsr.incidentId = (Guid)reader["IncidentId"];
                            fsr.date = (DateTime)reader["CreatedOn"];
                            fsr.caseNumber = (string)reader["CaseNumber"];
                            fsr.category = (string)reader["Category"];
                            fsr.connection = (int)reader["Connections"];
                            fsr.LP_Visits = (int)reader["Visits"];
                            fsr.region = (string)reader["Region"];
                            fsr.SMS_Sent = (int)reader["SentSms"];
                            fsr.videoDuration = (int)reader["VideoDuration"];
                            fsr.videoLink = (string)reader["VideoLink"];
                            fsr.videoViews = (int)reader["WatchVideo"];
                            fsr.isQaTest = reader["IsQa"] == DBNull.Value ? false:(bool)reader["IsQa"];
                            fsr.P1 = (int)reader["P1"];
                            fsr.P2 = (int)reader["P2"];
                            fsr.P3 = (int)reader["P3"];
                            fsr.P4 = (int)reader["P4"];
                            fsr.P5 = (int)reader["P5"];
                            list.Add(fsr);
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception in FunnelSummaryReport sql");
            }
            return list;
        }
        public AddAarSupplierToLeadResponse AddAarSupplierToLead(AddAarSupplierToLeadRequest request)
        {
            NoProblem.Core.BusinessLogic.AAR.AarManager am = new AAR.AarManager(null);
            return am.AddAarSupplierToLead(request.incidentId, request.phoneNumber, request.name);
        }
    }
}
/*
CreatedOn,
	CaseNumber,
	Category,
	VideoLink,
	VideoDuration,
	Region,
	SUM(WatchVideo) WatchVideo,
	SUM(Visits) Visits,
	COUNT(*) SentSms,
	SUM(Connection) Connections,
	(SELECT COUNT(T2.[Priority])
	FROM @table T2
	WHERE T2.[Priority] = 'P5' and T2.CaseNumber = T1.CaseNumber) P5,
	(SELECT COUNT(T2.[Priority])
	FROM @table T2
	WHERE T2.[Priority] = 'P4' and T2.CaseNumber = T1.CaseNumber) P4,
	(SELECT COUNT(T2.[Priority])
	FROM @table T2
	WHERE T2.[Priority] = 'P3' and T2.CaseNumber = T1.CaseNumber) P3,
	(SELECT COUNT(T2.[Priority])
	FROM @table T2
	WHERE T2.[Priority] = 'P2' and T2.CaseNumber = T1.CaseNumber) P2,
	(SELECT COUNT(T2.[Priority])
	FROM @table T2
	WHERE T2.[Priority] = 'P1' and T2.CaseNumber = T1.CaseNumber) P1
*/