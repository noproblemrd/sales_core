﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.ClipCall
{
    public class CreateMobileSystemNotification
    {        
        public string bodyMessage { get; private set; }
        public string titleMessage { get; private set; }
        
        public CreateMobileSystemNotification() { }
        
        public CreateMobileSystemNotification(string bodyMessage, string titleMessage)
        {
            this.bodyMessage = bodyMessage;
            this.titleMessage = titleMessage;
        }
        public void SendAsyncNotification()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
            {
                CreateMobileSystemNotification _this = (CreateMobileSystemNotification)state;
                _this.SendNotifications();
            }), this);
        }
        private void SendNotifications()
        {

            //        LogUtils.MyHandle.WriteToLog("Closing account {0} because of drop in acceptance rate. accountId = {1}", account.name, accountId);
            Notifications notifier = new Notifications(null);
            notifier.SendEmail("support@clipcall.it", bodyMessage, titleMessage);

            Slack.SlackNotifier slack = new Slack.SlackNotifier(bodyMessage);
            slack.Start();

        }
        
        public void SendAarRequestListEmailNotification(string FilePath)
        {
            bodyMessage = string.Empty;
            titleMessage = "AAR list for new case";
            Notifications notifier = new Notifications(null);
            if(!notifier.SendEmail(new List<string>() { "support@clipcall.it" }, bodyMessage, titleMessage, new List<string>() { FilePath }))
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(new Exception("SendAarRequestListEmailNotification"), "Exception sending email. FilePath={0}, bodyMessage={1}, titleMessage={2}",
                    FilePath);
        }
        
        public void SendNewVideoLeadNotification(string CategoryName, string RegionName, string VideoUrl, string consumerPhone)
        {
            const string _bodyMessage = "New video lead on {0} in {1}, VideoUrl: {2}, Consumer phone: {3}";
            const string _titleMessage = "New video lead on {0}";
            this.bodyMessage = String.Format(_bodyMessage, CategoryName, RegionName, VideoUrl, consumerPhone);
            this.titleMessage = String.Format(_titleMessage, CategoryName);
            SendAsyncNotification();
        }
        public void SendInstallationApp(string phone)
        {
            const string _bodyMessage = "New mobile user: phone number: {0}";
       //     const string _titleMessage = "New mobile user: phone number: {0}";
            this.bodyMessage = String.Format(_bodyMessage, phone);
            this.titleMessage = String.Format(_bodyMessage, phone);
            SendAsyncNotification();
        }
        public void SendAdvertiserRegistration(string phone, string name)
        {
            const string _bodyMessage = "New mobile app advertiser registration: name:{0}, phone:{1}";
            const string _titleMessage = "New advertiser registration - mobile app";
            this.bodyMessage = String.Format(_bodyMessage, name, phone);
            this.titleMessage = _titleMessage;
            SendAsyncNotification();
        }

        
    }
}
