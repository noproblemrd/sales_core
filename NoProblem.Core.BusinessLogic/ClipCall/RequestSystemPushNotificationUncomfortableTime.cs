﻿using NoProblem.Core.BusinessLogic.PushNotifications;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ClipCall
{

    public class RequestSystemPushNotificationUncomfortableTime : RequestSytemCustomerPushNotification
    {
        /*
        const string messageWeekend = "Videos submitted during the weekend might not get a quick respons. Be patient, and if no pro will contact you in the next 24 hours, send the video again.";
        const string messageNight = "Video requests from 10pm to 8am might not get immidiate respond. Be patient, and if no one will contact you in the next 24 hours you should resend you request again during work hours.";
         * */
        const string messageWeekend = "Videos submitted during the weekend might not get a fast response. Plz be patient, and consider resending the video Monday morning.";
        const string messageNight = "Your video was sent during non-working hours. You might not get a quick respons. Be patient, or try resending the video during a working day.";
        const string titleMessage = "Your ClipCall video";
        const ePushTypes PUSH_TYPE = ePushTypes.REQUEST_UNCONFORTABLE_TIME;

        static List<Guid> emergencyCategories;
        static RequestSystemPushNotificationUncomfortableTime()
        {
            emergencyCategories = new List<Guid>();
            string command = @"
SELECT New_primaryexpertiseId
FROM dbo.new_primaryexpertise
WHERE DeletionStateCode = 0 and statecode = 0 and New_isEmergencyCategory = 1";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            emergencyCategories.Add((Guid)reader["New_primaryexpertiseId"]);
                        }
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception initializer RequestSystemPushNotificationUncomfortableTime");
            }
        }

        Guid regionId;
        Guid expertiseId;
        public RequestSystemPushNotificationUncomfortableTime(Guid regionId, Guid customerId, Guid expertiseId) : base(customerId, true)
        {
            this.regionId = regionId;
            this.title = titleMessage;
            this.expertiseId = expertiseId;
            this.pushType = PUSH_TYPE;
        }

        protected override bool BeforeSendNotification()
        {
            if (this.regionId == Guid.Empty)
                return false;

            if (emergencyCategories.Contains(expertiseId))
                return false;

            DateTime dt = NoProblem.Core.BusinessLogic.Utils.ConvertUtils.GetLocalFromUtcByZipcode(DateTime.Now, this.regionId);
            if (IsWeekend(dt))
            {
                this.message = messageWeekend;
                return true;
            }
            else if (IsNigth(dt))
            {
                this.message = messageNight;
                return true;
            }
            return false;
            //    SendNotification(messageNight, titleMessage, pushType);
        }
        private bool IsWeekend(DateTime dt)
        {
            return (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday);
        }
        private bool IsNigth(DateTime dt)
        {
            //between 0 and 23.
            return (dt.Hour >= 19 || dt.Hour < 8);
        }
        
        
    }
}
