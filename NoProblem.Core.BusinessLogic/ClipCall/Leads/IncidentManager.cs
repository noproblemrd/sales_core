﻿using NoProblem.Core.DataModel.ClipCall.Request;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ClipCall.Leads
{
    delegate IncidentData AsyncClipCallIncidentData();
    delegate List<IncidentAccountData> AsyncCallData();
    delegate List<IncidentData.RelaunchStopData> AsyncRelaunchStopData();
    delegate List<AarIncidentData> AsycAarIncidentData();
    
    public class IncidentManager
    {
        Guid incidentId;
        public IncidentManager(Guid incidentId)
        {
            this.incidentId = incidentId;
        }
        public IncidentData GetIncidentData()
        {
            AsyncClipCallIncidentData callerGeneral = new AsyncClipCallIncidentData(GetGeneralIncidentData);
            AsyncCallData callerIncidentAccount = new AsyncCallData(GetIncidentAccountData);
            AsyncRelaunchStopData callerRelaunchStopData = new AsyncRelaunchStopData(GetRelaunchStopData);
            AsycAarIncidentData callerAarIncidentData = new AsycAarIncidentData(GetAarIncidentData); 
            IAsyncResult resultGeneral = callerGeneral.BeginInvoke(null, null);
            IAsyncResult resultIncidentAccount = callerIncidentAccount.BeginInvoke(null, null);
            IAsyncResult resultRelaunchStopData = callerRelaunchStopData.BeginInvoke(null, null);
            IAsyncResult resultAarIncidentData = callerAarIncidentData.BeginInvoke(null, null);
            IncidentData incidentData = callerGeneral.EndInvoke(resultGeneral);
            List<IncidentAccountData> listIncidentAccountData = callerIncidentAccount.EndInvoke(resultIncidentAccount);
            List<IncidentData.RelaunchStopData> listRelaunchStopData = callerRelaunchStopData.EndInvoke(resultRelaunchStopData);
            List<AarIncidentData> listAarIncidentData = callerAarIncidentData.EndInvoke(resultAarIncidentData);
            if (incidentData == null)
                return null;
            incidentData.Calls = listIncidentAccountData;
            incidentData.RelaunchStopDataList = listRelaunchStopData;
            incidentData.AarCalls = listAarIncidentData;
            incidentData.SetValues();
            return incidentData;
        }
        private IncidentData GetGeneralIncidentData()
        {
            IncidentData incidentData = null;
            string command = @"
SELECT i.ticketnumber, i.Title, i.New_videoUrl, i.New_ordered_providers,
	  i.New_requiredaccountno, c.New_name CategoryName, c.New_primaryexpertiseId,
	  r.New_englishname, r.New_Level RegionLevel, i.CustomerId, i.New_telephone1 CustomerPhone,
	  i.CreatedOn, i.New_relaunchDate, i.New_reviewStatus, i.statuscode [status],
	  cust.emailaddress1, cust.FullName, i.CaseTypeCode
FROM dbo.incident i WITH(NOLOCK)
	INNER JOIN dbo.New_primaryexpertiseExtensionBase c WITH(NOLOCK) on i.new_primaryexpertiseid = c.New_primaryexpertiseId
	INNER JOIN dbo.New_regionExtensionBase r WITH(NOLOCK) on r.New_regionId = i.new_regionid
	INNER JOIN dbo.Contact cust WITH(NOLOCK) on i.ContactId = cust.ContactId
WHERE i.IncidentId = @incidentId";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", incidentId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if(reader.Read())
                    {
                        incidentData = new IncidentData();
                        incidentData.TicketNumber = (string)reader["ticketnumber"];
                        incidentData.Title = (string)reader["Title"];
                        incidentData.VideoUrl = (reader["New_videoUrl"] == DBNull.Value) ? string.Empty : (string)reader["New_videoUrl"];
                        incidentData.ProvidedAdvertisers = (int)reader["New_ordered_providers"];
                        incidentData.RequestedAdvertisers = reader["New_requiredaccountno"] == DBNull.Value ? 0 : (int)reader["New_requiredaccountno"];
                        incidentData.CategoryId = (Guid)reader["New_primaryexpertiseId"];
                        incidentData.CategoryName = (string)reader["CategoryName"];
                        incidentData.RegionName = (string)reader["New_englishname"];
                        incidentData.RegionLevel = (int)reader["RegionLevel"];
                        incidentData.CustomerId = (Guid)reader["CustomerId"];
                        incidentData.CustomerPhone = (string)reader["CustomerPhone"];
                        incidentData.CreatedOn = (DateTime)reader["CreatedOn"];
                        incidentData.RelaunchDate = (DateTime)reader["New_relaunchDate"];
                        incidentData.ReviewStatus = (DataModel.Xrm.incident.IncidentReviewStatus)((int)reader["New_reviewStatus"]);
                        incidentData.IncidentStatus = (DataModel.Xrm.incident.Status)((int)reader["status"]);
                        incidentData.CustomerEmail = reader["emailaddress1"] == DBNull.Value ? string.Empty : (string)reader["emailaddress1"];
                        incidentData.CustomerName = reader["FullName"] == DBNull.Value ? string.Empty : (string)reader["FullName"];
                        incidentData.caseTypeStatus = (DataModel.Xrm.incident.CaseTypeCode)((int)reader["CaseTypeCode"]);
                    }
                    conn.Close();
                }
            }
            return incidentData;
        }
        private List<AarIncidentData> GetAarIncidentData()
        {
            List<AarIncidentData> list = new List<AarIncidentData>();
            string command = "EXEC [dbo].[GetAArCallByIncidentId] @IncidentId";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", incidentId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        AarIncidentData aid = new AarIncidentData();
                        aid.AdvertiserName = (string)reader["AdveriserName"];
                        aid.AdvertiserPhone = (string)reader["AdvertiserPhone"];
                        aid.CallBackMessage = (string)reader["CallBackMessage"];
                        aid.CallDuration = (int)reader["CallDuration"];
                        aid.CallRecord = (string)reader["CallRecord"];
                        aid.CallStatus = (string)reader["CallStatus"];
                        aid.Date = (DateTime)reader["CreatedOn"];
                        aid.TextMessage = reader["TextMessage"] == DBNull.Value ? string.Empty : (string)reader["TextMessage"];
                        aid.SetUserAgent = (string)reader["UserAgent"];
                        aid.ViewVideoCount = (int)reader["ViewVideoCount"];
                        aid.ViewAllVideo = (bool)reader["ViewAllVideo"];
                        aid.SetValues();
                        list.Add(aid);
                       
                    }
                    conn.Close();
                }
            }
            return list;
        }
        private List<IncidentData.RelaunchStopData> GetRelaunchStopData()
        {
            List<IncidentData.RelaunchStopData> List = new List<IncidentData.RelaunchStopData>();
            string command = @"
SELECT  [Date], IsRelaunch, UserType,
	(SELECT New_englishname
	FROM dbo.new_regionExtensionBase WITH(NOLOCK)
	WHERE new_regionid = NewRegion) NewRegion,
	(SELECT New_englishname
	FROM dbo.new_regionExtensionBase WITH(NOLOCK)
	WHERE new_regionid = OldRegion) OldRegion,
	(SELECT New_name
	FROM dbo.New_primaryexpertiseExtensionBase WITH(NOLOCK)
	WHERE New_primaryexpertiseId = NewCategory) NewCategory,
	(SELECT New_name
	FROM dbo.New_primaryexpertiseExtensionBase WITH(NOLOCK)
	WHERE New_primaryexpertiseId = OldCategory) OldCategory
	
  FROM [dbo].[IncidentRelaunchLog] WITH(NOLOCK)	
  WHERE IncidentId = @incidentId
  ORDER BY Date";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", incidentId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        IncidentData.RelaunchStopData rst = new IncidentData.RelaunchStopData();
                        rst.Date = (DateTime)reader["Date"];
                        rst.IsRelaunch = (bool)reader["IsRelaunch"];
                        rst.By = (IncidentData.eRelaunchStopBy)((int)reader["UserType"]);
                        rst.NewRegion = reader["NewRegion"] == DBNull.Value ? string.Empty : (string)reader["NewRegion"];
                        rst.OldRegion = reader["OldRegion"] == DBNull.Value ? string.Empty : (string)reader["OldRegion"];
                        rst.NewCategory = reader["NewCategory"] == DBNull.Value ? string.Empty : (string)reader["NewCategory"];
                        rst.OldCategory = reader["OldCategory"] == DBNull.Value ? string.Empty : (string)reader["OldCategory"];
                        List.Add(rst);
                    }
                    conn.Close();
                }
            }
            return List;
        }
        private List<IncidentAccountData> GetIncidentAccountData()
        {
            List<IncidentAccountData> list = new List<IncidentAccountData>();
            string command = @"
SELECT acc.Name AccountName, acc.Telephone1, ia.statuscode, ia.New_mobileInConversationDate,
	ia.New_recordfilelocation, ia.New_RecordDuration, acc.New_havevideo
FROM dbo.New_incidentaccount ia WITH(NOLOCK)
	INNER JOIN dbo.Account acc WITH(NOLOCK) on ia.new_accountid = acc.AccountId
WHERE ia.new_incidentid = @incidentId
ORDER BY ia.CreatedOn";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", incidentId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        IncidentAccountData ia = new IncidentAccountData();
                        ia.AdvertiserName = (string)reader["AccountName"];
                        ia.AdvertiserPhone = (string)reader["Telephone1"];
                        ia.Status = (DataModel.Xrm.new_incidentaccount.Status)((int)reader["statuscode"]);
                        ia.Date = reader["New_mobileInConversationDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)reader["New_mobileInConversationDate"];
                        ia.CallRecord = reader["New_recordfilelocation"] == DBNull.Value ? string.Empty : (string)reader["New_recordfilelocation"];
                        ia.CallRecordDuration = reader["New_RecordDuration"] == DBNull.Value ? 0 : (int)reader["New_RecordDuration"];
                        ia.HasMobileApp = reader["New_havevideo"] == DBNull.Value ? false : (bool)reader["New_havevideo"];
                        list.Add(ia);

                    }
                    conn.Close();
                }
            }
            return list;
        }
    }
}
