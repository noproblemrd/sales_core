﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using NoProblem.Core.DataAccessLayer.Bases;
using NoProblem.Core.DataModel.ClipCall;
using NoProblem.Core.DataModel.Refunds;
using NoProblem.Core.DataModel.Xrm;
using account = NoProblem.Core.DataModel.Xrm.account;
using incident = NoProblem.Core.DataModel.Xrm.incident;
using new_incidentaccount = NoProblem.Core.DataModel.Xrm.new_incidentaccount;

namespace NoProblem.Core.BusinessLogic.ClipCall.Leads
{
    public class LeadsManager
    {
        const int REFUND_REASON_FOR_SYSTEM_NOTIFICATION = 11;//Inappropriate content
        private readonly Repository repository;
        private readonly Notifications notifier;

        public LeadsManager(Repository repository, Notifications notifier)
        {
            this.repository = repository;
            this.notifier = notifier;
        }


        public virtual void ReportLead(LeadReport leadReport)
        {
            var incidentaccount = repository.GetSingleByCriteria<new_incidentaccount>(ia => ia.new_incidentaccountid == leadReport.IncidentAccountId);
            if(incidentaccount == null)
                throw new ResourceNotFoundException("IncidentAccount", leadReport.IncidentAccountId);

            account supplierAccount = incidentaccount.new_account_new_incidentaccount;
            if(supplierAccount.Id != leadReport.SupplierId)
                throw new InvalidOperationException("Cannot report this lead. The lead is not related to the provided supplier id." +
                                                    "The supplier id provided " + leadReport.SupplierId + " is not associated with the incident account id: " + leadReport.IncidentAccountId);


            if (incidentaccount.new_refundstatus.HasValue)
                throw new InvalidOperationException(string.Format("Cannot report this lead. lead account with id {0} was already reported", leadReport.IncidentAccountId));
          
            List<new_refundreason> refundReasons = repository.GetByCriteria<new_refundreason>(r => r.new_ismobile.Value && !r.new_disabled.Value);
            new_refundreason refundReason = refundReasons.FirstOrDefault(r => r.new_code == leadReport.ReasonCode);
            if (refundReason == null)
                throw new InvalidOperationException(
                    string.Format("Cannot report this lead. The provided reason code {0} is unknown", leadReport.ReasonCode));

            incidentaccount.new_refundstatus = (int)new_incidentaccount.RefundSatus.Pending; 
            incidentaccount.new_refundreason_incidentaccount = refundReason;
            incidentaccount.new_refundnoteforadvertiser = leadReport.Comment;
            this.repository.Update(incidentaccount);

            //TODO remove somewhere else
            var builder = new StringBuilder();
            builder.AppendLine("Supplier Details: ");
            builder.AppendFormat("name - {0}", supplierAccount.new_fullname);
            builder.AppendFormat("id - {0}", supplierAccount.Id);
            builder.AppendFormat("phone - {0}", supplierAccount.telephone1);
            builder.AppendFormat("report reason title - {0}", refundReason.new_name);
            builder.AppendFormat("report reason code - {0}", refundReason.new_code);
            builder.AppendFormat("request date - {0}", DateTime.UtcNow);
            builder.AppendLine("");
            builder.AppendLine("-------------------------------------------------");
            builder.AppendLine("");
            builder.AppendLine("Lead Details: ");

            incident lead = incidentaccount.new_incident_new_incidentaccount;
            account customerAccount = lead.incident_customer_accounts;
            if (customerAccount != null)
            {
                builder.AppendFormat("customer first name - {0}", customerAccount.new_firstname);
                builder.AppendFormat("customer last name - {0}", customerAccount.new_lastname);
                builder.AppendFormat("customer full name - {0}", customerAccount.new_fullname);
                builder.AppendFormat("customer id - {0}", customerAccount.Id);
            }
           
            builder.AppendFormat("lead create date - {0}", lead.createdon);
            builder.AppendFormat("video url - {0}", lead.new_videourl);
            builder.AppendFormat("video duration - {0}", lead.new_videoduration);
            if (refundReason.new_code == REFUND_REASON_FOR_SYSTEM_NOTIFICATION)
            {
                CreateMobileSystemNotification noti = new CreateMobileSystemNotification(builder.ToString(), 
                    string.Format("customer {0} Reported a lead as 'Inappropriate content'",
                            customerAccount != null ? customerAccount.new_fullname : string.Empty));
                noti.SendAsyncNotification();
            }
            else            
                notifier.SendEmail("support@clipcall.it", builder.ToString(),
                    string.Format("customer {0} Reported a lead",
                        customerAccount != null ? customerAccount.new_fullname : string.Empty));
            
            
            

        }

        public virtual void ClearLeadReport(LeadReport leadReport)
        {
            var incidentaccount = repository.GetSingleByCriteria<new_incidentaccount>(ia => ia.new_incidentaccountid == leadReport.IncidentAccountId);
            if (incidentaccount == null)
                throw new ResourceNotFoundException("IncidentAccount", leadReport.IncidentAccountId);

            account supplierAccount = incidentaccount.new_account_new_incidentaccount;
            if (supplierAccount.Id != leadReport.SupplierId)
                throw new InvalidOperationException(@"Cannot clear lead's report. The lead is not related to the provided supplier id." +
                                                    "The supplier id provided " + leadReport.SupplierId + " is not associated with the incident account id: " + leadReport.IncidentAccountId);


            if (incidentaccount.new_refundreason_incidentaccount == null)
                throw new InvalidOperationException(string.Format(@"Cannot clear this lead's report.lead account with id {0} was never reported", leadReport.IncidentAccountId));


            incidentaccount.new_refundreason_incidentaccount = null;
            this.repository.Update(incidentaccount);
        }

        public virtual List<RefundReasonData> GetLeadReportStatuses()
        {
            List<new_refundreason> refundReasons = this.repository.GetByCriteria<new_refundreason>(x => x.new_ismobile.Value && !x.new_disabled.Value);
            
            return refundReasons.Select(r => new RefundReasonData
            {
                Name = r.new_name,
                Code = r.new_code.Value,
                Guid = r.new_refundreasonid
            }).ToList();
        }
     

    }
}
