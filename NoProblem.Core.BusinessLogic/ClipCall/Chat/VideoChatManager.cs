﻿using System;


using NoProblem.Core.DataAccessLayer.Bases;
using NoProblem.Core.DataModel.ClipCall;
using NoProblem.Core.DataModel.Xrm;
using OpenTokSDK;
using NoProblem.Core.DataAccessLayer;
using NoProblem.Core.DataModel.ClipCall.Request;
using System.Threading;
using Effect.Crm.Logs;
using ClipCall.DomainModel.Entities.VideoInvitation.Response;
using ClipCall.DomainModel.Entities.VideoInvitation.Request;

namespace NoProblem.Core.BusinessLogic.ClipCall.Chat
{
    public class VideoChatManager
    {
        const string VIDEO_CHAT_MESSAGE = "Hi. {0} wants to start a live video chat so they can see what you need done. Just download the ClipCall app to get started: {1} to install.";
        const string VIDEO_MESSAGE = "Hi. {0} wants you to show what you need done by sending a video. Just download the ClipCall app to get started: {1} to install.";

        private static readonly string AMAZON_AWS_BASE_URL;// = @"https://s3.amazonaws.com/clipcall.opentok/";
        static VideoChatManager()
        {
            AMAZON_AWS_BASE_URL = System.Configuration.ConfigurationManager.AppSettings["AmazonAwsBaseUrl"];
        }


        private readonly OpenTok openTokClient;
        private readonly Repository repository;

        public VideoChatManager(OpenTok openTokClient, Repository repository)
        {
            this.openTokClient = openTokClient;
            this.repository = repository;
        }

        //supplier is responsible to create the invitations
        public virtual CreateVideoChatInvitationResponse CreateVideoChatInvitation(Guid supplierId, 
            CreateInvitationRequest.eInvitationType type)
        {
            NoProblem.Core.DataModel.Xrm.new_videochatinvitation.InvitationType invitationType = (NoProblem.Core.DataModel.Xrm.new_videochatinvitation.InvitationType)((int)type);
    //        var account = repository.GetSingleByCriteria<account>(a => a.accountid == supplierId);
            NoProblem.Core.DataAccessLayer.AccountRepository ar = new DataAccessLayer.AccountRepository(null);
            string supplierName;
            if (!ar.IsAccountExistsAndActive(supplierId, out supplierName))
                throw new ResourceNotFoundException("supplier", supplierId);

            new_videochatinvitation invitation = new new_videochatinvitation();
            invitation.new_accountid = supplierId;
            invitation.new_invitationtype = (int)type;
            repository.Create(invitation);
            CreateVideoChatInvitationResponse response = new CreateVideoChatInvitationResponse()
            {
                invitationId = invitation.new_videochatinvitationid,
                invitationType = type,
                message = string.Format((invitationType == new_videochatinvitation.InvitationType.VIDEO_CHAT ? VIDEO_CHAT_MESSAGE : VIDEO_MESSAGE), supplierName, "{0}")
            };
            /*
            NoProblem.Core.BusinessLogic.GoogleShorten.FactoryShortenUrl shortenUrl =
                new GoogleShorten.FactoryShortenUrl(DestinationWebPage + invitation.new_videochatinvitationid);
            */
            //TODO
            //1.create video invitation and associate it with the advertiser(status of invitation - CREATED)
            //2.generate shorten url based on the invitation 
            //3.save invitation to database
            //4. return the url

            return response;
        }


        public virtual InvitationCheckInResponse InvitationCheckIn(Guid invitationId)
        {
            
            VideoChatInvitation chatInvitation = new VideoChatInvitation();
            if (!chatInvitation.IsExistsAndAvailable(invitationId))
            {
                LogUtils.MyHandle.HandleException(new ResourceNotFoundException("VideoChatInvitation", invitationId), 
                    "Exception in InvitationCheckIn_IsExistsAndAvailable, InvitationId = {0}", invitationId);
               // throw new ResourceNotFoundException("VideoChatInvitation", invitationId);
                return new InvitationCheckInResponse() { checkInStatus = InvitationCheckInResponse.eInvitationCheckInStatus.notAvailable };
            }            
            InvitationCheckInResponse response = chatInvitation.GetSupplierDetails(invitationId);
            if(response.categoryId == Guid.Empty)
            {
                response.categoryId = NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.VideoRequestsManager.DEFAULT_EXPERTISE_ID;
                response.categoryName = NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.VideoRequestsManager.DEFAULT_EXPERTISE_NAME;
            }
          
            chatInvitation.UpdateVideoChatInvitationOnCheckedIn(invitationId);//, videoSession.ApiKey, videoSession.SessionId, videoSession.Token);

            return response;
        }
        public virtual void SetFavoriteServiceProvider(Guid invitationId, Guid customerId)
        {
            FavoriteServiceProvider favorite = new FavoriteServiceProvider(null);
            favorite.AddFavoriteServiceProviderByInvitation(customerId, invitationId);
        }
        public virtual CreateVideoChatConversationResponse CreateVideoChatCall(Guid customerId, Guid invitationId)
        {
            //invitation = GetInvitationById(invitationId);
            VideoChatInvitation chatInvitation = new VideoChatInvitation();
           /// VideoSession videoSession = chatInvitation.GetVideoChatSession(invitationId);
            VideoSession videoSession = this.CreateVideoSession();
            if (videoSession == null)
            {
                LogUtils.MyHandle.HandleException(new Exception("Failed to create VideoSession"),
                    "Exception in InvitationCheckIn_CreateVideoSession, InvitationId = {0}", invitationId);
                return new CreateVideoChatConversationResponse { status = CreateVideoChatConversationResponse.eCreateVideoChatConversationStatus.FAILED_CREATE_SESSION };
            }
                      
            
       //     CreateLead(invitationId);
            VideoChatPushSupplierData pushData = chatInvitation.GetInvitationDetailsForPushSupplier(invitationId, customerId);
            if (pushData == null)
                return new CreateVideoChatConversationResponse{status = CreateVideoChatConversationResponse.eCreateVideoChatConversationStatus.FAILED_PUSH_ADVERTISER};


            string advertiserToken = openTokClient.GenerateToken(videoSession.SessionId);
            if(string.IsNullOrEmpty(advertiserToken))
                return new CreateVideoChatConversationResponse { status = CreateVideoChatConversationResponse.eCreateVideoChatConversationStatus.FAILED_PUSH_ADVERTISER };

            chatInvitation.UpdateVideoChatInvitationOnCreateCall(invitationId, customerId, videoSession.SessionId, videoSession.ApiKey, videoSession.Token, Guid.Empty);
            ThreadPool.QueueUserWorkItem(new WaitCallback(CreateLead), invitationId);
            VideoChatJoinPushNotification noti = new VideoChatJoinPushNotification(pushData.supplierId, pushData.customerPhone,
                pushData.customerImage, invitationId, videoSession.ApiKey, videoSession.SessionId, advertiserToken);
            noti.SendAsyncNotification();
          //  VideoRecording videoRecording = this.StartRecording(videoSession.SessionId);
         //   Guid archiveId = (videoRecording != null) ? videoRecording.Id : Guid.Empty;
            
            return new CreateVideoChatConversationResponse
            {
                ApiKey = videoSession.ApiKey,
                SessionId = videoSession.SessionId,
                Token = videoSession.Token,
                status = CreateVideoChatConversationResponse.eCreateVideoChatConversationStatus.SUCCESS
            };
        }
       public void OnSentTextMessage(Guid supplierId,  Guid invitationId)
        {
            VideoChatInvitation chatInvitation = new VideoChatInvitation();
            chatInvitation.OnSentTextMessage(invitationId);
        }
        public void OnSupplierAnswer(Guid supplierId,  Guid invitationId)
       {
           VideoChatInvitation chatInvitation = new VideoChatInvitation();
           VideoSession videoSession = chatInvitation.GetVideoChatSession(invitationId);
           if (videoSession == null || string.IsNullOrEmpty(videoSession.SessionId))
               return;
           VideoRecording videoRecording = this.StartRecording(videoSession.SessionId);
           if (videoRecording != null)
               chatInvitation.UpdateVideoChatInvitationArchiveId(invitationId, videoRecording.Id, supplierId);
       }
       
        /*
        public void OnEndVideoChat(string sessionId, Guid Id, bool isSupplier)
        {
            Guid invitationid;
            VideoChatInvitation chatInvitation = new VideoChatInvitation();
            VideoRecording recording = GetRecording(chatInvitation.GetArchiveId(sessionId, Id, isSupplier, out invitationid));
            if (recording == null || invitationid == Guid.Empty || !recording.HasAudio)
                return;
            chatInvitation.UpdateVideoChatInvitation(invitationid, recording.Url, (int)(recording.Duration / 1000));
        }
        */
        public void OpentokArchiveStatusChanges(Guid archiveId, int partnerId, string sessionId, string videoUrl, long duration, string status)
        {
            if (status != "uploaded")
                return;
            string amazonVideoUrl = AMAZON_AWS_BASE_URL + partnerId + @"/" + archiveId.ToString() + "/archive.mp4";
            VideoChatInvitation chatInvitation = new VideoChatInvitation();
            chatInvitation.UpdateVideoChatInvitation(sessionId, archiveId, amazonVideoUrl, (int)(duration));
        }
        public GetFavoriteServiceProviderResponse GetFavoriteServiceProvider(Guid invitationId, Guid expertiseId)
        {
            VideoChatInvitation chatInvitation = new VideoChatInvitation();
            return chatInvitation.GetFavoriteServiceProvider(invitationId, expertiseId);
        }
        private void CreateLead(object state)
        {
            Guid _invitationId = (Guid)state;
            NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.VideoRequestsManager vrm =
                new ServiceRequest.VideoRequests.VideoRequestsManager(null);
            vrm.CreateNewVideoChat(_invitationId);
        }

        protected virtual VideoSession CreateVideoSession()
        {
            int apiKey = openTokClient.ApiKey;
            Session session = openTokClient.CreateSession(string.Empty, MediaMode.ROUTED, ArchiveMode.MANUAL);
            string token = session.GenerateToken();
            var response = new VideoSession
            {
                ApiKey = apiKey,
                SessionId = session.Id,
                Token = token
            };

            return response;
        }


        protected virtual VideoRecording StartRecording(string sessionId)
        {
            Archive archive;
            try
            {
        //        archive = openTokClient.StartArchive(sessionId, "", true, true, OutputMode.INDIVIDUAL);
                archive = openTokClient.StartArchive(sessionId);
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception in StartRecording sessionId: {0}", sessionId);
                return null;
            }
            if (archive == null)
                return null;


            var recording = new VideoRecording
            {
                HasVideo = archive.HasVideo,
                Name = archive.Name,
                Url = archive.Url,
                Id = archive.Id
            };

            return recording;
        }

        protected virtual VideoRecording StopRecording(string sessionId, string recordingId)
        {

            string archiveId = recordingId;

            Archive archive = openTokClient.StopArchive(archiveId);
            if (archive == null)
                return null;


            var recording = new VideoRecording
            {
                HasVideo = archive.HasVideo,
                Name = archive.Name,
                Url = archive.Url
            };

            return recording;
        }


        public virtual VideoRecording GetRecording(string archiveId)
        {
        //    string archiveId = recordingId;
            if (string.IsNullOrEmpty(archiveId))
                return null;
            Archive archive = openTokClient.GetArchive(archiveId);
            if (archive == null)
                return null;

            var recording = new VideoRecording
            {
                HasVideo = archive.HasVideo,
                Name = archive.Name,
                Url  = archive.Url,
                Duration = archive.Duration
            };

            return recording;
        }
       
        

        
    }
}
