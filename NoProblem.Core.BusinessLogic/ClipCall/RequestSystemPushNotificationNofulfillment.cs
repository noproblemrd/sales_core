﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ClipCall
{
    public class RequestSystemPushNotificationNofulfillment : RequestSytemCustomerPushNotification
    {
     //   const string MESSAGE = @"How embarrassing... We couldn't find a pro to match your request. Check your video and see that the message is clear. You can try resending or record a new one.";
        const string MESSAGE = @"How embarrassing... We couldn't find a pro for your request. Check your video to see that it's clear. You can then resend, or record a new one.";
        const string TITLE = "Your ClipCall video";
        public RequestSystemPushNotificationNofulfillment(Guid customerId):base(customerId, true)
        {
            this.message = MESSAGE;
            this.title = TITLE;
            this.pushType = PushNotifications.ePushTypes.UNFULFILLMENT_REQUEST;
        }        
        /*
        protected override bool BeforeSendNotification()
        {
            return true;
        }
         */
    }
}
