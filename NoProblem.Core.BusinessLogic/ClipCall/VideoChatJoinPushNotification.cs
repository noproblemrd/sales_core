﻿using NoProblem.Core.BusinessLogic.PushNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ClipCall
{
    
    public class VideoChatJoinPushNotification : RequestSytemCustomerPushNotification
    {
        const string MESSAGE = "Live ClipCall from {0}";
        const string titleMessage = "ClipCall";

        const ePushTypes PUSH_TYPE = ePushTypes.VIDEO_CHAT_JOIN_SUPPLIER;

        private string customerPhone;
        private string customerImage;
        private Guid invitationId;
        private int apiKey;
        private string sessionId;
        private string token;
        public VideoChatJoinPushNotification(Guid supplierId,string customerPhone, string customerImage,Guid invitationId,
            int apiKey,string sessionId,string token):base(supplierId,false)
        {
            this.customerPhone = customerPhone;            
            this.pushType = PUSH_TYPE;
            this.message = string.Format(MESSAGE, this.GetPhoneDisplay);
            this.title = titleMessage;
            this.customerImage = customerImage;
            this.customData = customData;
            this.invitationId = invitationId;
            this.apiKey=apiKey;
            this.sessionId=sessionId;
            this.token=token;
        }
        protected override bool BeforeSendNotification()
        {
            customData = new Dictionary<string, string>();
            customData.Add("customerPhone", customerPhone);
            customData.Add("customerImage", customerImage);
            customData.Add("invitationId", invitationId.ToString());
             customData.Add("apiKey",apiKey.ToString());
            customData.Add("sessionId", sessionId);
            customData.Add("token", token);
            return true;
        }
        private string GetPhoneDisplay
        {
            get
            {
                /*
                StringBuilder result = new StringBuilder("+1 (", 20);
                for (int i = 0; i < customerPhone.Length; i++)
                {
                    if (i == 3)
                        result.Append(") ");
                    else if (i == 6)
                        result.Append("-");
                    result.Append(customerPhone[i]);
                }
                return result.ToString();
                 * */
                return NoProblem.Core.BusinessLogic.Dialer.PhoneUtility.GetPhoneDisplay(customerPhone);
            }
        }
    }
}
