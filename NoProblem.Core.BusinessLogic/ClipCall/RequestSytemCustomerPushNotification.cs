﻿using NoProblem.Core.BusinessLogic.PushNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.ClipCall
{
    public abstract class RequestSytemCustomerPushNotification
    {
        protected Guid ID { get; set; }
        protected bool isCustomer { get; set; }
        protected string message { get; set; }
        protected string title { get; set; }
        protected ePushTypes pushType { get; set; }
        protected Dictionary<string, string> customData { get; set; }
        public RequestSytemCustomerPushNotification(Guid ID, bool isCustomer)
        {
            this.ID = ID;
            this.isCustomer = isCustomer;
        }
        public void SendAsyncNotification()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
            {
                RequestSytemCustomerPushNotification _this = (RequestSytemCustomerPushNotification)state;
                if (_this.BeforeSendNotification())
                    _this.SendNotification();
            }), this);
        }
        protected virtual bool BeforeSendNotification()
        {
            return true;
        }
        private bool SendNotification()
        {
            NoProblem.Core.DataAccessLayer.MobileDevice deviceDal = new DataAccessLayer.MobileDevice(null);
            DataModel.Xrm.new_mobiledevice device = isCustomer ? deviceDal.GetActiveMobileDeviceByContact(ID) :
                deviceDal.GetActiveMobileDeviceByAccount(ID);
            if (device == null || string.IsNullOrEmpty(device.new_uid))
                return false;
            IPushServer pushServer = PushServerFactory.GetPushServer(device.new_os, device.new_isdebug ?? false);
            PushRequest pushRequest = new PushRequest();
            pushRequest.DeviceId = device.new_uid;
            pushRequest.PushType = pushType;
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("message", message);
            dic.Add("title", title);
            if (customData != null)
                dic = dic.Union(customData).ToDictionary(k => k.Key, v => v.Value);
            pushRequest.Data = dic;
            NpPushResult pushResult = pushServer.Send(pushRequest);
            return (pushResult.IsSuccess);
        }
    }
}
