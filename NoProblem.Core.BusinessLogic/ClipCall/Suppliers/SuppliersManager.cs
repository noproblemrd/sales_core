﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk;
using NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call.ConnectAndRecord;
using NoProblem.Core.BusinessLogic.LeadBuyers;
using NoProblem.Core.DataAccessLayer;
using NoProblem.Core.DataAccessLayer.Bases;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;
using NoProblem.Core.DataModel.Xrm;
using Twilio;

namespace NoProblem.Core.BusinessLogic.ClipCall.Suppliers
{
    public class SuppliersManager
    {
        private readonly IncidentAccount incidentAccountRepository;
        private readonly AccountRepository accountRepository;
        private readonly Repository repository;

        public SuppliersManager(IncidentAccount incidentAccountRepository, AccountRepository accountRepository, Repository repository)
        {
            this.incidentAccountRepository = incidentAccountRepository;
            this.accountRepository = accountRepository;
            this.repository = repository;
        }

        public virtual bool ConnectToCustomer(Guid supplierId, Guid incidentAccountId, bool shouldRecordCall)
        {
            new_incidentaccount incidentAccount = incidentAccountRepository.Retrieve(incidentAccountId);
            if (incidentAccount == null)
                throw new Exception(string.Format("Cannot connect supplier to customer. could not find incidentAccount with id {0}", incidentAccountId));

            account supplierAccount = incidentAccount.new_account_new_incidentaccount;
            if (supplierAccount.accountid != supplierId)
                throw new Exception(string.Format("Cannot connect supplier to customer. supplier with id {0} is not associated with incident account id {1}", supplierId, incidentAccountId));

            if (shouldRecordCall && incidentAccount.new_customerdisallowedrecording.HasValue && incidentAccount.new_customerdisallowedrecording.Value)
                throw new Exception(String.Format("Cannot connect supplier to customer. the customer disallowed recording calls"));

            incident incident = incidentAccount.new_incident_new_incidentaccount;
            contact customerAccount = incident.incident_customer_contacts;
            var recordedCallsManager = new RecordedCallsManager(incidentAccountId);
            bool success = recordedCallsManager.StartCall(supplierAccount.telephone1, customerAccount.mobilephone);
            return success;
        }

        public virtual bool RemoveSupplierCategory(Guid supplierId, Guid categoryId)
        {
            var account = repository.GetSingleByCriteria<account>(a => a.accountid == supplierId);
            if (account == null)
                throw new Exception(string.Format("Cannot remove category from account. supplier with id {0} does not exist", supplierId));


            List<new_primaryexpertise> supplierCategories = account.new_account_primaryexpertise.ToList();
            new_primaryexpertise category = supplierCategories.SingleOrDefault(c => c.new_primaryexpertiseid == categoryId);
            if(category == null)
                throw new Exception(string.Format("Cannot remove category from account. category with id {0} does not exist", categoryId));

            repository.Delete(category);
            return true;
        }


        public virtual void Login(string phoneNumber)
        {
            account supplierAccount = accountRepository.All.SingleOrDefault(x => x.telephone1 == phoneNumber);
            if (supplierAccount == null)
            {
                string password = Utils.LoginUtils.GenerateNewPassword();
                supplierAccount = new account
                {
                    new_recordcalls = true,
                    new_password = password,
                    new_stepinregistration2014 =(int) account.StepInRegistration2014.BusinessDetails_General_3,
                    new_status = (int) account.SupplierStatus.Trial,
                    name = string.Empty,
                    new_fullname = string.Empty,
                    telephone1 = phoneNumber,
                    new_havevideo = true,
                    new_ismobile = true,
                    new_subscriptionplan = SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION
                };
                accountRepository.Create(supplierAccount);
            }            
            //string token = GetMobileToken(request.MobileLogOnData, customerId);
            //return new DataModel.Consumer.CustomerLogOnResponse()
            //{
            //    ApiToken = token,
            //    CustomerId = customerId
            //};
        }
    }

    internal class RecordedCallsManager
    {
        private readonly Guid incidentAccountId;

        public RecordedCallsManager(Guid incidentAccountId)
        {
            this.incidentAccountId = incidentAccountId;
        }

        public bool StartCall(string supplierPhone, string customerPhone)
        {
            var callManager = new ConnectAndRecordCallManagerConsumer(incidentAccountId, supplierPhone, customerPhone);
            callManager.OnCallEndedWithRecording+=callManager_OnCallEndedWithRecording;
            bool callStarted = callManager.StartCall();
            return callStarted;
        }

        void callManager_OnCallEndedWithRecording(string recordingUrl, int duration)
        {
            var dal = new CallRecording(null);
            var recording = new new_callrecording(dal.XrmDataContext)
            {
                new_url = recordingUrl,
                new_incidentaccountid = incidentAccountId,
                new_duration = duration
            };
            dal.Create(recording);
            dal.XrmDataContext.SaveChanges();
        }
    }
}
