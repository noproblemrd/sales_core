﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.ClipCall.Advertiser;
//using NoProblem.Core.DataModel.Xrm.account;

namespace NoProblem.Core.BusinessLogic.ClipCall.Suppliers
{
    public class AdvertiserReportManager
    {
        public List<AdvertiserReportResponse> AdvertiserReport(AdvertiserReportRequest request)
        {
            List<AdvertiserReportResponse> listResponse = new List<AdvertiserReportResponse>();
            string whereStep = " and acc.New_StepinRegistration2014 ";
            string command = @"
select  acc.AccountId, acc.New_StepinRegistration2014, acc.Telephone1, acc.CreatedOn, acc.Name, acc.New_FullAddress
	,c.New_name Category
from Account acc
	left join New_accountexpertiseExtensionBase ac with(nolock) on acc.AccountId = ac.new_accountid
	left join New_accountexpertiseBase acb with(nolock) on ac.New_accountexpertiseId = acb.New_accountexpertiseId and acb.DeletionStateCode = 0
	left join New_primaryexpertiseExtensionBase c with(nolock) on c.New_primaryexpertiseId = ac.new_primaryexpertiseid
where acc.DeletionStateCode = 0 and	acc.New_IsMobile = 1 and acc.New_havevideo = 1
    and acc.CreatedOn >= @from and acc.CreatedOn < @to
        {WHERE}
order by acc.CreatedOn desc, acc.AccountId";
            StringBuilder sb = new StringBuilder(command);
            try
            {
                using(SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using(SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        switch (request.stepInRegistration)
                        {
                            case (eStepInRegistration.ALL):
                                sb.Replace("{WHERE}", string.Empty);
                                break;                            
                            case (eStepInRegistration.BUSINESS_DETAILS):
                                sb.Replace("{WHERE}", whereStep + "= @stepinRegistration ");
                                cmd.Parameters.AddWithValue("@stepinRegistration", (int)NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_General_3);
                                
                                break;
                            case (eStepInRegistration.COVER_AREA):
                                sb.Replace("{WHERE}", whereStep + "in (@stepinRegistration1, @stepinRegistration2) ");
                                cmd.Parameters.AddWithValue("@stepinRegistration1", (int)NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_Category_4);
                                cmd.Parameters.AddWithValue("@stepinRegistration2", (int)NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_Area_5);
                                break;
                            case (eStepInRegistration.COMPLETE):
                                sb.Replace("{WHERE}", whereStep + ">= @stepinRegistration ");
                                cmd.Parameters.AddWithValue("@stepinRegistration", (int)NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014.Checkout_6);
                                break;

                        }
                        cmd.Parameters.AddWithValue("@from", request.GetFrom);
                        cmd.Parameters.AddWithValue("@to", request.GetTo);
                        cmd.CommandText = sb.ToString();
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        AdvertiserReportResponse row = null;
                        while(reader.Read())
                        {
                            Guid accountId = (Guid)reader["AccountId"];
                            if(row == null || row.accountId != accountId)
                            {
                                row = new AdvertiserReportResponse();
                                row.accountId = accountId;
                                if (reader["New_FullAddress"] != DBNull.Value)
                                    row.address = (string)reader["New_FullAddress"];
                                row.phone = (string)reader["Telephone1"];
                                row.createdOn = (DateTime)reader["CreatedOn"];
                                if (reader["Name"] != DBNull.Value)
                                    row.name = (string)reader["Name"];
                                if (reader["New_StepinRegistration2014"] != DBNull.Value)
                                {
                                    NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014 sir = (NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014)((int)reader["New_StepinRegistration2014"]);
                                    switch(sir)
                                    {
                                        case(NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_General_3):                                        
                                            row.stepInRegistration = eStepInRegistration.BUSINESS_DETAILS;
                                            break;
                                        case (NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_Category_4):
                                        case (NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_Area_5):
                                            row.stepInRegistration = eStepInRegistration.COVER_AREA;
                                            break;
                                        case (NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014.Checkout_6):
                                        case (NoProblem.Core.DataModel.Xrm.account.StepInRegistration2014.Dashboard_7):
                                            row.stepInRegistration = eStepInRegistration.COMPLETE;
                                            break;
                                    }
                                }
                                else
                                    row.stepInRegistration = eStepInRegistration.BUSINESS_DETAILS;
                                listResponse.Add(row);
                            }
                            if (reader["Category"] != DBNull.Value)
                                row.categories.Add((string)reader["Category"]);
                        }
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed to create clipcall SQL AdvertiserReport from={0}, to={1}, StepinRegistration={2}",
                    request.GetFrom, request.GetTo, request.stepInRegistration.ToString());

            }
            return listResponse;
            
        }
    }
}
