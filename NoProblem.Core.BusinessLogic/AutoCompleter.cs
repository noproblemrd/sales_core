﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic
{
    public class AutoCompleter : XrmUserBase
    {
        private const string CONTROL_NAME_DB = "new_controlname";
        private const string DOMAIN_DB = "SUBSTRING(new_url, 0, CHARINDEX('/', new_url, CHARINDEX('/', new_url, CHARINDEX('/', new_url)+1)+1))";
        private const string PAGE_NAME_DB = "new_pagename";
        private const string PLACE_IN_WEB_SITE_DB = "new_placeinwebsite";
        private const string URL_DB = "new_url";
        private const string KEYWORD_DB = "new_keyword";
        private const string INCIDENT_TABLE_DB = "incident";
        private const string EXPOSURE_TABLE_DB = "new_exposure";
        private const string AUTOCOMPLETE_QUERY =
            @"SELECT DISTINCT TOP 20 {1} FROM {0} WHERE deletionstatecode = 0 AND {1} LIKE N'%' + @input + '%'";

        public enum eAutoCompleteFromTable
        {
            Incident, Exposure
        }

        public enum eAtuoCompleteField
        {
            Url, ControlName, PageName, Domain, PlaceInWebSite, Keyword
        }

        public AutoCompleter(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<string> AutoComplete(string str, eAutoCompleteFromTable table, eAtuoCompleteField field)
        {
            List<string> retVal = new List<string>();
            if (!string.IsNullOrEmpty(str))
            {
                string tableName = GetTableName(table);
                string fieldName = GetFieldName(field);
                if (!string.IsNullOrEmpty(tableName) && !string.IsNullOrEmpty(fieldName))
                {
                    DataAccessLayer.Generic dal = new NoProblem.Core.DataAccessLayer.Generic();
                    List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
                    parameters.Add(new KeyValuePair<string, object>("@input", str));
                    string query = string.Format(AUTOCOMPLETE_QUERY, tableName, fieldName);
                    var reader = dal.ExecuteReader(query, parameters);
                    foreach (var row in reader)
                    {
                        retVal.Add((string)row.First().Value);
                    }
                }
            }
            return retVal;
        }

        private string GetFieldName(eAtuoCompleteField field)
        {
            string fieldName = null;
            switch (field)
            {
                case eAtuoCompleteField.ControlName:
                    fieldName = CONTROL_NAME_DB;
                    break;
                case eAtuoCompleteField.Domain:
                    fieldName = DOMAIN_DB;
                    break;
                case eAtuoCompleteField.PageName:
                    fieldName = PAGE_NAME_DB;
                    break;
                case eAtuoCompleteField.PlaceInWebSite:
                    fieldName = PLACE_IN_WEB_SITE_DB;
                    break;
                case eAtuoCompleteField.Url:
                    fieldName = URL_DB;
                    break;
                case eAtuoCompleteField.Keyword:
                    fieldName = KEYWORD_DB;
                    break;
            }
            return fieldName;
        }

        private string GetTableName(eAutoCompleteFromTable table)
        {
            string tableName = null;
            switch (table)
            {
                case eAutoCompleteFromTable.Exposure:
                    tableName = EXPOSURE_TABLE_DB;
                    break;
                case eAutoCompleteFromTable.Incident:
                    tableName = INCIDENT_TABLE_DB;
                    break;
            }
            return tableName;
        }
    }
}
