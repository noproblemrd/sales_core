﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.IvrWidget
{
    internal class AsteriskIvrWidgetManager : XrmUserBase
    {
        #region Ctor
        internal AsteriskIvrWidgetManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Internal Methods

        internal bool IsIvrWidgetNumber(string virtualNumber, string sessionId, string consumerPhone)
        {
            bool retVal = false;

            DataAccessLayer.IvrWidgetNumber dal = new NoProblem.Core.DataAccessLayer.IvrWidgetNumber(XrmDataContext);
            var number = dal.FindNumber(virtualNumber);
            if (number != null)
            {
                retVal = true;
                CreateIvrWidgetCall(sessionId, virtualNumber, number, consumerPhone);
            }
            return retVal;
        }

        internal void CloseIvrWidgetCall(string sessionId, string digit, string status)
        {
            DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
            Guid id = dal.GetCallIdByDialerCallId(sessionId);
            if (id != Guid.Empty)
            {
                bool isValidPhone = false;
                DataModel.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall(XrmDataContext);
                call.new_ivrwidgetcallid = id;
                digit = digit.Replace("*", string.Empty);
                call.new_phoneentered = digit;
                call.new_callendstatus = status;
                if (string.IsNullOrEmpty(digit) || status == NoProblem.Core.DataModel.Asterisk.VnStatusCode.CALL_CANCEL)
                {
                    call.new_processstatus = (int)DataModel.Xrm.new_ivrwidgetcall.ProcessStatus.NoNumberFound;
                }
                else
                {
                    isValidPhone = ValidatePhoneNumber(digit);
                    if (!isValidPhone)
                    {
                        call.new_processstatus = (int)DataModel.Xrm.new_ivrwidgetcall.ProcessStatus.InvalidPhone;
                    }
                    else
                    {
                        call.new_processstatus = (int)DataModel.Xrm.new_ivrwidgetcall.ProcessStatus.PhoneValid;
                    }
                }
                dal.Update(call);
                XrmDataContext.SaveChanges();
                if (isValidPhone)
                {
                    CreateServiceRequest(id);
                }
            }
            else
            {
                LogUtils.MyHandle.WriteToLog("IvrWidgetCall with dialerid {0} was not found in AsteriskIvrWidgetManager.CloseIvrWidgetCall", sessionId);
            }
        } 

        #endregion

        #region Private Methods

        private void CreateIvrWidgetCall(string sessionId, string virtualNumber, DataModel.Xrm.new_ivrwidgetnumber ivrWidgetNumber, string consumerPhone)
        {
            DataModel.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall();
            call.new_dialercallid = sessionId;
            call.new_ivrwidgetnumberid = ivrWidgetNumber.new_ivrwidgetnumberid;
            call.new_ivrwidgetnumber = virtualNumber;
            call.new_originid = ivrWidgetNumber.new_originid.Value;
            call.new_primaryexpertiseid = ivrWidgetNumber.new_primaryexpertiseid.Value;
            call.new_regionid = ivrWidgetNumber.new_regionid.Value;
            call.new_initialphone = consumerPhone;
            DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
            dal.Create(call);
            XrmDataContext.SaveChanges();
        }

        private void CreateServiceRequest(Guid ivrWidgetCallId)
        {
            CreateServiceRequestDelegate del = new CreateServiceRequestDelegate(CreateServiceRequestAsync);
            del.BeginInvoke(ivrWidgetCallId, CreateServiceRequestAsyncCallback, del);
        }

        private delegate void CreateServiceRequestDelegate(Guid ivrWidgetCallId);

        private void CreateServiceRequestAsync(Guid ivrWidgetCallId)
        {
            DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(null);
            var localContext = dal.XrmDataContext;
            var call = dal.Retrieve(ivrWidgetCallId);
            ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(localContext);
            var region = call.new_region_ivrwidgetcall;
            var request = new DataModel.ServiceRequest()
            {
                ContactPhoneNumber = call.new_phoneentered,
                ExpertiseType = 1,
                ExpertiseCode = call.new_primaryexpertise_ivrwidgetcall.new_code,
                NumOfSuppliers = 3,
                OriginId = call.new_originid.Value,
                RegionCode = region.new_code,
                RegionLevel = region.new_level.Value,
                PrefferedCallTime = DateTime.Now
            };
            var response = manager.CreateService(request);
            if (response.Status != NoProblem.Core.DataModel.StatusCode.Success)
            {
                LogUtils.MyHandle.WriteToLog("Received status {0} and message {1} when sending service request from AsteriskIvrWidgetManager.CreateServiceRequestAsync for IvrWidgetCallId {2}", response.Status, response.Message, ivrWidgetCallId);
                call.new_errorcode = response.Status.ToString();
                call.new_errorurl = response.Message;
            }
            else
            {
                call.new_incidentid = new Guid(response.ServiceRequestId);
            }
            dal.Update(call);
            localContext.SaveChanges();
        }

        private void CreateServiceRequestAsyncCallback(IAsyncResult result)
        {
            try
            {
                ((CreateServiceRequestDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AsteriskIvrWidgetManager.CreateServiceRequestAsyncCallback");
            }
        }

        private bool ValidatePhoneNumber(string digit)
        {
            string regexPattern = @"^\d{2,3}\d{7}$";
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(regexPattern);
            return regex.IsMatch(digit);
        }

        #endregion
    }
}
