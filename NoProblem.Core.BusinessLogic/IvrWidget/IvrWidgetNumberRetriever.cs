﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.IvrWidget
{
    public class IvrWidgetNumberRetriever : XrmUserBase
    {
        public IvrWidgetNumberRetriever(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {

        }

        public string GetIvrWidgetNumber(Guid originId, string headingCode)
        {
            DataAccessLayer.IvrWidgetNumber dal = new NoProblem.Core.DataAccessLayer.IvrWidgetNumber(XrmDataContext);
            string number = dal.GetIvrWidgetNumber(originId, headingCode);
            return number;
        }
    }
}
