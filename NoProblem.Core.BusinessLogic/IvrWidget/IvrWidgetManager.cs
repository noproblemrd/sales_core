﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.IvrWidget;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.IvrWidget
{
    internal class IvrWidgetManager : XrmUserBase
    {
        #region Ctor
        public IvrWidgetManager(DataModel.Xrm.DataContext xrmDataContext) :
            base(xrmDataContext)
        {
            configurationDal = new NoProblem.Core.DataAccessLayer.IvrWidgetConfiguration(XrmDataContext);
        }
        #endregion

        private delegate void StartServiceRequestDelegate(Guid ivrWidgetCallId);

        private DataAccessLayer.IvrWidgetConfiguration configurationDal;

        private static SynchronizedCollection<TimerContainer> timersHolder = new SynchronizedCollection<TimerContainer>();

        private class TimerContainer
        {
            public Guid IvrWidgetCallId { get; set; }
            public Timer Timer { get; set; }
        }

        #region Internal Methods

        internal ProcessStatus IncomingCall(
            string callSid, string from, string to, string callStatus,
            out string ttsText, out bool isManVoice, out Guid ivrWidgetCallId)
        {
            ProcessStatus status = ProcessStatus.Continue;
            //find ivr widget phone in db
            DataAccessLayer.IvrWidgetNumber numbersDal = new NoProblem.Core.DataAccessLayer.IvrWidgetNumber(XrmDataContext);
            DML.Xrm.new_ivrwidgetnumber number = numbersDal.FindNumber(to);
            string text;
            DML.Xrm.new_ivrwidgetcall.ProcessStatus callProcessStatus;
            string headingName = string.Empty;
            if (number == null)
            {
                text = "";
                status = ProcessStatus.HangUp;
                callProcessStatus = DML.Xrm.new_ivrwidgetcall.ProcessStatus.NoNumberFound;
                LogUtils.MyHandle.HandleException(new Exception("IvrWidget number could not be found. Please configure it correctly"), "Exception in IvrWidgetManager.IncomingCall. callSid = {0}, from = {1}, to = {2}, callStatus = {3}", callSid, from, to, callStatus);
            }
            else
            {
                bool isPhoneValid = ValidatePhone(from);
                if (isPhoneValid)
                {
                    text = configurationDal.GetConfigurationText("new_welcomewithvalidphone");
                    callProcessStatus = DML.Xrm.new_ivrwidgetcall.ProcessStatus.PhoneValid;
                }
                else
                {
                    text = configurationDal.GetConfigurationText("new_welcomewithinvalidphone");
                    status = ProcessStatus.GoBack;
                    callProcessStatus = DML.Xrm.new_ivrwidgetcall.ProcessStatus.InvalidPhone;
                }
                DataAccessLayer.PrimaryExpertise headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
                headingName = headingDal.GetIvrWidgetName(number.new_primaryexpertiseid.Value);
            }
            //create call in db
            Guid callId = CreateIvrWidgetCall(callSid, from, to, callStatus, callProcessStatus, number);
            ivrWidgetCallId = callId;
            ttsText = string.Format(text, SeparateNumbersForTTS(from), headingName);
            isManVoice = false;
            return status;
        }

        internal ProcessStatus PhoneGet(Guid ivrWidgetCallId, string digits, out string ttsText, out bool isManVoice, ref int tryCount)
        {
            ProcessStatus retVal = ProcessStatus.Continue;
            DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
            DML.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall(XrmDataContext);
            call.new_ivrwidgetcallid = ivrWidgetCallId;
            call.new_phoneentered = digits;
            bool isPhoneValid = ValidatePhone(digits);
            if (isPhoneValid)
            {
                string text = configurationDal.GetConfigurationText("new_enteredvalidphone");
                ttsText = string.Format(text, SeparateNumbersForTTS(digits));
                tryCount = 1;
                call.new_processstatus = (int)DML.Xrm.new_ivrwidgetcall.ProcessStatus.PhoneValid;
            }
            else
            {
                bool isOverTryCounts = CheckIsOverTriesAllowed(tryCount, ivrWidgetCallId, IvrWidgetCallStage.PhoneGet);
                if (isOverTryCounts)
                {
                    ttsText = configurationDal.GetConfigurationText("new_toomanyinvalidphones");
                    retVal = ProcessStatus.HangUp;
                    call.new_processstatus = (int)DML.Xrm.new_ivrwidgetcall.ProcessStatus.TooManyInvalidPhones;
                }
                else
                {
                    ttsText = configurationDal.GetConfigurationText("new_enteredinvalidphone");
                    retVal = ProcessStatus.Repeat;
                    tryCount++;
                    call.new_processstatus = (int)DML.Xrm.new_ivrwidgetcall.ProcessStatus.InvalidPhone;
                }
            }
            dal.Update(call);
            XrmDataContext.SaveChanges();
            isManVoice = false;
            return retVal;
        }

        internal ProcessStatus PhoneConfirm(Guid ivrWidgetCallId, string digits, out string ttsText, out bool isManVoice, ref int tryCount)
        {
            ProcessStatus retVal = ProcessStatus.Continue;

            if (digits == "1")
            {
                ttsText = configurationDal.GetConfigurationText("new_phoneconfirmed");
                tryCount = 1;
                DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
                DML.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall(XrmDataContext);
                call.new_ivrwidgetcallid = ivrWidgetCallId;
                call.new_processstatus = (int)DML.Xrm.new_ivrwidgetcall.ProcessStatus.PhoneConfirmed;
                dal.Update(call);
                XrmDataContext.SaveChanges();
            }
            else if (digits == "2")
            {
                ttsText = configurationDal.GetConfigurationText("new_wantstochangephone");
                retVal = ProcessStatus.GoBack;
                tryCount = 1;
            }
            else
            {
                bool isOverTries = CheckIsOverTriesAllowed(tryCount, ivrWidgetCallId, IvrWidgetCallStage.PhoneConfirm);
                if (isOverTries)
                {
                    ttsText = configurationDal.GetConfigurationText("new_toomanynotconfirmedphone");
                    retVal = ProcessStatus.HangUp;
                    DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
                    DML.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall(XrmDataContext);
                    call.new_ivrwidgetcallid = ivrWidgetCallId;
                    call.new_processstatus = (int)DML.Xrm.new_ivrwidgetcall.ProcessStatus.TooManyNotConfirmedPhone;
                    dal.Update(call);
                    XrmDataContext.SaveChanges();
                }
                else
                {
                    ttsText = configurationDal.GetConfigurationText("new_phoneconfirmbadkeyornothingpressed");
                    retVal = ProcessStatus.Repeat;
                    tryCount++;
                }
            }
            isManVoice = false;
            return retVal;
        }

        internal ProcessStatus AreaGet(Guid ivrWidgetCallId, string digits, out string ttsText, out bool isManVoice, ref int tryCount)
        {
            ProcessStatus retVal = ProcessStatus.Continue;
            DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
            DML.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall(XrmDataContext);
            call.new_ivrwidgetcallid = ivrWidgetCallId;
            call.new_areaentered = digits;
            Guid regionId;
            bool areaValid = ValidateArea(digits, out regionId);
            if (areaValid)
            {
                string txt = configurationDal.GetConfigurationText("new_enteredvalidarea");
                ttsText = string.Format(txt, SeparateNumbersForTTS(digits));
                tryCount = 1;
                call.new_processstatus = (int)DML.Xrm.new_ivrwidgetcall.ProcessStatus.AreaValid;
                call.new_regionid = regionId;
            }
            else
            {
                bool isOverTryCounts = CheckIsOverTriesAllowed(tryCount, ivrWidgetCallId, IvrWidgetCallStage.AreaGet);
                if (isOverTryCounts)
                {
                    ttsText = configurationDal.GetConfigurationText("new_toomanyinvalidareas");
                    retVal = ProcessStatus.HangUp;
                    call.new_processstatus = (int)DML.Xrm.new_ivrwidgetcall.ProcessStatus.TooManyInvalidAreas;
                }
                else
                {
                    ttsText = configurationDal.GetConfigurationText("new_enteredinvalidarea");
                    tryCount++;
                    retVal = ProcessStatus.Repeat;
                    call.new_processstatus = (int)DML.Xrm.new_ivrwidgetcall.ProcessStatus.InvalidArea;
                }
            }
            dal.Update(call);
            XrmDataContext.SaveChanges();
            isManVoice = false;
            return retVal;
        }

        internal ProcessStatus AreaConfirm(Guid ivrWidgetCallId, string digits, out string ttsText, out bool isManVoice, ref int tryCount)
        {
            ProcessStatus retVal = ProcessStatus.Continue;

            if (digits == "2")
            {
                ttsText = configurationDal.GetConfigurationText("new_wantstochangearea");
                retVal = ProcessStatus.GoBack;
                tryCount = 1;
            }
            else if (digits == "1")
            {
                DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
                DML.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall(XrmDataContext);
                call.new_ivrwidgetcallid = ivrWidgetCallId;
                call.new_processstatus = (int)DML.Xrm.new_ivrwidgetcall.ProcessStatus.AreaConfirmed;
                dal.Update(call);
                XrmDataContext.SaveChanges();
                //bool isAvailable = IsSuppliersAvailableNow(ivrWidgetCallId, dal);
                //if (isAvailable)
                //{
                    ttsText = configurationDal.GetConfigurationText("new_areaconfirmedandgoodbye");
                //}
                //else
                //{
                //    ttsText = configurationDal.GetConfigurationText("new_areaconfirmednoavailablesuppliers");
                //}

                TimerContainer container = new TimerContainer();
                container.IvrWidgetCallId = ivrWidgetCallId;
                Timer timer = new Timer(StartServiceRequest, container, 15000, Timeout.Infinite);
                container.Timer = timer;
                timersHolder.Add(container);
            }
            else
            {
                bool isOverTries = CheckIsOverTriesAllowed(tryCount, ivrWidgetCallId, IvrWidgetCallStage.AreaConfirm);
                if (isOverTries)
                {
                    ttsText = configurationDal.GetConfigurationText("new_toomanynotconfirmedarea");
                    retVal = ProcessStatus.HangUp;
                    DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
                    DML.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall(XrmDataContext);
                    call.new_ivrwidgetcallid = ivrWidgetCallId;
                    call.new_processstatus = (int)DML.Xrm.new_ivrwidgetcall.ProcessStatus.TooManyNotConfirmedArea;
                    dal.Update(call);
                    XrmDataContext.SaveChanges();
                }
                else
                {
                    ttsText = configurationDal.GetConfigurationText("new_areaconfirmbadkeyornothingpressed");
                    retVal = ProcessStatus.Repeat;
                    tryCount++;
                }
            }

            isManVoice = false;
            return retVal;
        }

        internal void StatusCallback(string callDuration, string callSid, string callStatus)
        {
            DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
            Guid callId = dal.GetCallIdByDialerCallId(callSid);
            if (callId != Guid.Empty)
            {
                DML.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall(XrmDataContext);
                call.new_ivrwidgetcallid = callId;
                call.new_duration = int.Parse(callDuration);
                call.new_callendstatus = callStatus;
                dal.Update(call);
                XrmDataContext.SaveChanges();
            }
        }

        internal void FallBack(string callSid, string errorCode, string errorUrl)
        {
            DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
            Guid callId = dal.GetCallIdByDialerCallId(callSid);
            if (callId != Guid.Empty)
            {
                DML.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall(XrmDataContext);
                call.new_ivrwidgetcallid = callId;
                call.new_errorcode = errorCode;
                call.new_errorurl = errorUrl;
                dal.Update(call);
                XrmDataContext.SaveChanges();
            }
        }

        #endregion

        #region Private Methods

        private bool IsSuppliersAvailableNow(Guid ivrWidgetCallId, DataAccessLayer.IvrWidgetCall dal)
        {
            bool retVal = true;
            try
            {
                Supplier supplierLogic = new Supplier(XrmDataContext, null);
                var call = dal.Retrieve(ivrWidgetCallId);
                DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(dal.XrmDataContext);
                var heading = expertiseDal.Retrieve(call.new_primaryexpertiseid.Value);
                int expertiseType = 1;
                string expertiseCode = heading.new_code;
                DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(dal.XrmDataContext);
                var region = regionDal.Retrieve(call.new_regionid.Value);
                int regionLevel = region.new_level.Value;
                string regionCode = region.new_code;
                var suppliers = supplierLogic.SearchSiteSuppliers("", expertiseCode, expertiseType, regionCode, regionLevel, call.new_originid.Value.ToString(), 1, Guid.Empty, 1, 1, false, false, true, null);
                if (suppliers != null)
                {
                    var supplierElements = suppliers.Elements("Supplier");
                    int count = supplierElements.Count();
                    retVal = count > 0;
                }
                else
                {
                    retVal = false;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IvrWidgetManager.IsSuppliersAvailableNow");
            }
            return retVal;
        }

        private void StartServiceRequest(object state)
        {
            TimerContainer container = (TimerContainer)state;
            Guid ivrWidgetCallId = container.IvrWidgetCallId;
            try
            {
                timersHolder.Remove(container);
                LogUtils.MyHandle.WriteToLog(8, "Sending service request from ivrWidget. ivrWidgetCallId = {0}", ivrWidgetCallId);
                DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(null);
                var call = dal.Retrieve(ivrWidgetCallId);
                ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(dal.XrmDataContext);
                var request = new NoProblem.Core.DataModel.ServiceRequest();
                request.ContactPhoneNumber = CleanPhone(string.IsNullOrEmpty(call.new_phoneentered) ? call.new_initialphone : call.new_phoneentered);
                DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(dal.XrmDataContext);
                var heading = expertiseDal.Retrieve(call.new_primaryexpertiseid.Value);
                request.ExpertiseType = 1;
                request.ExpertiseCode = heading.new_code;
                DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(dal.XrmDataContext);
                var region = regionDal.Retrieve(call.new_regionid.Value);
                request.RegionLevel = region.new_level.Value;
                request.RegionCode = region.new_code;
                request.NumOfSuppliers = 3;
                request.OriginId = call.new_originid.Value;
                request.PrefferedCallTime = DateTime.Now;
                request.RequestDescription = " ";
                request.ControlName = "Ivr Widget";
                var response = manager.CreateService(request);
                if (!string.IsNullOrEmpty(response.ServiceRequestId))
                {
                    Guid caseID = new Guid(response.ServiceRequestId);
                    call.new_incidentid = caseID;
                    dal.Update(call);
                    dal.XrmDataContext.SaveChanges();
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in IvrWidgetManager.StartServiceRequest. ivrWidgetCallId = {0}", ivrWidgetCallId);
            }
        }

        private bool CheckIsOverTriesAllowed(int tryCount, Guid ivrWidgetCallId, IvrWidgetCallStage stage)
        {
            bool retVal = false;
            if (tryCount >= 3)
            {
                retVal = true;
            }
            return retVal;
        }

        private string SeparateNumbersForTTS(string digitsTxt)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < digitsTxt.Length; i++)
            {
                builder.Append(digitsTxt[i]).Append(" ");
            }
            return builder.ToString();
        }

        private bool ValidateArea(string digits, out Guid regionId)
        {
            regionId = Guid.Empty;
            bool retVal = false;
            if (digits.Length == 5)
            {
                DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
                regionId = regionDal.GetRegionIdByName(digits);
                if (regionId != Guid.Empty)
                {
                    retVal = true;
                }
            }
            return retVal;
        }

        private bool ValidatePhone(string phone)
        {
            phone = CleanPhone(phone);
            bool retVal = false;
            if (phone.Length == 10 && phone != "7378742833" && phone != "7372562533 " && phone != "7378656696" && phone.Substring(3, 3) != "555")
            {
                retVal = true;
            }
            return retVal;
        }

        private string CleanPhone(string phone)
        {
            string retVal = phone;
            if (retVal.StartsWith("+"))
            {
                retVal = retVal.Substring(1);
            }
            if (retVal.Length == 11 && retVal.StartsWith("1"))
            {
                retVal = retVal.Substring(1);
            }
            return retVal;
        }

        private Guid CreateIvrWidgetCall(string callSid, string from, string to, string callStatus, DML.Xrm.new_ivrwidgetcall.ProcessStatus processStatus, DML.Xrm.new_ivrwidgetnumber number)
        {
            DataAccessLayer.IvrWidgetCall dal = new NoProblem.Core.DataAccessLayer.IvrWidgetCall(XrmDataContext);
            DML.Xrm.new_ivrwidgetcall call = new NoProblem.Core.DataModel.Xrm.new_ivrwidgetcall();
            call.new_initialphone = from;
            call.new_ivrwidgetnumber = to;
            call.new_dialercallid = callSid;
            call.new_callendstatus = callStatus;
            call.new_processstatus = (int)processStatus;
            if (number != null)
            {
                call.new_originid = number.new_originid;
                call.new_primaryexpertiseid = number.new_primaryexpertiseid;
                call.new_ivrwidgetnumberid = number.new_ivrwidgetnumberid;
            }
            dal.Create(call);
            XrmDataContext.SaveChanges();
            return call.new_ivrwidgetcallid;
        }

        #endregion
    }
}
