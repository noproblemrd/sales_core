﻿using System;
using System.Collections.Generic;
using NoProblem.Core.DataModel.PublisherPortal;
using DML = NoProblem.Core.DataModel;
using System.Linq;

namespace NoProblem.Core.BusinessLogic.PublisherPortal
{
    public class PricingPackagesManager : XrmUserBase
    {
        #region Ctor
        public PricingPackagesManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Consts
        private const string ACTIVE = "Active";
        #endregion

        #region Public Methods

        public string GetActivePricingPackages(string PricingMethodId)
        {
            DataAccessLayer.PricingPackage dal = new NoProblem.Core.DataAccessLayer.PricingPackage(XrmDataContext);
            return dal.GetActivePricingPackages(PricingMethodId);
        }

        public GetAllPricingPackagesResponse GetAllPricingPackages(DML.Xrm.new_pricingpackage.PricingPackageSupplierType supplierType)
        {
            GetAllPricingPackagesResponse resonse = new GetAllPricingPackagesResponse();
            List<PricingPackageData> dataList = new List<PricingPackageData>();
            DataModel.XrmDataContext.ClearCache("new_pricingpackage");
            DataAccessLayer.PricingPackage packageDal = new NoProblem.Core.DataAccessLayer.PricingPackage(XrmDataContext);
            IEnumerable<DML.Xrm.new_pricingpackage> packagesList = packageDal.PricingPackagesByState(ACTIVE);
            if (supplierType != NoProblem.Core.DataModel.Xrm.new_pricingpackage.PricingPackageSupplierType.All)
            {
                packagesList = from pack in packagesList
                               where pack.new_suppliertype.HasValue == false
                               ||
                               (
                               pack.new_suppliertype.HasValue &&
                               (pack.new_suppliertype.Value == (int)supplierType || pack.new_suppliertype.Value == (int)DML.Xrm.new_pricingpackage.PricingPackageSupplierType.All)
                               )
                               select pack;
            }
            foreach (DML.Xrm.new_pricingpackage package in packagesList)
            {
                PricingPackageData data = new PricingPackageData();
                data.BonusPercent = GetPercentFromBonusAmount(package.new_pricingunitamount.Value, package.new_freepricingunitamount.HasValue ? package.new_freepricingunitamount.Value : 0);
                data.FromAmount = package.new_pricingunitamount.HasValue ? package.new_pricingunitamount.Value : 0;
                data.Name = package.new_name;
                data.Number = package.new_number;
                data.PricingPackageId = package.new_pricingpackageid;
                data.IsActive = package.statecode == ACTIVE;
                data.SupplierType = 
                    package.new_suppliertype.HasValue == false ?
                    DML.Xrm.new_pricingpackage.PricingPackageSupplierType.All :
                    (DML.Xrm.new_pricingpackage.PricingPackageSupplierType)package.new_suppliertype.Value;
                dataList.Add(data);
            }
            NoProblem.Core.DataAccessLayer.PricingPackageDataComparer comparer = new NoProblem.Core.DataAccessLayer.PricingPackageDataComparer();
            dataList.Sort(comparer);
            resonse.Packages = dataList;
            return resonse;
        }

        public CreateNewPricingPackageResponse CreateNewPricingPackage(CreateNewPricingPackageRequest request)
        {
            DML.Xrm.new_pricingpackage pricingPackage = new DML.Xrm.new_pricingpackage();
            pricingPackage.new_name = request.Name;
            pricingPackage.new_pricingunitamount = request.FromAmount;
            pricingPackage.new_freepricingunitamount = GetBonusAmountFromPercent(request.FromAmount, request.BonusPercent);
            pricingPackage.new_paymentmethodid = GetDefaultPaymentMethod();
            pricingPackage.new_pricingmethodid = GetDefaultPricingMethod();
            pricingPackage.new_suppliertype = (int)request.SupplierType;
            DataAccessLayer.PricingPackage packageAccess = new NoProblem.Core.DataAccessLayer.PricingPackage(XrmDataContext);
            packageAccess.Create(pricingPackage);
            XrmDataContext.SaveChanges();
            CreateNewPricingPackageResponse response = new CreateNewPricingPackageResponse();
            response.PricingPackageId = pricingPackage.new_pricingpackageid;
            return response;
        }

        public void UpdatePricingPackage(UpdatePricingPackageRequest request)
        {
            DataAccessLayer.PricingPackage packagesAccess = new NoProblem.Core.DataAccessLayer.PricingPackage(XrmDataContext);
            DML.Xrm.new_pricingpackage package = packagesAccess.Retrieve(request.PricingPackageId);
            bool isDirty = false;
            if (request.Name != package.new_name)
            {
                package.new_name = request.Name;
                isDirty = true;
            }
            if (request.FromAmount != package.new_pricingunitamount)
            {
                package.new_pricingunitamount = request.FromAmount;
                isDirty = true;
            }
            int bonusAmount = GetBonusAmountFromPercent(package.new_pricingunitamount.Value, request.BonusPercent);
            if (bonusAmount != package.new_freepricingunitamount)
            {
                package.new_freepricingunitamount = bonusAmount;
                isDirty = true;
            }
            if (package.new_suppliertype.HasValue == false
                || package.new_suppliertype.Value != (int)request.SupplierType)
            {
                package.new_suppliertype = (int)request.SupplierType;
                isDirty = true;
            }
            if (isDirty)
            {
                packagesAccess.Update(package);
                XrmDataContext.SaveChanges();
            }
        }

        public void SetPricingPackageState(SetPricingPackageStateRequest request)
        {
            DataAccessLayer.PricingPackage pricingPackageAccess = new NoProblem.Core.DataAccessLayer.PricingPackage(XrmDataContext);
            DML.Xrm.new_pricingpackage package = pricingPackageAccess.Retrieve(request.PricingPackageId);
            pricingPackageAccess.SetState(package, request.IsActive);
        }

        #endregion

        #region Private Methods

        private Guid? GetDefaultPaymentMethod()
        {
            DataAccessLayer.PaymentMethod paymentMethodAccess = new NoProblem.Core.DataAccessLayer.PaymentMethod(XrmDataContext);
            DML.Xrm.new_paymentmethod defaultPaymentMethod = paymentMethodAccess.GetDefaultPaymentMethod();
            Guid? retVal = null;
            if (defaultPaymentMethod != null)
            {
                retVal = defaultPaymentMethod.new_paymentmethodid;
            }
            return retVal;
        }

        private Guid? GetDefaultPricingMethod()
        {
            DataAccessLayer.PricingMethod pricingMethodAccess = new NoProblem.Core.DataAccessLayer.PricingMethod(XrmDataContext);
            DML.Xrm.new_pricingmethod defaultMethod = pricingMethodAccess.GetDefaultPricingMethod();
            Guid? retVal = null;
            if (defaultMethod != null)
            {
                retVal = defaultMethod.new_pricingmethodid;
            }
            return retVal;
        }

        private int GetBonusAmountFromPercent(int amount, int percent)
        {
            int retVal = (amount * percent) / 100;
            return retVal;
        }

        private int GetPercentFromBonusAmount(int amount, int bonus)
        {
            int retVal = 0;
            if (amount != 0)
            {
                retVal = (bonus * 100) / amount;
            }
            return retVal;
        }

        #endregion
    }
}
