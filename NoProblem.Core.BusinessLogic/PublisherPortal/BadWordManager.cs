﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.PublisherPortal.BadWords;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.PublisherPortal
{
    public class BadWordManager : XrmUserBase
    {
        #region Ctor
        public BadWordManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods
        public CreateBadWordResponse CreateBadWord(CreateBadWordRequest request)
        {
            CreateBadWordResponse response = new CreateBadWordResponse();
            if (string.IsNullOrEmpty(request.Word))
            {
                response.Status = CreateBadWordStatus.WordIsNullOrEmpty;
            }
            else
            {
                bool exists = CheckIsBadWord(request.Word);
                if (exists)
                {
                    response.Status = CreateBadWordStatus.WordAlreadyExists;
                }
                else
                {
                    DML.Xrm.new_badword badWord = new NoProblem.Core.DataModel.Xrm.new_badword();
                    badWord.new_name = request.Word;
                    DataAccessLayer.BadWord badWordDAL = new NoProblem.Core.DataAccessLayer.BadWord(XrmDataContext);
                    badWordDAL.Create(badWord);
                    XrmDataContext.SaveChanges();
                    response.Status = CreateBadWordStatus.OK;
                    response.BadWordId = badWord.new_badwordid;
                }
            }
            return response;
        }

        public GetAllBadWordsResponse GetAllBadWords()
        {
            DataAccessLayer.BadWord badWordDal = new NoProblem.Core.DataAccessLayer.BadWord(XrmDataContext);
            IEnumerable<DML.Xrm.new_badword> allBadWords = badWordDal.GetAllBadWords();
            List<BadWordData> dataList = new List<BadWordData>();
            foreach (DML.Xrm.new_badword word in allBadWords)
            {
                BadWordData data = new BadWordData();
                data.BadWordId = word.new_badwordid;
                data.Word = word.new_name;
                dataList.Add(data);
            }
            GetAllBadWordsResponse response = new GetAllBadWordsResponse();
            response.BadWords = dataList;
            return response;
        }

        public void DeleteBadWord(DeleteBadWordRequest request)
        {
            DataAccessLayer.BadWord badWordDal = new NoProblem.Core.DataAccessLayer.BadWord(XrmDataContext);
            DML.Xrm.new_badword badWord = new NoProblem.Core.DataModel.Xrm.new_badword(XrmDataContext);
            badWord.new_badwordid = request.BadWordId;
            badWordDal.Delete(badWord);
            XrmDataContext.SaveChanges();
        }

        /// <summary>
        /// Check the given string for bad words and removes all bad words found from it.
        /// </summary>
        /// <param name="description"></param>
        /// <returns>If bad words were found or not.</returns>
        public bool CheckForBadWords(string description)
        {
            return CheckForBadWordsMultiWords(description);

            //this code only supports bad word that contain one word:

            //LogUtils.MyHandle.WriteToLog(10, "CheckForBadWords started. original string: {0}", description);
            //if (string.IsNullOrEmpty(description))
            //{
            //    return false;
            //}
            //string[] words = description.Split(' ', '\n', '\r', '\t', ',', '.', ';', '?', '!', ':');
            //bool retVal = false;
            //foreach (string word in words)
            //{
            //    if (CheckIsBadWord(word))
            //    {
            //        retVal = true;
            //        break;
            //    }
            //}
            //LogUtils.MyHandle.WriteToLog(10, "CheckForBadWords ending.");
            //return retVal;
        }

        #endregion

        #region Private Methods

        private bool CheckForBadWordsMultiWords(string description)
        {
            if (string.IsNullOrEmpty(description))
            {
                return false;
            }
            bool retVal = false;
            DataAccessLayer.BadWord badWordDAL = new NoProblem.Core.DataAccessLayer.BadWord(XrmDataContext);
            IEnumerable<DML.Xrm.new_badword> allBadWords = badWordDAL.GetAllBadWords();
            foreach (DML.Xrm.new_badword word in allBadWords)
            {
                string badWord = word.new_name;
                int index = description.IndexOf(badWord, 0, StringComparison.OrdinalIgnoreCase);
                while (index != -1)
                {
                    string before = index > 0 ? description.Substring(index - 1, 1) : " ";
                    int indexAfter = index + badWord.Length;
                    string after = description.Length > indexAfter ? description.Substring(index + badWord.Length, 1) : " ";
                    //' ', '\n', '\r', '\t', ',', '.', ';', '?', '!', ':'
                    if ((before == " " || before == "\n" || before == "\r" || before == "\t" || before == "," || before == "." || before == ";" || before == "?" || before == "!" || before == ":" || before == "\"" || before == "'") 
                        &&
                        (after == " " || after == "\n" || after == "\r" || after == "\t" || after == "," || after == "." || after == ";" || after == "?" || after == "!" || after == ":" || after == "\"" || after == "'"))
                    {
                        retVal = true;
                        break;
                    }
                    else
                    {
                        index = description.IndexOf(badWord, indexAfter);
                    }
                }
                if (retVal == true)
                {
                    break;
                }
            }
            return retVal;
        }

        private bool CheckIsBadWord(string word)
        {
            DataAccessLayer.BadWord badWordDal = new NoProblem.Core.DataAccessLayer.BadWord(XrmDataContext);
            DML.Xrm.new_badword existingBadWord = badWordDal.GetBadWordByName(word);
            return existingBadWord != null;
        } 

        #endregion
    }
}
