﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: Effect.Onecall.BusinessLogic.PublisherPortal.ConsumersManager
//  File: ConsumersManager.cs
//  Description: Manages all methods for the consumer module.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 25/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.PublisherPortal;
using NoProblem.Core.BusinessLogic.Utils;
using NoProblem.Core.DataModel.Case;
using NoProblem.Core.DataModel.Expertise;
using NoProblem.Core.DataModel.SupplierModel.Mobile;
using NoProblem.Core.DataModel.Xrm;
using System.Data.SqlClient;
using DML = NoProblem.Core.DataModel;
using ClipCall.DomainModel.Entities.VideoInvitation.Response;

namespace NoProblem.Core.BusinessLogic.PublisherPortal
{
    public class ConsumersManager : XrmUserBase
    {
        #region Ctor
        public ConsumersManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        public SearchBlackListResponse SearchBlackList(SearchBlackListRequest request)
        {
            DataAccessLayer.Contact contactDAL = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            IEnumerable<string> phonesFound = contactDAL.SearchBlackListByMobilePhone(request.PhoneNumber);
            SearchBlackListResponse response = new SearchBlackListResponse();
            response.PhoneNumbers = new List<string>(phonesFound);
            return response;
        }

        public SetBlackListStatusResponse SetBlackListStatus(SetBlackListStatusRequest request)
        {
            DML.Xrm.contact consumer = null;
            if (string.IsNullOrEmpty(request.PhoneNumber))
            {
                consumer = FindConsumerByIncidentId(request.IncidentId);
            }
            else
            {
                DataAccessLayer.Contact contactAccess = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
                consumer = contactAccess.GetContactByPhoneNumber(request.PhoneNumber);
                if (consumer == null)
                {
                    consumer = new NoProblem.Core.DataModel.Xrm.contact();
                    consumer.mobilephone = request.PhoneNumber;
                    consumer.firstname = request.PhoneNumber;
                    contactAccess.Create(consumer);
                    XrmDataContext.SaveChanges();
                }
            }
            bool changeMade = SetBlackListStatus(consumer, request.BlackListStatusToSet);
            SetBlackListStatusResponse response = new SetBlackListStatusResponse();
            response.Status = changeMade ? BlackListStatus.OK : BlackListStatus.NoChange;
            return response;
        }

        public bool SetBlackListStatus(DML.Xrm.contact contact, bool blackListStatusToSet)
        {
            if (contact.new_isanonymouscontact.HasValue == true && contact.new_isanonymouscontact.Value == true)
            {
                return false;
            }
            bool needsUpdate = false;
            if (blackListStatusToSet == true)
            {
                if (contact.new_blaklist.HasValue == false || contact.new_blaklist.Value == false)
                {
                    contact.new_blaklist = true;
                    needsUpdate = true;
                }
            }
            else
            {
                if (contact.new_blaklist.HasValue == true && contact.new_blaklist.Value == true)
                {
                    contact.new_blaklist = false;
                    needsUpdate = true;
                }
            }
            if (needsUpdate)
            {
                DataAccessLayer.Contact contactAccess = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
                contactAccess.Update(contact);
                XrmDataContext.SaveChanges();
            }
            return needsUpdate;
        }

        /// <summary>
        /// Searched for consumers (contacts) that have the request link in their name or phone.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchConsumersResponse SearchConsumers(SearchConsumersRequest request)
        {
            DataAccessLayer.Contact contactAccess = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            var contactsFound = contactAccess.SearchContacts(request.SearchParameter);
            SearchConsumersResponse response = new SearchConsumersResponse();
            response.Consumers = contactsFound;
            return response;
        }

        /// <summary>
        /// Get the general info of the consumer.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetConsumerInfoResponse GetConsumerInfo(GetConsumerInfoRequest request)
        {

            GetConsumerInfoResponse response = new GetConsumerInfoResponse();
            DataAccessLayer.Contact contactAccess = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            DML.Xrm.contact contact = contactAccess.Retrieve(request.ContactId);
            if (contact != null)
            {
                response.Email = contact.emailaddress1;
                response.MobilePhone = contact.mobilephone;
                response.FirstName = contact.firstname;
                response.LastName = contact.lastname;
                response.NumberOfRequests = contactAccess.GetNumberOfIncidentByContact(contact.contactid);
                response.PhoneNumber1 = contact.telephone1;
                response.ConsumerNumber = contact.new_consumernumber;
                response.IsBlackList = contact.new_blaklist.HasValue ? contact.new_blaklist.Value : false;
            }
            return response;
        }

        /// <summary>
        /// Updates the general info of a consumer.
        /// </summary>
        /// <param name="request"></param>
        public void UpdateConsumerInfo(UpdateConsumerInfoRequest request)
        {
            DataAccessLayer.Contact contactAccess = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            DML.Xrm.contact contact = contactAccess.Retrieve(request.ContactId);
            bool isDirty = false;
            if (contact.new_blaklist.HasValue)
            {
                if (request.IsBlackList != contact.new_blaklist.Value)
                {
                    isDirty = true;
                    contact.new_blaklist = request.IsBlackList;
                }
            }
            else
            {
                isDirty = true;
                contact.new_blaklist = request.IsBlackList;
            }
            if (request.Email != contact.emailaddress1)
            {
                contact.emailaddress1 = request.Email;
                isDirty = true;
            }
            if (request.MobilePhone != contact.mobilephone)
            {
                contact.mobilephone = request.MobilePhone;
                isDirty = true;
            }
            if (request.FirstName != contact.firstname)
            {
                contact.firstname = request.FirstName;
                isDirty = true;
            }
            if (request.LastName != contact.lastname)
            {
                contact.lastname = request.LastName;
                isDirty = true;
            }
            if (request.PhoneNumber1 != contact.telephone1)
            {
                contact.telephone1 = request.PhoneNumber1;
                isDirty = true;
            }
            if (request.PhoneNumber2 != contact.telephone2)
            {
                contact.telephone2 = request.PhoneNumber2;
                isDirty = true;
            }
            if (isDirty == true)
            {
                contactAccess.Update(contact);
                XrmDataContext.SaveChanges();
            }
        }
        string GetConsumerIncidentsQuery = @"
SELECT I.Description, I.IncidentId, C.New_name CategoryName,
	I.TicketNumber, r.New_name RegionName, I.new_relaunchdate,
	I.New_VideoDuration, I.New_videoUrl, I.New_PreviewVideoPicUrl,
	I.New_ordered_providers, 
	(SELECT COUNT(*) 
	FROM dbo.New_incidentaccount IA WITH(NOLOCK)
	WHERE I.IncidentId = IA.new_incidentid and IA.statuscode in(10))--closed
		NumberOfSuppliersContacted,		
    I.StatusCode 'status',
    I.New_reviewStatus,
    I.CaseTypeCode
FROM dbo.Incident I WITH(NOLOCK)
	INNER JOIN dbo.New_primaryexpertiseExtensionBase C  WITH(NOLOCK)
		on I.new_primaryexpertiseid = C.New_primaryexpertiseId
	LEFT JOIN dbo.New_regionExtensionBase R WITH(NOLOCK)
		on I.new_regionid = R.New_regionId
WHERE I.ContactId = @ContactId
    and I.DeletionStateCode = 0
    and I.CaseTypeCode in (200004,200005) --Mobile video,VideoChat
    {WHERE_STATUS}
ORDER BY I.new_relaunchdate desc";
        /// <summary>
        /// Get all the incidents of a consumer.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetConsumerIncidentsResponse GetConsumerIncidents(GetCustomerLeadsRequest request)
        {
            List<ConsumerIncidentData> incidents = new List<ConsumerIncidentData>();
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                string query = GetConsumerIncidentsQuery.Replace("{WHERE_STATUS}", GetIncidentStatusForWhereSql(request.LeadStatus));
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@ContactId", request.CustomerId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ConsumerIncidentData data = new ConsumerIncidentData();
                        int incident_status = (int)reader["status"];
                        NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode caseTypeCode = (NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode)((int)reader["CaseTypeCode"]);
                        data.Description = reader["Description"] == DBNull.Value ? null : (string)reader["Description"];
                        data.ExpertiseName = reader["CategoryName"] == DBNull.Value ? null : (string)reader["CategoryName"];
                        data.IncidentId = (Guid)reader["IncidentId"];
                        data.IncidentNumber = (string)reader["TicketNumber"];
                        data.OrderedProviders = reader["New_ordered_providers"] == DBNull.Value ? 0 : (int)reader["New_ordered_providers"];
                        data.NumberOfSuppliersContacted = reader["NumberOfSuppliersContacted"] == DBNull.Value ? 0 : (int)reader["NumberOfSuppliersContacted"];
                        data.SupplierAvailabilityStatus = GetSupplierAvailabilityStatus(data.OrderedProviders.Value, incident_status);
                        data.RegionName = reader["RegionName"] == DBNull.Value ? null : (string)reader["RegionName"];
                        data.CreatedOn = (DateTime)reader["new_relaunchdate"];
                        data.CanReviveIncident = true;// GetCanReviveIncident(data.OrderedProviders.Value, incident_status); FIELD NOT IN USED
                        data.VideoUrl = reader["New_videoUrl"] == DBNull.Value ? null : (string)reader["New_videoUrl"];
                        data.PreviewVideoImageUrl = reader["New_PreviewVideoPicUrl"] == DBNull.Value ? null : (string)reader["New_PreviewVideoPicUrl"];
                        data.VideoDuration = reader["New_VideoDuration"] == DBNull.Value ? 0 : (int)reader["New_VideoDuration"];
                        data.ReviewStatus = reader["New_reviewStatus"] == DBNull.Value ? (int)NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus.WaitnigForReview :
                            (int)reader["New_reviewStatus"];
                        data.AvailableAction = GetAvailableAction(incident_status, data.ReviewStatus, caseTypeCode);
                        data.IncidentStatus = incident_status;
                        incidents.Add(data);
                    }
                    conn.Close();
                }
            }
            /*
            if (request.LeadStatus == LeadStatus.Dead)
            {
                incidents = incidents.Where(i => i.IncidentStatus == (int)DML.Xrm.incident.Status.BADWORD
                                                            || i.IncidentStatus == (int)DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER
                                                            || i.IncidentStatus == (int)DML.Xrm.incident.Status.MISSING_DETAILS
                                                            || i.IncidentStatus == (int)DML.Xrm.incident.Status.CLOSED_BY_CUSTOMER
                                                            || i.IncidentStatus == (int)DML.Xrm.incident.Status.WORK_DONE).ToList();

            }
            else if (request.LeadStatus == LeadStatus.Live)
            {
                incidents = incidents.Where(i => i.IncidentStatus == (int)DML.Xrm.incident.Status.NEW
                                                      || i.IncidentStatus == (int) DML.Xrm.incident.Status.WAITING ||
                                                      i.IncidentStatus == (int) DML.Xrm.incident.Status.RELAUNCH_BY_CUSTOMER).ToList();
            }            
            */
            if (request.PageNumber.HasValue && request.PageSize.HasValue)
            {
                incidents = incidents.Skip(request.PageNumber.Value * request.PageSize.Value).Take(request.PageSize.Value).ToList();
            }


            /*
            DataModel.XrmDataContext.ClearCache("new_incidentaccount");
            DataModel.XrmDataContext.ClearCache("incident");

            DataAccessLayer.Incident incidentAccess = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            IEnumerable<DML.Xrm.incident> incidentsFound = incidentAccess.GetIncidentsByContact(request.CustomerId);

             if (request.LeadStatus == LeadStatus.Dead)
             {
                 incidentsFound = incidentsFound.Where(i => i.statuscode == (int) DML.Xrm.incident.Status.BADWORD
                                                             || i.statuscode == (int)DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER
                                                             || i.statuscode == (int)DML.Xrm.incident.Status.MISSING_DETAILS
                                                             || i.statuscode == (int)DML.Xrm.incident.Status.CLOSED_BY_CUSTOMER
                                                             || i.statuscode == (int)DML.Xrm.incident.Status.WORK_DONE);

             }
             else if (request.LeadStatus == LeadStatus.Live)
             {
                 incidentsFound = incidentsFound.Where(i => i.statuscode == (int)DML.Xrm.incident.Status.NEW
                                                             || i.statuscode == (int)DML.Xrm.incident.Status.WAITING
                                                             || i.statuscode == (int)DML.Xrm.incident.Status.RELAUNCH_BY_CUSTOMER);
             }

            if (request.PageNumber.HasValue && request.PageSize.HasValue)
            {
                incidentsFound = incidentsFound.Skip(request.PageNumber.Value * request.PageSize.Value).Take(request.PageSize.Value);
            }

             incidentsFound = incidentsFound.OrderByDescending(i => i.createdon);

            DataAccessLayer.Region regionAccess = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.PrimaryExpertise primaryExpertiseAccess = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);

            List<incident> foundIncidents = incidentsFound.ToList();

            List<ConsumerIncidentData> incidents = new List<ConsumerIncidentData>();
            foreach (incident lead in foundIncidents)
            {
                ConsumerIncidentData data = new ConsumerIncidentData
                {
                    Description = lead.description,
                    ExpertiseName = GetExpertiseNameFromIncident(lead, primaryExpertiseAccess),
                    IncidentId = lead.incidentid,
                    IncidentNumber = lead.ticketnumber,
                    NumberOfSuppliersContacted = GetNumberOfSuppliersContacted(lead),
                    SupplierAvailabilityStatus = GetSupplierAvailabilityStatus(lead),
                    AvailableAction = GetAvailableAction(lead),
                    OrderedProviders = lead.new_ordered_providers,
                    RegionName = GetRegionName(lead.new_regionid, regionAccess),
                    CreatedOn = lead.createdon ?? DateTime.MinValue,
                    CanReviveIncident = GetCanReviveIncident(lead),
                    VideoUrl = lead.new_videourl,
                    PreviewVideoImageUrl = lead.new_previewvideopicurl,
                    VideoDuration = lead.new_videoduration
                };
                incidents.Add(data);
            }
            * */
            GetConsumerIncidentsResponse response = new GetConsumerIncidentsResponse();
            response.ConsumerIncidents = incidents;
            return response;

        }
        string GetIncidentStatusForWhereSql(LeadStatus ls)
        {
            string result = " and I.StatusCode in(";//200014,200002,3,200019,1)
            if (ls == LeadStatus.Dead)
            {
                /*
                result += (int)DML.Xrm.incident.Status.BADWORD + "," + (int)DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER + "," +
                    (int)DML.Xrm.incident.Status.MISSING_DETAILS + "," + (int)DML.Xrm.incident.Status.CLOSED_BY_CUSTOMER + "," +
                    (int)DML.Xrm.incident.Status.WORK_DONE;
                 * */
                result += (int)DML.Xrm.incident.Status.BADWORD + "," + (int)DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER + "," +
                    (int)DML.Xrm.incident.Status.MISSING_DETAILS + "," + (int)DML.Xrm.incident.Status.WORK_DONE;
            }
            else if (ls == LeadStatus.Live)
            {
                result += (int)DML.Xrm.incident.Status.NEW + "," + (int)DML.Xrm.incident.Status.WAITING + "," +
                    (int)DML.Xrm.incident.Status.AFTER_AUCTION + "," + (int)DML.Xrm.incident.Status.IN_REVIEW;
            }
            else
                return string.Empty;
            result += ") ";
            return result;
        }
        /// <summary>
        /// Get the details and recordings of an incident.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetConsumerIncidentDataResponse GetConsumerIncidentData(GetConsumerIncidentDataRequest request)
        {
            DataModel.XrmDataContext.ClearCache("new_incidentaccount");
            DataModel.XrmDataContext.ClearCache("incident");

            DataAccessLayer.Incident incidentAccess = new DataAccessLayer.Incident(XrmDataContext);
            incident incident = incidentAccess.Retrieve(request.LeadId);
            CustomerBL.LoveManager manager = new CustomerBL.LoveManager(null);

            List<ConsumerIncidentAccountData> incidentAccounts = new List<ConsumerIncidentAccountData>();
            NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.LogoManager logoManager = new NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.LogoManager();
            foreach (DML.Xrm.new_incidentaccount incidentAccount in incident.new_incident_new_incidentaccount)
            {

                if (!incidentAccount.new_accountid.HasValue)
                    continue;


                if (!incidentAccount.IsConnectedSuccessfullyWithSupplier)
                    continue;

                account advertiserAccount = incidentAccount.new_account_new_incidentaccount;
                DataModel.Consumer.LoveResponse lr = manager.GetLoves(new DataModel.Consumer.LoveRequest { ContactId = request.CustomerId, SupplierId = incidentAccount.new_accountid.Value });
                ConsumerIncidentAccountData data = new ConsumerIncidentAccountData
                {
                    RecordFileLocation = incidentAccount.new_recordfilelocation,
                    SupplierId = incidentAccount.new_accountid.Value,
                    SupplierName = advertiserAccount.name,
                    AllowedToHear = IsAllowedToHear(incidentAccount, request.CustomerId),
                    LeadAccountId = incidentAccount.new_incidentaccountid,
                    // ImageUrl = logoManager.GetLogoImageUrl(incidentAccount.new_accountid.Value, advertiserAccount.new_businessimageurl),
                    ImageUrl = advertiserAccount.new_businessimageurl,
                    GoogleRating = advertiserAccount.new_googlerating,
                    YelpRating = advertiserAccount.new_yelprating,
                    MyLove = lr.MyLove,
                    AllLove = lr.AllLove,
                    GoogleUrl = advertiserAccount.new_googleurl,
                    YelpUrl = advertiserAccount.new_yelpurl
                };


                incidentAccounts.Add(data);
            }
            GetConsumerIncidentDataResponse response = new GetConsumerIncidentDataResponse
            {
                IncidentAccounts = incidentAccounts
            };
            return response;
        }

        /// <summary>
        /// Gets all the notes of a consumer.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetConsumerNotesResponse GetConsumerNotes(GetConsumerNotesRequest request)
        {
            DataAccessLayer.ConsumerNote consumerNoteAccess = new NoProblem.Core.DataAccessLayer.ConsumerNote(XrmDataContext);
            DataAccessLayer.AccountRepository accountRepositoryAccess = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            //DataAccessLayer.ConfigurationSettings settingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            //int publisherTimeZone = int.Parse(settingsAccess.GetConfigurationSettingValue(ConfigurationKeys.PUBLISHER_TIME_ZONE));
            IEnumerable<DML.Xrm.new_consumernote> notesFound = consumerNoteAccess.GetNotesByContactId(request.ContactId);
            List<ConsumerNoteData> notes = (from note in notesFound
                                            orderby note.createdon descending
                                            select
                                            new ConsumerNoteData
                                            {
                                                CreatedOn = FixDateToLocal(note.createdon.Value), //ConvertUtils.GetLocalFromUtc(XrmDataContext, note.createdon.Value, publisherTimeZone),
                                                NoteId = note.new_consumernoteid,
                                                Description = note.new_description,
                                                UserId = note.new_userid.Value,
                                                UserName = GetSupplierName(note.new_userid.Value, accountRepositoryAccess)
                                            }).ToList<ConsumerNoteData>();
            GetConsumerNotesResponse response = new GetConsumerNotesResponse();
            response.Notes = notes;
            return response;
        }

        /// <summary>
        /// Creates a new note for the consumer.
        /// </summary>
        /// <param name="request"></param>
        public void CreateConsumerNote(CreateConsumerNoteRequest request)
        {
            DML.Xrm.new_consumernote note = new DML.Xrm.new_consumernote();
            note.new_description = request.Description;
            note.new_userid = request.UserId;
            note.new_contactid = request.ContactId;
            DataAccessLayer.ConsumerNote noteDal = new NoProblem.Core.DataAccessLayer.ConsumerNote(XrmDataContext);
            noteDal.Create(note);
            XrmDataContext.SaveChanges();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Checks if the publisher is allowed to hear this call's recording.
        /// </summary>
        /// <param name="incidentAccount"></param>
        /// <returns>true if the there exist a recording and also refund has been asked for this call</returns>
        private bool IsAllowedToHear(new_incidentaccount incidentAccount, Guid userId)
        {
            bool retVal = false;
            if (string.IsNullOrEmpty(incidentAccount.new_recordfilelocation) == false)
            {
                if (incidentAccount.new_refundstatus.HasValue)
                {
                    retVal = true;
                }
                else if (userId != Guid.Empty)
                {
                    DataAccessLayer.AccountRepository accDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                    var user = accDal.Retrieve(userId);
                    if (user != null && user.new_canhearallrecordings.HasValue && user.new_canhearallrecordings.Value)
                    {
                        retVal = true;
                    }
                }
            }
            return retVal;
        }

        private contact FindConsumerByIncidentId(Guid incidentId)
        {
            DataAccessLayer.Incident incidentAccess = new DataAccessLayer.Incident(XrmDataContext);
            DML.Xrm.incident incident = incidentAccess.Retrieve(incidentId);
            Guid contactId = incident.customerid.Value;
            NoProblem.Core.DataAccessLayer.Contact contactAccess = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            DML.Xrm.contact contact = contactAccess.Retrieve(contactId);
            return contact;
        }

        private string GetSupplierName(Guid supplierId, NoProblem.Core.DataAccessLayer.AccountRepository accountRepositoryAccess)
        {
            DML.Xrm.account supplier = accountRepositoryAccess.Retrieve(supplierId);
            string retVal = supplier.name;
            return retVal;
        }

        private string GetRegionName(Guid? regionId, DataAccessLayer.Region regionAccess)
        {

            string retVal = string.Empty;
            if (regionId.HasValue)
            {
                DML.Xrm.new_region region = regionAccess.Retrieve(regionId.Value);
                retVal = region.new_name;
            }
            return retVal;
            //   return regionAccess.GetCityState(regionId);
        }


        private SupplierAvailabilityStatus GetSupplierAvailabilityStatus(int new_ordered_providers, int incidentStatus)
        {

            //        var status = (incident.Status) incidentStatus;
            /*
            if (status == incident.Status.CLOSED_BY_CUSTOMER)
            {
                if (new_ordered_providers > 0)
                {
                    return SupplierAvailabilityStatus.AVAILABLE_SUPPLIERS;
                }

                return SupplierAvailabilityStatus.NO_AVAILABLE_SUPPLIERS;
            }
            */
            /*
            if (status == incident.Status.RELAUNCH_BY_CUSTOMER)
            {
                return SupplierAvailabilityStatus.LOCATING_SUPPLIER;
            }
            */
            if (new_ordered_providers > 0)
                return SupplierAvailabilityStatus.AVAILABLE_SUPPLIERS;

            //     var incidentStatus = (DML.Xrm.incident.Status)incident.statuscode;
            if (incidentStatus == (int)DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER)
                return SupplierAvailabilityStatus.NO_AVAILABLE_SUPPLIERS;

            //default is locating supplier
            return SupplierAvailabilityStatus.LOCATING_SUPPLIER;
        }

        private eAvailableAction GetAvailableAction(int incidentStatus, int reviewStatus, incident.CaseTypeCode caseTypeCode)
        {
            if (caseTypeCode == incident.CaseTypeCode.VideoChat)
                return eAvailableAction.None;
            if (incident.Is_Invalid(incidentStatus))
                return eAvailableAction.None;


            if (incident.Is_Open(incidentStatus))
                return eAvailableAction.NoMoreCalls;


            if (incident.Is_Closed(incidentStatus))// && lead.new_ordered_providers < 6)
            {
                NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus rs = (NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus)reviewStatus;
                if (rs == incident.IncidentReviewStatus.Failed)
                    return eAvailableAction.None;
                return eAvailableAction.Relaunch;
            }


            return eAvailableAction.None;
        }
        /*
        private bool GetCanReviveIncident(incident incident)
        {
            if (incident.new_ordered_providers > 6)
                return false;

            if (incident.statuscode == null)
                return false;

            var incidentStatus = (incident.Status)incident.statuscode;
            if (incidentStatus == incident.Status.BADWORD)
                return false;

            if (incidentStatus != incident.Status.NEW && incidentStatus != incident.Status.WAITING)
                return true;

            return false;
        }
         * */
        /*
        private bool GetCanReviveIncident(int new_ordered_providers, int Incident_Status)
        {
            if (new_ordered_providers > 6)
                return false;
            
            var incidentStatus = (incident.Status)Incident_Status;
            if (incidentStatus == incident.Status.BADWORD)
                return false;

            if (incidentStatus != incident.Status.NEW && incidentStatus != incident.Status.WAITING)
                return true;

            return false;
        }
         * */
        private int GetNumberOfSuppliersContacted(DML.Xrm.incident incident)
        {

            int totalNumberOfAdvertisersContacted = incident.new_incident_new_incidentaccount.Count(ia => ia.IsConnectedSuccessfullyWithSupplier);
            return totalNumberOfAdvertisersContacted;
        }

        private string GetExpertiseNameFromIncident(DML.Xrm.incident incident, DataAccessLayer.PrimaryExpertise primaryExpertiseAccess/*, DataAccessLayer.SecondaryExpertise secondaryExpertiseAccess*/)
        {
            string retVal = string.Empty;
            if (incident.new_primaryexpertiseid.HasValue)
            {
                DML.Xrm.new_primaryexpertise primary = primaryExpertiseAccess.Retrieve(incident.new_primaryexpertiseid.Value);
                retVal = primary.new_name;
            }
            return retVal;
        }

        #endregion

        public RelaunchLeadResponse RelaunchLead(RelaunchLeadRequest request, NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.TypeExecuteUser typeExecuteUser)
        {

            if (!IsIncidentBelongsConsumer(request.LeadId, request.CustomerId))
            {
                RelaunchLeadResponse response = new RelaunchLeadResponse();
                response.IsSuccess = false;
                return response;
            }
            return RelaunchLead(request.LeadId, typeExecuteUser);
        }
        public RelaunchLeadResponse RelaunchLead(Guid incidentId, NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.TypeExecuteUser typeExecuteUser)
        {

            RelaunchLeadResponse response = new RelaunchLeadResponse();

            /*

            if (!IsIncidentBelongsConsumer(request.LeadId, request.CustomerId))
            {
                response.IsSuccess = false;
                return response;
            }
             * */
            DML.Xrm.incident.Status IncidentStatus;
            NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager srm = new ServiceRequest.ServiceRequestManager(null);
            if (srm.IsMobileIncidentOpen(incidentId, out IncidentStatus))
            {
                response.IsSuccess = false;
                response.requestStatus = DML.ClipCall.Request.eRequestsStatus.LIVE;
                return response;
            }
            incident.IncidentReviewStatus reviewStatus = srm.GetIncidentReviewStatus(incidentId);
            if (reviewStatus == incident.IncidentReviewStatus.Failed)
            {
                response.IsSuccess = false;
                response.requestStatus = DML.ClipCall.Request.eRequestsStatus.CLOSE;
                return response;
            }
            switch (IncidentStatus)
            {
                case (DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER):
                case (DML.Xrm.incident.Status.WORK_DONE):
                    response.requestStatus = srm.RelaunchIncident(incidentId, typeExecuteUser, null);
                    response.IsSuccess = true;
                    break;
                default:
                    response.IsSuccess = false;
                    response.requestStatus = DML.ClipCall.Request.eRequestsStatus.CLOSE;
                    break;


            }
            return response;
        }

        public CloseLeadResponse CloseLead(CloseLeadRequest request)
        {
            bool IsSuccess = false;

            // return new CloseLeadResponse { IsSuccess = result > 0 };

            NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager srm = new ServiceRequest.ServiceRequestManager(null);
            DML.Xrm.incident.Status IncidentStatus;
            if (srm.IsMobileIncidentOpen(request.LeadId, out IncidentStatus))
            {
                IsSuccess = IsIncidentBelongsConsumer(request.LeadId, request.CustomerId);
            }
            else IsSuccess = false;
            if (IsSuccess)
                srm.CloseMobileIncident(request.LeadId, ServiceRequest.VideoRequests.TypeExecuteUser.Customer);
            return new CloseLeadResponse { IsSuccess = IsSuccess };
        }
        private bool IsIncidentBelongsConsumer(Guid incidentId, Guid consumerId)
        {
            int result;
            string command = "SELECT COUNT(*) FROM dbo.IncidentBase WHERE IncidentId = @IncidentId and ContactId = @ContactId";

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@IncidentId", incidentId);
                    cmd.Parameters.AddWithValue("@ContactId", consumerId);
                    result = (int)cmd.ExecuteScalar();
                }
                conn.Close();
            }
            return result > 0;
        }
        public GetCustomerAdvertiserAboutResponse GetCustomerAdvertiserAbout(GetCustomerAdvertiserAboutRequest request)
        {
            DataAccessLayer.AccountRepository incidentRepository = new DataAccessLayer.AccountRepository(XrmDataContext);

            account advertiserAccount = incidentRepository.Retrieve(request.AdvertiserId);
            if (advertiserAccount == null)
                throw new Exception(string.Format("Account not found for advertiser {0} and consumer {1}", request.AdvertiserId, request.CustomerId));

            var advertiserAboutData = new AdvertiserAboutData
            {
                BusinessDescription = advertiserAccount.description,
                BusinessName = advertiserAccount.name,
                FullAddress = advertiserAccount.new_fulladdress,
                PhoneNumber = advertiserAccount.telephone1,
                ContactName = advertiserAccount.new_fullname,
                Fax = advertiserAccount.fax,
                Email = advertiserAccount.emailaddress1,
                Website = advertiserAccount.websiteurl,
                NumberOfEmployees = advertiserAccount.numberofemployees,
                Latitude = advertiserAccount.new_latitude,
                Longitude = advertiserAccount.new_longitude,
                YelpRating = advertiserAccount.new_yelprating.HasValue ? advertiserAccount.new_yelprating.Value : 0,
                GoogleRating = advertiserAccount.new_googlerating.HasValue ? advertiserAccount.new_googlerating.Value : 0,
                //    Categories = advertiserAccount.new_account_primaryexpertise.Select(e => new Category
                Categories = advertiserAccount.new_account_new_accountexpertise.Where(e => e.new_primaryexpertiseid.HasValue).Select(e => new Category
                {
                    Name = e.new_new_primaryexpertise_new_accountexpertise.new_name,
                    Id = e.new_primaryexpertiseid.Value
                }).ToList()
            };

            var response = new GetCustomerAdvertiserAboutResponse
            {
                AboutData = advertiserAboutData
            };

            return response;

        }

        public GetAdvertiserCallHistoryResponse GetAdvertiserCallHistory(GetAdvertiserCallHistoryRequest request)
        {
            DataAccessLayer.IncidentAccount incidentAccountRepository = new DataAccessLayer.IncidentAccount(XrmDataContext);
            /*
            incident incident = incidentRepository.Retrieve(request.LeadId);
            if (incident == null)
                throw new Exception("lead not found " + request.LeadId);

            if (incident.customerid.Value != request.CustomerId)
                throw new Exception(string.Format("lead {0} is not associated with customer {1}", request.LeadId, request.CustomerId));

            
            new_incidentaccount incidentAccount = incident.new_incident_new_incidentaccount.SingleOrDefault(a => a.new_accountid == request.AdvertiserId);
             */
            new_incidentaccount incidentAccount = incidentAccountRepository.Retrieve(request.LeadAccountId);
            if (incidentAccount == null)
                throw new Exception("lead account was not found for account " + request.AdvertiserId);


            var callRecords = new List<CallRecordData>();

            string mainRecordingUrl = incidentAccount.new_recordfilelocation;
            int mainRecordingDuration = incidentAccount.new_recordduration ?? 0;
            if (!string.IsNullOrEmpty(mainRecordingUrl) && mainRecordingDuration > 0)
            {
                callRecords.Add(new CallRecordData
                {
                    CreateDate = incidentAccount.createdon ?? DateTime.MinValue,
                    CallRecordUrl = mainRecordingUrl,
                    Duration = mainRecordingDuration
                });
            }

            List<CallRecordData> additionalCalls = incidentAccount.new_incidentaccount_callrecording.Select(c => new CallRecordData
            {
                CreateDate = c.createdon ?? DateTime.MinValue,
                Duration = c.new_duration ?? 0,
                CallRecordUrl = c.new_url,
                //      Status = c.statuscodeLabel,
                Id = c.new_callrecordingid,
                Advertiser = null,//TODO
                Customer = null//TODO
            }).ToList();

            callRecords.AddRange(additionalCalls);

            callRecords = callRecords.OrderByDescending(x => x.CreateDate).ToList();

            var response = new GetAdvertiserCallHistoryResponse { CallRecords = callRecords };
            return response;
        }

        public virtual ConsumerIncidentData GetCustomerLead(Guid customerId, Guid leadId)
        {
            string query = @"
SELECT I.IncidentId, E.New_name Category, TicketNumber,
	I.New_ordered_providers, I.StatusCode, R.New_name Region,
	I.CreatedOn, I.New_VideoDuration, I.New_videoUrl,
	I.New_PreviewVideoPicUrl, I.New_reviewStatus, I.CustomerId, I.CaseTypeCode
FROM dbo.incident I WITH(NOLOCK)
	INNER JOIN dbo.New_primaryexpertiseExtensionBase E WITH(NOLOCK) on I.new_primaryexpertiseid = E.New_primaryexpertiseId
	INNER JOIN dbo.New_regionExtensionBase R WITH(NOLOCK) on I.new_regionid = R.New_regionId
WHERE I.IncidentId = @incidentId";
            Guid _customerId = Guid.Empty;
            ConsumerIncidentData data = null;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {

                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", leadId);
                    cmd.Parameters.AddWithValue("@customerId", customerId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        data = new ConsumerIncidentData();
                        data.ExpertiseName = (string)reader["Category"];
                        incident.CaseTypeCode caseTypeCode = (incident.CaseTypeCode)((int)reader["CaseTypeCode"]);
                        data.IncidentId = (Guid)reader["IncidentId"];
                        data.IncidentNumber = (string)reader["TicketNumber"];
                        data.IncidentStatus = (int)reader["StatusCode"];
                        data.OrderedProviders = reader["New_ordered_providers"] == DBNull.Value ? 0 : (int)reader["New_ordered_providers"];
                        // NumberOfSuppliersContacted = GetNumberOfSuppliersContacted(lead),
                        data.SupplierAvailabilityStatus = GetSupplierAvailabilityStatus(data.OrderedProviders.Value, data.IncidentStatus);
                        data.ReviewStatus = reader["New_reviewStatus"] == DBNull.Value ? (int)NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus.WaitnigForReview :
                            (int)reader["New_reviewStatus"];
                        data.AvailableAction = GetAvailableAction(data.IncidentStatus, data.ReviewStatus, caseTypeCode);
                        data.RegionName = (string)reader["Region"];
                        data.CreatedOn = (DateTime)reader["CreatedOn"];
                        data.CanReviveIncident = true;//GetCanReviveIncident(lead), NOT IN USED
                        data.VideoUrl = (string)reader["New_videoUrl"];
                        data.PreviewVideoImageUrl = (string)reader["New_PreviewVideoPicUrl"];
                        data.VideoDuration = (int)reader["New_VideoDuration"];
                        _customerId = (Guid)reader["CustomerId"];
                    }
                    conn.Close();

                }
            }
            if (data == null)
                throw new Exception(string.Format("lead {0} not found ", leadId));

            if (_customerId != customerId)
                throw new Exception(string.Format("requested lead: {0} doesn't belong to customer: {1} not found ", leadId, customerId));

            return data;
            /************** Replace this method cause timeout exception 13/12/2015
            DataModel.XrmDataContext.ClearCache("new_incidentaccount");
            DataModel.XrmDataContext.ClearCache("incident");
            DataAccessLayer.Incident incidentRepository = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            incident lead = incidentRepository.Retrieve(leadId);
            if (lead == null)
                throw new Exception(string.Format("lead {0} not found ", leadId));

            if (lead.customerid.Value != customerId)
                throw new Exception(string.Format("requested lead: {0} doesn't belong to customer: {1} not found ", leadId, customerId));


            DataAccessLayer.Region regionAccess = new DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.PrimaryExpertise primaryExpertiseAccess = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);

            var data = new ConsumerIncidentData
            {
                Description = lead.description,
                ExpertiseName = GetExpertiseNameFromIncident(lead, primaryExpertiseAccess),
                IncidentId = lead.incidentid,
                IncidentNumber = lead.ticketnumber,
                NumberOfSuppliersContacted = GetNumberOfSuppliersContacted(lead),
                SupplierAvailabilityStatus = GetSupplierAvailabilityStatus(lead.new_ordered_providers ?? 0, lead.statuscode ?? 0),
                AvailableAction = GetAvailableAction(lead.statuscode ?? 0),
                OrderedProviders = lead.new_ordered_providers,
                RegionName = GetRegionName(lead.new_regionid, regionAccess),
                CreatedOn = lead.createdon ?? DateTime.MinValue,
                CanReviveIncident = true,//GetCanReviveIncident(lead), FAILD NOT IN USED
                VideoUrl = lead.new_videourl,
                PreviewVideoImageUrl = lead.new_previewvideopicurl,
                VideoDuration = lead.new_videoduration
            };

            return data;
             * */
        }



        public void UpdateCustomerProfile(UpdateCustomerProfileRequest request)
        {
            DataAccessLayer.Contact customerRepository = new DataAccessLayer.Contact(XrmDataContext);
            contact contact = customerRepository.Retrieve(request.CustomerId);
            if (contact == null)
                throw new Exception(string.Format("Cannot update customer. customer with id {0} wasn't found.", request.CustomerId));

            if (!string.IsNullOrEmpty(request.Name))
            {
                string name = request.Name.Trim();
                int indexOfSpace = name.IndexOf(' ');
                if (indexOfSpace == -1)
                {
                    contact.firstname = name;
                    contact.lastname = null;
                }
                else
                {
                    contact.firstname = name.Substring(0, indexOfSpace).Trim();
                    contact.lastname = name.Substring(indexOfSpace, name.Length - indexOfSpace).Trim();
                }
            }

            if (!string.IsNullOrEmpty(request.Email))
            {
                contact.emailaddress1 = request.Email;
            }

            if (!string.IsNullOrEmpty(request.ImageUrl))
            {

                contact.new_profileimageurl = request.ImageUrl;
            }

            customerRepository.Update(contact);
            XrmDataContext.SaveChanges();
        }

        public CustomerProfileData GetCustomerProfile(Guid customerId)
        {
            DataAccessLayer.Contact customerRepository = new DataAccessLayer.Contact(XrmDataContext);
            contact contact = customerRepository.Retrieve(customerId);
            if (contact == null)
                throw new Exception(string.Format("Cannot get customer profile. customer with id {0} wasn't found.", customerId));

            var profile = new CustomerProfileData
            {
                CustomerId = customerId,
                Name = contact.fullname,
                ImageUrl = contact.new_profileimageurl,
                Email = contact.emailaddress1
            };

            return profile;
        }

        public void UpdateCustomerSettings(UpdateCustomerSettingsRequest request)
        {
            DataAccessLayer.Contact customerRepository = new DataAccessLayer.Contact(XrmDataContext);
            contact contact = customerRepository.Retrieve(request.CustomerId);
            if (contact == null)
                throw new Exception(string.Format("Cannot update customer settings. customer with id {0} wasn't found.", request.CustomerId));


            contact.new_disablecallrexording = !request.RecordMyCalls;
            customerRepository.Update(contact);
            XrmDataContext.SaveChanges();
        }

        public CustomerSettingsData GetCustomerSettings(Guid customerId)
        {
            DataAccessLayer.Contact customerRepository = new DataAccessLayer.Contact(XrmDataContext);
            contact contact = customerRepository.Retrieve(customerId);
            if (contact == null)
                throw new Exception(string.Format("Cannot get customer settings. customer with id {0} wasn't found.", customerId));

            return new CustomerSettingsData
            {
                //              DisableCallRecording = contact.new_disablecallrexording ?? true,
                RecordMyCalls = !contact.new_disablecallrexording.HasValue ? true : !contact.new_disablecallrexording.Value
            };
        }
        public List<GetFavoriteServiceProviderResponse> GetFavoriteServiceProvider(Guid customerId, Guid expertiseId)
        {
            NoProblem.Core.DataAccessLayer.FavoriteServiceProvider favorite = new DataAccessLayer.FavoriteServiceProvider(null);
            return favorite.GetFavoriteServiceProvider(customerId, expertiseId);
        }
    }
}
