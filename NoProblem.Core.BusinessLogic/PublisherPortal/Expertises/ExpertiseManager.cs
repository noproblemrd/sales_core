﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.PublisherPortal.MultipleExpertiseCreator;
using NoProblem.Core.DataModel.PublisherPortal;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Expertise;

namespace NoProblem.Core.BusinessLogic.PublisherPortal
{
    public class ExpertiseManager : XrmUserBase
    {
        #region Ctor
        public ExpertiseManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Consts

        private const string EXCEPTIONSAVINGEXPERTISE = "Exception occurred while saving expertise in line {0}. Exception message: {1}";
        private const string NAMEREPEATED = "A primary expertise with the same name already exists";
        private const string CODEREPEATED = "A primary expertise with the same code already exists";

        #endregion

        #region Public Methods

        public InsertMultipleExpertisesResponse InsertMultipleExpertises(InsertMultipleExpertisesRequest request)
        {
            List<PrimaryExpertiseData> newExpertises = request.Expertises;
            List<ExpertiseResponseData> responseDataList = new List<ExpertiseResponseData>();
            foreach (PrimaryExpertiseData data in newExpertises)
            {
                ExpertiseResponseData responseData = new ExpertiseResponseData();
                responseData.LineInExcel = data.LineInExcel;
                try
                {
                    DML.Result result = CreateNewPrimaryExpertise(data);
                    if (result.IsSuccess)
                    {
                        responseData.SavedOK = true;
                    }
                    else
                    {
                        responseData.SavedOK = false;
                        responseData.FailureReason = result.ToString();
                    }
                }
                catch (Exception exc)
                {
                    responseData.SavedOK = false;
                    responseData.FailureReason = string.Format(EXCEPTIONSAVINGEXPERTISE, data.LineInExcel, exc.Message);
                }
                responseDataList.Add(responseData);
            }
            InsertMultipleExpertisesResponse response = new InsertMultipleExpertisesResponse();
            response.ResponseData = responseDataList;
            return response;
        }

        public void DeleteExpertises(DeleteExpertisesRequest request)
        {
            if (request.ExpertiseIds == null)
            {
                throw new ArgumentNullException("ExpertiseIds");
            }
            DataAccessLayer.PrimaryExpertise primaryDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            bool allOk = true;
            foreach (Guid id in request.ExpertiseIds)
            {
                try
                {
                    DML.Xrm.new_primaryexpertise expertise = primaryDal.Retrieve(id);
                    foreach (var accExp in expertise.new_new_primaryexpertise_new_accountexpertise)
                    {
                        accExpDal.Delete(accExp);
                    }
                    primaryDal.Delete(expertise);
                    XrmDataContext.SaveChanges();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception was thrown while deleting expertise with id {0}, or one of its accountexpertises. Code continued to delete other expertises in the request.", id.ToString());
                    allOk = false;
                }
            }
            if (allOk == false)
            {
                throw new Exception("Failed to delete some of the expertises or their related entries. Please speak to the system administrator.");
            }
        }

        public HashSet<CategoryGroupContainer> GetAllCategoryGroups()
        {
            DataAccessLayer.CategoryGroup dal = new NoProblem.Core.DataAccessLayer.CategoryGroup(XrmDataContext);
            return dal.GetAllCategoryGroups();
        }

        public List<DML.PublisherPortal.GuidStringPair> GetAllCategories()
        {
            DataAccessLayer.CategoryGroup dal = new NoProblem.Core.DataAccessLayer.CategoryGroup(XrmDataContext);
            return dal.GetAllCategories();
        }

        public List<DML.PublisherPortal.GuidStringPair> GetCategoryGroup(Guid categoryId)
        {
            DataAccessLayer.CategoryGroup dal = new NoProblem.Core.DataAccessLayer.CategoryGroup(XrmDataContext);
            return dal.GetCategoryGroup(categoryId);
        }

        public List<GuidStringPair> GetCategoryGroup(string categoryName)
        {
            DataAccessLayer.CategoryGroup dal = new NoProblem.Core.DataAccessLayer.CategoryGroup(XrmDataContext);
            return dal.GetCategoryGroup(categoryName);
        }

        public List<ExpertiseInSpecialCategoryContainer> GetSpecialCategoryGroup(string connectivityName)
        {
            DataAccessLayer.CategoryGroup dal = new NoProblem.Core.DataAccessLayer.CategoryGroup(XrmDataContext);
            return dal.GetSpecialCategoryGroup(connectivityName);
        }

        #endregion

        #region Private Methods

        private NoProblem.Core.DataModel.Result CreateNewPrimaryExpertise(PrimaryExpertiseData data)
        {
            data.Name = data.Name.Trim();
            DML.Result result = ExpertiseValidator.ValidatePrimaryExpertiseData(data);
            if (result.IsSuccess)
            {
                DataAccessLayer.PrimaryExpertise primaryExpertiseDAL = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
                DML.Xrm.new_primaryexpertise primaryExpertise = primaryExpertiseDAL.GetPrimaryExpertiseByCode(data.Code);
                if (primaryExpertise != null)
                {
                    result.Type = NoProblem.Core.DataModel.Result.eResultType.Failure;
                    result.Messages.Add(CODEREPEATED);
                    return result;
                }
                primaryExpertise = primaryExpertiseDAL.GetPrimaryExpertiseByName(data.Name);
                if (primaryExpertise != null)
                {
                    result.Type = NoProblem.Core.DataModel.Result.eResultType.Failure;
                    result.Messages.Add(NAMEREPEATED);
                    return result;
                }
                primaryExpertise = new NoProblem.Core.DataModel.Xrm.new_primaryexpertise();
                primaryExpertise.new_name = data.Name;
                primaryExpertise.new_minprice = data.MinimalPrice;
                primaryExpertise.new_showcertificate = data.ShowCertificate;
                primaryExpertise.new_code = data.Code;
                primaryExpertise.new_isauction = data.IsAuction;
                primaryExpertiseDAL.Create(primaryExpertise);
                XrmDataContext.SaveChanges();
            }
            return result;
        }  

        #endregion

    }
}
