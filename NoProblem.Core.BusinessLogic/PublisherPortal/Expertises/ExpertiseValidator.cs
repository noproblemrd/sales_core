﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.PublisherPortal.MultipleExpertiseCreator;

namespace NoProblem.Core.BusinessLogic.PublisherPortal
{
    internal class ExpertiseValidator : ValidatorBase
    {
        internal static DataModel.Result ValidatePrimaryExpertiseData(PrimaryExpertiseData data)
        {
            DataModel.Result result = new NoProblem.Core.DataModel.Result();
            if (string.IsNullOrEmpty(data.Code) == true)
            {
                AddFailureValidation(result, "Code cannot be empty.");
            }
            if (string.IsNullOrEmpty(data.Name))
            {
                AddFailureValidation(result, "Name cannot be empty.");
            }
            if (data.MinimalPrice.HasValue == false)
            {
                AddFailureValidation(result, "MinimalPrice cannot be empty.");
            }
            if (data.MinimalPrice.HasValue && data.MinimalPrice.Value < 0)
            {
                AddFailureValidation(result, "MinimalPrice must be larger or equal to zero.");
            }
            if (data.IsAuction.HasValue == false)
            {
                AddFailureValidation(result, "IsAuction cannot be empty.");
            }
            if (data.ShowCertificate.HasValue == false)
            {
                AddFailureValidation(result, "ShowCertificate cannot be empty.");
            }
            return result;
        }
    }
}
