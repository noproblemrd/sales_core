﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PublisherPortal.Configurations
{
    internal class MinCallDurationForExemptOfMonthlyPayment : Configurations.IConfigurationChangeHandler
    {
        #region IConfigurationChangeHandler Members

        public void OnChange(string value)
        {
            int minSeconds;
            if (!int.TryParse(value, out minSeconds))
                return;
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@minCallDuration", minSeconds);
            dal.ExecuteNonQuery(query, parameters);
        }

        #endregion

        private const string query =
            @"update accountextensionbase 
            set new_mincallduration = @minCallDuration
            where new_isexemptofppamonthlypayment = 1";
    }
}
