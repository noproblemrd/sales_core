﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PublisherPortal.Configurations
{
    internal interface IConfigurationChangeHandler
    {
        void OnChange(string value);
    }
}