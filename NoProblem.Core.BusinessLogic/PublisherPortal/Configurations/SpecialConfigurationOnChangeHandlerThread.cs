﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PublisherPortal
{
    internal class SpecialConfigurationOnChangeHandlerThread : Runnable
    {
        private string key;
        private string value;

        public SpecialConfigurationOnChangeHandlerThread(string key, string value)
        {
            this.key = key;
            this.value = value;
        }

        protected override void Run()
        {
            Configurations.IConfigurationChangeHandler handler;
            switch (key)
            {
                case DataModel.ConfigurationKeys.MIN_CALL_DURATION_FOR_EXEMPT_OF_MONTHLY_PAY:
                    handler = new Configurations.MinCallDurationForExemptOfMonthlyPayment();
                    break;
                default:
                    handler = null;
                    break;
            }
            if (handler != null)
                handler.OnChange(value);
        }
    }
}
