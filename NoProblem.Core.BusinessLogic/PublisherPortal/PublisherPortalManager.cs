﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Effect.Crm.Convertor;
using Effect.Crm.Logs;
using Effect.Crm.Methods;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using NoProblem.Core.BusinessLogic.Utils;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.PublisherPortal;
using DML = NoProblem.Core.DataModel;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.PublisherPortal
{
	public class PublisherPortalManager : ContextsUserBase
	{
		#region Consts

		private const string ACTIVE = "Active";

		#endregion

		#region Fields
		private readonly string connStr = ConfigurationManager.AppSettings["updateConnectionString"];
		#endregion

		#region Ctor
		public PublisherPortalManager(DML.Xrm.DataContext xrmDataContext, CrmService crmService)
			: base(xrmDataContext, crmService)
		{
		}
		#endregion

		public List<GuidUserNamePair> GetUserNames(GetUserNamesRequest request)
		{
			List<GuidUserNamePair> retVal = new List<GuidUserNamePair>();
			DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			foreach (var id in request.Guids)
			{
				DML.Xrm.account account = accountRepositoryDal.Retrieve(id);
				retVal.Add(new GuidUserNamePair()
				{
					UserId = id,
					UserName = account.name
				});
			}
			return retVal;
		}

		public GetAllPublishersResponse GetAllPublishers()
		{
			GetAllPublishersResponse response = new GetAllPublishersResponse();
			DataAccessLayer.AccountRepository accountRepositoryAccess = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			IEnumerable<DML.Xrm.account> publishersXrmList = accountRepositoryAccess.GetAllPublishers();
			List<PublisherData> publisherDataList = new List<PublisherData>();
			var publishers = from ac in publishersXrmList
							 select
							 new
							 {
								 accountnumber = ac.accountnumber,
								 emailaddress1 = ac.emailaddress1,
								 name = ac.name,
								 new_password = ac.new_password,
								 telephone1 = ac.telephone1,
								 new_securitylevel = ac.new_securitylevel.Value,
								 accountid = ac.accountid,
                                 BizId = ac.new_userbizid,
								 canHearAllRecordings = ac.new_canhearallrecordings.HasValue ? ac.new_canhearallrecordings.Value : false
							 };
			foreach (var ac in publishers)
			{
				PublisherData data = new PublisherData();
                data.BizId = ac.BizId;
				data.AccountNumber = ac.accountnumber;
				data.Email = ac.emailaddress1;
				data.Name = ac.name;
				data.Password = ac.new_password;
				data.PhoneNumber = ac.telephone1;
				data.SecurityLevel = ac.new_securitylevel;
				data.AccountId = ac.accountid;
				data.CanHearAllRecordings = ac.canHearAllRecordings;
				publisherDataList.Add(data);
			}
			response.Publishers = publisherDataList;
			return response;
		}

		public void DeletePublishers(DeletePublishersRequest request)
		{
			if (request.PublisherIds == null)
			{
				throw new ArgumentNullException("PublisherIds");
			}
			bool allOk = true;
			DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			foreach (Guid id in request.PublisherIds)
			{
				try
				{
					DML.Xrm.account publisher = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
					publisher.accountid = id;
					publisher.new_status = (int)DML.Xrm.account.SupplierStatus.Inactive;
					accountRepositoryDal.Update(publisher);
					XrmDataContext.SaveChanges();
				}
				catch (Exception exc)
				{
					LogUtils.MyHandle.HandleException(exc, "Failed to delete publisher with id {0}. Exception was thrown. Code continued to delete other publishers in the request.", id.ToString());
					allOk = false;
				}
			}
			if (allOk == false)
			{
				throw new Exception("Failed to delete some of the publishers. Please try again and if this continues, speak to the system administrator.");
			}
		}

		public string GetConfigurationSettings()
		{
			string query = "select new_configurationsettingid,new_key,new_value,new_description from new_configurationsetting with (nolock) WHERE deletionstatecode = 0";

			StringWriter xmlWriter = new StringWriter();
			XmlTextWriter xmlFormater = new XmlTextWriter(xmlWriter);

			using (SqlConnection connection = new SqlConnection(connStr))
			{
				using (SqlCommand command = new SqlCommand(query, connection))
				{
					command.Connection.Open();
					using (SqlDataReader configurations = command.ExecuteReader())
					{
						xmlFormater.WriteStartElement("Configurations");
						while (configurations.Read())
						{
							xmlFormater.WriteStartElement("Configuration");
							xmlFormater.WriteAttributeString("ID", configurations["new_configurationsettingid"].ToString());
							xmlFormater.WriteElementString("Key", configurations["new_key"].ToString());
							xmlFormater.WriteElementString("Value", configurations["new_value"].ToString());
							xmlFormater.WriteElementString("Description", configurations["new_description"].ToString());
							xmlFormater.WriteEndElement();
						}
						xmlFormater.WriteEndElement();
					}
				}
			}
			return xmlWriter.ToString();
		}

		public string GetExpertise(string SiteId, string PrimaryExpertisePreFix)
		{
			string query = @"SELECT new_showcertificate,new_primaryexpertise.new_primaryexpertiseid,new_primaryexpertise.new_name AS PrimaryName,new_secondaryexpertiseid,new_secondaryexpertise.new_name AS SecondaryName 
FROM new_primaryexpertise with (nolock)
LEFT JOIN new_secondaryexpertise with (nolock) ON new_secondaryexpertise.new_primaryexpertiseid = new_primaryexpertise.new_primaryexpertiseid AND new_secondaryexpertise.deletionstatecode = 0
WHERE new_primaryexpertise.deletionstatecode = 0 AND (new_primaryexpertise.new_name LIKE N'%{0}%' OR '' = '{0}')
ORDER BY PrimaryName,SecondaryName";

			StringWriter xmlText = new StringWriter();
			XmlTextWriter xmlConstructor = new XmlTextWriter(xmlText);

			using (SqlConnection connection = new SqlConnection(connStr))
			{
				using (SqlCommand command = new SqlCommand(string.Format(query, SqlUtils.EscapeCharacters(PrimaryExpertisePreFix)), connection))
				{
					command.Connection.Open();
					using (SqlDataReader expertise = command.ExecuteReader())
					{
						string currPrimaryExpertiseId = "";

						xmlConstructor.WriteStartElement("Expertise");

						while (expertise.Read())
						{
							if (expertise["new_primaryexpertiseid"].ToString() != currPrimaryExpertiseId)
							{
								if (currPrimaryExpertiseId != "")
									xmlConstructor.WriteEndElement();

								xmlConstructor.WriteStartElement("PrimaryExpertise");
								xmlConstructor.WriteAttributeString("Name", expertise["PrimaryName"].ToString());
								xmlConstructor.WriteAttributeString("ID", expertise["new_primaryexpertiseid"].ToString());
								string showCertification = expertise["new_showcertificate"].ToString();
								xmlConstructor.WriteAttributeString("ShowCertificate", string.IsNullOrEmpty(showCertification) ? "False" : showCertification);
								currPrimaryExpertiseId = expertise["new_primaryexpertiseid"].ToString();
							}

							if (expertise["new_secondaryexpertiseid"].ToString() != "")
							{
								xmlConstructor.WriteStartElement("SecondaryExpertise");
								xmlConstructor.WriteAttributeString("ID", expertise["new_secondaryexpertiseid"].ToString());
								xmlConstructor.WriteString(expertise["SecondaryName"].ToString());
								xmlConstructor.WriteEndElement();
							}
						}
						// if we added a primaryexpertise then we need to close the Tag
						if (currPrimaryExpertiseId != "")
							xmlConstructor.WriteEndElement();
						xmlConstructor.WriteEndElement();
					}
				}
			}
			return xmlText.ToString();
		}

		public string GetPrimaryExpertise(string siteId, string primaryExpertisePreFix)
		{
			string query = @"SELECT pe.new_primaryexpertiseid,pe.new_name,pe.new_code,pe.new_minprice,pe.new_isauction,pe.new_showcertificate,chn.new_code as chnlCode,pe.new_notificationdelay,pe.new_minimumbidpriceforbrokers
			FROM new_primaryexpertise pe with (nolock)
			LEFT JOIN new_channel chn with (nolock) ON chn.new_channelid = pe.new_channelid
			WHERE pe.deletionstatecode = 0
			AND (pe.new_name LIKE N'%{0}%' OR pe.new_code LIKE N'%{0}%' OR '' = '{0}') ORDER BY pe.new_name";

			XElement root = new XElement("PrimaryExpertise");
			using (SqlConnection connection = new SqlConnection(connStr))
			{
				using (SqlCommand command = new SqlCommand(string.Format(query, SqlUtils.EscapeCharacters(primaryExpertisePreFix)), connection))
				{
					command.Connection.Open();
					using (SqlDataReader expertise = command.ExecuteReader())
					{
						while (expertise.Read())
						{
							XElement element = new XElement("Expertise");
							element.Add(new XAttribute("Name", expertise["new_name"].ToString()));
							element.Add(new XAttribute("ID", expertise["new_primaryexpertiseid"].ToString()));
							element.Add(new XAttribute("Code", expertise["new_code"].ToString()));
							string showCertificate = expertise["new_showcertificate"].ToString();
							element.Add(new XAttribute("ShowCertificate", string.IsNullOrEmpty(showCertificate) ? "False" : showCertificate));
							decimal minPrice = 0;
							if (expertise["new_minprice"] != DBNull.Value)
								minPrice = decimal.Parse(expertise["new_minprice"].ToString());
							element.Add(new XAttribute("MinPrice", String.Format("{0:0.##}", minPrice)));
							element.Add(new XAttribute("IsAuction", expertise["new_isauction"].ToString()));
							element.Add(new XAttribute("Channel", expertise["chnlCode"] == DBNull.Value ? "1" : expertise["chnlCode"].ToString()));
							element.Add(new XAttribute("NotificationDelay", ConvertorUtils.ToInt(expertise["new_notificationdelay"]).ToString()));
							element.Add(new XAttribute("MinBrokerBid", ConvertorUtils.ToDecimal(expertise["new_minimumbidpriceforbrokers"]).ToString()));
							root.Add(element);
						}
					}
				}
			}
			return root.ToString();
		}

		public string GetSecondaryExpertise(string siteId, string primaryExpertiseId)
		{
			string query = "SELECT new_secondaryexpertiseid,new_name,new_code FROM new_secondaryexpertise with (nolock) WHERE new_primaryexpertiseid = '{0}' AND deletionstatecode = 0 ORDER BY new_name";
			StringWriter xmlText = new StringWriter();
			XmlTextWriter xmlConstructor = new XmlTextWriter(xmlText);

			using (SqlConnection connection = new SqlConnection(connStr))
			{
				using (SqlCommand command = new SqlCommand(string.Format(query, primaryExpertiseId), connection))
				{
					command.Connection.Open();
					using (SqlDataReader expertise = command.ExecuteReader())
					{
						xmlConstructor.WriteStartElement("PrimaryExpertise");
						while (expertise.Read())
						{
							xmlConstructor.WriteStartElement("SecondaryExpertise");
							xmlConstructor.WriteAttributeString("ID", expertise["new_secondaryexpertiseid"].ToString());
							xmlConstructor.WriteAttributeString("Code", expertise["new_code"].ToString());
							xmlConstructor.WriteValue(expertise["new_name"].ToString());
							xmlConstructor.WriteEndElement();
						}
						xmlConstructor.WriteEndElement();
					}
				}
			}

			return xmlText.ToString();
		}

		public string UpsertPrimaryExpertise(string Request)
		{
			/*
			 * <PrimaryExpertise>
			 *    <SiteId></SiteId>
			 *    <ID></ID>
			 *    <Name></Name>
			 *    <MinPrice></MinPrice>
			 *    <IsAuction></IsAuction>
			 *    <Code></Code>
			 *    <ShowCertificate></ShowCertificate>
			 *    <MinBrokerBid></MinBrokerBid>
			 * </PrimaryExpertise>
			 */
			XmlDocument requestDoc = new XmlDocument();
			requestDoc.LoadXml(Request);

			DynamicEntity primaryExpertise = ConvertorCRMParams.GetNewDynamicEntity("new_primaryexpertise");
			string _name = string.Empty;
			string _code = string.Empty;
			//requestDoc.DocumentElement["ID"] != null && requestDoc.DocumentElement["ID"].InnerText != ""
			Guid _id = Guid.Empty;
			if (requestDoc.DocumentElement["ID"] != null && requestDoc.DocumentElement["ID"].InnerText != "")
				_id = new Guid(requestDoc.DocumentElement["ID"].InnerText);
			if (requestDoc.DocumentElement["Name"] != null)
			{
				ConvertorCRMParams.SetDynamicEntityPropertyValue(primaryExpertise, "new_name",
				   requestDoc.DocumentElement["Name"].InnerText.Trim(), typeof(StringProperty));
				_name = requestDoc.DocumentElement["Name"].InnerText.Trim();
			}
			if (requestDoc.DocumentElement["Code"] != null)
			{
				_code = requestDoc.DocumentElement["Code"].InnerText;
				ConvertorCRMParams.SetDynamicEntityPropertyValue(primaryExpertise, "new_code",
				   _code, typeof(StringProperty));
				ConvertorCRMParams.SetDynamicEntityPropertyValue(primaryExpertise, "new_englishname",
				   _code, typeof(StringProperty));
			}
			//check if expertise name or code allready exists

			string query = "DECLARE @Result bit " +
						"IF(EXISTS(SELECT * FROM new_primaryexpertise " +
						"WHERE deletionstatecode = 0  AND (new_primaryexpertise.new_name  = @name OR new_primaryexpertise.New_code = @code) " +
						"AND New_primaryexpertiseId <> @id " +
						")) " +
						"SET @Result=1 ELSE SET @Result=0 " +
						"SELECT @Result";
			bool IfExists = true;
			using (SqlConnection connection = new SqlConnection(connStr))
			{
				using (SqlCommand cmd = new SqlCommand(query, connection))
				{
					cmd.Parameters.AddWithValue("@name", _name);
					cmd.Parameters.AddWithValue("@code", _code);
					cmd.Parameters.AddWithValue("@id", _id);
					cmd.Connection.Open();
					IfExists = (bool)cmd.ExecuteScalar();
				}
			}
			if (IfExists)
				return "<PrimaryExpertiseUpsert><Status>Exists</Status></PrimaryExpertiseUpsert>";

			if (requestDoc.DocumentElement["IsAuction"] != null && requestDoc.DocumentElement["IsAuction"].InnerText != "")
			{
				bool isAuction = ConvertorUtils.ToBoolean(requestDoc.DocumentElement["IsAuction"].InnerText);
				ConvertorCRMParams.SetDynamicEntityPropertyValue(primaryExpertise, "new_isauction",
				   ConvertorCRMParams.GetCrmBoolean(isAuction), typeof(CrmBooleanProperty));
			}

			if (requestDoc.DocumentElement["Channel"] != null && requestDoc.DocumentElement["Channel"].InnerText != "")
			{
				int channelCode = int.Parse(requestDoc.DocumentElement["Channel"].InnerText);
				DataAccessLayer.Channel channelDal = new NoProblem.Core.DataAccessLayer.Channel(XrmDataContext);
				DML.Xrm.new_channel channel = channelDal.GetChannelByCode((DML.Xrm.new_channel.ChannelCode)channelCode);
				Guid channelId = channel.new_channelid;
				ConvertorCRMParams.SetDynamicEntityPropertyValue(primaryExpertise, "new_channelid",
					ConvertorCRMParams.GetCrmLookup("new_channel", channelId), typeof(LookupProperty));
			}

			if (requestDoc.DocumentElement["MinPrice"] != null && requestDoc.DocumentElement["MinPrice"].InnerText != "")
			{
				decimal minPrice = decimal.Parse(requestDoc.DocumentElement["MinPrice"].InnerText);
				ConvertorCRMParams.SetDynamicEntityPropertyValue(primaryExpertise, "new_minprice",
				   ConvertorCRMParams.GetCrmMoney(minPrice), typeof(CrmMoneyProperty));
			}

			if (requestDoc.DocumentElement["MinBrokerBid"] != null && requestDoc.DocumentElement["MinBrokerBid"].InnerText != "")
			{
				decimal minBrokerBid = decimal.Parse(requestDoc.DocumentElement["MinBrokerBid"].InnerText);
				if (minBrokerBid >= 0)
				{
					CrmDecimal minBrokerBidCrmDecimal = new CrmDecimal();
					minBrokerBidCrmDecimal.IsNull = false;
					minBrokerBidCrmDecimal.Value = minBrokerBid;
					Utils.DynamicEntityUtils.SetPropertyValue(primaryExpertise, "new_minimumbidpriceforbrokers",
					  minBrokerBidCrmDecimal, typeof(CrmDecimalProperty));
				}
			}

			if (requestDoc.DocumentElement["ShowCertificate"] != null && requestDoc.DocumentElement["ShowCertificate"].InnerText != "")
			{
				bool showCertificate = ConvertorUtils.ToBoolean(requestDoc.DocumentElement["ShowCertificate"].InnerText);
				ConvertorCRMParams.SetDynamicEntityPropertyValue(primaryExpertise, "new_showcertificate",
					ConvertorCRMParams.GetCrmBoolean(showCertificate), typeof(CrmBooleanProperty));
			}

			if (requestDoc.DocumentElement["NotificationDelay"] != null && requestDoc.DocumentElement["NotificationDelay"].InnerText != "")
			{
				int delay = ConvertorUtils.ToInt(requestDoc.DocumentElement["NotificationDelay"].InnerText);
				ConvertorCRMParams.SetDynamicEntityPropertyValue(primaryExpertise, "new_notificationdelay",
					ConvertorCRMParams.GetCrmNumber(delay, false), typeof(CrmNumberProperty));
			}

			if (requestDoc.DocumentElement["ID"] != null && requestDoc.DocumentElement["ID"].InnerText != "")
			{
				ConvertorCRMParams.SetDynamicEntityPropertyValue(primaryExpertise, "new_primaryexpertiseid",
				   ConvertorCRMParams.GetCrmKey(new Guid(requestDoc.DocumentElement["ID"].InnerText)), typeof(KeyProperty));

				MethodsUtils.UpdateEntity(CrmService, primaryExpertise);
			}
			else
			{
				MethodsUtils.CreateEntity(CrmService, primaryExpertise);
			}

			return "<PrimaryExpertiseUpsert><Status>Success</Status></PrimaryExpertiseUpsert>";
		}

		public string GetUnavailabilityReasons()
		{
			string query = @"SELECT new_disabled,new_id,new_unavailabilityreasonid,ur.new_name,
CAST(ISNULL(new_issystemreason,0) AS INT) AS new_issystemreason,CAST(ISNULL(new_ispricingended,0) AS INT) AS new_ispricingended 
,new_code, new_translatedname
FROM new_unavailabilityreason ur with (nolock) 
join new_unavailabilityreasongroup grp with(nolock) on grp.new_unavailabilityreasongroupid = ur.new_groupid
WHERE ur.deletionstatecode = 0 and grp.deletionstatecode = 0";

			StringWriter xmlWriter = new StringWriter();
			XmlTextWriter xmlFormater = new XmlTextWriter(xmlWriter);

			using (SqlConnection connection = new SqlConnection(connStr))
			{
				using (SqlCommand command = new SqlCommand(query, connection))
				{
					command.Connection.Open();
					using (SqlDataReader reasons = command.ExecuteReader())
					{
						xmlFormater.WriteStartElement("UnavailabilityResons");
						while (reasons.Read())
						{
							xmlFormater.WriteStartElement("UnavailabilityReson");
							xmlFormater.WriteAttributeString("ID", reasons["new_unavailabilityreasonid"].ToString());
							int displayLevel = int.Parse(reasons["new_ispricingended"].ToString()) +
							   int.Parse(reasons["new_issystemreason"].ToString());
							xmlFormater.WriteAttributeString("DisplayLevel", displayLevel.ToString());
							xmlFormater.WriteAttributeString("IDsmall", (string)reasons["new_id"]);
							xmlFormater.WriteAttributeString("Inactive", reasons["new_disabled"] == DBNull.Value ? "0" : (((bool)reasons["new_disabled"]) ? "1" : "0"));
							xmlFormater.WriteAttributeString("GroupCode", reasons["new_code"] != DBNull.Value ? ((int)reasons["new_code"]).ToString() : "0");
							xmlFormater.WriteAttributeString("GroupName", reasons["new_translatedname"] != DBNull.Value ? (string)reasons["new_translatedname"] : string.Empty);
							xmlFormater.WriteValue(reasons["new_name"].ToString());
							xmlFormater.WriteEndElement();
						}
						xmlFormater.WriteEndElement();
					}
				}
			}
			return xmlWriter.ToString();
		}

		public string DeleteUnavailabilityReasons(string Request)
		{
			/*
			 * <UnavailabilityReasons>
			 *    <UnavailabilityReason>Guid</UnavailabilityReason>
			 *    <UnavailabilityReason>Guid</UnavailabilityReason>
			 * </UnavailabilityReasons>
			 */

			XmlDocument parser = new XmlDocument();
			parser.LoadXml(Request);

			foreach (XmlNode reason in parser.SelectNodes("//UnavailabilityReason"))
			{
				//DataBaseUtils.DeleteEntities("new_unavailabilityreason", string.Format("new_unavailabilityreasonid = '{0}'", new Guid(reason.InnerText)));
				//DataBaseUtils.ExecuteSqlString("UPDATE new_unavailabilityreaon SET statecode = 1 WHERE new_unavailabilityreasonid IN ({0})
				//Effect.Crm.DB.DataBaseUtils.DirectUpdateInstance("new_unavailabilityreason", new Guid(reason.InnerText), new string[] { "statecode" }, new object[] { 1 });
				string sqlUpdate = "update new_unavailabilityreason set statecode = 1 where new_unavailabilityreasonid = @id";
				DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
				DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
				parameters.Add("@id", new Guid(reason.InnerText));
				dal.ExecuteNonQuery(sqlUpdate, parameters);
			}

			return "<UnavailabilityReason><Status>Success</Status></UnavailabilityReason>";
		}

		public string GetSuppliersStatus(string incidentId)
		{
			Guid incidentGuid = new Guid(incidentId);
			// I add 'or ia.StatusCode = 12' (Yoav)
			string query =
				@"select acc.accountid,acc.accountnumber,acc.name,
				acc.numberofemployees,acc.new_description, ia.new_accountidname,acc.new_sumassistancerequests,
				acc.new_sumsurvey,new_accountexpertise.new_certificate,acc.address1_country
				,acc.address1_stateorprovince, acc.address1_city, acc.address1_county
				,acc.address1_line1,acc.address1_line2,acc.address1_postalcode,
				(case ia.StatusCode  when 1 /* Match */ then 'Init'  when 14 /* In Auction */  then 'Open' when 18 /* Waiting */ then 'Open' when 15 /* In Conversation */ then 'Initiated' when 10 /* Close */ then 'Succeed' else '' end) as IncidentAccountStatus, 
				(case inc.new_dialerstatus when 8 /* Auction Started */ then 'Init' when 2 /* Request Transfered */ then 'Active' else /* Request Completed */ 'Completed' end) as IncidentStatus,
				(select count(*) from new_review with (nolock) where new_accountid = acc.accountid and new_islike is not null and new_islike = 1) as likes,
				(select count(*) from new_review with (nolock) where new_accountid = acc.accountid and (new_islike is null or new_islike = 0)) as dislikes                
				,(select count(*) from new_incidentaccount where statuscode = 10 and deletionstatecode = 0 and new_incidentid = @incidentId) as CloseCount
				from new_incidentaccount ia with (nolock)
				inner join account acc with (nolock) on acc.accountid = ia.new_accountid
				inner join incident inc with (nolock) on inc.incidentid=ia.new_incidentid
				inner join new_accountexpertise with (nolock) ON new_accountexpertise.new_primaryexpertiseid = inc.new_primaryexpertiseid and acc.accountid = new_accountexpertise.new_accountid
				where new_incidentid = @incidentId
				and
				(
					ia.StatusCode = 14 or ia.StatusCode = 15 or ia.StatusCode = 10 or ia.StatusCode = 12 or ia.StatusCode = 17 or ia.StatusCode = 18
				)
				order by new_potentialincidentprice DESC";//, cleanIncidentId, version);

			XElement root = new XElement("Suppliers");
			using (SqlConnection connection = new SqlConnection(connStr))
			{
				using (SqlCommand command = new SqlCommand(query, connection))
				{
					command.Connection.Open();
					command.Parameters.AddWithValue("@incidentId", incidentGuid);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						bool isSetIncidentStatus = false;
						while (reader.Read())
						{
							if (!isSetIncidentStatus)
							{
								root.Add(new XAttribute("IncidentStatus", reader["IncidentStatus"].ToString()));
								root.Add(new XAttribute("Mode", ""));
								root.Add(new XAttribute("ConnectedCount", reader["CloseCount"].ToString()));
								isSetIncidentStatus = true;
							}
							XElement supplerNode = new XElement("Supplier");
							supplerNode.Add(new XAttribute("SupplierId", reader["accountid"].ToString()));
							supplerNode.Add(new XAttribute("SupplierNumber", reader["accountnumber"].ToString()));
							supplerNode.Add(new XAttribute("SupplierName", reader["name"].ToString()));
							supplerNode.Add(new XAttribute("SumSurvey", reader["new_sumsurvey"].ToString()));
							supplerNode.Add(new XAttribute("SumAssistanceRequests", reader["new_sumassistancerequests"].ToString()));
							supplerNode.Add(new XAttribute("NumberOfEmployees", reader["numberofemployees"].ToString()));
							supplerNode.Add(new XAttribute("ShortDescription", reader["new_description"].ToString()));
							supplerNode.Add(new XAttribute("Status", reader["IncidentAccountStatus"].ToString()));
							supplerNode.Add(new XAttribute("Certificate", reader["new_certificate"].ToString()));
							supplerNode.Add(new XAttribute("Country", reader["address1_country"].ToString()));
							supplerNode.Add(new XAttribute("State", reader["address1_stateorprovince"].ToString()));
							supplerNode.Add(new XAttribute("City", reader["address1_city"].ToString()));
							supplerNode.Add(new XAttribute("District", reader["address1_county"].ToString()));
							supplerNode.Add(new XAttribute("Street", reader["address1_line1"].ToString()));
							supplerNode.Add(new XAttribute("StreetNumber", reader["address1_line2"].ToString()));
							supplerNode.Add(new XAttribute("ZipCode", reader["address1_postalcode"].ToString()));
							supplerNode.Add(new XAttribute("Likes", reader["likes"].ToString()));
							supplerNode.Add(new XAttribute("Dislikes", reader["dislikes"].ToString()));
							root.Add(supplerNode);
						}
						if (isSetIncidentStatus == false)//in case no ia's matched the query, we need to send info about the incident itself.
						{
							root.Add(new XAttribute("ConnectedCount", "0"));
							DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
							DML.Xrm.incident incident = incidentDal.Retrieve(incidentGuid);
							if (incident != null)
							{
								DML.Xrm.incident.DialerStatus dialerStatus = (DML.Xrm.incident.DialerStatus)incident.new_dialerstatus.Value;
								DML.Xrm.incident.Status caseStatus = (DML.Xrm.incident.Status)incident.statuscode.Value;
								string status = string.Empty;
								if (caseStatus == NoProblem.Core.DataModel.Xrm.incident.Status.CUSTOMER_NOT_AVAILABLE_DIALER
									|| caseStatus == NoProblem.Core.DataModel.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER
									|| caseStatus == NoProblem.Core.DataModel.Xrm.incident.Status.WORK_DONE)
								{
									status = "Completed";
								}
								else if (dialerStatus == NoProblem.Core.DataModel.Xrm.incident.DialerStatus.DELIVERED_REQUEST)
								{
									status = "Active";
								}
								else if (dialerStatus == NoProblem.Core.DataModel.Xrm.incident.DialerStatus.AUCTION_STARTED)
								{
									status = "Init";
								}
								if (string.IsNullOrEmpty(status) == false)
								{
									root.Add(new XAttribute("IncidentStatus", status));
									root.Add(new XAttribute("Mode", ""));
								}
							}
						}
					}
				}
			}

			var intitiaed = root.Elements("Supplier").FirstOrDefault(x => x.Attribute("Status").Value == "Initiated");
			if (intitiaed != null)
			{
				root.Attribute("Mode").Value = "Initiated";
				//if (first.Attribute("Status").Value == "Initiated")
				//{
				//    root.Attribute("Mode").Value = "Initiated";
				//}
				//else if (first.Attribute("Status").Value == "Open")
				//{
				//    root.Attribute("Mode").Value = "Open";
				//}
			}
			else
			{
				var opened = root.Elements("Supplier").FirstOrDefault(x => x.Attribute("Status").Value == "Open");
				if (opened != null)
				{
					root.Attribute("Mode").Value = "Open";
				}
			}
			return root.ToString();
		}

		public void UpdateConfigurationSettings(string key, string value)
		{
			DataAccessLayer.ConfigurationSettings dal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
			bool changed = dal.SetConfigurationSettingValue(key, value);
			if (changed)
				CheckSpecialConfiguration(key, value);
		}

		public void UpdateConfigurationSettings(List<StringPair> configSettingsList)
		{
			DataAccessLayer.ConfigurationSettings dal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
			foreach (var pair in configSettingsList)
			{
				bool changed = dal.SetConfigurationSettingValue(pair.Key, pair.Value);
				if (changed)
					CheckSpecialConfiguration(pair.Key, pair.Value);
			}
		}

		private void CheckSpecialConfiguration(string key, string value)
		{
			if (key == DataModel.ConfigurationKeys.MIN_CALL_DURATION_FOR_EXEMPT_OF_MONTHLY_PAY)
			{
				SpecialConfigurationOnChangeHandlerThread handler = new SpecialConfigurationOnChangeHandlerThread(key, value);
				handler.Start();
			}
		}

		public void UpdateUpsale(Guid upsaleId, Guid publisherId, DML.Xrm.new_upsale.UpsaleStatus status, Guid statusReasonId, DateTime timeToCall)
		{
			DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
			var upsale = upsaleDal.Retrieve(upsaleId);

			if (upsale.statuscode == (int)DML.Xrm.new_upsale.UpsaleStatus.Open)
			{
				upsale.new_userid = publisherId;
				upsale.statuscode = (int)status;
				upsale.new_timetocall = timeToCall;
				if (status == NoProblem.Core.DataModel.Xrm.new_upsale.UpsaleStatus.Lost)
				{
					upsale.new_upsalelosereasonid = statusReasonId;
					DataAccessLayer.AffiliatePayStatusReason dal = new NoProblem.Core.DataAccessLayer.AffiliatePayStatusReason(XrmDataContext);
					Guid payStatusId;
					Guid payReasonId = dal.GetByUpsaleLoseReason(statusReasonId, out payStatusId);
					if (payReasonId != Guid.Empty && payStatusId != Guid.Empty)
					{
						DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
						var incidentsIds = incidentDal.GetIncidentsByUpsaleId(upsaleId);
						foreach (var inciId in incidentsIds)
						{
							DML.Xrm.incident incident = new NoProblem.Core.DataModel.Xrm.incident(XrmDataContext);
							incident.incidentid = inciId;
							incident.new_affiliatepaystatusreasonid = payReasonId;
							incident.new_affiliatepaystatusid = payStatusId;
							incidentDal.Update(incident);
						}
					}
				}
				else if (status == NoProblem.Core.DataModel.Xrm.new_upsale.UpsaleStatus.Open)
				{
					if (statusReasonId != Guid.Empty)
					{
						upsale.new_upsalependingreasonid = statusReasonId;
					}
				}
				upsaleDal.Update(upsale);
				XrmDataContext.SaveChanges();
			}
		}

		public string GetUpsaleLoseReasons()
		{
			XElement doc = new XElement("UpsaleLoseReasons");
			DataAccessLayer.UpsaleLoseReason dal = new NoProblem.Core.DataAccessLayer.UpsaleLoseReason(XrmDataContext);
			var dataList = dal.GetAll();
			foreach (var data in dataList)
			{
				doc.Add(new XElement("UpsaleLoseReason",
					new XAttribute("ID", data.Guid),
					new XAttribute("Name", data.Name),
					new XAttribute("Inactive", data.Inactive)));
			}
			return doc.ToString();
		}

		public Result<SupplierActivityStatusResponse> GetSupplierActivityStatus()
		{
			Result<SupplierActivityStatusResponse> retVal = new Result<SupplierActivityStatusResponse>();

			SupplierActivityStatusResponse status = new SupplierActivityStatusResponse();
			status.SupplierAvailabilityStatus = new List<DML.Xrm.account.SupplierStatus>();

			foreach (DML.Xrm.account.SupplierStatus value in Enum.GetValues(typeof(DML.Xrm.account.SupplierStatus)))
			{
				status.SupplierAvailabilityStatus.Add(value);
			}

			retVal.Value = status;
			retVal.Type = Result.eResultType.Success;

			return retVal;
		}

		public GetSuppliersDirectNumbersResponse GetSuppliersDirectNumbers(GetSuppliersDirectNumbersRequest request)
		{
			DataAccessLayer.AccountRepository accountRepositoryAccess = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			DataAccessLayer.PrimaryExpertise primaryExpertiseAccess = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
			DataAccessLayer.Origin originAccess = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
			DataModel.XrmDataContext.ClearCache("new_accountexpertise");
			DataModel.XrmDataContext.ClearCache("new_directnumber");
			DML.Xrm.account supplier = accountRepositoryAccess.Retrieve(request.SupplierId);
			List<DirectNumberData> dataList = new List<DirectNumberData>();
			foreach (DML.Xrm.new_accountexpertise acExpertise in supplier.new_account_new_accountexpertise)
			{
				if (acExpertise.statecode != ACTIVE)
				{
					continue;
				}
				string expertiseName = string.Empty;
				if (acExpertise.new_primaryexpertiseid.HasValue)
				{
					DML.Xrm.new_primaryexpertise expertise = primaryExpertiseAccess.Retrieve(acExpertise.new_primaryexpertiseid.Value);
					expertiseName = expertise.new_name;
				}
				foreach (DML.Xrm.new_directnumber directNumber in acExpertise.new_new_accountexpertise_new_directnumber)
				{
					DirectNumberData data = new DirectNumberData();
					data.ExpertiseName = expertiseName;
					data.ModifiedOn = directNumber.modifiedon.HasValue ? directNumber.modifiedon.Value : directNumber.createdon.Value;
					data.Number = directNumber.new_number;
					string originName = string.Empty;
					if (directNumber.new_originid.HasValue)
					{
						DML.Xrm.new_origin origin = originAccess.Retrieve(directNumber.new_originid.Value);
						originName = origin.new_name;
					}
					data.OriginName = originName;
					dataList.Add(data);
				}
			}
			GetSuppliersDirectNumbersResponse response = new GetSuppliersDirectNumbersResponse();
			response.DirectNumberCollection = dataList;
			return response;
		}

		public List<string> GetAllSuppliersNames()
		{
			List<string> dataList = new List<string>();
			DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			IEnumerable<DML.Xrm.account> suppliersList = accountRepositoryDal.GetAllSuppliers();
			suppliersList = suppliersList.Where(x => string.IsNullOrEmpty(x.name) == false);
			foreach (var supplier in suppliersList)
			{
				dataList.Add(supplier.name);
			}
			return dataList;
		}

		public List<DataModel.PublisherPortal.GuidStringPair> GetAllSupplierNamesAndIds()
		{
			List<DataModel.PublisherPortal.GuidStringPair> dataList = new List<DataModel.PublisherPortal.GuidStringPair>();
			DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			IEnumerable<DML.Xrm.account> suppliersList = accountRepositoryDal.GetAllSuppliers();
			suppliersList = suppliersList.Where(x => string.IsNullOrEmpty(x.name) == false);
			foreach (var supplier in suppliersList)
			{
				DataModel.PublisherPortal.GuidStringPair data = new GuidStringPair();
				data.Id = supplier.accountid;
				data.Name = supplier.name;
				dataList.Add(data);
			}
			return dataList;
		}

		public bool IsCanHearAllRecordings(Guid userId)
		{
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			bool retVal = dal.IsCanHearAllRecordings(userId);
			return retVal;
		}
        public bool ClearPhoneMobileApp(string phone)
        {
            if (string.IsNullOrEmpty(phone))
                return false;
            string command = "EXEC [dbo].[ClearPhoneMobileApp] @phone";

            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@phone", phone);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            return true;
        }
        public NoProblem.Core.DataModel.ClipCall.Request.CloseIncidentResponse CloseLead(Guid incidentId)
        {
            NoProblem.Core.DataModel.ClipCall.Request.CloseIncidentResponse response =
                new DML.ClipCall.Request.CloseIncidentResponse();
            // return new CloseLeadResponse { IsSuccess = result > 0 };

            NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager srm = new ServiceRequest.ServiceRequestManager(null);
            if (!srm.IsMobileIncidentOpen(incidentId))
            {
                response.Status = DML.ClipCall.Request.CloseIncidentResponse.eCloseIncidentStatus.INCIDENT_ALREADY_CLOSED;
                response.IsSuccess = false;
            }
            else
            {
                response.Status = DML.ClipCall.Request.CloseIncidentResponse.eCloseIncidentStatus.SUCCESS;
                response.IsSuccess = true;
                srm.CloseMobileIncident(incidentId, ServiceRequest.VideoRequests.TypeExecuteUser.Publisher);
            }
            return response;
        }
        public List<NoProblem.Core.DataModel.ClipCall.Request.RelaunchSearchSuppliersResponse> 
            SearchClipCallSuppliers(NoProblem.Core.DataModel.ClipCall.Request.RelaunchSearchSuppliersRequest request)
        {
            List<NoProblem.Core.DataModel.ClipCall.Request.RelaunchSearchSuppliersResponse> result = new List<DML.ClipCall.Request.RelaunchSearchSuppliersResponse>();
            NoProblem.Core.DataAccessLayer.Region regionDal = new DataAccessLayer.Region(null);
            Guid regionId = regionDal.GetRegionIdByNameAndLevel(request.RegionName, request.RegionLevel);            
            if (regionId == Guid.Empty)
                return result;
            NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager srm = new ServiceRequest.ServiceRequestManager(null);
            List<Guid> accountList = srm.GetAppPhoneSuppliers(request.IncidentId, regionId, request.CategoryId);
            if (accountList != null && accountList.Count > 0)
            {
                string command = @"
SELECT AccountId, Name, Telephone1, New_FullAddress
	FROM dbo.Account WITH(NOLOCK)
	WHERE AccountId in(
		SELECT Value
		FROM dbo.fn_ParseDelimitedGuid(@ids, ';'))";
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@ids", GetGuidStringList(accountList));
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            NoProblem.Core.DataModel.ClipCall.Request.RelaunchSearchSuppliersResponse data =
                                new DML.ClipCall.Request.RelaunchSearchSuppliersResponse();
                            data.AccountId = (Guid)reader["AccountId"];
                            data.AccountName = reader["Name"] == DBNull.Value ? string.Empty : (string)reader["Name"];
                            data.Address = reader["New_FullAddress"] == DBNull.Value ? string.Empty : (string)reader["New_FullAddress"];
                            data.Phone = (string)reader["Telephone1"];
                            result.Add(data);
                        }
                        conn.Close();
                    }
                }
            }
            return result;
            
        }
        private string GetGuidStringList(List<Guid> list)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Guid id in list)
                sb.Append(id.ToString() + ";");
            return sb.ToString();
        }
        public NoProblem.Core.DataModel.ClipCall.Request.RelaunchClipCallIncidentResponse
           RelaunchClipCallIncident(NoProblem.Core.DataModel.ClipCall.Request.RelaunchClipCallIncidentRequest request)
        {
            NoProblem.Core.DataModel.ClipCall.Request.RelaunchClipCallIncidentResponse result = new DML.ClipCall.Request.RelaunchClipCallIncidentResponse();
            NoProblem.Core.DataModel.ClipCall.Request.RelaunchChangeValues changeValues = null;
            NoProblem.Core.DataAccessLayer.Region regionDal = new DataAccessLayer.Region(null);
            Guid regionId = regionDal.GetRegionIdByNameAndLevel(request.regionName, request.regionLevel);
            if(regionId == Guid.Empty)
            {
                result.IsSuccess = false;
                return result;
            }
            NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager srm = new ServiceRequest.ServiceRequestManager(null);
            if(srm.IsMobileIncidentOpen(request.IncidentId))
            {
                result.IsSuccess = false;
                return result;
            }
            Guid oldRegionId = Guid.Empty, oldExpertiseId = Guid.Empty;
            string oldDescription = string.Empty;
             string command = @"
SELECT new_regionid, new_primaryexpertiseid, Description
FROM dbo.Incident WITH(NOLOCK)
where IncidentId = @incidentId";
             using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
             {
                 using (SqlCommand cmd = new SqlCommand(command, conn))
                 {
                     conn.Open();
                     cmd.Parameters.AddWithValue("@incidentId", request.IncidentId);
                     SqlDataReader reader = cmd.ExecuteReader();
                     if(reader.Read())
                     {
                         oldRegionId = (Guid)reader["new_regionid"];
                         oldExpertiseId = (Guid)reader["new_primaryexpertiseid"];
                         if(reader["Description"] != DBNull.Value)
                             oldDescription = (string)reader["Description"];
                          
                     }
                     conn.Close();
                     reader.Dispose();
                     cmd.Dispose();
                 }
                 conn.Dispose();
             }
            if(oldRegionId != request.expertiseId || oldRegionId != regionId)
            {
                command = "UPDATE dbo.IncidentExtensionBase SET new_regionid = @regionId, new_primaryexpertiseid = @expertiseId where IncidentId = @incidentId";
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@regionId", regionId);
                        cmd.Parameters.AddWithValue("@expertiseId", request.expertiseId);
                        cmd.Parameters.AddWithValue("@incidentId", request.IncidentId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        cmd.Dispose();
                    }
                    conn.Dispose();
                }
                changeValues = new DML.ClipCall.Request.RelaunchChangeValues(oldRegionId, regionId, oldExpertiseId, request.expertiseId);

            }
            if(oldDescription != request.desc)
            {
                command = "UPDATE dbo.IncidentBase SET Description = @description where IncidentId = @incidentId";
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@description", request.desc);
                        cmd.Parameters.AddWithValue("@incidentId", request.IncidentId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        cmd.Dispose();
                    }
                    conn.Dispose();
                }
            }
            NoProblem.Core.DataModel.ClipCall.Request.RelaunchJob rj = new DML.ClipCall.Request.RelaunchJob()
            {
                changeValues = changeValues,
                IncidentId = request.IncidentId
            };
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
            {
                NoProblem.Core.DataModel.ClipCall.Request.RelaunchJob _rj = (NoProblem.Core.DataModel.ClipCall.Request.RelaunchJob)state;
                NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager _srm = new ServiceRequest.ServiceRequestManager(null);
                _srm.RelaunchIncident(_rj.IncidentId, ServiceRequest.VideoRequests.TypeExecuteUser.Publisher, _rj.changeValues);
            }), rj);
            

            return new NoProblem.Core.DataModel.ClipCall.Request.RelaunchClipCallIncidentResponse() { IsSuccess = true };
        }
            
	}
}
