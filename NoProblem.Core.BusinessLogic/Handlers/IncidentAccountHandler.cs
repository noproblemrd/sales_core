﻿using Effect.Crm.Plugins.DataContext;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using NoProblem.Core.BusinessLogic.ServiceRequest;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Handlers
{
    public class IncidentAccountHandler : ContextsUserBase
    {
        #region Ctor
        public IncidentAccountHandler(DML.Xrm.DataContext xrmDataContext, CrmService crmService)
            : base(xrmDataContext, crmService)
        {

        } 
        #endregion

        public void HandlePostUpdateEvent(ExecutionContext context)
        {
            IncidentAccountUtils incidentAccountUtils = new IncidentAccountUtils(XrmDataContext, CrmService);
            incidentAccountUtils.UpdateIncidentByIncidentAccount(context);
            incidentAccountUtils.checkIfStatusChangeToParticularValues(context);
        }

        public void HandlePostCreateEvent(ExecutionContext context)
        {
            IncidentAccountUtils incidentAccountUtils = new IncidentAccountUtils(XrmDataContext, CrmService);
            incidentAccountUtils.checkIfStatusChangeToParticularValues(context);
            incidentAccountUtils.UpdateIncidentByIncidentAccount(context);
        }
    }
}
