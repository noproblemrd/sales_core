﻿using System;
using System.Collections.Generic;
using System.Linq;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.HelpDesk
{
    public class HelpDeskManager : XrmUserBase
    {
        #region Ctor
        public HelpDeskManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Public Methods

        public List<DataModel.PublisherPortal.GuidStringPair> GetAllHpTypes()
        {
            DataAccessLayer.HdType dal = new NoProblem.Core.DataAccessLayer.HdType(XrmDataContext);
            List<DataModel.PublisherPortal.GuidStringPair> dataList = dal.GetAll();
            return dataList;
        }

        public DML.HelpDesk.SeverityAndStatusContainer GetAllStatusesAndSeverities(Guid hdTypeId)
        {
            DataAccessLayer.HdStatus statusDal = new NoProblem.Core.DataAccessLayer.HdStatus(XrmDataContext);
            List<DML.HelpDesk.HdStatusData> statusList = statusDal.GetAll(hdTypeId);
            DataAccessLayer.HdSeverity severtityDal = new NoProblem.Core.DataAccessLayer.HdSeverity(XrmDataContext);
            List<DML.HelpDesk.HdSeverityData> severityList = severtityDal.GetAll(hdTypeId);
            DML.HelpDesk.SeverityAndStatusContainer retVal =
                new NoProblem.Core.DataModel.HelpDesk.SeverityAndStatusContainer()
                {
                    SeverityList = severityList,
                    StatusList = statusList
                };
            return retVal;
        }

        public Guid UpsertEntry(DML.HelpDesk.HdEntryData data)
        {
            bool isUpdate = data.Id != Guid.Empty;
            DML.Xrm.new_helpdeskentry entry = new NoProblem.Core.DataModel.Xrm.new_helpdeskentry(XrmDataContext);
            entry.new_regardingobjectid = data.RegardingObjectId.ToString();
            entry.new_initiatorid = data.InitiatorId.ToString();
            entry.new_createdbyid = data.CreatedById;
            entry.new_description = data.Description;
            entry.new_followup = data.FollowUp;
            entry.new_ownerid = data.OwnerId;
            entry.new_helpdeskseverityid = data.SeverityId;
            entry.new_helpdeskstatusid = data.StatusId;
            entry.new_title = data.Title;
            entry.new_helpdesktypeid = data.TypeId;
            DataAccessLayer.HdEntry dal = new NoProblem.Core.DataAccessLayer.HdEntry(XrmDataContext);
            if (isUpdate)
            {
                entry.new_helpdeskentryid = data.Id;
                dal.Update(entry);
            }
            else
            {
                dal.Create(entry);
            }
            XrmDataContext.SaveChanges();
            return entry.new_helpdeskentryid;
        }

        public List<DML.HelpDesk.HdEntryData> GetEntries(DML.HelpDesk.GetHdEntriesRequest request)
        {
            DataAccessLayer.HdType typeDal = new NoProblem.Core.DataAccessLayer.HdType(XrmDataContext);
            DML.HelpDesk.HdTypeData typeData = typeDal.GetTypeData(request.HdTypeId);
            if (request.FromDate.HasValue && request.ToDate.HasValue)
            {
                DateTime fromDate = request.FromDate.Value;
                DateTime toDate = request.ToDate.Value;
                FixDatesToUtcFullDays(ref fromDate, ref toDate);
                request.FromDate = fromDate;
                request.ToDate = toDate;
            }
            DataAccessLayer.HdEntry entryDal = new NoProblem.Core.DataAccessLayer.HdEntry(XrmDataContext);
            List<DML.HelpDesk.HdEntryData> entryList = entryDal.GetEntries(request.HdTypeId, request.RegardingObjectId, request.FromDate, request.ToDate, request.StatusId,
                request.InitiatorId, typeData.InitiatorType, request.OwnerId, request.TicketNumber, typeData.RegardingObjectType, request.SecondaryRegardingObjectName, request.FollowUp);
            return entryList;
        }

        public List<DML.HelpDesk.InitiatorsSearchData> SearchInitiators(string input, Guid hdTypeId, Guid initiatorId)
        {
            DataAccessLayer.HdType typeDal = new NoProblem.Core.DataAccessLayer.HdType(XrmDataContext);
            DML.HelpDesk.HdTypeData typeData = typeDal.GetTypeData(hdTypeId);
            List<DML.HelpDesk.InitiatorsSearchData> dataList = null;
            if (typeData.InitiatorType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.InitiatorType.Consumer)
            {
                DataAccessLayer.Contact dal = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
                var consumers = dal.FindConsumersFast(input, initiatorId);
                var transformation = from x in consumers
                                     select new
                                     DML.HelpDesk.InitiatorsSearchData()
                                     {
                                         Id = x.Id,
                                         Field1 = x.FirstName,
                                         Field2 = x.LastName,
                                         Field3 = x.Phone
                                     };
                dataList = transformation.ToList<DML.HelpDesk.InitiatorsSearchData>();
            }
            else if (typeData.InitiatorType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.InitiatorType.Supplier)
            {
                dataList = new List<NoProblem.Core.DataModel.HelpDesk.InitiatorsSearchData>();
            }
            return dataList;
        }

        public List<DataModel.HelpDesk.RegardingObjectSearchData> SearchRegardingObjects(string input, Guid initiatorId, Guid hdTypeId, Guid RegardingObjectId)
        {
            DataAccessLayer.HdType typeDal = new NoProblem.Core.DataAccessLayer.HdType(XrmDataContext);
            DML.HelpDesk.HdTypeData typeData = typeDal.GetTypeData(hdTypeId);
            List<DML.HelpDesk.RegardingObjectSearchData> dataList = null;
            if (typeData.RegardingObjectType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.RegardingObjectType.Call)
            {
                DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                dataList = dal.SearchCallsFast(input, initiatorId, RegardingObjectId);
            }
            else if (typeData.RegardingObjectType == NoProblem.Core.DataModel.Xrm.new_helpdesktype.RegardingObjectType.Case)
            {
                dataList = new List<NoProblem.Core.DataModel.HelpDesk.RegardingObjectSearchData>();
            }
            return dataList;
        }

        public void DeleteHelpDeskEntries(List<Guid> hdEntriesIds)
        {
            DataAccessLayer.HdEntry dal = new NoProblem.Core.DataAccessLayer.HdEntry(XrmDataContext);
            foreach (var id in hdEntriesIds)
            {                
                DML.Xrm.new_helpdeskentry entry = new NoProblem.Core.DataModel.Xrm.new_helpdeskentry(XrmDataContext);
                entry.new_helpdeskentryid = id;
                dal.Delete(entry);
            }
            XrmDataContext.SaveChanges();
        }

        #endregion
    }
}
