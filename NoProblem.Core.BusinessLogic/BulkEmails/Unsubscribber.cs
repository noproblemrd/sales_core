﻿using NoProblem.Core.DataModel.BulkEmails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.BulkEmails
{
    public class Unsubscribber
    {
        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();

        public static UnsubscribeFromBulkEmailsResponse Unsubscribe(Guid supplierId)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("supplierId", supplierId);
            int rowsChanged = dal.ExecuteNonQuery(unsubscribeSql, parameters);
            UnsubscribeFromBulkEmailsResponse response = new UnsubscribeFromBulkEmailsResponse();
            response.Success = rowsChanged == 1;
            if (response.Success)
            {
                response.Email = GetSupplierEmail(supplierId);
            }
            return response;
        }

        private static string GetSupplierEmail(Guid supplierId)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(null);
            return accountRepositoryDal.GetEmail(supplierId);
        }

        private const string unsubscribeSql =
            @"
update accountbase
set donotbulkemail = 1
where accountid = @supplierId
";
    }
}
