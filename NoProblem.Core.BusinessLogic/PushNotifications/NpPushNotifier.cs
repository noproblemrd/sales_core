﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PushNotifications
{
    internal class NpPushNotifier : XrmUserBase
    {
        public NpPushNotifier(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private static Android.GoogleCloundMessagingServer gcmServer = new Android.GoogleCloundMessagingServer();

        public NpPushResult SendPushNotification(Guid supplierId, Dictionary<string, string> data, ePushTypes pushType)
        {
            DataAccessLayer.MobileDevice dal = new DataAccessLayer.MobileDevice(XrmDataContext);
            var query = from md in dal.All
                        where md.new_accountid == supplierId
                        && md.new_isactive == true
                        && md.new_isauthorized == true
                        select new DataModel.Xrm.new_mobiledevice
                        {
                            new_os = md.new_os,
                            new_uid = md.new_uid
                        };
            var found = query.FirstOrDefault();
            if (found == null)
            {
                return new NpPushResult() { Message = "Advertiser does not have active and authorized mobile device.", IsSuccess = false };
            }
            return SendPushNotification(found, data, pushType);
        }

        public NpPushResult SendPushNotification(DataModel.Xrm.new_mobiledevice mobileDevice, Dictionary<string, string> data, ePushTypes pushType)
        {
//#if DEBUG
            IPushServer pushServer = PushServerFactory.GetPushServer(mobileDevice.new_os, mobileDevice.new_isdebug ?? false);
//#else
//            IPushServer pushServer = PushServerFactory.GetPushServer(mobileDevice.new_os);
//#endif
            var pushRequest = new PushRequest
            {
                Data = data,
                DeviceId = mobileDevice.new_uid,
                PushType = pushType
            };
            return pushServer.Send(pushRequest);
        }
    }
}
