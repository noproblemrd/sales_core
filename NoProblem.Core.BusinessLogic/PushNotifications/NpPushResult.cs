﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PushNotifications
{
    public class NpPushResult
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
    }
}
