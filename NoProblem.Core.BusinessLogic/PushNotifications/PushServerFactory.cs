﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PushNotifications
{
    public class PushServerFactory
    {
        /*
        public static IPushServer GetPushServer(string os, string UID)
        {
            IPushServer pushServer = null;
            switch (os)
            {
                case DataModel.Xrm.new_mobiledevice.MobileOSs.IOS:
                    pushServer = (UID == "c555260d51467c96a82f1009a55048d0e12fece9664f21eb5e81e4ca8606c5d3") ? new iOS.APNSServer(true) : new iOS.APNSServer();
                    break;
                case DataModel.Xrm.new_mobiledevice.MobileOSs.ANDROID:
                    pushServer = new Android.GoogleCloundMessagingServer();
                    break;
            }
            return pushServer;
        }
         * */
        public static IPushServer GetPushServer(string os, bool isDebug)
        {
            IPushServer pushServer = null;
            switch (os)
            {
                case DataModel.Xrm.new_mobiledevice.MobileOSs.IOS:
                    pushServer = new iOS.APNSServer(isDebug);
                    break;
                case DataModel.Xrm.new_mobiledevice.MobileOSs.ANDROID:
                    pushServer = new Android.GoogleCloundMessagingServer();
                    break;
            }
            return pushServer;
        }
    }
}
