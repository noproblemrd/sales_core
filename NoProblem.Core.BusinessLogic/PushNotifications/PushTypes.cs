﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PushNotifications
{
    public enum ePushTypes
    {
        BID_REQUEST,
        PAYMENT_ALERT,
        GENERAL_ALERT,
        PAYMENT_APPROVAL,
        CONGRATULATIONS_WON_LEAD,
        REGISTRATION_UNFINISHED,
        REQUEST_UNCONFORTABLE_TIME,
        UNFULFILLMENT_REQUEST ,
        VIDEO_CHAT_JOIN_SUPPLIER
    }
    public class PushTypeMissing
    {
        public enum eTypeMissing
        {
            NAME,
            CATEGORY,
            IMAGE,
            DESCRIPTION,
            COVERAREA,
            ADDRESS,
            GENERAL_MISSING
        }
        public static string GetTypeMissing(string typeMissing)
        {

            eTypeMissing etypeMissing;
            try
            {
                etypeMissing = (eTypeMissing)Enum.Parse(typeof(eTypeMissing), typeMissing);
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "GetTypeMissing. typeMissing = {0}", typeMissing);
                etypeMissing = eTypeMissing.GENERAL_MISSING;
            }

            switch (etypeMissing)
            {
                case (eTypeMissing.NAME):
                    return "company name";
                case (eTypeMissing.CATEGORY):
                    return "profession";
                case (eTypeMissing.IMAGE):
                    return "photo";
                case (eTypeMissing.DESCRIPTION):
                    return "description";
                case (eTypeMissing.COVERAREA):
                    return "service area";
                case (eTypeMissing.ADDRESS):
                    return "address";

            }
            return "details";
        }

    }
}
