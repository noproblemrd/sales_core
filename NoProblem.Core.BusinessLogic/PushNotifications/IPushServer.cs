﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PushNotifications
{
    public interface IPushServer
    {
        NpPushResult Send(PushRequest pushRequest);
    }
}
