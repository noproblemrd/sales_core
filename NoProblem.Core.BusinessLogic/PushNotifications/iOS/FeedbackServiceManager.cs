﻿using Effect.Crm.Logs;
using JdSoft.Apple.Apns.Feedback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PushNotifications.iOS
{
    public class FeedbackServiceManager
    {
        string p12FilePath;
        string p12Password;
        bool sandbox;

        public FeedbackServiceManager(string p12File, string p12Password, bool sandbox)
        {
            this.p12FilePath = p12File;
            this.p12Password = p12Password;
            this.sandbox = sandbox;
        }

        public void Execute()
        {
            //Create the feedback service consumer
            using (FeedbackService service = new FeedbackService(sandbox, p12FilePath, p12Password))
            {
                //Wireup the events
                service.Error += new FeedbackService.OnError(service_Error);
                service.Feedback += new FeedbackService.OnFeedback(service_Feedback);
                //Run it.  This actually connects and receives the feedback
                // the Feedback event will fire for each feedback object
                // received from the server
                service.Run();
                service.Dispose();
            }
        }

        static void service_Feedback(object sender, Feedback feedback)
        {
            LogUtils.MyHandle.WriteToLog("APNS Feedback - Timestamp: {0} - DeviceId: {1}", feedback.Timestamp, feedback.DeviceToken);
        }

        static void service_Error(object sender, Exception ex)
        {
            LogUtils.MyHandle.HandleException(ex, "APNS Feedback Server Error");
        }
    }
}
