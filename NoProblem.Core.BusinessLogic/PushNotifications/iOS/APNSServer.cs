﻿using Effect.Crm.Logs;
using JdSoft.Apple.Apns.Notifications;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using NoProblem.Core.BusinessLogic.LeadBuyers;

namespace NoProblem.Core.BusinessLogic.PushNotifications.iOS
{
    public class APNSServer : IPushServer
    {
        private static DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(null);
        private string p12FilePath;
        private string p12Password;
        private const string DEV_CERTIFICATE_END_NAME = "dev.p12";
        private const string DEV_CERTIFICATE_KEY_IN_WEB_CONFIG = "devCertificatePath";
        private bool sandBox;

        public APNSServer()
            : this(false)
        {

        }

        /// <summary>
        /// True is only for Ofir's tests
        /// </summary>
        /// <param name="useSandbox"></param>
        public APNSServer(bool isDebug)
        {

            if (isDebug)
            {

                p12FilePath = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings.Get(DEV_CERTIFICATE_KEY_IN_WEB_CONFIG); //@"\\c70415wcfil01fs\Web\production\ios_certificates\advertiser_app\cert_nokey_dev.p12";
                p12Password = String.Empty;
                sandBox = true;
            }
            else
            {
                p12FilePath = GetP12FilePath();
                p12Password = GetP12FilePassword();
                sandBox = IsSandbox(p12FilePath);
            }
        }

        private static string GetP12FilePath()
        {
            string retVal;
            if (GlobalConfigurations.IsDevEnvironment)
            {
                retVal = HttpRuntime.AppDomainAppPath + ConfigurationManager.AppSettings.Get(DEV_CERTIFICATE_KEY_IN_WEB_CONFIG);
            }
            else
            {
                retVal = HttpRuntime.AppDomainAppPath + config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.APNS_CERTIFICATE_PATH);
            }
            return retVal;
        }

        private static string GetP12FilePassword()
        {
            return config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.APNS_CERTIFICATE_PASSWORD);
        }

        private static bool IsSandbox(string p12File)
        {
            bool retVal = p12File.EndsWith(DEV_CERTIFICATE_END_NAME);
            return retVal;
        }

        public NpPushResult Send(PushRequest pushRequest)
        {
            int numConnections = 1; // you can change the number of connections here
            NotificationService notificationService = null;
            NpPushResult result = new NpPushResult();
            try
            {
                notificationService = new NotificationService(sandBox, p12FilePath, p12Password, numConnections);
                notificationService.NotificationFailed += notificationService_NotificationFailed;
                notificationService.NotificationTooLong += notificationService_NotificationTooLong;
                LogUtils.MyHandle.WriteToLog(9, string.Format("about to send push notification: {0}", JsonConvert.SerializeObject(pushRequest)));
                var notification = new Notification(pushRequest.DeviceId);
                notification.Payload.Alert.Body = GetAlertText(pushRequest);
                notification.Payload.Sound = GetSound(pushRequest);// "default";                
                notification.Payload.Badge = 1;
         //       notification.Payload.Badge = "'1'";
                foreach (var item in pushRequest.Data)
                {
                    notification.Payload.CustomItems.Add(item.Key, new object[] { item.Value });
                }
                notification.Payload.CustomItems.Add("service", new object[] { pushRequest.PushType.ToString() });
                LogUtils.MyHandle.WriteToLog(9, string.Format("enqueuing push notification : {0}", JsonConvert.SerializeObject(notification)));
                LogUtils.MyHandle.WriteToLog(9, string.Format("raw notification: {0}", notification));
                if (notificationService.QueueNotification(notification))
                {
                    // queued the notification
                    result.IsSuccess = true;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception sending iOS push notification. deviceId = {0}", pushRequest.DeviceId);
            }
            finally
            {
                if (notificationService != null)
                {
                    // This ensures any queued notifications get sent befor the connections are closed
                    notificationService.Close();
                    notificationService.Dispose();
                }
            }
            FeedbackServiceManager feedback = new FeedbackServiceManager(p12FilePath, p12Password, sandBox);
            feedback.Execute();
            return result;
        }

        void notificationService_NotificationTooLong(object sender, NotificationLengthException ex)
        {
            LogUtils.MyHandle.HandleException(ex, "APNS push notification too long");
        }

        void notificationService_NotificationFailed(object sender, Notification failed)
        {
            LogUtils.MyHandle.WriteToLog("APNS push notification failed, device token = {0}, payload = {1}", failed.DeviceToken, failed.Payload.ToString());
        }
        public string GetSound(PushRequest pushRequest)
        {
            ePushTypes pushType = pushRequest.PushType;
            switch (pushType)
            {
                case ePushTypes.VIDEO_CHAT_JOIN_SUPPLIER:
                    return "VideoChatIncomigCallRing.caf";
                default:
                    return "default";
            }
        }
        public static string GetAlertText(PushRequest pushRequest)
        {
            ePushTypes pushType = pushRequest.PushType;
            Dictionary<string, string> parameters = pushRequest.Data;
            switch (pushType)
            {
                case ePushTypes.BID_REQUEST:
                    string category = parameters.ContainsKey("category") ? parameters["category"] : string.Empty;
                    string zipCode = parameters.ContainsKey("address") ? parameters["address"] : string.Empty;
                    return string.Format(BID_REQUEST_ALERT_TEXT, category, zipCode);
                case ePushTypes.CONGRATULATIONS_WON_LEAD:
                    return CONGRATULATIONS_WON_LEAD_TEXT;
                case ePushTypes.REGISTRATION_UNFINISHED:
                    return string.Format(REGISTRATION_UNFINISHED_TEXT, PushTypeMissing.GetTypeMissing(parameters.ContainsKey("typemissing") ? parameters["typemissing"] : string.Empty));
                case ePushTypes.REQUEST_UNCONFORTABLE_TIME:
                case ePushTypes.UNFULFILLMENT_REQUEST:
                case ePushTypes.VIDEO_CHAT_JOIN_SUPPLIER:
                    return (parameters.ContainsKey("message") ? parameters["message"] : string.Empty);
                default:
                    return String.Empty;
            }
        }

        public static string GetAlertTitle(PushRequest pushRequest)
        {
            ePushTypes pushType = pushRequest.PushType;
            switch (pushType)
            {
                case ePushTypes.BID_REQUEST:
                    return "Video request";
                case ePushTypes.CONGRATULATIONS_WON_LEAD:
                    return "Lead won";
                case ePushTypes.REGISTRATION_UNFINISHED:
                    return "Registration unfinished";
                case ePushTypes.REQUEST_UNCONFORTABLE_TIME:
                case ePushTypes.UNFULFILLMENT_REQUEST:
                case ePushTypes.VIDEO_CHAT_JOIN_SUPPLIER:
                    return pushRequest.Data.ContainsKey("title") ? pushRequest.Data["title"] : "Your ClipCall video";
                default:
                    return String.Empty;
            }
        }

        private const string BID_REQUEST_ALERT_TEXT = @"customer video request for a {0} in {1}";
        private const string CONGRATULATIONS_WON_LEAD_TEXT = "Congratulation! You won the lead! We are about to connect you.";
        private const string REGISTRATION_UNFINISHED_TEXT = @"Hi, it's ClipCall. To send you videos from local customers, we need your {0}. Click here to solve this now.";
    }
}
