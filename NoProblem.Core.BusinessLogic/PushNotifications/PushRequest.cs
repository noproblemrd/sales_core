﻿using System.Collections.Generic;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.PushNotifications
{
    public class PushRequest
    {
        public string DeviceId { get; set; } 
        public Dictionary<string, string> Data { get; set; }
        public ePushTypes PushType { get; set; }
    }

    public class PushNotificationContext
    {
        public PushRequest PushRequest { get; set; }
        public new_mobiledevice MobileDevice { get; set; }
    }
}