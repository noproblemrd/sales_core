﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PushNotifications.Android
{
    class AndroidPushNotificationResult
    {
        public string MulticastId { get; set; }
        public int Success { get; set; }
        public int Failure { get; set; }
        public int CanonicalIds { get; set; }
        public List<Result> Results { get; set; }

        [Newtonsoft.Json.JsonIgnore()]
        public System.Net.HttpStatusCode StatusCode { get; set; }

        [Newtonsoft.Json.JsonIgnore()]
        public string ContentOfFailure { get; set; }

        public class Result
        {
            public string MessageId { get; set; }
            public string Error { get; set; }
            public string RegistrationId { get; set; }
        }
    }
}
