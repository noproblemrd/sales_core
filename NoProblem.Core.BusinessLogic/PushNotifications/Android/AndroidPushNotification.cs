﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.PushNotifications.Android
{
    class AndroidPushNotification
    {
        [Newtonsoft.Json.JsonProperty("registration_ids")]
        [RestSharp.Serializers.SerializeAs(Name = "registration_ids")]
        public List<string> RegistrationIds { get; private set; }

        [Newtonsoft.Json.JsonProperty("collapse_key", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public string CollapseKey { get; set; }

        [Newtonsoft.Json.JsonProperty("delay_while_idle", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public bool? DelayWhileIdle { get; set; }

        [Newtonsoft.Json.JsonProperty("time_to_live", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public int? TimeToLive { get; set; }

        [Newtonsoft.Json.JsonProperty("dry_run", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public bool? DryRun { get; set; }

        [Newtonsoft.Json.JsonProperty("data", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public object Data { get; set; }

        [Newtonsoft.Json.JsonProperty("notification", NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public object Notification { get; set; }

        public AndroidPushNotification()
        {
            RegistrationIds = new List<string>();
        }
    }
}
