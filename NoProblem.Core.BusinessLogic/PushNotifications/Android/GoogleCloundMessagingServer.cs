﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.PushNotifications.iOS;


namespace NoProblem.Core.BusinessLogic.PushNotifications.Android
{
    class GoogleCloundMessagingServer : IPushServer
    {
        private const string URL = "https://android.googleapis.com/gcm/send";
        private readonly string API_KEY;

        public GoogleCloundMessagingServer()
        {
            DataAccessLayer.ConfigurationSettings settings = new DataAccessLayer.ConfigurationSettings(null);
            API_KEY = settings.GetConfigurationSettingValue(DataModel.ConfigurationKeys.GCM_API_KEY_V2);
        }

        internal AndroidPushNotificationResult SendPushNotification(AndroidPushNotification pushNotification)
        {
            RestSharp.RestClient client = new RestSharp.RestClient(URL);
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.AddHeader("Authorization", "key=" + API_KEY);
            request.AddHeader("Content-Type", "application/json");
            request.RequestFormat = RestSharp.DataFormat.Json;
            request.JsonSerializer = new Utils.RestSharpJsonNetSerializer();
            request.AddBody(pushNotification);
            RestSharp.IRestResponse<AndroidPushNotificationResult> response = client.Execute<AndroidPushNotificationResult>(request);
            AndroidPushNotificationResult result = response.Data;
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                result = new AndroidPushNotificationResult();
                result.ContentOfFailure = response.Content;
            }
            result.StatusCode = response.StatusCode;
            return result;
        }

        public NpPushResult Send(PushRequest pushRequest)
        {
            var notificationPayload = new Dictionary<string, string>();
            notificationPayload.Add("title", APNSServer.GetAlertTitle(pushRequest));
            notificationPayload.Add("message", APNSServer.GetAlertText(pushRequest));
            notificationPayload.Add("sound", "default");
            var notification = new AndroidPushNotification {DelayWhileIdle = true};
            pushRequest.Data.Add("service", pushRequest.PushType.ToString());
            if (!pushRequest.Data.ContainsKey("message"))
            {
                string message = APNSServer.GetAlertText(pushRequest);
                pushRequest.Data.Add("message", message);
            }
            if (!pushRequest.Data.ContainsKey("title"))
            {
                pushRequest.Data.Add("title", APNSServer.GetAlertTitle(pushRequest));
            }
            pushRequest.Data.Add("badge", "1");
            notification.Data = pushRequest.Data;
            notification.RegistrationIds.Add(pushRequest.DeviceId);
            notification.CollapseKey = pushRequest.PushType.ToString();
            notification.Notification = notificationPayload;
            Android.AndroidPushNotificationResult result = SendPushNotification(notification);
            NpPushResult retVal = new NpPushResult();
            retVal.IsSuccess = result.StatusCode == System.Net.HttpStatusCode.OK && result.Success > 0;
            if (retVal.IsSuccess)
            {
                retVal.Message = result.Results.First().MessageId;
            }
            else if (result.Results.Count > 0)
            {
                retVal.Message = result.Results.First().Error;
            }
            else
            {
                retVal.Message = result.ContentOfFailure;
            }
            return retVal;
        }

        /*
          var data =
                new
                {
                    id = ia.new_incidentaccountid,
                    description = incident.description,
                    max_bid = maxPrice,
                    min_bid = minPrice,
                    category = incident.new_new_primaryexpertise_incident.new_name,
                    service = PUSH_SERVICE_NAME
                };
            return data;
         */
    }
}