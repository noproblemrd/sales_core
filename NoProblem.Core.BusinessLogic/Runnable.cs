﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic
{
    public abstract class Runnable : XrmUserBase
    {
        public Runnable()
            : this(ThreadPriority.Normal)
        {

        }

        public Runnable(ThreadPriority priority)
            : base(null)
        {
            thread = new Thread(new ThreadStart(InnerRun));
            thread.Priority = priority;
        }

        private Thread thread;

        protected abstract void Run();

        public void Start()
        {
            thread.Start();
        }

        public void Join()
        {
            thread.Join();
        }

        private void InnerRun()
        {
            try
            {
                Run();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Unhandled exception in thread caught at Runnable.InnerRun");
            }
        }
    }
}