﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using CookComputing.XmlRpc;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Asterisk;

namespace NoProblem.Core.BusinessLogic.AsteriskBL
{
    internal class AsteriskCallsExecuter : XrmUserBase
    {
        #region Ctor
        public AsteriskCallsExecuter(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            asteriskCofig = new NoProblem.Core.DataAccessLayer.AsteriskConfiguration(XrmDataContext);
        }
        #endregion

        #region Fields

        DataAccessLayer.AsteriskConfiguration asteriskCofig;

        private string _phoneProtocol;
        private string PhoneProtocol
        {
            get
            {
                if (string.IsNullOrEmpty(_phoneProtocol))
                {
                    _phoneProtocol = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.PHONE_PROTOCOL);
                }
                return _phoneProtocol;
            }
        }

        private int _callLimit;
        private int CallLimit
        {
            get
            {
                if (_callLimit == default(int))
                {
                    string callLimitStr = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.CALL_LIMIT);
                    if (!int.TryParse(callLimitStr, out _callLimit))
                    {
                        _callLimit = 600;
                    }
                }
                return _callLimit;
            }
        }

        private string _host;
        private string Host
        {
            get
            {
                if (string.IsNullOrEmpty(_host))
                {
                    _host = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.HOST);
                }
                return _host;
            }
        }

        private string _c2cReturnAddress;
        private string C2cReturnAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_c2cReturnAddress))
                {
                    _c2cReturnAddress = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.C2C_RETURN_ADDRESS);
                }
                return _c2cReturnAddress;
            }
        }

        private string _nonPpcReturnAddress;
        private string NonPpcReturnAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_nonPpcReturnAddress))
                {
                    _nonPpcReturnAddress = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.NON_PPC_RETURN_ADDRESS);
                }
                return _nonPpcReturnAddress;
            }
        }

        private string _ttsReturnAddress;
        private string TtsReturnAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_ttsReturnAddress))
                {
                    _ttsReturnAddress = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.TTS_RETURN_ADDRESS);
                }
                return _ttsReturnAddress;
            }
        }

        private string _appName;
        private string AppName
        {
            get
            {
                if (string.IsNullOrEmpty(_appName))
                {
                    _appName = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.APP_NAME);
                }
                return _appName;
            }
        }

        private string _cid;
        private string Cid
        {
            get
            {
                if (string.IsNullOrEmpty(_cid))
                {
                    _cid = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.CID);
                }
                return _cid;
            }
        }

        private string _promptClass;
        private string PromptClass
        {
            get
            {
                if (string.IsNullOrEmpty(_promptClass))
                {
                    _promptClass = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.PROMPTS_CLASS_OLD);
                }
                return _promptClass;
            }
        }

        private string _countryCode;
        private string CountryCode
        {
            get
            {
                if (string.IsNullOrEmpty(_countryCode))
                {
                    _countryCode = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.COUNTRY_CODE);
                }
                return _countryCode;
            }
        }

        private string _internalCountryPrefix;
        private string InternalCountryPrefix
        {
            get
            {
                if (string.IsNullOrEmpty(_internalCountryPrefix))
                {
                    _internalCountryPrefix = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.INTERNAL_COUNTRY_PREFIX);
                }
                return _internalCountryPrefix;
            }
        }

        private string _localAreaCode;
        private string LocalAreaCode
        {
            get
            {
                if (string.IsNullOrEmpty(_localAreaCode))
                {
                    _localAreaCode = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.LOCAL_AREA_CODE);
                }
                return _localAreaCode;
            }
        }

        private string _internationalExtension;
        private string InternationalExtension
        {
            get
            {
                if (string.IsNullOrEmpty(_internationalExtension))
                {
                    _internationalExtension = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.INTERNATIONAL_EXTENSION);
                }
                return _internationalExtension;
            }
        }

        private string _asteriskC2cUrl;
        private string AsteriskC2cUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_asteriskC2cUrl))
                {
                    _asteriskC2cUrl = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.ASTERISK_C2C_URL);
                }
                return _asteriskC2cUrl;
            }
        }

        private string _asteriskTtsUrl;
        private string AsteriskTtsUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_asteriskTtsUrl))
                {
                    _asteriskTtsUrl = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.ASTERISK_TTS_URL);
                }
                return _asteriskTtsUrl;
            }
        }

        private string _asteriskRecordingsUrl;
        private string AsteriskRecordingsUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_asteriskRecordingsUrl))
                {
                    _asteriskRecordingsUrl = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.ASTERISK_RECORDINGS_URL);
                }
                return _asteriskRecordingsUrl;
            }
        }

        private string _recordingsFolder;
        private string RecordingsFolder
        {
            get
            {
                if (string.IsNullOrEmpty(_recordingsFolder))
                {
                    _recordingsFolder = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.RECORDINGS_FOLDER);
                }
                return _recordingsFolder;
            }
        }

        private string _viennaReturnAddress;
        internal string ViennaReturnAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_viennaReturnAddress))
                {
                    _viennaReturnAddress = System.Configuration.ConfigurationManager.AppSettings["ViennaReturnAddress"];
                }
                return _viennaReturnAddress;
            }
        }

        private string _viennaTrunk;
        internal string ViennaTrunk
        {
            get
            {
                if (string.IsNullOrEmpty(_viennaTrunk))
                {
                    _viennaTrunk = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.VIENNA_TRUNK);
                }
                return _viennaTrunk;
            }
        }

        private int? _successfulCallMinLength;
        internal int SuccessfulCallMinLength
        {
            get
            {
                if (!_successfulCallMinLength.HasValue)
                {
                    string tmpStr = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.SUCCESSFUL_CALL_MIN_LENGTH);
                    int tmpInt;
                    if (int.TryParse(tmpStr, out tmpInt))
                    {
                        _successfulCallMinLength = tmpInt;
                    }
                    else
                    {
                        _successfulCallMinLength = -1;
                    }
                }
                return _successfulCallMinLength.Value;
            }
        }

        private int _recordLegs;
        internal int RecordLegs
        {
            get
            {
                if (_recordLegs == default(int))
                {
                    string recordLegsStr = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.RECORD_LEGS);
                    if (!int.TryParse(recordLegsStr, out _recordLegs))
                    {
                        _recordLegs = default(int);
                    }
                }
                return _recordLegs;
            }
        }

        private string _lang;
        private string Lang
        {
            get
            {
                if (string.IsNullOrEmpty(_lang))
                {
                    _lang = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.LANG);
                }
                return _lang;
            }
        }

        private string _voice;
        private string Voice
        {
            get
            {
                if (string.IsNullOrEmpty(_voice))
                {
                    _voice = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.VOICE);
                }
                return _voice;
            }
        }

        internal enum eRecordLegsValues
        {
            both = 1,
            target = 2,//supplier
            originator = 3//consumer
        }

        #endregion

        #region Internal Methods

        internal string InitiateC2c(string originatorPhone, string targetPhone, bool recordCall, string description, bool rejectEnabled, Guid id, string headingName, bool isNonPPC)
        {
            string sessionId = null;
            try
            {
                bool recordOriginator;
                bool recordTarget;
                CalculateRecordingLegs(recordCall, out recordOriginator, out recordTarget);
                C2cRequest request = CreateRequest(originatorPhone, targetPhone, description, id, recordOriginator, recordTarget, rejectEnabled, headingName, isNonPPC);
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(request);
                sessionId = PostJson(json);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in InitiateC2c");
            }
            return sessionId;
        }

        internal string InitiateTts(string targetPhone, string description, int minValue, int maxValue, Guid id, bool rejectEnabled)
        {
            string sessionId = null;
            try
            {
                TtsFileCreator ttsCreator = new TtsFileCreator();
                string ttsFile = ttsCreator.GetTtsFileUrl(id.ToString(), description);
                if (!string.IsNullOrEmpty(ttsFile))
                {
                    IAsteriskTts asteriskTts = (IAsteriskTts)XmlRpcProxyGen.Create(typeof(IAsteriskTts));
                    asteriskTts.Url = AsteriskTtsUrl;
                    XmlRpcStruct p = new XmlRpcStruct();
                    p.Add("target", NormalizeNumber(targetPhone));
                    p.Add("targetProto", PhoneProtocol);
                    p.Add("ttsAddress", ttsFile);
                    p.Add("returnAddress", TtsReturnAddress);
                    p.Add("app_session_id", id.ToString());
                    p.Add("lang", Lang);
                    p.Add("voice", Voice);
                    p.Add("recordingclass", rejectEnabled ? PromptClass + "_with_reject" : PromptClass);
                    p.Add("validdigits", "int");
                    if (minValue == 0)
                        minValue = 1;
                    p.Add("minval", minValue);
                    if (maxValue == 0)
                        maxValue = 1;
                    p.Add("maxval", maxValue);
                    p.Add("cid", Cid);
                    p.Add("rejectKey", rejectEnabled ? 1 : 0);

                    var mystruct = asteriskTts.TtsAdd(p);
                    sessionId = (string)((XmlRpcStruct)mystruct["initResponse"])["internal_id"];
                }
            }
            catch (XmlRpcFaultException fex)
            {
                LogUtils.MyHandle.HandleException(fex, "Exception (XmlRpcFaultException) calling asterisk tts. Fault Response: {0} {1}", fex.FaultCode, fex.FaultString);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception calling asterisk tts");
            }
            return sessionId;
        }

        /// <summary>
        /// Downloads the recording file from Asterisk, saves it in our server and returns the path to the file.
        /// </summary>
        /// <param name="asteriskSessionId">The ivrSession</param>
        /// <returns>The path to the new saved file</returns>
        internal string RetrieveRecording(string asteriskSessionId, string sessionServer, out bool alreadyExisted)
        {
            alreadyExisted = false;
            if (/*string.IsNullOrEmpty(AsteriskRecordingsUrl) || */string.IsNullOrEmpty(RecordingsFolder))
            {
                //throw new Exception("Asterisk configuration ASTERISK_RECORDINGS_URL or RECORDINGS_FOLDER are not configured correctly.");
                throw new Exception("Asterisk configuration RECORDINGS_FOLDER is not configured correctly.");
            }
            DateTime d = DateTime.Now;
            string fullDirectory = RecordingsFolder + "\\" + d.Year + "\\" + d.Month + "\\" + d.Day;
            if (!Directory.Exists(fullDirectory))
            {
                Directory.CreateDirectory(fullDirectory);
            }
            string fullPath = fullDirectory + "\\" + asteriskSessionId + ".mp3";
            string retVal = null;
            if (!File.Exists(fullPath))
            {
                //string uri = AsteriskRecordingsUrl + "?session_id=" + asteriskSessionId;
                string uri = sessionServer + "recordings/?session_id=" + asteriskSessionId;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Http.Get;
                try
                {
                    using (WebResponse response = request.GetResponse())
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            try
                            {
                                int bufferSize = 1024;
                                byte[] buffer = new byte[bufferSize];
                                int bytesRead = 0;
                                bool isWave = false;
                                using (FileStream fileStream = File.Create(fullPath))
                                {
                                    int iter = 0;
                                    while ((bytesRead = responseStream.Read(buffer, 0, bufferSize)) != 0)
                                    {
                                        fileStream.Write(buffer, 0, bytesRead);
                                        if (iter == 0)
                                        {
                                            string bytesString = System.Text.Encoding.UTF8.GetString(buffer);
                                            if (bytesString.Substring(0, 13).ToUpper().Contains("WAVE"))
                                            {
                                                isWave = true;
                                            }
                                        }
                                        iter++;
                                    }
                                }
                                if (isWave)
                                {
                                    string newPath = Path.ChangeExtension(fullPath, "wav");
                                    File.Move(fullPath, newPath);
                                    Utils.WavToMp3Converter converter = new NoProblem.Core.BusinessLogic.Utils.WavToMp3Converter(XrmDataContext);
                                    bool converted = converter.ConvertWavMP3(newPath);
                                    if (!converted)
                                    {
                                        fullPath = newPath;
                                    }
                                }
                                retVal = fullPath;
                            }
                            catch (Exception exc)
                            {
                                LogUtils.MyHandle.HandleException(exc, "Exception saving recording file. asteriskSessionId={0}", asteriskSessionId);
                            }
                        }
                    }
                }
                catch (WebException e)
                {
                    using (WebResponse response = e.Response)
                    {
                        HttpWebResponse httpResponse = (HttpWebResponse)response;
                        if (httpResponse != null)
                        {
                            if (httpResponse.StatusCode != HttpStatusCode.NotFound)
                            {
                                using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                                {
                                    string txt = sr.ReadToEnd();
                                    LogUtils.MyHandle.HandleException(e, "Exception (WebException) in AsteriskCallsExecuter.RetrieveRecording. result={0}. asteriskSessionId={1}", txt, asteriskSessionId);
                                }
                            }
                        }
                        else
                        {
                            LogUtils.MyHandle.HandleException(e, "Exception (WebException) in AsteriskCallsExecuter.RetrieveRecording. asteriskSessionId={1}", asteriskSessionId);
                        }
                    }
                }
            }
            else
            {
                retVal = fullPath;
                alreadyExisted = true;
            }
            return retVal;
        }

        internal void DeleteRecordingInAsteriskServer(string asteriskSessionId)
        {
            try
            {
                var client = new RestSharp.RestClient();
                client.BaseUrl = new Uri(AsteriskRecordingsUrl);
                var restrequest = new RestSharp.RestRequest();
                restrequest.Resource = "//";
                restrequest.Method = RestSharp.Method.DELETE;
                restrequest.AddParameter("session_id", asteriskSessionId);
                client.Execute(restrequest);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DeleteRecordingInAsteriskServer. asteriskSessionId = {0}", asteriskSessionId);
            }
        }

        internal string NormalizeNumber(String phone)
        {
            if (phone.StartsWith("+"))
            {
                phone = phone.Substring(1);
            }
            else
                return phone;

            String CountryPluslocalAreaCode = CountryCode + LocalAreaCode;
            if (phone.StartsWith(CountryPluslocalAreaCode) && !string.IsNullOrEmpty(LocalAreaCode))
            {
                return phone.Substring(CountryPluslocalAreaCode.Length);
            }
            else if (phone.StartsWith(CountryCode))
            {
                return InternalCountryPrefix + phone.Substring(CountryCode.Length);
            }
            else
            {
                return InternationalExtension + phone;
            }
        }

        #endregion

        #region Private Methods

        private void CalculateRecordingLegs(bool recordCall, out bool recordOriginator, out bool recordTarget)
        {
            recordOriginator = false;
            recordTarget = false;
            if (recordCall)
            {
                if (RecordLegs == (int)eRecordLegsValues.originator)
                {
                    recordOriginator = true;
                }
                else if (RecordLegs == (int)eRecordLegsValues.target)
                {
                    recordTarget = true;
                }
                else
                {
                    recordOriginator = true;
                    recordTarget = true;
                }
            }
        }

        private C2cRequest CreateRequest(string originatorPhone, string targetPhone, string description, Guid id, bool recordOriginator, bool recordTarget, bool rejectEnabled, string headingName, bool isNonPPC)
        {
            C2cRequest request = new C2cRequest();
            C2cRequest.InternalWrapper wrapper = new C2cRequest.InternalWrapper();
            wrapper.prompts_class = PromptClass;
            if (!string.IsNullOrEmpty(description))
            {
                TtsFileCreator ttsCreator = new TtsFileCreator();
                string ttsUrl = ttsCreator.GetTtsFileUrl(id.ToString(), description);
                if (!string.IsNullOrEmpty(ttsUrl))
                {
                    wrapper.ttsAddress = ttsUrl;
                    if (rejectEnabled)
                    {
                        wrapper.Keys = new Dictionary<string, string>();
                        wrapper.Keys.Add("1", C2cRequest.C2cClickMethods.PASS);
                        wrapper.Keys.Add("2", C2cRequest.C2cClickMethods.PASS);
                        wrapper.Keys.Add("3", C2cRequest.C2cClickMethods.PASS);
                        wrapper.Keys.Add("4", C2cRequest.C2cClickMethods.PASS);
                        wrapper.Keys.Add("5", C2cRequest.C2cClickMethods.PASS);
                        wrapper.Keys.Add("6", C2cRequest.C2cClickMethods.PASS);
                        wrapper.Keys.Add("7", C2cRequest.C2cClickMethods.PASS);
                        wrapper.Keys.Add("8", C2cRequest.C2cClickMethods.PASS);
                        wrapper.Keys.Add("9", C2cRequest.C2cClickMethods.PASS);
                        wrapper.Keys.Add("0", C2cRequest.C2cClickMethods.REJECT_CALL);
                        wrapper.Keys.Add("*", C2cRequest.C2cClickMethods.REPROMPT);
                        wrapper.Keys.Add("#", C2cRequest.C2cClickMethods.PASS);
                    }
                }
            }
            request.call_init = wrapper;
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            wrapper.call_limit = CallLimit;
            wrapper.host = Host;
            string consumerPromptText = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.CONSUMER_CONNECTION_PROMPT);
            if (!string.IsNullOrEmpty(consumerPromptText))
            {
                TtsFileCreator ttsCreator = new TtsFileCreator();
                string conPromptAddress = ttsCreator.GetTtsFileUrl(id.ToString() + "-conprompt", string.Format(consumerPromptText, headingName));
                wrapper.consumer_prompt_address = conPromptAddress;
            }
            wrapper.originator = NormalizeNumber(originatorPhone);
            wrapper.target = NormalizeNumber(targetPhone);
            wrapper.forceCall = 0;
            wrapper.recording = 1;
            wrapper.returnAddress = isNonPPC ? NonPpcReturnAddress : C2cReturnAddress;
            wrapper.app_name = AppName;
            wrapper.app_session_id = id.ToString();
            wrapper.cid = Cid;
            wrapper.moh_class = "";
            wrapper.originatorProto = PhoneProtocol;
            wrapper.record_originator = recordOriginator ? 1 : 0;
            wrapper.record_target = recordTarget ? 1 : 0;
            wrapper.retries = 1;
            wrapper.ring_policy = "ringall";
            wrapper.schedule = "5";
            wrapper.targetProto = PhoneProtocol;
            wrapper.validity = 1200;
            return request;
        }

        private string PostJson(string json)
        {
            string uri = AsteriskC2cUrl;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] postBuffer = Encoding.UTF8.GetBytes(json);
            request.ContentLength = postBuffer.Length;
            using (Stream RequestStream = request.GetRequestStream())
            {
                RequestStream.Write(postBuffer, 0, postBuffer.Length);
            }
            string result = null;
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        try
                        {
                            CookComputing.XmlRpc.XmlRpcSerializer ser = new CookComputing.XmlRpc.XmlRpcSerializer();
                            var res = ser.DeserializeResponse(responseStream, typeof(CookComputing.XmlRpc.XmlRpcStruct));
                            result = (string)((XmlRpcStruct)((XmlRpcStruct)res.retVal)["initResponse"])["ivr_session_id"];
                        }
                        catch (Exception exc)
                        {
                            using (StreamReader sr = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                string text = sr.ReadToEnd();
                                LogUtils.MyHandle.HandleException(exc, "Exception deserializing response of asterisk c2c. response: {0}", text);
                            }
                        }
                    }
                }
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    string resultStatusCode = httpResponse.StatusCode.ToString();
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        string text = sr.ReadToEnd();
                        LogUtils.MyHandle.HandleException(e, "Exception (WebException) calling asterisk c2c. httpResponse.StatusCode: {1} response: {0}", text, resultStatusCode);
                    }
                }
            }
            return result;
        }

        #endregion
    }
}
