﻿using CookComputing.XmlRpc;

namespace NoProblem.Core.BusinessLogic.AsteriskBL
{

    [XmlRpcUrl("http://10.0.0.24/tts/")]
    public interface IAsteriskTts : IXmlRpcProxy
    {
        [XmlRpcMethod("tts.add")]
        XmlRpcStruct TtsAdd(XmlRpcStruct myStruct);
    }
}
