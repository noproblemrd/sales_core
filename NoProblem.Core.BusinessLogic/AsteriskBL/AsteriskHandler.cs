﻿
using NoProblem.Core.DataModel.Asterisk;
namespace NoProblem.Core.BusinessLogic.AsteriskBL
{
    public class AsteriskHandler : XrmUserBase
    {
        public AsteriskHandler(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public void C2cStatus(DataModel.Asterisk.C2cStatusContainer statusContainer)
        {
            BusinessLogic.ServiceRequest.JonaManager jona = new NoProblem.Core.BusinessLogic.ServiceRequest.JonaManager(XrmDataContext);
            jona.HandleCallStatus(statusContainer);
        }

        public void TtsStatus(DataModel.Asterisk.TtsStatusContainer statusContainer)
        {
            BusinessLogic.ServiceRequest.JonaManager jona = new NoProblem.Core.BusinessLogic.ServiceRequest.JonaManager(XrmDataContext);
            jona.HandleTtsCallStatus(statusContainer);
        }

        public DataModel.Asterisk.VnQueryResponse VnQuery(DataModel.Asterisk.VnQueryRequest container)
        {
            ViennaManager manager = new ViennaManager(XrmDataContext);
            return manager.VnQuery(container);
        }

        public void VnStatus(NoProblem.Core.DataModel.Asterisk.VnStatusRequest container)
        {
            ViennaManager manager = new ViennaManager(XrmDataContext);
            manager.VnStatus(container);
        }

        public void NonPpcC2cStatus(C2cStatusContainer statusContainer)
        {
            NoProblem.Core.BusinessLogic.Dialer.NonPpcPhoneManager manager = new NoProblem.Core.BusinessLogic.Dialer.NonPpcPhoneManager(XrmDataContext);
            manager.HandleCallStatus(statusContainer);
        }
    }
}
