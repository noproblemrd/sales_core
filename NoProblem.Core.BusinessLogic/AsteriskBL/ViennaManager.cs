﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Asterisk;
using Effect.Crm.Logs;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.AsteriskBL
{
    internal class ViennaManager : XrmUserBase
    {
        #region Ctor
        internal ViennaManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Fields

        private static SynchronizedCollection<string> reportList = new SynchronizedCollection<string>();
        private static SynchronizedCollection<RecordingsRetryContainer> recordingsRetriesHolder = new SynchronizedCollection<RecordingsRetryContainer>();
        private delegate void GetRecordingDelegete(string ivrSession, string sessionServer);

        #endregion

        #region Internal Methods

        internal void VnStatus(VnStatusRequest container)
        {
            DataAccessLayer.VnRequest vnRequestDal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
            DataAccessLayer.VnStatus vnStatusDal = new NoProblem.Core.DataAccessLayer.VnStatus(XrmDataContext);

            var requestEntity = vnRequestDal.GetVnRequestByIvrSession(container.IvrSession);
            if (requestEntity == null)
            {
                throw new Exception(string.Format("VnRequest not found in DB. IvrSession: {0}. IvrStatus: {1}, IsWaitForDigit: {2}, Digit: {3}", container.IvrSession, container.IvrStatus, container.IsWaitForDigit, container.Digit));
            }
            string ivrStatus;
            int sourceCallDuration;
            string contactedPhoneNumber;
            int targetCallDuration;
            ParseStatusParameters(container, out ivrStatus, out sourceCallDuration, out contactedPhoneNumber, out targetCallDuration);

            CreateStatusEntity(container, vnStatusDal, requestEntity, targetCallDuration, sourceCallDuration);

            if (container.IsWaitForDigit == "dapaz_books")
            {
                IvrWidget.AsteriskIvrWidgetManager asteriskIvrWidgetManager = new NoProblem.Core.BusinessLogic.IvrWidget.AsteriskIvrWidgetManager(XrmDataContext);
                asteriskIvrWidgetManager.CloseIvrWidgetCall(container.IvrSession, container.Digit, container.IvrStatus);
            }
            else
            {                
                if (ivrStatus == VnStatusCode.CALL_CDR ||
                    ivrStatus == VnStatusCode.CALL_BUSY ||
                    ivrStatus == VnStatusCode.CALL_NOANSWER ||
                    ivrStatus == VnStatusCode.CALL_CANCEL ||
                    ivrStatus == VnStatusCode.CALL_CONGESTION ||
                    ivrStatus == VnStatusCode.CANCEL)
                {
                    bool report = false;
                    lock (reportList)
                    {
                        if (reportList.Contains(container.IvrSession))
                        {
                            reportList.Remove(container.IvrSession);
                            report = true;
                        }
                    }
                    if (report)
                    {
                        int reasonCode = 0;
                        switch (ivrStatus)
                        {
                            case VnStatusCode.CALL_CDR:
                                reasonCode = 0;
                                break;
                            case VnStatusCode.CALL_BUSY:
                            case VnStatusCode.CALL_NOANSWER:
                            case VnStatusCode.CALL_CANCEL:
                            case VnStatusCode.CALL_CONGESTION:
                            case VnStatusCode.CANCEL:
                                reasonCode = 1;
                                break;
                        }
                        //if call was short then change reasonCode to indicate no success
                        AsteriskCallsExecuter executer = new AsteriskCallsExecuter(XrmDataContext);
                        if (targetCallDuration < executer.SuccessfulCallMinLength)
                            reasonCode = 1;
                        Phones phones = new Phones(XrmDataContext, null);
                        phones.DirectNumberContactedAsync(
                            requestEntity.new_ivrcallerid,
                            requestEntity.new_ivrinbound,
                            reasonCode,
                            container.IvrSession,
                            targetCallDuration,
                            container.IvrSession,
                            null,
                            false,
                            null, null, null, null);

                        if (ivrStatus == VnStatusCode.CALL_CDR &&
                            requestEntity.new_recordcall.HasValue &&
                            requestEntity.new_recordcall.Value)
                        {
                            GetRecordingDelegete del = new GetRecordingDelegete(GetRecording);
                            del.BeginInvoke(container.IvrSession, requestEntity.new_ivrsourceurl, null, null);
                        }
                    }
                }
            }
        }

        internal VnQueryResponse VnQuery(DataModel.Asterisk.VnQueryRequest container)
        {
            VnQueryResponse response = new VnQueryResponse();
            try
            {
                DataAccessLayer.VnRequest dal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
                Dialer.IncomingCallsAntiFraud fraudAnalist = new NoProblem.Core.BusinessLogic.Dialer.IncomingCallsAntiFraud(XrmDataContext);
                bool passed = fraudAnalist.CheckAntiFraud(container.IvrCallerId, dal);
                if (!passed)
                {
                    DataModel.Xrm.new_vnrequest fraudentity = new NoProblem.Core.DataModel.Xrm.new_vnrequest();
                    fraudentity.new_ivrsession = container.IvrSession;
                    fraudentity.new_ivrinbound = container.IvrInbound;
                    fraudentity.new_ivrcallerid = container.IvrCallerId;
                    fraudentity.new_ivrsourceurl = container.SourceUrl;
                    fraudentity.new_doreport = false;
                    fraudentity.new_antifraudblocked = true;
                    dal.Create(fraudentity);
                    XrmDataContext.SaveChanges();
                    response.ErrorMsg = "AntiFraudBlocked";
                    response.ErrorCode = (int)VnQueryResponse.enumErrorCode.error;
                    return response;
                }
                Phones phones = new Phones(XrmDataContext, null);
                DataModel.DNQueryResult result = phones.GetPhoneNumber(container.IvrSession, container.IvrInbound, container.IvrCallerId);
                AsteriskCallsExecuter executer = new AsteriskCallsExecuter(XrmDataContext);

                if (!result.IsBlacklisted)
                {
                    if (result.IsWaitForDigit == "0")
                    {
                        for (int i = 0; i < result.TruePhoneNumber.Count; i++)
                        {
                            result.TruePhoneNumber[i] = executer.NormalizeNumber(result.TruePhoneNumber[i]);
                        }
                        response.IvrTarget = result.TruePhoneNumber.ToArray();
                    }
                    else
                    {
                        response.IsWaitForDigit = result.IsWaitForDigit;
                    }
                }
                else
                {
                    response.ErrorMsg = "BlackList";
                    response.ErrorCode = (int)VnQueryResponse.enumErrorCode.error;
                    result.MappingStatus = response.ErrorMsg;
                }
                response.ReturnAddress = executer.ViennaReturnAddress;
                response.IvrTrunk = executer.ViennaTrunk;
                response.Inc2Session = result.ProviderId.ToString();

                if (response.ErrorCode != (int)VnQueryResponse.enumErrorCode.error && result.IsWaitForDigit == "0")
                {
                    if (result.TruePhoneNumber.Count > 1)
                    {
                        response.ErrorCode = (int)VnQueryResponse.enumErrorCode.play_ivr_caller_announce_wait_for_input;
                    }
                    else
                    {
                        response.ErrorCode = (int)VnQueryResponse.enumErrorCode.success;
                    }
                    response.IvrAdvertiserAnnounce = result.IvrAdvertiserAnnounce;
                    response.IvrCallerAnnounce = result.IvrCallerAnnounce;
                    response.IvrDisplayClid = result.DisplayPhoneNumber;
                    response.IvrFindAlternativeAdvertiser = result.IvrFindAlternativeAdvertiser;
                    response.IvrNoAlternativeFound = result.IvrNoAlternativeFound;
                    if (result.IsRecording)
                    {
                        if (executer.RecordLegs == (int)AsteriskCallsExecuter.eRecordLegsValues.originator)
                        {
                            response.Recording = VnQueryResponse.RecordingValues.IN;
                        }
                        else if (executer.RecordLegs == (int)AsteriskCallsExecuter.eRecordLegsValues.target)
                        {
                            response.Recording = VnQueryResponse.RecordingValues.OUT;
                        }
                        else
                        {
                            response.Recording = VnQueryResponse.RecordingValues.YES;
                        }
                    }
                    else
                    {
                        response.Recording = VnQueryResponse.RecordingValues.NO;
                    }
                }
                DataModel.Xrm.new_vnrequest entity = new NoProblem.Core.DataModel.Xrm.new_vnrequest();
                if (result.IsWaitForDigit == "0" && response.IvrTarget.Length > 0)
                {
                    entity.new_target1 = response.IvrTarget[0];
                    if (response.IvrTarget.Length > 1)
                    {
                        entity.new_target2 = response.IvrTarget[1];
                    }
                }
                entity.new_recordcall = result.IsRecording;
                entity.new_ivrsession = container.IvrSession;
                entity.new_ivrinbound = container.IvrInbound;
                entity.new_ivrcallerid = container.IvrCallerId;
                entity.new_ivrsourceurl = container.SourceUrl;
                entity.new_mappingstatus = result.MappingStatus;
                entity.new_calltype = result.CallType;
                if (result.ProviderId != Guid.Empty)
                {
                    entity.new_accountid = result.ProviderId;
                }
                if (result.IsAvailable && result.IsWaitForDigit == "0")
                {
                    entity.new_doreport = true;
                    reportList.Add(container.IvrSession);
                }
                dal.Create(entity);
                XrmDataContext.SaveChanges();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ViennaManager.VnQuery");
                response.ErrorMsg = "UnexpectedException: " + exc.Message;
                response.ErrorCode = (int)VnQueryResponse.enumErrorCode.error;
            }

            return response;
        }

        #endregion

        #region Private Methods

        private void ParseStatusParameters(VnStatusRequest container, out string ivrStatus, out int sourceCallDuration, out string contactedPhoneNumber, out int targetCallDuration)
        {
            string[] ivrStatusCSVValues = container.IvrStatus.Split(';');
            ivrStatus = null;
            sourceCallDuration = 0;
            targetCallDuration = 0;
            ivrStatus = ivrStatusCSVValues[0];
            contactedPhoneNumber = null;
            if (ivrStatusCSVValues.Length == 7)
            {
                sourceCallDuration = int.Parse(ivrStatusCSVValues[3]);
                contactedPhoneNumber = ivrStatusCSVValues[4];
                targetCallDuration = int.Parse(ivrStatusCSVValues[6]);
                if (targetCallDuration > 10000)
                {
                    targetCallDuration = int.Parse(ivrStatusCSVValues[5]);
                }
            }
        }

        private void CreateStatusEntity(VnStatusRequest container, DataAccessLayer.VnStatus vnStatusDal, NoProblem.Core.DataModel.Xrm.new_vnrequest requestEntity, int targetCallDuration, int sourceCallDuration)
        {
            DataModel.Xrm.new_vnstatus statusEntity = new NoProblem.Core.DataModel.Xrm.new_vnstatus();
            statusEntity.new_vnrequestid = requestEntity.new_vnrequestid;
            statusEntity.new_ivrsession = container.IvrSession;
            statusEntity.new_ivrstatus = container.IvrStatus;
            statusEntity.new_ivrdigit = container.Digit;
            statusEntity.new_advertisercallduration = targetCallDuration;
            statusEntity.new_customercallduration = sourceCallDuration;
            vnStatusDal.Create(statusEntity);
            XrmDataContext.SaveChanges();
        }

        private void GetRecording(string ivrSession, string sessionServer)
        {
            SetupRecordingRetry(ivrSession, sessionServer, 1);
        }

        private void SetupRecordingRetry(string ivrSession, string sessionServer, int tryCount)
        {
            RecordingsRetryContainer container = new RecordingsRetryContainer();
            container.AsteriskSessionId = ivrSession;
            container.SessionServer = sessionServer;
            container.TryCount = tryCount;
            container.Timer = new Timer(
                new TimerCallback(RetryRetrieveRecording),
                container,
                tryCount == 1 ? 90000 : 30000,
                Timeout.Infinite);
            recordingsRetriesHolder.Add(container);
        }

        private void RetryRetrieveRecording(object obj)
        {
            RecordingsRetryContainer container = (RecordingsRetryContainer)obj;
            try
            {
                recordingsRetriesHolder.Remove(container);
                container.Timer.Dispose();
                AsteriskBL.AsteriskCallsExecuter executer = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskCallsExecuter(null);
                bool alreadyExisted;
                string file = executer.RetrieveRecording(container.AsteriskSessionId, container.SessionServer, out alreadyExisted);
                if (!alreadyExisted)
                {
                    if (!string.IsNullOrEmpty(file))
                    {
                        Phones phones = new Phones(executer.XrmDataContext, null);
                        var ans = phones.UpdateRecordingFileLocation(container.AsteriskSessionId, file);
                        if (ans == "1,record id not found")
                        {
                            if (container.TryCount >= 2)
                            {
                                LogUtils.MyHandle.WriteToLog("Failed to save recording file path to incidentAccount after 2 tries. asteriskSessionId={0}", container.AsteriskSessionId);
                                return;
                            }
                            SetupRecordingRetry(container.AsteriskSessionId, container.SessionServer, ++container.TryCount);
                        }
                        else
                        {
                            executer.DeleteRecordingInAsteriskServer(container.AsteriskSessionId);
                        }
                    }
                    else if (container.TryCount < 2)
                    {
                        SetupRecordingRetry(container.AsteriskSessionId, container.SessionServer, ++container.TryCount);
                    }
                    else
                    {
                        LogUtils.MyHandle.WriteToLog("Failed to retrieve recording file after 2 tries. asteriskSessionId={0}", container.AsteriskSessionId);
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ViennaManager.RetryRetrieveRecording. asteriskSessionId={0}", container.AsteriskSessionId);
            }
        }

        #endregion
    }
}
