﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.IvrWidget;

namespace NoProblem.Core.BusinessLogic.TwilioBL
{
    public class TwilioHandler : TwilioHandlerBase
    {
        #region Ctor
        public TwilioHandler(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region IVR Widget Methods

        public string IvrWidgetConsultation(string callSid, string from, string to, string callStatus)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            string ttsText;
            bool isManVoice;
            Guid ivrWidgetCallId;
            IvrWidget.IvrWidgetManager manager = new NoProblem.Core.BusinessLogic.IvrWidget.IvrWidgetManager(XrmDataContext);
            ProcessStatus status = manager.IncomingCall(callSid, from, to, callStatus, out ttsText, out isManVoice, out ivrWidgetCallId);
            string voiceToUse = GetVoiceGender(isManVoice);
            if (status == ProcessStatus.GoBack)
            {
                AddIvrWidgetPhoneGetGather(response, ttsText, ivrWidgetCallId, voiceToUse, 1);
            }
            else if (status == ProcessStatus.Continue)
            {
                AddIvrWidgetPhoneConfirmGather(response, ttsText, ivrWidgetCallId, voiceToUse, 1);
            }
            else
            {
                AddHangUpVerbs(response, ttsText, voiceToUse);
            }
            return response.ToString();
        }

        public string IvrWidgetPhoneGet(string digits, string callSid, string ivrWidgetCallId, int tryCount)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            IvrWidget.IvrWidgetManager manager = new NoProblem.Core.BusinessLogic.IvrWidget.IvrWidgetManager(XrmDataContext);
            string ttsText;
            bool isManVoice;
            Guid ivrWidgetCallIdGuid = new Guid(ivrWidgetCallId);
            ProcessStatus status = manager.PhoneGet(ivrWidgetCallIdGuid, digits, out ttsText, out isManVoice, ref tryCount);
            string voiceToUse = GetVoiceGender(isManVoice);
            if (status == ProcessStatus.Continue)
            {
                AddIvrWidgetPhoneConfirmGather(response, ttsText, ivrWidgetCallIdGuid, voiceToUse, tryCount);
            }
            else if (status == ProcessStatus.Repeat)
            {
                AddIvrWidgetPhoneGetGather(response, ttsText, ivrWidgetCallIdGuid, voiceToUse, tryCount);
            }
            else
            {
                AddHangUpVerbs(response, ttsText, voiceToUse);
            }
            return response.ToString();
        }

        public string IvrWidgetPhoneConfirm(string digits, string callSid, string ivrWidgetCallId, int tryCount)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            IvrWidget.IvrWidgetManager manager = new NoProblem.Core.BusinessLogic.IvrWidget.IvrWidgetManager(XrmDataContext);
            string ttsText;
            bool isManVoice;
            Guid ivrWidgetCallIdGuid = new Guid(ivrWidgetCallId);
            ProcessStatus status = manager.PhoneConfirm(ivrWidgetCallIdGuid, digits, out ttsText, out isManVoice, ref tryCount);
            string voiceToUse = GetVoiceGender(isManVoice);
            if (status == ProcessStatus.Continue)
            {
                AddIvrWidgetAreaGetGather(response, ttsText, ivrWidgetCallIdGuid, voiceToUse, tryCount);
            }
            else if (status == ProcessStatus.Repeat)
            {
                AddIvrWidgetPhoneConfirmGather(response, ttsText, ivrWidgetCallIdGuid, voiceToUse, tryCount);
            }
            else if (status == ProcessStatus.GoBack)
            {
                AddIvrWidgetPhoneGetGather(response, ttsText, ivrWidgetCallIdGuid, voiceToUse, tryCount);
            }
            else
            {
                AddHangUpVerbs(response, ttsText, voiceToUse);
            }
            return response.ToString();
        }

        public string IvrWidgetAreaGet(string digits, string callSid, string ivrWidgetCallId, int tryCount)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            IvrWidget.IvrWidgetManager manager = new NoProblem.Core.BusinessLogic.IvrWidget.IvrWidgetManager(XrmDataContext);
            string ttsText;
            bool isManVoice;
            Guid ivrWidgetCallIdGuid = new Guid(ivrWidgetCallId);
            ProcessStatus status = manager.AreaGet(ivrWidgetCallIdGuid, digits, out ttsText, out isManVoice, ref tryCount);
            string voiceToUse = GetVoiceGender(isManVoice);
            if (status == ProcessStatus.Continue)
            {
                AddIvrWidgetAreaConfirmGather(response, ttsText, ivrWidgetCallIdGuid, voiceToUse, tryCount);
            }
            else if (status == ProcessStatus.Repeat)
            {
                AddIvrWidgetAreaGetGather(response, ttsText, ivrWidgetCallIdGuid, voiceToUse, tryCount);
            }
            else
            {
                AddHangUpVerbs(response, ttsText, voiceToUse);
            }
            return response.ToString();
        }

        public string IvrWidgetAreaConfirm(string digits, string callSid, string ivrWidgetCallId, int tryCount)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            IvrWidget.IvrWidgetManager manager = new NoProblem.Core.BusinessLogic.IvrWidget.IvrWidgetManager(XrmDataContext);
            string ttsText;
            bool isManVoice;
            Guid ivrWidgetCallIdGuid = new Guid(ivrWidgetCallId);
            ProcessStatus status = manager.AreaConfirm(ivrWidgetCallIdGuid, digits, out ttsText, out isManVoice, ref tryCount);
            string voiceToUse = GetVoiceGender(isManVoice);
            if (status == ProcessStatus.Continue || status == ProcessStatus.HangUp)
            {
                AddHangUpVerbs(response, ttsText, voiceToUse);
            }
            else if (status == ProcessStatus.Repeat)
            {
                AddIvrWidgetAreaConfirmGather(response, ttsText, ivrWidgetCallIdGuid, voiceToUse, tryCount);
            }
            else
            {
                AddIvrWidgetAreaGetGather(response, ttsText, ivrWidgetCallIdGuid, voiceToUse, tryCount);
            }
            return response.ToString();
        }

        public void IvrWidgetStatusCallback(string callDuration, string callSid, string callStatus)
        {
            System.Threading.Thread tr = new System.Threading.Thread(delegate()
            {
                try
                {
                    IvrWidget.IvrWidgetManager manager = new NoProblem.Core.BusinessLogic.IvrWidget.IvrWidgetManager(XrmDataContext);
                    manager.StatusCallback(callDuration, callSid, callStatus);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in TwilioHandler.IvrWidgetStatusCallback");
                }
            });
            tr.Start();
        }

        public string IvrWidgetFallBack(string callSid, string from, string to, string callStatus, string errorCode, string errorUrl)
        {
            IvrWidget.IvrWidgetManager manager = new NoProblem.Core.BusinessLogic.IvrWidget.IvrWidgetManager(XrmDataContext);
            manager.FallBack(callSid, errorCode, errorUrl);
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            response.Say("Sorry, we could not handle your request at this time. Please try again later.");
            response.Hangup();
            return response.ToString();
        }

        #endregion

        #region Private Methods

        private void AddHangUpVerbs(Twilio.TwiML.TwilioResponse response, string ttsText, string voiceToUse)
        {
            response.Say(ttsText, new { voice = voiceToUse });
            response.Hangup();
        }

        private void AddIvrWidgetAreaConfirmGather(Twilio.TwiML.TwilioResponse response, string ttsText, Guid ivrWidgetCallId, string voiceToUse, int tryCount)
        {
            response.BeginGather(new
            {
                action = twilioCallBackUrlBase + "/IvrWidgetDataMethods?IvrWidgetCallId=" + ivrWidgetCallId.ToString() + "&SiteId=" + siteId + "&Method=IvrWidgetAreaConfirm&tryCount=" + tryCount,
                timeout = 10,
                method = "GET",
                numDigits = 1,
                finishOnKey = string.Empty
            });
            response.Say(ttsText, new { voice = voiceToUse });
            response.EndGather();
            response.Redirect(twilioCallBackUrlBase + "/IvrWidgetDataMethods?IvrWidgetCallId=" + ivrWidgetCallId.ToString() + "&SiteId=" + siteId + "&Digits=NOTHING&Method=IvrWidgetAreaConfirm&tryCount=" + tryCount, "GET");
        }

        private void AddIvrWidgetAreaGetGather(Twilio.TwiML.TwilioResponse response, string ttsText, Guid ivrWidgetCallId, string voiceToUse, int tryCount)
        {
            response.BeginGather(new
            {
                action = twilioCallBackUrlBase + "/IvrWidgetDataMethods?IvrWidgetCallId=" + ivrWidgetCallId.ToString() + "&SiteId=" + siteId + "&Method=IvrWidgetAreaGet&tryCount=" + tryCount,
                timeout = 10,
                method = "GET",
                numDigits = 5,
                finishOnKey = "#"
            });
            response.Say(ttsText, new { voice = voiceToUse });
            response.EndGather();
            response.Redirect(twilioCallBackUrlBase + "/IvrWidgetDataMethods?IvrWidgetCallId=" + ivrWidgetCallId.ToString() + "&SiteId=" + siteId + "&Digits=NOTHING&Method=IvrWidgetAreaGet&tryCount=" + tryCount, "GET");
        }

        private void AddIvrWidgetPhoneConfirmGather(Twilio.TwiML.TwilioResponse response, string ttsText, Guid ivrWidgetCallId, string voiceToUse, int tryCount)
        {
            response.BeginGather(new
            {
                action = twilioCallBackUrlBase + "/IvrWidgetDataMethods?IvrWidgetCallId=" + ivrWidgetCallId.ToString() + "&SiteId=" + siteId + "&Method=IvrWidgetPhoneConfirm&tryCount=" + tryCount,
                timeout = 10,
                method = "GET",
                numDigits = 1,
                finishOnKey = string.Empty
            });
            response.Say(ttsText, new { voice = voiceToUse });
            response.EndGather();
            response.Redirect(twilioCallBackUrlBase + "/IvrWidgetDataMethods?IvrWidgetCallId=" + ivrWidgetCallId.ToString() + "&SiteId=" + siteId + "&Digits=NOTHING&Method=IvrWidgetPhoneConfirm&tryCount=" + tryCount, "GET");
        }

        private void AddIvrWidgetPhoneGetGather(Twilio.TwiML.TwilioResponse response, string ttsText, Guid ivrWidgetCallId, string voiceToUse, int tryCount)
        {
            response.BeginGather(new
            {
                action = twilioCallBackUrlBase + "/IvrWidgetDataMethods?IvrWidgetCallId=" + ivrWidgetCallId.ToString() + "&SiteId=" + siteId + "&Method=IvrWidgetPhoneGet&tryCount=" + tryCount,
                timeout = 10,
                method = "GET",
                numDigits = 10,
                finishOnKey = "#"
            });
            response.Say(ttsText, new { voice = voiceToUse });
            response.EndGather();
            response.Redirect(twilioCallBackUrlBase + "/IvrWidgetDataMethods?IvrWidgetCallId=" + ivrWidgetCallId.ToString() + "&SiteId=" + siteId + "&Digits=NOTHING&Method=IvrWidgetPhoneGet&tryCount=" + tryCount, "GET");
        }

        #endregion
    }
}
