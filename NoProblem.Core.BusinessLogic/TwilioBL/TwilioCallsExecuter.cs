﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.TwilioBL
{
    internal class TwilioCallsExecuter : XrmUserBase
    {
        #region Fields

        private Twilio.TwilioRestClient restClient;
        DataAccessLayer.ConfigurationSettings configDal;

        private string _countryPrefix;
        private string CountryPrefix
        {
            get
            {
                if (string.IsNullOrEmpty(_countryPrefix))
                {
                    _countryPrefix = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.COUNTRY_PHONE_PREFIX);
                }
                return _countryPrefix;
            }
        }

        private string _internalPrefix;
        private string InternalPrefix
        {
            get
            {
                if (string.IsNullOrEmpty(_internalPrefix))
                {
                    _internalPrefix = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INTERNAL_PHONE_PREFIX);
                }
                return _internalPrefix;
            }
        }

        private bool? _checkMachineInAllAarCalls;
        private bool CheckMachineInAllAarCalls
        {
            get
            {
                if (!_checkMachineInAllAarCalls.HasValue)
                {
                    _checkMachineInAllAarCalls = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.CHECK_MACHINE_IN_ALL_AAR_CALLS) == "1";
                }
                return _checkMachineInAllAarCalls.Value;
            }
        }

        private string _machineCheckFromPhone;
        private string MachineCheckFromPhone
        {
            get
            {
                if (string.IsNullOrEmpty(_machineCheckFromPhone))
                {
                    _machineCheckFromPhone = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.MACHINE_CHECK_FROM_PHONE);
                }
                return _machineCheckFromPhone;
            }
        }

        private string _outboundCallFromPhone;
        private string OutboundCallFromPhone
        {
            get
            {
                if (string.IsNullOrEmpty(_outboundCallFromPhone))
                {
                    _outboundCallFromPhone = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.OUTBOUND_CALL_FROM_PHONE);
                }
                return _outboundCallFromPhone;
            }
        }

        private string _testCallCallerId;
        private string TestCallCallerId
        {
            get
            {
                if (string.IsNullOrEmpty(_testCallCallerId))
                {
                    _testCallCallerId = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.TEST_CALL_CALLER_ID_TWILIO_DID);
                }
                return _testCallCallerId;
            }
        }

        private string _outboundCallFromPhone_RegularNumber;
        private string OutboundCallFromPhone_RegularNumber
        {
            get
            {
                if (string.IsNullOrEmpty(_outboundCallFromPhone_RegularNumber))
                {
                    _outboundCallFromPhone_RegularNumber = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.OUTBOUND_CALL_FROM_PHONE_REGULAR_NUMBER);
                }
                return _outboundCallFromPhone_RegularNumber;
            }
        }

        private static string twilioCallBackUrlBase = System.Configuration.ConfigurationManager.AppSettings["TwilioCallBackUrlBase"];
        private static string siteId = System.Configuration.ConfigurationManager.AppSettings["SiteId"];
        private static string accountSid = System.Configuration.ConfigurationManager.AppSettings["TwilioAccountSid"];
        private static string authToken = System.Configuration.ConfigurationManager.AppSettings["TwilioAuthToken"];

        #endregion

        #region Ctor

        internal TwilioCallsExecuter(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            restClient = new Twilio.TwilioRestClient(accountSid, authToken);
            configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
        }

        #endregion

        #region Internal Methods

        internal string InitiateAutoUpsaleCallBackCall(string toPhone, Guid id, string headingIvrName)
        {
            toPhone = Phones.Deprecated_AppendPhonePrefix(CountryPrefix, InternalPrefix, toPhone);
            Twilio.CallOptions callOptions = new Twilio.CallOptions();
            callOptions.To = toPhone;
            callOptions.From = OutboundCallFromPhone;
            callOptions.FallbackUrl = twilioCallBackUrlBase + "/AutoUpsaleCallBackFallback?Id=" + id + "&SiteId=" + siteId;
            callOptions.FallbackMethod = "GET";
            callOptions.StatusCallback = twilioCallBackUrlBase + "/AutoUpsaleCallBackStatusCallback?Id=" + id + "&SiteId=" + siteId;
            callOptions.StatusCallbackMethod = "GET";
            callOptions.Url = twilioCallBackUrlBase + "/AutoUpsaleCallBackConsultation?Id=" + id + "&SiteId=" + siteId + "&HeadingIvrName=" + System.Web.HttpUtility.UrlEncode(headingIvrName);
            callOptions.Method = "GET";
            Twilio.Call call = restClient.InitiateOutboundCall(callOptions);
            string callSid = null;
            if (call != null)
            {
                callSid = call.Sid;
            }
            return callSid;
        }

        internal string InitiateAarAdvertiserCall(string toPhone, Guid incidentAccountId)
        {
            toPhone = Phones.Deprecated_AppendPhonePrefix(CountryPrefix, InternalPrefix, toPhone);
            Twilio.CallOptions callOptions = new Twilio.CallOptions();
            callOptions.To = toPhone;
            callOptions.From = OutboundCallFromPhone;
            callOptions.FallbackUrl = twilioCallBackUrlBase + "/AarFallBackAdvertiser?IncidentAccountId=" + incidentAccountId + "&SiteId=" + siteId;
            callOptions.FallbackMethod = "GET";
            callOptions.StatusCallback = twilioCallBackUrlBase + "/AarStatusCallBackAdvertiser?IncidentAccountId=" + incidentAccountId + "&SiteId=" + siteId;
            callOptions.StatusCallbackMethod = "GET";
            callOptions.Url = twilioCallBackUrlBase + "/AarAdvertiserConsultation?IncidentAccountId=" + incidentAccountId + "&SiteId=" + siteId;
            callOptions.Method = "GET";
            if (CheckMachineInAllAarCalls)
            {
                callOptions.IfMachine = "Continue";
            }
            Twilio.Call call = restClient.InitiateOutboundCall(callOptions);
            string callSid = null;
            if (call != null)
            {
                callSid = call.Sid;
            }
            return callSid;
        }

        internal string InitiateAarConsumerCall(string toPhone, Guid incidentAccountId, string advertiserCallSid, out bool isAdvertiserCallInactive)
        {
            isAdvertiserCallInactive = false;
            toPhone = Phones.Deprecated_AppendPhonePrefix(CountryPrefix, InternalPrefix, toPhone);

            var callOptions = new Twilio.CallOptions();
            callOptions.From = OutboundCallFromPhone;
            callOptions.To = toPhone;
            callOptions.Method = "GET";
            callOptions.Url = twilioCallBackUrlBase + "/AarConsumerConsultation?IncidentAccountId=" + incidentAccountId + "&SiteId=" + siteId;
            callOptions.FallbackMethod = "GET";
            callOptions.FallbackUrl = twilioCallBackUrlBase + "/AarFallBackConsumer?IncidentAccountId=" + incidentAccountId + "&SiteId=" + siteId;
            callOptions.StatusCallbackMethod = "GET";
            callOptions.StatusCallback = twilioCallBackUrlBase + "/AarStatusCallBackConsumer?IncidentAccountId=" + incidentAccountId + "&SiteId=" + siteId;

            //var confs = restClient.ListConferences(new Twilio.ConferenceListRequest()
            //{
            //    FriendlyName = incidentAccountId.ToString()
            //});
            //check if adv call is still valid
            bool ok = false;
            if (!string.IsNullOrEmpty(advertiserCallSid))
            {
                var ca = restClient.GetCall(advertiserCallSid);
                if (ca != null && ca.Status == "in-progress")
                {
                    ok = true;
                }
            }
            string callSid = null;
            if (ok)
            {
                var call = restClient.InitiateOutboundCall(callOptions);
                if (call != null)
                {
                    callSid = call.Sid;
                }
            }
            else
            {
                isAdvertiserCallInactive = true;
            }
            return callSid;
        }

        internal string InitiateMachineCheckCall(string toPhone, Guid supplierId)
        {
            toPhone = Phones.Deprecated_AppendPhonePrefix(CountryPrefix, InternalPrefix, toPhone);
            Twilio.CallOptions callOptions = new Twilio.CallOptions();
            callOptions.To = toPhone;
            callOptions.From = MachineCheckFromPhone;
            callOptions.FallbackUrl = twilioCallBackUrlBase + "/MachineCheckFallback?SupplierId=" + supplierId + "&SiteId=" + siteId;
            callOptions.FallbackMethod = "GET";
            callOptions.StatusCallback = twilioCallBackUrlBase + "/MachineCheckStatusCallback?SupplierId=" + supplierId + "&SiteId=" + siteId;
            callOptions.StatusCallbackMethod = "GET";
            callOptions.Url = twilioCallBackUrlBase + "/MachineCheckConsultation?SupplierId=" + supplierId + "&SiteId=" + siteId;
            callOptions.Method = "GET";
            callOptions.IfMachine = "Continue";
            callOptions.Timeout = 15;
            Twilio.Call call = restClient.InitiateOutboundCall(callOptions);
            string callSid = null;
            if (call != null)
            {
                callSid = call.Sid;
            }
            return callSid;
        }

        internal void HangUpCall(string callSid)
        {
            try
            {
                if (callSid != null)
                    restClient.HangupCall(callSid, Twilio.HangupStyle.Completed);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in HangUpCall");
            }
        }

        internal void RedirectCall(string callSid, string method)
        {
            try
            {
                if (!string.IsNullOrEmpty(callSid))
                    restClient.RedirectCall(
                        callSid,
                        twilioCallBackUrlBase + "/" + method + "?SiteId=" + siteId,
                        "GET");
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RedirectCall");
            }
        }

        internal Twilio.Call InitiateTestCall(string phone, Guid testCallId)
        {
            phone = Phones.Deprecated_AppendPhonePrefix(CountryPrefix, InternalPrefix, phone);
            Twilio.CallOptions callOptions = new Twilio.CallOptions();
            callOptions.To = phone;
            callOptions.From = TestCallCallerId;
            callOptions.StatusCallback = twilioCallBackUrlBase + "/TestCallCallback?TestCallId=" + testCallId + "&SiteId=" + siteId;
            callOptions.StatusCallbackMethod = "GET";
            callOptions.Url = twilioCallBackUrlBase + "/TestCallAnswered?TestCallId=" + testCallId + "&SiteId=" + siteId;
            callOptions.Method = "GET";
            Twilio.Call call = restClient.InitiateOutboundCall(callOptions);
            return call;
        }

        internal bool InitatePasswordCall(string phone, Guid supplierId)
        {
            phone = Phones.Deprecated_AppendPhonePrefix(CountryPrefix, InternalPrefix, phone);
            Twilio.CallOptions callOptions = new Twilio.CallOptions();
            callOptions.To = phone;
            callOptions.From = OutboundCallFromPhone;
            callOptions.Url = twilioCallBackUrlBase + "/PasswordCallAnswered?id=" + supplierId + "&SiteId=" + siteId;
            callOptions.Method = "GET";
            Twilio.Call call = restClient.InitiateOutboundCall(callOptions);
            return (call != null && call.Status != "failed");
        }

        internal string InitiateCall(string phone, string customIdName, string customId, string answeredMethod, string callbackMethod, string fallbackMethod, bool checkMachine, bool useTollFreeNumberToDial, string callerIdToUse, out string callStatus)
        {
            callStatus = null;
            phone = Phones.Deprecated_AppendPhonePrefix(CountryPrefix, InternalPrefix, phone);
            Twilio.CallOptions callOptions = new Twilio.CallOptions();
            callOptions.To = phone;
            if (!string.IsNullOrEmpty(callerIdToUse))
            {
                callerIdToUse = Phones.Deprecated_AppendPhonePrefix(CountryPrefix, InternalPrefix, callerIdToUse);
                callOptions.From = callerIdToUse;
            }
            else if (useTollFreeNumberToDial)
            {
                callOptions.From = OutboundCallFromPhone;
            }
            else
            {
                callOptions.From = OutboundCallFromPhone_RegularNumber;
            }
            callOptions.Url = twilioCallBackUrlBase + "/" + answeredMethod + "?" + customIdName + "=" + customId + "&SiteId=" + siteId;
            callOptions.Method = "GET";
            if (!string.IsNullOrEmpty(callbackMethod))
            {
                callOptions.StatusCallback = twilioCallBackUrlBase + "/" + callbackMethod + "?" + customIdName + "=" + customId + "&SiteId=" + siteId;
                callOptions.StatusCallbackMethod = "GET";
            }
            if (!string.IsNullOrEmpty(fallbackMethod))
            {
                callOptions.FallbackUrl = twilioCallBackUrlBase + "/" + fallbackMethod + "?" + customIdName + "=" + customId + "&SiteId=" + siteId;
                callOptions.FallbackMethod = "GET";
            }
            if (checkMachine)
            {
                callOptions.IfMachine = "Continue";
            }
            Twilio.Call call = restClient.InitiateOutboundCall(callOptions);
            string retVal = null;
            if (call != null)
            {
                callStatus = call.Status;
                retVal = call.Sid;
            }
            return retVal;
        }

        #endregion
        

        internal bool IsConferenceActive(string confName)
        {
            var confs = restClient.ListConferences(new Twilio.ConferenceListRequest()
            {
                FriendlyName = confName
            });
            bool retVal = false;
            if (confs != null && confs.Conferences != null && confs.Conferences.Count > 0)
            {
                 retVal = confs.Conferences.First().Status != "completed";
            }
            return retVal;
        }
    }
}
