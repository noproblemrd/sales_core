﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
//using RestSharp.Contrib;
using NoProblem.Core.DataModel;
using RestSharp.Extensions.MonoHttp;

namespace NoProblem.Core.BusinessLogic.TwilioBL.IncomingCalls
{
    public class IncomingCallsHandler : TwilioHandlerBase
    {
        private const string _ANTIFRAUDBLOCKED = "AntiFraudBlocked";
        private const string _SOURCE = "Twilio";
        private const string _BLACKLIST = "Blacklist";
        private const string _LOG_MESSAGE_FOR_VN_NOT_FOUND = "Vienna in Twilio: VnRequest not found in DB. IvrSession: {0}. IvrStatus: {1}";

        public IncomingCallsHandler()
            : base(null)
        {

        }

        public string Consultation(string callSid, string from, string to, string callStatus)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();

            Dialer.IncomingCallsAntiFraud fraudAnalist = new NoProblem.Core.BusinessLogic.Dialer.IncomingCallsAntiFraud(XrmDataContext);
            bool passed = fraudAnalist.CheckAntiFraud(from);
            if (!passed)
            {
                SaveVnRequest(callSid, to, from, _SOURCE, false, true, _ANTIFRAUDBLOCKED, null, null, null, false, Guid.Empty);
                response.Hangup();
                return response.ToString();
            }

            Phones phones = new Phones(XrmDataContext, null);
            var dnQueryResponse = phones.GetPhoneNumber(callSid, to, RemovePlus(from));

            if (dnQueryResponse.IsBlacklisted)
            {
                //handle black listed customer in incoming calls.
                SaveVnRequest(callSid, to, from, _SOURCE, false, false, _BLACKLIST, dnQueryResponse.CallType, dnQueryResponse.TruePhoneNumber.FirstOrDefault(), null, dnQueryResponse.IsRecording, dnQueryResponse.ProviderId);
                response.Hangup();
            }
            else if (dnQueryResponse.IsAvailable)
            {
                if (!string.IsNullOrEmpty(dnQueryResponse.IvrCallerAnnounceAdvertiserSpecific))
                {
                    DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                    string audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
                    response.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.INCOMING_CALLS + AudioFilesConsts.SLASH + dnQueryResponse.IvrCallerAnnounceAdvertiserSpecific);
                }
                response.Dial(dnQueryResponse.TruePhoneNumber.First(),
                        new
                        {
                            record = "true",
                            action = twilioCallBackUrlBase + "/ViennaDialStatusCallback?SiteId=" + siteId + "&IsUnmapped=False&IncomingCallSid=" + callSid + "&CustomerPhone=" + HttpUtility.UrlEncode(from) + "&VirtualNumber=" + HttpUtility.UrlEncode(to) + "&AdvertiserPhone=" + dnQueryResponse.TruePhoneNumber.First(),
                            method = "GET",
                            timeout = 30
                        });
                //create vnrequest (maybe async).
                SaveVnRequest(callSid, to, from, _SOURCE, true, false, dnQueryResponse.MappingStatus, dnQueryResponse.CallType, dnQueryResponse.TruePhoneNumber.FirstOrDefault(), null, dnQueryResponse.IsRecording, dnQueryResponse.ProviderId);
            }
            else
            {
                //handle no mapping in incoming calls.
                SaveVnRequest(callSid, to, from, _SOURCE, false, false, dnQueryResponse.MappingStatus, dnQueryResponse.CallType, dnQueryResponse.TruePhoneNumber.FirstOrDefault(), null, dnQueryResponse.IsRecording, dnQueryResponse.ProviderId);
                response.Dial(dnQueryResponse.TruePhoneNumber.First(),
                        new
                        {
                            record = "true",
                            action = twilioCallBackUrlBase + "/ViennaDialStatusCallback?SiteId=" + siteId + "&IsUnmapped=True&IncomingCallSid=" + callSid + "&CustomerPhone=" + HttpUtility.UrlEncode(from) + "&VirtualNumber=" + HttpUtility.UrlEncode(to) + "&AdvertiserPhone=" + dnQueryResponse.TruePhoneNumber.First(),
                            method = "GET",
                            timeout = 30,
                            timeLimit = 600
                        });
            }
            return response.ToString();
        }

        public void DialStatusCallBack(
            string dialCallStatus, string dialCallSid, string dialCallDuration, string recordingUrl, bool isUnmapped,
            string incomingCallSid, string customerPhone, string virtualNumber, string advertiserPhone)
        {
            //insert incomingCallSid to synchronization table in db
            DataAccessLayer.TwilioViennaCallBacksSync syncDal = new NoProblem.Core.DataAccessLayer.TwilioViennaCallBacksSync();
            syncDal.Insert(incomingCallSid);
            //LogUtils.MyHandle.WriteToLog("callSid " + incomingCallSid + " inserted");
            int duration;
            int.TryParse(dialCallDuration, out duration);
            CreateVnStatus(incomingCallSid, dialCallStatus, duration);
            if (!isUnmapped)//is not call to moked from unmapped // is normal call to mapped advertiser
            {
                //do reporting according to hang up causes.
                int reasonCode;
                switch (dialCallStatus)
                {
                    case "completed"://good
                        reasonCode = 0;
                        break;
                    default://bad
                        reasonCode = 1;
                        break;
                }

                Phones phones = new Phones(XrmDataContext, null);
                phones.DirectNumberContactedAsync(
                    RemovePlus(customerPhone),
                    virtualNumber,
                    reasonCode,
                    incomingCallSid,
                    duration,
                    incomingCallSid,
                    recordingUrl,
                    false, null, null, null, null);
            }
        }

        public void IncomingCallStatusCallBack(string callDuration, string callSid, string callStatus, string from, string to)
        {
            IncomingCallStatusCallBackHandlerThread handler = new IncomingCallStatusCallBackHandlerThread(callSid, from, to);
            handler.Start();
        }

        internal static string RemovePlus(string phone)
        {
            if (phone.StartsWith("+1"))
            {
                phone = phone.Substring(2);
            }
            else if (phone.StartsWith("+"))
            {
                phone = phone.Substring(1);
            }
            return phone;
        }

        private void CreateVnStatus(string callId, string hangupCause, int duration)
        {
            DataAccessLayer.VnRequest vnRequestDal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
            var requestEntity = vnRequestDal.GetVnRequestByIvrSession(callId);
            if (requestEntity == null)
            {
                LogUtils.MyHandle.WriteToLog(string.Format(_LOG_MESSAGE_FOR_VN_NOT_FOUND, callId, hangupCause));
            }
            else
            {
                DataAccessLayer.VnStatus vnStatusDal = new NoProblem.Core.DataAccessLayer.VnStatus(XrmDataContext);
                DataModel.Xrm.new_vnstatus statusEntity = new NoProblem.Core.DataModel.Xrm.new_vnstatus();
                statusEntity.new_vnrequestid = requestEntity.new_vnrequestid;
                statusEntity.new_ivrsession = callId;
                statusEntity.new_ivrstatus = hangupCause;
                statusEntity.new_advertisercallduration = duration;
                vnStatusDal.Create(statusEntity);
                XrmDataContext.SaveChanges();
            }
        }

        private void SaveVnRequest(
            string callId,
            string didNumber,
            string customerCallerId,
            string sourceUrl,
            bool doReport,
            bool antiFraudBlocked,
            string mappingStatus,
            string callType,
            string target,
            string outCallId,
            bool recordCall,
            Guid supplierId)
        {
            DataAccessLayer.VnRequest dal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
            DataModel.Xrm.new_vnrequest entity = new NoProblem.Core.DataModel.Xrm.new_vnrequest();
            entity.new_ivrsession = callId;
            entity.new_ivrinbound = didNumber;
            entity.new_ivrcallerid = customerCallerId;
            entity.new_ivrsourceurl = sourceUrl;
            entity.new_doreport = doReport;
            entity.new_antifraudblocked = antiFraudBlocked;
            entity.new_target1 = target;
            entity.new_mappingstatus = mappingStatus;
            entity.new_calltype = callType;
            entity.new_outcallid = outCallId;
            entity.new_recordcall = recordCall;
            if (supplierId != Guid.Empty)
            {
                entity.new_accountid = supplierId;
            }
            dal.Create(entity);
            XrmDataContext.SaveChanges();
        }

    }
}
