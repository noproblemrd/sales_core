﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.TwilioBL.IncomingCalls
{
    class IncomingCallStatusCallBackHandlerThread : Runnable
    {
        public IncomingCallStatusCallBackHandlerThread(string callSid, string from, string to)
        {
            this.callSid = callSid;
            this.from = from;
            this.to = to;
        }

        private string callSid;
        private string from;
        private string to;
        private static DataAccessLayer.TwilioViennaCallBacksSync dal = new NoProblem.Core.DataAccessLayer.TwilioViennaCallBacksSync();

        protected override void Run()
        {
            try
            {
                bool found = FindAndDeleteIncomingCallSidInDB(callSid);
                //if id was not found in DB, then we asses that the dial call was never executed and we need to save the request through here (in the incoming call).
                if (!found)
                {
                    Phones phones = new Phones(XrmDataContext, null);
                    phones.DirectNumberContactedAsync(
                        IncomingCallsHandler.RemovePlus(from),
                        to,
                        1,//bad (the dial call was not even started).
                        callSid,
                        0,
                        callSid,
                        null,
                        false, null, null, null, null);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in NoProblem.Core.BusinessLogic.TwilioBL.IncomingCalls.IncomingCallStatusCallBackHandlerThread.Run");
            }
        }

        /// <summary>
        /// Looks for the id in the DB three times with few seconds interval.
        /// </summary>
        /// <param name="callSid"></param>
        /// <returns></returns>
        private bool FindAndDeleteIncomingCallSidInDB(string callSid)
        {
            bool retVal = false;
            for (int i = 0; i < 3; i++)
            {
                bool found = dal.FindAndDelete(callSid);
                if (found)
                {
                    retVal = true;
                    break;
                }
                Thread.Sleep(2500);
            }
            return retVal;
        }
    }
}
