﻿using System;
using System.Web;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.ServiceRequest.CallBack;

namespace NoProblem.Core.BusinessLogic.TwilioBL
{
    public class TwilioAutoUpsaleCallBackHandler : TwilioHandlerBase
    {
        public TwilioAutoUpsaleCallBackHandler(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public string MainConsultation(string callSid, string accountSid, string from, string to, string callStatus, string apiVersion, string direction, string id, string headingIvrName)
        {
            ServiceRequest.CallBack.AutoUpsaleCallBackManager callBackManager = new NoProblem.Core.BusinessLogic.ServiceRequest.CallBack.AutoUpsaleCallBackManager(XrmDataContext);
            string txt = callBackManager.HandleMainConsultation(id, callSid, callStatus, headingIvrName);
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            AddMainClickGatherToResponse(response, id, txt, headingIvrName);
            return response.ToString();
        }

        public string MainClickHandler(string digits, string callSid, string id, string headingIvrName)
        {
            AutoUpsaleCallBackManager callBackManager = new AutoUpsaleCallBackManager(XrmDataContext);
            string txt;
            bool isConfirmed = callBackManager.HandleMainMenuClicks(digits, id, headingIvrName, out txt);
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            //switch (toDo)
            //{
            //    case AutoUpsaleCallBackManager.END_CALL:
            if (isConfirmed)
            {
                //response.Say(txt, new { voice = "woman" });
                DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
                response.Play(audioFilesUrlBase + "/autoupsalecallback/callback3.mp3");
            }
            response.Hangup();
            //        break;
            //    case AutoUpsaleCallBackManager.REPEAT:
            //        AddMainClickGatherToResponse(response, id, txt, headingIvrName);
            //        break;
            //    case AutoUpsaleCallBackManager.ARE_YOU_SURE:
            //        response.BeginGather(new
            //        {
            //            action = twilioCallBackUrlBase + "/AutoUpsaleCallBackSecondaryClickHandler?Id=" + id + "&SiteId=" + siteId + "&HeadingIvrName=" + HttpUtility.UrlEncode(headingIvrName),
            //            timeout = 8,
            //            method = "GET",
            //            numDigits = 1,
            //            finishOnKey = string.Empty
            //        });
            //        response.Say(txt, new { voice = "woman" });
            //        response.EndGather();
            //        response.Redirect(twilioCallBackUrlBase + "/AutoUpsaleCallBackSecondaryClickHandler?Id=" + id + "&SiteId=" + siteId + "&Digits=NOTHING&HeadingIvrName=" + HttpUtility.UrlEncode(headingIvrName), "GET");
            //        break;
            //}
            return response.ToString();
        }

        public string SecodaryClickHandler(string digits, string callSid, string id, string headingIvrName)
        {
            ServiceRequest.CallBack.AutoUpsaleCallBackManager callBackManager = new NoProblem.Core.BusinessLogic.ServiceRequest.CallBack.AutoUpsaleCallBackManager(XrmDataContext);
            string txt;
            string toDo = callBackManager.HandleSecondaryMenuClicks(digits, id, headingIvrName, out txt);
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            switch (toDo)
            {
                case AutoUpsaleCallBackManager.REPEAT:
                    AddMainClickGatherToResponse(response, id, txt, headingIvrName);
                    break;
                case AutoUpsaleCallBackManager.END_CALL:
                    response.Say(txt, new { voice = "woman" });
                    response.Hangup();
                    break;
            }
            return response.ToString();
        }

        private void AddMainClickGatherToResponse(Twilio.TwiML.TwilioResponse response, string id, string txt, string headingIvrName)
        {
            response.BeginGather(new
            {
                action = twilioCallBackUrlBase + "/AutoUpsaleCallBackMainClickHandler?Id=" + id + "&SiteId=" + siteId + "&HeadingIvrName=" + HttpUtility.UrlEncode(headingIvrName),
                timeout = 8,
                method = "GET",
                numDigits = 1,
                finishOnKey = string.Empty
            });
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
            response.Play(audioFilesUrlBase + "/autoupsalecallback/callback1.mp3");
            response.Play(audioFilesUrlBase + "/sharon/headings/" + headingIvrName + DataModel.AudioFilesConsts.MP3_FILE_EXTENSION);
            response.Play(audioFilesUrlBase + "/autoupsalecallback/callback2.mp3");
            //response.Say(txt, new { voice = "woman" });
            response.EndGather();
            response.Redirect(twilioCallBackUrlBase + "/AutoUpsaleCallBackMainClickHandler?Id=" + id + "&SiteId=" + siteId + "&Digits=NOTHING&HeadingIvrName=" + HttpUtility.UrlEncode(headingIvrName), "GET");
        }

        public string Fallback(string callSid, string accountSid, string from, string to, string callStatus, string apiVersion, string direction, string id, string errorCode, string errorUrl)
        {
            ServiceRequest.CallBack.AutoUpsaleCallBackManager callBackManager = new NoProblem.Core.BusinessLogic.ServiceRequest.CallBack.AutoUpsaleCallBackManager(XrmDataContext);
            callBackManager.FallBack(callSid, callStatus, id, errorCode, errorUrl);
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            response.Hangup();
            return response.ToString();
        }

        public void StatusCallBack(string callDuration, string callSid, string accountSid, string from, string to, string callStatus, string apiVersion, string direction, string id)
        {
            StatusCallBackDelegate del = new StatusCallBackDelegate(StatusCallBackAsync);
            del.BeginInvoke(callDuration, callSid, from, to, callStatus, id, StatusCallBackDelegateCallBack, del);
        }

        private delegate void StatusCallBackDelegate(string callDuration, string callSid, string from, string to, string callStatus, string id);
        private void StatusCallBackAsync(string callDuration, string callSid, string from, string to, string callStatus, string id)
        {
            ServiceRequest.CallBack.AutoUpsaleCallBackManager callBackManager = new NoProblem.Core.BusinessLogic.ServiceRequest.CallBack.AutoUpsaleCallBackManager(XrmDataContext);
            callBackManager.StatusCallBack(callDuration, callSid, from, to, callStatus, id);
        }

        private void StatusCallBackDelegateCallBack(IAsyncResult result)
        {
            try
            {
                ((StatusCallBackDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in StatusCallBackDelegateCallBack");
            }
        }
    }
}
