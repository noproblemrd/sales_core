﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.TwilioBL
{
    public abstract class TwilioHandlerBase : XrmUserBase
    {
        #region Fields
        protected static string twilioCallBackUrlBase = System.Configuration.ConfigurationManager.AppSettings["TwilioCallBackUrlBase"];
        protected static string siteId = System.Configuration.ConfigurationManager.AppSettings["SiteId"];
        #endregion

        #region Ctor
        public TwilioHandlerBase(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        protected string GetVoiceGender(bool isManVoice)
        {
            return isManVoice ? "man" : "woman";
        }
    }
}
