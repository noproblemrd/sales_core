﻿using System;
using System.Collections.Generic;
using System.Linq;
using Effect.Crm.Logs;
using DML = NoProblem.Core.DataModel;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.Campaigns
{
    public class CampaignsManager : XrmUserBase
    {
        #region Ctor
        public CampaignsManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        public void InitiateCampaignsDailyActivities()
        {
            Thread tr = new Thread(
                delegate()
                {
                    try
                    {
                        InitiateCampaignsDailyActivitiesPrivate();
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception in async method CampaignsManager.InitiateCampaignsDailyActivitiesPrivate");
                    }
                });
            tr.Start();
        }

        private void InitiateCampaignsDailyActivitiesPrivate()
        {
            //Find campaigns that are active and that their time to send has passed
            DateTime localTime = FixDateToLocal(DateTime.Now);
            DataAccessLayer.Campaign campaignDal = new NoProblem.Core.DataAccessLayer.Campaign(XrmDataContext);
            IEnumerable<Guid> campaigns = campaignDal.GetActiveAfterTimeCampaigns(localTime);
            if (campaigns.Count() == 0)
            {
                return;
            }
            DataAccessLayer.CampaignActivity activityDal = new NoProblem.Core.DataAccessLayer.CampaignActivity(XrmDataContext);
            //Get acitvities from campaigns:
            IEnumerable<DML.Xrm.new_campaignactivity> activities = activityDal.GetAllActivitiesFromCampaigns(campaigns);
            //create an event for every activity:
            DataAccessLayer.CampaignEvent eventDal = new NoProblem.Core.DataAccessLayer.CampaignEvent(XrmDataContext);
            DateTime todayFrom = localTime.Date;
            DateTime todayTo = todayFrom;
            FixDatesToUtcFullDays(ref todayFrom, ref todayTo);
            foreach (var act in activities)
            {
                CreateEvent(act, eventDal, todayFrom, todayTo);
            }
            XrmDataContext.SaveChanges();
            //Get all open events:
            IEnumerable<DML.Xrm.new_campaignevent> openEvents = eventDal.GetAllOpenEvents(activities);
            //Check is sending time (business closures and weekly sending times):
            BusinessClosures.ClosuresManager closures = new BusinessClosures.ClosuresManager(XrmDataContext);
            bool isClosureTime = closures.IsNowClosure(BusinessClosures.ClosuresManager.ClosureOptions.WeeklyAndClosureDates);
            if (isClosureTime)
            {
                //all open for current activities, events counter++:
                foreach (var eve in openEvents)
                {
                    eve.new_counter++;
                    eventDal.Update(eve);
                }
                XrmDataContext.SaveChanges();
            }
            else
            {
                //filter events:
                IEnumerable<DML.Xrm.new_campaignevent> filteredEvents = FilterEvents(openEvents);
                //send filtered events:
                SendEvents(filteredEvents);
            }
        }

        #endregion

        #region Private Methods

        private void SendEvents(IEnumerable<DML.Xrm.new_campaignevent> events)
        {
            DataAccessLayer.CampaignEvent dal = new NoProblem.Core.DataAccessLayer.CampaignEvent(XrmDataContext);
            Notifications notifier = new Notifications(XrmDataContext);
            foreach (var eve in events)
            {
                try
                {
                    //Get population:
                    IEnumerable<Guid> population = GetPopulation(eve);
                    //Get sending data from activity:
                    var activity = eve.new_campaignactivity_campaignevent;
                    bool useSms = activity.new_usesms.Value;
                    bool useEmail = activity.new_useemail.Value;
                    string smsTemplate = activity.new_smstemplate;
                    string emailTemplate = activity.new_emailtemplate;
                    //send:
                    foreach (var supplierId in population)
                    {
                        try
                        {
                            bool notified = notifier.NotifySupplier(
                                smsTemplate,
                                emailTemplate,
                                useSms,
                                useEmail,
                                supplierId, supplierId, "account",
                                supplierId, "account");
                            if (!notified)
                            {
                                LogUtils.MyHandle.WriteToLog("Unable to notify supplier {0} in event {1}", supplierId, eve.new_campaigneventid);
                            }
                        }
                        catch (Exception exc)
                        {
                            LogUtils.MyHandle.HandleException(exc,
                                "Exception while sending campaign notifications to supplier {0} in event {1}",
                                supplierId, eve.new_campaigneventid);
                        }
                    }
                    //mark as sent:
                    eve.statuscode = (int)DML.Xrm.new_campaignevent.CampaignEventStatusCode.Sent;
                    dal.Update(eve);
                    XrmDataContext.SaveChanges();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc,
                                "Exception while sending campaign notifications in event {0}",
                                eve.new_campaigneventid);
                }
            }
        }

        private IEnumerable<Guid> GetPopulation(NoProblem.Core.DataModel.Xrm.new_campaignevent campaignEvent)
        {
            var activity = campaignEvent.new_campaignactivity_campaignevent;
            var campaign = activity.new_campaign_campaignactivity;
            string populationSql = campaign.new_populationsql;
            int daysBack = campaignEvent.new_counter.Value;
            string dateTimeField = campaign.new_triggerfield;
            DateTime fromDate = FixDateToLocal(DateTime.Now).Date.AddDays(daysBack * -1);
            DateTime toDate = fromDate;
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            IEnumerable<Guid> population = accountRepositoryDal.GetSupplierPopulation(populationSql, dateTimeField, fromDate, toDate);
            return population;
        }

        private IEnumerable<NoProblem.Core.DataModel.Xrm.new_campaignevent> FilterEvents(IEnumerable<NoProblem.Core.DataModel.Xrm.new_campaignevent> openEvents)
        {
            List<DML.Xrm.new_campaignevent> filteredEvents = new List<NoProblem.Core.DataModel.Xrm.new_campaignevent>();
            DataAccessLayer.CampaignEvent eventDal = new NoProblem.Core.DataAccessLayer.CampaignEvent(XrmDataContext);
            foreach (var eve in openEvents)
            {
                var existing = filteredEvents.FirstOrDefault(
                    x => x.new_counter.Value == eve.new_counter.Value
                        && x.new_campaignactivity_campaignevent.new_campaignid.Value ==
                            eve.new_campaignactivity_campaignevent.new_campaignid.Value);
                if (existing != null)
                {
                    //compare by priority, and then by creation date:
                    if (eve.CompareTo(existing) >= 0)
                    {
                        filteredEvents.Remove(existing);
                        filteredEvents.Add(eve);
                        DiscardEvent(existing, eventDal);
                    }
                    else
                    {
                        DiscardEvent(eve, eventDal);
                    }
                }
                else
                {
                    filteredEvents.Add(eve);
                }
            }
            return filteredEvents;
        }

        private void DiscardEvent(NoProblem.Core.DataModel.Xrm.new_campaignevent campaignEvent, DataAccessLayer.CampaignEvent dal)
        {
            campaignEvent.statuscode = (int)DML.Xrm.new_campaignevent.CampaignEventStatusCode.Discarded;
            dal.Update(campaignEvent);
            XrmDataContext.SaveChanges();
        }

        private void CreateEvent(DML.Xrm.new_campaignactivity activity, DataAccessLayer.CampaignEvent dal, DateTime todayFrom, DateTime todayTo)
        {
            if (dal.EventExists(activity.new_campaignactivityid, todayFrom, todayTo) == false)
            {
                DML.Xrm.new_campaignevent campaignEvent = new NoProblem.Core.DataModel.Xrm.new_campaignevent();
                campaignEvent.new_campaignactivityid = activity.new_campaignactivityid;
                campaignEvent.new_counter = activity.new_interval;
                dal.Create(campaignEvent);
            }
        }

        #endregion

    }
}
