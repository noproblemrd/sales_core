﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.Campaigns
{
    public class LosersCampaignManager : XrmUserBase
    {
        #region Ctor
        public LosersCampaignManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        public void StartLoserCampaign()
        {
            Thread tr = new Thread(
                delegate()
                {
                    try
                    {
                        StartLoserCampaignPrivate();
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception in async method LosersCampaignManager.StartLoserCampaignPrivate");
                    }
                });
            tr.Start();
        }

        private void StartLoserCampaignPrivate()
        {
            //check closure times
            BusinessClosures.ClosuresManager closures = new NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager(XrmDataContext);
            bool isClosureNow = closures.IsNowClosure(NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager.ClosureOptions.WeeklyAndClosureDates);
            if (isClosureNow)
            {
                LogUtils.MyHandle.WriteToLog(8, "Not sending weekly notifications because of closing times or time to send messages");
                return;
            }
            //get config settings
            int minimumCalls;
            int dayOfWeek;
            int hour;
            bool isValid = GetConfigurationValues(out minimumCalls, out dayOfWeek, out hour);
            if (!isValid)
            {
                LogUtils.MyHandle.WriteToLog(8, "Not sending weekly notifications because of not configured settings");
                return;
            }
            //check if it's sending time
            bool isTimeToSend = IsTimeToSend(dayOfWeek, hour);
            if (!isTimeToSend)
            {
                LogUtils.MyHandle.WriteToLog(8, "Not sending weekly notifications because of the config time");
                return;
            }
            //get suppliers
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            Regions.RegionsManager regManager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(XrmDataContext);
            var suppliersList = dal.GetSuppliersByStatus(NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Approved);
            suppliersList = from sup in suppliersList
                            where sup.new_isexternalsupplier.HasValue == false
                            || sup.new_isexternalsupplier.Value == false
                            select sup;
            List<Guid> recipients = new List<Guid>();
            //calculate lost calls            
            foreach (var supplier in suppliersList)
            {
                CalculateSupplier(minimumCalls, dal, recipients, incidentDal, regManager, supplier);
            }
            //send
            LogUtils.MyHandle.WriteToLog(0, "Found {0} suppliers to send weekly loser notification", recipients.Count);
            SendNotifications(recipients);
        }

        #endregion

        #region Private Methods

        private void CalculateSupplier(int minimumCalls, DataAccessLayer.AccountRepository dal, List<Guid> recipients, DataAccessLayer.Incident incidentDal, Regions.RegionsManager regManager, NoProblem.Core.DataModel.Xrm.account supplier)
        {
            try
            {
                int count = GetLostCallsCount(minimumCalls, incidentDal, regManager, supplier.accountid);
                if (count >= minimumCalls)
                {
                    supplier.new_lostcallscount = count;
                    dal.Update(supplier);
                    XrmDataContext.SaveChanges();
                    recipients.Add(supplier.accountid);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception calculating supplier {0} in Weekly Lost Calls Campaign", supplier.accountid);
            }
        }

        private bool IsTimeToSend(int dayOfWeek, int hour)
        {
            bool retVal = true;
            DateTime nowLocal = FixDateToLocal(DateTime.Now);
            int hourNow = nowLocal.Hour;
            int dayNow = ((int)nowLocal.DayOfWeek) + 1;
            if (dayNow != dayOfWeek || hour != hourNow)
            {
                retVal = false;
            }
            return retVal;
        }

        private bool GetConfigurationValues(out int minimumCalls, out int dayOfWeek, out int hour)
        {
            bool retVal = true;
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string dayStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.WEEKLY_LOST_CALLS_DAY);
            string hourStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.WEEKLY_LOST_CALLS_HOUR);
            string minimumStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.WEEKLY_LOST_CALLS_MINIMUM);
            if (!int.TryParse(minimumStr, out minimumCalls)
                | !int.TryParse(dayStr, out dayOfWeek)
                | !int.TryParse(hourStr, out hour))
            {
                retVal = false;
            }
            if (retVal &&
                ((dayOfWeek < 1 || dayOfWeek > 7)
                || (hour < 0 || hour > 23)
                || (minimumCalls <= 0)
                ))
            {
                retVal = false;
            }
            return retVal;
        }

        private void SendNotifications(List<Guid> recipients)
        {
            Notifications notifier = new Notifications(XrmDataContext);
            foreach (var id in recipients)
            {
                try
                {
                    LogUtils.MyHandle.WriteToLog(10, "Sending weekly loser notification to supplier {0}", id);                    
                    notifier.NotifySupplier(NoProblem.Core.DataModel.enumSupplierNotificationTypes.WEEKLY_LOST_CALLS,
                        id, id.ToString(), "account",
                        id.ToString(), "account");
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception sending notification to supplier {0} in Weekly Lost Calls Campaign", id);
                }
            }
        }

        private int GetLostCallsCount(int minimumCalls, DataAccessLayer.Incident incidentDal, Regions.RegionsManager regManager, Guid supplierId)
        {
            var regionsList = incidentDal.GetRegionsOfSuppliersLostIncidents(supplierId);
            int retVal = 0;
            if (regionsList.Count >= minimumCalls)
            {
                int i = regionsList.Count - 1;
                while (i >= 0)
                {
                    Guid regId = regionsList[i];
                    bool works = regManager.IsSupplierWorksInRegion(supplierId, regId);
                    int found = regionsList.RemoveAll(x => x == regId);
                    if (works)
                    {
                        retVal += found;
                    }
                    i = regionsList.Count - 1;
                }
            }
            return retVal;
        }

        #endregion

    }
}
