﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.AdvertiserRankings;
using Effect.Crm.Logs;
using System.Data;

namespace NoProblem.Core.BusinessLogic.AdvertiserRankings
{
    class AarRankingManager
    {
        public static List<AarSupplierEntry> SortSuppliersByRanking(DataTable suppliersTable, List<Guid> suppliersDone)
        {
            List<AarSupplierEntry> suppliersList = new List<AarSupplierEntry>();
            for (int i = 0; i < suppliersTable.Rows.Count; i++)
            {
                string accountid = "";
                AarSupplierEntry entry = new AarSupplierEntry();
                try
                {
                    accountid = suppliersTable.Rows[i]["AccountID"].ToString();
                    entry.SupplierId = new Guid(accountid);
                    if (suppliersDone.Contains(entry.SupplierId))
                    {
                        continue;
                    }
                    entry.Balance = (decimal)suppliersTable.Rows[i]["new_availablecashbalance"];
                    entry.TrialCallsAnswered = suppliersTable.Rows[i]["new_trialcallsanswered"] != DBNull.Value ? (int)suppliersTable.Rows[i]["new_trialcallsanswered"] : 0;
                    entry.TrialAttempts = suppliersTable.Rows[i]["new_trialattempts"] != DBNull.Value ? (int)suppliersTable.Rows[i]["new_trialattempts"] : 0;
                    entry.TrailStatusReason = (NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode)((int)suppliersTable.Rows[i]["new_trialstatusreason"]);
                    entry.Phone = (string)suppliersTable.Rows[i]["telephone1"];
                    entry.IncidentPrice = (decimal)suppliersTable.Rows[i]["new_incidentprice"];
                    entry.InitialTrialBudget = suppliersTable.Rows[i]["new_initialtrialbudget"] != DBNull.Value ? (int)suppliersTable.Rows[i]["new_initialtrialbudget"] : 0;
                    entry.AvailableCashBalance = suppliersTable.Rows[i]["new_availablecashbalance"] != DBNull.Value ? (decimal)suppliersTable.Rows[i]["new_availablecashbalance"] : 0;
                    entry.StageInTrialRegistration = (DataModel.Xrm.account.StageInTrialRegistration)(suppliersTable.Rows[i]["new_stageintrialregistration"] != DBNull.Value ? (int)suppliersTable.Rows[i]["new_stageintrialregistration"] : 0);
                    suppliersList.Add(entry);
                }
                catch (Exception ex)
                {
                    LogUtils.MyHandle.HandleException(ex, "Exception in AarRankingManager.SortSuppliersByRanking when extracting supplierId = {0}", accountid);
                }
            }
            suppliersList.Sort(SupplierComparer);
            return suppliersList;
        }

        private static int SupplierComparer(AarSupplierEntry entry1, AarSupplierEntry entry2)
        {
            int retVal = 0;
            try
            {
                if (entry1.TrailStatusReason > entry2.TrailStatusReason)
                {
                    retVal = -1;
                }
                else if (entry1.TrailStatusReason < entry2.TrailStatusReason)
                {
                    retVal = 1;
                }
                else//both same status reason
                {
                    if (entry1.TrailStatusReason == NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.InTrial)
                    {
                        decimal rank1 = entry1.Balance * (1 - (decimal)((decimal)entry1.TrialCallsAnswered / (decimal)(entry1.TrialAttempts + 1)));
                        decimal rank2 = entry2.Balance * (1 - (decimal)((decimal)entry2.TrialCallsAnswered / (decimal)(entry2.TrialAttempts + 1)));
                        if (rank1 < rank2)
                        {
                            retVal = -1;
                        }
                        else if (rank1 > rank2)
                        {
                            retVal = 1;
                        }
                    }
                    else//both accessible
                    {
                        if (entry1.StageInTrialRegistration > entry2.StageInTrialRegistration)
                        {
                            retVal = -1;
                        }
                        else if (entry1.StageInTrialRegistration < entry2.StageInTrialRegistration)
                        {
                            retVal = 1;
                        }
                        else//same stage in registration
                        {
                            decimal answeringRate1 = (decimal)entry1.TrialCallsAnswered / (decimal)(entry1.TrialAttempts + 1);
                            decimal answeringRate2 = (decimal)entry2.TrialCallsAnswered / (decimal)(entry2.TrialAttempts + 1);
                            if (answeringRate1 > answeringRate2)
                            {
                                retVal = -1;
                            }
                            else if (answeringRate1 < answeringRate2)
                            {
                                retVal = 1;
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "An exception was thrown while comparing two suppliers, they were returned as equals.");
            }
            return retVal;
        }
    }
}
