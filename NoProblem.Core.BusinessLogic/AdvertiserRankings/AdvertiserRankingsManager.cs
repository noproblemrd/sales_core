﻿/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: Effect.Onecall.BusinessLogic.AdvertiserRankingsManager
//  File: AdvertiserRankingsManager.cs
//  Description: Sets and Gets ranking parameters, Sorts suppliers by factor ranking.
//  Company: Onecall
//  Author: Alain Adler
//  CreatedOn: 19/10/10
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.AdvertiserRankings;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic
{
    public class AdvertiserRankingsManager : XrmUserBase
    {
        #region Ctor
        public AdvertiserRankingsManager(DML.Xrm.DataContext dataContext)
            : base(dataContext)
        {
        }
        #endregion

        #region Public Methods

        public GetRankingParametersResponse GetRankingParameters()
        {
            DataModel.XrmDataContext.ClearCache("new_rankingparameter");
            GetRankingParametersResponse response = new GetRankingParametersResponse();
            response.Parameters = new List<RankingParametersEntry>();
            foreach (DML.Xrm.new_rankingparameter param in XrmDataContext.new_rankingparameters)
            {
                RankingParametersEntry entry = new RankingParametersEntry();
                entry.Code = param.new_code.Value;
                entry.Weight = param.new_weight.Value;
                entry.Name = param.new_name;
                response.Parameters.Add(entry);
            }
            return response;
        }

        public void SetRankingParametersValues(SetAdvertiserRankingsRequest request)
        {
            DataAccessLayer.RankingParameter paramsAccess = new NoProblem.Core.DataAccessLayer.RankingParameter(XrmDataContext);
            foreach (RankParamCodeValuePair pair in request.Parameters)
            {
                DML.Xrm.new_rankingparameter crmRank = paramsAccess.GetRankingParameterByCode((int)pair.Code);
                crmRank.new_weight = pair.Weight;
                paramsAccess.Update(crmRank);
            }
            XrmDataContext.SaveChanges();
        }

        public List<SupplierEntry> SortSuppliersByRanking(DataTable suppliersTable)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.XrmDataContext.ClearCache("new_rankingparameter");
            List<SupplierEntry> suppliersList = new List<SupplierEntry>();
            for (int i = 0; i < suppliersTable.Rows.Count; i++)
            {
                string accountid = "";
                SupplierEntry entry = new SupplierEntry();
                try
                {
                    accountid = suppliersTable.Rows[i]["AccountID"].ToString();
                    entry.SupplierId = new Guid(accountid);
                    entry.SupplierNumber = suppliersTable.Rows[i]["AccountNumber"].ToString();
                    entry.SupplierName = suppliersTable.Rows[i]["name"].ToString();
                    entry.SumSurvey = suppliersTable.Rows[i]["new_sumsurvey"].ToString();
                    entry.SumAssistanceRequests = suppliersTable.Rows[i]["new_sumassistancerequests"].ToString();
                    entry.NumberOfEmployees = suppliersTable.Rows[i]["NumberOfEmployees"].ToString();
                    entry.ShortDescription = suppliersTable.Rows[i]["new_description"].ToString();
                    entry.Certificate = suppliersTable.Rows[i]["new_certificate"].ToString();
                    entry.DirectNumber = suppliersTable.Rows[i]["new_directnumber"].ToString();
                    entry.IncidentPrice = (decimal)suppliersTable.Rows[i]["new_incidentprice"];
                    entry.Telephone1 = suppliersTable.Rows[i]["telephone1"].ToString();
                    entry.ConsiderAsPrice = (decimal)suppliersTable.Rows[i]["New_considerasprice"];
                    suppliersList.Add(entry);
                }
                catch (Exception ex)
                {
                    LogUtils.MyHandle.HandleException(ex, "SearchSiteSuppliers: Failed adding the account the XML. Accountid:{0}", accountid);
                }
            }
            //suppliersList.Sort(SupplierComparer);
            return suppliersList;
        }

        #endregion

        #region Private Methods

        private int SupplierComparer(SupplierEntry entry1, SupplierEntry entry2)
        {
            int retVal = 0;
            try
            {
                //decimal entry1Score = 0;
                //decimal entry2Score = 0;
                //foreach (DML.Xrm.new_rankingparameter param in XrmDataContext.new_rankingparameters)
                //{
                //    try
                //    {
                //        PropertyInfo property = typeof(DML.Xrm.account).GetProperty(param.new_advertiserfield);

                //        decimal propValue1 = GetValueFromProperty(entry1.SupplierXrm, property);
                //        entry1Score += (propValue1 * param.new_weight.Value);

                //        decimal propValue2 = GetValueFromProperty(entry2.SupplierXrm, property);
                //        entry2Score += (propValue2 * param.new_weight.Value);
                //    }
                //    catch (Exception exc)
                //    {
                //        LogUtils.MyHandle.WriteToLog(1, "Clould not use ranking parameter {0}. Exception: {1}", param.new_name, exc.Message);
                //    }
                //}
                //if (entry1Score > entry2Score)
                //{
                //    retVal = -1;
                //}
                //else if (entry1Score < entry2Score)
                //{
                //    retVal = 1;
                //}
                //else//ranking params gave equal value. sort by incident price
                //{
                    decimal xPrice = entry1.IncidentPrice;
                    decimal yPrice = entry2.IncidentPrice;
                    if (xPrice > yPrice)
                    {
                        retVal = -1;
                    }
                    else if (xPrice < yPrice)
                    {
                        retVal = 1;
                    }
                    else//incident price also gave equal results. sort by social fairness.
                    {
                        DataAccessLayer.IncidentAccount incidentAccountDAL = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                        DateTime xLastWinDate = incidentAccountDAL.GetLastWinDate(entry1.SupplierId);
                        DateTime yLastWindDate = incidentAccountDAL.GetLastWinDate(entry2.SupplierId);
                        int isXbeforeY = xLastWinDate.CompareTo(yLastWindDate);
                        if (isXbeforeY < 0)// x before y.
                        {
                            retVal = -1;
                        }
                        else if (isXbeforeY > 0)//x after y.
                        {
                            retVal = 1;
                        }
                    }
               // }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "An exception was thrown while comparing two suppliers, they were returned as equals.");
            }
            return retVal;
        }

        private decimal GetValueFromProperty(DML.Xrm.account entry, PropertyInfo property)
        {
            decimal retVal = decimal.Zero;
            object valueObj = property.GetValue(entry, null);
            if (valueObj is decimal)
            {
                retVal = (decimal)valueObj;
            }
            else if (valueObj is double)
            {
                double value = (double)valueObj;
                retVal = decimal.Parse(value.ToString());
            }
            else if (valueObj is int)
            {
                int value = (int)valueObj;
                retVal = decimal.Parse(value.ToString());
            }
            else if (valueObj is float)
            {
                float value = (float)valueObj;
                retVal = decimal.Parse(value.ToString());
            }
            return retVal;
        }

        #endregion
    }
}
