﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic
{
    public class CustomerPageDataManager
    {
        
        public int GetTotalCaseCount()
        {
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(null);
            string query =
                "EXEC [dbo].[GetCounter]";
            /*
            @"select count(incidentid) from incident with(nolock) 
            where deletionstatecode = 0";
             * */
            object scalar = dal.ExecuteScalar(query);
            return (int)scalar;// +1465000;
        }

        public List<DataModel.Case.CustomerPageCaseData> GetLastThreeCases(Guid headingId)
        {
            string query =
                @"select top 3 inc.new_primaryexpertiseid, inc.description, inc.createdon, inc.incidentid, rg1.New_name rg1, rg2.New_name rg2, rg3.New_name rg3, rg4.New_name rg4
                from incident inc with(nolock)
                join New_region rg1 with(nolock) on inc.new_regionid = rg1.New_regionId
                left join New_region rg2 with(nolock) on rg1.new_parentregionid = rg2.new_regionid
                left join New_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionid
                left join New_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionid
                where inc.deletionstatecode = 0
                and inc.statuscode != 200013
                and inc.statuscode != 200014 
                and (inc.new_isstoppedmanually is null or inc.new_isstoppedmanually = 0) ";
            //200014=badword, 200013=blacklist
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            if (headingId != Guid.Empty)
            {
                parameters.Add(new KeyValuePair<string, object>("@headingId", headingId));
                query += " and new_primaryexpertiseid = @headingId ";
            }
            query += " order by createdon desc";
            List<DataModel.Case.CustomerPageCaseData> dataList = new List<NoProblem.Core.DataModel.Case.CustomerPageCaseData>();
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(null);
            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                DataModel.Case.CustomerPageCaseData data = new NoProblem.Core.DataModel.Case.CustomerPageCaseData();
                data.CreatedOn = ((DateTime)row["createdon"]);
                data.Description = Convert.ToString(row["description"]);
                data.HeadingId = (Guid)row["new_primaryexpertiseid"];
                data.CaseId = (Guid)row["incidentid"];
                List<string> regions = new List<string>();
                for (int i = 1; i <= 4; i++)
                {
                    string rg = Convert.ToString(row["rg" + i]);
                    if (!string.IsNullOrEmpty(rg))
                    {
                        regions.Add(rg);
                    }
                    else
                    {
                        break;
                    }
                }
                data.RegionName = ResolveRegionName(regions);
                dataList.Add(data);
            }
            return dataList;
        }

        private string ResolveRegionName(List<string> regions)
        {
            string retVal;
            if (regions.Count > 0)
            {
                if (regions.Count >= 4)
                {
                    retVal = regions[1] + ", " + regions[3];
                }
                else if (regions.Count == 3)
                {
                    retVal = regions[0] + ", " + regions[2];
                }
                else if (regions.Count == 2)
                {
                    retVal = regions[0] + ", " + regions[1];
                }
                else
                {
                    retVal = regions[0];
                }
            }
            else
            {
                retVal = string.Empty;
            }
            return retVal;
        }

        public List<DataModel.Consumer.Containers.TipData> GetAllTips()
        {
            DataAccessLayer.Tip dal = new NoProblem.Core.DataAccessLayer.Tip(null);
            List<DataModel.Consumer.Containers.TipData> dataList = dal.GetAll();
            return dataList;
        }

        public bool CheckUsaZipCode(string zipCode)
        {
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(null);
            string code = dal.GetCodeByNameAndLevel(zipCode, 4);
            return !string.IsNullOrEmpty(code);
        }
    }
}
