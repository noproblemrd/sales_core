﻿using System;
using System.Collections.Generic;
using NoProblem.Core.DataModel.SeoData;

namespace NoProblem.Core.BusinessLogic.SEODataServices
{
    public class SEOGeneralDataRetriever : SEODataRetrieverBase
    {
        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();
        private const int popularHeadingsAmount = 4;

        public SEOGeneralDataRetriever(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<string> GetPopularHeadingsInRegion(string state)
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<string>>(cachePrefix + ".GetPopularHeadingsInState", state, GetPopularHeadingsInState, 1440);
        }

        private List<string> GetPopularHeadingsInState(string state)
        {
            List<string> retVal = new List<string>();
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.Region.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_region>.SqlParametersList();
            parameters.Add("@state", state);
            var reader = dal.ExecuteReader(queryForGetPopularHeadingInState, parameters);
            foreach (var row in reader)
            {
                string heading = Convert.ToString(row["new_name"]);
                retVal.Add(heading);
            }
            if (retVal.Count < popularHeadingsAmount)
            {
                AddTopHeadings(retVal);
            }
            return retVal;
        }

        public List<string> GetPopularHeadingsInRegion(string state, string city)
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<string>>(cachePrefix + ".GetPopularHeadingsInCity", state + ";" + city, GetPopularHeadingsInCity, 1440);
        }

        private List<string> GetPopularHeadingsInCity(string stateCityConcat)
        {
            string[] split = stateCityConcat.Split(';');
            List<string> retVal = new List<string>();
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.Region.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_region>.SqlParametersList();
            parameters.Add("@state", split[0]);
            parameters.Add("@city", split[1]);
            var reader = dal.ExecuteReader(queryForGetPopularHeadingInCity, parameters);
            foreach (var row in reader)
            {
                string heading = Convert.ToString(row["new_name"]);
                retVal.Add(heading);
            }
            if (retVal.Count < popularHeadingsAmount)
            {
                AddTopHeadings(retVal);
            }
            return retVal;
        }

        private void AddTopHeadings(List<string> foundHeadings)
        {
            var topHeadings = GetTopHeadings();
            int index = 0;
            while (foundHeadings.Count < popularHeadingsAmount && index < topHeadings.Count)
            {
                if (foundHeadings.Contains(topHeadings[index]))
                {
                    index++;
                }
                else
                {
                    foundHeadings.Add(topHeadings[index]);
                }
            }
        }

        private List<string> GetTopHeadings()
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<string>>(cachePrefix, "GetTopHeadings", GetTopHeadingsMethod, 1440);
        }

        private List<string> GetTopHeadingsMethod()
        {
            List<string> retVal = new List<string>();
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var reader = dal.ExecuteReader(queryForGetTopHeadings);
            foreach (var row in reader)
            {
                string heading = Convert.ToString(row["new_name"]);
                retVal.Add(heading);
            }
            return retVal;
        }

        public List<SEOPopularCityData> GetPopularCities()
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<SEOPopularCityData>>(cachePrefix, "GetPopularCities", GetPopularCitiesMethod, DataAccessLayer.CacheManager.ONE_DAY);
        }

        private List<SEOPopularCityData> GetPopularCitiesMethod()
        {
            List<SEOPopularCityData> retVal = new List<SEOPopularCityData>();
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var reader = dal.ExecuteReader(queryForGetPopularCities);
            foreach (var row in reader)
            {
                SEOPopularCityData data = new SEOPopularCityData();
                data.CityName = Convert.ToString(row["cityName"]);
                data.StateAbbreviation = Convert.ToString(row["stateAbb"]);
                data.StateFullName = Convert.ToString(row["stateFullName"]);
                data.RegionId = (Guid)row["regionId"];
                retVal.Add(data);
            }
            return retVal;
        }

        public List<string> GetPopularCitiesInState(string state)
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<string>>(cachePrefix + ".GetPopularCitiesInState", state, GetPopularCitiesInStateMethod, 1440);
        }

        private List<string> GetPopularCitiesInStateMethod(string state)
        {
            List<string> retVal = new List<string>();
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.Region.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_region>.SqlParametersList();
            parameters.Add("@state", state);
            var reader = dal.ExecuteReader(queryForGetPopularCitiesInState, parameters);
            foreach (var row in reader)
            {
                retVal.Add(Convert.ToString(row["new_name"]));
            }
            return retVal;
        }

        public List<string> GetAllCitiesInState(string state)
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<string>>(cachePrefix + ".GetAllCitiesInState", state, GetAllCitiesInStateMethod, 1440);
        }

        private List<string> GetAllCitiesInStateMethod(string state)
        {
            List<string> retVal = new List<string>();
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.Region.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_region>.SqlParametersList();
            parameters.Add("@state", state);
            var reader = dal.ExecuteReader(queryForGetAllCitiesInState, parameters);
            foreach (var row in reader)
            {
                retVal.Add(Convert.ToString(row["new_name"]));
            }
            return retVal;
        }

        public List<SEOStateData> GetAllStates()
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<SEOStateData>>(cachePrefix, "GetAllStates", GetAllStatesMethod, 1440);
        }

        private List<SEOStateData> GetAllStatesMethod()
        {
            List<SEOStateData> retVal = new List<SEOStateData>();
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var reader = dal.ExecuteReader(queryForGetAllStates);
            foreach (var row in reader)
            {
                SEOStateData data = new SEOStateData();
                data.Abbreviation = Convert.ToString(row["new_name"]);
                data.FullName = Convert.ToString(row["new_englishname"]);
                data.IsPopular = row["new_ispopular"] != DBNull.Value ? (bool)row["new_ispopular"] : false;
                retVal.Add(data);
            }
            return retVal;
        }

        private const string queryForGetAllStates =
            @"
select new_name, new_englishname, new_ispopular from new_region with(nolock) where deletionstatecode = 0 and new_level = 1
order by new_englishname
";

        private const string queryForGetPopularCities =
            @"
select city.new_name as cityName, state.new_name as stateAbb, state.new_englishname as stateFullName, city.new_regionid as regionId
from new_region city with(nolock) 
join new_region county with(nolock) on city.new_parentregionid = county.new_regionid
join new_region state with(nolock) on county.new_parentregionid = state.new_regionid
where city.deletionstatecode = 0 and city.new_level = 3 and city.new_ispopular = 1 
order by cityName
";

        private const string queryForGetPopularCitiesInState =
            @"
select distinct city.new_name 
from new_region city with(nolock)
join new_region county with(nolock) on city.new_parentregionid = county.new_regionid
join new_region state with(nolock) on county.new_parentregionid = state.new_regionid
where city.new_ispopularinstate = 1 and city.new_level = 3
and state.new_englishname = @state
order by city.new_name
";

        private const string queryForGetAllCitiesInState =
            @"
select distinct city.new_name 
from new_region city with(nolock)
join new_region county with(nolock) on city.new_parentregionid = county.new_regionid
join new_region state with(nolock) on county.new_parentregionid = state.new_regionid
where city.new_level = 3 and state.new_level = 1
and state.new_englishname = @state
order by city.new_name
";

        private const string queryForGetPopularHeadingInState =
            @"
SELECT 
top 4
pe.new_name
  FROM popularHeadingsInRegionsStats stat
  join new_region state on state.new_regionid = stat.new_regionid
  join new_primaryexpertise pe on pe.new_primaryexpertiseid = stat.new_primaryexpertiseid
  where state.new_englishname = @state
  and state.new_level = 1
  and isnull(pe.new_display, 1) = 1
  group by pe.new_name
  order by sum(count) desc
";

        private const string queryForGetPopularHeadingInCity =
  @"
  SELECT 
top 4
pe.new_name
  FROM popularHeadingsInRegionsStats stat
  join new_region city on city.new_regionid = stat.new_regionid
  join new_region county on county.new_regionid = city.new_parentregionid
  join new_region state on state.new_regionid = county.new_parentregionid
  join new_primaryexpertise pe on pe.new_primaryexpertiseid = stat.new_primaryexpertiseid
  where state.new_englishname = @state
  and city.new_name = @city
  and city.new_level = 3
  and isnull(pe.new_display, 1) = 1
  group by pe.new_name
  order by sum(count) desc
";

        private const string queryForGetTopHeadings =
            @"
select top 4 
pe.new_name
from popularHeadingsInRegionsStats stat
join new_primaryexpertise pe on pe.new_primaryexpertiseid = stat.new_primaryexpertiseid
where isnull(pe.new_display, 1) = 1
group by pe.new_name
order by sum(count) desc
";
    }    
}
