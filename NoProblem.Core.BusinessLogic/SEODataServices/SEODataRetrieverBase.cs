﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SEODataServices
{
    public class SEODataRetrieverBase : XrmUserBase
    {
        public SEODataRetrieverBase(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        protected string FixRegionsString(string regions, string state)
        {
            string retVal;
            string[] split = regions.Split(';');
            if (split.Length == 4)
            {
                string zipcode = split[0];
                string city = split[1];
                string county = split[2];
                StringBuilder builder = new StringBuilder();
                builder.Append(city).Append(", ").Append(county).Append(", ").Append(state).Append(" ").Append(zipcode);
                retVal = builder.ToString();
            }
            else if (split.Length > 0)
            {
                retVal = split[0];
            }
            else
            {
                retVal = string.Empty;
            }
            return retVal;
        }
    }
}
