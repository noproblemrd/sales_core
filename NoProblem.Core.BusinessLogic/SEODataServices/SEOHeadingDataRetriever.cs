﻿using System;
using System.Collections.Generic;
using System.Linq;
using NoProblem.Core.DataModel.SeoData;

namespace NoProblem.Core.BusinessLogic.SEODataServices
{
    public class SEOHeadingDataRetriever : SEODataRetrieverBase
    {
        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();

        public SEOHeadingDataRetriever(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public SEOLeadsAndSupplierForHeadingAndRegion GetLeadsAndSuppliers(string headingName, string state)
        {
            return DataAccessLayer.CacheManager.Instance.Get<SEOLeadsAndSupplierForHeadingAndRegion>(cachePrefix, headingName + ";" + state, GetLeadsAndSuppliersWithStateOnlyMethod, 60);
        }

        private SEOLeadsAndSupplierForHeadingAndRegion GetLeadsAndSuppliersWithStateOnlyMethod(string headingNameStateConcat)
        {
            SEOLeadsAndSupplierForHeadingAndRegion retVal = new SEOLeadsAndSupplierForHeadingAndRegion();
            string[] split = headingNameStateConcat.Split(';');
            string state = split[1];
            string headingName = split[0];
            DataAccessLayer.PrimaryExpertise.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_primaryexpertise>.SqlParametersList();
            parameters.Add("@state", state);
            parameters.Add("@headingName", headingName);
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var reader = dal.ExecuteReader("SEO_GetLeadsAndSuppliersWithStateOnly", parameters, System.Data.CommandType.StoredProcedure);
            foreach (var row in reader)
            {
                bool isLead = (bool)row["isLead"];
                if (isLead)
                {
                    SEOLeadData data = CreateLeadDataFromDbRow(row);
                    retVal.Leads.Add(data);
                    retVal.LeadsCount = (int)row["cnt"];
                }
                else
                {
                    SEOSupplierData data = CreateSupplierDataFromDbRow(state, row);
                    retVal.Suppliers.Add(data);
                    retVal.SupplierCount = (int)row["cnt"];
                }
            }
            retVal.StateName = state;
            retVal.StateAbbreviation = dal.GetStateAbbreviation(state);
            return retVal;
        }

        public SEOLeadsAndSupplierForHeadingAndRegion GetLeadsAndSuppliers(string headingName, string state, string city)
        {
            return DataAccessLayer.CacheManager.Instance.Get<SEOLeadsAndSupplierForHeadingAndRegion>(cachePrefix, headingName + ";" + state + ";" + city, GetLeadsAndSuppliersWithStateAndCityMethod, 60);
        }

        private SEOLeadsAndSupplierForHeadingAndRegion GetLeadsAndSuppliersWithStateAndCityMethod(string headingNameStateCityConcat)
        {
            SEOLeadsAndSupplierForHeadingAndRegion retVal = new SEOLeadsAndSupplierForHeadingAndRegion();
            string[] split = headingNameStateCityConcat.Split(';');
            string city = split[2];
            string state = split[1];
            string headingName = split[0];
            DataAccessLayer.PrimaryExpertise.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_primaryexpertise>.SqlParametersList();
            parameters.Add("@state", state);
            parameters.Add("@headingName", headingName);
            parameters.Add("@city", city);
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var reader = dal.ExecuteReader("SEO_GetLeadsAndSuppliersWithStateAndCity", parameters, System.Data.CommandType.StoredProcedure);
            foreach (var row in reader)
            {
                bool isLead = (bool)row["isLead"];
                if (isLead)
                {
                    SEOLeadData data = CreateLeadDataFromDbRow(row);
                    retVal.Leads.Add(data);
                    retVal.LeadsCount = (int)row["cnt"];
                }
                else
                {
                    SEOSupplierData data = CreateSupplierDataFromDbRow(state, row);
                    retVal.Suppliers.Add(data);
                    retVal.SupplierCount = (int)row["cnt"];
                }
            }
            retVal.StateName = state;
            retVal.CityName = city;
            retVal.StateAbbreviation = dal.GetStateAbbreviation(state);
            return retVal;
        }

        private SEOSupplierData CreateSupplierDataFromDbRow(string defaultRegion, Dictionary<string, object> row)
        {
            SEOSupplierData data = new SEOSupplierData();
            data.Name = Convert.ToString(row["one"]);
            data.Phone = Convert.ToString(row["two"]);
            data.Region = Convert.ToString(row["three"]);
            if (string.IsNullOrEmpty(data.Region))
            {
                data.Region = defaultRegion;
            }
            string servicesStr = Convert.ToString(row["four"]);
            servicesStr = System.Web.HttpUtility.HtmlDecode(servicesStr);// takes care of turning &amp; to & in headings like "air con & heating".
            string[] servicesSplit = servicesStr.Split(';');
            for (int i = 0; i < servicesSplit.Length - 1; i++)
            {
                data.Services.Add(servicesSplit[i]);
            }
            data.Id = (Guid)row["five"];
            data.OnDuty = (bool)row["six"];
            data.IsPaying = (bool)row["seven"];
            return data;
        }

        private SEOLeadData CreateLeadDataFromDbRow(Dictionary<string, object> row)
        {
            SEOLeadData data = new SEOLeadData();
            data.Description = Convert.ToString(row["one"]);
            string stateLead = Convert.ToString(row["three"]);
            string regions = Convert.ToString(row["two"]);
            data.Region = FixRegionsString(regions, stateLead);
            data.IsNew = (bool)row["six"];
            return data;
        }

        public SEOLeadsAndSupplierForHeadingAndRegion GetLeadsAndSuppliersWithZipCode(string headingName, string zipcode)
        {
            return DataAccessLayer.CacheManager.Instance.Get<SEOLeadsAndSupplierForHeadingAndRegion>(cachePrefix, headingName + ";" + zipcode, GetLeadsAndSuppliersWithZipcodeMethod, 60);
        }

        private SEOLeadsAndSupplierForHeadingAndRegion GetLeadsAndSuppliersWithZipcodeMethod(string headingNameZipcodeConcat)
        {
            SEOLeadsAndSupplierForHeadingAndRegion retVal = new SEOLeadsAndSupplierForHeadingAndRegion();
            string[] split = headingNameZipcodeConcat.Split(';');
            string zipcode = split[1];
            string headingName = split[0];
            DataAccessLayer.PrimaryExpertise.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_primaryexpertise>.SqlParametersList();
            parameters.Add("@zipcode", zipcode);
            parameters.Add("@headingName", headingName);
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var reader = dal.ExecuteReader("SEO_GetLeadsAndSuppliersWithZipcode", parameters, System.Data.CommandType.StoredProcedure);
            foreach (var row in reader)
            {
                bool isLead = (bool)row["isLead"];
                if (isLead)
                {
                    SEOLeadData data = CreateLeadDataFromDbRow(row);
                    retVal.Leads.Add(data);
                    retVal.LeadsCount = (int)row["cnt"];
                }
                else
                {
                    SEOSupplierData data = CreateSupplierDataFromDbRow(zipcode, row);
                    retVal.Suppliers.Add(data);
                    retVal.SupplierCount = (int)row["cnt"];
                }
            }
            NoProblem.Core.DataModel.Regions.ZipCodeData zipCodeData = dal.GetZipCodeData(zipcode);
            if (string.IsNullOrEmpty(zipCodeData.State))
            {
                throw new Exception("ZIPCODE_DOES_NOT_EXIST");
            }
            retVal.CityName = zipCodeData.City;
            retVal.StateName = zipCodeData.State;
            retVal.StateAbbreviation = zipCodeData.StateAbbreviation;
            return retVal;
        }

        public SEOLeadsForHeading GetLeadsForHeading(Guid headingId)
        {
            return DataAccessLayer.CacheManager.Instance.Get<SEOLeadsForHeading>(cachePrefix, headingId.ToString(), GetLeadsForHeadingByIdMethod, 30);
        }

        public SEOLeadsForHeading GetLeadsForHeading(string headingName)
        {
            return DataAccessLayer.CacheManager.Instance.Get<SEOLeadsForHeading>(cachePrefix, headingName, GetLeadsForHeadingByNameMethod, 30);
        }

        private SEOLeadsForHeading GetLeadsForHeadingByIdMethod(string headingId)
        {
            DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            DataAccessLayer.PrimaryExpertise.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_primaryexpertise>.SqlParametersList();
            parameters.Add("@id", headingId);
            var reader = dal.ExecuteReader(queryForGetLeadsForHeadingByIdMethod, parameters);
            SEOLeadsForHeading retVal = new SEOLeadsForHeading();
            if (reader.Count > 0)
            {
                retVal.Count = (int)reader.First()["cnt"];
                foreach (var row in reader)
                {
                    SEOLeadData data = new SEOLeadData();
                    data.Description = Convert.ToString(row["description"]);
                    string state = Convert.ToString(row["state"]);
                    string regions = Convert.ToString(row["regions"]);
                    data.Region = FixRegionsString(regions, state);
                    data.IsNew = (bool)row["isNew"];
                    retVal.Leads.Add(data);
                }
            }
            return retVal;
        }

        private SEOLeadsForHeading GetLeadsForHeadingByNameMethod(string headingName)
        {
            DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            DataAccessLayer.PrimaryExpertise.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_primaryexpertise>.SqlParametersList();
            parameters.Add("@name", headingName);
            var reader = dal.ExecuteReader(queryForGetLeadsForHeadingByNameMethod, parameters);
            SEOLeadsForHeading retVal = new SEOLeadsForHeading();
            if (reader.Count > 0)
            {
                retVal.Count = (int)reader.First()["cnt"];
                foreach (var row in reader)
                {
                    SEOLeadData data = new SEOLeadData();
                    data.Description = Convert.ToString(row["description"]);
                    string state = Convert.ToString(row["state"]);
                    string regions = Convert.ToString(row["regions"]);
                    data.Region = FixRegionsString(regions, state);
                    data.IsNew = (bool)row["isNew"];
                    retVal.Leads.Add(data);
                }
            }
            var randomized = retVal.Leads.Skip(20);
            Random rnd = new Random();
            randomized = from item in randomized
                         orderby rnd.Next()
                         select item;
            retVal.Leads = randomized.ToList();
            return retVal;
        }

        public SEOLeadsAndHeadingForGroup GetLeadsAndHeadingsForCategoryGroup(string categoryGroupName)
        {
            return DataAccessLayer.CacheManager.Instance.Get<SEOLeadsAndHeadingForGroup>(cachePrefix, categoryGroupName, GetLeadsAndHeadingsForCategoryGroupByNameMethod, 30);
        }

        private SEOLeadsAndHeadingForGroup GetLeadsAndHeadingsForCategoryGroupByNameMethod(string categoryGroupName)
        {
            DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            DataAccessLayer.PrimaryExpertise.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_primaryexpertise>.SqlParametersList();
            parameters.Add("@groupName", categoryGroupName);
            var reader = dal.ExecuteReader(queryForGetLeadsForCategoryGroupByName, parameters);
            SEOLeadsAndHeadingForGroup retVal = new SEOLeadsAndHeadingForGroup();
            if (reader.Count > 0)
            {
                retVal.Count = (int)reader.First()["cnt"];
                foreach (var row in reader)
                {
                    SEOLeadData data = new SEOLeadData();
                    data.Description = Convert.ToString(row["description"]);
                    string state = Convert.ToString(row["state"]);
                    string regions = Convert.ToString(row["regions"]);
                    data.Region = FixRegionsString(regions, state);
                    data.HeadingName = Convert.ToString(row["headingName"]);
                    data.IsNew = (bool)row["isNew"];
                    retVal.Leads.Add(data);
                }
            }
            var readerHeadings = dal.ExecuteReader(queryForGetHeadingInGroupByName, parameters);
            foreach (var row in readerHeadings)
            {
                string headName = Convert.ToString(row["new_name"]);
                if (!string.IsNullOrEmpty(headName))
                {
                    retVal.Headings.Add(headName);
                }
            }
            return retVal;
        }

        private const string queryForGetLeadsForHeadingByNameMethod =
            @"declare @peId uniqueidentifier
select @peId = new_primaryexpertiseid from new_primaryexpertise with(nolock)
where deletionstatecode = 0 and new_name = @name
declare @cnt int
select @cnt = count(*) from incident with(nolock)
where new_primaryexpertiseid = @peId
select top 170 @cnt as cnt, incE.new_webdescription as description, rg.new_englishname as 'regions', rg4.new_name as 'state'
, cast(case when new_ordered_providers < 3 and incB.createdon > dateadd(day, -2, getdate()) then 1 else 0 end as bit) as 'isNew'
from 
incidentbase incB with(nolock)
join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
where new_primaryexpertiseid = @peId
and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
and isnull(New_IsStoppedManually, 0) = 0
order by isNew desc, incB.createdon desc";

        private const string queryForGetLeadsForHeadingByIdMethod =
           @"
declare @cnt int
select @cnt = count(*) from incident with(nolock)
where new_primaryexpertiseid = @id
select top 150 @cnt as cnt, incE.new_webdescription as description, rg.new_englishname as 'regions', rg4.new_name as 'state'
, cast(case when new_ordered_providers < 3 and incB.createdon > dateadd(day, -2, getdate()) then 1 else 0 end as bit) as 'isNew'
from 
incidentbase incB with(nolock)
join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
where new_primaryexpertiseid = @id
and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
and isnull(New_IsStoppedManually, 0) = 0
order by isNew desc, incB.createdon desc";

//        private const string queryForGetLeadsAndSuppliersWithStateOnly =
//            @"
//declare @peId uniqueidentifier
//declare @reId uniqueidentifier
//
//select @peId = new_primaryexpertiseid from new_primaryexpertise with(nolock)
//where deletionstatecode = 0 and new_name = @headingName
//
//select @reId = new_regionid from new_regionextensionbase with(nolock)
//where  new_englishname = @state and new_level = 1
//
//declare @cntSupps int
//
//select @cntSupps = count(*) 
//from account acc with(nolock) 
//join new_accountexpertise ae with(nolock) on ae.new_accountid = acc.accountid and ae.statecode = 0 and new_primaryexpertiseid = @peId
//join new_regionaccountexpertise ra with(nolock) on ra.new_accountid = acc.accountid and ra.new_regionstate = 1 and ra.new_regionid = @reId and ra.deletionstatecode = 0
//where acc.deletionstatecode = 0
//and (acc.new_status = 1
//or (acc.new_status = 4 and acc.new_trialstatusreason in (3, 4)))
//and isnull(new_isleadbuyer, 0) = 0
//
//declare @cntLeads int
//
//select @cntLeads = count(*) from
//incidentbase incB with(nolock)
//join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
//join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
//join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
//join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
//join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
//where new_primaryexpertiseid = @peId
//and rg4.new_regionid = @reId
//
//create table #tmpOnlyState (cnt int, one nvarchar(max), two nvarchar(max), three nvarchar(max), four nvarchar(max), five uniqueidentifier, six bit, isLead bit)
//
//insert into #tmpOnlyState
//select top 150 
//@cntSupps as cnt
//, acc.name as '1'
//, acc.telephone1 as '2'
//, new_fulladdress as '3'
//, (select new_primaryexpertiseidname + ';' AS [text()] from new_accountexpertise ae with(nolock) 
//	join new_primaryexpertise pe with(Nolock) on pe.New_primaryexpertiseId = ae.new_primaryexpertiseid
//	where ae.statecode = 0 and ae.new_accountid = acc.accountid and isnull(pe.new_display, 1) = 1
//	For XML PATH(''))
//, acc.accountid
//, 
//cast( case when av.new_accountid is not null
//AND 
//isnull(new_firstpaymentneeded, 0) = 0 then 1 else 0 end as bit)
//, 0
//from account acc with(nolock) 
//join new_accountexpertise ae with(nolock) on ae.new_accountid = acc.accountid and ae.statecode = 0 and new_primaryexpertiseid = @peId
//join new_regionaccountexpertise ra with(nolock) on ra.new_accountid = acc.accountid and ra.new_regionstate = 1 and ra.new_regionid = @reId and ra.deletionstatecode = 0
//left join new_availability av with(nolock) 
//on av.deletionstatecode = 0
//and new_day = datepart(dw,getdate())
//and new_from <= CONVERT(char(8),getdate(),108) 
//AND new_to >= CONVERT(char(8),getdate(),108)
//and ra.new_accountid = av.new_accountid
//where acc.deletionstatecode = 0
//and (acc.new_status = 1
//or (acc.new_status = 4 and acc.new_trialstatusreason in (3, 4)))
//and isnull(new_isleadbuyer, 0) = 0
//order by new_status, case new_status when 1 then 100 else new_trialstatusreason end desc, ae.new_incidentprice desc
//
//
//insert into #tmpOnlyState
//select top 150 
//@cntLeads as cnt
//, incE.new_webdescription as '1'
//, rg.new_englishname as '2'
//, rg4.new_name as '3'
//, ''
//, '00000000-0000-0000-0000-000000000000'
//, case when new_ordered_providers < 3 and incB.createdon > dateadd(day, -2, getdate()) then 1 else 0 end as 'isNew'
//, 1
//from incidentbase incB with(nolock)
//join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
//join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
//join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
//join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
//join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
//where new_primaryexpertiseid = @peId
//and rg4.new_regionid = @reId
//and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
//and isnull(New_IsStoppedManually, 0) = 0
//order by isNew desc, incB.createdon desc
//
//select * from #tmpOnlyState
//
//drop table #tmpOnlyState
//";

//        private const string queryForGetLeadsAndSuppliersWithStateAndCity =
//            @"
//declare @peId uniqueidentifier
//declare @stateId uniqueidentifier
//declare @cityId uniqueidentifier
//declare @countyId uniqueidentifier
//
//select @peId = new_primaryexpertiseid from new_primaryexpertise with(nolock)
//where deletionstatecode = 0 and new_name = @headingName
//
//select @stateId = new_regionid from new_regionextensionbase with(nolock)
//where  new_englishname = @state and new_level = 1
//
//select @cityId = rgCity.new_regionid, @countyId = rgCounty.new_regionid from new_regionextensionbase rgCity with(nolock)
//join new_regionextensionbase rgCounty with(nolock) on rgCity.new_parentregionid = rgCounty.new_regionid
//join new_regionextensionbase rgState with(nolock) on rgCounty.new_parentregionid = rgState.new_regionid
//where  rgCity.new_name = @city and rgCity.new_level = 3 and rgState.new_regionid = @stateId
//
//declare @cntSupps int
//
//create table #accs (id uniqueidentifier, price decimal)
//
//insert into #accs
//select AccountId, ae.new_incidentprice price
//from account acc with(nolock) 
//join new_accountexpertise ae with(nolock) on ae.new_accountid = acc.accountid and ae.statecode = 0 and new_primaryexpertiseid = @peId
//where acc.deletionstatecode = 0
//and (acc.new_status = 1
//or (acc.new_status = 4 and acc.new_trialstatusreason in (3, 4)))
//and isnull(new_isleadbuyer, 0) = 0
//
//	create table #tmpRegionHirarchy ( RegionLevel int, RegionId uniqueidentifier, ParentRegionId uniqueidentifier );
//create index idx_region on #tmpRegionHirarchy(RegionId) ;
//
//WITH RegionHirarchy(RegionLevel,RegionId,ParentRegionId)
//AS
//(
//	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
//	FROM new_region with(nolock)
//	WHERE new_region.new_regionid = @cityId and new_region.deletionstatecode = 0
//	UNION ALL
//	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
//	FROM new_region with(nolock)
//	INNER JOIN RegionHirarchy ON RegionHirarchy.ParentRegionId = new_regionid
//)
//insert into #tmpRegionHirarchy
//	select RegionLevel,  RegionId,  ParentRegionId from RegionHirarchy
//	
//	
//	create table #tmpJoinning ( accId uniqueidentifier, RegionId uniqueidentifier, RegionState int, price decimal)
//	insert into #tmpJoinning
//			select #accs.id , regaccount.new_regionid, regaccount.New_regionstate, #accs.price
//	from #accs
//	INNER JOIN(
//	select a.new_regionid, a.New_regionstate, a.new_accountid, b.deletionstatecode
//	from dbo.New_regionaccountexpertiseExtensionBase a
//	join dbo.New_regionaccountexpertiseBase b
//	on a.New_regionaccountexpertiseId = b.New_regionaccountexpertiseId
//	where b.deletionstatecode = 0)
//	
//	
// regaccount  ON regaccount.new_accountid = #accs.id 
//INNER JOIN #tmpRegionHirarchy ON #tmpRegionHirarchy.RegionId = regaccount.new_regionid	 
//	
//	create table #tmpNotGoodCity (accountexpertise uniqueidentifier)
//	insert into #tmpNotGoodCity
//	select #tmpJoinning.accId
//	from #tmpJoinning	
//	where
//     (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel = 3) and #tmpJoinning.Regionstate != 1)
//     or (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel != 3) and #tmpJoinning.Regionstate = 2)
//		
//		create table #finalAccs (id uniqueidentifier, price decimal)
//		
//insert into #finalAccs
//SELECT distinct #tmpJoinning.accId, #tmpJoinning.price
//FROM #tmpJoinning
//where
//	#tmpJoinning.accId not in ( select accountexpertise from #tmpNotGoodCity )
//	
//	select @cntSupps = COUNT(*) from #finalAccs
//
//declare @cntLeads int
//
//select @cntLeads = count(*) from
//incidentbase incB with(nolock)
//join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
//join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
//join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
//join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
//join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
//where new_primaryexpertiseid = @peId
//and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
//and isnull(New_IsStoppedManually, 0) = 0
//and rg4.new_regionid = @stateId
//and rg2.new_regionid = @cityId
//
//declare @wentUp bit
//set @wentUp = 0
//if @cntLeads = 0
//begin
//set @wentUp = 1
//select @cntLeads = count(*) from
//incidentbase incB with(nolock)
//join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
//join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
//join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
//join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
//join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
//where new_primaryexpertiseid = @peId
//and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
//and isnull(New_IsStoppedManually, 0) = 0
//and rg4.new_regionid = @stateId
//end
//
//create table #tmpOnlyState (cnt int, one nvarchar(max), two nvarchar(max), three nvarchar(max), four nvarchar(max), five uniqueidentifier, six bit, isLead bit)
//
//insert into #tmpOnlyState
//select top 150 
//@cntSupps as cnt
//, acc.name as '1'
//, acc.telephone1 as '2'
//, new_fulladdress as '3'
//, (select new_primaryexpertiseidname + ';' AS [text()] from new_accountexpertise ae with(nolock) 
//	join new_primaryexpertise pe with(Nolock) on pe.New_primaryexpertiseId = ae.new_primaryexpertiseid
//	where ae.statecode = 0 and ae.new_accountid = acc.accountid and isnull(pe.new_display, 1) = 1
//	For XML PATH(''))
//, acc.accountid
//,
//cast( case when isnull(availableTable.ok, 0) = 1
//AND 
//isnull(new_firstpaymentneeded, 0) = 0 then 1 else 0 end as bit)
//, 0
//from #finalAccs
//join account acc with(nolock) on acc.AccountId = #finalAccs.id
//left join 
//(
//select new_from, new_to, new_accountid, 1 as 'ok' from new_availability av with(nolock) 
//where av.deletionstatecode = 0
//and new_day = datepart(dw,getdate())
//and new_from <= CONVERT(char(8),getdate(),108) 
//AND new_to >= CONVERT(char(8),getdate(),108)
//and new_accountid in (select id from #finalAccs)
//) as availableTable
//on acc.accountid = availableTable.new_accountid
//order by new_status, case new_status when 1 then 100 else new_trialstatusreason end desc, #finalAccs.price desc
//
//if @wentUp = 0
//begin
//insert into #tmpOnlyState
//select top 150 
//@cntLeads as cnt
//, incE.new_webdescription as '1'
//, rg.new_englishname as '2'
//, rg4.new_name as '3'
//, ''
//, '00000000-0000-0000-0000-000000000000'
//, case when new_ordered_providers < 3 and incB.createdon > dateadd(day, -2, getdate()) then 1 else 0 end as 'isNew'
//, 1
//from incidentbase incB with(nolock)
//join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
//join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
//join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
//join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
//join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
//where new_primaryexpertiseid = @peId
//and rg4.new_regionid = @stateId
//and rg2.new_regionid = @cityId
//and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
//and isnull(New_IsStoppedManually, 0) = 0
//order by isNew desc, incB.createdon desc
//end
//else
//begin 
//insert into #tmpOnlyState
//select top 150 
//@cntLeads as cnt
//, incE.new_webdescription as '1'
//, rg.new_englishname as '2'
//, rg4.new_name as '3'
//, ''
//, '00000000-0000-0000-0000-000000000000'
//, case when new_ordered_providers < 3 and incB.createdon > dateadd(day, -2, getdate()) then 1 else 0 end as 'isNew'
//, 1
//from incidentbase incB with(nolock)
//join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
//join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
//join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
//join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
//join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
//where new_primaryexpertiseid = @peId
//and rg4.new_regionid = @stateId
//and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
//and isnull(New_IsStoppedManually, 0) = 0
//order by isNew desc, incB.createdon desc
//end
//
//select * from #tmpOnlyState
//
//drop table #tmpOnlyState
//drop table #accs
//drop table #finalAccs
//drop table #tmpJoinning
//drop table #tmpNotGoodCity
//drop table #tmpRegionHirarchy
//";

//        private const string queryForGetLeadsAndSuppliersWithZipcode =
//            @"
//declare @peId uniqueidentifier
//declare @stateId uniqueidentifier
//declare @cityId uniqueidentifier
//declare @countyId uniqueidentifier
//declare @zipcodeId uniqueidentifier
//
//select @peId = new_primaryexpertiseid from new_primaryexpertise with(nolock)
//where deletionstatecode = 0 and new_name = @headingName
//
//select @cityId = rgCity.new_regionid
//, @countyId = rgCounty.new_regionid 
//, @stateId = rgState.new_regionid
//, @zipcodeId = rgZip.new_regionid
//from new_regionextensionbase rgZip with(nolock)
//join new_regionextensionbase rgCity with(nolock) on rgZip.new_parentregionid = rgCity.new_regionid
//join new_regionextensionbase rgCounty with(nolock) on rgCity.new_parentregionid = rgCounty.new_regionid
//join new_regionextensionbase rgState with(nolock) on rgCounty.new_parentregionid = rgState.new_regionid
//where  rgZip.new_name = @zipcode and rgZip.new_level = 4
//
//declare @cntSupps int
//
//create table #accs (id uniqueidentifier, price decimal)
//
//insert into #accs
//select AccountId, ae.new_incidentprice price
//from account acc with(nolock) 
//join new_accountexpertise ae with(nolock) on ae.new_accountid = acc.accountid and ae.statecode = 0 and new_primaryexpertiseid = @peId
//where acc.deletionstatecode = 0
//and (acc.new_status = 1
//or (acc.new_status = 4 and acc.new_trialstatusreason in (3, 4)))
//and isnull(new_isleadbuyer, 0) = 0
//
//create table #tmpRegionHirarchy ( RegionLevel int, RegionId uniqueidentifier, ParentRegionId uniqueidentifier );
//create index idx_region on #tmpRegionHirarchy(RegionId) ;
//
//WITH RegionHirarchy(RegionLevel,RegionId,ParentRegionId)
//AS
//(
//	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
//	FROM new_region with(nolock)
//	WHERE new_region.new_regionid = @cityId and new_region.deletionstatecode = 0
//	UNION ALL
//	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
//	FROM new_region with(nolock)
//	INNER JOIN RegionHirarchy ON RegionHirarchy.ParentRegionId = new_regionid
//)
//insert into #tmpRegionHirarchy
//	select RegionLevel,  RegionId,  ParentRegionId from RegionHirarchy
//	
//	
//	create table #tmpJoinning ( accId uniqueidentifier, RegionId uniqueidentifier, RegionState int, price decimal)
//	insert into #tmpJoinning
//			select #accs.id , regaccount.new_regionid, regaccount.New_regionstate, #accs.price
//	from #accs
//	INNER JOIN(
//	select a.new_regionid, a.New_regionstate, a.new_accountid, b.deletionstatecode
//	from dbo.New_regionaccountexpertiseExtensionBase a
//	join dbo.New_regionaccountexpertiseBase b
//	on a.New_regionaccountexpertiseId = b.New_regionaccountexpertiseId
//	where b.deletionstatecode = 0)
//	
//	
// regaccount  ON regaccount.new_accountid = #accs.id 
//INNER JOIN #tmpRegionHirarchy ON #tmpRegionHirarchy.RegionId = regaccount.new_regionid	 
//	
//	create table #tmpNotGoodCity (accountexpertise uniqueidentifier)
//	insert into #tmpNotGoodCity
//	select #tmpJoinning.accId
//	from #tmpJoinning	
//	where
//     (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel = 4) and #tmpJoinning.Regionstate != 1)
//     or (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel != 4) and #tmpJoinning.Regionstate = 2)
//		
//		create table #finalAccs (id uniqueidentifier, price decimal)
//		
//insert into #finalAccs
//SELECT distinct #tmpJoinning.accId, #tmpJoinning.price
//FROM #tmpJoinning
//where
//	#tmpJoinning.accId not in ( select accountexpertise from #tmpNotGoodCity )
//	
//	select @cntSupps = COUNT(*) from #finalAccs
//
//
//
//declare @cntLeads int
//
//select @cntLeads = count(*) 
//from incidentextensionbase incE with(nolock)
//join incidentbase incB with(nolock) on incB.incidentid = incE.incidentid
//join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
//where new_primaryexpertiseid = @peId
//and rg.new_parentregionid = @cityId
//and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
//and isnull(New_IsStoppedManually, 0) = 0
//
//declare @wentUp bit
//set @wentUp = 0
//if @cntLeads = 0
//begin
//set @wentUp = 1
//select @cntLeads = count(*) from
//incidentbase incB with(nolock) 
//join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
//join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
//join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
//join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
//join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
//where new_primaryexpertiseid = @peId
//and rg4.new_regionid = @stateId
//and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
//and isnull(New_IsStoppedManually, 0) = 0
//end
//
//create table #tmpOnlyState (cnt int, one nvarchar(max), two nvarchar(max), three nvarchar(max), four nvarchar(max), five uniqueidentifier, six bit, isLead bit)
//
//insert into #tmpOnlyState
//select top 150 
//@cntSupps as cnt
//, acc.name as '1'
//, acc.telephone1 as '2'
//, new_fulladdress as '3'
//, (select new_primaryexpertiseidname + ';' AS [text()] from new_accountexpertise ae with(nolock) 
//	join new_primaryexpertise pe with(Nolock) on pe.New_primaryexpertiseId = ae.new_primaryexpertiseid
//	where ae.statecode = 0 and ae.new_accountid = acc.accountid and isnull(pe.new_display, 1) = 1
//	For XML PATH(''))
//, acc.accountid
//, 
//cast( case when isnull(availableTable.ok, 0) = 1
//AND 
//isnull(new_firstpaymentneeded, 0) = 0 then 1 else 0 end as bit)
//, 0
//from account acc with(nolock) 
//join #finalAccs on #finalAccs.id = acc.AccountId
//left join 
//(
//select new_from, new_to, new_accountid, 1 as 'ok' from new_availability av with(nolock) 
//where av.deletionstatecode = 0
//and new_day = datepart(dw,getdate())
//and new_from <= CONVERT(char(8),getdate(),108) 
//AND new_to >= CONVERT(char(8),getdate(),108)
//and new_accountid in (select id from #finalAccs)
//) as availableTable
//on acc.accountid = availableTable.new_accountid
//order by new_status, case new_status when 1 then 100 else new_trialstatusreason end desc, #finalAccs.price desc
//
//if @wentUp = 0
//begin
//insert into #tmpOnlyState
//select top 150 
//@cntLeads as cnt
//, incE.new_webdescription as '1'
//, rg.new_englishname as '2'
//, rg4.new_name as '3'
//, ''
//, '00000000-0000-0000-0000-000000000000'
//, case when new_ordered_providers < 3 and incB.createdon > dateadd(day, -2, getdate()) then 1 else 0 end as 'isNew'
//, 1
//from incidentbase incB with(nolock)
//join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
//join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
//join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
//join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
//join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
//where new_primaryexpertiseid = @peId
//and rg2.new_regionid = @cityId
//and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
//and isnull(New_IsStoppedManually, 0) = 0
//order by isNew desc, incB.createdon desc
//end
//else
//begin 
//
//insert into #tmpOnlyState
//select top 150 
//@cntLeads as cnt
//, incE.new_webdescription as '1'
//, rg.new_englishname as '2'
//, rg4.new_name as '3'
//, ''
//, '00000000-0000-0000-0000-000000000000'
//, case when new_ordered_providers < 3 and incB.createdon > dateadd(day, -2, getdate()) then 1 else 0 end as 'isNew'
//, 1
//from incidentbase incB with(nolock)
//join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
//join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
//join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
//join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
//join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
//where new_primaryexpertiseid = @peId
//and rg4.new_regionid = @stateId
//and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
//and isnull(New_IsStoppedManually, 0) = 0
//order by isNew desc, incB.createdon desc
//
//end
//
//
//select * from #tmpOnlyState
//
//drop table #tmpOnlyState
//drop table #accs
//drop table #finalAccs
//drop table #tmpJoinning
//drop table #tmpNotGoodCity
//drop table #tmpRegionHirarchy
//";

        private const string queryForGetLeadsForCategoryGroupByName =
            @"
create table #tmpHeads (id uniqueidentifier)

insert into #tmpHeads
select x.new_primaryexpertiseid
                from new_primaryexpertise_categorygroup x with(nolock)
                join new_categorygroup cg with(nolock) on cg.new_categorygroupid = x.new_categorygroupid
                where
                new_name = @groupName

declare @cntLeads int

select @cntLeads = count(*) 
from incidentextensionbase incE with(nolock)
join incidentbase incB with(nolock) on incB.incidentid = incE.incidentid
where new_primaryexpertiseid in
(select * from #tmpHeads)
and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
and isnull(New_IsStoppedManually, 0) = 0

select top 150 
@cntLeads as cnt
, incE.new_webdescription as description
, rg.new_englishname as 'regions'
, rg4.new_name as 'state'
, pe.new_name as 'headingName'
, cast( case when new_ordered_providers < 3 and incB.createdon > dateadd(day, -2, getdate()) then 1 else 0 end as bit) as 'isNew'
from incidentbase incB with(nolock)
join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
join new_region rg with(nolock) on incE.new_regionId = rg.new_regionId
join new_region rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
join new_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
join new_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = incE.new_primaryexpertiseid
where pe.new_primaryexpertiseid in
(
select * from #tmpHeads
)
and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
and isnull(New_IsStoppedManually, 0) = 0
order by isNew desc, incB.createdon desc

drop table #tmpHeads
";

        private const string queryForGetHeadingInGroupByName =
            @"
select pe.new_name
                from new_primaryexpertise_categorygroup x with(nolock)
                join new_categorygroup cg with(nolock) on cg.new_categorygroupid = x.new_categorygroupid
                join new_primaryexpertiseextensionbase pe with(nolock) on pe.new_primaryexpertiseid = x.new_primaryexpertiseid
                where
                cg.new_name = @groupName
";
    }
}
