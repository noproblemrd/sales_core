﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SeoData;

namespace NoProblem.Core.BusinessLogic.SEODataServices
{
    public class SEOServicesProvider : XrmUserBase
    {
        private const string sendToMyMobileTextBody = "{0} {1} via NoProblem {2}";

        public SEOServicesProvider(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public string ShortenSupplierPageUrl(string longUrl, Guid supplierId)
        {
            string shortUrl = null;
            if (supplierId != Guid.Empty)
            {
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                var supplier = dal.Retrieve(supplierId);
                if (supplier != null)
                {
                    if (!string.IsNullOrEmpty(supplier.new_shorturl))
                    {
                        shortUrl = supplier.new_shorturl;
                    }
                    else
                    {
                        Bitly.BitlyDriver driver = new NoProblem.Core.BusinessLogic.Bitly.BitlyDriver();
                        shortUrl = driver.ShortenUrl(longUrl);
                        if (!string.IsNullOrEmpty(shortUrl))
                        {
                            supplier.new_shorturl = shortUrl;
                            dal.Update(supplier);
                            XrmDataContext.SaveChanges();
                        }
                        else
                        {
                            //make sure to return null and not string.Empty.
                            shortUrl = null;
                        }
                    }
                }
            }
            return shortUrl;
        }

        public bool SendToMyMobile(SendToMyMobileRequest request)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = dal.Retrieve(request.SupplierId);
            if (supplier == null)
            {
                throw new ArgumentException("No supplier found with given SupplierId");
            }
            string supplierPhone = supplier.telephone1;
            string supplierName = supplier.name;
            string shortUrl = supplier.new_shorturl;
            if (string.IsNullOrEmpty(shortUrl))
            {
                Bitly.BitlyDriver driver = new NoProblem.Core.BusinessLogic.Bitly.BitlyDriver();
                shortUrl = driver.ShortenUrl(request.Url);
            }
            if (shortUrl == null)
            {
                shortUrl = string.Empty;
            }
            else if (supplier.new_shorturl != shortUrl)
            {
                //save short url
                supplier.new_shorturl = shortUrl;
                dal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
            Notifications notificator = new Notifications(XrmDataContext);
            string body = string.Format(sendToMyMobileTextBody, supplierName, FormatPhoneAmericanWay(supplierPhone), shortUrl);
            return notificator.SendSMSTemplate(body, request.Phone, GlobalConfigurations.DefaultCountry);
        }

        private string FormatPhoneAmericanWay(string phone)
        {
            string tempPhone;
            if (phone.Length == 10)
                tempPhone = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6, 4);
            else
                tempPhone = phone;
            return tempPhone;
        }
    }
}
