﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SEODataServices
{
    public class SEOLastCasesRetriever
    {
        private const string CACHE_PREFIX = "NoProblem.Core.BusinessLogic.CustomerPageDataManager";
        private const string GET_LAST_CASES_CACHE_KEY = "GetLastCases";
        private const string PSIK = ", ";
        private const string INCIDENTID = "incidentid";
        private const string CREATEDON = "createdon";
        private const string DESCRIPTION = "description";
        private const string NEW_PRIMARYEXPERTISEID = "new_primaryexpertiseid";
        private const string STATE = "state";
        private const string CITY = "city";
        private const string ZIPCODE = "zipcode";
        private const string NEW_LONGITUDE = "new_longitude";
        private const string NEW_LATITUDE = "new_latitude";

        public List<DataModel.Case.CustomerPageCaseData> GetLastCases(int howMany)
        {
            List<DataModel.Case.CustomerPageCaseData> lst = DataAccessLayer.CacheManager.Instance.Get<List<DataModel.Case.CustomerPageCaseData>>(CACHE_PREFIX, GET_LAST_CASES_CACHE_KEY, GetLastCasesMethod, 1);
            return lst.Take(howMany).ToList();
        }

        private List<DataModel.Case.CustomerPageCaseData> GetLastCasesMethod()
        {
            List<DataModel.Case.CustomerPageCaseData> retVal = new List<NoProblem.Core.DataModel.Case.CustomerPageCaseData>();
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(null);
            var reader = dal.ExecuteReader(queryForGetLastCases);
            foreach (var row in reader)
            {
                DataModel.Case.CustomerPageCaseData data = new NoProblem.Core.DataModel.Case.CustomerPageCaseData();
                data.CaseId = (Guid)row[INCIDENTID];
                data.CreatedOn = (DateTime)row[CREATEDON];
                data.Description = Convert.ToString(row[DESCRIPTION]);
                data.HeadingId = (Guid)row[NEW_PRIMARYEXPERTISEID];
                string state = Convert.ToString(row[STATE]);
                string city = Convert.ToString(row[CITY]);
                data.RegionName = city + PSIK + state;
                data.ZipCode = Convert.ToString(row[ZIPCODE]);
                data.Longitude = (decimal)row[NEW_LONGITUDE];
                data.Latitude = (decimal)row[NEW_LATITUDE];
                retVal.Add(data);
            }
            return retVal;
        }
        private const string queryForGetLastCases = "[dbo].[Get500LastCases]";

        /*
        private const string queryForGetLastCases =
            @"
select top 500 inc.new_primaryexpertiseid, inc.new_webdescription as description, inc.createdon, inc.incidentid, rg1.new_name as zipcode, rg2.new_name as city, rg4.new_name as state, coo.new_longitude, coo.new_latitude
from incident inc with(nolock)
join New_region rg1 with(nolock) on inc.new_regionid = rg1.New_regionId
join New_region rg2 with(nolock) on rg1.new_parentregionid = rg2.new_regionid
join New_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionid
join New_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionid
join new_coordinate coo with(nolock) on coo.new_regionid = rg1.new_regionid
where 
inc.statuscode != 200013
and inc.statuscode != 200014 
and isnull(inc.new_isstoppedmanually, 0) = 0
and rg1.new_level = 4               
order by inc.createdon desc
";
         * */
    }
}
