﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SeoData;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.SEODataServices
{
    public class SEOSupplierDataRetriever : SEODataRetrieverBase
    {
        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();
        private static Random random = new Random();

        public SEOSupplierDataRetriever(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
        }

        public static void ClearSupplierPageDataCache(Guid supplierId)
        {
            DataAccessLayer.CacheManager.Instance.ClearCache(cachePrefix, supplierId.ToString());
        }

        public SEOSupplierPageData GetSupplierPageData(SEOGetSupplierPageDataRequest request)
        {
            SEOSupplierPageData retVal = DataAccessLayer.CacheManager.Instance.Get<SEOSupplierPageData>(cachePrefix, request.SupplierId.ToString(), GetSupplierDataMethod, DataAccessLayer.CacheManager.ONE_DAY);
            retVal.IsOnDuty = IsOnDuty(request.SupplierId);
            DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            Guid regionId = Guid.Empty;
            Guid parentRegionId = Guid.Empty;
            if (!string.IsNullOrEmpty(request.Zipcode))
            {
                var regionData = regDal.GetRegionMinDataByName(request.Zipcode);
                if (regionData != null)
                {
                    regionId = regionData.Id;
                    parentRegionId = regionData.ParentId;
                }
                retVal.RegionOfLeads = request.Zipcode;
            }
            else if (!string.IsNullOrEmpty(request.City) && !string.IsNullOrEmpty(request.State))
            {
                regionId = regDal.GetRegionIdByCityAndState(request.State, request.City);
                string abbreviation = regDal.GetStateAbbreviation(request.State);
                retVal.RegionOfLeads = request.City + ", " + (string.IsNullOrEmpty(abbreviation) ? request.State : abbreviation);
            }
            else if (!string.IsNullOrEmpty(request.State))
            {
                regionId = regDal.GetRegionIdByName(request.State);
                retVal.RegionOfLeads = request.State;
            }
            else//no regions supplied, get leads from address
            {
                regionId = GetRegionIdFromSupplierData(request.SupplierId);
            }
            if (!retVal.IsPaying)
            {
                retVal.FeaturedSuppliers = GetFeaturedSuppliers(regionId, retVal.ServicesIdsStr, request.SupplierId);
            }
            SEOLeadsForHeading leads = GetLeadsInRegionAndHeadings(parentRegionId != Guid.Empty ? parentRegionId : regionId, retVal.ServicesIdsStr);
            retVal.Leads = leads.Leads;
            retVal.LeadCount = leads.Count;
            if (string.IsNullOrEmpty(retVal.Address))
            {
                retVal.Address = retVal.RegionOfLeads;
            }
            return retVal;
        }

        private Guid GetRegionIdFromSupplierData(Guid supplierId)
        {
            Guid retVal = Guid.Empty;
            DataAccessLayer.AccountRepository accDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = (from acc in accDal.All
                            where acc.accountid == supplierId
                            select new
                            {
                                Latitude = acc.new_latitude,
                                Longitude = acc.new_longitude
                            }).First();
            if (!String.IsNullOrEmpty(supplier.Latitude) && !String.IsNullOrEmpty(supplier.Longitude))
            {
                decimal latitude, longitude;
                decimal.TryParse(supplier.Latitude, out latitude);
                decimal.TryParse(supplier.Longitude, out longitude);
                if (latitude != 0 || longitude != 0)
                {
                    Utils.RegionUtils utils = new Utils.RegionUtils();
                    var areas = utils.GetAreasInRadiusOO(longitude, latitude, 1m);
                    if (areas.Count() > 0)
                    {
                        retVal = areas.First().RegionId;
                    }
                    else
                    {
                        var tenMileAreas = utils.GetAreasInRadiusOO(longitude, latitude, 10m);
                        if (tenMileAreas.Count() > 0)
                        {
                            retVal = tenMileAreas.First().RegionId;
                        }
                    }
                    if (retVal != Guid.Empty)
                    {
                        DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
                        retVal = regDal.GetParentId(retVal);
                    }
                }
            }
            return retVal;
        }

        private SEOSupplierPageData GetSupplierDataMethod(string supplierIdStr)
        {
            SEOSupplierPageData retVal = new SEOSupplierPageData();
            Guid supplierId = new Guid(supplierIdStr);
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
            parameters.Add("@id", supplierId);
            var reader = dal.ExecuteReader(queryForSupplierData, parameters);
            var dbData = reader.FirstOrDefault();
            if (dbData != null)
            {
                var servicesIdsStr = Convert.ToString(dbData["servicesIds"]);
                retVal.ServicesIdsStr = servicesIdsStr;
                var servicesStr = Convert.ToString(dbData["services"]);
                servicesStr = System.Web.HttpUtility.HtmlDecode(servicesStr);// takes care of turning &amp; to & in headings like "air con & heating".
                var splitServices = servicesStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in splitServices)
                {
                    retVal.Services.Add(item);
                }
                retVal.IsPaying = (bool)dbData["isPaying"];
                retVal.Address = Convert.ToString(dbData["new_fulladdress"]);
                retVal.Radius = (dbData["New_radius"] == DBNull.Value) ? 0 : (int)dbData["New_radius"];
                retVal.Phone = Convert.ToString(dbData["telephone1"]);
                retVal.Name = Convert.ToString(dbData["name"]);
                retVal.About = Convert.ToString(dbData["description"]);
                retVal.Website = Convert.ToString(dbData["websiteurl"]);

                retVal.PayWithAMEX = dbData["new_paywithamex"] != DBNull.Value ? (bool)dbData["new_paywithamex"] : false;
                retVal.PayWithVISA = dbData["new_paywithvisa"] != DBNull.Value ? (bool)dbData["new_paywithvisa"] : false;
                retVal.PayWithMASTERCARD = dbData["new_paywithmastercard"] != DBNull.Value ? (bool)dbData["new_paywithmastercard"] : false;
                retVal.PayWithCASH = dbData["new_paywithcash"] != DBNull.Value ? (bool)dbData["new_paywithcash"] : false;
                List<SEOSupplierDaysData> workHours = CalculateWorkHours(dbData, supplierId);
                retVal.WorkingHours = workHours;
                var splitVideos = System.Web.HttpUtility.HtmlDecode(Convert.ToString(dbData["videos"])).Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in splitVideos)
                {
                    retVal.Videos.Add(item);
                }
            }
            return retVal;
        }

        private bool IsOnDuty(Guid supplierId)
        {
            SupplierManager supManager = new SupplierManager(XrmDataContext);
            var statusContainer = supManager.GetSupplierStatus(supplierId);
            return (statusContainer.IsAvailableHours);
        }

        private List<SEOFeaturedSupplierData> GetFeaturedSuppliers(Guid regionId, string servicesIdsStr, Guid supplierId)
        {
            if (string.IsNullOrEmpty(servicesIdsStr) || regionId == Guid.Empty)
                return new List<SEOFeaturedSupplierData>();
            var retVal = DataAccessLayer.CacheManager.Instance.Get<List<SEOFeaturedSupplierData>>(cachePrefix + ".GetFeaturedSuppliers", regionId.ToString() + ";" + servicesIdsStr, GetFeaturedSuppliersMethod, DataAccessLayer.CacheManager.ONE_DAY);
            foreach (var item in retVal)
            {
                if (item.SupplierId == supplierId)
                {
                    retVal.Remove(item);
                    break;
                }
            }
            while (retVal.Count > 3)
            {
                retVal.RemoveAt(retVal.Count - 1);
            }
            return retVal;
        }

        private List<SEOFeaturedSupplierData> GetFeaturedSuppliersMethod(string concat)
        {
            List<SEOFeaturedSupplierData> retVal = new List<SEOFeaturedSupplierData>();
            try
            {
                string[] split = concat.Split(';');
                Guid regionId = new Guid(split[0]);
                List<string> serviceIdsWithQuotes = new List<string>();
                if (split.Length <= 1)
                    return retVal;
                for (int i = 1; i < split.Length; i++)
                {
                    string item = split[i];
                    if (!string.IsNullOrEmpty(item))
                    {
                        serviceIdsWithQuotes.Add("'" + item + "'");
                    }
                }
                if (serviceIdsWithQuotes.Count == 0)
                {
                    return retVal;
                }
                string idsReady = string.Join(",", serviceIdsWithQuotes.ToArray());
                string queryReady = string.Format(queryForFeaturedSuppliers, idsReady);
                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
                var region = regDal.GetRegionMinDataById(regionId);
                DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
                parameters.Add("@level", region.Level);
                parameters.Add("@code", region.Code);
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                var reader = dal.ExecuteReader(queryReady, parameters);
                foreach (var row in reader)
                {
                    SEOFeaturedSupplierData data = new SEOFeaturedSupplierData();
                    data.Address = Convert.ToString(row["address"]);
                    data.SupplierId = (Guid)row["id"];
                    data.Name = Convert.ToString(row["name"]);
                    data.Phone = Convert.ToString(row["phone"]);
                    retVal.Add(data);
                }
            }
            catch (Exception) { }
            return retVal;
        }

        private List<SEOSupplierDaysData> CalculateWorkHours(Dictionary<string, object> dbData, Guid supplierId)
        {
            List<SEOSupplierDaysData> retVal = new List<SEOSupplierDaysData>();

            //do query to retireve work hour units.
            DataAccessLayer.Generic genericDal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            var reader = genericDal.ExecuteReader(queryForWorkHourUnits, parameters);
            if (reader.Count > 0)
            {
                foreach (var row in reader)
                {
                    SEOSupplierDaysData dayData = new SEOSupplierDaysData();
                    bool allDay = (bool)row["New_AllDay"];
                    if (allDay)
                    {
                        dayData.To = 24;
                    }
                    else
                    {
                        dayData.From = (int)row["New_FromHour"];
                        dayData.FromMinute = (int)row["New_FromMinute"];
                        dayData.To = (int)row["New_ToHour"];
                        dayData.ToMInute = (int)row["New_ToMinute"];
                    }
                    AddDayOfWeek(dayData, "New_Monday", row, DayOfWeek.Monday);
                    AddDayOfWeek(dayData, "New_Tuesday", row, DayOfWeek.Tuesday);
                    AddDayOfWeek(dayData, "New_Wednesday", row, DayOfWeek.Wednesday);
                    AddDayOfWeek(dayData, "New_Thursday", row, DayOfWeek.Thursday);
                    AddDayOfWeek(dayData, "New_Friday", row, DayOfWeek.Friday);
                    AddDayOfWeek(dayData, "New_Saturday", row, DayOfWeek.Saturday);
                    AddDayOfWeek(dayData, "New_Sunday", row, DayOfWeek.Sunday);

                    retVal.Add(dayData);
                }
            }
            //if work hour units are empty try to retrieve form legacy fields:
            else
            {
                string sundayFrom = Convert.ToString(dbData["new_sundayavailablefrom"]);
                string sundayTo = Convert.ToString(dbData["new_sundayavailableto"]);
                string mondayFrom = Convert.ToString(dbData["new_mondayavailablefrom"]);
                string mondayTo = Convert.ToString(dbData["new_mondayavailableto"]);
                string tuesdayFrom = Convert.ToString(dbData["new_tuesdayavailablefrom"]);
                string tuesdayTo = Convert.ToString(dbData["new_tuesdayavailableto"]);
                string wednesdayFrom = Convert.ToString(dbData["new_wednesdayavailablefrom"]);
                string wednesdayTo = Convert.ToString(dbData["new_wednesdayavailableto"]);
                string thursdayFrom = Convert.ToString(dbData["new_thursdayavailablefrom"]);
                string thursdayTo = Convert.ToString(dbData["new_thursdayavailableto"]);
                string fridayFrom = Convert.ToString(dbData["new_fridayavailablefrom"]);
                string fridayTo = Convert.ToString(dbData["new_fridayavailableto"]);
                string saturdayFrom = Convert.ToString(dbData["new_saturdayavailablefrom"]);
                string saturdayTo = Convert.ToString(dbData["new_saturdayavailableto"]);

                //create object for every day
                AddDayToWorkDays(retVal, mondayFrom, mondayTo, DayOfWeek.Monday);
                AddDayToWorkDays(retVal, tuesdayFrom, tuesdayTo, DayOfWeek.Tuesday);
                AddDayToWorkDays(retVal, wednesdayFrom, wednesdayTo, DayOfWeek.Wednesday);
                AddDayToWorkDays(retVal, thursdayFrom, thursdayTo, DayOfWeek.Thursday);
                AddDayToWorkDays(retVal, fridayFrom, fridayTo, DayOfWeek.Friday);
                AddDayToWorkDays(retVal, saturdayFrom, saturdayTo, DayOfWeek.Saturday);
                AddDayToWorkDays(retVal, sundayFrom, sundayTo, DayOfWeek.Sunday);
            }
            return retVal;
        }

        private void AddDayOfWeek(SEOSupplierDaysData dayData, string columnName, Dictionary<string, object> dbRow, DayOfWeek dayOfWeek)
        {
            bool worksInDay = (bool)dbRow[columnName];
            if (worksInDay)
            {
                dayData.DaysOfWeek.Add(dayOfWeek);
            }
        }

        private void AddDayToWorkDays(List<SEOSupplierDaysData> lst, string fromStr, string toStr, DayOfWeek dayOfWeek)
        {
            SEOSupplierDaysData lastEntry = null;
            if (lst.Count > 0)
                lastEntry = lst[lst.Count - 1];
            if (string.IsNullOrEmpty(fromStr) || string.IsNullOrEmpty(toStr) || fromStr == "1:00" || toStr == "1:01")
            {
                if (lastEntry != null && lastEntry.IsClosed)
                {
                    lastEntry.DaysOfWeek.Add(dayOfWeek);
                }
                else
                {
                    SEOSupplierDaysData day = new SEOSupplierDaysData();
                    day.IsClosed = true;
                    day.DaysOfWeek.Add(dayOfWeek);
                    lst.Add(day);
                }
            }
            else
            {
                int from = int.Parse(fromStr.Substring(0, 2));
                int to;
                if (toStr.StartsWith("23:59"))
                {
                    to = 24;
                }
                else
                {
                    to = int.Parse(toStr.Substring(0, 2));
                }
                if (lastEntry != null && lastEntry.From == from && lastEntry.To == to)
                {
                    lastEntry.DaysOfWeek.Add(dayOfWeek);
                }
                else
                {
                    SEOSupplierDaysData day = new SEOSupplierDaysData();
                    day.From = from;
                    day.To = to;
                    day.DaysOfWeek.Add(dayOfWeek);
                    lst.Add(day);
                }
            }
        }

        private SEOLeadsForHeading GetLeadsInRegionAndHeadings(Guid regionId, string servicesIds)
        {
            if (string.IsNullOrEmpty(servicesIds))
                return new SEOLeadsForHeading();
            if (regionId == Guid.Empty)
            {
                regionId = GetPopularRegionId();
            }
            SEOLeadsForHeading leads = DataAccessLayer.CacheManager.Instance.Get<SEOLeadsForHeading>(cachePrefix + ".GetLeadsInRegionAndHeadings", regionId.ToString() + ";" + servicesIds, GetLeadsInRegionAndHeadingsMethod, DataAccessLayer.CacheManager.ONE_HOUR * 3);
            while (leads.Count == 0)
            {
                DataAccessLayer.Region dal = new DataAccessLayer.Region(XrmDataContext);
                regionId = dal.GetParentId(regionId);
                if (regionId == Guid.Empty)
                {
                    break;
                }
                leads = DataAccessLayer.CacheManager.Instance.Get<SEOLeadsForHeading>(cachePrefix + ".GetLeadsInRegionAndHeadings", regionId.ToString() + ";" + servicesIds, GetLeadsInRegionAndHeadingsMethod, DataAccessLayer.CacheManager.ONE_HOUR * 3);
            }
            return leads;
        }

        private Guid GetPopularRegionId()
        {
            SEOGeneralDataRetriever dataRetriever = new SEOGeneralDataRetriever(XrmDataContext);
            var popular = dataRetriever.GetPopularCities();
            int index = random.Next(popular.Count);
            return popular.ElementAt(index).RegionId;
        }

        private SEOLeadsForHeading GetLeadsInRegionAndHeadingsMethod(string regionAndServiceConcat)
        {
            SEOLeadsForHeading retVal = new SEOLeadsForHeading();
            try
            {
                string[] argsSplit = regionAndServiceConcat.Split(';');
                Guid regionId = new Guid(argsSplit[0]);
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
                parameters.Add("@regId", regionId);
                if (argsSplit.Length < 2)
                    return retVal;
                StringBuilder builder = new StringBuilder();
                for (int i = 1; i < argsSplit.Length - 1; i++)
                {
                    builder.Append("'").Append(argsSplit[i]).Append("'");
                    if (i < argsSplit.Length - 2)
                    {
                        builder.Append(",");
                    }
                }
                var reader = dal.ExecuteReader(string.Format(queryForLeads, builder.ToString()), parameters);
                if (reader.Count > 0)
                {
                    retVal.Count = (int)reader.First()["cnt"];
                    foreach (var row in reader)
                    {
                        SEOLeadData data = new SEOLeadData();
                        data.Description = Convert.ToString(row["description"]);
                        string state = Convert.ToString(row["state"]);
                        string regions = Convert.ToString(row["regions"]);
                        data.Region = FixRegionsString(regions, state);
                        data.HeadingName = Convert.ToString(row["new_name"]);
                        retVal.Leads.Add(data);
                    }
                }
            }
            catch (Exception)
            {
            }
            return retVal;
        }

        private const string queryForWorkHourUnits =
            @"
select New_workinghoursunitExtensionBase.*
from New_workinghoursunitExtensionBase
join New_workinghoursunitBase on New_workinghoursunitExtensionBase.New_workinghoursunitId = New_workinghoursunitBase.New_workinghoursunitId
where new_accountid = @supplierId
and statecode = 0
";

        private const string queryForFeaturedSuppliers =
            @"
create table #TempMatchPrimaryExpertise
	(New_AccountExpertiseId uniqueidentifier primary key, hasCash bit, accountId uniqueidentifier)
	
	create index idx_nu_na_tempmatch on #TempMatchPrimaryExpertise(accountId)include(New_AccountExpertiseId,hasCash)

/*** check for suppliers that match to @PrimaryExpertiseId ****/
insert into #TempMatchPrimaryExpertise (New_AccountExpertiseId, hasCash, accountId)
SELECT tempAccounts.New_AccountExpertiseId, tempAccounts.hasCash, tempAccounts.new_accountid
FROM 

(SELECT ae.New_AccountExpertiseId
, CASE WHEN ae.new_incidentprice <= Account.new_availablecashbalance THEN 1 ELSE 0 END AS hasCash
, ae.new_accountid, ae.new_incidentprice
FROM Account with (nolock)
INNER JOIN New_AccountExpertise ae with (nolock) ON ae.New_accountId = Account.AccountId AND 
	ae.deletionstatecode = 0 AND ae.statecode = 0 AND ae.new_primaryexpertiseid 
	in ( {0} )
WHERE 	
    --Account.accountid != @ownId
	--and
    Account.telephone1 IS NOT NULL
    and isnull(Account.new_firstpaymentneeded, 0)= 0
    and isnull(Account.new_waitingforinvitation, 0) = 0
	AND Account.new_status = 1
	and isnull(Account.new_isleadbuyer, 0) = 0
	) tempAccounts 

----------------------------------------------------------------------------------------

create table #TempMatchCity
	(New_AccountExpertiseId uniqueidentifier primary key, hasCash bit)

create table #tmpRegionHirarchy ( RegionLevel int, RegionId uniqueidentifier, ParentRegionId uniqueidentifier );
create index idx_region on #tmpRegionHirarchy(RegionId) ;

WITH RegionHirarchy(RegionLevel,RegionId,ParentRegionId)
AS
(
	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
	FROM new_region with(nolock)
	WHERE new_region.new_level = @level AND new_code = @code and new_region.deletionstatecode = 0
	UNION ALL
	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
	FROM new_region with(nolock)
	INNER JOIN RegionHirarchy ON RegionHirarchy.ParentRegionId = new_regionid
)
insert into #tmpRegionHirarchy
	select RegionLevel,  RegionId,  ParentRegionId from RegionHirarchy
	
	create table #tmpJoinning ( AccountExpertiseId uniqueidentifier, RegionId uniqueidentifier, RegionState int, hasCash bit)
	insert into #tmpJoinning
			select #TempMatchPrimaryExpertise.New_AccountExpertiseId , regaccount.new_regionid, regaccount.New_regionstate, #TempMatchPrimaryExpertise.hasCash
	from #TempMatchPrimaryExpertise
	INNER JOIN(
	select a.new_regionid, a.New_regionstate, a.new_accountid, b.deletionstatecode
	from dbo.New_regionaccountexpertiseExtensionBase a
	join dbo.New_regionaccountexpertiseBase b
	on a.New_regionaccountexpertiseId = b.New_regionaccountexpertiseId
	where b.deletionstatecode = 0)
		
 
 regaccount  ON regaccount.new_accountid = #TempMatchPrimaryExpertise.accountId 
INNER JOIN #tmpRegionHirarchy ON #tmpRegionHirarchy.RegionId = regaccount.new_regionid	 
	
	create table #tmpNotGoodCity (accountexpertise uniqueidentifier)
	insert into #tmpNotGoodCity
	select #tmpJoinning.AccountExpertiseId
	from #tmpJoinning	
	where
     (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel = @level) and #tmpJoinning.Regionstate != 1)
     or (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel != @level) and #tmpJoinning.Regionstate = 2)
		
insert into #TempMatchCity (New_AccountExpertiseId, hasCash)
SELECT distinct #tmpJoinning.AccountExpertiseId, #tmpJoinning.hasCash
FROM #tmpJoinning
where
	#tmpJoinning.AccountexpertiseId not in ( select accountexpertise from #tmpNotGoodCity )

create table #final (name nvarchar(max), id uniqueidentifier, address nvarchar(max), phone nvarchar(max))
insert into #final
select acc.name, acc.accountid, acc.new_fulladdress, acc.telephone1
from #TempMatchCity tmp
join new_accountexpertiseextensionbase ae with(Nolock) on ae.new_accountexpertiseid = tmp.new_accountexpertiseid
join account acc with(nolock) on ae.new_accountid = acc.accountid
order by ae.new_incidentprice desc

select distinct top 4 * from #final

drop table #final

drop table #TempMatchPrimaryExpertise
drop table #TempMatchCity
drop table #tmpRegionHirarchy
drop table #tmpJoinning
drop table #tmpNotGoodCity
";

        private const string queryForLeads =
            @"
declare @cntLeads int

select @cntLeads = count(*) 
from incidentbase incB with(nolock)
join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
join New_regionExtensionBase rg with(nolock) on incE.new_regionId = rg.new_regionId
join New_regionExtensionBase rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
join New_regionExtensionBase rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
join New_regionExtensionBase rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
where new_primaryexpertiseid in
(
{0}
)
and (rg4.new_regionid = @regId
or rg3.new_regionid = @regId
or rg2.new_regionid = @regId
or rg.new_regionid = @regId)


select top 150 
@cntLeads as cnt
, incE.new_webdescription as description
, rg.new_englishname as 'regions'
, rg4.new_name as 'state'
, pe.new_name
from incidentbase incB with(nolock)
join incidentextensionbase incE with(nolock) on incB.incidentid = incE.incidentid
join New_regionExtensionBase rg with(nolock) on incE.new_regionId = rg.new_regionId
join New_regionExtensionBase rg2 with(nolock) on rg.new_parentregionid = rg2.new_regionId
join New_regionExtensionBase rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionId
join New_regionExtensionBase rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionId
join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = incE.new_primaryexpertiseid
where pe.new_primaryexpertiseid in
(
{0}
)
and (rg4.new_regionid = @regId
or rg3.new_regionid = @regId
or rg2.new_regionid = @regId
or rg.new_regionid = @regId)
and (incB.statuscode != 200013 and incB.statuscode != 200014 and incB.statuscode != 200000)
order by incB.createdon desc
";

        private const string queryForSupplierData =
            @"
select 
new_sundayavailablefrom
,new_sundayavailableto
,new_mondayavailablefrom
,new_mondayavailableto
,new_tuesdayavailablefrom
,new_tuesdayavailableto
,new_wednesdayavailablefrom
,new_wednesdayavailableto
,new_thursdayavailablefrom
,new_thursdayavailableto
,new_fridayavailablefrom
,new_fridayavailableto
,new_saturdayavailablefrom
,new_saturdayavailableto
,new_fulladdress
,New_radius
,name
,description
,websiteurl
,new_paywithvisa
,new_paywithmastercard
,new_paywithcash
,new_paywithamex
, (select new_uri + ';' AS [text()] from new_accountmedia with(nolock)
    where deletionstatecode = 0
    and new_accountid = accountid
    and new_type = 'VIDEO'
    For XML PATH('')) as videos
, (select new_primaryexpertiseidname + ';' AS [text()] from new_accountexpertise ae with(nolock) 
    join new_primaryexpertise pe on pe.new_primaryexpertiseid = ae.new_primaryexpertiseid
	where ae.statecode = 0 and ae.new_accountid = accountid
    and isnull(pe.new_display, 1) = 1
	For XML PATH('')) as services
, (select cast(ae.new_primaryexpertiseid as nvarchar(max)) + ';' AS [text()] from new_accountexpertise ae with(nolock) 
    join new_primaryexpertise pe on pe.new_primaryexpertiseid = ae.new_primaryexpertiseid
	where ae.statecode = 0 and ae.new_accountid = accountid
    and isnull(pe.new_display, 1) = 1
    order by ae.new_primaryexpertiseid
	For XML PATH('')) as servicesIds
,telephone1
,
cast(isnull((
select top 1 1 from 
new_supplierpricing sp 
where sp.new_accountid = @id and new_voucherid is null
), 0) as bit) as 'isPaying'
from account
where accountid = @id
";
    }
}
