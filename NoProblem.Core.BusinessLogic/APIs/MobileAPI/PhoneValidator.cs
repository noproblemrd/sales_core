﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.MobileAPI
{
    public class PhoneValidator
    {
        public static bool SendPhoneValidationSms(string phone, string code)
        {
            Notifications notifier = new Notifications(null);
            return notifier.SendSMSTemplate(code, phone, GlobalConfigurations.DefaultCountry);
        }
    }
}
