﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.MobileAPI
{
    public class FeedbackManager : XrmUserBase
    {
        public FeedbackManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private const string SLACK_TEXT = "new mobile app feedback: header={0}. comment={1}";

        public void CreateFeedback(DataModel.APIs.MobileApi.Feedback feedback)
        {
            DataModel.Xrm.new_mobileappfeedback entity = DataModel.EntityFactories.MobileAppFeddbackFactory.Build(feedback.SupplierId, feedback.Comment, feedback.Header);
            DataAccessLayer.APIs.MobileApi.MobileAppFeedback dal = new DataAccessLayer.APIs.MobileApi.MobileAppFeedback(XrmDataContext);
            dal.Create(entity);
            XrmDataContext.SaveChanges();
            Slack.SlackNotifier notifier = new Slack.SlackNotifier(String.Format(SLACK_TEXT, feedback.Header, feedback.Comment));
            notifier.Start();
        }
    }
}
