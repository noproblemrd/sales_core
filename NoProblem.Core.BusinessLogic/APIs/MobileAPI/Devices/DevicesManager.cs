﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.APIs.MobileAPI.Devices
{
    public class DevicesManager : XrmUserBase
    {
        DataAccessLayer.MobileDevice dalMobileDevice;

        public DevicesManager(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            dalMobileDevice = new DataAccessLayer.MobileDevice(XrmDataContext);
        }

        private class CurrentDeviceData
        {
            public Guid MobileDeviceId { get; set; }
            public Guid SupplierId { get; set; }
            public Guid CustomerId { get; set; }
            public string Token { get; set; }
            public bool IsActive { get; set; }
            public bool IsAuthorized { get; set; }
        }

        public void Logout(Guid supplierId)
        {
            DeactivateOtherDevices(supplierId, Guid.Empty, Guid.NewGuid());
        }

        public void CustomerLogout(Guid customerId, Guid deviceId)
        {
            DeactivateOtherDevices(Guid.Empty, customerId, deviceId);
        }




        public string DeviceLogin(
         //   Guid supplierId,
            Guid customerId,
            string deviceName,
            string deviceUId,
            string deviceOS,
            string deviceOSVersion,
            string npAppVersion,
            bool isDebug)
        {
            if (deviceUId == string.Empty)
                deviceUId = null;
            Guid supplierId = GetSupplierId(customerId);
            var currentDeviceData = (from md in dalMobileDevice.All
                                     where
                                     md.new_uid == deviceUId
                                     && md.new_os == deviceOS.ToUpper()
                                     && md.new_contactid == customerId
                                     && (md.new_isdebug ?? false) == isDebug
                                     select
                                     new CurrentDeviceData
                                     {
                                         SupplierId = md.new_accountid.HasValue ? md.new_accountid.Value : Guid.Empty,
                                         CustomerId = md.new_contactid.HasValue ? md.new_contactid.Value : Guid.Empty,
                                         Token = md.new_token,
                                         IsActive = md.new_isactive.Value,
                                         IsAuthorized = md.new_isauthorized.Value,
                                         MobileDeviceId = md.new_mobiledeviceid
                                     }
                        ).FirstOrDefault();
            string token;
            if (currentDeviceData == null)
            {
                token = CreateNewDevice(supplierId, customerId, deviceName, deviceUId, deviceOS, deviceOSVersion, npAppVersion, isDebug);
            }
            else
            {
                token = UpdateDeviceData(currentDeviceData, supplierId, customerId, deviceName, deviceOSVersion, npAppVersion, isDebug);
            }
            return token;
        }
        private Guid GetSupplierId(Guid customerId)
        {
            Guid supplierId;
            string command = "SELECT New_AccountId FROM [dbo].[ContactExtensionBase] WHERE ContactId = @ContactId";
            using(SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@ContactId", customerId);
                    object o = cmd.ExecuteScalar();
                    supplierId = (o == null || o == DBNull.Value) ? Guid.Empty : (Guid)o;
                    conn.Close();
                }
            }
            return supplierId;
        }

        private string UpdateDeviceData(CurrentDeviceData currentDeviceData, Guid supplierId, Guid customerId, string deviceName, string deviceOSVersion, 
            string npAppVersion, bool isDebug)
        {
            DataModel.Xrm.new_mobiledevice device = new DataModel.Xrm.new_mobiledevice(XrmDataContext);
            device.new_mobiledeviceid = currentDeviceData.MobileDeviceId;
            device.new_isactive = true;
            device.new_isauthorized = true;
            device.new_isdebug = isDebug;
            string token = currentDeviceData.Token;
            if (supplierId != Guid.Empty && supplierId != currentDeviceData.SupplierId)
            {
                device.new_accountid = supplierId;
            }
            if (customerId != Guid.Empty && customerId != currentDeviceData.CustomerId)
            {
                device.new_contactid = customerId;
            }
            if (!String.IsNullOrEmpty(deviceName))
            {
                device.new_name = deviceName;
            }
            if (!String.IsNullOrEmpty(deviceOSVersion))
            {
                device.new_osversion = deviceOSVersion;
            }
            if (!String.IsNullOrEmpty(npAppVersion))
            {
                device.new_npappversion = npAppVersion;
            }
            dalMobileDevice.Update(device);
            XrmDataContext.SaveChanges();
            DeactivateOtherDevices(supplierId, customerId, device.new_mobiledeviceid);
            return token;
        }

        private string CreateNewDevice(Guid supplierId, Guid customerId, string deviceName, string deviceUId, string deviceOS, 
            string deviceOSVersion, string npAppVersion, bool isDebug)
        {
            DataModel.Xrm.new_mobiledevice device = DataModel.EntityFactories.MobileDeviceFactory.Create(supplierId, customerId, deviceName, 
                deviceUId, deviceOS, deviceOSVersion, npAppVersion, isDebug);
            dalMobileDevice.Create(device);
            XrmDataContext.SaveChanges();
            DeactivateOtherDevices(supplierId, customerId, device.new_mobiledeviceid);
            return device.new_token;
        }

        private void DeactivateOtherDevices(Guid supplierId, Guid customerId, Guid deviceId)
        {
            List<Guid> devicesIds = new List<Guid>();
            if (supplierId != Guid.Empty)
            {
                var allDevices = from md in dalMobileDevice.All
                                 where md.new_accountid == supplierId
                                 select new { DeviceId = md.new_mobiledeviceid };
                foreach (var item in allDevices)
                {
                    devicesIds.Add(item.DeviceId);
                }
            }
            if (customerId != Guid.Empty)
            {
                var allDevices = from md in dalMobileDevice.All
                                 where md.new_contactid == customerId
                                 select new { DeviceId = md.new_mobiledeviceid };
                foreach (var item in allDevices)
                {
                    devicesIds.Add(item.DeviceId);
                }
            }
            foreach (var id in devicesIds)
            {
                if (id != deviceId)
                {
                    DataModel.Xrm.new_mobiledevice d = new DataModel.Xrm.new_mobiledevice(XrmDataContext);
                    d.new_mobiledeviceid = id;
                    d.new_isactive = false;
                    dalMobileDevice.Update(d);
                }
            }
            XrmDataContext.SaveChanges();
        }
    }
}
