﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.MobileAPI.Devices
{
    public class AuthTokensManager
    {
        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();

        public bool VerifyToken(string token, Guid supplierId, bool isSupplier)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@id", supplierId);
            parameters.Add("@token", token);
            object scalar = dal.ExecuteScalar(String.Format(verifyTokenQuery, isSupplier ? "account" : "contact"), parameters);
            return scalar != null;
        }

        private const string verifyTokenQuery =
            @"
select top 1 1
from new_mobiledevice
where
new_{0}id = @id
and new_token = @token
and new_isauthorized = 1
";
    }
}
