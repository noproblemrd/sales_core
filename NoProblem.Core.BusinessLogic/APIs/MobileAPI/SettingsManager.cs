﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.MobileAPI
{
    public class SettingsManager : XrmUserBase
    {
        DataAccessLayer.AccountRepository dal;

        public SettingsManager()
            : base(null)
        {
            dal = new DataAccessLayer.AccountRepository(XrmDataContext);
        }

        public void SetSettings(Guid supplierId, DataModel.APIs.MobileApi.Settings settings)
        {
            DataModel.Xrm.account account = new DataModel.Xrm.account(XrmDataContext);
            account.accountid = supplierId;
            account.new_recordcalls = settings.RecordMyCalls;
            account.new_stop_mobile_notifications_until = settings.StopNotificationsUntil;
            dal.Update(account);
            XrmDataContext.SaveChanges();
        }

        public DataModel.APIs.MobileApi.Settings GetSettings(Guid supplierId)
        {
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new { StopNotficationsUntil = acc.new_stop_mobile_notifications_until, RecordCalls = acc.new_recordcalls };
            var foundData = query.First();
            var retVal = new DataModel.APIs.MobileApi.Settings();
            retVal.RecordMyCalls = foundData.RecordCalls.HasValue ? foundData.RecordCalls.Value : false;
            retVal.StopNotificationsUntil = foundData.StopNotficationsUntil;
            retVal.CreditCardLastFourDigits = GetCCLastFourDigits(supplierId);
            return retVal;
        }

        private string GetCCLastFourDigits(Guid supplierId)
        {
            string ccToken;
            string cvv2;
            string ccOwnerId;
            string ccOwnerName;
            string last4digits;
            DateTime tokenDate;
            string chargingCompanyAccountNumber;
            string chargingCompanyContractId;
            string ccType;
            string ccExpDate;
            string billingAddress;
            DataAccessLayer.SupplierPricing spDal = new DataAccessLayer.SupplierPricing(XrmDataContext);
            bool ccFound = spDal.FindCreditCardInfo(supplierId, out ccToken, out cvv2, out ccOwnerId, out ccOwnerName, out last4digits, out tokenDate, out chargingCompanyContractId, out chargingCompanyAccountNumber, out ccType, out ccExpDate, out billingAddress);
            return last4digits;
        }
    }
}
