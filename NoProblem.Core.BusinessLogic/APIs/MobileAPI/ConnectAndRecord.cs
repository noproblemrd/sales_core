﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call.ConnectAndRecord;

namespace NoProblem.Core.BusinessLogic.APIs.MobileAPI
{
    public class ConnectAndRecord : XrmUserBase
    {
        private Guid incidentAccountId;
        private Guid supplierId;

        public ConnectAndRecord(Guid incidentAccountId, Guid supplierId)
            : base(null)
        {
            this.incidentAccountId = incidentAccountId;
            this.supplierId = supplierId;
        }

        public bool InitiateCall()
        {
            DataAccessLayer.IncidentAccount dal = new DataAccessLayer.IncidentAccount(XrmDataContext);
            var ia = (from x in dal.All
                      where x.new_incidentaccountid == incidentAccountId
                      && x.new_accountid == supplierId
                      && x.new_tocharge == true
                      select new { IncidentId = x.new_incidentid.Value }).FirstOrDefault();
            if (ia == null)
                return false;
            string advertiserPhone = GetAdvertiserPhone();
            string customerPhone = GetCustomerPhone(ia.IncidentId);
            var callManager = new ConnectAndRecordCallManager(incidentAccountId, advertiserPhone, customerPhone);
            callManager.OnCallEndedWithRecording += callManager_OnCallEndedWithRecording;
            bool callStarted = callManager.StartCall();
            return callStarted;
        }

        void callManager_OnCallEndedWithRecording(string recordingUrl, int duration)
        {
            DataAccessLayer.CallRecording dal = new DataAccessLayer.CallRecording(null);
            DataModel.Xrm.new_callrecording recording = new DataModel.Xrm.new_callrecording(dal.XrmDataContext);
            recording.new_url = recordingUrl;
            recording.new_incidentaccountid = incidentAccountId;
            recording.new_duration = duration;
            dal.Create(recording);
            dal.XrmDataContext.SaveChanges();
        }

        private string GetCustomerPhone(Guid incidentId)
        {
            DataAccessLayer.Incident dal = new DataAccessLayer.Incident(XrmDataContext);
            var inc = (from x in dal.All
                       where x.incidentid == incidentId
                       select new { Phone = x.new_telephone1 }).FirstOrDefault();
            return inc.Phone;
        }

        private string GetAdvertiserPhone()
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = (from x in dal.All
                            where x.accountid == supplierId
                            select new { Phone = x.telephone1 }).FirstOrDefault();
            return supplier.Phone;
        }
    }
}
