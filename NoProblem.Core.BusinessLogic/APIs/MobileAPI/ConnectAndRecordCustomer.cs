﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.APIs.MobileAPI
{
    public class ConnectAndRecordCustomer : XrmUserBase
    {
        private Guid incidentAccountId;
        private Guid CustomerId;

        public ConnectAndRecordCustomer(Guid incidentAccountId, Guid CustomerId)
            : base(null)
        {
            this.incidentAccountId = incidentAccountId;
            this.CustomerId = CustomerId;
        }

        public bool InitiateCall()
        {
            string customerPhone;
            if (!EnableCallRecordAndGetPhone(CustomerId, out customerPhone) || string.IsNullOrEmpty(customerPhone))
                return false;
        //     = GetCustomerPhone(CustomerId);
            string command = @"SELECT ic.new_incidentid, acc.Telephone1
FROM dbo.New_incidentaccountExtensionBase ic WITH(NOLOCK)
	INNER JOIN dbo.Account acc WITH(NOLOCK) on acc.AccountId = ic.new_AccountId
WHERE New_incidentaccountId =  @IncidentaccountId";
            Guid IncidentId = Guid.Empty;
            string advertiserPhone = null;
            using(SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@IncidentaccountId", incidentAccountId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if(reader.Read())
                    {
                        IncidentId = (Guid)reader["new_incidentid"];
                        advertiserPhone = reader["Telephone1"] == DBNull.Value ? null : (string)reader["Telephone1"];
                    }
                    conn.Close();
                }
            }
            if (IncidentId == Guid.Empty || string.IsNullOrEmpty(advertiserPhone))
                return false;
            
            FoneApiBL.Handlers.Click2Call.ConnectAndRecord.ConnectAndRecordCallManagerConsumer callManager =
                new FoneApiBL.Handlers.Click2Call.ConnectAndRecord.ConnectAndRecordCallManagerConsumer(incidentAccountId, advertiserPhone, customerPhone);
            callManager.OnCallEndedWithRecording += callManager_OnCallEndedWithRecording;
            bool callStarted = callManager.StartCall();
            return callStarted;
        }

        void callManager_OnCallEndedWithRecording(string recordingUrl, int duration)
        {
            DataAccessLayer.CallRecording dal = new DataAccessLayer.CallRecording(null);
            DataModel.Xrm.new_callrecording recording = new DataModel.Xrm.new_callrecording(dal.XrmDataContext);
            recording.new_url = recordingUrl;
            recording.new_incidentaccountid = incidentAccountId;
            recording.new_duration = duration;
            dal.Create(recording);
            dal.XrmDataContext.SaveChanges();
        }

        private bool EnableCallRecordAndGetPhone(Guid CustomerId, out string CustomerPhone)
        {
            /*
            DataAccessLayer.Incident dal = new DataAccessLayer.Incident(XrmDataContext);
            var inc = (from x in dal.All
                       where x.incidentid == incidentId
                       select new { Phone = x.new_telephone1 }).FirstOrDefault();
            return inc.Phone;
             * */
            CustomerPhone = null;
            bool _enable = true;
            string command = @"SELECT MobilePhone, new_disablecallrexording
FROM dbo.Contact WITH(NOLOCK)
WHERE ContactId = @ContactId";
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@ContactId", CustomerId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        CustomerPhone = reader["MobilePhone"] == DBNull.Value ? string.Empty : (string)reader["MobilePhone"];
                        _enable = reader["new_disablecallrexording"] == DBNull.Value ? true : !(bool)reader["new_disablecallrexording"];
                    }
                    conn.Close();
                }
            }
            return _enable;
        }
        /*
        private string GetAdvertiserPhone()
        {
            DataAccessLayer.Account dal = new DataAccessLayer.Account(XrmDataContext);
            var supplier = (from x in dal.All
                            where x.accountid == supplierId
                            select new { Phone = x.telephone1 }).FirstOrDefault();
            return supplier.Phone;
        }
         * */
    
    }
}
