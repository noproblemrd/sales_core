﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi
{
    public class SellersApiHelper
    {
        public static bool VerifySellerId(string sellerId)
        {
            if (String.IsNullOrEmpty(sellerId))
                return false;
            Guid originId;
            try
            {
                originId = new Guid(sellerId);
            }
            catch
            {
                return false;
            }
            DataAccessLayer.Origin dal = new DataAccessLayer.Origin(null);
            var query = from ori in dal.All
                        where ori.new_isapiseller == true
                        && ori.new_originid == originId
                        select ori.new_originid;
            return query.FirstOrDefault() != Guid.Empty;
        }
    }
}
