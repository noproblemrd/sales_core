﻿using Effect.Crm.Logs;
using NoProblem.Core.DataModel.APIs.SellersApi.PingPost;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPost
{
    public class PingManager : XrmUserBase
    {
        private static DataAccessLayer.PingPostTransaction pingPostDal = new DataAccessLayer.PingPostTransaction();
        private DataAccessLayer.PrimaryExpertise categoryDal;
        private const decimal DEFAULT_PERCENTAGE_FOR_SELLER = 60;
        private static DataAccessLayer.Generic genericDal = new DataAccessLayer.Generic();
        private decimal minimalProfit;
        private const decimal MINIMAL_PROFIT_DEFAULT = 0.01m;
        private const string DUPLICATE_STATUS = "duplicate";

        private const string getSupplierForPingPostApi_query =
           @"dbo.GetSupplierForPingPostApi";

        public PingManager()
            : base(null)
        {
            categoryDal = new DataAccessLayer.PrimaryExpertise(XrmDataContext);
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string minimalProfitStr = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PING_POST_API_STATIC_PRICING_MINIMAL_PROFIT);
            if (!Decimal.TryParse(minimalProfitStr, out minimalProfit))
            {
                minimalProfit = MINIMAL_PROFIT_DEFAULT;
            }
            if (minimalProfit <= 0)
            {
                minimalProfit = MINIMAL_PROFIT_DEFAULT;
            }
        }

        public DataModel.APIs.SellersApi.PingPost.PingResponse Ping(DataModel.APIs.SellersApi.PingPost.PingRequest request)
        {
            var sellerData = GetSellerData(request.SellerId);
            bool isDuplicate = CheckDuplicate(request);
            DataModel.APIs.SellersApi.PingPost.PingPostTransaction entry = CreateTransactionEntry(request, sellerData, isDuplicate);
            if (isDuplicate)
            {
                return new PingResponse()
                {
                    Interested = false,
                    NpPingId = "duplicate_" + entry._id.ToString()
                };
            }
            bool interested;
            decimal priceForSeller;
            if (entry.Test)
            {
                interested = true;
                priceForSeller = sellerData.IsStaticPricing ? sellerData.StaticPrice : 10;
            }
            else
            {
                Guid categoryId = categoryDal.GetPrimaryExpertiseIdByCode(request.CategoryCode);
                List<BuyerData> buyers = SearchForBuyers(request.SellerId, categoryId, request.Zip);
                List<Runnable> pingThreads = new List<Runnable>();
                foreach (var buyerData in buyers)
                {
                    Runnable thread = PingBuyer(request.Zip, categoryId, entry, buyerData);
                    pingThreads.Add(thread);
                }
                foreach (var thread in pingThreads)
                {
                    thread.Join();
                }
                if (entry.IsStaticPricing)
                {
                    interested = CheckInterestForStaticPricing(entry);
                    priceForSeller = interested ? entry.Price : 0;
                }
                else
                {
                    interested = entry.Buyers.Count > 0;
                    priceForSeller = CalculatePriceForSellerInDynamicPricing(entry, interested);
                }
            }
            entry.Price = priceForSeller;
            if (interested && entry.Price <= 0)
                interested = false;
            UpdateTransactionToDB(interested, request, entry);
            var response = BuildResponse(priceForSeller, interested, entry._id);
            return response;
        }

        private bool CheckDuplicate(PingRequest request)
        {
            var found = pingPostDal.FindOne(
                MongoDB.Driver.Builders.Query<PingPostTransaction>.Where(x => x.CategoryCode == request.CategoryCode && x.Zip == request.Zip && x.PingOn > DateTime.Now.AddMinutes(-1))
            );
            return found != null;
        }

        private bool CheckInterestForStaticPricing(PingPostTransaction entry)
        {
            bool interested = entry.Buyers.Exists(x => entry.Price + minimalProfit <= x.Price);
            return interested;
        }

        private static DataModel.APIs.SellersApi.PingPost.PingPostTransaction CreateTransactionEntry(DataModel.APIs.SellersApi.PingPost.PingRequest request, SellerData sellerData, bool isDuplicate)
        {
            DataModel.APIs.SellersApi.PingPost.PingPostTransaction entry = new DataModel.APIs.SellersApi.PingPost.PingPostTransaction();
            entry.CategoryCode = request.CategoryCode;
            entry.LeadId = request.LeadId;
            entry.OriginId = request.SellerId.ToString();
            entry.IsStaticPricing = sellerData.IsStaticPricing;
            if (sellerData.IsStaticPricing)
                entry.Price = sellerData.StaticPrice;
            else
                entry.RevenueSharePercentage = sellerData.RevenueSharePercentage;
            entry.Zip = request.Zip;
            entry.Test = request.Test > 0;
            entry.PingData.IP = request.IP;
            entry.PingData.Description = request.Description;
            entry.IsDuplicate = isDuplicate;
            if (isDuplicate)
            {
                entry.Status = DUPLICATE_STATUS;
            }
            pingPostDal.Save(entry);
            return entry;
        }

        private DataModel.APIs.SellersApi.PingPost.SellerData GetSellerData(Guid originId)
        {
            DataModel.APIs.SellersApi.PingPost.SellerData retVal = new DataModel.APIs.SellersApi.PingPost.SellerData();
            DataAccessLayer.Origin originDal = new DataAccessLayer.Origin(XrmDataContext);
            var origin = (from or in originDal.All
                          where or.new_originid == originId
                          select new { RevShare = or.new_revenueshare, StaticPrice = or.new_costperrequest }).First();
            if (origin.RevShare.HasValue && origin.RevShare.Value > 0)
            {
                retVal.RevenueSharePercentage = origin.RevShare.Value;
                retVal.IsStaticPricing = false;
            }
            else if (origin.StaticPrice.HasValue && origin.StaticPrice.Value > 0)
            {
                retVal.StaticPrice = origin.StaticPrice.Value;
                retVal.IsStaticPricing = true;
            }
            else
            {
                retVal.RevenueSharePercentage = DEFAULT_PERCENTAGE_FOR_SELLER;
                retVal.IsStaticPricing = false;
            }
            return retVal;
        }

        private decimal CalculatePriceForSellerInDynamicPricing(DataModel.APIs.SellersApi.PingPost.PingPostTransaction entry, bool interested)
        {
            decimal priceForSeller = 0;
            if (interested)
            {
                decimal bestRankPrice = entry.Buyers.Where(x => x.Price > 0).OrderByDescending(x => x.Rank).First().Price;
                priceForSeller = bestRankPrice * entry.RevenueSharePercentage / 100;
            }
            return priceForSeller;
        }

        private void UpdateTransactionToDB(bool interested, DataModel.APIs.SellersApi.PingPost.PingRequest request, DataModel.APIs.SellersApi.PingPost.PingPostTransaction entry)
        {
            if (interested)
            {
                entry.Status = DataModel.APIs.SellersApi.PingPost.PingPostTransaction.StatusOptions.PING_ACCEPTED;
            }
            else
            {
                entry.Status = DataModel.APIs.SellersApi.PingPost.PingPostTransaction.StatusOptions.PING_REJECTED;
            }
            var transactionId = pingPostDal.Save(entry);
        }

        private static DataModel.APIs.SellersApi.PingPost.PingResponse BuildResponse(decimal priceForSeller, bool interested, MongoDB.Bson.ObjectId transactionId)
        {
            var response = new DataModel.APIs.SellersApi.PingPost.PingResponse();
            response.Price = priceForSeller;
            response.Interested = interested;
            response.NpPingId = transactionId.ToString();
            return response;
        }

        private PingBuyerThread PingBuyer(string zip, Guid categoryId, DataModel.APIs.SellersApi.PingPost.PingPostTransaction entry, BuyerData buyerData)
        {
            PingBuyerThread tr = new PingBuyerThread(zip, categoryId, entry, buyerData);
            tr.Start();
            return tr;
        }

        private List<BuyerData> SearchForBuyers(Guid originId, Guid categoryId, string zip)
        {
            var irrelevantBuyers = GetIrrelevantBuyers(originId);
            DataAccessLayer.Generic.SqlParametersList parameters = PrepareSqlParameters(categoryId, zip, irrelevantBuyers);
            DataAccessLayer.Reader reader = genericDal.ExecuteReader(getSupplierForPingPostApi_query, parameters, System.Data.CommandType.StoredProcedure);
            var retVal = new List<BuyerData>();
            foreach (var row in reader)
            {
                var buyerData = new BuyerData();
                buyerData.DefaultPrice = (decimal)row["price"];
                buyerData.ExternalCode = Convert.ToString(row["externalCode"]);
                buyerData.Id = (Guid)row["supplierId"];
                buyerData.LeadBuyerName = Convert.ToString(row["leadBuyerName"]);
                buyerData.BillingRate = (decimal)row["billing_rate"];
                retVal.Add(buyerData);
            }
            return retVal;
        }

        private DataAccessLayer.Generic.SqlParametersList PrepareSqlParameters(Guid categoryId, string zip, IEnumerable<Guid> irrelevantBuyers)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@PrimaryExpertiseId", categoryId);
            parameters.Add("@zip", zip);
            DataTable table = new DataTable("IdsTable");
            table.Columns.Add("id", typeof(Guid));
            if (irrelevantBuyers != null && irrelevantBuyers.Count() > 0)
            {
                foreach (var id in irrelevantBuyers)
                {
                    table.Rows.Add(id);
                }
            }
            parameters.Add("@irrelevantBuyers", table);
            return parameters;
        }

        private IEnumerable<Guid> GetIrrelevantBuyers(Guid originId)
        {
            DataAccessLayer.Origin originDal = new DataAccessLayer.Origin(XrmDataContext);
            return originDal.GetBuyersOfThisSeller(originId);
        }

        internal class BuyerData
        {
            public Guid Id { get; set; }
            public decimal DefaultPrice { get; set; }
            public string ExternalCode { get; set; }
            public string LeadBuyerName { get; set; }
            public decimal BillingRate { get; set; }
        }
    }
}
