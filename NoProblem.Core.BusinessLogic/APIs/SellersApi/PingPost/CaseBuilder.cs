﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPost
{
    internal class CaseBuilder : Runnable
    {
        private DataModel.APIs.SellersApi.PingPost.PingPostTransaction transaction;
        private static DataAccessLayer.PrimaryExpertise categoryDal = new DataAccessLayer.PrimaryExpertise(null);
        private static DataAccessLayer.Region regionDal = new DataAccessLayer.Region(null);
        private ServiceRequest.AffiliatePaymentResolver affiliatePaymentResolver;
        private DataAccessLayer.Incident incidentDal;
        private DataAccessLayer.IncidentAccount incAccDal;

        public CaseBuilder(DataModel.APIs.SellersApi.PingPost.PingPostTransaction transaction)
        {
            this.transaction = transaction;
            affiliatePaymentResolver = new ServiceRequest.AffiliatePaymentResolver(XrmDataContext);
            incidentDal = new DataAccessLayer.Incident(XrmDataContext);
            incAccDal = new DataAccessLayer.IncidentAccount(XrmDataContext);
        }

        protected override void Run()
        {
            DataModel.Xrm.incident incident = new DataModel.Xrm.incident(XrmDataContext);
            Guid contactId = GetContactId();
            incident.customerid = new Microsoft.Crm.Sdk.Customer("contact", contactId);
            incident.new_telephone1 = transaction.PostData.Phone;
            incident.new_country = GlobalConfigurations.DefaultCountry;
            incident.casetypecode = (int)DataModel.Xrm.incident.CaseTypeCode.PingPost;
            incident.new_primaryexpertiseid = categoryDal.GetPrimaryExpertiseIdByCode(transaction.CategoryCode);
            incident.title = "PING-POST API request";
            incident.new_preferedcalltime = DateTime.Now;
            incident.new_callendtime = incident.new_preferedcalltime.Value.AddHours(2);
            incident.description = String.Empty;
            incident.new_webdescription = String.Empty;
            incident.new_dialerstatus = (int)DataModel.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
            incident.statuscode = (int)DataModel.Xrm.incident.Status.WORK_DONE;
            incident.new_originid = new Guid(transaction.OriginId);
            incident.new_requiredaccountno = 1;
            incident.new_ordered_providers = 1;
            incident.new_regionid = regionDal.GetRegionIdByName(transaction.Zip);
            affiliatePaymentResolver.ResolveAffiliatePayment(incident);
            incident.new_pingposttransactionid = transaction._id.ToString();
            incidentDal.Create(incident);
            XrmDataContext.SaveChanges();

            var buyer = transaction.Buyers.First(x => x.Sold);
            DataModel.Xrm.new_incidentaccount ia = new DataModel.Xrm.new_incidentaccount();
            ia.new_incidentid = incident.incidentid;
            ia.new_accountid = new Guid(buyer.SupplierId);
            ia.new_tocharge = true;
            ia.statuscode = (int)DataModel.Xrm.new_incidentaccount.Status.CLOSE;
            ia.new_potentialincidentprice = buyer.Price;
            ia.new_predefinedprice = buyer.Price;
            incAccDal.Create(ia);
            XrmDataContext.SaveChanges();
        }

        private Guid GetContactId()
        {
            DataAccessLayer.Contact contactDAL = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            DataModel.SqlHelper.ContactMinData consumer = contactDAL.GetContactMinDataByPhoneNumber(transaction.PostData.Phone);
            Guid contactId;
            if (consumer == null)
            {
                var contact = contactDAL.CreateAnonymousContact(transaction.PostData.Phone, transaction.PostData.FirstName + " " + transaction.PostData.LastName, transaction.PostData.Email, transaction.PostData.StreetAddress, false, null, null, null, null, true, null, null, false);
                contactId = contact.contactid;
            }
            else
            {
                contactId = consumer.Id;
            }
            return contactId;
        }
    }
}
