﻿using Effect.Crm.Logs;
using System;
using System.Linq;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPost
{
    public class PostManager
    {
        private static DataAccessLayer.PingPostTransaction pingPostDal = new DataAccessLayer.PingPostTransaction();

        public DataModel.APIs.SellersApi.PingPost.PostResponse Post(DataModel.APIs.SellersApi.PingPost.PostRequest request)
        {
            var transaction = pingPostDal.Retrieve(request.NpPingId);
            transaction.PostOn = DateTime.Now;
            transaction.PostData.Email = request.Email;
            transaction.PostData.FirstName = request.FirstName;
            transaction.PostData.LastName = request.LastName;
            transaction.PostData.Phone = request.Phone;
            transaction.PostData.StreetAddress = GetStreetAddress(request.StreetAddress, transaction.Zip);
            transaction.PostData.IP = request.IP;
            transaction.PostData.Description = request.Description;
            transaction.PostData.AdditionalInfo = request.AdditionalInfo;
            bool sold = false;
            if (transaction.Test)
            {
                sold = true;
            }
            else
            {
                var orderedBuyers = transaction.Buyers.OrderByDescending(x => x.Rank);
                for (int i = 0; i < orderedBuyers.Count() && !sold; i++)
                {
                    sold = PostBuyer(transaction, orderedBuyers.ElementAt(i));
                }
            }
            if (sold)
            {
                transaction.Status = DataModel.APIs.SellersApi.PingPost.PingPostTransaction.StatusOptions.POST_SUCCESS;
            }
            else
            {
                transaction.Status = DataModel.APIs.SellersApi.PingPost.PingPostTransaction.StatusOptions.POST_FAILED;
                transaction.PostMessages.Add("All relevant buyers rejected the post.");
            }

            pingPostDal.Save(transaction);
            if (sold && !transaction.Test)
                CreateCase(transaction);
            var response = BuildResponse(transaction, sold);
            return response;
        }

        private void CreateCase(DataModel.APIs.SellersApi.PingPost.PingPostTransaction transaction)
        {
            CaseBuilder builder = new CaseBuilder(transaction);
            builder.Start();
        }

        private static DataModel.APIs.SellersApi.PingPost.PostResponse BuildResponse(DataModel.APIs.SellersApi.PingPost.PingPostTransaction transaction, bool sold)
        {
            var response = new DataModel.APIs.SellersApi.PingPost.PostResponse();
            response.Messages = transaction.PostMessages;
            response.Success = sold;
            return response;
        }

        private static bool PostBuyer(DataModel.APIs.SellersApi.PingPost.PingPostTransaction transaction, DataModel.APIs.SellersApi.PingPost.PingPostTransaction.Buyer buyer)
        {
            bool sold = false;
            if (buyer.Price > transaction.Price)
            {
                LeadBuyers.IPingPostApiBuyer buyerImpl = LeadBuyers.PingPostApiBuyerFactory.GetInstance(buyer.LeadBuyerName);
                if (buyerImpl != null)
                {
                    LeadBuyers.PostResult result;
                    try
                    {
                        LeadBuyers.PostData postData = BuildPostData(transaction, buyer);
                        result = buyerImpl.Post(postData);
                        sold = result.Accepted;
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception posting to buyer in PingPost API.");
                        result = new LeadBuyers.PostResult();
                        result.PostMessage = "Exception posting to buyer in PingPost API. buyerName = " + buyer.LeadBuyerName;
                    }
                    if (sold)
                    {
                        buyer.Sold = true;
                        buyer.Message = result.PostId;
                    }
                    else
                    {
                        transaction.StatusReasons.Add(buyer.LeadBuyerName + " rejected post with message: " + result.PostMessage);
                        buyer.Message = result.PostMessage;
                    }
                }
            }
            return sold;
        }

        private static LeadBuyers.PostData BuildPostData(DataModel.APIs.SellersApi.PingPost.PingPostTransaction transaction, DataModel.APIs.SellersApi.PingPost.PingPostTransaction.Buyer buyer)
        {
            LeadBuyers.PostData postData = new LeadBuyers.PostData();
            postData.Id = transaction._id.ToString();
            postData.Description = String.Empty;
            postData.Zip = transaction.Zip;
            postData.ExternalCode = buyer.ExternalCode;
            postData.CategoryCode = transaction.CategoryCode;
            postData.PingId = buyer.PingId;
            postData.Phone = transaction.PostData.Phone;
            postData.Email = transaction.PostData.Email;
            postData.FirstName = transaction.PostData.FirstName;
            postData.LastName = transaction.PostData.LastName;
            postData.StreetAddress = transaction.PostData.StreetAddress;
            postData.IP = transaction.PostData.IP;
            return postData;
        }

        private string GetStreetAddress(string streetAddress, string zip)
        {
            return streetAddress;
            //if (!String.IsNullOrEmpty(streetAddress))
            //{
            //    return streetAddress;
            //}
            //return CreateStreetAddressFromZipcode(zip);
        }

        private string CreateStreetAddressFromZipcode(string zip)
        {
            return "sheker kolsheu";
        }
    }
}
