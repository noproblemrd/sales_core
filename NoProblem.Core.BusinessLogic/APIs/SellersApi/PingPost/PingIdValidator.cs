﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPost
{
    public class PingIdValidator
    {
        private static DataAccessLayer.PingPostTransaction pingPostDal = new DataAccessLayer.PingPostTransaction();

        public static bool IsNpPingIdValidForPost(string npPingId)
        {
            var transaction = pingPostDal.Retrieve(npPingId);
            return
                transaction != null
                && transaction.Status == DataModel.APIs.SellersApi.PingPost.PingPostTransaction.StatusOptions.PING_ACCEPTED;
        }
    }
}
