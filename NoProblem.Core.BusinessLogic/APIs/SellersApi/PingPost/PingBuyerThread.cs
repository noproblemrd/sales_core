﻿using Effect.Crm.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPost
{
    class PingBuyerThread : Runnable
    {
        public PingBuyerThread(string zip, Guid categoryId, DataModel.APIs.SellersApi.PingPost.PingPostTransaction entry, NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPost.PingManager.BuyerData buyerData)
        {
            this.zip = zip;
            this.categoryId = categoryId;
            this.entry = entry;
            this.buyerData = buyerData;
        }

        string zip;
        Guid categoryId;
        DataModel.APIs.SellersApi.PingPost.PingPostTransaction entry;
        NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPost.PingManager.BuyerData buyerData;

        protected override void Run()
        {
            LeadBuyers.IPingPostApiBuyer buyerImpl = LeadBuyers.PingPostApiBuyerFactory.GetInstance(buyerData.LeadBuyerName);
            if (buyerImpl != null)
            {
                LeadBuyers.PingResult result;
                try
                {
                    LeadBuyers.PingData pingData = BuildPingData(entry, buyerData);
                    result = buyerImpl.Ping(pingData);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception pinging buyer");
                    result = new LeadBuyers.PingResult();
                }
                var buyer = new DataModel.APIs.SellersApi.PingPost.PingPostTransaction.Buyer()
                {
                    PingId = result.PingId,
                    Price = result.PingPrice,
                    SupplierId = buyerData.Id.ToString(),
                    Rank = buyerData.BillingRate * result.PingPrice,
                    ExternalCode = buyerData.ExternalCode,
                    LeadBuyerName = buyerData.LeadBuyerName
                };
                if (result.Accepted)
                {
                    lock (entry)
                    {
                        entry.Buyers.Add(buyer);
                    }
                }
                else
                {
                    buyer.Message = result.PingMessage;
                    lock (entry)
                    {
                        entry.PingRejects.Add(buyer);
                    }
                }
            }
        }

        private static LeadBuyers.PingData BuildPingData(DataModel.APIs.SellersApi.PingPost.PingPostTransaction entry, NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPost.PingManager.BuyerData buyerData)
        {
            LeadBuyers.PingData pingData = new LeadBuyers.PingData();
            pingData.Id = entry._id.ToString();
            pingData.Zip = entry.Zip;
            pingData.CategoryCode = entry.CategoryCode;
            pingData.DefaultPrice = buyerData.DefaultPrice;
            pingData.ExternalCode = buyerData.ExternalCode;
            return pingData;
        }
    }
}
