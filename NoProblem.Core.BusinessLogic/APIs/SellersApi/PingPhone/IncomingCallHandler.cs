﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPhone
{
    public class IncomingCallHandler : XrmUserBase
    {
        DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry dal;

        public IncomingCallHandler(DataModel.Xrm.DataContext xrmDataContext) :
            base(xrmDataContext)
        {
            dal = new DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry(XrmDataContext);
        }

        internal IncomingCallContainer IncomingPhone(string consumerPhoneNumber)
        {
            var query = from x in dal.All
                        where x.new_callerid == consumerPhoneNumber
                        && x.new_status == (int)DataModel.Xrm.new_pingphoneapientry.Status.Ping2Done
                        orderby x.createdon descending
                        select new { EntryId = x.new_pingphoneapientryid, IsTest = x.new_istest.HasValue ? x.new_istest.Value : false, TestPhone = x.new_testproviderphone, SupplierId = x.new_accountid, Price = x.new_price.Value };
            var item = query.FirstOrDefault();
            if (item == null)
            {
          //      Effect.Crm.Logs.LogUtils.MyHandle.WriteToLog(1, "IncomingCallHandler IncomingPhone NULL");
                return HandleNotFound(consumerPhoneNumber);
            }
            else
            {
        //        Effect.Crm.Logs.LogUtils.MyHandle.WriteToLog(1, "IncomingCallHandler IncomingPhone: " + Newtonsoft.Json.JsonConvert.SerializeObject(item));
                return HandleFound(item.EntryId, item.IsTest, item.TestPhone, item.SupplierId, item.Price);
            }
        }

        private IncomingCallContainer HandleNotFound(string consumerPhoneNumber)
        {
            IncomingCallContainer container = new IncomingCallContainer()
            {
                IsAvailable = false
            };
            IncomingCallNotFoundHandlerThread handler = new IncomingCallNotFoundHandlerThread(consumerPhoneNumber);
            handler.Start();
            return container;
        }

        private IncomingCallContainer HandleFound(Guid pingPhoneEntryId, bool isTest, string testPhone, Guid? supplierId, decimal price)
        {
            UpdateEntry(pingPhoneEntryId);
            IncomingCallContainer response;
            if (isTest)
            {
                response = CreateTestResponse(testPhone, price);
            }
            else
            {
                response = CreateRealResponse(supplierId, price);
            }
            response.Custom = CustomFieldManager.BuildCustomField(pingPhoneEntryId);
            Effect.Crm.Logs.LogUtils.MyHandle.WriteToLog(1, Newtonsoft.Json.JsonConvert.SerializeObject(response));
            return response;
        }

        private void UpdateEntry(Guid pingPhoneEntryId)
        {
            DataModel.Xrm.new_pingphoneapientry entry = new DataModel.Xrm.new_pingphoneapientry(XrmDataContext);
            entry.new_pingphoneapientryid = pingPhoneEntryId;
            entry.new_status = (int)DataModel.Xrm.new_pingphoneapientry.Status.InCall;
            dal.Update(entry);
            XrmDataContext.SaveChanges();
        }

        private IncomingCallContainer CreateRealResponse(Guid? supplierId, decimal price)
        {
            DataAccessLayer.AccountRepository accDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = (from acc in accDal.All
                            where acc.accountid == supplierId
                            select new
                            {
                                Phone = acc.telephone1,
                                Country = acc.new_country,
                                IsLeadBuyer = acc.new_isleadbuyer,
                                LimitCallDur = acc.new_limitcallduration,
                                RecordCall = acc.new_recordcalls,
                                Prompt = acc.new_incomingcallcallerprompt
                            }).First();
            return new IncomingCallContainer()
            {
                IsAvailable = true,
                IsLeadBuyer = supplier.IsLeadBuyer.HasValue ? supplier.IsLeadBuyer.Value : false,
                LimitCallDuration = supplier.LimitCallDur.HasValue ? supplier.LimitCallDur.Value : 0,
                PricePerCall = price,
                RecordCalls = supplier.RecordCall.HasValue ? supplier.RecordCall.Value : false,
                SupplierCountry = String.IsNullOrEmpty(supplier.Country) ? GlobalConfigurations.DefaultCountry : supplier.Country,
                TruePhoneNumber = supplier.Phone,
                SupplierId = supplierId.Value,
                IncomingCallCallerPrompt = supplier.Prompt
            };
        }

        private IncomingCallContainer CreateTestResponse(string testPhone, decimal price)
        {
            return new IncomingCallContainer()
            {
                IsAvailable = true,
                IsLeadBuyer = true,
                PricePerCall = price,
                RecordCalls = true,
                SupplierCountry = GlobalConfigurations.DefaultCountry,
                TruePhoneNumber = testPhone
            };
        }

        public class IncomingCallContainer
        {
            public bool IsLeadBuyer { get; set; }
            public string IncomingCallCallerPrompt { get; set; }
            public int LimitCallDuration { get; set; }
            public decimal PricePerCall { get; set; }
            public bool IsAvailable { get; set; }
            public Guid SupplierId { get; set; }
            public bool RecordCalls { get; set; }
            public string TruePhoneNumber { get; set; }
            public string SupplierCountry { get; set; }
            public string Custom { get; set; }
        }
    }
}
