﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPhone
{
    class CallbackSenderThread : Runnable
    {
        bool toCharge;
        Guid pingPhoneEntryId;
        DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry dal;

        public CallbackSenderThread(bool toCharge, string custom)
        {
            this.toCharge = toCharge;
            this.pingPhoneEntryId = CustomFieldManager.GetPingPhoneId(custom);
            dal = new DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry(XrmDataContext);
        }

        protected override void Run()
        {
            var entryData = (from x in dal.All
                             where x.new_pingphoneapientryid == pingPhoneEntryId
                             select new { CallbackUrl = x.new_callbackurl, LeadId = x.new_leadid }).First();
            if (!String.IsNullOrEmpty(entryData.CallbackUrl))
            {
                RestSharp.RestClient client = new RestSharp.RestClient(entryData.CallbackUrl);
                RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
                CallbackRequest body = new CallbackRequest();
                body.is_billable = toCharge;
                body.lead_id = entryData.LeadId;
                body.np_ping_id = pingPhoneEntryId.ToString();
                request.RequestFormat = RestSharp.DataFormat.Json;
                request.AddBody(body);
                var response = client.Execute(request);
            }
        }

        private class CallbackRequest
        {
            public bool is_billable { get; set; }
            public string lead_id { get; set; }
            public string np_ping_id { get; set; }
        }
    }
}
