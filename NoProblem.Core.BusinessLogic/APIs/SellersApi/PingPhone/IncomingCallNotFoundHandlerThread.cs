﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataAccessLayer.APIs.SellersApi;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPhone
{
    public class IncomingCallNotFoundHandlerThread : Runnable
    {
        string consumerPhoneNumber;
        PingPhoneApiFailedIncomingMap dal;

        public IncomingCallNotFoundHandlerThread(string consumerPhoneNumber)
        {
            this.consumerPhoneNumber = consumerPhoneNumber;
            dal = new PingPhoneApiFailedIncomingMap(XrmDataContext);
        }

        protected override void Run()
        {
            DataModel.Xrm.new_pingphoneapifailedincomingmap entry = new DataModel.Xrm.new_pingphoneapifailedincomingmap();
            entry.new_failurereason = DataModel.Xrm.new_pingphoneapifailedincomingmap.FailureReasons.NOT_FOUND;
            entry.new_callerid = consumerPhoneNumber;
            dal.Create(entry);
            XrmDataContext.SaveChanges();
        }
    }
}
