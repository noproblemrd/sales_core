﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.APIs.SellersApi.PingPhone;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPhone
{
    public class Ping2Manager : XrmUserBase
    {
        DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry dal;

        public Ping2Manager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            dal = new DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry(XrmDataContext);
        }

        public bool IsNpPingIdValidForPing2(Guid npPingId)
        {
            Guid id = (from e in dal.All
                       where e.new_pingphoneapientryid == npPingId
                       && e.new_status == (int)DataModel.Xrm.new_pingphoneapientry.Status.Ping1Accepted
                       select e.new_pingphoneapientryid).FirstOrDefault();
            return id != Guid.Empty;
        }

        public Ping2Response Ping2(Ping2Request request)
        {
            DataModel.Xrm.new_pingphoneapientry entry = new DataModel.Xrm.new_pingphoneapientry(XrmDataContext);
            entry.new_pingphoneapientryid = new Guid(request.NpPingId);
            entry.new_status = (int)DataModel.Xrm.new_pingphoneapientry.Status.Ping2Done;
            entry.new_callerid = request.CallerId;
            entry.new_callbackurl = request.CallbackUrl;
            entry.new_iscalleridreal = request.IsCallerIdReal;
            dal.Update(entry);
            XrmDataContext.SaveChanges();

            string npPhone = PingPhoneDidFinder.GetPhone(XrmDataContext);

            return new Ping2Response()
            {
                NpPhone = npPhone
            };
        }
    }
}
