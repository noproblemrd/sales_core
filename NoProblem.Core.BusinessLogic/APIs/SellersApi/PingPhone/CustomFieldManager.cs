﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPhone
{
    internal static class CustomFieldManager
    {
        private const string PING_PHONE = "P_P";
        private const string SEPARATOR = ":";

        public static string BuildCustomField(Guid pingPhoneEntryId)
        {
            return PING_PHONE + SEPARATOR + pingPhoneEntryId.ToString();
        }

        internal static bool IsPingPhoneCustom(string custom)
        {
            return custom != null && custom.StartsWith(PING_PHONE);
        }

        internal static Guid GetPingPhoneId(string custom)
        {
            int index = custom.IndexOf(SEPARATOR);
            if (IsPingPhoneCustom(custom) && index > 0)
            {
                return new Guid(custom.Substring(index + 1));
            }
            return Guid.Empty;
        }
    }
}
