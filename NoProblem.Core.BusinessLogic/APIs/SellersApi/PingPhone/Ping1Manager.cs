﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.APIs.SellersApi.PingPhone;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPhone
{
    public class Ping1Manager : XrmUserBase
    {
        DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry pingPhoneApiEntryDal;
        DataAccessLayer.Origin originDal;
        internal const int MIN_CALL_DURATION_FOR_TESTS = 20;

        public Ping1Manager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            pingPhoneApiEntryDal = new DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry(XrmDataContext);
            originDal = new DataAccessLayer.Origin(XrmDataContext);
        }

        public Ping1Response Ping1(Ping1Request request)
        {
            var response = new Ping1Response();
            bool isTest = request.Test == 1;
            Guid categoryId = FindCategoryId(request.Category);
            Guid regionId = FindRegionId(request.Zip);
            Guid supplierId = Guid.Empty;
            decimal price = 15m;//if test => always 15.
            Guid originId = new Guid(request.SellerId);
            int billableCallDuration = MIN_CALL_DURATION_FOR_TESTS;
            if (!isTest)
            {
                IEnumerable<Guid> irrelevantBuyers = GetIrrelevantBuyers(originId);
                var realTimeRoutingData = Dialer.RealTimeRouting.RealTimeRouter.GetSupplierForRealTimeRouting(categoryId, GlobalConfigurations.DefaultCountry, irrelevantBuyers, regionId);
                if (!realTimeRoutingData.IsAvailable)
                {
                    response.Interested = false;
                    price = 0;
                }
                else
                {
                    supplierId = realTimeRoutingData.SupplierId;
                    price = CalculatePingPrice(realTimeRoutingData.PricePerCall, originId);
                    billableCallDuration = realTimeRoutingData.BillableCallDuration;
                }
            }
            Guid entryId = CreateApiEntry(request, response, supplierId, price, categoryId, regionId, originId);
            response.NpPingId = entryId.ToString();
            response.Price = price;
            response.BillingCallDurationSeconds = billableCallDuration;
            return response;
        }

        private IEnumerable<Guid> GetIrrelevantBuyers(Guid originId)
        {
            //avoid to sell the lead to the same seller.
            return originDal.GetBuyersOfThisSeller(originId);
        }

        private decimal CalculatePingPrice(decimal buyersPrice, Guid originId)
        {
            var revenueSharePercentage = (from ori in originDal.All
                                          where ori.new_originid == originId
                                          select ori.new_revenueshare.HasValue ? ori.new_revenueshare.Value : 0).First();
            return buyersPrice * revenueSharePercentage / 100;
        }

        private Guid FindRegionId(string zip)
        {
            DataAccessLayer.Region dal = new DataAccessLayer.Region(XrmDataContext);
            return dal.GetRegionIdByName(zip);
        }

        private Guid FindCategoryId(string categoryCode)
        {
            DataAccessLayer.PrimaryExpertise dal = new DataAccessLayer.PrimaryExpertise(XrmDataContext);
            return dal.GetPrimaryExpertiseIdByCode(categoryCode);
        }

        private Guid CreateApiEntry(Ping1Request request, Ping1Response response, Guid supplierId, decimal price, Guid categoryId, Guid regionId, Guid originId)
        {
            DataModel.Xrm.new_pingphoneapientry apiEntry = DataModel.EntityFactories.PingPhoneApiEntryFactory.Create(
                response.Interested,
                request.LeadId,
                categoryId,
                regionId,
                originId,
                supplierId,
                price,
                request.Test == 1,
                request.TestProviderPhone,
                false);
            pingPhoneApiEntryDal.Create(apiEntry);
            XrmDataContext.SaveChanges();
            return apiEntry.new_pingphoneapientryid;
        }
    }
}
