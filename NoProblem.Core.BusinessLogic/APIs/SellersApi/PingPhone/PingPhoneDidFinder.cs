﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPhone
{
    class PingPhoneDidFinder
    {
        public static string GetPhone(DataModel.Xrm.DataContext xrmDataContext)
        {
            DataAccessLayer.DirectNumber dnDal = new DataAccessLayer.DirectNumber(xrmDataContext);
            string phone = (from dn in dnDal.All
                            where dn.new_ispingphoneincomingdid == true
                            select dn.new_number).FirstOrDefault();
            return phone;
        }
    }
}
