﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPhone
{
    public class IncomingCallEndedHandler : XrmUserBase
    {
        public class CallEndedContainer
        {
            public Guid CategoryId { get; set; }
            public Guid SupplierId { get; set; }
            public Guid OriginId { get; set; }
            public decimal PriceForCall { get; set; }
            public int MinCallDuration { get; set; }
            public bool IsTest { get; set; }
        }

        DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry dal;
        DataAccessLayer.AccountExpertise accExpDal;
        DataAccessLayer.AccountRepository accDal;

        public IncomingCallEndedHandler(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            dal = new DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry(XrmDataContext);
            accExpDal = new DataAccessLayer.AccountExpertise(XrmDataContext);
            accDal = new DataAccessLayer.AccountRepository(XrmDataContext);
        }

        internal CallEndedContainer CallEnded(string custom)
        {
            Guid id = CustomFieldManager.GetPingPhoneId(custom);
            var entryData = (from x in dal.All
                             where x.new_pingphoneapientryid == id
                             select new
                             {
                                 SupplierId = x.new_accountid.Value,
                                 PriceToSeller = x.new_price.Value,
                                 CategoryId = x.new_primaryexpertiseid.Value,
                                 OriginId = x.new_originid.Value,
                                 IsTest = x.new_istest.HasValue ? x.new_istest.Value : false
                             }).First();
            decimal priceForCall = 0;
            int minCallDuration = APIs.SellersApi.PingPhone.Ping1Manager.MIN_CALL_DURATION_FOR_TESTS;
            if (!entryData.IsTest)
            {
                priceForCall = (from x in accExpDal.All
                                where
                                x.new_accountid == entryData.SupplierId
                                && x.new_primaryexpertiseid == entryData.CategoryId
                                && x.statecode == DataAccessLayer.AccountExpertise.ACTIVE
                                select x.new_incidentprice.Value).First();
                minCallDuration = (from x in accDal.All
                                   where x.accountid == entryData.SupplierId
                                   select x.new_mincallduration.HasValue ? x.new_mincallduration.Value : -1).First();
            }
            CallEndedContainer container = new CallEndedContainer()
            {
                CategoryId = entryData.CategoryId,
                MinCallDuration = minCallDuration,
                OriginId = entryData.OriginId,
                PriceForCall = priceForCall,
                SupplierId = entryData.SupplierId,
                IsTest = entryData.IsTest
            };

            UpdateCallDone(id);

            return container;
        }

        private void UpdateCallDone(Guid id)
        {
            DataModel.Xrm.new_pingphoneapientry entry = new DataModel.Xrm.new_pingphoneapientry(XrmDataContext);
            entry.new_pingphoneapientryid = id;
            entry.new_status = (int)DataModel.Xrm.new_pingphoneapientry.Status.CallDone;
            dal.Update(entry);
            XrmDataContext.SaveChanges();
        }
    }
}
