﻿using NoProblem.Core.DataModel.APIs.SellersApi.Soleo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.APIs.SellersApi.Soleo
{
    public class SoleoManager
    {
        private static readonly Guid PingPostTestAccount;
        static SoleoManager()
        {
            string id = System.Configuration.ConfigurationManager.AppSettings["PingPostTestAccount"];
            PingPostTestAccount = new Guid(id);
        }
        NoProblem.Core.DataModel.Xrm.DataContext XrmDataContext;
        public SoleoManager()
        {
            XrmDataContext = DataModel.XrmDataContext.Create();
        }
        public SoleoPingResponse Get(SoleoPingRequest request)
        {
            Effect.Crm.Logs.LogUtils.MyHandle.WriteToLog(1, "SoleoManager request= {0}", Newtonsoft.Json.JsonConvert.SerializeObject(request));
            SoleoPingResponse response = new SoleoPingResponse();
            Guid supplierId = Guid.Empty;
            decimal price = 15m;
            int billableCallDuration = 0;
            if(request.isTest)
            {
                return ResponseTest(request);
            }
            
            Guid categoryId = FindCategoryName(request.category);
            Guid regionId = FindRegionId(request.zip);
            Guid originId = request.sellerId;
            IEnumerable<Guid> irrelevantBuyers = GetIrrelevantBuyers(originId);
            var realTimeRoutingData = Dialer.RealTimeRouting.RealTimeRouter.GetSupplierForRealTimeRouting(categoryId, GlobalConfigurations.DefaultCountry, irrelevantBuyers, regionId);
            if (!realTimeRoutingData.IsAvailable)
            {
                response.SetResult(true);
                return response;
            }
            else
            {
                supplierId = realTimeRoutingData.SupplierId;
                price = CalculatePingPrice(realTimeRoutingData.PricePerCall, originId);
                billableCallDuration = realTimeRoutingData.BillableCallDuration;
            }
            Guid entryId = CreateApiEntry(request.requesterid, realTimeRoutingData.SupplierId, price, categoryId, regionId, originId, request.callerId);
            response.searchid = entryId.ToString();
            response.SetResult(true);
            SoleoDataResponse data = new SoleoDataResponse();
            data.price = price;
            data.phone = GetPhoneNumber(entryId);//realTimeRoutingData.TruePhoneNumber;
            data.state = realTimeRoutingData.SupplierCountry;
            data.recordedname = realTimeRoutingData.IncomingCallCallerPrompt;
            data.duration = billableCallDuration;
            response.listings.Add(data);
            return response;
            
        }
        public SoleoPingResponse ResponseTest(SoleoPingRequest request)
        {
            SoleoPingResponse response = new SoleoPingResponse();
            Guid supplierId = PingPostTestAccount;
            decimal price = 15m;
            int billableCallDuration = 0;            

            Guid categoryId = FindCategoryName(request.category);
            Guid regionId = FindRegionId(request.zip);
            Guid originId = request.sellerId;

            Guid entryId = CreateApiEntry(request.requesterid, supplierId, price, categoryId, regionId, originId, request.callerId);
            response.searchid = entryId.ToString();
            response.SetResult(true);
            SoleoDataResponse data = new SoleoDataResponse();
            data.price = price;
            data.phone = GetPhoneNumber(entryId);//realTimeRoutingData.TruePhoneNumber;
            data.state = "UNITED STATES";
            data.recordedname = null;
            data.duration = billableCallDuration;
            response.listings.Add(data);
            return response;
        }
        Guid FindCategoryName(string categoryName)
        {
            //Computer Repair
           // return new Guid("AAE69263-8E43-E211-A9A0-001517D10F6E");
            NoProblem.Core.DataAccessLayer.SlikerKeyword sk = new DataAccessLayer.SlikerKeyword(XrmDataContext);
            return sk.GetPrimaryExpertiseByKeyword(categoryName);
        }
        private Guid FindRegionId(string zip)
        {
            DataAccessLayer.Region dal = new DataAccessLayer.Region(XrmDataContext);
            return dal.GetRegionIdByName(zip);
        }
        private IEnumerable<Guid> GetIrrelevantBuyers(Guid originId)
        {
            //avoid to sell the lead to the same seller.
            DataAccessLayer.Origin originDal = new DataAccessLayer.Origin(XrmDataContext);
            return originDal.GetBuyersOfThisSeller(originId);
        }
        private decimal CalculatePingPrice(decimal buyersPrice, Guid originId)
        {
            DataAccessLayer.Origin originDal = new DataAccessLayer.Origin(XrmDataContext);
            var revenueSharePercentage = (from ori in originDal.All
                                          where ori.new_originid == originId
                                          select ori.new_revenueshare.HasValue ? ori.new_revenueshare.Value : 0).First();
            return buyersPrice * revenueSharePercentage / 100;
        }
        private Guid CreateApiEntry(string requestId, Guid supplierId, decimal price, Guid categoryId, Guid regionId, Guid originId, string callerId)
        {
            DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry pingPhoneApiEntryDal = new DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry(XrmDataContext);
            DataModel.Xrm.new_pingphoneapientry apiEntry = DataModel.EntityFactories.PingPhoneApiEntryFactory.Create(
                true,
                requestId,
                categoryId,
                regionId,
                originId,
                supplierId,
                price,
                //request.Test == 1,
                false,
               // request.TestProviderPhone,
               "",
                false,
                callerId,
                true);
            pingPhoneApiEntryDal.Create(apiEntry);
            XrmDataContext.SaveChanges();
            return apiEntry.new_pingphoneapientryid;
        }
        public string GetPhoneNumber(Guid entryId)
        {
            DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry dal = new DataAccessLayer.APIs.SellersApi.PingPhoneApiEntry(XrmDataContext);
            DataModel.Xrm.new_pingphoneapientry entry = new DataModel.Xrm.new_pingphoneapientry(XrmDataContext);
            entry.new_pingphoneapientryid = entryId;
            entry.new_status = (int)DataModel.Xrm.new_pingphoneapientry.Status.Ping2Done;
        //    entry.new_callerid = request.CallerId;
        //    entry.new_callbackurl = request.CallbackUrl;
            entry.new_iscalleridreal = false;// request.IsCallerIdReal;
            dal.Update(entry);
            XrmDataContext.SaveChanges();

            return NoProblem.Core.BusinessLogic.APIs.SellersApi.PingPhone.PingPhoneDidFinder.GetPhone(XrmDataContext);

        }
       
    }
}
