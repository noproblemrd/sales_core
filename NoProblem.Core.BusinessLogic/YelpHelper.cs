﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic
{
    internal class YelpHelper
    {
        private DataAccessLayer.ConfigurationSettings config;
        private readonly string accessToken;
        private readonly string accessTokenSecret;
        private readonly string consumerKey;
        private readonly string consumerKeySecret;

        private volatile static YelpHelper instance;
        private static object locker = new object();

        private YelpHelper()
        {
            config = new DataAccessLayer.ConfigurationSettings(null);
            accessToken = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.YELP_ACCESS_TOKEN);
            accessTokenSecret = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.YELP_ACCESS_TOKEN_SECRET);
            consumerKey = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.YELP_CONSUMER_KEY);
            consumerKeySecret = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.YELP_CONSUMER_KEY_SECRET);
        }

        private YelpSharp.Yelp CreateYelpClient()
        {
            YelpSharp.Options options = new YelpSharp.Options();
            options.AccessToken = accessToken;
            options.AccessTokenSecret = accessTokenSecret;
            options.ConsumerKey = consumerKey;
            options.ConsumerSecret = consumerKeySecret;
            return new YelpSharp.Yelp(options);
        }

        public static YelpSharp.Yelp GetYelp()
        {
            if (instance == null)
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new YelpHelper();
                    }
                }
            }
            return instance.CreateYelpClient();
        }
    }
}
