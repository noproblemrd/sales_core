﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Effect.Crm.Configuration;
using Effect.Crm.Logs;
using Microsoft.Crm.SdkTypeProxy;
using NoProblem.Core.DataModel;
using NoProblem.Core.Notifications;
using DML = NoProblem.Core.DataModel;
using System.Collections.Generic;
using System.Text;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic
{
    public class Notifications : XrmUserBase
    {
        #region Ctor
        public Notifications() : this(null)
        { }
        public Notifications(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
        }
        #endregion

        private static Regex instantinateTemplateRegex = new Regex(@"\<\!\[CDATA\[(?<text>[^\]]*)\]\]\>", RegexOptions.None);

        public bool SendEmail(string toEmail, string body, string subject)
        {
            if (!string.IsNullOrEmpty(toEmail))
            {
                List<string> emailsLst = new List<string>();
                emailsLst.Add(toEmail);
                return SendEmail(emailsLst, body, subject);
            }
            return false;
        }
      

        internal bool SendEmail(List<string> toEmails, string body, string subject)
        {
            DataAccessLayer.ConfigurationSettings dal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            return SendEmail(toEmails, body, subject, dal);
        }
        internal bool SendEmail(List<string> toEmails, string body, string subject, List<string> AttachFilesPath)
        {
            DataAccessLayer.ConfigurationSettings dal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string emailSender = dal.GetConfigurationSettingValue(DML.ConfigurationKeys.EMAIL_SENDER);
     //       return SendEmail(toEmails, body, subject, dal);
            return SendEmail(emailSender, toEmails, body, subject, dal, AttachFilesPath);
        }

        internal bool SendEmail(string fromEmail, List<string> toEmails, string body, string subject)
        {
            DataAccessLayer.ConfigurationSettings dal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            return SendEmail(fromEmail, toEmails, body, subject, dal);
        }

        internal bool SendEmail(List<string> toEmails, string body, string subject, DataAccessLayer.ConfigurationSettings dal)
        {
            string emailSender = dal.GetConfigurationSettingValue(DML.ConfigurationKeys.EMAIL_SENDER);
            return SendEmail(emailSender, toEmails, body, subject, dal);
        }

        internal bool SendEmail(string toEmail, string body, string subject, DataAccessLayer.ConfigurationSettings dal)
        {
            if (!string.IsNullOrEmpty(toEmail))
            {
                List<string> emailsLst = new List<string>();
                emailsLst.Add(toEmail);
                return SendEmail(emailsLst, body, subject, dal);
            }
            return false;
        }

        internal bool SendEmail(string fromEmail, string toEmail, string body, string subject, DataAccessLayer.ConfigurationSettings dal)
        {
            if (!string.IsNullOrEmpty(toEmail))
            {
                List<string> emailsLst = new List<string>();
                emailsLst.Add(toEmail);
                return SendEmail(fromEmail, emailsLst, body, subject, dal);
            }
            return false;
        }

        internal bool SendEmail(string fromEmail, List<string> toEmails, string body, string subject, DataAccessLayer.ConfigurationSettings dal)
        {
            return SendEmail(fromEmail, toEmails, body, subject, dal, null);
            /*
            bool retVal = false;
            try
            {
                if (toEmails != null && toEmails.Count > 0 && !string.IsNullOrEmpty(fromEmail) && (!string.IsNullOrEmpty(body) || !string.IsNullOrEmpty(subject)))
                {
                    string smtpHost = dal.GetConfigurationSettingValue(DML.ConfigurationKeys.SMTP_HOST);
                    if (string.IsNullOrEmpty(smtpHost))
                    {
                        smtpHost = "localhost";
                    }
                    System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(smtpHost, 25);
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(fromEmail);
                    foreach (var email in toEmails)
                    {
                        mail.To.Add(new MailAddress(email));
                    }
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = true;
                    client.Send(mail);
                    retVal = true;
                }
            }
            catch (Exception exc)
            {
                StringBuilder builder = new StringBuilder();
                if (toEmails != null)
                {
                    foreach (string email in toEmails)
                    {
                        builder.Append(email).Append(";");
                    }
                }
                LogUtils.MyHandle.HandleException(exc, "Exception sending email. From={0}, To={1}, Subject={2}", fromEmail, builder.ToString(), subject);
                retVal = false;
            }
            return retVal;
             * */
        }
        internal bool SendEmail(string fromEmail, List<string> toEmails, string body, string subject, DataAccessLayer.ConfigurationSettings dal, List<string> AttachFiles)
        {
            bool retVal = false;
            string smtpHost = string.Empty;
            string ToEmails = string.Empty;
            /*
            if (toEmails != null)
            {
                foreach (string _email in toEmails)
                {
                    ToEmails += ";" + _email;
                }
            }
            LogUtils.MyHandle.HandleException(new Exception("SendEmail with file"), "SendEmail with file. From={0}, body={1}, Subject={2}, toEmails={3}",
                    fromEmail, body, subject, ToEmails);
             */
            try
            {
                if (toEmails != null && toEmails.Count > 0 && !string.IsNullOrEmpty(fromEmail) && (!string.IsNullOrEmpty(body) || !string.IsNullOrEmpty(subject)))
                {
                    smtpHost = dal.GetConfigurationSettingValue(DML.ConfigurationKeys.SMTP_HOST);
                    if (string.IsNullOrEmpty(smtpHost))
                    {
                        smtpHost = "localhost";
                    }
                   
                    System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(smtpHost, 25);
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(fromEmail);
                    foreach (var email in toEmails)
                    {
                        mail.To.Add(new MailAddress(email));
                    }
                    mail.Subject = subject;
                    mail.Body = body;
                    if(AttachFiles != null)
                    {
                        foreach(string _file in AttachFiles)
                            mail.Attachments.Add(new Attachment(_file));
                    }
                    mail.IsBodyHtml = true;
                    client.Send(mail);
                    retVal = true;
                }
            }
            catch (Exception exc)
            {
                
                StringBuilder builder = new StringBuilder();
                if (toEmails != null)
                {
                    foreach (string email in toEmails)
                    {
                        builder.Append(email).Append(";");
                    }
                }
                LogUtils.MyHandle.HandleException(exc, "Exception sending email. smtpHost={3}, From={0}, To={1}, Subject={2}", 
                    fromEmail, builder.ToString(), subject, smtpHost);
                retVal = false;
            }
            return retVal;
        }
        internal void InstatiateTemplate(string templateName, Guid entryId, string entityName, out string subject, out string body)
        {
            DataAccessLayer.Template templateDal = new NoProblem.Core.DataAccessLayer.Template(XrmDataContext);
            using (var service = XrmDataContext.CreateService())
            {
                InstantiateTemplateRequest request = new InstantiateTemplateRequest();
                request.ObjectId = entryId;
                request.ObjectType = entityName;
                DataModel.Xrm.template template = templateDal.GetTemplateByTitle(templateName);
                if (template != null)
                {
                    request.TemplateId = template.templateid;
                    var response = service.Execute(request) as InstantiateTemplateResponse;
                    email email = response.BusinessEntityCollection.BusinessEntities.FirstOrDefault() as email;
                    subject = email.subject;
                    body = email.description;
                }
                else
                {
                    subject = string.Empty;
                    body = string.Empty;
                }
            }
        }

        internal void InstatiateTemplate(string templateName, out string subject, out string body)
        {
            DataAccessLayer.Template templateDal = new NoProblem.Core.DataAccessLayer.Template(XrmDataContext);
            DataModel.Xrm.template template = templateDal.GetTemplateByTitle(templateName);
            subject = template.subject;
            body = template.body;
            subject = ExtractHtmlText(subject);
            body = ExtractHtmlText(body);
        }

        private static string ExtractHtmlText(string input)
        {
            bool isMatch = instantinateTemplateRegex.IsMatch(input);
            if (isMatch)
            {
                Match match = instantinateTemplateRegex.Match(input);
                string HTMLtext = match.Groups["text"].Value;
                input = HTMLtext;
            }
            else
            {
                input = string.Empty;
            }
            return input;
        }

        private readonly string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];

        public bool NotifySupplier(string smsTemplate, string emailTemplate, bool useSms, bool useEmail, Guid supplierId, Guid regardingObjectID, string regardingObjectType, Guid templateObjectID, string templateObjectType)
        {
            bool notificationSent = false;
            DataAccessLayer.AccountRepository accountRepositoryAccess = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryAccess.Retrieve(supplierId);

            if (supplier == null)
            {
                LogUtils.MyHandle.WriteToLog(1, "NotifySupplier Failed with SupplierID not found.");
                return false;
            }

            if (supplier.new_isleadbuyer.HasValue && supplier.new_isleadbuyer.Value)
            {
                return false;
            }

            bool sendMail = useEmail;
            bool sendSMS = useSms;

            string emailaddress1 = supplier.emailaddress1;
            if (sendMail && string.IsNullOrEmpty(emailaddress1))
            {
                sendMail = false;
            }
            string mainPhone = supplier.telephone1;
            string smsPhone = supplier.telephone3;
            string phonenumber = string.Empty;
            if (sendSMS)
            {
                if (Phones.IsMobilePhone(smsPhone))
                {
                    phonenumber = smsPhone;
                }
                else if (Phones.IsMobilePhone(mainPhone))
                {
                    phonenumber = mainPhone;
                }
                if (phonenumber == string.Empty)
                {
                    sendSMS = false;
                }
            }

            if (sendMail)
            {
                notificationSent = SendMailTemplate(emailTemplate, supplierId, "account", regardingObjectID, regardingObjectType, templateObjectID, templateObjectType);
            }
            if (sendSMS && !(supplier.new_donotsendsms.HasValue && supplier.new_donotsendsms.Value))
            {
                bool smsSent = SendSMSTemplate(smsTemplate, phonenumber, supplier.new_country, regardingObjectID, regardingObjectType, templateObjectID, templateObjectType);
                if (sendMail)
                {
                    notificationSent = notificationSent && smsSent;
                }
                else
                {
                    notificationSent = smsSent;
                }
            }
            return notificationSent;
        }

        public bool NotifySupplier(DML.enumSupplierNotificationTypes supplierNotificationType, Guid supplierID, string regardingObjectID, string regardingObjectType, string templateObjectID, string templateObjectType)
        {
            bool notificationSent = false;
            DataAccessLayer.AccountRepository accountRepositoryAccess = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryAccess.Retrieve(supplierID);

            if (supplier == null)
            {
                LogUtils.MyHandle.WriteToLog(1, "NotifySupplier Failed with SupplierID not found.");
                return false;
            }
            if (supplier.new_isleadbuyer.HasValue && supplier.new_isleadbuyer.Value)
            {
                return false;
            }
            //if (supplier.new_status == (int)DML.Xrm.account.SupplierStatus.Rarah)
            //{
            //    return false;
            //}
            string EmailTemplateName = string.Empty;
            string SMSTemplateName = string.Empty;
            DML.enumNotificationMethod eNotificationMethod = DML.enumNotificationMethod.NONE;

            switch (supplierNotificationType)
            {
                case (enumSupplierNotificationTypes.AFTER_CANDIDATE_CONNECTION_TO_CANDIDATE):
                    {
                        eNotificationMethod = GetNotificationMethod(DML.NotificationConfigurationKeys.NOTIFICATION_AFTER_CANDIDATE_CONNECTION_TO_CANDIDATE);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = DML.TemplateNames.AFTER_CANDIDATE_CONNECTION_TO_CANDIDATE;
                            SMSTemplateName = DML.TemplateNames.AFTER_CANDIDATE_CONNECTION_TO_CANDIDATE;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.ACCESSIBLE_CANDIDATE_UNREACHED):
                    {
                        eNotificationMethod = GetNotificationMethod(DML.NotificationConfigurationKeys.NOTIFICATION_ACCESSIBLE_CANDIDATE_UNREACHED);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = DML.TemplateNames.ACCESSIBLE_CANDIDATE_UNREACHED_SMS;
                            SMSTemplateName = DML.TemplateNames.ACCESSIBLE_CANDIDATE_UNREACHED_SMS;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.IN_TRIAL_CANDIDATE_UNREACHED):
                    {
                        eNotificationMethod = GetNotificationMethod(DML.NotificationConfigurationKeys.NOTIFICATION_IN_TRIAL_CANDIDATE_UNREACHED);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = DML.TemplateNames.IN_TRIAL_CANDIDATE_UNREACHED_SMS;
                            SMSTemplateName = DML.TemplateNames.IN_TRIAL_CANDIDATE_UNREACHED_SMS;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.WEEKLY_LOST_CALLS):
                    {
                        eNotificationMethod = GetNotificationMethod(DML.NotificationConfigurationKeys.NOTIFICATION_WEEKLY_LOST_CALLS);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = DML.TemplateNames.WEEKLY_LOST_CALLS_SMS;
                            SMSTemplateName = DML.TemplateNames.WEEKLY_LOST_CALLS_SMS;
                        }
                        break;
                    }
                case (DML.enumSupplierNotificationTypes.RANK_LOW):
                    {
                        eNotificationMethod = GetNotificationMethod(DML.NotificationConfigurationKeys.NOTIFICATION_RANK_LOW);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = DML.TemplateNames.RANK_LOW;
                            SMSTemplateName = DML.TemplateNames.RANK_LOW;
                        }
                        break;
                    }
                case (DML.enumSupplierNotificationTypes.CONSUMER_UNAVAILABLE):
                    {
                        eNotificationMethod = GetNotificationMethod(DML.NotificationConfigurationKeys.NOTIFICATION_SUPPLIER_CONSUMER_UNAVAILABLE);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = DML.TemplateNames.SUPPLIER_CONSUMER_UNAVAILABLE_SMS;
                            SMSTemplateName = DML.TemplateNames.SUPPLIER_CONSUMER_UNAVAILABLE_SMS;
                        }
                        break;
                    }
                case (DML.enumSupplierNotificationTypes.SUPPLIER_LOSER):
                    {
                        eNotificationMethod = GetNotificationMethod(DML.NotificationConfigurationKeys.NOTIFICATION_SUPPLIER_LOSER);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = DML.TemplateNames.SUPPLIER_LOSER_SMS;
                            SMSTemplateName = DML.TemplateNames.SUPPLIER_LOSER_SMS;
                        }
                        break;
                    }
                case (DML.enumSupplierNotificationTypes.BALANCE_UNDER_INCIDENT_PRICE):
                    {
                        if ((supplier.new_isexternalsupplier.HasValue
                            && supplier.new_isexternalsupplier.Value)
                            || supplier.new_status.Value == (int)DML.Xrm.account.SupplierStatus.Trial
                            || (supplier.new_rechargeenabled.HasValue && supplier.new_rechargeenabled.Value))
                        {
                            return true;
                        }
                        eNotificationMethod = GetNotificationMethod(DML.NotificationConfigurationKeys.NOTIFICATION_SUPPLIER_BALANCE_UNDER_INCIDENT_PRICE);
                        if (eNotificationMethod != DML.enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = DML.TemplateNames.SUPPLIER_BALANCE_UNDER_INCIDENT_PRICE;
                            SMSTemplateName = DML.TemplateNames.SUPPLIER_BALANCE_UNDER_INCIDENT_PRICE_SMS;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.INCIDENT_INFO):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_SUPPLIER_INCIDENT_INFO);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.SUPPLIER_INCIDENT_INFO;
                            SMSTemplateName = TemplateNames.SUPPLIER_INCIDENT_INFO_SMS;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.SUPPLIER_UNREACHED):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_SUPPLIER_UNREACHED);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.SUPPLIER_UNREACHED_INFO;
                            SMSTemplateName = TemplateNames.SUPPLIER_UNREACHED_INFO_SMS;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.SUPPLIER_UNREACHED_GOLDEN_NUMBER_ADV):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_SUPPLIER_UNREACHED_GOLDEN_NUMBER_ADV);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.SUPPLIER_UNREACHED_INFO_GOLDEN_NUMBER_ADV;
                            SMSTemplateName = TemplateNames.SUPPLIER_UNREACHED_INFO_GOLDEN_NUMBER_ADV_SMS;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.SUPPLIER_SIGNUP):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_SUPPLIER_SIGNUP);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.ADVERTISER_WELCOME_BY_MAIL;
                            SMSTemplateName = TemplateNames.ADVERTISER_WELCOME_BY_SMS;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.LOW_BALANCE):
                    {
                        if ((supplier.new_isexternalsupplier.HasValue
                            && supplier.new_isexternalsupplier.Value)
                            || supplier.new_status.Value == (int)DML.Xrm.account.SupplierStatus.Trial
                            || (supplier.new_rechargeenabled.HasValue && supplier.new_rechargeenabled.Value))
                        {
                            return true;
                        }
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_SUPPLIER_LOW_BALANCE);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.SUPPLIER_LOW_AMOUNT_SMS;
                            SMSTemplateName = TemplateNames.SUPPLIER_LOW_AMOUNT_SMS;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.REFUNDED):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_SUPPLIER_REFUNDED);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.SUPPLIER_REFUNDED;
                            SMSTemplateName = TemplateNames.SUPPLIER_REFUNDED;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.AFTER_FIRST_PAYMENT):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_AFTER_FIRST_PAYMENT);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.AFTER_FIRST_PAYMENT_SMS;
                            SMSTemplateName = TemplateNames.AFTER_FIRST_PAYMENT_SMS;
                        }
                        break;
                    }
                case (enumSupplierNotificationTypes.REMOVE_ME_FROM_TRIAL):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_REMOVE_ME_FROM_TRAIL);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.REMOVE_ME_FROM_TRAIL_SMS;
                            SMSTemplateName = TemplateNames.REMOVE_ME_FROM_TRAIL_SMS;
                        }
                        break;
                    }
            }

            bool sendMail = false;
            bool sendSMS = false;

            string emailaddress1 = supplier.emailaddress1;

            if (emailaddress1 != "" && EmailTemplateName != string.Empty)
            {
                sendMail = true;
            }

            string mainPhone = supplier.telephone1;
            string smsPhone = supplier.telephone3;

            string phonenumber = string.Empty;
            if (SMSTemplateName != string.Empty)
            {
                if (Phones.IsMobilePhone(smsPhone))
                {
                    phonenumber = smsPhone;
                }
                else if (Phones.IsMobilePhone(mainPhone))
                {
                    phonenumber = mainPhone;
                }
                if (phonenumber != string.Empty)
                {
                    sendSMS = true;
                }
            }

            switch (eNotificationMethod)
            {
                case enumNotificationMethod.NONE:
                    {
                        break;
                    }
                case enumNotificationMethod.ONLY_MAIL:
                    {
                        if (sendMail)
                        {
                            notificationSent = SendMailTemplate(EmailTemplateName, supplierID, "account", new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                        }
                        break;
                    }
                case enumNotificationMethod.ONLY_SMS:
                    {
                        if (sendSMS && !(supplier.new_donotsendsms.HasValue && supplier.new_donotsendsms.Value))
                        {
                            notificationSent = SendSMSTemplate(SMSTemplateName, phonenumber, supplier.new_country, new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                        }
                        break;
                    }
                case enumNotificationMethod.MAIL_OR_SMS:
                    {
                        if (sendMail)
                        {
                            notificationSent = SendMailTemplate(EmailTemplateName, supplierID, "account", new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                            sendSMS = !notificationSent;
                        }
                        if (sendSMS && !(supplier.new_donotsendsms.HasValue && supplier.new_donotsendsms.Value))
                        {
                            notificationSent = SendSMSTemplate(SMSTemplateName, phonenumber, supplier.new_country, new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                        }
                        break;
                    }
                case enumNotificationMethod.SMS_OR_MAIL:
                    {
                        if (sendSMS && !(supplier.new_donotsendsms.HasValue && supplier.new_donotsendsms.Value))
                        {
                            notificationSent = SendSMSTemplate(SMSTemplateName, phonenumber, supplier.new_country, new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                            sendMail = !notificationSent;
                        }
                        if (sendMail)
                        {
                            notificationSent = SendMailTemplate(EmailTemplateName, supplierID, "account", new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                        }
                        break;
                    }
                case enumNotificationMethod.MAIL_AND_SMS:
                    {
                        if (sendMail)
                        {
                            notificationSent = SendMailTemplate(EmailTemplateName, supplierID, "account", new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                        }
                        if (sendSMS && !(supplier.new_donotsendsms.HasValue && supplier.new_donotsendsms.Value))
                        {
                            bool smsSent = SendSMSTemplate(SMSTemplateName, phonenumber, supplier.new_country, new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);

                            if (notificationSent)
                            {
                                notificationSent = smsSent;
                            }
                        }
                        break;
                    }
            }

            return notificationSent;
        }

        public bool NotifyCustomer(DML.enumCustomerNotificationTypes customerNotificationType, Guid customerID, string regardingObjectID, string regardingObjectType, string templateObjectID, string templateObjectType)
        {
            bool notificationSent = false;
            DataAccessLayer.Contact contactAccess = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            DML.Xrm.contact customer = contactAccess.Retrieve(customerID);

            if (customer == null)
            {
                LogUtils.MyHandle.WriteToLog(1, "NotifyCustomer Failed with Customer id not found.");
                return false;
            }
            if (customer.new_isfromapi.IsTrue())
                return false;

            string EmailTemplateName = string.Empty;
            string SMSTemplateName = string.Empty;
            enumNotificationMethod eNotificationMethod = enumNotificationMethod.NONE;

            switch (customerNotificationType)
            {
                case (enumCustomerNotificationTypes.AFTER_CANDIDATE_CONNECTION_TO_CONSUMER):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOFIFICATION_AFTER_CANDIDATE_CONNECTION_TO_CONSUMER);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            SMSTemplateName = TemplateNames.AFTER_CANDIDATE_CONNECTION_TO_CONSUMER;
                            EmailTemplateName = TemplateNames.AFTER_CANDIDATE_CONNECTION_TO_CONSUMER;
                        }
                        break;
                    }
                case (enumCustomerNotificationTypes.CONSUMER_UNAVAILABLE):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_CONSUMER_CONSUMER_UNAVAILABLE);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            SMSTemplateName = TemplateNames.CONSUMER_CONSUMER_UNAVAILABLE_SMS;
                        }
                        break;
                    }
                case (enumCustomerNotificationTypes.NEW_SERVICE_REQUEST):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_CUSTOMER_NEW_SERVICE_REQUEST);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.CUSTOMER_NEW_SERVICE_REQUEST;
                            SMSTemplateName = TemplateNames.CUSTOMER_NEW_SERVICE_REQUEST_SMS;
                        }
                        break;
                    }
                case (enumCustomerNotificationTypes.SUPPLIERS_CONTACTED):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_CUSTOMER_ALL_SUPPLIERS_CONTACTED);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.CUSTOMER_ALL_SUPPLIERS_CONTACTED;
                            SMSTemplateName = TemplateNames.CUSTOMER_ALL_SUPPLIERS_CONTACTED_SMS;
                        }
                        break;
                    }
                case (enumCustomerNotificationTypes.SUPPLIER_INFO):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_CUSTOMER_SUPPLIER_INFO);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.CUSTOMER_SUPPLIER_INFO;
                            SMSTemplateName = TemplateNames.CUSTOMER_SUPPLIER_INFO_SMS;
                        }
                        break;
                    }
                case (enumCustomerNotificationTypes.CUSTOMER_NOT_AVAILABLE):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_CUSTOMER_NOT_AVAILABLE);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.CUSTOMER_NOT_AVAILABLE;
                            SMSTemplateName = TemplateNames.CUSTOMER_NOT_AVAILABLE_SMS;
                        }
                        break;
                    }
                case (enumCustomerNotificationTypes.SUPPLIER_UNREACHED):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_CUSTOMER_SUPPLIER_UNREACHED);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = TemplateNames.CUSTOMER_SUPPLIER_UNREACHED;
                            SMSTemplateName = TemplateNames.CUSTOMER_SUPPLIER_UNREACHED_SMS;
                        }
                        break;
                    }
                case (enumCustomerNotificationTypes.DIRECT_NUMBERS):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_CUSTOMER_RECHED_DIRECT_NUMBERS);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = string.Empty;
                            SMSTemplateName = TemplateNames.CUSTOMER_REACHED_DIRECT_NUMBERS;
                        }
                        break;
                    }
                case (enumCustomerNotificationTypes.DIRECT_NUMBERS_UNRECHED):
                    {
                        eNotificationMethod = GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_CUSTOMER_UNREACHED_DIRECT_NUMBERS);
                        if (eNotificationMethod != enumNotificationMethod.NONE)
                        {
                            EmailTemplateName = string.Empty;
                            SMSTemplateName = TemplateNames.CUSTOMER_UNREACHED_DIRECT_NUMBERS;
                        }
                        break;
                    }
            }

            bool sendSMS = false;
            bool sendMail = false;

            string emailaddress1 = customer.emailaddress1;

            if (emailaddress1 != "" && EmailTemplateName != string.Empty)
            {
                sendMail = true;
            }

            string mobilephone = customer.mobilephone;
            string telephone2 = customer.telephone2;

            string phonenumber = string.Empty;
            if (SMSTemplateName != string.Empty)
            {
                if (Phones.IsMobilePhone(mobilephone))
                {
                    phonenumber = mobilephone;
                }
                else if (Phones.IsMobilePhone(telephone2))
                {
                    phonenumber = telephone2;
                }


                if (phonenumber != string.Empty)
                {
                    sendSMS = true;
                }
            }

            switch (eNotificationMethod)
            {
                case enumNotificationMethod.NONE:
                    {
                        break;
                    }
                case enumNotificationMethod.ONLY_MAIL:
                    {
                        if (sendMail)
                        {
                            notificationSent = SendMailTemplate(EmailTemplateName, customerID, "contact", new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                        }
                        break;
                    }
                case enumNotificationMethod.ONLY_SMS:
                    {
                        if (sendSMS)
                        {
                            notificationSent = SendSMSTemplate(SMSTemplateName, phonenumber, customer.new_country, new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                        }
                        break;
                    }
                case enumNotificationMethod.MAIL_OR_SMS:
                    {
                        if (sendMail)
                        {
                            notificationSent = SendMailTemplate(EmailTemplateName, customerID, "contact", new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                            sendSMS = !notificationSent;
                        }
                        if (sendSMS)
                        {
                            notificationSent = SendSMSTemplate(SMSTemplateName, phonenumber, customer.new_country, new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                        }
                        break;
                    }
                case enumNotificationMethod.SMS_OR_MAIL:
                    {
                        if (sendSMS)
                        {
                            notificationSent = SendSMSTemplate(SMSTemplateName, phonenumber, customer.new_country, new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                            sendMail = !notificationSent;
                        }
                        if (sendMail)
                        {
                            notificationSent = SendMailTemplate(EmailTemplateName, customerID, "contact", new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                        }
                        break;
                    }
                case enumNotificationMethod.MAIL_AND_SMS:
                    {
                        if (sendMail)
                        {
                            notificationSent = SendMailTemplate(EmailTemplateName, customerID, "contact", new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);
                        }
                        if (sendSMS)
                        {
                            bool smsSent = SendSMSTemplate(SMSTemplateName, phonenumber, customer.new_country, new Guid(regardingObjectID), regardingObjectType, new Guid(templateObjectID), templateObjectType);

                            if (notificationSent)
                            {
                                notificationSent = smsSent;
                            }
                        }
                        break;
                    }
            }
            return notificationSent;
        }

        public bool SendMailTemplate(string emailTemplateName, Guid recipientID, string recipientType, Guid regardingObjectID, string regardingObjectType, Guid templateObjectID, string templateObjectType)
        {
            try
            {
                string userName = ConfigurationUtils.GetConfigurationItem("serviceUserName");
                string domain = ConfigurationUtils.GetConfigurationItem("serviceDomain");

                DML.Xrm.systemuser user = XrmDataContext.systemusers.FirstOrDefault(x => x.domainname == domain + "\\" + userName);
                //BusinessEntity[] users = MethodsUtils.FindEntities(CrmService, "systemuser", new string[] { "domainname" }, new object[] { domain + "\\" + userName }, "", "", true);

                if (user == null)
                {
                    return false;
                }

                EmailNotificationObject NotificationDetails = new EmailNotificationObject();

                NotificationDetails.EmailOwner = user.systemuserid;
                NotificationDetails.To = new Guid[1];
                NotificationDetails.To[0] = recipientID;
                NotificationDetails.ToEntityName = recipientType;

                NotificationDetails.From = user.systemuserid;

                NotificationDetails.RegardingObjectID = regardingObjectID;
                NotificationDetails.RegardingObjectType = regardingObjectType;

                EmailNotificationSender EmailSender = new EmailNotificationSender(null);

                EmailSender.CrmUserDomain = ConfigurationManager.AppSettings["serviceDomain"];
                EmailSender.CrmUserName = ConfigurationManager.AppSettings["serviceUserName"];
                EmailSender.CrmUserPassword = ConfigurationManager.AppSettings["servicePassword"];

                EmailSender.SendEffectTemplate(emailTemplateName, NotificationDetails, templateObjectID, templateObjectType);

                return true;
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SendMailTemplate Failed with exception. emailTemplateName: {0}, recipientID:{1}, recipientType: {2}, regardingObjectID: {3}, regardingObjectType: {4}, templateObjectID: {5}, templateObjectType: {6}", emailTemplateName, recipientID, recipientType, regardingObjectID, regardingObjectType, templateObjectID, templateObjectType);
                return false;
            }
        }

        public bool SendSMSTemplate(string templateName, string telephoneTo, string phoneCountry, Guid regardingObjectID, string regardingObjectType, Guid templateObjectID, string templateObjectType)
        {
            try
            {
                DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                SMSNotificationObject notificationDetails = new SMSNotificationObject();
                notificationDetails.FromPhoneNumber = configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.SMS_CALLER_ID);
                notificationDetails.SMSAccount = "onecall";
                notificationDetails.SMSUser = "noproblem";
                notificationDetails.SMSPassword = "nuwx5p6e";
                notificationDetails.ToPhoneNumber = telephoneTo;
                notificationDetails.PhoneOriginal = telephoneTo;
                notificationDetails.NumOfTries = 0;
                notificationDetails.PhoneCountry = phoneCountry;

                if (notificationDetails.ToPhoneNumber == string.Empty)
                {
                    return false;
                }

                notificationDetails.RegardingObjectID = regardingObjectID;
                notificationDetails.RegardingObjectType = regardingObjectType;

                SMSNotificationSender SMSSender = new SMSNotificationSender(null);

                SMSSender.CrmUserDomain = ConfigurationManager.AppSettings["serviceDomain"];
                SMSSender.CrmUserName = ConfigurationManager.AppSettings["serviceUserName"];
                SMSSender.CrmUserPassword = ConfigurationManager.AppSettings["servicePassword"];

                SMSSender.OnSendingSms += new SMSNotificationSender.onSendingSms(SMSSender_OnSendingSms);
                SMSSender.OnSmsSent += new SMSNotificationSender.onSmsSent(SMSSender_OnSmsSent);
                SMSSender.OnSmsSendFailed += new SMSNotificationSender.onSmsSendFailed(SMSSender_OnSmsSendFailed);
                return SMSSender.SendEffectTemplate(templateName, notificationDetails, templateObjectID, templateObjectType);

            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SendSMSTemplate Failed. templateName: {0}. telephoneTo: {1}. regardingObjectId: {2}. regardingObjectType: {3}. templateObjectID: {4}. templateObjectType: {5}.", templateName, telephoneTo, regardingObjectID, regardingObjectType, templateObjectID, templateObjectType);
                return false;
            }
        }

        public bool SendSMSTemplate(string body, string telephoneTo, string phoneCountry)
        {
            try
            {
                DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                SMSNotificationObject notificationDetails = new SMSNotificationObject();
                notificationDetails.FromPhoneNumber = configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.SMS_CALLER_ID);
                notificationDetails.SMSAccount = "onecall";
                notificationDetails.SMSUser = "noproblem";
                notificationDetails.SMSPassword = "nuwx5p6e";
                notificationDetails.ToPhoneNumber = telephoneTo;
                notificationDetails.PhoneOriginal = telephoneTo;
                notificationDetails.NumOfTries = 0;
                notificationDetails.BodyText = body;
                notificationDetails.PhoneCountry = phoneCountry;

                if (notificationDetails.ToPhoneNumber == string.Empty)
                {
                    return false;
                }
                SMSNotificationSender SMSSender = new SMSNotificationSender(null);

                SMSSender.CrmUserDomain = ConfigurationManager.AppSettings["serviceDomain"];
                SMSSender.CrmUserName = ConfigurationManager.AppSettings["serviceUserName"];
                SMSSender.CrmUserPassword = ConfigurationManager.AppSettings["servicePassword"];

                SMSSender.OnSendingSms += new SMSNotificationSender.onSendingSms(SMSSender_OnSendingSms);
                SMSSender.OnSmsSent += new SMSNotificationSender.onSmsSent(SMSSender_OnSmsSent);
                SMSSender.OnSmsSendFailed += new SMSNotificationSender.onSmsSendFailed(SMSSender_OnSmsSendFailed);
                return SMSSender.SendEffectTemplate(null, notificationDetails, Guid.Empty, null);

            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SendSMSTemplate Failed. body: {0}. telephoneTo: {1}.", body, telephoneTo);
                return false;
            }
        }
        public bool SendSMSTemplate(string body, string telephoneTo, string phoneCountry, string FromPhoneNumber)
        {
            try
            {
                DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                SMSNotificationObject notificationDetails = new SMSNotificationObject();
                notificationDetails.FromPhoneNumber = FromPhoneNumber;// configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.SMS_CALLER_ID);
                notificationDetails.SMSAccount = "onecall";
                notificationDetails.SMSUser = "noproblem";
                notificationDetails.SMSPassword = "nuwx5p6e";
                notificationDetails.ToPhoneNumber = telephoneTo;
                notificationDetails.PhoneOriginal = telephoneTo;
                notificationDetails.NumOfTries = 0;
                notificationDetails.BodyText = body;
                notificationDetails.PhoneCountry = phoneCountry;

                if (notificationDetails.ToPhoneNumber == string.Empty)
                {
                    return false;
                }
                SMSNotificationSender SMSSender = new SMSNotificationSender(null);

                SMSSender.CrmUserDomain = ConfigurationManager.AppSettings["serviceDomain"];
                SMSSender.CrmUserName = ConfigurationManager.AppSettings["serviceUserName"];
                SMSSender.CrmUserPassword = ConfigurationManager.AppSettings["servicePassword"];
                
                SMSSender.OnSendingSms += new SMSNotificationSender.onSendingSms(SMSSender_OnSendingSms);
                SMSSender.OnSmsSent += new SMSNotificationSender.onSmsSent(SMSSender_OnSmsSent);
                SMSSender.OnSmsSendFailed += new SMSNotificationSender.onSmsSendFailed(SMSSender_OnSmsSendFailed);
                 
                /*
                SMSSender.OnSendingSms += new SMSNotificationSender.onSendingSms(delegate(NotificationObect NotificationDetails) { });
                SMSSender.OnSmsSent += new SMSNotificationSender.onSmsSent(delegate(NotificationObect NotificationDetails) { });
                SMSSender.OnSmsSendFailed += new SMSNotificationSender.onSmsSendFailed(delegate(NotificationObect NotificationDetails) 
                    {
                        SMSNotificationObject smsNotification = NotificationDetails as SMSNotificationObject;
                        
                        DataAccessLayer.ConfigurationSettings _configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                        string amount = configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.SEND_SMS_RETRY_AMOUNT);
                        
                        return smsNotification.NumOfTries < (amount == "" ? 0 : int.Parse(amount));
                    });
                */
                return SMSSender.SendEffectTemplate(null, notificationDetails, Guid.Empty, null);

            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "SendSMSTemplate Failed. body: {0}. telephoneTo: {1}.", body, telephoneTo);
                return false;
            }
        }
        /// <summary>
        /// This method returns the value of the notification method according to the key
        /// </summary>
        /// <param name="Key">A string matching a Key in the Configuration Settings entity</param>
        /// <returns>Notification Method according to the key or ONLY_SMS as default</returns>
        public DML.enumNotificationMethod GetNotificationMethod(string Key)
        {
            try
            {
                DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string NotificationMethod = configSettingsAccess.GetConfigurationSettingValue(Key);

                if (NotificationMethod == null || NotificationMethod == string.Empty || NotificationMethod == "")
                {
                    return enumNotificationMethod.ONLY_SMS;
                }

                int nNotificationMethod = int.Parse(NotificationMethod);

                return (enumNotificationMethod)nNotificationMethod;
            }
            catch
            {
                return enumNotificationMethod.ONLY_SMS;
            }
        }

        #region SMS methods
        private void SMSSender_OnSendingSms(NotificationObect NotificationDetails)
        {
            SMSNotificationObject smsNotification = NotificationDetails as SMSNotificationObject;
            if (smsNotification.UniqueId == null)
            {
                getFirstSmsSupplier(smsNotification);
                DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                Guid smsGuid = createSms(smsNotification.FromPhoneNumber, smsNotification.ToPhoneNumber.Replace("+972", "0"), smsNotification.BodyText, configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.SMS_CALLER_ID), smsNotification.SmsSupplierId);
                if (smsGuid != Guid.Empty)
                {
                    smsNotification.UniqueId = smsGuid.ToString();
                }
            }
        }

        private void SMSSender_OnSmsSent(NotificationObect NotificationDetails)
        {
            SMSNotificationObject smsNotification = NotificationDetails as SMSNotificationObject;
            updateSmsSmsSupplierAndMessageId(new Guid(smsNotification.UniqueId), smsNotification.SmsSupplierId, smsNotification.MessageId);
        }

        private bool SMSSender_OnSmsSendFailed(NotificationObect NotificationDetails)
        {
            SMSNotificationObject smsNotification = NotificationDetails as SMSNotificationObject;
            GetNextSmsSupplier(smsNotification);
            DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string amount = configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.SEND_SMS_RETRY_AMOUNT);
            LogUtils.MyHandle.WriteToLog(8, "amount: {0}", amount);
            LogUtils.MyHandle.WriteToLog(8, "Try to Send again: {0}", smsNotification.NumOfTries < (amount == "" ? 0 : int.Parse(amount)));
            return smsNotification.NumOfTries < (amount == "" ? 0 : int.Parse(amount));
        }

        public void updateSmsSmsSupplierAndMessageId(Guid smsId, Guid smsSupplierId, string messageId)
        {
            //DynamicEntity sms = ConvertorCRMParams.GetNewDynamicEntity("new_sms");
            DataAccessLayer.SMS smsAccess = new NoProblem.Core.DataAccessLayer.SMS(XrmDataContext);
            DML.Xrm.new_sms sms = smsAccess.Retrieve(smsId);

            //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_smsid", ConvertorCRMParams.GetCrmKey(smsId), typeof(KeyProperty));
            sms.new_smssupplierid = smsSupplierId;
            //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_smssupplierid", ConvertorCRMParams.GetCrmLookup("new_smssupplier", smsSupplierId), typeof(LookupProperty));

            if (messageId == null || messageId.Trim() == string.Empty)// Shamir
            {
                //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_messageid", smsId.ToString(), typeof(StringProperty));
                sms.new_messageid = smsId.ToString();
            }
            else
            {
                //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_messageid", messageId, typeof(StringProperty));
                sms.new_messageid = messageId;
            }
            smsAccess.Update(sms);
            XrmDataContext.SaveChanges();
            //MethodsUtils.UpdateEntity(CrmService, sms);
        }

        public void getFirstSmsSupplier(NotificationObect NotificationDetails)
        {
            SMSNotificationObject smsNotification = NotificationDetails as SMSNotificationObject;

            string getNextSmsSupplierQuery = @"SELECT new_smssupplierid,new_messagetemplate,new_url,new_workingmethod,new_encodingname,new_senddata,new_name,new_addprefix
         FROM new_smssupplier with (nolock)    
         WHERE new_isfirst = 1 AND deletionstatecode = 0 and statecode = 0";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(getNextSmsSupplierQuery, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader nextSupplier = command.ExecuteReader())
                    {
                        if (nextSupplier.Read())
                        {
                            if (nextSupplier["new_smssupplierid"] != DBNull.Value)
                                smsNotification.SmsSupplierId = (Guid)nextSupplier["new_smssupplierid"];

                            if (nextSupplier["new_workingmethod"] != DBNull.Value)
                                smsNotification.WorkingMethod = (SMSWorkingMethods)nextSupplier["new_workingmethod"];

                            if (nextSupplier["new_senddata"] != DBNull.Value)
                                smsNotification.SendDataMethod = (SMSSendData)nextSupplier["new_senddata"];

                            if (nextSupplier["new_encodingname"] != DBNull.Value)
                                smsNotification.Encoding = nextSupplier["new_encodingname"].ToString();

                            if (nextSupplier["new_url"] != DBNull.Value)
                                smsNotification.URL = nextSupplier["new_url"].ToString();

                            if (nextSupplier["new_messagetemplate"] != DBNull.Value)
                                smsNotification.DataTemplate = nextSupplier["new_messagetemplate"].ToString();

                            if (nextSupplier["new_name"] != DBNull.Value)
                                smsNotification.Name = nextSupplier["new_name"].ToString();

                            object addPrefixObj = nextSupplier["new_addprefix"];
                            if (addPrefixObj != DBNull.Value && (bool)addPrefixObj)
                            {
                                smsNotification.ToPhoneNumber = Dialer.PhoneToSmsNormalizer.NormalizePhone(smsNotification.PhoneOriginal, smsNotification.PhoneCountry);
                            }
                        }
                    }
                }
            }
        }

        private void GetNextSmsSupplier(NotificationObect NotificationDetails)
        {
            SMSNotificationObject smsNotification = NotificationDetails as SMSNotificationObject;

            string getNextSmsSupplierQuery = @"SELECT nextSupplier.new_smssupplierid,nextSupplier.new_messagetemplate,
         nextSupplier.new_url,nextSupplier.new_workingmethod,nextSupplier.new_encodingname,nextSupplier.new_senddata,nextSupplier.new_name,nextSupplier.new_addprefix
         FROM new_smssupplier smsSupplier with (nolock)
         INNER JOIN new_smssupplier nextSupplier with (nolock) ON nextSupplier.new_smssupplierid = smsSupplier.new_nextsmssupplierid AND 
            nextSupplier.deletionstatecode = 0 AND nextSupplier.statecode = 0
         WHERE smsSupplier.new_smssupplierid = @smsSupplierId";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(getNextSmsSupplierQuery, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@smsSupplierId", smsNotification.SmsSupplierId);
                    using (SqlDataReader nextSupplier = command.ExecuteReader())
                    {

                        if (nextSupplier.Read())
                        {
                            if (nextSupplier["new_smssupplierid"] != DBNull.Value)
                                smsNotification.SmsSupplierId = (Guid)nextSupplier["new_smssupplierid"];

                            if (nextSupplier["new_workingmethod"] != DBNull.Value)
                                smsNotification.WorkingMethod = (SMSWorkingMethods)nextSupplier["new_workingmethod"];

                            if (nextSupplier["new_senddata"] != DBNull.Value)
                                smsNotification.SendDataMethod = (SMSSendData)nextSupplier["new_senddata"];

                            if (nextSupplier["new_encodingname"] != DBNull.Value)
                                smsNotification.Encoding = nextSupplier["new_encodingname"].ToString();

                            if (nextSupplier["new_url"] != DBNull.Value)
                                smsNotification.URL = nextSupplier["new_url"].ToString();

                            if (nextSupplier["new_messagetemplate"] != DBNull.Value)
                                smsNotification.DataTemplate = nextSupplier["new_messagetemplate"].ToString();

                            if (nextSupplier["new_name"] != DBNull.Value)
                            {
                                smsNotification.Name = nextSupplier["new_name"].ToString();
                            }

                            object addPrefixObj = nextSupplier["new_addprefix"];
                            if (addPrefixObj != DBNull.Value && (bool)addPrefixObj)
                            {
                                smsNotification.ToPhoneNumber = Dialer.PhoneToSmsNormalizer.NormalizePhone(smsNotification.PhoneOriginal, smsNotification.PhoneCountry);
                            }
                        }
                    }
                }
            }
        }

        public Guid createSms(string from, string to, string body, string presentationPhone, Guid smsSupplierId)
        {
            DML.Xrm.new_sms sms = new NoProblem.Core.DataModel.Xrm.new_sms(XrmDataContext);
            DataAccessLayer.SMS smsAccess = new NoProblem.Core.DataAccessLayer.SMS(XrmDataContext);
            sms.new_sendermobilenumber = from;
            sms.new_recipientmobilenumber = to;
            sms.new_smsbody = body;
            sms.new_presentationphone = presentationPhone;
            sms.new_attemptnumber = 1;
            sms.new_smssupplierid = smsSupplierId;
            sms.new_name = string.Format("To={0}", to);
            smsAccess.Create(sms);
            XrmDataContext.SaveChanges();

            //DynamicEntity sms = ConvertorCRMParams.GetNewDynamicEntity("new_sms");
            //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_sendermobilenumber",
            //      from, typeof(StringProperty));
            //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_recipientmobilenumber",
            //      to, typeof(StringProperty));            
            //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_smsbody",
            //      body, typeof(StringProperty));            
            //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_presentationphone",
            //      presentationPhone, typeof(StringProperty));            
            //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_attemptnumber",
            //      ConvertorCRMParams.GetCrmNumber(1, false), typeof(CrmNumberProperty));            
            //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_smssupplierid",
            //      ConvertorCRMParams.GetCrmLookup("new_smssupplier", smsSupplierId), typeof(LookupProperty));
            //ConvertorCRMParams.SetDynamicEntityPropertyValue(sms, "new_name",
            //      string.Format("To={0}", to), typeof(StringProperty));
            //Guid res = MethodsUtils.CreateEntity(CrmService, sms);
            return sms.new_smsid;
        }

        #endregion
        
    }
}
