﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: Effect.Onecall.BusinessLogic.Refunds.RefundsManager
//  File: RefundsManager.cs
//  Description: Entry point for all operations that have to do with refunds.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using Effect.Crm.Logs;
using Microsoft.Crm.SdkTypeProxy;
using NoProblem.Core.DataModel.Refunds;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Refunds
{
    public class RefundsManager : XrmUserBase
    {
        #region Ctor
        public RefundsManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
        }
        #endregion

        #region Fields
        private DataAccessLayer.IncidentAccount incidentAccountDal;
        private delegate void sendRefundedNotificationDelegate(DML.Xrm.new_incidentaccount incidentAccount);
        #endregion

        #region Public Methods

        public bool RefundFromMobile(DataModel.Refunds.RefundFromMobileRequest request)
        {
            UpsertRefundRequest upsertRequest = new UpsertRefundRequest();
            upsertRequest.IncidentAccountId = request.IncidentAccountId;
            upsertRequest.RefundNoteForAdvertiser = request.Comment;
            upsertRequest.RefundReasonCode = request.ReasonCode;
            upsertRequest.SupplierId = request.SupplierId;
            upsertRequest.RefundStatus = DML.Xrm.new_incidentaccount.RefundSatus.Approved;
            var upsertResult = UpsertRefund(upsertRequest);
            return upsertResult.Status == UpsertRefundStatus.OK;
        }

        public DataModel.Refunds.GetAllRefundReasonsResponse GetAllRefundReasons()
        {
            var response = new GetAllRefundReasonsResponse();
            DataAccessLayer.RefundReason dal = new NoProblem.Core.DataAccessLayer.RefundReason(XrmDataContext);
            List<DataModel.Refunds.RefundReasonData> reasons = dal.GetAll();
            response.RefundReasons = reasons;
            return response;
        }

        public GetAllRefundReasonsResponse GetAllActiveMobileRefundReasons()
        {
            var response = new GetAllRefundReasonsResponse();
            var dal = new DataAccessLayer.RefundReason(XrmDataContext);
            List<RefundReasonData> reasons = dal.GetAllActiveMobileReasons();
            response.RefundReasons = reasons;
            return response;
        }

        public UpsertRefundResponse UpsertRefund(UpsertRefundRequest request)
        {
            UpsertRefundResponse response = new UpsertRefundResponse();
            DML.Xrm.new_incidentaccount incidentAccount;
            if (request.IncidentAccountId == Guid.Empty)
            {
                incidentAccount = incidentAccountDal.GetIncidentAccountByIncidentAndAccount(request.IncidentId, request.SupplierId);
            }
            else
            {
                incidentAccount = incidentAccountDal.Retrieve(request.IncidentAccountId);
            }
            if (incidentAccount.new_tocharge.HasValue == false || incidentAccount.new_tocharge.Value == false)
            {
                response.Status = UpsertRefundStatus.CallWasNotCharged;
                return response;// this call was not charged, there is no refund to talk about. Just for safety.
            }
            if (request.RefundReasonCode.HasValue && request.RefundReasonCode.Value > 0)
            {
                DataAccessLayer.RefundReason refundReasonAccess = new NoProblem.Core.DataAccessLayer.RefundReason(XrmDataContext);
                Guid refundReasonId = refundReasonAccess.GetRefundReasonIdByCode(request.RefundReasonCode.Value);
                incidentAccount.new_refundreasonid = refundReasonId;
            }
            if (!string.IsNullOrEmpty(request.RefundNote))
            {
                incidentAccount.new_refundnote = request.RefundNote;
            }
            if (!string.IsNullOrEmpty(request.RefundNoteForAdvertiser))
            {
                incidentAccount.new_refundnoteforadvertiser = request.RefundNoteForAdvertiser;
            }
            if (request.RefundOwnerId != Guid.Empty)
            {
                incidentAccount.new_refundownerid = request.RefundOwnerId;
            }
            bool giveRefund = false;
            if (request.RefundStatus.HasValue)
            {
                if (request.RefundStatus.Value == DML.Xrm.new_incidentaccount.RefundSatus.Approved && (incidentAccount.new_refundstatus.HasValue == false || incidentAccount.new_refundstatus.Value != (int)DML.Xrm.new_incidentaccount.RefundSatus.Approved))
                {
                    giveRefund = true;
                    string sentenceReady = BuildRefundSentence(incidentAccount);
                    incidentAccount.new_refundnote += sentenceReady;
                }
                if ((request.RefundStatus.Value == DML.Xrm.new_incidentaccount.RefundSatus.Approved
                    ||
                    request.RefundStatus.Value == DML.Xrm.new_incidentaccount.RefundSatus.Cancelled
                    )
                    &&
                    (incidentAccount.new_refundstatus.HasValue == false || (
                    incidentAccount.new_refundstatus.Value != (int)DML.Xrm.new_incidentaccount.RefundSatus.Approved
                    && incidentAccount.new_refundstatus.Value != (int)DML.Xrm.new_incidentaccount.RefundSatus.Cancelled))
                    )
                {
                    incidentAccount.new_refundprocesseddate = DateTime.Now;
                }
                incidentAccount.new_refundstatus = new int?((int)request.RefundStatus.Value);
            }
            if (request.RefundStatus.HasValue == false || request.RefundStatus.Value == NoProblem.Core.DataModel.Xrm.new_incidentaccount.RefundSatus.Pending)
            {
                if (IsAllowedToRefund(incidentAccount) == false)
                {
                    response.Status = UpsertRefundStatus.NoRefundAllowed;
                    return response;
                }
                if (!request.IsBlockApproved)
                {
                    bool willBeOverPercentage = WillBeOverRefundRequestPercentage(request.SupplierId);
                    if (willBeOverPercentage)
                    {
                        response.Status = UpsertRefundStatus.BlockApprovalNeeded;
                        return response;
                    }
                }
                else
                {
                    BlockSupplierForRefundPercentage(request.SupplierId);
                }
                SendRefundAlert(request);
            }
            if (incidentAccount.new_refundrequestdate.HasValue == false)
            {
                incidentAccount.new_refundrequestdate = DateTime.Now;
                incidentAccount.new_refundrequestdatelocal = FixDateToLocal(DateTime.Now);
            }
            incidentAccountDal.Update(incidentAccount);
            XrmDataContext.SaveChanges();

            if (giveRefund)
            {
                response.NewBalance = RefundSupplier(incidentAccount, NoProblem.Core.DataModel.Xrm.new_balancerow.Action.Refund);
                System.Threading.Thread dashBoardDataThread = new System.Threading.Thread(delegate()
                {
                    try
                    {
                        Dashboard.DashboardDataCreator dataCreator = new NoProblem.Core.BusinessLogic.Dashboard.DashboardDataCreator(null);
                        dataCreator.CreateDataAfterRefund(incidentAccount.createdon.Value);
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception in async CreateDataAfterRefund");
                    }
                });
                dashBoardDataThread.Start();
                sendRefundedNotificationDelegate sendSmsAsync = new sendRefundedNotificationDelegate(SendRefundedNotification);
                sendSmsAsync.BeginInvoke(incidentAccount, null, null);
                CheckAffiliatePaymentAfterRefund(incidentAccount);
            }
            response.Status = UpsertRefundStatus.OK;
            return response;
        }

        public GetRefundsReportResponse GetRefundsReport(GetRefundsReportRequest request)
        {
            DateTime fromDate = request.FromDate;
            DateTime toDate = request.ToDate;
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            int totalRows;
            List<DML.Refunds.RefundData> dataList = incidentAccountDal.GetIncidentAccountsByRefundDatesAndRefundStatus(fromDate, toDate, request.RefundStatus, request.PageSize, request.PageNumber, out totalRows);
            GetRefundsReportResponse response = new GetRefundsReportResponse();
            response.TotalRows = totalRows;
            response.CurrentPage = request.PageNumber;
            response.TotalPages = (int)Math.Ceiling(((double)totalRows) / ((double)request.PageSize));
            response.RefundRequests = dataList;
            return response;
        }

        public decimal RefundSupplier(DML.Xrm.new_incidentaccount incidentAccount, DML.Xrm.new_balancerow.Action balanceRowAction)
        {
            decimal retVal = RefundWithCashBankMethod(incidentAccount, balanceRowAction);
            if (balanceRowAction == DML.Xrm.new_balancerow.Action.Interval)
            {
                incidentAccount.new_lostoncustomersfault = true;
                incidentAccountDal.Update(incidentAccount);
                XrmDataContext.SaveChanges();
            }
            return retVal;
        }

        #endregion

        #region Private Methods

        private bool IsAllowedToRefund(NoProblem.Core.DataModel.Xrm.new_incidentaccount incidentAccount)
        {
            var supplier = incidentAccount.new_account_new_incidentaccount;
            if (supplier.new_isrefundblocked.HasValue && supplier.new_isrefundblocked.Value)
            {
                return false;
            }
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string daysLimitStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.REFUND_DAYS_BACK_LIMIT);
            int daysLimit;
            if (int.TryParse(daysLimitStr, out daysLimit) == false)
            {
                daysLimit = 365;
            }
            if (incidentAccount.createdon.Value.Date < DateTime.Now.Date.AddDays(daysLimit * -1))
            {
                return false;
            }
            //if (incidentAccount.statuscode == (int)DataModel.Xrm.new_incidentaccount.Status.INVITED_BY_SMS)
            //{
            //    return false;
            //}
            //DataAccessLayer.Channel channelDal = new NoProblem.Core.DataAccessLayer.Channel(XrmDataContext);
            //Guid callChannelId = channelDal.GetChannelIdByCode(NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Call);
            //if (incidentAccount.new_channelid.HasValue && incidentAccount.new_channelid.Value != callChannelId)
            //{
            //    return false;
            //}
            if (string.IsNullOrEmpty(incidentAccount.new_recordfilelocation))
            {
                string recordStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.RECORD_CALLS);
                bool recordCalls = recordStr == "1";
                if (recordCalls)
                {
                    return false;
                }
            }
            return true;
        }

        private void CheckAffiliatePaymentAfterRefund(NoProblem.Core.DataModel.Xrm.new_incidentaccount incidentAccount)
        {
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentAccount.new_incidentid.Value);
            bool allRefunded = true;
            var refundStatuses = from call in incident.new_incident_new_incidentaccount
                                 where
                                 (call.statuscode == (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS
                                 || call.statuscode == (int)DML.Xrm.new_incidentaccount.Status.CLOSE
                                 || call.statuscode == (int)DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS
                                 )
                                 && call.new_incidentaccountid != incidentAccount.new_incidentaccountid
                                 select
                                 call.new_refundstatus;
            foreach (var item in refundStatuses)
            {
                if (item.HasValue == false || item.Value != (int)DML.Xrm.new_incidentaccount.RefundSatus.Approved)
                {
                    allRefunded = false;
                    break;
                }
            }
            if (allRefunded)
            {
                DataAccessLayer.AffiliatePayStatusReason affiliateDal = new NoProblem.Core.DataAccessLayer.AffiliatePayStatusReason(XrmDataContext);
                Guid payStatusId;
                Guid payReasonId = affiliateDal.GetByRefundReason(incidentAccount.new_refundreasonid.Value, out payStatusId);
                incident.new_affiliatepaystatusid = payStatusId;
                incident.new_affiliatepaystatusreasonid = payReasonId;
                incidentDal.Update(incident);
                XrmDataContext.SaveChanges();
            }
        }

        private void BlockSupplierForRefundPercentage(Guid supplierId)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = accountRepositoryDal.Retrieve(supplierId);
            DataAccessLayer.UnavailabilityReason reasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
            bool changeUnavailability = true;
            if (supplier.new_unavailabilityreasonid.HasValue)
            {
                var outOfMoneyReason = reasonDal.GetPricingEndedReason();
                if (supplier.new_unavailabilityreasonid.Value == outOfMoneyReason.new_unavailabilityreasonid)
                {
                    changeUnavailability = false;
                }
            }
            if (changeUnavailability)
            {
                var reason = reasonDal.GetOverRefundPercentReason();
                DateTime nowLocal = FixDateToLocal(DateTime.Now);
                DateTime dateStartUtc = FixDateToUtcFromLocal(nowLocal.Date);
                supplier.new_unavailablefrom = dateStartUtc;
                supplier.new_unavailablelocalfrom = nowLocal.Date;
                supplier.new_unavailableto = dateStartUtc.AddDays(30);
                supplier.new_unavailablelocalto = nowLocal.Date.AddDays(30);
                supplier.new_unavailabilityreasonid = reason.new_unavailabilityreasonid;
            }
            supplier.new_isrefundblocked = true;
            accountRepositoryDal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        private bool WillBeOverRefundRequestPercentage(Guid supplierId)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            double refundRequestPercent = accountRepositoryDal.GetRefundRequestPercent(supplierId, true);
            string limitStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.REFUND_REQUEST_PERCENTAGE_LIMIT);
            int limitPercent;
            if (int.TryParse(limitStr, out limitPercent) == false)
            {
                limitPercent = 101;
            }
            return refundRequestPercent >= limitPercent;
        }

        private void SendRefundedNotification(DML.Xrm.new_incidentaccount incidentAccount)
        {
            //send null data context because this method runs asyc, so it need its own context.
            Notifications notifier = new Notifications(null);
            notifier.NotifySupplier(NoProblem.Core.DataModel.enumSupplierNotificationTypes.REFUNDED,
                incidentAccount.new_accountid.Value, incidentAccount.new_incidentid.Value.ToString(),
                 "incident",
                 incidentAccount.new_incidentid.Value.ToString(), "incident");
        }

        private void SendRefundAlert(UpsertRefundRequest request)
        {
            System.Threading.Thread t1 = new System.Threading.Thread(delegate()
            {
                try
                {
                    DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
                    string alertUserIdStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.REFUND_ALERT_USER);
                    if (string.IsNullOrEmpty(alertUserIdStr) == false)
                    {

                        Guid userId = new Guid(alertUserIdStr);
                        DataAccessLayer.AccountRepository accDal = new NoProblem.Core.DataAccessLayer.AccountRepository(configDal.XrmDataContext);
                        string email = accDal.GetEmail(userId);
                        string subject = string.Empty;
                        string body = string.Empty;
                        Notifications notifier = new Notifications(configDal.XrmDataContext);
                        notifier.InstatiateTemplate(DML.TemplateNames.REFUND_ALERT, userId, "account", out subject, out body);
                        notifier.SendEmail(email, body, subject);
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "SendRefundAlert failed with exception");
                }
            });
            t1.Start();
        }

        private string BuildRefundSentence(DML.Xrm.new_incidentaccount incidentAccount)
        {
            DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string sentence = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.REFUND_SENTENCE);
            DataAccessLayer.SystemUser userDAL = new NoProblem.Core.DataAccessLayer.SystemUser(XrmDataContext);
            DML.Xrm.systemuser user = userDAL.GetCurrentUser();
            DataAccessLayer.UserSettings settingsDAL = new NoProblem.Core.DataAccessLayer.UserSettings(XrmDataContext);
            string currencySymbol = settingsDAL.GetUsersDefaultCurrencySymbol(user.systemuserid);
            string moneyString = currencySymbol + " ";
            moneyString += incidentAccount.new_potentialincidentprice.Value.ToString("0,0.00");
            DateTime timeOfRefund = FixDateToLocal(DateTime.Now);
            string sentenceReady = string.Format(sentence, moneyString, timeOfRefund);
            return "\n" + sentenceReady;
        }

        private decimal RefundWithCashBankMethod(DML.Xrm.new_incidentaccount incidentAccount, DataModel.Xrm.new_balancerow.Action balanceRowAction)
        {
            decimal pricePaid = incidentAccount.new_potentialincidentprice.Value;
            incidentAccount.new_tocharge = new bool?(false);
            incidentAccountDal.Update(incidentAccount);
            XrmDataContext.SaveChanges();
            string leadNumber = incidentAccount.new_incident_new_incidentaccount.ticketnumber;
            Balance.BalanceManager balanceManager = new NoProblem.Core.BusinessLogic.Balance.BalanceManager(XrmDataContext);
            return balanceManager.AddNewCashToAccount(incidentAccount.new_accountid.Value, pricePaid, balanceRowAction, null, incidentAccount.new_incidentaccountid, leadNumber);
        }

        #endregion
    }
}
