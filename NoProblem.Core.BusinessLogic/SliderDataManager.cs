﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.BusinessLogic
{
    public class SliderDataManager : XrmUserBase
    {
        public SliderDataManager(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {

        }

        public List<DataModel.SliderData.ExpertiseData> GetAllExpertiseSliderData()
        {
            DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            var retVal = dal.GetAllExpertiseSliderData();
            return retVal;
        }

        public
            //List<DataModel.SliderData.KeywordHeadingIdPair>
            DataModel.SliderData.KeywordsContainer 
            GetAllSliderKeywords()
        {
            DataAccessLayer.SlikerKeyword dal = new DataAccessLayer.SlikerKeyword(XrmDataContext);
            var retVal = dal.GetAllSliderKeywords();
            return retVal;
        }

        public List<StringBoolPair> GetAllBadDomains()
        {
            DataAccessLayer.BadDomain dal = new NoProblem.Core.DataAccessLayer.BadDomain(XrmDataContext);
            var retVal = dal.GetAllBadDomains();
            return retVal;
        }

        public HashSet<DataModel.SliderData.SpecialCategoryGroup> GetAllSpecialCategroyGroups()
        {
            DataAccessLayer.CategoryGroup dal = new NoProblem.Core.DataAccessLayer.CategoryGroup(XrmDataContext);
            return dal.GetAllSpecialCategoryGroups();
        }

        public HashSet<DataModel.SliderData.CategoryGroupForDisplay> GetCategoryGroupsForDisplay()
        {
            DataAccessLayer.CategoryGroup dal = new NoProblem.Core.DataAccessLayer.CategoryGroup(XrmDataContext);
            return dal.GetCategoryGroupsForDisplay();
        }
    }
}
