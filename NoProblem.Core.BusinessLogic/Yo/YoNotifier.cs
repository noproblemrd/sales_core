﻿using System.Collections.Generic;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Yo
{
    public class YoNotifier : Runnable
    {
        private static Dictionary<eYoApps, string> keys;
        private eYoApps yoApp;
        private const string YO_ALL_URL = "http://api.justyo.co/yoall/";
        private const string API_TOKEN_PARAM_NAME = "api_token";

        public enum eYoApps
        {
            NPNEWADV
        }

        static YoNotifier()
        {
            keys = new Dictionary<eYoApps, string>();
            keys.Add(eYoApps.NPNEWADV, "be774f9b-5bd6-3c48-e8e3-43d1ea20a908");
        }

        public YoNotifier(eYoApps yoApp)
        {
            this.yoApp = yoApp;
        }

        protected override void Run()
        {
            if (GlobalConfigurations.IsProductionUsa())
            {
                RestSharp.RestClient client = new RestSharp.RestClient(YO_ALL_URL);
                RestSharp.RestRequest req = new RestSharp.RestRequest(RestSharp.Method.POST);
                req.AddParameter(API_TOKEN_PARAM_NAME, keys[yoApp]);
                var ans = client.Execute(req);
                if (ans.StatusCode != System.Net.HttpStatusCode.Created)
                {
                    LogUtils.MyHandle.WriteToLog("Yo failed with status {0} and content: {1}", ans.StatusCode, ans.Content);
                }
            }
        }
    }
}
