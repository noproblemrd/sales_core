﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic
{
    public class DistributedLocksManager
    {
        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();

        public enum eLocks
        {
            InjectionsEmails,
            InterYieldApi,
            JollyWalletApi,
            FirstOfferzFtp,
            AutoResubmission,
            AdmediaApi,
            AdcashApi
        }

        public static bool AquireLock(eLocks lockName)
        {
            return ExecuteSql(lockName, true);
        }

        public static bool ReleaseLock(eLocks lockName)
        {
            return ExecuteSql(lockName, false);
        }

        private static bool ExecuteSql(eLocks lockName, bool isAquire)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@name", lockName.ToString());
            string sql = isAquire ? aquireSql : releaseSql;
            int rowsUpdated = dal.ExecuteNonQuery(sql, parameters);
            return rowsUpdated > 0;
        }

        private const string aquireSql = "update DistributedLocks set Locked = 1 where Locked = 0 and Name = @name";

        private const string releaseSql = "update DistributedLocks set Locked = 0 where Locked = 1 and Name = @name";
    }
}
