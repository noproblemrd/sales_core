﻿using System;
using System.Collections.Generic;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.DomainBlackList;

namespace NoProblem.Core.BusinessLogic.DomainBlackList
{
    public class DomainBlackListManager : XrmUserBase
    {
        public DomainBlackListManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        delegate void insertDelegate(List<DomainBlackListData> dataList, string callBackUrl);
        Dictionary<string, Guid> keywordsAndIds;
        private const string cachePrefix = "DomainBlackListManager";
        private const string cacheBlackListKey = "GetAllBlackList";

        public string[][] GetAllBlackList()
        {
            return DataAccessLayer.CacheManager.Instance.Get<string[][]>(cachePrefix, cacheBlackListKey, GetAllBlackListMethod);
        }

        public void Insert(List<DomainBlackListData> dataList, string callBackUrl)
        {
            insertDelegate del = new insertDelegate(InsertPrivate);
            del.BeginInvoke(dataList, callBackUrl, insertDelegateAsyncCallBack, del);
        }

        private void insertDelegateAsyncCallBack(IAsyncResult res)
        {
            try
            {
                ((insertDelegate)res.AsyncState).EndInvoke(res);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DomainBlackListManager.insertDelegateAsyncCallBack.");
            }
        }

        private void InsertPrivate(List<DomainBlackListData> dataList, string callBackUrl)
        {
            LogUtils.MyHandle.WriteToLog("Inserting to Domain black list started");
            int countPassed = 0;
            int countBlack = 0;
            DataAccessLayer.SlikerKeyword dal = new NoProblem.Core.DataAccessLayer.SlikerKeyword(XrmDataContext);
            keywordsAndIds = dal.GetAllKeywordsIds();
            DataAccessLayer.DomainBlackList domainBlackListDal = new NoProblem.Core.DataAccessLayer.DomainBlackList(XrmDataContext);
            DataAccessLayer.BlackListCheckPassed blackListCheckPassedDal = new NoProblem.Core.DataAccessLayer.BlackListCheckPassed(XrmDataContext);
            foreach (var item in dataList)
            {
                try
                {
                    if (item.Passed)
                    {
                        CreateCheckPassed(item, blackListCheckPassedDal);
                        countPassed++;
                    }
                    else
                    {
                        CreateBlackList(item, domainBlackListDal);
                        countBlack++;
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in DomainBlackListManager.InsertPrivate in create loop, continuing to next one.");
                }
            }
            DataAccessLayer.CacheManager.Instance.ClearCache(cachePrefix, cacheBlackListKey);
            if (!string.IsNullOrEmpty(callBackUrl))
            {
                Utils.HttpClient client = new NoProblem.Core.BusinessLogic.Utils.HttpClient(callBackUrl, NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpMethodEnum.Post);
                var postAns = client.Post();
                if (postAns.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    LogUtils.MyHandle.WriteToLog("Clean cache post after inserting black list returned bad HttpStatus. HttpStatus = {0}. Content = {1}.", postAns.StatusCode, postAns.Content);
                }
            }
            LogUtils.MyHandle.WriteToLog("Inserting to Domain black list ended. passed inserted: {0}. black inserted: {1}", countPassed, countBlack);
        }

        private void CreateBlackList(DomainBlackListData item, DataAccessLayer.DomainBlackList domainBlackListDal)
        {
            DataModel.Xrm.new_domainblacklist black = new NoProblem.Core.DataModel.Xrm.new_domainblacklist();
            black.new_sliderkeywordid = FindSliderKeywordId(item.Keyword);
            if (!black.new_sliderkeywordid.HasValue)
            {
                LogUtils.MyHandle.WriteToLog("Problem in DomainBlackListManager.Insert, Slider Keyword not found inserting to black list. Keyword = {0}, domain = {1}", item.Keyword, item.Domain);
                return;
            }
            black.new_domain = item.Domain;
            black.new_requests = item.Requests;
            black.new_exposures = item.Exposures;
            black.new_recruitmentrevenue = item.RecruitmentRevenue;
            black.new_minconversionconst = item.MinConversionConst;
            black.new_dateoffirstexposure = item.DateOfFirstExposure;
            black.new_potentialrevenue = item.PotentialRevenue;
            black.new_netactualrevenue = item.NetActualRevenue;
            black.new_actualrevenue = item.ActualRevenue;
            black.new_minnetactualrpmconst = item.MinNetActualRpmConst;
            black.new_webexposures = item.WebExposures;
            domainBlackListDal.Create(black);
            XrmDataContext.SaveChanges();
        }

        private void CreateCheckPassed(DomainBlackListData item, DataAccessLayer.BlackListCheckPassed blackListCheckPassedDal)
        {
            DataModel.Xrm.new_blacklistcheckpassed passed = new NoProblem.Core.DataModel.Xrm.new_blacklistcheckpassed();
            passed.new_sliderkeywordid = FindSliderKeywordId(item.Keyword);
            if (!passed.new_sliderkeywordid.HasValue)
            {
                LogUtils.MyHandle.WriteToLog("Problem in DomainBlackListManager.Insert, Slider Keyword not found inserting to check passed. Keyword = {0}, domain = {1}", item.Keyword, item.Domain);
                return;
            }
            passed.new_domain = item.Domain;
            passed.new_requests = item.Requests;
            passed.new_exposures = item.Exposures;
            passed.new_minconversionconst = item.MinConversionConst;
            passed.new_minnetactualrpmconst = item.MinNetActualRpmConst;
            passed.new_netactualrevenue = item.NetActualRevenue;
            passed.new_webexposures = item.WebExposures;
            blackListCheckPassedDal.Create(passed);
            XrmDataContext.SaveChanges();
        }

        private Guid? FindSliderKeywordId(string keyword)
        {
            if (keywordsAndIds.ContainsKey(keyword))
            {
                return keywordsAndIds[keyword];
            }
            return null;
        }

        private string[][] GetAllBlackListMethod()
        {
            DataAccessLayer.DomainBlackList dal = new NoProblem.Core.DataAccessLayer.DomainBlackList(XrmDataContext);
            var reader = dal.ExecuteReader(getAllQuery);
            string[][] retVal = new string[reader.Count][];
            for (int i = 0; i < reader.Count; i++)
            {
                retVal[i] = new string[2];
                retVal[i][0] = Convert.ToString(reader[i]["new_keyword"]);
                retVal[i][1] = Convert.ToString(reader[i]["new_domain"]);
            }
            return retVal;
        }

        private const string getAllQuery =
            @"
select k.new_keyword, dbk.new_domain
from 
new_domainblacklist dbk with(nolock)
join new_sliderkeyword k with(nolock) on dbk.new_sliderkeywordid = k.new_sliderkeywordid
where dbk.deletionstatecode = 0
and k.deletionstatecode = 0
and dbk.CreatedOn < '2013-12-01'
";
    }
}
