﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.DomainBlackList
{
    public class  BadDomainManager : XrmUserBase
    {
        public BadDomainManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        void InsertBadDomain(string domain)
        {
             NoProblem.Core.DataModel.Xrm.new_baddomain baddomain = new NoProblem.Core.DataModel.Xrm.new_baddomain();
                baddomain.new_name = domain;
                NoProblem.Core.DataAccessLayer.BadDomain bd = new NoProblem.Core.DataAccessLayer.BadDomain(this.XrmDataContext);
            bd.Create(baddomain);
            XrmDataContext.SaveChanges();
        }
        public int InsertBadDomain(string[] domains)
        {
            HashSet<string> list = new HashSet<string>();
            string command =
                @"select bd.New_name
                    from dbo.New_baddomainExtensionBase bd
	                    inner join dbo.New_baddomainBase b
	                    on b.New_baddomainId = bd.New_baddomainId
                    where b.DeletionStateCode = 0";
            string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        if (reader["New_name"] == DBNull.Value)
                            continue;
                        string domain = (string)reader["New_name"];
                        if (string.IsNullOrEmpty(domain))
                            continue;
                        list.Add(domain);
                    }
                }
                conn.Close();
            }
            int result = 0;
            foreach (string domain in domains)
            {                
                if (string.IsNullOrEmpty(domain) || list.Contains(domain))
                    continue;
                InsertBadDomain(domain);
                list.Add(domain);
                result++;
            }
            return result;
        }
    }
}
