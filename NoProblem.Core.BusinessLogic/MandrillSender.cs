﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic
{
    public class MandrillRequest
    {
        public string To;
        public string Bcc;
        public string Subject;
        public string TemplateName;
        public Dictionary<string, string> Parameters;
        public string FromEmail { get; set; }
        public string FromName { get; set; }
    }
    public class MandrillSender
    {
        public MandrillSender()
        {
            configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
        }

        DataAccessLayer.ConfigurationSettings configDal;
        private const string mandrillApiKey = "9c68e152-f34f-4fc9-ba2a-406139cf78a3";
        private const string REJECTED = "rejected";

        private string _fromEmail;
        private string FromEmail
        {
            get
            {
                return _fromEmail ?? (_fromEmail = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.EMAIL_SENDER));
            }
        }

        public bool SendTemplate(string to, string subject, string templateName)
        {
            return SendTemplate(to, subject, templateName, new Dictionary<string, string>());
        }

        public bool SendTemplate(MandrillRequest request)
        {
            if (request.Parameters == null)
                request.Parameters = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(request.FromEmail))
                request.FromEmail = this.FromEmail;//default value

            if (string.IsNullOrEmpty(request.FromName))
                request.FromName = "The NoProblem Team";//default value

            var client = new MandrillApi.MandrillApi(mandrillApiKey);
            var message = new MandrillApi.Model.MessageTemplate();
            message.to.Add(new MandrillApi.Model.ToItem() { email = request.To });
            message.subject = request.Subject;

            message.from_email = request.FromEmail;
            message.from_name =  request.FromName;
            message.bcc_address = request.Bcc;
            var rcptMergeVars = new MandrillApi.Model.RecepientMergeVars();
            rcptMergeVars.rcpt = request.To;
            foreach (var item in request.Parameters)
            {
                rcptMergeVars.vars.Add(
                    new MandrillApi.Model.MergeVar()
                    {
                        name = item.Key,
                        content = item.Value
                    });
            }
            message.merge_vars.Add(rcptMergeVars);
            List<MandrillApi.Model.RecipientReturn> retVal = client.sendtemplate(request.TemplateName, null, message);
            if (retVal != null)
            {
                var result = retVal[0];
                return result.status != REJECTED;
            }
            else
            {
                return false;
            }
        }

        public bool SendTemplate(string to, string subject, string templateName, Dictionary<string, string> mergeVars)
        {
            return SendTemplate(to, null, subject, templateName, mergeVars);
        }

        public bool SendTemplate(string to, string bcc, string subject, string templateName, Dictionary<string, string> mergeVars)
        {
            var request = new MandrillRequest
            {
                To = to,
                Bcc = bcc,
                Parameters = mergeVars,
                Subject = subject,
                TemplateName = templateName
            };

            bool success = this.SendTemplate(request);
            return success;
        }

        public Dictionary<string, bool> SendTemplate(List<string> to, string subject, string templateName, Dictionary<string, Dictionary<string, string>> mergeVars, Dictionary<string, string> globalMergeVars)
        {
            MandrillApi.MandrillApi client = new MandrillApi.MandrillApi(mandrillApiKey);
            MandrillApi.Model.MessageTemplate message = new MandrillApi.Model.MessageTemplate();
            foreach (var item in to)
            {
                message.to.Add(new MandrillApi.Model.ToItem() { email = item });
            }
            message.from_email = FromEmail;
            message.subject = subject;
            foreach (var rcpt in mergeVars)
            {
                var rcptMergeVars = new MandrillApi.Model.RecepientMergeVars();
                rcptMergeVars.rcpt = rcpt.Key;
                foreach (var item in rcpt.Value)
                {
                    rcptMergeVars.vars.Add(
                        new MandrillApi.Model.MergeVar()
                        {
                            name = item.Key,
                            content = item.Value
                        });
                }
                message.merge_vars.Add(rcptMergeVars);
            }
            foreach (var item in globalMergeVars)
            {
                message.global_merge_vars.Add(
                    new MandrillApi.Model.MergeVar()
                    {
                        name = item.Key,
                        content = item.Value
                    });
            }
            List<MandrillApi.Model.RecipientReturn> results = client.sendtemplate(templateName, null, message);
            Dictionary<string, bool> retVal = new Dictionary<string, bool>();
            foreach (var item in results)
            {
                retVal.Add(item.email, item.status != REJECTED);
            }
            return retVal;
        }
    }
}
