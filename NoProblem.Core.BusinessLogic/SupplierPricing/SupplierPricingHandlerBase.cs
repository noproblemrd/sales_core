﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.BusinessLogic.Utils;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.SupplierPricing
{
    public abstract class SupplierPricingHandlerBase : XrmUserBase
    {
        #region Ctor
        public SupplierPricingHandlerBase(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Protected Methods

        protected string BuildAddressField(CreditCardSolek.InvoiceDataContainer invoiceDataContainer)
        {
            string retVal = invoiceDataContainer.BillingAddress1 + " " +
                invoiceDataContainer.BillingAddress2 + ", " +
                invoiceDataContainer.BillingCity + ", " +
                invoiceDataContainer.BillingState + " " +
                invoiceDataContainer.BillingZip + " " +
                invoiceDataContainer.BillingCountry;
            if (retVal.Length > 1000)
                retVal = retVal.Substring(0, 1000);
            return retVal;
        }

        protected string UpdateTheSuppliersAccount(Guid supplierId, int PaymentAndBonusAmount, DML.Xrm.new_supplierpricing supplierPricing, bool isFromAAR, 
            DML.Xrm.new_balancerow.Action action, string description, out string accountManagerId, bool isFromDollar)
        {
            Balance.BalanceManager balanceManager = new NoProblem.Core.BusinessLogic.Balance.BalanceManager(XrmDataContext);
            decimal newBalance = balanceManager.AddNewCashToAccount(supplierId, PaymentAndBonusAmount, action, supplierPricing.new_supplierpricingid, null, description);
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            //change to approved status 
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(supplierId);
            string accountNumber;
            bool updateNeeded = false;
            if (action == DML.Xrm.new_balancerow.Action.AutoRecharge && supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPAHold)
            {
                supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.PPA;
                updateNeeded = true;
            }
            if (supplier.new_dapazstatus != (int)DataModel.Xrm.account.DapazStatus.PPAHold)
            {
                accountRepositoryDal.SetSupplierRegistrationStage(supplierId, NoProblem.Core.DataModel.Xrm.account.StageInRegistration.PAID);
            }
            if (supplier.new_ismobile.HasValue && supplier.new_ismobile.Value && supplier.new_status != (int)DML.Xrm.account.SupplierStatus.Approved)
            {
                supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Approved;
                updateNeeded = true;
            }
            if ((supplier.new_status == (int)DML.Xrm.account.SupplierStatus.Candidate || (supplier.new_status == (int)DML.Xrm.account.SupplierStatus.Trial && !isFromAAR))
                && supplier.new_dapazstatus != (int)DataModel.Xrm.account.DapazStatus.PPAHold)
            {
                accountNumber = HandleNewActiveSupplier(supplier, accountRepositoryDal);
            }
            else
            {
                accountNumber = supplier.accountnumber;
            }
            if (string.IsNullOrEmpty(accountNumber) || accountNumber == "***")
            {
                DataAccessLayer.Autonumber.AutonumberManager autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
                string nextNumber = autoNumber.GetNextNumber("account");
                supplier.accountnumber = nextNumber;
                accountNumber = supplier.accountnumber;
                updateNeeded = true;
            }
            if (!supplierPricing.new_voucherid.HasValue)
            {
                supplier.new_accountcompleteness_payment = true;
                supplier.new_firstpaymentneeded = false;
                updateNeeded = true;
            }
            //check this was created by a publisher user (and not automatic nor the advertiser himself)
            //If so, the user becomes the advertiser's account manager.
            //  --------- Removed by Ofir Shapirovitz request on August 7 2013 ------------------
            //if (createdByUserId != Guid.Empty && createdByUserId != supplier.accountid)
            //{
            //    supplier.new_managerid = createdByUserId;
            //    updateNeeded = true;
            //}
            if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.RarahQuote)
            {
                supplier.new_rarahquotestartdate = DateTime.Now;
                supplier.new_rarahquotestart = 5;
                supplier.new_rarahquoteleft = 5;
                updateNeeded = true;
            }
            if (isFromDollar)
            {
                supplier.new_isdollaraccount = true;
                updateNeeded = true;
            }
            if (!supplier.new_stepinregistration2014.HasValue || supplier.new_stepinregistration2014.Value < (int)DataModel.Xrm.account.StepInRegistration2014.Dashboard_7)
            {
                supplier.new_stepinregistration2014 = (int)DataModel.Xrm.account.StepInRegistration2014.Dashboard_7;
                updateNeeded = true;
            }
            if (supplier.new_paymentmethodfailed == true)
            {
                supplier.new_paymentmethodfailed = false;
                updateNeeded = true;
            }
            if (String.IsNullOrEmpty(supplier.new_steplegacy2014) || supplier.new_steplegacy2014 != DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.FINISHED_3)
            {
                supplier.new_steplegacy2014 = DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.FINISHED_3;
                updateNeeded = true;
            }
            if (supplier.new_paymentmethodauthorizationneeded == true)
            {
                supplier.new_paymentmethodauthorizationneeded = false;
                updateNeeded = true;
            }
            if (updateNeeded)
            {
                accountRepositoryDal.Update(supplier);
                XrmDataContext.SaveChanges();
            }

            accountManagerId = "3281";//Saar
            if (supplier.new_managerid.HasValue && !string.IsNullOrEmpty(supplier.new_account_account_account.new_userbizid))
            {
                accountManagerId = supplier.new_account_account_account.new_userbizid;
            }

            if (!supplier.new_disableautodirectnumbers.HasValue || !supplier.new_disableautodirectnumbers.Value)
            {
                //SupplierManager supManager = new SupplierManager(XrmDataContext);
                //var supStatus = supManager.GetSupplierStatus(supplierId);
                //if (supStatus.SupplierStatus == NoProblem.Core.DataModel.SupplierModel.SupplierStatus.Available)
                //{
                DirectNumbersAssigner assigner = new DirectNumbersAssigner();
                assigner.AssignDirectNumbersToSupplier(supplierId);
                //}
            }
            if (Dapaz.PpaOnlineSync.SyncConfiguration.IsEnabled)
            {
                Dapaz.PpaOnlineSync.Syncher syncher = new Dapaz.PpaOnlineSync.Syncher(supplierId);
                syncher.Start();
            }
            return accountNumber;
        }

        protected void SendInvoice(NoProblem.Core.DataModel.Xrm.new_supplierpricing newSupplierPricing, DataAccessLayer.ConfigurationSettings configDal, CreditCardSolek.InvoiceDataContainer invoiceDataContainer, NoProblem.Core.BusinessLogic.Invoices.eInvoiceType type)
        {
            string invoiceDispatcherName = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.INVOICE_DISPATCHER);
            Invoices.InvoiceDispatcher dispatcher = Invoices.InvoiceDispatcherFactory.CreateDispatcher(invoiceDispatcherName);
            if (dispatcher != null)
            {
                dispatcher.DispatchInvoice(newSupplierPricing.new_accountid.Value, newSupplierPricing.new_total.HasValue ? newSupplierPricing.new_total.Value : 0, invoiceDataContainer, type);
            }
        }

        protected void AuditAutomaticRecharge(NoProblem.Core.DataModel.Xrm.new_supplierpricing newSupplierPricing)
        {
            Thread thr = new Thread(delegate()
            {
                try
                {
                    DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(null);
                    var supplier = accountRepositoryDal.Retrieve(newSupplierPricing.new_accountid.Value);
                    var newBalance = supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value : 0;
                    decimal substraction = newSupplierPricing.new_total.Value;
                    if (newSupplierPricing.new_credit.HasValue)
                    {
                        substraction += newSupplierPricing.new_credit.Value;
                    }
                    if (newSupplierPricing.new_extrabonus.HasValue)
                    {
                        substraction += newSupplierPricing.new_extrabonus.Value;
                    }
                    var oldBalance = newBalance - substraction;
                    Audit.AuditManager auditor = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
                    List<DML.Audit.AuditEntry> entries = new List<NoProblem.Core.DataModel.Audit.AuditEntry>();
                    DML.Audit.AuditEntry entry = new NoProblem.Core.DataModel.Audit.AuditEntry();
                    entry.AdvertiseId = newSupplierPricing.new_accountid.Value;
                    entry.AdvertiserName = newSupplierPricing.new_account_new_supplierpricing.name;
                    entry.AuditStatus = NoProblem.Core.DataModel.Xrm.new_auditentry.Status.Update;
                    entry.CreatedOn = DateTime.Now;
                    entry.FieldId = "lbl_CurrentBalance";
                    entry.FieldName = "Your balance:";
                    entry.OldValue = oldBalance.ToString();
                    entry.NewValue = newBalance.ToString();
                    entry.PageId = "PublisherPayment.aspx";
                    entry.PageName = "Publisher Payment";
                    entry.UserName = "Automatic (System)";
                    entries.Add(entry);
                    auditor.CreateAudit(entries);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in AuditAutomaticRecharge, supplierPricingId: {0}", newSupplierPricing.new_supplierpricingid);
                }
            });
            thr.Start();
        }

        protected void HandleRechargeErrors(Guid supplierId, string error, bool notifySupplierAlso, decimal amount)
        {
            if (GlobalConfigurations.IsUsaSystem)
            {
                Balance.CreditCardChargeFailHandler failHandler = new Balance.CreditCardChargeFailHandler(XrmDataContext);
                failHandler.HandleCCChargeFail(amount, amount, supplierId, null, "Auto Recharge Failed", error, false);
            }
            System.Threading.Thread t1 = new System.Threading.Thread(delegate()
            {
                try
                {
                    LogUtils.MyHandle.HandleException(new Exception(error), "Could not do auto recharge for supplierid {0}", supplierId);
                    DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
                    DataAccessLayer.AccountRepository accDal = new NoProblem.Core.DataAccessLayer.AccountRepository(configDal.XrmDataContext);
                    string emailSender = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.EMAIL_SENDER);
                    Notifications notifier = new Notifications(configDal.XrmDataContext);
                    try
                    {
                        string alertUserIdStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.RECHARGE_FAILED_ALERT_USER);
                        if (string.IsNullOrEmpty(alertUserIdStr) == false)
                        {
                            Guid userId = new Guid(alertUserIdStr);
                            string email = accDal.GetEmail(userId);
                            string subject = string.Empty;
                            string body = string.Empty;
                            notifier.InstatiateTemplate(DML.TemplateNames.RECHARGE_FAILURE_ALERT_FOR_PUBLISHER, supplierId, "account", out subject, out body);
                            body = body + "\n\r Error: " + error;
                            notifier.SendEmail(emailSender, email, body, subject, configDal);
                        }
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Send email to publisher about failed recharge failed with exception");
                    }
                    try
                    {
                        string email = accDal.GetAccountManagerEmail(supplierId);
                        if (string.IsNullOrEmpty(email) == false)
                        {
                            string subject = string.Empty;
                            string body = string.Empty;
                            notifier.InstatiateTemplate(DML.TemplateNames.RECHARGE_FAILURE_ALERT_FOR_PUBLISHER, supplierId, "account", out subject, out body);
                            notifier.SendEmail(emailSender, email, body + "\n\r Error: " + error, subject, configDal);
                        }
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Send email to account manager about failed recharge failed with exception");
                    }
                    if (!GlobalConfigurations.IsUsaSystem && notifySupplierAlso)
                    {
                        try
                        {
                            string email = accDal.GetEmail(supplierId);
                            string subject = string.Empty;
                            string body = string.Empty;
                            notifier.InstatiateTemplate(DML.TemplateNames.RECHARGE_FAILURE_ALERT_FOR_SUPPLIER, supplierId, "account", out subject, out body);
                            notifier.SendEmail(emailSender, email, body, subject, configDal);
                        }
                        catch (Exception exc)
                        {
                            LogUtils.MyHandle.HandleException(exc, "Send email to supplier about failed recharge failed with exception");
                        }
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Send emails about failed recharge failed with exception");
                }
            });
            t1.Start();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// This is only for suppliers that are candidates and need to be updated to active.
        /// </summary>
        /// <param name="supplier">a candidate supplier</param>
        /// <param name="dal"></param>
        private string HandleNewActiveSupplier(NoProblem.Core.DataModel.Xrm.account supplier, DataAccessLayer.AccountRepository dal)
        {
            supplier.new_status = (int)DML.Xrm.account.SupplierStatus.Approved;
            supplier.new_approvedon = DateTime.Now;
            if (string.IsNullOrEmpty(supplier.new_password))
            {
                string password = LoginUtils.GenerateNewPassword();
                supplier.new_password = password;
            }
            if (supplier.new_isexternalsupplier.HasValue == false || supplier.new_isexternalsupplier.Value == false)
            {
                DataAccessLayer.Autonumber.AutonumberManager autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
                string nextNumber = autoNumber.GetNextNumber("account");
                supplier.accountnumber = nextNumber;
            }
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
            SupplierBL.BonusGiver bonusGiver = new SupplierBL.BonusGiver(supplier.accountid);
            bonusGiver.Start();
            SendSignUpNotification(supplier);
            if (supplier.new_referrerid.HasValue)
            {
                Balance.InviteAFriendCreditor creditor = new Balance.InviteAFriendCreditor(supplier.new_referrerid.Value);
                creditor.Start();
            }
            SendYo();
            NotifySlack(supplier);
            return supplier.accountnumber;
        }

        public void NotifySlack(DML.Xrm.account supplier)
        {
            try
            {
                Slack.SlackNotifier notifier = new Slack.SlackNotifier(String.Format(slackNotification, supplier.name, supplier.telephone1, supplier.emailaddress1));
                notifier.Start();
            }
            catch (Exception exc)
            {
                 LogUtils.MyHandle.HandleException(exc, "Slack! Exception in SupplierPricingHandlerBase");
            }
        }

        private const string slackNotification =
@"New Registered Advertiser
Name: {0}
Phone: {1}
Email: {2}";

        private void SendYo()
        {
            try
            {
                Yo.YoNotifier yoNotifier = new Yo.YoNotifier(Yo.YoNotifier.eYoApps.NPNEWADV);
                yoNotifier.Start();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Yo! Exception in SupplierPricingHandlerBase");
            }
        }

        private void SendSignUpNotification(DataModel.Xrm.account supplier)
        {
            SupplierBL.SignUpNotifier notifier = new SupplierBL.SignUpNotifier(supplier);
            notifier.Start();
        }

        #endregion
    }
}
