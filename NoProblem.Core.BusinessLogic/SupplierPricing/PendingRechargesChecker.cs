﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.SupplierPricing;

namespace NoProblem.Core.BusinessLogic.SupplierPricing
{
    public class PendingRechargesChecker : SupplierPricingHandlerBase
    {
        #region Ctor
        public PendingRechargesChecker(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        public void CheckAllPending()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string solekName = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.SOLEK);
            if (string.IsNullOrEmpty(solekName))
            {
                LogUtils.MyHandle.WriteToLog("CheckAllPending did nothing becuase there is no solek in the environment");
                return;//auto recharge disabled in environment.
            }
            CreditCardSolek.Solek solek = CreditCardSolek.SolekFactory.GetSolek(solekName);
            DataAccessLayer.PendingSupplierPricing pendingDal = new NoProblem.Core.DataAccessLayer.PendingSupplierPricing(XrmDataContext);
            var allPending = pendingDal.GetAllPending();
            DataAccessLayer.SupplierPricing spDal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            foreach (var item in allPending)
            {
                try
                {
                    string orderStatusReason;
                    CreditCardSolek.InvoiceDataContainer invoiceDataContainer;
                    DML.Xrm.new_pendingsupplierpricing.OrderStatusCode orderStatus = solek.CheckPendingRecharge(item.transactionId, out orderStatusReason, out invoiceDataContainer);
                    if (orderStatus == NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Accepted)
                    {
                        HandleAccepted(configDal, pendingDal, spDal, item, orderStatusReason, invoiceDataContainer);
                    }
                    else if (orderStatus == NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Cancelled)
                    {
                        HandleCancelled(pendingDal, item, orderStatusReason);
                    }
                }
                catch (Exception exc)
                {
                    HandleRechargeErrors(item.SupplierId, string.Format("Checking pending recharge failed with exception. PendingSupplierPricingId = {0}, Exception: {1}. StackTrace: {2}.", item.PendingId, exc.Message, exc.StackTrace), true, item.Total);
                }
            }
        }

        #region Private Methods

        private void HandleCancelled(DataAccessLayer.PendingSupplierPricing pendingDal, PendingSupplierPricingDataContainer pendingData, string orderStatusReason)
        {
            DML.Xrm.new_pendingsupplierpricing pendingSP = new NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing(XrmDataContext);
            pendingSP.new_pendingsupplierpricingid = pendingData.PendingId;
            pendingSP.new_orderstatusreason = orderStatusReason;
            pendingSP.new_orderstatus = (int)DML.Xrm.new_pendingsupplierpricing.OrderStatusCode.Cancelled;
            pendingDal.Update(pendingSP);
            XrmDataContext.SaveChanges();
            HandleRechargeErrors(pendingData.SupplierId, string.Format("Recharge cancelled by credit card charging company. TranscationId = {0}. Reason = {1}", pendingData.transactionId, orderStatusReason), true, pendingData.Total);
        }

        private void HandleAccepted(DataAccessLayer.ConfigurationSettings configDal, DataAccessLayer.PendingSupplierPricing pendingDal, DataAccessLayer.SupplierPricing spDal, PendingSupplierPricingDataContainer pendingData, string orderStatusReason, CreditCardSolek.InvoiceDataContainer invoiceDataContainer)
        {
            var pendingSP = pendingDal.Retrieve(pendingData.PendingId);
            pendingSP.new_orderstatusreason = orderStatusReason;
            pendingSP.new_orderstatus = (int)DML.Xrm.new_pendingsupplierpricing.OrderStatusCode.Accepted;
            pendingDal.Update(pendingSP);
            DataModel.Xrm.new_supplierpricing newSupplierPricing = CreateSupplierPricingFromPending(pendingSP);
            newSupplierPricing.new_invoicenumber = invoiceDataContainer.InvoiceNumber;
            newSupplierPricing.new_invoicepdflink = invoiceDataContainer.InvoiceUrl;
            newSupplierPricing.new_billingaddress = BuildAddressField(invoiceDataContainer);
            newSupplierPricing.new_creditcardownername = invoiceDataContainer.BillingFirstName + " " + invoiceDataContainer.BillingLastName;
            newSupplierPricing.new_billingemail = invoiceDataContainer.BillingEmail;
            newSupplierPricing.new_billingphone = invoiceDataContainer.BillingPhone;
            spDal.Create(newSupplierPricing);
            XrmDataContext.SaveChanges();
            DataAccessLayer.PaymentComponent paymentCompDal = new NoProblem.Core.DataAccessLayer.PaymentComponent(XrmDataContext);
            int amountForBalance = 0;
            decimal amountNotForBalance = 0;
            foreach (var item in pendingSP.new_pendingsupplierpricing_paymentcomponent)
            {
                item.new_supplierpricingid = newSupplierPricing.new_supplierpricingid;
                paymentCompDal.Update(item);
                if (item.new_type != (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.MonthlyFeePayment)
                {
                    amountForBalance += decimal.ToInt32(item.new_currentpaymentamount.Value);
                }
                else
                {
                    amountNotForBalance += item.new_currentpaymentamount.Value;
                }
            }
            XrmDataContext.SaveChanges();

            string accountManagerId;
            UpdateTheSuppliersAccount(pendingSP.new_accountid.Value, amountForBalance + newSupplierPricing.new_credit.Value, newSupplierPricing, false, DataModel.Xrm.new_balancerow.Action.AutoRecharge, null, out accountManagerId, false);
            if (amountNotForBalance > 0)
            {
                Balance.BalanceUtils.CreateNewBalanceRow(DataModel.Xrm.new_balancerow.Action.MonthlyFee, amountNotForBalance, 0, newSupplierPricing.new_supplierpricingid, null, String.Empty, newSupplierPricing.new_accountid.Value, XrmDataContext);
                XrmDataContext.SaveChanges();
            }

            AuditAutomaticRecharge(newSupplierPricing);
            SendInvoice(newSupplierPricing, configDal, invoiceDataContainer, Invoices.eInvoiceType.Monthly);
        }        

        private DML.Xrm.new_supplierpricing CreateSupplierPricingFromPending(DML.Xrm.new_pendingsupplierpricing pendingSupplierPricing)
        {
            DML.Xrm.new_supplierpricing newSp = new NoProblem.Core.DataModel.Xrm.new_supplierpricing();
            newSp.new_accountid = pendingSupplierPricing.new_accountid;
            newSp.new_bonustype = pendingSupplierPricing.new_bonustype;
            newSp.new_chargingcompanyaccountnumber = pendingSupplierPricing.new_chargingcompanyaccountnumber;
            newSp.new_chargingcompanycontractid = pendingSupplierPricing.new_chargingcompanycontractid;
            newSp.new_credit = pendingSupplierPricing.new_credit;
            newSp.new_creditcardownerid = pendingSupplierPricing.new_creditcardownerid;
            newSp.new_creditcardownername = pendingSupplierPricing.new_creditcardownername;
            newSp.new_creditcardtoken = pendingSupplierPricing.new_creditcardtoken;
            newSp.new_cvv2 = pendingSupplierPricing.new_cvv2;
            newSp.new_isautorecharge = pendingSupplierPricing.new_isautorecharge;
            newSp.new_isfirstdeposit = pendingSupplierPricing.new_isfirstdeposit;
            newSp.new_last4digitscc = pendingSupplierPricing.new_last4digitscc;
            newSp.new_numberofpayments = pendingSupplierPricing.new_numberofpayments;
            newSp.new_paymentmethodid = pendingSupplierPricing.new_paymentmethodid;
            newSp.new_pricingmethodid = pendingSupplierPricing.new_pricingmethodid;
            newSp.new_rechargeamount = pendingSupplierPricing.new_rechargeamount;
            newSp.new_transactionid = pendingSupplierPricing.new_transactionid;
            newSp.new_total = pendingSupplierPricing.new_total;
            newSp.new_creditcardtype = pendingSupplierPricing.new_creditcardtype;
            newSp.new_subscriptionplan = pendingSupplierPricing.new_subscriptionplan;
            return newSp;
        }

        #endregion
    }
}
