﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Effect.Crm.Logs;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using NoProblem.Core.BusinessLogic.PublisherPortal;
using NoProblem.Core.DataModel.SupplierPricing;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.SupplierPricing
{
    public class SupplierPricingManager : SupplierPricingHandlerBase
    {
        #region Ctor
        public SupplierPricingManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Returns all data needed for tab number 6 (payment) of the management portal.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetSupplierChargingDataResponse GetSupplierChargingData(GetSupplierChargingDataRequest request)
        {
            GetSupplierChargingDataResponse response = new GetSupplierChargingDataResponse();
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(request.SupplierId);
            GetSupplierBasicData(response, supplier);
            response.StageInRegistration = supplier.new_stageinregistration.HasValue ? supplier.new_stageinregistration.Value : 1;
            GetPricingPackages(response, (DataModel.Xrm.account.StageInRegistration)response.StageInRegistration);
            response.CurrentBalance = supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value : 0;
            response.CompanyName = string.IsNullOrEmpty(supplier.new_custom5) ? supplier.name : supplier.new_custom5;
            response.Email = supplier.emailaddress1;
            response.SupplierBizId = supplier.new_bizid;
            if (supplier.new_managerid.HasValue)
            {
                response.AccountManagerBizId = supplier.new_account_account_account.new_userbizid;
            }
            DataAccessLayer.SupplierPricing spDal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            GetLasSupplierPricingData(request.SupplierId, response, spDal);
            string ccToken;
            string cvv2;
            string ccOwnerId;
            string ccOwnerName;
            string last4digits;
            DateTime tokenDate;
            string chargingCompanyAccountNumber;
            string chargingCompanyContractId;
            string ccType;
            string ccExpDate;
            string billingAddress;
            bool ccFound = spDal.FindCreditCardInfo(request.SupplierId, out ccToken, out cvv2, out ccOwnerId, out ccOwnerName, out last4digits, out tokenDate, out chargingCompanyContractId, out chargingCompanyAccountNumber, out ccType, out ccExpDate, out billingAddress);
            if (ccFound)
            {
                response.CreditCardToken = ccToken;
                response.TokenDate = FixDateToLocal(tokenDate);
                response.CreditCardOwnerId = ccOwnerId;
                response.CreditCardOwnerName = ccOwnerName;
                response.CVV2 = cvv2;
                response.Last4DigitsCC = last4digits;
                response.ChargingCompanyAccountNumber = chargingCompanyAccountNumber;
                response.ChargingCompanyContractId = chargingCompanyContractId;
                response.CreditCardType = ccType;
            }
            response.HasCreditCardToken = ccFound;
            DML.Xrm.account.RechargeTypeCode rechargeType;
            int rechargeAmount;
            int dayOfMonth;
            int rechargeBonusPercent;
            int numberOfPayments;
            string userIP;
            string userAgent;
            string userRemoteHost;
            string userAcceptLanguage;
            decimal cashBalance;
            string subscriptionPlan;
            bool rechargeEnabledBySupplier = accountRepositoryDal.GetRechargeData(request.SupplierId, out rechargeAmount, out rechargeType, out dayOfMonth, out rechargeBonusPercent, out numberOfPayments, out userIP, out userAgent, out userAcceptLanguage, out userRemoteHost, out cashBalance, out subscriptionPlan);
            response.IsAutoRenew = rechargeEnabledBySupplier;
            response.RechargeAmount = rechargeAmount;
            response.RechargeDayOfMonth = dayOfMonth;
            response.RechargeTypeCode = rechargeType;
            response.UserIP = userIP;
            response.UserAgent = userAgent;
            response.UserRemoteHost = userRemoteHost;
            response.UserAcceptLanguage = userAcceptLanguage;
            GetCurrency(response);
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string invoiceSendOptionsOn = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.INVOICE_SEND_OPTIONS_ENABLED);
            if (invoiceSendOptionsOn == "1")
            {
                response.InvoiceSendOptionsEnabled = true;
            }
            else
            {
                response.InvoiceSendOptionsEnabled = false;
            }
            string minPriceStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.MIN_DEPOSIT_PRICE);
            response.MinDepositPrice = 1;
            if (string.IsNullOrEmpty(minPriceStr) == false)
            {
                decimal minPrice;
                if (decimal.TryParse(minPriceStr, out minPrice))
                {
                    response.MinDepositPrice = minPrice;
                }
            }
            string solekName = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.SOLEK);
            if (string.IsNullOrEmpty(solekName) == false)
            {
                response.RechargeEnabled = true;
            }
            else
            {
                response.RechargeEnabled = false;
            }
            if (response.RechargeEnabled)
            {
                string bonusPercentStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.RECHARGE_BONUS_PERCENT);
                if (string.IsNullOrEmpty(bonusPercentStr) == false)
                {
                    int bonusPercent = int.Parse(bonusPercentStr);
                    response.RechargeBonusPercent = bonusPercent;
                }
            }
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            decimal maxPrice, minPriceStam;
            accExpDal.GetSupplierMinAndMaxPrices(request.SupplierId, out maxPrice, out minPriceStam);
            response.MaxIncidentPrice = maxPrice;
            DataAccessLayer.PaymentsNumberTable pnDal = new NoProblem.Core.DataAccessLayer.PaymentsNumberTable(XrmDataContext);
            response.PaymentsTable = pnDal.GetAll();
            string vatStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.VAT);
            decimal vat;
            decimal.TryParse(vatStr, out vat);
            response.Vat = vat;
            if (supplier.new_fundsstatus.HasValue && supplier.new_fundsstatus.Value == (int)DataModel.Xrm.account.FundsStatus.LegalHandling)
            {
                response.CanChargeByFundsStatus = false;
            }
            else
            {
                response.CanChargeByFundsStatus = true;
            }
            return response;
        }

        public ChargingDataForHeadingPaymentPage GetSupplierChargingDataForHeadingPaymentPage(Guid supplierId)
        {
            ChargingDataForHeadingPaymentPage response = new ChargingDataForHeadingPaymentPage();
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            var headingData = GetHeadings(supplierId);
            response.HeadingsData = headingData;
            if (!IsSupplierExemptOfMonthlyPpaPayment(supplierId))
            {
                int monthlyPaymentFee;
                if (int.TryParse(configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.MONTHLY_PAYMENT_FEE), out monthlyPaymentFee))
                {
                    response.MonthlyPaymentBase = monthlyPaymentFee;
                    response.MonthlyPaymentToPay = CalculateMonthlyPaymentToPay(supplierId, monthlyPaymentFee);
                }
            }
            response.MinAmountForAutoRechargeLowBalance = GetValueFromConfig(configDal, DML.ConfigurationKeys.MIN_AMOUNT_FOR_AUTO_RECHARGE_LOW_BALANCE, 200);
            response.MinAmountForAutoRechargeMaxMonthly = GetValueFromConfig(configDal, DML.ConfigurationKeys.MIN_AMOUNT_FOR_AUTO_RECHARGE_MAX_MONTHLY, 350);
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = accountRepositoryDal.Retrieve(supplierId);
            response.SupplierBizId = supplier.new_bizid;
            response.IsExemptOfMonthlyPayment = supplier.new_isexemptofppamonthlypayment.HasValue && supplier.new_isexemptofppamonthlypayment.Value;
            response.DapazStatus = supplier.new_dapazstatus.HasValue ? (DataModel.Xrm.account.DapazStatus)supplier.new_dapazstatus.Value : DML.Xrm.account.DapazStatus.PPA;
            response.RechargeAmount = supplier.new_rechargeamount.HasValue ? supplier.new_rechargeamount.Value : 0;
            if (supplier.new_rechargetypecode.HasValue)
            {
                response.RechargeTypeCode = (DataModel.Xrm.account.RechargeTypeCode)supplier.new_rechargetypecode.Value;
            }
            if (supplier.new_managerid.HasValue)
            {
                response.AccountManagerBizId = supplier.new_account_account_account.new_userbizid;
            }
            decimal vat;
            decimal.TryParse(configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.VAT), out vat);
            response.Vat = vat;

            string ccToken;
            string cvv2;
            string ccOwnerId;
            string ccOwnerName;
            string last4digits;
            DateTime tokenDate;
            string chargingCompanyAccountNumber;
            string chargingCompanyContractId;
            string ccType;
            string ccExpDate;
            string billingAddress;
            DataAccessLayer.SupplierPricing spDal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            bool ccFound = spDal.FindCreditCardInfo(supplierId, out ccToken, out cvv2, out ccOwnerId, out ccOwnerName, out last4digits, out tokenDate, out chargingCompanyContractId, out chargingCompanyAccountNumber, out ccType, out ccExpDate, out billingAddress);
            response.HasCreditCardToken = ccFound;
            if (supplier.new_fundsstatus.HasValue && supplier.new_fundsstatus.Value == (int)DataModel.Xrm.account.FundsStatus.LegalHandling)
            {
                response.CanChargeByFundsStatus = false;
            }
            else
            {
                response.CanChargeByFundsStatus = true;
            }
            response.Balance = supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value : 0M;
            return response;
        }

        public CreateSupplierPricingResponse CreateSupplierPricing(CreateSupplierPricingRequest request)
        {
            if (!request.Action.HasValue)
            {
                request.Action = DataModel.Xrm.new_balancerow.Action.Deposit;
            }

            int paymentForBalance = 0;
            decimal amountNotForBalance = 0;
            List<DML.Xrm.new_paymentcomponent> payComponents = CalculateComponents(request, ref paymentForBalance, ref amountNotForBalance);

            DataAccessLayer.SupplierPricing supplierPricingDAL = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            if (!string.IsNullOrEmpty(request.ChargingCompany) && !string.IsNullOrEmpty(request.TransactionId))
            {
                DML.Xrm.new_supplierpricing exitingPricing = supplierPricingDAL.GetByTransactionIdAndChargingCompany(request.TransactionId, request.ChargingCompany);
                if (exitingPricing != null)
                {
                    return new CreateSupplierPricingResponse() { PricingId = Guid.Empty, DepositStatus = eDepositStatus.Repeated };
                }
            }
            if (paymentForBalance < 0)
            {
                eDepositStatus status;
                bool isValid = ValidateNegativeDeposit(request, supplierPricingDAL, out status);
                if (!isValid)
                {
                    return new CreateSupplierPricingResponse() { DepositStatus = status, PricingId = Guid.Empty };
                }
            }
            var newSupplierPricing = new DataModel.Xrm.new_supplierpricing
            {
                new_total = request.PaymentAmount,
                new_credit = request.BonusAmount,
                new_creditcardtoken = request.CreditCardToken,
                new_chargingcompanyaccountnumber = request.ChargingCompanyAccountNumber,
                new_chargingcompanycontractid = request.ChargingCompanyContractId,
                new_creditcardtype = request.CreditCardType,
                new_creditcardexpdate = request.CreditCardExpDate,
                new_numberofpayments = request.NumberOfPayments == 0 ? 1 : request.NumberOfPayments,
                new_invoicepdflink = request.InvoiceUrl,
                new_invoicenumber = request.InvoiceNumber,
                new_billingemail = request.BillingEmail,
                new_billingphone = request.BillingPhone,
                new_billingcity = request.BillingCity,
                new_billingstate = request.BillingState,
                new_billingzip = request.BillingZip
            };
            if (request.CheckDate.HasValue)
            {
                newSupplierPricing.new_checkdate = request.CheckDate.Value;
            }
            if (request.TransferDate.HasValue)
            {
                newSupplierPricing.new_transferdate = request.TransferDate.Value;
            }
            newSupplierPricing.new_checknumber = request.CheckNumber;
            DataAccessLayer.PricingMethod pricingMethodDAL = new NoProblem.Core.DataAccessLayer.PricingMethod(XrmDataContext);
            DML.Xrm.new_pricingmethod pricingMethod = pricingMethodDAL.GetDefaultPricingMethod();
            newSupplierPricing.new_pricingmethodid = pricingMethod.new_pricingmethodid;
            DataAccessLayer.PaymentMethod paymentMethodDAL = new NoProblem.Core.DataAccessLayer.PaymentMethod(XrmDataContext);
            DML.Xrm.new_paymentmethod paymentMethod = paymentMethodDAL.GetPaymentMethodByCode(request.PaymentMethod);
            newSupplierPricing.new_paymentmethodid = paymentMethod.new_paymentmethodid;
            if (request.PaymentMethod == NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.Voucher)
            {
                if (string.IsNullOrEmpty(request.VoucherNumber))
                {
                    return new CreateSupplierPricingResponse() { DepositStatus = eDepositStatus.BadVoucherNumber, PricingId = Guid.Empty };
                }
                DataAccessLayer.Voucher voucherDal = new DataAccessLayer.Voucher(XrmDataContext);
                int voucherBalance;
                Guid voucherId = voucherDal.GetVoucher(request.VoucherNumber, out voucherBalance);
                if (voucherId == Guid.Empty)
                {
                    return new CreateSupplierPricingResponse() { DepositStatus = eDepositStatus.BadVoucherNumber, PricingId = Guid.Empty };
                }
                if (voucherBalance < request.PaymentAmount)
                {
                    return new CreateSupplierPricingResponse() { DepositStatus = eDepositStatus.NotEnoughCashInVoucher, PricingId = Guid.Empty };
                }
                newSupplierPricing.new_voucherid = voucherId;
                if (request.VoucherReasonId != Guid.Empty)
                {
                    newSupplierPricing.new_voucherreasonid = request.VoucherReasonId;
                }
                voucherDal.DecreaseVoucherBalance(voucherId, request.PaymentAmount);
            }

            newSupplierPricing.new_accountid = request.SupplierId;
            newSupplierPricing.new_last4digitscc = request.Last4DigitsCC;
            newSupplierPricing.new_cvv2 = request.CVV2;
            newSupplierPricing.new_transactionid = request.TransactionId;
            newSupplierPricing.new_creditcardownerid = request.CreditCardOwnerId;
            newSupplierPricing.new_creditcardownername = request.CreditCardOwnerName;
            newSupplierPricing.new_chargingcompany = request.ChargingCompany;
            newSupplierPricing.new_isfirstdeposit = supplierPricingDAL.IsFirstDeposit(request.SupplierId);
            newSupplierPricing.new_isautorecharge = false;
            newSupplierPricing.new_bank = request.Bank;
            newSupplierPricing.new_bankaccount = request.BankAccount;
            newSupplierPricing.new_bankbranch = request.BankBranch;
            newSupplierPricing.new_billingaddress = request.BillingAddress;
            if (request.CreatedByUserId != Guid.Empty)
            {
                newSupplierPricing.new_userid = request.CreatedByUserId;
            }
            newSupplierPricing.new_bonustype = (int)request.BonusType;
            newSupplierPricing.new_extrabonus = request.ExtraBonus;
            if (request.ExtraBonusReasonId != Guid.Empty)
            {
                newSupplierPricing.new_extrabonusreasonid = request.ExtraBonusReasonId;
            }
            DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            newSupplierPricing.new_subscriptionplan = (from acc in accountRepositoryDal.All where acc.accountid == request.SupplierId select acc.new_subscriptionplan).FirstOrDefault();
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            if (config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.EXTERNAL_PAYMENTS_SYSTEM) == 1.ToString()
                && request.PaymentAmount != 0
                && request.PaymentMethod != NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.Voucher
                && request.PaymentMethod != NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.Check)
            {
                string solekName = config.GetConfigurationSettingValue(DML.ConfigurationKeys.SOLEK);
                CreditCardSolek.Solek solek = CreditCardSolek.SolekFactory.GetSolek(solekName);
                if (solek == null)
                {
                    LogUtils.MyHandle.WriteToLog("Failed to charge with external payment system '{0}'. Charging company (solek) is not configured correctly. supplierId = {1}", solekName, request.SupplierId);
                    return new CreateSupplierPricingResponse() { DepositStatus = eDepositStatus.ChargingWithExternalSystemFailed, PricingId = Guid.Empty };
                }
                decimal vat;
                decimal.TryParse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.VAT), out vat);
                decimal rechargeAmountNoVat = new decimal(request.PaymentAmount);
                decimal rechargeAmountWithVat = Math.Round((rechargeAmountNoVat * (vat / 100m + 1m)), 2);
                //DataAccessLayer.Account accountDal = new NoProblem.Core.DataAccessLayer.Account(XrmDataContext);
                var supplier = accountRepositoryDal.Retrieve(request.SupplierId);
                string solekAnswer;
                string transactionConfirmNum;
                bool needsAsyncApproval;
                bool charged = solek.RechargeSupplier(request.CreditCardToken, request.CVV2, request.CreditCardOwnerId, rechargeAmountNoVat, rechargeAmountWithVat, request.NumberOfPayments, out solekAnswer, out transactionConfirmNum, request.Last4DigitsCC, request.CreditCardType, request.ChargingCompanyAccountNumber, request.ChargingCompanyContractId, request.UserIP, request.UserAgent, request.UserRemoteHost, request.UserAcceptLanguage, out needsAsyncApproval, request.CreditCardExpDate, request.CreditCardOwnerName, newSupplierPricing, supplier, request.PaymentMethod, payComponents, false);
                if (!charged)
                {
                    LogUtils.MyHandle.WriteToLog("Failed to charge with external payment system '{0}'. supplierId = {2}. solekAnswer = {1}", solekName, solekAnswer, request.SupplierId);
                    return new CreateSupplierPricingResponse() { DepositStatus = eDepositStatus.ChargingWithExternalSystemFailed, PricingId = Guid.Empty, ErrorDesc = solekAnswer };
                }
                newSupplierPricing.new_transactionid = transactionConfirmNum;
            }
            supplierPricingDAL.Create(newSupplierPricing);
            XrmDataContext.SaveChanges();

            DataAccessLayer.PaymentComponent payComponentDal = new NoProblem.Core.DataAccessLayer.PaymentComponent(XrmDataContext);
            foreach (var comp in payComponents)
            {
                comp.new_supplierpricingid = newSupplierPricing.new_supplierpricingid;
                payComponentDal.Create(comp);
            }
            XrmDataContext.SaveChanges();

            //take care of recharge values
            if (request.UpdateRechargeFields && request.PaymentAmount >= 0)//if is negative deposit. don't change the recharge status.
            {
                UpdateRechargeFieldsAtPayment(request);
            }

            string accountManagerId;
            string accountNumber = UpdateTheSuppliersAccount(request.SupplierId, paymentForBalance, newSupplierPricing, request.IsFromAAR, 
                request.Action.Value, request.Description, out accountManagerId, request.IsFromDollar);
            if (amountNotForBalance > 0)
            {
                Balance.BalanceUtils.CreateNewBalanceRow(DataModel.Xrm.new_balancerow.Action.MonthlyFee, amountNotForBalance, 0, newSupplierPricing.new_supplierpricingid, null, String.Empty, request.SupplierId, XrmDataContext);
                XrmDataContext.SaveChanges();
            }
            SendInvoice(request, newSupplierPricing, config);
            CreateSupplierPricingResponse response = new CreateSupplierPricingResponse();
            response.PricingId = newSupplierPricing.new_supplierpricingid;
            response.AccountNumber = accountNumber;
            response.AccountManagerId = accountManagerId;
            response.DepositStatus = eDepositStatus.OK;
            if (request.IsFromDollar)
            {
                //SendOrder:
                new Dapaz.Orders.OrdersSender(request.CreatedByUserId, NoProblem.Core.BusinessLogic.Dapaz.Orders.eOrderType.Payment, request.SupplierId).SendOrder();
            }
            return response;
        }

        private void SendInvoice(CreateSupplierPricingRequest request, DML.Xrm.new_supplierpricing newSupplierPricing, DataAccessLayer.ConfigurationSettings config)
        {
            if (request.PaymentMethod == DML.Xrm.new_paymentmethod.ePaymentMethod.CreditCard)
            {
                CreditCardSolek.InvoiceDataContainer invoiceDataContainer = new CreditCardSolek.InvoiceDataContainer();
                invoiceDataContainer.BillingAddress1 = request.BillingAddress;
                invoiceDataContainer.BillingCity = request.BillingCity;
                invoiceDataContainer.BillingState = request.BillingState;
                invoiceDataContainer.BillingEmail = request.BillingEmail;
                invoiceDataContainer.BillingFirstName = request.CreditCardOwnerName;
                invoiceDataContainer.BillingPhone = request.BillingPhone;
                invoiceDataContainer.InvoiceNumber = request.InvoiceNumber;
                invoiceDataContainer.InvoiceUrl = request.InvoiceUrl;
                SendInvoice(newSupplierPricing, config, invoiceDataContainer, Invoices.eInvoiceType.Registration);
            }
        }

        public void UpdateExemptionOfMonthlyPayment(UpdateExemptionOfMonthlyPaymentRequest request)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = new DML.Xrm.account();
            supplier.new_isexemptofppamonthlypayment = request.IsExemptOfMonthlyPayment;
            supplier.accountid = request.SupplierId;
            if (request.IsExemptOfMonthlyPayment)
            {
                DataAccessLayer.ConfigurationSettings configDal = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string minStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.MIN_CALL_DURATION_FOR_EXEMPT_OF_MONTHLY_PAY);
                int minSeconds;
                if (int.TryParse(minStr, out minSeconds))
                    supplier.new_mincallduration = minSeconds;
            }
            else
            {
                supplier.new_mincallduration = null;
            }
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        public void CreateTransactionLog(CreateTransactionLogRequest request)
        {
            DataAccessLayer.TransactionLog transactionLogDAL = new NoProblem.Core.DataAccessLayer.TransactionLog(XrmDataContext);
            foreach (var entry in request.ResultValueDictionary)
            {
                DML.Xrm.new_transactionlog logEntry = new NoProblem.Core.DataModel.Xrm.new_transactionlog();
                logEntry.new_name = entry.Key;
                logEntry.new_value = entry.Value;
                if (request.PricingId != Guid.Empty)
                {
                    logEntry.new_supplierpricingid = request.PricingId;
                }
                if (request.SupplierId != Guid.Empty)
                {
                    logEntry.new_accountid = request.SupplierId;
                }
                transactionLogDAL.Create(logEntry);
            }
            XrmDataContext.SaveChanges();
        }

        public void UpdateInvoiceData(UpdateInvoicePdfLinkRequest request)
        {
            DataAccessLayer.SupplierPricing supplierPricingDAL = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            supplierPricingDAL.UpdateInvoiceData(request.PricingId, request.InvoicePdfLink, request.InvoiceNumber, request.InvoiceNumberRefund);
        }

        /// <summary>
        /// Allows the supplier to change his auto recharge settings. Cancel/Enabel recharges. Change recharge amount.
        /// </summary>
        /// <param name="request"></param>
        public void UpdateAutoRechargeOptions(UpdateAutoRechargeOptionsRequest request)
        {
            bool changed = false;
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account account = dal.Retrieve(request.SupplierId);
            string oldRechargeType = GetCurrentRechargeStatus(account);
            if (!account.new_rechargeamount.HasValue || !account.new_rechargeamount.HasValue || account.new_rechargeamount.Value != request.RechargeAmount || account.new_rechargeenabled != request.IsAutoRecharge)
            {
                changed = true;
            }
            account.new_rechargeenabled = request.IsAutoRecharge;
            account.new_rechargeamount = request.RechargeAmount;
            bool isLowBalance = false;
            if (request.RechargeTypeCode.HasValue)
            {
                if (!changed && (!account.new_rechargetypecode.HasValue || account.new_rechargetypecode.Value != (int)request.RechargeTypeCode.Value))
                {
                    changed = true;
                }
                account.new_rechargetypecode = (int)request.RechargeTypeCode.Value;
                if (request.RechargeTypeCode.Value == NoProblem.Core.DataModel.Xrm.account.RechargeTypeCode.Monthly)
                {
                    account.new_rechargedayofmonth = request.DayOfMonth;
                    isLowBalance = false;
                }
                else if (request.RechargeTypeCode.Value == NoProblem.Core.DataModel.Xrm.account.RechargeTypeCode.MaxMonthly)
                {
                    account.new_rechargedayofmonth = 1;
                    isLowBalance = false;
                }
                else
                {
                    isLowBalance = true;
                }
            }
            if (account.new_rechargebonuspercent.HasValue == false
                || account.new_rechargebonuspercent.Value == 0)
            {
                DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                int rechargeBonusPercent;
                string percentStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.RECHARGE_BONUS_PERCENT);
                int.TryParse(percentStr, out rechargeBonusPercent);
                account.new_rechargebonuspercent = rechargeBonusPercent;
            }
            if (account.new_rechargeenabledon.HasValue == false && request.IsAutoRecharge == true)
            {
                account.new_rechargeenabledon = DateTime.Now;
            }
            dal.Update(account);
            XrmDataContext.SaveChanges();
            if (isLowBalance && request.IsAutoRecharge && account.new_unavailabilityreasonid.HasValue)
            {
                DataAccessLayer.UnavailabilityReason urDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
                var outOfMoneyReason = urDal.GetPricingEndedReason();
                if (account.new_unavailabilityreasonid.Value == outOfMoneyReason.new_unavailabilityreasonid
                    && !account.new_isinactivewithmoney.Value
                    && (!account.new_dapazstatus.HasValue || account.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPA)
                    && account.new_status == (int)DataModel.Xrm.account.SupplierStatus.Approved)
                {
                    AutoRechargeSupplier(account.accountid, true);
                }
            }
            if (request.IsFromDollar && changed)
            {
                new Dapaz.Orders.OrdersSender(request.CreatedByUserId, NoProblem.Core.BusinessLogic.Dapaz.Orders.eOrderType.Payment, request.SupplierId).SendOrder();
            }
            string newRechargeType = GetCurrentRechargeStatus(account);
            AuditRechargeTypeChange(request.CreatedByUserId, dal, account, oldRechargeType, newRechargeType);
        }

        private void AuditRechargeTypeChange(Guid userId, DataAccessLayer.AccountRepository dal, DML.Xrm.account account, string oldRechargeType, string newRechargeType)
        {
            if (userId == Guid.Empty)
                return;
            if (oldRechargeType.Equals(newRechargeType))
                return;
            Audit.AuditManager audit = new Audit.AuditManager();
            List<DML.Audit.AuditEntry> auditLst = new List<DML.Audit.AuditEntry>();
            DML.Audit.AuditEntry entry = new DML.Audit.AuditEntry();
            entry.AdvertiseId = account.accountid;
            entry.AdvertiserName = account.name;
            entry.AuditStatus = DML.Xrm.new_auditentry.Status.Update;
            entry.CreatedOn = DateTime.Now;
            entry.FieldId = "RechargeTypeCode";
            entry.FieldName = "Recharge Type";
            entry.NewValue = newRechargeType;
            entry.OldValue = oldRechargeType;
            entry.PageId = "Payments";
            entry.PageName = "Payments";
            entry.UserId = userId;
            var user = dal.Retrieve(userId);
            entry.UserName = user.name;
            auditLst.Add(entry);
            audit.CreateAudit(auditLst);
        }

        private string GetCurrentRechargeStatus(DML.Xrm.account account)
        {
            string retVal = "OFF";
            if (account.new_rechargeenabled.HasValue && account.new_rechargeenabled.Value && account.new_rechargetypecode.HasValue && account.new_rechargeamount.HasValue)
            {
                retVal = ((DML.Xrm.account.RechargeTypeCode)account.new_rechargetypecode.Value).ToString();
            }
            return retVal;
        }

        public GetSupplierPricingHistoryResponse GetSupplierPricingHistory(GetSupplierPricingHistoryRequest request)
        {
            GetSupplierPricingHistoryResponse response = new GetSupplierPricingHistoryResponse();
            DataAccessLayer.SupplierPricing dal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            int totalRows;
            response.Pricings = dal.GetSupplierPricingHistory(request.SupplierId, request.PricingNumber, request.PageNumber, request.PageSize, out totalRows);
            foreach (var item in response.Pricings)
            {
                item.CreatedOn = FixDateToLocal(item.CreatedOn);
            }
            response.TotalRows = totalRows;
            response.TotalPages = (int)Math.Ceiling(((double)totalRows) / ((double)request.PageSize));
            response.CurrentPage = request.PageNumber;
            return response;
        }

        public GetPricingDataResponse GetPricingData(Guid supplierPricingId)
        {
            DataAccessLayer.SupplierPricing dal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            GetPricingDataResponse response = dal.GetPricingData(supplierPricingId);
            response.CreatedOn = FixDateToLocal(response.CreatedOn);
            return response;
        }

        public void DoMonthlyDeposits()
        {
            DoMonthlyDeposits(false);
        }

        public void DoMonthlyDeposits(bool testFirstOfTheMonth)
        {
            Thread tr = new Thread(
                new ParameterizedThreadStart(
                    isTest =>
                    {
                        try
                        {
                            DoMonthlyDepositsPrivate((bool)isTest);
                        }
                        catch (Exception exc)
                        {
                            LogUtils.MyHandle.HandleException(exc, "Exception in async method SupplierPricingManager.DoMonthlyDepositsPrivate");
                        }
                    }));
            tr.Start(testFirstOfTheMonth);
        }

        public string GetChargingErrorUserEmail()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string userIdStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.RECHARGE_FAILED_ALERT_USER);
            string retVal = string.Empty;
            if (string.IsNullOrEmpty(userIdStr) == false)
            {
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                var user = accountRepositoryDal.Retrieve(new Guid(userIdStr));
                retVal = string.IsNullOrEmpty(user.emailaddress1) ? string.Empty : user.emailaddress1;
            }
            return retVal;
        }

        public List<string> GetActivePaymentMethods()
        {
            var retVal = new List<string>();
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string cashConfig = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.CASH);
            if (!string.IsNullOrEmpty(cashConfig) && cashConfig == "1")
            {
                retVal.Add(DML.Xrm.new_paymentmethod.ePaymentMethod.Cash.ToString());
            }
            string checkConfig = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.CHECK);
            if (!string.IsNullOrEmpty(checkConfig) && checkConfig == "1")
            {
                retVal.Add(DML.Xrm.new_paymentmethod.ePaymentMethod.Check.ToString());
            }
            string wireTrConfig = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.WIRETRANSFER);
            if (!string.IsNullOrEmpty(wireTrConfig) && wireTrConfig == "1")
            {
                retVal.Add(DML.Xrm.new_paymentmethod.ePaymentMethod.WireTransfer.ToString());
            }
            string voucherConfig = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.VOUCHER);
            if (!string.IsNullOrEmpty(voucherConfig) && voucherConfig == "1")
            {
                retVal.Add(DML.Xrm.new_paymentmethod.ePaymentMethod.Voucher.ToString());
            }
            string ccConfig = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.CREDITCARD);
            if (!string.IsNullOrEmpty(ccConfig) && ccConfig == "1")
            {
                retVal.Add(DML.Xrm.new_paymentmethod.ePaymentMethod.CreditCard.ToString());
            }
            return retVal;
        }

        public void UpdateInvoiceSendOptions(UpdateInvoiceSendOptionsRequest request)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
            supplier.accountid = request.SupplierId;
            supplier.new_invoicesendoption = (int)request.InvoiceSendOption;
            supplier.new_invoicesendaddress = request.InvoiceSendAddress;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        #endregion

        #region Internal Methods

        /// <summary>
        /// Automatically charges the supplier's credit card if recharge is enabled and the suppliered allowed this.
        /// </summary>
        /// <param name="supplierId">The guid of the supplier we want to charge.</param>
        internal void AutoRechargeSupplier(Guid supplierId, bool isLowBalance)
        {
            AutoRechargeSupplier(supplierId, isLowBalance, false);
        }

        /// <summary>
        /// Automatically charges the supplier's credit card if recharge is enabled and the suppliered allowed this.
        /// </summary>
        /// <param name="supplierId">The guid of the supplier we want to charge.</param>
        internal void AutoRechargeSupplier(Guid supplierId, bool isLowBalance, bool testFirstOfTheMonth)
        {
            try
            {
                LogUtils.MyHandle.WriteToLog(8, "Auto Recharge started. supplierId = {0}", supplierId);
                DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);

                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                int rechargeAmount;
                DML.Xrm.account.RechargeTypeCode rechargeType;
                int dayOfMonth;
                int rechargeBonusPercent;
                int numberOfPayments;
                string userAgent;
                string userIP;
                string userAcceptLanguage;
                string userRemoteHost;
                decimal cashBalance;
                string subscriptionPlan;
                bool rechargeEnabledBySupplier = accountRepositoryDal.GetRechargeData(supplierId, out rechargeAmount, out rechargeType, out dayOfMonth, out rechargeBonusPercent, out numberOfPayments, out userIP, out userAgent, out userAcceptLanguage, out userRemoteHost, out cashBalance, out subscriptionPlan);
                if (!rechargeEnabledBySupplier)
                {
                    return;//the supplier does not want to be recharged.
                }
                if (rechargeAmount <= 0)
                {
                    HandleRechargeErrors(supplierId, "Recharge amount was not set properly.", true, 0);
                    return;
                }

                string solekName = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.SOLEK);
                if (string.IsNullOrEmpty(solekName))
                {
                    HandleRechargeErrors(supplierId, "Auto recharge disabled in environment.", false, rechargeAmount);
                    return;//auto recharge disabled in environment.
                }

                DataAccessLayer.SupplierPricing spDal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
                string ccToken;
                string cvv2;
                string ccOwnerId;
                string ccOwnerName;
                string last4digits;
                DateTime tokenDate;
                string chargingCompanyContractId;
                string chargingCompanyAccountNumber;
                string ccType;
                string billingAddress;
                string ccExpDate;
                bool found = spDal.FindCreditCardInfo(supplierId, out ccToken, out cvv2, out ccOwnerId, out ccOwnerName, out last4digits, out tokenDate, out chargingCompanyContractId, out chargingCompanyAccountNumber, out ccType, out ccExpDate, out billingAddress);
                if (!found)
                {
                    HandleRechargeErrors(supplierId, "Supplier's credit card info not found", true, rechargeAmount);
                    return;
                }
                DML.Xrm.new_supplierpricing newSupplierPricing = new NoProblem.Core.DataModel.Xrm.new_supplierpricing();
                newSupplierPricing.new_accountid = supplierId;
                newSupplierPricing.new_last4digitscc = last4digits;
                newSupplierPricing.new_cvv2 = cvv2;
                newSupplierPricing.new_creditcardownerid = ccOwnerId;
                newSupplierPricing.new_creditcardownername = ccOwnerName;
                newSupplierPricing.new_isfirstdeposit = false;
                newSupplierPricing.new_creditcardtoken = ccToken;
                newSupplierPricing.new_isautorecharge = true;
                newSupplierPricing.new_numberofpayments = numberOfPayments;
                newSupplierPricing.new_chargingcompanyaccountnumber = chargingCompanyAccountNumber;
                newSupplierPricing.new_chargingcompanycontractid = chargingCompanyContractId;
                newSupplierPricing.new_creditcardtype = ccType;
                newSupplierPricing.new_creditcardexpdate = ccExpDate;
                DataAccessLayer.PricingMethod pricingMethodDAL = new NoProblem.Core.DataAccessLayer.PricingMethod(XrmDataContext);
                DML.Xrm.new_pricingmethod pricingMethod = pricingMethodDAL.GetDefaultPricingMethod();
                newSupplierPricing.new_pricingmethodid = pricingMethod.new_pricingmethodid;
                DataAccessLayer.PaymentMethod paymentMethodDAL = new NoProblem.Core.DataAccessLayer.PaymentMethod(XrmDataContext);
                DML.Xrm.new_paymentmethod paymentMethod = paymentMethodDAL.GetPaymentMethodByCode(DML.Xrm.new_paymentmethod.ePaymentMethod.CreditCard);
                newSupplierPricing.new_paymentmethodid = paymentMethod.new_paymentmethodid;

                CreditCardSolek.Solek solek = CreditCardSolek.SolekFactory.GetSolek(solekName);
                if (solek == null)
                {
                    HandleRechargeErrors(supplierId, "Credit card charger in not configured correctly. Please contact \"No Problem\".", false, rechargeAmount);
                    return;
                }
                string solekAnswer;
                string transactionConfirmNum;
                bool needsAsyncApproval;
                List<DML.Xrm.new_paymentcomponent> components = CreateComponentsAtAutoRecharge(supplierId, isLowBalance, configDal, ref rechargeAmount, rechargeType, cashBalance, testFirstOfTheMonth, dayOfMonth, subscriptionPlan);
                if (rechargeAmount < 0)
                    return;

                decimal rechargeAmountNoVat = new decimal(rechargeAmount);
                decimal vat;
                decimal.TryParse(configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.VAT), out vat);
                decimal rechargeAmountWithVat = Math.Round((rechargeAmountNoVat * (vat / 100m + 1m)), 2);

                var supplier = accountRepositoryDal.Retrieve(supplierId);
                bool charged = solek.RechargeSupplier(ccToken, cvv2, ccOwnerId, rechargeAmountNoVat, rechargeAmountWithVat, numberOfPayments, out solekAnswer, out transactionConfirmNum, last4digits, ccType, chargingCompanyAccountNumber, chargingCompanyContractId, userIP, userAgent, userRemoteHost, userAcceptLanguage, out needsAsyncApproval, ccExpDate, ccOwnerName, newSupplierPricing, supplier, DataModel.Xrm.new_paymentmethod.ePaymentMethod.CreditCard, components, !isLowBalance);
                if (charged)
                {
                    newSupplierPricing.new_transactionid = transactionConfirmNum;
                    newSupplierPricing.new_total = rechargeAmount;
                    newSupplierPricing.new_credit = CalculatePaymentBonus(rechargeAmount, rechargeBonusPercent);
                    newSupplierPricing.new_bonustype = (int)DML.Xrm.new_supplierpricing.BonusType.Recharge;
                    newSupplierPricing.new_subscriptionplan = supplier.new_subscriptionplan;
                    if (!needsAsyncApproval)
                    {
                        spDal.Create(newSupplierPricing);
                        XrmDataContext.SaveChanges();
                        int amountForBalance = 0;
                        DataAccessLayer.PaymentComponent paymentCompDal = new NoProblem.Core.DataAccessLayer.PaymentComponent(XrmDataContext);
                        decimal amountNotForBalance = 0;
                        foreach (var item in components)
                        {
                            item.new_supplierpricingid = newSupplierPricing.new_supplierpricingid;
                            paymentCompDal.Create(item);
                            if (item.new_type != (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.MonthlyFeePayment)
                            {
                                amountForBalance += decimal.ToInt32(item.new_currentpaymentamount.Value);
                            }
                            else
                            {
                                amountNotForBalance += item.new_currentpaymentamount.Value;
                            }
                        }
                        XrmDataContext.SaveChanges();
                        if (amountForBalance > 0)
                        {
                            string accountManagerId;
                            UpdateTheSuppliersAccount(supplierId, amountForBalance + newSupplierPricing.new_credit.Value, newSupplierPricing, false, NoProblem.Core.DataModel.Xrm.new_balancerow.Action.AutoRecharge, null, out accountManagerId, false);
                            //Invitations.InvitationsManager.CreditTheSupplier(newSupplierPricing.new_accountid.Value, NoProblem.Core.BusinessLogic.Invitations.InvitationsManager.CreditType.LifeTime, amountForBalance);
                        }
                        if (amountNotForBalance > 0)
                        {
                            Balance.BalanceUtils.CreateNewBalanceRow(DataModel.Xrm.new_balancerow.Action.MonthlyFee, amountNotForBalance, 0, newSupplierPricing.new_supplierpricingid, null, String.Empty, supplierId, XrmDataContext);
                            XrmDataContext.SaveChanges();
                        }
                        AuditAutomaticRecharge(newSupplierPricing);
                        SendInvoice(newSupplierPricing, configDal, new CreditCardSolek.InvoiceDataContainer(), Invoices.eInvoiceType.Monthly);
                    }
                    else
                    {
                        DataAccessLayer.PendingSupplierPricing pendingSpDal = new NoProblem.Core.DataAccessLayer.PendingSupplierPricing(XrmDataContext);
                        DML.Xrm.new_pendingsupplierpricing pendingSP = CreatePendingSpFromRealSp(newSupplierPricing);
                        pendingSpDal.Create(pendingSP);
                        XrmDataContext.SaveChanges();
                        DataAccessLayer.PaymentComponent paymentCompDal = new NoProblem.Core.DataAccessLayer.PaymentComponent(XrmDataContext);
                        foreach (var item in components)
                        {
                            item.new_pendingsupplierpricingid = pendingSP.new_pendingsupplierpricingid;
                            paymentCompDal.Create(item);
                        }
                        XrmDataContext.SaveChanges();
                    }
                }
                else if (solekAnswer != NoProblem.Core.BusinessLogic.CreditCardSolek.Zap.DONT_NOTIFY_CODE)
                {
                    HandleRechargeErrors(supplierId, string.Format("The credit card charger was not able to recharge the credit card. Charger response = {0}", solekAnswer), true, rechargeAmount);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception attempting to make a recharge. supplierid = {0}.", supplierId);
            }
        }

        #endregion

        #region Private Methods

        private List<DML.Xrm.new_paymentcomponent> CreateComponentsAtAutoRecharge(Guid supplierId, bool isLowBalance, DataAccessLayer.ConfigurationSettings configDal, ref int rechargeAmount, DML.Xrm.account.RechargeTypeCode rechargeType, decimal cashBalance, bool isTestFirstOfTheMonth, int rechargeDayInMonth, string subscriptionPlan)
        {
            List<DML.Xrm.new_paymentcomponent> components = new List<NoProblem.Core.DataModel.Xrm.new_paymentcomponent>();
            if (!isLowBalance)
            {
                if (GlobalConfigurations.IsUsaSystem && !String.IsNullOrEmpty(subscriptionPlan))
                {
                    bool payMonthlyFee = false;
                    if (
                        !SupplierPricing.SubscriptionPlansUtils.AlreadyPaidThisMonth(supplierId, subscriptionPlan)
                        &&
                        !IsSupplierExemptOfMonthlyPpaPayment(supplierId)
                        )
                    {
                        if (subscriptionPlan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING)
                        {
                            bool acceptedEnoughLeads = SupplierPricing.SubscriptionPlansUtils.AcceptedEnoughLeads(supplierId);
                            payMonthlyFee = !acceptedEnoughLeads;
                            if (!payMonthlyFee)
                            {
                                //create + and - balance rows.
                                Balance.BalanceUtils.CreateNewBalanceRow(DataModel.Xrm.new_balancerow.Action.MonthlyFee, rechargeAmount, 0, null, null, String.Empty, supplierId, XrmDataContext);
                                Balance.BalanceUtils.CreateNewBalanceRow(DataModel.Xrm.new_balancerow.Action.MonthlyFeeRefund, rechargeAmount, 0, null, null, String.Empty, supplierId, XrmDataContext);
                                XrmDataContext.SaveChanges();
                            }
                        }
                        else//is featured listing
                        {
                            payMonthlyFee = true;
                        }
                    }

                    if (payMonthlyFee)
                    {
                        DML.Xrm.new_paymentcomponent headComp = new NoProblem.Core.DataModel.Xrm.new_paymentcomponent();
                        headComp.new_currentpaymentamount = rechargeAmount;
                        headComp.new_baseamount = rechargeAmount;
                        headComp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.MonthlyFeePayment;
                        components.Add(headComp);
                    }
                    else
                    {
                        rechargeAmount = -1;
                    }
                    return components;
                }

                int dayInMonth = isTestFirstOfTheMonth ? 1 : FixDateToLocal(DateTime.Now).Day;
                if (rechargeType == NoProblem.Core.DataModel.Xrm.account.RechargeTypeCode.Monthly && rechargeDayInMonth == dayInMonth)
                {
                    DML.Xrm.new_paymentcomponent monthComp = new NoProblem.Core.DataModel.Xrm.new_paymentcomponent();
                    monthComp.new_currentpaymentamount = rechargeAmount;
                    monthComp.new_baseamount = rechargeAmount;
                    monthComp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.Monthly;
                    components.Add(monthComp);
                }
                if (dayInMonth == 1)
                {
                    if (rechargeType == NoProblem.Core.DataModel.Xrm.account.RechargeTypeCode.MaxMonthly)
                    {
                        //add max monthly too.
                        int baseAmount = rechargeAmount;
                        int amountToCharge = rechargeAmount - decimal.ToInt32(cashBalance);
                        if (amountToCharge > 0)
                        {
                            DML.Xrm.new_paymentcomponent maxMonthlyComp = new NoProblem.Core.DataModel.Xrm.new_paymentcomponent();
                            maxMonthlyComp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.AutoMaxMonthly;
                            maxMonthlyComp.new_baseamount = baseAmount;
                            maxMonthlyComp.new_currentpaymentamount = amountToCharge;
                            components.Add(maxMonthlyComp);
                            rechargeAmount = amountToCharge;
                        }
                    }
                    else if (rechargeType == DML.Xrm.account.RechargeTypeCode.BalanceLow && IsDapazPpaHold(supplierId))//is low balance and is ppa hold (first charge on first of the month).
                    {
                        DML.Xrm.new_paymentcomponent lowComp = new NoProblem.Core.DataModel.Xrm.new_paymentcomponent();
                        lowComp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.AutoLow;
                        lowComp.new_baseamount = rechargeAmount;
                        lowComp.new_currentpaymentamount = rechargeAmount;
                        components.Add(lowComp);
                    }
                    //add headings payment if needed. check environment config and if has already payed this month.
                    int monthlyFeeBase;
                    if (configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.MONTHLY_PAYMENT_FOR_PPA_ENABLED) == 1.ToString()
                        && int.TryParse(configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.MONTHLY_PAYMENT_FEE), out monthlyFeeBase))
                    {
                        if (components.Count == 0)
                        {
                            rechargeAmount = 0;
                        }
                        bool isExempt = IsSupplierExemptOfMonthlyPpaPayment(supplierId);
                        int monthlyFeeToPay = 0;
                        if (!isExempt)
                        {
                            monthlyFeeToPay = CalculateMonthlyPaymentToPay(supplierId, monthlyFeeBase, isTestFirstOfTheMonth);
                        }
                        DML.Xrm.new_paymentcomponent headComp = new NoProblem.Core.DataModel.Xrm.new_paymentcomponent();
                        headComp.new_currentpaymentamount = monthlyFeeToPay;
                        headComp.new_baseamount = monthlyFeeBase;
                        headComp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.MonthlyFeePayment;
                        components.Add(headComp);
                        rechargeAmount += monthlyFeeToPay;
                    }
                }
            }
            else
            {
                DML.Xrm.new_paymentcomponent lowComp = new NoProblem.Core.DataModel.Xrm.new_paymentcomponent();
                lowComp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.AutoLow;
                lowComp.new_baseamount = rechargeAmount;
                lowComp.new_currentpaymentamount = rechargeAmount;
                components.Add(lowComp);
            }
            if (components.Count == 0)
            {
                rechargeAmount = 0;
            }
            return components;
        }

        private bool IsDapazPpaHold(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = dal.Retrieve(supplierId);
            return (supplier.new_dapazstatus.HasValue && supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.PPAHold);
        }

        private bool IsSupplierExemptOfMonthlyPpaPayment(Guid supplierId)
        {
            bool retVal = false;
            string query = @"select new_isexemptofppamonthlypayment from account with(nolock) where accountid = @supplierId";
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            object scalar = dal.ExecuteScalar(query, parameters);
            retVal = (scalar != null && scalar != DBNull.Value && ((bool)scalar));
            return retVal;
        }

        private int GetValueFromConfig(DataAccessLayer.ConfigurationSettings configDal, string key, int defaultValue)
        {
            int value;
            string strValue = configDal.GetConfigurationSettingValue(key);
            if (!int.TryParse(strValue, out value))
            {
                value = defaultValue;
            }
            return value;
        }

        private List<DML.Xrm.new_paymentcomponent> CalculateComponents(CreateSupplierPricingRequest request, ref int paymentForBalance, ref decimal amountNotForBalance)
        {
            //foreach (var item in request.Components)
            //{
            //    LogUtils.MyHandle.WriteToLog("type=" + item.Type + " amountPayed=" + item.AmountPayed + " baseAmount=" + item.BaseAmount);
            //}
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            List<DML.Xrm.new_paymentcomponent> payComponents = new List<NoProblem.Core.DataModel.Xrm.new_paymentcomponent>();
            if (request.Components != null && request.Components.Count > 0)
            {
                foreach (var item in request.Components)
                {
                    DML.Xrm.new_paymentcomponent comp = new NoProblem.Core.DataModel.Xrm.new_paymentcomponent();
                    switch (item.Type)
                    {
                        case SupplierPricingComponent.eSupplierPricingComponentType.MonthlyFeePayment:
                            comp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.MonthlyFeePayment;
                            comp.new_currentpaymentamount = item.AmountPayed;
                            comp.new_baseamount = item.BaseAmount;
                            amountNotForBalance += item.AmountPayed;
                            break;
                        case SupplierPricingComponent.eSupplierPricingComponentType.Manual:
                            comp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.Manual;
                            comp.new_currentpaymentamount = item.AmountPayed;
                            paymentForBalance += decimal.ToInt32(item.AmountPayed);
                            break;
                        case SupplierPricingComponent.eSupplierPricingComponentType.AutoLow:
                            comp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.AutoLow;
                            comp.new_currentpaymentamount = item.AmountPayed;
                            comp.new_baseamount = item.BaseAmount;
                            paymentForBalance += decimal.ToInt32(item.AmountPayed);
                            break;
                        case SupplierPricingComponent.eSupplierPricingComponentType.AutoMaxMonthly:
                            comp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.AutoMaxMonthly;
                            comp.new_currentpaymentamount = item.AmountPayed;
                            comp.new_baseamount = item.BaseAmount;
                            paymentForBalance += decimal.ToInt32(item.AmountPayed);
                            break;
                    }
                    payComponents.Add(comp);
                }
            }
            else
            {
                DML.Xrm.new_paymentcomponent comp = new NoProblem.Core.DataModel.Xrm.new_paymentcomponent();
                comp.new_type = (int)DML.Xrm.new_paymentcomponent.ePaymentComponentType.Manual;
                comp.new_currentpaymentamount = request.PaymentAmount;
                payComponents.Add(comp);
                paymentForBalance = request.PaymentAmount;
            }
            paymentForBalance += request.BonusAmount + request.ExtraBonus;
            return payComponents;
        }

        /// <summary>
        /// must receive as param only real money deposit. return if this is the first.
        /// </summary>
        /// <param name="newSupplierPricing">a real money deposit</param>
        /// <returns></returns>
        private bool IsFirstRealMoneyPayment(NoProblem.Core.DataModel.Xrm.new_supplierpricing newSupplierPricing)
        {
            if (newSupplierPricing.new_isfirstdeposit.HasValue && newSupplierPricing.new_isfirstdeposit.Value)
            {
                return true;
            }
            var supplierId = newSupplierPricing.new_accountid.Value;
            DataAccessLayer.SupplierPricing dal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            bool hasRealButThis = dal.HasRealMoneyButThis(supplierId, newSupplierPricing.new_supplierpricingid);
            return !hasRealButThis;
        }

        private List<DataModel.PublisherPortal.GuidStringPair> GetHeadings(Guid supplierId)
        {
            List<DataModel.PublisherPortal.GuidStringPair> retVal = new List<DataModel.PublisherPortal.GuidStringPair>();
            DataAccessLayer.AccountExpertise acExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            var acExps = acExpDal.GetActiveByAccount(supplierId);
            foreach (var acEx in acExps)
            {
                DataModel.PublisherPortal.GuidStringPair pair = new DataModel.PublisherPortal.GuidStringPair(acEx.new_new_primaryexpertise_new_accountexpertise.new_name, acEx.new_primaryexpertiseid.Value);
                retVal.Add(pair);
            }
            return retVal;
        }

        private int CalculateMonthlyPaymentToPay(Guid supplierId, decimal feeBase)
        {
            return CalculateMonthlyPaymentToPay(supplierId, feeBase, false);
        }

        private int CalculateMonthlyPaymentToPay(Guid supplierId, decimal feeBase, bool isTestFirstOfTheMonth)
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            if (feeBase == 0 || configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.MONTHLY_PAYMENT_FOR_PPA_ENABLED) != 1.ToString())
            {
                return 0;
            }
            decimal thisMonthToPay = 0;
            DataAccessLayer.PaymentComponent payComponentDal = new NoProblem.Core.DataAccessLayer.PaymentComponent(XrmDataContext);
            DateTime? lastPaymentDate = payComponentDal.GetLastPaymentDateOfMonthlyFee(supplierId);
            DateTime nowLocal = FixDateToLocal(DateTime.Now);
            bool alreadyPaidThisMonth = false;
            if (lastPaymentDate.HasValue)
            {
                lastPaymentDate = FixDateToLocal(lastPaymentDate.Value);
                alreadyPaidThisMonth = lastPaymentDate.Value.Month == nowLocal.Month;
            }
            if (!alreadyPaidThisMonth)
            {
                int daysInMonth = DateTime.DaysInMonth(nowLocal.Year, nowLocal.Month);
                int daysToPay = daysInMonth - (isTestFirstOfTheMonth ? 1 : nowLocal.Day) + 1;
                thisMonthToPay = new decimal(daysToPay) * feeBase / new decimal(daysInMonth);
            }
            return (int)Math.Round(thisMonthToPay);
        }

        //private List<ChargingDataHeadingData> GetDataForHeadingsPayments(Guid supplierId, DataAccessLayer.ConfigurationSettings configDal)
        //{
        //    List<ChargingDataHeadingData> retVal = new List<ChargingDataHeadingData>();
        //    if (configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.MONTHLY_PAYMENT_FOR_PPA_ENABLED) == "1")
        //    {
        //        DataAccessLayer.PrimaryExpertise headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
        //        DataAccessLayer.AccountExpertise acExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
        //        DataAccessLayer.PaymentComponent payComponentDal = new NoProblem.Core.DataAccessLayer.PaymentComponent(XrmDataContext);
        //        var acExps = acExpDal.GetActiveByAccount(supplierId);
        //        foreach (var acEx in acExps)
        //        {
        //            ChargingDataHeadingData headData = new ChargingDataHeadingData();
        //            headData.HeadingId = acEx.new_primaryexpertiseid.Value;
        //            var heading = headingDal.Retrieve(headData.HeadingId);
        //            headData.BaseAmount = heading.new_monthlypaymentamount.Value;
        //            headData.HeadingName = heading.new_name;
        //            //DateTime? lastPaymentDate = payComponentDal.GetLastPayment(supplierId, heading.new_primaryexpertiseid);
        //            DateTime nowLocal = FixDateToLocal(DateTime.Now);
        //            //if (lastPaymentDate.HasValue)
        //            //{
        //            //    lastPaymentDate = FixDateToLocal(lastPaymentDate.Value);
        //            //    headData.AlreadyPaidThisMonth = lastPaymentDate.Value.Month == nowLocal.Month;
        //            //}
        //            //if (headData.AlreadyPaidThisMonth)
        //            //{
        //            //    headData.AmountToPay = 0;
        //            //}
        //            //else
        //            //{
        //            //    int daysInMonth = DateTime.DaysInMonth(nowLocal.Year, nowLocal.Month);
        //            //    int daysToPay = daysInMonth - nowLocal.Day + 1;
        //            //    headData.AmountToPay = new decimal(daysToPay) * headData.BaseAmount / new decimal(daysInMonth);
        //            //}
        //            retVal.Add(headData);
        //        }
        //    }
        //    return retVal;
        //}

        private void DoMonthlyDepositsPrivate(bool testFirstOfTheMonth)
        {
            DateTime localNow = FixDateToLocal(DateTime.Now);
            int dayOfMonth = testFirstOfTheMonth ? 1 : localNow.Day;
            DateTime todayUtcStartOfDay = FixDateToUtcFromLocal(localNow.Date);
            DataAccessLayer.AccountRepository accDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            IEnumerable<Guid> suppliersIds = accDal.FindSuppliersForMonthlyCharge(dayOfMonth, todayUtcStartOfDay);
            foreach (var id in suppliersIds)
            {
                AutoRechargeSupplier(id, false, testFirstOfTheMonth);
                Thread.Sleep(1000);
            }
            if (!testFirstOfTheMonth)
            {
                //Check if today is the end of the month and there are more days in other months.
                if (localNow.Day != 31
                    && localNow.Month != localNow.AddDays(1).Month)
                {
                    List<int> days = new List<int>();
                    int currDay = localNow.Day + 1;
                    while (currDay <= 31)
                    {
                        days.Add(currDay);
                        currDay++;
                    }
                    suppliersIds = accDal.FindSuppliersForMonthlyCharge(days, todayUtcStartOfDay);
                    foreach (var id in suppliersIds)
                    {
                        AutoRechargeSupplier(id, false);
                        Thread.Sleep(1000);
                    }
                }
            }
        }

        private DML.Xrm.new_pendingsupplierpricing CreatePendingSpFromRealSp(DML.Xrm.new_supplierpricing supplierPricing)
        {
            DML.Xrm.new_pendingsupplierpricing newPendingSp = new NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing();
            newPendingSp.new_orderstatus = (int)NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Pending;
            newPendingSp.new_orderstatusreason = NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Pending.ToString();
            newPendingSp.new_accountid = supplierPricing.new_accountid;
            newPendingSp.new_bonustype = supplierPricing.new_bonustype;
            newPendingSp.new_chargingcompanyaccountnumber = supplierPricing.new_chargingcompanyaccountnumber;
            newPendingSp.new_chargingcompanycontractid = supplierPricing.new_chargingcompanycontractid;
            newPendingSp.new_credit = supplierPricing.new_credit;
            newPendingSp.new_creditcardownerid = supplierPricing.new_creditcardownerid;
            newPendingSp.new_creditcardownername = supplierPricing.new_creditcardownername;
            newPendingSp.new_creditcardtoken = supplierPricing.new_creditcardtoken;
            newPendingSp.new_cvv2 = supplierPricing.new_cvv2;
            newPendingSp.new_isautorecharge = supplierPricing.new_isautorecharge;
            newPendingSp.new_isfirstdeposit = supplierPricing.new_isfirstdeposit;
            newPendingSp.new_last4digitscc = supplierPricing.new_last4digitscc;
            newPendingSp.new_numberofpayments = supplierPricing.new_numberofpayments;
            newPendingSp.new_paymentmethodid = supplierPricing.new_paymentmethodid;
            newPendingSp.new_pricingmethodid = supplierPricing.new_pricingmethodid;
            newPendingSp.new_rechargeamount = supplierPricing.new_rechargeamount;
            newPendingSp.new_transactionid = supplierPricing.new_transactionid;
            newPendingSp.new_total = supplierPricing.new_total;
            newPendingSp.new_creditcardtype = supplierPricing.new_creditcardtype;
            newPendingSp.new_subscriptionplan = supplierPricing.new_subscriptionplan;
            return newPendingSp;
        }

        private bool ValidateNegativeDeposit(CreateSupplierPricingRequest request, DataAccessLayer.SupplierPricing dal, out eDepositStatus status)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = accountRepositoryDal.Retrieve(request.SupplierId);
            if (supplier.new_cashbalance.HasValue == false)
            {
                status = eDepositStatus.RefundBiggerThanBalance;
                return false;
            }
            var balanceAfterAction = supplier.new_cashbalance.Value + (request.PaymentAmount + request.BonusAmount + request.ExtraBonus);
            if (balanceAfterAction < 0)
            {
                status = eDepositStatus.RefundBiggerThanBalance;
                return false;
            }
            if (request.LookForInvoiceInRefund && request.PaymentMethod != NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.Voucher)
            {
                bool isHisInvoice = dal.IsInvoiceNumberOfSupplier(request.InvoiceNumber, request.SupplierId);
                if (!isHisInvoice)
                {
                    status = eDepositStatus.InvalidInvoiceNumber;
                    return false;
                }
            }
            status = eDepositStatus.OK;
            return true;
        }

        private void UpdateRechargeFieldsAtPayment(CreateSupplierPricingRequest request)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(request.SupplierId);
            string oldRechargeType = GetCurrentRechargeStatus(supplier);
            supplier.new_rechargeenabled = request.IsAutoRenew;
            if (request.IsAutoRenew && request.RechargeAmount > 0)
            {
                supplier.new_rechargeamount = request.RechargeAmount;
                supplier.new_rechargetypecode = (int)request.RechargeTypeCode;
                if (request.RechargeTypeCode == NoProblem.Core.DataModel.Xrm.account.RechargeTypeCode.Monthly)
                {
                    supplier.new_rechargedayofmonth = request.RechargeDayOfMonth;
                }
                else if (request.RechargeTypeCode == NoProblem.Core.DataModel.Xrm.account.RechargeTypeCode.MaxMonthly)
                {
                    supplier.new_rechargedayofmonth = 1;
                }
                DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                int rechargeBonusPercent;
                string percentStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.RECHARGE_BONUS_PERCENT);
                int.TryParse(percentStr, out rechargeBonusPercent);
                supplier.new_rechargebonuspercent = rechargeBonusPercent;
                supplier.new_numberofpayments = request.NumberOfPayments == 0 ? 1 : request.NumberOfPayments;
                supplier.new_userip = request.UserIP;
                supplier.new_useragent = request.UserAgent;
                supplier.new_userremotehost = request.UserRemoteHost;
                supplier.new_useracceptlanguage = request.UserAcceptLanguage;
                if (supplier.new_rechargeenabledon.HasValue == false)
                {
                    supplier.new_rechargeenabledon = DateTime.Now;
                }
            }
            accountRepositoryDal.Update(supplier);
            XrmDataContext.SaveChanges();
            string newRechargeType = GetCurrentRechargeStatus(supplier);
            AuditRechargeTypeChange(request.CreatedByUserId, accountRepositoryDal, supplier, oldRechargeType, newRechargeType);
        }

        private int CalculatePaymentBonus(int rechargeAmount, int bonusPercent)
        {
            int retVal = 0;
            if (bonusPercent > 0)
            {
                retVal = (int)Math.Ceiling(((double)rechargeAmount * (double)bonusPercent / (double)100));
            }
            return retVal;
        }

        private void GetSupplierBasicData(GetSupplierChargingDataResponse response, DML.Xrm.account supplier)
        {
            response.AddressCity = supplier.address1_city;
            response.AddressCounty = supplier.address1_county;
            response.AddressCountry = supplier.address1_country;
            response.AddressState = supplier.address1_stateorprovince;
            response.AddressPostalCode = supplier.address1_postalcode;
            response.AddressStreet = supplier.address1_line1;
            response.AddressHouseNumber = supplier.address1_line2;
            response.FirstName = supplier.new_firstname;
            response.LastName = supplier.new_lastname;
            response.PhoneNumber = supplier.telephone1;
            response.CompanyId = supplier.new_accountgovernmentid;
            response.WebSite = supplier.websiteurl;
            response.OtherPhone = supplier.telephone2;
            response.Fax = supplier.fax;
            response.AccountNumber = supplier.accountnumber;
            response.InvoiceSendAddress = supplier.new_invoicesendaddress;
            response.InvoiceSendOption = supplier.new_invoicesendoption.HasValue ? (DML.Xrm.account.InvoiceSendOption)supplier.new_invoicesendoption.Value : NoProblem.Core.DataModel.Xrm.account.InvoiceSendOption.Email;
        }

        private void GetPricingPackages(GetSupplierChargingDataResponse response, DML.Xrm.account.StageInRegistration stageInRegistration)
        {
            PricingPackagesManager packagesManager = new PricingPackagesManager(XrmDataContext);
            DML.Xrm.new_pricingpackage.PricingPackageSupplierType supplierType;
            if (stageInRegistration == NoProblem.Core.DataModel.Xrm.account.StageInRegistration.PAID)
            {
                supplierType = NoProblem.Core.DataModel.Xrm.new_pricingpackage.PricingPackageSupplierType.Approved;
            }
            else
            {
                supplierType = NoProblem.Core.DataModel.Xrm.new_pricingpackage.PricingPackageSupplierType.Candidate;
            }
            DML.PublisherPortal.GetAllPricingPackagesResponse allPackagesContainer = packagesManager.GetAllPricingPackages(supplierType);
            response.Packages = allPackagesContainer.Packages.Where(x => x.IsActive == true).ToList();
        }

        private bool GetLasSupplierPricingData(Guid supplierId, GetSupplierChargingDataResponse response, DataAccessLayer.SupplierPricing dal)
        {
            DML.Xrm.new_supplierpricing lastSupplierPricing = dal.GetLastSupplierPricing(supplierId);
            bool found = true;
            if (lastSupplierPricing != null)
            {
                response.LastPaymentAmount = lastSupplierPricing.new_total.Value;
                //response.CreditCardToken = lastSupplierPricing.new_creditcardtoken;
                response.LastBonusAmount = lastSupplierPricing.new_credit.Value;
                //response.CVV2 = lastSupplierPricing.new_cvv2;
                //response.Last4DigitsCC = lastSupplierPricing.new_last4digitscc;
                //response.CreditCardOwnerId = lastSupplierPricing.new_creditcardownerid;
                //response.CreditCardOwnerName = lastSupplierPricing.new_creditcardownername;
                //response.IsAutoRenew = lastSupplierPricing.new_renew.Value;
                //response.RechargeAmount = lastSupplierPricing.new_rechargeamount;
                int paymentMethodCode = (lastSupplierPricing.new_new_paymentmethod_new_supplierpricing != null && lastSupplierPricing.new_new_paymentmethod_new_supplierpricing.new_paymentmethodcode.HasValue) ?
                    lastSupplierPricing.new_new_paymentmethod_new_supplierpricing.new_paymentmethodcode.Value :
                 (int)DML.Xrm.new_paymentmethod.ePaymentMethod.Cash;
                response.LastPaymentMethod = (DML.Xrm.new_paymentmethod.ePaymentMethod)paymentMethodCode;
            }
            else
            {
                response.LastPaymentMethod = DML.Xrm.new_paymentmethod.ePaymentMethod.Cash;
                found = false;
            }
            return found;
        }

        private void GetCurrency(GetSupplierChargingDataResponse response)
        {
            DataAccessLayer.SystemUser userDAL = new NoProblem.Core.DataAccessLayer.SystemUser(XrmDataContext);
            DML.Xrm.systemuser user = userDAL.GetCurrentUser();
            DataAccessLayer.UserSettings settingsDAL = new NoProblem.Core.DataAccessLayer.UserSettings(XrmDataContext);
            string currencyCode = settingsDAL.GetUsersDefaultCurrencyCode(user.systemuserid);
            response.Currency = currencyCode;
        }

        #endregion
    }
}
