﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierPricing
{
    internal static class SubscriptionPlansUtils
    {
        public static bool AcceptedEnoughLeads(Guid supplierId)
        {
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            var reader = dal.ExecuteReader(queryAcceptedEnoughLeads, parameters);
            int accepted = 0;
            int rejected = 0;
            foreach (var row in reader)
            {
                if ((bool)row["accepted"])
                    accepted = (int)row["count"];
                else
                    rejected = (int)row["count"];
            }
            if (accepted >= 4)
                return true;
            int total = accepted + rejected;
            if (total == 0)
                return true;
            return ((double)accepted / (double)total) > 0.5;
        }

        private const string queryAcceptedEnoughLeads =
            @"
select COUNT(*) count , isnull(New_LeadAccepted, 0) accepted
from New_incidentaccount
where CreatedOn > DATEADD(month, -1, getdate())
and new_accountid = @supplierId
group by isnull(New_LeadAccepted, 0)
";

        internal static bool AlreadyPaidThisMonth(Guid supplierId, string subscriptionPlan)
        {
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            var reader = dal.ExecuteReader(queryAlreadyPaidThisMonth, parameters);
            if (reader.Count == 0)
                return false;
            var data = reader.First();
            string lastSubscriptionPlan = (string)data["new_subscriptionplan"];
            DateTime lastPayDate = (DateTime)data["createdon"];
            if (lastPayDate.Month == DateTime.Now.Month)
                return true;
            if (lastPayDate.Month + 1 == DateTime.Now.Month && lastSubscriptionPlan == SubscriptionPlansContract.Plans.FEATURED_LISTING
                && subscriptionPlan == SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING)
            {
                return true;
            }
            return false;
        }

        private const string queryAlreadyPaidThisMonth =
            @"
select top 1 new_subscriptionplan, createdon
from New_supplierpricing
where new_accountid = @supplierId
and New_total > 0
and New_SubscriptionPlan is not null
order by CreatedOn desc
";
    }
}
