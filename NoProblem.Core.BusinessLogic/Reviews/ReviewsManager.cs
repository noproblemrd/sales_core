﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Reviews
{
    public class ReviewsManager : XrmUserBase
    {
        #region Ctor
        public ReviewsManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Public Methods

        public DML.Reviews.InsertReviewStatus InsertReview(string description, string phone, string name, bool isLike, Guid supplierId)
        {
            if (string.IsNullOrEmpty(description.Trim())
                || string.IsNullOrEmpty(phone.Trim())
                || string.IsNullOrEmpty(name.Trim())
                || supplierId == Guid.Empty)
            {
                return NoProblem.Core.DataModel.Reviews.InsertReviewStatus.MissingParams;
            }
            DataAccessLayer.Review reviewDal = new NoProblem.Core.DataAccessLayer.Review(XrmDataContext);
            if (AlreadyExists(supplierId, phone, reviewDal))
            {
                return NoProblem.Core.DataModel.Reviews.InsertReviewStatus.AlreadyExists;
            }
            DML.Xrm.incident incident = FindIncident(supplierId, phone);
            if (incident == null)
            {
                return NoProblem.Core.DataModel.Reviews.InsertReviewStatus.NoCaseFound;
            }
            DML.Xrm.new_review review = new NoProblem.Core.DataModel.Xrm.new_review();
            review.new_islike = isLike;
            review.new_phone = phone;
            review.new_description = description;
            review.new_name = name;
            review.new_accountid = supplierId;
            review.new_incidentid = incident.incidentid;
            if(incident.customerid != null){
                review.new_contactid = incident.customerid.Value;
            }
            reviewDal.Create(review);
            XrmDataContext.SaveChanges();
            return NoProblem.Core.DataModel.Reviews.InsertReviewStatus.OK;
        }

        public void UpdateReviewStatus(Guid reviewId, DML.Xrm.new_review.ReviewStatus status)
        {
            DML.Xrm.new_review review = new NoProblem.Core.DataModel.Xrm.new_review(XrmDataContext);
            review.new_reviewid = reviewId;
            review.new_status = (int)status;
            DataAccessLayer.Review reviewDal = new NoProblem.Core.DataAccessLayer.Review(XrmDataContext);
            reviewDal.Update(review);
            XrmDataContext.SaveChanges();
        }

        public List<DML.Reviews.ReviewData> GetReviews(DateTime? fromDate, DateTime? toDate, DML.Xrm.new_review.ReviewStatus? status, Guid? supplierId)
        {
            List<DML.Reviews.ReviewData> retVal = new List<NoProblem.Core.DataModel.Reviews.ReviewData>();
            bool useDates = false;
            DateTime fromDateFixed = new DateTime();
            DateTime toDateFixed = new DateTime();
            if (fromDate.HasValue && toDate.HasValue)
            {
                useDates = true;
                fromDateFixed = fromDate.Value;
                toDateFixed = toDate.Value;
                FixDatesToUtcFullDays(ref fromDateFixed, ref toDateFixed);
            }

            DataAccessLayer.Review reviewDal = new NoProblem.Core.DataAccessLayer.Review(XrmDataContext);
            IEnumerable<DML.Xrm.new_review> reviewsFound = reviewDal.GetReviews(
                (useDates ? fromDateFixed : new DateTime?()),
                (useDates ? toDateFixed : new DateTime?()),
                status,
                supplierId);

            foreach (var review in reviewsFound)
            {
                DML.Reviews.ReviewData data = CreateReviewDataContainer(review);
                retVal.Add(data);
            }
            retVal = (from rev in retVal
                      orderby rev.CreatedOn
                      descending
                      select rev).ToList();
            return retVal;
        } 

        #endregion

        #region Private Methods

        private DML.Reviews.ReviewData CreateReviewDataContainer(NoProblem.Core.DataModel.Xrm.new_review review)
        {
            DML.Reviews.ReviewData data = new NoProblem.Core.DataModel.Reviews.ReviewData();
            data.Description = review.new_description;
            if (review.new_incidentid.HasValue)
            {
                data.IncidentId = review.new_incidentid.Value;
                data.IncidentNumber = review.new_incident_new_review.ticketnumber;
            }
            data.IsLike = review.new_islike.HasValue ? review.new_islike.Value : false;
            data.CostumerId = review.new_contactid.HasValue ? review.new_contactid.Value : Guid.Empty;
            data.Name = review.new_name;
            data.Number = review.new_number;
            data.Phone = review.new_phone;
            data.ReviewStatus = (review.new_status.HasValue ?
                (DML.Xrm.new_review.ReviewStatus)review.new_status.Value :
                DML.Xrm.new_review.ReviewStatus.Candidate);
            if (review.new_accountid.HasValue)
            {
                data.SupplierId = review.new_accountid.Value;
                data.SupplierName = review.new_account_review.name;
            }
            data.CreatedOn = FixDateToLocal(review.createdon.Value);
            data.ReviewId = review.new_reviewid;
            return data;
        }

        private bool AlreadyExists(Guid supplierId, string phone, DataAccessLayer.Review reviewDal)
        {
            IEnumerable<DML.Xrm.new_review> reviewsFound = reviewDal.GetBySupplierAndPhone(supplierId, phone);
            bool retVal = false;
            if (reviewsFound.Count() > 0)
            {
                retVal = true;
            }
            return retVal;
        }

        private NoProblem.Core.DataModel.Xrm.incident FindIncident(Guid supplierId, string phone)
        {
            DML.Xrm.incident incident = null;
            DataAccessLayer.Contact contactDal = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            DML.Xrm.contact contact = contactDal.GetContactByPhoneNumber(phone);
            if (contact != null)
            {
                DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                IEnumerable<DML.Xrm.incident> incidentsFound = incidentDal.GetIncidentsByContact(contact.contactid);
                DataAccessLayer.IncidentAccount inciAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                foreach (var inci in incidentsFound)
                {
                    DML.Xrm.new_incidentaccount inciAccFound = inciAccDal.GetIncidentAccountByIncidentAndAccount(inci.incidentid, supplierId);
                    if (inciAccFound != null)
                    {
                        if (incident == null)
                        {
                            incident = inci;
                        }
                        else if (inci.createdon.Value > incident.createdon.Value)
                        {
                            incident = inci;
                        }
                    }
                }
            }
            return incident;
        }

        #endregion
    }
}
