﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Balance
{
    internal class InviteAFriendCreditor : Runnable
    {
        private Guid referredId;
        DataAccessLayer.ConfigurationSettings config;
        DataAccessLayer.AccountRepository dal;

        public InviteAFriendCreditor(Guid referredId)
        {
            this.referredId = referredId;
            config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
            dal = new DataAccessLayer.AccountRepository(XrmDataContext);
        }

        protected override void Run()
        {
            decimal bonus;
            string strBonus = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_SUCCESS_BONUS);
            if (!decimal.TryParse(strBonus, out bonus))
                bonus = 25;
            var supplier = (
                    from x in dal.All
                    where x.accountid == referredId
                    select new { Cash = x.new_cashbalance }
                ).First();
            decimal balance = supplier.Cash.HasValue ? supplier.Cash.Value : 0;
            decimal newBalance = balance + bonus;
            DataModel.Xrm.account entity = new DataModel.Xrm.account();
            entity.new_cashbalance = newBalance;
            entity.accountid = referredId;
            dal.Update(entity);
            XrmDataContext.SaveChanges();
            BalanceUtils.CreateNewBalanceRow(DataModel.Xrm.new_balancerow.Action.CreditEarned, bonus, newBalance, null, null, "Invite a Friend", referredId, XrmDataContext);
            XrmDataContext.SaveChanges();
        }
    }
}
