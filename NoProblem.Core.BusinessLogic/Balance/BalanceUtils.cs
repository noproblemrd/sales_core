﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Balance
{
    internal static class BalanceUtils
    {
        internal static DataModel.Xrm.new_balancerow CreateNewBalanceRow(
            DataModel.Xrm.new_balancerow.Action action, 
            decimal amount, 
            decimal newBalance, 
            Guid? supplierPricingId, 
            Guid? incidentAccountId, 
            string description,
            Guid supplierId,
            DataModel.Xrm.DataContext xrmDataContext)
        {
            DataAccessLayer.BalanceRow rowDal = new NoProblem.Core.DataAccessLayer.BalanceRow(xrmDataContext);
            DataModel.Xrm.new_balancerow row = new NoProblem.Core.DataModel.Xrm.new_balancerow();
            row.new_action = (int)action;
            if (action == DataModel.Xrm.new_balancerow.Action.Deposit
                || action == NoProblem.Core.DataModel.Xrm.new_balancerow.Action.AutoRecharge
                || action == NoProblem.Core.DataModel.Xrm.new_balancerow.Action.BonusCredit
                || action == NoProblem.Core.DataModel.Xrm.new_balancerow.Action.CreditEarned)
            {
                row.new_supplierpricingid = supplierPricingId;
            }
            else
            {
                row.new_incidentaccountid = incidentAccountId;
            }
            row.new_amount = amount;
            row.new_balanceafteraction = newBalance;
            row.new_description = description;
            row.new_accountid = supplierId;
            rowDal.Create(row);
            return row;
        }
    }
}
