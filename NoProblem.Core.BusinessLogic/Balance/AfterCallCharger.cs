﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.Balance
{
    internal class AfterCallCharger : XrmUserBase
    {
        readonly DataAccessLayer.AccountRepository accountRepository;
        private const string NO_CC_ON_FILE = "No CC found on file.";
        private const string RE_AUTHORIZATION_NEEDED = "Re-authorization of payment method is needed for this advertiser.";
        private bool isReAuthorizationNeeded;
        private Guid supplierId;
        private Guid incidentAccountId;
        private string leadNumber;

        public AfterCallCharger(DataModel.Xrm.DataContext xrmDataContext, Guid supplierId, Guid incidentAccountId, string leadNumber)
            : base(xrmDataContext)
        {
            accountRepository = new DataAccessLayer.AccountRepository(XrmDataContext);
            this.supplierId = supplierId;
            this.incidentAccountId = incidentAccountId;
            this.leadNumber = leadNumber;
        }

        internal void ChargeAfterCall(decimal amountToPay)
        {
            var supplier = (
                    from x in accountRepository.All
                    where x.accountid == supplierId
                    select new { Cash = x.new_cashbalance, IsReAuthorizationNeeded = x.new_paymentmethodauthorizationneeded }
                ).First();
            isReAuthorizationNeeded = supplier.IsReAuthorizationNeeded.HasValue ? supplier.IsReAuthorizationNeeded.Value : false;
            decimal cash = supplier.Cash.HasValue ? supplier.Cash.Value : 0;
            decimal toChargeCC = amountToPay;
            if (toChargeCC > 0 && toChargeCC > cash)
            {
                if (cash > 0)
                {
                    DataModel.Xrm.account entity = new DataModel.Xrm.account();
                    entity.new_cashbalance = 0;
                    entity.accountid = supplierId;
                    accountRepository.Update(entity);
                    BalanceUtils.CreateNewBalanceRow(DataModel.Xrm.new_balancerow.Action.Call, cash * -1, 0, null, incidentAccountId, leadNumber, supplierId, XrmDataContext);
                    XrmDataContext.SaveChanges();
                    toChargeCC -= cash;
                }
                ChargeCreditCard(toChargeCC, amountToPay);
            }
            else//cash covers the amount to pay (no need to charge CC).
            {
                cash -= amountToPay;
                DataModel.Xrm.account entity = new DataModel.Xrm.account();
                entity.new_cashbalance = cash;
                entity.accountid = supplierId;
                accountRepository.Update(entity);
                Balance.BalanceUtils.CreateNewBalanceRow(DataModel.Xrm.new_balancerow.Action.Call, amountToPay * -1, cash, null, incidentAccountId, leadNumber, supplierId, XrmDataContext);
                XrmDataContext.SaveChanges();
            }
        }

        private void ChargeCreditCard(decimal amountToChargeCC, decimal amountToPay)
        {           
            account supplierAccount = accountRepository.Retrieve(supplierId);
            new_incidentaccount leadAccount = supplierAccount.new_account_new_incidentaccount.SingleOrDefault(ic => ic.new_incidentaccountid == incidentAccountId);
            DataModel.Xrm.account.RechargeTypeCode rechargeType = supplierAccount.new_rechargetypecode.HasValue
                ? (account.RechargeTypeCode) supplierAccount.new_rechargetypecode
                : account.RechargeTypeCode.BalanceLow;
            int rechargeAmount = supplierAccount.new_rechargeamount ?? 0;
            int dayOfMonth = supplierAccount.new_rechargedayofmonth ?? 1;
            int rechargeBonusPercent = supplierAccount.new_rechargebonuspercent ?? 0;
            int numberOfPayments = supplierAccount.new_numberofpayments.HasValue &&
                                   supplierAccount.new_numberofpayments.Value > 0
                ? supplierAccount.new_numberofpayments.Value
                : 1;
            string userIP = leadAccount.new_bidip;
            if (GlobalConfigurations.IsDevEnvironment)
            {
                userIP = "62.219.121.253";//override in dev environment since we are gettin invalid ip: ::1
            }
            string userAgent = leadAccount.new_biduseragent;
            string userRemoteHost = leadAccount.new_bidhost;
            string userAcceptLanguage = supplierAccount.new_useracceptlanguage ?? string.Empty;
            decimal cashBalance = supplierAccount.new_cashbalance ?? 0;
            string subscriptionPlan = supplierAccount.new_subscriptionplan;


            string ccToken;
            string cvv2;
            string ccOwnerId;
            string ccOwnerName;
            string last4digits;
            DateTime tokenDate;
            string chargingCompanyContractId;
            string chargingCompanyAccountNumber;
            string ccType;
            string ccExpDate;
            string billingAddress;
            DataAccessLayer.SupplierPricing spDal = new DataAccessLayer.SupplierPricing(XrmDataContext);
            bool found = spDal.FindCreditCardInfo(supplierId, out ccToken, out cvv2, out ccOwnerId, out ccOwnerName, out last4digits, out tokenDate, out chargingCompanyContractId, out chargingCompanyAccountNumber, out ccType, out ccExpDate, out billingAddress);
            if (!found)
            {
                HandleCallChargeFailure(amountToChargeCC, amountToPay, NO_CC_ON_FILE);
                CreatePendingCallCharge(amountToChargeCC, amountToPay, String.Empty, last4digits, true, NO_CC_ON_FILE);
                return;
            }
                        
            if (isReAuthorizationNeeded)
            {
                HandleCallChargeFailure(amountToChargeCC, amountToPay, RE_AUTHORIZATION_NEEDED);
                CreatePendingCallCharge(amountToChargeCC, amountToPay, String.Empty, last4digits, true, RE_AUTHORIZATION_NEEDED);
                return;
            }

            CreditCardSolek.Plimus plimus = new CreditCardSolek.Plimus();
            string solekAnswer, transactionConfirmationNumber;
            bool needsAsyncAproval;
            bool rechargeOk = plimus.RechargeSupplier(ccToken, cvv2, ccOwnerId, amountToChargeCC, amountToChargeCC, 1, out solekAnswer, out transactionConfirmationNumber, last4digits, ccType, chargingCompanyAccountNumber, chargingCompanyContractId,
                userIP, userAgent, userRemoteHost, userAcceptLanguage, out needsAsyncAproval, ccExpDate, ccOwnerName, null, null, DataModel.Xrm.new_paymentmethod.ePaymentMethod.CreditCard, null, false);

            if (!rechargeOk)
            {
                HandleCallChargeFailure(amountToChargeCC, amountToPay, solekAnswer);
            }

            CreatePendingCallCharge(amountToChargeCC, amountToPay, transactionConfirmationNumber, last4digits, !rechargeOk, solekAnswer);
        }

        private void CreatePendingCallCharge(decimal amountToChargeCC, decimal amountToPay, string transactionConfirmationNumber, string last4DigitsCC, bool alreadyFailed, string failReason)
        {
            DataModel.Xrm.new_leadcreditcardcharge charge = new DataModel.Xrm.new_leadcreditcardcharge();
            charge.new_accountid = supplierId;
            charge.new_incidentaccountid = incidentAccountId;
            charge.new_totalamouttopay = amountToPay;
            charge.new_amounttochargecc = amountToChargeCC;
            charge.new_leadnumber = leadNumber;
            charge.new_transactionnumber = transactionConfirmationNumber;
            charge.new_last4digitscc = last4DigitsCC;
            if (alreadyFailed)
            {
                charge.new_solekstatus = (int)DataModel.Xrm.new_leadcreditcardcharge.SolekStatus.Cancelled;
                charge.new_waitingtobechecked = false;
                if (failReason.Length > 4000)
                    failReason = failReason.Substring(0, 4000);
                charge.new_failreason = failReason;
            }
            else
            {
                charge.new_solekstatus = (int)DataModel.Xrm.new_leadcreditcardcharge.SolekStatus.Pending;
                charge.new_waitingtobechecked = true;
                charge.new_waitstartdate = DateTime.Now;
            }
            DataAccessLayer.LeadCreditCardCharge chargeDal = new DataAccessLayer.LeadCreditCardCharge(XrmDataContext);
            chargeDal.Create(charge);
            XrmDataContext.SaveChanges();
        }

        private void HandleCallChargeFailure(decimal amountToChargeCC, decimal amountToPay, string reason)
        {
            CreditCardChargeFailHandler handler = new CreditCardChargeFailHandler(XrmDataContext);
            handler.HandleCCChargeFail(amountToChargeCC, amountToPay, supplierId, incidentAccountId, leadNumber, reason);
        }

    }
}
