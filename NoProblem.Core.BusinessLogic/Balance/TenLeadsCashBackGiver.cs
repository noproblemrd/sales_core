﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Balance
{
    public class TenLeadsCashBackGiver : XrmUserBase
    {
        public TenLeadsCashBackGiver(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public void OnLeadCharge(Guid supplierId)
        {
            DataAccessLayer.LeadCreditCardCharge chargeDal = new DataAccessLayer.LeadCreditCardCharge(XrmDataContext);            
            var query = from b in chargeDal.All
                        where b.new_accountid == supplierId
                        select new { Status = b.new_solekstatus.HasValue ? b.new_solekstatus.Value : 0 };
            var howManyCharged = query.Count(x => x.Status == (int)DataModel.Xrm.new_leadcreditcardcharge.SolekStatus.Accepted);
            if (howManyCharged == 10)
            {
                DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
                decimal tenLeadsBonus;
                string tenLeadsConfig = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.TEN_LEADS_BONUS);
                if (!decimal.TryParse(tenLeadsConfig, out tenLeadsBonus))
                {
                    tenLeadsBonus = 30;
                }
                DataAccessLayer.AccountRepository accDal = new DataAccessLayer.AccountRepository(XrmDataContext);
                var currentBalance = (from acc in accDal.All
                                      where acc.accountid == supplierId
                                      select acc.new_cashbalance).First();
                DataModel.Xrm.account supplier = new DataModel.Xrm.account();
                supplier.accountid = supplierId;
                supplier.new_cashbalance = currentBalance.HasValue ? currentBalance.Value + tenLeadsBonus : tenLeadsBonus;
                accDal.Update(supplier);
                XrmDataContext.SaveChanges();
                BalanceUtils.CreateNewBalanceRow(
                    DataModel.Xrm.new_balancerow.Action.TenLeadsBonus,
                    tenLeadsBonus,
                    supplier.new_cashbalance.Value,
                    null,
                    null,
                    String.Empty,
                    supplierId,
                    XrmDataContext);
                XrmDataContext.SaveChanges();
            }
        }
    }
}
