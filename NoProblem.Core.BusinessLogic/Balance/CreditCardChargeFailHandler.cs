﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Balance
{
    internal class CreditCardChargeFailHandler : XrmUserBase
    {
        public CreditCardChargeFailHandler(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        internal void HandleCCChargeFail(decimal toChargeCC, decimal amountToPay, Guid supplierId, Guid? incidentAccountId, string chargeDescription, string reason)
        {
            HandleCCChargeFail(toChargeCC, amountToPay, supplierId, incidentAccountId, chargeDescription, reason, true);
        }

        internal void HandleCCChargeFail(decimal toChargeCC, decimal amountToPay, Guid supplierId, Guid? incidentAccountId, string chargeDescription, string reason, bool isLeadCharge)
        {
            LogUtils.MyHandle.WriteToLog("Micro payment failed. SupplierId: {0}. ChargeDescription: {1}. IncidentAccountId: {2}. TotalAmount: {3}. ToChargeCC: {4}. Reason: {5}",
                supplierId, chargeDescription, incidentAccountId, amountToPay, toChargeCC, reason);

            //set cash balance to minus and stop supplier from receiving leads.

            bool updated = false;
            decimal newBalance = 0;
            while (!updated)
            {
                DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
                var supplier = (
                        from x in dal.All
                        where x.accountid == supplierId
                        select new { Balance = x.new_cashbalance }
                    ).First();
                decimal oldBalance = supplier.Balance.HasValue ? supplier.Balance.Value : 0;
                newBalance = oldBalance - toChargeCC;
                DataAccessLayer.AccountRepository.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
                parameters.Add("@newBalance", newBalance);
                parameters.Add("@id", supplierId);
                parameters.Add("@oldBalance", oldBalance);
                int changedRows = dal.ExecuteNonQuery(updateBalanceSql, parameters);
                updated = changedRows > 0;
            }

            PaymentFailureMessagesSender sender = new PaymentFailureMessagesSender(supplierId, toChargeCC);
            sender.Start();

            //create balance row (for CC part only).
            BalanceUtils.CreateNewBalanceRow(
                isLeadCharge ? DataModel.Xrm.new_balancerow.Action.Call : DataModel.Xrm.new_balancerow.Action.AutoRecharge,
                toChargeCC * -1,
                newBalance,
                null,
                incidentAccountId,
                chargeDescription,
                supplierId,
                XrmDataContext
                );
            XrmDataContext.SaveChanges();
        }

        private const string updateBalanceSql =
            @"update accountextensionbase set new_cashbalance = @newBalance, new_paymentmethodfailed = 1 where accountid = @id and new_cashbalance = @oldBalance";
    }
}
