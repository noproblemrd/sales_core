﻿using System;
using System.Threading;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Balance
{
    public class BalanceManager : XrmUserBase
    {
        #region Ctor
        public BalanceManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// This function adds the specified amount to the CashBalance property on the account and checks if low balance sms is needed.
        /// </summary>
        /// <param name="AccountId">The balance's Account ID</param>
        /// <param name="Amount">The Amount To Add, if a negative number is passed then it will be subtracted from the balance</param>
        public void ChargeLead(Guid supplierId, decimal amount, DML.Xrm.new_balancerow.Action action, Guid incidentAccountId, string description)
        {
            if (GlobalConfigurations.IsUsaSystem)
            {
                AfterCallCharger moneyManager = new AfterCallCharger(XrmDataContext, supplierId, incidentAccountId, description);
                moneyManager.ChargeAfterCall(amount * -1);
            }
            else
            {
                DataAccessLayer.AccountRepository accountRepositoryAccess = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                DML.Xrm.account supplier = accountRepositoryAccess.Retrieve(supplierId);
                AddToBalance(supplier, amount, accountRepositoryAccess, action, null, incidentAccountId, description);
            }
        }

        /// <summary>
        /// This function adds the specified amount to the CashBalance property on the account and checks if low balance sms is needed.
        /// </summary>
        /// <param name="supplier"></param>
        /// <param name="amount"></param>
        /// <param name="accountRepositoryDal"></param>
        private void AddToBalance(DML.Xrm.account supplier, decimal amount, DataAccessLayer.AccountRepository accountRepositoryDal, DML.Xrm.new_balancerow.Action action, Guid? supplierPricingId, Guid? incidentAccountId, string description)
        {
            bool isUsaSystem = GlobalConfigurations.IsUsaSystem;
            decimal minPrice, maxPrice;
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            accExpDal.GetSupplierMinAndMaxPrices(supplier.accountid, out maxPrice, out minPrice);
            decimal oldBalance = supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value : 0;
            decimal newBalance = amount;
            newBalance += oldBalance;
            supplier.new_cashbalance = newBalance;
            if (!isUsaSystem && oldBalance > newBalance && newBalance < minPrice)
            {
                MakeSupplierOutOfMoney(supplier);
            }
            accountRepositoryDal.Update(supplier);
            if (amount > 0 || !isUsaSystem)
                Balance.BalanceUtils.CreateNewBalanceRow(action, amount, newBalance, supplierPricingId, incidentAccountId, description, supplier.accountid, XrmDataContext);
            XrmDataContext.SaveChanges();
            if (!isUsaSystem && oldBalance > supplier.new_cashbalance)//balance went down.
            {
                SendLowBalanceNotificationIfNeeded(supplier, maxPrice);
                if (supplier.new_cashbalance < maxPrice)
                {
                    if (supplier.new_rechargeenabled.HasValue &&
                           supplier.new_rechargeenabled.Value &&
                           supplier.new_rechargetypecode.HasValue &&
                           supplier.new_rechargetypecode.Value == (int)DML.Xrm.account.RechargeTypeCode.BalanceLow &&
                           !supplier.new_isinactivewithmoney.Value &&
                           (!supplier.new_dapazstatus.HasValue || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPA) &&
                           supplier.new_status == (int)DataModel.Xrm.account.SupplierStatus.Approved)
                    {
                        Thread t1 = new Thread(new ParameterizedThreadStart(AutoRechargeAsync));
                        t1.Start(supplier.accountid);
                    }
                    else if (supplier.new_isinactivewithmoney.Value)
                    {
                        supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Inactive;
                        supplier.new_cashbalance = 0;
                        supplier.new_availablecashbalance = 0;
                        supplier.new_isinactivewithmoney = false;
                        accountRepositoryDal.Update(supplier);
                        XrmDataContext.SaveChanges();
                    }
                }
            }
        }

        public void SendBalanceUnderIncidentPriceNotification(DML.Xrm.account account)
        {
            Notifications notificationsManager = new Notifications(XrmDataContext);
            notificationsManager.NotifySupplier(DML.enumSupplierNotificationTypes.BALANCE_UNDER_INCIDENT_PRICE, account.accountid, account.accountid.ToString(), "account", account.accountid.ToString(), "account");
        }

        /// <summary>
        /// This function adds the specified amount to the Available Cash Balance property on the account
        /// </summary>
        /// <param name="AccountId">The balance's Account ID</param>
        /// <param name="Amount">The Amount To Add, if a negative number is passed then it will be subtracted from the balance</param>
        public void AddToAvailableBalance(Guid supplierId, decimal amount)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(supplierId);
            AddToAvailableBalance(supplier, amount, accountRepositoryDal);
        }

        /// <summary>
        /// This function adds the specified amount to the Available Cash Balance property on the account
        /// </summary>
        /// <param name="supplier"></param>
        /// <param name="amount"></param>
        /// <param name="accountRepositoryDal"></param>
        public void AddToAvailableBalance(DML.Xrm.account supplier, decimal amount, DataAccessLayer.AccountRepository accountRepositoryDal)
        {
            decimal newBalance = amount;
            if (supplier.new_availablecashbalance.HasValue)
            {
                newBalance += supplier.new_availablecashbalance.Value;
            }
            supplier.new_availablecashbalance = newBalance;
            accountRepositoryDal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        /// <summary>
        /// Adds the amount to the balance and returns the new balance.
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="totalAmount"></param>
        /// <param name="action"></param>
        /// <param name="supplierPricingId"></param>
        /// <param name="incidentAccountId"></param>
        /// <returns>The new balance</returns>
        public decimal AddNewCashToAccount(Guid supplierId, decimal totalAmount, DML.Xrm.new_balancerow.Action action, Guid? supplierPricingId, Guid? incidentAccountId, string description)
        {
            DataAccessLayer.AccountRepository accountRepositoryAccess = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryAccess.Retrieve(supplierId);
            decimal oldBalance = supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value : 0;
            AddToBalance(supplier, totalAmount, accountRepositoryAccess, action, supplierPricingId, incidentAccountId, description);
            AddToAvailableBalance(supplier, totalAmount, accountRepositoryAccess);
            decimal maxPrice, minPrice;
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            accExpDal.GetSupplierMinAndMaxPrices(supplierId, out maxPrice, out minPrice);
            if (supplier.new_unavailabilityreasonid.HasValue)
            {
                DataAccessLayer.UnavailabilityReason reasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
                DML.Xrm.new_unavailabilityreason reason = reasonDal.Retrieve(supplier.new_unavailabilityreasonid.Value);
                if (reason.new_ispricingended.HasValue && reason.new_ispricingended.Value && (totalAmount + oldBalance) >= minPrice)
                {
                    supplier.new_unavailabilityreasonid = null;
                    supplier.new_unavailablefrom = null;
                    supplier.new_unavailablelocalfrom = null;
                    supplier.new_unavailableto = null;
                    supplier.new_unavailablelocalto = null;
                }
            }
            //if ((totalAmount + oldBalance) < minPrice)
            //{
            //    //make unavailable because of "out of money":
            //    MakeSupplierOutOfMoney(supplier);
            //}
            accountRepositoryAccess.Update(supplier);
            XrmDataContext.SaveChanges();
            //if (oldBalance == 0 && totalAmount < maxPrice)
            //{
            //    SendBalanceUnderIncidentPriceNotification(supplier);
            //}
            return totalAmount + oldBalance;
        }

        #endregion

        #region Private Methods

        private void AutoRechargeAsync(object supplierIdObj)
        {
            SupplierPricing.SupplierPricingManager spManager = new NoProblem.Core.BusinessLogic.SupplierPricing.SupplierPricingManager(null);
            spManager.AutoRechargeSupplier((Guid)supplierIdObj, true);
        }

        private void MakeSupplierOutOfMoney(DML.Xrm.account supplier)
        {
            DataAccessLayer.UnavailabilityReason reasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
            var pricingEndedReason = reasonDal.GetPricingEndedReason();
            supplier.new_unavailablelocalto = null;
            supplier.new_unavailableto = null;
            supplier.new_unavailablefrom = DateTime.Now;
            if (supplier.address1_utcoffset.HasValue)
            {
                supplier.new_unavailablelocalfrom = FixDateToLocal(DateTime.Now, supplier.address1_utcoffset.Value);
            }
            else
            {
                supplier.new_unavailablelocalfrom = FixDateToLocal(DateTime.Now);
            }
            supplier.new_unavailabilityreasonid = pricingEndedReason.new_unavailabilityreasonid;
        }

        private void SendLowBalanceNotificationIfNeeded(DML.Xrm.account account, decimal maxPrice)
        {
            if (GlobalConfigurations.IsUsaSystem)
                return;
            DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string lowBalanceConfig = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.SUPPLIER_NOTIFY_BALANCE_AMOUNT);

            if (maxPrice > account.new_cashbalance)
            {
                SendBalanceUnderIncidentPriceNotification(account);
            }
            else if (account.new_cashbalance <= decimal.Parse(lowBalanceConfig))
            {
                Notifications notificationsManager = new Notifications(XrmDataContext);
                notificationsManager.NotifySupplier(DML.enumSupplierNotificationTypes.LOW_BALANCE, account.accountid, account.accountid.ToString(), "account", account.accountid.ToString(), "account");
            }
        }

        #endregion
    }
}
