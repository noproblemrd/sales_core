﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Balance
{
    public class PendingCallChargesChecker : XrmUserBase
    {
        private static Timer timer;
        private const int ONE_MINUTE = 1000 * 60;
        private const int TWO_MINUTES = ONE_MINUTE * 2;
        private static CreditCardSolek.Plimus plimus = new CreditCardSolek.Plimus();
        private static DataAccessLayer.Generic genericDal = new DataAccessLayer.Generic();

        private PendingCallChargesChecker()
            : base(null)
        {

        }

        public static void OnAppStart()
        {
            timer = new Timer(
                new TimerCallback(CallBack),
                null,
                ONE_MINUTE,
                System.Threading.Timeout.Infinite);
        }

        private static void CallBack(object status)
        {
            try
            {
                PendingCallChargesChecker instance = new PendingCallChargesChecker();
                instance.Work();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Balance.PendingCallChargesChecker.CallBack");
            }
            finally
            {
                timer = new Timer(
                new TimerCallback(CallBack),
                null,
                TWO_MINUTES,
                System.Threading.Timeout.Infinite);
            }
        }

        private void Work()
        {
            PendingPair pair = FindPendingCallCharge();
            while (pair != null)
            {
                CheckCharge(pair);
                pair = FindPendingCallCharge();
            }
        }

        private void CheckCharge(PendingPair pair)
        {
            string orderStatusReason;
            CreditCardSolek.InvoiceDataContainer invoiceDataContainer;
            DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode orderStatus;
            try
            {
                orderStatus = plimus.CheckPendingRecharge(pair.Transaction, out orderStatusReason, out invoiceDataContainer);
            }
            catch
            {
                orderStatus = DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Pending;
                orderStatusReason = null;
                invoiceDataContainer = null;
            }
            if (orderStatus == NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Accepted)
            {
                HandleAccepted(pair, orderStatus, orderStatusReason, invoiceDataContainer);
            }
            else if (orderStatus == NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Cancelled)
            {
                HandleCancelled(pair, orderStatus, orderStatusReason);
            }
            else
            {
                HandleStillPending(pair);
            }
        }

        private void HandleStillPending(PendingPair pair)
        {
            DataAccessLayer.LeadCreditCardCharge dal = new DataAccessLayer.LeadCreditCardCharge(XrmDataContext);
            DataModel.Xrm.new_leadcreditcardcharge entity = new DataModel.Xrm.new_leadcreditcardcharge();
            entity.new_leadcreditcardchargeid = pair.PendingCallChargeId;
            entity.new_waitingtobechecked = true;
            entity.new_waitstartdate = DateTime.Now;
            dal.Update(entity);
            XrmDataContext.SaveChanges();
        }

        private void HandleCancelled(PendingPair pair, DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode orderStatus, string orderStatusReason)
        {
            DataAccessLayer.LeadCreditCardCharge dal = new DataAccessLayer.LeadCreditCardCharge(XrmDataContext);
            var charge =
                (
                    from x in dal.All
                    where x.new_leadcreditcardchargeid == pair.PendingCallChargeId
                    select new
                    {
                        AmountToChargeCC = x.new_amounttochargecc.Value,
                        TotalAmountToPay = x.new_totalamouttopay.Value,
                        SupplierId = x.new_accountid.Value,
                        IncidentAccountId = x.new_incidentaccountid.Value,
                        LeadNumber = x.new_leadnumber
                    }
                ).First();
            UpdateLeadChargeAfterCheckingDone(pair.PendingCallChargeId, dal, DataModel.Xrm.new_leadcreditcardcharge.SolekStatus.Cancelled, orderStatusReason);
            CreditCardChargeFailHandler handler = new CreditCardChargeFailHandler(XrmDataContext);
            handler.HandleCCChargeFail(charge.AmountToChargeCC, charge.TotalAmountToPay, charge.SupplierId, charge.IncidentAccountId, charge.LeadNumber, orderStatusReason);
        }

        private void HandleAccepted(PendingPair pair, DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode orderStatus, string orderStatusReason, CreditCardSolek.InvoiceDataContainer invoiceDataContainer)
        {
            DataAccessLayer.LeadCreditCardCharge chargeDal = new DataAccessLayer.LeadCreditCardCharge(XrmDataContext);
            var charge = (
                    from x in chargeDal.All
                    where x.new_leadcreditcardchargeid == pair.PendingCallChargeId
                    select new
                    {
                        toChargeCC = x.new_amounttochargecc.Value,
                        IncidentAccountId = x.new_incidentaccountid,
                        LeadNumber = x.new_leadnumber,
                        SupplierId = x.new_accountid
                    }
                ).First();
            UpdateLeadChargeAfterCheckingDone(pair.PendingCallChargeId, chargeDal, DataModel.Xrm.new_leadcreditcardcharge.SolekStatus.Accepted, invoiceDataContainer);
            BalanceUtils.CreateNewBalanceRow(
                DataModel.Xrm.new_balancerow.Action.Call,
                charge.toChargeCC * -1,
                0,
                null,
                charge.IncidentAccountId,
                charge.LeadNumber,
                charge.SupplierId.Value,
                XrmDataContext);
            XrmDataContext.SaveChanges();
            //CheckFor10LeadsBonus(charge.SupplierId.Value);
            SendInvoice(charge.SupplierId.Value, charge.toChargeCC, invoiceDataContainer);
        }

        private void SendInvoice(Guid supplierId, decimal amount, CreditCardSolek.InvoiceDataContainer invoiceDataContainer)
        {               
            DataAccessLayer.ConfigurationSettings configDal = new DataAccessLayer.ConfigurationSettings(XrmDataContext);            
            string invoiceDispatcherName = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVOICE_DISPATCHER);
            Invoices.InvoiceDispatcher dispatcher = Invoices.InvoiceDispatcherFactory.CreateDispatcher(invoiceDispatcherName);
            if (dispatcher != null)
            {
                dispatcher.DispatchInvoice(supplierId, amount, invoiceDataContainer, Invoices.eInvoiceType.SingleLead);
            }
        }

        private void CheckFor10LeadsBonus(Guid supplierId)
        {
            DataAccessLayer.AccountRepository accDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            bool isLegacy = (from acc in accDal.All
                           where acc.accountid == supplierId
                           select acc.new_legacyadvertiser2014.HasValue ? acc.new_legacyadvertiser2014.Value : false).First();
            if (!isLegacy)
            {
                TenLeadsCashBackGiver giver = new TenLeadsCashBackGiver(XrmDataContext);
                giver.OnLeadCharge(supplierId);
            }
        }

        private void UpdateLeadChargeAfterCheckingDone(Guid leadCCChargeId, 
            DataAccessLayer.LeadCreditCardCharge chargeDal, 
            DataModel.Xrm.new_leadcreditcardcharge.SolekStatus status,
            CreditCardSolek.InvoiceDataContainer invoiceDataContainer)
        {
            UpdateLeadChargeAfterCheckingDone(leadCCChargeId, chargeDal, status, String.Empty, invoiceDataContainer);
        }

        private void UpdateLeadChargeAfterCheckingDone(Guid leadCCChargeId,
            DataAccessLayer.LeadCreditCardCharge chargeDal,
            DataModel.Xrm.new_leadcreditcardcharge.SolekStatus status,
            string reason)
        {
            UpdateLeadChargeAfterCheckingDone(leadCCChargeId, chargeDal, status, reason, new CreditCardSolek.InvoiceDataContainer());
        }

        private void UpdateLeadChargeAfterCheckingDone(
            Guid leadCCChargeId, 
            DataAccessLayer.LeadCreditCardCharge chargeDal,
            DataModel.Xrm.new_leadcreditcardcharge.SolekStatus status, 
            string reason,
            CreditCardSolek.InvoiceDataContainer invoiceDataContainer)
        {
            DataModel.Xrm.new_leadcreditcardcharge entity = new DataModel.Xrm.new_leadcreditcardcharge();
            entity.new_leadcreditcardchargeid = leadCCChargeId;
            entity.new_solekstatus = (int)status;
            if (reason.Length > 4000)
                reason = reason.Substring(0, 4000);
            entity.new_failreason = reason;
            entity.new_invoicenumber = invoiceDataContainer.InvoiceNumber;
            entity.new_invoiceurl = invoiceDataContainer.InvoiceUrl;
            entity.new_billingaddress = invoiceDataContainer.BillingAddress1 + " " + invoiceDataContainer.BillingAddress2;
            if (entity.new_billingaddress.Length > 100)
                entity.new_billingaddress = entity.new_billingaddress.Substring(0, 100);
            entity.new_billingcity = invoiceDataContainer.BillingCity;
            entity.new_billingstate = invoiceDataContainer.BillingState;
            entity.new_billingzipcode = invoiceDataContainer.BillingZip;
            entity.new_billingcountry = invoiceDataContainer.BillingCountry;
            entity.new_billingname = invoiceDataContainer.BillingFirstName + " " + invoiceDataContainer.BillingLastName;
            entity.new_billingphone = invoiceDataContainer.BillingPhone;
            chargeDal.Update(entity);
            XrmDataContext.SaveChanges();
        }

        private class PendingPair
        {
            public Guid PendingCallChargeId { get; set; }
            public string Transaction { get; set; }
        }

        private PendingPair FindPendingCallCharge()
        {
            var reader = genericDal.ExecuteReader(findSqlString);
            var found = reader.FirstOrDefault();
            if (found == null)
                return null;
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@id", (Guid)found["new_leadcreditcardchargeid"]);
            int rowsUpdated = genericDal.ExecuteNonQuery(updateSqlString, parameters);
            if (rowsUpdated == 0)
                return FindPendingCallCharge();
            else
                return new PendingPair { PendingCallChargeId = (Guid)found["new_leadcreditcardchargeid"], Transaction = Convert.ToString(found["new_transactionnumber"]) };
        }

        private const string findSqlString =
            @"select top 1 new_leadcreditcardchargeid, new_transactionnumber from new_leadcreditcardcharge where deletionstatecode = 0 and new_waitingtobechecked = 1 and new_waitstartdate < dateadd(minute, -2, getdate())";

        private const string updateSqlString =
            @"update new_leadcreditcardcharge set new_waitingtobechecked = 0 where new_leadcreditcardchargeid = @id and new_waitingtobechecked = 1";
    }
}
