﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Balance
{
    public class PaymentFailureMessagesSender : Runnable
    {
        private Guid supplierId;
        private decimal paymentAmount;
        private DataAccessLayer.AccountRepository dal;
        private DataAccessLayer.ConfigurationSettings config;

        public PaymentFailureMessagesSender(Guid supplierId, decimal paymentAmount)
        {
            this.supplierId = supplierId;
            this.paymentAmount = paymentAmount;
            dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
        }

        protected override void Run()
        {
            SupplierContactData data = GetSupplierData();
            SendEmail(data.Name, data.Email);
            SendSms(data.Phone, data.Country);
        }

        private void SendEmail(string name, string email)
        {
            try
            {
                MandrillSender mSender = new MandrillSender();
                DataAccessLayer.MandrillEmailSubjects subjectsDal = new DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
                string subject = subjectsDal.GetEmailSubject(DataModel.MandrillTemplates.E_AD_PaymentFailed);
                Dictionary<string, string> vars = new Dictionary<string, string>();
                vars.Add("NPVAR_ADV_NAME", name);
                vars.Add("NPVAR_PAY_URL", GetFullUrl());
                vars.Add("NPVAR_CHARGE_AMOUNT", String.Format("{0:0.00}", paymentAmount));
                bool sent = mSender.SendTemplate(email, subject, DataModel.MandrillTemplates.E_AD_PaymentFailed, vars);
                if (!sent)
                    throw new Exception("Email driver returned false!");
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Failed to send email in PaymentFailureMessagesSender to {0}. Email: {1}", name, email);
            }
        }

        private string GetFullUrl()
        {
            return config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PAYMENT_FAILED_EMAIL_URL);
        }

        private string GetShortUrl()
        {
            return config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PAYMENT_FAILED_SMS_URL);
        }

        private const string SMS_TEMPLATE = "Dear NoProblem advertiser, your payment method failed on your last charge. Go to {0} to pay now to keep getting leads.";

        private void SendSms(string phone, string country)
        {
            try
            {
                Notifications notifier = new Notifications(XrmDataContext);
                string url = GetShortUrl();
                string text = String.Format(SMS_TEMPLATE, url);
                bool sent = notifier.SendSMSTemplate(text, phone, country);
                if (!sent)
                    throw new Exception("Sms sender class returned false!");
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Failed to send sms in PaymentFailureMessagesSender to {0}.", phone);
            }
        }

        private SupplierContactData GetSupplierData()
        {
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new SupplierContactData { Phone = acc.telephone1, Email = acc.emailaddress1, Name = acc.name, Country = acc.new_country };
            SupplierContactData supplierData = query.FirstOrDefault();
            if (supplierId == null)
                throw new Exception("SupplierId not found");
            return supplierData;
        }

        private class SupplierContactData
        {
            public string Name { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public string Country { get; set; }
        }
    }
}
