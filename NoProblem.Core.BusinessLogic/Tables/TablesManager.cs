﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Tables;

namespace NoProblem.Core.BusinessLogic.Tables
{
    public class TablesManager : XrmUserBase
    {
        #region Ctor
        public TablesManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        public void DeleteUnavailabilityReasons(List<Guid> lst)
        {
            DataAccessLayer.UnavailabilityReason dal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
            dal.Disable(lst);
        }

        public void UpsertUnavailabilityReason(Guid guid, string name, bool isSystem, int groupCode)
        {
            DataAccessLayer.UnavailabilityReason dal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
            DataAccessLayer.UnavailabilityReasonGroup groupDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReasonGroup(XrmDataContext);
            Guid groupId = groupDal.GetIdByCode(groupCode);
            if (groupId == Guid.Empty)
            {
                throw new Exception("GroupCode does not exists");
            }
            if (guid != Guid.Empty)
            {
                dal.UpdateDirect(guid, name, isSystem, groupId);
            }
            else
            {
                DataModel.Xrm.new_unavailabilityreason reason = new NoProblem.Core.DataModel.Xrm.new_unavailabilityreason();
                reason.new_name = name;
                reason.new_issystemreason = isSystem;
                reason.new_groupid = groupId;
                dal.Create(reason);
                XrmDataContext.SaveChanges();
            }
        }

        public void DeleteTableData(eTableType type, List<Guid> ids)
        {
            string tableName = GetTableName(type);
            StringBuilder query = new StringBuilder();
            query.Append("update ");
            query.Append(tableName);
            query.Append(" set new_disabled = 1 where ");
            query.Append(tableName).Append("id ");
            query.Append(" in ( ");
            for (int i = 0; i < ids.Count; i++)
            {
                query.Append("'").Append(ids[i].ToString()).Append("'");
                if (i < ids.Count - 1)
                {
                    query.Append(",");
                }
            }
            query.Append(" ) ");
            if (type == eTableType.InactivityReason)
            {
                query.Append(@" and new_inactivityreasonid != ( select top 1 new_inactivityreasonid
                        from new_inactivityreason with(nolock)
                        where deletionstatecode = 0
                        and new_isrequestedbyapireason = 1 ) ");
            }
            DataAccessLayer.UpsaleLoseReason dal = new NoProblem.Core.DataAccessLayer.UpsaleLoseReason(XrmDataContext);
            dal.ExecuteNonQuery(query.ToString());
        }

        public void UpsertTableData(eTableType type, Guid guid, string name)
        {
            if (guid == Guid.Empty)
            {
                string nextId = GetNextId(type);
                switch (type)
                {
                    case eTableType.ComplaintSeverity:
                        CreateComplaintSeveriry(name, nextId);
                        break;
                    case eTableType.ComplaintStatus:
                        CreateComplaintStatus(name, nextId);
                        break;
                    case eTableType.InactivityReason:
                        CreateInactivityReason(name, nextId);
                        break;
                    case eTableType.RefundReason:
                        CreateRefundReason(name, nextId);
                        break;
                    case eTableType.UpsaleLoseReason:
                        CreateUpsaleLoseReason(name, nextId);
                        break;
                    case eTableType.ExtraBonusReason:
                        CreateExtraBonusReason(name, nextId);
                        break;
                    case eTableType.VoucherReason:
                        CreateVoucherReason(name, nextId);
                        break;
                }
            }
            else
            {
                string tableName = GetTableName(type);
                string updateQuery =
                    @"update " + tableName + " set new_name = @name where " + tableName + "id = @guid";
                DataAccessLayer.UpsaleLoseReason dal = new NoProblem.Core.DataAccessLayer.UpsaleLoseReason(XrmDataContext);
                List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
                parameters.Add(new KeyValuePair<string, object>("@guid", guid));
                parameters.Add(new KeyValuePair<string, object>("@name", name));
                dal.ExecuteNonQuery(updateQuery, parameters);
            }
        }

        public List<TableRowData> GetTableData(DataModel.Tables.eTableType type)
        {
            List<TableRowData> retVal = null;
            switch (type)
            {                
                case eTableType.ComplaintStatus:
                    retVal = GetComplaintStatuses();
                    break;
                case eTableType.ComplaintSeverity:
                    retVal = GetComplaintSeverities();
                    break;
                default:
                    retVal = GetAllGeneric(type);
                    break;
            }
            return retVal;
        }

        #endregion

        #region Private Methods

        private string GetTableName(eTableType type)
        {
            string tableName = string.Empty;
            switch (type)
            {
                case eTableType.UpsaleLoseReason:
                    tableName = "new_upsalelosereason";
                    break;
                case eTableType.RefundReason:
                    tableName = "new_refundreason";
                    break;
                case eTableType.InactivityReason:
                    tableName = "new_inactivityreason";
                    break;
                case eTableType.ComplaintStatus:
                    tableName = "new_helpdeskstatus";
                    break;
                case eTableType.ComplaintSeverity:
                    tableName = "new_helpdeskseverity";
                    break;
                case eTableType.VoucherReason:
                    tableName = "new_voucherreason";
                    break;
                case eTableType.ExtraBonusReason:
                    tableName = "new_extrabonusreason";
                    break;
            }
            return tableName;
        }

        private string GetNextId(eTableType type)
        {
            string tableName = GetTableName(type);
            string idField = GetIdFieldName(type);
            string query = @"select max(cast(" + idField + " as int)) + 1 from " + tableName + " with(nolock) where deletionstatecode = 0";
            DataAccessLayer.UpsaleLoseReason dal = new NoProblem.Core.DataAccessLayer.UpsaleLoseReason(XrmDataContext);
            return ((int)dal.ExecuteScalar(query)).ToString();
        }

        private string GetIdFieldName(eTableType type)
        {
            if (type == eTableType.RefundReason)
            {
                return "new_code";
            }
            return "new_id";
        }

        #region Get Methods

        private List<TableRowData> GetAllGeneric(eTableType type)
        {
            var dal = TablesDalFactory.GetTableDal(type, XrmDataContext);
            return dal.GetAll();
        }
           
        private List<TableRowData> GetComplaintSeverities()
        {
            DataAccessLayer.HdSeverity dal = new NoProblem.Core.DataAccessLayer.HdSeverity(XrmDataContext);
            Guid hdTypeId = GetComplaintHdTypeId();
            var severities = dal.GetAll(hdTypeId);
            List<TableRowData> retVal = (from s in severities
                                         select new TableRowData()
                                         {
                                             Guid = s.Id,
                                             Id = s.IdSmall,
                                             Inactive = s.Inactive,
                                             Name = s.Name
                                         }).ToList();
            return retVal;
        }

        private List<TableRowData> GetComplaintStatuses()
        {

            DataAccessLayer.HdStatus dal = new NoProblem.Core.DataAccessLayer.HdStatus(XrmDataContext);
            Guid hdTypeId = GetComplaintHdTypeId();
            var severities = dal.GetAll(hdTypeId);
            List<TableRowData> retVal = (from s in severities
                                         select new TableRowData()
                                         {
                                             Guid = s.Id,
                                             Id = s.IdSmall,
                                             Inactive = s.Inactive,
                                             Name = s.Name
                                         }).ToList();
            return retVal;
        }

        #endregion

        #region Create Methods

        private void CreateUpsaleLoseReason(string name, string nextId)
        {
            DataModel.Xrm.new_upsalelosereason reason = new NoProblem.Core.DataModel.Xrm.new_upsalelosereason();
            reason.new_name = name;
            reason.new_id = nextId;
            DataAccessLayer.UpsaleLoseReason dal = new NoProblem.Core.DataAccessLayer.UpsaleLoseReason(XrmDataContext);
            dal.Create(reason);
            XrmDataContext.SaveChanges();
        }

        private void CreateRefundReason(string name, string nextId)
        {
            DataModel.Xrm.new_refundreason reason = new NoProblem.Core.DataModel.Xrm.new_refundreason();
            reason.new_name = name;
            reason.new_code = int.Parse(nextId);
            DataAccessLayer.RefundReason dal = new NoProblem.Core.DataAccessLayer.RefundReason(XrmDataContext);
            dal.Create(reason);
            XrmDataContext.SaveChanges();
        }

        private void CreateInactivityReason(string name, string nextId)
        {
            DataModel.Xrm.new_inactivityreason reason = new NoProblem.Core.DataModel.Xrm.new_inactivityreason();
            reason.new_name = name;
            reason.new_id = nextId;
            DataAccessLayer.InactivityReason dal = new NoProblem.Core.DataAccessLayer.InactivityReason(XrmDataContext);
            dal.Create(reason);
            XrmDataContext.SaveChanges();
        }

        private void CreateComplaintStatus(string name, string nextId)
        {
            Guid hdTypeId = GetComplaintHdTypeId();
            DataModel.Xrm.new_helpdeskstatus status = new NoProblem.Core.DataModel.Xrm.new_helpdeskstatus();
            status.new_name = name;
            status.new_helpdesktypeid = hdTypeId;
            status.new_id = nextId;
            DataAccessLayer.HdStatus dal = new NoProblem.Core.DataAccessLayer.HdStatus(XrmDataContext);
            dal.Create(status);
            XrmDataContext.SaveChanges();
        }

        private void CreateComplaintSeveriry(string name, string nextId)
        {
            Guid hdTypeId = GetComplaintHdTypeId();
            DataModel.Xrm.new_helpdeskseverity severity = new NoProblem.Core.DataModel.Xrm.new_helpdeskseverity();
            severity.new_name = name;
            severity.new_id = nextId;
            severity.new_helpdesktypeid = hdTypeId;
            DataAccessLayer.HdSeverity dal = new NoProblem.Core.DataAccessLayer.HdSeverity(XrmDataContext);
            dal.Create(severity);
            XrmDataContext.SaveChanges();
        }

        private void CreateVoucherReason(string name, string nextId)
        {
            DataAccessLayer.VoucherReason dal = new NoProblem.Core.DataAccessLayer.VoucherReason(XrmDataContext);
            DataModel.Xrm.new_voucherreason reason = new NoProblem.Core.DataModel.Xrm.new_voucherreason();
            reason.new_name = name;
            reason.new_id = nextId;
            dal.Create(reason);
            XrmDataContext.SaveChanges();
        }

        private void CreateExtraBonusReason(string name, string nextId)
        {
            DataAccessLayer.ExtraBonusReason dal = new NoProblem.Core.DataAccessLayer.ExtraBonusReason(XrmDataContext);
            DataModel.Xrm.new_extrabonusreason reason = new NoProblem.Core.DataModel.Xrm.new_extrabonusreason();
            reason.new_name = name;
            reason.new_id = nextId;
            dal.Create(reason);
            XrmDataContext.SaveChanges();
        }

        #endregion

        private Guid GetComplaintHdTypeId()
        {
            DataAccessLayer.HdType hdTypesDal = new NoProblem.Core.DataAccessLayer.HdType(XrmDataContext);
            var hdTypes = hdTypesDal.GetAll();
            var complaintsType = hdTypes.FirstOrDefault(x => x.Name == "Complaint");
            if (complaintsType == null)
            {
                throw new Exception("Complaint helpdesk type not define in the system, please define it.");
            }
            return complaintsType.Id;
        }

        #endregion
    }
}