﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Tables
{
    internal class TablesDalFactory
    {
        internal static DataAccessLayer.ITableDal GetTableDal(DataModel.Tables.eTableType tableType, DataModel.Xrm.DataContext xrmDataContext)
        {
            DataAccessLayer.ITableDal retVal = null;
            switch (tableType)
            {
                case NoProblem.Core.DataModel.Tables.eTableType.ExtraBonusReason:
                    retVal = new DataAccessLayer.ExtraBonusReason(xrmDataContext);
                    break;
                case NoProblem.Core.DataModel.Tables.eTableType.InactivityReason:
                    retVal = new DataAccessLayer.InactivityReason(xrmDataContext);
                    break;
                case NoProblem.Core.DataModel.Tables.eTableType.RefundReason:
                    retVal = new DataAccessLayer.RefundReason(xrmDataContext);
                    break;
                case NoProblem.Core.DataModel.Tables.eTableType.UpsaleLoseReason:
                    retVal = new DataAccessLayer.UpsaleLoseReason(xrmDataContext);
                    break;
                case NoProblem.Core.DataModel.Tables.eTableType.UpsalePendingReason:
                    retVal = new DataAccessLayer.UpsalePendingReason(xrmDataContext);
                    break;
                case NoProblem.Core.DataModel.Tables.eTableType.VoucherReason:
                    retVal = new DataAccessLayer.VoucherReason(xrmDataContext);
                    break;
            }
            return retVal;
        }
    }
}
