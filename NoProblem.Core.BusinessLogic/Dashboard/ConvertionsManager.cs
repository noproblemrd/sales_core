﻿using System;
using NoProblem.Core.DataModel.Dashboard;
using System.Collections.Generic;
using NoProblem.Core.DataModel.Dashboard.Convertion;
using System.Linq;

namespace NoProblem.Core.BusinessLogic.Dashboard
{
    public class ConvertionsManager : XrmUserBase
    {
        #region Ctor
        public ConvertionsManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        public void CreateExposure(CreateExposureRequest request)
        {
            //all exposures are saves in the web only from now.
            //ExposureCreator creator = new ExposureCreator(request);
            //creator.CreateExposure();
        }
        
        public ExposuresPicklistsContainer GetExposuresPicklists()
        {
            ExposuresPicklistsContainer container = new ExposuresPicklistsContainer();
            DataAccessLayer.Exposure dal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
            List<string> controls = dal.GetExposuresControlNames();
            container.Controls = controls;
            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            IEnumerable<DataModel.PublisherPortal.GuidStringPair> origins = originDal.GetAllOrigins(true, false);
            container.Origins = origins.ToList();
            return container;
        }

        public void CreateWidgetRemovalLog(CreateWidgetRemovalLogRequest request)
        {
            DataAccessLayer.WidgetRemovalLog dal = new NoProblem.Core.DataAccessLayer.WidgetRemovalLog(XrmDataContext);
            DataModel.Xrm.new_widgetremovallog log = new NoProblem.Core.DataModel.Xrm.new_widgetremovallog();
            if (request.OriginId != Guid.Empty)
            {
                log.new_originid = request.OriginId;
            }
            if (!string.IsNullOrEmpty(request.HeadingCode))
            {
                DataAccessLayer.PrimaryExpertise headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
                Guid headingId = headingDal.GetPrimaryExpertiseIdByCode(request.HeadingCode);
                if (headingId != Guid.Empty)
                {
                    log.new_primaryexpertiseid = headingId;
                }
            }
            log.new_keyword = request.Keyword;
            log.new_controlname = request.ControlName;
            log.new_removeperiod = request.RemovePeriod;
            log.new_type = request.LogType.ToString();
            if (request.Url != null)
            {
                if (request.Url.Length > 1000)
                {
                    request.Url = request.Url.Substring(0, 1000);
                }
                log.new_url = request.Url;
            }
            dal.Create(log);
            XrmDataContext.SaveChanges();
        }

        #endregion
    }
}
