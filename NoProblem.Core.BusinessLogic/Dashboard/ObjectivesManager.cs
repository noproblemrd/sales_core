﻿using System;
using System.Collections.Generic;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Dashboard
{
    public class ObjectivesManager : XrmUserBase
    {
        #region Ctor
        public ObjectivesManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Public Methods

        public DML.Dashboard.CreateObjectiveResponse CreateObjective(DML.Dashboard.CreateObjectiveRequest request)
        {
            DML.Dashboard.CreateObjectiveResponse response = new NoProblem.Core.DataModel.Dashboard.CreateObjectiveResponse();
            bool overlaps = false;
            DataAccessLayer.Objective objectiveDal = new NoProblem.Core.DataAccessLayer.Objective(XrmDataContext);
            if (request.IsApproved == false)
            {
                overlaps = objectiveDal.IsOverlaping(request.FromDate.Date, request.ToDate.Date, request.ObjectiveId, request.Criteria);
                if (overlaps)
                {
                    response.NeedsApproval = true;
                    return response;
                }
            }
            else
            {
                overlaps = true;
            }
            if (request.ObjectiveId != Guid.Empty)
            {
                objectiveDal.Delete(request.ObjectiveId);
            }
            if (overlaps)
            {
                bool done = CheckCutsOneInTheMiddle(request, objectiveDal);
                if (!done)
                {
                    objectiveDal.FixOldOverlapedObjectives(request.FromDate.Date, request.ToDate.Date, request.Criteria);
                }
            }
            CreateNewObjective(request.Amount, request.FromDate, request.ToDate, request.Criteria, objectiveDal);
            response.NeedsApproval = false;
            return response;
        }

        public List<DML.Dashboard.ObjectiveData> GetAllObjectives()
        {
            DataAccessLayer.Objective objectiveDal = new NoProblem.Core.DataAccessLayer.Objective(XrmDataContext);
            List<DML.Dashboard.ObjectiveData> dataList = objectiveDal.GetAllObjectives();
            return dataList;
        }

        public void DeleteObjectives(List<Guid> objectives)
        {
            DataAccessLayer.Objective dal = new NoProblem.Core.DataAccessLayer.Objective(XrmDataContext);
            foreach (Guid id in objectives)
            {
                dal.Delete(id);
            }
        }

        #endregion

        #region Private Methods

        private bool CheckCutsOneInTheMiddle(DML.Dashboard.CreateObjectiveRequest request, DataAccessLayer.Objective dal)
        {
            DateTime existsFrom;
            DateTime existsTo;
            decimal existsAmount;
            Guid existsId;
            bool cuts = dal.IsCutsInTheMiddle(request.Criteria, request.FromDate.Date, request.ToDate.Date, out existsFrom, out existsTo, out existsAmount, out existsId);
            if (cuts)
            {
                dal.Delete(existsId);
                CreateNewObjective(existsAmount, existsFrom, request.FromDate.Date.AddDays(-1), request.Criteria, dal);
                CreateNewObjective(existsAmount, request.ToDate.Date.AddDays(1), existsTo, request.Criteria, dal);
            }
            return cuts;
        }

        private void CreateNewObjective(decimal amount, DateTime fromDate, DateTime toDate, DML.Dashboard.Criteria criteria , DataAccessLayer.Objective objectiveDal)
        {
            DML.Xrm.new_objective newObjective = new NoProblem.Core.DataModel.Xrm.new_objective();
            newObjective.new_amount = amount;
            newObjective.new_fromdate = fromDate.Date;
            newObjective.new_todate = toDate.Date;
            newObjective.new_criteria = (int)criteria;
            objectiveDal.Create(newObjective);
            XrmDataContext.SaveChanges();

            DataAccessLayer.ObjectiveDayData objDayDataDal = new NoProblem.Core.DataAccessLayer.ObjectiveDayData(XrmDataContext);
            TimeSpan span = newObjective.new_todate.Value - newObjective.new_fromdate.Value;
            decimal days = span.Days + 1;
            decimal amountPerDay = newObjective.new_amount.Value;
            for (int i = 0; i < days; i++)
            {
                DML.Xrm.new_objectivedaydata dayData = new NoProblem.Core.DataModel.Xrm.new_objectivedaydata();
                dayData.new_objectiveid = newObjective.new_objectiveid;
                dayData.new_date = newObjective.new_fromdate.Value.AddDays(i);
                dayData.new_amount = amountPerDay;
                objDayDataDal.Create(dayData);
                XrmDataContext.SaveChanges();
            }
        } 

        #endregion
    }
}
