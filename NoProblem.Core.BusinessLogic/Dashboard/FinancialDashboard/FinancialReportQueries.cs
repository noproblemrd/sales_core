﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dashboard.FinancialDashboard
{
    internal sealed class FinancialReportQueries
    {
        #region Distributions Queries

        #region queryAllDistributionsAndAllAndAll
        public const string queryAllDistributionsAndAllAndAll =
@"
declare @exposures int
declare @requests int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @requestsWithNetActualRevenue int
declare @costOfInstallation real
declare @fullFullfilmentRevenue decimal
declare @netActualRevenueUpsales decimal
declare @upsales int

select @costOfInstallation = avg(new_costofinstallation)
from new_origin with(nolock)
where new_isinfinancialdashboard = 1
and new_isdistribution = 1
and new_costofinstallation is not null
and deletionstatecode = 0

select @exposures = SUM(exp.Exposures)
from ConversionReportMongo exp with(nolock)
join new_origin org with(nolock) on org.New_originId = exp.OriginId
where dateadd(day, datediff(day, 0, exp.Date),0) = @date
and org.New_IsDistribution = 1
and org.New_IsInFinancialDashboard = 1

select @requests = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @upsales = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0
and inc.new_isupsale = 1

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1) tbl1

select @potentailRevenue = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1

select @actualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1

select @netActualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithNetActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and New_tocharge = 1
and ia.CreatedOn between @from and @to
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1

select @netActualRevenueUpsales = SUM(ia.new_potentialincidentprice), @requestsWithNetActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and New_tocharge = 1
and ia.CreatedOn between @from and @to
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1
and inc.new_isupsale = 1

select 
@exposures as exposures
,@requests as requests
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@requestsWithActualRevenue as requestsWithActualRevenue 
,@netActualRevenue as netActualRevenue
,@requestsWithNetActualRevenue as requestsWithNetActualRevenue
,@costOfInstallation as costOfInstallation
,@fullFullfilmentRevenue as fullFullfilmentRevenue
,@netActualRevenueUpsales as netActualRevenueUpsales
,@upsales as upsales
";
        #endregion
        
        #region queryAllDistributionsAndAllAndFlavor
        /*
        public const string queryAllDistributionsAndAllAndFlavor =
            @"
declare @exposures int
declare @requests int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @requestsWithNetActualRevenue int
declare @costOfInstallation real
declare @fullFullfilmentRevenue decimal

select @costOfInstallation = avg(new_costofinstallation)
from new_origin with(nolock)
where new_isinfinancialdashboard = 1
and new_isdistribution = 1
and new_costofinstallation is not null
and deletionstatecode = 0

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
join new_origin org with(nolock) on exp.new_originid = org.New_originId
where exp.createdon between @from and @to
and org.new_isdistribution = 1
and (exp.new_originid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or exp.new_originid is null)
                and (org.new_parentoriginid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or org.new_parentoriginid is null)
                and isnull(exp.new_iswelcomescreenevent, 0) = 0
and exp.new_controlname = @flavor
and org.new_isinfinancialdashboard = 1

select @requests = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
and org.new_isdistribution = 1
and new_controlname = @flavor
and org.new_isinfinancialdashboard = 1
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1
and inc.new_controlname = @flavor) tbl1

select @potentailRevenue = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and org.new_isdistribution = 1
and inc.new_controlname = @flavor
and org.new_isinfinancialdashboard = 1

select @actualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and org.new_isdistribution = 1
and inc.new_controlname = @flavor
and org.new_isinfinancialdashboard = 1

select @netActualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithNetActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and New_tocharge = 1
and ia.CreatedOn between @from and @to
and org.new_isdistribution = 1
and inc.new_controlname = @flavor
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0

select 
@exposures as exposures
,@requests as requests
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@requestsWithActualRevenue as requestsWithActualRevenue 
,@netActualRevenue as netActualRevenue
,@requestsWithNetActualRevenue as requestsWithNetActualRevenue
,@costOfInstallation as costOfInstallation
,@fullFullfilmentRevenue as fullFullfilmentRevenue
";
         */
        #endregion
         
        #region queryAllDistributionsAndHeadingAndAll
        /*
        public const string queryAllDistributionsAndHeadingAndAll =
            @"
declare @exposures int
declare @requests int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @requestsWithNetActualRevenue int
declare @costOfInstallation real
declare @fullFullfilmentRevenue decimal

select @costOfInstallation = avg(new_costofinstallation)
from new_origin with(nolock)
where new_isinfinancialdashboard = 1
and new_isdistribution = 1
and new_costofinstallation is not null
and deletionstatecode = 0

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
join new_origin org with(nolock) on exp.new_originid = org.New_originId
where exp.createdon between @from and @to
and org.new_isdistribution = 1
and exp.new_primaryexpertiseid = @headingId
and (exp.new_originid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or exp.new_originid is null)
                and (org.new_parentoriginid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or org.new_parentoriginid is null)
                and isnull(exp.new_iswelcomescreenevent, 0) = 0
and org.new_isinfinancialdashboard = 1

select @requests = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
and org.new_isdistribution = 1
and inc.new_primaryexpertiseid = @headingId
and org.new_isinfinancialdashboard = 1
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId) tbl1

select @potentailRevenue = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_primaryexpertiseid = @headingId
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1

select @actualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_primaryexpertiseid = @headingId
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1

select @netActualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithNetActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_primaryexpertiseid = @headingId
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0

select 
@exposures as exposures
,@requests as requests
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@requestsWithActualRevenue as requestsWithActualRevenue 
,@netActualRevenue as netActualRevenue
,@requestsWithNetActualRevenue as requestsWithNetActualRevenue
,@costOfInstallation as costOfInstallation
,@fullFullfilmentRevenue as fullFullfilmentRevenue
";
         */
        #endregion
        
        #region queryAllDistributionsAndHeadingAndFlavor
        /*
        public const string queryAllDistributionsAndHeadingAndFlavor =
            @"
declare @exposures int
declare @requests int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @requestsWithNetActualRevenue int
declare @costOfInstallation real
declare @fullFullfilmentRevenue decimal

select @costOfInstallation = avg(new_costofinstallation)
from new_origin with(nolock)
where new_isinfinancialdashboard = 1
and new_isdistribution = 1
and new_costofinstallation is not null
and deletionstatecode = 0

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
join new_origin org with(nolock) on exp.new_originid = org.New_originId
where exp.createdon between @from and @to
and org.new_isdistribution = 1
and exp.new_primaryexpertiseid = @headingId
and (exp.new_originid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or exp.new_originid is null)
                and (org.new_parentoriginid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or org.new_parentoriginid is null)
                and isnull(exp.new_iswelcomescreenevent, 0) = 0
and exp.new_controlname = @flavor
and org.new_isinfinancialdashboard = 1

select @requests = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
and org.new_isdistribution = 1
and inc.new_primaryexpertiseid = @headingId
and new_controlname = @flavor
and org.new_isinfinancialdashboard = 1
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isdistribution = 1
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor) tbl1

select @potentailRevenue = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_primaryexpertiseid = @headingId
and org.new_isdistribution = 1
and inc.new_controlname = @flavor
and org.new_isinfinancialdashboard = 1

select @actualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_primaryexpertiseid = @headingId
and org.new_isdistribution = 1
and inc.new_controlname = @flavor
and org.new_isinfinancialdashboard = 1

select @netActualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithNetActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_primaryexpertiseid = @headingId
and org.new_isdistribution = 1
and inc.new_controlname = @flavor
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0

select 
@exposures as exposures
,@requests as requests
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@requestsWithActualRevenue as requestsWithActualRevenue 
,@netActualRevenue as netActualRevenue
,@requestsWithNetActualRevenue as requestsWithNetActualRevenue
,@costOfInstallation as costOfInstallation
,@fullFullfilmentRevenue as fullFullfilmentRevenue
";
         */
        #endregion

        #region queryDistributionAndAllAndAll
        public const string queryDistributionAndAllAndAll = @"
declare @exposures int
declare @requests int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @requestsWithNetActualRevenue int
declare @costOfInstallation real
declare @fullFullfilmentRevenue decimal
declare @netActualRevenueUpsales decimal
declare @upsales int

select @costOfInstallation = isnull(new_costofinstallation, 0)
from new_origin with(nolock)
where new_originid = @originId

select @exposures = SUM(exp.Exposures)
from ConversionReportMongo exp with(nolock)
where dateadd(day, datediff(day, 0, exp.Date),0) = @date
and exp.OriginId = @originId

select @requests = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @upsales = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0
and inc.new_isupsale = 1

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and inc.new_originid = @originId) tbl1

select @potentailRevenue = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId

select @actualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId

select @netActualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithNetActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId

select @netActualRevenueUpsales = SUM(ia.new_potentialincidentprice), @requestsWithNetActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_isupsale = 1

select 
@exposures as exposures
,@requests as requests
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@requestsWithActualRevenue as requestsWithActualRevenue 
,@netActualRevenue as netActualRevenue
,@requestsWithNetActualRevenue as requestsWithNetActualRevenue
,@costOfInstallation as costOfInstallation
,@fullFullfilmentRevenue as fullFullfilmentRevenue
,@netActualRevenueUpsales as netActualRevenueUpsales
,@upsales as upsales
";
        #endregion

        #region queryDistributionAndHeadingAndAll
        /*
        public const string queryDistributionAndHeadingAndAll = @"
declare @exposures int
declare @requests int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @requestsWithNetActualRevenue int
declare @costOfInstallation real
declare @fullFullfilmentRevenue decimal

select @costOfInstallation = isnull(new_costofinstallation, 0)
from new_origin with(nolock)
where new_originid = @originId

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
where exp.createdon between @from and @to
and exp.new_originid = @originId
and isnull(exp.new_iswelcomescreenevent, 0) = 0
and exp.new_primaryexpertiseid = @headingId

select @requests = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId) tbl1

select @potentailRevenue = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId

select @actualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId

select @netActualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithNetActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and isnull(new_isstoppedmanually, 0) = 0

select 
@exposures as exposures
,@requests as requests
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@requestsWithActualRevenue as requestsWithActualRevenue 
,@netActualRevenue as netActualRevenue
,@requestsWithNetActualRevenue as requestsWithNetActualRevenue
,@costOfInstallation as costOfInstallation
,@fullFullfilmentRevenue as fullFullfilmentRevenue
";
        */
        #endregion

        #region queryDistributionAndAllAndFlavor
        /*
        public const string queryDistributionAndAllAndFlavor = @"
declare @exposures int
declare @requests int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @requestsWithNetActualRevenue int
declare @costOfInstallation real
declare @fullFullfilmentRevenue decimal

select @costOfInstallation = isnull(new_costofinstallation, 0)
from new_origin with(nolock)
where new_originid = @originId

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
where exp.createdon between @from and @to
and exp.new_originid = @originId
and isnull(exp.new_iswelcomescreenevent, 0) = 0
and exp.new_controlname = @flavor

select @requests = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor) tbl1

select @potentailRevenue = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor

select @actualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor

select @netActualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithNetActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor
and isnull(new_isstoppedmanually, 0) = 0

select 
@exposures as exposures
,@requests as requests
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@requestsWithActualRevenue as requestsWithActualRevenue 
,@netActualRevenue as netActualRevenue
,@requestsWithNetActualRevenue as requestsWithNetActualRevenue
,@costOfInstallation as costOfInstallation
,@fullFullfilmentRevenue as fullFullfilmentRevenue
";
        */
        #endregion

        #region queryDistributionAndHeadingAndFlavor
        /*
        public const string queryDistributionAndHeadingAndFlavor = @"
declare @exposures int
declare @requests int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @requestsWithNetActualRevenue int
declare @costOfInstallation real
declare @fullFullfilmentRevenue decimal

select @costOfInstallation = isnull(new_costofinstallation, 0)
from new_origin with(nolock)
where new_originid = @originId

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
where exp.createdon between @from and @to
and exp.new_originid = @originId
and isnull(exp.new_iswelcomescreenevent, 0) = 0
and exp.new_primaryexpertiseid = @headingId
and exp.new_controlname = @flavor

select @requests = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock) 
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor
and inc.new_primaryexpertiseid = @headingId) tbl1

select @potentailRevenue = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor

select @actualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor

select @netActualRevenue = SUM(ia.new_potentialincidentprice), @requestsWithNetActualRevenue = COUNT(distinct incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and isnull(new_isstoppedmanually, 0) = 0

select 
@exposures as exposures
,@requests as requests
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@requestsWithActualRevenue as requestsWithActualRevenue 
,@netActualRevenue as netActualRevenue
,@requestsWithNetActualRevenue as requestsWithNetActualRevenue
,@costOfInstallation as costOfInstallation
,@fullFullfilmentRevenue as fullFullfilmentRevenue
";
        */
        #endregion

        #endregion

        #region InjectionsQueries

        #region queryAllInjectionsAndAllAndAll
        public const string queryAllInjectionsAndAllAndAll =
            @"
declare @exposures int
declare @requests int
declare @upsales int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @netActualRevenueUpsales decimal
declare @requestsWithNetActualRevenue int
declare @potentialCost real
declare @actualCost real
declare @netActualCost real
declare @reqCountPotential real
declare @reqCountActual real
declare @reqCountNetActual real
declare @fullFullfilmentRevenue decimal
declare @fullFullfilmentCost real
declare @reqCountFullFullfilment real

declare @toPayId uniqueidentifier
select @toPayId = new_affiliatepaystatusid 
from new_affiliatepaystatus where new_istopay = 1

select @exposures = SUM(exp.Exposures)
from ConversionReportMongo exp with(nolock)
join new_origin org with(nolock) on org.New_originId = exp.OriginId
where dateadd(day, datediff(day, 0, exp.Date),0) = @date
and org.New_IsDistribution = 0
and org.New_IsInFinancialDashboard = 1

select @requests = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @upsales = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0
and new_isupsale = 1

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
,
@fullFullfilmentCost = sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * isnull(new_revenueshare, 0) / 100)
+
(
isnull(new_costperrequest, 0)
)
)
,
@reqCountFullFullfilment = count(*)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
,org.new_costperrequest
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isinfinancialdashboard = 1
and isnull(org.new_isdistribution, 0) = 0) tbl1

select @potentialCost = sum(cost), @potentailRevenue = sum(revenue), @reqCountPotential = sum(requestCount)
from 
(
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @actualCost = sum(cost), @actualRevenue = SUM(revenue), @reqCountActual = sum(requestCount)
from(
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @netActualCost = sum(cost) , @netActualRevenue = SUM(revenue), @reqCountNetActual = sum(requestCount)
from (
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @netActualRevenueUpsales = SUM(revenue)
from (
select 
 SUM(new_potentialincidentprice) as revenue
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

declare @potentialCost2 real
declare @actualCost2 real
declare @netActualCost2 real
declare @fullFullfilmentCost2 real
declare @netActualCost2upsales real

declare @costOfRequestsFromPerRequestOrigins real
select @costOfRequestsFromPerRequestOrigins = isnull( sum(cost), 0)
from 
(
select 
 (count(distinct inc.incidentid) * org.new_costperrequest) as cost
from Incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
and inc.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_costperrequest is not null
group by org.new_originid, org.new_costperrequest
) as tbl

declare @costOfRequestsFromPerRequestOriginsUpsales real
select @costOfRequestsFromPerRequestOriginsUpsales = isnull( sum(cost), 0)
from 
(
select 
 (count(distinct inc.incidentid) * org.new_costperrequest) as cost
from Incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
and inc.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_costperrequest is not null
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest
) as tbl

select @potentialCost2 = isnull( sum(cost), 0)
from 
(
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_revenueshare is not null
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @potentialCost2 = @potentialCost2 + @costOfRequestsFromPerRequestOrigins;

select @actualCost2 = isnull( sum(cost), 0)
from(
select
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_revenueshare is not null
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @actualCost2 = @actualCost2 + @costOfRequestsFromPerRequestOrigins;

select @netActualCost2 = isnull( sum(cost), 0)
from (
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and org.new_revenueshare is not null
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @netActualCost2 = @netActualCost2 + @costOfRequestsFromPerRequestOrigins;

select @netActualCost2upsales = isnull( sum(cost), 0)
from (
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and org.new_revenueshare is not null
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @netActualCost2upsales = @netActualCost2upsales + @costOfRequestsFromPerRequestOriginsUpsales;

select @fullFullfilmentCost2 = isnull( sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * new_revenueshare / 100)
) , 0)
 from
(select new_primaryexpertiseid as headingid,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isinfinancialdashboard = 1
and isnull(org.new_isdistribution, 0) = 0
and org.new_revenueshare is not null) tbl1

set @fullFullfilmentCost2 = @fullFullfilmentCost2 + @costOfRequestsFromPerRequestOrigins;

select 
@exposures as exposures
,@requests as requests
,@upsales as upsales
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@netActualRevenue as netActualRevenue
,@netActualRevenueUpsales as netActualRevenueUpsales
,@potentialCost as potentialCost
,@actualCost as actualCost
,@netActualCost as netActualCost
,@reqCountPotential as requestCountPotential
,@reqCountActual as requestCountActual
,@reqCountNetActual as requestCountNetActual
,@fullFullfilmentRevenue as fullFullfilmentRevenue
,@fullFullfilmentCost as fullFullfilmentCost
,@reqCountFullFullfilment as reqCountFullFullfilment
,@potentialCost2 as potentialCost2
,@actualCost2 as actualCost2
,@netActualCost2 as netActualCost2
,@fullFullfilmentCost2 as fullFullfilmentCost2
,@netActualCost2upsales as netActualCost2upsales
";
        #endregion

        #region queryAllInjectionsAndAllAndFlavor
        /*
        public const string queryAllInjectionsAndAllAndFlavor =
            @"
declare @exposures int
declare @requests int
declare @upsales int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @netActualRevenueUpsales decimal
declare @requestsWithNetActualRevenue int
declare @potentialCost real
declare @actualCost real
declare @netActualCost real
declare @reqCountPotential real
declare @reqCountActual real
declare @reqCountNetActual real
declare @fullFullfilmentRevenue decimal
declare @fullFullfilmentCost real
declare @reqCountFullFullfilment real

declare @toPayId uniqueidentifier
select @toPayId = new_affiliatepaystatusid 
from new_affiliatepaystatus where new_istopay = 1

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
join new_origin org with(nolock) on exp.new_originid = org.New_originId
where exp.createdon between @from and @to
and (exp.new_originid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or exp.new_originid is null)
                and (org.new_parentoriginid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or org.new_parentoriginid is null)
                and isnull(exp.new_iswelcomescreenevent, 0) = 0
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and exp.new_controlname = @flavor

select @requests = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_controlname = @flavor
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @upsales = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_controlname = @flavor
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0
and new_isupsale = 1

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
,
@fullFullfilmentCost = sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * isnull(new_revenueshare, 0) / 100)
+
(
isnull(new_costperrequest, 0)
)
)
,
@reqCountFullFullfilment = count(*)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
,org.new_costperrequest
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isinfinancialdashboard = 1
and isnull(org.new_isdistribution, 0) = 0
and inc.new_controlname = @flavor) tbl1

select @potentialCost = sum(cost), @potentailRevenue = sum(revenue), @reqCountPotential = sum(requestCount)
from 
(
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @actualCost = sum(cost), @actualRevenue = SUM(revenue), @reqCountActual = sum(requestCount)
from(
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @netActualCost = sum(cost) , @netActualRevenue = SUM(revenue), @reqCountNetActual = sum(requestCount)
from (
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_controlname = @flavor
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @netActualRevenueUpsales = SUM(revenue)
from (
select 
 SUM(new_potentialincidentprice) as revenue
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_controlname = @flavor
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

declare @potentialCost2 real
declare @actualCost2 real
declare @netActualCost2 real
declare @fullFullfilmentCost2 real
declare @netActualCost2upsales real

declare @costOfRequestsFromPerRequestOrigins real
select @costOfRequestsFromPerRequestOrigins = isnull( sum(cost), 0)
from 
(
select 
 (count(distinct inc.incidentid) * org.new_costperrequest) as cost
from Incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
and inc.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_costperrequest is not null
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest
) as tbl

declare @costOfRequestsFromPerRequestOriginsUpsales real
select @costOfRequestsFromPerRequestOriginsUpsales = isnull( sum(cost), 0)
from 
(
select 
 (count(distinct inc.incidentid) * org.new_costperrequest) as cost
from Incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
and inc.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_costperrequest is not null
and inc.new_controlname = @flavor
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest
) as tbl

select @potentialCost2 = isnull( sum(cost), 0)
from 
(
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_revenueshare is not null
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @potentialCost2 = @potentialCost2 + @costOfRequestsFromPerRequestOrigins;

select @actualCost2 = isnull( sum(cost), 0)
from(
select
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_revenueshare is not null
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @actualCost2 = @actualCost2 + @costOfRequestsFromPerRequestOrigins;

select @netActualCost2 = isnull( sum(cost), 0)
from (
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and org.new_revenueshare is not null
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @netActualCost2 = @netActualCost2 + @costOfRequestsFromPerRequestOrigins;

select @netActualCost2upsales = isnull( sum(cost), 0)
from (
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and org.new_revenueshare is not null
and inc.new_controlname = @flavor
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @netActualCost2upsales = @netActualCost2upsales + @costOfRequestsFromPerRequestOriginsUpsales;

select @fullFullfilmentCost2 = isnull( sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * new_revenueshare / 100)
) , 0)
 from
(select new_primaryexpertiseid as headingid,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isinfinancialdashboard = 1
and isnull(org.new_isdistribution, 0) = 0
and org.new_revenueshare is not null
and inc.new_controlname = @flavor) tbl1

set @fullFullfilmentCost2 = @fullFullfilmentCost2 + @costOfRequestsFromPerRequestOrigins;

select 
@exposures as exposures
,@requests as requests
,@upsales as upsales
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@netActualRevenue as netActualRevenue
,@netActualRevenueUpsales as netActualRevenueUpsales
,@potentialCost as potentialCost
,@actualCost as actualCost
,@netActualCost as netActualCost
,@reqCountPotential as requestCountPotential
,@reqCountActual as requestCountActual
,@reqCountNetActual as requestCountNetActual
,@fullFullfilmentRevenue as fullFullfilmentRevenue
,@fullFullfilmentCost as fullFullfilmentCost
,@reqCountFullFullfilment as reqCountFullFullfilment
,@potentialCost2 as potentialCost2
,@actualCost2 as actualCost2
,@netActualCost2 as netActualCost2
,@fullFullfilmentCost2 as fullFullfilmentCost2
,@netActualCost2upsales as netActualCost2upsales
";
        */
        #endregion

        #region queryAllInjectionsAndHeadingAndAll
        /*
        public const string queryAllInjectionsAndHeadingAndAll =
            @"
declare @exposures int
declare @requests int
declare @upsales int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @netActualRevenueUpsales decimal
declare @requestsWithNetActualRevenue int
declare @potentialCost real
declare @actualCost real
declare @netActualCost real
declare @reqCountPotential real
declare @reqCountActual real
declare @reqCountNetActual real
declare @fullFullfilmentRevenue decimal
declare @fullFullfilmentCost real
declare @reqCountFullFullfilment real

declare @toPayId uniqueidentifier
select @toPayId = new_affiliatepaystatusid 
from new_affiliatepaystatus where new_istopay = 1

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
join new_origin org with(nolock) on exp.new_originid = org.New_originId
where exp.createdon between @from and @to
and (exp.new_originid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or exp.new_originid is null)
                and (org.new_parentoriginid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or org.new_parentoriginid is null)
                and isnull(exp.new_iswelcomescreenevent, 0) = 0
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and exp.new_primaryexpertiseid = @headingId

select @requests = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @upsales = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0
and new_isupsale = 1

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
,
@fullFullfilmentCost = sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * isnull(new_revenueshare, 0) / 100)
+
(
isnull(new_costperrequest, 0)
)
)
,
@reqCountFullFullfilment = count(*)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
,org.new_costperrequest
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isinfinancialdashboard = 1
and isnull(org.new_isdistribution, 0) = 0
and inc.new_primaryexpertiseid = @headingId) tbl1

select @potentialCost = sum(cost), @potentailRevenue = sum(revenue), @reqCountPotential = sum(requestCount)
from 
(
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @actualCost = sum(cost), @actualRevenue = SUM(revenue), @reqCountActual = sum(requestCount)
from(
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @netActualCost = sum(cost) , @netActualRevenue = SUM(revenue), @reqCountNetActual = sum(requestCount)
from (
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @netActualRevenueUpsales = SUM(revenue)
from (
select 
SUM(new_potentialincidentprice) as revenue
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

declare @potentialCost2 real
declare @actualCost2 real
declare @netActualCost2 real
declare @fullFullfilmentCost2 real
declare @netActualCost2upsales real

declare @costOfRequestsFromPerRequestOrigins real
select @costOfRequestsFromPerRequestOrigins = isnull( sum(cost), 0)
from 
(
select 
 (count(distinct inc.incidentid) * org.new_costperrequest) as cost
from Incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
and inc.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_costperrequest is not null
and inc.new_primaryexpertiseid = @headingId
group by org.new_originid, org.new_costperrequest
) as tbl

declare @costOfRequestsFromPerRequestOriginsUpsales real
select @costOfRequestsFromPerRequestOriginsUpsales = isnull( sum(cost), 0)
from 
(
select 
 (count(distinct inc.incidentid) * org.new_costperrequest) as cost
from Incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
and inc.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_costperrequest is not null
and inc.new_primaryexpertiseid = @headingId
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest
) as tbl

select @potentialCost2 = isnull( sum(cost), 0)
from 
(
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_revenueshare is not null
and inc.new_primaryexpertiseid = @headingId
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @potentialCost2 = @potentialCost2 + @costOfRequestsFromPerRequestOrigins;

select @actualCost2 = isnull( sum(cost), 0)
from(
select
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_revenueshare is not null
and inc.new_primaryexpertiseid = @headingId
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @actualCost2 = @actualCost2 + @costOfRequestsFromPerRequestOrigins;

select @netActualCost2 = isnull( sum(cost), 0)
from (
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and org.new_revenueshare is not null
and inc.new_primaryexpertiseid = @headingId
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @netActualCost2 = @netActualCost2 + @costOfRequestsFromPerRequestOrigins;

select @netActualCost2upsales = isnull( sum(cost), 0)
from (
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and org.new_revenueshare is not null
and inc.new_primaryexpertiseid = @headingId
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @netActualCost2upsales = @netActualCost2upsales + @costOfRequestsFromPerRequestOriginsUpsales;

select @fullFullfilmentCost2 = isnull( sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * new_revenueshare / 100)
) , 0)
 from
(select new_primaryexpertiseid as headingid,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isinfinancialdashboard = 1
and isnull(org.new_isdistribution, 0) = 0
and inc.new_primaryexpertiseid = @headingId
and org.new_revenueshare is not null) tbl1

set @fullFullfilmentCost2 = @fullFullfilmentCost2 + @costOfRequestsFromPerRequestOrigins;

select 
@exposures as exposures
,@requests as requests
,@upsales as upsales
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@netActualRevenue as netActualRevenue
,@netActualRevenueUpsales as netActualRevenueUpsales
,@potentialCost as potentialCost
,@actualCost as actualCost
,@netActualCost as netActualCost
,@reqCountPotential as requestCountPotential
,@reqCountActual as requestCountActual
,@reqCountNetActual as requestCountNetActual
,@fullFullfilmentRevenue as fullFullfilmentRevenue
,@fullFullfilmentCost as fullFullfilmentCost
,@reqCountFullFullfilment as reqCountFullFullfilment
,@potentialCost2 as potentialCost2
,@actualCost2 as actualCost2
,@netActualCost2 as netActualCost2
,@fullFullfilmentCost2 as fullFullfilmentCost2
,@netActualCost2upsales as netActualCost2upsales
";
        */
        #endregion

        #region queryAllInjectionsAndHeadingAndFlavor
        /*
        public const string queryAllInjectionsAndHeadingAndFlavor =
            @"
declare @exposures int
declare @requests int
declare @upsales int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @netActualRevenueUpsales decimal
declare @requestsWithNetActualRevenue int
declare @potentialCost real
declare @actualCost real
declare @netActualCost real
declare @reqCountPotential real
declare @reqCountActual real
declare @reqCountNetActual real
declare @fullFullfilmentRevenue decimal
declare @fullFullfilmentCost real
declare @reqCountFullFullfilment real

declare @toPayId uniqueidentifier
select @toPayId = new_affiliatepaystatusid 
from new_affiliatepaystatus where new_istopay = 1

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
join new_origin org with(nolock) on exp.new_originid = org.New_originId
where exp.createdon between @from and @to
and (exp.new_originid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or exp.new_originid is null)
                and (org.new_parentoriginid not in(
                    '62F83DD9-7588-E111-9538-001517D1792A', 
                    '7E70F76A-9F71-E111-BE62-001517D10F6E'
                ) or org.new_parentoriginid is null)
                and isnull(exp.new_iswelcomescreenevent, 0) = 0
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and exp.new_primaryexpertiseid = @headingId
and exp.new_controlname = @flavor

select @requests = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @upsales = count(incidentid)
from incident inc with(nolock)
join new_origin org with(nolock) on org.new_originid = inc.new_originid
where inc.createdon between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0
and new_isupsale = 1

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
,
@fullFullfilmentCost = sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * isnull(new_revenueshare, 0) / 100)
+
(
isnull(new_costperrequest, 0)
)
)
,
@reqCountFullFullfilment = count(*)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
,org.new_costperrequest
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isinfinancialdashboard = 1
and isnull(org.new_isdistribution, 0) = 0
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor) tbl1

select @potentialCost = sum(cost), @potentailRevenue = sum(revenue), @reqCountPotential = sum(requestCount)
from 
(
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @actualCost = sum(cost), @actualRevenue = SUM(revenue), @reqCountActual = sum(requestCount)
from(
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @netActualCost = sum(cost) , @netActualRevenue = SUM(revenue), @reqCountNetActual = sum(requestCount)
from (
select 
 (count(distinct inc.incidentid) * isnull(org.new_costperrequest, 0))
 + 
(SUM(new_potentialincidentprice) * isnull(org.new_revenueshare, 0) / 100) as cost
, SUM(new_potentialincidentprice) as revenue
, count(distinct inc.incidentid) as requestCount
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

select @netActualRevenueUpsales = SUM(revenue)
from (
select 
SUM(new_potentialincidentprice) as revenue
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl



declare @potentialCost2 real
declare @actualCost2 real
declare @netActualCost2 real
declare @fullFullfilmentCost2 real
declare @netActualCost2upsales real

declare @costOfRequestsFromPerRequestOrigins real
select @costOfRequestsFromPerRequestOrigins = isnull( sum(cost), 0)
from 
(
select 
 (count(distinct inc.incidentid) * org.new_costperrequest) as cost
from Incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
and inc.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_costperrequest is not null
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest
) as tbl

declare @costOfRequestsFromPerRequestOriginsUpsales real
select @costOfRequestsFromPerRequestOriginsUpsales = isnull( sum(cost), 0)
from 
(
select 
 (count(distinct inc.incidentid) * org.new_costperrequest) as cost
from Incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
and inc.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_costperrequest is not null
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest
) as tbl

select @potentialCost2 = isnull( sum(cost), 0)
from 
(
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_revenueshare is not null
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @potentialCost2 = @potentialCost2 + @costOfRequestsFromPerRequestOrigins;

select @actualCost2 = isnull( sum(cost), 0)
from(
select
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and org.new_revenueshare is not null
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @actualCost2 = @actualCost2 + @costOfRequestsFromPerRequestOrigins;

select @netActualCost2 = isnull( sum(cost), 0)
from (
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and org.new_revenueshare is not null
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @netActualCost2 = @netActualCost2 + @costOfRequestsFromPerRequestOrigins;

select @netActualCost2upsales = isnull( sum(cost), 0)
from (
select 
(SUM(new_potentialincidentprice) * org.new_revenueshare / 100) as cost
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
--and isnull(org.new_isdistribution, 0) = 0
and org.new_isinfinancialdashboard = 1
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and org.new_revenueshare is not null
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and new_isupsale = 1
group by org.new_originid, org.new_costperrequest, org.new_revenueshare
) as tbl

set @netActualCost2upsales = @netActualCost2upsales + @costOfRequestsFromPerRequestOriginsUpsales;

select @fullFullfilmentCost2 = isnull( sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * new_revenueshare / 100)
) , 0)
 from
(select new_primaryexpertiseid as headingid,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and org.new_isinfinancialdashboard = 1
and isnull(org.new_isdistribution, 0) = 0
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and org.new_revenueshare is not null) tbl1

set @fullFullfilmentCost2 = @fullFullfilmentCost2 + @costOfRequestsFromPerRequestOrigins;

select 
@exposures as exposures
,@requests as requests
,@upsales as upsales
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@netActualRevenue as netActualRevenue
,@netActualRevenueUpsales as netActualRevenueUpsales
,@potentialCost as potentialCost
,@actualCost as actualCost
,@netActualCost as netActualCost
,@reqCountPotential as requestCountPotential
,@reqCountActual as requestCountActual
,@reqCountNetActual as requestCountNetActual
,@fullFullfilmentRevenue as fullFullfilmentRevenue
,@fullFullfilmentCost as fullFullfilmentCost
,@reqCountFullFullfilment as reqCountFullFullfilment
,@potentialCost2 as potentialCost2
,@actualCost2 as actualCost2
,@netActualCost2 as netActualCost2
,@fullFullfilmentCost2 as fullFullfilmentCost2
,@netActualCost2upsales as netActualCost2upsales
";
        */
        #endregion

        #region queryInjectionAndAllAndAll
        public const string queryInjectionAndAllAndAll = @"
declare @exposures int
declare @requests int
declare @upsales int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @netActualRevenueUpsales decimal
declare @requestsWithNetActualRevenue int
declare @potentialCost real
declare @actualCost real
declare @netActualCost real
declare @reqCountPotential real
declare @reqCountActual real
declare @reqCountNetActual real
declare @fullFullfilmentRevenue decimal
declare @fullFullfilmentCost real
declare @reqCountFullFullfilment real

declare @toPayId uniqueidentifier
select @toPayId = new_affiliatepaystatusid 
from new_affiliatepaystatus where new_istopay = 1

select @exposures = SUM(exp.Exposures)
from ConversionReportMongo exp with(nolock)
where dateadd(day, datediff(day, 0, exp.Date),0) = @date
and exp.OriginId = @originId

select @requests = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @upsales = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0
and new_isupsale = 1

declare @costPerRequest real
declare @revenueShare real

select @costPerRequest = isnull(new_costperrequest, 0),
@revenueShare = isnull(new_revenueshare, 0)
from new_origin with(nolock)
where new_originid = @originId

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
,
@fullFullfilmentCost = sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * isnull(new_revenueshare, 0) / 100)
+
(
isnull(new_costperrequest, 0)
)
)
,
@reqCountFullFullfilment = count(*)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
,org.new_costperrequest
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and inc.new_originid = @originId) tbl1

select 
 @potentialCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
,@potentailRevenue = SUM(new_potentialincidentprice)
,@reqCountPotential = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId

select 
@actualCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
, @actualRevenue = SUM(new_potentialincidentprice)
,@reqCountActual = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId

select 
 @netActualCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
, @netActualRevenue = SUM(new_potentialincidentprice)
,@reqCountNetActual = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId

declare @netActualCostUpsales real

select 
@netActualCostUpsales = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
,@netActualRevenueUpsales = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and new_isupsale = 1

declare @potentialCost2 real
declare @actualCost2 real
declare @netActualCost2 real
declare @fullFullfilmentCost2 real
declare @netActualCost2upsales real

if @costPerRequest > 0
begin
set @potentialCost2 = @requests * @costPerRequest
set @actualCost2 = @potentialCost2
set @netActualCost2 = @potentialCost2
set @fullFullfilmentCost2 = @potentialCost2
set @netActualCost2upsales = @upsales * @costPerRequest
end
else
begin
set @potentialCost2 = @potentialCost
set @actualCost2 = @actualCost
set @netActualCost2 = @netActualCost
set @fullFullfilmentCost2 = @fullFullfilmentCost
set @netActualCost2upsales = @netActualCostUpsales
end


select 
@exposures as exposures
,@requests as requests
,@upsales as upsales
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@netActualRevenue as netActualRevenue
,@netActualRevenueUpsales as netActualRevenueUpsales
,@potentialCost as potentialCost
,@actualCost as actualCost
,@netActualCost as netActualCost
,@reqCountPotential as requestCountPotential
,@reqCountActual as requestCountActual
,@reqCountNetActual as requestCountNetActual
,@fullFullfilmentRevenue as fullFullfilmentRevenue
,@fullFullfilmentCost as fullFullfilmentCost
,@reqCountFullFullfilment as reqCountFullFullfilment
,@potentialCost2 as potentialCost2
,@actualCost2 as actualCost2
,@netActualCost2 as netActualCost2
,@fullFullfilmentCost2 as fullFullfilmentCost2
,@netActualCost2upsales as netActualCost2upsales
";
        #endregion

        #region queryInjectionAndHeadingAndAll
        /*
        public const string queryInjectionAndHeadingAndAll = @"
declare @exposures int
declare @requests int
declare @upsales int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @netActualRevenueUpsales decimal
declare @requestsWithNetActualRevenue int
declare @potentialCost real
declare @actualCost real
declare @netActualCost real
declare @reqCountPotential real
declare @reqCountActual real
declare @reqCountNetActual real
declare @fullFullfilmentRevenue decimal
declare @fullFullfilmentCost real
declare @reqCountFullFullfilment real

declare @toPayId uniqueidentifier
select @toPayId = new_affiliatepaystatusid 
from new_affiliatepaystatus where new_istopay = 1

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
where exp.createdon between @from and @to
and exp.new_originid = @originId
and isnull(exp.new_iswelcomescreenevent, 0) = 0
and exp.new_primaryexpertiseid = @headingId

select @requests = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @upsales = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0
and new_isupsale = 1

declare @costPerRequest real
declare @revenueShare real

select @costPerRequest = isnull(new_costperrequest, 0),
@revenueShare = isnull(new_revenueshare, 0)
from new_origin with(nolock)
where new_originid = @originId

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
,
@fullFullfilmentCost = sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * isnull(new_revenueshare, 0) / 100)
+
(
isnull(new_costperrequest, 0)
)
)
,
@reqCountFullFullfilment = count(*)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
,org.new_costperrequest
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId) tbl1

select 
 @potentialCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
,@potentailRevenue = SUM(new_potentialincidentprice)
,@reqCountPotential = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId

select 
@actualCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
, @actualRevenue = SUM(new_potentialincidentprice)
,@reqCountActual = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId

select 
 @netActualCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
, @netActualRevenue = SUM(new_potentialincidentprice)
,@reqCountNetActual = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId

declare @netActualCostUpsales real

select 
@netActualCostUpsales = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
, @netActualRevenueUpsales = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and new_isupsale = 1

declare @potentialCost2 real
declare @actualCost2 real
declare @netActualCost2 real
declare @fullFullfilmentCost2 real
declare @netActualCost2upsales real

if @costPerRequest > 0
begin
set @potentialCost2 = @requests * @costPerRequest
set @actualCost2 = @potentialCost2
set @netActualCost2 = @potentialCost2
set @fullFullfilmentCost2 = @potentialCost2
set @netActualCost2upsales = @upsales * @costPerRequest
end
else
begin
set @potentialCost2 = @potentialCost
set @actualCost2 = @actualCost
set @netActualCost2 = @netActualCost
set @fullFullfilmentCost2 = @fullFullfilmentCost
set @netActualCost2upsales = @netActualCostUpsales
end


select 
@exposures as exposures
,@requests as requests
,@upsales as upsales
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@netActualRevenue as netActualRevenue
,@netActualRevenueUpsales as netActualRevenueUpsales
,@potentialCost as potentialCost
,@actualCost as actualCost
,@netActualCost as netActualCost
,@reqCountPotential as requestCountPotential
,@reqCountActual as requestCountActual
,@reqCountNetActual as requestCountNetActual
,@fullFullfilmentRevenue as fullFullfilmentRevenue
,@fullFullfilmentCost as fullFullfilmentCost
,@reqCountFullFullfilment as reqCountFullFullfilment
,@potentialCost2 as potentialCost2
,@actualCost2 as actualCost2
,@netActualCost2 as netActualCost2
,@fullFullfilmentCost2 as fullFullfilmentCost2
,@netActualCost2upsales as netActualCost2upsales
";
        */
        #endregion

        #region queryInjectionAndAllAndFlavor
        /*
        public const string queryInjectionAndAllAndFlavor = @"
declare @exposures int
declare @requests int
declare @upsales int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @netActualRevenueUpsales decimal
declare @requestsWithNetActualRevenue int
declare @potentialCost real
declare @actualCost real
declare @netActualCost real
declare @reqCountPotential real
declare @reqCountActual real
declare @reqCountNetActual real
declare @fullFullfilmentRevenue decimal
declare @fullFullfilmentCost real
declare @reqCountFullFullfilment real

declare @toPayId uniqueidentifier
select @toPayId = new_affiliatepaystatusid 
from new_affiliatepaystatus where new_istopay = 1

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
where exp.createdon between @from and @to
and exp.new_originid = @originId
and isnull(exp.new_iswelcomescreenevent, 0) = 0
and exp.new_controlname = @flavor

select @requests = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @upsales = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0
and new_isupsale = 1

declare @costPerRequest real
declare @revenueShare real

select @costPerRequest = isnull(new_costperrequest, 0),
@revenueShare = isnull(new_revenueshare, 0)
from new_origin with(nolock)
where new_originid = @originId

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
,
@fullFullfilmentCost = sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * isnull(new_revenueshare, 0) / 100)
+
(
isnull(new_costperrequest, 0)
)
)
,
@reqCountFullFullfilment = count(*)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
,org.new_costperrequest
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor) tbl1

select 
 @potentialCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
,@potentailRevenue = SUM(new_potentialincidentprice)
,@reqCountPotential = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor

select 
@actualCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
, @actualRevenue = SUM(new_potentialincidentprice)
,@reqCountActual = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor

select 
 @netActualCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
, @netActualRevenue = SUM(new_potentialincidentprice)
,@reqCountNetActual = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId

declare @netActualCostUpsales real

select @netActualCostUpsales = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
,@netActualRevenueUpsales = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and new_isupsale = 1

declare @potentialCost2 real
declare @actualCost2 real
declare @netActualCost2 real
declare @fullFullfilmentCost2 real
declare @netActualCost2upsales real

if @costPerRequest > 0
begin
set @potentialCost2 = @requests * @costPerRequest
set @actualCost2 = @potentialCost2
set @netActualCost2 = @potentialCost2
set @fullFullfilmentCost2 = @potentialCost2
set @netActualCost2upsales = @upsales * @costPerRequest
end
else
begin
set @potentialCost2 = @potentialCost
set @actualCost2 = @actualCost
set @netActualCost2 = @netActualCost
set @fullFullfilmentCost2 = @fullFullfilmentCost
set @netActualCost2upsales = @netActualCostUpsales
end

select 
@exposures as exposures
,@requests as requests
,@upsales as upsales
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@netActualRevenue as netActualRevenue
,@netActualRevenueUpsales as netActualRevenueUpsales
,@potentialCost as potentialCost
,@actualCost as actualCost
,@netActualCost as netActualCost
,@reqCountPotential as requestCountPotential
,@reqCountActual as requestCountActual
,@reqCountNetActual as requestCountNetActual
,@fullFullfilmentRevenue as fullFullfilmentRevenue
,@fullFullfilmentCost as fullFullfilmentCost
,@reqCountFullFullfilment as reqCountFullFullfilment
,@potentialCost2 as potentialCost2
,@actualCost2 as actualCost2
,@netActualCost2 as netActualCost2
,@fullFullfilmentCost2 as fullFullfilmentCost2
,@netActualCost2upsales as netActualCost2upsales
";
        */
        #endregion

        #region queryInjectionAndHeadingAndFlavor
        /*
        public const string queryInjectionAndHeadingAndFlavor = @"
declare @exposures int
declare @requests int
declare @upsales int
declare @potentailRevenue decimal
declare @actualRevenue decimal
declare @requestsWithActualRevenue int
declare @netActualRevenue decimal
declare @netActualRevenueUpsales decimal
declare @requestsWithNetActualRevenue int
declare @potentialCost real
declare @actualCost real
declare @netActualCost real
declare @reqCountPotential real
declare @reqCountActual real
declare @reqCountNetActual real
declare @fullFullfilmentRevenue decimal
declare @fullFullfilmentCost real
declare @reqCountFullFullfilment real

declare @toPayId uniqueidentifier
select @toPayId = new_affiliatepaystatusid 
from new_affiliatepaystatus where new_istopay = 1

select @exposures = count(new_exposureid)
from new_exposure exp with(nolock)
where exp.createdon between @from and @to
and exp.new_originid = @originId
and isnull(exp.new_iswelcomescreenevent, 0) = 0
and exp.new_primaryexpertiseid = @headingId
and exp.new_controlname = @flavor

select @requests = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0

select @upsales = count(incidentid)
from incident inc with(nolock)
where inc.createdon between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and inc.statuscode != 200013 and inc.statuscode != 200014
and isnull(inc.new_isstoppedmanually, 0) = 0
and new_isupsale = 1

declare @costPerRequest real
declare @revenueShare real

select @costPerRequest = isnull(new_costperrequest, 0),
@revenueShare = isnull(new_revenueshare, 0)
from new_origin with(nolock)
where new_originid = @originId

select @fullFullfilmentRevenue = sum(
case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end)
,
@fullFullfilmentCost = sum(
((case counter
when 0 then
(
3 * dbo.getAveragePpc(headingid)
)
else (case counter 
when 1
then 
(3 * priceSum)
else
(case counter 
when 2
then
3 * (priceSum / 2)
else
priceSum
end)end)end) * isnull(new_revenueshare, 0) / 100)
+
(
isnull(new_costperrequest, 0)
)
)
,
@reqCountFullFullfilment = count(*)
 from
(select new_primaryexpertiseid as headingid
,
(
select count(*) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as counter
,
(select sum(new_potentialincidentprice) from new_incidentaccount with(nolock)
where new_incidentid = incidentid
and statuscode in (10, 12, 13)
) as priceSum
,org.new_revenueshare
,org.new_costperrequest
from incident inc with(nolock)
join new_origin org with(nolock) on inc.new_originid = org.new_originid
where
inc.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_controlname = @flavor
and inc.new_primaryexpertiseid = @headingId) tbl1

select 
 @potentialCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
,@potentailRevenue = SUM(new_potentialincidentprice)
,@reqCountPotential = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor

select 
@actualCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
, @actualRevenue = SUM(new_potentialincidentprice)
,@reqCountActual = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor

select 
 @netActualCost = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100)
, @netActualRevenue = SUM(new_potentialincidentprice)
,@reqCountNetActual = count(distinct inc.incidentid)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId

declare @netActualCostUpsales real

select 
@netActualCostUpsales = (count(distinct inc.incidentid) * @costPerRequest) + (SUM(new_potentialincidentprice) * @revenueShare / 100),
@netActualRevenueUpsales = SUM(new_potentialincidentprice)
from New_incidentaccount ia with(nolock)
join Incident inc with(nolock) on ia.new_incidentid = inc.IncidentId
where 
ia.new_accountid in ({0})
and ia.statuscode in (10, 12, 13)
and ia.New_tocharge = 1
and ia.CreatedOn between @from and @to
and inc.new_originid = @originId
and inc.new_primaryexpertiseid = @headingId
and inc.new_controlname = @flavor
and isnull(new_isstoppedmanually, 0) = 0
and new_affiliatepaystatusid = @toPayId
and inc.new_isupsale = 1

declare @potentialCost2 real
declare @actualCost2 real
declare @netActualCost2 real
declare @fullFullfilmentCost2 real
declare @netActualCost2upsales real

if @costPerRequest > 0
begin
set @potentialCost2 = @requests * @costPerRequest
set @actualCost2 = @potentialCost2
set @netActualCost2 = @potentialCost2
set @fullFullfilmentCost2 = @potentialCost2
set @netActualCost2upsales = @upsales * @costPerRequest
end
else
begin
set @potentialCost2 = @potentialCost
set @actualCost2 = @actualCost
set @netActualCost2 = @netActualCost
set @fullFullfilmentCost2 = @fullFullfilmentCost
set @netActualCost2upsales = @netActualCostUpsales
end

select 
@exposures as exposures
,@requests as requests
,@upsales as upsales
,@potentailRevenue as potentailRevenue 
,@actualRevenue as actualRevenue
,@netActualRevenue as netActualRevenue
,@netActualRevenueUpsales as netActualRevenueUpsales
,@potentialCost as potentialCost
,@actualCost as actualCost
,@netActualCost as netActualCost
,@reqCountPotential as requestCountPotential
,@reqCountActual as requestCountActual
,@reqCountNetActual as requestCountNetActual
,@fullFullfilmentRevenue as fullFullfilmentRevenue
,@fullFullfilmentCost as fullFullfilmentCost
,@reqCountFullFullfilment as reqCountFullFullfilment
,@potentialCost2 as potentialCost2
,@actualCost2 as actualCost2
,@netActualCost2 as netActualCost2
,@fullFullfilmentCost2 as fullFullfilmentCost2
,@netActualCost2upsales as netActualCost2upsales
";
         */
        #endregion

        #endregion

        public const string updateAveragePpcTable =
            @"
DELETE FROM tbl_averageppc

insert into tbl_averageppc (average, headingid, createdon)
select avg(isnull(new_potentialincidentprice, 0)), new_primaryexpertiseid, getdate()
			from new_incidentaccount ia
			join incident on ia.new_incidentid = incidentid
			where ia.statuscode in (10, 12, 13) and new_primaryexpertiseid is not null
			group by new_primaryexpertiseid
";
    }
}
