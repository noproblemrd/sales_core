﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Dashboard.FinancialDashboard;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.Dashboard.FinancialDashboard
{
    public class ReportFiltersCreator : XrmUserBase
    {
        private const string cachePrefix = "ReportFiltersCreator";
        private const string cacheKey = "GetFilters";
        private const string ORIGINID = "new_originid";
        private const string NAME = "new_name";
        private const string DAUS = "daus";

        public ReportFiltersCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public FinancialDashboardFiltersContainer GetFilters()
        {
            return DataAccessLayer.CacheManager.Instance.Get<FinancialDashboardFiltersContainer>(cachePrefix, cacheKey, GetFiltersMethod);
        }

        private FinancialDashboardFiltersContainer GetFiltersMethod()
        {
            FinancialDashboardFiltersContainer retVal = new FinancialDashboardFiltersContainer();
            DataAccessLayer.FinancialDashboardDailyData dal = new NoProblem.Core.DataAccessLayer.FinancialDashboardDailyData(XrmDataContext);
            using (SqlConnection connection = dal.GetSqlConnectionString())
            {
                retVal.Flavors = RetrieveFlavors(connection, dal);
                retVal.AddOnOrigins = RetrieveOrigins(connection, dal, true);
                retVal.InjectionOrigins = RetrieveOrigins(connection, dal, false);
            }
            return retVal;
        }

        private List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair> RetrieveOrigins(SqlConnection connection, DataAccessLayer.FinancialDashboardDailyData dal, bool isDisbritutions)
        {
            List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair> retVal = new List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>();
            string query;
            if (isDisbritutions)
            {
                query = getDistributionOrigins;
            }
            else
            {
                query = getInjectionOrigins;
            }
            var reader = dal.ExecuteReader(query, connection);
            foreach (var row in reader)
            {
                retVal.Add(
                    new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair()
                    {
                        Id = (Guid)row[ORIGINID],
                        Name = (string)row[NAME]
                    });
            }
            return retVal;
        }

        public List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs> GetDistributionOriginsWithDaus()
        {
            DataAccessLayer.Generic dal = new NoProblem.Core.DataAccessLayer.Generic();
            List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs> retVal = new List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs>();
            var reader = dal.ExecuteReader(getDistributionOriginsAndDaus);
            foreach (var row in reader)
            {
                retVal.Add(
                    new DataModel.Origins.InjectionsCockpit.OriginWithDAUs()
                    {
                        OriginId = (Guid)row[ORIGINID],
                        OriginName = (string)row[NAME],
                        DAUs = (int)row[DAUS]
                    });
            }
            return retVal;
        }
        public List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs> GetDistributionOrigins()
        {
            DataAccessLayer.Generic dal = new NoProblem.Core.DataAccessLayer.Generic();
            List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs> retVal = new List<DataModel.Origins.InjectionsCockpit.OriginWithDAUs>();
            var reader = dal.ExecuteReader(_getDistributionOrigins);
            foreach (var row in reader)
            {
                retVal.Add(
                    new DataModel.Origins.InjectionsCockpit.OriginWithDAUs()
                    {
                        OriginId = (Guid)row[ORIGINID],
                        OriginName = (string)row[NAME],
                        DAUs = 0
                    });
            }
            return retVal;
        }

        private List<string> RetrieveFlavors(SqlConnection connection, DataAccessLayer.FinancialDashboardDailyData dal)
        {
            List<string> retVal = new List<string>();
            var reader = dal.ExecuteReader(getFlavors, connection);
            foreach (var row in reader)
            {
                retVal.Add((string)row[NAME]);
            }
            return retVal;
        }

        #region Queries

        public const string getDistributionOrigins =
@"
select new_originid, new_name
from new_origin with(nolock)
where deletionstatecode = 0
and new_isdistribution = 1
and new_isinfinancialdashboard = 1
order by new_name
";

        public const string getDistributionOriginsAndDaus =
@"
declare @maxDate datetime
select @maxDate = MAX(date) from [DynamicNoProblem].[dbo].[Exposure]

select
isnull(SUM([Visitor]), 0) daus,
new_originid, new_name
  FROM [DynamicNoProblem].[dbo].[Exposure] e with(nolock)
  right join new_origin org with(nolock) on e.OriginId = org.new_originid and date = @maxDate
  where 
   deletionstatecode = 0
  and new_isdistribution = 1
  group by new_originid, new_name
 order by new_name
";
        public const string _getDistributionOrigins =
@"
select new_originid, new_name
from new_origin with(nolock)
where deletionstatecode = 0
and isnull(new_isdistribution, 0) = 1
order by new_name
";

        public const string getInjectionOrigins =
@"
select new_originid, new_name
from new_origin with(nolock)
where deletionstatecode = 0
and isnull(new_isdistribution, 0) = 0
and new_isinfinancialdashboard = 1
order by new_name
";

        public const string getFlavors =
@"
select new_name
from new_sliderflavor with(nolock)
where deletionstatecode = 0
";

        #endregion
    }
}
