﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Dashboard.FinancialDashboard
{
    public class DataCreator : XrmUserBase
    {
        public DataCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            dal = new NoProblem.Core.DataAccessLayer.FinancialDashboardDailyData(XrmDataContext);
            config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(xrmDataContext);
            distributionOriginIds = GetAllDistributionOriginIds();
            injectionOriginIds = GetAllInjectionOriginIds();
            //headingIds = GetAllHeadingIds();
            //missingHeadingIds = GetAllMissingHeadingIds();
            //flavors = GetFlavors();
            List<Guid> payingAccountIds = GetPayingAccountIds();
            StringBuilder builer = new StringBuilder();
            int insertPsikCount = payingAccountIds.Count - 1;
            for (int i = 0; i < payingAccountIds.Count; i++)
            {
                builer.Append(geresh).Append(payingAccountIds[i]).Append(geresh);
                if (i < insertPsikCount)
                {
                    builer.Append(psik);
                }
            }
            payingAccountIdsStr = builer.ToString();
            UpdateAveragePpcTable();
        }

        private const string psik = ",";
        private const string geresh = "'";
        private DataAccessLayer.FinancialDashboardDailyData dal;
        private DataAccessLayer.ConfigurationSettings config;
        private List<Guid> distributionOriginIds;
        private List<Guid> injectionOriginIds;
        //private List<Guid> headingIds;
        //private List<Guid> missingHeadingIds;
        //private List<string> flavors;
        private readonly string payingAccountIdsStr;
        private static Timer timer;
        private System.Collections.Generic.Dictionary<Guid, DauDataContainer> dauDataMap;
        private int dauAllVisitorsSumDistributions;
        private int dauAllVisitorsSumInjections;
        private static Guid widditOriginId = new Guid("F0AB6155-8751-E111-BE62-001517D10F6E");

        public void CalculatePastDays(int daysBack, bool doMissing)
        {
            int counter = daysBack;
            DateTime date = DateTime.Now.Date.AddDays(-1);
            while (counter > 0)
            {
                if (/*(doMissing && !Exists(date, missingHeadingIds.First())) || */ !Exists(date))
                {
                    CreateDataOfDate(date, doMissing);
                }
                date = date.AddDays(-1);
                counter--;
            }
        }

        public static void SetTimerAtAppStart()
        {
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            string enabled = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.FINANCIAL_DASHBOARD_ENABLED);
            if (enabled == "1")
            {
                TimeSpan invokeSpan = GetInvokeSpan();
                timer = new Timer(
                    TimerCallBack,
                    null,
                    invokeSpan,
                    new TimeSpan(1, 0, 0, 0)
                    );
            }
        }

        private bool Exists(DateTime date)
        {
            string query =
                @"select top 1 1 from new_financialdashboarddailydata where deletionstatecode = 0 and new_date = @date";
            DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata>.SqlParametersList();
            parameters.Add("@date", date);
            object scalar = dal.ExecuteScalar(query, parameters);
            return scalar != null;
        }

        private bool Exists(DateTime date, Guid headingId)
        {
            string query =
                @"select top 1 1 from new_financialdashboarddailydata where deletionstatecode = 0 and new_date = @date and new_primaryexpertiseid = @headingId";
            DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata>.SqlParametersList();
            parameters.Add("@date", date);
            parameters.Add("@headingId", headingId);
            object scalar = dal.ExecuteScalar(query, parameters);
            return scalar != null;
        }

        private static TimeSpan GetInvokeSpan()
        {
            DateTime invokeTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0);
            if (DateTime.Now.Hour >= 10)
            {
                invokeTime = invokeTime.AddDays(1);
            }
            return invokeTime - DateTime.Now;
        }

        private static void TimerCallBack(object obj)
        {
            LogUtils.MyHandle.WriteToLog("FinancialDashboard daily run started");
            try
            {
                DataCreator creator = new DataCreator(null);
                creator.CalculateYesterday();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in FinancialDashboard's TimerCallBack");
            }
            LogUtils.MyHandle.WriteToLog("FinancialDashboard daily run ended");
        }

        private void CalculateYesterday()
        {
            CreateDataOfDate(DateTime.Now.Date.AddDays(-1), false);
        }

        private void CreateDataOfDate(DateTime calculatingDate, bool doMissing)
        {
            dauDataMap = GetDauData(calculatingDate);

            DateTime fromDate = calculatingDate;
            DateTime toDate = fromDate;
            FixDatesToUtcFullDays(ref fromDate, ref toDate);

            using (SqlConnection connection = dal.GetSqlConnectionString())
            {
                connection.Open();

                if (!doMissing)
                {
                    //no filtering
                    ExecuteSet(calculatingDate, fromDate, toDate, connection, Guid.Empty, Guid.Empty, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndAllAndAll, false);
                    ExecuteSet(calculatingDate, fromDate, toDate, connection, Guid.Empty, Guid.Empty, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndAllAndAll, true);

                    foreach (var injectionId in injectionOriginIds)
                    {
                        //injections by themselfs
                        ExecuteSet(calculatingDate, fromDate, toDate, connection, Guid.Empty, injectionId, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndAllAndAll, false);
                    }

                    foreach (var distributionId in distributionOriginIds)
                    {
                        //distributions by themselfs
                        ExecuteSet(calculatingDate, fromDate, toDate, connection, Guid.Empty, distributionId, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndAllAndAll, true);
                    }

                    //foreach (var headId in headingIds)
                    //{
                    //    //headings by themselfs
                    //    ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, Guid.Empty, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndHeadingAndAll, true);
                    //    ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, Guid.Empty, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndHeadingAndAll, false);
                    //}

                    //foreach (var flvr in flavors)
                    //{
                    //    //flavors by themselfs
                    //    ExecuteSet(calculatingDate, fromDate, toDate, connection, Guid.Empty, Guid.Empty, flvr, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndAllAndFlavor, true);
                    //    ExecuteSet(calculatingDate, fromDate, toDate, connection, Guid.Empty, Guid.Empty, flvr, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndAllAndFlavor, false);
                    //}

                    //foreach (var headId in headingIds)
                    //{
                    //    //foreach (var flvr in flavors)
                    //    //{
                    //    //    //flavor and heading
                    //    //    ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, Guid.Empty, flvr, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndHeadingAndFlavor, true);
                    //    //    ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, Guid.Empty, flvr, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndHeadingAndFlavor, false);
                    //    //}

                    //    foreach (var distributionId in distributionOriginIds)
                    //    {
                    //        //distribution and heading
                    //        ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, distributionId, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndHeadingAndAll, true);
                    //    }

                    //    foreach (var injectionId in injectionOriginIds)
                    //    {
                    //        //injection and heading
                    //        ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, injectionId, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndHeadingAndAll, false);
                    //    }
                    //}


                    //foreach (var distributionId in distributionOriginIds)
                    //{
                    //    foreach (var flvr in flavors)
                    //    {
                    //        //distribution and flavor
                    //        ExecuteSet(calculatingDate, fromDate, toDate, connection, Guid.Empty, distributionId, flvr, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndAllAndFlavor, true);

                    //        foreach (var headId in headingIds)
                    //        {
                    //            //flavor and heading and distribution
                    //            ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, distributionId, flvr, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndHeadingAndFlavor, true);
                    //        }
                    //    }
                    //}

                    //foreach (var injectionId in injectionOriginIds)
                    //{
                    //    foreach (var flvr in flavors)
                    //    {
                    //        //injection and flavor
                    //        ExecuteSet(calculatingDate, fromDate, toDate, connection, Guid.Empty, injectionId, flvr, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndAllAndFlavor, false);

                    //        foreach (var headId in headingIds)
                    //        {
                    //            //flavor and heading and injection
                    //            ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, injectionId, flvr, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndHeadingAndFlavor, false);
                    //        }
                    //    }
                    //}
                }
                else
                {
                    //foreach (var headId in missingHeadingIds)
                    //{
                    //    //headings by themselfs
                    //    ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, Guid.Empty, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndHeadingAndAll, true);
                    //    ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, Guid.Empty, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndHeadingAndAll, false);
                    //}

                    //foreach (var headId in missingHeadingIds)
                    //{
                    //    //foreach (var flvr in flavors)
                    //    //{
                    //    //    //flavor and heading
                    //    //    ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, Guid.Empty, flvr, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndHeadingAndFlavor, true);
                    //    //    ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, Guid.Empty, flvr, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndHeadingAndFlavor, false);
                    //    //}

                    //    foreach (var distributionId in distributionOriginIds)
                    //    {
                    //        //distribution and heading
                    //        ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, distributionId, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndHeadingAndAll, true);
                    //    }

                    //    foreach (var injectionId in injectionOriginIds)
                    //    {
                    //        //injection and heading
                    //        ExecuteSet(calculatingDate, fromDate, toDate, connection, headId, injectionId, null, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndHeadingAndAll, false);
                    //    }
                    //}
                }
            }
        }

        private Dictionary<Guid, DauDataContainer> GetDauData(DateTime calculatingDate)
        {
            Dictionary<Guid, DauDataContainer> retVal = new Dictionary<Guid, DauDataContainer>();
            DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata>.SqlParametersList();
            parameters.Add("@date", calculatingDate);
            string dbName = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.DB_NAME);
            var reader = dal.ExecuteReader(string.Format(queryDau, dbName), parameters);
            foreach (var row in reader)
            {
                DauDataContainer container = new DauDataContainer();
                container.Originid = (Guid)row["OriginId"];
                container.IsDistribution = row["new_isdistribution"] != DBNull.Value ? (bool)row["new_isdistribution"] : false;
                container.Visitors = (int)row["Visitor"];
                retVal.Add(container.Originid, container);
            }
            dauAllVisitorsSumDistributions = retVal.Where(x => x.Value.IsDistribution).Sum(x => x.Value.Visitors);
            dauAllVisitorsSumInjections = retVal.Where(x => !x.Value.IsDistribution).Sum(x => x.Value.Visitors);
            return retVal;
        }

        private List<Guid> GetPayingAccountIds()
        {
            List<Guid> retVal = new List<Guid>();
            var reader = dal.ExecuteReader(queryPaying);
            foreach (var row in reader)
            {
                retVal.Add((Guid)row["Accountid"]);
            }
            return retVal;
        }

        private void ExecuteSet(DateTime calculatingDate, DateTime fromDate, DateTime toDate, SqlConnection connection, Guid headingId, Guid originId, string flavor, DataModel.Xrm.new_financialdashboarddailydata.Type type, bool isDistribution)
        {
            try
            {
                DataModel.Xrm.new_financialdashboarddailydata dayData = DoQuery(calculatingDate, fromDate, toDate, originId, headingId, flavor, connection, type, isDistribution);
                SaveDayData(dayData, calculatingDate, originId, headingId, flavor, type, isDistribution);
            }
            catch (Exception exc)
            {

                LogUtils.MyHandle.HandleException(exc, "Exception in FinancialDashboard.DataCreator.ExecuteOriginHeadingPair. Advancing to next pair. HeadingId = {0}, OriginId = {1}, CalculatingDate = {2}", headingId, originId, calculatingDate);
            }
        }

        private void SaveDayData(NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata dayData, DateTime calculatingDate, Guid originId, Guid headingId, string flavor, DataModel.Xrm.new_financialdashboarddailydata.Type type, bool isDistribution)
        {
            dayData.new_date = calculatingDate;
            if (originId != Guid.Empty)
            {
                dayData.new_originid = originId;
                Guid guidToUse;
                if (originId.Equals(widditOriginId))//widdit is Guid.Empty in the dau table
                {
                    guidToUse = Guid.Empty;
                }
                else
                {
                    guidToUse = originId;
                }
                DauDataContainer container;
                if (dauDataMap.TryGetValue(guidToUse, out container))
                {
                    dayData.new_dau = container.Visitors;
                }
                else
                {
                    dayData.new_dau = 0;
                }
            }
            else
            {
                if (isDistribution)
                {
                    dayData.new_dau = dauAllVisitorsSumDistributions;
                }
                else
                {
                    dayData.new_dau = dauAllVisitorsSumInjections;
                }
            }
            if (headingId != Guid.Empty)
            {
                dayData.new_primaryexpertiseid = headingId;
            }
            if (flavor != null)
            {
                dayData.new_flavor = flavor;
            }
            dayData.new_type = (int)type;
            dal.Create(dayData);
            XrmDataContext.SaveChanges();
        }

        private DataModel.Xrm.new_financialdashboarddailydata DoQuery(DateTime calculatingDate, DateTime fromDate, DateTime toDate, Guid originId, Guid headingId, string flavor, SqlConnection connection, DataModel.Xrm.new_financialdashboarddailydata.Type type, bool isDistribution)
        {
            DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata>.SqlParametersList();
            string query = GetQuery(originId, headingId, flavor, parameters, type);
            query = string.Format(query, payingAccountIdsStr);
            parameters.Add("@from", fromDate);
            parameters.Add("@to", toDate);
            parameters.Add("@date", calculatingDate);
            var reader = dal.ExecuteReader(query, parameters, connection, System.Data.CommandType.Text);
            var row = reader.First();
            DataModel.Xrm.new_financialdashboarddailydata dayData = new NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata();
            dayData.new_exposures = row["exposures"] != DBNull.Value ? (int)row["exposures"] : 0;
            dayData.new_requests = (int)row["requests"];
            dayData.new_potentialrevenue = row["potentailRevenue"] != DBNull.Value ? (decimal)row["potentailRevenue"] : 0;
            dayData.new_actualrevenue = row["actualRevenue"] != DBNull.Value ? (decimal)row["actualRevenue"] : 0;
            dayData.new_netactualrevenue = row["netActualRevenue"] != DBNull.Value ? (decimal)row["netActualRevenue"] : 0;
            dayData.new_fullfullfilmentrevenue = row["fullFullfilmentRevenue"] != DBNull.Value ? (decimal)row["fullFullfilmentRevenue"] : 0;
            dayData.new_upsales = (int)row["upsales"];
            dayData.new_netactualrevenueupsales = row["netActualRevenueUpsales"] != DBNull.Value ? (decimal)row["netActualRevenueUpsales"] : 0;
            if (isDistribution)
            {
                dayData.new_requestswithactualrevenue = (int)row["requestsWithActualRevenue"];
                dayData.new_requestswithnetactualrevenue = (int)row["requestsWithNetActualRevenue"];
                dayData.new_costofinstallation = Convert.ToDecimal(row["costOfInstallation"]);
                dayData.new_potentialcost = 0;
                dayData.new_actualcost = 0;
                dayData.new_netactualcost = 0;
                dayData.new_fullfullfilmentcost = 0;
                dayData.new_potentialcost2 = 0;
                dayData.new_actualcost2 = 0;
                dayData.new_netactualcost2 = 0;
                dayData.new_fullfullfilmentcost2 = 0;
                dayData.new_requestcountpotential = 0;
                dayData.new_requestcountactual = 0;
                dayData.new_requestcountnetactual = 0;
                dayData.new_requestcountfullfullfilment = 0;
                dayData.new_netactualcost2upsales = 0;
            }
            else
            {
                dayData.new_netactualcost2upsales = row["netActualCost2upsales"] != DBNull.Value ? Convert.ToDecimal(row["netActualCost2upsales"]) : 0;
                dayData.new_potentialcost = row["potentialCost"] != DBNull.Value ? Convert.ToDecimal(row["potentialCost"]) : 0;
                dayData.new_actualcost = row["actualCost"] != DBNull.Value ? Convert.ToDecimal(row["actualCost"]) : 0;
                dayData.new_netactualcost = row["netActualCost"] != DBNull.Value ? Convert.ToDecimal(row["netActualCost"]) : 0;
                dayData.new_fullfullfilmentcost = row["fullFullfilmentCost"] != DBNull.Value ? Convert.ToDecimal(row["fullFullfilmentCost"]) : 0;
                dayData.new_potentialcost2 = row["potentialCost2"] != DBNull.Value ? Convert.ToDecimal(row["potentialCost2"]) : 0;
                dayData.new_actualcost2 = row["actualCost2"] != DBNull.Value ? Convert.ToDecimal(row["actualCost2"]) : 0;
                dayData.new_netactualcost2 = row["netActualCost2"] != DBNull.Value ? Convert.ToDecimal(row["netActualCost2"]) : 0;
                dayData.new_fullfullfilmentcost2 = row["fullFullfilmentCost2"] != DBNull.Value ? Convert.ToDecimal(row["fullFullfilmentCost2"]) : 0;
                dayData.new_requestswithactualrevenue = 0;
                dayData.new_requestswithnetactualrevenue = 0;
                dayData.new_costofinstallation = 0;
                dayData.new_requestcountpotential = row["requestCountPotential"] != DBNull.Value ? Convert.ToDecimal(row["requestCountPotential"]) : 0;
                dayData.new_requestcountactual = row["requestCountActual"] != DBNull.Value ? Convert.ToDecimal(row["requestCountActual"]) : 0;
                dayData.new_requestcountnetactual = row["requestCountNetActual"] != DBNull.Value ? Convert.ToDecimal(row["requestCountNetActual"]) : 0;
                dayData.new_requestcountfullfullfilment = row["reqCountFullFullfilment"] != DBNull.Value ? Convert.ToDecimal(row["reqCountFullFullfilment"]) : 0;
            }
            return dayData;
        }

        private string GetQuery(
            Guid originId,
            Guid headingId,
            string flavor,
            DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters,
            DataModel.Xrm.new_financialdashboarddailydata.Type type)
        {
            string query = string.Empty;
            switch (type)
            {
                //Distributions:
                case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndAllAndAll:
                    query = FinancialReportQueries.queryAllDistributionsAndAllAndAll;
                    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndHeadingAndAll:
                //    query = FinancialReportQueries.queryAllDistributionsAndHeadingAndAll;
                //    parameters.Add("@headingId", headingId);
                //    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndAllAndFlavor:
                //    query = FinancialReportQueries.queryAllDistributionsAndAllAndFlavor;
                //    parameters.Add("@flavor", flavor);
                //    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndHeadingAndFlavor:
                //    query = FinancialReportQueries.queryAllDistributionsAndHeadingAndFlavor;
                //    parameters.Add("@headingId", headingId);
                //    parameters.Add("@flavor", flavor);
                //    break;
                case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndAllAndAll:
                    query = FinancialReportQueries.queryDistributionAndAllAndAll;
                    parameters.Add("@originId", originId);
                    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndHeadingAndAll:
                //    query = FinancialReportQueries.queryDistributionAndHeadingAndAll;
                //    parameters.Add("@originId", originId);
                //    parameters.Add("@headingId", headingId);
                //    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndAllAndFlavor:
                //    query = FinancialReportQueries.queryDistributionAndAllAndFlavor;
                //    parameters.Add("@originId", originId);
                //    parameters.Add("@flavor", flavor);
                //    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndHeadingAndFlavor:
                //    query = FinancialReportQueries.queryDistributionAndHeadingAndFlavor;
                //    parameters.Add("@originId", originId);
                //    parameters.Add("@headingId", headingId);
                //    parameters.Add("@flavor", flavor);
                //    break;
                //Injections:
                case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndAllAndAll:
                    query = FinancialReportQueries.queryAllInjectionsAndAllAndAll;
                    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndHeadingAndAll:
                //    query = FinancialReportQueries.queryAllInjectionsAndHeadingAndAll;
                //    parameters.Add("@headingId", headingId);
                //    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndAllAndFlavor:
                //    query = FinancialReportQueries.queryAllInjectionsAndAllAndFlavor;
                //    parameters.Add("@flavor", flavor);
                //    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndHeadingAndFlavor:
                //    query = FinancialReportQueries.queryAllInjectionsAndHeadingAndFlavor;
                //    parameters.Add("@headingId", headingId);
                //    parameters.Add("@flavor", flavor);
                //    break;
                case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndAllAndAll:
                    query = FinancialReportQueries.queryInjectionAndAllAndAll;
                    parameters.Add("@originId", originId);
                    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndHeadingAndAll:
                //    query = FinancialReportQueries.queryInjectionAndHeadingAndAll;
                //    parameters.Add("@originId", originId);
                //    parameters.Add("@headingId", headingId);
                //    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndAllAndFlavor:
                //    query = FinancialReportQueries.queryInjectionAndAllAndFlavor;
                //    parameters.Add("@originId", originId);
                //    parameters.Add("@flavor", flavor);
                //    break;
                //case NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndHeadingAndFlavor:
                //    query = FinancialReportQueries.queryInjectionAndHeadingAndFlavor;
                //    parameters.Add("@originId", originId);
                //    parameters.Add("@headingId", headingId);
                //    parameters.Add("@flavor", flavor);
                //    break;
            }
            return query;
        }

        private List<Guid> GetAllHeadingIds()
        {
            List<Guid> retVal = new List<Guid>();
            var reader = dal.ExecuteReader(queryAllHeadings);
            foreach (var row in reader)
            {
                retVal.Add((Guid)row["new_primaryexpertiseid"]);
            }
            return retVal;
        }

        private List<Guid> GetAllMissingHeadingIds()
        {
            List<Guid> retVal = new List<Guid>();
            var reader = dal.ExecuteReader(queryMissingHeading);
            foreach (var row in reader)
            {
                retVal.Add((Guid)row["new_primaryexpertiseid"]);
            }
            return retVal;
        }

        private List<Guid> GetAllDistributionOriginIds()
        {
            List<Guid> retVal = new List<Guid>();
            var reader = dal.ExecuteReader(queryAllDistributionOrigins);
            foreach (var row in reader)
            {
                retVal.Add((Guid)row["new_originid"]);
            }
            return retVal;
        }

        private List<Guid> GetAllInjectionOriginIds()
        {
            List<Guid> retVal = new List<Guid>();
            var reader = dal.ExecuteReader(queryAllInjectionOrigins);
            foreach (var row in reader)
            {
                retVal.Add((Guid)row["new_originid"]);
            }
            return retVal;
        }

        private List<string> GetFlavors()
        {
            List<string> retVal = new List<string>();
            var reader = dal.ExecuteReader(queryGetFlavors);
            foreach (var row in reader)
            {
                retVal.Add((string)row["new_name"]);
            }
            return retVal;
        }

        private void UpdateAveragePpcTable()
        {
            dal.ExecuteNonQuery(FinancialReportQueries.updateAveragePpcTable);
        }

        #region Small Queries

        private const string queryGetFlavors =
            @"select new_name from new_sliderflavor with(nolock) where deletionstatecode = 0 and statecode = 0";

        //        private const string queryAllHeadings =
        //            @"select new_primaryexpertiseid from
        //                new_primaryexpertisebase with(nolock)
        //                where deletionstatecode = 0";

        private const string queryAllHeadings =
            @"select distinct new_primaryexpertiseid
            from
            New_sliderkeyword with(nolock)
            where deletionstatecode = 0
            and new_primaryexpertiseid is not null
            union
            select distinct new_primaryexpertiseid
            from Incident
            where new_primaryexpertiseid is not null";

        private const string queryMissingHeading =
            @"select distinct new_primaryexpertiseid
from Incident
where
new_primaryexpertiseid
NOT IN(
select distinct new_primaryexpertiseid
from new_financialdashboarddailydata
where new_primaryexpertiseid is not null
and createdon < dateadd(day, -1, getdate())
)";

        private const string queryAllDistributionOrigins =
            @"select new_originid from
                new_origin with(nolock) 
                where deletionstatecode = 0
                and new_isinfinancialdashboard = 1
                and new_isdistribution = 1";

        private const string queryAllInjectionOrigins =
           @"select new_originid from
                new_origin with(nolock) 
                where deletionstatecode = 0
                and new_isinfinancialdashboard = 1
                and isnull(new_isdistribution, 0) = 0";

        private const string queryDau =
            @"
use DynamicNoProblem
select OriginId, SUM(Visitor) as Visitor, new_isdistribution
from dbo.Exposure with(nolock)
left join {0}.dbo.new_origin with(nolock) on New_originId = OriginId
where Date = @date
group by OriginId, New_IsDistribution
";

        private const string queryPaying =
            @"
select distinct Accountid 
from Account acc with(nolock)
join New_supplierpricing sp with(nolock) on sp.new_accountid = acc.AccountId
where acc.New_IsLeadBuyer = 1
or sp.new_voucherid is null
order by Accountid
";

        #endregion

        private class DauDataContainer
        {
            public Guid Originid { get; set; }
            public bool IsDistribution { get; set; }
            public int Visitors { get; set; }

            public override int GetHashCode()
            {
                return Originid.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return false;
                }

                if (!(obj is DauDataContainer))
                {
                    return false;
                }
                DauDataContainer other = (DauDataContainer)obj;
                return this.Originid.Equals(other.Originid);
            }
        }
    }
}
