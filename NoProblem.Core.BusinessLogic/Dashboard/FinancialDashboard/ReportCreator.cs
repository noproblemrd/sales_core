﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Dashboard.FinancialDashboard;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.Dashboard.FinancialDashboard
{
    public class ReportCreator : XrmUserBase
    {
        public ReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            dal = new NoProblem.Core.DataAccessLayer.FinancialDashboardDailyData(XrmDataContext);
        }

        private DataAccessLayer.FinancialDashboardDailyData dal;

        public FinancialDashboardResponse CreateReport(FinancialDashboardRequest request)
        {
            FinancialDashboardResponse response = new FinancialDashboardResponse();
            List<DataModel.Xrm.new_financialdashboarddailydata> mainData = GetData(request.FromDate, request.ToDate, request.OriginId, request.HeadingId, request.Flavor, request.IsInjections, request.UseOnlySoldHeadings);
            response.MainGraph = CreateGraph(request.GraphToShow, request.FromDate, request.ToDate, mainData, request.LifeTime, request.IsInjections);
            if (!request.OnlyGraph)
            {
                int dayCount = (request.ToDate.Date - request.FromDate.Date).Days + 1;
                response.MainTableData = CalculateTable(mainData, dayCount, request.LifeTime, request.IsInjections);
            }
            if (request.UseCompare)
            {
                List<DataModel.Xrm.new_financialdashboarddailydata> compareData = GetData(request.CompareFromDate, request.CompareToDate, request.CompareOriginId, request.CompareHeadingId, request.CompareFlavor, request.IsInjections, request.UseOnlySoldHeadings);
                response.CompareGraph = CreateGraph(request.GraphToShow, request.CompareFromDate, request.CompareToDate, compareData, request.LifeTime, request.IsInjections);
                if (!request.OnlyGraph)
                {
                    int compareDayCount = (request.CompareToDate.Date - request.CompareFromDate.Date).Days + 1;
                    response.CompareTableData = CalculateTable(compareData, compareDayCount, request.LifeTime, request.IsInjections);
                    response.GapTableData = CalculateGap(response.MainTableData, response.CompareTableData);
                }
            }
            return response;
        }
        public DateTime GetFinancialLastUpdate()
        {
            string _connectionString = System.Configuration.ConfigurationManager.AppSettings["updateConnectionString"];
            //string _connectionString = "Data Source=10.154.29.6;Initial Catalog=Sales_MSCRM;Persist Security Info=True;User ID=crmdb;Password=Pass@word1";
            string command = "SELECT max([New_Date]) FROM [dbo].[New_financialdashboarddailydataExtensionBase]";
            DateTime dt = DateTime.MinValue;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    dt = (DateTime)cmd.ExecuteScalar();
                    conn.Close();
                }
            }
            return dt;
  
        }
        private FinancialTableData CalculateGap(FinancialTableData mainTable, FinancialTableData compareTable)
        {
            FinancialTableData table = new FinancialTableData();
            var properties = typeof(FinancialTableData).GetProperties();
            foreach (var prop in properties)
            {
                object mainValue = prop.GetValue(mainTable, null);
                object compareValue = prop.GetValue(compareTable, null);
                prop.SetValue(
                    table,
                    (double)mainValue / (double)compareValue * 100,
                    null);
            }
            return table;
        }

        private List<FinancialGraphNode> CreateGraph(string graphToShow, DateTime fromDate, DateTime toDate, List<DataModel.Xrm.new_financialdashboarddailydata> dayDataList, double lifeTime, bool isInjection)
        {
            List<FinancialGraphNode> graph = new List<FinancialGraphNode>();
            if (!string.IsNullOrEmpty(graphToShow))
            {
                DateTime current = fromDate.Date;
                while (current <= toDate.Date)
                {
                    var dayData = dayDataList.FirstOrDefault(x => x.new_date == current);
                    if (dayData != null)
                    {
                        double value = CalculateGraphValue(graphToShow, dayData, lifeTime, isInjection);
                        FinancialGraphNode node = new FinancialGraphNode();
                        node.Date = current;
                        node.Value = value;
                        graph.Add(node);
                    }
                    current = current.AddDays(1);
                }
            }
            return graph;
        }

        private double CalculateGraphValue(string graphToShow, NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata dayData, double lifeTime, bool isInjection)
        {
            switch (graphToShow)
            {
                case "AverageDau":
                    return dayData.new_dau.Value;
                case "Exposures":
                    return dayData.new_exposures.Value;
                case "Requests":
                    return dayData.new_requests.Value;
                case "Upsales":
                    return dayData.new_upsales.Value;
                case "UpsalesAgain":
                    return dayData.new_upsales.Value;
                case "AverageDailyExposures":
                    return dayData.new_exposures.Value;
                case "AverageDailyExposuresDividedByAverageDau":
                    return (double)dayData.new_exposures.Value / (double)dayData.new_dau.Value * 100;
                case "PotentialRevenue":
                    return decimal.ToDouble(dayData.new_potentialrevenue.Value);
                case "DailyPotentialRevenue":
                    return decimal.ToDouble(dayData.new_potentialrevenue.Value);
                case "PotentialARDAU":
                    return decimal.ToDouble(dayData.new_potentialrevenue.Value) / (double)dayData.new_dau.Value;
                case "LifetimeTimesPotentialARDAU":
                    return lifeTime * decimal.ToDouble(dayData.new_potentialrevenue.Value) / (double)dayData.new_dau.Value;
                case "ActualRevenue":
                    return decimal.ToDouble(dayData.new_actualrevenue.Value);
                case "RequestsThatGeneratedActualRevenues":
                    return dayData.new_requestswithactualrevenue.Value;
                case "DailyActualRevenue":
                    return decimal.ToDouble(dayData.new_actualrevenue.Value);
                case "ActualARDAU":
                    return decimal.ToDouble(dayData.new_actualrevenue.Value) / (double)dayData.new_dau.Value;
                case "LifetimeTimesActualARDAU":
                    return lifeTime * decimal.ToDouble(dayData.new_actualrevenue.Value) / (double)dayData.new_dau.Value;
                case "NetActualRevenue":
                    return decimal.ToDouble(dayData.new_netactualrevenue.Value);
                case "NetActualRevenueUpsales":
                    return decimal.ToDouble(dayData.new_netactualrevenueupsales.Value);
                case "RequestsThatGeneratedNetActualRevenue":
                    return dayData.new_requestswithnetactualrevenue.Value;
                case "DailyNetActualRevenue":
                    return decimal.ToDouble(dayData.new_netactualrevenue.Value);
                case "NetActualARDAU":
                    return decimal.ToDouble(dayData.new_netactualrevenue.Value) / (double)dayData.new_dau.Value;
                case "LifetimeTimesNetActualRevenueARDAU":
                    return lifeTime * decimal.ToDouble(dayData.new_netactualrevenue.Value) / (double)dayData.new_dau.Value;
                case "Conversion":
                    return (double)dayData.new_requests.Value / (double)dayData.new_exposures.Value;
                case "AveragePotentialPPL":
                    if (!isInjection)
                    {
                        return decimal.ToDouble(dayData.new_potentialrevenue.Value) / (double)dayData.new_requests.Value;
                    }
                    else
                    {
                        return (decimal.ToDouble(dayData.new_potentialrevenue.Value)) / (double)dayData.new_requests;
                    }
                case "AverageActualPPL":
                    if (!isInjection)
                    {
                        return decimal.ToDouble(dayData.new_actualrevenue.Value) / (double)dayData.new_requestswithactualrevenue.Value;
                    }
                    else
                    {
                        return (decimal.ToDouble(dayData.new_actualrevenue.Value)) / decimal.ToDouble(dayData.new_requestcountactual.Value);
                    }
                case "AverageNetActualPPL":
                    if (!isInjection)
                    {
                        return decimal.ToDouble(dayData.new_netactualrevenue.Value) / (double)dayData.new_requestswithnetactualrevenue.Value;
                    }
                    else
                    {
                        return (decimal.ToDouble(dayData.new_netactualrevenue.Value)) / decimal.ToDouble(dayData.new_requestcountnetactual.Value);
                    }
                case "TotalCostOfRequestsPotential":
                    return decimal.ToDouble(dayData.new_potentialcost2.Value);
                case "TotalCostOfRequestsActual":
                    return decimal.ToDouble(dayData.new_actualcost2.Value);
                case "TotalCostOfRequestsNetActual":
                    return decimal.ToDouble(dayData.new_netactualcost2.Value);
                case "TotalCostOfRequestsNetActualUpsales":
                    return decimal.ToDouble(dayData.new_netactualcost2upsales.Value);
                case "CostPerRequestPotential":
                    return decimal.ToDouble(dayData.new_potentialcost.Value) / decimal.ToDouble(dayData.new_requestcountpotential.Value);
                case "CostPerRequestActual":
                    return decimal.ToDouble(dayData.new_actualcost.Value) / decimal.ToDouble(dayData.new_requestcountactual.Value);
                case "CostPerRequestNetActual":
                    return decimal.ToDouble(dayData.new_netactualcost.Value) / decimal.ToDouble(dayData.new_requestcountnetactual.Value);
                case "CostPerMilliaPotential":
                    return decimal.ToDouble(dayData.new_potentialcost.Value) / decimal.ToDouble(dayData.new_exposures.Value) * 1000;
                case "RevenuePerMilliaPotential":
                    return (decimal.ToDouble(dayData.new_potentialrevenue.Value)) / decimal.ToDouble(dayData.new_exposures.Value) * 1000;
                case "CostPerMilliaActual":
                    return decimal.ToDouble(dayData.new_actualcost.Value) / decimal.ToDouble(dayData.new_exposures.Value) * 1000;
                case "RevenuePerMilliaActual":
                    return (decimal.ToDouble(dayData.new_actualrevenue.Value)) / decimal.ToDouble(dayData.new_exposures.Value) * 1000;
                case "CostPerMilliaNetActual":
                    return decimal.ToDouble(dayData.new_netactualcost.Value) / decimal.ToDouble(dayData.new_exposures.Value) * 1000;
                case "RevenuePerMilliaNetActual":
                    return (decimal.ToDouble(dayData.new_netactualrevenue.Value)) / decimal.ToDouble(dayData.new_exposures.Value) * 1000;
                case "RoiPotential":
                    return lifeTime * decimal.ToDouble(dayData.new_potentialrevenue.Value) / (double)dayData.new_dau.Value / decimal.ToDouble(dayData.new_costofinstallation.Value);
                case "RoiActual":
                    return lifeTime * decimal.ToDouble(dayData.new_actualrevenue.Value) / (double)dayData.new_dau.Value / decimal.ToDouble(dayData.new_costofinstallation.Value);
                case "RoiNetActual":
                    return lifeTime * decimal.ToDouble(dayData.new_netactualrevenue.Value) / (double)dayData.new_dau.Value / decimal.ToDouble(dayData.new_costofinstallation.Value);
                case "CostOfAddOnInstallation":
                    return decimal.ToDouble(dayData.new_costofinstallation.Value);
                case "FullFullfilmentRevenue":
                    return decimal.ToDouble(dayData.new_fullfullfilmentrevenue.Value);
                case "TotalCostOfRequestsFullFullfilment":
                    return decimal.ToDouble(dayData.new_fullfullfilmentcost2.Value);
                case "CostPerRequestFullFullfilment":
                    return decimal.ToDouble(dayData.new_fullfullfilmentcost.Value) / decimal.ToDouble(dayData.new_requestcountfullfullfilment.Value);
                case "CostPerMilliaFullFullfilment":
                    return decimal.ToDouble(dayData.new_fullfullfilmentcost.Value) / decimal.ToDouble(dayData.new_exposures.Value) * 1000;
                case "RevenuePerMilliaFullFullfilment":
                    return (decimal.ToDouble(dayData.new_fullfullfilmentrevenue.Value)) / decimal.ToDouble(dayData.new_exposures.Value) * 1000;
                case "FullFullfilmentARDAU":
                    return decimal.ToDouble(dayData.new_fullfullfilmentrevenue.Value) / (double)dayData.new_dau.Value;
                case "LifetimeTimesFullFullfilmentRevenueARDAU":
                    return lifeTime * decimal.ToDouble(dayData.new_fullfullfilmentrevenue.Value) / (double)dayData.new_dau.Value;
                case "DailyFullFullfilmentRevenue":
                    return decimal.ToDouble(dayData.new_fullfullfilmentrevenue.Value);
                case "RoiFullFullfilment":
                    return lifeTime * decimal.ToDouble(dayData.new_fullfullfilmentrevenue.Value) / (double)dayData.new_dau.Value / decimal.ToDouble(dayData.new_costofinstallation.Value);
                case "CostOfRevenueGeneratingLeadsPotential":
                    return decimal.ToDouble(dayData.new_potentialcost.Value);
                case "CostOfRevenueGeneratingLeadsActual":
                    return decimal.ToDouble(dayData.new_actualcost.Value);
                case "CostOfRevenueGeneratingLeadsNetActual":
                    return decimal.ToDouble(dayData.new_netactualcost.Value);
                case "CostOfRevenueGeneratingLeadsFullFullfilment":
                    return decimal.ToDouble(dayData.new_fullfullfilmentcost.Value);
                default:
                    return -1;
            }
        }

        private FinancialTableData CalculateTable(List<DataModel.Xrm.new_financialdashboarddailydata> dayDataList, double dayCount, double lifeTime, bool isInjection)
        {
            FinancialTableData table = new FinancialTableData();
            table.Exposures = dayDataList.Sum(x => x.new_exposures).Value;
            table.Requests = dayDataList.Sum(x => x.new_requests).Value;
            table.Upsales = dayDataList.Sum(x => x.new_upsales).Value;
            table.UpsalesAgain = dayDataList.Sum(x => x.new_upsales).Value;
            table.PotentialRevenue = decimal.ToDouble(dayDataList.Sum(x => x.new_potentialrevenue).Value);
            table.ActualRevenue = decimal.ToDouble(dayDataList.Sum(x => x.new_actualrevenue).Value);
            table.NetActualRevenue = decimal.ToDouble(dayDataList.Sum(x => x.new_netactualrevenue).Value);
            table.NetActualRevenueUpsales = decimal.ToDouble(dayDataList.Sum(x => x.new_netactualrevenueupsales).Value);
            table.Conversion = (double)table.Requests / (double)table.Exposures * 100;

            table.TotalCostOfRequestsFullFullfilment = decimal.ToDouble(dayDataList.Sum(x => x.new_fullfullfilmentcost2.Value));
            table.TotalCostOfRequestsPotential = decimal.ToDouble(dayDataList.Sum(x => x.new_potentialcost2.Value));
            table.TotalCostOfRequestsActual = decimal.ToDouble(dayDataList.Sum(x => x.new_actualcost2.Value));
            table.TotalCostOfRequestsNetActual = decimal.ToDouble(dayDataList.Sum(x => x.new_netactualcost2.Value));
            table.TotalCostOfRequestsNetActualUpsales = decimal.ToDouble(dayDataList.Sum(x => x.new_netactualcost2upsales.Value));

            //if (isInjection)
            //{
            table.CostOfRevenueGeneratingLeadsPotential = decimal.ToDouble(dayDataList.Sum(x => x.new_potentialcost.Value));
            table.CostOfRevenueGeneratingLeadsActual = decimal.ToDouble(dayDataList.Sum(x => x.new_actualcost.Value));
            table.CostOfRevenueGeneratingLeadsNetActual = decimal.ToDouble(dayDataList.Sum(x => x.new_netactualcost.Value));
            table.CostPerRequestPotential = table.TotalCostOfRequestsPotential / decimal.ToDouble(dayDataList.Sum(x => x.new_requestcountpotential.Value));
            table.CostPerRequestActual = table.TotalCostOfRequestsActual / decimal.ToDouble(dayDataList.Sum(x => x.new_requestcountactual.Value));
            table.CostPerRequestNetActual = table.TotalCostOfRequestsNetActual / decimal.ToDouble(dayDataList.Sum(x => x.new_requestcountnetactual.Value));
            table.CostPerMilliaPotential = table.TotalCostOfRequestsPotential / (double)table.Exposures * 1000;
            table.RevenuePerMilliaPotential = (table.PotentialRevenue) / table.Exposures * 1000;
            table.CostPerMilliaActual = table.TotalCostOfRequestsActual / (double)table.Exposures * 1000;
            table.RevenuePerMilliaActual = (table.ActualRevenue) / table.Exposures * 1000;
            table.CostPerMilliaNetActual = table.TotalCostOfRequestsNetActual / (double)table.Exposures * 1000;
            table.RevenuePerMilliaNetActual = (table.NetActualRevenue) / table.Exposures * 1000;
            if (isInjection)
            {
                table.AveragePotentialPPL = (table.PotentialRevenue) / (double)table.Requests;
                table.AverageActualPPL = (table.ActualRevenue) / decimal.ToDouble(dayDataList.Sum(x => x.new_requestcountactual).Value);
                table.AverageNetActualPPL = (table.NetActualRevenue) / decimal.ToDouble(dayDataList.Sum(x => x.new_requestcountnetactual).Value);
            }
            //}
            //else
            //{
            table.DailyPotentialRevenue = table.PotentialRevenue / dayCount;
            table.RequestsThatGeneratedActualRevenues = dayDataList.Sum(x => x.new_requestswithactualrevenue).Value;
            table.DailyActualRevenue = table.ActualRevenue / dayCount;
            table.DailyNetActualRevenue = table.NetActualRevenue / dayCount;
            table.AverageDailyExposures = (double)table.Exposures / dayCount;
            table.AverageDau = (double)dayDataList.Sum(x => x.new_dau).Value / dayCount;
            table.NetActualARDAU = table.DailyNetActualRevenue / table.AverageDau;
            table.LifetimeTimesNetActualRevenueARDAU = table.NetActualARDAU * lifeTime;
            table.ActualARDAU = table.DailyActualRevenue / table.AverageDau;
            table.LifetimeTimesActualARDAU = table.ActualARDAU * lifeTime;
            table.PotentialARDAU = table.DailyPotentialRevenue / table.AverageDau;
            table.LifetimeTimesPotentialARDAU = table.PotentialARDAU * lifeTime;
            table.AverageDailyExposuresDividedByAverageDau = table.AverageDailyExposures / table.AverageDau * 100;
            table.RequestsThatGeneratedNetActualRevenue = dayDataList.Sum(x => x.new_requestswithnetactualrevenue).Value;
            table.CostOfAddOnInstallation = (dayDataList.Count == 0) ? 0 :
                decimal.ToDouble(dayDataList.Average(x => (x.new_costofinstallation.HasValue) ? x.new_costofinstallation.Value : 0));
            table.RoiPotential = table.LifetimeTimesPotentialARDAU / table.CostOfAddOnInstallation;
            table.RoiActual = table.LifetimeTimesActualARDAU / table.CostOfAddOnInstallation;
            table.RoiNetActual = table.LifetimeTimesNetActualRevenueARDAU / table.CostOfAddOnInstallation;
            if (!isInjection)
            {
                table.AveragePotentialPPL = table.PotentialRevenue / (double)table.Requests;
                table.AverageActualPPL = table.ActualRevenue / (double)table.RequestsThatGeneratedActualRevenues;
                table.AverageNetActualPPL = table.NetActualRevenue / (double)table.RequestsThatGeneratedNetActualRevenue;
            }
            //}

            table.FullFullfilmentRevenue = decimal.ToDouble(dayDataList.Sum(x => x.new_fullfullfilmentrevenue).Value);
            table.DailyFullFullfilmentRevenue = table.FullFullfilmentRevenue / dayCount;
            table.FullFullfilmentARDAU = table.DailyFullFullfilmentRevenue / table.AverageDau;
            table.LifetimeTimesFullFullfilmentRevenueARDAU = table.FullFullfilmentARDAU * lifeTime;
            table.RoiFullFullfilment = table.LifetimeTimesFullFullfilmentRevenueARDAU / table.CostOfAddOnInstallation;

            table.CostOfRevenueGeneratingLeadsFullFullfilment = decimal.ToDouble(dayDataList.Sum(x => x.new_fullfullfilmentcost.Value));
            table.CostPerRequestFullFullfilment = table.TotalCostOfRequestsFullFullfilment / decimal.ToDouble(dayDataList.Sum(x => x.new_requestcountfullfullfilment.Value));
            table.CostPerMilliaFullFullfilment = table.TotalCostOfRequestsFullFullfilment / (double)table.Exposures * 1000;
            table.RevenuePerMilliaFullFullfilment = (table.FullFullfilmentRevenue) / table.Exposures * 1000;

            if (isInjection)
            {
                table.AverageFullFullfilmentPPL = table.FullFullfilmentRevenue / decimal.ToDouble(dayDataList.Sum(x => x.new_requestcountfullfullfilment).Value);
            }
            else
            {
                table.AverageFullFullfilmentPPL = table.FullFullfilmentRevenue / (double)table.Requests;
            }           

            return table;
        }

        private List<DataModel.Xrm.new_financialdashboarddailydata> GetData(DateTime fromDate, DateTime toDate, Guid originId, Guid headingId, string flavor, bool isInjection, bool useOnlySoldHeadings)
        {
            fromDate = fromDate.Date;
            toDate = toDate.Date;//don't add + 1. 
            DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata>.SqlParametersList();
            parameters.Add("@from", fromDate);
            parameters.Add("@to", toDate);
            string query = GetQuery(originId, headingId, flavor, parameters, isInjection, useOnlySoldHeadings);
            List<DataModel.Xrm.new_financialdashboarddailydata> dayDataList = ExecuteQuery(parameters, query);
            return dayDataList;
        }

        private List<DataModel.Xrm.new_financialdashboarddailydata> ExecuteQuery(DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters, string query)
        {
            var reader = dal.ExecuteReader(query, parameters);
            List<DataModel.Xrm.new_financialdashboarddailydata> dayDataList = new List<NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata>();
            foreach (var row in reader)
            {
                DataModel.Xrm.new_financialdashboarddailydata dayData = new NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata();
                dayData.new_date = (DateTime)row["new_date"];
                dayData.new_exposures = (int)row["new_exposures"];
                dayData.new_requests = (int)row["new_requests"];
                dayData.new_upsales = row["new_upsales"] != DBNull.Value ? (int)row["new_upsales"] : 0;
                dayData.new_dau = (int)row["new_dau"];
                dayData.new_netactualrevenue = (decimal)row["new_netactualrevenue"];
                dayData.new_netactualrevenueupsales = row["new_netactualrevenueupsales"] != DBNull.Value ? (decimal)row["new_netactualrevenueupsales"] : 0;
                dayData.new_potentialrevenue = (decimal)row["new_potentialrevenue"];
                dayData.new_requestswithactualrevenue = (int)row["new_requestswithactualrevenue"];
                dayData.new_requestswithnetactualrevenue = (int)row["new_requestswithnetactualrevenue"];
                dayData.new_actualrevenue = (decimal)row["new_actualrevenue"];
                dayData.new_potentialcost = (decimal)row["new_potentialcost"];
                dayData.new_actualcost = (decimal)row["new_actualcost"];
                dayData.new_netactualcost = (decimal)row["new_netactualcost"];
                dayData.new_costofinstallation = (decimal)row["new_costofinstallation"];
                dayData.new_requestcountpotential = (decimal)row["new_requestcountpotential"];
                dayData.new_requestcountactual = (decimal)row["new_requestcountactual"];
                dayData.new_requestcountnetactual = (decimal)row["new_requestcountnetactual"];
                dayData.new_fullfullfilmentcost = (decimal)row["new_fullfullfilmentcost"];
                dayData.new_fullfullfilmentrevenue = (decimal)row["new_fullfullfilmentrevenue"];
                dayData.new_requestcountfullfullfilment = (decimal)row["new_requestcountfullfullfilment"];
                dayData.new_potentialcost2 = (decimal)row["new_potentialcost2"];
                dayData.new_actualcost2 = (decimal)row["new_actualcost2"];
                dayData.new_netactualcost2 = (decimal)row["new_netactualcost2"];
                dayData.new_fullfullfilmentcost2 = (decimal)row["new_fullfullfilmentcost2"];
                dayData.new_netactualcost2upsales = row["new_netactualcost2upsales"] != DBNull.Value ? (decimal)row["new_netactualcost2upsales"] : 0;
                dayDataList.Add(dayData);
            }
            return dayDataList;
        }

        private string GetQuery(Guid originId, Guid headingId, string flavor, DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters, bool isInjection, bool useOnlySoldHeadings)
        {
            //DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            //bool useOnlySoldHeading = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.FINANCIAL_DASHBOARD_ONLY_SOLD_HEADINGS) == 1.ToString();
            useOnlySoldHeadings = (useOnlySoldHeadings && isInjection && (headingId == Guid.Empty));
            StringBuilder query;
            if (useOnlySoldHeadings)
            {
                query = new StringBuilder(getDataQueryOnlySold);
            }
            else
            {
                query = new StringBuilder(getDataQuery);
            }
            DataModel.Xrm.new_financialdashboarddailydata.Type type;
            //all origins
            if (Guid.Empty == originId)
            {
                //all headings
                if (Guid.Empty == headingId)
                {
                    //all,all,all
                    if (string.IsNullOrEmpty(flavor))
                    {
                        if (isInjection)
                        {
                            if (useOnlySoldHeadings)
                            {
                                type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndHeadingAndAll;
                            }
                            else
                            {
                                type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndAllAndAll;
                            }
                        }
                        else
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndAllAndAll;
                        }
                    }
                    //all origins, all heading, specific flavor
                    else
                    {
                        AddFlavorToQuery(flavor, parameters, query);
                        if (isInjection)
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndAllAndFlavor;

                        }
                        else
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndAllAndFlavor;
                        }
                    }
                }
                //specific heading
                else
                {
                    AddHeadingToQuery(headingId, parameters, query);
                    //all origins, specific heading, all flavors
                    if (string.IsNullOrEmpty(flavor))
                    {
                        if (isInjection)
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndHeadingAndAll;
                        }
                        else
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndHeadingAndAll;
                        }
                    }
                    //all origin, specific heading, specific flavor
                    else
                    {
                        AddFlavorToQuery(flavor, parameters, query);
                        if (isInjection)
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllInjectionsAndHeadingAndFlavor;
                        }
                        else
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.AllDistributionsAndHeadingAndFlavor;
                        }
                    }
                }
            }
            //specific origin
            else
            {
                AddOriginToQuery(originId, parameters, query);
                //all headings
                if (Guid.Empty == headingId)
                {
                    //specific origin, all heading, all flavors
                    if (string.IsNullOrEmpty(flavor))
                    {
                        if (isInjection)
                        {
                            if (useOnlySoldHeadings)
                            {
                                type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndHeadingAndAll;
                            }
                            else
                            {
                                type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndAllAndAll;
                            }
                        }
                        else
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndAllAndAll;
                        }
                    }
                    //specific origin, all heading, specific flavor
                    else
                    {
                        AddFlavorToQuery(flavor, parameters, query);
                        if (isInjection)
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndAllAndFlavor;
                        }
                        else
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndAllAndFlavor;
                        }
                    }
                }
                //specific heading
                else
                {
                    AddHeadingToQuery(headingId, parameters, query);
                    //specific origin, specific heading, all flavors
                    if (string.IsNullOrEmpty(flavor))
                    {
                        if (isInjection)
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndHeadingAndAll;
                        }
                        else
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndHeadingAndAll;
                        }
                    }
                    //specific origin, specific heading, specific flavor
                    else
                    {
                        AddFlavorToQuery(flavor, parameters, query);
                        if (isInjection)
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.InjectionAndHeadingAndFlavor;
                        }
                        else
                        {
                            type = NoProblem.Core.DataModel.Xrm.new_financialdashboarddailydata.Type.DistributionAndHeadingAndFlavor;
                        }
                    }
                }
            }
            parameters.Add("@type", (int)type);
            if (useOnlySoldHeadings)
            {
                query.Append(addGrouping);
            }
            string retVal = query.ToString();
            if (useOnlySoldHeadings)
            {
                if (originId == Guid.Empty)
                {
                    retVal = string.Format(retVal, string.Empty);
                }
                else
                {
                    retVal = string.Format(retVal, addOrigin);
                }
            }
            return retVal;
        }

        private void AddOriginToQuery(Guid originId, DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters, StringBuilder query)
        {
            parameters.Add("@originId", originId);
            query.Append(addOrigin);
        }

        private void AddHeadingToQuery(Guid headingId, DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters, StringBuilder query)
        {
            parameters.Add("@headingId", headingId);
            query.Append(addHeading);
        }

        private void AddFlavorToQuery(string flavor, DataAccessLayer.FinancialDashboardDailyData.SqlParametersList parameters, StringBuilder query)
        {
            parameters.Add("@flavor", flavor);
            query.Append(addFlavor);
        }

        private const string getDataQuery =
            @"
select 
new_date
, new_exposures
, new_requests
, new_upsales
, new_dau
, new_netactualrevenue
, new_netactualrevenueupsales
, new_potentialrevenue
, new_requestswithactualrevenue
, new_requestswithnetactualrevenue
, new_actualrevenue
, new_costofinstallation
, new_potentialcost
, new_actualcost
, new_netactualcost
, new_requestcountpotential
, new_requestcountactual
, new_requestcountnetactual
, new_requestcountfullfullfilment
, new_fullfullfilmentcost
, new_fullfullfilmentrevenue
, new_potentialcost2
, new_actualcost2
, new_netactualcost2
, new_fullfullfilmentcost2
, new_netactualcost2upsales
from new_financialdashboarddailydata with(nolock)
where new_date between @from and @to
and deletionstatecode = 0
and new_type = @type
";

        private const string getDataQueryOnlySold =
            @"
select 
new_date
,sum(new_exposures) new_exposures
,sum(new_requests) new_requests
,max(new_dau) new_dau
,sum(new_netactualrevenue) new_netactualrevenue
,sum(new_potentialrevenue) new_potentialrevenue
,sum(new_requestswithactualrevenue) new_requestswithactualrevenue
,sum(new_requestswithnetactualrevenue) new_requestswithnetactualrevenue
,sum(new_actualrevenue) new_actualrevenue
,sum(new_costofinstallation) new_costofinstallation
,sum(new_potentialcost) new_potentialcost
,sum(new_actualcost) new_actualcost
,sum(new_netactualcost) new_netactualcost
,sum(new_requestcountpotential) new_requestcountpotential
,sum(new_requestcountactual) new_requestcountactual
,sum(new_requestcountnetactual) new_requestcountnetactual
,sum(new_requestcountfullfullfilment) new_requestcountfullfullfilment
,sum(new_fullfullfilmentcost) new_fullfullfilmentcost
,sum(new_fullfullfilmentrevenue) new_fullfullfilmentrevenue
,sum(new_potentialcost2) new_potentialcost2
,sum(new_actualcost2) new_actualcost2
,sum(new_netactualcost2) new_netactualcost2
,sum(new_fullfullfilmentcost2) new_fullfullfilmentcost2
from new_financialdashboarddailydata with(nolock)
where new_date between @from and @to
and
 deletionstatecode = 0
and new_type = @type
--and new_actualrevenue is not null and new_actualrevenue > 0
and 
new_primaryexpertiseid
in
(
select distinct new_primaryexpertiseid
from new_financialdashboarddailydata
where
new_date between @from and @to
and
 deletionstatecode = 0
 and
new_actualrevenue is not null and new_actualrevenue > 0
and new_type = @type
{0}
)
";
        private const string addGrouping = " group by new_date ";
        private const string addHeading = @" and new_primaryexpertiseid = @headingId ";
        private const string addOrigin = @" and new_originid = @originId ";
        private const string addFlavor = @" and new_flavor = @flavor ";
    }
}
