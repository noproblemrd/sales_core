﻿using System;
using Effect.Crm.Logs;
using System.Collections.Generic;
using Effect.Crm.Convertor;

namespace NoProblem.Core.BusinessLogic.Dashboard
{
    public class DashboardDataCreator : XrmUserBase
    {
        #region Private Fields

        private readonly DateTime earliestDate;
        private delegate void RecalculateDataDelegate();
        private delegate void CreateDataDelegate();

        #endregion

        #region Ctor
        public DashboardDataCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            earliestDate = DateTime.Now.AddDays(-10).Date;
        }
        #endregion

        #region Public Methods

        public void CreateData()
        {
            CreateDataDelegate asyncCreate = new CreateDataDelegate(CreateDataPrivate);
            asyncCreate.BeginInvoke(null, null);
        }

        public void RecalculateData()
        {
            RecalculateDataDelegate asyncRecalculate = new RecalculateDataDelegate(RecalculateAllDataPrivate);
            asyncRecalculate.BeginInvoke(null, null);
        }

        public void CreateDataAfterRefund(DateTime incidentAccountCreatedOn)
        {
            DateTime local = FixDateToLocal(incidentAccountCreatedOn);
            string dateString = CreateDateString(local);
            DataAccessLayer.DashboardData dal = new NoProblem.Core.DataAccessLayer.DashboardData(XrmDataContext);
            DataAccessLayer.SystemUser userDal = new NoProblem.Core.DataAccessLayer.SystemUser(XrmDataContext);
            DataModel.Xrm.systemuser user = userDal.GetCurrentUser();
            UpdateDashboardData(local, dateString, dal, user, local.Year, local.Month, local.Day, true);
        }

        #endregion

        #region Private Methods

        private void RecalculateAllDataPrivate()
        {
            LogUtils.MyHandle.WriteToLog(8, "Recalculating all data for dashboard started");
            try
            {
                DateTime currentDate = DateTime.Now;
                DataAccessLayer.DashboardData dal = new NoProblem.Core.DataAccessLayer.DashboardData(XrmDataContext);
                DataAccessLayer.SystemUser userDal = new NoProblem.Core.DataAccessLayer.SystemUser(XrmDataContext);
                DataModel.Xrm.systemuser user = userDal.GetCurrentUser();
                while (currentDate.Date >= earliestDate.Date)
                {
                    string dateString = CreateDateString(currentDate);
                    UpdateDashboardData(currentDate, dateString, dal, user, currentDate.Year, currentDate.Month, currentDate.Day, true);
                    currentDate = currentDate.AddDays(-1);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "RecalculateAllDataPrivate for dashboard failed with exception");
            }
            LogUtils.MyHandle.WriteToLog(8, "Recalculating all data for dashboard ended");
        }

        private void CreateDataPrivate()
        {
            LogUtils.MyHandle.WriteToLog(9, "Creating data for dashboard started");
            try
            {
                DateTime today = DateTime.Now;
                today = FixDateToLocal(today);
                DateTime yesterday = today.AddDays(-1);
                DateTime currentDate = today;
                DataAccessLayer.DashboardData dal = new NoProblem.Core.DataAccessLayer.DashboardData(XrmDataContext);
                DataAccessLayer.PredefinedPrice preDefinedDal = new NoProblem.Core.DataAccessLayer.PredefinedPrice(XrmDataContext);
                DataAccessLayer.SystemUser userDal = new NoProblem.Core.DataAccessLayer.SystemUser(XrmDataContext);
                DataModel.Xrm.systemuser user = userDal.GetCurrentUser();
                while (currentDate.Date >= earliestDate.Date)
                {
                    string dateString = CreateDateString(currentDate);
                    if (currentDate.Date == today.Date || dal.IsExistsInDB(dateString) == false)
                    {
                        UpdateDashboardData(currentDate, dateString, dal, user, currentDate.Year, currentDate.Month, currentDate.Day, false);
                    }
                    else
                    {
                        break;
                    }
                    currentDate = currentDate.AddDays(-1);
                }
                currentDate = today;
                while (currentDate.Date >= earliestDate.Date)
                {
                    string dateString = CreateDateString(currentDate);
                    if (currentDate.Date == today.Date || preDefinedDal.IsExistsInDB(dateString) == false)
                    {
                        UpdatePerDefinedPrices(currentDate, dateString, preDefinedDal, user);
                    }
                    else
                    {
                        break;
                    }
                    currentDate = currentDate.AddDays(-1);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "CreateDataPrivate for dashboard failed with exception");
            }
            LogUtils.MyHandle.WriteToLog(9, "Creating data for dashboard ended");
        }

        private string CreateDateString(DateTime currentDate)
        {
            int month = currentDate.Month;
            int day = currentDate.Day;
            int year = currentDate.Year;
            string dateString = month.ToString() + "/" + day.ToString() + "/" + year.ToString();
            return dateString;
        }

        private void UpdateDashboardData(DateTime currentDate, string dateString, DataAccessLayer.DashboardData dal, DataModel.Xrm.systemuser user, int year, int month, int day, bool isRecalculation)
        {
            DateTime fromDate = currentDate.Date;
            DateTime toDate = currentDate.Date;
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            DataModel.Xrm.new_dashboarddata data = dal.GetDashboardData(fromDate, toDate);
            if (dal.IsExistsInDB(dateString))
            {
                dal.UpdateDirectSql(data, dateString, isRecalculation);
            }
            else
            {
                data.new_date = dateString;
                dal.CreateDirectSql(data, user, year, month, day);
            }
        }

        private void UpdatePerDefinedPrices(DateTime currentDate, string dateString, DataAccessLayer.PredefinedPrice dal, DataModel.Xrm.systemuser user)
        {
            Dictionary<string, decimal> dic = dal.CalculatePreDefinedPrices();
            foreach (var item in dic)
            {
                int rowsAffected = dal.UpdatePreDefined(item.Key, item.Value, dateString);
                if (rowsAffected == 0)
                {
                    CreatePreDefined(item.Key, item.Value, dateString, currentDate, user, dal);
                }
            }
        }

        private void CreatePreDefined(string expertiseCode, decimal avgPreDefinedPrice, string dateString, DateTime currentDate, DataModel.Xrm.systemuser user, DataAccessLayer.PredefinedPrice dal)
        {
            Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity entity = ConvertorCRMParams.GetNewDynamicEntity("new_predefinedprice");
            ConvertorCRMParams.SetDynamicEntityPropertyValue(entity, "new_date",
                dateString, typeof(Effect.Crm.SDKProviders.CrmServiceSdk.StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(entity, "new_expertisecode",
                expertiseCode, typeof(Effect.Crm.SDKProviders.CrmServiceSdk.StringProperty));
            float f = Convert.ToSingle(avgPreDefinedPrice);
            ConvertorCRMParams.SetDynamicEntityPropertyValue(entity, "new_avgpredefinedprice",
                 ConvertorCRMParams.GetCrmFloat(f, false),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.CrmFloatProperty));
            dal.AddRecordToCRM(entity, user, currentDate);
        }

        #endregion
    }
}
