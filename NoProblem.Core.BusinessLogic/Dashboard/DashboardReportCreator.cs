﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Dashboard;

namespace NoProblem.Core.BusinessLogic.Dashboard
{
    public class DashboardReportCreator : XrmUserBase
    {
        #region Ctor
        public DashboardReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            done = false;
            currencySymbol = "";
        }
        #endregion

        #region Constants
        private const string doubleSpecifier = "{0:#,0.##}";
        private const string integerSpecifier = "{0:#,0.##}";
        private const string zeroPercent = "0%";
        private const string percent = "%";
        #endregion

        #region Fields

        private bool done;
        private string currencySymbol;

        #endregion

        #region Public Methods

        public DML.Dashboard.DashboardReportResponse DashboardReport(DML.Dashboard.DashboardReportRequest request)
        {
            currencySymbol = request.CurrencySymbol;
            DML.Dashboard.DashboardReportResponse response = new DML.Dashboard.DashboardReportResponse();
            DataAccessLayer.DashboardData dal = new NoProblem.Core.DataAccessLayer.DashboardData(XrmDataContext);
            List<DML.Dashboard.DayData> currentList = dal.GetRangeData(request.FromDate.Date, request.ToDate.Date);
            List<DML.Dashboard.DayData> pastList = null;
            List<DML.Dashboard.DayData> objectivesList = null;
            if (request.CompareToPast)
            {
                pastList = dal.GetRangeData(request.PastFromDate.Date, request.PastToDate.Date);
            }
            if (request.ShowObjectives)
            {
                DataAccessLayer.ObjectiveDayData oddDal = new NoProblem.Core.DataAccessLayer.ObjectiveDayData(XrmDataContext);
                objectivesList = oddDal.GetRangeData(request.FromDate.Date, request.ToDate.Date);
            }
            if (request.OnlyGraphs == false)
            {
                CreateTable(request, response, currentList, pastList, objectivesList);
            }

            List<DML.Dashboard.GraphNode> currentGraph = BuildGraph(request.Interval, request.ShowMe, currentList, request.FromDate, request.ToDate, false);
            response.CurrentGraph = currentGraph;

            if (request.CompareToPast)
            {
                List<DML.Dashboard.GraphNode> pastGraph = BuildGraph(request.Interval, request.ShowMe, pastList, request.PastFromDate, request.PastToDate, false);
                response.PastGraph = pastGraph;
            }

            if (request.ShowObjectives)
            {
                List<DML.Dashboard.GraphNode> objectiveGraph = BuildGraph(request.Interval, request.ShowMe, objectivesList, request.FromDate, request.ToDate, true);
                response.ObjectivesGraph = objectiveGraph;
            }

            return response;
        }

        #endregion

        #region Private Methods

        #region Graph Methods

        private List<DML.Dashboard.GraphNode> BuildGraph(DML.Dashboard.Interval interval, DML.Dashboard.Criteria showMe, List<DML.Dashboard.DayData> dataList, DateTime fromDate, DateTime toDate, bool isObjectives)
        {
            List<DML.Dashboard.GraphNode> graph = new List<NoProblem.Core.DataModel.Dashboard.GraphNode>();
            DateTime current = fromDate.Date;
            DateTime last = toDate.Date;
            DateTime next = fromDate.Date;
            var dataFirst = dataList.FirstOrDefault(x => x.CreatedOn.Date == current);
            DML.Dashboard.GraphNode nodeFirst = new NoProblem.Core.DataModel.Dashboard.GraphNode();
            if (dataFirst != null)
            {
                decimal yValue = GetY(showMe, dataFirst);
                nodeFirst.Y = yValue;
            }
            else
            {
                nodeFirst.Y = 0;
            }
            nodeFirst.X = current.ToShortDateString();
            graph.Add(nodeFirst);
            done = false;
            if (last > current)
            {
                while (!done)
                {
                    GetNextDate(ref current, ref next, last, interval);
                    var data = dataList.Where(x => x.CreatedOn.Date >= current && x.CreatedOn.Date <= next);
                    DML.Dashboard.GraphNode node = new NoProblem.Core.DataModel.Dashboard.GraphNode();
                    decimal yValue = 0;
                    if (data.Count() > 0)
                    {
                        yValue = GetY(showMe, data, isObjectives);
                    }
                    node.Y = yValue;
                    string xStr =
                        (interval == Interval.Days ?
                        current.ToShortDateString() :
                        current.ToShortDateString() + "-\n" + next.ToShortDateString());
                    node.X = xStr;
                    graph.Add(node);
                }
            }
            return graph;
        }

        private decimal GetY(NoProblem.Core.DataModel.Dashboard.Criteria showMe, IEnumerable<NoProblem.Core.DataModel.Dashboard.DayData> data, bool isObjectives)
        {
            switch (showMe)
            {
                case Criteria.ConversionRateSrNoUpsales:
                    if (isObjectives)
                    {
                        return data.Average(x => x.ConversionRateSrNoUpsales);
                    }
                    decimal sumExpoWidgetNU = data.Sum(x => x.ExposuresWidget);
                    return sumExpoWidgetNU == 0 ? 0 : (data.Sum(x => x.RequestsWidgetNoUpsales) / sumExpoWidgetNU * 100);
                case Criteria.NumberOfAttemptsAar:
                    return data.Sum(x => x.NumberOfAttemptsAar);
                case Criteria.RejectedCalls:
                    return data.Sum(x => x.RejectedCalls);
                case Criteria.AutoUpsaleCallbacks:
                    return data.Sum(x => x.AutoUpsaleCallbacks);
                case Criteria.AutoUpsaleCallbacksConfirmed:
                    return data.Sum(x => x.AutoUpsaleCallbacksConfirmed);
                case Criteria.PayingAdvertisers:
                    return data.Average(x => x.PayingAdvertisers);
                case Criteria.AmountOfAdvertisers:
                    return data.Average(x => x.AmountOfAdvertisers);
                case Criteria.AmountOfCalls:
                    return data.Sum(x => x.AmountOfCalls);
                case Criteria.AmountOfCallsRealMoney:
                    return data.Sum(x => x.AmountOfCallsRealMoney);
                case Criteria.AmountOfDeposits:
                    return data.Sum(x => x.AmountOfDeposits);
                case Criteria.AmountOfRecurringDeposits:
                    return data.Sum(x => x.AmountOfRecurringDeposits);
                case Criteria.AmountOfRefundedCalls:
                    return data.Sum(x => x.AmountOfRefundedCalls);
                case Criteria.AmountOfRequests:
                    return data.Sum(x => x.AmountOfRequests);
                case Criteria.AverageDeposits:
                    if (isObjectives)
                    {
                        return data.Average(x => x.AverageDeposits);
                    }
                    decimal sumDeposits = data.Sum(x => x.AmountOfDeposits);
                    return sumDeposits == 0 ? 0 : (data.Sum(x => x.RevenueFromDeposits) / sumDeposits);
                case Criteria.AveragePreDefinedPrice:
                    return data.Average(x => x.AveragePreDefinedPrice);
                case Criteria.AveragePricePerCall:
                    if (isObjectives)
                    {
                        return data.Average(x => x.AveragePricePerCall);
                    }
                    decimal sumCalls = data.Sum(x => x.AmountOfCalls);
                    return sumCalls == 0 ? 0 : (data.Sum(x => x.RevenueFromCalls) / sumCalls);
                case Criteria.AvgPricePerCallRealMoney:
                    if (isObjectives)
                    {
                        return data.Average(x => x.AvgPricePerCallRealMoney);
                    }
                    decimal sumCallsRealMoney = data.Sum(x => x.AmountOfCallsRealMoney);
                    return sumCallsRealMoney == 0 ? 0 : (data.Sum(x => x.RevenueCallsRealMoney) / sumCallsRealMoney);
                case Criteria.AverageRecurringDeposits:
                    if (isObjectives)
                    {
                        return data.Average(x => x.AverageRecurringDeposits);
                    }
                    decimal sumRecDeposits = data.Sum(x => x.AmountOfRecurringDeposits);
                    return sumRecDeposits == 0 ? 0 : (data.Sum(x => x.RevenueFromRecurringDeposits) / sumRecDeposits);
                case Criteria.BalanceSum:
                    return data.Average(x => x.BalanceSum);
                case Criteria.ConversionRateDN:
                    if (isObjectives)
                    {
                        return data.Average(x => x.ConversionRateDN);
                    }
                    decimal sumExpoList = data.Sum(x => x.ExposuresListing);
                    return sumExpoList == 0 ? 0 : (data.Sum(x => x.RequestsListing) / sumExpoList * 100);
                case Criteria.ConversionRateSR:
                    if (isObjectives)
                    {
                        return data.Average(x => x.ConversionRateSR);
                    }
                    decimal sumExpoWidget = data.Sum(x => x.ExposuresWidget);
                    return sumExpoWidget == 0 ? 0 : (data.Sum(x => x.RequestsWidget) / sumExpoWidget * 100);
                case Criteria.MoneyRefunded:
                    return data.Sum(x => x.MoneyRefunded);
                case Criteria.OutOfMoneyCount:
                    return data.Average(x => x.OutOfMoneyCount);
                case Criteria.PercentageOfRefunds:
                    if (isObjectives)
                    {
                        return data.Average(x => x.PercentageOfRefunds);
                    }
                    decimal sumWonCalls = data.Sum(x => x.AmountOfWonCalls);
                    return sumWonCalls == 0 ? 0 : (data.Sum(x => x.AmountOfRefundedCalls) / sumWonCalls * 100);
                case Criteria.RevenueFromCalls:
                    return data.Sum(x => x.RevenueFromCalls);
                case Criteria.RevenueCallsRealMoney:
                    return data.Sum(x => x.RevenueCallsRealMoney);
                case Criteria.RevenueFromDeposits:
                    return data.Sum(x => x.RevenueFromDeposits);
                case Criteria.RevenueFromRecurringDeposits:
                    return data.Sum(x => x.RevenueFromRecurringDeposits);
                case Criteria.VirtualMoneyDeposits:
                    return data.Sum(x => x.VirtualMoneyDeposits);
                case Criteria.VirtualMoneyRecurringDeposits:
                    return data.Sum(x => x.VirtualMoneyRecurringDeposits);
                case Criteria.SoldCallsPerRequets:
                    if (isObjectives)
                    {
                        return data.Average(x => x.SoldCallsPerRequets);
                    }
                    decimal sumRequests = data.Sum(x => x.AmountOfRequests);
                    return sumRequests == 0 ? 0 : (data.Sum(x => x.AmountOfCalls) / sumRequests);
                case Criteria.AvailableAdvertisersCount:
                    return data.Average(x => x.AvailableAdvertisersCount);
                case Criteria.CandidatesCount:
                    return data.Average(x => x.CandidatesCount);
                case Criteria.CustomerServiceUnavailableCount:
                    return data.Average(x => x.CustomerServiceUnavailableCount);
                case Criteria.TemporaryUnavailableCount:
                    return data.Average(x => x.TemporaryUnavailableCount);
                case Criteria.AmountOfAccessibles:
                    return data.Average(x => x.AmountOfAccessibles);
                case Criteria.AmountOfDoNotCallList:
                    return data.Average(x => x.AmountOfDoNotCallList);
                case Criteria.AmountOfExpired:
                    return data.Average(x => x.AmountOfExpired);
                case Criteria.AmountOfFreshlyImported:
                    return data.Average(x => x.AmountOfFreshlyImported);
                case Criteria.AmountOfInRegistration:
                    return data.Average(x => x.AmountOfInRegistration);
                case Criteria.AmountOfITCs:
                    return data.Average(x => x.AmountOfITCs);
                case Criteria.AmountOfMachines:
                    return data.Average(x => x.AmountOfMachines);
                case Criteria.AmountOfRemoveMe:
                    return data.Average(x => x.AmountOfRemoveMe);
                case Criteria.AmountOfWebRegistrants:
                    return data.Average(x => x.AmountOfWebRegistrants);
                case Criteria.RerouteCallsCount:
                    return data.Sum(x => x.RerouteCallsCount);
                case Criteria.RerouteCallsUnansweredCount:
                    return data.Sum(x => x.RerouteCallsUnansweredCount);
                case Criteria.RerouteCallsReroutedCount:
                    return data.Sum(x => x.RerouteCallsReroutedCount);
                case Criteria.RerouteCallsUnansweredPercent:
                    if (isObjectives)
                    {
                        return data.Average(x => x.RerouteCallsUnansweredPercent);
                    }
                    decimal sumRerouteCalls = data.Sum(x => x.RerouteCallsCount);
                    return sumRerouteCalls == 0 ? 0 : (data.Sum(x => x.RerouteCallsUnansweredCount) / sumRerouteCalls * 100);
                case Criteria.RerouteCallsReroutedPercent:
                    if (isObjectives)
                    {
                        return data.Average(x => x.RerouteCallsReroutedPercent);
                    }
                    decimal sumRerouteUnansweredCalls = data.Sum(x => x.RerouteCallsUnansweredCount);
                    return sumRerouteUnansweredCalls == 0 ? 0 : (data.Sum(x => x.RerouteCallsReroutedCount) / sumRerouteUnansweredCalls * 100);
            }
            return 0;
        }

        private DateTime GetNextDate(ref DateTime current, ref DateTime next, DateTime last, DML.Dashboard.Interval interval)
        {
            DateTime tmp = next;
            switch (interval)
            {
                case NoProblem.Core.DataModel.Dashboard.Interval.Days:
                    next = next.AddDays(1);
                    break;
                case NoProblem.Core.DataModel.Dashboard.Interval.Weeks:
                    next = next.AddDays(7);
                    break;
                case NoProblem.Core.DataModel.Dashboard.Interval.Months:
                    next = next.AddMonths(1);
                    break;
                case NoProblem.Core.DataModel.Dashboard.Interval.Quarters:
                    next = next.AddMonths(3);
                    break;
                case NoProblem.Core.DataModel.Dashboard.Interval.Years:
                    next = next.AddYears(1);
                    break;
            }
            if (next >= last)
            {
                next = last;
                done = true;
            }
            current = tmp.AddDays(1);
            return next;
        }

        private decimal GetY(DML.Dashboard.Criteria showMe, DML.Dashboard.DayData data)
        {
            decimal retVal = 0;
            PropertyInfo property = typeof(DML.Dashboard.DayData).GetProperty(showMe.ToString());
            object valueObj = property.GetValue(data, null);
            if (valueObj != null)
            {
                retVal = (decimal)valueObj;
            }
            return retVal;
        }

        #endregion

        #region Table Methods

        private void CreateTable(DML.Dashboard.DashboardReportRequest request, DML.Dashboard.DashboardReportResponse response, List<DML.Dashboard.DayData> currentList, List<DML.Dashboard.DayData> pastList, List<DML.Dashboard.DayData> objectivesList)
        {
            var tableRows = new List<NoProblem.Core.DataModel.Dashboard.TableData>();
            AddWeightedAverageRows(request, currentList, pastList, objectivesList, tableRows,
                DML.Dashboard.Criteria.AveragePricePerCall, DML.Dashboard.Criteria.RevenueFromCalls, DML.Dashboard.Criteria.AmountOfCalls, false, true, false);
            AddWeightedAverageRows(request, currentList, pastList, objectivesList, tableRows,
                Criteria.AvgPricePerCallRealMoney, Criteria.RevenueCallsRealMoney, Criteria.AmountOfCallsRealMoney, false, true, false);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, DML.Dashboard.Criteria.AveragePreDefinedPrice, doubleSpecifier);
            var preDef = tableRows.Last();
            AddCurrencySymbol(preDef);
            AddWeightedAverageRows(request, currentList, pastList, objectivesList, tableRows,
                DML.Dashboard.Criteria.SoldCallsPerRequets, DML.Dashboard.Criteria.AmountOfCalls, DML.Dashboard.Criteria.AmountOfRequests, true, false, false);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, DML.Dashboard.Criteria.AmountOfRequests, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, DML.Dashboard.Criteria.AmountOfAdvertisers, integerSpecifier);
            AddWeightedAverageRows(request, currentList, pastList, objectivesList, tableRows,
                DML.Dashboard.Criteria.AverageDeposits, DML.Dashboard.Criteria.RevenueFromDeposits, DML.Dashboard.Criteria.AmountOfDeposits, false, true, false);
            AddWeightedAverageRows(request, currentList, pastList, objectivesList, tableRows,
                DML.Dashboard.Criteria.AverageRecurringDeposits, DML.Dashboard.Criteria.RevenueFromRecurringDeposits, DML.Dashboard.Criteria.AmountOfRecurringDeposits, false, true, false);
            AddAverageRow(request, currentList, pastList, objectivesList, tableRows, DML.Dashboard.Criteria.ConversionRateSR, doubleSpecifier);
            var sr = tableRows.Last();
            AddPercentageSymbol(sr);
            AddAverageRow(request, currentList, pastList, objectivesList, tableRows, DML.Dashboard.Criteria.ConversionRateDN, doubleSpecifier);
            var dn = tableRows.Last();
            AddPercentageSymbol(dn);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, NoProblem.Core.DataModel.Dashboard.Criteria.AmountOfRefundedCalls, integerSpecifier);
            AddPercentageOfRefundsRow(request, currentList, pastList, objectivesList, tableRows);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, NoProblem.Core.DataModel.Dashboard.Criteria.MoneyRefunded, doubleSpecifier);
            var moneyRefundedRow = tableRows.Last();
            AddCurrencySymbol(moneyRefundedRow);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, NoProblem.Core.DataModel.Dashboard.Criteria.BalanceSum, doubleSpecifier);
            var balanceRow = tableRows.Last();
            AddCurrencySymbol(balanceRow);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, NoProblem.Core.DataModel.Dashboard.Criteria.OutOfMoneyCount, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, NoProblem.Core.DataModel.Dashboard.Criteria.AvailableAdvertisersCount, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, NoProblem.Core.DataModel.Dashboard.Criteria.CandidatesCount, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, NoProblem.Core.DataModel.Dashboard.Criteria.CustomerServiceUnavailableCount, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, NoProblem.Core.DataModel.Dashboard.Criteria.TemporaryUnavailableCount, integerSpecifier);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, Criteria.VirtualMoneyDeposits, doubleSpecifier);
            var virtualSum = tableRows.Last();
            AddCurrencySymbol(virtualSum);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, Criteria.VirtualMoneyRecurringDeposits, doubleSpecifier);
            var virtualRecurringSum = tableRows.Last();
            AddCurrencySymbol(virtualRecurringSum);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AmountOfAccessibles, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AmountOfDoNotCallList, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AmountOfExpired, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AmountOfFreshlyImported, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AmountOfInRegistration, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AmountOfITCs, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AmountOfMachines, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AmountOfRemoveMe, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AmountOfWebRegistrants, integerSpecifier);
            AddLastValueRow(request, currentList, pastList, objectivesList, tableRows, Criteria.PayingAdvertisers, integerSpecifier);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, Criteria.RerouteCallsCount, integerSpecifier);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, Criteria.RerouteCallsUnansweredCount, integerSpecifier);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, Criteria.RerouteCallsReroutedCount, integerSpecifier);
            AddWeightedAverageRows(request, currentList, pastList, objectivesList, tableRows, Criteria.RerouteCallsUnansweredPercent,
                Criteria.RerouteCallsUnansweredCount, Criteria.RerouteCallsCount, true, false, true);
            var runap = tableRows.Last();
            AddPercentageSymbol(runap);
            AddWeightedAverageRows(request, currentList, pastList, objectivesList, tableRows, Criteria.RerouteCallsReroutedPercent,
                Criteria.RerouteCallsReroutedCount, Criteria.RerouteCallsUnansweredCount, true, false, true);
            var rrp = tableRows.Last();
            AddPercentageSymbol(rrp);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AutoUpsaleCallbacksConfirmed, integerSpecifier);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, Criteria.AutoUpsaleCallbacks, integerSpecifier);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, Criteria.RejectedCalls, integerSpecifier);
            AddSumRow(request, currentList, pastList, objectivesList, tableRows, Criteria.NumberOfAttemptsAar, integerSpecifier);
            AddAverageRow(request, currentList, pastList, objectivesList, tableRows, Criteria.ConversionRateSrNoUpsales, doubleSpecifier);
            var srnu = tableRows.Last();
            AddPercentageSymbol(srnu);
            //order and group rows:
            List<DataModel.Dashboard.TableData> lst = OrderAndGroupRows(tableRows);
            response.TableRows = lst;
        }

        private static void AddPercentageSymbol(NoProblem.Core.DataModel.Dashboard.TableData row)
        {
            row.Total = row.Total + percent;
            if (string.IsNullOrEmpty(row.PastPeriod) == false)
                row.PastPeriod = row.PastPeriod + percent;
            if (string.IsNullOrEmpty(row.Objective) == false)
                row.Objective = row.Objective + percent;
        }

        private void AddCurrencySymbol(NoProblem.Core.DataModel.Dashboard.TableData row)
        {
            row.Total = currencySymbol + row.Total;
            if (string.IsNullOrEmpty(row.PastPeriod) == false)
                row.PastPeriod = currencySymbol + row.PastPeriod;
            if (string.IsNullOrEmpty(row.Objective) == false)
                row.Objective = currencySymbol + row.Objective;
        }

        private List<DataModel.Dashboard.TableData> OrderAndGroupRows(List<NoProblem.Core.DataModel.Dashboard.TableData> tableRows)
        {
            List<DataModel.Dashboard.TableData> lst = new List<NoProblem.Core.DataModel.Dashboard.TableData>();
            AddToOrderedList(DML.Dashboard.Criteria.AmountOfCalls, 1, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.RevenueFromCalls, 1, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.AveragePricePerCall, 1, tableRows, lst);
            AddToOrderedList(Criteria.AmountOfCallsRealMoney, 1, tableRows, lst);
            AddToOrderedList(Criteria.RevenueCallsRealMoney, 1, tableRows, lst);
            AddToOrderedList(Criteria.AvgPricePerCallRealMoney, 1, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.AveragePreDefinedPrice, 1, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.AmountOfRequests, 1, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.SoldCallsPerRequets, 1, tableRows, lst);
            AddToOrderedList(Criteria.RejectedCalls, 1, tableRows, lst);
            AddToOrderedList(Criteria.AutoUpsaleCallbacks, 1, tableRows, lst);
            AddToOrderedList(Criteria.AutoUpsaleCallbacksConfirmed, 1, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.AmountOfDeposits, 2, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.AmountOfRecurringDeposits, 2, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.RevenueFromDeposits, 2, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.RevenueFromRecurringDeposits, 2, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.AverageDeposits, 2, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.AverageRecurringDeposits, 2, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.VirtualMoneyDeposits, 2, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.VirtualMoneyRecurringDeposits, 2, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.BalanceSum, 2, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.AmountOfAdvertisers, 3, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.AvailableAdvertisersCount, 3, tableRows, lst);
            AddToOrderedList(Criteria.AmountOfWebRegistrants, 3, tableRows, lst);
            AddToOrderedList(Criteria.PayingAdvertisers, 3, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.OutOfMoneyCount, 3, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.CustomerServiceUnavailableCount, 3, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.TemporaryUnavailableCount, 3, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.CandidatesCount, 3, tableRows, lst);
            AddToOrderedList(Criteria.AmountOfAccessibles, 6, tableRows, lst);
            AddToOrderedList(Criteria.AmountOfITCs, 6, tableRows, lst);
            AddToOrderedList(Criteria.AmountOfExpired, 6, tableRows, lst);
            AddToOrderedList(Criteria.AmountOfInRegistration, 6, tableRows, lst);
            AddToOrderedList(Criteria.NumberOfAttemptsAar, 6, tableRows, lst);
            AddToOrderedList(Criteria.AmountOfRemoveMe, 6, tableRows, lst);
            //AddToOrderedList(Criteria.AmountOfFreshlyImported, 6, tableRows, lst);
            //AddToOrderedList(Criteria.AmountOfMachines, 6, tableRows, lst);
            //AddToOrderedList(Criteria.AmountOfDoNotCallList, 6, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.ConversionRateDN, 4, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.ConversionRateSR, 4, tableRows, lst);
            AddToOrderedList(Criteria.ConversionRateSrNoUpsales, 4, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.AmountOfRefundedCalls, 5, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.PercentageOfRefunds, 5, tableRows, lst);
            AddToOrderedList(DML.Dashboard.Criteria.MoneyRefunded, 5, tableRows, lst);
            AddToOrderedList(Criteria.RerouteCallsCount, 7, tableRows, lst);
            AddToOrderedList(Criteria.RerouteCallsUnansweredCount, 7, tableRows, lst);
            AddToOrderedList(Criteria.RerouteCallsReroutedCount, 7, tableRows, lst);
            AddToOrderedList(Criteria.RerouteCallsUnansweredPercent, 7, tableRows, lst);
            AddToOrderedList(Criteria.RerouteCallsReroutedPercent, 7, tableRows, lst);
            return lst;
        }

        private void AddToOrderedList(DML.Dashboard.Criteria criteria, int group, List<DML.Dashboard.TableData> tableRows, List<DML.Dashboard.TableData> orderedList)
        {
            var row = tableRows.First(x => x.Row == criteria);
            row.Group = group;
            if (group == 5/*refunds*/ || criteria == Criteria.OutOfMoneyCount)
            {
                row.GapColor = ReverseColor(row.GapColor);
                row.PercentageColor = ReverseColor(row.PercentageColor);
            }
            orderedList.Add(row);
        }

        private Color ReverseColor(Color color)
        {
            Color retVal = Color.Red;
            if (color == Color.Red)
            {
                retVal = Color.Green;
            }
            return retVal;
        }

        private void AddPercentageOfRefundsRow(DashboardReportRequest request, List<DayData> currentList, List<DayData> pastList, List<DayData> objectivesList, List<TableData> tableRows)
        {
            DML.Dashboard.TableData row1 = new NoProblem.Core.DataModel.Dashboard.TableData();
            row1.Row = Criteria.PercentageOfRefunds;
            decimal numeratorSum = currentList.Sum(x => x.AmountOfRefundedCalls);
            decimal denominatorSum = currentList.Sum(x => x.AmountOfCalls) + numeratorSum;
            decimal current = denominatorSum == 0 ? 0 : numeratorSum / denominatorSum * 100;
            row1.Total = string.Format(doubleSpecifier, current);
            if (request.CompareToPast)
            {
                decimal numeratorPast = pastList.Sum(x => x.AmountOfRefundedCalls);
                decimal denominatorPast = pastList.Sum(x => x.AmountOfCalls) + numeratorPast;
                decimal past = denominatorPast == 0 ? 0 : numeratorPast / denominatorPast * 100;
                row1.PastPeriod = string.Format(doubleSpecifier, past);
                decimal gap = 0;
                if (past != 0)
                {
                    gap = ((-1 * (past - current)) * 100 / past);
                    row1.Gap = string.Format(doubleSpecifier, gap) + percent;
                }
                else
                {
                    row1.Gap = zeroPercent;
                }
                row1.GapColor = GetGapColor(gap);
            }
            if (request.ShowObjectives)
            {
                decimal objective = 0;
                if (objectivesList.Count() > 0)
                {
                    objective = objectivesList.Average(x => x.PercentageOfRefunds);
                }
                row1.Objective = string.Format(doubleSpecifier, objective);
                decimal perc = 0;
                if (objective != 0)
                {
                    perc = (current * 100 / objective);
                    row1.Percentage = string.Format(doubleSpecifier, perc) + percent;
                }
                else
                {
                    row1.Percentage = zeroPercent;
                }
                row1.PercentageColor = GetPercentColor(perc);
            }
            AddPercentageSymbol(row1);
            tableRows.Add(row1);
        }

        private void AddSumRow(NoProblem.Core.DataModel.Dashboard.DashboardReportRequest request, List<NoProblem.Core.DataModel.Dashboard.DayData> currentList, List<NoProblem.Core.DataModel.Dashboard.DayData> pastList, List<NoProblem.Core.DataModel.Dashboard.DayData> objectivesList, List<NoProblem.Core.DataModel.Dashboard.TableData> tableRows, NoProblem.Core.DataModel.Dashboard.Criteria rowName, string specifier)
        {
            System.Reflection.PropertyInfo property = typeof(DML.Dashboard.DayData).GetProperty(rowName.ToString());
            DML.Dashboard.TableData row1 = new NoProblem.Core.DataModel.Dashboard.TableData();
            row1.Row = rowName;
            decimal current = 0;
            if (currentList.Count() > 0)
            {
                current = currentList.Sum(x => (decimal)property.GetValue(x, null));
            }
            row1.Total = string.Format(specifier, current);
            if (request.CompareToPast)
            {
                decimal past = 0;
                if (pastList.Count() > 0)
                {
                    past = pastList.Sum(x => (decimal)property.GetValue(x, null));
                }
                row1.PastPeriod = string.Format(specifier, past);
                decimal gap = 0;
                if (past != 0)
                {
                    gap = ((-1 * (past - current)) * 100 / past);
                    row1.Gap = string.Format(specifier, gap) + percent;
                }
                else
                {
                    row1.Gap = zeroPercent;
                }
                row1.GapColor = GetGapColor(gap);
            }
            if (request.ShowObjectives)
            {
                decimal objective = 0;
                if (objectivesList.Count() > 0)
                {
                    objective = objectivesList.Sum(x => (decimal)property.GetValue(x, null));
                }
                row1.Objective = string.Format(specifier, objective);
                decimal percentage = 0;
                if (objective != 0)
                {
                    percentage = (current * 100 / objective);
                    row1.Percentage = string.Format(specifier, percentage) + percent;
                }
                else
                {
                    row1.Percentage = zeroPercent;
                }
                row1.PercentageColor = GetPercentColor(percentage);
            }
            tableRows.Add(row1);
        }

        private void AddLastValueRow(DML.Dashboard.DashboardReportRequest request, List<DML.Dashboard.DayData> currentList, List<DML.Dashboard.DayData> pastList, List<DML.Dashboard.DayData> objectivesList, List<NoProblem.Core.DataModel.Dashboard.TableData> tableRows, NoProblem.Core.DataModel.Dashboard.Criteria rowName, string specifier)
        {
            System.Reflection.PropertyInfo property = typeof(DML.Dashboard.DayData).GetProperty(rowName.ToString());
            DML.Dashboard.TableData row1 = new NoProblem.Core.DataModel.Dashboard.TableData();
            row1.Row = rowName;
            decimal current = 0;
            if (currentList.Count() > 0)
            {
                var currentOrdered = currentList.OrderBy(x => x.CreatedOn);
                current = (decimal)property.GetValue(currentOrdered.Last(), null);
            }
            row1.Total = string.Format(specifier, current);
            if (request.CompareToPast)
            {
                decimal past = 0;
                if (pastList.Count() > 0)
                {
                    var pastOrdered = pastList.OrderBy(x => x.CreatedOn);
                    past = (decimal)property.GetValue(pastOrdered.Last(), null);
                }
                row1.PastPeriod = string.Format(specifier, past);
                decimal gap = 0;
                if (past != 0)
                {
                    gap = ((-1 * (past - current)) * 100 / past);
                    row1.Gap = string.Format(specifier, gap) + percent;
                }
                else
                {
                    row1.Gap = zeroPercent;
                }
                row1.GapColor = GetGapColor(gap);
            }
            if (request.ShowObjectives)
            {
                decimal objective = 0;
                if (objectivesList.Count() > 0)
                {
                    var objectivesOrdered = objectivesList.OrderBy(x => x.CreatedOn);
                    objective = (decimal)property.GetValue(objectivesOrdered.Last(), null);
                }
                row1.Objective = string.Format(specifier, objective);
                decimal percentage = 0;
                if (objective != 0)
                {
                    percentage = (current * 100 / objective);
                    row1.Percentage = string.Format(specifier, percentage) + percent;
                }
                else
                {
                    row1.Percentage = zeroPercent;
                }
                row1.PercentageColor = GetPercentColor(percentage);
            }
            tableRows.Add(row1);
        }

        private void AddAverageRow(DML.Dashboard.DashboardReportRequest request, List<DML.Dashboard.DayData> currentList, List<DML.Dashboard.DayData> pastList, List<DML.Dashboard.DayData> objectivesList, List<NoProblem.Core.DataModel.Dashboard.TableData> tableRows, NoProblem.Core.DataModel.Dashboard.Criteria rowName, string specifier)
        {
            System.Reflection.PropertyInfo property = typeof(DML.Dashboard.DayData).GetProperty(rowName.ToString());
            DML.Dashboard.TableData row1 = new NoProblem.Core.DataModel.Dashboard.TableData();
            row1.Row = rowName;
            decimal current = 0;
            if (currentList.Count() > 0)
            {
                current = currentList.Average(x => (decimal)property.GetValue(x, null));
            }
            row1.Total = string.Format(specifier, current);
            if (request.CompareToPast)
            {
                decimal past = 0;
                if (pastList.Count() > 0)
                {
                    past = pastList.Average(x => (decimal)property.GetValue(x, null));
                }
                row1.PastPeriod = string.Format(specifier, past);
                decimal gap = 0;
                if (past != 0)
                {
                    gap = ((-1 * (past - current)) * 100 / past);
                    row1.Gap = string.Format(specifier, gap) + percent;
                }
                else
                {
                    row1.Gap = zeroPercent;
                }
                row1.GapColor = GetGapColor(gap);
            }
            if (request.ShowObjectives)
            {
                decimal objective = 0;
                if (objectivesList.Count() > 0)
                {
                    objective = objectivesList.Average(x => (decimal)property.GetValue(x, null));
                }
                row1.Objective = string.Format(specifier, objective);
                decimal percentage = 0;
                if (objective != 0)
                {
                    percentage = (current * 100 / objective);
                    row1.Percentage = string.Format(specifier, percentage) + percent;
                }
                else
                {
                    row1.Percentage = zeroPercent;
                }
                row1.PercentageColor = GetPercentColor(percentage);
            }
            tableRows.Add(row1);
        }

        private void AddWeightedAverageRows(DML.Dashboard.DashboardReportRequest request,
            List<DML.Dashboard.DayData> currentList, List<DML.Dashboard.DayData> pastList,
             List<DML.Dashboard.DayData> objectivesList,
            List<NoProblem.Core.DataModel.Dashboard.TableData> tableRows,
            DML.Dashboard.Criteria weightedRow,
            DML.Dashboard.Criteria numeratorRow,
            DML.Dashboard.Criteria denominatorRow,
            bool addOnlyWeightedRow,
            bool addCurrencySymbol,
            bool multiplyBy100)
        {
            System.Reflection.PropertyInfo property = typeof(DML.Dashboard.DayData).GetProperty(weightedRow.ToString());
            DML.Dashboard.TableData rowWeighted = new NoProblem.Core.DataModel.Dashboard.TableData();
            DML.Dashboard.TableData rowNumerator = new NoProblem.Core.DataModel.Dashboard.TableData();
            DML.Dashboard.TableData rowDenominator = new NoProblem.Core.DataModel.Dashboard.TableData();
            rowWeighted.Row = weightedRow;
            rowNumerator.Row = numeratorRow;
            rowDenominator.Row = denominatorRow;
            System.Reflection.PropertyInfo propertyNumerator = typeof(DML.Dashboard.DayData).GetProperty(numeratorRow.ToString());
            decimal numerator = currentList.Sum(x => (decimal)propertyNumerator.GetValue(x, null));
            rowNumerator.Total = currencySymbol + string.Format(doubleSpecifier, numerator);
            System.Reflection.PropertyInfo PropertyDenominator = typeof(DML.Dashboard.DayData).GetProperty(denominatorRow.ToString());
            decimal denominator = currentList.Sum(x => (decimal)PropertyDenominator.GetValue(x, null));
            rowDenominator.Total = string.Format(integerSpecifier, denominator);
            decimal current = denominator == 0 ? 0 : numerator / denominator;
            if(multiplyBy100)
                current = current * 100;
            if (addCurrencySymbol)
                rowWeighted.Total = currencySymbol + string.Format(doubleSpecifier, current);
            else
                rowWeighted.Total = string.Format(doubleSpecifier, current);
            if (request.CompareToPast)
            {
                decimal numeratorPast = pastList.Sum(x => (decimal)propertyNumerator.GetValue(x, null));
                decimal denominatorPast = pastList.Sum(x => (decimal)PropertyDenominator.GetValue(x, null));
                decimal past = denominatorPast == 0 ? 0 : numeratorPast / denominatorPast;
                if (multiplyBy100)
                    past = past * 100;
                rowNumerator.PastPeriod = currencySymbol + string.Format(doubleSpecifier, numeratorPast);
                rowDenominator.PastPeriod = string.Format(integerSpecifier, denominatorPast);
                if (addCurrencySymbol)
                    rowWeighted.PastPeriod = currencySymbol + string.Format(doubleSpecifier, past);
                else
                    rowWeighted.PastPeriod = string.Format(doubleSpecifier, past);
                decimal gap = 0;
                if (past != 0)
                {
                    gap = ((-1 * (past - current)) * 100 / past);
                    rowWeighted.Gap = string.Format(doubleSpecifier, gap) + percent;
                }
                else
                {
                    rowWeighted.Gap = zeroPercent;
                }
                rowWeighted.GapColor = GetGapColor(gap);
                decimal gapNumerator = 0;
                if (numeratorPast != 0)
                {
                    gapNumerator = ((-1 * (numeratorPast - numerator)) * 100 / numeratorPast);
                    rowNumerator.Gap = string.Format(doubleSpecifier, gapNumerator) + percent;
                }
                else
                {
                    rowNumerator.Gap = zeroPercent;
                }
                rowNumerator.GapColor = GetGapColor(gapNumerator);
                decimal gapDenominator = 0;
                if (denominatorPast != 0)
                {
                    gapDenominator = ((-1 * (denominatorPast - denominator)) * 100 / denominatorPast);
                    rowDenominator.Gap = string.Format(doubleSpecifier, gapDenominator) + percent;
                }
                else
                {
                    rowDenominator.Gap = zeroPercent;
                }
                rowDenominator.GapColor = GetGapColor(gapDenominator);
            }
            if (request.ShowObjectives)
            {
                decimal numeratorObj = 0;
                decimal denominatorObj = 0;
                decimal objective = 0;
                if (objectivesList.Count() > 0)
                {
                    numeratorObj = objectivesList.Sum(x => (decimal)propertyNumerator.GetValue(x, null));
                    denominatorObj = objectivesList.Sum(x => (decimal)PropertyDenominator.GetValue(x, null));
                    objective = objectivesList.Average(x => (decimal)property.GetValue(x, null));
                }
                rowNumerator.Objective = currencySymbol + string.Format(doubleSpecifier, numeratorObj);
                rowDenominator.Objective = string.Format(integerSpecifier, denominatorObj);
                if (addCurrencySymbol)
                    rowWeighted.Objective = currencySymbol + string.Format(doubleSpecifier, objective);
                else
                    rowWeighted.Objective = string.Format(doubleSpecifier, objective);
                decimal perc = 0;
                if (objective != 0)
                {
                    perc = (current * 100 / objective);
                    rowWeighted.Percentage = string.Format(doubleSpecifier, perc) + percent;
                }
                else
                {
                    rowWeighted.Percentage = zeroPercent;
                }
                rowWeighted.PercentageColor = GetPercentColor(perc);
                decimal percNumerator = 0;
                if (numeratorObj != 0)
                {
                    percNumerator = (numerator * 100 / numeratorObj);
                    rowNumerator.Percentage = string.Format(doubleSpecifier, percNumerator) + percent;
                }
                else
                {
                    rowNumerator.Percentage = zeroPercent;
                }
                rowNumerator.PercentageColor = GetPercentColor(percNumerator);
                decimal percDenominator = 0;
                if (denominatorObj != 0)
                {
                    percDenominator = (denominator * 100 / denominatorObj);
                    rowDenominator.Percentage = string.Format(doubleSpecifier, percDenominator) + percent;
                }
                else
                {
                    rowDenominator.Percentage = zeroPercent;
                }
                rowDenominator.PercentageColor = GetPercentColor(percDenominator);
            }
            if (!addOnlyWeightedRow)
            {
                tableRows.Add(rowDenominator);
                tableRows.Add(rowNumerator);
            }
            tableRows.Add(rowWeighted);
        }

        private NoProblem.Core.DataModel.Dashboard.Color GetPercentColor(decimal amount)
        {
            DML.Dashboard.Color retVal = NoProblem.Core.DataModel.Dashboard.Color.Red;
            if (amount >= 100)
            {
                retVal = NoProblem.Core.DataModel.Dashboard.Color.Green;
            }
            return retVal;
        }

        private DataModel.Dashboard.Color GetGapColor(decimal amount)
        {
            DML.Dashboard.Color retVal = NoProblem.Core.DataModel.Dashboard.Color.Red;
            if (amount >= 0)
            {
                retVal = NoProblem.Core.DataModel.Dashboard.Color.Green;
            }
            return retVal;
        }

        #endregion

        #endregion
    }
}
