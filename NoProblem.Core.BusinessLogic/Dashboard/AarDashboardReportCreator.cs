﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Dashboard;
using System.Reflection;
using NoProblem.Core.DataModel.Reports;

namespace NoProblem.Core.BusinessLogic.Dashboard
{
    public class AarDashboardReportCreator : XrmUserBase
    {
        #region Ctor
        public AarDashboardReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Constants
        private const string doubleSpecifier = "{0:0.##}";
        private const string percent = "%";
        #endregion

        #region Public Methods

        public DML.Dashboard.AarDashboardReportResponse AarDashboardReport(DML.Dashboard.AarDashboardReportRequest request)
        {
            DML.Dashboard.AarDashboardReportResponse response = new NoProblem.Core.DataModel.Dashboard.AarDashboardReportResponse();
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            Dictionary<DateTime, DML.Reports.AarRequestsReportRow> currentList = GetDataList(request.FromDate, request.ToDate, request.HeadingId, dal);
            List<GraphNode> currentGraph = BuildGraph(currentList, request.ShowMe);
            response.CurrentGraph = currentGraph;
            if (request.CompareToPast)
            {
                Dictionary<DateTime, DML.Reports.AarRequestsReportRow> pastList = GetDataList(request.PastFromDate, request.PastToDate, request.HeadingId, dal);
                List<GraphNode> pastGraph = BuildGraph(pastList, request.ShowMe);
                response.PastGraph = pastGraph;
            }
            if (!request.OnlyGraphs)
            {
                var currentReport = GetReport(request.FromDate, request.ToDate, request.HeadingId, dal);
                AarRequestsReportRow pastReport = null;
                if (request.CompareToPast)
                {
                    pastReport = GetReport(request.PastFromDate, request.PastToDate, request.HeadingId, dal);
                }
                response.TableRows = BuildTable(currentReport, pastReport);
            }
            return response;
        }

        #endregion

        #region Private Methods

        private AarRequestsReportRow GetReport(DateTime fromDate, DateTime toDate, Guid headingId, DataAccessLayer.Incident dal)
        {
            FixDatesToFullDays(ref fromDate, ref toDate);
            var report = dal.GetAarRequestsReport(fromDate, toDate, headingId);
            return report;
        }

        private List<AarTableData> BuildTable(AarRequestsReportRow currentReport, AarRequestsReportRow pastReport)
        {
            List<AarTableData> table = new List<AarTableData>();
            bool compareToPast = pastReport != null;
            var v = Enum.GetValues(typeof(AarCriteria));
            foreach (var item in v)
            {
                AarCriteria criteria = (AarCriteria)item;
                decimal value = GetCriteriaValue(criteria, currentReport);
                AarTableData data = new AarTableData();
                if (value == -1)
                {
                    data.Total = "-";
                }
                else
                {
                    data.Total = string.Format(doubleSpecifier, value);
                }
                data.Row = criteria;
                if (compareToPast)
                {
                    decimal pastValue = GetCriteriaValue(criteria, pastReport);
                    if (pastValue == -1)
                    {
                        data.PastPeriod = "-";
                    }
                    else
                    {
                        data.PastPeriod = string.Format(doubleSpecifier, pastValue);
                    }
                    if (pastValue != 0 && pastValue != -1 && value != -1)
                    {
                        decimal gap = ((-1 * (pastValue - value)) * 100 / pastValue);
                        data.Gap = string.Format(doubleSpecifier, gap) + percent;
                        data.GapColor = GetGapColor(gap);
                    }
                    else
                    {
                        data.Gap = "-";
                    }
                }                
                table.Add(data);
            }
            return table;
        }

        private DataModel.Dashboard.Color GetGapColor(decimal amount)
        {
            DML.Dashboard.Color retVal = NoProblem.Core.DataModel.Dashboard.Color.Red;
            if (amount >= 0)
            {
                retVal = NoProblem.Core.DataModel.Dashboard.Color.Green;
            }
            return retVal;
        }

        private Dictionary<DateTime, DML.Reports.AarRequestsReportRow> GetDataList(DateTime fromDate, DateTime toDate, Guid headingId, DataAccessLayer.Incident dal)
        {
            DateTime actualLastDate = FixDateToLocal(toDate.Date);
            DateTime currentFromDate = fromDate.Date;
            currentFromDate = FixDateToLocal(currentFromDate);
            DateTime currentToDate = currentFromDate.AddDays(1);
            Dictionary<DateTime, DML.Reports.AarRequestsReportRow> dataList = new Dictionary<DateTime, NoProblem.Core.DataModel.Reports.AarRequestsReportRow>();
            while (currentFromDate.Date <= actualLastDate.Date)
            {
                var reportAns = dal.GetAarRequestsReport(currentFromDate, currentToDate, headingId);
                dataList.Add(currentFromDate.Date, reportAns);
                currentFromDate = currentFromDate.AddDays(1);
                currentToDate = currentToDate.AddDays(1);
            }
            return dataList;
        }

        private List<GraphNode> BuildGraph(Dictionary<DateTime, DML.Reports.AarRequestsReportRow> dataList, AarCriteria showMe)
        {
            List<GraphNode> graph = new List<GraphNode>();
            foreach (var item in dataList)
            {
                GraphNode node = new GraphNode();
                node.X = item.Key.ToShortDateString();
                node.Y = GetCriteriaValue(showMe, item.Value);
                if (node.Y == -1)
                {
                    node.Y = 0;
                }
                graph.Add(node);
            }
            return graph;
        }

        private decimal GetCriteriaValue(DML.Dashboard.AarCriteria showMe, DML.Reports.AarRequestsReportRow data)
        {
            decimal retVal = 0;
            PropertyInfo property = typeof(DML.Reports.AarRequestsReportRow).GetProperty(showMe.ToString());
            object valueObj = property.GetValue(data, null);
            if (valueObj != null)
            {
                retVal = decimal.Parse(valueObj.ToString());
            }
            return retVal;
        }

        #endregion
    }
}
