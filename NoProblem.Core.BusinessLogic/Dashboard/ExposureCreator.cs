﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Dashboard;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Dashboard
{
    public class ExposureCreator : XrmUserBase
    {
        #region Ctors

        static ExposureCreator()
        {
            DataAccessLayer.SystemUser systemUserDal = new NoProblem.Core.DataAccessLayer.SystemUser(null);
            currentUser = systemUserDal.GetCurrentUser();
            syncObj = new object();
            holder = new SynchronizedCollection<ExposureCreator>();
            CreateExtensionTable();
            CreateBaseTable();
            CreateBaseMapping();
            CreateExtensionMapping();
        }

        public ExposureCreator(CreateExposureRequest request)
            : base(null)
        {
            thread = new Thread(new ThreadStart(Run));
            this.request = request;
        }

        public ExposureCreator() : base(null) { }

        #endregion

        #region Fields

        private static object syncObj;
        private static DataModel.Xrm.systemuser currentUser;
        private static System.Collections.Generic.SynchronizedCollection<ExposureCreator> holder;
        private static DataTable tableBase;
        private static DataTable tableExtension;
        private static List<SqlBulkCopyColumnMapping> baseMapping;
        private static List<SqlBulkCopyColumnMapping> extensionMapping;
        private Thread thread;
        private CreateExposureRequest request;
        private const string BASE_TABLE_NAME = "new_exposurebase";
        private const string EXTENSION_TABLE_NAME = "new_exposureextensionbase";

        #endregion

        #region Public Methods

        public void CreateExposure()
        {
            if (request != null)
            {
                thread.Start();
                holder.Add(this);
            }
        }

        public void Flush()
        {
            lock (syncObj)
            {
                if (tableBase.Rows.Count > 0)
                {
                    try
                    {
                        DataAccessLayer.Exposure dal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
                        dal.BulkCopy(BASE_TABLE_NAME, tableBase, baseMapping);
                        dal.BulkCopy(EXTENSION_TABLE_NAME, tableExtension, extensionMapping);
                        //tableBase.Clear();
                        //tableBase.AcceptChanges();
                        //tableExtension.Clear();
                        //tableExtension.AcceptChanges();
                    }
                    finally
                    {
                        tableBase.Dispose();
                        tableExtension.Dispose();
                        CreateBaseTable();
                        CreateExtensionTable();
                    }
                }
            }
        }


        public string GetCount()
        {
            lock (syncObj)
            {
                return "base = " + tableBase.Rows.Count + ". extension = " + tableExtension.Rows.Count + ".";
            }
        }

        #endregion

        #region Private Methods

        private void Run()
        {
            try
            {
                DataModel.Xrm.new_exposure exposure = CreateExposureObjectWithAllData();
                DataAccessLayer.Exposure dal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
                dal.Create(exposure);
                XrmDataContext.SaveChanges();
                //lock (syncObj)
                //{
                //    Guid id = Guid.NewGuid();
                //    DateTime createdOn = DateTime.Now;
                //    AddRowToBaseTable(id, createdOn);
                //    AddRowToExtensionTable(exposure, id);
                //    if (tableBase.Rows.Count >= 128)
                //    {
                //        Flush();
                //    }
                //}
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in async method ExposureCreator.Run");
            }
            finally
            {
                holder.Remove(this);
            }
        }

        private void AddRowToExtensionTable(DataModel.Xrm.new_exposure exposure, Guid id)
        {
            tableExtension.Rows.Add(
                exposure.new_controlname,
                exposure.new_date,
                exposure.new_domain,
                id,
                exposure.new_name,
                exposure.new_pagename,
                exposure.new_place,
                exposure.new_sessionid,
                exposure.new_siteid,
                exposure.new_type,
                exposure.new_url,
                exposure.new_originid,
                exposure.new_channelid,
                exposure.new_primaryexpertiseid,
                exposure.new_regionid,
                exposure.new_secondaryexpertiseid,
                exposure.new_advertisersreturned,
                exposure.new_websiteid,
                exposure.new_keyword,
                exposure.new_toolbaridid,
                exposure.new_ip,
                exposure.new_iswelcomescreenevent,
                exposure.new_userid,
                exposure.new_welcomescreeneventtype,
                exposure.new_sitetitle,
                exposure.new_browser,
                exposure.new_browserversion);
            tableExtension.AcceptChanges();
        }

        private void AddRowToBaseTable(Guid id, DateTime createdOn)
        {
            tableBase.Rows.Add(
                currentUser.systemuserid,
                createdOn,
                0,
                currentUser.systemuserid,
                createdOn,
                id,
                currentUser.businessunitid,
                0, 1,
                currentUser.systemuserid);
            tableBase.AcceptChanges();
        }

        private DataModel.Xrm.new_exposure CreateExposureObjectWithAllData()
        {
            DataModel.Xrm.new_exposure exposure = new NoProblem.Core.DataModel.Xrm.new_exposure();
            exposure.new_type = (int)request.Type;
            exposure.new_advertisersreturned = request.NumAdvertisersReturned;
            exposure.new_date = FixDateToLocal(DateTime.Now);
            exposure.new_place = request.PlaceInWebSite;
            exposure.new_pagename = request.PageName;
            exposure.new_siteid = request.SiteId;
            exposure.new_sessionid = request.SessionId;
            exposure.new_keyword = request.Keyword;
            exposure.new_browser = request.Browser;
            exposure.new_browserversion = request.BrowserVersion;
            if (request.Channel.HasValue)
            {
                exposure.new_channelid = FindChannelId(request.Channel.Value);
            }
            if (request.Url != null &&
                request.Url.Length > 999)
            {
                request.Url = request.Url.Substring(0, 998);
            }
            exposure.new_url = request.Url;
            if (!string.IsNullOrEmpty(request.SiteTitle)
                && request.SiteTitle.Length > 99)
            {
                request.SiteTitle = request.SiteTitle.Substring(0, 99);
            }
            exposure.new_sitetitle = request.SiteTitle;
            //if (string.IsNullOrEmpty(request.Domain) == false)
            //{
            //    exposure.new_domain = request.Domain;
            //    if (request.OriginId != Guid.Empty)
            //    {
            //        exposure.new_originid = request.OriginId;
            //    //    Origins.AffiliatesManager affiliatesManager = new Origins.AffiliatesManager(XrmDataContext);
            //    //    exposure.new_websiteid = affiliatesManager.GetCreateWebSiteId(request.Domain, request.OriginId);
            //    }
            //}
            exposure.new_domain = request.Domain;
            if (request.OriginId != Guid.Empty)
            {
                exposure.new_originid = request.OriginId;
                if (!string.IsNullOrEmpty(request.ToolbarId))
                {
                    Origins.AffiliatesManager affiliatesManager = new Origins.AffiliatesManager(XrmDataContext);
                    exposure.new_toolbaridid = affiliatesManager.GetCreateToolbarId(request.ToolbarId, request.OriginId);
                }
            }

            exposure.new_controlname = request.ControlName;
            if (string.IsNullOrEmpty(request.RegionCode) == false)
            {
                FindRegionId(exposure, request.RegionCode, request.RegionLevel);
            }
            if (string.IsNullOrEmpty(request.HeadingCode) == false)
            {
                FindHeadingId(exposure, request.HeadingCode, request.HeadingLevel);
            }
            if (request.IsWelcomeScreenEvent)
            {
                exposure.new_iswelcomescreenevent = true;
                switch (request.WelcomeScreenEventType)
                {
                    case CreateExposureRequest.WelcomScreenEventTypes.YES:
                        exposure.new_welcomescreeneventtype = (int)DataModel.Xrm.new_exposure.WelcomeScreenEventType.ClickedYes;
                        break;
                    case CreateExposureRequest.WelcomScreenEventTypes.NO:
                        exposure.new_welcomescreeneventtype = (int)DataModel.Xrm.new_exposure.WelcomeScreenEventType.ClickedNo;
                        break;
                    case CreateExposureRequest.WelcomScreenEventTypes.CLOSE_ENABLE:
                        exposure.new_welcomescreeneventtype = (int)DataModel.Xrm.new_exposure.WelcomeScreenEventType.ClickedCloseEnable;
                        break;
                    default:
                        exposure.new_welcomescreeneventtype = (int)DataModel.Xrm.new_exposure.WelcomeScreenEventType.Opened;
                        break;
                }
            }
            if (!string.IsNullOrEmpty(request.UserId))
            {
                exposure.new_userid = request.UserId;
            }
            if (!string.IsNullOrEmpty(request.IP))
            {
                exposure.new_ip = request.IP;
            }
            return exposure;
        }

        private static void CreateBaseTable()
        {
            tableBase = new DataTable("new_exposurebase");
            tableBase.Columns.Add("CreatedBy", typeof(Guid));
            tableBase.Columns.Add("CreatedOn", typeof(DateTime));
            tableBase.Columns.Add("DeletionStateCode", typeof(int));
            tableBase.Columns.Add("ModifiedBy", typeof(Guid));
            tableBase.Columns.Add("ModifiedOn", typeof(DateTime));
            tableBase.Columns.Add("New_exposureId", typeof(Guid));
            tableBase.Columns.Add("OwningBusinessUnit", typeof(Guid));
            tableBase.Columns.Add("statecode", typeof(int));
            tableBase.Columns.Add("statuscode", typeof(int));
            tableBase.Columns.Add("OwningUser", typeof(Guid));
        }

        private static void CreateBaseMapping()
        {
            baseMapping = new List<SqlBulkCopyColumnMapping>();
            baseMapping.Add(new SqlBulkCopyColumnMapping("CreatedBy", "CreatedBy"));
            baseMapping.Add(new SqlBulkCopyColumnMapping("CreatedOn", "CreatedOn"));
            baseMapping.Add(new SqlBulkCopyColumnMapping("DeletionStateCode", "DeletionStateCode"));
            baseMapping.Add(new SqlBulkCopyColumnMapping("ModifiedBy", "ModifiedBy"));
            baseMapping.Add(new SqlBulkCopyColumnMapping("ModifiedOn", "ModifiedOn"));
            baseMapping.Add(new SqlBulkCopyColumnMapping("New_exposureId", "New_exposureId"));
            baseMapping.Add(new SqlBulkCopyColumnMapping("OwningBusinessUnit", "OwningBusinessUnit"));
            baseMapping.Add(new SqlBulkCopyColumnMapping("statecode", "statecode"));
            baseMapping.Add(new SqlBulkCopyColumnMapping("statuscode", "statuscode"));
            baseMapping.Add(new SqlBulkCopyColumnMapping("OwningUser", "OwningUser"));
        }

        private static void CreateExtensionTable()
        {
            tableExtension = new DataTable("new_exposureextensionbase");
            tableExtension.Columns.Add("New_ControlName", typeof(string));
            tableExtension.Columns.Add("New_Date", typeof(DateTime));
            tableExtension.Columns.Add("New_Domain", typeof(string));
            tableExtension.Columns.Add("New_exposureId", typeof(Guid));
            tableExtension.Columns.Add("New_name", typeof(string));
            tableExtension.Columns.Add("New_PageName", typeof(string));
            tableExtension.Columns.Add("New_Place", typeof(string));
            tableExtension.Columns.Add("New_SessionId", typeof(string));
            tableExtension.Columns.Add("New_SiteId", typeof(string));
            tableExtension.Columns.Add("New_Type", typeof(int));
            tableExtension.Columns.Add("New_Url", typeof(string));
            tableExtension.Columns.Add("new_originid", typeof(Guid));
            tableExtension.Columns.Add("new_channelid", typeof(Guid));
            tableExtension.Columns.Add("new_primaryexpertiseid", typeof(Guid));
            tableExtension.Columns.Add("new_regionid", typeof(Guid));
            tableExtension.Columns.Add("new_secondaryexpertiseid", typeof(Guid));
            tableExtension.Columns.Add("New_AdvertisersReturned", typeof(int));
            tableExtension.Columns.Add("new_websiteid", typeof(Guid));
            tableExtension.Columns.Add("New_Keyword", typeof(string));
            tableExtension.Columns.Add("new_toolbaridid", typeof(Guid));
            tableExtension.Columns.Add("New_IP", typeof(string));
            tableExtension.Columns.Add("New_IsWelcomeScreenEvent", typeof(bool));
            tableExtension.Columns.Add("New_UserId", typeof(Guid));
            tableExtension.Columns.Add("New_WelcomeScreenEventType", typeof(int));
            tableExtension.Columns.Add("New_SiteTitle", typeof(string));
            tableExtension.Columns.Add("New_Browser", typeof(string));
            tableExtension.Columns.Add("New_BrowserVersion", typeof(string));
        }

        private static void CreateExtensionMapping()
        {
            extensionMapping = new List<SqlBulkCopyColumnMapping>();
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_ControlName", "New_ControlName"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_Date", "New_Date"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_Domain", "New_Domain"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_exposureId", "New_exposureId"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_name", "New_name"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_PageName", "New_PageName"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_Place", "New_Place"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_SessionId", "New_SessionId"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_SiteId", "New_SiteId"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_Type", "New_Type"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_Url", "New_Url"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("new_originid", "new_originid"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("new_channelid", "new_channelid"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("new_primaryexpertiseid", "new_primaryexpertiseid"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("new_regionid", "new_regionid"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("new_secondaryexpertiseid", "new_secondaryexpertiseid"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_AdvertisersReturned", "New_AdvertisersReturned"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("new_websiteid", "new_websiteid"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_Keyword", "New_Keyword"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("new_toolbaridid", "new_toolbaridid"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_IP", "New_IP"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_IsWelcomeScreenEvent", "New_IsWelcomeScreenEvent"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_UserId", "New_UserId"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_WelcomeScreenEventType", "New_WelcomeScreenEventType"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_SiteTitle", "New_SiteTitle"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_Browser", "New_Browser"));
            extensionMapping.Add(new SqlBulkCopyColumnMapping("New_BrowserVersion", "New_BrowserVersion"));
        }

        private void FindHeadingId(NoProblem.Core.DataModel.Xrm.new_exposure exposure, string code, int level)
        {
            if (level == 1)
            {
                DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
                Guid id = dal.GetPrimaryExpertiseIdByCode(code);
                if (id != Guid.Empty)
                {
                    exposure.new_primaryexpertiseid = id;
                }
            }
            else if (level == 2)
            {
                DataAccessLayer.SecondaryExpertise dal = new NoProblem.Core.DataAccessLayer.SecondaryExpertise(XrmDataContext);
                DataModel.Xrm.new_secondaryexpertise secondary = dal.GetSecondaryExpertiseByCode(code);
                if (secondary != null)
                {
                    exposure.new_secondaryexpertiseid = secondary.new_secondaryexpertiseid;
                    exposure.new_primaryexpertiseid = secondary.new_primaryexpertiseid.Value;
                }
            }
        }

        private void FindRegionId(DataModel.Xrm.new_exposure exposure, string code, int level)
        {
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            Guid id = dal.GetRegionIdByCode(code);
            if (id != Guid.Empty)
            {
                exposure.new_regionid = id;
            }
        }

        private Guid FindChannelId(NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode channelCode)
        {
            DataAccessLayer.Channel channelDal = new NoProblem.Core.DataAccessLayer.Channel(XrmDataContext);
            return channelDal.GetChannelIdByCode(channelCode);
        }

        #endregion
    }
}


/*
                   [CreatedBy]
                  ,[CreatedOn]
                  ,[DeletionStateCode]
                  ,[ModifiedBy]
                  ,[ModifiedOn]
                  ,[New_exposureId]                  
                  ,[OwningBusinessUnit]
                  ,[statecode]
                  ,[statuscode]
                  ,[OwningUser]
                 */
/*
                     [New_ControlName]
                    ,[New_Date]
                    ,[New_Domain]
                    ,[New_exposureId]
                    ,[New_name]
                    ,[New_PageName]
                    ,[New_Place]
                    ,[New_SessionId]
                    ,[New_SiteId]
                    ,[New_Type]
                    ,[New_Url]
                    ,[new_originid]
                    ,[new_channelid]
                    ,[new_primaryexpertiseid]
                    ,[new_regionid]
                    ,[new_secondaryexpertiseid]
                    ,[New_AdvertisersReturned]
                    ,[new_websiteid]
                    ,[New_Keyword]
                    ,[new_toolbaridid]
                    ,[New_IP]
                    ,[New_IsWelcomeScreenEvent]
                    ,[New_UserId]
                    ,[New_WelcomeScreenEventType]
                    ,[New_SiteTitle]
                    ,[New_Browser]
                    ,[New_BrowserVersion]
                  */