﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class GooglePlacesManager : AarSearchManagerBase
    {

        public GooglePlacesManager(DataModel.Xrm.DataContext xrmDataContext, DataModel.Xrm.incident incident)
            : base(xrmDataContext, incident)
        {
            this.AarSource = DataModel.Xrm.new_yelpsearch.eAarSearchSource.GOOGLE;
        }
        /*
         public override YelpSupplierMinData GetSupplier()
         {
             IEnumerable<Guid> uneligibleSuppliers = GetAlreadyCalledSuppliers();
             List<YelpSupplierMinData> suppliers = GetSuppliersFromLastYelpSearch(uneligibleSuppliers);
             if (suppliers == null || suppliers.Count == 0)
             {
                 bool newSuppliersFound = GetSuppliersFromApi();
                 if (newSuppliersFound)
                     suppliers = GetSuppliersFromLastYelpSearch(uneligibleSuppliers);
                 if (suppliers == null || suppliers.Count == 0)
                     return null;
             }
             YelpSupplierMinData chosenSupplier = ChooseSupplierByRankAndBusinessLogic(suppliers);
             return chosenSupplier;
         }
         * */
        private string GetGoogleQuery()
         {
             return category.new_name + " in " + locationName;
         }
         internal override void GetSuppliersFromApi()
         {
             GoogleApiHelper helper = new GoogleApiHelper(category);
             List<GooglePlaceResult> list = null;
             try
             {
                 list = helper.GetSuppliersFromApi(GetGoogleQuery());
             }
             catch (Exception exc)
             {
                 Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception search Yelp API");
             }
             if (list == null || list.Count == 0)
                 return;
             CreateNewYelpSearchEntry();
             UpsertYelpSuppliers(list);
         }
         private void UpsertYelpSuppliers(List<GooglePlaceResult> list)
         {
        //     bool newDataFound = false;
             foreach (var supplier in list)
             {
                 if (supplier.details == null || string.IsNullOrEmpty(supplier.details.formatted_phone_number))
                     continue;
                 supplier.details.formatted_phone_number = GetCleanPhone(supplier.details.formatted_phone_number);
                 Guid yelpSupplierId = FindExistingYelpSupplier(supplier.details.formatted_phone_number);
                 
                 if (yelpSupplierId != Guid.Empty)
                 {
               //      newDataFound |= UpdateYelpSupplier(yelpSupplierId);
                     UpdateYelpSupplier(yelpSupplierId, new decimal(supplier.rating), supplier.id, supplier.details.url, 
                         supplier.details.user_ratings_total, supplier.details.icon);
                 }
                 else                  
                 if (yelpSupplierId == Guid.Empty)
                 {
        //             newDataFound = true;
                     InsertYelpSupplier(GetYelpSupplierData(supplier));
                 }
             }
         //    return newDataFound;
         }
       
        private DataModel.Xrm.new_yelpsupplier GetYelpSupplierData(GooglePlaceResult supplier)
         {
             DataModel.Xrm.new_yelpsupplier yelpSupplier = new DataModel.Xrm.new_yelpsupplier();
             yelpSupplier.new_name = supplier.name;
             yelpSupplier.new_googleid = supplier.id;
          //   yelpSupplier.new_phone = isProduction ? supplier.details.formatted_phone_number : "0543388917";
             yelpSupplier.new_phone = supplier.details.formatted_phone_number;
             yelpSupplier.new_googlerating = new decimal(supplier.rating);
             yelpSupplier.new_rating = new decimal(0);
             yelpSupplier.new_googleurl = supplier.details.url;
             yelpSupplier.new_googlereviewcount = supplier.details.user_ratings_total;
             yelpSupplier.new_googlebusinessicon = supplier.details.icon;
             if (supplier.geometry != null && supplier.geometry.location != null)
             {
                 yelpSupplier.new_latitude = new decimal(supplier.geometry.location.lat);
                 yelpSupplier.new_longitude = new decimal(supplier.geometry.location.lng);
             }
             yelpSupplier.new_address = supplier.formatted_address;
             yelpSupplier.new_callcount = 0;
             yelpSupplier.new_dontcallme = false;
             yelpSupplier.new_successfulcallcount = 0;
             return yelpSupplier;
         }
        
       
        

        protected override string queryUpdateYelpSupplier
        {
            get
            {
                return @"
UPDATE dbo.New_yelpsupplierExtensionBase
SET New_GoogleRating = @Rating,
    new_googleid = @AarId,
    new_googleurl = @AarUrl,
    New_googleReviewCount = @ReviewCount,
    New_googleBusinessIcon = @IconUrl
WHERE New_yelpsupplierId = @YelpSupplierId";
            }
        }
        /*
        protected override string queryGetSuppliersFromLastSearch
        {
            //  private const string queryGetSuppliersFromLastSearch =
            get
            {
                return @"
select ys.new_yelpsupplierid, ys.new_phone,  ys.new_rating, ys.New_GoogleRating, ys.new_callcount, ys.new_successfulcallcount
from new_yelpsupplier ys with(nolock)
join new_yelpsearchresult sr with(nolock) on ys.new_yelpsupplierid = sr.new_yelpsupplierid
where sr.new_yelpsearchid = @searchId
and ys.new_dontcallme = 0
order by ys.new_callcount asc, ys.New_GoogleRating desc, ys.new_successfulcallcount desc
";
            }
        }
         * */
        
    }

    /*
     * select ys.new_yelpsupplierid, ys.new_phone,  ys.new_rating, ys.new_callcount, ys.new_successfulcallcount
from dbo.New_yelpsupplierExtensionBase ys with(nolock)
	inner join dbo.New_yelpsupplierBase ys_base with(nolock) on ys.New_yelpsupplierId = ys_base.New_yelpsupplierId
	inner join dbo.New_yelpsearchresultExtensionBase sr with(nolock) on ys.new_yelpsupplierid = sr.new_yelpsupplierid
where  ys.new_dontcallme = 0
	and ys_base.DeletionStateCode = 0
order by ys.new_rating desc, ys.new_callcount asc, ys.new_successfulcallcount desc
     * */
}
