﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class GoogleApiHelper
    {
        #region Static
        static readonly string GoogleKey;
        static GoogleApiHelper()
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(null);
            GoogleKey = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.GOOGLE_API_KEY);
        }
        #endregion

        private DataModel.Xrm.new_primaryexpertise category;
        private string GoogleCategoryTypeName;
        internal GoogleApiHelper(DataModel.Xrm.new_primaryexpertise category) 
        {
            this.category = category;
            if (this.category.new_googlecategorytype.HasValue)
                this.GoogleCategoryTypeName = ((NoProblem.Core.DataModel.Xrm.new_primaryexpertise.eGoogleCategoryType)this.category.new_googlecategorytype).ToString();
        }
        internal List<GooglePlaceResult> GetSuppliersFromApi(string query)
        {
            List<GooglePlaceResult> list = new List<GooglePlaceResult>();
     //       RestSharp.RestClient client = new RestSharp.RestClient();
            RestSharp.RestClient client = new RestSharp.RestClient(GetSearchURL(query));

            //AIzaSyDJQ_-mhV9NHoI-q9kOS23Soqx97e9rq5E
           
            GooglePlaceResultJson GoogleResult;
            int i = 0;
            do
            {
                i++;
                RestSharp.RestRequest request = new RestSharp.RestRequest();
                var response = client.Execute(request);
                using (var ms = new MemoryStream(response.RawBytes))
                {
                    var serializer = new DataContractJsonSerializer(typeof(GooglePlaceResultJson));
                    GoogleResult = (GooglePlaceResultJson)serializer.ReadObject(ms);
                }
                if (GoogleResult.status != "OK")
                    break;
                foreach(GooglePlaceResult gpr in GoogleResult.results)
                {
                    bool IsValid = false;
                    if (!string.IsNullOrEmpty(GoogleCategoryTypeName))
                    {
                        foreach (string _type in gpr.types)
                        {
                            if (_type == GoogleCategoryTypeName)
                            {
                                IsValid = true;
                                break;
                            }
                        }
                    }
                    else
                        IsValid = true;
                    if (!IsValid)
                        continue;
                    GoogleSupplierJson details = GetSupplierDetails(gpr.place_id);
                    
                    if (details.status != "OK")
                        continue;
                    gpr.details = details.result;
                    list.Add(gpr);
                }

                client = new RestSharp.RestClient(GetNextPageTokenURL(GoogleResult.next_page_token));
                
            } while (!string.IsNullOrEmpty(GoogleResult.next_page_token) && i < 3);
            return list;
        }
        private string GetSearchURL(string query)
        {
            return "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + HttpUtility.UrlEncode(query) + "&key=" + GoogleKey;
        }
        private string GetNextPageTokenURL(string next_page_token)
        {
            return "https://maps.googleapis.com/maps/api/place/textsearch/json?pagetoken=" + next_page_token + "&key=" + GoogleKey;            
        }
        internal GoogleSupplierJson GetSupplierDetails(string placeid)
        {
            RestSharp.RestClient client = new RestSharp.RestClient("https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeid + "&key=" + GoogleKey);
            //AIzaSyDJQ_-mhV9NHoI-q9kOS23Soqx97e9rq5E
            RestSharp.RestRequest request = new RestSharp.RestRequest();
            var response = client.Execute(request);
            GoogleSupplierJson SupplierDetails = null;
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(response.Content)))
            {
                var serializer = new DataContractJsonSerializer(typeof(GoogleSupplierJson));
                SupplierDetails = (GoogleSupplierJson)serializer.ReadObject(ms);
            }
            return SupplierDetails;
        }
       

    }
}
