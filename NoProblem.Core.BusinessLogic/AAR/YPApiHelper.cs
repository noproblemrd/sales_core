﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
//using System.Web.Helpers;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class YPApiHelper
    {
         #region Static
        static readonly string YP_KEY;
        static YPApiHelper()
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(null);
            YP_KEY = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.YP_API_KEY);
        }
        #endregion
         private DataModel.Xrm.new_primaryexpertise category;
        DataModel.Xrm.incident incident;
        string zipcode;
        internal YPApiHelper(DataModel.Xrm.new_primaryexpertise category, DataModel.Xrm.incident incident, string zipcode) 
        {
            this.category = category;
            this.incident = incident;
            this.zipcode = zipcode;
         }
        internal List<YP_SearchListing> GetSuppliersFromApi()
        {
            YPSupplierJson YPResult;
            List<YP_SearchListing> list = new List<YP_SearchListing>();
            for (int i = 1; i < 3; i++)
            {
                
                RestSharp.RestClient client = new RestSharp.RestClient(GetSearchURL());

                RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
                SetParametersRestRequest(request, i);
                var response = client.Execute(request);
                if (response == null || response.RawBytes == null)
                    return list;
                using (var ms = new MemoryStream(response.RawBytes))
                {
                    var serializer = new DataContractJsonSerializer(typeof(YPSupplierJson));
                    YPResult = (YPSupplierJson)serializer.ReadObject(ms);
                }
                if (YPResult.searchResult.metaProperties == null)
                    return list;
                if (YPResult.searchResult.metaProperties.resultCode != "Success")
                    continue;
                foreach(YP_SearchListing ysl in YPResult.searchResult.searchListings.searchListing)
                    list.Add(ysl);

                 
                /*
                RestSharp.RestClient client = new RestSharp.RestClient(GetSearchURL());
                RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
                SetParametersRestRequest(request, i);
                var response = client.Execute(request);
                using (var ms = new MemoryStream(response.RawBytes))
                {
                    using (StreamReader sr = new StreamReader(ms))
                    {
                        string _json = sr.ReadToEnd();
                        dynamic d = JObject.Parse(_json);
                        object o = d.searchResult.metaProperties.resultCode;
                        
                    }
                }
                 */

            }
            return list;
        }
        private string GetSearchURL()
        {
            return "http://api2.yp.com/listings/v1/search";//?query=" + HttpUtility.UrlEncode(query) + "&key=" + GoogleKey;
        }
        void SetParametersRestRequest(RestSharp.RestRequest request, int pageNum)
        {
            request.AddParameter("key", YP_KEY);
            request.AddParameter("term", category.new_name);
            request.AddParameter("searchloc", zipcode);
            request.AddParameter("phonesearch", "1");
            request.AddParameter("listingcount", "40");
            request.AddParameter("pagenum", pageNum.ToString());
            request.AddParameter("format", "json");
            
        }
    }
}
