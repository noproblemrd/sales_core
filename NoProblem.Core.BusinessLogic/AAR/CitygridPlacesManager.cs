﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class CitygridPlacesManager : AarSearchManagerBase
    {
        public CitygridPlacesManager(DataModel.Xrm.DataContext xrmDataContext, DataModel.Xrm.incident incident)
            : base(xrmDataContext, incident)
        {
            this.AarSource = DataModel.Xrm.new_yelpsearch.eAarSearchSource.CITYGRID;
        }
        
        internal override void GetSuppliersFromApi()
        {
            CitygridApiHelper helper = new CitygridApiHelper(category, incident, locationName);
            List<CitygridSupplierLocation> list = null;
            try
            {
                list = helper.GetSuppliersFromApi();
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception search CitygridApi");
            }
            if (list == null || list.Count == 0)
                return;
            CreateNewYelpSearchEntry();
            UpsertYelpSuppliers(list);
        }
        private void UpsertYelpSuppliers(List<CitygridSupplierLocation> list)
        {
            //     bool newDataFound = false;
            foreach (CitygridSupplierLocation supplier in list)
            {               
     //           supplier.phone_number = GetCleanPhone(supplier.details.formatted_phone_number);
                if (string.IsNullOrEmpty(supplier.phone_number))
                    continue;
                Guid yelpSupplierId = FindExistingYelpSupplier(supplier.phone_number);

                if (yelpSupplierId != Guid.Empty)
                {
                    //      newDataFound |= UpdateYelpSupplier(yelpSupplierId);
                    UpdateYelpSupplier(yelpSupplierId, new decimal(supplier.rating), supplier.id.ToString(), supplier.profile,
                        supplier.user_review_count, supplier.image);
                }
                else
                    if (yelpSupplierId == Guid.Empty)
                    {
                        //             newDataFound = true;
                        InsertYelpSupplier(GetYelpSupplierData(supplier));
                    }
            }
            //    return newDataFound;
        }
        private DataModel.Xrm.new_yelpsupplier GetYelpSupplierData(CitygridSupplierLocation supplier)
        {
            DataModel.Xrm.new_yelpsupplier yelpSupplier = new DataModel.Xrm.new_yelpsupplier();
            yelpSupplier.new_name = supplier.name;
            yelpSupplier.new_citygridid = supplier.id.ToString();
            //   yelpSupplier.new_phone = isProduction ? supplier.details.formatted_phone_number : "0543388917";
            yelpSupplier.new_phone = supplier.phone_number;
            yelpSupplier.new_citygridrating = new decimal(supplier.rating);
          //  yelpSupplier.new_rating = new decimal(0);
            yelpSupplier.new_citygridurl = supplier.profile;
            yelpSupplier.new_citygridreviewcount = supplier.user_review_count;
            yelpSupplier.new_citygridbusinessicon = supplier.image;
            if (supplier.latitude.HasValue)
                yelpSupplier.new_latitude = new decimal(supplier.latitude.Value);
            if (supplier.longitude.HasValue)
                yelpSupplier.new_longitude = new decimal(supplier.longitude.Value);

            yelpSupplier.new_address = GetAddress(supplier);
            yelpSupplier.new_callcount = 0;
            yelpSupplier.new_dontcallme = false;
            yelpSupplier.new_successfulcallcount = 0;
            return yelpSupplier;
        }
        private string GetAddress(CitygridSupplierLocation supplier)
        {
            if (supplier.address == null)
                return string.Empty;
            return supplier.address.street + ", " + supplier.address.city + ", " + supplier.address.state + ", " + supplier.address.postal_code;
        }
        protected override string queryUpdateYelpSupplier
        {
            get
            {
                return @"
UPDATE dbo.New_yelpsupplierExtensionBase
SET New_citygridRating = @Rating,
    New_citygridId = @AarId,
    New_citygridUrl = @AarUrl,
    New_citygridReviewCount = @ReviewCount,
    New_citygridBusinessIcon = @IconUrl
WHERE New_yelpsupplierId = @YelpSupplierId";
            }
        }
        

        
    }
}
