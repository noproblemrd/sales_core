﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class CustomAarManager : AarSearchManagerBase
    {
        string supplierName;
        string phoneNumber;
        Guid yelpSupplierId;
        public CustomAarManager(DataModel.Xrm.DataContext xrmDataContext, DataModel.Xrm.incident incident, 
            string supplierName, string phoneNumber)
            : base(xrmDataContext, incident)
        {           
            this.supplierName = supplierName;
            this.phoneNumber = phoneNumber;
            AarSource = DataModel.Xrm.new_yelpsearch.eAarSearchSource.CLIPCALL;
        }
        
        internal override void GetSuppliersFromApi()
        {
            this.yelpSupplierId = FindExistingYelpSupplier(phoneNumber);
            if(yelpSupplierId == Guid.Empty)
                this.yelpSupplierId = InsertYelpSupplier(GetYelpSupplierData());
            else
                _UpdateAarSupplier(yelpSupplierId);
            
        }
        protected DataModel.Xrm.new_yelpsupplier GetYelpSupplierData()
        {
            DataModel.Xrm.new_yelpsupplier yelpSupplier = new DataModel.Xrm.new_yelpsupplier();
            yelpSupplier.new_name = supplierName;           
            yelpSupplier.new_phone = phoneNumber;           
            yelpSupplier.new_callcount = 0;
            yelpSupplier.new_dontcallme = false;
            yelpSupplier.new_successfulcallcount = 0;
            return yelpSupplier;
        }
        public Guid GetYelpSupplierId
        {
            get
            {
                return this.yelpSupplierId;
            }
        }

        protected override string queryUpdateYelpSupplier
        {
            get { throw new NotImplementedException(); }
        }
    }
}
