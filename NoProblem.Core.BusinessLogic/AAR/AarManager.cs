﻿using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils;
using NoProblem.Core.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DML = NoProblem.Core.DataModel;
using System.Data.SqlClient;
using NoProblem.Core.BusinessLogic.AAR.Events;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.AAR
{
    public class AarManager : XrmUserBase
    {
        const string DEFAULT_PROMPT_NAME = "Flavor2f";
        #region Ctor
        public AarManager(DataModel.Xrm.DataContext xrmDataContext) :
            base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal bool ContinueServiceRequest(Guid incidentId)
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string aarEnabled = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.AAR_ENABLED);
            if (aarEnabled != "1")
            {
                return false;
            }
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentId);
            if (IsFulfillmentDone(incident))
            {
                return false;
            }
            OpenServiceRequestForAar(incident, incidentDal);
            ContinueWithAarThread continueThread = new ContinueWithAarThread(incidentId);
            continueThread.Start();
            return true;
        }

        public ConnectSupplierToCustomerResponse ConnectSupplierToCustomer(Guid callId, Guid incidentId)
        {
            ConnectSupplierToCustomerResponse response;
            try
            {
                response = _ConnectSupplierToCustomer(callId, incidentId);
            }
            catch(Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, 
                    string.Format("Exception ConnectSupplierToCustomer IncidentId:{0}, CallId:{1}", incidentId, callId));

                response = new ConnectSupplierToCustomerResponse();
                response.IsSuccess = false;
                response.message = ConnectSupplierToCustomerResponse.Message.FAILED;
            }
            if(!response.IsSuccess)
            {
                NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.AarTextMessage.AarTextMessageHandler textMessage = new FoneApiBL.Handlers.AarTextMessage.AarTextMessageHandler();
                textMessage.SendMissedConnectMeToCustomer(callId);
            }
            return response;
        }
        public ConnectSupplierToCustomerResponse _ConnectSupplierToCustomer(Guid callId, Guid incidentId)
        {
            /*
            AarVideoTestResponse videoTest = GetAarVideoTest(callId);
            if (videoTest != null)
            {
                throw new Exception("Lead is closed");
            }
            */
            ConnectSupplierToCustomerResponse response = new ConnectSupplierToCustomerResponse();
            Guid AarSupplierId = Guid.Empty;
            Guid accountid = Guid.Empty;
            string command = @"
SELECT YC.New_YelpSupplierId, S.New_AccountId
FROM [dbo].[New_yelpaarcallExtensionBase] YC WITH(NOLOCK) 
	INNER JOIN dbo.IncidentExtensionBase I WITH(NOLOCK)  on YC.New_IncidentId = I.IncidentId
	INNER JOIN dbo.New_yelpsupplierExtensionBase S WITH(NOLOCK) on S.New_yelpsupplierId = YC.New_YelpSupplierId
WHERE YC.New_yelpaarcallId = @CallId and I.IncidentId = @IncidentId";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@CallId", callId);
                    cmd.Parameters.AddWithValue("@IncidentId", incidentId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if(reader.Read())
                    {
                        if (reader["New_YelpSupplierId"] != DBNull.Value)
                            AarSupplierId = (Guid)reader["New_YelpSupplierId"];
                        if (reader["New_AccountId"] != DBNull.Value)
                            accountid = (Guid)reader["New_AccountId"];
                    }
                    conn.Close();
                    cmd.Dispose();
                }
                conn.Dispose();
            }
            if(AarSupplierId == Guid.Empty)
            {
                response.IsSuccess = false;
                response.message = ConnectSupplierToCustomerResponse.Message.BAD_ARGUMENT;
                return response;
            }
            /*
            if(accountid != Guid.Empty)
            {
                response.IsSuccess = false;
                response.message = ConnectSupplierToCustomerResponse.Message.DONT_HAVE_FREE_LEAD;
                return response;
            }
             * */
            /*
            DataAccessLayer.Yelp.YelpAARCall callsDal = new DataAccessLayer.Yelp.YelpAARCall(XrmDataContext);
            new_yelpaarcall aarCall = callsDal.All.SingleOrDefault(c => c.new_yelpaarcallid == callId);
            if (aarCall == null || aarCall.new_incidentid.HasValue == false)
            

            if (incidentId != aarCall.new_incidentid.Value)
            {
                response.IsSuccess = false;
                response.Message = "Bad argument";
            }
            
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
            incident incident = incidentDal.Retrieve(incidentId);
            if (incident == null)
            {
                response.IsSuccess = false;
                response.Message = "Bad argument";
            }

            Guid? supplierId = aarCall.new_yelpsupplierid;
            if (supplierId.HasValue == false)
                return;
           
            DataAccessLayer.Yelp.YelpSupplier yelpSupplierDal = new DataAccessLayer.Yelp.YelpSupplier(XrmDataContext);
            new_yelpsupplier supplier = yelpSupplierDal.Retrieve(supplierId.Value);

            if (supplier == null)
                return;
             */


            /*
            bool fulfillmentDone = IsFulfillmentDone(incident);
            if (fulfillmentDone || incident.new_isstoppedmanually.IsTrue())
            {
                CloseServiceRequestAfterAarDone(incident);
                return;
            }
             

            DataAccessLayer.Yelp.YelpAARCall aarCallDal = new DataAccessLayer.Yelp.YelpAARCall(XrmDataContext);
            new_yelpaarcall yelpaarcall = aarCallDal.All.FirstOrDefault(c => c.new_yelpsupplierid == supplier.new_yelpsupplierid && c.new_incidentid == incident.incidentid);
            if (yelpaarcall != null) // this supplier already requested this lead
            {
                return;
            }
            * */
            NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager srm = new ServiceRequest.ServiceRequestManager(XrmDataContext);
            incident.Status incidentStatus;
            if(!srm.IsMobileIncidentOpen(incidentId, out incidentStatus))
            {
                response.IsSuccess = false;
                response.message = ConnectSupplierToCustomerResponse.Message.INCIDENT_CLOSE;
                return response;
            }
            NoProblem.Core.BusinessLogic.SupplierBL.CreateClipCallSupplier createSupplier = new SupplierBL.CreateClipCallSupplier(XrmDataContext);
            DataModel.Xrm.account clipCallSupplier = createSupplier.CreateClipCallSupplierFromAar(AarSupplierId);
            if (clipCallSupplier == null)
            {
                response.IsSuccess = false;
                response.message = ConnectSupplierToCustomerResponse.Message.BAD_ARGUMENT;
                return response;
            }

            Guid incidentAccountId = srm.CreateLeadForAar(clipCallSupplier, incidentId);
            if (incidentAccountId != Guid.Empty)
            {
                string command2 = @"
UPDATE dbo.New_yelpaarcallExtensionBase
SET New_incidentAccountId = @IncidentAccountId
WHERE New_yelpaarcallId = @AarCallId";
                using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command2, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@IncidentAccountId", incidentAccountId);
                        cmd.Parameters.AddWithValue("@AarCallId", callId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            response.IsSuccess = true;
            response.message = ConnectSupplierToCustomerResponse.Message.SUCCESS;
            return response;
            /*
            DataModel.Xrm.new_yelpaarcall callEntry = new DML.Xrm.new_yelpaarcall();
            callEntry.new_yelpsupplierid = supplier.new_yelpsupplierid;
            callEntry.new_incidentid = incident.incidentid;
            callEntry.new_type = (int)(DML.Xrm.new_yelpaarcall.eAarCallType.IncidentCall);

            aarCallDal.Create(callEntry);
            XrmDataContext.SaveChanges();
            FoneApiBL.Handlers.Click2Call.AarSupplier.AarCallManager callHandler =
                new FoneApiBL.Handlers.Click2Call.AarSupplier.AarCallManager(callEntry.new_yelpaarcallid, supplier.new_phone, incident.new_telephone1, incident.new_new_primaryexpertise_incident.new_code);
            bool callOk = callHandler.StartCall();
            /*
            if (!callOk)
            {
                ContinueWithAarThread continueThread = new ContinueWithAarThread(incident.incidentid);
                continueThread.Start();
            }
             **/


        }

        internal void ContinueAarServiceRequest(Guid incidentId)
        {
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentId);
            bool fulfillmentDone = IsFulfillmentDone(incident);
            if (fulfillmentDone || incident.new_isstoppedmanually.IsTrue())
            {
                CloseServiceRequestAfterAarDone(incident);
                return;
            }
            YelpSupplierMinData supplier = GetSupplier(incident);
            if (supplier == null)
            {
                CloseServiceRequestAfterAarDone(incident);
                /*It make a loop if not fulfillmentDone and there are no supplier from AAR
                if (fulfillmentDone)
                {
                    CloseServiceRequestAfterAarDone(incident);
                }
                else
                {
                    //if no fullfilment and no more Yelps, go to regular flow:  --- It make a loop
                    ServiceRequest.ServiceRequestManager manager = new ServiceRequest.ServiceRequestManager(XrmDataContext);
                    NoProblem.Core.DataModel.Consumer.Interfaces.IServiceResponse response = new NoProblem.Core.DataModel.Consumer.ScheduledServiceResponse() { Status = StatusCode.Success };
                    manager.SendServiceRequest(incident, response, null, DML.Consumer.eServiceType.Normal, true);
                }
                 * */
            }
            else//Yelp supplier found:
            {
                ExecuteCall(incident, supplier);
        //        ExecuteTextMessage(incident, supplier);
            }
        }
        public int CreateTextMessagesForServiceRequest(Guid incidentId, int? count)
        {
            return CreateTextMessagesForServiceRequest(incidentId, count, null);
        }
        public int CreateTextMessagesForServiceRequest(Guid incidentId, int? count, string RecordName)
        {
           
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentId);
            if (incident == null)
                throw new AarException("Incident not found!");
       //     NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.AarTextMessage.AarTextMessageHandler.UpdateAarPromptStep1();
            if (string.IsNullOrEmpty(RecordName))
                RecordName = DEFAULT_PROMPT_NAME;
            NoProblem.Core.DataAccessLayer.Aar.AarRecord ar = NoProblem.Core.DataAccessLayer.Aar.AarRecord.GetAarRecord(RecordName);
            if (ar == null)
                throw new AarException("Record not found!");
            Guid AarIncidentId = CreateAarIncident(ar, incident, new_aarincident.Source.SYSTEM_NEW_INCIDENT);
            List<YelpSupplierMinData> suppliers = GetSuppliers(incident, count);
            AarTextMessageJob atmj = new AarTextMessageJob()
            {
                incident = incident,
                AarIncidentId = AarIncidentId,
                ar = ar,
                suppliers = suppliers
            };
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
                {
    //                Thread.CurrentThread.Name = incident.incidentid.ToString();
                    AarTextMessageJob _atmj = (AarTextMessageJob)state;
                    ExecuteTextMessage(_atmj.incident, _atmj.suppliers, _atmj.ar, _atmj.AarIncidentId);
   //                 ExecuteTextMessage(incident, suppliers, ar, AarIncidentId);
                }), atmj);            
       //     return "Excute on " + suppliers.Count() + " suppliers!";
            return suppliers.Count();
            
        }
        
        public void CreateTextMessagesForNoAnswer(Guid YelpAarCallId, Guid AarIncidentId)
        {
            string query = @"
SELECT C.New_IncidentId, C.New_YelpSupplierId, S.New_Phone, I.new_telephone1
FROM dbo.New_yelpaarcallExtensionBase C WITH(NOLOCK)
	INNER JOIN dbo.New_yelpsupplierExtensionBase S WITH(NOLOCK)
		on C.New_YelpSupplierId = S.New_yelpsupplierId
	INNER JOIN dbo.IncidentExtensionBase I WITH(NOLOCK)
		on C.New_IncidentId = I.IncidentId
WHERE C.New_yelpaarcallId = @YelpAarCallId";
            Guid IncidentId, YelpSupplierId;
            string SupplierPhone, CustomerPhone;
            int count = 0;
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using(SqlCommand cmd = new SqlCommand(query, conn))
                {
                    
                    cmd.Parameters.AddWithValue("@YelpAarCallId", YelpAarCallId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        IncidentId = (Guid)reader["New_IncidentId"];
                        YelpSupplierId = (Guid)reader["New_YelpSupplierId"];
                        SupplierPhone = (string)reader["New_Phone"];
                        CustomerPhone = (string)reader["new_telephone1"];
                    }
                    else
                        return;
                    reader.Dispose();
                    cmd.Dispose();
                }
                query = @"
SELECT COUNT(*) as 'count'
FROM dbo.New_aarincidentExtensionBase I WITH(NOLOCK)
	INNER JOIN dbo.New_yelpaarcallExtensionBase C WITH(NOLOCK)
		on I.New_aarincidentId = C.New_AarIncidentId
WHERE I.New_aarincidentId = @AarIncidentId
    AND C.New_YelpSupplierId = @YelpSupplierId";
                
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@AarIncidentId", AarIncidentId);
                    cmd.Parameters.AddWithValue("@YelpSupplierId", YelpSupplierId);
                    object obj = cmd.ExecuteScalar();
                    if (obj != null && obj != DBNull.Value)
                        count = (int)obj;
                    cmd.Dispose();
                }
                
                conn.Close();
            }
            NoProblem.Core.DataAccessLayer.Aar.AarRecord ar = new DataAccessLayer.Aar.AarRecord(AarIncidentId);
            if (count >= ar.RedailTimes)
                return;
            NoProblem.Core.DataAccessLayer.Aar.AarTextMessageCall atmc = new DataAccessLayer.Aar.AarTextMessageCall(IncidentId, CustomerPhone, YelpSupplierId, SupplierPhone, ar, AarIncidentId);

            Timer timer = new Timer(
                new TimerCallback(delegate (object state)
                    {
                        NoProblem.Core.DataAccessLayer.Aar.AarTextMessageCall _atmc = (NoProblem.Core.DataAccessLayer.Aar.AarTextMessageCall)state;
          //              LogUtils.MyHandle.WriteToLog(new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(_atmc));
                        ExecuteTextMessage(_atmc.IncidentId, _atmc.CustomerPhone, _atmc.YelpSupplierId, _atmc.SupplierPhone, 
                            _atmc.aarRecord, _atmc.AarIncidentId);
                    }),
                atmc,
                ar.RedailIntervalMinutes * 60 * 1000,
                System.Threading.Timeout.Infinite);
            atmc.timer = timer;
   //         ExecuteTextMessage(incident.incidentid, incident.new_telephone1, supplier.YelpSupplierId, supplier.Phone, ar, AarIncidentId);
        }
        Guid CreateAarIncident(NoProblem.Core.DataAccessLayer.Aar.AarRecord ar, DML.Xrm.incident incident, DataModel.Xrm.new_aarincident.Source source)
        {
            DataAccessLayer.Aar.AarIcidentDal incDal = new DataAccessLayer.Aar.AarIcidentDal(XrmDataContext);
            DataModel.Xrm.new_aarincident inc = new DML.Xrm.new_aarincident(XrmDataContext);
            inc.new_incidentid = incident.incidentid;
            inc.new_prompt = ar.AarRecordName;
            inc.new_redailtimes = ar.RedailTimes;
            inc.new_redialintervalminutes = ar.RedailIntervalMinutes;
            inc.new_aarpromptid = ar.Id;
            inc.new_source = (int)source;
            incDal.Create(inc);
            XrmDataContext.SaveChanges();
            return inc.new_aarincidentid;

        }
        internal void CallEnded(Guid yelpAarCallId, string recordingUrl, int customerCallDuration, int supplierCallDuration, string customerEndStatus, string supplierEndStatus, string customerCallId, string supplierCallId)
        {
            DataAccessLayer.Yelp.YelpAARCall yelpCallDal = new DataAccessLayer.Yelp.YelpAARCall(XrmDataContext);
            DataModel.Xrm.new_yelpaarcall call = UpdateCallDataAtCallEnded(yelpAarCallId, recordingUrl, customerCallDuration, supplierCallDuration, customerEndStatus, supplierEndStatus, customerCallId, supplierCallId, yelpCallDal);
            Guid incidentId = UpdateYelpSupplierAndIncidentAfterCallEnded(call, yelpCallDal);
            ContinueWithAarThread t = new ContinueWithAarThread(incidentId);
            t.Start();
        }

        internal DataModel.AudioFilesActionsList GetIvrTextForAar(Guid callId, out string intro, out string menu, out bool isManVoice, out bool isAudioFiles)
        {
            DataModel.AudioFilesActionsList dic = null;
            DataAccessLayer.IvrFlavor dal = new NoProblem.Core.DataAccessLayer.IvrFlavor(XrmDataContext);
            string headingCode;
            int leadsLeft;
            string description;
            dal.GetIvrMenuDataForCall(callId, out intro, out menu, out isManVoice, out isAudioFiles, out headingCode, out leadsLeft, out description);
            if (isAudioFiles)
            {
                DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
                dic = new DataModel.AudioFilesActionsList();
                string[] split = menu.Split(';');
                foreach (var item in split)
                {
                    if (item.StartsWith(AudioFilesConsts.VARIABLE_PREFIX))
                    {
                        string[] varSplit = item.Split('_');
                        string varType = varSplit[1];
                        switch (varType)
                        {
                            case AudioFilesConsts.HEADING_POSTFIX:
                                try
                                {
                                    string voiceFolder = varSplit[2];
                                    dic.Add(
                                        AudioFilesConsts.FILE_ACTION,
                                        audioFilesUrlBase + AudioFilesConsts.SLASH + voiceFolder + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.MP3_FILE_EXTENSION
                                        );
                                }
                                catch (Exception exc)
                                {
                                    LogUtils.MyHandle.HandleException(exc, "Exception finding recording for heading... saying 'a service provider'");
                                    dic.Add(
                                        AudioFilesConsts.TTS_ACTION,
                                        "a service provider");
                                }
                                break;
                            case AudioFilesConsts.DESCRIPTION_POSTFIX:
                                dic.Add(AudioFilesConsts.TTS_ACTION,
                                    description);
                                break;
                            case AudioFilesConsts.LEADS_LEFT_POSTFIX:
                                var numbersFileList = Utils.AudioFilesUtils.NumberToAudioFilesPartition(leadsLeft);
                                if (numbersFileList != null)
                                {
                                    try
                                    {
                                        string voiceFolder = varSplit[2];
                                        foreach (int filePartition in numbersFileList)
                                        {
                                            dic.Add(
                                                AudioFilesConsts.FILE_ACTION,
                                                audioFilesUrlBase + AudioFilesConsts.SLASH + voiceFolder + AudioFilesConsts.SLASH + AudioFilesConsts.NATURAL_NUMBERS_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + filePartition.ToString() + "n" + AudioFilesConsts.MP3_FILE_EXTENSION);
                                        }
                                    }
                                    catch (Exception exc)
                                    {
                                        LogUtils.MyHandle.HandleException(exc, "Exception finding recordings for the leads number... saying the number with TTS");
                                        dic.Add(
                                            AudioFilesConsts.TTS_ACTION,
                                            leadsLeft.ToString());
                                    }
                                }
                                else
                                {
                                    dic.Add(
                                        AudioFilesConsts.TTS_ACTION,
                                        leadsLeft.ToString());
                                }
                                break;
                        }
                    }
                    else
                    {
                        dic.Add(AudioFilesConsts.FILE_ACTION, audioFilesUrlBase + AudioFilesConsts.SLASH + item);
                    }
                }
            }
            return dic;
        }

        internal void DtmfPressed(Guid yelpAarCallId, string digits)
        {
            DataAccessLayer.Yelp.YelpAARCall yelpAarCallDal = new DataAccessLayer.Yelp.YelpAARCall(XrmDataContext);
            //TODO: need to check what will be the digits used.
            if (digits == "3")
            {
                var yelpSupplierId = (from c in yelpAarCallDal.All
                                      where c.new_yelpaarcallid == yelpAarCallId
                                      select c.new_yelpsupplierid.Value).First();
                DataAccessLayer.Yelp.YelpSupplier yelpSupplierDal = new DataAccessLayer.Yelp.YelpSupplier(XrmDataContext);
                DataModel.Xrm.new_yelpsupplier yelpSupplier = new DML.Xrm.new_yelpsupplier(XrmDataContext);
                yelpSupplier.new_dontcallme = true;
                yelpSupplier.new_yelpsupplierid = yelpSupplierId;
                yelpSupplierDal.Update(yelpSupplier);
            }
            DataModel.Xrm.new_yelpaarcall call = new DML.Xrm.new_yelpaarcall(XrmDataContext);
            call.new_yelpaarcallid = yelpAarCallId;
            call.new_dtmfpressed = digits;
            yelpAarCallDal.Update(call);
            XrmDataContext.SaveChanges();
        }

        #endregion

        #region Private Methods

        private Guid UpdateYelpSupplierAndIncidentAfterCallEnded(DataModel.Xrm.new_yelpaarcall call, DataAccessLayer.Yelp.YelpAARCall yelpCallDal)
        {
            var callData = (from c in yelpCallDal.All
                            where c.new_yelpaarcallid == call.new_yelpaarcallid
                            select new { IncidentId = c.new_incidentid.Value, YelpSupplierId = c.new_yelpsupplierid.Value }).First();
            DataAccessLayer.Yelp.YelpSupplier supplierDal = new DataAccessLayer.Yelp.YelpSupplier(XrmDataContext);
            var supplierCallsData = (from s in supplierDal.All
                                     where s.new_yelpsupplierid == callData.YelpSupplierId
                                     select new { CallCount = s.new_callcount.Value, SuccessCallsCount = s.new_successfulcallcount.Value, Phone = s.new_phone }).First();
            DataModel.Xrm.new_yelpsupplier supplier = new DML.Xrm.new_yelpsupplier(XrmDataContext);
            supplier.new_yelpsupplierid = callData.YelpSupplierId;
            supplier.new_callcount = supplierCallsData.CallCount + 1;
            if (call.new_successfulcall.Value)
            {
                supplier.new_successfulcallcount = supplierCallsData.SuccessCallsCount + 1;
                DataAccessLayer.Incident inciDal = new DataAccessLayer.Incident(XrmDataContext);
                var fullfilment = (from inc in inciDal.All
                                   where inc.incidentid == callData.IncidentId
                                   select inc.new_ordered_providers).First();
                DataModel.Xrm.incident incident = new DML.Xrm.incident(XrmDataContext);
                incident.incidentid = callData.IncidentId;
                incident.new_ordered_providers = (fullfilment.HasValue ? fullfilment.Value : 0) + 1;
                inciDal.Update(incident);
                AarSuccessSmsThread smsThread = new AarSuccessSmsThread(supplierCallsData.Phone);
                smsThread.Start();
            }
            supplierDal.Update(supplier);
            XrmDataContext.SaveChanges();
            return callData.IncidentId;
        }

        private DML.Xrm.new_yelpaarcall UpdateCallDataAtCallEnded(Guid yelpAarCallId, string recordingUrl, int customerCallDuration, int supplierCallDuration, string customerEndStatus, string supplierEndStatus, string customerCallId, string supplierCallId, DataAccessLayer.Yelp.YelpAARCall yelpCallDal)
        {
            DataModel.Xrm.new_yelpaarcall call = new DML.Xrm.new_yelpaarcall(XrmDataContext);
            call.new_yelpaarcallid = yelpAarCallId;
            call.new_callendstatuscustomer = customerEndStatus;
            call.new_callendstatussupplier = supplierEndStatus;
            call.new_callidcustomer = customerCallId;
            call.new_callidsupplier = supplierCallId;
            call.new_calllengthcustomer = customerCallDuration;
            call.new_calllengthsupplier = supplierCallDuration;
            if (!String.IsNullOrEmpty(recordingUrl))
            {
                call.new_recording = recordingUrl;
                //if there is a recording that means that they spoke to each other so it's a successfull call.
                call.new_successfulcall = true;
            }
            else
            {
                call.new_successfulcall = false;
            }
            yelpCallDal.Update(call);
            XrmDataContext.SaveChanges();
            return call;
        }
        /*
        private void ExecuteTextMessage(DataModel.Xrm.incident incident, YelpSupplierMinData supplier,
            NoProblem.Core.DataAccessLayer.Aar.AarRecord ar, Guid AarIncidentId)
        {
            ExecuteTextMessage(incident.incidentid, incident.new_telephone1, supplier.YelpSupplierId, supplier.Phone, ar, AarIncidentId);           
        }
         * */
        private void ExecuteTextMessage(Guid incidentId, string CustomerPhone, Guid YelpSupplierId, string SupplierPhone,
            NoProblem.Core.DataAccessLayer.Aar.AarRecord ar, Guid AarIncidentId)
        {
            DataModel.Xrm.new_yelpaarcall callEntry = new DML.Xrm.new_yelpaarcall();
            callEntry.new_yelpsupplierid = YelpSupplierId;
            callEntry.new_incidentid = incidentId;
            callEntry.new_aarincidentid = AarIncidentId;
            callEntry.new_type = (int)(DML.Xrm.new_yelpaarcall.eAarCallType.TextMessage);
            DataAccessLayer.Yelp.YelpAARCall aarCallDal = new DataAccessLayer.Yelp.YelpAARCall(XrmDataContext);
            aarCallDal.Create(callEntry);
            XrmDataContext.SaveChanges();
            NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.AarTextMessage.AarTextMessageHandler textMessageHandler = 
                new FoneApiBL.Handlers.AarTextMessage.AarTextMessageHandler(ar);
            bool IsSent = textMessageHandler.SendTextMessage(AarIncidentId, callEntry.new_yelpaarcallid);
            if (IsSent)
                return;
            aarCallDal.Delete(callEntry);
            XrmDataContext.SaveChanges();
        }
        private void ExecuteTextMessage(DataModel.Xrm.incident incident, IEnumerable<YelpSupplierMinData> suppliers,
            NoProblem.Core.DataAccessLayer.Aar.AarRecord ar, Guid AarIncidentId)
        {
            foreach(YelpSupplierMinData supplier in suppliers)
            {                
              //  ExecuteTextMessage(incident, supplier, ar, AarIncidentId);
                ExecuteTextMessage(incident.incidentid, incident.new_telephone1, supplier.YelpSupplierId, supplier.Phone, ar, AarIncidentId);
            }
            SendCsvAar(incident);
        }
        private void SendCsvAar(DataModel.Xrm.incident incident)
        {
            CsvAarCreator cac = new CsvAarCreator(incident.incidentid, incident.ticketnumber);
            try
            {
                cac.CreateSendCsv();
            }
            catch(Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CsvAarCreator");
            }
        }
        private void ExecuteCall(DataModel.Xrm.incident incident, YelpSupplierMinData supplier)
        {
            DataModel.Xrm.new_yelpaarcall callEntry = new DML.Xrm.new_yelpaarcall();
            callEntry.new_yelpsupplierid = supplier.YelpSupplierId;
            callEntry.new_incidentid = incident.incidentid;
            callEntry.new_type = (int)(DML.Xrm.new_yelpaarcall.eAarCallType.IncidentCall);
            DataAccessLayer.Yelp.YelpAARCall aarCallDal = new DataAccessLayer.Yelp.YelpAARCall(XrmDataContext);
            aarCallDal.Create(callEntry);
            XrmDataContext.SaveChanges();
            FoneApiBL.Handlers.Click2Call.YelpAar.YelpAarCallManager callHandler =
                new FoneApiBL.Handlers.Click2Call.YelpAar.YelpAarCallManager(callEntry.new_yelpaarcallid, supplier.Phone, incident.new_telephone1, incident.new_new_primaryexpertise_incident.new_code);
            bool callOk = callHandler.StartCall();
            if (!callOk)
            {
                ContinueWithAarThread continueThread = new ContinueWithAarThread(incident.incidentid);
                continueThread.Start();
            }
        }

        private List<YelpSupplierMinData> GetSuppliers(DML.Xrm.incident incident, int? count)
        {
            /*
            YelpSearchManager search = new YelpSearchManager(XrmDataContext, incident);
            IEnumerable<YelpSupplierMinData> suppliers = search.GetSupplier(count);
            List<YelpSupplierMinData> list = new List<YelpSupplierMinData>(suppliers);
       //     List<YelpSupplierMinData> list = new List<YelpSupplierMinData>();
            count -= list.Count();
            if (count > 0)
            {
                GooglePlacesManager GoogleSearch = new GooglePlacesManager(XrmDataContext, incident);
                suppliers = GoogleSearch.GetSupplier(count);
                list.AddRange(suppliers);
            }
            return list;
             * */
            AarSearchManagerMuster search = new AarSearchManagerMuster(XrmDataContext, incident);
            IEnumerable<YelpSupplierMinData> suppliers = search.GetSupplier(count);
            return new List<YelpSupplierMinData>(suppliers);
        }

        private YelpSupplierMinData GetSupplier(DML.Xrm.incident incident)
        {
           // YelpSearchManager search = new YelpSearchManager(XrmDataContext, incident);
            AarSearchManagerMuster search = new AarSearchManagerMuster(XrmDataContext, incident);
            YelpSupplierMinData supplier = search.GetSupplierForCall();//search.GetSupplier();
      //      YelpSupplierMinData supplier = null;
            /*
           if(supplier == null)
            {
                GooglePlacesManager GoogleSearch = new GooglePlacesManager(XrmDataContext, incident);
                supplier = GoogleSearch.GetSupplier();
            }
             */
            return supplier;
        }

        private Guid GetIvrFlavor(NoProblem.Core.DataModel.AdvertiserRankings.AarSupplierEntry supplier, bool hasDescription)
        {
            DML.Xrm.new_ivrtype.IvrTypeCode ivrType;
            if (supplier.TrailStatusReason == NoProblem.Core.DataModel.Xrm.account.TrialStatusReasonCode.Accessible)
            {
                ivrType = NoProblem.Core.DataModel.Xrm.new_ivrtype.IvrTypeCode.Accessible;
            }
            else if (supplier.InitialTrialBudget == 0 || ((supplier.Balance - supplier.IncidentPrice) / (decimal)supplier.InitialTrialBudget) * 100 >= 20)
            {
                ivrType = NoProblem.Core.DataModel.Xrm.new_ivrtype.IvrTypeCode.InTrial;
            }
            else
            {
                ivrType = NoProblem.Core.DataModel.Xrm.new_ivrtype.IvrTypeCode.InTrialLast;
            }
            DataAccessLayer.IvrFlavor ivrFlavorDal = new NoProblem.Core.DataAccessLayer.IvrFlavor(XrmDataContext);
            List<Guid> flavors = ivrFlavorDal.GetEnabledFlavorsByIvrType(ivrType, hasDescription);
            int flavorCount = flavors.Count();
            if (flavorCount == 0)
            {
                throw new Exception(string.Format("No IVR flavor defined for {0}", ivrType.ToString()));
            }
            int selectedIndex = SelectRandomFlavor(flavorCount);
            return flavors[selectedIndex];
        }

        private int SelectRandomFlavor(int totalFlavors)
        {
            int left = totalFlavors;
            Random random = new Random();
            while (true)
            {
                if (left == 1 || random.NextDouble() <= (double)((double)1 / (double)left))
                {
                    break;
                }
                left--;
            }
            int indexSelected = left - 1;
            return indexSelected;
        }

        private bool IsFulfillmentDone(DML.Xrm.incident incident)
        {
            int fullfilment = incident.new_ordered_providers.Value;
            int requested = incident.new_requiredaccountno.Value;
            return requested <= fullfilment;
        }

        private void CloseServiceRequestAfterAarDone(DML.Xrm.incident incident)
        {
            incident.statuscode = (int)DML.Xrm.incident.Status.WORK_DONE;
            incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
            ServiceRequest.ServiceRequestCloser closer = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestCloser(XrmDataContext);
            closer.CloseRequest(incident);
        }

        private void OpenServiceRequestForAar(NoProblem.Core.DataModel.Xrm.incident incident, DataAccessLayer.Incident incidentDal)
        {
            incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.DELIVERED_REQUEST;
            incident.statuscode = (int)DML.Xrm.incident.Status.OnAar;
            incident.new_usedaar = true;
            incidentDal.Update(incident);
            XrmDataContext.SaveChanges();
        }
        private const string UpdateCameToLandingPageQuery = @"EXEC [dbo].[SetAarCameToLandingPage] " + 
            "@AarCallId, @browser, @UserAgent, @Platform, @ip";
        public void UpdateCameToLandingPage(AarLandingPageRequestData _AarLandingPageRequestData)
        {
            if (_AarLandingPageRequestData.isRobot)
                return;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(UpdateCameToLandingPageQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@AarCallId", _AarLandingPageRequestData.AarCallId);
                    cmd.Parameters.AddWithValue("@browser", _AarLandingPageRequestData.Browser);
                    cmd.Parameters.AddWithValue("@UserAgent", _AarLandingPageRequestData.UserAgent);
                    cmd.Parameters.AddWithValue("@Platform", _AarLandingPageRequestData.Platform);
                    cmd.Parameters.AddWithValue("@ip", _AarLandingPageRequestData.IP);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }
        #endregion

        public AarCallIncidentData GetSupplierByCallId(Guid aarCallId)
        {
       //     AarVideoTestResponse response = this.GetAarVideoTest(aarCallId);
            AarIncident response = this.GetAarVideoLead(aarCallId);
            if (response == null)
                return null;

            return new AarCallIncidentData
            {
                AarCallId = aarCallId,
                HasRequested = false,
                Incident = response
                /*
                Incident = new AarIncident
                {
                    Category = response.CategoryName,
                    ZipCode = response.ZipCode,
                    Id = response.IncidentId,
                    VideoUrl = response.VideoUrl,
                    ThumbnailUrl = response.ImageUrl
                }
                 */
            };
            

           /*

            DataAccessLayer.Yelp.YelpAARCall callsDal = new DataAccessLayer.Yelp.YelpAARCall(XrmDataContext);
            new_yelpaarcall aarCall = callsDal.All.SingleOrDefault(c => c.new_yelpaarcallid == aarCallId);
            if(aarCall == null || aarCall.new_incidentid.HasValue == false)
                return null;

            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(aarCall.new_incidentid.Value);
            if (incident == null)
                return null;

            var result = new AarCallIncidentData
            {
                AarCallId = aarCallId,
                HasRequested = aarCall.new_yelpsupplierid.HasValue,
                Incident = new AarIncident
                {
                    Category = incident.new_new_primaryexpertise_incident.new_name,
                    ZipCode = incident.new_new_region_incident != null ? incident.new_new_region_incident.new_name : string.Empty,
                    Id = aarCall.new_incidentid.Value
                }
            };
            return result;
            * */
        }
        public void GetSmsCallBack(string FromPhone, string message)
        {
            
            string phone = GetCleanPhoneFromTwillio(FromPhone);
       //     Guid YelpSupplierId = Guid.Empty;
       //     LogUtils.MyHandle.WriteToLog("GetSmsCallBack From: {0}, Fix Phone: {1}, Message: {2}", FromPhone,phone, message);
            if (!string.IsNullOrEmpty(phone))
            {
                string command = "EXEC [dbo].[SetDoNotCallMeBySMS] @phone, @OriginalPhone, @SetDoNotCallMe, @Message";
                using(SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using(SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@phone", phone);
                        cmd.Parameters.AddWithValue("@OriginalPhone", FromPhone);
                        cmd.Parameters.AddWithValue("@SetDoNotCallMe", (message.ToLower() == "remove"));
                        cmd.Parameters.AddWithValue("@Message", message);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
          //  InsertCallbackSMS(FromPhone, message, YelpSupplierId);
        }
        /*
        private void InsertCallbackSMS(string FromPhone, string message, Guid YelpSupplierId)
        {
            string command = @"
INSERT INTO dbo.CallbackTextMessage
	(YelpSupplierId, Phone, [Message])
VALUES
	(@YelpSupplierId, @phone, @Message)";
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    if (YelpSupplierId == Guid.Empty)
                        cmd.Parameters.AddWithValue("@YelpSupplierId", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@YelpSupplierId", YelpSupplierId);
                    cmd.Parameters.AddWithValue("@phone", FromPhone);
                    cmd.Parameters.AddWithValue("@Message", message);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
         * */
        private string GetCleanPhoneFromTwillio(string phone)
        {
            if (string.IsNullOrEmpty(phone))
                return null;
            //+17076907398
            if(phone.Length > 10)
                phone = phone.Substring(phone.Length - 10, 10);
            string result = string.Empty;
            foreach (char c in phone)
            {
                int i = (int)c;
                if (i > 47 && i < 58)
                    result += c;
            }
            

            return result;
        }
        public AarIncident GetAarVideoLead(Guid callId)
        {
            AarIncident response = null;
            string command = "EXEC [dbo].[GetAarVideoLead] @AarCallId";
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@AarCallId", callId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["New_videoUrl"] != DBNull.Value)
                        {
                            NoProblem.Core.BusinessLogic.SupplierBL.MobileQueries mq = new NoProblem.Core.BusinessLogic.SupplierBL.MobileQueries();
                            response = new AarIncident();
                            
                            response.VideoUrl = (string)reader["New_videoUrl"];
                            response.ThumbnailUrl = (string)reader["New_PreviewVideoPicUrl"];
                            response.Category = (string)reader["CategoryName"];
                            response.ZipCode = (string)reader["RegionName"];
                            response.RegionFullName = mq.GetResponseAddress((string)reader["RegionFullName"]);
                            response.Id = (Guid)reader["IncidentId"];
                            response.VideoDuration = (int)reader["New_VideoDuration"];
                            response.IsConnected = reader["IncidentAccountId"] != DBNull.Value;
                            response.VideoType = GetVideoHtml5Type(response.VideoUrl);

                            
                        }
                    }
                    conn.Close();
                }
            }
            if (response == null)
                return null;
            NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager sm = new ServiceRequest.ServiceRequestManager(XrmDataContext);
            incident.Status IncStatus;
            response.IsOpen = sm.IsMobileIncidentOpen(response.Id, out IncStatus);
            return response;
        }
        private string GetVideoHtml5Type(string videoUrl)
        {
            return videoUrl.EndsWith(".m3u8") ? "application/x-mpegURL" : "video/mp4";
        }
        public AarIncident GetAarVideoTest(Guid callId)
        {
            string location = @"https://s3.amazonaws.com/aarclip/";
            
            string command = "EXEC [dbo].[GetAarVideoTest] @AarCallId ";
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@AarCallId", callId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.Read())
                        return null;

                    AarIncident response = new AarIncident
                    {
                        VideoUrl = location + (string) reader["FileName"],
                        ThumbnailUrl = location + (string)reader["Image"],
                        Category = (string)reader["CategoryName"],
                        ZipCode = (string) reader["ZipCode"],
                        Id = (Guid) reader["IncidentId"]
                    };

                    conn.Close();
                    return response;                    
                }
            }
        }

        public void FireEvent(string eventName, Guid callId, Guid incidentId)
        {
            DataAccessLayer.Yelp.YelpAARCall callsDal = new DataAccessLayer.Yelp.YelpAARCall(XrmDataContext);
            new_yelpaarcall aarCall = callsDal.All.SingleOrDefault(c => c.new_yelpaarcallid == callId);
            if (aarCall == null || aarCall.new_incidentid.HasValue == false)
                return;

            if (incidentId != aarCall.new_incidentid.Value)
                return;

            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(null);
            incident incident = incidentDal.Retrieve(incidentId);
            if (incident == null)
                return;

            Guid? supplierId = aarCall.new_yelpsupplierid;
            if (supplierId.HasValue == false)
                return;

            var dispatcher = new EventDispatcher();
            dispatcher.DispatchEvent(eventName, aarCall);

            callsDal.Update(aarCall);
            XrmDataContext.SaveChanges();
        }
        public void LandingOnGetAppLink(Guid callId)
        {
            string command = "EXEC [dbo].[AarLandingOnGetAppLink] @CallId";
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@CallId", callId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        public void ReportLead(Guid callId, Guid incidentId, int reportType, string commentText)
        {
            
        }
        public NoProblem.Core.DataModel.ClipCall.Request.AddAarSupplierToLeadResponse AddAarSupplierToLead(Guid incidentId, string phoneNumber, string supplierName)
        {
            NoProblem.Core.DataModel.ClipCall.Request.AddAarSupplierToLeadResponse _response = new DML.ClipCall.Request.AddAarSupplierToLeadResponse();
            if (string.IsNullOrEmpty(supplierName))
            {

            }
            phoneNumber = GetCleanPhoneFromTwillio(phoneNumber);
            if(!IsValidatePhoneNumber(phoneNumber))
            {
                _response.isSuccess = false;
                _response.status = DML.ClipCall.Request.AddAarSupplierToLeadResponse.AddAarSupplierStatus.INVALID_PHONE_NUMBER;
                return _response;
            }
            NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager srm = new ServiceRequest.ServiceRequestManager(null);
            DML.Xrm.incident.Status incidentStatus;
            if (!srm.IsMobileIncidentOpen(incidentId, out incidentStatus))
            {
                _response.isSuccess = false;
                _response.status = DML.ClipCall.Request.AddAarSupplierToLeadResponse.AddAarSupplierStatus.INVALID_INCIDENT;
                return _response;
            }
            if(incidentStatus == incident.Status.IN_REVIEW)
            {
                _response.isSuccess = false;
                _response.status = DML.ClipCall.Request.AddAarSupplierToLeadResponse.AddAarSupplierStatus.INCIDENT_IN_REVIEW;
                return _response;
            }
            DataAccessLayer.Incident incidentDAL = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DML.Xrm.incident _incident = incidentDAL.Retrieve(incidentId);
            CustomAarManager cam = new CustomAarManager(XrmDataContext, _incident, supplierName, phoneNumber);
            cam.GetSuppliersFromApi();            
            NoProblem.Core.DataAccessLayer.Aar.AarRecord ar = NoProblem.Core.DataAccessLayer.Aar.AarRecord.GetAarRecord(DEFAULT_PROMPT_NAME);
            Guid AarIncidentId = CreateAarIncident(ar, _incident, new_aarincident.Source.PROMOTING);
            ExecuteTextMessage(incidentId, _incident.new_telephone1, cam.GetYelpSupplierId, phoneNumber, ar, AarIncidentId);
            _response.isSuccess = true;
            _response.status = DML.ClipCall.Request.AddAarSupplierToLeadResponse.AddAarSupplierStatus.SUCCESS;
            return _response;
        }
        private bool IsValidatePhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
                return false;
            string regexPattern = @"^\d{2,3}\d{7}$";
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(regexPattern);
            return regex.IsMatch(phoneNumber);
        }
        
    }
    /*
    public class AarVideoResponse
    {
        public string VideoUrl { get; set; }
        public int VideoDuration { get; set; }
        public string ImageUrl { get; set; }
        public Guid IncidentId { get; set; }
        public string CategoryName { get; set; }
        public string ZipCode { get; set; }
    //    public bool IsLead
    }
     */
    public class AarLandingPageRequestData
    {
        public Guid AarCallId { get; set; }
        public string UserAgent { get; set; }
        public string Browser { get; set; }
        public string Platform { get; set; }
        public string IP { get; set; }
        public bool isRobot { get; set; }
    }

    public class AarCallIncidentData
    {
        public bool HasRequested { get; set; }
        public Guid AarCallId { get; set; }
        public AarIncident Incident { get; set; }
    }

    public class AarIncident
    {
        public string VideoUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Category { get; set; }
        public string ZipCode { get; set; }
        public string RegionFullName { get; set; }
        public int VideoDuration { get; set; }
        public Guid Id { get; set; }
        public bool IsOpen { get; set; }
        public bool IsConnected { get; set; }
        public string VideoType { get; set; }
    }
    public class AarException : Exception
    {
        public AarException() { }
        public AarException(string message) : base(message) { }
    }
    public class ConnectSupplierToCustomerResponse
    {
        public bool IsSuccess { get; set; }
        public Message message { get; set; }
        public enum Message
        {
            BAD_ARGUMENT,
            DONT_HAVE_FREE_LEAD,
            INCIDENT_CLOSE,
            SUCCESS,
            FAILED
        }
    }
    internal class AarTextMessageJob
    {
        public NoProblem.Core.DataModel.Xrm.incident incident{get;set;}
        public List<YelpSupplierMinData> suppliers { get; set; }
        public NoProblem.Core.DataAccessLayer.Aar.AarRecord ar { get; set; }
        public Guid AarIncidentId { get; set; }
    }
}
