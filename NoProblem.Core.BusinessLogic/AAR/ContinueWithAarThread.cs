﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class ContinueWithAarThread : Runnable
    {
        private Guid incidentId;

        public ContinueWithAarThread(Guid incidentId)
        {
            this.incidentId = incidentId;
        }

        protected override void Run()
        {
            AarManager aarMananger = new AarManager(XrmDataContext);
            aarMananger.ContinueAarServiceRequest(incidentId);
        }
    }
}
