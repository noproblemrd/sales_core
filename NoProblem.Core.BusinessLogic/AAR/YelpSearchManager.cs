﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class YelpSearchManager : AarSearchManagerBase
    {
        /*
        private DataModel.Xrm.incident incident;
        private Guid cityId;
        private string locationName;
        private DataModel.Xrm.new_primaryexpertise category;
        private Guid yelpSearchId;
        private DataAccessLayer.Yelp.YelpSupplier yelpSupplierDal;
        private DataAccessLayer.Yelp.YelpSearchResult searchResultDal;
        private static DataAccessLayer.Generic genericDal = new DataAccessLayer.Generic();
        private static readonly bool isProduction = GlobalConfigurations.IsProductionUsa();
        */
        public YelpSearchManager(DataModel.Xrm.DataContext xrmDataContext, DataModel.Xrm.incident incident)
            : base(xrmDataContext, incident)
        {
            /*
            this.incident = incident;
            this.category = incident.new_new_primaryexpertise_incident;
            DataAccessLayer.Region regionDal = new DataAccessLayer.Region(XrmDataContext);
            var parents = regionDal.GetParents(incident.new_new_region_incident);
            var city = parents.First(x => x.new_level == 3);
            cityId = city.new_regionid;
            var state = parents.First(x => x.new_level == 1);
            locationName = city.new_name + ", " + state.new_name;
            yelpSupplierDal = new DataAccessLayer.Yelp.YelpSupplier(XrmDataContext);
            searchResultDal = new DataAccessLayer.Yelp.YelpSearchResult(XrmDataContext);
             * */
            AarSource = DataModel.Xrm.new_yelpsearch.eAarSearchSource.YELP;
        }
        /*
        public override YelpSupplierMinData GetSupplier()
        {
            IEnumerable<Guid> uneligibleSuppliers = GetAlreadyCalledSuppliers();
            SearchId = GetLastSearchId();
            if(SearchId == Guid.Empty)
                GetSuppliersFromApi();
            if (SearchId == Guid.Empty)
                return null;
            List<YelpSupplierMinData> suppliers = GetSuppliersFromLastYelpSearch(uneligibleSuppliers);
            
            if (suppliers == null || suppliers.Count == 0)
            {
                /*
                bool newSuppliersFound = GetSuppliersFromApi();
                if (suppliers == null || newSuppliersFound)
                    suppliers = GetSuppliersFromLastYelpSearch(uneligibleSuppliers);
                if (suppliers == null || suppliers.Count == 0)
                    return null;
                 * * /
                return null;
            }
            YelpSupplierMinData chosenSupplier = ChooseSupplierByRankAndBusinessLogic(suppliers);
            return chosenSupplier;
        }
         */
        internal override void GetSuppliersFromApi()        
        {
         //   bool newSuppliersFound = false;
            DataModel.Xrm.new_yelpsearch Ysearch = CreateNewYelpSearchEntry();
            int retrieved = 0;
            try
            {
                var yelp = YelpHelper.GetYelp();
                YelpSharp.Data.Options.SearchOptions options = new YelpSharp.Data.Options.SearchOptions();
                options.GeneralOptions = new YelpSharp.Data.Options.GeneralOptions();
                options.GeneralOptions.sort = 2;
                options.GeneralOptions.term = category.new_name;
                options.LocationOptions = new YelpSharp.Data.Options.LocationOptions() { location = locationName };

                int max = 300;
                int total;
                do
                {
                    var results = yelp.Search(options);
                    total = results.total;
                    if (results.businesses == null && results.businesses.Count == 0)
                        break;
                    retrieved += results.businesses.Count;
                    options.GeneralOptions.offset = retrieved;
                    //         newSuppliersFound |= UpsertYelpSuppliers(results.businesses);
                    UpsertYelpSuppliers(results.businesses);
                }
                while (retrieved < total && retrieved < max);
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception search Yelp API");
            }
            if(retrieved == 0)
            {
                DataAccessLayer.Yelp.YelpSearch dal = new DataAccessLayer.Yelp.YelpSearch(XrmDataContext);
                dal.Delete(Ysearch);
                XrmDataContext.SaveChanges();
                SearchId = Guid.Empty;
            }
       //     return newSuppliersFound;
        }

       

        private void UpsertYelpSuppliers(List<YelpSharp.Data.Business> list)
        {
         //   bool newDataFound = false;
            foreach (var supplier in list)
            {
                if (string.IsNullOrEmpty(supplier.phone))
                    continue;
                Guid yelpSupplierId = FindExistingYelpSupplier(supplier.phone);
                if (yelpSupplierId != Guid.Empty)
                {
                //    newDataFound |= UpdateYelpSupplier(yelpSupplierId);
                    UpdateYelpSupplier(yelpSupplierId, new decimal(supplier.rating), supplier.id, supplier.mobile_url, supplier.review_count, supplier.image_url);
                }
                else
                {
                 //   newDataFound = true;
                    InsertYelpSupplier(GetYelpSupplierData(supplier));
                }
            }
        //    return newDataFound;
        }
        protected DataModel.Xrm.new_yelpsupplier GetYelpSupplierData(YelpSharp.Data.Business supplier)
        {
            DataModel.Xrm.new_yelpsupplier yelpSupplier = new DataModel.Xrm.new_yelpsupplier();
            yelpSupplier.new_name = supplier.name;
            yelpSupplier.new_yelpid = supplier.id;
        //    yelpSupplier.new_phone = isProduction ? supplier.phone : "0543388917";
            yelpSupplier.new_phone = supplier.phone;
            yelpSupplier.new_rating = new decimal(supplier.rating);
            yelpSupplier.new_googlerating = new decimal(0);
            yelpSupplier.new_yelpurl = supplier.mobile_url;
            yelpSupplier.new_yelpreviewcount = supplier.review_count;
            yelpSupplier.new_yelpbusinessicon = supplier.image_url;
            if (supplier.location != null && supplier.location.coordinate != null)
            {
                yelpSupplier.new_latitude = new decimal(supplier.location.coordinate.latitude);
                yelpSupplier.new_longitude = new decimal(supplier.location.coordinate.longitude);
            }
            yelpSupplier.new_address = CreateAddressFromYelpArray(supplier.location.display_address);
            yelpSupplier.new_callcount = 0;
            yelpSupplier.new_dontcallme = false;
            yelpSupplier.new_successfulcallcount = 0;
            return yelpSupplier;
        }
      
        private string CreateAddressFromYelpArray(string[] addressArray)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < addressArray.Length; i++)
            {
                builder.Append(addressArray[i]);
                if (i < addressArray.Length - 1)
                {
                    builder.Append(", ");
                }
            }
            return builder.ToString();
        }
        
        protected override string queryUpdateYelpSupplier
        {
            get 
            {
                return @"UPDATE dbo.New_yelpsupplierExtensionBase
	SET New_Rating = @Rating,
		new_yelpid = @AarId,
        new_yelpurl = @AarUrl,
        New_yelpReviewCount = @ReviewCount,
        New_yelpBusinessIcon = @IconUrl
	WHERE New_yelpsupplierId = @YelpSupplierId";
            }
        }
       /*
        protected override string queryGetSuppliersFromLastSearch { 
      //  private const string queryGetSuppliersFromLastSearch =
            get
            {
                return @"
select ys.new_yelpsupplierid, ys.new_phone, ys.new_rating, ys.New_GoogleRating, ys.new_callcount, ys.new_successfulcallcount
from new_yelpsupplier ys with(nolock)
join new_yelpsearchresult sr with(nolock) on ys.new_yelpsupplierid = sr.new_yelpsupplierid
where sr.new_yelpsearchid = @searchId
and ys.new_dontcallme = 0
order by ys.new_callcount asc, ys.new_rating desc, ys.new_successfulcallcount desc
";
            }
        }
        */

     

        

        
    }
}
