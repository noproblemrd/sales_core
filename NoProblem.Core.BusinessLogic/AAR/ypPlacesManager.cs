﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class ypPlacesManager : AarSearchManagerBase
    {
        public ypPlacesManager(DataModel.Xrm.DataContext xrmDataContext, DataModel.Xrm.incident incident)
            : base(xrmDataContext, incident)
        {
            this.AarSource = DataModel.Xrm.new_yelpsearch.eAarSearchSource.YP;
        }
        internal override void GetSuppliersFromApi()
        {
            YPApiHelper helper = new YPApiHelper(category, incident, zipcode);
            List<YP_SearchListing> list = null;
            try
            {
                list = helper.GetSuppliersFromApi();
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception search ypPlacesManager inicentId:{0}", incident.incidentid);
            }
            if (list == null || list.Count == 0)
                return;
            CreateNewYelpSearchEntry();
            UpsertYelpSuppliers(list);
        }
        private void UpsertYelpSuppliers(List<YP_SearchListing> list)
        {
            //     bool newDataFound = false;
            foreach (YP_SearchListing supplier in list)
            {
               
                if (string.IsNullOrEmpty(supplier.phone))
                    continue;
                supplier.phone = GetCleanPhone(supplier.phone);
                Guid yelpSupplierId = FindExistingYelpSupplier(supplier.phone);

                if (yelpSupplierId != Guid.Empty)
                {
                    //      newDataFound |= UpdateYelpSupplier(yelpSupplierId);
                    UpdateYelpSupplier(yelpSupplierId, new decimal(supplier.averageRating), supplier.listingId.ToString(), supplier.moreInfoURL,
                        supplier.ratingCount, supplier.adImage);
                }
                else
                    if (yelpSupplierId == Guid.Empty)
                    {
                        //             newDataFound = true;
                        InsertYelpSupplier(GetYelpSupplierData(supplier));
                    }
            }
            //    return newDataFound;
        }
        protected override string queryUpdateYelpSupplier
        {
            get
            {
                return @"
UPDATE dbo.New_yelpsupplierExtensionBase
SET New_YPRating = @Rating,
    New_YPId = @AarId,
    New_YPUrl = @AarUrl,
    New_YPReviewCount = @ReviewCount,
    New_YPBusinessIcon = @IconUrl
WHERE New_yelpsupplierId = @YelpSupplierId";
            }
        }
        private DataModel.Xrm.new_yelpsupplier GetYelpSupplierData(YP_SearchListing supplier)
        {
            DataModel.Xrm.new_yelpsupplier yelpSupplier = new DataModel.Xrm.new_yelpsupplier();
            yelpSupplier.new_name = supplier.businessName;
            yelpSupplier.new_ypid = supplier.listingId.ToString();
            //   yelpSupplier.new_phone = isProduction ? supplier.details.formatted_phone_number : "0543388917";
            yelpSupplier.new_phone = supplier.phone;
            yelpSupplier.new_yprating = new decimal(supplier.averageRating);
            //  yelpSupplier.new_rating = new decimal(0);
            yelpSupplier.new_ypurl = supplier.moreInfoURL;
            yelpSupplier.new_ypreviewcount = supplier.ratingCount;
            yelpSupplier.new_ypbusinessicon = supplier.adImage;
            if (supplier.latitude is double)
                yelpSupplier.new_latitude = new decimal((double)supplier.latitude);
            if (supplier.longitude is double)
                yelpSupplier.new_longitude = new decimal((double)supplier.longitude);

            yelpSupplier.new_address = GetAddress(supplier.street, supplier.city, supplier.state, supplier.zip.ToString());
            yelpSupplier.new_callcount = 0;
            yelpSupplier.new_dontcallme = false;
            yelpSupplier.new_successfulcallcount = 0;
            return yelpSupplier;
        }
    }
}
