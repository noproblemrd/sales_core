﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    
    internal abstract class AarSearchManagerBase : XrmUserBase
    {
        protected DataModel.Xrm.incident incident;
        protected Guid cityId;
        protected string zipcode;
        protected string locationName;
        protected DataModel.Xrm.new_primaryexpertise category;
        protected Guid SearchId;
        protected DataAccessLayer.Yelp.YelpSupplier SupplierDal;
        protected DataAccessLayer.Yelp.YelpSearchResult searchResultDal;        
        protected static DataAccessLayer.Generic genericDal = new DataAccessLayer.Generic();
        protected static readonly bool isProduction = GlobalConfigurations.IsProductionUsa();
        protected DataModel.Xrm.new_yelpsearch.eAarSearchSource AarSource;

        public AarSearchManagerBase(DataModel.Xrm.DataContext xrmDataContext, DataModel.Xrm.incident incident)
            : base(xrmDataContext)
        {
            this.incident = incident;
            this.category = incident.new_new_primaryexpertise_incident;
            DataAccessLayer.Region regionDal = new DataAccessLayer.Region(XrmDataContext);
            zipcode = incident.new_new_region_incident.new_name;
            var parents = regionDal.GetParents(incident.new_new_region_incident);
            var city = parents.First(x => x.new_level == 3);
            cityId = city.new_regionid;
            var state = parents.First(x => x.new_level == 1);
            locationName = city.new_name + ", " + state.new_name;
            SupplierDal = new DataAccessLayer.Yelp.YelpSupplier(XrmDataContext);
            searchResultDal = new DataAccessLayer.Yelp.YelpSearchResult(XrmDataContext);
        }
        internal Guid GetSearchId
        {
            get
            {
                return SearchId;
            }
        }
        /*
        public virtual YelpSupplierMinData GetSupplier()
        {
           
            CheckUpdateLastSearchId();
            IEnumerable<Guid> uneligibleSuppliers = GetAlreadyCalledSuppliers();
            List<YelpSupplierMinData> suppliers = GetSuppliersFromLastYelpSearch(uneligibleSuppliers);

            if (suppliers == null || suppliers.Count == 0)
            {
                
                return null;
            }
            YelpSupplierMinData chosenSupplier = ChooseSupplierByRankAndBusinessLogic(suppliers);
            return chosenSupplier;
        }
         * */
        public void CheckUpdateLastSearchId()
        {
            SearchId = GetLastSearchId();
            if (SearchId == Guid.Empty)
            {
                GetSuppliersFromApi();
    //            if (SearchId == Guid.Empty)
   //                 return null;
            }
        }
        /*
        public virtual IEnumerable<YelpSupplierMinData> GetSupplier(int? count)
        {
            CheckUpdateLastSearchId();
           
     //       IEnumerable<Guid> uneligibleSuppliers = GetAlreadyCalledSuppliers();
            List<YelpSupplierMinData> suppliers = GetSuppliersFromLastYelpSearch();
            return count.HasValue ? suppliers.Take(count.Value) : suppliers;
           
        }
         * */
        protected DataModel.Xrm.new_yelpsearch CreateNewYelpSearchEntry()
        {
            DataAccessLayer.Yelp.YelpSearch dal = new DataAccessLayer.Yelp.YelpSearch(XrmDataContext);
            DataModel.Xrm.new_yelpsearch searchEntry = new DataModel.Xrm.new_yelpsearch();
            searchEntry.new_regionid = cityId;
            searchEntry.new_primaryexpertiseid = category.new_primaryexpertiseid;
            searchEntry.new_source = (int)AarSource;
            dal.Create(searchEntry);
            XrmDataContext.SaveChanges();
            SearchId = searchEntry.new_yelpsearchid;
            return searchEntry;
        }
        /*
        protected void InsertYelpSupplier(object supplier)
        {
            DataModel.Xrm.new_yelpsupplier ys = GetYelpSupplierData(supplier);
            _InsertYelpSupplier(ys);
        }
         * */
        protected Guid InsertYelpSupplier(DataModel.Xrm.new_yelpsupplier yelpSupplier)
        {
    
            SupplierDal.Create(yelpSupplier);
            XrmDataContext.SaveChanges();
            CreateSearchResultEntry(yelpSupplier.new_yelpsupplierid);
            _UpdateAarSupplier(yelpSupplier.new_yelpsupplierid);
            return yelpSupplier.new_yelpsupplierid;
        }
        protected void CreateSearchResultEntry(Guid yelpSupplierId)
        {
            DataModel.Xrm.new_yelpsearchresult searchResult = new DataModel.Xrm.new_yelpsearchresult();
            searchResult.new_yelpsupplierid = yelpSupplierId;
            if (SearchId != Guid.Empty)
                searchResult.new_yelpsearchid = SearchId;
            searchResultDal.Create(searchResult);
            XrmDataContext.SaveChanges();
        }
        private const string GetLastSearchIdQuery = @"
SELECT TOP 1 S.New_yelpsearchId
FROM [dbo].[New_yelpsearchExtensionBase] S  WITH(NOLOCK)
	inner join dbo.New_yelpsearchBase SBASE  WITH(NOLOCK) on S.New_yelpsearchId = SBASE.New_yelpsearchId
WHERE New_RegionId = @regionId
	and New_PrimaryExpertiseId = @PrimaryExpertiseId
    and New_Source = @source
	and DATEDIFF ( DAY , SBASE.CreatedOn , GETDATE() ) < 30	
order by SBASE.CreatedOn desc
";
        
        protected Guid GetLastSearchId()
        {
            Guid _id;
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GetLastSearchIdQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@regionId", cityId);
                    cmd.Parameters.AddWithValue("@PrimaryExpertiseId", category.new_primaryexpertiseid);
                    cmd.Parameters.AddWithValue("@source", (int)AarSource);
                    object obj = cmd.ExecuteScalar();
                    _id = (obj == null || obj == DBNull.Value) ?  Guid.Empty : (Guid)obj;
                    conn.Close();
                }
            }
            return _id;
            
        }
    /*
        protected IEnumerable<Guid> GetAlreadyCalledSuppliers()
        {
            DataAccessLayer.Yelp.YelpAARCall callsDal = new DataAccessLayer.Yelp.YelpAARCall(XrmDataContext);
            var query = from s in callsDal.All
                        where s.new_incidentid == incident.incidentid
                        select s.new_yelpsupplierid.Value;
            return query;
        }
     * */
        /*
       protected virtual List<YelpSupplierMinData> GetSuppliersFromLastYelpSearch()
       {
           return GetSuppliersFromLastYelpSearch(new List<Guid>());
       }
       
       protected List<YelpSupplierMinData> GetSuppliersFromLastYelpSearch(IEnumerable<Guid> uneligibleSuppliers)
       {
           
           DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
//           parameters.Add("@searchId", lastSearchId);
           parameters.Add("@searchId", SearchId);
           var reader = genericDal.ExecuteReader(queryGetSuppliersFromLastSearch, parameters);
           List<YelpSupplierMinData> retVal = new List<YelpSupplierMinData>();
           foreach (var row in reader)
           {
               Guid yelpSupplierId = (Guid)row["new_yelpsupplierid"];
               if (!uneligibleSuppliers.Contains<Guid>(yelpSupplierId))
               {
                   YelpSupplierMinData data = new YelpSupplierMinData();
                   data.YelpSupplierId = yelpSupplierId;
                   data.CallCount = (int)row["new_callcount"];
                   data.Phone = Convert.ToString(row["new_phone"]);
                   data.YelpRating = row["new_rating"] == DBNull.Value ? 0 : (decimal)row["new_rating"];
                   data.GoogleRating = row["New_GoogleRating"] == DBNull.Value ? 0 : (decimal)row["New_GoogleRating"];
                   data.SuccessfulCallCount = (int)row["new_successfulcallcount"];
                   retVal.Add(data);
               }
           }
           return retVal;
       }
        * */

       /* 
        protected YelpSupplierMinData ChooseSupplierByRankAndBusinessLogic(List<YelpSupplierMinData> suppliers)
        {
            return suppliers.FirstOrDefault(x => x.CallCount < 5 && x.SuccessfulCallCount < 2);
        }
        * */
        
   //     protected abstract Guid FindExistingAarSupplier(string Id);
   //     protected abstract DataModel.Xrm.new_yelpsupplier GetYelpSupplierData<T>(T supllier);
        internal abstract void GetSuppliersFromApi();
 //       protected abstract string queryGetSuppliersFromLastSearch { get; }
        protected abstract string queryUpdateYelpSupplier { get; }
        public const string queryUpdateYelpSupplierRegionExpertise = "EXEC [dbo].[UpdateAarSupplier]	@YelpSupplierId, @PrimaryExpertiseId, @RegionId";
        protected virtual void UpdateYelpSupplier(Guid yelpSupplierId, decimal rating, string AarId, string AarUrl, int reviewCount, string iconUrl)
        {
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(queryUpdateYelpSupplier, conn))
                {
                    
         //           cmd.Parameters.AddWithValue("@phone", phoneNumber);
                    cmd.Parameters.AddWithValue("@Rating", rating);
                    cmd.Parameters.AddWithValue("@AarId", AarId);
                    cmd.Parameters.AddWithValue("@YelpSupplierId", yelpSupplierId);
                    cmd.Parameters.AddWithValue("@AarUrl", AarUrl);
                    cmd.Parameters.AddWithValue("@ReviewCount", reviewCount);
                    if(string.IsNullOrEmpty(iconUrl))
                        cmd.Parameters.AddWithValue("@IconUrl", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@IconUrl", iconUrl);
     //               cmd.Parameters.AddWithValue("@PrimaryExpertiseId", category.new_primaryexpertiseid);
     //               cmd.Parameters.AddWithValue("@RegionId", cityId);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                
                conn.Close();
            }
            _UpdateAarSupplier(yelpSupplierId);
            CreateSearchResultEntry(yelpSupplierId);
        }
        protected void _UpdateAarSupplier(Guid yelpSupplierId)
        {
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(queryUpdateYelpSupplierRegionExpertise, conn))
                {
                    cmd.Parameters.AddWithValue("@YelpSupplierId", yelpSupplierId);
                    cmd.Parameters.AddWithValue("@PrimaryExpertiseId", category.new_primaryexpertiseid);
                    cmd.Parameters.AddWithValue("@RegionId", cityId);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        
        }
        protected virtual Guid FindExistingYelpSupplier(string phone)
        {
            Guid id;
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(FindExistingYelpSupplierQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@phone", phone);
                    object obj = cmd.ExecuteScalar();
                    if (obj == null || obj == DBNull.Value)
                        id = Guid.Empty;
                    else
                        id = (Guid)obj;
                    conn.Close();
                }
            }
            return id;
        }
        const string FindExistingYelpSupplierQuery = @"
SELECT  New_yelpsupplierId
FROM dbo.New_yelpsupplierExtensionBase 
WHERE New_Phone = @phone";
        protected string GetCleanPhone(string phone)
        {
            string result = string.Empty;
            foreach (char c in phone)
            {
                int i = (int)c;
                if (i > 47 && i < 58)
                    result += c;
            }
            return result;
        }
        protected virtual string GetAddress(string street, string city, string state, string postalCode)
        {
            return street + ", " + city + ", " + state + ", " + postalCode;
        }
    }
    
}
