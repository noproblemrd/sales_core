﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    public class AarReportManager
    {
        public List<AarCallCenterResponse> CreateAarCallCenterReport(AarCallCenterRequest request)
        {
            List<AarCallCenterResponse> list = new List<AarCallCenterResponse>();
            string command = "EXEC [dbo].[AarCallCenterReport] @from, @to";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@from", request.From);
                    cmd.Parameters.AddWithValue("@to", request.To);

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        AarCallCenterResponse data = new AarCallCenterResponse();
                        data.IncidentId = (Guid)reader["IncidentId"];
                        data.CreatedOn = (DateTime)reader["CreatedOn"];
                        data.CaseNumber = (string)reader["CaseNumber"];
                        data.Category = (string)reader["Category"];
                        data.Name = (string)reader["Name"];
                        data.Phone = (string)reader["Phone"];
                        data.Priority = (string)reader["Priority"];
                        data.Region = (string)reader["Region"];
                        data.VideoDuration = reader["VideoDuration"] == DBNull.Value ? 0 : (int)reader["VideoDuration"];
                        data.VideoLink = reader["VideoLink"] == DBNull.Value ? string.Empty : (string)reader["VideoLink"];
                        data.WatchVideo = (string)reader["WatchVideo"];
                        data.CallRecord = reader["CallRecord"] == DBNull.Value ? string.Empty : (string)reader["CallRecord"];
                        data.IsQaTest = reader["IsQaTest"] == DBNull.Value ? false : (bool)reader["IsQaTest"];
                        list.Add(data);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        public class AarCallCenterRequest
        {
            public DateTime From { get; set; }
            public DateTime To { get; set; }
        }
        public class AarCallCenterResponse
        {
            public Guid IncidentId { get; set; }
            public DateTime CreatedOn { get; set; }
            public string CaseNumber { get; set; }
            public string Category { get; set; }
            public string VideoLink { get; set; }
            public int VideoDuration { get; set; }
            public string Region { get; set; }
            public string Name { get; set; }
            public string Phone { get; set; }
            public string Priority { get; set; }
            public string WatchVideo { get; set; }
            public string CallRecord { get; set; }
            public bool IsQaTest { get; set; }
        }
    }
}
