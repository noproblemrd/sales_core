﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Json;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class CitygridApiHelper
    {
        #region Static
        static readonly string CitygridKey;
        static CitygridApiHelper()
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(null);
            CitygridKey = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.CITYGRID_API_KEY);
        }
        #endregion
        private DataModel.Xrm.new_primaryexpertise category;
        DataModel.Xrm.incident incident;
        string locationName;
        internal CitygridApiHelper(DataModel.Xrm.new_primaryexpertise category, DataModel.Xrm.incident incident, string locationName) 
        {
            this.category = category;
            this.incident = incident;
            this.locationName = locationName;
         }
        internal List<CitygridSupplierLocation> GetSuppliersFromApi()
        {

            //       RestSharp.RestClient client = new RestSharp.RestClient();
            RestSharp.RestClient client = new RestSharp.RestClient(GetSearchURL());
            

            //AIzaSyDJQ_-mhV9NHoI-q9kOS23Soqx97e9rq5E

            CitygridSupplierJson CitygridResult;
            
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
            SetParametersRestRequest(request);
            var response = client.Execute(request);
            using (var ms = new MemoryStream(response.RawBytes))
            {
                var serializer = new DataContractJsonSerializer(typeof(CitygridSupplierJson));
                CitygridResult = (CitygridSupplierJson)serializer.ReadObject(ms);
            }


            if (CitygridResult == null || CitygridResult.results == null || CitygridResult.results.locations == null)
                return new List<CitygridSupplierLocation>();
            return CitygridResult.results.locations;
        }
        void SetParametersRestRequest(RestSharp.RestRequest request)
        {
            request.AddParameter("type", category.new_name);
            request.AddParameter("where", locationName);
            request.AddParameter("publisher", CitygridKey);
            request.AddParameter("format", "json");
        }
        private string GetSearchURL()
        {
            return "https://api.citygridmedia.com/content/places/v2/search/where";//?query=" + HttpUtility.UrlEncode(query) + "&key=" + GoogleKey;
        }
    }
}
