﻿using System;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.AAR.Events
{
    public class ViewLeadEvent : AarEvent
    {
        public override void OnFiring(new_yelpaarcall call)
        {
            if (call.new_firstvideoviewdate.HasValue == false)
                call.new_firstvideoviewdate = DateTime.UtcNow;

            if (call.new_viewvideocount.HasValue == false)
                call.new_viewvideocount = 0;

            call.new_viewvideocount++;
        }
    }
}