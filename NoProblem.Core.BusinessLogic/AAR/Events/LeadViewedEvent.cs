﻿using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.AAR.Events
{
    public class LeadViewedEvent : AarEvent
    {
        public override void OnFiring(new_yelpaarcall call)
        {
            if (call.new_leadviewed.HasValue == false || call.new_leadviewed.Value == false)
            {
                call.new_leadviewed = true;
            }
        }
    }
}