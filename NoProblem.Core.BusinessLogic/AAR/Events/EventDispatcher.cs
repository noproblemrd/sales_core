﻿using System;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.AAR.Events
{
    public class EventDispatcher
    {
        public void DispatchEvent(string eventName, new_yelpaarcall call)
        {
            string @namespace = typeof (AarEvent).Namespace;
            string fullName = string.Format(@namespace + ".{0}Event", eventName);
            Type type = Type.GetType(fullName,throwOnError:false,ignoreCase:true);
            if (type == null)
                return;

            AarEvent @event = Activator.CreateInstance(type) as AarEvent;
            if (@event == null)
                return;

            @event.OnFiring(call);
        }
    }
}
