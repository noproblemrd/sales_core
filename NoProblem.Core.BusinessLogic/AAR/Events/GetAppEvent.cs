﻿using System;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.AAR.Events
{
    public class GetAppEvent : AarEvent
    {
        public override void OnFiring(new_yelpaarcall call)
        {
            if (call.new_getapprequestcount.HasValue == false)
                call.new_getapprequestcount = 0;

            call.new_getapprequestcount++;
        }
    }
}