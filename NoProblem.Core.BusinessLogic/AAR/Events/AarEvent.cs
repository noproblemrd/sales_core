﻿using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.AAR.Events
{
    public abstract class AarEvent
    {
        public abstract void OnFiring(new_yelpaarcall call);
    }
}