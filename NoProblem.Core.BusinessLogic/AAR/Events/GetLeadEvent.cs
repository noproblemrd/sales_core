﻿using System;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.AAR.Events
{
    public class GetLeadEvent : AarEvent
    {
        public override void OnFiring(new_yelpaarcall call)
        {
            if (call.new_requestedleaddate.HasValue == false)
                call.new_requestedleaddate = DateTime.UtcNow;

            if (call.new_requestedleadcount.HasValue == false)
                call.new_requestedleadcount = 0;

            call.new_requestedleadcount++;
        }
    }
}
