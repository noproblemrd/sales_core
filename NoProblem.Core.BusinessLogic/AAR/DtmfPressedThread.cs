﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    public class DtmfPressedThread : Runnable
    {
        private Guid yelpAarCallId;
        private string digits;

        public DtmfPressedThread(Guid yelpAarCallId, string digits)
        {
            this.yelpAarCallId = yelpAarCallId;
            this.digits = digits;
        }

        protected override void Run()
        {
            AAR.AarManager manager = new AarManager(XrmDataContext);
            manager.DtmfPressed(yelpAarCallId, digits);
        }
    }
}
