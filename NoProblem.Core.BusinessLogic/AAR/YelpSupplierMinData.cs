﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class YelpSupplierMinData
    {
        public Guid YelpSupplierId { get; set; }
        public string Phone { get; set; }
        public int CallCount { get; set; }
        public int SuccessfulCallCount { get; set; }
        public decimal YelpRating { get; set; }
        public decimal GoogleRating { get; set; }
        public decimal CityGridReating { get; set; }
        public decimal YellowPagesRating { get; set; }
    }
}
