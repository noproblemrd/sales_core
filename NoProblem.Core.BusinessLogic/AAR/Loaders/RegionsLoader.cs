﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SqlHelper;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.AAR.Loaders
{
    internal class RegionsLoader : XrmUserBase
    {
        RegionMinData region;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xrmDataContext"></param>
        /// <param name="region">Must be a region in level 2</param>
        public RegionsLoader(DataModel.Xrm.DataContext xrmDataContext, RegionMinData region)
            : base(xrmDataContext)
        {
            this.region = region;
        }

        public bool DoRegion(Guid supplierId)
        {
            try
            {
                DataAccessLayer.RegionAccount regAccountDal = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);

                if (region.Level == 1)
                {
                    NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise ra = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
                    ra.new_accountid = supplierId;
                    ra.new_regionid = region.Id;
                    ra.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.Working;
                    regAccountDal.Create(ra);
                    XrmDataContext.SaveChanges();
                }
                else//should be level 2
                {
                    DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
                    var brothers = XrmDataContext.new_regions.Where(x => x.new_parentregionid.Value == region.ParentId && x.new_regionid != region.Id);

                    foreach (var bro in brothers)
                    {
                        NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise regAcc = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
                        regAcc.new_accountid = supplierId;
                        regAcc.new_regionid = bro.new_regionid;
                        regAcc.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.NotWorking;
                        regAccountDal.Create(regAcc);
                        XrmDataContext.SaveChanges();
                    }

                    NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise regAccParent = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
                    regAccParent.new_accountid = supplierId;
                    regAccParent.new_regionid = region.ParentId;
                    if (brothers.Count() > 0)
                    {
                        regAccParent.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild;
                    }
                    else
                    {
                        regAccParent.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.Working;
                    }
                    regAccountDal.Create(regAccParent);
                    XrmDataContext.SaveChanges();
                }
            }
            catch (Exception exc)
            {
                XrmDataContext.ClearChanges();
                LogUtils.MyHandle.HandleException(exc, "Exception in RegionsLoader");
                var existing = XrmDataContext.accounts.FirstOrDefault(x => x.accountid == supplierId);
                if (existing != null)
                {
                    var accExps = XrmDataContext.new_accountexpertises.Where(x => x.new_accountid == supplierId);
                    foreach (var acExp in accExps)
                    {
                        XrmDataContext.DeleteObject(acExp);
                    }

                    var regAccs = XrmDataContext.new_regionaccountexpertises.Where(x => x.new_accountid == supplierId);
                    foreach (var regAc in regAccs)
                    {
                        XrmDataContext.DeleteObject(regAc);
                    }

                    XrmDataContext.DeleteObject(existing);
                    XrmDataContext.SaveChanges();
                }
                return false;
            }
            return true;
        }
    }
}
