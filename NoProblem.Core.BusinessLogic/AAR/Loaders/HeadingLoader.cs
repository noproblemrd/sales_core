﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.AAR.Loaders
{
    internal class HeadingLoader : XrmUserBase
    {
        Guid headingId;
        Supplier supplierLogic;

        public HeadingLoader(DataModel.Xrm.DataContext xrmDataContext, Guid headingId, Supplier supplierLogic)
            :base(xrmDataContext)
        {
            this.headingId = headingId;
            this.supplierLogic = supplierLogic;
        }

        public bool DoHeading(Guid supplierId)
        {
            XElement requestXml = new XElement("SupplierExpertise");
            requestXml.Add(new XAttribute("SiteId", ""));
            requestXml.Add(new XAttribute("SupplierId", supplierId));

            requestXml.Add(
                            new XElement("PrimaryExpertise",
                                new XAttribute("ID", headingId),
                                new XAttribute("Certificate", false))
                                );
            try
            {
                supplierLogic.CreateSupplierExpertise(requestXml.ToString(), true, Guid.Empty);
            }
            catch (Exception exc)
            {
                XrmDataContext.ClearChanges();
                LogUtils.MyHandle.HandleException(exc, "Exception in HeadingLoader");
                var existing = XrmDataContext.accounts.FirstOrDefault(x => x.accountid == supplierId);
                if (existing != null)
                {
                    var accExps = XrmDataContext.new_accountexpertises.Where(x => x.new_accountid == supplierId);
                    foreach (var acExp in accExps)
                    {
                        XrmDataContext.DeleteObject(acExp);
                    }
                    XrmDataContext.DeleteObject(existing);
                    XrmDataContext.SaveChanges();
                }
                return false;
            }
            return true;
        }
    }
}
