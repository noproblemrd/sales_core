﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel;

namespace NoProblem.Core.BusinessLogic.AAR.Loaders
{
    internal class GeneralInfoLoader : XrmUserBase
    {
        int timeZone;
        int paymentAmount;

        public GeneralInfoLoader(DataModel.Xrm.DataContext xrmDataContext, int timeZone, int paymentAmount)
            : base(xrmDataContext)
        {
            this.timeZone = timeZone;
            this.paymentAmount = paymentAmount;
        }

        public Guid DoGeneralInfo(string name, string phone)
        {
            SupplierManager manager = new SupplierManager(XrmDataContext);
            var ans = manager.UpsertAdvertiser(
               new UpsertAdvertiserRequest()
               {
                   Company = name,
                   ContactPhone = phone,
                   Email = phone + "@temp.noproblemppc.com",
                   IsFromAAR = true,
                   AllowRecordings = true,
                   TimeZone = timeZone
               });
            if (ans.Status != UpsertAdvertiserStatus.Success)
            {
                return Guid.Empty;
            }
            else
            {
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                DataModel.Xrm.account supplierXrm = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
                supplierXrm.accountid = ans.SupplierId;
                supplierXrm.new_isfromaar = true;
                supplierXrm.new_initialtrialbudget = paymentAmount;
                accountRepositoryDal.Update(supplierXrm);
                XrmDataContext.SaveChanges();
                return ans.SupplierId;
            }
        }
    }
}
