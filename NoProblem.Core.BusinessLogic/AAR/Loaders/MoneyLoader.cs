﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierPricing;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.AAR.Loaders
{
    internal class MoneyLoader : XrmUserBase
    {
        Guid voucherReasonId;
        string voucherNumber;
        int paymentAmount;

        public MoneyLoader(DataModel.Xrm.DataContext xrmDataContext, Guid voucherReasonId, string voucherNumber, int paymentAmount)
            : base(xrmDataContext)
        {
            this.voucherNumber = voucherNumber;
            this.voucherReasonId = voucherReasonId;
            this.paymentAmount = paymentAmount;
        }

        public bool DoMoney(Guid supplierId)
        {
            try
            {
                SupplierPricing.SupplierPricingManager pricingManager = new NoProblem.Core.BusinessLogic.SupplierPricing.SupplierPricingManager(XrmDataContext);
                var ans = pricingManager.CreateSupplierPricing(
                    new CreateSupplierPricingRequest()
                    {
                        BonusAmount = 0,
                        PaymentAmount = paymentAmount,
                        PaymentMethod = NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.Voucher,
                        IsAutoRenew = false,
                        SupplierId = supplierId,
                        VoucherNumber = voucherNumber,
                        VoucherReasonId = voucherReasonId,
                        IsFromAAR = true,
                        BonusType = NoProblem.Core.DataModel.Xrm.new_supplierpricing.BonusType.None,
                        Action = DataModel.Xrm.new_balancerow.Action.BonusCredit,
                        Description = "Registration"
                    });

                if (ans.DepositStatus != eDepositStatus.OK)
                {
                    DeleteSupplier(supplierId);
                    return false;
                }
            }
            catch (Exception exc)
            {
                XrmDataContext.ClearChanges();
                LogUtils.MyHandle.HandleException(exc, "Exception in MoneyLoader");
                DeleteSupplier(supplierId);
                return false;
            }
            return true;
        }

        private void DeleteSupplier(Guid supplierId)
        {
            var existing = XrmDataContext.accounts.FirstOrDefault(x => x.accountid == supplierId);
            if (existing != null)
            {
                var accExps = XrmDataContext.new_accountexpertises.Where(x => x.new_accountid == supplierId);
                foreach (var acExp in accExps)
                {
                    XrmDataContext.DeleteObject(acExp);
                }

                var regAccs = XrmDataContext.new_regionaccountexpertises.Where(x => x.new_accountid == supplierId);
                foreach (var regAc in regAccs)
                {
                    XrmDataContext.DeleteObject(regAc);
                }

                var daysAvs = XrmDataContext.new_availabilitynew_availabilities.Where(x => x.new_accountid == supplierId);
                foreach (var day in daysAvs)
                {
                    XrmDataContext.DeleteObject(day);
                }

                XrmDataContext.DeleteObject(existing);
                XrmDataContext.SaveChanges();
            }
        }
    }
}
