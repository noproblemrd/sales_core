﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR.Loaders
{
    internal class LoadingManager : XrmUserBase
    {
        private DataModel.SqlHelper.RegionMinData region;
        private Guid headingId;
        int timeZone;
        Guid voucherReasonId;
        string voucherNumber;
        int paymentAmount;

        public LoadingManager(DataModel.Xrm.DataContext xrmDataContext, Guid regionId, Guid headingId, out bool ok)
            : base(xrmDataContext)
        {
            ok = false;
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var scrapedRegion = regionDal.GetRegionMinDataById(regionId);
            NoProblem.Core.DataModel.SqlHelper.RegionMinData regionToLoad = null;
            NoProblem.Core.DataModel.SqlHelper.RegionMinData level1parent = null;
            if (scrapedRegion.Level > 2)
            {
                var parents = regionDal.GetParents(scrapedRegion);
                regionToLoad = parents.FirstOrDefault(x => x.Level == 2);
                level1parent = parents.FirstOrDefault(x => x.Level == 1);
            }
            else if (scrapedRegion.Level == 2)
            {
                regionToLoad = scrapedRegion;
                level1parent = regionDal.GetRegionMinDataById(scrapedRegion.ParentId);
            }
            else
            {
                regionToLoad = scrapedRegion;
                level1parent = scrapedRegion;
            }
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(xrmDataContext);
            string amountStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INITIAL_TRAIL_CREDIT);
            paymentAmount = int.Parse(amountStr);
            voucherNumber = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AAR_VOUCHER_NUMBER);
            voucherReasonId = new Guid(configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AAR_VOUCHER_REASON_ID));
            if (regionToLoad != null && headingId != Guid.Empty && level1parent != null)
            {
                this.timeZone = level1parent.TimeZoneCode;
                this.region = regionToLoad;
                this.headingId = headingId;
                ok = true;
            }
        }

        public bool LoadAdvertiser(string name, string phone)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            Guid existingId = accountRepositoryDal.GetSupplierIdByPhone(phone);
            if (existingId != Guid.Empty)
            {
                //Remove this when all attorneys are cleaned up.
                var supplier = accountRepositoryDal.Retrieve(existingId);
                if (supplier.new_status == (int)DataModel.Xrm.account.SupplierStatus.Trial && (supplier.new_isfromaar.HasValue && supplier.new_isfromaar.Value))
                {
                    var expertises = accountRepositoryDal.GetAccountExpertise(existingId).Where(x => x.Value.IsActive);
                    bool isAttorney = false;
                    foreach (var item in expertises)
                    {
                        if (item.Value.PrimaryExpertiseId == new Guid("519ADA2F-081C-E111-B6BD-001517D1792A"))
                        {
                            isAttorney = true;
                            break;
                        }
                    }
                    if (isAttorney)
                    {
                        Supplier attornerySupplierLogic = new Supplier(XrmDataContext, null);
                        HeadingLoader attorneryHeadingLoader = new HeadingLoader(XrmDataContext, headingId, attornerySupplierLogic);
                        bool attorneryHeadingOk = attorneryHeadingLoader.DoHeading(existingId);
                        if (attorneryHeadingOk)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            GeneralInfoLoader generalInfo = new GeneralInfoLoader(XrmDataContext, timeZone, paymentAmount);
            Guid supplierId = generalInfo.DoGeneralInfo(name, phone);
            if (supplierId == Guid.Empty)
            {
                return false;
            }

            Supplier supplierLogic = new Supplier(XrmDataContext, null);
            HeadingLoader headingLoader = new HeadingLoader(XrmDataContext, headingId, supplierLogic);
            bool headingOk = headingLoader.DoHeading(supplierId);
            if (!headingOk)
            {
                return false;
            }

            RegionsLoader regionLoader = new RegionsLoader(XrmDataContext, region);
            bool regionOk = regionLoader.DoRegion(supplierId);
            if (!regionOk)
            {
                return false;
            }

            AvailabilityLoader availabilityLoader = new AvailabilityLoader(XrmDataContext, supplierLogic);
            bool dayOk = availabilityLoader.DoAvailability(supplierId);
            if (!dayOk)
            {
                return false;
            }

            MoneyLoader moneyLoader = new MoneyLoader(XrmDataContext, voucherReasonId, voucherNumber, paymentAmount);
            bool moneyOk = moneyLoader.DoMoney(supplierId);
            if (!moneyOk)
            {
                return false;
            }

            return true;
        }
    }
}
