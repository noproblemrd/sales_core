﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.AAR.Loaders
{
    internal class AvailabilityLoader : XrmUserBase
    {
        Supplier supplierLogic;

        public AvailabilityLoader(DataModel.Xrm.DataContext xrmDataContext, Supplier supplierLogic)
            :base(xrmDataContext)
        {
            this.supplierLogic = supplierLogic;
        }

        public bool DoAvailability(Guid supplierId)
        {
            XElement xml = new XElement("SupplierAvailability");
            xml.Add(new XAttribute("SiteId", ""));
            xml.Add(new XAttribute("SupplierId", supplierId));

            XElement availability = new XElement("Availability");
            availability.Add(
                new XElement("Day",
                    new XAttribute("Name", "sunday"),
                    new XElement("FromTime", "1:00"),
                    new XElement("TillTime", "1:01")),
                new XElement("Day",
                    new XAttribute("Name", "monday"),
                    new XElement("FromTime", "08:00"),
                    new XElement("TillTime", "19:00")),
                new XElement("Day",
                    new XAttribute("Name", "tuesday"),
                    new XElement("FromTime", "08:00"),
                    new XElement("TillTime", "19:00")),
                new XElement("Day",
                    new XAttribute("Name", "wednesday"),
                    new XElement("FromTime", "08:00"),
                    new XElement("TillTime", "19:00")),
                new XElement("Day",
                    new XAttribute("Name", "thursday"),
                    new XElement("FromTime", "08:00"),
                    new XElement("TillTime", "19:00")),
                new XElement("Day",
                    new XAttribute("Name", "friday"),
                    new XElement("FromTime", "08:00"),
                    new XElement("TillTime", "19:00")),
                new XElement("Day",
                    new XAttribute("Name", "saturday"),
                    new XElement("FromTime", "08:00"),
                    new XElement("TillTime", "19:00"))
                );

            xml.Add(availability);
            try
            {
                supplierLogic.SetSupplierAvailability(xml.ToString(), true);
            }
            catch (Exception exc)
            {
                XrmDataContext.ClearChanges();
                LogUtils.MyHandle.HandleException(exc, "Exception in AvailabilityLoader");
                var existing = XrmDataContext.accounts.FirstOrDefault(x => x.accountid == supplierId);
                if (existing != null)
                {
                    var accExps = XrmDataContext.new_accountexpertises.Where(x => x.new_accountid == supplierId);
                    foreach (var acExp in accExps)
                    {
                        XrmDataContext.DeleteObject(acExp);
                    }

                    var regAccs = XrmDataContext.new_regionaccountexpertises.Where(x => x.new_accountid == supplierId);
                    foreach (var regAc in regAccs)
                    {
                        XrmDataContext.DeleteObject(regAc);
                    }

                    var daysAvs = XrmDataContext.new_availabilitynew_availabilities.Where(x => x.new_accountid == supplierId);
                    foreach (var day in daysAvs)
                    {
                        XrmDataContext.DeleteObject(day);
                    }

                    XrmDataContext.DeleteObject(existing);
                    XrmDataContext.SaveChanges();
                }
                return false;
            }
            return true;
        }
    }
}
