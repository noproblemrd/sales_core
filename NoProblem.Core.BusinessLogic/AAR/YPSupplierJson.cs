﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NoProblem.Core.BusinessLogic.AAR
{
     [DataContract]
    public class YPSupplierJson
    {
         [DataMember]
          public YP_SearchResult searchResult { get; set; }
    }
    [DataContract(Name = "relatedCategory", Namespace = "")]
    public class YP_RelatedCategory
    {
        [DataMember]
        public int count { get; set; }
        [DataMember]
        public string name { get; set; }
    }
    [DataContract(Name = "relatedCategories", Namespace = "")]
    public class YP_RelatedCategories
    {
        //public string __invalid_name__@xsi.type { get; set; }
        [DataMember]
        public List<YP_RelatedCategory> relatedCategory { get; set; }
    }
    [DataContract(Name = "inputParams", Namespace = "")]
    public class YP_InputParams
    {
        //public string __invalid_name__@xsi.type { get; set; }
        [DataMember]
        public string appId { get; set; }
        [DataMember]
        public string dnt { get; set; }
        [DataMember]
        public string format { get; set; }
        [DataMember]
        public string userIpAddress { get; set; }
        [DataMember]
        public string userReferrer { get; set; }
        [DataMember]
        public string requestId { get; set; }
        [DataMember]
        public bool shortUrl { get; set; }
        [DataMember]
        public string test { get; set; }
        [DataMember]
        public string userAgent { get; set; }
        [DataMember]
        public string visitorId { get; set; }
        [DataMember]
        public int listingCount { get; set; }
        [DataMember]
        public int pageNum { get; set; }
        [DataMember]
        public bool phoneSearch { get; set; }
        [DataMember]
        public int searchLocation { get; set; }
        [DataMember]
        public string term { get; set; }
         [DataMember]
        public string termType { get; set; }
    }
     [DataContract(Name = "metaProperties", Namespace = "")]
    public class YP_MetaProperties
    {
          [DataMember]
        public string didYouMean { get; set; }
          [DataMember]
        public string errorCode { get; set; }
          [DataMember]
        public int listingCount { get; set; }
          [DataMember]
        public string message { get; set; }
          [DataMember]
        public YP_RelatedCategories relatedCategories { get; set; }
          [DataMember]
        public YP_InputParams inputParams { get; set; }
          [DataMember]
        public string requestId { get; set; }
          [DataMember]
        public string resultCode { get; set; }
          [DataMember]
        public string searchCity { get; set; }
          [DataMember]
        public double searchLat { get; set; }
          [DataMember]
        public double searchLon { get; set; }
          [DataMember]
        public string searchState { get; set; }
          [DataMember]
        public string searchType { get; set; }
          [DataMember]
        public int searchZip { get; set; }
          [DataMember]
        public int totalAvailable { get; set; }
          [DataMember]
        public string trackingRequestURL { get; set; }
          [DataMember]
        public string ypcAttribution { get; set; }
    }
     [DataContract(Name = "searchListing", Namespace = "")]
    public class YP_SearchListing
    {
         [DataMember]
        public string adImage { get; set; }
         [DataMember]
        public string adImageClick { get; set; }
         [DataMember]
        public string additionalText { get; set; }
         [DataMember]
        public string audioURL { get; set; }
         [DataMember]
        public double averageRating { get; set; }
         [DataMember]
        public string baseClickURL { get; set; }
         [DataMember]
        public string businessName { get; set; }
         [DataMember]
        public string businessNameURL { get; set; }
         [DataMember]
        public string categories { get; set; }
         [DataMember]
        public string city { get; set; }
         [DataMember]
        public bool claimed { get; set; }
         [DataMember]
        public bool claimedStatus { get; set; }
         [DataMember]
        public bool couponFlag { get; set; }
         [DataMember]
        public string couponImageURL { get; set; }
         [DataMember]
        public string couponTitle { get; set; }
         [DataMember]
        public string couponURL { get; set; }
         [DataMember]
        public string customLink { get; set; }
         [DataMember]
        public string customLinkText { get; set; }
         [DataMember]
        public string description1 { get; set; }
         [DataMember]
        public string description2 { get; set; }
         [DataMember]
        public double distance { get; set; }
         [DataMember]
        public string distribAdImage { get; set; }
         [DataMember]
        public string distribAdImageClick { get; set; }
         [DataMember]
        public string email { get; set; }
         [DataMember]
        public bool hasDisplayAddress { get; set; }
         [DataMember]
        public bool hasPriorityShading { get; set; }
         [DataMember]
        public bool isRedListing { get; set; }
         [DataMember]
        public object latitude { get; set; }
         [DataMember]
        public int listingId { get; set; }
         [DataMember]
        public object longitude { get; set; }
         [DataMember]
        public string moreInfoURL { get; set; }
         [DataMember]
        public string noAddressMessage { get; set; }
         [DataMember]
        public bool omitAddress { get; set; }
         [DataMember]
        public bool omitPhone { get; set; }
         [DataMember]
        public string openHours { get; set; }
         [DataMember]
        public string openStatus { get; set; }
         [DataMember]
        public string paymentMethods { get; set; }
         [DataMember]
        public string phone { get; set; }
         [DataMember]
        public string pricePerCall { get; set; }
         [DataMember]
        public string primaryCategory { get; set; }
         [DataMember]
        public string printAdImage { get; set; }
         [DataMember]
        public string printAdImageClick { get; set; }
         [DataMember]
        public int ratingCount { get; set; }
         [DataMember]
        public string ringToNumberDisplay { get; set; }
         [DataMember]
        public string searchResultType { get; set; }
         [DataMember]
        public string services { get; set; }
         [DataMember]
        public string slogan { get; set; }
         [DataMember]
        public string state { get; set; }
         [DataMember]
        public string street { get; set; }
         [DataMember]
         public string videoURL { get; set; }
         [DataMember]
        public string viewPhone { get; set; }
         [DataMember]
        public string websiteDisplayURL { get; set; }
         [DataMember]
        public string websiteURL { get; set; }
         [DataMember]
        public object zip { get; set; }
    }
    [DataContract(Name = "searchListings", Namespace = "")]
    public class YP_SearchListings
    {
        [DataMember]
        public List<YP_SearchListing> searchListing { get; set; }
    }
     [DataContract(Name = "searchResult", Namespace = "")]
    public class YP_SearchResult
    {
         [DataMember]
        public YP_MetaProperties metaProperties { get; set; }
          [DataMember]
        public YP_SearchListings searchListings { get; set; }
    }
}
