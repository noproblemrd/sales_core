﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;


namespace NoProblem.Core.BusinessLogic.AAR
{
    [DataContract]
    public class GoogleSupplierJson
    {
        [DataMember]
        public List<object> html_attributions { get; set; }
        [DataMember]
        public GoogleSupplierResult result { get; set; }
        [DataMember]
        public string status { get; set; }
    }
    [DataContract(Name = "address_components", Namespace="")]
    public class GoogleSupplierAddressComponent
    {
        [DataMember]
        public string long_name { get; set; }
        [DataMember]
        public string short_name { get; set; }
        [DataMember]
        public List<string> types { get; set; }
    }
    [DataContract(Name = "location", Namespace = "")]
    public class GoogleSupplierLocation
    {
        [DataMember]
        public double lat { get; set; }
        [DataMember]
        public double lng { get; set; }
    }
    [DataContract(Name = "geometry", Namespace = "")]
    public class GoogleSupplierGeometry
    {
        [DataMember]
        public GoogleSupplierLocation location { get; set; }
    }
    [DataContract(Name = "aspects", Namespace = "")]
    public class GoogleSupplierAspect
    {
        [DataMember]
        public int rating { get; set; }
        [DataMember]
        public string type { get; set; }
    }
    [DataContract(Name = "reviews", Namespace = "")]
    public class GoogleSupplierReview
    {
        [DataMember]
        public List<GoogleSupplierAspect> aspects { get; set; }
        [DataMember]
        public string author_name { get; set; }
        [DataMember]
        public string author_url { get; set; }
        [DataMember]
        public string language { get; set; }
        [DataMember]
        public int rating { get; set; }
        [DataMember]
        public string text { get; set; }
        [DataMember]
        public int time { get; set; }
    }
    [DataContract(Name = "results", Namespace = "")]
    public class GoogleSupplierResult
    {
        [DataMember]
        public List<GoogleSupplierAddressComponent> address_components { get; set; }
        [DataMember]
        public string adr_address { get; set; }
        [DataMember]
        public string formatted_address { get; set; }
        [DataMember]
        public string formatted_phone_number { get; set; }
        [DataMember]
        public GoogleSupplierGeometry geometry { get; set; }
        [DataMember]
        public string icon { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string international_phone_number { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string place_id { get; set; }
        [DataMember]
        public string reference { get; set; }
        [DataMember]
        public List<GoogleSupplierReview> reviews { get; set; }
        [DataMember]
        public string scope { get; set; }
        [DataMember]
        public List<string> types { get; set; }
        [DataMember]
        public string url { get; set; }
        [DataMember]
        public int user_ratings_total { get; set; }
        [DataMember]
        public int utc_offset { get; set; }
        [DataMember]
        public string vicinity { get; set; }
    }
}
