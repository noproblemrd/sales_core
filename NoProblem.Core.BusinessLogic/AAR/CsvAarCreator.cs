﻿using NoProblem.Core.BusinessLogic.Utils.Csv;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    public class CsvAarCreator
    {
        const string DateFormat = "{0:dd-MM-yyyy HH:mm:ss}";
        const string FILE_NAME = "AAR_Request_{0}.csv";
        readonly static string FOLDER_PATH;
        static CsvAarCreator()
        {
            FOLDER_PATH = System.Configuration.ConfigurationManager.AppSettings["AarRequestFolderPath"];
        }


        private Guid incidentId;
        string CaseNumber;
        string FileFullName;
        List<CsvRow> CsvList;
        public CsvAarCreator(Guid incidentId, string CaseNumber)
        {
            this.incidentId = incidentId;
            this.CaseNumber = CaseNumber;
        }
        public void CreateSendCsv()
        {
            CreateCsvList();
            SaveFile();
            SendEmail();
        }
        private void SendEmail()
        {
            NoProblem.Core.BusinessLogic.ClipCall.CreateMobileSystemNotification noti = new ClipCall.CreateMobileSystemNotification();
            noti.SendAarRequestListEmailNotification(FileFullName);
        }
        private void SaveFile()
        {
            string fileName = string.Format(FILE_NAME, CaseNumber);

            FileFullName = FOLDER_PATH + "\\" + fileName;
            int ind = 0;
            while (File.Exists(FileFullName))
            {
                FileFullName = FileFullName.Insert(FileFullName.Length - 4, ind.ToString());
                ind++;
            }
            using (FileStream file = File.Create(FileFullName))
            {
                using (CsvFileWriter writer = new CsvFileWriter(file))
                {
                    foreach (var row in CsvList)
                    {
                        writer.WriteRow(row);
                    }
                }
            }
        }
        private void CreateCsvList()
        {
            CsvList = new List<CsvRow>();
            CsvList.Add(GetHeaderRow());
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {

                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", incidentId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        CsvRow row = new CsvRow();
                        row.Add((string)reader["Supplier"]);
                        row.Add((string)reader["Phone"]);
                        row.Add((string)reader["SourceSupplier"]);
                        row.Add((string)reader["Category"]);
                        row.Add((string)reader["CaseNumber"]);
                        row.Add((string)reader["videoUrl"]);
                        row.Add((string)reader["TextMessage"]);
                        row.Add(string.Format(DateFormat, (DateTime)reader["CreatedOn"]));
                        row.Add((string)reader["Region"]);
                        row.Add((string)reader["LandingPage"]);
                        CsvList.Add(row);
                    }
                    conn.Close();
                }
            }
        }
        private CsvRow GetHeaderRow()
        {
            CsvRow column = new CsvRow();
            column.Add("Supplier");
            column.Add("Phone");
            column.Add("SourceSupplier");
            column.Add("Category");
            column.Add("CaseNumber");
            column.Add("videoUrl");
            column.Add("TextMessage");
            column.Add("CreatedOn");
            column.Add("Region");
            column.Add("LandingPage");
            return column;
        }
        const string command = @"
select YS.New_name Supplier, YS.New_Phone Phone,
	(case when YS.New_YelpId is not null and YS.New_GoogleId is not null then 'GoogleYelp'
	when YS.New_YelpId is not null then 'Yelp'
	else  'Google' end) SourceSupplier,
	C.New_name Category,
	INC.TicketNumber CaseNumber,
	ISNULL(INC.New_videoUrl,'') videoUrl,
	AC.New_TextMessage TextMessage,
	AC.CreatedOn,	
	R.New_englishname Region,
	'https://app.clipcall.it/tm/callback1.aspx?callid=' + CAST(AC.New_yelpaarcallId as nvarchar(50)) LandingPage
from dbo.New_yelpaarcall AC
	inner join dbo.New_yelpsupplierExtensionBase YS on AC.new_yelpsupplierid = YS.New_yelpsupplierId
	inner join dbo.Incident INC on INC.IncidentId = AC.new_incidentid
	inner join dbo.New_primaryexpertiseExtensionBase C on C.New_primaryexpertiseId = INC.new_primaryexpertiseid
	inner join dbo.New_regionExtensionBase R on R.New_regionId = INC.new_regionid	
	left join dbo.CallbackTextMessage CTM on CTM.AarCallId = AC.New_yelpaarcallId
	left join dbo.New_incidentaccountExtensionBase IAEB on IAEB.New_incidentaccountId = AC.new_incidentaccountid
where AC.New_Type = 2
    and AC.DeletionStateCode = 0
	and INC.IncidentId = @incidentId";
    }
}
