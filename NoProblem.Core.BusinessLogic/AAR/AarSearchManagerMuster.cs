﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class AarSearchManagerMuster //: AarSearchManagerBase
    {
        protected Dictionary<DataModel.Xrm.new_yelpsearch.eAarSearchSource, Guid> SearchIds;
        DataModel.Xrm.DataContext xrmDataContext;
        DataModel.Xrm.incident incident;
        public AarSearchManagerMuster(DataModel.Xrm.DataContext xrmDataContext, DataModel.Xrm.incident incident)
   //         : base(xrmDataContext)
        {
            this.xrmDataContext = xrmDataContext;
            this.incident = incident;
        }
        
        public IEnumerable<YelpSupplierMinData> GetSupplier(int? count)
        {
            UpdateSearch();
            /*
            SearchIds = GetLastSearchIds();
            foreach(DataModel.Xrm.new_yelpsearch.eAarSearchSource _value in Enum.GetValues(typeof(DataModel.Xrm.new_yelpsearch.eAarSearchSource)))
            {
                if(!SearchIds.ContainsKey(_value))
                {
                    AarSearchManagerBase _search = GetInstance(_value);
                    _search.GetSuppliersFromApi();
                    if (_search.GetSearchId == Guid.Empty)
                        continue;
                    SearchIds.Add(_value, _search.GetSearchId);
                }
            }
             */
            /*
            if (SearchIds.Count == 0)
                return null;
             * */
            List<YelpSupplierMinData> suppliers = GetSuppliersFromDB();

            return count.HasValue ? suppliers.Take(count.Value) : suppliers;
        }
        void UpdateSearch()
        {
            foreach (DataModel.Xrm.new_yelpsearch.eAarSearchSource _value in Enum.GetValues(typeof(DataModel.Xrm.new_yelpsearch.eAarSearchSource)))
            {
                if (_value == DataModel.Xrm.new_yelpsearch.eAarSearchSource.CLIPCALL)
                    continue;
                AarSearchManagerBase _search = GetInstance(_value);
                if (_search != null)
                    _search.CheckUpdateLastSearchId();
            }

        }
        /*
        private const string GetLastSearchIdQuery = @"
SELECT DISTINCT S1.New_Source, S2.New_yelpsearchId
	FROM [dbo].[New_yelpsearchExtensionBase] S1 WITH(NOLOCK) 
		CROSS APPLY (SELECT TOP 1 S.*
					FROM [dbo].[New_yelpsearchExtensionBase] S  WITH(NOLOCK)						
						inner join dbo.New_yelpsearchBase SBASE  WITH(NOLOCK) on S.New_yelpsearchId = SBASE.New_yelpsearchId
					WHERE SBASE.DeletionStateCode = 0
						and S.New_RegionId = @regionId
						and S.New_PrimaryExpertiseId = @PrimaryExpertiseId
					    and S.New_Source = S1.New_Source
						and DATEDIFF ( DAY , SBASE.CreatedOn , GETDATE() ) < 30	
					order by SBASE.CreatedOn desc) S2";
        protected Dictionary<DataModel.Xrm.new_yelpsearch.eAarSearchSource, Guid> GetLastSearchIds()
        {
            Dictionary<DataModel.Xrm.new_yelpsearch.eAarSearchSource, Guid> list = new Dictionary<DataModel.Xrm.new_yelpsearch.eAarSearchSource, Guid>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GetLastSearchIdQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@regionId", cityId);
                    cmd.Parameters.AddWithValue("@PrimaryExpertiseId", category.new_primaryexpertiseid);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                        list.Add((DataModel.Xrm.new_yelpsearch.eAarSearchSource)((int)reader["New_Source"]), (Guid)reader["New_yelpsearchId"]);
                    conn.Close();
                }
            }
            return list;
           
        }
         * */
        /*
        protected override string queryGetSuppliersFromLastSearch
        {
            get
            {
                return @"
select distinct ys.New_name, ys.new_yelpsupplierid, ys.new_phone, ys.new_rating, ys.New_GoogleRating, ys.new_callcount, ys.new_successfulcallcount
from new_yelpsupplier ys with(nolock)
join new_yelpsearchresult sr with(nolock) on ys.new_yelpsupplierid = sr.new_yelpsupplierid
where sr.new_yelpsearchid in
	(select Value from dbo.fn_ParseDelimitedGuid(@SearchIds, ';')) 
and ys.new_dontcallme = 0
order by ys.new_callcount asc, ys.new_rating desc, ys.New_GoogleRating desc
";
            }
        }
         * */
        /*
        protected override List<YelpSupplierMinData> GetSuppliersFromLastYelpSearch()
        {
            List<YelpSupplierMinData> retVal = new List<YelpSupplierMinData>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(queryGetSuppliersFromLastSearch, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@SearchIds", GetSearchIdsForSql());
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        YelpSupplierMinData data = new YelpSupplierMinData();
                        data.YelpSupplierId = (Guid)reader["new_yelpsupplierid"];
                        data.CallCount = (int)reader["new_callcount"];
                        data.Phone = Convert.ToString(reader["new_phone"]);
                        data.YelpRating = reader["new_rating"] == DBNull.Value ? 0 : (decimal)reader["new_rating"];
                        data.GoogleRating = reader["New_GoogleRating"] == DBNull.Value ? 0 : (decimal)reader["New_GoogleRating"];
                        data.SuccessfulCallCount = (int)reader["new_successfulcallcount"];
                        retVal.Add(data);
                    }
                    conn.Close();
                }
            }
            return retVal;
        }
         */
        /*
        private const string GetSuppliersFromDBQuery = @"
select distinct YS.New_name, YS.new_yelpsupplierid, YS.new_phone, YS.new_rating, YS.New_GoogleRating, YS.new_callcount, YS.new_successfulcallcount
from dbo.New_yelpsupplierExtensionBase YS with(nolock)
	INNER JOIN dbo.AarSupplierExpertise E on YS.New_yelpsupplierId = E.YelpSupplierId
	INNER JOIN dbo.AarSupplierRegion R on YS.New_yelpsupplierId = R.YelpSupplierId
WHERE E.PrimaryExpertiseId = @PrimaryExpertiseId
	and R.RegionId = @RegionId
and YS.new_dontcallme = 0
order by YS.new_callcount asc, YS.new_rating desc, YS.New_GoogleRating desc";
         * */
        protected List<YelpSupplierMinData> GetSuppliersFromDB()
        {
            /*
#if DEBUG
            string GetSuppliersFromDBQuery = "EXEC [dbo].[GetAarSuppliersQA] @PrimaryExpertiseId, @RegionId";
#else
             * */
            string GetSuppliersFromDBQuery = "EXEC [dbo].[GetAarSuppliers] @PrimaryExpertiseId, @RegionId, @IncidentId";
//#endif

            List<YelpSupplierMinData> retVal = new List<YelpSupplierMinData>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GetSuppliersFromDBQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@RegionId", GetCityRegionId());
                    cmd.Parameters.AddWithValue("@PrimaryExpertiseId", incident.new_primaryexpertiseid);
                    cmd.Parameters.AddWithValue("@IncidentId", incident.incidentid);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        YelpSupplierMinData data = new YelpSupplierMinData();
                        data.CallCount = (int)reader["callcount"];
                        data.CityGridReating = (reader["CitygridRating"] == DBNull.Value) ? 0m : new decimal((int)reader["CitygridRating"]);
                        data.GoogleRating = (reader["GoogleRating"] == DBNull.Value) ? 0m : new decimal((int)reader["GoogleRating"]);
                        data.YelpRating = (reader["YelpRating"] == DBNull.Value) ? 0m : new decimal((int)reader["YelpRating"]);
                        data.YellowPagesRating = (reader["YPRating"] == DBNull.Value) ? 0m : new decimal((int)reader["YPRating"]);
                        data.Phone = (string)reader["SupplierPhone"];
                        data.SuccessfulCallCount = (int)reader["successfulcallcount"];
                        data.YelpSupplierId = (Guid)reader["new_yelpsupplierid"];
                        retVal.Add(data);
                    }
                    conn.Close();
                }
            }            
            return retVal;
        }
        public YelpSupplierMinData GetSupplierForCall()
        {
            UpdateSearch();
            return GetSupplierForCallDB();
        }
        Guid GetCityRegionId()
        {
            DataAccessLayer.Region regionDal = new DataAccessLayer.Region(xrmDataContext);
            var parents = regionDal.GetParents(incident.new_new_region_incident);
            var city = parents.First(x => x.new_level == 3);
            return city.new_regionid;
        }
        YelpSupplierMinData GetSupplierForCallDB()
        {
            YelpSupplierMinData ys = null;
            
   //         cityId = city.new_regionid;
            string command = "EXEC [dbo].[GetAarSupplierForCall] @incidentId, @PrimaryExpertiseId, @RegionId";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@incidentId", incident.incidentid);
                        cmd.Parameters.AddWithValue("@PrimaryExpertiseId", incident.new_primaryexpertiseid);
                        cmd.Parameters.AddWithValue("@RegionId", GetCityRegionId());
                        SqlDataReader reader = cmd.ExecuteReader();
                        if(reader.Read())
                        {
 //SELECT TOP 1 YS.New_name, YS.new_yelpsupplierid, YS.new_phone, ISNULL(YS.new_rating, 0) YelpRating, ISNULL(YS.New_GoogleRating, 0) GoogleRating
 //       ,ISNULL(YS.New_citygridRating, 0) citygridRating, YS.new_callcount, YS.new_successfulcallcount
                            ys = new YelpSupplierMinData();
                            ys.CallCount = (int)reader["new_callcount"];
                            ys.CityGridReating = (reader["citygridRating"] == DBNull.Value) ? 0m : (decimal)reader["citygridRating"];
                            ys.GoogleRating = (reader["GoogleRating"] == DBNull.Value) ? 0m : (decimal)reader["GoogleRating"];
                            ys.YelpRating = (reader["YelpRating"] == DBNull.Value) ? 0m : (decimal)reader["YelpRating"];
                            ys.Phone = (string)reader["new_phone"];
                            ys.SuccessfulCallCount = (int)reader["new_successfulcallcount"];
                            ys.YelpSupplierId = (Guid)reader["new_yelpsupplierid"];
                        }
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception GetSupplierForCall procedure GetAarSupplierForCall IncidentId:" + incident.incidentid);
            }
            return ys;
        }
        private string GetSearchIdsForSql()
        {
            string result = string.Empty;
            foreach(KeyValuePair<DataModel.Xrm.new_yelpsearch.eAarSearchSource, Guid> kvp in SearchIds)
                result += kvp.Value.ToString() + ";";
            return result;
        }
        private AarSearchManagerBase GetInstance(DataModel.Xrm.new_yelpsearch.eAarSearchSource source)
        {
            switch(source)
            {
                case(DataModel.Xrm.new_yelpsearch.eAarSearchSource.CITYGRID):
                    return new CitygridPlacesManager(xrmDataContext, incident);
                case(DataModel.Xrm.new_yelpsearch.eAarSearchSource.GOOGLE):
                    return new GooglePlacesManager(xrmDataContext, incident);
                case(DataModel.Xrm.new_yelpsearch.eAarSearchSource.YELP):
                    return new YelpSearchManager(xrmDataContext, incident);
                case(DataModel.Xrm.new_yelpsearch.eAarSearchSource.YP):
                    return new ypPlacesManager(xrmDataContext, incident);
                default:
                    return null;
                    
            }
        }        
    }
}
