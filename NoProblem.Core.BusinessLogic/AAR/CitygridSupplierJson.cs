﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NoProblem.Core.BusinessLogic.AAR
{
     [DataContract]
    public class CitygridSupplierJson
    {
         [DataMember]
         public CitygridSupplierResults results { get; set; }
    }
     [DataContract(Name = "Region", Namespace = "")]
     public class CitygridSupplierRegion
    {
         [DataMember]
        public string type { get; set; }
         [DataMember]
        public string name { get; set; }
         [DataMember]
        public double? latitude { get; set; }
         [DataMember]
        public double? longitude { get; set; }
         [DataMember]
        public double? default_radius { get; set; }
    }
    [DataContract(Name = "Address", Namespace = "")]
     public class CitygridSupplierAddress
    {
        [DataMember]
        public string street { get; set; }
        [DataMember]
        public string city { get; set; }
        [DataMember]
        public string state { get; set; }
        [DataMember]
        public string postal_code { get; set; }
    }
   [DataContract(Name = "Expansion", Namespace = "")]
    public class CitygridSupplierExpansion
    {
       [DataMember]
        public object count { get; set; }
       [DataMember]
        public object uri { get; set; }
    }
    [DataContract(Name = "Tag", Namespace = "")]
    public class CitygridSupplierTag
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public bool primary { get; set; }
    }
   [DataContract(Name = "Location", Namespace = "")]
    public class CitygridSupplierLocation
    {
       [DataMember]
        public int id { get; set; }
       [DataMember]
        public bool featured { get; set; }
       [DataMember]
        public string name { get; set; }
       [DataMember]
        public CitygridSupplierAddress address { get; set; }
       [DataMember]
        public string neighborhood { get; set; }
       [DataMember]
        public double? latitude { get; set; }
       [DataMember]
        public double? longitude { get; set; }
       [DataMember]
        public object distance { get; set; }
       [DataMember]
        public string image { get; set; }
       [DataMember]
        public string phone_number { get; set; }
       [DataMember]
        public object fax_number { get; set; }
       [DataMember]
        public int rating { get; set; }
       [DataMember]
        public string tagline { get; set; }
       [DataMember]
        public string profile { get; set; }
       [DataMember]
        public string website { get; set; }
       [DataMember]
       public bool has_video { get; set; }
       [DataMember]
        public bool has_offers { get; set; }
       [DataMember]
        public string offers { get; set; }
       [DataMember]
        public int user_review_count { get; set; }
       [DataMember]
        public string sample_categories { get; set; }
       [DataMember]
        public string impression_id { get; set; }
       [DataMember]
        public CitygridSupplierExpansion expansion { get; set; }
       [DataMember]
        public List<CitygridSupplierTag> tags { get; set; }
       [DataMember]
        public string public_id { get; set; }
       [DataMember]
        public int business_operation_status { get; set; }
       [DataMember]
        public string scorecard { get; set; }
       [DataMember]
        public int votes { get; set; }
       [DataMember]
        public List<object> awards { get; set; }
       [DataMember]
        public bool has_menu { get; set; }
       [DataMember]
        public object needs_review { get; set; }
       [DataMember]
        public object needs_photo { get; set; }
       [DataMember]
        public bool has_reward { get; set; }
       [DataMember]
        public string politanName { get; set; }
    }
    [DataContract(Name = "Results", Namespace = "")]
    public class CitygridSupplierResults
    {
        [DataMember]
        public string uri { get; set; }
        [DataMember]
        public int first_hit { get; set; }
        [DataMember]
        public int last_hit { get; set; }
        [DataMember]
        public int total_hits { get; set; }
        [DataMember]
        public int page { get; set; }
        [DataMember]
        public int rpp { get; set; }
        [DataMember]
        public List<CitygridSupplierRegion> regions { get; set; }
        [DataMember]
        public List<int> geo_ids { get; set; }
        [DataMember]
        public List<int> tag_ids { get; set; }
        [DataMember]
        public List<CitygridSupplierLocation> locations { get; set; }
        [DataMember]
        public string call_id { get; set; }
    }
    
}
