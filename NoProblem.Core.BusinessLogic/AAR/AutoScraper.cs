﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using Effect.Crm.Logs;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.AAR
{
    public class AutoScraper : XrmUserBase
    {
        #region Fields
        private static string scrapeUrl = ConfigurationManager.AppSettings["scraperUrl"];
        #endregion

        #region Ctor
        public AutoScraper(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        //public void LoadScrapedAdvertisers()
        //{
        //    Thread tr = new Thread(
        //        delegate()
        //        {
        //            try
        //            {
        //                LoadScrapedAdvertisersPrivate();
        //            }
        //            catch (Exception exc)
        //            {
        //                LogUtils.MyHandle.HandleException(exc, "Exception in async method AutoScraper.LoadScrapedAdvertisersPrivate");
        //            }
        //        });
        //    tr.Start();
        //}

        //private void LoadScrapedAdvertisersPrivate()
        //{
        //    XElement doc = CallGetDoneRequestService();
        //    if (doc == null)
        //    {
        //        return;
        //    }
        //    Guid caseId = new Guid(doc.Attribute("CaseId").Value);
        //    Guid regionId = new Guid(doc.Attribute("RegionId").Value);
        //    Guid headingId = new Guid(doc.Attribute("HeadingId").Value);
        //    var advElements = doc.Elements("Advertiser");
        //    bool ok;
        //    Loaders.LoadingManager loader = new NoProblem.Core.BusinessLogic.AAR.Loaders.LoadingManager(XrmDataContext, regionId, headingId, out ok);
        //    if (ok)
        //    {
        //        int loadCount = 0;
        //        foreach (var item in advElements)
        //        {
        //            try
        //            {
        //                string name = item.Element("Name").Value;
        //                string phone = item.Element("Phone").Value;
        //                bool loaded = loader.LoadAdvertiser(name, phone);
        //                if (loaded)
        //                {
        //                    loadCount++;
        //                }
        //            }
        //            catch (ThreadAbortException threadAbortException)
        //            {
        //                LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
        //                Thread.ResetAbort();
        //                XrmDataContext.ClearChanges();
        //            }
        //            catch (Exception exc)
        //            {
        //                LogUtils.MyHandle.HandleException(exc, "Exception in method LoadScrapedAdvertisers in the scraped advertisers iteration.");
        //                XrmDataContext.ClearChanges();
        //            }
        //        }
        //        LogUtils.MyHandle.WriteToLog("New {0} advertisers have been loaded from scraping for case {1}", loadCount, caseId);
        //        RestartCase(caseId);
        //    }
        //    else
        //    {
        //        throw new Exception(string.Format("LoadingManager could not be instantiated in LoadScrapedAdvertisers. RegionId = {0}, HeadingId = {1}, CaseId = {2}", regionId, headingId, caseId));
        //    }
        //}

        #endregion

        #region Internal Methods

        internal bool InitiateAutoScraping(Guid incidentId)
        {
            bool retVal = true;
            try
            {
                DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                var incident = dal.Retrieve(incidentId);
                ScraperServiceReference.RequestContainer container = new NoProblem.Core.BusinessLogic.ScraperServiceReference.RequestContainer();
                container.CaseId = incident.incidentid;
                container.HeadingId = incident.new_primaryexpertiseid.Value;
                container.HeadingName = incident.new_new_primaryexpertise_incident.new_name;
                DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
                var searchedRegion = regionDal.Retrieve(incident.new_regionid.Value);
                var parents = regionDal.GetParents(searchedRegion);
                if (searchedRegion.new_level == 4)
                {
                    var parent = parents.Single(x => x.new_level == 3);
                    var state = parents.Single(x => x.new_level == 1);
                    container.RegionId = parent.new_regionid;
                    container.RegionName = parent.new_name + " " + state.new_name;
                }
                else if (searchedRegion.new_level == 3 || searchedRegion.new_level == 2)
                {
                    var state = parents.Single(x => x.new_level == 1);
                    container.RegionId = incident.new_regionid.Value;
                    container.RegionName = searchedRegion.new_name + " " + state.new_name; ;
                }
                else
                {
                    container.RegionId = incident.new_regionid.Value;
                    container.RegionName = searchedRegion.new_name;
                }
                ScraperServiceReference.ScraperServiceClient client = null;
                try
                {
                    client = new ScraperServiceReference.ScraperServiceClient("WSHttpBinding_IScraperService", scrapeUrl);
                    client.MakeNewRequest(container);
                }
                finally
                {
                    if (client != null)
                    {
                        if (client.State != System.ServiceModel.CommunicationState.Faulted)
                        {
                            client.Close();
                        }
                        else
                        {
                            client.Abort();
                        }
                    }
                }
                incident.statuscode = (int)DML.Xrm.incident.Status.Scraping;
                incident.new_scraped = true;
                dal.Update(incident);
                XrmDataContext.SaveChanges();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AutoScraper.InitiateAutoScraping");
                retVal = false;
            }
            return retVal;
        }

        #endregion

        #region Private Methods

        private XElement CallGetDoneRequestService()
        {
            XElement doc = null;
            ScraperServiceReference.ScraperServiceClient client = null;
            try
            {
                client = new ScraperServiceReference.ScraperServiceClient("WSHttpBinding_IScraperService", scrapeUrl);
                doc = client.GetDoneRequest();
            }
            finally
            {
                if (client != null)
                {
                    if (client.State != System.ServiceModel.CommunicationState.Faulted)
                    {
                        client.Close();
                    }
                    else
                    {
                        client.Abort();
                    }
                }
            }
            return doc;
        }

        private void RestartCase(Guid caseId)
        {
            //DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            //DML.Xrm.incident incident = incidentDal.Retrieve(caseId);
            //if (incident.statuscode == (int)DML.Xrm.incident.Status.Scraping)
            //{
            //    AarManager aarManager = new AarManager(XrmDataContext);
            //    bool foundNewSuppliers = aarManager.ContinueServiceRequest(caseId);
            //    if (!foundNewSuppliers)
            //    {
            //        if (incident.new_incident_new_incidentaccount.Count() == 0)
            //        {
            //            incident.statuscode = (int)DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER;
            //            incidentDal.Update(incident);
            //            XrmDataContext.SaveChanges();
            //        }
            //    }
            //}
        }

        #endregion
    }
}
