﻿using Effect.Crm.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AAR
{
    internal class AarSuccessSmsThread : Runnable
    {
        private string phone;
        private const string SMS_BODY = "We just sent you a potential customer! For more great phone leads, register now at http://NoProblem.me?sms1";

        public AarSuccessSmsThread(string phone)
        {
            this.phone = phone;
        }

        protected override void Run()
        {
            Notifications notifier = new Notifications(XrmDataContext);
            bool sent = notifier.SendSMSTemplate(SMS_BODY, phone, GlobalConfigurations.DefaultCountry);
            if (!sent)
                LogUtils.MyHandle.WriteToLog("Failed to send sms after AAR successful call to phone = {0}.", phone);
        }
    }
}
