﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.GoogleShorten
{
    public class GoogleShortenUrl //: XrmUserBase
    {
        #region static
        private const string baseUrl = "https://www.googleapis.com/";
        private const string shortenResource = "urlshortener/v1/url";
        private readonly static string _apiToken;
        static readonly string MASKING_DOMAIN;
        static GoogleShortenUrl()
        {
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            _apiToken = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.GOOGLE_SHORTEN_URL_API_KEY);
            MASKING_DOMAIN = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.CNAME_DOMAIN_SHORTENURL);   
        }
        #endregion
        /*
        public GoogleShortenUrl(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {

        }
         * */
        
        public GoogleShortenUrl() { }
        
        string ApiToken
        {
            get
            {
                /*
                if (string.IsNullOrEmpty(_apiToken))
                {
                    DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                    _apiToken = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.GOOGLE_SHORTEN_URL_API_KEY);
                }
                 * */
                return _apiToken;
            }
        }

        public string ShortenUrl(string longUrl)
        {
            string shortUrl = null;
            try
            {
                RestSharp.RestClient client = new RestSharp.RestClient(baseUrl);
                RestSharp.RestRequest request = new RestSharp.RestRequest(shortenResource, RestSharp.Method.POST);
            //    request.AddHeader("Content-Type", "application/json");
                request.RequestFormat = RestSharp.DataFormat.Json;
                request.Parameters.Add(new RestSharp.Parameter() { Name = "key", Value = ApiToken, Type = RestSharp.ParameterType.QueryString });
                request.AddBody(new { longUrl = longUrl });
            //    request.Parameters.Add(new RestSharp.Parameter() { Name = "longUrl", Value = longUrl, Type = RestSharp.ParameterType.RequestBody });
                RestSharp.RestResponse response = (RestSharp.RestResponse)client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    GoogleShortenResponse shortenResponse = null;
                    try
                    {
                        shortenResponse = JsonConvert.DeserializeObject<GoogleShortenResponse>(response.Content);
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Unable to serialize url with Google. longUrl: {0}", exc.Message);
                        return null;
                        /*
                        var shortenResponseOnError = JsonConvert.DeserializeObject<ShortenReponseOnError>(response.Content);
                        shortenResponse = new ShortenResponse();
                        shortenResponse.StatusCode = shortenResponseOnError.StatusCode;
                        shortenResponse.StatusTxt = shortenResponseOnError.StatusTxt;
                         * */
                        //shortenResponse = new ShortenResponse();
                    }

                 //   shortUrl = GetMaskingUrl(shortenResponse.shortenUrl);
                    shortUrl = shortenResponse.shortenUrl;
                    /*
                    if (shortenResponse.StatusCode == 200)
                    {
                        shortUrl = shortenResponse.Data.Url;
                    }
                    else
                    {
                        LogUtils.MyHandle.WriteToLog("Unable to shorten url with bitly. statusCode: {0}, statusTxt: {1}", shortenResponse.StatusCode, shortenResponse.StatusTxt);
                    }
                     * */
                }
                else
                {
                    LogUtils.MyHandle.WriteToLog("Unable to shorten url with Google. HTTP protocol error.  httpStatusCode: {0}, content: {1}", response.StatusCode, response.Content);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception shortening url with Google. longUrl: {0}", longUrl);
            }
            return shortUrl;
        }
        private string GetMaskingUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
                return null;
            int start = url.IndexOf("//") + 2;
            int end = url.IndexOf("/", start);
            return url.Substring(0, start) + MASKING_DOMAIN + url.Substring(end);
        }
    }
}
