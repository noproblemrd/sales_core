﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace NoProblem.Core.BusinessLogic.GoogleShorten
{
    [JsonObject]
    internal class GoogleShortenResponse
    {
        [JsonProperty("kind")]
        public string kind { get; set; }
        [JsonProperty("id")]
        public string shortenUrl { get; set; }
        [JsonProperty("longUrl")]
        public string longUrl { get; set; }
    }
    /*
     * "kind": "urlshortener#url",
  "id": "http://goo.gl/YbRCgN",
  "longUrl": "http://app.noproblemppc.com/"
     * */

    /*
     * {
  "error": {
    "errors": [
      {
        "domain": "usageLimits",
        "reason": "keyInvalid",
        "message": "Bad Request"
      }
    ],
    "code": 400,
    "message": "Bad Request"
  }
}*/
    [JsonObject("error")]
    internal class GoogleShortenErrorResponse
    {
        [JsonProperty("errors")]
        public GoogleShortenErrorDataResponse errors { get; set; }
        [JsonProperty("code")]
        public int code { get; set; }
        [JsonProperty("message")]
        public string message { get; set; }
    }
    [JsonObject("errors")]
    internal class GoogleShortenErrorDataResponse
    {
        [JsonProperty("domain")]
        public string domain { get; set; }
        [JsonProperty("reason")]
        public string reason { get; set; }
        [JsonProperty("message")]
        public string message { get; set; }
    }

}
