﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.GoogleShorten
{
    public class FactoryShortenUrl
    {
        private string originalUrl;
     //   private DML.Xrm.DataContext _xrmDataContext;
        public FactoryShortenUrl(string url)
        {
            originalUrl = url;
 //           _xrmDataContext = DataModel.XrmDataContext.Create();
        }
        public string ShortenUrl()
        {
            GoogleShorten.GoogleShortenUrl gsu = new GoogleShorten.GoogleShortenUrl();
            string url = gsu.ShortenUrl(originalUrl);
            if(string.IsNullOrEmpty(url))
            {
                Bitly.BitlyDriver bd = new Bitly.BitlyDriver();
                url = bd.ShortenUrl(originalUrl);
            }
            return url;
        }
    }
}
