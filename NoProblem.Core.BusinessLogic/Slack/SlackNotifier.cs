﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Slack
{
    internal class SlackNotifier : Runnable
    {
        private const string URL = "https://noproblem.slack.com/services/hooks/incoming-webhook?token=oobWDdLagrK7dv35YpYAhNpX";
        private string message;
        private const string USER = "NPBOT";
        private string channel;

        public SlackNotifier(string message)
            : this(message, eSlackChannel.np_notifications)
        {
        }

        public SlackNotifier(string message, eSlackChannel channel)
        {
            this.message = message;
            this.channel = GetChannelFromEnum(channel);
        }

        private string GetChannelFromEnum(eSlackChannel channel)
        {
            switch (channel)
            {
                case eSlackChannel.new_request:
                    return "#new-request";
                default:
                    return "#np-notifications";
            }
        }

        public enum eSlackChannel
        {
            np_notifications, new_request
        }

        protected override void Run()
        {
            if (!GlobalConfigurations.IsProductionUsa())
            {
                return;
            }
            RestSharp.RestClient client = new RestSharp.RestClient(URL);
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.RequestFormat = RestSharp.DataFormat.Json;
            request.AddBody(new Data()
            {
                text = message,
                username = USER,
                channel = channel
            });
            client.Execute(request);
        }

        private class Data
        {
            public string text { get; set; }
            public string username { get; set; }
            public string channel { get; set; }
        }
    }
}
