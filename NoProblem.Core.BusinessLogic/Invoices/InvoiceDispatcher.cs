﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Invoices
{
    internal interface InvoiceDispatcher
    {
        void DispatchInvoice(Guid supplierId, decimal amount, CreditCardSolek.InvoiceDataContainer invoiceDataContainer, eInvoiceType type);
    }
}
