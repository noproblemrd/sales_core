﻿using System;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.Invoices
{
    public class FeaturedListingSubscriptionPlan : SubscriptionPlan
    {
        public override string Name
        {
            get { return SubscriptionPlansContract.Plans.FEATURED_LISTING; }
        }

        public override string GetBusinessPageUrl(string templateName, string supplierName, Guid supplierId)
        {
            string retVal = String.Empty;
            if (templateName == MandrillTemplates.E_AD_INVOICE_FEATURED_LISTING_REG
                || templateName == MandrillTemplates.E_AD_INVOICE_FEATURED_LISTING_MONTH
                || templateName == MandrillTemplates.E_AD_INVOICE_LEAD_BOOSTER_MONTH)
                retVal = "http://www.noproblemppc.com/business/" + System.Web.HttpUtility.UrlEncode(supplierName) + "/" + supplierId;
            return retVal;
        }

        public override string GetSubject(eInvoiceType invoiceType, string supplierName)
        {
            return string.Empty;
        }

        public override string GetTemplateName(eInvoiceType invoiceType)
        {
            if (invoiceType == eInvoiceType.Monthly)
            {
                return MandrillTemplates.E_AD_INVOICE_FEATURED_LISTING_MONTH;
            }

            if (invoiceType == eInvoiceType.Registration)
                return MandrillTemplates.E_AD_INVOICE_FEATURED_LISTING_REG;

            if (invoiceType == eInvoiceType.SingleLead)
                return MandrillTemplates.E_AD_INVOICE_SINGLE_LEAD_PAYMENT;

            return string.Empty;
        }
    }
}