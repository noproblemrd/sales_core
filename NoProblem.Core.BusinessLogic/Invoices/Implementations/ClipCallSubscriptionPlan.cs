using System;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.Invoices
{
    public class ClipCallSubscriptionPlan : SubscriptionPlan {
        public override string Name
        {
            get { return SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION; }
        }


        public override string GetBusinessPageUrl(string templateName, string supplierName, Guid supplierId)
        {
            return string.Empty;
        }

        public override string GetSubject(eInvoiceType invoiceType, string supplierName)
        {
            if (invoiceType == eInvoiceType.Registration)
                return string.Format("ClipCall Account Confirmation for {0}", supplierName);

            if (invoiceType == eInvoiceType.SingleLead)
                return string.Format("{0}: Your recent ClipCall charge", supplierName);

            throw new InvalidOperationException("Are you trying to");
        }

        public override string GetTemplateName(eInvoiceType invoiceType)
        {
            if (invoiceType == eInvoiceType.Registration)
                return MandrillTemplates.E_AD_INVOICE_CLIPCALL_REG;

            if (invoiceType == eInvoiceType.SingleLead)
                return MandrillTemplates.E_AD_INVOICE_CLIPCALL_SINGLE_LEAD_PAYMENT;

            throw new InvalidOperationException();
        }
    }
}