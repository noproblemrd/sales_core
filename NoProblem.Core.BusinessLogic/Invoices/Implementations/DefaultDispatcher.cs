﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using Microsoft.Crm.SdkTypeProxy;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.Invoices
{
    /// <summary>
    /// Default dispatcher, sends email to supplier.
    /// </summary>
    public class DefaultDispatcher :XrmUserBase, InvoiceDispatcher
    {
        #region InvoiceDispatcher Members


        public DefaultDispatcher() : base(null)
        {
            
        }

        public void DispatchInvoice(Guid supplierId, decimal amount, CreditCardSolek.InvoiceDataContainer invoiceDataContainer, eInvoiceType type)
        {
            var accountRepositoy = new DataAccessLayer.AccountRepository(XrmDataContext);

            var supplier = (from x in accountRepositoy.All
                            where x.accountid == supplierId
                            select new SupplierRef { SubscriptionPlan = x.new_subscriptionplan, SupplierName = x.name }).First();



            Sender sender = new Sender(invoiceDataContainer, type, supplier, amount);
            sender.Start();
        }

        #endregion

        public class Sender : Runnable
        {
            private readonly CreditCardSolek.InvoiceDataContainer invoiceDataContainer;
            private readonly eInvoiceType type;
            private SupplierRef supplier;
            private readonly decimal amount;

            public Sender(CreditCardSolek.InvoiceDataContainer invoiceDataContainer, eInvoiceType type, SupplierRef supplier, decimal amount)
            {
                this.invoiceDataContainer = invoiceDataContainer;
                this.type = type;
                this.amount = amount;
                this.supplier = supplier;

            }

            protected override void Run()
            {

                var store = new SubscriptionPlanStore();
                store.AddPlan(new ClipCallSubscriptionPlan
                {
                    FromName = "ClipCall",
                    FromEmail = "noreply@clipcall.it"

                });

                store.AddPlan(new LeadBoosterSubscriptionPlan
                {
                    FromName = "The NoProblem Team",
                    FromEmail = string.Empty

                });

                store.AddPlan(new FeaturedListingSubscriptionPlan
                {
                    FromName = "The NoProblem Team",
                    FromEmail = string.Empty

                });

             InvoiceData invoiceData = store.GetInvoiceData(new InvoiceContext
                {
                    InvoiceType = type,
                    SupplierId = supplier.SupplierId,
                    SubscriptionPlanName = supplier.SubscriptionPlan,
                    SupplierName = supplier.SupplierName
                });


                //string templateName = GetTemplateName(type, supplier.SubscriptionPlan);
                //if (string.IsNullOrEmpty(templateName))
                //    return;
                //string businessPageUrl = GetBusinessPageUrl(templateName, supplier.Name);
             Dictionary<string, string> vars = CreateVariables(supplier.SupplierName, invoiceData.BusinessPageUrl);

                var sender = new MandrillSender();

                var request = new MandrillRequest
                {
                    To = invoiceDataContainer.BillingEmail,
                    Bcc = null,
                    FromEmail = invoiceData.FromEmail,
                    FromName = invoiceData.FromName,
                    Parameters = vars,
                    Subject = invoiceData.Subject,
                    TemplateName = invoiceData.TemplateName
                };

                sender.SendTemplate(request);
            }

            private string GetBusinessPageUrl(string templateName, string supplierName)
            {
                string retVal = String.Empty;
                if(templateName == DataModel.MandrillTemplates.E_AD_INVOICE_FEATURED_LISTING_REG
                    || templateName == DataModel.MandrillTemplates.E_AD_INVOICE_FEATURED_LISTING_MONTH
                    || templateName == DataModel.MandrillTemplates.E_AD_INVOICE_LEAD_BOOSTER_MONTH)
                    retVal = "http://www.noproblemppc.com/business/" + System.Web.HttpUtility.UrlEncode(supplierName) + "/" + supplier.SupplierId;
                return retVal;
            }


            private Dictionary<string, string> CreateVariables(string name, string businessPageUrl)
            {
                var vars = new Dictionary<string, string>();
                vars.Add("NPVAR_AD_NAME", GetVariableValue(name));
                vars.Add("NPVAR_INVOICE_URL", GetVariableValue(invoiceDataContainer.InvoiceUrl));
                vars.Add("NPVAR_INVOICE_NUMBER", GetVariableValue(invoiceDataContainer.InvoiceNumber));
                vars.Add("NPVAR_DATE", DateTime.Now.ToString("dd MMM yyyy"));
                vars.Add("NPVAR_BILLING_NAME", invoiceDataContainer.BillingFirstName + " " + invoiceDataContainer.BillingLastName);
                vars.Add("NPVAR_BILLING_ADDRESS", invoiceDataContainer.BillingAddress1 + " " + invoiceDataContainer.BillingAddress2);
                vars.Add("NPVAR_BILLING_CITY", GetVariableValue(invoiceDataContainer.BillingCity));
                vars.Add("NPVAR_BILLING_STATE", GetVariableValue(invoiceDataContainer.BillingState));
                vars.Add("NPVAR_BILLING_ZIP", GetVariableValue(invoiceDataContainer.BillingZip));
                vars.Add("NPVAR_BILLING_COUNTRY", GetVariableValue(invoiceDataContainer.BillingCountry));
                vars.Add("NPVAR_BILLING_PHONE", GetVariableValue(invoiceDataContainer.BillingPhone));
                vars.Add("NPVAR_BILLING_EMAIL", GetVariableValue(invoiceDataContainer.BillingEmail));
                vars.Add("NPVAR_AMOUNT", String.Format("{0:0.00}", amount));
                vars.Add("NPVAR_BUSINESS_PAGE_URL", businessPageUrl);
                vars.Add("NPVAR_VERIFY_URL", GetVerificationUrl());
                vars.Add("NPVAR_BONUS_AMOUNT", GlobalConfigurations.GetRegistrationBonusAmount().ToString());
                return vars;
            }

            private string GetVerificationUrl()
            {
                DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string url = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.EMAIL_VERIFICATION_URL_REGISTRATION_2014);
                return url + "?id=" + supplier.SupplierId;
            }

            private string GetVariableValue(string currentValue)
            {
                return !String.IsNullOrEmpty(currentValue) ? currentValue : String.Empty;
            }

            private string GetTemplateName(eInvoiceType type, string plan)
            {
                string retVal = null;
                switch (type)
                {
                    case eInvoiceType.Monthly:
                        if (plan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.FEATURED_LISTING)
                        {
                            retVal = DataModel.MandrillTemplates.E_AD_INVOICE_FEATURED_LISTING_MONTH;
                        }
                        else if (plan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING)
                        {
                            retVal = DataModel.MandrillTemplates.E_AD_INVOICE_LEAD_BOOSTER_MONTH;
                        }
                        //else if (plan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans
                        //             .CLIP_CALL_SUBSCRIPTION)
                        //{
                        //    retVal = DataModel.MandrillTemplates.E_AD_INVOICE_LEAD_BOOSTER_MONTH;
                        //}
                        break;
                    case eInvoiceType.Registration:
                        if (plan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.FEATURED_LISTING)
                        {
                            retVal = DataModel.MandrillTemplates.E_AD_INVOICE_FEATURED_LISTING_REG;
                        }
                        else if (plan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING)
                        {
                            retVal = DataModel.MandrillTemplates.E_AD_INVOICE_LEAD_BOOSTER_REG_WITH_EMAIL_VERIFICATION;
                        }
                        else if (plan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION)
                        {
                            retVal = DataModel.MandrillTemplates.E_AD_INVOICE_CLIPCALL_REG;
                        }
                        break;
                    case eInvoiceType.SingleLead:
                        if (plan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.FEATURED_LISTING ||
                            plan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING)
                        {
                            retVal = DataModel.MandrillTemplates.E_AD_INVOICE_SINGLE_LEAD_PAYMENT;
                        }
                        else if (plan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION)
                        {
                            retVal = DataModel.MandrillTemplates.E_AD_INVOICE_CLIPCALL_SINGLE_LEAD_PAYMENT;
                        }
                        break;
                }
                return retVal;
            }
        }


        public class SupplierRef
        {
            public Guid SupplierId { get; set; }
            public string SupplierName { get; set; }
            public string SubscriptionPlan{ get; set; }
        }
    }

    public class SubscriptionPlanStore
    {
        private readonly List<SubscriptionPlan> subscriptionPlans;

        public SubscriptionPlanStore()
        {
            this.subscriptionPlans = new List<SubscriptionPlan>();
        }

        public virtual void AddPlan(SubscriptionPlan plan)
        {
            subscriptionPlans.Add(plan);
        }

        public virtual InvoiceData GetInvoiceData(InvoiceContext context)
        {
            SubscriptionPlan subscriptionPlan = subscriptionPlans.SingleOrDefault(p => p.Name == context.SubscriptionPlanName);
            if (subscriptionPlan == null)
                throw new InvalidOperationException("cannot get template for an invalid subscription plan named: " + context.SubscriptionPlanName);

            string templateName = subscriptionPlan.GetTemplateName(context.InvoiceType);
            string businessPageUrl = subscriptionPlan.GetBusinessPageUrl(templateName, context.SupplierName, context.SupplierId);
            string fromEmail = subscriptionPlan.FromEmail;
            string fromName = subscriptionPlan.FromName;
            string subject = subscriptionPlan.GetSubject(context.InvoiceType, context.SupplierName);

            var invoiceData = new InvoiceData
            {
                BusinessPageUrl = businessPageUrl,
                FromEmail = fromEmail,
                FromName = fromName,
                Subject = subject,
                TemplateName = templateName
            };
            return invoiceData;
        }
    }
}
