using System;

namespace NoProblem.Core.BusinessLogic.Invoices
{
    public abstract class SubscriptionPlan
    {
        public abstract string Name { get; }
        public abstract string GetBusinessPageUrl(string templateName, string supplierName, Guid supplierId);
        public abstract string GetSubject(eInvoiceType invoiceType,string supplierName);
        public abstract string GetTemplateName(eInvoiceType invoiceType);
        public string FromName { get; set; }
        public string FromEmail { get; set; }
    }
}