namespace NoProblem.Core.BusinessLogic.Invoices
{
    public class InvoiceData
    {
        public string Subject { get; set; }
        public string TemplateName { get; set; }
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string BusinessPageUrl { get; set; }
    }
}