using System;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.Invoices
{
    public class LeadBoosterSubscriptionPlan : SubscriptionPlan
    {
        public override string Name
        {
            get { return SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING; }
        }

        public override string GetBusinessPageUrl(string templateName, string supplierName, Guid supplierId)
        {
            string retVal = String.Empty;
            if (templateName == DataModel.MandrillTemplates.E_AD_INVOICE_FEATURED_LISTING_REG
                || templateName == DataModel.MandrillTemplates.E_AD_INVOICE_FEATURED_LISTING_MONTH
                || templateName == DataModel.MandrillTemplates.E_AD_INVOICE_LEAD_BOOSTER_MONTH)
                retVal = "http://www.noproblemppc.com/business/" + System.Web.HttpUtility.UrlEncode(supplierName) + "/" + supplierId;
            return retVal;
        }

        public override string GetSubject(eInvoiceType invoiceType, string supplierName)
        {
            return string.Empty;
        }

        public override string GetTemplateName(eInvoiceType invoiceType)
        {
            if (invoiceType == eInvoiceType.Monthly)
            {
                return MandrillTemplates.E_AD_INVOICE_LEAD_BOOSTER_MONTH;
            }

            if (invoiceType == eInvoiceType.Registration)
                return MandrillTemplates.E_AD_INVOICE_LEAD_BOOSTER_REG_WITH_EMAIL_VERIFICATION;

            if (invoiceType == eInvoiceType.SingleLead)
                return MandrillTemplates.E_AD_INVOICE_SINGLE_LEAD_PAYMENT;

            return null;   
        }
    }
}