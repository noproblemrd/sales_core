﻿using System;

namespace NoProblem.Core.BusinessLogic.Invoices
{
    public class InvoiceContext
    {
        public string SubscriptionPlanName { get; set; }
        public eInvoiceType InvoiceType { get; set; }
        public string SupplierName { get; set; }
        public Guid SupplierId { get; set; }
    }
}