﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Invoices
{
    public enum eInvoiceType
    {
        SingleLead, Registration, Monthly
    }
}
