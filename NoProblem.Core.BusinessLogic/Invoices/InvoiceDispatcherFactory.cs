﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Invoices
{
    internal class InvoiceDispatcherFactory
    {
        private InvoiceDispatcherFactory()
        {

        }

        /// <summary>
        /// Creates an instance of the relevant dispatcher.
        /// </summary>
        /// <param name="dispatcherName">The name of the dispacher. Send empty string for default dispatcher.</param>
        /// <returns>An instance of the relevant dispatcher.</returns>
        internal static InvoiceDispatcher CreateDispatcher(string dispatcherName)
        {
            switch (dispatcherName)
            {
                case "None":
                    return null;
            }
            return new DefaultDispatcher();
        }
    }
}
