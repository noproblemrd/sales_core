﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.CustomerBL
{
    public class LogOnManager : XrmUserBase
    {
        const string NewMobileUserNotification = "New mobile user: phone number: {0}";
        DataAccessLayer.Contact contactDal;

        public LogOnManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            contactDal = new DataAccessLayer.Contact(XrmDataContext);
        }

        public DataModel.Consumer.CustomerLogOnResponse LogOn(DataModel.Consumer.CustomerLogOnRequest request)
        {
            var customerData = contactDal.GetClipCallContactMinDataByPhoneNumber(request.PhoneNumber);
            Guid customerId;
            bool IsNew = false;
            if (customerData == null)
            {
                customerId = CreateCustomer(request.PhoneNumber);
                IsNew = true;
            }
            else
            {
                customerId = customerData.Id;
                UpdateIsMobileApp(customerId);
            }
            /*
            if (request.invitationid != Guid.Empty)
            {
                //Add favorite service provider
                ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
                    {
                        NoProblem.Core.DataAccessLayer.FavoriteServiceProvider.AddFavoriteServiceData data = (NoProblem.Core.DataAccessLayer.FavoriteServiceProvider.AddFavoriteServiceData)state;
                        NoProblem.Core.DataAccessLayer.FavoriteServiceProvider favoriteProvider = new DataAccessLayer.FavoriteServiceProvider(null);
                        favoriteProvider.AddFavoriteServiceProviderByInvitation(data.customerId, data.invitationId);
                    }), new NoProblem.Core.DataAccessLayer.FavoriteServiceProvider.AddFavoriteServiceData { customerId = customerId, invitationId = request.invitationid });
            }
             */
            string token = GetMobileToken(request.MobileLogOnData, customerId);
            if (IsNew)
            {
                DisableAllSameDeviceInOtherCustomers(request.MobileLogOnData, customerId);
                NoProblem.Core.BusinessLogic.ClipCall.CreateMobileSystemNotification noti = new ClipCall.CreateMobileSystemNotification();
                noti.SendInstallationApp(request.PhoneNumber);
            }
            return new DataModel.Consumer.CustomerLogOnResponse()
            {
                ApiToken = token,
                CustomerId = customerId
            };
        }
        public DataModel.Consumer.CustomerLogOnResponse ChangePhoneNumber(DataModel.Consumer.CustomerChangePhoneNumberRequest request)
        {
            List<NoProblem.Core.DataModel.SqlHelper.ContactMinData> ListCustomerData = 
                contactDal.GetContactMinDataByPhoneNumberExeptSupplier(request.PhoneNumber, request.customerId);
            Guid customerId;
            bool ToDisable = false;
            /*
            if (ListCustomerData == null || ListCustomerData.Count == 0)
            {
                customerId = request.customerId;
                UpdatePhoneNumberMobileApp(request.customerId, request.PhoneNumber);
            }
            else
            {
                ToDisable = true;
                
                if (!HasIncidentAccount(request.customerId))
                {
                    customerId = customerData.Id;
                    UpdateIsMobileApp(customerId);
                    SetActiveCustomer(request.customerId, false);
                }
                else
                {
                
                    customerId = request.customerId;
                    UpdatePhoneNumberMobileApp(request.customerId, request.PhoneNumber);
                    SetActiveCustomer(customerData.Id, false);
                }
            }
             * */
           
            
            customerId = request.customerId;
            UpdatePhoneNumberMobileApp(request.customerId, request.PhoneNumber);

            if (ListCustomerData != null && ListCustomerData.Count > 0)
            {
                foreach (NoProblem.Core.DataModel.SqlHelper.ContactMinData customerData in ListCustomerData)
                    SetActiveCustomer(customerData.Id, false);
            }
            SetInactiveAdvertisersExeptAccount(request.PhoneNumber, request.customerId);
            
            string token = GetMobileToken(request.MobileLogOnData, customerId);
            if (ToDisable)
            {
                DisableAllSameDeviceInOtherCustomers(request.MobileLogOnData, customerId);
             //   NoProblem.Core.BusinessLogic.ClipCall.CreateMobileSystemNotification noti = new ClipCall.CreateMobileSystemNotification();
             //   noti.SendInstallationApp(request.PhoneNumber);
            }
            return new DataModel.Consumer.CustomerLogOnResponse()
            {
                ApiToken = token,
                CustomerId = customerId
            };
        }

        private void SetInactiveAdvertisersExeptAccount(string phoneNumber, Guid contactId)
        {
            string command = @"
DECLARE @accountId uniqueidentifier

SELECT @accountId = New_AccountId
FROM dbo.ContactExtensionBase
WHERE ContactId = @contactId

DECLARE @table table
(
	AccountId uniqueidentifier
)

INSERT INTO @table
SELECT b.AccountId
FROM dbo.AccountBase b
	INNER JOIN dbo.AccountExtensionBase a on a.AccountId = b.AccountId
WHERE b.DeletionStateCode = 0 and a.New_IsMobile = 1
	and  Telephone1 = @phone and a.AccountId <> @accountId

UPDATE dbo.AccountBase
SET Telephone2 = Telephone1, Telephone1 = null
WHERE AccountId in (SELECT AccountId FROM @table)

UPDATE dbo.AccountExtensionBase
SET New_status = @inactiveStatus
WHERE AccountId in (SELECT AccountId FROM @table)";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@phone", phoneNumber);
                    cmd.Parameters.AddWithValue("@contactId", contactId);
                    cmd.Parameters.AddWithValue("@inactiveStatus", (int)NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Inactive);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        private bool HasIncidentAccount(Guid customerId)
        {
            string command = @"
select COUNT(*)
from dbo.IncidentBase
WHERE ContactId = @customerId";
            bool result;
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@customerId", customerId);
                    int count = (int)cmd.ExecuteScalar();
                    conn.Close();
                    result = count > 0;
                }
            }
            return result;
        }
        private void SetActiveCustomer(Guid customerId, bool toActive)
        {
            string command = @"UPDATE dbo.ContactBase
SET StatusCode = @status
WHERE ContactId = @customerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@status", (toActive ? (int)NoProblem.Core.DataModel.Xrm.contact.eStatusCode.Active :
                            (int)NoProblem.Core.DataModel.Xrm.contact.eStatusCode.Inactive));
                        cmd.Parameters.AddWithValue("@contactId", customerId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in SetActiveCustomer CustomerId:{0}, ToActive:{1}",
                    customerId, toActive));

            }
        }
        private bool UpdatePhoneNumberMobileApp(Guid customerId, string phoneNumber)
        {
            string command = @"
DECLARE @supplierId uniqueidentifier = null

SELECT @supplierId = New_AccountId
FROM dbo.ContactExtensionBase
WHERE ContactId = @contactId

IF(@supplierId IS NOT NULL)
BEGIN
	UPDATE dbo.AccountBase
	SET Telephone2 = Telephone1, Telephone1 = @phone
	WHERE AccountId = @supplierId
END

UPDATE dbo.ContactBase
SET Telephone1 = MobilePhone, MobilePhone = @phone
WHERE ContactId = @contactId";
            
            try
            {
                using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@phone", phoneNumber);
                        cmd.Parameters.AddWithValue("@contactId", customerId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in UpdatePhoneNumberMobileApp CustomerId:{0}", customerId));

                return false;
            }
            return true;
        }

        private void UpdateIsMobileApp(Guid customerId)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
                    {
                        Guid _customerId = (Guid)state;
                        string command = @"
UPDATE dbo.ContactExtensionBase
SET New_isMobileApp = @isMobileApp
WHERE ContactId = @contactId";
                        try
                        {
                            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
                            {
                                using (SqlCommand cmd = new SqlCommand(command, conn))
                                {
                                    conn.Open();
                                    cmd.Parameters.AddWithValue("@isMobileApp", true);
                                    cmd.Parameters.AddWithValue("@contactId", _customerId);
                                    cmd.ExecuteNonQuery();
                                    conn.Close();
                                }
                            }
                        }
                        catch (Exception exc)
                        {
                            Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in UpdateIsMobileApp CustomerId:{0}", _customerId));
                        }
                    }), customerId);
        }       

        private Guid CreateCustomer(string phoneNumber)
        {
            var contact = contactDal.CreateAnonymousContact(phoneNumber, null, null, null, false, null, null, null, null, null, null, true);
            return contact.contactid;
        }

        private string GetMobileToken(NoProblem.Core.DataModel.SupplierModel.Registration2014.Mobile.MobileLogOnData mobileLogOnData, Guid customerId)
        {
            APIs.MobileAPI.Devices.DevicesManager devicesManager = new APIs.MobileAPI.Devices.DevicesManager(XrmDataContext);
            string token = devicesManager.DeviceLogin(customerId, mobileLogOnData.DeviceName, mobileLogOnData.DeviceUId, mobileLogOnData.DeviceOS, mobileLogOnData.DeviceOSVersion, 
                mobileLogOnData.NpAppVersion, mobileLogOnData.isDebug);
            return token;
        }
        private void DisableAllSameDeviceInOtherCustomers(DataModel.SupplierModel.Registration2014.Mobile.MobileLogOnData mobileLogOnData, Guid customerId)
        {
            string command = @"
UPDATE dbo.New_mobiledevice
SET New_isactive = 0
WHERE New_isactive = 1
	and DeletionStateCode = 0
	and New_UID = @uid
	and New_ContactId <> @ContactId";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@uid", mobileLogOnData.DeviceUId);
                    cmd.Parameters.AddWithValue("@ContactId", customerId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
    }
}
