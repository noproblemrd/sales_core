﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.CustomerBL
{
    public class LoveManager : XrmUserBase
    {
        public LoveManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
           // contactDal = new DataAccessLayer.Contact(XrmDataContext);
        }
        public void AddLove(NoProblem.Core.DataModel.Consumer.LoveRequest request)
        {
           ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
           {
               DataModel.Xrm.DataContext _XrmDataContext = XrmDataContext;
               DataAccessLayer.Love loveDal = new DataAccessLayer.Love(_XrmDataContext);
               NoProblem.Core.DataModel.Consumer.LoveRequest _request = (NoProblem.Core.DataModel.Consumer.LoveRequest)state;
               DataModel.Xrm.new_love love = new DataModel.Xrm.new_love(_XrmDataContext);
               love.new_accountid = _request.SupplierId;
               love.new_contactid = _request.ContactId;
               loveDal.Create(love);
               XrmDataContext.SaveChanges();
           }), request);
        }
        private const string GetLovesQuery = @"
DECLARE @MyLove int, @AllLove int

SELECT @MyLove = COUNT(*)
FROM [dbo].[New_loveExtensionBase] 
WHERE New_AccountId = @AccountId and New_ContactId = @ContactId

SELECT @AllLove = COUNT(*)
FROM [dbo].[New_loveExtensionBase] 
WHERE New_AccountId = @AccountId 

SELECT @MyLove 'MyLove', @AllLove 'AllLove'";
        public NoProblem.Core.DataModel.Consumer.LoveResponse GetLoves(NoProblem.Core.DataModel.Consumer.LoveRequest request)
        {
            NoProblem.Core.DataModel.Consumer.LoveResponse response = new DataModel.Consumer.LoveResponse();
            using(SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GetLovesQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@AccountId", request.SupplierId);
                    cmd.Parameters.AddWithValue("@ContactId", request.ContactId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if(reader.Read())
                    {
                        response.MyLove = reader["MyLove"] == DBNull.Value ? 0 : (int)reader["MyLove"];
                        response.AllLove = reader["AllLove"] == DBNull.Value ? 0 : (int)reader["AllLove"];
                    }
                    else
                    {
                        response.MyLove = 0;
                        response.AllLove = 0;
                    }
                    conn.Close();
                }
            }
            return response;

        }
    }
}
