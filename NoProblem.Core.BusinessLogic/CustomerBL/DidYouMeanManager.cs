﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.CustomerBL
{
    public class DidYouMeanManager : XrmUserBase
    {
        public DidYouMeanManager(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            categoryDal = new DataAccessLayer.PrimaryExpertise(XrmDataContext);
        }

        private DataAccessLayer.PrimaryExpertise categoryDal;

        private const string LOCATION = "New York, NY";

        public DataModel.Expertise.DidYouMeanResponse DidYouMean(string searchTerm)
        {
            HashSet<string> catSet = SearchYelp(searchTerm);

            DataModel.Expertise.DidYouMeanResponse response = new DataModel.Expertise.DidYouMeanResponse();

            HashSet<string> notFoundInKnownCats = SearchKnownCategories(catSet, response);

            HashSet<string> notfoundInKeywords = SearchInKeywords(response, notFoundInKnownCats);

            foreach (var yelpCat in notfoundInKeywords)
            {
                response.AddCategory(yelpCat, Guid.Empty);
            }

            return response;
        }

        private HashSet<string> SearchInKeywords(DataModel.Expertise.DidYouMeanResponse response, HashSet<string> notFoundInKnownCats)
        {
            HashSet<string> notFoundInKeywords = new HashSet<string>();

            if (notFoundInKnownCats.Count > 0)
            {
                DataAccessLayer.SlikerKeyword keywordDal = new DataAccessLayer.SlikerKeyword(XrmDataContext);
                var keywords = keywordDal.GetAllSliderKeywords().Keywords;
                foreach (var yelpCat in notFoundInKnownCats)
                {
                    bool found = false;
                    foreach (var item in keywords)
                    {
                        if (item.Keyword.ToLower().Contains(yelpCat))
                        {
                            found = true;
                            response.AddCategory(GetCategoryName(item.HeadingId), item.HeadingId);
                            break;
                        }
                    }
                    if (!found)
                        notFoundInKeywords.Add(yelpCat);
                }
            }

            return notFoundInKeywords;
        }

        private HashSet<string> SearchKnownCategories(HashSet<string> catSet, DataModel.Expertise.DidYouMeanResponse response)
        {
            var categories = categoryDal.GetAllActive();

            Dictionary<string, DataModel.Expertise.ExpertiseWithIsActive> dict =
                new Dictionary<string, DataModel.Expertise.ExpertiseWithIsActive>();
            foreach (var knownCat in categories)
            {
                dict.Add(knownCat.Name.ToLower(), knownCat);
            }

            HashSet<string> notFoundInKnownCats = new HashSet<string>();

            foreach (var yelpCat in catSet)
            {
                DataModel.Expertise.ExpertiseWithIsActive knownCat;
                if (dict.TryGetValue(yelpCat, out knownCat))
                {
                    response.AddCategory(knownCat.Name, knownCat.Id);
                }
                else
                {
                    notFoundInKnownCats.Add(yelpCat);
                }
            }
            return notFoundInKnownCats;
        }

        private static HashSet<string> SearchYelp(string searchTerm)
        {
            HashSet<string> catSet = new HashSet<string>();
            var yelp = YelpHelper.GetYelp();
            var yelpResult = yelp.Search(searchTerm, LOCATION);
            if (yelpResult.businesses != null)
            {
                foreach (var business in yelpResult.businesses)
                {
                    if (business.categories != null)
                    {
                        foreach (var cat in business.categories)
                        {
                            if (cat != null && cat.Length > 0)
                            {
                                //Friendly name is first element in array.
                                catSet.Add(cat[0].ToLower());
                            }
                        }
                    }
                }
            }
            return catSet;
        }

        private string GetCategoryName(Guid categoryId)
        {
            return categoryDal.GetPrimaryExpertiseNameById(categoryId);
        }
    }
}
