﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic
{
    public class ValidatorBase
    {
        protected static void AddFailureValidation(DataModel.Result result, string failureReason)
        {
            result.Type = NoProblem.Core.DataModel.Result.eResultType.Failure;
            result.Messages.Add(failureReason);
        }
    }
}
