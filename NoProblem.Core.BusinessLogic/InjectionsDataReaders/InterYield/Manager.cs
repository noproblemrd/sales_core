﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.InterYield
{
    class Manager
    {
        private class Campaign
        {
            public string InjectionName { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
        }

        private const int TEN_SECS = 1000 * 10;
        private const int FIVE_MINUTES = 5;
        private const string URL_BASE = "https://api01.advertise.com/ads-wsapi/api";
        private const string DATE_FORMAT = "MM-dd-yyyy";
        private const string TIME_ZONE = "Pacific Standard Time";

        private static readonly List<Campaign> campaigns;

        static Manager()
        {
            campaigns = new List<Campaign>();
            Campaign interYield = new Campaign()
            {
                InjectionName = DataModel.Constants.InjectionNames.INTER_YIELD,
                UserName = "noprobppc",
                Password = "qazwsx!"
            };
            campaigns.Add(interYield);
            Campaign intext = new Campaign()
            {
                InjectionName = DataModel.Constants.InjectionNames.INTEXT,
                UserName = "noprobppc2",
                Password = "Am123456"
            };
            campaigns.Add(intext);
        }

        private RestSharp.IRestClient client;
        private DateTime yesterday;
        private DateTime nowLocal;
        private static InjectionsDataUtils utils = new InjectionsDataUtils();

        public Manager()
        {
            nowLocal = Utils.ConvertUtils.GetLocalFromUtc(DateTime.Now, TIME_ZONE);
            yesterday = nowLocal.Date.AddDays(-1);
            //init rest client
            client = new RestSharp.RestClient(URL_BASE);
        }

        internal void Work()
        {
            //Check not too early to ask for reports.
            if (nowLocal.Hour < 4)
                return;
            foreach (var campaign in campaigns)
            {
                try
                {
                    //check if yesterdays report exists
                    bool yesterdayDone = utils.CheckIfReportExistsInDate(yesterday, campaign.InjectionName);
                    if (!yesterdayDone)
                    {
                        //query api to prepare report
                        string reportId = AskForReport(campaign.UserName, campaign.Password);
                        if (reportId != null)
                        {
                            ReportData data = null;
                            //wait while it is ready and query to get it
                            DateTime waitStart = DateTime.Now;
                            while (true)
                            {
                                System.Threading.Thread.Sleep(TEN_SECS);
                                bool escape = false;
                                data = GetReport(reportId, campaign, ref escape);
                                if (data != null || escape)
                                {
                                    break;
                                }
                                if ((DateTime.Now - waitStart).Minutes >= FIVE_MINUTES)
                                {
                                    LogUtils.MyHandle.WriteToLog("IterYield waiting for report to be ready timed out. reportId = {0}", reportId);
                                    break;
                                }
                            }
                            //save the data to the db
                            Saver saver = new Saver();
                            saver.Save(data);
                        }
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in InterYield.Manager.Work while doing campaign {0}", campaign.InjectionName);
                }
            }
        }

        private ReportData GetReport(string reportId, Campaign campaign, ref bool escape)
        {
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
            request.Resource = "getreport";
            request.AddParameter("user", campaign.UserName);
            request.AddParameter("password", campaign.Password);
            request.AddParameter("scheduledreportid", reportId);
            var response = client.Execute(request);

            ReportData data = null;
            try
            {
                XElement doc = XElement.Parse(response.Content);
                var node = doc.Element("elements").Element("entity").Element("contents");
                var pendingPathNode = node.Element("getReportDetailResponse");
                var readyPathNode = node.Element("getSubAffPerfomanceReportResponse");
                if (pendingPathNode != null)
                {
                    if (pendingPathNode.Element("reportDetails").Element("scheduledReportStatus").Value != "Pending")
                    {
                        HandleErrorInRetrievingReport(reportId, response.Content, ref escape);
                    }
                }
                else if (readyPathNode != null)
                {
                    data = ParseReportXml(readyPathNode, campaign.InjectionName);
                }
                else
                {
                    HandleErrorInRetrievingReport(reportId, response.Content, ref escape);
                }
            }
            catch (Exception)
            {
                HandleErrorInRetrievingReport(reportId, response.Content, ref escape);
            }
            return data;
        }

        private ReportData ParseReportXml(XElement readyPathNode, string injectionName)
        {
            ReportData data = new ReportData(injectionName);
            foreach (var node in readyPathNode.Elements())
            {
                string subId = node.Element("subId").Value;
                Guid originId = GetOriginIdFromSubId(subId);
                string earningsNodeValue = node.Element("earnings").Value;
                decimal revenue = ParseRevenueFromNodeValue(earningsNodeValue);
                data.Rows.Add(
                    new ReportData.ReportRow()
                {
                    Date = yesterday,
                    OriginId = originId,
                    Revenue = revenue
                });
            }
            return data;
        }

        private decimal ParseRevenueFromNodeValue(string earningsNodeValue)
        {
            if (earningsNodeValue.StartsWith("$"))
            {
                earningsNodeValue = earningsNodeValue.Substring(1);
            }
            return decimal.Parse(earningsNodeValue);
        }

        private Guid GetOriginIdFromSubId(string subId)
        {
            Guid retVal;
            subId.TryParseToGuid(out retVal);
            return retVal;
        }

        private void HandleErrorInRetrievingReport(string reportId, string xml, ref bool escape)
        {
            LogUtils.MyHandle.WriteToLog("Unable to retrieve the actual report from InterYields reports API. ReportId: {1}. Response content: {0}", xml, reportId);
            escape = true;
        }

        private string AskForReport(string userName, string password)
        {
            string id = null;
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
            request.Resource = "schedulesubaffiliateperformancereport";
            request.AddParameter("user", userName);
            request.AddParameter("password", password);
            string yesterdayString = yesterday.ToString(DATE_FORMAT);
            request.AddParameter("startdate", yesterdayString);
            request.AddParameter("enddate", yesterdayString);
            var response = client.Execute(request);
            try
            {
                XElement doc = XElement.Parse(response.Content);
                id = doc.Element("elements").Element("entity").Element("contents").Element("scheduleSubAffiliatePerformanceReportResponse").Element("scheduledReports").Element("scheduledReportDetails").Element("scheduledReportId").Value;
            }
            catch (Exception)
            {
                LogUtils.MyHandle.WriteToLog("Unable to retrieve report id from InterYields report scheduler. Response content: {0}", response.Content);
            }
            return id;
        }       
    }
}

/*
 SCHEDULE RESPONSE:
 <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<globalResponse response="Success">
<elements><entity type="report">
<operationResult>Success</operationResult><errors/>
<contents><scheduleSubAffiliatePerformanceReportResponse>
<scheduledReports><scheduledReportDetails>
<scheduledReportId>840585</scheduledReportId>
<scheduledReportStatus>Pending</scheduledReportStatus>
<message>Scheduled the requested report. Please query after some time with the above id to view the report data.</message>
</scheduledReportDetails></scheduledReports></scheduleSubAffiliatePerformanceReportResponse></contents></entity></elements>
</globalResponse>
 
 
 PENDING RESPONSE:
 <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<globalResponse response="Success">
	<elements>
		<entity id="840588" type="report">
			<operationResult>Success</operationResult>
			<errors/>
			<contents>
				<getReportDetailResponse>
					<reportDetails>
						<scheduledReportId>840588</scheduledReportId>
						<scheduledReportStatus>Pending</scheduledReportStatus>
						<message>Report is Pending or In Process. Please query after some time with the above id to view the report data.</message>
					</reportDetails>
				</getReportDetailResponse>
			</contents>
		</entity>
	</elements>
</globalResponse>
 
 REPORT RESPONSE:
 <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<globalResponse response="SUCCESS">
	<elements>
		<entity id="840585" type="REPORT">
			<operationResult>SUCCESS</operationResult>
			<contents>
				<getSubAffPerfomanceReportResponse>
					<subAffiliatePerformanceReport>
						<subId></subId>
						<date>08-01-2014</date>
						<billedClicks>0</billedClicks>
						<earnings>$0.000</earnings>
						<searchCount>0</searchCount>
					</subAffiliatePerformanceReport>
					<subAffiliatePerformanceReport>
						<subId>07721550-71da-e311-b491-001517d1792a</subId>
						<date>08-01-2014</date>
						<billedClicks>6883</billedClicks>
						<earnings>$101.711</earnings
						<searchCount>105958</searchCount>
					</subAffiliatePerformanceReport>
					<subAffiliatePerformanceReport>
						<subId>23b30349-fd93-e311-b7da-001517d1792a</subId><date>08-01-2014</date><billedClicks>87</billedClicks><earnings>$0.926</earnings><searchCount>691</searchCount>
					</subAffiliatePerformanceReport>
					<subAffiliatePerformanceReport>
						<subId>2414e830-d9ef-e311-b4c4-001517d1792a</subId><date>08-01-2014</date><billedClicks>5020</billedClicks><earnings>$34.950</earnings><searchCount>93581</searchCount>
					</subAffiliatePerformanceReport>
					<subAffiliatePerformanceReport>
						<subId>33da79ee-82d1-e111-9538-001517d1792a</subId><date>08-01-2014</date><billedClicks>35</billedClicks><earnings>$0.103</earnings><searchCount>710</searchCount>
					</subAffiliatePerformanceReport>
					<subAffiliatePerformanceReport>
						<subId>39239192-99ae-e311-99c2-001517d1792a</subId><date>08-01-2014</date><billedClicks>2995</billedClicks><earnings>$28.066</earnings><searchCount>62045</searchCount>
					</subAffiliatePerformanceReport>
					<subAffiliatePerformanceReport>
						<subId>55caa5d1-66b3-e311-99c2-001517d1792a</subId><date>08-01-2014</date><billedClicks>66</billedClicks><earnings>$0.649</earnings><searchCount>433</searchCount></subAffiliatePerformanceReport><subAffiliatePerformanceReport><subId>6648C279-0DFA-E311-B4C4-001517D1792A</subId><date>08-01-2014</date><billedClicks>2</billedClicks><earnings>$0.013</earnings><searchCount>19</searchCount></subAffiliatePerformanceReport><subAffiliatePerformanceReport><subId>6765ec8e-dbee-e311-b4c4-001517d1792a</subId><date>08-01-2014</date><billedClicks>419</billedClicks><earnings>$2.663</earnings><searchCount>13757</searchCount></subAffiliatePerformanceReport><subAffiliatePerformanceReport><subId>7c420e32-fdc9-e311-99c2-001517d1792a</subId><date>08-01-2014</date><billedClicks>2540</billedClicks><earnings>$15.191</earnings><searchCount>34585</searchCount></subAffiliatePerformanceReport><subAffiliatePerformanceReport><subId>a840ca95-2306-e211-8a14-001517d1792a</subId><date>08-01-2014</date><billedClicks>49</billedClicks><earnings>$0.658</earnings><searchCount>322</searchCount></subAffiliatePerformanceReport><subAffiliatePerformanceReport><subId>b4e3fece-fcd5-e311-b491-001517d1792a</subId><date>08-01-2014</date><billedClicks>816</billedClicks><earnings>$5.821</earnings><searchCount>9420</searchCount></subAffiliatePerformanceReport><subAffiliatePerformanceReport><subId>c23bf9c5-d881-e311-b7da-001517d1792a</subId><date>08-01-2014</date><billedClicks>1830</billedClicks><earnings>$15.004</earnings><searchCount>23893</searchCount></subAffiliatePerformanceReport><subAffiliatePerformanceReport><subId>ca175672-0dca-e311-99c2-001517d1792a</subId><date>08-01-2014</date><billedClicks>4743</billedClicks><earnings>$49.594</earnings><searchCount>149090</searchCount></subAffiliatePerformanceReport><subAffiliatePerformanceReport><subId>cf4aee0a-a2ca-e111-9538-001517d1792a</subId><date>08-01-2014</date><billedClicks>308</billedClicks><earnings>$1.971</earnings><searchCount>3907</searchCount>
					</subAffiliatePerformanceReport>
				</getSubAffPerfomanceReportResponse>
			</contents>
		</entity>
	</elements>
</globalResponse>

*/
