﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.FirstOfferz
{
    public class Manager
    {
        private const string directory = @"\\10.154.28.136\ftp\firstofferz";
        //private const string directory_dev = @"D:\tmp\firstofferz";
        private const string injectionName = DataModel.Constants.InjectionNames.FIRST_OFFERZ;
        private const string DATE_FORMAT = "d-MMM-yy";
        private Saver saver;

        public Manager()
        {
            saver = new Saver();
        }

        public void Work()
        {           
            string[] filesNames = Directory.GetFiles(directory);
            foreach (var fileNameWithPath in filesNames)
            {
                try
                {
                    string fileName = Path.GetFileNameWithoutExtension(fileNameWithPath);
                    if (fileName.StartsWith("nop_"))
                    {
                        ReportData data = new ReportData(injectionName);
                        using (Utils.Csv.CsvFileReader reader = new Utils.Csv.CsvFileReader(fileNameWithPath))
                        {                            
                            Utils.Csv.CsvRow row = new Utils.Csv.CsvRow();
                            reader.ReadRow(row);//read the columns titles
                            row = new Utils.Csv.CsvRow();
                            while (reader.ReadRow(row))
                            {
                                if (row.Count >= 6)
                                {
                                    ReportData.ReportRow reportRow = new ReportData.ReportRow();
                                    reportRow.Date = DateTime.ParseExact(row[0], DATE_FORMAT, System.Globalization.CultureInfo.InvariantCulture);
                                    reportRow.OriginId = new Guid(row[1]);
                                    reportRow.Revenue = decimal.Parse(row[5]);
                                    data.Rows.Add(reportRow);
                                }
                                row = new Utils.Csv.CsvRow();
                            }
                        }
                        saver.Save(data);
                        File.Delete(fileNameWithPath);
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in FirstOfferz FTP when handling file {0}", fileNameWithPath);
                }
            }
        }
    }
}
