﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders
{
    public class ReportData
    {
        public string InjectionName { get; private set; }
        public List<ReportRow> Rows { get; private set; }

        public ReportData(string injectionName)
        {
            Rows = new List<ReportRow>();
            InjectionName = injectionName;
        }

        public class ReportRow
        {
            public DateTime Date { get; set; }
            public decimal Revenue { get; set; }
            public Guid? OriginId { get; set; }
        }
    }
}
