﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.JollyWallet
{
    public class Manager
    {
        private const string API_KEY = "36cf7d72-2824-11e4-8bad-0a4ca41b7fbe";
        private const string URL = "https://api.jollywallet.com/partners/distributor_activity";
        private const string DATE_FORMAT = "yyyy-MM-dd";
        private const string TIME_ZONE = "Pacific Standard Time";
        private static readonly string INJECTION_NAME = DataModel.Constants.InjectionNames.JOLLYWALLET;

        private DateTime reportDate;
        private DateTime nowLocal;
        private static InjectionsDataUtils utils = new InjectionsDataUtils();

        public Manager()
        {
            nowLocal = Utils.ConvertUtils.GetLocalFromUtc(DateTime.Now, TIME_ZONE);
            reportDate = nowLocal.Date.AddDays(-3);
        }

        public void Work()
        {
            //Check not too early to ask for reports.
            if (nowLocal.Hour < 4)
                return;
            //check if yesterdays report exists
            bool yesterdayDone = utils.CheckIfReportExistsInDate(reportDate, INJECTION_NAME);
            if (!yesterdayDone)
            {
                try
                {
                    RestSharp.RestClient client = new RestSharp.RestClient(URL);
                    ReportData data = new ReportData(INJECTION_NAME);
                    int page = 1;
                    int pageSize = 1000;
                    int totalPages = 1;
                    bool isLastPage = false;
                    while (!isLastPage)
                    {
                        var response = SendRequest(client, page, pageSize);
                        XElement doc = XElement.Parse(response.Content);
                        var headerE = doc.Element("header");
                        if (page == 1)
                        {
                            int total = int.Parse(headerE.Element("total").Value);
                            totalPages = (int)Math.Ceiling(((double)total / (double)pageSize));
                        }
                        if (page >= totalPages)
                        {
                            isLastPage = true;
                        }
                        string dataString = doc.Element("body").Element("data").Value;
                        string[] rows = dataString.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (var row in rows)
                        {
                            string[] cells = row.Split(',');
                            if (cells.Length >= 9)
                            {
                                data.Rows.Add(
                                    new ReportData.ReportRow()
                                    {
                                        Date = reportDate,
                                        OriginId = new Guid(cells[3]),
                                        Revenue = decimal.Parse(cells[8])
                                    });
                            }
                        }
                        page++;
                    }
                    //save the data to the db
                    Saver saver = new Saver();
                    saver.Save(data);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in JollyWallet.Manager.Work");
                }
            }
        }

        private RestSharp.IRestResponse SendRequest(RestSharp.RestClient client, int page, int pageSize)
        {
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
            request.AddParameter("key", API_KEY);
            request.AddParameter("date_to", reportDate.ToString(DATE_FORMAT));
            request.AddParameter("date_from", reportDate.ToString(DATE_FORMAT));
            request.AddParameter("page", page);
            request.AddParameter("page_size", pageSize);
            var res = client.Execute(request);
            return res;
        }


    }
}
