﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.Injection;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders
{
    public class InjectionManager
    {
        public InjectionManager() { }
        public List<InjectionData> GetInjectionData()
        {
            string command = @"SELECT inj.New_injectionId, inj.New_name, inj.New_scriptbefore, inj.New_Url, ISNULL(inj.New_Blockwithus, 0) New_Blockwithus, New_ScriptElementId
                                FROM dbo.New_injectionExtensionBase inj
	                                INNER JOIN dbo.New_injectionBase i on inj.New_injectionId = i.New_injectionId
                                WHERE i.DeletionStateCode = 0 AND ISNULL(inj.New_Url, '') <> ''
                                ORDER BY inj.New_name";
            List<InjectionData> list = new List<InjectionData>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        InjectionData data = new InjectionData();
                        data.InjectionId = (Guid)reader["New_injectionId"];
                        data.Name = (string)reader["New_name"];
                        data.ScriptBefore = (reader["New_scriptbefore"] == DBNull.Value) ? string.Empty : (string)reader["New_scriptbefore"];
                        data.ScriptUrl = (string)reader["New_Url"];
                        data.BlockWithUs = (bool)reader["New_Blockwithus"];
                        data.ScriptElementId = (reader["New_ScriptElementId"] == DBNull.Value) ? string.Empty : (string)reader["New_ScriptElementId"];
                        list.Add(data);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        public List<InjectionCampaign> GetInjectionCampaign()
        {
            string command = @"SELECT ie.New_InjectionId, ie.New_OriginId, ie.New_HitCounter, ie.New_DayCounter
                                FROM dbo.New_injectionoriginExtensionBase ie
	                                INNER JOIN dbo.New_injectionoriginBase ib on ie.New_injectionoriginId = ib.New_injectionoriginId
                                WHERE ib.DeletionStateCode = 0";
            List<InjectionCampaign> list = new List<InjectionCampaign>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        InjectionCampaign data = new InjectionCampaign();
                        data.InjectionId = (Guid)reader["New_InjectionId"];
                        data.OriginId = (Guid)reader["New_OriginId"];
                        data.HitCounter = (reader["New_HitCounter"] == DBNull.Value) ? 0 : (int)reader["New_HitCounter"];
                        data.DayCounter = (reader["New_DayCounter"] == DBNull.Value) ? 0 : (int)reader["New_DayCounter"];
                        list.Add(data);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        public List<InjectionCampaignParasite> GetInjectionParasiteCampaign()
        {
            string command = 
@"SELECT ie.New_InjectionId, ie.New_OriginId, New_isDefault, New_waitHits, New_hourInterval
FROM dbo.New_injectionparasiteoriginExtensionBase ie
    INNER JOIN dbo.New_injectionparasiteoriginBase ib on ie.New_injectionparasiteoriginId = ib.New_injectionparasiteoriginId
WHERE ib.DeletionStateCode = 0";
            List<InjectionCampaignParasite> list = new List<InjectionCampaignParasite>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        InjectionCampaignParasite data = new InjectionCampaignParasite();
                        data.InjectionId = (Guid)reader["New_InjectionId"];
                        data.OriginId = reader["New_OriginId"] == DBNull.Value ? Guid.Empty : (Guid)reader["New_OriginId"];
                        data.WaitHits = (reader["New_waitHits"] == DBNull.Value) ? 0 : (int)reader["New_waitHits"];
                        data.HourInterval = (reader["New_hourInterval"] == DBNull.Value) ? 0 : (int)reader["New_hourInterval"];
                        data.IsDefault = (reader["New_isDefault"] == DBNull.Value) ? false : (bool)reader["New_isDefault"];
                        list.Add(data);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        public List<Guid> GetParasiteOriginBlackList()
        {
            string command =
@"SELECT oe.New_OriginId
FROM dbo.New_parasiteoriginblacklistExtensionBase oe
    INNER JOIN dbo.New_parasiteoriginblacklistBase o on oe.New_parasiteoriginblacklistId = o.New_parasiteoriginblacklistId
WHERE o.DeletionStateCode = 0";
            List<Guid> list = new List<Guid>();
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {                        
                        list.Add((Guid)reader["New_OriginId"]);
                    }
                    conn.Close();
                }
            }
            return list;
        }
    }
}
