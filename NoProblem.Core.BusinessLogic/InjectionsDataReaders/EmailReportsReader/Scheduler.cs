﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader
{
    public class Scheduler
    {
        private static Timer timer;
        private const int ONE_MIN = 1000 * 60;
        private const int FIVE_MINS = ONE_MIN * 5;

        public static void OnApplicationStart()
        {
            DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.InjectionsEmails);
            timer = new Timer(TimerCallbackMethod, null, ONE_MIN, Timeout.Infinite);
        }

        private static void TimerCallbackMethod(object state)
        {
            try
            {
                if (DistributedLocksManager.AquireLock(DistributedLocksManager.eLocks.InjectionsEmails))
                {
                    try
                    {
                        Manager manager = new Manager();
                        manager.Work();
                    }
                    finally
                    {
                        DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.InjectionsEmails);
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in EmailReportsReader.Scheduler");
            }
            finally
            {
                timer = new Timer(TimerCallbackMethod, null, FIVE_MINS, Timeout.Infinite);
            }
        }
    }
}
