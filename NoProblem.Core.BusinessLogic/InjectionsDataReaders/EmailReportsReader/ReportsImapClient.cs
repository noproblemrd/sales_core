﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AE.Net.Mail;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader
{
    class ReportsImapClient : IDisposable
    {
        private ImapClient client;
        private const string IMAP = "imap.googlemail.com";
        private const int PORT = 993;
        private bool isActive;

        public ReportsImapClient()
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(null);
            string user = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INJECTION_EMAIL_REPORTS_GMAIL_USER);
            string password = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INJECTION_EMAIL_REPORTS_GMAIL_PASSWORD);
            if (String.IsNullOrEmpty(user) || String.IsNullOrEmpty(password))
            {
                isActive = false;
            }
            else
            {
                isActive = true;
                client = new ImapClient(IMAP, user, password, ImapClient.AuthMethods.Login, PORT, true, false);
            }
        }

        /// <summary>
        /// Retrieves the oldest unread message and marks it as read.
        /// </summary>
        /// <returns></returns>
        public MailMessage GetNewEmail()
        {
            if (!isActive)
                return null;
            var item = client.SearchMessages(SearchCondition.Unseen().And(SearchCondition.Unflagged())).OrderBy(x => x.Value.Date).FirstOrDefault();
            if (item == null)
                return null;
            client.SetFlags(Flags.Seen, item.Value);
            return item.Value;
        }

        /// <summary>
        /// Marks the message as flagged.
        /// </summary>
        /// <param name="mailMessage"></param>
        public void MarkEmailAsFlagged(MailMessage mailMessage)
        {
            if (!isActive)
                return;
            client.SetFlags(Flags.Flagged, mailMessage);
        }

        #region IDisposable Members

        public void Dispose()
        {
            client.Dispose();
        }

        #endregion
    }
}
