﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader
{
    public class Manager
    {
        public void Work()
        {
            using (ReportsImapClient imap = new ReportsImapClient())
            {
                var email = imap.GetNewEmail();
                while (email != null)
                {
                    try
                    {
                        ReadEmailIntoDB(email);  
                    }
                    catch (Exception exc)
                    {
                        EmailReportFailed(imap, email, exc);
                    }
                    email = imap.GetNewEmail();
                }
            }
        }

        private void EmailReportFailed(ReportsImapClient imap, AE.Net.Mail.MailMessage email, Exception exc)
        {
            LogUtils.MyHandle.HandleException(exc, "Exception handling email. Continueing to next one.");
            imap.MarkEmailAsFlagged(email);
        }

        private void ReadEmailIntoDB(AE.Net.Mail.MailMessage email)
        {
            Readers.IReader reader = Readers.ReaderFactory.GetReader(email);
            if (reader == null)
                throw new Exception("Could not find reader for email with subject: " + email.Subject);
            ReportData data = reader.Read(email);
            if (data == null || data.Rows.Count == 0)
                throw new Exception("Could not find anything to read in email with subject: " + email.Subject);
            Saver saver = new Saver();
            saver.Save(data);
        }
    }
}
