﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AE.Net.Mail;
using System.IO;
using NoProblem.Core.BusinessLogic.Utils.Csv;
using NoProblem.Core.BusinessLogic.Utils;
using Ionic.Zip;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader.Readers
{
    class SuperFish : SimpleCsvReader
    {
        private const string DATE_FORMAT = "yyyy-MM-dd";
        private Unzipper unzipper = new Unzipper();
      
        protected override int GetOriginIdIndex()
        {
            return 2;
        }

        protected override int GetRevenueIndex()
        {
            return 4;
        }

        protected override int GetDateIndex()
        {
            return 0;
        }

        protected override string GetDateFormat()
        {
            return DATE_FORMAT;
        }

        protected override List<Stream> GetAttachment(AE.Net.Mail.MailMessage mailMessage)
        {            
            var attachment = mailMessage.Attachments.First();
            return new List<Stream>() { unzipper.Unzip(attachment, ChooseFileFromZipAction) };
        }

        protected override string GetInjectionName()
        {
            return DataModel.Constants.InjectionNames.SUPERFISH;
        }

        private ZipEntry ChooseFileFromZipAction(ZipFile zip)
        {
            ZipEntry retVal = null;
            foreach (ZipEntry entry in zip)
            {
                if (entry.FileName.ToLower().Contains("revenue"))
                {
                    retVal = entry;
                    break;
                }
            }
            return retVal;
        }
    }
}
