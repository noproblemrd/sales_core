﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader.Readers
{
    class Engageya : SimpleCsvReader
    {
        private const string DATE_FORMAT = "dd/MM/yyyy";

        protected override int GetOriginIdIndex()
        {
            return 2;
        }

        protected override int GetRevenueIndex()
        {
            return 1;
        }

        protected override int GetDateIndex()
        {
            return 0;
        }

        protected override string GetDateFormat()
        {
            return DATE_FORMAT;
        }

        protected override List<System.IO.Stream> GetAttachment(AE.Net.Mail.MailMessage mailMessage)
        {
            var attachment = mailMessage.Attachments.First();
            MemoryStream ms = new MemoryStream();
            attachment.Save(ms);
            ms.Seek(0, SeekOrigin.Begin);
            return new List<Stream>() { ms };
        }

        protected override string GetInjectionName()
        {
            return DataModel.Constants.InjectionNames.ENGAGEYA;
        }
    }
}
