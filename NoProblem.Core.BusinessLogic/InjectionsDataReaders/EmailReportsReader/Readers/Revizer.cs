﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Ionic.Zip;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader.Readers
{
    public class Revizer : SimpleCsvReader
    {
        private const string DATE_FORMAT = "yyyy-MM-dd";
        private Unzipper unzipper = new Unzipper();

        protected override int GetOriginIdIndex()
        {
            return 3;
        }

        protected override int GetRevenueIndex()
        {
            return 5;
        }

        protected override int GetDateIndex()
        {
            return 0;
        }

        protected override string GetDateFormat()
        {
            return DATE_FORMAT;
        }

        protected override List<System.IO.Stream> GetAttachment(AE.Net.Mail.MailMessage mailMessage)
        {
            List<Stream> streams = new List<Stream>();
            foreach (var atts in mailMessage.Attachments)
            {
                if (atts.Filename.EndsWith(".zip"))
                {
                    List<Stream> lst = unzipper.UnzipAll(atts);
                    foreach (Stream zipStream in lst)
                    {
                        List<Stream> reportsStreams = unzipper.UnzipAllFromZipStream(zipStream);
                        streams.AddRange(reportsStreams);
                    }
                }
            }
            return streams;
        }

        protected override string GetInjectionName()
        {
            return DataModel.Constants.InjectionNames.REVIZER;
        }
    }
}
