﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AE.Net.Mail;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader.Readers
{
    class ReaderFactory
    {
        public static IReader GetReader(MailMessage mailMessage)
        {
            string subject = mailMessage.Subject.ToLower();
            if (subject.Contains("engageya"))
            {
                return new Engageya();
            }
            else if (subject.Contains("superfish"))
            {
                return new SuperFish();
            }
            if (mailMessage.From.Address.ToLower().Contains("revizer") || subject.Contains("revizer"))
            {
                return new Revizer();
            }
            foreach (var attachment in mailMessage.Attachments)
            {
                string fileName = attachment.Filename.ToLower();
                if (fileName.Contains("dealply"))
                {
                    return new Dealply();
                }
            }
            return null;
        }
    }
}
