﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NoProblem.Core.BusinessLogic.Utils.Csv;
using NoProblem.Core.BusinessLogic.Utils;
using Ionic.Zip;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader.Readers
{
    class Dealply : SimpleCsvReader
    {
        private const string DATE_FORMAT = "yyyy-MM-dd";
        private Unzipper unzipper = new Unzipper();

        protected override Guid? GetOriginId(CsvRow row)
        {
            string subid = row[GetOriginIdIndex()];            
            if (subid.StartsWith("nopr"))
            {
                subid = subid.Substring(4);
            }
            Guid originId;
            if (subid.TryParseToGuid(out originId))
            {
                return originId;
            }
            return null;
        }

        protected override decimal RevenueShareMultiplier
        {
            get
            {
                return 0.6m;
            }
        }

        protected override int GetOriginIdIndex()
        {
            return 2;
        }

        protected override int GetRevenueIndex()
        {
            return 5;
        }

        protected override int GetDateIndex()
        {
            return 0;
        }

        protected override string GetDateFormat()
        {
            return DATE_FORMAT;
        }

        protected override List<Stream> GetAttachment(AE.Net.Mail.MailMessage mailMessage)
        {            
            var attachment = mailMessage.Attachments.First();
            return new List<Stream>() { unzipper.Unzip(attachment, ChooseFileFromZipAction) };
        }

        protected override string GetInjectionName()
        {
            return DataModel.Constants.InjectionNames.DEALPLY;
        }

        private ZipEntry ChooseFileFromZipAction(ZipFile zip)
        {
            ZipEntry entry = zip.First();
            return entry;
        }
    }
}
