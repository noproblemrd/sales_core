﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AE.Net.Mail;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader.Readers
{
    interface IReader
    {
        ReportData Read(MailMessage mailMessage);
    }
}
