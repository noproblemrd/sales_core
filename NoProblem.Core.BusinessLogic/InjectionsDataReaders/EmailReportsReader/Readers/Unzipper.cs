﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Ionic.Zip;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader.Readers
{
    class Unzipper
    {
        public Stream Unzip(AE.Net.Mail.Attachment attachment, Func<ZipFile, ZipEntry> chooseFileFromZipAction)
        {
            Stream outputStream = new MemoryStream();
            using (MemoryStream ms = new MemoryStream())
            {
                attachment.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);
                using (ZipFile zip = ZipFile.Read(ms))
                {
                    ZipEntry entry = chooseFileFromZipAction(zip);
                    entry.Extract(outputStream);
                }
            }
            return outputStream;
        }

        public List<Stream> UnzipAll(AE.Net.Mail.Attachment attachment)
        {
            List<Stream> retVal = new List<Stream>();
            using (MemoryStream ms = new MemoryStream())
            {
                attachment.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);
                using (ZipFile zip = ZipFile.Read(ms))
                {
                    foreach (ZipEntry entry in zip)
                    {
                        Stream outputStream = new MemoryStream();
                        entry.Extract(outputStream);
                        retVal.Add(outputStream);
                    }
                }
            }
            return retVal;
        }

        public List<Stream> UnzipAllFromZipStream(Stream zipStream)
        {
            List<Stream> retVal = new List<Stream>();
            zipStream.Seek(0, SeekOrigin.Begin);
            using (ZipFile zip = ZipFile.Read(zipStream))
            {
                foreach (var entry in zip)
                {
                    Stream outputStream = new MemoryStream();
                    entry.Extract(outputStream);
                    retVal.Add(outputStream);
                }
            }
            return retVal;
        }
    }
}
