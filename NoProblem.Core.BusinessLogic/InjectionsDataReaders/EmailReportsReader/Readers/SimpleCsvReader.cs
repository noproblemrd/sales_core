﻿using System;
using System.IO;
using AE.Net.Mail;
using NoProblem.Core.BusinessLogic.Utils;
using NoProblem.Core.BusinessLogic.Utils.Csv;
using System.Collections.Generic;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.EmailReportsReader.Readers
{
    public abstract class SimpleCsvReader : IReader
    {
        public ReportData Read(AE.Net.Mail.MailMessage mailMessage)
        {
            ReportData data = new ReportData(GetInjectionName());
            List<Stream> streams = GetAttachment(mailMessage);
            if (streams == null || streams.Count == 0)
                throw new Exception("Failed to retrieve attachment from email with subject: " + mailMessage.Subject);
            foreach (var attachmentStream in streams)
            {
                using (attachmentStream)
                {
                    using (CsvFileReader reader = new CsvFileReader(attachmentStream))
                    {
                        CsvRow row = new CsvRow();
                        reader.ReadRow(row);//read the columns titles
                        row = new CsvRow();
                        while (reader.ReadRow(row))
                        {
                            ReportData.ReportRow rr = new ReportData.ReportRow();
                            rr.Date = DateTime.ParseExact(row[GetDateIndex()], GetDateFormat(), System.Globalization.CultureInfo.InvariantCulture);
                            rr.OriginId = GetOriginId(row);
                            rr.Revenue = RevenueShareMultiplier * decimal.Parse(row[GetRevenueIndex()]);
                            data.Rows.Add(rr);
                            row = new CsvRow();
                        }
                    }
                }
            }
            return data;
        }

        protected virtual decimal RevenueShareMultiplier
        {
            get { return 1; }
        }

        protected virtual Guid? GetOriginId(CsvRow row)
        {
            string subid = row[GetOriginIdIndex()];
            Guid originId;
            if (subid.TryParseToGuid(out originId))
            {
                return originId;
            }
            return null;
        }

        protected abstract int GetOriginIdIndex();

        protected abstract int GetRevenueIndex();

        protected abstract int GetDateIndex();

        protected abstract string GetDateFormat();

        protected abstract List<Stream> GetAttachment(MailMessage mailMessage);

        protected abstract string GetInjectionName();
    }
}
