﻿using Effect.Crm.Logs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.Adcash
{
    internal class Manager
    {
    //    private const string USER_NAME = "borisk@noproblemppc.com";
        private const string USER_NAME = "segevn@noproblemppc.com";
        private const string PASSWORD = "adcash123";
        private const string URL_LOGIN = "https://www.adcash.com/console/login_proxy.php";
        private const string URL_CALL = " https://www.adcash.com/console/api_proxy.php";
        private const string TIME_ZONE = "Pacific Standard Time";
        private static readonly string INJECTION_NAME = DataModel.Constants.InjectionNames.ADCASH;
        private const string DATE_FORMAT = "yyyy-MM-dd";

        private DateTime reportDate;
        private DateTime nowLocal;
        private string reportDateString;

        private static InjectionsDataUtils utils = new InjectionsDataUtils();

        public Manager()
        {
            nowLocal = Utils.ConvertUtils.GetLocalFromUtc(DateTime.Now, TIME_ZONE);
            reportDate = nowLocal.Date.AddDays(-3);
            reportDateString = reportDate.ToString(DATE_FORMAT);
        }

        public void Work()
        {
            //Check not too early to ask for reports.
            if (nowLocal.Hour < 4)
                return;
            //check if report for the date exists
            bool dateDone = utils.CheckIfReportExistsInDate(reportDate, INJECTION_NAME);
            if (!dateDone)
            {
                try
                {
                    RestSharp.RestClient clientLogin = new RestSharp.RestClient(URL_LOGIN);
                    RestSharp.RestRequest tokenRequest = new RestSharp.RestRequest(RestSharp.Method.POST);
                    tokenRequest.AddParameter("login", USER_NAME);
                    tokenRequest.AddParameter("password", PASSWORD);
                    var tokenResponse = clientLogin.Execute(tokenRequest);
                    var tokenObject = Newtonsoft.Json.JsonConvert.DeserializeObject<TokenResponse>(tokenResponse.Content);
                    string token = tokenObject.token;
                    RestSharp.RestRequest reportRequest = new RestSharp.RestRequest(RestSharp.Method.POST);
                    reportRequest.AddParameter("token", token);
                    reportRequest.AddParameter("call", "get_publisher_detailed_statistics");
                    reportRequest.AddParameter("start_date", reportDateString);
                    reportRequest.AddParameter("end_date", reportDateString);
                    RestSharp.RestClient clientCall = new RestSharp.RestClient(URL_CALL);
                    var reportResponse = clientCall.Execute(reportRequest);
                    var reportResponseObj = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiDataResponse>(reportResponse.Content);
                    ReportData data = new ReportData(INJECTION_NAME);
                    decimal revenue = reportResponseObj.Totals.Earnings;
                    data.Rows.Add(
                        new ReportData.ReportRow()
                        {
                            Date = reportDate,
                            Revenue = revenue
                        });

                    //save the data to the db
                    Saver saver = new Saver();
                    saver.Save(data);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in Adcash.Manager.Work");
                }
            }
        }

        private class TokenResponse
        {
            public string token { get; set; }
        }

        private class ApiDataResponse
        {
            public TotalsC Totals { get; set; }

            public class TotalsC
            {
                public decimal Earnings { get; set; }
            }
        }
    }
}
