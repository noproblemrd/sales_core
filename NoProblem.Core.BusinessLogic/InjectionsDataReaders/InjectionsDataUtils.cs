﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders
{
    internal class InjectionsDataUtils
    {
        public bool CheckIfReportExistsInDate(DateTime date, string injectionName)
        {
            DataAccessLayer.Injection injectionDal = new DataAccessLayer.Injection(null);
            Guid injectionId = injectionDal.GetIdByName(injectionName);
            DataAccessLayer.InjectionReport reportDal = new DataAccessLayer.InjectionReport(injectionDal.XrmDataContext);
            var firstFound = (from r in reportDal.All
                              where r.new_injectionid == injectionId
                              && r.new_date == date
                              select r.new_injectionreportid).FirstOrDefault();
            bool yesterdayDone = firstFound != Guid.Empty;
            return yesterdayDone;
        }
    }
}
