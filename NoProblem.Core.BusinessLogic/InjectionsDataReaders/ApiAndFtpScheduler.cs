﻿using System;
using System.Configuration;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders
{
    public class ApiAndFtpScheduler
    {
        private static Timer timer;
        private const int ONE_MIN = 1000 * 60;
        private const int HALF_HOUR = ONE_MIN * 30;

        public static void OnApplicationStart()
        {
            DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.FirstOfferzFtp);
            DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.InterYieldApi);
            DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.AdmediaApi);
            DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.AdcashApi);
            if (!IsTest())
                timer = new Timer(TimerCallbackMethod, null, ONE_MIN, Timeout.Infinite);
        }

        private static bool IsTest()
        {
            return ConfigurationManager.AppSettings.Get("SiteId") != DataModel.Constants.SiteIds.SALES;
        }

        private static void TimerCallbackMethod(object state)
        {
            try
            {
                ExecuteAdmediaApi();
                ExecuteAdcashApi();
               // ExecuteFirstOfferzFtp();
                ExecuteInterYieldApi();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ApiAndFtpScheduler");
            }
            finally
            {
                timer = new Timer(TimerCallbackMethod, null, HALF_HOUR, Timeout.Infinite);
            }
        }

        private static void ExecuteAdcashApi()
        {
            if (DistributedLocksManager.AquireLock(DistributedLocksManager.eLocks.AdcashApi))
            {
                try
                {
                    Adcash.Manager manager = new Adcash.Manager();
                    manager.Work();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in ApiAndFtpScheduler => Adcash API");
                }
                finally
                {
                    DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.AdcashApi);
                }
            }
        }

        private static void ExecuteAdmediaApi()
        {
            if (DistributedLocksManager.AquireLock(DistributedLocksManager.eLocks.AdmediaApi))
            {
                try
                {
                    AdMedia.Manager manager = new AdMedia.Manager();
                    manager.Work();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in ApiAndFtpScheduler => Admedia API");
                }
                finally
                {
                    DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.AdmediaApi);
                }
            }
        }

        private static void ExecuteFirstOfferzFtp()
        {
            if (DistributedLocksManager.AquireLock(DistributedLocksManager.eLocks.FirstOfferzFtp))
            {
                try
                {
                    FirstOfferz.Manager manager = new FirstOfferz.Manager();
                    manager.Work();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in ApiAndFtpScheduler => FirstOfferz FTP");
                }
                finally
                {
                    DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.FirstOfferzFtp);
                }
            }
        }

        private static void ExecuteJollyWalletApi()
        {
            if (DistributedLocksManager.AquireLock(DistributedLocksManager.eLocks.JollyWalletApi))
            {
                try
                {
                    JollyWallet.Manager manager = new JollyWallet.Manager();
                    manager.Work();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in ApiAndFtpScheduler => JollyWallet API");
                }
                finally
                {
                    DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.JollyWalletApi);
                }
            }
        }

        private static void ExecuteInterYieldApi()
        {
            if (DistributedLocksManager.AquireLock(DistributedLocksManager.eLocks.InterYieldApi))
            {
                try
                {
                    InterYield.Manager manager = new InterYield.Manager();
                    manager.Work();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in ApiAndFtpScheduler => InterYield API");
                }
                finally
                {
                    DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.InterYieldApi);
                }
            }
        }
    }
}
