﻿using Effect.Crm.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders.AdMedia
{
    class Manager
    {
        private const string API_KEY = "f13449704523f69a6e526e289a134c80";
        private const string AID = "57282";
        private const string URL = "http://api.admedia.com/subid_report.php";
        private const string TIME_ZONE = "Pacific Standard Time";
        private static readonly string INJECTION_NAME = DataModel.Constants.InjectionNames.ADMEDIA_INTEXT;

        private DateTime reportDate;
        private DateTime nowLocal;
        private static InjectionsDataUtils utils = new InjectionsDataUtils();

        public Manager()
        {
            nowLocal = Utils.ConvertUtils.GetLocalFromUtc(DateTime.Now, TIME_ZONE);
            reportDate = nowLocal.Date.AddDays(-3);
        }

        public void Work()
        {
            //Check not too early to ask for reports.
            if (nowLocal.Hour < 4)
                return;
            //check if report for the date exists
            bool dateDone = utils.CheckIfReportExistsInDate(reportDate, INJECTION_NAME);
            if (!dateDone)
            {
                try
                {
                    RestSharp.RestClient client = new RestSharp.RestClient(URL);
                    RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
                    request.AddParameter("year", reportDate.Year);
                    request.AddParameter("month", reportDate.Month);
                    request.AddParameter("day", reportDate.Day);
                    request.AddParameter("aid", AID);
                    request.AddParameter("auth", API_KEY);
                    var response = client.Execute(request);
                    XElement doc = XElement.Parse(response.Content);
                    var dataElement = doc.Element("data");
                    if (dataElement != null)
                    {
                        ReportData data = new ReportData(INJECTION_NAME);
                        foreach (var element in dataElement.Elements("listing"))
                        {
                            decimal revenue;
                            try
                            {
                                revenue = decimal.Parse(element.Element("Revenue").Value);
                            }
                            catch
                            {
                                revenue = 0;
                            }
                            if (revenue > 0)
                            {
                                data.Rows.Add(
                                    new ReportData.ReportRow()
                                    {
                                        Date = reportDate,
                                        OriginId = GetOriginFromXmlElement(element.Element("Subid")),
                                        Revenue = revenue
                                    });
                            }
                        }
                        //save the data to the db
                        Saver saver = new Saver();
                        saver.Save(data);
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in AdMedia.Manager.Work");
                }
            }
        }

        private const int GUID_LENGTH = 36;

        private Guid GetOriginFromXmlElement(XElement xElement)
        {
            Guid retVal = Guid.Empty;
            string value = xElement.Value;
            if (value != null)
            {
                if (value.Length > GUID_LENGTH)
                {
                    value = value.Substring(value.Length - GUID_LENGTH);
                }
                try
                {
                    retVal = new Guid(value);
                }
                catch
                {
                    retVal = Guid.Empty;
                }
            }
            return retVal;
        }
    }
}
