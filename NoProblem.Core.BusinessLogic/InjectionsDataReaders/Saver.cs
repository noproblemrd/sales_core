﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.InjectionsDataReaders
{
    class Saver : XrmUserBase
    {
        DataAccessLayer.Injection injectionDal;
        DataAccessLayer.InjectionReport reportDal;

        public Saver()
            : base(null)
        {
            injectionDal = new DataAccessLayer.Injection(XrmDataContext);
            reportDal = new DataAccessLayer.InjectionReport(XrmDataContext);
        }

        public void Save(ReportData data)
        {
            if (data == null)
                return;
            Guid injectionId = injectionDal.GetIdByName(data.InjectionName);
            if (injectionId == Guid.Empty)
            {
                throw new Exception(String.Format("Injection with name {0} not found. Report was not saved.", data.InjectionName));                
            }
            List<ReportData.ReportRow> cleanRows = GetCleanRows(data, injectionId);
            foreach (var row in cleanRows)
            {
                try
                {
                    var entity = DataModel.EntityFactories.InjectionReportEntityFactory.Create(row.OriginId, injectionId, row.Revenue, row.Date);
                    reportDal.Create(entity);
                    XrmDataContext.SaveChanges();
                }
                catch (Exception exc)
                {
                    XrmDataContext.ClearChanges();
                    LogUtils.MyHandle.HandleException(exc, "Exception in EmailReportsReader.Saver. Unable to save one row, going to next one. Injection = {0}.", data.InjectionName);
                }
            }
        }

        private List<ReportData.ReportRow> GetCleanRows(ReportData data, Guid injectionid)
        {
            List<ReportData.ReportRow> retVal = new List<ReportData.ReportRow>();
            var newDates = (from r in data.Rows
                            select r.Date.Date).Distinct<DateTime>();
            var existingDates = (from r in reportDal.All
                                 where r.new_injectionid == injectionid
                                 select r.new_date.Value.Date).Distinct<DateTime>();
            List<DateTime> datesToUse = new List<DateTime>();
            foreach (var d in newDates)
            {
                if (!existingDates.Contains(d))
                {
                    datesToUse.Add(d);
                }
            }
            foreach (var r in data.Rows)
            {
                if (datesToUse.Contains(r.Date.Date))
                {
                    retVal.Add(r);
                }
            }
            return retVal;
        }
    }
}
