﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
//sing RestSharp.Contrib;
using System.Net;
using Effect.Crm.Logs;
using RestSharp.Extensions.MonoHttp;

namespace NoProblem.Core.BusinessLogic
{
    internal class TtsFileCreator : XrmUserBase
    {
        public TtsFileCreator()
            :base(null)
        {
            asteriskCofig = new NoProblem.Core.DataAccessLayer.AsteriskConfiguration(XrmDataContext);
        }

        DataAccessLayer.AsteriskConfiguration asteriskCofig;

        private string _ttsFilesSaveDirectory;
        private string TtsFilesSaveDirectory
        {
            get
            {
                if (string.IsNullOrEmpty(_ttsFilesSaveDirectory))
                {
                    _ttsFilesSaveDirectory = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.TTS_FILES_SAVE_DIRECTORY);
                }
                return _ttsFilesSaveDirectory;
            }
        }

        private string _ttsFilesUrlBase;
        private string TtsFilesUrlBase
        {
            get
            {
                if (string.IsNullOrEmpty(_ttsFilesUrlBase))
                {
                    _ttsFilesUrlBase = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.TTS_FILES_URL_BASE);
                }
                return _ttsFilesUrlBase;
            }
        }

        private string _ttsEncoding;
        private string TtsEnconding
        {
            get
            {
                if (string.IsNullOrEmpty(_ttsEncoding))
                {
                    _ttsEncoding = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.ENCODING);
                }
                return _ttsEncoding;
            }
        }

        private string _lang;
        private string Lang
        {
            get
            {
                if (string.IsNullOrEmpty(_lang))
                {
                    _lang = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.LANG);
                }
                return _lang;
            }
        }

        private string _voice;
        private string Voice
        {
            get
            {
                if (string.IsNullOrEmpty(_voice))
                {
                    _voice = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.VOICE);
                }
                return _voice;
            }
        }

        private string _ttsAddress;
        private string TtsAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_ttsAddress))
                {
                    _ttsAddress = asteriskCofig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.TTS_ADDRESS);
                }
                return _ttsAddress;
            }
        }

        internal string GetTtsFileUrl(string id, string txt)
        {
            DateTime d = DateTime.Now;
            string fullDirectory = TtsFilesSaveDirectory + "\\" + d.Year + "\\" + d.Month + "\\" + d.Day;
            if (!Directory.Exists(fullDirectory))
            {
                Directory.CreateDirectory(fullDirectory);
            }
            string fullPath = fullDirectory + "\\" + id + ".wav";
            if (!File.Exists(fullPath))
            {
                if (!CreateTtsFile(id, txt, fullPath))
                {
                    return string.Empty;
                }
            }
            string url = TtsFilesUrlBase + "/" + d.Year + "/" + d.Month + "/" + d.Day + "/" + id + ".wav";
            return url;
        }

        private bool CreateTtsFile(string id, string txt, string fullPath)
        {
            WebRequest request = WebRequest.Create(TtsAddress);
            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/x-www-form-urlencoded";
            string parameters = "lang=" + HttpUtility.UrlEncode(Lang);
            parameters += "&voice=" + HttpUtility.UrlEncode(Voice);
            parameters += "&tts=" + HttpUtility.UrlEncode(txt);
            parameters += "&encoding=" + HttpUtility.UrlEncode(TtsEnconding);
            request.ContentLength = parameters.Length;
            using (StreamWriter sw = new StreamWriter(request.GetRequestStream()))
            {
                sw.Write(parameters);
            }
            bool result = false;
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (var rs = response.GetResponseStream())
                    {
                        int bufferSize = 1024;
                        byte[] buffer = new byte[bufferSize];
                        int bytesRead = 0;
                        using (FileStream fileStream = File.Create(fullPath))
                        {
                            while ((bytesRead = rs.Read(buffer, 0, bufferSize)) != 0)
                            {
                                fileStream.Write(buffer, 0, bytesRead);
                            }
                        }
                        result = true;
                    }
                }
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        string text = sr.ReadToEnd();
                        LogUtils.MyHandle.HandleException(e, "Exception in CreateTtsFile. HttpResponse.StatusCode = {0}. result = {1}. JonaId = {2}, text = {3}, fullPath = {4}", httpResponse.StatusCode.ToString(), text, id, txt, fullPath);
                        result = false;
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateTtsFile. jonaId = {0}, text = {1}, fullPath = {2}", id, txt, fullPath);
                result = false;
            }
            return result;
        }
    }
}
