﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher
{
    internal class PusherEventSender
    {
        private static readonly string PUSHER_APP_ID;
        private static readonly string PUSHER_APP_KEY;
        private static readonly string PUSHER_APP_SECRET;

        static PusherEventSender()
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(null);
            PUSHER_APP_ID = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PUSHER_APP_ID);
            PUSHER_APP_KEY = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PUSHER_APP_KEY);
            PUSHER_APP_SECRET = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PUSHER_APP_SECRET);
        }

        public static void TriggerEvent(PusherEvent pusherEvent)
        {
            SenderThread thread = new SenderThread(pusherEvent);
            thread.Start();
        }
        public static PusherServer.IGetResult<object> getchannel(string channel)
        {
            PusherServer.Pusher pusherServer = new PusherServer.Pusher(PUSHER_APP_ID, PUSHER_APP_KEY, PUSHER_APP_SECRET);
            PusherServer.IGetResult<object> result = pusherServer.Get<object>("/channels/" + channel);
            return result;
        }
        public static PusherServer.IGetResult<PusherServer.ChannelsList> getchannels()
        {
            PusherServer.Pusher pusherServer = new PusherServer.Pusher(PUSHER_APP_ID, PUSHER_APP_KEY, PUSHER_APP_SECRET);
             return pusherServer.Get<PusherServer.ChannelsList>("/channels");
        }

        private class SenderThread : Runnable
        {
            public SenderThread(PusherEvent pusherEvent)
            {
                this.pusherEvent = pusherEvent;
            }

            private PusherEvent pusherEvent;

            protected override void Run()
            {
                PusherServer.Pusher pusherServer = new PusherServer.Pusher(PUSHER_APP_ID, PUSHER_APP_KEY, PUSHER_APP_SECRET);
                var triggerResult = pusherServer.Trigger(pusherEvent.Channel, pusherEvent.EventName, pusherEvent.Data);
            }
        }
    }
}
