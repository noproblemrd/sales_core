﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher
{
    internal class PusherEvent
    {
        public PusherEvent(string channel, string eventName, object data)
        {
            Channel = channel;
            EventName = eventName;
            Data = data;
        }

        public string Channel { get; private set; }
        public string EventName { get; private set; }
        public object Data { get; private set; }
    }
}
