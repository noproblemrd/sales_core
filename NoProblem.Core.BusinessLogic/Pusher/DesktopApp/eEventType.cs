﻿
namespace NoProblem.Core.BusinessLogic.Pusher.DesktopApp
{
    internal enum eEventType
    {
        inject_js,
        set_size,
        disconnect
    }
}