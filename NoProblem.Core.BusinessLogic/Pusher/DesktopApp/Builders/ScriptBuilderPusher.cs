﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher.DesktopApp.Builders
{
    internal abstract class ScriptBuilderPusher
    {

        public virtual string BuildJs(object data, string method)
        {
            StringBuilder builder = new StringBuilder();
            if (data != null)
            {
                if (data.GetType() == typeof(string))
                    builder.Append(method).Append("('" + (string)data + "');");
                else
                {
                    string objJson = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                    builder.Append("var data = ").Append(objJson).Append("; ").Append(method).Append("(data);");
                }
            }
            else
            {
                builder.Append(method).Append("();");
            }
            return builder.ToString();
        }
        
    }
    
}
