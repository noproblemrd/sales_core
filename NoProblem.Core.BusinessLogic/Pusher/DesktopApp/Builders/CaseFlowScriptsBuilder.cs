﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher.DesktopApp.Builders
{
    internal class CaseFlowScriptsBuilder : ScriptBuilderPusher
    {
        
        private const string CASE_ALL_DATA_METHOD = "caseDataReady";
        private const string CALLING_CUSTOMER_METHOD = "callingCustomer";
        private const string CUSTOMER_CONNECTED = "customerConnected";
        private const string CALL_ENDED = "callEnded";

        public CaseFlowScriptsBuilder() { }

        public string BuildCaseAllDataScript(DataModel.Pusher.CaseFlow.CaseAllDataContainer data)
        {
            return BuildJs(data, CASE_ALL_DATA_METHOD);
        }

        public string BuildCallingCustomerScript(DataModel.Pusher.CaseFlow.CallStatusContainer data)
        {
            return BuildJs(data, CALLING_CUSTOMER_METHOD);
        }

        public string BuildCustomerConnectedScript(DataModel.Pusher.CaseFlow.CallStatusContainer data)
        {
            return BuildJs(data, CUSTOMER_CONNECTED);
        }

        public string BuildCallEndedScript(bool hasMoreSuppliers)
        {
            var data = new DataModel.Pusher.CaseFlow.CallEndedContainer();
            data.HasMoreSuppliers = hasMoreSuppliers;
            return BuildJs(data, CALL_ENDED);
        }

        
    }
}
