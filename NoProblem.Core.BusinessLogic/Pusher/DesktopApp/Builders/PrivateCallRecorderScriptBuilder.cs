﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher.DesktopApp.Builders
{
    internal class PrivateCallRecorderScriptBuilder : ScriptBuilderPusher
    {
        private const string PRIVATE_RECORD_ALICE_CALLING = "weAreCallingYouNow";
        private const string PRIVATE_RECORD_ALICE_ANSWERED = "YouAreAnswered";
        private const string PRIVATE_RECORD_BOB_CALLING = "weAreCallingToBob";
        private const string PRIVATE_RECORD_TWO_SIDES_CONNECTED = "connectToTheDial";
        private const string PRIVATE_RECORD_HANGUP = "HangUp";
        internal PrivateCallRecorderScriptBuilder() { }
        /*
        public string BuildPrivateRecordAliceCallingDataScript()
        {
            return BuildJs(null, PRIVATE_RECORD_ALICE_CALLING);
        }
         * */
        public string BuildPrivateRecordAliceAnsweredDataScript()
        {
            return BuildJs(null, PRIVATE_RECORD_ALICE_ANSWERED);
        }
        public string BuildPrivateRecordBobCallingDataScript()
        {
            return BuildJs(null, PRIVATE_RECORD_BOB_CALLING);
        }
        public string BuildPrivateRecordTwoSidesConnectedDataScript()
        {
            return BuildJs(null, PRIVATE_RECORD_TWO_SIDES_CONNECTED);
        }
        public string BuildPrivateRecordHangUpDataScript(string message)
        {
            return BuildJs(message, PRIVATE_RECORD_HANGUP);
        }

    }
}
