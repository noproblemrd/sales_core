﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher.DesktopApp.Builders
{
    internal class CustomerPaymentScriptBuilder : ScriptBuilderPusher
    {
        private const string PAYMENT_RESULT_CLIENT_METHOD = "customerPaymentResult";

        public string BuildPaymentResultDataScript(bool success)
        {
            var data = new PaymentResult()
            {
                Success = success
            };
            return BuildJs(data, PAYMENT_RESULT_CLIENT_METHOD);
        }

        internal class PaymentResult
        {
            public bool Success { get; set; }
        }
    }
}
