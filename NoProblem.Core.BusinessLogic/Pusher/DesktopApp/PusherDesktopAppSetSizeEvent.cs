﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher.DesktopApp
{
    internal class PusherDesktopAppSetSizeEvent : PusherDesktopAppEvent
    {
        public PusherDesktopAppSetSizeEvent(string channel, int width, int height)
            : base(channel, eEventType.set_size, new EventData() { height = height, width = width })
        {

        }
    }
}
