﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher.DesktopApp
{
    internal class PusherDesktopAppDisconnectEvent : PusherDesktopAppEvent
    {
        public PusherDesktopAppDisconnectEvent(string channel)
            : base(channel, eEventType.disconnect, new EventData())
        {

        }
    }
}
