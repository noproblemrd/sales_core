﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher.DesktopApp
{
    public struct EventData
    {
        public int? width { get; set; }
        public int? height { get; set; }
        public string script { get; set; }
    }
}
