﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher.DesktopApp
{
    internal class PusherDesktopAppInjectJsEvent : PusherDesktopAppEvent
    {
        public PusherDesktopAppInjectJsEvent(string channel, string jsScript)
            : base(channel, eEventType.inject_js, new EventData() { script = jsScript })
        {

        }
    }
}
