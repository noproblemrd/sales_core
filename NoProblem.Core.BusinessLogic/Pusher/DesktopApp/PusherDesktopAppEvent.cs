﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Pusher.DesktopApp
{
    internal abstract class PusherDesktopAppEvent : PusherEvent
    {
        public PusherDesktopAppEvent(string channel, eEventType eventType, EventData data)
            : base(channel, eventType.ToString(), data)
        {

        }
    }
}
