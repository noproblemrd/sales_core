﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.SdkTypeProxy;
using Microsoft.Xrm.Client;
using DML = NoProblem.Core.DataModel;
using System.Text.RegularExpressions;
using NoProblem.Core.BusinessLogic.Utils;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.Channels
{
    internal abstract class ChannelManager : XrmUserBase
    {
        #region Fields

        protected Guid channelId; 

        #endregion

        #region Ctor
        internal ChannelManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Internal Methods

        internal abstract void SendAndClose(DML.Xrm.new_notificationitem notificationItem, DataAccessLayer.IncidentAccount incidentAccountDal, DataAccessLayer.AccountRepository accountRepositoryDal, DataAccessLayer.Incident incidentDal);

        internal virtual void SendRequestToChannel(NoProblem.Core.DataModel.Xrm.incident incident, List<NoProblem.Core.DataModel.Xrm.new_incidentaccount> incidentAccountList)
        {
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            DataAccessLayer.NotificationItem notificationsDal = new NoProblem.Core.DataAccessLayer.NotificationItem(XrmDataContext);
            incident.statuscode = (int)DML.Xrm.incident.Status.WAITING;
            incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.DELIVERED_REQUEST;
            incidentDal.Update(incident);
            XrmDataContext.SaveChanges();
            SendAndCloseFirstOne(incidentAccountList[0], incidentAccountDal, accountRepositoryDal, incidentDal);
            DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            int delay = expertiseDal.GetNotificationDelay(incident.new_primaryexpertiseid.Value);
            DateTime timeToSend = DateTime.Now;
            for( int i  = 1 ; i < incidentAccountList.Count; i++)
            {
                DML.Xrm.new_incidentaccount item = incidentAccountList[i];
                DML.Xrm.new_notificationitem notification = new NoProblem.Core.DataModel.Xrm.new_notificationitem();
                notification.new_incidentaccountid = item.new_incidentaccountid;
                if (delay == 0)
                {
                    SendAndClose(notification, incidentAccountDal, accountRepositoryDal, incidentDal);
                }
                else
                {
                    timeToSend = timeToSend.AddMinutes(delay);
                    notification.new_timetosend = timeToSend;
                    notification.new_channelid = channelId;
                    notificationsDal.Create(notification);                    
                }
            }    
            XrmDataContext.SaveChanges();
        }        

        #endregion

        #region Protected Methods

        protected string GetConsumerPhone(NoProblem.Core.DataModel.Xrm.incident incident)
        {
            DataAccessLayer.Contact contactDAL = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            DataModel.Xrm.contact consumer = contactDAL.Retrieve(incident.customerid.Value);
            return consumer.mobilephone;
        }

        protected void InstatiateTemplate(string templateName, ICrmEntity entry, Microsoft.Crm.SdkTypeProxy.EntityName entityName, out string subject, out string body)
        {
            DataAccessLayer.Template templateDal = new NoProblem.Core.DataAccessLayer.Template(XrmDataContext);
            using (var service = XrmDataContext.CreateService())
            {
                InstantiateTemplateRequest request = new InstantiateTemplateRequest();
                request.ObjectId = entry.Id.Value;
                request.ObjectType = entityName.ToString();
                DataModel.Xrm.template template = templateDal.GetTemplateByTitle(templateName);
                request.TemplateId = template.templateid;
                var response = service.Execute(request) as InstantiateTemplateResponse;
                email email = response.BusinessEntityCollection.BusinessEntities.FirstOrDefault() as email;
                subject = email.subject;
                body = email.description;
                subject = Regex.Replace(subject, @"<(.|\n)*?>", string.Empty);
                body = Regex.Replace(body, @"<(.|\n)*?>", string.Empty);
                subject = System.Web.HttpUtility.HtmlDecode(subject);
                body = System.Web.HttpUtility.HtmlDecode(body);
            }
        }

        protected void UpdateIncident(DML.Xrm.incident incident, DataAccessLayer.IncidentAccount incidentAccountDal, DataAccessLayer.Incident incidentDal)
        {
            if (IsAllNotificationsDone(incident, incidentAccountDal))//is last notification
            {
                incident.statuscode = (int)DML.Xrm.incident.Status.WORK_DONE;
                incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
                ServiceRequest.ServiceRequestCloser closer = new ServiceRequestCloser(incidentDal.XrmDataContext);
                closer.CloseRequest(incident, incidentDal);
                Notifications notificationsManager = new Notifications(XrmDataContext);
                notificationsManager.NotifyCustomer(DML.enumCustomerNotificationTypes.SUPPLIERS_CONTACTED, incident.customerid.Value, incident.incidentid.ToString(), "incident", incident.incidentid.ToString(), "incident");
            }
        }

        protected void CheckIncidentTimePass(NoProblem.Core.DataModel.Xrm.new_incidentaccount incidentAccount)
        {
            DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string configVal = configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.NEW_INCIDENT_TIME_PASS);
            int passedDays = int.Parse(configVal);
            if (passedDays > 0)
            {
                //int publisherTimeZoneCode = int.Parse(configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.PUBLISHER_TIME_ZONE));
                DateTime localDate = FixDateToLocal(DateTime.Now).Date; //ConvertUtils.GetLocalFromUtc(XrmDataContext, DateTime.Now, publisherTimeZoneCode).Date;
                DateTime midNightLocal = FixDateToUtcFromLocal(localDate); //ConvertUtils.GetUtcFromLocal(XrmDataContext, localDate, publisherTimeZoneCode);
                DateTime dateReady = midNightLocal.AddDays((passedDays - 1) * -1);
                DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                var incident = incidentDal.Retrieve(incidentAccount.new_incidentid.Value);
                bool wasCharged = incidentDal.WasChargedInTimePass(incident.new_primaryexpertiseid.Value, incident.customerid.Value, incidentAccount.new_accountid.Value, dateReady, incident.incidentid);
                if (wasCharged)
                {
                    Refunds.RefundsManager refunder = new NoProblem.Core.BusinessLogic.Refunds.RefundsManager(XrmDataContext);
                    refunder.RefundSupplier(incidentAccount, NoProblem.Core.DataModel.Xrm.new_balancerow.Action.Interval);
                }
            }
        }

        #endregion

        #region Private Methods

        private void SendAndCloseFirstOne(NoProblem.Core.DataModel.Xrm.new_incidentaccount item,
            DataAccessLayer.IncidentAccount incidentAccountDal, DataAccessLayer.AccountRepository accountRepositoryDal
            , DataAccessLayer.Incident incidentDal)
        {
            DML.Xrm.new_notificationitem notificationItem = new NoProblem.Core.DataModel.Xrm.new_notificationitem();
            notificationItem.new_incidentaccountid = item.new_incidentaccountid;
            SendAndClose(notificationItem, incidentAccountDal, accountRepositoryDal, incidentDal);
        }

        private bool IsAllNotificationsDone(DataModel.Xrm.incident incident, DataAccessLayer.IncidentAccount incidentAccountDal)
        {
            IEnumerable<DML.Xrm.new_incidentaccount> incAccs = incidentAccountDal.GetIncidentAccountsByIncidentId(incident.incidentid);
            int openItemsCount = incAccs.Count(x => x.statuscode == (int)DML.Xrm.new_incidentaccount.Status.IN_AUCTION);
            bool retVal = false;
            if (openItemsCount == 0)
            {
                retVal = true;
            }
            return retVal;
        }

        #endregion
    }
}
