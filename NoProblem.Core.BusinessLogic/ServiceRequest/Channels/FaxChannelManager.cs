﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.Channels
{
    internal class FaxChannelManager : ChannelManager
    {
        internal FaxChannelManager(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {

            DataAccessLayer.Channel channelDal = new NoProblem.Core.DataAccessLayer.Channel(XrmDataContext);
            DataModel.Xrm.new_channel channel = channelDal.GetChannelByCode(NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Fax);
            channelId = channel.new_channelid;
        }

        internal override void SendAndClose(NoProblem.Core.DataModel.Xrm.new_notificationitem notificationItem, NoProblem.Core.DataAccessLayer.IncidentAccount incidentAccountDal, NoProblem.Core.DataAccessLayer.AccountRepository accountRepositoryDal, NoProblem.Core.DataAccessLayer.Incident incidentDal)
        {
            throw new NotImplementedException();
        }
    }
}
