﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.Channels
{
    internal class CallChannelManager : ChannelManager
    {
        #region Ctor
        internal CallChannelManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal override void SendAndClose(NoProblem.Core.DataModel.Xrm.new_notificationitem notificationItem, NoProblem.Core.DataAccessLayer.IncidentAccount incidentAccountDal, NoProblem.Core.DataAccessLayer.AccountRepository accountRepositoryDal, NoProblem.Core.DataAccessLayer.Incident incidentDal)
        {
        }

        internal override void SendRequestToChannel(NoProblem.Core.DataModel.Xrm.incident incident, List<NoProblem.Core.DataModel.Xrm.new_incidentaccount> incidentAccountList)
        {
            string dialerResult = string.Empty;
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                DataAccessLayer.PrimaryExpertise primaryDAL = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
                DML.Xrm.new_primaryexpertise primary = primaryDAL.Retrieve(incident.new_primaryexpertiseid.Value);
                bool isAuction = false;
                if (primary.new_isauction.HasValue
                    && primary.new_isauction.Value
                    && incident.casetypecode != (int)DML.Xrm.incident.CaseTypeCode.SupplierSpecified
                    && string.IsNullOrEmpty(incident.description) == false)
                {
                    isAuction = true;
                }
                bool rejectEnabled = primary.new_rejectenabled.HasValue ? primary.new_rejectenabled.Value : false;
                List<inc2.NPProvider> providersList = CreateProvidersList(incidentAccountList, incident.new_requiredaccountno.Value, isAuction, primary);
                string startTime = DateTime.Now.AddMinutes(-10).ToString("dd/MM/yyyy HH:mm");
                string endTime = DateTime.Now.AddHours(2).ToString("dd/MM/yyyy HH:mm");
                string consumerPhone = GetConsumerPhone(incident);
                DataAccessLayer.ConfigurationSettings configurationDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string phonePrefix = configurationDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.COUNTRY_PHONE_PREFIX);
                string internalPhonePrefix = configurationDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.INTERNAL_PHONE_PREFIX);
                consumerPhone = Phones.Deprecated_AppendPhonePrefix(phonePrefix, internalPhonePrefix, consumerPhone);
                int numOfRetries = int.Parse(configurationDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.SUPPLIER_ATTEMPTED_TRIES));
                int retryDelay = int.Parse(configurationDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.ATTEMPTED_TRIES_INTERVAL));
                bool publisherRecordsCalls = configurationDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.RECORD_CALLS) == "1";
                string callBackUrl = ConfigurationManager.AppSettings["DialerCallbackUrl"];
                string requestId = incident.incidentid.ToString();
                inc2.NPService inc2Service = new NoProblem.Core.BusinessLogic.inc2.NPService();
                inc2Service.Url = ConfigurationManager.AppSettings["inc2Url"];
                int supplierAmountToCall = incident.new_requiredaccountno.Value <= providersList.Count ? incident.new_requiredaccountno.Value : providersList.Count;
                bool ttsForJona = (configurationDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.TTS_FOR_JONA) == "1");
                if (ttsForJona && string.IsNullOrEmpty(incident.description))
                {
                    ttsForJona = false;
                }
                string suppliersTextForLog = CreateSuppliersTextForLog(providersList);
                LogUtils.MyHandle.WriteToLog(8, "Sending request to dialer with params: requestId = {0}, consumerPhone = {1}, numofsuppliers = {2}, startTime = {3}, endTime = {4}, numOfRetries = {5}, retryDelay = {6}, callBackUrl = {7}, description = {8}.\n\r{9}"
                    , requestId, consumerPhone, supplierAmountToCall, startTime, endTime, numOfRetries, retryDelay, callBackUrl, incident.description, suppliersTextForLog);
                bool asteriskDirect = configurationDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.ASTERISK_DIRECT) == "1";
                //bool foneApiOn = configurationDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.FONE_API_ON) == "1";
                if (isAuction)
                {
                    if (asteriskDirect)
                    {
                        JonaManager jona = new JonaManager(XrmDataContext);
                        dialerResult = jona.InitiateAuction(incident.incidentid, consumerPhone, providersList, supplierAmountToCall, numOfRetries, retryDelay, incident.description, ttsForJona, rejectEnabled); 
                    }
                    else
                    {
                        dialerResult = inc2Service.initAuction(requestId, "", consumerPhone, providersList.ToArray(),
                            supplierAmountToCall, startTime, endTime, numOfRetries, retryDelay, callBackUrl, incident.description, ttsForJona);
                        //incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.AUCTION_STARTED;
                    }
                }
                else
                {
                    //if (foneApiOn)
                    //{

                    //}
                    //else 
                    if (asteriskDirect)
                    {
                        JonaManager jona = new JonaManager(XrmDataContext);
                        dialerResult = jona.InitiateServiceRequest(incident.incidentid, consumerPhone, providersList, numOfRetries, retryDelay, incident.description, ttsForJona, rejectEnabled); 
                    }
                    else
                    {
                        dialerResult = inc2Service.initServiceRequest(requestId, "", consumerPhone, providersList.ToArray(),
                             supplierAmountToCall, startTime, endTime, numOfRetries, retryDelay, callBackUrl, incident.description, ttsForJona);
                        incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.DELIVERED_REQUEST;
                    }
                }
                incident.statuscode = (int)DML.Xrm.incident.Status.WAITING;
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception sending request to dialer. incidentId = {0}", incident.incidentid.ToString());
                incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.ERROR;
                incident.statuscode = (int)DML.Xrm.incident.Status.WORK_DONE;
                DialerErrorHandler(incident);
            }
            try
            {
                string[] dialerResultSplit = dialerResult.Split(',');
                if (dialerResultSplit[0].Trim() == "1")
                {
                    LogUtils.MyHandle.WriteToLog("init service request to dialer return with error. incidentId = {0}", incident.incidentid.ToString());
                    incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.ERROR;
                    incident.statuscode = (int)DML.Xrm.incident.Status.WORK_DONE;
                    DialerErrorHandler(incident);
                }
                ServiceRequest.ServiceRequestCloser closer = new ServiceRequestCloser(XrmDataContext);
                closer.CloseRequest(incident);                
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception while parsing and handling dialers response.");
            }
        }

        #endregion

        #region Private Method

        private void DialerErrorHandler(DML.Xrm.incident incident)
        {
            //put everyone to lost.
            DataAccessLayer.IncidentAccount inciAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            var incAccs = inciAccDal.GetIncidentAccountsByIncidentId(incident.incidentid);
            foreach (var item in incAccs)
            {
                item.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
                inciAccDal.Update(item);
            }
        }

        private List<NoProblem.Core.BusinessLogic.inc2.NPProvider> CreateProvidersList(List<DML.Xrm.new_incidentaccount> incidentAccountList, int wantedSuppliersAmount, bool isAuction, DML.Xrm.new_primaryexpertise primary)
        {
            List<NoProblem.Core.BusinessLogic.inc2.NPProvider> providersList = new List<NoProblem.Core.BusinessLogic.inc2.NPProvider>();
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.IncidentAccount incidentAccountDAL = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataAccessLayer.ConfigurationSettings configurationDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            DataAccessLayer.AccountExpertise accountExpertiseDAL = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            string countryPrefix = configurationDAL.GetConfigurationSettingValue(ConfigurationKeys.COUNTRY_PHONE_PREFIX);
            string internalPrefix = configurationDAL.GetConfigurationSettingValue(ConfigurationKeys.INTERNAL_PHONE_PREFIX);
            string recordCalls = configurationDAL.GetConfigurationSettingValue(ConfigurationKeys.RECORD_CALLS);
            for (int i = 0; i < incidentAccountList.Count; i++)
            {
                DML.Xrm.new_incidentaccount inciAcc = incidentAccountList[i];
                inc2.NPProvider npProvider = CreateNPProvider(accountRepositoryDal, countryPrefix, internalPrefix, inciAcc, isAuction, incidentAccountDAL, accountExpertiseDAL, primary, recordCalls);
                providersList.Add(npProvider);
            }
            return providersList;
        }

        private inc2.NPProvider CreateNPProvider(DataAccessLayer.AccountRepository accountRepositoryDal, string countryPhonePrefix, string internalPhonePrefix, DML.Xrm.new_incidentaccount inciAcc, bool isAuction, DataAccessLayer.IncidentAccount incidentAccountDAL, DataAccessLayer.AccountExpertise accountExpertiseDAL, DML.Xrm.new_primaryexpertise primary, string recordCalls)
        {
            inc2.NPProvider npProvider = new NoProblem.Core.BusinessLogic.inc2.NPProvider();
            DML.Xrm.account account = accountRepositoryDal.Retrieve(inciAcc.new_accountid.Value);
            npProvider.defaultBidValue = (float)inciAcc.new_potentialincidentprice.Value;
            npProvider.defaultBidValueSpecified = true;
            npProvider.minBidValue = (float)inciAcc.new_potentialincidentprice.Value;
            npProvider.minBidValueSpecified = true;
            npProvider.maxBidValue = (float)account.new_cashbalance.Value;//never change this to available cash balance. think about it.
            npProvider.maxBidValueSpecified = true;
            npProvider.providerUniqueId = account.accountid.ToString();
            DML.Xrm.new_accountexpertise accExpertise = accountExpertiseDAL.GetByAccountAndPrimaryExpertise(account.accountid, primary.new_primaryexpertiseid);
            npProvider.realTimeBid = accExpertise.new_incidentpricechangeable.HasValue ? accExpertise.new_incidentpricechangeable.Value : false;
            npProvider.realTimeBidSpecified = true;
            npProvider.recording = (recordCalls == "1" ? (account.new_recordcalls.HasValue ? account.new_recordcalls.Value : false) : false);
            npProvider.recordingSpecified = true;
            if (isAuction)
            {
                DateTime lastWinDate = incidentAccountDAL.GetLastWinDate(account.accountid);
                npProvider.lastWinDate = lastWinDate.ToString("dd/MM/yyyy HH:mm");
                npProvider.qualityRank = 1;
                npProvider.qualityRankSpecified = true;
            }
            List<string> phonesList = new List<string>();
            string phone1 = Phones.Deprecated_AppendPhonePrefix(countryPhonePrefix, internalPhonePrefix, account.telephone1);
            phonesList.Add(phone1);
            if (string.IsNullOrEmpty(account.telephone2) == false)
            {
                string phone2 = Phones.Deprecated_AppendPhonePrefix(countryPhonePrefix, internalPhonePrefix, account.telephone2);
                phonesList.Add(phone2);
            }
            npProvider.phoneNumbers = phonesList.ToArray();
            return npProvider;
        }

        private string CreateSuppliersTextForLog(List<NoProblem.Core.BusinessLogic.inc2.NPProvider> providersList)
        {
            StringBuilder builder = new StringBuilder("Providers:\n\r");
            foreach (inc2.NPProvider provider in providersList)
            {
                builder.Append("Provider: id = ");
                builder.Append(provider.providerUniqueId.ToString());
                builder.Append(", defaultBidValue = ");
                builder.Append(provider.defaultBidValue.ToString());
                builder.Append(", maxBidValue = ");
                builder.Append(provider.maxBidValue.ToString());
                builder.Append(", minBidValue = ");
                builder.Append(provider.minBidValue.ToString());
                builder.Append(", realTimeBid = ");
                builder.Append(provider.realTimeBid.ToString());
                builder.Append(", recording = ");
                builder.Append(provider.recording.ToString());
                builder.Append(", lastWindDate = ");
                builder.Append(provider.lastWinDate);
                builder.Append(", qualityRank = ");
                builder.Append(provider.qualityRank.ToString());
                builder.Append(". Phones:\n\r");
                foreach (string phone in provider.phoneNumbers)
                {
                    builder.Append("Phone = ");
                    builder.Append(phone);
                    builder.Append(". ");
                }
                builder.Append("\n\r");
            }
            return builder.ToString();
        }

        #endregion
    }
}
