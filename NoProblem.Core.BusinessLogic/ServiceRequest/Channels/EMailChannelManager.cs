﻿using System;
using Effect.Crm.Logs;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.Channels
{
    internal class EMailChannelManager : ChannelManager
    {
        #region Ctor
        internal EMailChannelManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataAccessLayer.Channel channelDal = new NoProblem.Core.DataAccessLayer.Channel(XrmDataContext);
            DML.Xrm.new_channel channel = channelDal.GetChannelByCode(NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Email);
            channelId = channel.new_channelid;
        } 
        #endregion

        #region Internal Methods

        internal override void SendAndClose(DML.Xrm.new_notificationitem notificationItem, DataAccessLayer.IncidentAccount incidentAccountDal, DataAccessLayer.AccountRepository accountRepositoryDal, DataAccessLayer.Incident incidentDal)
        {
            DML.Xrm.new_incidentaccount item = incidentAccountDal.Retrieve(notificationItem.new_incidentaccountid.Value);
            DML.Xrm.incident incident = incidentDal.Retrieve(item.new_incidentid.Value);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(item.new_accountid.Value);
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            try
            {
                string subject = string.Empty;
                string body = string.Empty;
                InstatiateTemplate(DML.TemplateNames.EMAIL_LEAD, incident, Microsoft.Crm.SdkTypeProxy.EntityName.incident, out subject, out body);
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("localhost");
                string emailSender = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.EMAIL_SENDER);
                client.Send(emailSender, supplier.emailaddress1, subject, body);
                item.statuscode = (int)DML.Xrm.new_incidentaccount.Status.CLOSE;
                XrmDataContext.SaveChanges();
                item.new_tocharge = true;
                CheckIncidentTimePass(item);
                DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
                var upsale = upsaleDal.GetUpsaleByIncidentId(incident.incidentid);
                if (upsale != null)
                {
                    upsale.new_numofconnectedadvertisers =
                        (upsale.new_numofconnectedadvertisers.HasValue ? upsale.new_numofconnectedadvertisers.Value + 1 : 1);
                    upsaleDal.Update(upsale);
                }
                incident.new_ordered_providers += 1;
                incidentDal.Update(incident);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception sending email lead to supplier. Supplier passed to lost.");
                item.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
                item.new_tocharge = false;
            }
            incidentAccountDal.Update(item);
            XrmDataContext.SaveChanges();
            UpdateIncident(incident, incidentAccountDal, incidentDal);
            DailyBudgetManager budgetManager = new DailyBudgetManager(XrmDataContext);
            budgetManager.CheckDailyBudgetIsReached(supplier.accountid, false);
        } 

        #endregion
    }
}
