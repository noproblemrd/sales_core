﻿using System;
using Effect.Crm.Logs;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.Channels
{
    internal class SmsChannelManager : ChannelManager
    {
        #region Ctor
        internal SmsChannelManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataAccessLayer.Channel channelDal = new NoProblem.Core.DataAccessLayer.Channel(XrmDataContext);
            DML.Xrm.new_channel channel = channelDal.GetChannelByCode(NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.SMS);
            channelId = channel.new_channelid;
        }   
        #endregion      

        #region Internal Methods

        internal override void SendAndClose(DML.Xrm.new_notificationitem notificationItem, DataAccessLayer.IncidentAccount incidentAccountDal, DataAccessLayer.AccountRepository accountRepositoryDal, DataAccessLayer.Incident incidentDal)
        {
            Notifications notifier = new Notifications(XrmDataContext);
            DML.Xrm.new_incidentaccount item = incidentAccountDal.Retrieve(notificationItem.new_incidentaccountid.Value);
            DML.Xrm.incident incident = incidentDal.Retrieve(item.new_incidentid.Value);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(item.new_accountid.Value);
            try
            {
                
                bool sent = notifier.SendSMSTemplate(DML.TemplateNames.SMS_LEAD, supplier.telephone1, supplier.new_country, incident.incidentid,
                     "incident", incident.incidentid, "incident");
                if (sent)
                {
                    item.statuscode = (int)DML.Xrm.new_incidentaccount.Status.CLOSE;
                    item.new_tocharge = true;
                    XrmDataContext.SaveChanges();
                    CheckIncidentTimePass(item);
                    DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
                    var upsale = upsaleDal.GetUpsaleByIncidentId(incident.incidentid);
                    if (upsale != null)
                    {
                        upsale.new_numofconnectedadvertisers =
                            (upsale.new_numofconnectedadvertisers.HasValue ? upsale.new_numofconnectedadvertisers.Value + 1 : 1);
                        upsaleDal.Update(upsale);
                    }
                    incident.new_ordered_providers += 1;
                    incidentDal.Update(incident);
                }
                else
                {
                    item.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
                    item.new_tocharge = false;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception sending sms lead to supplier. passing supplier to lost status.");
                item.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
                item.new_tocharge = false;
            }
            incidentAccountDal.Update(item);
            XrmDataContext.SaveChanges();
            UpdateIncident(incident, incidentAccountDal, incidentDal);
            DailyBudgetManager budgetManager = new DailyBudgetManager(XrmDataContext);
            budgetManager.CheckDailyBudgetIsReached(supplier.accountid, false);
        } 

        #endregion
    }
}
