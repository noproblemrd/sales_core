﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.Channels
{
    internal class ChannelManagerFactory
    {
        internal static ChannelManager GetChannelManager(DataModel.Xrm.new_channel.ChannelCode channel, DataModel.Xrm.DataContext xrmDataContext)
        {
            ChannelManager manager = null;
            switch (channel)
            {
                case DataModel.Xrm.new_channel.ChannelCode.Call:
                    manager = new CallChannelManager(xrmDataContext);
                    break;
                case DataModel.Xrm.new_channel.ChannelCode.Email:
                    manager = new EMailChannelManager(xrmDataContext);
                    break;
                case DataModel.Xrm.new_channel.ChannelCode.Fax:
                    manager = new FaxChannelManager(xrmDataContext);
                    break;
                case DataModel.Xrm.new_channel.ChannelCode.SMS:
                    manager = new SmsChannelManager(xrmDataContext);
                    break;
            }
            return manager;
        }
    }
}
