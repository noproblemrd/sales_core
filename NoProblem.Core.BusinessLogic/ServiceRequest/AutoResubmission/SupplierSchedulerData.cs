﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.AutoResubmission
{
    internal class SupplierSchedulerData
    {
        public SupplierSchedulerData()
        {
            UnavailableTo = null;
            UnavailableFrom = null;
        }

        public Guid SupplierId { get; set; }
        public int? From1 { get; set; }
        public int? To1 { get; set; }
        public int? From2 { get; set; }
        public int? To2 { get; set; }
        public int? From3 { get; set; }
        public int? To3 { get; set; }
        public int? From4 { get; set; }
        public int? To4 { get; set; }
        public DateTime? UnavailableTo { get; set; }
        public DateTime? UnavailableFrom { get; set; }
    }
}
