﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.AutoResubmission
{
    public class ResubmissionScheduler : Runnable
    {
        Guid incidentId;
        static DataAccessLayer.Generic genericDal = new DataAccessLayer.Generic();

        public ResubmissionScheduler(Guid incidentId)
        {
            this.incidentId = incidentId;
        }
        public void TestPolite()
        {
            DataAccessLayer.Incident dal = new DataAccessLayer.Incident(XrmDataContext);
            var incident = dal.Retrieve(incidentId);
            int timeZoneCode = GetConsumerTimeZone(incident);
            List<HourRangeData> politeHours = GetPoliteHoursInUtc(timeZoneCode);
            int t = 0;
        }
        protected override void Run()
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
            if (config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUTO_RESUBMISSION_ENABLED) != 1.ToString())
            {
                return;
            }

            DataAccessLayer.Incident dal = new DataAccessLayer.Incident(XrmDataContext);
            var incident = dal.Retrieve(incidentId);
            bool continueWithResubmission = ResubmitterValidator.ValidateResubmission(incident);
            if (continueWithResubmission)
            {
                if (incident.new_isdirectnumber.HasValue && incident.new_isdirectnumber.Value)
                {
                    ScheduleDirectNumber(incident);
                }
                else
                {
                    ScheduleSelfService(incident);
                }
            }
        }

        private void ScheduleSelfService(DataModel.Xrm.incident incident)
        {
            Guid headingId = incident.new_primaryexpertiseid.Value;
            Guid regionId = incident.new_regionid.Value;
            Guid upsaleId = incident.new_upsaleid.Value;
            List<SupplierSchedulerData> suppliers = GetSuppliers(headingId, regionId, upsaleId);
            if (suppliers.Count == 0)
                return;
            int timeZoneCode = GetConsumerTimeZone(incident);
            List<HourRangeData> politeHours = GetPoliteHoursInUtc(timeZoneCode);
            DateTime? selectedDate = null;
            DateTime now = DateTime.Now;
            foreach (var sup in suppliers)
            {
                List<HourRangeData> supplierHours = CreateSupplierHoursRanges(now, sup);
                HourRangeData unavailable = null;
                if (sup.UnavailableFrom.HasValue)
                {
                    unavailable = new HourRangeData()
                    {
                        From = sup.UnavailableFrom.Value,
                        To = sup.UnavailableTo.Value
                    };
                }
                DateTime? lastScheduledResubmission = GetLastScheduledResubmission(sup.SupplierId);
                DateTime? nearestDate = FindNearesOverlappingDateTime(politeHours, supplierHours, unavailable, lastScheduledResubmission);
                if (nearestDate.HasValue)
                {
                    selectedDate = nearestDate;
                    SaveSchedule(sup.SupplierId, selectedDate.Value, incident.incidentid);
                    break;
                }
            }
        }

        private void SaveSchedule(Guid supplierId, DateTime date, Guid incidentId)
        {
            DataAccessLayer.Resubmission resubmissionDal = new DataAccessLayer.Resubmission(XrmDataContext);
            DataModel.Xrm.new_resubmission resubmission = new DataModel.Xrm.new_resubmission();
            resubmission.new_scheduleddate = date;
            resubmission.new_accountid = supplierId;
            resubmission.new_incidentoriginalid = incidentId;
            resubmissionDal.Create(resubmission);
            XrmDataContext.SaveChanges();
        }

        private DateTime? GetLastScheduledResubmission(Guid supplierId)
        {
            DataAccessLayer.Resubmission resubmissionDal = new DataAccessLayer.Resubmission(XrmDataContext);
            DateTime? last = resubmissionDal.GetLastScheduledResubmissionForSupplier(supplierId);
            if (last.HasValue)
            {
                return last.Value.AddMinutes(5);
            }
            else
            {
                return null;
            }
        }

        private DateTime? FindNearesOverlappingDateTime(List<HourRangeData> c1, List<HourRangeData> c2, HourRangeData unavailable, DateTime? lastScheduledResubmission)
        {
            DateTime? retVal = null;
            foreach (var i1 in c1)
            {
                foreach (var i2 in c2)
                {
                    if (i1.From < i2.To && i2.From < i1.To)
                    {
                        DateTime tmp;
                        if (i2.From > i1.From)
                        {
                            tmp = i2.From;
                        }
                        else
                        {
                            tmp = i1.From;
                        }
                        bool gotOutOfRange = false;
                        if (unavailable != null)
                        {
                            while (!gotOutOfRange && tmp >= unavailable.From && tmp < unavailable.To)
                            {
                                tmp = tmp.AddHours(1);
                                if (tmp >= i1.To || tmp >= i2.To)
                                {
                                    gotOutOfRange = true;
                                }
                            }
                        }
                        if (lastScheduledResubmission.HasValue)
                        {
                            while (!gotOutOfRange && lastScheduledResubmission.Value >= tmp)
                            {
                                tmp = tmp.AddMinutes(5);
                                if (tmp >= i1.To || tmp >= i2.To)
                                {
                                    gotOutOfRange = true;
                                }
                            }
                        }
                        if (!gotOutOfRange && (!retVal.HasValue || tmp < retVal.Value))
                        {
                            retVal = tmp;
                        }
                    }
                }
            }
            return retVal;
        }

        private List<HourRangeData> CreateSupplierHoursRanges(DateTime now, SupplierSchedulerData sup)
        {
            List<HourRangeData> supplierHours = new List<HourRangeData>();

            if (sup.From1.HasValue && sup.To1.HasValue)
            {
                DateTime supFrom1;
                DateTime supTo1;
                supFrom1 = now.Date.AddHours(sup.From1.Value);
                if (sup.To1.Value == 24)
                {
                    supTo1 = now.Date.AddDays(1);
                }
                else
                {
                    supTo1 = now.Date.AddHours(sup.To1.Value);
                }
                if (supFrom1 < now.AddMinutes(20))
                {
                    supFrom1 = now.AddMinutes(20);                    
                }
                if (supTo1 > supFrom1)
                {
                    supplierHours.Add(
                        new HourRangeData()
                        {
                            From = supFrom1,
                            To = supTo1
                        });
                }
            }

            if (sup.From2.HasValue && sup.To2.HasValue)
            {
                DateTime supFrom2;
                DateTime supTo2;
                supFrom2 = now.Date.AddHours(sup.From2.Value);
                if (sup.To2.Value == 24)
                {
                    supTo2 = now.Date.AddDays(1);
                }
                else
                {
                    supTo2 = now.Date.AddHours(sup.To2.Value);
                }
                if (supFrom2 < now.AddMinutes(20))
                {
                    supFrom2 = now.AddMinutes(20);
                }
                if (supTo2 > supFrom2)
                {
                    supplierHours.Add(
                        new HourRangeData()
                        {
                            From = supFrom2,
                            To = supTo2
                        });
                }
            }

            if (sup.From3.HasValue && sup.To3.HasValue)
            {
                DateTime supFrom3;
                DateTime supTo3;
                supFrom3 = now.Date.AddDays(1).AddHours(sup.From3.Value);
                if (sup.To3.Value == 24)
                {
                    supTo3 = now.Date.AddDays(2);
                }
                else
                {
                    supTo3 = now.Date.AddDays(1).AddHours(sup.To3.Value);
                }
                supplierHours.Add(
                    new HourRangeData()
                    {
                        From = supFrom3,
                        To = supTo3
                    });
            }

            if (sup.From4.HasValue && sup.To4.HasValue)
            {
                DateTime supFrom4;
                DateTime supTo4;
                supFrom4 = now.Date.AddDays(1).AddHours(sup.From4.Value);
                if (sup.To4.Value == 24)
                {
                    supTo4 = now.Date.AddDays(2);
                }
                else
                {
                    supTo4 = now.Date.AddDays(1).AddHours(sup.To4.Value);
                }
                supplierHours.Add(
                    new HourRangeData()
                    {
                        From = supFrom4,
                        To = supTo4
                    });
            }
            return supplierHours;
        }

        private List<HourRangeData> GetPoliteHoursInUtc(int timeZoneCode)
        {
            DataAccessLayer.Availability avDal = new DataAccessLayer.Availability(XrmDataContext);
            var messagingTimes = avDal.GetMessagingTimes();
            DateTime nowLocal = FixDateToLocal(DateTime.Now, timeZoneCode);
            var todayPolite = messagingTimes.FirstOrDefault(x => (int)x.DayOfWeek == (((int)nowLocal.DayOfWeek) + 1));
            var tommorrowPolite = messagingTimes.FirstOrDefault(x => (int)x.DayOfWeek == (((int)nowLocal.AddDays(1).DayOfWeek) + 1));
            List<HourRangeData> retVal = new List<HourRangeData>();
            if (todayPolite != null)
            {
                HourRangeData todayData = CreateHourRangeData(timeZoneCode, nowLocal, todayPolite, 0);
                if (todayData != null)
                    retVal.Add(todayData);
            }
            if (tommorrowPolite != null)
            {
                HourRangeData tommorrowData = CreateHourRangeData(timeZoneCode, nowLocal, tommorrowPolite, 1);
                if (tommorrowData != null)
                    retVal.Add(tommorrowData);
            }
            return retVal;
        }

        private HourRangeData CreateHourRangeData(int timeZoneCode, DateTime nowLocal, DataModel.BusinessClosures.MessagingTimeData politeHours, int daysToAdd)
        {
            DateTime localFrom = nowLocal.Date.AddHours(Utils.ConvertUtils.TranslateHourStringToInt(politeHours.FromHour));
            DateTime localTo;
            int toHour = Utils.ConvertUtils.TranslateHourStringToInt(politeHours.ToHour);
            if (toHour == 24)
            {
                localTo = nowLocal.Date.AddDays(1);
            }
            else
            {
                localTo = nowLocal.Date.AddHours(toHour);
            }
            if (daysToAdd > 0)
            {
                localFrom = localFrom.AddDays(daysToAdd);
                localTo = localTo.AddDays(daysToAdd);
            }
            HourRangeData data = new HourRangeData();
            data.From = FixDateToUtcFromLocal(localFrom, timeZoneCode);
            data.To = FixDateToUtcFromLocal(localTo, timeZoneCode);
            if (data.From < DateTime.Now.AddMinutes(20))
            {
                data.From = DateTime.Now.AddMinutes(20);
                if (data.To <= data.From)
                {
                    return null;
                }
            }
            return data;
        }

        private int GetConsumerTimeZone(DataModel.Xrm.incident incident)
        {
            var region = incident.new_new_region_incident;
            int timeZoneCode;
            if (region.new_level == 1)
            {
                timeZoneCode = region.new_timezonecode.Value;
            }
            else
            {
                DataAccessLayer.Region regDal = new DataAccessLayer.Region(XrmDataContext);
                var parents = regDal.GetParents(region);
                var state = parents.First(x => x.new_level == 1);
                timeZoneCode = state.new_timezonecode.Value;
            }
            return timeZoneCode;
        }

        private List<SupplierSchedulerData> GetSuppliers(Guid headingId, Guid regionId, Guid upsaleId)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@headingId", headingId);
            parameters.Add("@regionId", regionId);
            parameters.Add("@upsaleId", upsaleId);
            var reader = genericDal.ExecuteReader("GetSuppliersForResubmission", parameters, System.Data.CommandType.StoredProcedure);
            List<SupplierSchedulerData> retVal = new List<SupplierSchedulerData>();
            foreach (var row in reader)
            {
                SupplierSchedulerData supplier = new SupplierSchedulerData();
                supplier.SupplierId = (Guid)row["supplierId"];
                supplier.From1 = HourToNumber(row["from1"]);
                supplier.From2 = HourToNumber(row["from2"]);
                supplier.From3 = HourToNumber(row["from3"]);
                supplier.From4 = HourToNumber(row["from4"]);
                supplier.To1 = HourToNumber(row["to1"]);
                supplier.To2 = HourToNumber(row["to2"]);
                supplier.To3 = HourToNumber(row["to3"]);
                supplier.To4 = HourToNumber(row["to4"]);
                if (row["unavailableTo"] != DBNull.Value)
                {
                    supplier.UnavailableTo = (DateTime)row["unavailableTo"];
                }
                if (row["unavailableFrom"] != DBNull.Value)
                {
                    supplier.UnavailableFrom = (DateTime)row["unavailableFrom"];
                }
                if (supplier.UnavailableFrom.HasValue && !supplier.UnavailableTo.HasValue)
                {
                    supplier.UnavailableTo = supplier.UnavailableFrom.Value.AddYears(10);
                }
                retVal.Add(supplier);
            }
            return retVal;
        }

        private int? HourToNumber(object hour)
        {
            int? retVal = null;
            if (hour != DBNull.Value)
            {
                string hourStr = (string)hour;
                if (hourStr.StartsWith("23:59"))
                {
                    retVal = 24;
                }
                else
                {
                    retVal = int.Parse(hourStr.Split(':')[0]);
                }
            }
            return retVal;
        }

        private void ScheduleDirectNumber(DataModel.Xrm.incident incident)
        {

        }
    }
}
