﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.AutoResubmission
{
    internal class ResubmittedRequestsSender : Runnable
    {
        Guid incidentId;
        Guid resubmissionId;

        public ResubmittedRequestsSender(Guid resubmissionId, Guid incidentId)
        {
            this.incidentId = incidentId;
            this.resubmissionId = resubmissionId;
        }

        protected override void Run()
        {
            Guid createdIncidentId = Guid.Empty;
            try
            {
                DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
                var incident = incidentDal.Retrieve(incidentId);
                if (!ResubmitterValidator.ValidateResubmission(incident))
                {
                    MarkResubmissionAsDone();
                    return;
                }
                DataModel.ServiceRequest request = CreateRequestObject(incident);
                ServiceRequestManager srManager = new ServiceRequestManager(XrmDataContext);
                var response = srManager.CreateService(request);
                if (!String.IsNullOrEmpty(response.ServiceRequestId))
                {
                    createdIncidentId = new Guid(response.ServiceRequestId);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ResubmittedRequestsSender.Run");
            }
            finally
            {
                MarkResubmissionAsDone(createdIncidentId);
            }
        }

        private DataModel.ServiceRequest CreateRequestObject(DataModel.Xrm.incident incident)
        {
            var consumerPhone = incident.new_telephone1;
            var expertiseCode = incident.new_new_primaryexpertise_incident.new_code;
            var expertiseLevel = 1;
            var numberOfSuppliers = incident.new_requiredaccountno.Value - incident.new_ordered_providers.Value;
            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            bool? useSameInUpsale = incident.new_new_origin_incident.new_usesameinupsale.HasValue && incident.new_new_origin_incident.new_usesameinupsale.Value;
            Guid originId;
            if (useSameInUpsale.HasValue && useSameInUpsale.Value)
            {
                originId = incident.new_originid.Value;
            }
            else
            {
                originId = originDal.GetAutoUpsaleOriginId();
            }
            var regionCode = incident.new_new_region_incident.new_code;
            var regionLevel = incident.new_new_region_incident.new_level.Value;
            var description = incident.description;
            DataModel.ServiceRequest request = new NoProblem.Core.DataModel.ServiceRequest();
            request.ContactPhoneNumber = incident.new_telephone1;
            request.ExpertiseCode = expertiseCode;
            request.ExpertiseType = expertiseLevel;
            request.NumOfSuppliers = numberOfSuppliers;
            request.OriginId = originId;
            request.RegionCode = regionCode;
            request.RegionLevel = regionLevel;
            request.RequestDescription = description;
            request.UpsaleId = incident.new_upsaleid.Value;
            DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            request.UserId = accountRepositoryDal.GetAutoUpsaleUserId();
            request.IsAutoUpsale = true;
            request.SubType = incident.new_subtype;
            request.UpsaleRequestId = incident.incidentid;
            request.Country = incident.new_country;
            if (incident.new_budget.HasValue)
            {
                request.Budget = (DataModel.Consumer.eBudget)incident.new_budget.Value;
            }
            request.Type = incident.new_type;
            request.AppType = incident.new_apptype;
            request.DesktopAppCameFrom = incident.new_desktopappcamefrom;
            AddSpecialHeadingsData(incident, request);
            return request;
        }

        private void MarkResubmissionAsDone()
        {
            MarkResubmissionAsDone(Guid.Empty);
        }

        private void MarkResubmissionAsDone(Guid createdIncidentId)
        {
            DataAccessLayer.Resubmission resubmissionDal = new DataAccessLayer.Resubmission(XrmDataContext);
            DataModel.Xrm.new_resubmission resubmission = new DataModel.Xrm.new_resubmission(XrmDataContext);
            resubmission.new_resubmissionid = resubmissionId;
            resubmission.new_done = true;
            if (createdIncidentId != Guid.Empty)
            {
                resubmission.new_incidentcreatedid = createdIncidentId;
            }
            resubmissionDal.Update(resubmission);
            XrmDataContext.SaveChanges();
        }

        private void AddSpecialHeadingsData(NoProblem.Core.DataModel.Xrm.incident incident, DataModel.ServiceRequest request)
        {
            AddMoverData(incident, request);
            AddPersonalInjuryAttorneyData(incident, request);
            AddCriminalLawAttorneyData(incident, request);
            AddDuiAttorneyData(incident, request);
            AddDivorceAttorneyData(incident, request);
            AddBankruptcyAttorneyData(incident, request);
            AddPayDayLoanData(incident, request);
            AddSocialSecurityDisabilityData(incident, request);
            AddHomeSecurityData(incident, request);
            AddStorageData(incident, request);
            AddHealthInsurance(incident, request);
        }

        private void AddHealthInsurance(DML.Xrm.incident incident, DML.ServiceRequest request)
        {
            if (incident.new_healthinsurancecasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestHealthInsuranceData requestData = new DML.Consumer.ServiceRequestHealthInsuranceData();
                var oldData = incident.new_healthinsurancecasedata_incident;
                if (!String.IsNullOrEmpty(oldData.new_existingcondition))
                {
                    var condition = (DataModel.Consumer.ServiceRequestHealthInsuranceData.eExistingCondition)Enum.Parse(typeof(DataModel.Consumer.ServiceRequestHealthInsuranceData.eExistingCondition), oldData.new_existingcondition, true);
                    requestData.ExistingCondition = condition;
                }
                else
                {
                    requestData.ExistingCondition = DataModel.Consumer.ServiceRequestHealthInsuranceData.eExistingCondition.None;
                }
                requestData.ExpectantParent = oldData.new_expectantparent;
                requestData.Household = oldData.new_household;
                requestData.Income = oldData.new_income;
                requestData.IsInsured = oldData.new_isinsured;
                requestData.IsSmoker = oldData.new_issmoker;
                requestData.PreviouslyDenied = oldData.new_previouslydenied;
                request.HealthInsuranceData = requestData;
            }
        }

        private void AddStorageData(DML.Xrm.incident incident, DML.ServiceRequest request)
        {
            if (incident.new_storagecasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestStorageData requestData = new DML.Consumer.ServiceRequestStorageData();
                var oldData = incident.new_storagecasedata_incident;
                if (oldData.new_deliverystorage.HasValue)
                    requestData.DeliveryStorage = (DML.Consumer.ServiceRequestStorageData.eDeliveryStorage)oldData.new_deliverystorage.Value;
                if (oldData.new_financestorage.HasValue)
                    requestData.FinanceStorage = (DML.Consumer.ServiceRequestStorageData.eFinanceStorage)oldData.new_financestorage.Value;
                if (oldData.new_howmanycontainers.HasValue)
                    requestData.HowManyContainers = (DML.Consumer.ServiceRequestStorageData.eHowManyContainers)oldData.new_howmanycontainers.Value;
                requestData.HowManyContainersSpecify = oldData.new_howmanycontainersspecify;
                if (oldData.new_planningtousestorage.HasValue)
                    requestData.PlanningToUseStorage = (DML.Consumer.ServiceRequestStorageData.ePlanningToUseStorage)oldData.new_planningtousestorage.Value;
                if (oldData.new_storagelength.HasValue)
                    requestData.StorageLength = (DML.Consumer.ServiceRequestStorageData.eStorageLength)oldData.new_storagelength.Value;
                requestData.StorageLengthSpecify = oldData.new_storagelengthspecify;
                if (oldData.new_typeofstorage.HasValue)
                    requestData.TypeOfStorage = (DML.Consumer.ServiceRequestStorageData.eTypeOfStorage)oldData.new_typeofstorage.Value;
                requestData.ZipCodeOther = oldData.new_zipcodeother;
                request.StorageData = requestData;
            }
        }

        private void AddHomeSecurityData(DML.Xrm.incident incident, DML.ServiceRequest request)
        {
            if (incident.new_homesecuritycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestHomeSecurityData requestData = new DML.Consumer.ServiceRequestHomeSecurityData();
                var oldData = incident.new_homesecuritycasedata_incident;
                if (oldData.new_creditranking.HasValue)
                    requestData.CreditRanking = (DML.Consumer.ServiceRequestHomeSecurityData.eHomeSecurityCreditRankings)oldData.new_creditranking.Value;
                if (oldData.new_timeframe.HasValue)
                    requestData.TimeFrame = (DML.Consumer.ServiceRequestHomeSecurityData.eHomeSecurityTimeFrame)oldData.new_timeframe.Value;
                if (oldData.new_propertytype.HasValue)
                    requestData.PropertyType = (DML.Consumer.ServiceRequestHomeSecurityData.eHomeSecurityPropertyType)oldData.new_propertytype.Value;
                requestData.OwnProperty = oldData.new_ownproperty;
                request.HomeSecurityData = requestData;
            }
        }

        private void AddSocialSecurityDisabilityData(DML.Xrm.incident incident, DML.ServiceRequest request)
        {
            if (incident.new_socialsecuritydisabilitydataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestSocialSecurityDisabilityData requestData = new DML.Consumer.ServiceRequestSocialSecurityDisabilityData();
                var oldData = incident.new_socialsecuritydisabilitycasedata_incident;
                requestData.AbleToWork = oldData.new_abletowork;
                requestData.AlreadyReceiveBenefits = oldData.new_alreadyreceivebenefits;
                requestData.HaveAttorney = oldData.new_haveattorney;
                requestData.MissWork = oldData.new_misswork;
                requestData.PrescribedMedication = oldData.new_prescribedmedication;
                requestData.HasDoctor = oldData.new_hasdoctor;
                requestData.Work5Of10 = oldData.new_work5of10;
                request.SocialSecurityDisabilityData = requestData;
            }
        }

        private void AddPayDayLoanData(DML.Xrm.incident incident, DML.ServiceRequest request)
        {
            if (incident.new_paydayloandataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestPayDayLoanData requestData = new DataModel.Consumer.ServiceRequestPayDayLoanData();
                var oldData = incident.new_paydayloandata_incident;
                requestData.RequestedLoanAmount = oldData.new_loanamount;
                requestData.IsRent = oldData.new_isrentaddress;
                requestData.MonthlyIncome = oldData.new_monthlynetincome;
                if (oldData.new_accounttype.HasValue)
                    requestData.AccountType = (DML.Consumer.ServiceRequestPayDayLoanData.eAccountType)oldData.new_accounttype.Value;
                requestData.DirectDeposit = oldData.new_directdeposit;
                if (oldData.new_payfrequency.HasValue)
                    requestData.PayPeriod = (DML.Consumer.ServiceRequestPayDayLoanData.ePayPeriod)oldData.new_payfrequency.Value;
                requestData.NextPayDate = oldData.new_nextpayday;
                requestData.SecondPayDate = oldData.new_secondpayday;
                requestData.MonthsAtResidence = oldData.new_legthataddress;
                if (oldData.new_incomesource.HasValue)
                    requestData.IncomeType = (DML.Consumer.ServiceRequestPayDayLoanData.eIncomeType)oldData.new_incomesource.Value;
                requestData.IsActiveMilitary = oldData.new_activemilitary;
                requestData.Ocupation = oldData.new_jobtitle;
                requestData.Employer = oldData.new_employername;
                requestData.WorkPhone = oldData.new_workphone;
                requestData.MonthsEmployed = oldData.new_employedmonths;
                requestData.BankName = oldData.new_bankname;
                requestData.AccountNumber = oldData.new_bankaccountnumber;
                requestData.RoutingNumber = oldData.new_abarounting;
                requestData.DrivingLicenseNumber = oldData.new_driverlicense;
                requestData.DrivingLicenseState = oldData.new_driverlicensestate;
                requestData.SocialSecurityNumber = oldData.new_ssn;
                request.PaydayLoanData = requestData;
            }
        }

        private void AddDivorceAttorneyData(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.ServiceRequest request)
        {
            if (incident.new_divorceattorneycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestDivorceAttorneyData requestData = new NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData();
                var oldData = incident.new_divorceattorneycasedata_incident;
                requestData.BeenFiled = oldData.new_hasadivorcecasebeenfiledincourt.HasValue ? oldData.new_hasadivorcecasebeenfiledincourt.Value : false;
                if (oldData.new_howmanychildren.HasValue)
                {
                    requestData.Children = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceChildren)((int)oldData.new_howmanychildren.Value);
                }
                else
                {
                    requestData.Children = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceChildren.Two;
                }
                if (oldData.new_howareyoufinancing.HasValue)
                {
                    requestData.Financing = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceFinancing)((int)oldData.new_howareyoufinancing.Value);
                }
                else
                {
                    requestData.Financing = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceFinancing.IWillDiscussPaymentOptionsWithMyAttorney;
                }
                if (oldData.new_yourannualincome.HasValue)
                {
                    requestData.Income = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceIncome)((int)oldData.new_yourannualincome.Value);
                }
                else
                {
                    requestData.Income = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceIncome.LessThan10K;
                }
                if (oldData.new_spouseannualincome.HasValue)
                {
                    requestData.SpouseIncome = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceIncome)((int)oldData.new_spouseannualincome.Value);
                }
                else
                {
                    requestData.SpouseIncome = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceIncome.LessThan10K;
                }
                requestData.Live = oldData.new_doyoucurrentlylivewithyourspouse.HasValue ? oldData.new_doyoucurrentlylivewithyourspouse.Value : false;
                if (oldData.new_ownedproperty.HasValue)
                {
                    requestData.Property = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceProperty)((int)oldData.new_ownedproperty.Value);
                }
                else
                {
                    requestData.Property = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceProperty.Other;
                }
                requestData.Represented = oldData.new_areyoucurrentlyrepresented.HasValue ? oldData.new_areyoucurrentlyrepresented.Value : false;
                if (oldData.new_howsoonareyoulookingtofile.HasValue)
                {
                    requestData.Soon = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceHowSoon)((int)oldData.new_howsoonareyoulookingtofile.Value);
                }
                else
                {
                    requestData.Soon = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceHowSoon.Immediately;
                }
                if (oldData.new_termsofdivorceagreedby.HasValue)
                {
                    requestData.Terms = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eTermsOfDivorce)((int)oldData.new_termsofdivorceagreedby.Value);
                }
                else
                {
                    requestData.Terms = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eTermsOfDivorce.WeAgreeOnSomeButNotAll;
                }
                request.DivorceAttorneyData = requestData;
            }
        }

        private void AddBankruptcyAttorneyData(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.ServiceRequest request)
        {
            if (incident.new_bankruptcyattorneycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestBankruptcyAttorneyData requestData = new NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData();
                var oldData = incident.new_bankruptcyattorneycasedata_incident;
                requestData.AffordAttorney = oldData.new_canyouaffordanattorney.HasValue ? oldData.new_canyouaffordanattorney.Value : true;
                requestData.Assets = oldData.new_haveadditionalassets.HasValue ? oldData.new_haveadditionalassets.Value : false;
                requestData.BehindAutomobile = oldData.new_behindinautomobilepayments.HasValue ? oldData.new_behindinautomobilepayments.Value : true;
                requestData.BehindRealEstate = oldData.new_behindinrealestatepayments.HasValue ? oldData.new_behindinrealestatepayments.Value : true;
                if (oldData.new_whichbillsdoyouhave.HasValue)
                {
                    requestData.Bills = (DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eBillsBankruptcy)((int)oldData.new_whichbillsdoyouhave.Value);
                }
                else
                {
                    requestData.Bills = NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eBillsBankruptcy.Other;
                }
                if (oldData.new_whyconsideringbankruptcy.HasValue)
                {
                    requestData.Considering = (NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eConsideringBankruptcy)((int)oldData.new_whyconsideringbankruptcy.Value);
                }
                else
                {
                    requestData.Considering = NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eConsideringBankruptcy.Other;
                }
                if (oldData.new_estimatetotalmonthlyexpenses.HasValue)
                {
                    requestData.Expenses = (NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eMoneyEstimateBankruptcy)((int)oldData.new_estimatetotalmonthlyexpenses.Value);
                }
                else
                {
                    requestData.Expenses = NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eMoneyEstimateBankruptcy.LessThan3K;
                }
                if (oldData.new_estimatetotalmonthlyincome.HasValue)
                {
                    requestData.Income = (NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eMoneyEstimateBankruptcy)((int)oldData.new_estimatetotalmonthlyincome.Value);
                }
                else
                {
                    requestData.Income = NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eMoneyEstimateBankruptcy.LessThan3K;
                }
                if (oldData.new_typesofincome.HasValue)
                {
                    requestData.IncomeTypes = (NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eTypesOfIncomeBankruptcy)((int)oldData.new_typesofincome.Value);
                }
                else
                {
                    requestData.IncomeTypes = NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eTypesOfIncomeBankruptcy.Other;
                }
                request.BankruptcyAttorneyData = requestData;
            }
        }

        private void AddDuiAttorneyData(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.ServiceRequest request)
        {
            if (incident.new_duiattorneycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestDuiAttorneyData requestData = new NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData();
                var oldData = incident.new_duiattorneycasedata_incident;
                requestData.AffordAttorney = oldData.new_canyouaffordanattorney.HasValue ? oldData.new_canyouaffordanattorney.Value : true;
                if (oldData.new_areyourepresented.HasValue)
                {
                    switch ((DML.Xrm.new_duiattorneycasedata.AreYouRepresented)oldData.new_areyourepresented.Value)
                    {
                        case NoProblem.Core.DataModel.Xrm.new_duiattorneycasedata.AreYouRepresented.Yes:
                            requestData.Represented = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiAreYouRepresented.Yes;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_duiattorneycasedata.AreYouRepresented.YesButLookingForNewRepresentation:
                            requestData.Represented = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiAreYouRepresented.YesButLookingForNewRepresentation;
                            break;
                        default:
                            requestData.Represented = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiAreYouRepresented.No;
                            break;
                    }
                }
                else
                {
                    requestData.Represented = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiAreYouRepresented.No;
                }
                if (oldData.new_statusofthedui.HasValue)
                {
                    switch ((DML.Xrm.new_duiattorneycasedata.StatusOfTheDui)oldData.new_statusofthedui.Value)
                    {
                        case NoProblem.Core.DataModel.Xrm.new_duiattorneycasedata.StatusOfTheDui.GotDuiAndHaveCourtDate:
                            requestData.Status = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiStatusOfTheDui.GotDuiAndHaveCourtDate;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_duiattorneycasedata.StatusOfTheDui.WantPastDuiRemoved:
                            requestData.Status = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiStatusOfTheDui.WantPastDuiRemoved;
                            break;
                        default:
                            requestData.Status = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiStatusOfTheDui.GotDuiButNoCourtDate;
                            break;
                    }
                }
                else
                {
                    requestData.Status = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiStatusOfTheDui.GotDuiButNoCourtDate;
                }
                if (oldData.new_financing.HasValue)
                {
                    switch ((DML.Xrm.new_duiattorneycasedata.Financing)oldData.new_financing.Value)
                    {
                        case DML.Xrm.new_duiattorneycasedata.Financing.Borrowing:
                            requestData.Financing = DML.Consumer.ServiceRequestDuiAttorneyData.eFinancing.Borrowing;
                            break;
                        case DML.Xrm.new_duiattorneycasedata.Financing.CannotAfford:
                            requestData.Financing = DML.Consumer.ServiceRequestDuiAttorneyData.eFinancing.CannotAfford;
                            break;
                        case DML.Xrm.new_duiattorneycasedata.Financing.CurrentIncome:
                            requestData.Financing = DML.Consumer.ServiceRequestDuiAttorneyData.eFinancing.CurrentIncome;
                            break;
                        case DML.Xrm.new_duiattorneycasedata.Financing.FamilySupport:
                            requestData.Financing = DML.Consumer.ServiceRequestDuiAttorneyData.eFinancing.FamilySupport;
                            break;
                        case DML.Xrm.new_duiattorneycasedata.Financing.PersonalSavings:
                            requestData.Financing = DML.Consumer.ServiceRequestDuiAttorneyData.eFinancing.PersonalSavings;
                            break;
                        case DML.Xrm.new_duiattorneycasedata.Financing.WillDiscussWithAttorney:
                            requestData.Financing = DML.Consumer.ServiceRequestDuiAttorneyData.eFinancing.WillDiscussWithAttorney;
                            break;
                    }
                }
                else
                {
                    requestData.Financing = DML.Consumer.ServiceRequestDuiAttorneyData.eFinancing.CurrentIncome;
                }
                request.DuiAttorneyData = requestData;
            }
        }

        private void AddCriminalLawAttorneyData(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.ServiceRequest request)
        {
            if (incident.new_criminallawattorneycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestCriminalLawAttorneyData requestData = new NoProblem.Core.DataModel.Consumer.ServiceRequestCriminalLawAttorneyData();
                var oldData = incident.new_criminallawattorneycasedata_incident;
                requestData.CurrentCharges = oldData.new_doyouhavecurrentchargesagainstyou.HasValue ? oldData.new_doyouhavecurrentchargesagainstyou.Value : true;
                requestData.AffordAttorney = oldData.new_canyouaffordanattorney.HasValue ? oldData.new_canyouaffordanattorney.Value : true;
                request.CriminalLawAttorneyData = requestData;
            }
        }

        private void AddPersonalInjuryAttorneyData(NoProblem.Core.DataModel.Xrm.incident incident, DataModel.ServiceRequest request)
        {
            if (incident.new_personalinjurycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData piData = new NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData();
                var oldData = incident.new_personalinjurycasedata_incident;
                if (oldData.new_accidentresult.HasValue)
                {
                    switch ((DataModel.Xrm.new_personalinjurycasedata.AccidentResult)oldData.new_accidentresult.Value)
                    {
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.hospitalization:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.hospitalization;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.missedWork:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.missedWork;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.noneOfTheAbove:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.noneOfTheAbove;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.surgery:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.surgery;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.wrongfulDeath:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.wrongfulDeath;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.medicalTreatment:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.medicalTreatment;
                            break;
                    }
                }
                else
                {
                    piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.medicalTreatment;
                }
                if (oldData.new_doesanyonehasinsurance.HasValue)
                {
                    switch (oldData.new_doesanyonehasinsurance.Value)
                    {
                        case (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.Yes:
                            piData.DoesAnyoneHasVehicleInsurance = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAnyoneHasInsurance.Yes;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.No:
                            piData.DoesAnyoneHasVehicleInsurance = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAnyoneHasInsurance.No;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.DontKnow:
                            piData.DoesAnyoneHasVehicleInsurance = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAnyoneHasInsurance.DontKnow;
                            break;
                    }
                }
                else
                {
                    piData.DoesAnyoneHasVehicleInsurance = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAnyoneHasInsurance.Yes;
                }
                if (oldData.new_medicalbills.HasValue)
                {
                    switch (oldData.new_medicalbills.Value)
                    {
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.dontKnow:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.dontKnow;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e10kTo25k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.e10kTo25k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e1kTo5k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.e1kTo5k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e25kTo100k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.e25kTo100k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e5kTo10k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.e5kTo10k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.lessThan1k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.lessThan1k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.moreThan100k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.moreThan100k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.noMedicalBills:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.noMedicalBills;
                            break;
                    }
                }
                else
                {
                    piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.dontKnow;
                }
                bool atLeastOneMarked = false;
                if (oldData.new_whiplash.HasValue && oldData.new_whiplash.Value)
                {
                    piData.Whiplash = true;
                    atLeastOneMarked = true;
                }
                if (oldData.new_brokenbones.HasValue && oldData.new_brokenbones.Value)
                {
                    piData.BrokenBones = true;
                    atLeastOneMarked = true;
                }
                if (oldData.new_lostlimb.HasValue && oldData.new_lostlimb.Value)
                {
                    piData.LostLimb = true;
                    atLeastOneMarked = true;
                }
                if (oldData.new_spinalcordinjuryorparalysis.HasValue && oldData.new_spinalcordinjuryorparalysis.Value)
                {
                    piData.SpinalCordInjuryOrParalysis = true;
                    atLeastOneMarked = true;
                }
                if (oldData.new_braininjury.HasValue && oldData.new_braininjury.Value)
                {
                    piData.BrainInjury = true;
                    atLeastOneMarked = true;
                }
                if (oldData.new_lossoflife.HasValue && oldData.new_lossoflife.Value)
                {
                    piData.LossOfLife = true;
                    atLeastOneMarked = true;
                }
                if ((oldData.new_other.HasValue && oldData.new_other.Value) || !atLeastOneMarked)
                {
                    piData.Other = true;
                }
                piData.CurrentlyRepresented = oldData.new_currentlyrepresented.HasValue ? oldData.new_currentlyrepresented.Value : false;
                piData.IsYourFault = oldData.new_isyourfault.HasValue ? oldData.new_isyourfault.Value : false;
                request.PersonalInjuryAttorneyData = piData;
            }
        }

        private void AddMoverData(NoProblem.Core.DataModel.Xrm.incident incident, DataModel.ServiceRequest request)
        {
            bool needsMoverData = false;
            DataModel.Consumer.ServiceRequestMoverData moverData = new NoProblem.Core.DataModel.Consumer.ServiceRequestMoverData();
            if (incident.new_movedate.HasValue)
            {
                needsMoverData = true;
                moverData.MoveDate = incident.new_movedate;
            }
            if (!string.IsNullOrEmpty(incident.new_movetozipcode))
            {
                needsMoverData = true;
                moverData.MoveToZipCode = incident.new_movetozipcode;
            }
            if (!string.IsNullOrEmpty(incident.new_movesize))
            {
                needsMoverData = true;
                moverData.MoveSize = (DataModel.Consumer.ServiceRequestMoverData.eMoveSize)Enum.Parse(typeof(DataModel.Consumer.ServiceRequestMoverData.eMoveSize), incident.new_movesize);
            }
            if (!string.IsNullOrEmpty(incident.new_movetype))
            {
                needsMoverData = true;
                moverData.MoveType = (DataModel.Consumer.ServiceRequestMoverData.eMoveType)Enum.Parse(typeof(DataModel.Consumer.ServiceRequestMoverData.eMoveType), incident.new_movetype);
            }
            if (needsMoverData)
            {
                request.MoverData = moverData;
            }
        }

    }
}
