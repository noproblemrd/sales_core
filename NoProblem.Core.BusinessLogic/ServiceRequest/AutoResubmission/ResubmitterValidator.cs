﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.AutoResubmission
{
    internal class ResubmitterValidator
    {
        static DataAccessLayer.Generic genericDal = new DataAccessLayer.Generic();

        public static bool ValidateResubmission(DataModel.Xrm.incident incident)
        {
            if (!incident.new_upsaleid.HasValue)//cases that are direct numbers from blocked telephones cannot be upsaled and do not have an upsaleid.
            {
                return false;
            }
            return (ValidateResubmissionAtIncidentLevel(incident) && ValidateResubmissionAtUpsaleLevel(incident));
        }

        private static bool ValidateResubmissionAtIncidentLevel(DataModel.Xrm.incident incident)
        {
            if (incident.new_usedaar.HasValue && incident.new_usedaar.Value)
            {
                return false;
            }
            if (incident.statuscode != (int)DataModel.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER
                 && incident.statuscode != (int)DataModel.Xrm.incident.Status.WORK_DONE)
            {
                //ended with status that we don't resubmit.
                return false;
            }
            if (incident.new_isstoppedmanually.HasValue && incident.new_isstoppedmanually.Value)
            {
                //stopped manually.
                return false;
            }
            if (incident.new_requiredaccountno <= incident.new_ordered_providers)
            {
                //fully fullfiled.
                return false;
            }
            if (SoldToExclusive(incident))
            {
                //sold to exclusive, can't sell this anymore.
                return false;
            }
            return true;
        }

        private static bool ValidateResubmissionAtUpsaleLevel(DataModel.Xrm.incident incident)
        {
            if (GetNumberOfUpsales(incident.new_upsaleid.Value) >= 2)
            {
                //incident has been resubmitted already twice at least.
                return false;
            }
            if (GetNumberOfCallsToConsumer(incident.new_upsaleid.Value) >= 9)
            {
                //consumer was called at least 9 times.
                return false;
            }
            if (GetMaxConnectionTime(incident.new_upsaleid.Value) > 40)
            {
                //consumer talked to advertiser for at least 40 seconds
                return false;
            }
            return true;
        }

        private static bool SoldToExclusive(DataModel.Xrm.incident incident)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@incidentId", incident.incidentid);
            object scalar = genericDal.ExecuteScalar(queryIsSoldToExclusive, parameters);
            return (scalar != null && scalar != DBNull.Value);
        }

        private static int GetMaxConnectionTime(Guid upsaleId)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@upsaleId", upsaleId);
            int max = (int)genericDal.ExecuteScalar(queryMaxConnectionTime, parameters);
            return max;
        }

        private static int GetNumberOfCallsToConsumer(Guid upsaleId)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@upsaleId", upsaleId);
            int num = (int)genericDal.ExecuteScalar(queryNumberOfCallsToConsumer, parameters);
            return num;
        }

        private static int GetNumberOfUpsales(Guid upsaleId)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@upsaleId", upsaleId);
            int num = (int)genericDal.ExecuteScalar(queryNumberOfUpsales, parameters);
            return num;
        }

        private const string queryIsSoldToExclusive =
            @"
 select 1
 from 
 New_incidentaccount ia with(nolock)
 join Account acc with(nolock) on ia.new_accountid = acc.AccountId
 where
 ia.new_incidentid = @incidentId
 and acc.New_IsExclusiveBroker = 1
 and ia.New_tocharge = 1
";

        private const string queryMaxConnectionTime =
            @"
select isnull(max(new_duration), 0) maxconnection
from New_calldetailrecord cdr with(nolock)
join New_incidentaccount ia with(Nolock)on ia.New_incidentaccountId = cdr.new_incidentaccountid
join Account acc with(nolock) on acc.AccountId = ia.new_accountid
join incident inc with(nolock) on inc.IncidentId = ia.new_incidentid
where
inc.new_upsaleid = @upsaleId
and 
(
(cdr.New_Type = 2 and acc.New_IsLeadBuyer = 1 and acc.New_LeadBuyerName = 'phone')
or
(cdr.New_Type = 4)
or
(cdr.New_Type = 3 and (acc.New_IsLeadBuyer = 0 or acc.New_IsLeadBuyer is null))
)
";

        private const string queryNumberOfCallsToConsumer =
            @"
select COUNT(*)
from New_calldetailrecord cdr with(nolock)
join New_incidentaccount ia with(Nolock) on ia.New_incidentaccountId = cdr.new_incidentaccountid
join incident inc with(nolock) on inc.IncidentId = ia.new_incidentid
where
inc.new_upsaleid = @upsaleId
and cdr.New_Type = 3
";

        private const string queryNumberOfUpsales =
            @"
select COUNT(*)
from incident inc with(nolock)
where 
inc.new_upsaleid = @upsaleId
and inc.New_isupsale = 1
";
    }
}
