﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.AutoResubmission
{
    public class Resubmitter
    {
        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
        private static Timer timer;

        private Resubmitter()
        {

        }

        public static void OnAppStart()
        {
            DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.AutoResubmission);
            timer = new Timer(
                Callback,
                null,
                1000 * 60,
                1000 * 60 * 5);
        }

        private static void Callback(object obj)
        {
            try
            {
                DataAccessLayer.ConfigurationSettings configDal = new DataAccessLayer.ConfigurationSettings(null);
                if (configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUTO_RESUBMISSION_ENABLED) == 1.ToString())
                {
                    Start();
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Resubmitter.Callback");
            }
        }

        private static void Start()
        {
            if (DistributedLocksManager.AquireLock(DistributedLocksManager.eLocks.AutoResubmission))            
            {
                try
                {
                    List<Runnable> threads = new List<Runnable>();
                    var reader = dal.ExecuteReader(query);
                    foreach (var row in reader)
                    {
                        try
                        {
                            Guid resubmissionId = (Guid)row["resubmissionId"];
                            Guid incidentId = (Guid)row["incidentOriginalId"];
                            ResubmittedRequestsSender sender = new ResubmittedRequestsSender(resubmissionId, incidentId);
                            sender.Start();
                            threads.Add(sender);
                        }
                        catch (Exception exc)
                        {
                            LogUtils.MyHandle.HandleException(exc, "Exception in Resubmitter loop. Advancing to next submission.");
                        }
                    }
                    foreach (var thread in threads)
                    {
                        thread.Join();
                    }
                }
                finally
                {
                    DistributedLocksManager.ReleaseLock(DistributedLocksManager.eLocks.AutoResubmission);
                }
            }
        }

        private const string query =
            @"select New_resubmissionid resubmissionId, new_incidentoriginalid incidentOriginalId
            from New_resubmission
            where New_Done = 0
            and New_ScheduledDate between DATEADD(MINUTE, -5, getdate()) and getdate()";
    }
}
