﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class WaitForStepTimer : XrmUserBase
    {
        public WaitForStepTimer()
            : base(null)
        {

        }

        private static Timer waitTimer;

        public static void SetTimerOnAppStart()
        {
            if (waitTimer == null)
            {
                waitTimer = new Timer(
                    TimerCallback,
                    null,
                    CalculateFirstInvoke(),
                    new TimeSpan(0, 1, 0)
                    );
            }
        }

        private static TimeSpan CalculateFirstInvoke()
        {
            return DateTime.Now.AddMinutes(1) - DateTime.Now;
        }

        private static void TimerCallback(object obj)
        {
            try
            {
                WaitForStepTimer instance = new WaitForStepTimer();
                instance.CheckCasesWaitingForStep();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WaitForStepTimer.TimerCallback.");
            }
        }

        private void CheckCasesWaitingForStep()
        {
            DataAccessLayer.CaseWaitForStep dal = new NoProblem.Core.DataAccessLayer.CaseWaitForStep(XrmDataContext);
            var reader = dal.ExecuteReader(query);
            DataAccessLayer.Contact contactDal = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            DataAccessLayer.PrimaryExpertise headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            foreach (var row in reader)
            {
                try
                {
                    DataModel.ServiceRequest request = new NoProblem.Core.DataModel.ServiceRequest();
                    var contact = contactDal.Retrieve((Guid)row["contactId"]);
                    request.ContactEmail = contact.emailaddress1;
                    request.ContactFullName = contact.fullname;
                    request.ContactPhoneNumber = contact.mobilephone;
                    request.ControlName = Convert.ToString(row["controlname"]);
                    request.Domain = Convert.ToString(row["domain"]);
                    var heading = headingDal.Retrieve((Guid)row["headingId"]);
                    request.ExpertiseCode = heading.new_code;
                    request.ExpertiseType = 1;
                    request.IP = Convert.ToString(row["ip"]);
                    request.Keyword = Convert.ToString(row["keyword"]);
                    request.NumOfSuppliers = row["numberofsuppliers"] != DBNull.Value ? (int)row["numberofsuppliers"] : 3;
                    request.OriginId = (Guid)row["originid"];
                    request.PageName = Convert.ToString(row["pagename"]);
                    request.PlaceInWebSite = Convert.ToString(row["placeinwebsite"]);
                    request.PrefferedCallTime = DateTime.Now;
                    var region = regionDal.Retrieve((Guid)row["regionId"]);
                    request.RegionCode = region.new_code;
                    request.RegionLevel = region.new_level.Value;
                    request.RequestDescription = Convert.ToString(row["description"]);
                    request.SessionId = Convert.ToString(row["sessionid"]);
                    request.SubType = Convert.ToString(row["subtype"]);
                    request.ToolbarId = Convert.ToString(row["toolbarid"]);
                    request.Url = Convert.ToString(row["url"]);
                    request.ExposureId = row["exposureId"] == DBNull.Value ? string.Empty : (string)row["exposureId"];
                    request.Browser = Convert.ToString(row["browser"]);
                    request.BrowserVersion = Convert.ToString(row["browserversion"]);
                    request.SiteId = Convert.ToString(row["siteid"]);
                    request.SiteTitle = Convert.ToString(row["title"]);
                    request.Country = Convert.ToString(row["country"]);
                    request.Step = -10;
                    Thread tr = new Thread(new ParameterizedThreadStart(SendRequestAsync));
                    tr.Start(request);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in WaitForStepTimer.CheckCasesWaitingForStep in foreach, going to next case.");
                }
            }
        }

        private void SendRequestAsync(object obj)
        {
            try
            {
                DataModel.ServiceRequest request = (DataModel.ServiceRequest)obj;
                ServiceRequest.ServiceRequestManager manager = new ServiceRequestManager(null);
                manager.CreateService(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WaitForStepTimer.SendRequestAsync.");
            }
        }

        private const string query =
            @"
create table #tmpWaiting 
(contactId uniqueidentifier
, headingId uniqueidentifier
, regionId uniqueidentifier
, caseWaitId uniqueidentifier
, controlname nvarchar(100)
, description nvarchar(2000)
, domain nvarchar(100)
, ip nvarchar(100)
, keyword nvarchar(100)
, numberofsuppliers int
, originid uniqueidentifier
, pagename nvarchar(100)
, placeinwebsite nvarchar(100)
, sessionid nvarchar(100)
, subtype nvarchar(100)
, toolbarid nvarchar(100)
, url nvarchar(2000)
, exposureId nvarchar(100)
, browser nvarchar(100)
, siteid nvarchar(100)
, title nvarchar(100)
, browserversion nvarchar(100)
, country nvarchar(100)
)

insert into #tmpWaiting
select 
cwfs.new_contactid
, cwfs.new_primaryexpertiseid
, cwfs.new_regionid
, cwfs.new_casewaitforstepid
, cwfs.new_controlname
, cwfs.new_description
, cwfs.new_domain
, cwfs.new_ip
, cwfs.new_keyword
, cwfs.new_numberofsuppliers
, cwfs.new_originid
, cwfs.new_pagename
, cwfs.new_placeinwebsite
, cwfs.new_sessionid
, cwfs.new_subtype 
, tlbr.new_name
, cwfs.new_url
, cwfs.new_exposureid 
, cwfs.new_browser
, cwfs.new_siteid
, cwfs.new_sitetitle
, cwfs.new_browserversion
, cwfs.new_country
from new_casewaitforstep cwfs
left join new_toolbarid tlbr on tlbr.new_toolbaridid = cwfs.new_toolbarid
            where isnull(cwfs.new_done, 0) = 0
            and cwfs.new_launchtime < getdate()
            
update new_casewaitforstepextensionbase
set new_done = 1,
new_senttype = 1,
new_senton = getdate()
where new_casewaitforstepid in
(select caseWaitId from #tmpWaiting)            
            
select * from #tmpWaiting 

drop table #tmpWaiting ";
    }
}
