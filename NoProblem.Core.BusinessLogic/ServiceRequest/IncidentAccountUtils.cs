﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using Effect.Crm.Configuration;
using Effect.Crm.Convertor;
using Effect.Crm.Logs;
using Effect.Crm.Methods;
using Effect.Crm.Plugins.DataContext;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class IncidentAccountUtils : ContextsUserBase
    {
        #region Ctor
        public IncidentAccountUtils(DML.Xrm.DataContext xrmDataContext, CrmService crmService)
            : base(xrmDataContext, crmService)
        {

        }
        #endregion
    
        public void checkIfStatusChangeToParticularValues(ExecutionContext context)
        {
            Microsoft.Crm.Sdk.DynamicEntity postImage = null;
            if (context.PostEntityImages.Contains("Image"))
                postImage = context.PostEntityImages.Properties["Image"] as Microsoft.Crm.Sdk.DynamicEntity;

            Microsoft.Crm.Sdk.DynamicEntity preImage = null;

            if (context.PreEntityImages.Contains("Image"))
                preImage = context.PreEntityImages.Properties["Image"] as Microsoft.Crm.Sdk.DynamicEntity;

            if (DynamicUtils.CheckPropertyModified(preImage, postImage, "statuscode"))
            {
                string PostStatusCode = DynamicUtils.GetPropertyValueFromEntity(postImage, "statuscode");
                int PostStatusCodeNumebr = int.Parse(PostStatusCode);

                string potentialIncidentPrice = DynamicUtils.GetPropertyValueFromEntity(postImage, "new_potentialincidentprice");
                string accountIdStr = DynamicUtils.GetPropertyValueFromEntity(postImage, "new_accountid");
                Guid accountId = new Guid(accountIdStr);

                string incidentIdStr = DynamicUtils.GetPropertyValueFromEntity(postImage, "new_incidentid");
                DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
                NoProblem.Core.DataModel.Xrm.incident _incident = incidentDal.Retrieve(new Guid(incidentIdStr));

                Balance.BalanceManager balanceManager = new NoProblem.Core.BusinessLogic.Balance.BalanceManager(XrmDataContext);                
                if (PostStatusCodeNumebr == (int)DML.Xrm.new_incidentaccount.Status.MATCH)
                {
                    if (_incident.casetypecode != (int)DataModel.Xrm.incident.CaseTypeCode.Video)  
                        balanceManager.AddToAvailableBalance(accountId, decimal.Parse(potentialIncidentPrice) * -1);
                }
                else if (PostStatusCodeNumebr == (int)DML.Xrm.new_incidentaccount.Status.LOST)
                {
                    if (_incident.casetypecode != (int)DataModel.Xrm.incident.CaseTypeCode.Video)  
                        balanceManager.AddToAvailableBalance(accountId, decimal.Parse(potentialIncidentPrice));
                    string idStr = DynamicUtils.GetPropertyValueFromEntity(postImage, "new_incidentaccountid");
                    DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                    Guid id = new Guid(idStr);
                    dal.SetClosedOn(id);
                }
                else if (
                    PostStatusCodeNumebr == (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS ||
                    PostStatusCodeNumebr == (int)DML.Xrm.new_incidentaccount.Status.CLOSE ||
                    PostStatusCodeNumebr == (int)DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS)
                {
                    string idStr = DynamicUtils.GetPropertyValueFromEntity(postImage, "new_incidentaccountid");
                    DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                    Guid id = new Guid(idStr);
                    dal.SetClosedOn(id);
                    
                    string leadNumber = dal.GetTicketNumber(id);
                    if(_incident.casetypecode != (int)DataModel.Xrm.incident.CaseTypeCode.Video)                    
                        balanceManager.ChargeLead(accountId, decimal.Parse(potentialIncidentPrice) * -1, NoProblem.Core.DataModel.Xrm.new_balancerow.Action.Call, id, leadNumber);
                    AddToServiceRequestCounter(accountId);
                    CheckRefundRequestPercentage(accountId);
                    CallPricePickerTool.CallPricePickerToolManager.SaveStats(accountId, id);
                }
            }
        }

        private void CheckRefundRequestPercentage(Guid accountId)
        {
            System.Threading.Thread tr = new System.Threading.Thread(
                new System.Threading.ParameterizedThreadStart(
                    delegate(object idObject)
                    {
                        try
                        {
                            Guid supplierId = (Guid)idObject;
                            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(null);
                            var supplier = dal.Retrieve(supplierId);
                            if (supplier.new_isrefundblocked.HasValue && supplier.new_isrefundblocked.Value)
                            {
                                DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(dal.XrmDataContext);
                                double refundRequestPercent = dal.GetRefundRequestPercent(supplierId, false);
                                string limitStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.REFUND_REQUEST_PERCENTAGE_LIMIT);
                                int limitPercent;
                                if (int.TryParse(limitStr, out limitPercent) == false)
                                {
                                    limitPercent = 101;
                                }
                                if (limitPercent > refundRequestPercent)
                                {
                                    supplier.new_isrefundblocked = false;
                                    dal.Update(supplier);
                                    dal.XrmDataContext.SaveChanges();
                                }
                            }
                        }
                        catch (Exception exc)
                        {
                            LogUtils.MyHandle.HandleException(exc, "Exception in CheckRefundRequestPercentage");
                        }
                    }));
            tr.Start(accountId);
        }



        private void AddToServiceRequestCounter(Guid accountId)
        {
            //string query = "UPDATE account SET new_sumassistancerequests = ISNULL(new_sumassistancerequests,0) +1 WHERE accountid = '{0}'";
            //Effect.Crm.DB.DataBaseUtils.ExecuteSqlString(query, accountId);
            string query = "UPDATE account SET new_sumassistancerequests = ISNULL(new_sumassistancerequests,0) + 1 WHERE accountid = @id";
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@id", accountId);
            dal.ExecuteNonQuery(query, parameters);
        }

        public void UpdateIncidentByIncidentAccount(ExecutionContext context)
        {
            Microsoft.Crm.Sdk.DynamicEntity postImage = context.PostEntityImages["Image"] as Microsoft.Crm.Sdk.DynamicEntity;

            Microsoft.Crm.Sdk.DynamicEntity preImage = null;
            if (context.PreEntityImages.Contains("Image"))
                preImage = context.PreEntityImages["Image"] as Microsoft.Crm.Sdk.DynamicEntity;

            string postIncidentId = DynamicUtils.GetPropertyValueFromEntity(postImage, "new_incidentid");

            if (postIncidentId == null || postIncidentId == "")
                return;

            DynamicEntity updateIncident = MethodsUtils.GetDynamicEntity(CrmService, "incident", new Guid(postIncidentId));

            if (updateIncident == null)
                return;

            string preStatusCode = "";
            if (preImage != null)
                preStatusCode = DynamicUtils.GetPropertyValueFromEntity(preImage, "statuscode");
            string postStatusCode = DynamicUtils.GetPropertyValueFromEntity(postImage, "statuscode");

            DML.Xrm.new_incidentaccount.Status preIncidentAccountStatusCode = (DML.Xrm.new_incidentaccount.Status)int.Parse(preStatusCode == "" ? "-1" : preStatusCode);
            DML.Xrm.new_incidentaccount.Status postIncidentAccountStatusCode = (DML.Xrm.new_incidentaccount.Status)int.Parse(postStatusCode == "" ? "-1" : postStatusCode);

            if (preIncidentAccountStatusCode != postIncidentAccountStatusCode && postIncidentAccountStatusCode == DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS)
            {
                string accountIDstr = DynamicUtils.GetPropertyValueFromEntity(postImage, "new_accountid");

                //object customerid = ConvertorCRMParams.GetDynamicEntityPropertyValue(updateIncident, "customerid");
                //if (customerid != null)
                //{
                //DynamicEntity anonymousContact = MethodsUtils.GetDynamicEntity(CrmService, "contact", new Guid(customerid.ToString()));

                //object new_contactnumber = ConvertorCRMParams.GetDynamicEntityPropertyValue(anonymousContact, "new_contactnumber");
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                Guid accountId = new Guid(accountIDstr);
                var account = dal.Retrieve(accountId);
                if (!account.new_dapazstatus.HasValue ||
                    account.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.PPA)
                {
                    Notifications notificationsLogic = new Notifications(XrmDataContext);
                    notificationsLogic.NotifySupplier(DML.enumSupplierNotificationTypes.INCIDENT_INFO, accountId, postIncidentId, "incident", postIncidentId, "incident");
                }
                //}
            }

            //UpdateIncidentProvidersSum(preIncidentAccountStatusCode, postIncidentAccountStatusCode, updateIncident);
            //UpdateIncidentStatus(preIncidentAccountStatusCode, postIncidentAccountStatusCode, updateIncident);

            //CrmService.Update(updateIncident);
        }
    }
}
