﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;
using NoProblem.Core.DataModel.Consumer;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class DuiAttorneyRules : TwoStepWaitingRulesBase
    {
        public DuiAttorneyRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private const string STATUS_HAVE_DATE = "I got a DUI and have a court date soon. ";
        private const string STATUS_DONT_HAVE_DATE = "I got a DUI but not a court date yet. ";
        private const string REPRESENTED = "I am represented. ";
        private const string LOOKING = "I am represented but I am looking for new representation. ";
        private const string PERIOD = ". ";

        protected override Microsoft.Xrm.Client.CrmEntity HandleManualUpsale(incident incident, incident lastIncident)
        {
            new_duiattorneycasedata data = null;
            if (lastIncident.new_duiattorneycasedataid.HasValue)
            {
                incident.new_duiattorneycasedataid = lastIncident.new_duiattorneycasedataid.Value;
                data = lastIncident.new_duiattorneycasedata_incident;
            }
            //else
            //{
            //    data = CreateDefaultCaseDataEntry();
            //    incident.new_duiattorneycasedataid = data.new_duiattorneycasedataid;
            //}
            return data;
        }

        protected override string CreateDescription(string descriptionFromRequest, Microsoft.Xrm.Client.CrmEntity data)
        {
            StringBuilder description = new StringBuilder(descriptionFromRequest);
            if (data != null)
            {                
                description.Append(PERIOD);
                DataModel.Xrm.new_duiattorneycasedata duiData = (DataModel.Xrm.new_duiattorneycasedata)data;
                if (duiData.new_statusofthedui == (int)DataModel.Xrm.new_duiattorneycasedata.StatusOfTheDui.GotDuiAndHaveCourtDate)
                {
                    description.Append(STATUS_HAVE_DATE);
                }
                else if (duiData.new_statusofthedui == (int)DataModel.Xrm.new_duiattorneycasedata.StatusOfTheDui.GotDuiButNoCourtDate)
                {
                    description.Append(STATUS_DONT_HAVE_DATE);
                }
                if (duiData.new_areyourepresented == (int)DataModel.Xrm.new_duiattorneycasedata.AreYouRepresented.Yes)
                {
                    description.Append(REPRESENTED);
                }
                else if (duiData.new_areyourepresented == (int)DataModel.Xrm.new_duiattorneycasedata.AreYouRepresented.YesButLookingForNewRepresentation)
                {
                    description.Append(LOOKING);
                }
            }
            return description.ToString();
        }

        protected override Microsoft.Xrm.Client.CrmEntity CreateAndAddCaseDataToIncident(incident incident, ServiceRequestTwoStepWaitingData reqData)
        {
            DataModel.Consumer.ServiceRequestDuiAttorneyData duiReqData = (DataModel.Consumer.ServiceRequestDuiAttorneyData)reqData;
            new_duiattorneycasedata data = null;
            if (reqData != null)
            {
                data = CreateCaseDataEntry(duiReqData);
                incident.new_duiattorneycasedataid = data.new_duiattorneycasedataid;
            }
            //else
            //{
            //    data = CreateDefaultCaseDataEntry();
            //}
            //incident.new_duiattorneycasedataid = data.new_duiattorneycasedataid;
            return data;
        }

        protected override ServiceRequestTwoStepWaitingData GetRequestData(DataModel.ServiceRequest servRequest)
        {
            return servRequest.DuiAttorneyData;
        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleTimeOutRequest(incident incident)
        {
            //DataModel.Xrm.new_duiattorneycasedata data = CreateDefaultCaseDataEntry();
            //incident.new_duiattorneycasedataid = data.new_duiattorneycasedataid;
            //return data;
            return null;
        }

        //private new_duiattorneycasedata CreateDefaultCaseDataEntry()
        //{
        //    new_duiattorneycasedata data = new new_duiattorneycasedata();
        //     int _rnd = new Random().Next(2);
        //     switch (_rnd)
        //     {
        //         case(0):
        //             data.new_statusofthedui = (int)new_duiattorneycasedata.StatusOfTheDui.GotDuiButNoCourtDate;
        //             data.new_canyouaffordanattorney = true;
        //             data.new_areyourepresented = (int)new_duiattorneycasedata.AreYouRepresented.No;
        //             break;
        //         default:
        //             data.new_statusofthedui = (int)new_duiattorneycasedata.StatusOfTheDui.GotDuiAndHaveCourtDate;
        //             data.new_canyouaffordanattorney = true;
        //             data.new_areyourepresented = (int)new_duiattorneycasedata.AreYouRepresented.No;
        //             break;
        //     }           
        //    DataAccessLayer.DuiAttorneyCaseData dal = new NoProblem.Core.DataAccessLayer.DuiAttorneyCaseData(XrmDataContext);
        //    dal.Create(data);
        //    XrmDataContext.SaveChanges();
        //    return data;
        //}

        private new_duiattorneycasedata CreateCaseDataEntry(ServiceRequestDuiAttorneyData reqData)
        {
            new_duiattorneycasedata data = new new_duiattorneycasedata();
            data.new_areyourepresented = (int)reqData.Represented;
            data.new_canyouaffordanattorney = reqData.AffordAttorney;
            data.new_financing = (int)reqData.Financing;
            data.new_statusofthedui = (int)reqData.Status;
            DataAccessLayer.DuiAttorneyCaseData dal = new NoProblem.Core.DataAccessLayer.DuiAttorneyCaseData(XrmDataContext);
            dal.Create(data);
            XrmDataContext.SaveChanges();
            return data;
        }
    }
}
