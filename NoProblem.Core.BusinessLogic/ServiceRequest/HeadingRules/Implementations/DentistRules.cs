﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class DentistRules : HeadingRuleBase
    {
        private const string DESCRIPTION_WITH = "I need a dentist for {0}";
        private const string DESCRIPTION_WITHOUT = "I need a dentist";

        public DentistRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        #region IHeadingRule Members

        public override bool ApplyRules(IServiceRequest request, IServiceResponse response, DataModel.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            var servRequest = (DataModel.ServiceRequest)request;
            if (!string.IsNullOrEmpty(servRequest.SubType))
            {
                incident.description = string.Format(DESCRIPTION_WITH, servRequest.SubType);
            }
            else
            {
                incident.description = DESCRIPTION_WITHOUT;
            }
            incident.new_webdescription = incident.description;
            RestoreSubType(isManualUpsale, lastIncidentId, incident);
            return true;
        }

        #endregion
    }
}
