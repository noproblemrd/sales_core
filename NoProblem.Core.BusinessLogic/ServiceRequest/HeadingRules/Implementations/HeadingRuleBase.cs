﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal abstract class HeadingRuleBase : XrmUserBase, IHeadingRule
    {
        public HeadingRuleBase(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        protected void RestoreSubType(bool isManualUpsale, Guid lastIncidentId, DataModel.Xrm.incident incident)
        {
            if (isManualUpsale)
            {
                DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                var lastIncident = dal.Retrieve(lastIncidentId);
                incident.new_subtype = lastIncident.new_subtype;
                incident.new_budget = lastIncident.new_budget;
            }
        }

        protected void RestoreSubType(DataModel.Xrm.incident lastIncident, DataModel.Xrm.incident incident)
        {
            incident.new_subtype = lastIncident.new_subtype;
        }

        public virtual bool ApplyRules(IServiceRequest request, IServiceResponse response, DataModel.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            RestoreSubType(isManualUpsale, lastIncidentId, incident);
            return true;
        }
    }
}
