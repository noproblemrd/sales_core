﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class CleanerRules : HeadingRuleBase
    {
        private const string DESCRIPTION = "I need a cleaner for {0}";

        public CleanerRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public override bool ApplyRules(IServiceRequest request, IServiceResponse response, DataModel.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            var servRequest = (DataModel.ServiceRequest)request;
            if (!string.IsNullOrEmpty(servRequest.SubType))
            {
                incident.description = string.Format(DESCRIPTION, servRequest.SubType);
                incident.new_webdescription = incident.description;
            }
            RestoreSubType(isManualUpsale, lastIncidentId, incident);
            return true;
        }
    }
}
