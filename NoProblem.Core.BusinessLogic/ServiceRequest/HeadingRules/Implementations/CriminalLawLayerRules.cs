﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class CriminalLawLayerRules : TwoStepWaitingRulesBase
    {
        public CriminalLawLayerRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private const string DESCRIPTION_TEMPLATE =
            @"{2}. I {0} current charges agains me. I {1} afford an attorney.";
        private const string HAVE = "have";
        private const string HAVE_NO = "have no";
        private const string CAN = "can";
        private const string CANT = "can't";

        protected override Microsoft.Xrm.Client.CrmEntity HandleTimeOutRequest(incident incident)
        {
            //DataModel.Xrm.new_criminallawattorneycasedata data = CreateDefaultCaseDataEntry();
            //incident.new_criminallawattorneycasedataid = data.new_criminallawattorneycasedataid;
            //return data;
            return null;
        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleManualUpsale(incident incident, incident lastIncident)
        {
            DataModel.Xrm.new_criminallawattorneycasedata data = null;
            if (lastIncident.new_criminallawattorneycasedataid.HasValue)
            {
                incident.new_criminallawattorneycasedataid = lastIncident.new_criminallawattorneycasedataid.Value;
                data = lastIncident.new_criminallawattorneycasedata_incident;
            }
            //else
            //{
            //    data = CreateDefaultCaseDataEntry();
            //    incident.new_criminallawattorneycasedataid = data.new_criminallawattorneycasedataid;
            //}
            return data;
        }

        protected override string CreateDescription(string descriptionFromRequest, Microsoft.Xrm.Client.CrmEntity data)
        {
            try
            {
                if (data != null)
                {
                    DataModel.Xrm.new_criminallawattorneycasedata criData = (DataModel.Xrm.new_criminallawattorneycasedata)data;
                    string charges = criData.new_doyouhavecurrentchargesagainstyou.Value ? HAVE : HAVE_NO;
                    string afford = criData.new_canyouaffordanattorney.Value ? CAN : CANT;
                    string description = string.Format(DESCRIPTION_TEMPLATE, charges, afford, descriptionFromRequest);
                    return description;
                }
                else
                {
                    return descriptionFromRequest;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Failed to create description.");
                return descriptionFromRequest;
            }
        }

        protected override Microsoft.Xrm.Client.CrmEntity CreateAndAddCaseDataToIncident(incident incident, NoProblem.Core.DataModel.Consumer.ServiceRequestTwoStepWaitingData reqData)
        {
            DataModel.Consumer.ServiceRequestCriminalLawAttorneyData criReqData = (DataModel.Consumer.ServiceRequestCriminalLawAttorneyData)reqData;
            DataModel.Xrm.new_criminallawattorneycasedata data = null;
            if (reqData != null)
            {
                data = CreateCaseDataEntry(criReqData);
                incident.new_criminallawattorneycasedataid = data.new_criminallawattorneycasedataid;
            }
            //else
            //{
                //data = CreateDefaultCaseDataEntry();
            //}
            //incident.new_criminallawattorneycasedataid = data.new_criminallawattorneycasedataid;
            return data;
        }

        protected override NoProblem.Core.DataModel.Consumer.ServiceRequestTwoStepWaitingData GetRequestData(DataModel.ServiceRequest servRequest)
        {
            return servRequest.CriminalLawAttorneyData;
        }

        //private DataModel.Xrm.new_criminallawattorneycasedata CreateDefaultCaseDataEntry()
        //{
        //    new_criminallawattorneycasedata data = new new_criminallawattorneycasedata();
        //    int _rnd = new Random().Next(2);
        //    switch (_rnd)
        //    {
        //        case (0):
        //            data.new_canyouaffordanattorney = true;
        //            data.new_doyouhavecurrentchargesagainstyou = true;
        //            break;
        //        default:
        //            data.new_canyouaffordanattorney = true;
        //            data.new_doyouhavecurrentchargesagainstyou = false;
        //            break;

        //    }
        //    DataAccessLayer.CriminalLawAttorneyCaseData dal = new DataAccessLayer.CriminalLawAttorneyCaseData(XrmDataContext);
        //    dal.Create(data);
        //    XrmDataContext.SaveChanges();
        //    return data;
        //}

        private new_criminallawattorneycasedata CreateCaseDataEntry(NoProblem.Core.DataModel.Consumer.ServiceRequestCriminalLawAttorneyData reqData)
        {
            new_criminallawattorneycasedata data = new new_criminallawattorneycasedata();
            data.new_doyouhavecurrentchargesagainstyou = reqData.CurrentCharges;
            data.new_canyouaffordanattorney = reqData.AffordAttorney;
            DataAccessLayer.CriminalLawAttorneyCaseData dal = new NoProblem.Core.DataAccessLayer.CriminalLawAttorneyCaseData(XrmDataContext);
            dal.Create(data);
            XrmDataContext.SaveChanges();
            return data;
        }
    }
}
