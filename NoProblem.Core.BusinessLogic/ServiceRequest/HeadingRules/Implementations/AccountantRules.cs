﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class AccountantRules : HeadingRuleBase
    {
        private const string NO_DESCRIPTION = "I am looking for {0}.";
        private const string WITH_DESCRIPTION = " {1}. I need an accountant for {0}.";
        private const string DEFAULT_DESCRIPTION = "I need an accountant";

        public AccountantRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        
        public override bool ApplyRules(IServiceRequest request, IServiceResponse response, DML.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            var servRequest = (DataModel.ServiceRequest)request;
            if (!string.IsNullOrEmpty(servRequest.SubType))
            {
                string desc;
                if (servRequest.RequestDescription.Equals(DEFAULT_DESCRIPTION, StringComparison.InvariantCultureIgnoreCase))
                {
                    desc = string.Format(NO_DESCRIPTION, servRequest.SubType);
                }
                else
                {
                    desc = string.Format(WITH_DESCRIPTION, servRequest.SubType, servRequest.RequestDescription);
                }
                incident.new_webdescription = desc;
            }
            RestoreSubType(isManualUpsale, lastIncidentId, incident);
            return true;
        }

    }
}
