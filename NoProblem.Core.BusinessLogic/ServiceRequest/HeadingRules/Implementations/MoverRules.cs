﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Consumer;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class MoverRules : HeadingRuleBase
    {
        private const string I_NEED_TO_MOVE_A = "I need to move a";
        private const string FROM_ZIP_CODE = "from zip code ";
        private const string TO_ZIP_CODE = " to zip code ";
        private const string IN = " in ";

        public MoverRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        #region IHeadingRule Members

        public override bool ApplyRules(IServiceRequest request, IServiceResponse response, DML.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            var moveData = ((DataModel.ServiceRequest)request).MoverData;
            if (moveData != null)
            {
                if (moveData.MoveDate.HasValue)
                {
                    incident.new_movedate = moveData.MoveDate.Value;
                }
                if (moveData.MoveSize.HasValue)
                {
                    incident.new_movesize = moveData.MoveSize.Value.ToString();
                }
                if (moveData.MoveType.HasValue)
                {
                    incident.new_movetype = moveData.MoveType.Value.ToString();
                    if (moveData.MoveType.Value != ServiceRequestMoverData.eMoveType.residential
                        && moveData.MoveType.Value != ServiceRequestMoverData.eMoveType.office)
                    {
                        incident.new_isresearch = true;
                        if (response.Status == StatusCode.Success)
                        {
                            response.Status = StatusCode.NoSuppliers;
                            incident.statuscode = (int)DataModel.Xrm.incident.Status.WORK_DONE;
                            incident.new_dialerstatus = (int)DataModel.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
                        }
                    }
                }
                incident.new_movetozipcode = moveData.MoveToZipCode;
                if (!string.IsNullOrEmpty(moveData.MoveToZipCode)
                    && moveData.MoveDate.HasValue
                    && moveData.MoveSize.HasValue
                    && moveData.MoveType.HasValue
                    && (
                       moveData.MoveType.Value == ServiceRequestMoverData.eMoveType.residential
                        || moveData.MoveType.Value == ServiceRequestMoverData.eMoveType.office
                    ))
                {
                    incident.description = BuildDescription(moveData, incident);
                    incident.new_webdescription = incident.description;
                }
            }
            else if(isManualUpsale)//move data is null and is manual upsale
            {
                DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                var lastIncident = dal.Retrieve(lastIncidentId);
                incident.new_movetype = lastIncident.new_movetype;
                incident.new_movesize = lastIncident.new_movesize;
                incident.new_movetozipcode = lastIncident.new_movetozipcode;
                incident.new_movedate = lastIncident.new_movedate;
                RestoreSubType(lastIncident, incident);
            }
            return true;
        }

        #endregion

        #region Private methods

        private string BuildDescription(ServiceRequestMoverData moveData, DML.Xrm.incident incident)
        {
            string fromZipcode;
            string fromCity;
            string fromState;
            FindStateName(incident.new_regionid.Value, out fromZipcode, out fromState, out fromCity);
            string toCity;
            string toState;
            FindStateName(incident.new_movetozipcode, out toState, out toCity);
            StringBuilder builder = new StringBuilder();                                
            builder.Append(I_NEED_TO_MOVE_A);
            builder.Append(GetSizeString(moveData.MoveSize.Value));
            builder.Append(GetTypeString(moveData.MoveType.Value));
            builder.Append(FROM_ZIP_CODE);
            builder.Append(GetZipCodeString(fromZipcode));
            builder.Append(IN);
            builder.Append(GetCityStateString(fromCity, fromState));
            builder.Append(TO_ZIP_CODE);
            builder.Append(GetZipCodeString(moveData.MoveToZipCode));
            builder.Append(IN);
            builder.Append(GetCityStateString(toCity, toState));
            builder.Append(IN);
            builder.Append(GetDateString(moveData.MoveDate.Value));
            return builder.ToString();
        }

        private string GetCityStateString(string city, string state)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(city).Append(" ");
            builder.Append(state).Append(",");
            return builder.ToString();
        }

        private string GetZipCodeString(string zipCode)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var c in zipCode)
            {
                builder.Append(c).Append(" ");
            }
            builder.Append(",");
            return builder.ToString();
        }

        private string GetDateString(DateTime dateTime)
        {
            return dateTime.ToString("MMMM");
        }

        private string GetSizeString(ServiceRequestMoverData.eMoveSize eMoveSize)
        {
            switch (eMoveSize)
            {
                case ServiceRequestMoverData.eMoveSize.fiveBdr:
                    return " 5 bedroom ";
                case ServiceRequestMoverData.eMoveSize.fourBdr:
                    return " 4 bedroom ";
                case ServiceRequestMoverData.eMoveSize.threeBdr:
                    return " 3 bedroom ";
                case ServiceRequestMoverData.eMoveSize.twoBdr:
                    return " 2 bedroom ";
                case ServiceRequestMoverData.eMoveSize.oneBdr:
                    return " 1 bedroom ";
                case ServiceRequestMoverData.eMoveSize.studio:
                    return " studio ";
                default:
                    return " larger than 5 bedroom ";
            }
        }

        private string GetTypeString(ServiceRequestMoverData.eMoveType eMoveType)
        {
            if (eMoveType == ServiceRequestMoverData.eMoveType.office)
                return "office, ";
            else
                return "apartment, ";
        }

        private void FindStateName(Guid regionId, out string zipCode, out string state, out string city)
        {
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var region = dal.Retrieve(regionId);
            zipCode = region.new_name;
            FindStateName(region, out state, out city);
        }

        private void FindStateName(string zipCode, out string state, out string city)
        {
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var region = dal.GetRegionByName(zipCode);
            FindStateName(region, out state, out city);
        }

        private void FindStateName(DataModel.Xrm.new_region region, out string state, out string city)
        {
            state = string.Empty;
            city = string.Empty;
            if (region != null)
            {
                string[] split = region.new_englishname.Split(';');
                if (split.Length >= 4)
                {
                    state = split[3];
                    city = split[1];
                }
            }
        }

        #endregion
    }
}
