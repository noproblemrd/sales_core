﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Consumer;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class ComputerRepairRules : HeadingRuleBase
    {
        private const string DESCRIPTION = "I need a computer repair technician for {0}";

        public ComputerRepairRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public override bool ApplyRules(IServiceRequest request, IServiceResponse response, DML.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            var servRequest = (DataModel.ServiceRequest)request;
            if (!string.IsNullOrEmpty(servRequest.SubType))
            {                
                incident.description = string.Format(DESCRIPTION, servRequest.SubType);
                incident.new_webdescription = incident.description;
            }
            RestoreSubType(isManualUpsale, lastIncidentId, incident);
            return true;
        }
    }
}
