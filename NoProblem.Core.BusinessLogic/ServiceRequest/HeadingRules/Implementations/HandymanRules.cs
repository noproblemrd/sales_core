﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class HandymanRules : HeadingRuleBase
    {
        private const string DESCRIPTION = "I need a handyman for {0}";

        public HandymanRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        #region IHeadingRule Members

        public override bool ApplyRules(NoProblem.Core.DataModel.Consumer.Interfaces.IServiceRequest request, NoProblem.Core.DataModel.Consumer.Interfaces.IServiceResponse response, NoProblem.Core.DataModel.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            var servRequest = (DataModel.ServiceRequest)request;
            if (!string.IsNullOrEmpty(servRequest.SubType))
            {
                incident.description = string.Format(DESCRIPTION, servRequest.SubType);
                incident.new_webdescription = incident.description;
            }
            RestoreSubType(isManualUpsale, lastIncidentId, incident);
            return true;
        }

        #endregion
    }
}
