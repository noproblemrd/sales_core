﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    class HomeSecurityRules : HeadingRuleBase
    {
        public HomeSecurityRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public override bool ApplyRules(DataModel.Consumer.Interfaces.IServiceRequest request, DataModel.Consumer.Interfaces.IServiceResponse response, DataModel.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            var data = ((DataModel.ServiceRequest)request).HomeSecurityData;
            if (data != null)
            {
                DataModel.Xrm.new_homesecuritycasedata caseData = new DataModel.Xrm.new_homesecuritycasedata();
                if (data.CreditRanking.HasValue)
                {
                    caseData.new_creditranking = (int)data.CreditRanking.Value;
                }
                if (data.OwnProperty.HasValue)
                {
                    caseData.new_ownproperty = data.OwnProperty.Value;
                }
                if (data.PropertyType.HasValue)
                {
                    caseData.new_propertytype = (int)data.PropertyType.Value;
                }
                if (data.TimeFrame.HasValue)
                {
                    caseData.new_timeframe = (int)data.TimeFrame.Value;
                }
                DataAccessLayer.HomeSecurityCaseData dal = new DataAccessLayer.HomeSecurityCaseData(XrmDataContext);
                dal.Create(caseData);
                XrmDataContext.SaveChanges();
                incident.new_homesecuritycasedataid = caseData.new_homesecuritycasedataid;
            }
            else if (isManualUpsale)//move data is null and is manual upsale
            {
                DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                var lastIncident = dal.Retrieve(lastIncidentId);
                if (lastIncident.new_homesecuritycasedataid.HasValue)
                {
                    incident.new_homesecuritycasedataid = lastIncident.new_homesecuritycasedataid.Value;
                }
                RestoreSubType(lastIncident, incident);
            }
            return true;
        }
    }
}
