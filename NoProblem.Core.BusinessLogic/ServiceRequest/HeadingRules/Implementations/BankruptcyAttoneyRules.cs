﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;
using NoProblem.Core.DataModel.Consumer;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class BankruptcyAttoneyRules : TwoStepWaitingRulesBase
    {
        public BankruptcyAttoneyRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private const string CONSIDERING = "I am considering bankruptcy because of {0}. ";
        private const string BILLS = "I have bills from {0}. ";
        private const string EXPENSES = "My estimate total monthly expenses are {0}. ";
        private const string BEHIND_SINGLE = "I am behind on my {0} payments. ";
        private const string BEHIND_DOUBLE = "I am behind on my real-estate and automobile payments. ";
        private const string INCOME_TYPE = "I {0}. ";
        private const string INCOME = "My estimate total monthly income is {0}. ";

        protected override Microsoft.Xrm.Client.CrmEntity HandleManualUpsale(incident incident, incident lastIncident)
        {
            new_bankruptcyattorneycasedata data = null;
            if (lastIncident.new_bankruptcyattorneycasedataid.HasValue)
            {
                incident.new_bankruptcyattorneycasedataid = lastIncident.new_bankruptcyattorneycasedataid.Value;
                data = lastIncident.new_bankruptcyattorneycasedata_incident;
            }
            //else
            //{
            //    data = CreateDefaultCaseDataEntry();
            //    incident.new_bankruptcyattorneycasedataid = data.new_bankruptcyattorneycasedataid;
            //}
            return data;
        }

        protected override string CreateDescription(string descriptionFromRequest, Microsoft.Xrm.Client.CrmEntity data)
        {
            try
            {
                StringBuilder descriptionBuilder = new StringBuilder(descriptionFromRequest);
                if (data != null)
                {                    
                    descriptionBuilder.Append(". ");
                    DataModel.Xrm.new_bankruptcyattorneycasedata bankData = (DataModel.Xrm.new_bankruptcyattorneycasedata)data;
                    AddConsideringToDescription(descriptionBuilder, bankData);
                    AddBillsToDescription(descriptionBuilder, bankData);
                    AddBehindToDescription(descriptionBuilder, bankData);
                    AddExpensesToDescription(descriptionBuilder, bankData);
                    AddTypesOfIncomeToDescription(descriptionBuilder, bankData);
                    AddIncomeToDescription(descriptionBuilder, bankData);                    
                }
                return descriptionBuilder.ToString();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Failed to create description.");
                return descriptionFromRequest;
            }
        }

        private void AddTypesOfIncomeToDescription(StringBuilder descriptionBuilder, new_bankruptcyattorneycasedata bankData)
        {
            if (bankData.new_typesofincome != (int)DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.Other)
            {
                string str = null;
                switch ((DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy)bankData.new_typesofincome)
                {
                    case new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.EmployedFullTime:
                        str = "am employed full-time";
                        break;
                    case new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.EmployedPartTime:
                        str = "am employed part-time";
                        break;
                    case new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.Maintenance:
                        str = "get maintenance";
                        break;
                    case new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.NoIncome:
                        str = "have no income";
                        break;
                    case new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.Pension:
                        str = "get a pension";
                        break;
                    case new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.RetirementChildSupport:
                        str = "get a retirement child support";
                        break;
                    case new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.SocialSecurity:
                        str = "am on social security";
                        break;
                }
                descriptionBuilder.Append(string.Format(INCOME_TYPE, str));
            }
        }

        private void AddBehindToDescription(StringBuilder descriptionBuilder, new_bankruptcyattorneycasedata bankData)
        {
            if (bankData.new_behindinrealestatepayments.Value && bankData.new_behindinautomobilepayments.Value)
            {
                descriptionBuilder.Append(BEHIND_DOUBLE);
            }
            else if (bankData.new_behindinrealestatepayments.Value)
            {
                descriptionBuilder.Append(string.Format(BEHIND_SINGLE, "real-estate"));
            }
            else if (bankData.new_behindinautomobilepayments.Value)
            {
                descriptionBuilder.Append(string.Format(BEHIND_SINGLE, "automobile"));
            }
        }

        private void AddExpensesToDescription(StringBuilder descriptionBuilder, new_bankruptcyattorneycasedata bankData)
        {
            string money = GetMoneyString(bankData.new_estimatetotalmonthlyexpenses);
            descriptionBuilder.Append(string.Format(EXPENSES, money));
        }

        private void AddIncomeToDescription(StringBuilder descriptionBuilder, new_bankruptcyattorneycasedata bankData)
        {
            string money = GetMoneyString(bankData.new_estimatetotalmonthlyincome);
            descriptionBuilder.Append(string.Format(INCOME, money));
        }

        private string GetMoneyString(int? money)
        {
            string retVal = null;
            switch ((DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy)money)
            {
                case new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.LessThan3K:
                    retVal = "less than $3,000";
                    break;
                case new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.MoreThan15K:
                    retVal = "more than $15,000";
                    break;
                case new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.SixKTo10K:
                    retVal = "$6,000 to $10,000";
                    break;
                case new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.TenKTo15K:
                    retVal = "$10,000 to $15,000";
                    break;
                case new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.ThreeKTo6K:
                    retVal = "$3,000 to $6,000";
                    break;
            }
            return retVal;
        }

        private void AddBillsToDescription(StringBuilder descriptionBuilder, DataModel.Xrm.new_bankruptcyattorneycasedata bankData)
        {
            if (bankData.new_whichbillsdoyouhave != (int)DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.Other)
            {
                string billsStr = null;
                switch ((DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy)bankData.new_whichbillsdoyouhave)
                {
                    case new_bankruptcyattorneycasedata.BillsBankruptcy.AutoLoansIncomeTaxes:
                        billsStr = "auto loans income taxes";
                        break;
                    case new_bankruptcyattorneycasedata.BillsBankruptcy.ChildSupport:
                        billsStr = "child support";
                        break;
                    case new_bankruptcyattorneycasedata.BillsBankruptcy.CreditCards:
                    case new_bankruptcyattorneycasedata.BillsBankruptcy.StoreCards:
                        billsStr = "credit cards and store cards";
                        break;
                    case new_bankruptcyattorneycasedata.BillsBankruptcy.MedicalBills:
                        billsStr = "medical bills";
                        break;
                    case new_bankruptcyattorneycasedata.BillsBankruptcy.PaydayLoans:
                        billsStr = "payday loans";
                        break;
                    case new_bankruptcyattorneycasedata.BillsBankruptcy.PersonalLoans:
                        billsStr = "personal loans";
                        break;
                    case new_bankruptcyattorneycasedata.BillsBankruptcy.StudentLoans:
                        billsStr = "student loans";
                        break;
                }
                descriptionBuilder.Append(string.Format(BILLS, billsStr));
            }
        }

        private void AddConsideringToDescription(StringBuilder descriptionBuilder, DataModel.Xrm.new_bankruptcyattorneycasedata bankData)
        {
            if (bankData.new_whyconsideringbankruptcy != (int)DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Other)
            {
                string considerStr = null;
                switch ((DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy)bankData.new_whyconsideringbankruptcy)
                {
                    case new_bankruptcyattorneycasedata.ConsideringBankruptcy.CreditorHarassment:
                        considerStr = "creditor harassment";
                        break;
                    case new_bankruptcyattorneycasedata.ConsideringBankruptcy.Disability:
                        considerStr = "disability";
                        break;
                    case new_bankruptcyattorneycasedata.ConsideringBankruptcy.Divorce:
                        considerStr = "divorce";
                        break;
                    case new_bankruptcyattorneycasedata.ConsideringBankruptcy.Foreclosure:
                        considerStr = "foreclosure";
                        break;
                    case new_bankruptcyattorneycasedata.ConsideringBankruptcy.Garnishment:
                        considerStr = "garnishment";
                        break;
                    case new_bankruptcyattorneycasedata.ConsideringBankruptcy.Illness:
                        considerStr = "illness";
                        break;
                    case new_bankruptcyattorneycasedata.ConsideringBankruptcy.Lawsuits:
                        considerStr = "lawsuits";
                        break;
                    case new_bankruptcyattorneycasedata.ConsideringBankruptcy.LicenseSuspension:
                        considerStr = "license suspension";
                        break;
                    case new_bankruptcyattorneycasedata.ConsideringBankruptcy.LossOfIncome:
                        considerStr = "loss of income";
                        break;
                    case new_bankruptcyattorneycasedata.ConsideringBankruptcy.Repossession:
                        considerStr = "repossession";
                        break;
                }
                descriptionBuilder.Append(string.Format(CONSIDERING, considerStr));
            }
        }

        protected override Microsoft.Xrm.Client.CrmEntity CreateAndAddCaseDataToIncident(incident incident, ServiceRequestTwoStepWaitingData reqData)
        {
            DataModel.Consumer.ServiceRequestBankruptcyAttorneyData bankReqData = (DataModel.Consumer.ServiceRequestBankruptcyAttorneyData)reqData;
            new_bankruptcyattorneycasedata data = null;
            if (reqData != null)
            {
                data = CreateCaseDataEntry(bankReqData);
                incident.new_bankruptcyattorneycasedataid = data.new_bankruptcyattorneycasedataid;
            }
            //else
            //{
            //    data = CreateDefaultCaseDataEntry();
            //}
            //incident.new_bankruptcyattorneycasedataid = data.new_bankruptcyattorneycasedataid;
            return data;
        }

        protected override ServiceRequestTwoStepWaitingData GetRequestData(DataModel.ServiceRequest servRequest)
        {
            return servRequest.BankruptcyAttorneyData;
        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleTimeOutRequest(incident incident)
        {
            //DataModel.Xrm.new_bankruptcyattorneycasedata data = CreateDefaultCaseDataEntry();
            //incident.new_bankruptcyattorneycasedataid = data.new_bankruptcyattorneycasedataid;
            //return data;
            return null;
        }

        //private new_bankruptcyattorneycasedata CreateDefaultCaseDataEntry()
        //{
        //    new_bankruptcyattorneycasedata data = new new_bankruptcyattorneycasedata();
        //     int _rnd = new Random().Next(6);
        //     switch (_rnd)
        //     {
        //         case (0):
        //             data.new_whyconsideringbankruptcy = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Other;
        //             data.new_whichbillsdoyouhave = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.Other;
        //             data.new_estimatetotalmonthlyexpenses = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.LessThan3K;
        //             data.new_behindinrealestatepayments = true;
        //             data.new_behindinautomobilepayments = true;
        //             data.new_haveadditionalassets = false;
        //             data.new_typesofincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.Other;
        //             data.new_estimatetotalmonthlyincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.LessThan3K;
        //             data.new_canyouaffordanattorney = true;
        //             break;
        //         case (1):
        //             data.new_whyconsideringbankruptcy = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.LossOfIncome;
        //             data.new_whichbillsdoyouhave = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.CreditCards;
        //             data.new_estimatetotalmonthlyexpenses = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.ThreeKTo6K;
        //             data.new_behindinrealestatepayments = true;
        //             data.new_behindinautomobilepayments = false;
        //             data.new_haveadditionalassets = true;
        //             data.new_typesofincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.EmployedPartTime;
        //             data.new_estimatetotalmonthlyincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.ThreeKTo6K;
        //             data.new_canyouaffordanattorney = true;
        //             break;
        //         case (2):
        //             data.new_whyconsideringbankruptcy = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Divorce;
        //             data.new_whichbillsdoyouhave = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.PersonalLoans;
        //             data.new_estimatetotalmonthlyexpenses = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.ThreeKTo6K;
        //             data.new_behindinrealestatepayments = true;
        //             data.new_behindinautomobilepayments = false;
        //             data.new_haveadditionalassets = true;
        //             data.new_typesofincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.EmployedPartTime;
        //             data.new_estimatetotalmonthlyincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.SixKTo10K;
        //             data.new_canyouaffordanattorney = true;
        //             break;
        //         case (3):
        //             data.new_whyconsideringbankruptcy = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Foreclosure;
        //             data.new_whichbillsdoyouhave = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.PaydayLoans;
        //             data.new_estimatetotalmonthlyexpenses = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.ThreeKTo6K;
        //             data.new_behindinrealestatepayments = false;
        //             data.new_behindinautomobilepayments = false;
        //             data.new_haveadditionalassets = false;
        //             data.new_typesofincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.Other;
        //             data.new_estimatetotalmonthlyincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.LessThan3K;
        //             data.new_canyouaffordanattorney = true;
        //             break;
        //         case (4):
        //             data.new_whyconsideringbankruptcy = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.LossOfIncome;
        //             data.new_whichbillsdoyouhave = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.CreditCards;
        //             data.new_estimatetotalmonthlyexpenses = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.SixKTo10K;
        //             data.new_behindinrealestatepayments = true;
        //             data.new_behindinautomobilepayments = true;
        //             data.new_haveadditionalassets = false;
        //             data.new_typesofincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.Other;
        //             data.new_estimatetotalmonthlyincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.LessThan3K;
        //             data.new_canyouaffordanattorney = true;
        //             break;
        //         default:
        //             data.new_whyconsideringbankruptcy = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.LossOfIncome;
        //             data.new_whichbillsdoyouhave = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.PersonalLoans;
        //             data.new_estimatetotalmonthlyexpenses = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.LessThan3K;
        //             data.new_behindinrealestatepayments = true;
        //             data.new_behindinautomobilepayments = false;
        //             data.new_haveadditionalassets = true;
        //             data.new_typesofincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.EmployedPartTime;
        //             data.new_estimatetotalmonthlyincome = (int)DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.ThreeKTo6K;
        //             data.new_canyouaffordanattorney = true;
        //             break;

        //     }

        //    DataAccessLayer.BankruptcyAttorneyCaseData dal = new NoProblem.Core.DataAccessLayer.BankruptcyAttorneyCaseData(XrmDataContext);
        //    dal.Create(data);
        //    XrmDataContext.SaveChanges();
        //    return data;
        //}

        private new_bankruptcyattorneycasedata CreateCaseDataEntry(ServiceRequestBankruptcyAttorneyData reqData)
        {
            new_bankruptcyattorneycasedata data = new new_bankruptcyattorneycasedata();
            data.new_whyconsideringbankruptcy = (int)reqData.Considering;
            data.new_whichbillsdoyouhave = (int)reqData.Bills;
            data.new_estimatetotalmonthlyexpenses = (int)reqData.Expenses;
            data.new_behindinrealestatepayments = reqData.BehindRealEstate;
            data.new_behindinautomobilepayments = reqData.BehindAutomobile;
            data.new_haveadditionalassets = reqData.Assets;
            data.new_typesofincome = (int)reqData.IncomeTypes;
            data.new_estimatetotalmonthlyincome = (int)reqData.Income;
            data.new_canyouaffordanattorney = reqData.AffordAttorney;
            DataAccessLayer.BankruptcyAttorneyCaseData dal = new NoProblem.Core.DataAccessLayer.BankruptcyAttorneyCaseData(XrmDataContext);
            dal.Create(data);
            XrmDataContext.SaveChanges();
            return data;
        }
    }
}
