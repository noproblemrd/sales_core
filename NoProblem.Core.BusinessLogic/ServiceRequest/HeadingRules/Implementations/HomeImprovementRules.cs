﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class HomeImprovementRules : HeadingRuleBase
    {
        private const string DEFAULT_DESCRIPTION = "I need {0}: {1}.\r\nMy budget is: {2}.";
        public HomeImprovementRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public override bool ApplyRules(IServiceRequest request, IServiceResponse response, DML.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            var pe = dal.Retrieve(incident.new_primaryexpertiseid.Value);
            DataModel.Consumer.eBudget budget = NoProblem.Core.DataModel.Consumer.eBudget.From2500To5000;
            if (incident.new_budget.HasValue)
            {
                budget = (NoProblem.Core.DataModel.Consumer.eBudget)incident.new_budget.Value;
            }
            string catname = pe.new_ivrname;
            if (string.IsNullOrEmpty(catname))
            {
                catname = "a service provider";
            }
            incident.description = string.Format(DEFAULT_DESCRIPTION, catname.ToLower(), request.RequestDescription, budget.ToString());
            incident.new_webdescription = incident.description;
            RestoreSubType(isManualUpsale, lastIncidentId, incident);
            return true;
        }

    }
}
