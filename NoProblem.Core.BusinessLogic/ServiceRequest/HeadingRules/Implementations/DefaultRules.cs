﻿using System;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class DefaultRules : HeadingRuleBase
    {
        public DefaultRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
    }
}
