﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    class SocialSecurityDisabilityRules : TwoStepWaitingRulesBase
    {
        public SocialSecurityDisabilityRules(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {

        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleManualUpsale(DataModel.Xrm.incident incident, DataModel.Xrm.incident lastIncident)
        {
            DataModel.Xrm.new_socialsecuritydisabilitycasedata data = null;
            if (lastIncident.new_socialsecuritydisabilitydataid.HasValue)
            {
                incident.new_socialsecuritydisabilitydataid = lastIncident.new_socialsecuritydisabilitydataid.Value;
                data = lastIncident.new_socialsecuritydisabilitycasedata_incident;
            }
            return data;
        }

        protected override string CreateDescription(string descriptionFromRequest, Microsoft.Xrm.Client.CrmEntity data)
        {
            return descriptionFromRequest;
        }

        protected override Microsoft.Xrm.Client.CrmEntity CreateAndAddCaseDataToIncident(DataModel.Xrm.incident incident, DataModel.Consumer.ServiceRequestTwoStepWaitingData reqData)
        {
            DataModel.Consumer.ServiceRequestSocialSecurityDisabilityData pdReqData = (DataModel.Consumer.ServiceRequestSocialSecurityDisabilityData)reqData;
            DataModel.Xrm.new_socialsecuritydisabilitycasedata data = null;
            if (reqData != null)
            {
                data = CreateCaseDataEntry(pdReqData);
                incident.new_socialsecuritydisabilitydataid = data.new_socialsecuritydisabilitycasedataid;
            }
            return data;
        }

        protected override DataModel.Consumer.ServiceRequestTwoStepWaitingData GetRequestData(DataModel.ServiceRequest servRequest)
        {
            return servRequest.SocialSecurityDisabilityData;
        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleTimeOutRequest(DataModel.Xrm.incident incident)
        {
            return null;
        }

        private DataModel.Xrm.new_socialsecuritydisabilitycasedata CreateCaseDataEntry(DataModel.Consumer.ServiceRequestSocialSecurityDisabilityData reqData)
        {
            DataModel.Xrm.new_socialsecuritydisabilitycasedata data = new DataModel.Xrm.new_socialsecuritydisabilitycasedata();
            data.new_abletowork = reqData.AbleToWork;
            data.new_alreadyreceivebenefits = reqData.AlreadyReceiveBenefits;
            data.new_haveattorney = reqData.HaveAttorney;
            data.new_misswork = reqData.MissWork;
            data.new_hasdoctor = reqData.HasDoctor;
            data.new_prescribedmedication = reqData.PrescribedMedication;
            data.new_work5of10 = reqData.Work5Of10;
            DataAccessLayer.SocialSecurityDisabilityCaseData dal
                = new DataAccessLayer.SocialSecurityDisabilityCaseData(XrmDataContext);
            dal.Create(data);
            XrmDataContext.SaveChanges();
            return data;
        }
    }
}
