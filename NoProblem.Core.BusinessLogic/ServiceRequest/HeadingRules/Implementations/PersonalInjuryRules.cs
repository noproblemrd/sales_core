﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class PersonalInjuryRules : TwoStepWaitingRulesBase
    {
        public PersonalInjuryRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private const string defaultDescription = "I need a personal injury attorney";
        private const string descriptionBills = "Medical bills estimation is {0}. ";
        private const string descriptionResult = "The accident resulted in {0}. ";
        private const string descriptionInjuries = "My injuries are {0}. ";

        protected override string CreateDescription(string descriptionFromRequest, Microsoft.Xrm.Client.CrmEntity data)
        {
            if (data == null)
            {
                return descriptionFromRequest;
            }
            DataModel.Xrm.new_personalinjurycasedata pidata = (DataModel.Xrm.new_personalinjurycasedata)data;
            StringBuilder builder = new StringBuilder();
            if (!string.IsNullOrEmpty(descriptionFromRequest) && descriptionFromRequest != defaultDescription)
            {
                builder.Append(descriptionFromRequest).Append(". ");
            }
            if (data != null)
            {
                if (pidata.new_medicalbills.HasValue && pidata.new_medicalbills.Value != (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.noMedicalBills)
                {
                    string billsStr = null;
                    switch ((DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills)pidata.new_medicalbills.Value)
                    {
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.dontKnow:
                            billsStr = "unknown";
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e10kTo25k:
                            billsStr = "10,000 to 25,000 dollars";
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e1kTo5k:
                            billsStr = "1,000 to 5,000 dollars";
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e25kTo100k:
                            billsStr = "25,000 to 100,000 dollars";
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e5kTo10k:
                            billsStr = "5,000 to 10,000 dollars";
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.lessThan1k:
                            billsStr = "less than 1,000 dollars";
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.moreThan100k:
                            billsStr = "more than 100,000 dollars";
                            break;
                    }
                    if (!string.IsNullOrEmpty(billsStr))
                    {
                        builder.Append(string.Format(descriptionBills, billsStr));
                    }
                }
                if (pidata.new_accidentresult.HasValue && pidata.new_accidentresult.Value != (int)DataModel.Xrm.new_personalinjurycasedata.AccidentResult.noneOfTheAbove)
                {
                    string resultStr = null;
                    switch ((DataModel.Xrm.new_personalinjurycasedata.AccidentResult)pidata.new_accidentresult.Value)
                    {
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.hospitalization:
                            resultStr = "hospitalization";
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.medicalTreatment:
                            resultStr = "medical treatment";
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.missedWork:
                            resultStr = "missed work days";
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.surgery:
                            resultStr = "surgery";
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.wrongfulDeath:
                            resultStr = "wrongful death";
                            break;
                    }
                    if (!string.IsNullOrEmpty(resultStr))
                    {
                        builder.Append(string.Format(descriptionResult, resultStr));
                    }
                }
                List<string> lstOfInjuries = new List<string>();
                if (pidata.new_braininjury.HasValue && pidata.new_braininjury.Value)
                {
                    lstOfInjuries.Add("brain injury");
                }
                if (pidata.new_brokenbones.HasValue && pidata.new_brokenbones.Value)
                {
                    lstOfInjuries.Add("broken bones");
                }
                if (pidata.new_lossoflife.HasValue && pidata.new_lossoflife.Value)
                {
                    lstOfInjuries.Add("loss of life");
                }
                if (pidata.new_spinalcordinjuryorparalysis.HasValue && pidata.new_spinalcordinjuryorparalysis.Value)
                {
                    lstOfInjuries.Add("spinal cord injury or paralysis");
                }
                if (pidata.new_whiplash.HasValue && pidata.new_whiplash.Value)
                {
                    lstOfInjuries.Add("whiplash");
                }
                if (pidata.new_lostlimb.HasValue && pidata.new_lostlimb.Value)
                {
                    lstOfInjuries.Add("lost limb");
                }
                if (pidata.new_other.HasValue && pidata.new_other.Value)
                {
                    lstOfInjuries.Add("different injuries");
                }
                if (lstOfInjuries.Count > 0)
                {
                    StringBuilder injuriesBuilder = new StringBuilder();
                    for (int i = 0; i < lstOfInjuries.Count; i++)
                    {
                        bool isLast = i == (lstOfInjuries.Count - 1);
                        if (isLast && lstOfInjuries.Count > 1)
                        {
                            injuriesBuilder.Append("and ");
                        }
                        injuriesBuilder.Append(lstOfInjuries[i]);
                        if (!isLast)
                        {
                            injuriesBuilder.Append(", ");
                        }
                    }
                    builder.Append(string.Format(descriptionInjuries, injuriesBuilder.ToString()));
                }
            }
            else if (String.IsNullOrEmpty(builder.ToString()))
            {
                builder.Append(defaultDescription);
            }
            return builder.ToString();
        }

        protected override Microsoft.Xrm.Client.CrmEntity CreateAndAddCaseDataToIncident(DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Consumer.ServiceRequestTwoStepWaitingData reqData)
        {
            DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData piReqData = (DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData)reqData;
            DataModel.Xrm.new_personalinjurycasedata data = null;
            if (reqData != null)
            {
                data = CreateCaseDataEntry(piReqData);
                incident.new_personalinjurycasedataid = data.new_personalinjurycasedataid;
            }
            //else
            //{
            //    data = CreateDefaultCaseDataEntry();
            //}
            //incident.new_personalinjurycasedataid = data.new_personalinjurycasedataid;
            return data;
        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleManualUpsale(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Xrm.incident lastIncident)
        {
            DataModel.Xrm.new_personalinjurycasedata data = null;
            if (lastIncident.new_personalinjurycasedataid.HasValue)
            {
                incident.new_personalinjurycasedataid = lastIncident.new_personalinjurycasedataid.Value;
                data = lastIncident.new_personalinjurycasedata_incident;
            }
            //else
            //{
            //    data = CreateDefaultCaseDataEntry();
            //    incident.new_personalinjurycasedataid = data.new_personalinjurycasedataid;
            //}
            return data;
        }

        protected override NoProblem.Core.DataModel.Consumer.ServiceRequestTwoStepWaitingData GetRequestData(NoProblem.Core.DataModel.ServiceRequest servRequest)
        {
            return servRequest.PersonalInjuryAttorneyData;
        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleTimeOutRequest(NoProblem.Core.DataModel.Xrm.incident incident)
        {
            //DataModel.Xrm.new_personalinjurycasedata data = CreateDefaultCaseDataEntry();
            //incident.new_personalinjurycasedataid = data.new_personalinjurycasedataid;
            //return data;
            return null;
        }

        //private DataModel.Xrm.new_personalinjurycasedata CreateDefaultCaseDataEntry()
        //{
        //    DataModel.Xrm.new_personalinjurycasedata data = new NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata();
        //    data.new_accidentresult = (int)DataModel.Xrm.new_personalinjurycasedata.AccidentResult.medicalTreatment;
        //    data.new_currentlyrepresented = false;
        //    data.new_doesanyonehasinsurance = (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.Yes;
        //    data.new_medicalbills = (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.dontKnow;
        //    data.new_other = true;
        //    DataAccessLayer.PersonalInjuryCaseData dal = new NoProblem.Core.DataAccessLayer.PersonalInjuryCaseData(XrmDataContext);
        //    dal.Create(data);
        //    XrmDataContext.SaveChanges();
        //    return data;
        //}

        private DataModel.Xrm.new_personalinjurycasedata CreateCaseDataEntry(NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData reqData)
        {
            DataModel.Xrm.new_personalinjurycasedata data = new NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata();
            data.new_accidentresult = (int)reqData.AccidentResult;
            data.new_braininjury = reqData.BrainInjury;
            data.new_brokenbones = reqData.BrokenBones;
            data.new_currentlyrepresented = reqData.CurrentlyRepresented;
            data.new_isyourfault = reqData.IsYourFault;
            data.new_doesanyonehasinsurance = (int)reqData.DoesAnyoneHasVehicleInsurance;
            data.new_medicalbills = (int)reqData.EstimatedMedicalBills;
            data.new_lossoflife = reqData.LossOfLife;
            data.new_lostlimb = reqData.LostLimb;
            data.new_other = reqData.Other;
            data.new_spinalcordinjuryorparalysis = reqData.SpinalCordInjuryOrParalysis;
            data.new_whiplash = reqData.Whiplash;
            DataAccessLayer.PersonalInjuryCaseData dal = new NoProblem.Core.DataAccessLayer.PersonalInjuryCaseData(XrmDataContext);
            dal.Create(data);
            XrmDataContext.SaveChanges();
            return data;
        }
    }
}
