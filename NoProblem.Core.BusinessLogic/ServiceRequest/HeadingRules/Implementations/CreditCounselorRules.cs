﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class CreditCounselorRules : HeadingRuleBase
    {
        public CreditCounselorRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private const string DESCRIPTION_NO_RANGE = "I need to settle my {0} debt";
        private const string DESCRIPTION_WITH_RANGE = "I need to settle my {0} to {1} debt";
        private const string DESCRIPTION_NO_AMOUNT = "I need to settle my debts";

        #region IHeadingRule Members

        public override bool ApplyRules(IServiceRequest request, IServiceResponse response, DataModel.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            var servRequest = (DataModel.ServiceRequest)request;
            if (!string.IsNullOrEmpty(servRequest.SubType))
            {
                string[] splitted = servRequest.SubType.Split('-');
                if (splitted.Length == 2)
                {
                    incident.description = string.Format(DESCRIPTION_WITH_RANGE, splitted[0].Trim(), splitted[1].Trim());                }
                else
                {
                    incident.description = string.Format(DESCRIPTION_NO_RANGE, servRequest.SubType.ToLower());
                }
            }
            else 
            {
                incident.description = DESCRIPTION_NO_AMOUNT;
            }
            incident.new_webdescription = incident.description;
            RestoreSubType(isManualUpsale, lastIncidentId, incident);
            return true;
        }

        #endregion
    }
}
