﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class HealthInsuranceRules : TwoStepWaitingRulesBase
    {
        public HealthInsuranceRules(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {

        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleManualUpsale(DataModel.Xrm.incident incident, DataModel.Xrm.incident lastIncident)
        {
            DataModel.Xrm.new_healthinsurancecasedata data = null;
            if (lastIncident.new_healthinsurancecasedataid.HasValue)
            {
                incident.new_healthinsurancecasedataid = lastIncident.new_healthinsurancecasedataid.Value;
                data = lastIncident.new_healthinsurancecasedata_incident;
            }
            return data;
        }

        protected override string CreateDescription(string descriptionFromRequest, Microsoft.Xrm.Client.CrmEntity data)
        {
            return descriptionFromRequest;
        }

        protected override Microsoft.Xrm.Client.CrmEntity CreateAndAddCaseDataToIncident(DataModel.Xrm.incident incident, DataModel.Consumer.ServiceRequestTwoStepWaitingData reqData)
        {
            DataModel.Consumer.ServiceRequestHealthInsuranceData pdReqData = (DataModel.Consumer.ServiceRequestHealthInsuranceData)reqData;
            DataModel.Xrm.new_healthinsurancecasedata data = null;
            if (reqData != null)
            {
                data = CreateCaseDataEntry(pdReqData);
                incident.new_healthinsurancecasedataid = data.new_healthinsurancecasedataid;
            }
            return data;
        }

        protected override DataModel.Consumer.ServiceRequestTwoStepWaitingData GetRequestData(DataModel.ServiceRequest servRequest)
        {
            return servRequest.HealthInsuranceData;
        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleTimeOutRequest(DataModel.Xrm.incident incident)
        {
            return null;
        }

        private DataModel.Xrm.new_healthinsurancecasedata CreateCaseDataEntry(DataModel.Consumer.ServiceRequestHealthInsuranceData reqData)
        {
            DataModel.Xrm.new_healthinsurancecasedata data = new DataModel.Xrm.new_healthinsurancecasedata();
            data.new_issmoker = reqData.IsSmoker;
            data.new_isinsured = reqData.IsInsured;
            data.new_existingcondition = reqData.ExistingCondition.HasValue ? reqData.ExistingCondition.Value.ToString() : DataModel.Consumer.ServiceRequestHealthInsuranceData.eExistingCondition.None.ToString();
            data.new_expectantparent = reqData.ExpectantParent;
            data.new_previouslydenied = reqData.PreviouslyDenied;
            data.new_household = reqData.Household;
            data.new_income = reqData.Income;
            DataAccessLayer.HealthInsuranceCaseData dal
                = new DataAccessLayer.HealthInsuranceCaseData(XrmDataContext);
            dal.Create(data);
            XrmDataContext.SaveChanges();
            return data;
        }
    }
}
