﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using DML = NoProblem.Core.DataModel;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    [Obsolete("Don't use this. We use iFrame for payday (not two step).", true)]
    internal class PaydayLoanRules : TwoStepWaitingRulesBase
    {
        private const string DESCRIPTION_NORMAL = "I need a ${0} loan";
        //private const string DESCRIPTION_WITH_AN = "I need an {0} loan";
        //private const string EIGHT_HUNDRED_DOLLARS = "$800";

        public PaydayLoanRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        //#region IHeadingRule Members

        //public bool ApplyRules(IServiceRequest request, IServiceResponse response, DML.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId)
        //{
        //    var servRequest = (DataModel.ServiceRequest)request;
        //    if (!string.IsNullOrEmpty(servRequest.SubType))
        //    {
        //        if (servRequest.SubType == EIGHT_HUNDRED_DOLLARS)
        //        {
        //            incident.description = string.Format(DESCRIPTION_WITH_AN, servRequest.SubType);
        //        }
        //        else
        //        {
        //            incident.description = string.Format(DESCRIPTION_NORMAL, servRequest.SubType);
        //        }
        //        incident.new_webdescription = incident.description;
        //    }
        //    RestoreSubType(isManualUpsale, lastIncidentId, incident);
        //    return true;            
        //}

        //#endregion

        protected override Microsoft.Xrm.Client.CrmEntity HandleManualUpsale(DML.Xrm.incident incident, DML.Xrm.incident lastIncident)
        {
            DataModel.Xrm.new_paydayloandata data = null;
            if (lastIncident.new_paydayloandataid.HasValue)
            {
                incident.new_paydayloandataid = lastIncident.new_paydayloandataid.Value;
                data = lastIncident.new_paydayloandata_incident;
            }
            return data;
        }

        protected override string CreateDescription(string descriptionFromRequest, Microsoft.Xrm.Client.CrmEntity data)
        {
            string retVal = descriptionFromRequest;
            try
            {
                if (data != null)
                {
                    DataModel.Xrm.new_paydayloandata payDayData = (DataModel.Xrm.new_paydayloandata)data;
                    if (payDayData.new_loanamount.HasValue)
                    {
                        retVal = String.Format(DESCRIPTION_NORMAL, payDayData.new_loanamount.Value);
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Failed to create description.");
            }
            return retVal;
        }

        protected override Microsoft.Xrm.Client.CrmEntity CreateAndAddCaseDataToIncident(DML.Xrm.incident incident, DML.Consumer.ServiceRequestTwoStepWaitingData reqData)
        {
            DataModel.Consumer.ServiceRequestPayDayLoanData pdReqData = (DataModel.Consumer.ServiceRequestPayDayLoanData)reqData;
            DataModel.Xrm.new_paydayloandata data = null;
            if (reqData != null)
            {
                data = CreateCaseDataEntry(pdReqData);
                incident.new_paydayloandataid = data.new_paydayloandataid;
            }
            return data;
        }

        protected override DML.Consumer.ServiceRequestTwoStepWaitingData GetRequestData(DML.ServiceRequest servRequest)
        {
            return servRequest.PaydayLoanData;
        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleTimeOutRequest(DML.Xrm.incident incident)
        {
            return null;
        }

        private DML.Xrm.new_paydayloandata CreateCaseDataEntry(DML.Consumer.ServiceRequestPayDayLoanData reqData)
        {
            DataModel.Xrm.new_paydayloandata data = new DML.Xrm.new_paydayloandata();
            data.new_loanamount = reqData.RequestedLoanAmount;
            data.new_isrentaddress = reqData.IsRent;
            data.new_monthlynetincome = reqData.MonthlyIncome;
            if (reqData.AccountType.HasValue)
                data.new_accounttype = (int)reqData.AccountType.Value;
            data.new_directdeposit = reqData.DirectDeposit;
            if (reqData.PayPeriod.HasValue)
                data.new_payfrequency = (int)reqData.PayPeriod.Value;
            data.new_nextpayday = reqData.NextPayDate;
            data.new_secondpayday = reqData.SecondPayDate;
            data.new_legthataddress = reqData.MonthsAtResidence;
            if (reqData.IncomeType.HasValue)
                data.new_incomesource = (int)reqData.IncomeType.Value;
            data.new_activemilitary = reqData.IsActiveMilitary;
            data.new_jobtitle = reqData.Ocupation;
            data.new_employername = reqData.Employer;
            data.new_employedmonths = reqData.MonthsEmployed;
            data.new_workphone = reqData.WorkPhone;
            data.new_bankname = reqData.BankName;
            data.new_bankaccountnumber = reqData.AccountNumber;
            data.new_abarounting = reqData.RoutingNumber;
            data.new_monthswithbank = reqData.MonthsWithBank;
            data.new_driverlicense = reqData.DrivingLicenseNumber;
            data.new_driverlicensestate = reqData.DrivingLicenseState;
            data.new_ssn = reqData.SocialSecurityNumber;
            DataAccessLayer.PaydayLoanData dal = new DataAccessLayer.PaydayLoanData(XrmDataContext);
            dal.Create(data);
            XrmDataContext.SaveChanges();
            return data;
        }
    }
}
