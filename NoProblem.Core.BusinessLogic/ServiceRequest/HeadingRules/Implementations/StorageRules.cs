﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    class StorageRules : TwoStepWaitingRulesBase
    {
        public StorageRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleManualUpsale(DataModel.Xrm.incident incident, DataModel.Xrm.incident lastIncident)
        {
            new_storagecasedata data = null;
            if (lastIncident.new_storagecasedataid.HasValue)
            {
                incident.new_storagecasedataid = lastIncident.new_storagecasedataid.Value;
                data = lastIncident.new_storagecasedata_incident;
            }
            return data;
        }

        protected override string CreateDescription(string descriptionFromRequest, Microsoft.Xrm.Client.CrmEntity data)
        {
            return descriptionFromRequest;
        }

        protected override Microsoft.Xrm.Client.CrmEntity CreateAndAddCaseDataToIncident(DataModel.Xrm.incident incident, DataModel.Consumer.ServiceRequestTwoStepWaitingData reqData)
        {
            DataModel.Consumer.ServiceRequestStorageData storageReqData = (DataModel.Consumer.ServiceRequestStorageData)reqData;
            new_storagecasedata data = null;
            if (reqData != null)
            {
                data = CreateCaseDataEntry(storageReqData);
                incident.new_storagecasedataid = data.new_storagecasedataid;
            }
            return data;
        }

        private new_storagecasedata CreateCaseDataEntry(DataModel.Consumer.ServiceRequestStorageData reqData)
        {
            new_storagecasedata data = new new_storagecasedata();
            if (reqData.DeliveryStorage.HasValue)
                data.new_deliverystorage = (int)reqData.DeliveryStorage.Value;
            if (reqData.FinanceStorage.HasValue)
                data.new_financestorage = (int)reqData.FinanceStorage.Value;
            if (reqData.HowManyContainers.HasValue)
                data.new_howmanycontainers = (int)reqData.HowManyContainers.Value;
            if (reqData.HowManyContainersSpecify.HasValue)
                data.new_howmanycontainersspecify = reqData.HowManyContainersSpecify.Value;
            if (reqData.PlanningToUseStorage.HasValue)
                data.new_planningtousestorage = (int)reqData.PlanningToUseStorage.Value;
            if (reqData.StorageLength.HasValue)
                data.new_storagelength = (int)reqData.StorageLength.Value;
            if (reqData.StorageLengthSpecify.HasValue)
                data.new_storagelengthspecify = reqData.StorageLengthSpecify.Value;
            if (reqData.TypeOfStorage.HasValue)
                data.new_typeofstorage = (int)reqData.TypeOfStorage.Value;
            data.new_zipcodeother = reqData.ZipCodeOther;
            DataAccessLayer.StorageCaseData dal = new NoProblem.Core.DataAccessLayer.StorageCaseData(XrmDataContext);
            dal.Create(data);
            XrmDataContext.SaveChanges();
            return data;
        }

        protected override DataModel.Consumer.ServiceRequestTwoStepWaitingData GetRequestData(DataModel.ServiceRequest servRequest)
        {
            return servRequest.StorageData;
        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleTimeOutRequest(DataModel.Xrm.incident incident)
        {
            return null;
        }
    }
}
