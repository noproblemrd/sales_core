﻿using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Consumer;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using NoProblem.Core.DataModel.Xrm;
using System;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal abstract class TwoStepWaitingRulesBase : HeadingRuleBase
    {
        public TwoStepWaitingRulesBase(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public override bool ApplyRules(IServiceRequest request, IServiceResponse response, incident incident, bool isManualUpsale, Guid lastIncidentId)
        {
            bool continueRequest = true;
            try
            {
                var servRequest = (DataModel.ServiceRequest)request;
                ServiceRequestTwoStepWaitingData reqData = GetRequestData(servRequest);
                Microsoft.Xrm.Client.CrmEntity data = null;
                if (servRequest.Step == 1)//from slider first step
                {
                    if (reqData == null)
                    {
                        CreateCaseWaitEntry(incident);
                        continueRequest = false;
                    }
                    else//is slider (or landing page) with all data in one step (like storage).
                    {
                        CreateAndAddCaseDataToIncident(incident, reqData);
                    }
                }
                else if (servRequest.Step == -10)//is timeout request, set defaults
                {
                    data = HandleTimeOutRequest(incident);
                }
                else if (servRequest.Step == 2)//from slider second step
                {
                    DataAccessLayer.CaseWaitForStep caseWaitDal = new NoProblem.Core.DataAccessLayer.CaseWaitForStep(XrmDataContext);
                    bool found = caseWaitDal.FindAndMarkCaseWait(incident.new_primaryexpertiseid.Value, incident.new_regionid.Value, incident.customerid.Value);
                    if (found)
                    {
                        data = CreateAndAddCaseDataToIncident(incident, reqData);
                    }
                    else
                    {
                        continueRequest = false;
                    }
                }
                else if (servRequest.IsAutoUpsale)
                {
                    data = CreateAndAddCaseDataToIncident(incident, reqData);
                }
                else if (isManualUpsale)
                {
                    DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                    var lastIncident = dal.Retrieve(lastIncidentId);
                    data = HandleManualUpsale(incident, lastIncident);
                    RestoreSubType(lastIncident, incident);
                }
                else//is request from a slider with no special fields
                {
                    data = HandleNoSpecialFieldsRequest(incident);
                }
                if (continueRequest)
                {
                    incident.new_webdescription = CreateDescription(servRequest.RequestDescription, data);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception applying rules. class type = {0}", this.GetType());
            }
            return continueRequest;
        }

        protected abstract Microsoft.Xrm.Client.CrmEntity HandleManualUpsale(incident incident, incident lastIncident);

        protected abstract string CreateDescription(string descriptionFromRequest, Microsoft.Xrm.Client.CrmEntity data);

        protected abstract Microsoft.Xrm.Client.CrmEntity CreateAndAddCaseDataToIncident(incident incident, ServiceRequestTwoStepWaitingData reqData);

        protected abstract ServiceRequestTwoStepWaitingData GetRequestData(DataModel.ServiceRequest servRequest);

        protected abstract Microsoft.Xrm.Client.CrmEntity HandleTimeOutRequest(incident incident);

        private void CreateCaseWaitEntry(incident incident)
        {
            DataAccessLayer.CaseWaitForStep caseWaitDal = new NoProblem.Core.DataAccessLayer.CaseWaitForStep(XrmDataContext);
            DataModel.Xrm.new_casewaitforstep caseWait = new NoProblem.Core.DataModel.Xrm.new_casewaitforstep();
            caseWait.new_contactid = incident.customerid.Value;
            caseWait.new_primaryexpertiseid = incident.new_primaryexpertiseid;
            caseWait.new_regionid = incident.new_regionid;
            caseWait.new_controlname = incident.new_controlname;
            caseWait.new_description = incident.description;
            caseWait.new_domain = incident.new_domain;
            caseWait.new_done = false;
            caseWait.new_ip = incident.new_ip;
            caseWait.new_keyword = incident.new_keyword;
            caseWait.new_numberofsuppliers = incident.new_requiredaccountno;
            caseWait.new_originid = incident.new_originid;
            caseWait.new_pagename = incident.new_pagename;
            caseWait.new_placeinwebsite = incident.new_placeinwebsite;
            caseWait.new_sessionid = incident.new_sessionid;
            caseWait.new_subtype = incident.new_subtype;
            caseWait.new_toolbarid = incident.new_toolbaridid.HasValue ? incident.new_toolbaridid.Value.ToString() : string.Empty;
            caseWait.new_url = incident.new_url;
            caseWait.new_exposureid = incident.new_exposureid;
            caseWait.new_browser = incident.new_browser;
            caseWait.new_browserversion = incident.new_browserversion;
            caseWait.new_siteid = incident.new_siteid;
            caseWait.new_sitetitle = incident.new_sitetitle;
            caseWait.new_country = incident.new_country;
            caseWait.new_launchtime = DateTime.Now.AddMinutes(5);
            caseWaitDal.Create(caseWait);
            XrmDataContext.SaveChanges();
        }

        private Microsoft.Xrm.Client.CrmEntity HandleNoSpecialFieldsRequest(incident incident)
        {
            return HandleTimeOutRequest(incident);
        }
    }
}
