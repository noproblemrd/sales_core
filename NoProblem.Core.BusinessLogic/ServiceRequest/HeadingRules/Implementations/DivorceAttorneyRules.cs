﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Xrm;
using NoProblem.Core.DataModel.Consumer;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules.Implementations
{
    internal class DivorceAttorneyRules : Implementations.TwoStepWaitingRulesBase
    {
        public DivorceAttorneyRules(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private const string AND = " and ";

        protected override Microsoft.Xrm.Client.CrmEntity HandleManualUpsale(incident incident, incident lastIncident)
        {
            DataModel.Xrm.new_divorceattorneycasedata data = null;
            if (lastIncident.new_divorceattorneycasedataid.HasValue)
            {
                incident.new_divorceattorneycasedataid = lastIncident.new_divorceattorneycasedataid.Value;
                data = lastIncident.new_divorceattorneycasedata_incident;
            }
            //else
            //{
            //    data = CreateDefaultCaseDataEntry();
            //    incident.new_divorceattorneycasedataid = data.new_divorceattorneycasedataid;
            //}
            return data;
        }

        protected override string CreateDescription(string descriptionFromRequest, Microsoft.Xrm.Client.CrmEntity data)
        {
            try
            {
                StringBuilder descriptionBuilder = new StringBuilder(descriptionFromRequest);
                if (data != null)
                {
                    descriptionBuilder.Append(". ");
                    DataModel.Xrm.new_divorceattorneycasedata divData = (DataModel.Xrm.new_divorceattorneycasedata)data;
                    switch ((DataModel.Xrm.new_divorceattorneycasedata.TermsOfDivorce)divData.new_termsofdivorceagreedby)
                    {
                        case new_divorceattorneycasedata.TermsOfDivorce.WeAgreeOnAllTerms:
                            descriptionBuilder.Append("We agree on all terms");
                            break;
                        case new_divorceattorneycasedata.TermsOfDivorce.WeDoNotAgree:
                            descriptionBuilder.Append("We do not agree");
                            break;
                        default:
                            descriptionBuilder.Append("We agree on some but not all");
                            break;
                    }
                    descriptionBuilder.Append(AND);
                    switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon)divData.new_howsoonareyoulookingtofile)
                    {
                        case new_divorceattorneycasedata.DivorceHowSoon.OneToSixMonths:
                            descriptionBuilder.Append("looking to file in 1 to 6 months. ");
                            break;
                        case new_divorceattorneycasedata.DivorceHowSoon.SixMonthsOrLater:
                            descriptionBuilder.Append("looking to file 6 months or later. ");
                            break;
                        case new_divorceattorneycasedata.DivorceHowSoon.WeDoNotWantToFile:
                            descriptionBuilder.Append("we do not want to file. ");
                            break;
                        default:
                            descriptionBuilder.Append("looking to file immediately. ");
                            break;
                    }
                    if (divData.new_doyoucurrentlylivewithyourspouse.Value)
                    {
                        descriptionBuilder.Append("I am currently living with my spouse. ");
                    }
                    else
                    {
                        descriptionBuilder.Append("I am currently not living with my spouse. ");
                    }
                    if (divData.new_hasadivorcecasebeenfiledincourt.Value)
                    {
                        descriptionBuilder.Append("A divorce case has been filled in court");
                    }
                    else
                    {
                        descriptionBuilder.Append("A divorce case hasn't been filled in court");
                    }
                    descriptionBuilder.Append(AND);
                    if (divData.new_areyoucurrentlyrepresented.Value)
                    {
                        descriptionBuilder.Append("I am currently represented. ");
                    }
                    else
                    {
                        descriptionBuilder.Append("I am currently not represented. ");
                    }
                    switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing)divData.new_howareyoufinancing)
                    {
                        case new_divorceattorneycasedata.DivorceFinancing.CreditCardOrPersonalLoan:
                            descriptionBuilder.Append("I plan on financing my divorce case by credit card or personal loan. ");
                            break;
                        case new_divorceattorneycasedata.DivorceFinancing.FamilySupport:
                            descriptionBuilder.Append("I plan on financing my divorce case by family support. ");
                            break;
                        case new_divorceattorneycasedata.DivorceFinancing.ICannotAffordLegalFees:
                            descriptionBuilder.Append("I cannot afford legal feed. ");
                            break;
                        case new_divorceattorneycasedata.DivorceFinancing.IWillDiscussPaymentOptionsWithMyAttorney:
                            descriptionBuilder.Append("I plan on financing my divorce case by discussing payment options with my attorney. ");
                            break;
                        default:
                            descriptionBuilder.Append("I plan on financing my divorce case by personal savings. ");
                            break;
                    }
                    descriptionBuilder.Append("My approximate annual income is ");
                    descriptionBuilder.Append(GetMoneyString((DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome)divData.new_yourannualincome));
                    descriptionBuilder.Append(AND);
                    descriptionBuilder.Append("my spouses income is ");
                    descriptionBuilder.Append(GetMoneyString((DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome)divData.new_spouseannualincome));
                    descriptionBuilder.Append(". ");
                    if (divData.new_ownedproperty != (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.Other)
                    {
                        descriptionBuilder.Append("My spouse and I own ");
                        switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty)divData.new_ownedproperty)
                        {
                            case new_divorceattorneycasedata.DivorceProperty.HouseOrCondo:
                                descriptionBuilder.Append("a house. ");
                                break;
                            case new_divorceattorneycasedata.DivorceProperty.IRA:
                                descriptionBuilder.Append("IRA. ");
                                break;
                            case new_divorceattorneycasedata.DivorceProperty.Jewelry:
                                descriptionBuilder.Append("jewelry. ");
                                break;
                            case new_divorceattorneycasedata.DivorceProperty.Pension:
                                descriptionBuilder.Append("a pension. ");
                                break;
                            case new_divorceattorneycasedata.DivorceProperty.StocksOrBonds:
                                descriptionBuilder.Append("stocks. ");
                                break;
                            case new_divorceattorneycasedata.DivorceProperty.VacationProperty:
                                descriptionBuilder.Append("vacation property. ");
                                break;
                            default:
                                descriptionBuilder.Append("vehicle(s). ");
                                break;
                        }
                    }
                    descriptionBuilder.Append("We share ");
                    switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren)divData.new_howmanychildren)
                    {
                        case new_divorceattorneycasedata.DivorceChildren.Five:
                            descriptionBuilder.Append("5");
                            break;
                        case new_divorceattorneycasedata.DivorceChildren.Four:
                            descriptionBuilder.Append("4");
                            break;
                        case new_divorceattorneycasedata.DivorceChildren.Three:
                            descriptionBuilder.Append("3");
                            break;
                        case new_divorceattorneycasedata.DivorceChildren.Two:
                            descriptionBuilder.Append("2");
                            break;
                        case new_divorceattorneycasedata.DivorceChildren.One:
                            descriptionBuilder.Append("1");
                            break;
                        case new_divorceattorneycasedata.DivorceChildren.Zero:
                            descriptionBuilder.Append("no");
                            break;
                        default:
                            descriptionBuilder.Append("5+");
                            break;
                    }
                    descriptionBuilder.Append(" children.");
                }
                return descriptionBuilder.ToString();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Failed to create description.");
                return descriptionFromRequest;
            }
        }

        private string GetMoneyString(DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome money)
        {
            string retVal;
            switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome)money)
            {
                case new_divorceattorneycasedata.DivorceIncome.LessThan10K:
                    retVal = "less than $10,000";
                    break;
                case new_divorceattorneycasedata.DivorceIncome.MoreThan100K:
                    retVal = "more than $100,000";
                    break;
                case new_divorceattorneycasedata.DivorceIncome.SixtyKTo100K:
                    retVal = "$60,000 to $100,000";
                    break;
                case new_divorceattorneycasedata.DivorceIncome.TenKTo30K:
                    retVal = "$10,000 to $30,000";
                    break;
                default:
                    retVal = "$30,000 to $60,000";
                    break;
            }
            return retVal;
        }

        protected override Microsoft.Xrm.Client.CrmEntity CreateAndAddCaseDataToIncident(incident incident, ServiceRequestTwoStepWaitingData reqData)
        {
            DataModel.Consumer.ServiceRequestDivorceAttorneyData divorceReqData = (DataModel.Consumer.ServiceRequestDivorceAttorneyData)reqData;
            DataModel.Xrm.new_divorceattorneycasedata data = null;
            if (reqData != null)
            {
                data = CreateCaseDataEntry(divorceReqData);
                incident.new_divorceattorneycasedataid = data.new_divorceattorneycasedataid;
            }
            //else
            //{
            //    data = CreateDefaultCaseDataEntry();
            //}
            //incident.new_divorceattorneycasedataid = data.new_divorceattorneycasedataid;
            return data;
        }

        protected override ServiceRequestTwoStepWaitingData GetRequestData(DataModel.ServiceRequest servRequest)
        {
            return servRequest.DivorceAttorneyData;
        }

        protected override Microsoft.Xrm.Client.CrmEntity HandleTimeOutRequest(incident incident)
        {
            //DataModel.Xrm.new_divorceattorneycasedata data = CreateDefaultCaseDataEntry();
            //incident.new_divorceattorneycasedataid = data.new_divorceattorneycasedataid;
            //return data;
            return null;
        }

        //private new_divorceattorneycasedata CreateDefaultCaseDataEntry()
        //{
        //    new_divorceattorneycasedata data = new new_divorceattorneycasedata();
        //    int _rnd = new Random().Next(6);
        //    switch (_rnd)
        //    {
        //        case(0):
        //            data.new_termsofdivorceagreedby = (int)DataModel.Xrm.new_divorceattorneycasedata.TermsOfDivorce.WeAgreeOnSomeButNotAll;
        //            data.new_howsoonareyoulookingtofile = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon.Immediately;
        //            data.new_doyoucurrentlylivewithyourspouse = false;
        //            data.new_hasadivorcecasebeenfiledincourt = false;
        //            data.new_areyoucurrentlyrepresented = false;
        //            data.new_howareyoufinancing = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.IWillDiscussPaymentOptionsWithMyAttorney;
        //            data.new_yourannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.TenKTo30K;
        //            data.new_spouseannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.TenKTo30K;
        //            data.new_ownedproperty = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.Pension;
        //            data.new_howmanychildren = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.Two;
        //            break;
        //        case (1):
        //            data.new_termsofdivorceagreedby = (int)DataModel.Xrm.new_divorceattorneycasedata.TermsOfDivorce.WeDoNotAgree;
        //            data.new_howsoonareyoulookingtofile = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon.OneToSixMonths;
        //            data.new_doyoucurrentlylivewithyourspouse = false;
        //            data.new_hasadivorcecasebeenfiledincourt = false;
        //            data.new_areyoucurrentlyrepresented = false;
        //            data.new_howareyoufinancing = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.IWillDiscussPaymentOptionsWithMyAttorney;
        //            data.new_yourannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.ThirtyKTo60K;
        //            data.new_spouseannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.ThirtyKTo60K;
        //            data.new_ownedproperty = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.Vehicles;
        //            data.new_howmanychildren = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.Three;
        //            break;
        //        case (2):
        //            data.new_termsofdivorceagreedby = (int)DataModel.Xrm.new_divorceattorneycasedata.TermsOfDivorce.WeAgreeOnSomeButNotAll;
        //            data.new_howsoonareyoulookingtofile = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon.Immediately;
        //            data.new_doyoucurrentlylivewithyourspouse = true;
        //            data.new_hasadivorcecasebeenfiledincourt = false;
        //            data.new_areyoucurrentlyrepresented = false;
        //            data.new_howareyoufinancing = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.IWillDiscussPaymentOptionsWithMyAttorney;
        //            data.new_yourannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.ThirtyKTo60K;
        //            data.new_spouseannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.TenKTo30K;
        //            data.new_ownedproperty = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.Other;
        //            data.new_howmanychildren = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.Two;
        //            break;
        //        case (3):
        //            data.new_termsofdivorceagreedby = (int)DataModel.Xrm.new_divorceattorneycasedata.TermsOfDivorce.WeDoNotAgree;
        //            data.new_howsoonareyoulookingtofile = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon.OneToSixMonths;
        //            data.new_doyoucurrentlylivewithyourspouse = false;
        //            data.new_hasadivorcecasebeenfiledincourt = false;
        //            data.new_areyoucurrentlyrepresented = false;
        //            data.new_howareyoufinancing = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.IWillDiscussPaymentOptionsWithMyAttorney;
        //            data.new_yourannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.TenKTo30K;
        //            data.new_spouseannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.ThirtyKTo60K;
        //            data.new_ownedproperty = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.Other;
        //            data.new_howmanychildren = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.One;
        //            break;
        //        case (4):
        //            data.new_termsofdivorceagreedby = (int)DataModel.Xrm.new_divorceattorneycasedata.TermsOfDivorce.WeAgreeOnSomeButNotAll;
        //            data.new_howsoonareyoulookingtofile = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon.SixMonthsOrLater;
        //            data.new_doyoucurrentlylivewithyourspouse = false;
        //            data.new_hasadivorcecasebeenfiledincourt = false;
        //            data.new_areyoucurrentlyrepresented = false;
        //            data.new_howareyoufinancing = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.IWillDiscussPaymentOptionsWithMyAttorney;
        //            data.new_yourannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.TenKTo30K;
        //            data.new_spouseannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.ThirtyKTo60K;
        //            data.new_ownedproperty = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.Pension;
        //            data.new_howmanychildren = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.Three;
        //            break;
        //        default:
        //            data.new_termsofdivorceagreedby = (int)DataModel.Xrm.new_divorceattorneycasedata.TermsOfDivorce.WeDoNotAgree;
        //            data.new_howsoonareyoulookingtofile = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon.WeDoNotWantToFile;
        //            data.new_doyoucurrentlylivewithyourspouse = true;
        //            data.new_hasadivorcecasebeenfiledincourt = false;
        //            data.new_areyoucurrentlyrepresented = false;
        //            data.new_howareyoufinancing = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.IWillDiscussPaymentOptionsWithMyAttorney;
        //            data.new_yourannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.ThirtyKTo60K;
        //            data.new_spouseannualincome = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.TenKTo30K;
        //            data.new_ownedproperty = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.Vehicles;
        //            data.new_howmanychildren = (int)DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.Two;
        //            break;
        //    }     
        //    DataAccessLayer.DivorceAttorneyCaseData dal = new DataAccessLayer.DivorceAttorneyCaseData(XrmDataContext);
        //    dal.Create(data);
        //    XrmDataContext.SaveChanges();
        //    return data;
        //}

        private new_divorceattorneycasedata CreateCaseDataEntry(ServiceRequestDivorceAttorneyData reqData)
        {
            new_divorceattorneycasedata data = new new_divorceattorneycasedata();
            data.new_termsofdivorceagreedby = (int)reqData.Terms;
            data.new_howsoonareyoulookingtofile = (int)reqData.Soon;
            data.new_doyoucurrentlylivewithyourspouse = reqData.Live;
            data.new_hasadivorcecasebeenfiledincourt = reqData.BeenFiled;
            data.new_areyoucurrentlyrepresented = reqData.Represented;
            data.new_howareyoufinancing = (int)reqData.Financing;
            data.new_yourannualincome = (int)reqData.Income;
            data.new_spouseannualincome = (int)reqData.SpouseIncome;
            data.new_ownedproperty = (int)reqData.Property;
            data.new_howmanychildren = (int)reqData.Children;
            DataAccessLayer.DivorceAttorneyCaseData dal = new NoProblem.Core.DataAccessLayer.DivorceAttorneyCaseData(XrmDataContext);
            dal.Create(data);
            XrmDataContext.SaveChanges();
            return data;
        }
    }
}
