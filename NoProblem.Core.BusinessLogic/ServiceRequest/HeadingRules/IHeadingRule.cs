﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules
{
    interface IHeadingRule
    {
        /// <summary>
        /// Applies special rules for this request. Returns if the system should continue processing the request or not (waiting for more steps).
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        /// <param name="incident"></param>
        /// <param name="isManualUpsale"></param>
        /// <param name="lastIncidentId"></param>
        /// <returns></returns>
        bool ApplyRules(IServiceRequest request, IServiceResponse response, DML.Xrm.incident incident, bool isManualUpsale, Guid lastIncidentId);
    }
}
