﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.HeadingRules
{
    internal class HeadingRulesFactory
    {
        public static IHeadingRule GetRuleInstance(IServiceRequest request, DataModel.Xrm.new_primaryexpertise heading, DML.Xrm.DataContext xrmDataContext)
        {
            if (!(request is DataModel.ServiceRequest))
            {
                return null;
            }
            IHeadingRule retVal = null;
            string ruleName = heading.new_headingrulename;
            if (!string.IsNullOrEmpty(ruleName))
            {
                switch (ruleName.ToLower())
                {
                    case "accountant":
                        retVal = new Implementations.AccountantRules(xrmDataContext);
                        break;
                    case "mover":
                        retVal = new Implementations.MoverRules(xrmDataContext);
                        break;
                    case "handyman":
                        retVal = new Implementations.HandymanRules(xrmDataContext);
                        break;
                    case "computerrepair":
                        retVal = new Implementations.ComputerRepairRules(xrmDataContext);
                        break;
                    //case "payday":
                    //    retVal = new Implementations.PaydayLoanRules(xrmDataContext);
                    //    break;
                    case "creditcounselor":
                        retVal = new Implementations.CreditCounselorRules(xrmDataContext);
                        break;
                    case "cleaner":
                        retVal = new Implementations.CleanerRules(xrmDataContext);
                        break;
                    case "dentist":
                        retVal = new Implementations.DentistRules(xrmDataContext);
                        break;
                    case "personalinjuryattorney":
                        retVal = new Implementations.PersonalInjuryRules(xrmDataContext);
                        break;
                    case "criminalattorney":
                        retVal = new Implementations.CriminalLawLayerRules(xrmDataContext);
                        break;
                    case "duiattorney":
                        retVal = new Implementations.DuiAttorneyRules(xrmDataContext);
                        break;
                    case "divorceattorney":
                        retVal = new Implementations.DivorceAttorneyRules(xrmDataContext);
                        break;
                    case "bankruptcyattorney":
                        retVal = new Implementations.BankruptcyAttoneyRules(xrmDataContext);
                        break;
                    case "homeimprovement":
                        retVal = new Implementations.HomeImprovementRules(xrmDataContext);
                        break;
                    case "socialsecuritydisability":
                        retVal = new Implementations.SocialSecurityDisabilityRules(xrmDataContext);
                        break;
                    case "homesecurity":
                        retVal = new Implementations.HomeSecurityRules(xrmDataContext);
                        break;
                    case "storage":
                        retVal = new Implementations.StorageRules(xrmDataContext);
                        break;
                    case "healthinsurance":
                        retVal = new Implementations.HealthInsuranceRules(xrmDataContext);
                        break;
                    default:
                        retVal = new Implementations.DefaultRules(xrmDataContext);
                        break;
                }
            }
            else
            {
                retVal = new Implementations.DefaultRules(xrmDataContext);
            }
            return retVal;
        }
    }
}
