﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Asterisk;
using NoProblem.Core.DataModel.Jona;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class JonaManager : XrmUserBase
    {
        #region Ctor
        public JonaManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Fields

        private static SynchronizedCollection<TimerContainer> ttsTimersHolder = new System.Collections.Generic.SynchronizedCollection<TimerContainer>();
        private static SynchronizedCollection<NoProblem.Core.DataModel.Jona.RecordingsRetryContainerJona> recordingsRetriesHolder = new SynchronizedCollection<NoProblem.Core.DataModel.Jona.RecordingsRetryContainerJona>();
        private static SynchronizedCollection<TimerContainer> jonaWaitTimerHolder = new SynchronizedCollection<TimerContainer>();
        private delegate void CreateCdrsDelegate(DataModel.Xrm.new_jonamanagement jona);
        private delegate void GetRecordingsDelegate(DataModel.Xrm.new_jonamanagement jona);
        private delegate void SaveTtsAsteriskStatusEventDelegate(DataModel.Asterisk.TtsStatusContainer statusContainer, enumTtsStatus currentStatus, enumTtsStatus nextStatus, Guid jonaCallId);
        private delegate void SaveC2cAsteriskStatusEventDelegate(DataModel.Asterisk.C2cStatusContainer statusContainer, enumCallStatus currentStatus, enumCallStatus nextStatus, Guid jonaCallId);

        #endregion

        #region Public Methods

        public string InitiateCall(string originatorPhone, string targetPhone, string description, bool rejectEnabled)
        {
            AsteriskBL.AsteriskCallsExecuter ex = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskCallsExecuter(null);
            return ex.InitiateC2c(originatorPhone, targetPhone, true, description, rejectEnabled, Guid.NewGuid(), "a programmer", false);
        }

        public string InitiateTts(string phone, string description, bool rejectEnabled)
        {
            AsteriskBL.AsteriskCallsExecuter ex = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskCallsExecuter(null);
            return ex.InitiateTts(phone, description, 10, 100, Guid.NewGuid(), rejectEnabled);
        }

        #endregion

        #region Internal Methods

        #region Initiators

        internal string InitiateAuction(Guid incidentId, string consumerPhone, List<inc2.NPProvider> providersList,
                       int supplierAmountToCall, int numOfRetries, int retryDelay, string description, bool ttsForJona, bool rejectEnabled)
        {
            DataAccessLayer.JonaManagement jonaDal = new NoProblem.Core.DataAccessLayer.JonaManagement(XrmDataContext);
            DataModel.Xrm.new_jonamanagement jona = new NoProblem.Core.DataModel.Xrm.new_jonamanagement();
            jona.new_incidentid = incidentId;
            jona.new_consumerfailedtries = 0;
            jona.new_status = "TTS_STAGE";
            jona.new_numsupplierstocall = supplierAmountToCall;
            jona.new_ttsforjona = ttsForJona;
            jona.new_rejectenabled = rejectEnabled;
            jona.new_retrydelay = retryDelay;
            jona.new_numofretriesforadveritsers = numOfRetries;
            jonaDal.Create(jona);
            XrmDataContext.SaveChanges();

            DataAccessLayer.IncidentAccount inciAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataAccessLayer.JonaCall jonaCallDal = new NoProblem.Core.DataAccessLayer.JonaCall(XrmDataContext);
            List<DataModel.Xrm.new_jonacall> calls = new List<NoProblem.Core.DataModel.Xrm.new_jonacall>();
            AsteriskBL.AsteriskCallsExecuter executer = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskCallsExecuter(null);
            foreach (var prov in providersList)
            {
                DataModel.Xrm.new_jonacall call = new NoProblem.Core.DataModel.Xrm.new_jonacall();
                call.new_ttsstatus = DataModel.Jona.enumTtsStatus.QUEUED.ToString();
                call.new_jonamanagementid = jona.new_jonamanagementid;
                call.new_bid = (int)prov.defaultBidValue;
                call.new_done = false;
                call.new_ttsdone = false;
                call.new_advertiserfailedtries = 0;
                var inciAcc = inciAccDal.GetIncidentAccountByIncidentAndAccount(incidentId, new Guid(prov.providerUniqueId));
                call.new_incidentaccountid = inciAcc.new_incidentaccountid;
                jonaCallDal.Create(call);
                XrmDataContext.SaveChanges();
                calls.Add(call);

                bool ttsCallSent = false;
                if (prov.realTimeBid)
                {
                    var asteriskSessionId = executer.InitiateTts(prov.phoneNumbers.First(), description, (int)prov.minBidValue, (int)prov.maxBidValue, jona.new_jonamanagementid, rejectEnabled);
                    if (!string.IsNullOrEmpty(asteriskSessionId))//if asterisk tts sent ok.
                    {
                        call.new_ttscallid = asteriskSessionId;
                        call.new_isrealtimebidder = true;
                        jonaCallDal.Update(call);
                        XrmDataContext.SaveChanges();
                        ttsCallSent = true;
                    }
                }
                if (!ttsCallSent)//if is not a bidder or asterisk tts failed.
                {
                    call.new_ttsdone = true;
                    jonaCallDal.Update(call);
                    XrmDataContext.SaveChanges();
                }
            }
            TimerContainer timerContainer = new TimerContainer();
            timerContainer.Timer = new Timer(
                new TimerCallback(CheckIfTtsDone),
                timerContainer,
                10000,
                30000);
            timerContainer.JonaId = jona.new_jonamanagementid;
            timerContainer.TimerStartTime = DateTime.Now;
            ttsTimersHolder.Add(timerContainer);
            return "0,success";
        }

        internal string InitiateServiceRequest(Guid incidentId, string consumerPhone, List<inc2.NPProvider> providerList,
                         int numOfRetries, int retryDelay, string description, bool ttsForJona, bool rejectEnabled)
        {
            DataAccessLayer.JonaManagement jonaDal = new NoProblem.Core.DataAccessLayer.JonaManagement(XrmDataContext);
            DataModel.Xrm.new_jonamanagement jona = new NoProblem.Core.DataModel.Xrm.new_jonamanagement();
            jona.new_incidentid = incidentId;
            jona.new_consumerfailedtries = 0;
            jona.new_status = "SR_STAGE";
            jona.new_ttsforjona = ttsForJona;
            jona.new_rejectenabled = rejectEnabled;
            jona.new_retrydelay = retryDelay;
            jona.new_numofretriesforadveritsers = numOfRetries;
            jonaDal.Create(jona);
            XrmDataContext.SaveChanges();

            DataAccessLayer.IncidentAccount inciAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataAccessLayer.JonaCall jonaCallDal = new NoProblem.Core.DataAccessLayer.JonaCall(XrmDataContext);
            List<DataModel.Xrm.new_jonacall> calls = new List<NoProblem.Core.DataModel.Xrm.new_jonacall>();
            foreach (var prov in providerList)
            {
                DataModel.Xrm.new_jonacall call = new NoProblem.Core.DataModel.Xrm.new_jonacall();
                call.new_status = DataModel.Jona.enumCallStatus.QUEUED.ToString();
                call.new_laststatustimestamp = DateTime.Now;
                call.new_jonamanagementid = jona.new_jonamanagementid;
                call.new_bid = (int)prov.defaultBidValue;
                call.new_done = false;
                call.new_advertiserfailedtries = 0;
                var inciAcc = inciAccDal.GetIncidentAccountByIncidentAndAccount(incidentId, new Guid(prov.providerUniqueId));
                call.new_incidentaccountid = inciAcc.new_incidentaccountid;
                jonaCallDal.Create(call);
                XrmDataContext.SaveChanges();
                calls.Add(call);
            }
            if (calls.Count > 0)
            {
                var firstCall = calls.OrderByDescending(x => x.new_bid.Value).First();
                bool callSent = ExecuteCall(firstCall, consumerPhone, description, jonaCallDal, inciAccDal, ttsForJona, rejectEnabled, jona.new_jonamanagementid);
                if (!callSent)
                {
                    return "1,FailedToExecuteCall";
                }
            }
            else
            {
                ContinueJona(jona.new_jonamanagementid);
            }
            return "0,success";
        }

        #endregion

        internal void HandleTtsCallStatus(DataModel.Asterisk.TtsStatusContainer statusContainer)
        {
            DataAccessLayer.JonaCall callDal = new NoProblem.Core.DataAccessLayer.JonaCall(XrmDataContext);
            DataModel.Xrm.new_jonacall call = callDal.GetCallByTtsCallId(statusContainer.Internal_Id);
            enumTtsStatus currentStatus = (enumTtsStatus)Enum.Parse(typeof(enumTtsStatus), call.new_ttsstatus);
            enumTtsStatus nextStatus = getNextTtsStatus(currentStatus, statusContainer);
            SaveTtsAsteriskStatusEventDelegate del = new SaveTtsAsteriskStatusEventDelegate(SaveAsteriskStatusEvent);
            del.BeginInvoke(statusContainer, currentStatus, nextStatus, call.new_jonacallid, SaveTtsAsteriskStatuaEventAsyncCallBack, del);
            LogUtils.MyHandle.WriteToLog(8, "--tts-- {0} => {1} --", currentStatus, nextStatus);
            if (currentStatus != nextStatus)
            {
                call.new_ttsstatus = nextStatus.ToString();
                call.new_laststatustimestamp = DateTime.Now;
                if (nextStatus == enumTtsStatus.CONFIRMED)
                {
                    int newBid;
                    if (int.TryParse(statusContainer.Digit, out newBid))
                    {
                        call.new_bid = newBid;
                        call.new_enteredrealtimebid = true;
                    }
                }
                else if (nextStatus == enumTtsStatus.REJECT)
                {
                    call.new_isrejected = true;
                }
                if (
                    nextStatus == enumTtsStatus.BUSY ||
                    nextStatus == enumTtsStatus.CALL_INIT_TIMEOUT ||
                    nextStatus == enumTtsStatus.CONFIRMED ||
                    nextStatus == enumTtsStatus.ERROR ||
                    nextStatus == enumTtsStatus.NOT_CONFIRMED ||
                    nextStatus == enumTtsStatus.REJECT)
                {
                    call.new_ttscallend = DateTime.Now;
                    call.new_ttsdone = true;
                }
                else if (nextStatus == enumTtsStatus.ADVERTISER_ANSWERED)
                {
                    call.new_ttscallstart = DateTime.Now;
                }
                callDal.Update(call);
                XrmDataContext.SaveChanges();
            }
        }

        internal void HandleCallStatus(DataModel.Asterisk.C2cStatusContainer statusContainer)
        {
            DataAccessLayer.JonaCall callDal = new NoProblem.Core.DataAccessLayer.JonaCall(XrmDataContext);
            DataModel.Xrm.new_jonacall call = callDal.GetCallByCallId(statusContainer.Session);
            if (call == null)
            {
                if (statusContainer.Status != "ack")
                {
                    throw new Exception("Call is null and status is not ack in HandleCallStatus");
                }
                return;
            }
            enumCallStatus currentStatus = (enumCallStatus)Enum.Parse(typeof(enumCallStatus), call.new_status);
            enumCallStatus nextStatus = GetNextStatus(currentStatus, statusContainer);
            SaveC2cAsteriskStatusEventDelegate del = new SaveC2cAsteriskStatusEventDelegate(SaveAsteriskStatusEvent);
            del.BeginInvoke(statusContainer, currentStatus, nextStatus, call.new_jonacallid, SaveC2cAsteriskStatuaEventAsyncCallBack, del);
            LogUtils.MyHandle.WriteToLog(8, "-- {0} => {1} --", currentStatus, nextStatus);
            if (currentStatus != nextStatus)
            {
                call.new_status = nextStatus.ToString();
                call.new_laststatustimestamp = DateTime.Now;
                call.new_sessionserver = statusContainer.HostName;
                callDal.Update(call);
                XrmDataContext.SaveChanges();
                if (nextStatus == enumCallStatus.CALL_COMPLETED)
                {
                    call.new_done = true;
                    callDal.Update(call);
                    XrmDataContext.SaveChanges();
                    SaveEndOfCall(statusContainer);
                    int duration = CalculateConsumerCallLength(statusContainer.Session);
                    Dialer.PhoneManager phoneManager = new NoProblem.Core.BusinessLogic.Dialer.PhoneManager(XrmDataContext);
                    phoneManager.ConsumerContactedSucc(
                        call.new_jonamanagement_jonacall.new_incidentid.Value.ToString(),
                        call.new_incidentaccount_jonacall.new_account_new_incidentaccount.accountid.ToString(),
                        statusContainer.Session,
                        DateTime.Now.ToString(),
                        duration.ToString());
                    ContinueJonaAsync(call.new_jonamanagementid.Value);
                }
                else if (nextStatus == enumCallStatus.REJECTED)
                {
                    call.new_done = true;
                    callDal.Update(call);
                    XrmDataContext.SaveChanges();
                    SaveEndOfCall(statusContainer);
                    Dialer.CallManager callManager = new NoProblem.Core.BusinessLogic.Dialer.CallManager(XrmDataContext, null);
                    callManager.CallStatus(
                        call.new_jonamanagement_jonacall.new_incidentid.Value,
                        call.new_incidentaccount_jonacall.new_account_new_incidentaccount.accountid,
                        (int)nextStatus);
                    ContinueJonaAsync(call.new_jonamanagementid.Value);
                }
                else
                {
                    Dialer.CallManager callManager = new NoProblem.Core.BusinessLogic.Dialer.CallManager(XrmDataContext, null);
                    callManager.CallStatus(
                        call.new_jonamanagement_jonacall.new_incidentid.Value,
                        call.new_incidentaccount_jonacall.new_account_new_incidentaccount.accountid,
                        (int)nextStatus);
                    if (nextStatus == enumCallStatus.CUSTOMER_BUSY)
                    {
                        HandleCustomerNotAnswering(call.new_jonamanagementid.Value, call, callDal);
                        SaveEndOfCall(statusContainer);
                    }
                    else if (nextStatus == enumCallStatus.ADVERTISER_BUSY
                        || nextStatus == enumCallStatus.ADVERTISER_NOT_CONFIRMED)
                    {
                        HandleAdvertiserNotAnswering(call.new_jonamanagementid.Value, call, callDal);
                        SaveEndOfCall(statusContainer);
                    }
                    else if (nextStatus == enumCallStatus.CALL_INIT_TIMEOUT
                        || nextStatus == enumCallStatus.CALL_TERMINATED
                        || nextStatus == enumCallStatus.ERROR
                        || nextStatus == enumCallStatus.UNKNOWN)
                    {
                        call.new_done = true;
                        callDal.Update(call);
                        XrmDataContext.SaveChanges();
                        SaveEndOfCall(statusContainer);
                        ContinueJonaAsync(call.new_jonamanagementid.Value);
                    }
                    else if (nextStatus == enumCallStatus.CALL_IN_PROGRESS)
                    {
                        //save start of consumer call.
                        SaveConsumerCallStartTime(statusContainer);
                    }
                    else if (nextStatus == enumCallStatus.ADVERTISER_ANSWERED)
                    {
                        //save start of call
                        SaveCallStartTime(statusContainer);
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        private void SaveAsteriskStatusEvent(DataModel.Asterisk.TtsStatusContainer statusContainer, enumTtsStatus currentStatus, enumTtsStatus nextStatus, Guid jonaCallId)
        {
            DataAccessLayer.TtsStatusEvent dal = new NoProblem.Core.DataAccessLayer.TtsStatusEvent(null);
            DataModel.Xrm.new_ttsstatusevent ttsEvent = new NoProblem.Core.DataModel.Xrm.new_ttsstatusevent();
            ttsEvent.new_status = statusContainer.Status;
            ttsEvent.new_jonacallid = jonaCallId;
            ttsEvent.new_internal_id = statusContainer.Internal_Id;
            ttsEvent.new_digit = statusContainer.Digit;
            ttsEvent.new_currentstatus = currentStatus.ToString();
            ttsEvent.new_nextstatus = nextStatus.ToString();
            dal.Create(ttsEvent);
            dal.XrmDataContext.SaveChanges();
        }

        private void SaveAsteriskStatusEvent(DataModel.Asterisk.C2cStatusContainer statusContainer, enumCallStatus currentStatus, enumCallStatus nextStatus, Guid jonaCallId)
        {
            DataAccessLayer.C2cStatusEvent dal = new NoProblem.Core.DataAccessLayer.C2cStatusEvent(null);
            DataModel.Xrm.new_c2cstatusevent c2cEvent = new NoProblem.Core.DataModel.Xrm.new_c2cstatusevent();
            c2cEvent.new_jonacallid = jonaCallId;
            c2cEvent.new_session = statusContainer.Session;
            c2cEvent.new_side = statusContainer.Side;
            c2cEvent.new_status = statusContainer.Status;
            c2cEvent.new_time = statusContainer.Time;
            c2cEvent.new_type = statusContainer.Type;
            c2cEvent.new_currentstatus = currentStatus.ToString();
            c2cEvent.new_nextstatus = nextStatus.ToString();
            c2cEvent.new_sessionserver = statusContainer.HostName;
            dal.Create(c2cEvent);
            dal.XrmDataContext.SaveChanges();
        }

        private void SaveC2cAsteriskStatuaEventAsyncCallBack(IAsyncResult result)
        {
            try
            {
                ((SaveC2cAsteriskStatusEventDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SaveC2cAsteriskStatuaEventAsyncCallBack");
            }
        }

        private void SaveTtsAsteriskStatuaEventAsyncCallBack(IAsyncResult result)
        {
            try
            {
                ((SaveTtsAsteriskStatusEventDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SaveTtsAsteriskStatuaEventAsyncCallBack");
            }
        }

        private void HandleAdvertiserNotAnswering(Guid jonaId, NoProblem.Core.DataModel.Xrm.new_jonacall call, NoProblem.Core.DataAccessLayer.JonaCall callDal)
        {
            //increase advertiser tries and wait to retry if has tries left.
            DataAccessLayer.JonaManagement jonaDal = new NoProblem.Core.DataAccessLayer.JonaManagement(XrmDataContext);
            var jona = jonaDal.Retrieve(jonaId);
            if (call.new_advertiserfailedtries + 1 >= jona.new_numofretriesforadveritsers)
            {
                //end advertiser retries
                call.new_done = true;
                callDal.Update(call);
                XrmDataContext.SaveChanges();
                ContinueJonaAsync(call.new_jonamanagementid.Value);
            }
            else
            {
                call.new_advertiserfailedtries++;
                call.new_status = enumCallStatus.QUEUED.ToString();
                call.new_laststatustimestamp = DateTime.Now;
                callDal.Update(call);
                XrmDataContext.SaveChanges();
                //wait
                TimerContainer container = new TimerContainer();
                container.JonaId = jonaId;
                container.Timer = new Timer(
                    new TimerCallback(ContinueWaitingJona),
                    container,
                    jona.new_retrydelay.Value * 60000,
                    Timeout.Infinite);
                jonaWaitTimerHolder.Add(container);
            }
        }

        private void HandleCustomerNotAnswering(Guid jonaId, DataModel.Xrm.new_jonacall call, DataAccessLayer.JonaCall jonaCallDal)
        {
            //increase customer tries and wait to retry if has tries left.
            DataAccessLayer.JonaManagement jonaDal = new NoProblem.Core.DataAccessLayer.JonaManagement(XrmDataContext);
            var jona = jonaDal.Retrieve(jonaId);
            if (jona.new_consumerfailedtries > 0)
            {
                //end it all
                jona.new_consumerfailedtries = 2;
                jona.new_status = "CUSTOMER_IS_UNAVAILABLE";
                jonaDal.Update(jona);
                XrmDataContext.SaveChanges();
                foreach (var item in jona.new_jonamanagement_jonacall)
                {
                    item.new_done = true;
                    jonaCallDal.Update(item);
                }
                XrmDataContext.SaveChanges();
                FinishJona(XrmDataContext, jona, 2);
            }
            else
            {
                jona.new_consumerfailedtries = 1;
                jonaDal.Update(jona);
                call.new_status = enumCallStatus.QUEUED.ToString();
                call.new_laststatustimestamp = DateTime.Now;
                jonaCallDal.Update(call);
                XrmDataContext.SaveChanges();
                //wait
                TimerContainer container = new TimerContainer();
                container.JonaId = jonaId;
                container.Timer = new Timer(
                    new TimerCallback(ContinueWaitingJona),
                    container,
                    60000,
                    Timeout.Infinite);
                jonaWaitTimerHolder.Add(container);
            }
        }

        private void ContinueWaitingJona(object timerContainerObj)
        {
            try
            {
                TimerContainer timerContainer = (TimerContainer)timerContainerObj;
                Guid jonaId = timerContainer.JonaId;
                LogUtils.MyHandle.WriteToLog(8, "ContinueWaitingJona invoked. jonaId = {0}", jonaId);
                timerContainer.Timer.Dispose();
                jonaWaitTimerHolder.Remove(timerContainer);
                ContinueJona(jonaId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in JonaManager.ContinueWaitingJona. jonaId = {0}", ((TimerContainer)timerContainerObj).JonaId);
            }
        }

        private int CalculateConsumerCallLength(string asteriskCallId)
        {
            DataAccessLayer.JonaCallCDR cdrDal = new NoProblem.Core.DataAccessLayer.JonaCallCDR(null);
            var cdr = cdrDal.GetByAsteriskCallId(asteriskCallId);
            int retVal = 0;
            if (cdr != null && cdr.new_consumercallstart.HasValue && cdr.new_callend.HasValue)
            {
                retVal = (int)(cdr.new_callend.Value - cdr.new_consumercallstart.Value).TotalSeconds;
            }
            return retVal;
        }

        private void SaveEndOfCall(DataModel.Asterisk.C2cStatusContainer statusContainer)
        {
            DataAccessLayer.JonaCallCDR cdrDal = new NoProblem.Core.DataAccessLayer.JonaCallCDR(null);
            DataModel.Xrm.new_jonacallcdr cdr = cdrDal.GetByAsteriskCallId(statusContainer.Session);
            if (cdr == null)
            {
                LogUtils.MyHandle.WriteToLog("Could not find jona call CDR with callId {0} in JonaManager.SaveEndOfCall", statusContainer.Session);
                return;
            }
            cdr.new_callend = DateTime.Now;
            cdrDal.Update(cdr);
            cdrDal.XrmDataContext.SaveChanges();
        }

        private void SaveCallStartTime(DataModel.Asterisk.C2cStatusContainer statusContainer)
        {
            DataAccessLayer.JonaCallCDR cdrDal = new NoProblem.Core.DataAccessLayer.JonaCallCDR(null);
            DataModel.Xrm.new_jonacallcdr cdr = cdrDal.GetByAsteriskCallId(statusContainer.Session);
            if (cdr == null)
            {
                LogUtils.MyHandle.WriteToLog("Could not find jona call CDR with callId {0} in JonaManager.SaveCallStartTime", statusContainer.Session);
                return;
            }
            cdr.new_callstart = DateTime.Now;
            cdrDal.Update(cdr);
            cdrDal.XrmDataContext.SaveChanges();
        }

        private void SaveConsumerCallStartTime(DataModel.Asterisk.C2cStatusContainer statusContainer)
        {
            DataAccessLayer.JonaCallCDR cdrDal = new NoProblem.Core.DataAccessLayer.JonaCallCDR(null);
            DataModel.Xrm.new_jonacallcdr cdr = cdrDal.GetByAsteriskCallId(statusContainer.Session);
            if (cdr == null)
            {
                LogUtils.MyHandle.WriteToLog("Could not find jona call CDR with callId {0} in JonaManager.SaveConsumerCallStartTime", statusContainer.Session);
                return;
            }
            cdr.new_consumercallstart = DateTime.Now;
            cdrDal.Update(cdr);
            cdrDal.XrmDataContext.SaveChanges();
        }

        private void CheckIfTtsDone(object timerContainerObj)
        {
            try
            {
                TimerContainer timerContainer = (TimerContainer)timerContainerObj;
                Guid jonaId = timerContainer.JonaId;
                LogUtils.MyHandle.WriteToLog(8, "Checking if tts is done. jonaId = {0}", jonaId);
                DataAccessLayer.JonaManagement dal = new NoProblem.Core.DataAccessLayer.JonaManagement(null);
                var localContext = dal.XrmDataContext;
                var jona = dal.Retrieve(jonaId);
                bool allDone = true;
                DataAccessLayer.JonaCall jonaCallDal = new NoProblem.Core.DataAccessLayer.JonaCall(localContext);//clears crmContext cache.
                foreach (var call in jona.new_jonamanagement_jonacall)
                {
                    if (!call.new_ttsdone.HasValue || !call.new_ttsdone.Value)
                    {
                        allDone = false;
                        break;
                    }
                }
                if (allDone || (DateTime.Now - timerContainer.TimerStartTime) > new TimeSpan(0, 3, 0))
                {
                    timerContainer.Timer.Dispose();
                    ttsTimersHolder.Remove(timerContainer);
                    Dialer.CallManager callManager = new NoProblem.Core.BusinessLogic.Dialer.CallManager(null, null);
                    var auctionResult = new DataModel.AuctionResult();
                    auctionResult.ServiceRequestId = jona.new_incidentid.Value.ToString();
                    var providersList = new List<NoProblem.Core.DataModel.AuctionProvider>();
                    var callsList =
                        from c in jona.new_jonamanagement_jonacall
                        orderby c.new_bid.Value descending
                        select c;
                    int winners = 0;
                    List<DataModel.Xrm.new_jonacall> calls = new List<NoProblem.Core.DataModel.Xrm.new_jonacall>();
                    foreach (var call in callsList)
                    {
                        DataModel.AuctionProvider prov = new NoProblem.Core.DataModel.AuctionProvider();
                        prov.BidValue = call.new_bid.Value;
                        prov.IsRealTimeBid = call.new_enteredrealtimebid.Value;
                        prov.ProviderId = call.new_incidentaccount_jonacall.new_account_new_incidentaccount.accountid.ToString();
                        prov.IsRejected = call.new_isrejected.HasValue ? call.new_isrejected.Value : false;
                        if (prov.IsRejected || winners >= jona.new_numsupplierstocall.Value)
                        {
                            prov.IsWinner = false;
                            call.new_status = DataModel.Jona.enumCallStatus.CALL_COMPLETED.ToString();
                            call.new_laststatustimestamp = DateTime.Now;
                            call.new_winner = false;
                            call.new_done = true;
                        }
                        else
                        {
                            prov.IsWinner = true;
                            call.new_status = DataModel.Jona.enumCallStatus.QUEUED.ToString();
                            call.new_laststatustimestamp = DateTime.Now;
                            call.new_done = false;
                            call.new_winner = true;
                            winners++;
                        }
                        jonaCallDal.Update(call);
                        localContext.SaveChanges();
                        providersList.Add(prov);
                        calls.Add(call);
                    }
                    auctionResult.Providers = providersList.ToArray();
                    callManager.AuctionCompleted(auctionResult);
                    InitiateServiceRequest(jona, localContext, calls);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in JonaManager.CheckIfTtsDone. jonaId = {0}", ((TimerContainer)timerContainerObj).JonaId);
            }
        }

        private void InitiateServiceRequest(DataModel.Xrm.new_jonamanagement jona, DataModel.Xrm.DataContext localContext, List<DataModel.Xrm.new_jonacall> calls)
        {
            DataAccessLayer.JonaManagement jonaDal = new NoProblem.Core.DataAccessLayer.JonaManagement(localContext);
            jona.new_consumerfailedtries = 0;
            jona.new_status = "SR_STAGE";
            jonaDal.Update(jona);
            localContext.SaveChanges();

            var firstCall = calls.Where(x => !x.new_done.HasValue || !x.new_done.Value).OrderByDescending(x => x.new_bid.Value).FirstOrDefault();
            if (firstCall != null)
            {
                var incident = jona.new_incident_jonamanagement;
                bool ttsForJona = jona.new_ttsforjona.HasValue ? jona.new_ttsforjona.Value : false;
                bool rejectEnabled = jona.new_rejectenabled.HasValue ? jona.new_rejectenabled.Value : false;
                bool callSent = ExecuteCall(firstCall, incident.new_telephone1, incident.description, new DataAccessLayer.JonaCall(localContext), new DataAccessLayer.IncidentAccount(localContext), ttsForJona, rejectEnabled, jona.new_jonamanagementid);
                if (!callSent)
                {
                    FinishJona(localContext, jona, 4);//4 is error.
                }
            }
            else
            {
                ContinueJona(jona.new_jonamanagementid);
            }
        }

        private bool ExecuteCall(DataModel.Xrm.new_jonacall call, string consumerPhone, string description, DataAccessLayer.JonaCall jonaCallDal, DataAccessLayer.IncidentAccount incAccDal, bool ttsForJona, bool rejectEnabled, Guid jonaId)
        {
            var incAcc = call.new_incidentaccount_jonacall;
            var supplier = incAcc.new_account_new_incidentaccount;
            string phone = supplier.telephone1;
            DataAccessLayer.ConfigurationSettings settingsDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            bool publisherRecordsCalls = settingsDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.RECORD_CALLS) == "1";
            bool recordCall = publisherRecordsCalls && supplier.new_recordcalls.HasValue ? supplier.new_recordcalls.Value : false;
            rejectEnabled = rejectEnabled && (!call.new_isrealtimebidder.HasValue || !call.new_isrealtimebidder.Value);
            AsteriskBL.AsteriskCallsExecuter executer = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskCallsExecuter(null);
            if (!ttsForJona || !rejectEnabled)
            {
                description = null;
            }
            string countryPrefix = settingsDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.COUNTRY_PHONE_PREFIX);
            string internalPrefix = settingsDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INTERNAL_PHONE_PREFIX);
            phone = Phones.Deprecated_AppendPhonePrefix(countryPrefix, internalPrefix, supplier.telephone1);
            consumerPhone = Phones.Deprecated_AppendPhonePrefix(countryPrefix, internalPrefix, consumerPhone);
            DataAccessLayer.PrimaryExpertise headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(settingsDal.XrmDataContext);
            string headingIvrName = headingDal.GetIvrName(call.new_incidentaccountid.Value.ToString());
            string asteriskCallId = executer.InitiateC2c(consumerPhone, phone, recordCall, description, rejectEnabled, jonaId, headingIvrName, false);
            if (!string.IsNullOrEmpty(asteriskCallId))
            {
                call.new_callid = asteriskCallId;
                jonaCallDal.Update(call);
                jonaCallDal.XrmDataContext.SaveChanges();
                incAcc.new_dialercallid = asteriskCallId;
                incAccDal.Update(incAcc);
                incAccDal.XrmDataContext.SaveChanges();

                //create jona call cdr that points to call and has asterisk id asteriskCallId.
                DataAccessLayer.JonaCallCDR cdrDal = new NoProblem.Core.DataAccessLayer.JonaCallCDR(incAccDal.XrmDataContext);
                DataModel.Xrm.new_jonacallcdr cdr = new NoProblem.Core.DataModel.Xrm.new_jonacallcdr();
                cdr.new_callid = asteriskCallId;
                cdr.new_jonacallid = call.new_jonacallid;
                cdrDal.Create(cdr);
                cdrDal.XrmDataContext.SaveChanges();

                return true;
            }
            else
            {
                //Handle error sending call to asterisk.
                Dialer.CallManager callManager = new NoProblem.Core.BusinessLogic.Dialer.CallManager(null, null);
                callManager.CallStatus(
                    call.new_jonamanagement_jonacall.new_incidentid.Value,
                    supplier.accountid,
                    (int)enumCallStatus.ERROR);
                return false;
            }
        }

        private void ContinueJonaAsync(Guid jonaId)
        {
            Thread tr = new Thread(new ParameterizedThreadStart(
                (object obj) =>
                {
                    try
                    {
                        ContinueJona((Guid)obj);
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception in JonaManager.ContinueJonaAsync. JonaId = {0}", jonaId);
                    }
                }));
            tr.Start(jonaId);
        }

        private void ContinueJona(Guid jonaId)
        {
            DataAccessLayer.JonaManagement dal = new NoProblem.Core.DataAccessLayer.JonaManagement(null);
            var localContext = dal.XrmDataContext;
            DataAccessLayer.JonaCall callDal = new NoProblem.Core.DataAccessLayer.JonaCall(localContext);//clear cache for jona call table.
            var jona = dal.Retrieve(jonaId);
            bool allOk = true;
            bool allFinished = true;
            foreach (var call in jona.new_jonamanagement_jonacall)
            {
                var callStatus = ((enumCallStatus)Enum.Parse(typeof(enumCallStatus), call.new_status));
                if (callStatus == enumCallStatus.QUEUED)
                {
                    allFinished = false;
                    break;
                }
                if (callStatus != enumCallStatus.CALL_COMPLETED)
                {
                    allOk = false;
                }
            }
            if (allFinished)
            {
                int status = allOk ? 0 : 1;
                jona.new_status = "COMPLETED";
                dal.Update(jona);
                localContext.SaveChanges();
                FinishJona(localContext, jona, status);
            }
            else
            {
                var call = jona.new_jonamanagement_jonacall.Where(x => !x.new_done.HasValue || !x.new_done.Value).OrderByDescending(x => x.new_bid).First();
                var incident = jona.new_incident_jonamanagement;
                bool ttsForJona = jona.new_ttsforjona.HasValue ? jona.new_ttsforjona.Value : false;
                bool rejectEnabled = jona.new_rejectenabled.HasValue ? jona.new_rejectenabled.Value : false;
                bool callSent = ExecuteCall(call, incident.new_telephone1, incident.description, new DataAccessLayer.JonaCall(localContext), new DataAccessLayer.IncidentAccount(localContext), ttsForJona, rejectEnabled, jonaId);
                if (!callSent)
                {
                    FinishJona(localContext, jona, 4);//4 = error
                }
            }
        }

        private void FinishJona(NoProblem.Core.DataModel.Xrm.DataContext localContext, NoProblem.Core.DataModel.Xrm.new_jonamanagement jona, int status)
        {
            Dialer.PhoneManager phoneManager = new NoProblem.Core.BusinessLogic.Dialer.PhoneManager(localContext);
            CreateCdrsDelegate del = new CreateCdrsDelegate(CreateCdrs);
            del.BeginInvoke(jona, null, null);
            GetRecordingsDelegate recordingsDel = new GetRecordingsDelegate(GetRecordings);
            recordingsDel.BeginInvoke(jona, null, null);
            phoneManager.ServiceRequestCompleted(jona.new_incident_jonamanagement.incidentid.ToString(), status, null);
        }

        private void CreateCdrs(DataModel.Xrm.new_jonamanagement jona)
        {
            try
            {
                DataAccessLayer.JonaCall callDal = new NoProblem.Core.DataAccessLayer.JonaCall(null);
                DataModel.Xrm.DataContext localContext = callDal.XrmDataContext;
                DataAccessLayer.JonaCallCDR cdrDal = new NoProblem.Core.DataAccessLayer.JonaCallCDR(localContext);
                DataAccessLayer.CallDetailRecord detailRecordDal = new NoProblem.Core.DataAccessLayer.CallDetailRecord(localContext);
                DataAccessLayer.JonaCall callDall = new NoProblem.Core.DataAccessLayer.JonaCall(localContext);

                foreach (var call in jona.new_jonamanagement_jonacall)
                {
                    CreateCallDetailRecord(detailRecordDal, call.new_ttscallstart, call.new_ttscallend, DataModel.Xrm.new_calldetailrecord.Type.tts, call.new_incidentaccountid);
                    foreach (var cdr in call.new_jonacall_jonacallcdr)
                    {
                        CreateCallDetailRecord(detailRecordDal, cdr.new_callstart, cdr.new_callend, DataModel.Xrm.new_calldetailrecord.Type.jona_advertiser, call.new_incidentaccountid);
                        CreateCallDetailRecord(detailRecordDal, cdr.new_consumercallstart, cdr.new_callend, DataModel.Xrm.new_calldetailrecord.Type.jona_consumer, call.new_incidentaccountid);
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception creating Call Detail Records for JonaManagement {0}", jona.new_jonamanagementid);
            }
        }

        private static void CreateCallDetailRecord(DataAccessLayer.CallDetailRecord detailRecordDal, DateTime? callStart, DateTime? callEnd, DataModel.Xrm.new_calldetailrecord.Type type, Guid? incidentAccountId)
        {
            if (callStart.HasValue && callEnd.HasValue)
            {
                DataModel.Xrm.new_calldetailrecord callDetailRecord = new NoProblem.Core.DataModel.Xrm.new_calldetailrecord();
                callDetailRecord.new_type = (int)type;
                callDetailRecord.new_trunk = "Astrisk Direct Jona";
                callDetailRecord.new_name = type.ToString();
                callDetailRecord.new_timeofcall = callStart.Value;
                callDetailRecord.new_duration = (int)(callEnd.Value - callStart.Value).TotalSeconds;
                callDetailRecord.new_incidentaccountid = incidentAccountId;
                detailRecordDal.Create(callDetailRecord);
                detailRecordDal.XrmDataContext.SaveChanges();
            }
        }

        private enumCallStatus GetNextStatus(DataModel.Jona.enumCallStatus currentStatus, DataModel.Asterisk.C2cStatusContainer statusContainer)
        {
            enumCallStatus retStatus = enumCallStatus.UNKNOWN;
            string side = statusContainer.Side;
            string status = statusContainer.Status;
            if (status == C2cStatusCode.FAILED)
                currentStatus = enumCallStatus.ERROR;
            string action = side + status;
            switch (currentStatus)
            {
                case DataModel.Jona.enumCallStatus.QUEUED:
                    if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ACK)
                        retStatus = enumCallStatus.QUEUED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.BUSY)
                        retStatus = enumCallStatus.ADVERTISER_BUSY;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.NO_ANSWER)
                        retStatus = enumCallStatus.ADVERTISER_BUSY;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ANSWER)
                        retStatus = enumCallStatus.ADVERTISER_ANSWERED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.IN_PROGRESS)
                        retStatus = enumCallStatus.ADVERTISER_ANSWERED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.FAILED)
                        retStatus = enumCallStatus.ADVERTISER_NOT_CONFIRMED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.CANCEL)
                        retStatus = enumCallStatus.ADVERTISER_NOT_CONFIRMED;
                    else
                        retStatus = enumCallStatus.QUEUED;
                    break;
                case DataModel.Jona.enumCallStatus.ADVERTISER_ANSWERED:
                    if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.DONE || action == C2cStatusContainer.CON_SIDE + C2cStatusCode.IN_PROGRESS)
                        retStatus = enumCallStatus.ADVERTISER_NOT_CONFIRMED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ANSWER)
                        retStatus = enumCallStatus.ADVERTISER_ANSWERED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.APPROVED)
                        retStatus = enumCallStatus.ADVERTISER_CONFIRMED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ACK) //Found to occur when advertiser and consumer phone are the same 
                        retStatus = enumCallStatus.ADVERTISER_BUSY;
                    else if (action == C2cStatusContainer.ADV_SIDE + enumCallStatus.REJECTED)
                        retStatus = enumCallStatus.REJECTED;
                    break;
                case DataModel.Jona.enumCallStatus.ADVERTISER_CONFIRMED:
                    if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.IN_PROGRESS)
                        retStatus = enumCallStatus.CALL_IN_PROGRESS;
                    else if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.FAILED)
                        retStatus = enumCallStatus.CUSTOMER_BUSY; //Should be ERROR. But not supported in this version by UI.
                    else if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.BUSY)
                        retStatus = enumCallStatus.CUSTOMER_BUSY;
                    else if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.NO_ANSWER)
                        retStatus = enumCallStatus.CUSTOMER_BUSY;
                    else if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.NO_ANSWER)
                        retStatus = enumCallStatus.CUSTOMER_BUSY;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ANSWER)
                        retStatus = enumCallStatus.ADVERTISER_CONFIRMED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.DONE) //Probably because recevied before busy message
                        retStatus = enumCallStatus.CUSTOMER_BUSY; //retStatus = enumCallStatus.ADVERTISER_CONFIRMED;
                    else if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.CANCEL) //Consumer disconnected immediatly.
                        retStatus = enumCallStatus.CUSTOMER_BUSY;
                    break;
                case enumCallStatus.CALL_IN_PROGRESS:
                    if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.DONE || action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.DONE)
                        retStatus = enumCallStatus.CALL_COMPLETED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ANSWER)
                        retStatus = enumCallStatus.CALL_IN_PROGRESS;
                    break;
                case enumCallStatus.REJECTED:
                case enumCallStatus.CUSTOMER_BUSY:
                case enumCallStatus.ADVERTISER_BUSY:
                case enumCallStatus.CALL_COMPLETED:
                case enumCallStatus.ADVERTISER_NOT_CONFIRMED:
                case enumCallStatus.ERROR:
                    retStatus = currentStatus;
                    break;
            }

            if (retStatus == enumCallStatus.UNKNOWN)
            {
                LogUtils.MyHandle.WriteToLog(
                    "Unkwown status recevied from Linesip in JonaManager.GetNextStatus for session {0} current status: {1}, status from Atelis: {2}",
                    statusContainer.Session, currentStatus, action);
            }
            if (retStatus == enumCallStatus.ERROR)
            {
                LogUtils.MyHandle.WriteToLog(
                    "Received FAILED status from Linesip in JonaManager.GetNextStatus for session {0}. status received: {1}",
                    statusContainer.Session, action);
            }
            return retStatus;
        }

        private enumTtsStatus getNextTtsStatus(enumTtsStatus currentStatus, DataModel.Asterisk.TtsStatusContainer statusContainer)
        {
            enumTtsStatus retStatus = enumTtsStatus.UNKNOWN;
            String status = statusContainer.Status;
            String action = status;
            String digit = "";
            if (statusContainer.Digit != null)
                digit = statusContainer.Digit;
            switch (currentStatus)
            {
                case enumTtsStatus.QUEUED:
                case enumTtsStatus.ADVERTISER_ANSWERED:
                    if (digit.Equals("c", StringComparison.OrdinalIgnoreCase))
                        retStatus = enumTtsStatus.REJECT;
                    else if (action == TtsStatusCode.BUSY)
                        retStatus = enumTtsStatus.BUSY;
                    else if (action == TtsStatusCode.ADVERTISER_ANSWERED)
                        retStatus = enumTtsStatus.ADVERTISER_ANSWERED;
                    else if (action == TtsStatusCode.NO_ANSWER)
                        retStatus = enumTtsStatus.BUSY;
                    else if (action == TtsStatusCode.FAILED)
                        retStatus = enumTtsStatus.NOT_CONFIRMED;
                    else if (action == TtsStatusCode.CANCEL)
                        retStatus = enumTtsStatus.NOT_CONFIRMED;
                    else if (action == TtsStatusCode.HANGUP && (digit == null || digit.Length == 0))
                        retStatus = enumTtsStatus.NOT_CONFIRMED;
                    else if (action == TtsStatusCode.ANSWER && (digit == null || digit.Length == 0))
                        retStatus = enumTtsStatus.NOT_CONFIRMED;
                    else if ((action == TtsStatusCode.ANSWER || action == TtsStatusCode.HANGUP) && digit != null && digit.ToUpper().CompareTo("F") == 0)
                        retStatus = enumTtsStatus.NOT_CONFIRMED;
                    else if ((action == TtsStatusCode.ANSWER || action == TtsStatusCode.HANGUP) && digit != null && digit.Length > 0)
                        retStatus = enumTtsStatus.CONFIRMED;

                    break;
                case enumTtsStatus.ERROR:
                    retStatus = currentStatus;
                    break;
            }

            if (retStatus == enumTtsStatus.UNKNOWN)
            {
                LogUtils.MyHandle.WriteToLog(
                    "Unkwown status recevied from Atelis in JonaManager.getNextTtsStatus. Session: {0}, Current status: {1}, Status from Atelis: {2}",
                    statusContainer.Internal_Id, currentStatus, action);
            }
            return retStatus;
        }

        private void GetRecordings(DataModel.Xrm.new_jonamanagement jona)
        {
            try
            {
                DataAccessLayer.JonaCall callDal = new NoProblem.Core.DataAccessLayer.JonaCall(null);
                DataModel.Xrm.DataContext localContext = callDal.XrmDataContext;
                DataAccessLayer.IncidentAccount incAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(localContext);
                foreach (var call in jona.new_jonamanagement_jonacall)
                {
                    if (!string.IsNullOrEmpty(call.new_callid))
                    {
                        var incAcc = call.new_incidentaccount_jonacall;
                        var supplier = incAcc.new_account_new_incidentaccount;
                        if (supplier.new_recordcalls.HasValue && supplier.new_recordcalls.Value)
                        {
                            SetupRecordingRetry(call.new_callid, incAcc.new_incidentaccountid, call.new_sessionserver, 1);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in async method JonaManager.GetRecordings. JonaManagementId={0}", jona.new_jonamanagementid);
            }
        }

        private void SetupRecordingRetry(string asteriskSessionId, Guid incidentAccountId, string sessionServer, int tryCount)
        {
            NoProblem.Core.DataModel.Jona.RecordingsRetryContainerJona container = new NoProblem.Core.DataModel.Jona.RecordingsRetryContainerJona();
            container.AsteriskSessionId = asteriskSessionId;
            container.IncidentAccountId = incidentAccountId;
            container.SessionServer = sessionServer;
            container.TryCount = tryCount;
            container.Timer = new Timer(
                new TimerCallback(RetryRetrieveRecording),
                container,
                tryCount == 1 ? 60000 : 20000,
                Timeout.Infinite);
            recordingsRetriesHolder.Add(container);
        }

        private void RetryRetrieveRecording(object obj)
        {
            RecordingsRetryContainerJona container = (RecordingsRetryContainerJona)obj;
            try
            {
                recordingsRetriesHolder.Remove(container);
                container.Timer.Dispose();
                AsteriskBL.AsteriskCallsExecuter executer = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskCallsExecuter(null);
                bool alreadyExisted;
                string file = executer.RetrieveRecording(container.AsteriskSessionId, container.SessionServer, out alreadyExisted);
                if (!alreadyExisted)
                {
                    if (!string.IsNullOrEmpty(file))
                    {
                        DataAccessLayer.IncidentAccount incAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(executer.XrmDataContext);
                        incAccDal.UpdateRecordFileLocation(container.IncidentAccountId, file, 0);
                        executer.DeleteRecordingInAsteriskServer(container.AsteriskSessionId);
                    }
                    else if (container.TryCount < 2)
                    {
                        SetupRecordingRetry(container.AsteriskSessionId, container.IncidentAccountId, container.SessionServer, ++container.TryCount);
                    }
                    else
                    {
                        LogUtils.MyHandle.WriteToLog("Failed to retrieve recording file after 2 tries. asteriskSessionId={0}", container.AsteriskSessionId);
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in JonaManager.RetryRetrieveRecording. asteriskSessionId={0}", container.AsteriskSessionId);
            }
        }

        #endregion
    }
}
