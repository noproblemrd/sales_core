﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.LeadBuyers;
using Effect.Crm.Logs;
using System.Threading;
using NoProblem.Core.BusinessLogic.Utils;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class BiddingManager : XrmUserBase
    {
        static BiddingManager()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            MOBILE_BID_TIMEOUT = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_BID_TIMEOUT));
            MOBILE_DEFAULT_MIN_PRICE = decimal.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_DEFAULT_MIN_PRICE));
            MOBILE_DEFAULT_MED_PRICE = decimal.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_DEFAULT_MED_PRICE));
            MOBILE_DEFAULT_MAX_PRICE = decimal.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_DEFAULT_MAX_PRICE));
        }
        public BiddingManager(DataModel.Xrm.DataContext xrmDataContext) :
            base(xrmDataContext)
        {
        }
        private static readonly int MOBILE_BID_TIMEOUT;
        private static readonly decimal MOBILE_DEFAULT_MIN_PRICE;
        private static readonly decimal MOBILE_DEFAULT_MED_PRICE;
        private static readonly decimal MOBILE_DEFAULT_MAX_PRICE;

        private static SynchronizedCollection<TimerContainer> ttsTimersHolder = new System.Collections.Generic.SynchronizedCollection<TimerContainer>();
        private static TtsFileCreator ttsFileCreator = new TtsFileCreator();
        private const string BID_LOWER_THAN_MINIMUN_BROKER_BID_MSG = "The bid {2} is lower than {0}, the minimum broker bid for {1}.";
        private const string EXPECTED_NON_API = "expected_non_api";
        private const int BID_TIMEOUT = 70;
        private const string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        private static DataAccessLayer.Generic genericDal = new NoProblem.Core.DataAccessLayer.Generic();

        public DataModel.SupplierModel.Mobile.SetBidForLeadResponse SetBidForLead(DataModel.SupplierModel.Mobile.SetBidForLeadRequest request)
        {
            DataModel.SupplierModel.Mobile.SetBidForLeadResponse response = new DataModel.SupplierModel.Mobile.SetBidForLeadResponse();
            MobileBidManager mbm = new MobileBidManager(request);
            if(!mbm.ValidBid)
            {
                response.Ok = false;
                response.FaildMessage = mbm.FaildMessage;
                if(mbm.FaildMessage != NoProblem.Core.DataModel.SupplierModel.Mobile.SetBidForLeadResponse.BidForLeadResponseMessage.DUPLICATE 
                    && request.Status == DataModel.SupplierModel.Mobile.SetBidForLeadRequest.StatusOptions.REJECTED)
                    RejectBid(request.IncidentAccountId);
                return response;
            }
            response.Ok = true;
            if(request.Status == DataModel.SupplierModel.Mobile.SetBidForLeadRequest.StatusOptions.REJECTED)
                    RejectBid(request.IncidentAccountId);
            else if(request.Price == 0)
            {
                UpdateMobileBid(request.IncidentAccountId, request.Price, request.IP, request.UserAgent, request.Host);
                ServiceRequestManager srm = new ServiceRequestManager(XrmDataContext);
                srm.SendWInMobileLead(mbm.IncidentAccountId, mbm.IncidentId, request);
              //  return response;
            }
            else if(mbm.incidentStatus == DataModel.Xrm.incident.Status.NEW)
            {
                UpdateMobileBid(request.IncidentAccountId, request.Price, (int)DataModel.Xrm.new_incidentaccount.Status.IN_AUCTION, request.IP, request.UserAgent, request.Host);
                SetIncidentToInAuction(mbm.IncidentId);
                //start to counting auction
                TimerContainer timerContainer = new TimerContainer();
                ttsTimersHolder.Add(timerContainer);
                timerContainer.incidentId = mbm.IncidentId;
                timerContainer.Timer = new Timer(
                    new TimerCallback(MobileBiddingTimeout),
                    timerContainer,
                    (MOBILE_BID_TIMEOUT * 1000),
                    System.Threading.Timeout.Infinite);
            }
            else if(mbm.incidentStatus == DataModel.Xrm.incident.Status.WAITING)
            {
                UpdateMobileBid(request.IncidentAccountId, request.Price, (int)DataModel.Xrm.new_incidentaccount.Status.IN_AUCTION, request.IP, request.UserAgent, request.Host);
            }
            else
            {
                UpdateMobileBid(request.IncidentAccountId, request.Price, request.IP, request.UserAgent, request.Host);
                ServiceRequestManager srm = new ServiceRequestManager(XrmDataContext);
                srm.SendWInMobileLead(mbm.IncidentAccountId, mbm.IncidentId, request);
            }
            return response;
            /*
     //       bool isValidBid = CheckValidMobileBid(request, out FaildMessage);
     //       MarkBidDone(request.IncidentAccountId);
   //         DataModel.SupplierModel.Mobile.SetBidForLeadResponse response = new DataModel.SupplierModel.Mobile.SetBidForLeadResponse();
  //          if (!isValidBid)
  //          {
  //              response.Ok = false;
   //         }
  //          else if (request.Status != DataModel.SupplierModel.Mobile.SetBidForLeadRequest.StatusOptions.MISSED)
            {
                UpdateBidCallEnd(request.IncidentAccountId, request.Price, true, request.Status == DataModel.SupplierModel.Mobile.SetBidForLeadRequest.StatusOptions.REJECTED);
            }
            return response;
             * */

        }
        private void SetIncidentToInAuction(Guid incidentId)
        {
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DataModel.Xrm.incident incident = incidentDal.Retrieve(incidentId);
            incident.statuscode = (int)DataModel.Xrm.incident.Status.WAITING;
            incident.new_startauctionon = DateTime.UtcNow;
            incidentDal.Update(incident);
            XrmDataContext.SaveChanges();
        }
        /*
        private bool CheckValidBid(DataModel.SupplierModel.Mobile.SetBidForLeadRequest request)
        {
            decimal maxPrice = GetMaxBid();
            if (request.Price > maxPrice)
                return false;
            DataAccessLayer.IncidentAccount iaDal = new DataAccessLayer.IncidentAccount(XrmDataContext);
            var incAcc = (from ia in iaDal.All
                          where ia.new_incidentaccountid == request.IncidentAccountId
                          select new { incidentId = ia.new_incidentid.Value, currentPrice = ia.new_potentialincidentprice.Value, status = ia.statuscode.Value, bidDone = ia.new_biddone }
                     ).First();
            if (incAcc == null)
                return false;
            if (incAcc.bidDone.IsTrue())
                return false;
            if (incAcc.status != (int)DataModel.Xrm.new_incidentaccount.Status.MATCH
                && incAcc.status != (int)DataModel.Xrm.new_incidentaccount.Status.IN_AUCTION)
                return false;
            //if (incAcc.currentPrice > request.Price)
            //    return false;
            return true;
        }
        */
        internal void StartBidding(Guid incidentId)
        {
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DataModel.Xrm.incident incident = incidentDal.Retrieve(incidentId);
            DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataAccessLayer.AccountExpertise acExDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            decimal? minBrokerBid = incident.new_new_primaryexpertise_incident.new_minimumbidpriceforbrokers;
            string headingName = incident.new_new_primaryexpertise_incident.new_name;
            if (headingName == null)
                headingName = String.Empty;
            if (!minBrokerBid.HasValue)
                minBrokerBid = 0;
            ServiceRequest.ServiceRequestManager srManager = new ServiceRequestManager(XrmDataContext);
            var incidentAccountList = incident.new_incident_new_incidentaccount;
            List<DataModel.Xrm.new_incidentaccount> regularAdvertiserIas = new List<NoProblem.Core.DataModel.Xrm.new_incidentaccount>();
            foreach (DataModel.Xrm.new_incidentaccount ia in incidentAccountList)
            {
                var supplier = ia.new_account_new_incidentaccount;
                if (supplier.new_isleadbuyer.HasValue
                       && supplier.new_isleadbuyer.Value)
                {
                    DispatchBidToApiPartner(incident, incidentAccountDal, acExDal, minBrokerBid, headingName, srManager, ia, supplier);
                }
                else
                {
                    if (incident.casetypecode == (int)NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode.Video)
                    {
                        if (ia.statuscode == (int)DataModel.Xrm.new_incidentaccount.Status.MATCH)
                            regularAdvertiserIas.Add(ia);
                    }
                    else
                        regularAdvertiserIas.Add(ia);
                }
            }
            if (regularAdvertiserIas.Count == 0)
            {
                srManager.ContinueRequestAfterBidding(incidentId);
            }
            else
            {
                bool oneCallSent = false;
                
                
                decimal minPrice = incident.new_new_primaryexpertise_incident.new_minprice.HasValue ? incident.new_new_primaryexpertise_incident.new_minprice.Value : 5;
                decimal maxPrice = GetMaxBid(minPrice);
                
                DataAccessLayer.MobileDevice mobileDal = new DataAccessLayer.MobileDevice(XrmDataContext);
                PushNotifications.NpPushNotifier pushNotifier = new PushNotifications.NpPushNotifier(XrmDataContext);
                string customerCityAndState = BuildCityAndStateString(incident.new_new_region_incident);
                string hiddenPhone = BuildHiddenPhone(incident.new_telephone1);
                string hiddenName = BuildHiddenName(incident.customerid.name);
                if (incident.casetypecode == (int)NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode.Video)
                {
                    foreach (DataModel.Xrm.new_incidentaccount ia in regularAdvertiserIas)
                    {
                        DipatchBidToMobileAdvertiser(incident, incidentAccountDal, acExDal, ref oneCallSent, pushNotifier, ia, customerCityAndState, hiddenPhone, 
                            hiddenName);
                    }
                    /*/setup timer
                    TimerContainer timerContainer = new TimerContainer();
                    ttsTimersHolder.Add(timerContainer);
                    timerContainer.incidentId = incidentId;
                    timerContainer.Timer = new Timer(
                        new TimerCallback(MobileBiddingTimeout),
                        timerContainer,
                        (MOBILE_BID_TIMEOUT * 1000),
                        System.Threading.Timeout.Infinite);
                     */
                }
                else
                {
                    string ttsFileUrl = CreateTtsFileUrl(incident);
                    FoneApiBL.Handlers.BidCall.IBidCallHandler bidCallHandler;
                    bidCallHandler = new NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.BidCall.BidCall2014.BidCall2014Handler();
                    foreach (DataModel.Xrm.new_incidentaccount ia in regularAdvertiserIas)
                    {
                        DipatchBidToRegularAdvertiser(incident, incidentAccountDal, acExDal, ref oneCallSent, ttsFileUrl, bidCallHandler, minPrice, 
                            maxPrice, mobileDal, pushNotifier, ia, customerCityAndState, hiddenPhone, hiddenName);
                    }

                    if (!oneCallSent)
                    {
                        srManager.ContinueRequestAfterBidding(incidentId);
                    }
                    else
                    {
                        //setup timer
                        TimerContainer timerContainer = new TimerContainer();
                        ttsTimersHolder.Add(timerContainer);
                        timerContainer.incidentId = incidentId;
                        timerContainer.Timer = new Timer(
                            new TimerCallback(CheckIfBiddingIsDone),
                            timerContainer,
                            10000,
                            30000);
                    }
                }
                
            }
        }

        private string BuildHiddenPhone(string phone)
        {
            if (phone.Length > 6)
            {
                return phone.Substring(0, 3) + "-" + phone.Substring(3, 3) + "-****";
            }
            return "****";
        }

        private string BuildHiddenName(string name)
        {
            if(string.IsNullOrEmpty(name))
                return "*****";
            string[] splitted = name.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            if (splitted.Length > 1)
            {
                return splitted[0] + " *****";
            }
            return "*****";
        }

        private string BuildCityAndStateString(DataModel.Xrm.new_region region)
        {
            return BuildCityAndStateString(region.new_englishname);
        }

        private string BuildCityAndStateString(string fullNameWithParents)
        {
            string[] splitted = fullNameWithParents.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (splitted.Length == 4)
            {
                return splitted[1] + ", " + splitted[3];
            }
            return "****";
        }

        private void DipatchBidToRegularAdvertiser(DataModel.Xrm.incident incident,
            DataAccessLayer.IncidentAccount incidentAccountDal,
            DataAccessLayer.AccountExpertise acExDal,
            ref bool oneCallSent,
            string ttsFileUrl,
            FoneApiBL.Handlers.BidCall.IBidCallHandler bidCallHandler,
            decimal minPrice,
            decimal maxPrice,
            DataAccessLayer.MobileDevice mobileDal,
            PushNotifications.NpPushNotifier pushNotifier,
            DataModel.Xrm.new_incidentaccount ia,
            string cityAndState,
            string hiddenPhone,
            string hiddenName)
        {
            var supplier = ia.new_account_new_incidentaccount;
            var acEx = acExDal.GetByAccountAndPrimaryExpertise(supplier.accountid, incident.new_primaryexpertiseid.Value);
            
            decimal defaultPrice = ValidateDefaultBid(minPrice, maxPrice, acEx.new_incidentprice.Value);
            DateTime bidTimeOutUTC = DateTime.Now.AddSeconds(BID_TIMEOUT);
            bool userBidSent = false;
            bool isSendFailure = false;
            /*
            if (incident.casetypecode == (int)NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode.Video)
            {
                var devices = GetMobileDevices(mobileDal, supplier);
                bool useMobile = devices.Count() > 0;
                if (useMobile)
                {
                    if (!supplier.new_stop_mobile_notifications_until.HasValue || supplier.new_stop_mobile_notifications_until.Value <= DateTime.Now)
                    {
                        var activeDevice = FindActiveDevice(devices);
                        if (activeDevice != null)
                        {
                            bool pushOk = SendPushNotification(incident, incidentAccountDal, pushNotifier, ia, activeDevice, minPrice, maxPrice, defaultPrice, bidTimeOutUTC, cityAndState, hiddenPhone, hiddenName);
                            if (pushOk)
                            {
                                oneCallSent = true;
                                userBidSent = true;
               //                 SetBidTimeoutInIncidentAccount(ia, bidTimeOutUTC, incidentAccountDal);
                            }
                            else
                            {
                                isSendFailure = true;
                            }
                        }
                    }
                }                
            }
            else//user with no mobile app
            {
             */
            if (!String.IsNullOrEmpty(ttsFileUrl))
            {
                //click to call
                bidCallHandler.StartCall(ttsFileUrl, ia.new_incidentaccountid, maxPrice, minPrice, true, supplier.telephone1, supplier.new_country);
                oneCallSent = true;
                userBidSent = true;
                SetBidTimeoutInIncidentAccount(ia, bidTimeOutUTC, incidentAccountDal);
            }
            else
            {
                isSendFailure = true;
            }
       //     }
            if (!userBidSent)
            {
                ia.new_biddone = true;
                ia.new_bid_send_failed = isSendFailure;
                incidentAccountDal.Update(ia);
                XrmDataContext.SaveChanges();
            }
        }
        private void DipatchBidToMobileAdvertiser(DataModel.Xrm.incident incident,
           DataAccessLayer.IncidentAccount incidentAccountDal,
           DataAccessLayer.AccountExpertise acExDal,
           ref bool oneCallSent,           
           PushNotifications.NpPushNotifier pushNotifier,
           DataModel.Xrm.new_incidentaccount ia,
           string cityAndState,
           string hiddenPhone,
           string hiddenName)
        {
            DataAccessLayer.MobileDevice mobileDal = new DataAccessLayer.MobileDevice(XrmDataContext);
            var supplier = ia.new_account_new_incidentaccount;
            var acEx = acExDal.GetByAccountAndPrimaryExpertise(supplier.accountid, incident.new_primaryexpertiseid.Value);
            bool userBidSent = false;
            bool isSendFailure = false;
            decimal MobileMaxPrice = incident.new_new_primaryexpertise_incident.new_mobilemaxprice.HasValue ? incident.new_new_primaryexpertise_incident.new_mobilemaxprice.Value :
                MOBILE_DEFAULT_MAX_PRICE;
            decimal MobileMinPrice = incident.new_new_primaryexpertise_incident.new_mobileminprice.HasValue ? incident.new_new_primaryexpertise_incident.new_mobileminprice.Value :
                MOBILE_DEFAULT_MIN_PRICE;
            decimal MobileMedPrice = incident.new_new_primaryexpertise_incident.new_mobilemedprice.HasValue ? incident.new_new_primaryexpertise_incident.new_mobilemedprice.Value :
                MOBILE_DEFAULT_MED_PRICE;
            
            var devices = GetMobileDevices(mobileDal, supplier);
            bool useMobile = devices.Count() > 0;
            if (useMobile)
            {
                if (!supplier.new_stop_mobile_notifications_until.HasValue || supplier.new_stop_mobile_notifications_until.Value <= DateTime.Now)
                {
                    var activeDevice = FindActiveDevice(devices);
                    if (activeDevice != null)
                    {
                        bool pushOk = SendPushNotification(incident, incidentAccountDal, pushNotifier, ia, activeDevice, MobileMinPrice, MobileMaxPrice,
                            MobileMedPrice, cityAndState, hiddenPhone, hiddenName, false);
                        if (pushOk)
                        {
                            oneCallSent = true;
                            userBidSent = true;
                            //                 SetBidTimeoutInIncidentAccount(ia, bidTimeOutUTC, incidentAccountDal);
                        }
                        else
                        {
                            isSendFailure = true;
                        }
                    }
                }
            }
            if (!userBidSent)
            {
             //   ia.new_biddone = true;
                ia.new_bid_send_failed = isSendFailure;
                incidentAccountDal.Update(ia);
                XrmDataContext.SaveChanges();
            }    
        }
        private DataModel.Xrm.new_mobiledevice FindActiveDevice(IEnumerable<DataModel.Xrm.new_mobiledevice> devices)
        {
            return devices.FirstOrDefault(x => x.new_isactive == true);
        }

        private static decimal ValidateDefaultBid(decimal minPrice, decimal maxPrice, decimal defaultPrice)
        {
            if (defaultPrice < minPrice)
                defaultPrice = minPrice;
            else if (defaultPrice > maxPrice)
                defaultPrice = maxPrice;
            return defaultPrice;
        }

        private void DispatchBidToApiPartner(DataModel.Xrm.incident incident, DataAccessLayer.IncidentAccount incidentAccountDal, DataAccessLayer.AccountExpertise acExDal, decimal? minBrokerBid, string headingName, ServiceRequest.ServiceRequestManager srManager, DataModel.Xrm.new_incidentaccount ia, DataModel.Xrm.account supplier)
        {
            var acEx = acExDal.GetByAccountAndPrimaryExpertise(supplier.accountid, incident.new_primaryexpertiseid.Value);
            var bidResponse = GetBrokerBid(supplier, incident, ia, acEx);
            ia.new_biddone = true;
            ia.new_pingid = bidResponse.BrokerId;
            if (bidResponse.WantsToBid && bidResponse.Price > 0)
            {
                if (!bidResponse.IsPredefinedPrice)
                {
                    UpdateCallPrice(supplier, ia, bidResponse.Price);
                }
                if (bidResponse.Price < minBrokerBid.Value)
                {
                    ia.new_rejectedreason = String.Format(BID_LOWER_THAN_MINIMUN_BROKER_BID_MSG, minBrokerBid.Value, headingName, bidResponse.Price);
                    ia.new_dialercallid = bidResponse.BrokerId;
                    srManager.MarkIncidentAccountAsLostAfterLeadBuyerFailed(incidentAccountDal, ia);
                }
                else
                {
                    incidentAccountDal.Update(ia);
                    XrmDataContext.SaveChanges();
                }
            }
            else
            {
                ia.new_isrejected = bidResponse.MarkAsRejected;
                ia.new_dialercallid = bidResponse.BrokerId;
                ia.new_rejectedreason = bidResponse.Reason;
                srManager.MarkIncidentAccountAsLostAfterLeadBuyerFailed(incidentAccountDal, ia);
            }
        }
        //NOT IN USED
        private void SetBidTimeoutInIncidentAccount(DataModel.Xrm.new_incidentaccount ia, DateTime bidTimeOutUTC, DataAccessLayer.IncidentAccount incidentAccountDal)
        {
            ia.new_bidtimeoutat_utc = bidTimeOutUTC;
            incidentAccountDal.Update(ia);
            XrmDataContext.SaveChanges();
        }

        private decimal GetMaxBid(decimal minBid)
        {
            var maxPrice = GetMaxBid();
            if (maxPrice < minBid)
                maxPrice = minBid;
            return maxPrice;
        }

        private decimal GetMaxBid()
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
            decimal maxPrice;
            if (!decimal.TryParse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.MAX_BID), out maxPrice))
            {
                maxPrice = 99;
            }
            return maxPrice;
        }

        private bool SendPushNotification(DataModel.Xrm.incident incident, DataAccessLayer.IncidentAccount incidentAccountDal, 
            PushNotifications.NpPushNotifier pushNotifier, DataModel.Xrm.new_incidentaccount ia, DataModel.Xrm.new_mobiledevice device, 
            decimal minPrice, decimal maxPrice, decimal medPrice, string address, string phone, string customerName, bool isFreeBid)
        {
            bool ok = true;
            PushNotifications.NpPushResult pushResult;
            try
            {
                pushResult = pushNotifier.SendPushNotification(device, CreateNewBidPushDataObject(incident, ia, maxPrice, minPrice, medPrice, 
                    address, phone, customerName, isFreeBid), PushNotifications.ePushTypes.BID_REQUEST);
            }
            catch (Exception exc)
            {
                pushResult = new PushNotifications.NpPushResult() { IsSuccess = false, Message = exc.Message };
                LogUtils.MyHandle.HandleException(exc, "Exception sending push notification");
            }
            if (!pushResult.IsSuccess)
            {
                ia.new_rejectedreason = String.Format("failed to send push notification. message = {0}", pushResult.Message);
            //    ia.new_biddone = true;
                incidentAccountDal.Update(ia);
                XrmDataContext.SaveChanges();
                ok = false;
            }
            return ok;
        }

        private Dictionary<string, string> CreateNewBidPushDataObject(DataModel.Xrm.incident incident, DataModel.Xrm.new_incidentaccount ia, 
            decimal maxPrice, decimal minPrice, decimal medPrice, string address, string phone, string customerName, bool isFreeBid)
        {
            Dictionary<string, string> retVal = new Dictionary<string, string>()
                { 
                    { "leadAccountId", ia.new_incidentaccountid.ToString() },
                 //   { "description", incident.description },
                    { "maxBid", maxPrice.ToString() },
                    { "minBid", minPrice.ToString() },
                    { "medBid", medPrice.ToString() },
                    { "category", incident.new_new_primaryexpertise_incident.new_name },
                    { "zipCode", incident.new_new_region_incident.new_name },
                   // { "timeout_at_utc", bidTimeOutUtc.ToString(DATE_FORMAT) },
                    { "timeStampUtc", incident.createdon.Value.ToString(DATE_FORMAT) },
                    { "address", address },
                    { "customerName", customerName },
                    { "phoneNumber", phone },
                    { "videoUrl", incident.new_videourl},
                    { "videoPicUrl",incident.new_previewvideopicurl},
                    { "videoDuration", incident.new_videoduration.Value.ToString()},
                    { "isFreeBid", isFreeBid.ToString().ToLower()}
                };
            return retVal;
        }

        private static IEnumerable<DataModel.Xrm.new_mobiledevice> GetMobileDevices(DataAccessLayer.MobileDevice mobileDal, DataModel.Xrm.account supplier)
        {
            IEnumerable<DataModel.Xrm.new_mobiledevice> devices = (from m in mobileDal.All
                                                                   where m.new_accountid == supplier.accountid
                                                                   && m.new_isauthorized == true
                                                                   select new DataModel.Xrm.new_mobiledevice
                                                                   {
                                                                       new_os = m.new_os,
                                                                       new_uid = m.new_uid,
                                                                       new_isactive = m.new_isactive
                                                                   }
                                                      );
            return devices;
        }

        internal void UpdateBidCallEnd(Guid incidentAccountId, decimal newPrice, bool priceChanged, bool rejected)
        {
            if (rejected)
            {
                DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                var ia = dal.Retrieve(incidentAccountId);
                ServiceRequest.ServiceRequestManager srManager = new ServiceRequestManager(XrmDataContext);
                srManager.RejectCall(dal, ia);
            }
            else
            {
                DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                DataModel.Xrm.new_incidentaccount ia = null;
                if (priceChanged)
                {
                    ia = dal.Retrieve(incidentAccountId);
                    UpdateCallPrice(ia.new_account_new_incidentaccount, ia, newPrice);
                }
                if (ia == null)
                {
                    ia = new DataModel.Xrm.new_incidentaccount();
                    ia.new_incidentaccountid = incidentAccountId;
                }
                ia.new_leadaccepted = true;
                dal.Update(ia);
                XrmDataContext.SaveChanges();
            }
        }
        internal void UpdateMobileBid(Guid incidentAccountId, decimal newPrice, string IP, string UserAgent, string Host)
        {
            UpdateMobileBid(incidentAccountId, newPrice, -1, IP, UserAgent, Host);        
        }
        internal void UpdateMobileBid(Guid incidentAccountId, decimal newPrice, int _Status, string IP, string UserAgent, string Host)//NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status ToStatus)
        {
            //UpdateMobileBid(incidentAccountId, newPrice, (int)ToStatus);
            DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataModel.Xrm.new_incidentaccount incidentAccount = incidentAccountDal.Retrieve(incidentAccountId);
            UpdateMobileBid(incidentAccountDal, incidentAccount, newPrice, _Status, IP, UserAgent, Host);
        }
        internal void UpdateMobileBid(DataAccessLayer.IncidentAccount incidentAccountDal, DataModel.Xrm.new_incidentaccount incidentAccount,
            decimal newPrice, int _Status, string IP, string UserAgent, string Host)//NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status ToStatus)
        {
            UpdateCallPrice(incidentAccount.new_account_new_incidentaccount, incidentAccount, newPrice);
            incidentAccount.new_leadaccepted = true;
            incidentAccount.new_biddone = true;
            incidentAccount.new_bidip = IP;
            incidentAccount.new_biduseragent = UserAgent;
            incidentAccount.new_bidhost = Host;
            if (_Status > -1)
                incidentAccount.statuscode = _Status;
            incidentAccountDal.Update(incidentAccount);
            XrmDataContext.SaveChanges();
        }
        /*
        private void UpdateMobileBid(Guid incidentAccountId, decimal newPrice, int _Status)
        {
            DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataModel.Xrm.new_incidentaccount ia = dal.Retrieve(incidentAccountId);
            UpdateMobileBid(dal, ia, newPrice, _Status);
           
        }
        internal void UpdateMobileBid(DataAccessLayer.IncidentAccount incidentAccountDal, DataModel.Xrm.new_incidentaccount incidentAccount, 
            decimal newPrice, int _Status)
        {
            UpdateCallPrice(incidentAccount.new_account_new_incidentaccount, incidentAccount, newPrice);
            incidentAccount.new_leadaccepted = true;
            incidentAccount.new_biddone = true;
            if (_Status > -1)
                incidentAccount.statuscode = _Status;
            incidentAccountDal.Update(incidentAccount);
            XrmDataContext.SaveChanges();
        }
        */
        
        internal void MarkBidDone(Guid incidentAccountId)
        {
            DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataModel.Xrm.new_incidentaccount ia = new NoProblem.Core.DataModel.Xrm.new_incidentaccount(XrmDataContext);
            ia.new_incidentaccountid = incidentAccountId;
            ia.new_biddone = true;
            dal.Update(ia);
            XrmDataContext.SaveChanges();
        }
         internal void RejectBid(Guid incidentAccountId)
        {
            DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            var ia = dal.Retrieve(incidentAccountId);
            ServiceRequest.ServiceRequestManager srManager = new ServiceRequestManager(XrmDataContext);
            srManager.RejectCall(dal, ia);
        }
       
        private string CreateTtsFileUrl(DataModel.Xrm.incident incident)
        {
            if (GlobalConfigurations.IsDevEnvironment)
                return "http://audiofiles.ppcnp.com/qa/v12/fakering.wav";
            string ttsFileUrl = null;
            if (!String.IsNullOrEmpty(incident.description))
            {
                try
                {
                    ttsFileUrl = ttsFileCreator.GetTtsFileUrl(incident.incidentid.ToString(), incident.description);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception creating tts file in BiddingManager. The running user is " + System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                }
            }
            return ttsFileUrl;
        }

        internal static void UpdateCallPrice(NoProblem.Core.DataModel.Xrm.account supplier, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc, decimal newPrice)
        {
            Balance.BalanceManager balance = new NoProblem.Core.BusinessLogic.Balance.BalanceManager(null);
            balance.AddToAvailableBalance(supplier.accountid, incAcc.new_potentialincidentprice.Value + (newPrice * -1));
            incAcc.new_potentialincidentprice = newPrice;
            incAcc.new_isdefaultpricechanged = true;
        }

        private LeadBuyers.GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.account supplier, DataModel.Xrm.incident incident, DataModel.Xrm.new_incidentaccount ia, DataModel.Xrm.new_accountexpertise accExp)
        {
            if (supplier.new_leadbuyername == EXPECTED_NON_API)
            {
                return new GetBrokerBidResponse() { WantsToBid = false };
            }
            LeadBuyers.ILeadBuyer instance = LeadBuyers.LeadBuyerFactory.GetInstance(supplier.new_leadbuyername);
            if (instance != null)
            {
                return instance.GetBrokerBid(incident, accExp, ia);
            }
            else
            {
                LogUtils.MyHandle.WriteToLog("Could not find implementation of lead buyer with name '{0}' in BiddingManager.GetBrokerBid", supplier.new_leadbuyername);
                return new GetBrokerBidResponse() { WantsToBid = false };
            }
        }

        private void CheckIfBiddingIsDone(object timerContainerObj)
        {
            try
            {
                TimerContainer timerContainer = (TimerContainer)timerContainerObj;
                DataAccessLayer.Generic.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.Generic.SqlParametersList();
                parameters.Add("@incidentId", timerContainer.incidentId);
                object scalar = genericDal.ExecuteScalar(checkQueryIfAllBidddingIsDone, parameters);
                if (scalar == null)
                {
                    timerContainer.Timer.Dispose();
                    ttsTimersHolder.Remove(timerContainer);
                    ServiceRequest.ServiceRequestManager srManager = new ServiceRequestManager(XrmDataContext);
                    srManager.ContinueRequestAfterBidding(timerContainer.incidentId);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in BiddingManager.CheckIfBiddingIsDone");
            }
        }
        private void MobileBiddingTimeout(object timerContainerObj)
        {            
            TimerContainer timerContainer = (TimerContainer)timerContainerObj;
            timerContainer.Timer.Dispose();
            ttsTimersHolder.Remove(timerContainer);
            ServiceRequest.ServiceRequestManager srManager = new ServiceRequestManager(XrmDataContext);
            srManager.ContinueRequestAfterMobileBidding(timerContainer.incidentId);                     
        }

        private class TimerContainer
        {
            public Timer Timer { get; set; }
            public Guid incidentId { get; set; }
            public DateTime TimerStartTime { get; set; }
        }

        private const string checkQueryIfAllBidddingIsDone =
            @"update new_incidentaccount
            set new_biddone = 1,
            new_bidtimedout = 1
            where 
            new_incidentid = @incidentId 
            and new_biddone = 0 
            and new_bidtimeoutat_utc < GETDATE();

            select top 1 1 from new_incidentaccount where new_incidentid = @incidentId and new_biddone = 0
            ";

        private const string queryGetLiveBidRequest =
            @"
select
ia.new_bidtimeoutat_utc
, ia.New_incidentaccountId
, inc.Description
, inc.new_primaryexpertiseidName
, ae.new_incidentprice
, pe.new_minprice
, inc.createdon
, reg.new_englishname
, inc.new_telephone1
, inc.customeridname
from new_incidentaccount ia with(nolock)
join incident inc with(nolock) on inc.incidentid = ia.new_incidentid
join new_accountexpertise ae with(nolock) on ae.new_accountid = @supplierId and ae.new_primaryexpertiseid = inc.new_primaryexpertiseid
join new_primaryexpertise pe with(nolock) on pe.new_primaryexpertiseid = inc.new_primaryexpertiseid
join new_region reg with(nolock) on reg.new_regionid = inc.new_regionid
where
ia.new_biddone = 0
and ia.new_accountid = @supplierId
and ia.new_bidtimeoutat_utc > GETDATE()
order by ia.new_bidtimeoutat_utc
";

        public List<DataModel.APIs.MobileApi.LiveBid> GetLiveBidRequest(Guid supplierId)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            var reader = genericDal.ExecuteReader(queryGetLiveBidRequest, parameters);
            List<DataModel.APIs.MobileApi.LiveBid> retVal = new List<DataModel.APIs.MobileApi.LiveBid>();
            foreach (var row in reader)
            {
                var bid = new DataModel.APIs.MobileApi.LiveBid();
                bid.BidTimeOutAtUtc = (DateTime)row["new_bidtimeoutat_utc"];
                bid.Category = Convert.ToString(row["new_primaryexpertiseidName"]);
                bid.Description = Convert.ToString(row["Description"]);
                bid.IncidentAccountId = (Guid)row["New_incidentaccountId"];
                bid.MinBid = row["new_minprice"] != DBNull.Value ? (decimal)row["new_minprice"] : 5;
                bid.MaxBid = GetMaxBid(bid.MinBid);
                decimal defaultBid = (decimal)row["new_incidentprice"];
                bid.DefaultBid = ValidateDefaultBid(bid.MinBid, bid.MaxBid, defaultBid);
                bid.LeadCreatedOnUtc = (DateTime)row["createdon"];
                bid.CustomerCityAndState = BuildCityAndStateString(Convert.ToString(row["new_englishname"]));
                bid.CustomerHiddenName = BuildHiddenName(Convert.ToString(row["customeridname"]));
                bid.CustomerHiddenPhone = BuildHiddenPhone(Convert.ToString(row["new_telephone1"]));
                retVal.Add(bid);
            }
            return retVal;
        }
    }
}
