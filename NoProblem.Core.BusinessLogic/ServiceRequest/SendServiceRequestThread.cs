﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using NoProblem.Core.DataModel.Consumer;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    internal class SendServiceRequestThread: Runnable
    {
        public SendServiceRequestThread(Guid incidentId, IServiceResponse response, IServiceRequest request, eServiceType serviceType)
        {
            this.incidentId = incidentId;
            this.response = response;
            this.request = request;
            this.serviceType = serviceType;
        }

        Guid incidentId;
        IServiceResponse response;
        IServiceRequest request;
        eServiceType serviceType;

        protected override void Run()
        {
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DataModel.Xrm.incident incident = dal.Retrieve(incidentId);
            ServiceRequestManager manager = new ServiceRequestManager(XrmDataContext);
            manager.SendServiceRequest(incident, response, request, serviceType);
        }
    }
}
