﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class RequestStopper : XrmUserBase
    {
        public RequestStopper(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public void StopRequestManually(Guid caseId, Guid userId)
        {
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DML.Xrm.incident incident = new NoProblem.Core.DataModel.Xrm.incident(XrmDataContext);
            incident.incidentid = caseId;
            incident.new_isstoppedmanually = true;
            DataAccessLayer.AffiliatePayStatusReason payReasonsDal = new NoProblem.Core.DataAccessLayer.AffiliatePayStatusReason(XrmDataContext);
            Guid payStatusId;
            Guid payStatusReasonId = payReasonsDal.GetStoppedManuallyPayStatus(out payStatusId);
            incident.new_affiliatepaystatusid = payStatusId;
            incident.new_affiliatepaystatusreasonid = payStatusReasonId;
            dal.Update(incident);
            XrmDataContext.SaveChanges();
            Thread tr = new Thread(new ParameterizedThreadStart(SendEmailOnRequestStoppedManually));
            Guid[] arr = new Guid[] { caseId, userId };
            tr.Start(arr);
        }

        private void SendEmailOnRequestStoppedManually(object obj)
        {
            try
            {
                Guid[] arr = (Guid[])obj;
                Guid caseId = arr[0];
                Guid userId = arr[1];
                Notifications notificationsManager = new Notifications(null);
                DML.Xrm.DataContext localDataContext = notificationsManager.XrmDataContext;//this method runs async.
                DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(localDataContext);
                string recipientEmail = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.NEW_REQUEST_ALERT_EMAIL);
                string subject = string.Empty;
                string body = string.Empty;
                notificationsManager.InstatiateTemplate(DML.TemplateNames.NEW_STOPPED_REQUEST_ALERT, caseId, "incident", out subject, out body);
                //Put user name in body.
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(localDataContext);
                var user = accountRepositoryDal.Retrieve(userId);
                body = string.Format(body, user.name);
                //Put heading name in subject after capitalizing it.
                DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(localDataContext);
                var incident = incidentDal.Retrieve(caseId);
                string headingName = incident.new_new_primaryexpertise_incident.new_name;
                subject = string.Format(subject, headingName);
                //send
                notificationsManager.SendEmail(recipientEmail, body, subject, configDal);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RequestStopper.SendEmailOnRequestStoppedManually");
            }
        }
    }
}
