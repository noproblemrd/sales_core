﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using Effect.Crm.Configuration;
using Effect.Crm.Convertor;
using Effect.Crm.Logs;
using Effect.Crm.Methods;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using NoProblem.Core.BusinessLogic.Utils;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class Incident : ContextsUserBase
    {
        public Incident(DML.Xrm.DataContext xrmDataContext, CrmService crmService)
            : base(xrmDataContext, crmService)
        {
        }

        private readonly string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];

        public void UpdateMatchSuppliersToLose(Guid incidentId)
        {
            LogUtils.MyHandle.WriteToLog(9, "UpdateMatchSuppliersToLose started");
            string query = @"SELECT new_incidentaccountid, statuscode FROM new_incidentaccount with (nolock) WHERE new_incidentid = @incidentId AND deletionstatecode = 0 AND 
                (statuscode != 10 and statuscode != 12 and statuscode != 13 and statuscode != 16)";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@incidentId", incidentId);
                    using (SqlDataReader incidentAccounts = command.ExecuteReader())
                    {
                        Notifications notificator = new Notifications(XrmDataContext);
                        while (incidentAccounts.Read())
                        {
                            DynamicEntity incidentAccount = ConvertorCRMParams.GetNewDynamicEntity("new_incidentaccount");
                            ConvertorCRMParams.SetDynamicEntityPropertyValue(incidentAccount, "new_incidentaccountid",
                               ConvertorCRMParams.GetCrmKey(new Guid(incidentAccounts["new_incidentaccountid"].ToString())), typeof(KeyProperty));
                            ConvertorCRMParams.SetDynamicEntityPropertyValue(incidentAccount, "statuscode",
                               ConvertorCRMParams.GetCrmStatus((int)DML.Xrm.new_incidentaccount.Status.LOST), typeof(StatusProperty));
                            MethodsUtils.UpdateEntity(CrmService, incidentAccount);
                        }
                    }
                }
            }
            LogUtils.MyHandle.WriteToLog(9, "UpdateMatchSuppliersToLose ended");
        }        

        public string GetIncidentPackages()
        {
            string retVal = "";
            try
            {
                SqlHelper sqlh = new SqlHelper(ConfigurationUtils.GetConfigurationItem("ConnectionString"));
                retVal = sqlh.GetIncidentPackageListAsXml();
            }
            catch (Exception ex)
            {
                retVal = ex.Message;
            }

            return retVal;
        }
    }
}
