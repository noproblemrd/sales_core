﻿using Effect.Crm.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class ServiceRequestJobChecker : XrmUserBase
    {
        static ServiceRequestJobChecker()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            MOBILE_BID_TIMEOUT = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_BID_TIMEOUT));
            MOBILE_AAR_TIMEOUT = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_AAR_TIMEOUT));
            MOBILE_INCIDENT_TIMEOUT = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_INCIDENT_TIMEOUT)) * 60;
        }
        private static Timer timerJob;
        private static object _lock = new object();
        private const int ONE_MIN = 1000 * 60;
        private const int FIVE_MINS = ONE_MIN * 5;
    //    private const int FIVE_MINS = 5 * 1000;
        private const int EXTRA_MINS = 1;
        private static readonly int MOBILE_BID_TIMEOUT;
        private static readonly int MOBILE_AAR_TIMEOUT;
        private static readonly int MOBILE_INCIDENT_TIMEOUT;
        
        #region Ctor
        public ServiceRequestJobChecker(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        private int AuctionTimeOutSeconds
        {
            get { return MOBILE_BID_TIMEOUT + (EXTRA_MINS * 60); }
        }
        private int AarTimeoutSeconds
        {
            get { return MOBILE_AAR_TIMEOUT + (EXTRA_MINS * 60); }
        }
        private int IncidentTimeoutSeconds
        {
            get { return MOBILE_INCIDENT_TIMEOUT + (EXTRA_MINS * 60); }
        }
        public static void OnApplicationStart()
        {
            timerJob = new Timer(ServiceRequestManagerJob, null, 10000, FIVE_MINS);
        }
        private static void ServiceRequestManagerJob(object state)
        {
            
            ServiceRequestJobChecker checker = new ServiceRequestJobChecker(null);
            checker.Check();         
            
        }

        private void Check()
        {
            lock (_lock)
            {
                CheckIfAuctionClosed();
                CheckIfToCloseIncident();
                CheckIfAarExecute();
                
            }

        }

        private void CheckIfToCloseIncident()
        {
            string command = @"
SELECT i.IncidentId, DATEDIFF(second, ISNULL(i.New_relaunchDate, i.CreatedOn), GETDATE()) Seconds
from dbo.Incident i WITH(NOLOCK)
where i.DeletionStateCode = 0
	and i.CaseTypeCode = 200004 --Mobile video
	and i.StatusCode in (2, 200000, 200022, 200023)";
            try
            {
                List<Guid> list = new List<Guid>();
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            int seconds = (int)reader["Seconds"];
                            if (seconds > IncidentTimeoutSeconds)
                            {
                                list.Add((Guid)reader["IncidentId"]);

                            }
                        }
                        conn.Close();
                    }
                }
                foreach (Guid incidentId in list)
                {
                    ServiceRequestManager srm = new ServiceRequestManager(XrmDataContext);
                    srm.CloseMobileIncident(incidentId, VideoRequests.TypeExecuteUser.System);
                }
            }
            catch(Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckIfToCloseIncident");
            }
        }
        private void CheckIfAuctionClosed()
        {
            string command = @"
SELECT i.IncidentId, i.CreatedOn, DATEDIFF(second, i.New_startAuctionOn, GETDATE()) startAuctionSeconds
from dbo.Incident i WITH(NOLOCK)
where i.DeletionStateCode = 0
	and i.CaseTypeCode = 200004 --Mobile video
	and i.StatusCode = 2 --wating";
            List<Guid> list = new List<Guid>();
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {

                            object o = reader["startAuctionSeconds"];
                            if (o != DBNull.Value)
                            {
                                int seconds = (int)o;
                                if (seconds < AuctionTimeOutSeconds)
                                    continue;
                            }
                            list.Add((Guid)reader["IncidentId"]);

                        }
                        conn.Close();
                    }
                }
                foreach (Guid incidentId in list)
                {
                    ServiceRequestManager srm = new ServiceRequestManager(XrmDataContext);
                    srm.ContinueRequestAfterMobileBidding(incidentId);
                }
            }
            catch(Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckIfAuctionClosed");
            }
        }
         private void CheckIfAarExecute()
        {
            if (!ServiceRequestManager.MOBILE_APP_USE_AAR)
                return;
            string command = @"
SELECT i.IncidentId, DATEDIFF(second, i.New_relaunchDate, GETDATE()) StartOn
from dbo.Incident i WITH(NOLOCK)
where i.DeletionStateCode = 0
	and i.CaseTypeCode = 200004 --Mobile video
	and i.StatusCode in (2, 200000, 200022)
	and ISNULL(i.New_UsedAAR, 0) = 0";
            try
            {
                List<Guid> incidentList = new List<Guid>();
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            int StartOn = (int)reader["StartOn"];
                            if (StartOn > AarTimeoutSeconds)
                            {
                                incidentList.Add((Guid)reader["IncidentId"]);
                            }
                        }
                        conn.Close();
                    }
                }
                foreach (Guid incidentId in incidentList)
                {
                    ServiceRequestManager srm = new ServiceRequestManager(XrmDataContext);
                    srm.ExecuteAar(incidentId);
                }
            }
            catch(Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckIfAarExecute");
            }
        }

    }
}
