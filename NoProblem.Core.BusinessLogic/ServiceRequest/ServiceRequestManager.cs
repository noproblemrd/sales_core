﻿using Effect.Crm.Logs;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using NoProblem.Core.BusinessLogic.LeadBuyers;
using NoProblem.Core.BusinessLogic.Origins;
using NoProblem.Core.BusinessLogic.PublisherPortal;
using NoProblem.Core.BusinessLogic.Utils;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Consumer;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using NoProblem.Core.DataModel.SupplierModel.Mobile;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class ServiceRequestManager : XrmUserBase
    {
        static ServiceRequestManager()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
        //    MOBILE_BID_TIMEOUT = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_BID_TIMEOUT));
            MOBILE_AAR_TIMEOUT = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_AAR_TIMEOUT));
            MOBILE_INCIDENT_TIMEOUT = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_AAR_TIMEOUT)) * 60;
            MOBILE_APP_USE_AAR = configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_APP_USE_AAR) == "1";
        }
        public static readonly int NUMBER_PROVIDERS_MOBILE = 3;
        private static readonly int MOBILE_AAR_TIMEOUT;
        private static readonly int MOBILE_INCIDENT_TIMEOUT;
        public static readonly bool MOBILE_APP_USE_AAR;
        private static SynchronizedCollection<TimerContainer> ttsTimersHolder = new System.Collections.Generic.SynchronizedCollection<TimerContainer>();
        private static Object LockExecuteMobileAuction = new object();
        #region Ctor
        public ServiceRequestManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

       

        private static Random random = new Random();
        private bool isVideoLead;

        #region delegates

        private delegate void SendDelayedNotificationsDelegate();
        private delegate void SendRequestToChannelDelegate(DML.Xrm.incident incident, List<DML.Xrm.new_incidentaccount> incidentAccountList);
        private delegate void SendServiceSavedSMSDelegate(NoProblem.Core.DataModel.Xrm.incident incident);

        #endregion

        #region Public Methods

        public IServiceResponse CreateServiceWithZipcodeUSA(IServiceRequest request)
        {
            
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            string fixedZipCode = FixZipCode(request.RegionCode);
            string code = dal.GetCodeByNameAndLevel(fixedZipCode, 4);
            if (code == null)
            {
                DescriptionValidationLogger validationLogger = new DescriptionValidationLogger(XrmDataContext);
                validationLogger.LogValidation(
                    new DescriptionValidationLogRequest()
                    {
                        Description = request.RequestDescription,
                        Event = "request submitted",
                        Flavor = request.ControlName,
                        Heading = request.ExpertiseCode,
                        Keyword = request.Keyword,
                        OriginId = request.OriginId,
                        Phone = request.ContactPhoneNumber,
                        QualityCheckName = enumQualityCheckName.OK,
                        Region = request.RegionCode,
                        Session = request.SessionId,
                        Url = request.Url,
                        ZipCodeCheckName = "does not exists"
                    });
                LogUtils.MyHandle.WriteToLog("CreateServiceWithZipcodeUSA - Zip code not found");
                if (request is ScheduledServiceRequest)
                {
                    return new ScheduledServiceResponse()
                    {
                        Status = StatusCode.BadParametersReceived,
                        Message = "Zip code not found"
                    };
                }
                else
                {
                    return new ServiceResponse()
                    {
                        Status = StatusCode.BadParametersReceived,
                        Message = "Zip code not found"
                    };
                }
            }
            request.RegionCode = code;
            request.RegionLevel = 4;
            return CreateService(request);
        }

        public static string FixZipCode(string zipCode)
        {
            if (String.IsNullOrEmpty(zipCode))
                return zipCode;
            int indexOfSpace = zipCode.IndexOf(' ');
            if (indexOfSpace > 0)
            {
                return zipCode.Substring(0, indexOfSpace);
            }
            int indexOfDash = zipCode.IndexOf('-');
            if (indexOfDash > 0)
            {
                return zipCode.Substring(0, indexOfDash);
            }
            return zipCode;
        }

        public IServiceResponse CreateService(IServiceRequest request)
        {
            IServiceResponse response;
            eServiceType serviceType = ResolveServiceType(request, out response);
            /* NOT IN USED (np_israel)
            if (ConfigurationManager.AppSettings["SiteId"] == "np_israel" && request.RegionLevel == 2)
            {
                request.RegionLevel = 3;
            }
            */
            Result validationResult = Validator.ValidateServiceRequest(request);
            if (!validationResult.IsSuccess)
            {
                response.Status = StatusCode.BadParametersReceived;
                response.Message = validationResult.ToString();
                LogUtils.MyHandle.WriteToLog(
                    @"Bad params received in CreateService and did not pass the validation. 
                    bad params: {0}. OriginId: {1}. ExpertiseCode: {2}. ExpertiseType: {3}. RegionCode: {4}. RegionType: {5}.
                    ContactNumber: {6}. NumOfSuppliers: {7}, UpsaleId: {8}"
                    , response.Message, request.OriginId, request.ExpertiseCode, request.ExpertiseType,
                    request.RegionCode, request.RegionLevel, request.ContactPhoneNumber, request.NumOfSuppliers
                    , request.UpsaleId);
                return response;
            }

            response.Status = StatusCode.Success;
            if (IsDirectNumber(request.ContactPhoneNumber))
            {
                response.Status = StatusCode.BlackList;
                return response;
            }
            DML.Xrm.incident incident = GetWorkingIncident(request, response, serviceType);
            if (incident == null)
            {
                response.ServiceRequestId = Guid.Empty.ToString();
                return response;
            }
            bool isUpsale = (incident.new_isupsale.HasValue && incident.new_isupsale.Value);
            if (isUpsale)
            {
                CloseUpsale(request.UpsaleId, request.UserId);
            }
            response.ServiceRequestId = incident.incidentid.ToString();
            if (response.Status == StatusCode.Success)
            {
                if (isVideoLead)
                {
#if DEBUG
                    
                    if (GlobalConfigurations.IsDevEnvironment)
                    {
                        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
                        {
                            DML.Xrm.incident _incident = (DML.Xrm.incident)state;
                            ConfirmRejectIncidentReview(_incident.incidentid, true);
                        }), incident);
                    }
                    
                    /*
                    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
                        {
                            DML.Xrm.incident _incident = (DML.Xrm.incident)state;
                            SendServiceRequestVideoPhoneApp(_incident);
                        }), incident);
                     * */
#endif
                }
                else
                {
                    if (!isUpsale)
                    {
                        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
                        {
                            DML.Xrm.incident _incident = (DML.Xrm.incident)state;
                            NoProblem.Core.BusinessLogic.SmsEngine.SmsAfterRequest sar =
                                new SmsEngine.SmsAfterRequest(incident.new_telephone1, incident.new_new_primaryexpertise_incident.new_name, incident.incidentid);
                            sar.SendSms();
                        }), incident);
                    }
                    if (serviceType != eServiceType.Scheduled
                       && request.AppType != (int)DataModel.Consumer.ServiceRequestEnums.eAppType.DeskApp
                       && !isVideoLead)
                    {
                        SendServiceRequestAsynchronously(incident.incidentid, response, request, serviceType);
                    }
                }
            }
            else
            {
                ServiceRequestCloser closer = new ServiceRequestCloser(XrmDataContext);
                closer.CloseRequest(incident);
            }
            return response;
        }
       
        public void SendScheduledServices()
        {
            DataAccessLayer.Incident incidentDAL = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            IEnumerable<DML.Xrm.incident> incidents = incidentDAL.GetScheduledIncidentsToSend();
            DateTime now = DateTime.Now.AddMinutes(3);
            foreach (DML.Xrm.incident inci in incidents)
            {
                try
                {
                    if (inci.new_preferedcalltime.HasValue && inci.new_preferedcalltime.Value.CompareTo(now) <= 0)
                    {
                        inci.customersatisfactioncode = 3;//marks the incident as sent so if it is found again it won't be sent again.
                        incidentDAL.Update(inci);
                        XrmDataContext.SaveChanges();
                        Thread tr = new Thread(new ParameterizedThreadStart(SendSingleScheduledRequestAsync));
                        tr.Start(inci.incidentid);
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to send scheduled incident with exception. incidentId = {0}", inci.incidentid.ToString());
                }
            }
        }

        public void SendDelayedNotifications()
        {
            SendDelayedNotificationsDelegate del = new SendDelayedNotificationsDelegate(SendDelayedNotificationPrivate);
            del.BeginInvoke(null, null);
        }

        public DML.Case.CaseData GetCaseData(Guid caseId)
        {
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DML.Case.CaseData data = dal.GetCaseData(caseId);
            data.CreatedOn = FixDateToLocal(data.CreatedOn);
            return data;
        }

        public void StartDesktopAppCase(Guid incidentId)
        {
            IServiceResponse response = new ScheduledServiceResponse() { Status = StatusCode.Success };
            SendServiceRequestAsynchronously(incidentId, response, null, eServiceType.Normal);
        }

        public void ContinueDesktopAppCase(Guid incidentId, bool wantToContinue)
        {
            DataAccessLayer.Incident inciDal = new DataAccessLayer.Incident(XrmDataContext);
            var incident = inciDal.Retrieve(incidentId);
            SellLeadsToLeadBuyers(incident, inciDal, true);
        }

        public DataModel.Pusher.CaseFlow.CaseAllDataContainer GetCaseDataForDesktopApp(Guid incidentId)
        {
            var list = GetSortedIncidentAccountsList(incidentId, new DataAccessLayer.IncidentAccount(XrmDataContext));
            DataModel.Pusher.CaseFlow.CaseAllDataContainer data = new DML.Pusher.CaseFlow.CaseAllDataContainer();
            CustomerTips.TipStatistics stats = new CustomerTips.TipStatistics();
            YelpSharp.Yelp yelp = YelpHelper.GetYelp();
            for (int i = 0; i < list.Count() && i < 3; i++)
            {
                var supplier = list.ElementAt(i).new_account_new_incidentaccount;
                //if we are in dev environment, we pass a real number to Yelp so we can check the integrations.
                var yelpSearchResult = yelp.SearchByPhone(GlobalConfigurations.IsDevEnvironment ? (i % 2 == 0 ? "5184491782" : "1111111111") : supplier.telephone1);
                var yelpBusiness = yelpSearchResult != null && yelpSearchResult.businesses != null ? yelpSearchResult.businesses.FirstOrDefault() : null;
                var advData =
                    new DML.Pusher.CaseFlow.AdvertiserData()
                    {
                        Name = supplier.name,
                        Address = supplier.new_fulladdress,
                        SupplierId = supplier.accountid,
                        Index = i,
                        RecordingCall = supplier.new_recordcalls.IsTrue()
                    };
                if (yelpBusiness != null)
                {
                    advData.YelpRating = yelpBusiness.rating_img_url;
                    advData.YelpLink = yelpBusiness.url;
                    var yelpBusinessWithReviews = yelp.GetBusiness(yelpBusiness.id);
                    if (yelpBusinessWithReviews != null && yelpBusinessWithReviews.reviews != null && yelpBusinessWithReviews.reviews.Count > 0)
                    {
                        advData.YelpReview = yelpBusinessWithReviews.reviews.OrderByDescending(x => x.time_created).First().excerpt;
                    }
                }
                if (supplier.new_paywithamex.IsTrue())
                    advData.PayWith.Add("AMEX");
                if (supplier.new_paywithcash.IsTrue())
                    advData.PayWith.Add("CASH");
                if (supplier.new_paywithmastercard.IsTrue())
                    advData.PayWith.Add("MASTER CARD");
                if (supplier.new_paywithvisa.IsTrue())
                    advData.PayWith.Add("VISA");
                var tipStats = stats.GetTipsPercentages(supplier.accountid, incidentId);
                advData.TipsPercentages = tipStats;
                data.Advertisers.Add(advData);
            }
            data.InterestedAdvertisers = data.Advertisers.Count;
            return data;
        }

        #endregion

        #region Internal Methods

        internal void ContinueRequestAfterBidding(Guid incidentId)
        {
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentId);
            MarkNotAcceptionAdvetisersAsLost(incident);
            if (incident.new_apptype == (int)DataModel.Consumer.ServiceRequestEnums.eAppType.DeskApp)
            {
                try
                {
                    string js = BuildCaseAllDataScriptForPusherEvent(incident);
                    Pusher.DesktopApp.PusherDesktopAppInjectJsEvent pusherEvent = new Pusher.DesktopApp.PusherDesktopAppInjectJsEvent(incidentId.ToString(), js);
                    Pusher.PusherEventSender.TriggerEvent(pusherEvent);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception sending pusher to desktop app after bidding done");
                }
            }
            SellLeadsToLeadBuyers(incident, incidentDal, false);
        }
            

        private string BuildCaseAllDataScriptForPusherEvent(DML.Xrm.incident incident)
        {
            string js = new Pusher.DesktopApp.Builders.CaseFlowScriptsBuilder().BuildCaseAllDataScript(null);
            return js;
        }

        private void MarkNotAcceptionAdvetisersAsLost(DML.Xrm.incident incident)
        {
            DataAccessLayer.IncidentAccount incidentAccountDAL = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            List<Guid> guids = incidentAccountDAL.GetNonDoneAndNotAcceptingNormalAdvertisers(incident.incidentid);
            foreach (var id in guids)
            {
                DataModel.Xrm.new_incidentaccount ia = new NoProblem.Core.DataModel.Xrm.new_incidentaccount();
                ia.new_incidentaccountid = id;
                MarkIncidentAccountAsLostAfterLeadBuyerFailed(incidentAccountDAL, ia);
            }
        }

        internal void ContinueRequestAfterBrokerCall(bool sold, Guid incidentId, bool isExclusive)
        {
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentId);

            if ((sold & isExclusive) || (incident.new_requiredaccountno <= incident.new_ordered_providers))
            {
                if (incident.new_apptype == (int)DataModel.Consumer.ServiceRequestEnums.eAppType.DeskApp)
                    SendPusherCallEnded(incidentId, false);
                StopAll(incident, incidentDal, false);
            }
            else
            {
                bool soldAtLeastOnce;
                if (!sold)
                {
                    DataAccessLayer.IncidentAccount incidentAccountDAL = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                    soldAtLeastOnce = incidentAccountDAL.ClosedAtLeastOnce(incidentId);
                }
                else
                {
                    soldAtLeastOnce = sold;
                }
                if (incident.new_apptype == (int)DataModel.Consumer.ServiceRequestEnums.eAppType.DeskApp)
                {
                    DataAccessLayer.IncidentAccount incidentAccountDal = new DataAccessLayer.IncidentAccount(XrmDataContext);
                    List<Guid> guids = incidentAccountDal.GetNonDoneIncidentAccount(incidentId);
                    bool hasMoreSuppliers = guids.Count > 0;
                    SendPusherCallEnded(incidentId, hasMoreSuppliers);
                    if (!hasMoreSuppliers)
                    {
                        StopAll(incident, incidentDal, false);
                    }
                    else
                    {
                        //TODO: set timeout to auto close case if no event is sent from client.
                    }
                }
                else
                {
                    SellLeadsToLeadBuyers(incident, incidentDal, soldAtLeastOnce);
                }
            }
        }

        private void SendPusherCallEnded(Guid incidentId, bool hasMoreSuppliers)
        {
            var js = new Pusher.DesktopApp.Builders.CaseFlowScriptsBuilder().BuildCallEndedScript(hasMoreSuppliers);
            Pusher.DesktopApp.PusherDesktopAppInjectJsEvent pushEvent = new Pusher.DesktopApp.PusherDesktopAppInjectJsEvent(incidentId.ToString(), js);
            Pusher.PusherEventSender.TriggerEvent(pushEvent);
        }

        internal void MarkIncidentAccountAsLostAfterLeadBuyerFailed(
           DataAccessLayer.IncidentAccount incidentAccountDal,
           DML.Xrm.new_incidentaccount ia)
        {
            ia.statuscode = (int)DataModel.Xrm.new_incidentaccount.Status.LOST;
            incidentAccountDal.Update(ia);
            XrmDataContext.SaveChanges();
        }

        internal void RejectCall(
            DataAccessLayer.IncidentAccount incidentAccountDal,
            DML.Xrm.new_incidentaccount ia)
        {
            ia.new_isrejected = true;
            ia.new_rejectedreason = "Advertiser rejected the call";
            ia.statuscode = (int)DataModel.Xrm.new_incidentaccount.Status.REJECT;
            incidentAccountDal.Update(ia);
            XrmDataContext.SaveChanges();
        }

        internal void CloseIncidentAccountAfterLeadBuyer(
           DataModel.Xrm.new_incidentaccount incAcc,
           DataModel.Xrm.incident incident,
           DataAccessLayer.Incident inciDal,
           DataAccessLayer.IncidentAccount incAccDal,
           bool isBySms)
        {
            if (isBySms)
            {
                incAcc.statuscode = (int)DataModel.Xrm.new_incidentaccount.Status.INVITED_BY_SMS;
            }
            else
            {
                incAcc.statuscode = (int)DataModel.Xrm.new_incidentaccount.Status.CLOSE;
            }
            incAcc.new_tocharge = true;
            if (incident.casetypecode == (int)DataModel.Xrm.incident.CaseTypeCode.Video)
            {
                incAcc.new_callprocess = (int)DataModel.Xrm.new_incidentaccount.CallProcess.WaitForCall;
                incAcc.new_mobilecallorder = inciDal.GetNextMobileCallOrder(incident.incidentid);                
            }
            incAccDal.Update(incAcc);
            incident.new_ordered_providers = incident.new_ordered_providers.HasValue ? incident.new_ordered_providers.Value + 1 : 1;
            inciDal.Update(incident);
            DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
            DML.Xrm.new_upsale upsale = upsaleDal.GetUpsaleByIncidentId(incident.incidentid);
            if (upsale != null)
            {
                upsale.new_numofconnectedadvertisers =
                    (upsale.new_numofconnectedadvertisers.HasValue ? upsale.new_numofconnectedadvertisers.Value + 1 : 1);
                upsaleDal.Update(upsale);
            }
            XrmDataContext.SaveChanges();
            DailyBudgetManager budgetManager = new DailyBudgetManager(XrmDataContext);
            budgetManager.CheckDailyBudgetIsReached(incAcc.new_accountid.Value, false);
            if (!isBySms && incident.casetypecode != (int)DataModel.Xrm.incident.CaseTypeCode.Video)
            {
                Notifications notificationsManager = new Notifications(XrmDataContext);
                notificationsManager.NotifySupplier(DML.enumSupplierNotificationTypes.INCIDENT_INFO, incAcc.new_accountid.Value, incident.incidentid.ToString(), "incident", incident.incidentid.ToString(), "incident");
            }
            if (Dapaz.PpaOnlineSync.SyncConfiguration.IsEnabled)
            {
                Dapaz.PpaOnlineSync.Syncher syncher = new Dapaz.PpaOnlineSync.Syncher(incAcc.new_accountid.Value);
                syncher.Start();
            }
            if (incident.casetypecode == (int)DataModel.Xrm.incident.CaseTypeCode.Video)
            {
                DataAccessLayer.AccountRepository acc = new DataAccessLayer.AccountRepository(XrmDataContext);
                acc.CheckMobileAccountAfterRequest(incAcc.new_accountid.Value);
            }
        }

        internal bool InviteBySms(
            DataModel.Xrm.new_incidentaccount incAcc,
            DataModel.Xrm.incident incident,
             DataAccessLayer.Incident inciDal,
            DataAccessLayer.IncidentAccount incAccDal)
        {
            Notifications notifier = new Notifications(XrmDataContext);
            bool supplierNotified = notifier.NotifySupplier(enumSupplierNotificationTypes.SUPPLIER_UNREACHED, incAcc.new_accountid.Value, incident.incidentid.ToString(), "incident", incident.incidentid.ToString(), "incident");
            if (supplierNotified)
            {
                CloseIncidentAccountAfterLeadBuyer(incAcc, incident, inciDal, incAccDal, true);
            }
            else
            {
                MarkIncidentAccountAsLostAfterLeadBuyerFailed(incAccDal, incAcc);
            }
            return supplierNotified;
        }

        internal void StopAll(DML.Xrm.incident incident, DataAccessLayer.Incident incidentDal, bool isCustomerFault)
        {
            //close incident 

            DataAccessLayer.IncidentAccount incidentAccountDAL = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            List<Guid> guids = incidentAccountDAL.GetNonDoneIncidentAccount(incident.incidentid);
            foreach (var id in guids)
            {
                DataModel.Xrm.new_incidentaccount ia = new NoProblem.Core.DataModel.Xrm.new_incidentaccount();
                ia.new_incidentaccountid = id;
                MarkIncidentAccountAsLostAfterLeadBuyerFailed(incidentAccountDAL, ia);
            }

            incident.new_winningprice = GetIncidentWinningPrice(incident);
            incident.statuscode = isCustomerFault ? (int)DML.Xrm.incident.Status.CUSTOMER_NOT_AVAILABLE_DIALER : (int)DML.Xrm.incident.Status.WORK_DONE;
            incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
            ServiceRequestCloser closer = new ServiceRequestCloser(incidentDal.XrmDataContext);
            closer.CloseRequest(incident, incidentDal);
        }

      

        internal static bool IsRejectEnabledForJonaCall(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise acExp)
        {
            if (GlobalConfigurations.IsUsaSystem)
                return false;
            bool rejectEnabled = incident.new_new_primaryexpertise_incident.new_rejectenabled.HasValue && incident.new_new_primaryexpertise_incident.new_rejectenabled.Value;
            if (rejectEnabled)
            {
                bool isAuctionAtHeadingLevel = incident.new_new_primaryexpertise_incident.new_isauction.HasValue && incident.new_new_primaryexpertise_incident.new_isauction.Value;
                if (isAuctionAtHeadingLevel)
                {
                    bool isBidder = acExp.new_incidentpricechangeable.HasValue && acExp.new_incidentpricechangeable.Value;
                    rejectEnabled = !isBidder;
                }
            }
            return rejectEnabled;
        }


        #endregion

        #region Private Methods

        private void SendSingleScheduledRequestAsync(object incidentIdObj)
        {
            try
            {
                Guid incidentId = (Guid)incidentIdObj;
                LogUtils.MyHandle.WriteToLog(8, "About to send scheduled incident with id {0}", incidentId.ToString());
                ServiceRequestManager innerManager = new ServiceRequestManager(null);
                DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(innerManager.XrmDataContext);
                var inci = incidentDal.Retrieve(incidentId);
                IServiceResponse response = new ScheduledServiceResponse() { Status = StatusCode.Success };
                innerManager.SendServiceRequest(inci, response, null, eServiceType.Scheduled);
                LogUtils.MyHandle.WriteToLog(8, "SendServiceRequest finished with status {0}, for incidentId = {1}", response.Status, incidentId.ToString());
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SendSingleScheduledRequestAsync. Failed to send scheduled incident with exception. incidentId = {0}", incidentIdObj.ToString());
            }
        }

        private void SendDelayedNotificationPrivate()
        {
            DataAccessLayer.NotificationItem notiItemDal = new NoProblem.Core.DataAccessLayer.NotificationItem(XrmDataContext);
            IEnumerable<DML.Xrm.new_notificationitem> items = notiItemDal.GetItemsToSend();
            DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            foreach (var item in items)
            {
                try
                {
                    DML.Xrm.new_channel.ChannelCode channelCode = (DML.Xrm.new_channel.ChannelCode)item.new_channel_notificationitem.new_code.Value;
                    Channels.ChannelManager channelManager = Channels.ChannelManagerFactory.GetChannelManager(channelCode, XrmDataContext);
                    channelManager.SendAndClose(item, incidentAccountDal, accountRepositoryDal, incidentDal);
                    notiItemDal.Delete(item);
                    XrmDataContext.SaveChanges();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in SendDelayedNotifications. moving to next notification item.");
                }
            }
        }

        private bool IsDirectNumber(string number)
        {
            DataAccessLayer.DirectNumber directNumberDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            bool isDirect = directNumberDal.IsDirectNumber(number);
            return isDirect;
        }

        private DML.SqlHelper.ContactMinData GetConsumer(string consumerPhoneNumber, string consumerName, string consumerEmail, string consumerAddress, bool isFemale, DateTime? birthDate, string height, string weight, string country, int? heightFeet, int? heightInches, IServiceResponse response)
        {
            DataAccessLayer.Contact contactDAL = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            DML.SqlHelper.ContactMinData consumer = contactDAL.GetContactMinDataByPhoneNumber(consumerPhoneNumber);
            if (consumer != null)
            {
                if (consumer.IsBlackList)
                {
                    response.Status = NoProblem.Core.DataModel.StatusCode.BlackList;
                }
                else
                {
                    DML.Xrm.contact c = new NoProblem.Core.DataModel.Xrm.contact(XrmDataContext);
                    c.contactid = consumer.Id;
                    if (!string.IsNullOrEmpty(consumerName))
                    {
                        consumerName = FilterXss(consumerName);
                        BadWordManager badWords = new BadWordManager(XrmDataContext);
                        bool foundBadWord = badWords.CheckForBadWords(consumerName);
                        if (!foundBadWord)
                        {
                            consumerName = consumerName.Trim();
                            int indexOfSpace = consumerName.IndexOf(' ');
                            if (indexOfSpace == -1)
                            {
                                c.firstname = consumerName;
                            }
                            else
                            {
                                c.firstname = consumerName.Substring(0, indexOfSpace).Trim();
                                c.lastname = consumerName.Substring(indexOfSpace, consumerName.Length - indexOfSpace).Trim();
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(consumerAddress))
                    {
                        consumerAddress = FilterXss(consumerAddress);
                        BadWordManager badWords = new BadWordManager(XrmDataContext);
                        bool foundBadWord = badWords.CheckForBadWords(consumerAddress);
                        if (!foundBadWord)
                        {
                            if (consumerAddress.Length > 250)
                            {
                                consumerAddress = consumerAddress.Substring(0, 250);
                            }
                            c.address1_line1 = consumerAddress;
                        }
                    }
                    if (!string.IsNullOrEmpty(consumerEmail))
                    {
                        c.emailaddress1 = consumerEmail;
                    }
                    if (birthDate.HasValue)
                    {
                        c.birthdate = birthDate.Value;
                    }
                    if (!String.IsNullOrEmpty(weight))
                    {
                        c.new_weight = weight;
                    }
                    if (!String.IsNullOrEmpty(height))
                    {
                        c.new_height = height;
                    }
                    if (heightFeet.HasValue)
                    {
                        c.new_heightfeet = heightFeet;
                    }
                    if (heightInches.HasValue)
                    {
                        c.new_heightinches = heightInches;
                    }
                    c.gendercode = isFemale ? (int)DataModel.Xrm.contact.GenderCode.FEMALE : (int)NoProblem.Core.DataModel.Xrm.contact.GenderCode.MALE;
                    if (!String.IsNullOrEmpty(country))
                    {
                        c.new_country = country;
                    }
                    contactDAL.Update(c);
                    XrmDataContext.SaveChanges();
                }
            }
            else//is new consumer
            {
                consumerName = FilterXss(consumerName);
                BadWordManager badWords = new BadWordManager(XrmDataContext);
                bool foundBadWord = badWords.CheckForBadWords(consumerName);
                if (foundBadWord)
                {
                    consumerName = null;
                    foundBadWord = false;
                }
                consumerAddress = FilterXss(consumerAddress);
                foundBadWord = badWords.CheckForBadWords(consumerAddress);
                if (foundBadWord)
                {
                    consumerAddress = null;
                }
                DML.Xrm.contact contact = contactDAL.CreateAnonymousContact(consumerPhoneNumber, consumerName, consumerEmail, consumerAddress, isFemale, birthDate, height, weight, country, heightFeet, heightInches, false);
                consumer = new NoProblem.Core.DataModel.SqlHelper.ContactMinData();
                consumer.Id = contact.contactid;
                consumer.IsBlackList = false;
                consumer.MobilePhone = consumerPhoneNumber;
            }
            return consumer;
        }
        private List<NoProblem.Core.DataModel.Xrm.new_incidentaccount> CreateIncidentAccounts(NoProblem.Core.DataModel.Xrm.incident incident, List<Guid> SuppliersIds)
        {
            List<NoProblem.Core.DataModel.Xrm.new_incidentaccount> ListIncidentAccounts = new List<DML.Xrm.new_incidentaccount>();
            foreach (Guid SupplierId in SuppliersIds)
            {
                DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
                DML.Xrm.account _account = accountRepositoryDal.Retrieve(SupplierId);
                ListIncidentAccounts.AddRange(CreateIncidentAccounts(incident, _account));
            }
            return ListIncidentAccounts;
        }
        private List<NoProblem.Core.DataModel.Xrm.new_incidentaccount> CreateIncidentAccounts(NoProblem.Core.DataModel.Xrm.incident incident, DML.Xrm.account supplier)
        {
            return CreateIncidentAccounts(incident, supplier, new DataAccessLayer.IncidentAccount(XrmDataContext));
        }

        private List<NoProblem.Core.DataModel.Xrm.new_incidentaccount> CreateIncidentAccounts(NoProblem.Core.DataModel.Xrm.incident incident, DML.Xrm.account supplier, DataAccessLayer.IncidentAccount incidentAccountDAL)
        {
            List<DML.Xrm.new_incidentaccount> incidentAccountList = new List<NoProblem.Core.DataModel.Xrm.new_incidentaccount>();
            DataAccessLayer.AccountExpertise accountExpertiseDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            DML.Xrm.new_accountexpertise accExpertise = accountExpertiseDal.GetByAccountAndPrimaryExpertise(supplier.accountid, incident.new_primaryexpertiseid.Value);
            DML.Xrm.new_incidentaccount incidentAccount = new NoProblem.Core.DataModel.Xrm.new_incidentaccount();
            incidentAccount.new_accountid = supplier.accountid;
            incidentAccount.new_incidentid = incident.incidentid;
            incidentAccount.new_potentialincidentprice = accExpertise.new_incidentprice.Value;
            incidentAccount.new_considerasprice = accExpertise.new_considerasprice.HasValue ? accExpertise.new_considerasprice.Value : accExpertise.new_incidentprice.Value;
            incidentAccount.new_predefinedprice = accExpertise.new_incidentprice.Value;
            incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.MATCH;
            incidentAccount.new_isjobdone = null;
            incidentAccount.new_isansweredbymachine = null;
            incidentAccountDAL.Create(incidentAccount);
            XrmDataContext.SaveChanges();
            incidentAccountList.Add(incidentAccount);
            return incidentAccountList;
        }

        private DML.Xrm.incident GetWorkingIncident(IServiceRequest request, IServiceResponse response, eServiceType serviceType)
        {
            string country;
            DataModel.Xrm.new_region region = GetRegionIdAndCountry(out country, request);
            DML.SqlHelper.ContactMinData consumer;
            if (request.ContactId != Guid.Empty)
            {
                DataAccessLayer.Contact contactDal = new DataAccessLayer.Contact(XrmDataContext);
                consumer = contactDal.GetContactMinDataById(request.ContactId);
            }
            else
            {
                consumer = GetConsumer(request.ContactPhoneNumber, request.ContactFullName, request.ContactEmail, request.ContactAddress, request.IsFemaleGender, request.DateOfBirth, request.ContactHeight, request.ContactWeight, country, request.ContactHeightFeet, request.ContactHeightInches, response);
            }
            Guid upsaleId = request.UpsaleId;
            if (serviceType == eServiceType.SupplierSpecified || upsaleId == Guid.Empty)//is not upsale
            {
                int requestedSuppliers = serviceType == eServiceType.SupplierSpecified ? 1 : request.NumOfSuppliers;
                upsaleId = GetUpsaleId(consumer.Id, request.ExpertiseCode, request.ExpertiseType, requestedSuppliers);
            }
            DML.Xrm.incident incident = CreateNewIncident(request, response, serviceType, consumer, upsaleId, region, country);
            return incident;
        }

        private Guid GetUpsaleId(Guid consumerId, string expertiseCode, int expertiseType, int requestedSuppliers)
        {
            DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
            Guid primaryExptertiseId = GetPrimaryExpertiseId(expertiseCode);
            Guid upsaleId = upsaleDal.FindOpenUpsale(consumerId, primaryExptertiseId);
            if (upsaleId == Guid.Empty)
            {
                DML.Xrm.new_upsale upsale = new NoProblem.Core.DataModel.Xrm.new_upsale();
                upsale.new_primaryexpertiseid = primaryExptertiseId;
                upsale.new_contactid = consumerId;
                upsale.new_numofrequestedadvertisers = requestedSuppliers;
                upsale.new_timetocall = FixDateToLocal(DateTime.Now);
                upsaleDal.Create(upsale);
                XrmDataContext.SaveChanges();
                upsaleId = upsale.new_upsaleid;
            }
            else
            {
                DML.Xrm.new_upsale upsale = upsaleDal.Retrieve(upsaleId);
                upsale.new_numofrequestedadvertisers += requestedSuppliers;
                upsaleDal.Update(upsale);
                XrmDataContext.SaveChanges();
            }
            return upsaleId;
        }

        private string FilterXss(string str)
        {
            string retVal = str;
            if (string.IsNullOrEmpty(retVal) == false)
            {
                retVal = retVal.Replace("<", " ").Replace(">", " ");
            }
            return retVal;
        }

        /// <summary>
        /// Removes characters that the TTS can't read.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string FilterSpecialChars(string str)
        {
            return str.Replace("\"", "").Replace("'", "");
        }

        private void CloseUpsale(Guid upsaleId, Guid userId)
        {
            DataAccessLayer.Upsale upsaleDAL = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
            DML.Xrm.new_upsale upsale = new NoProblem.Core.DataModel.Xrm.new_upsale(XrmDataContext);
            upsale.new_upsaleid = upsaleId;
            upsale.new_userid = userId;
            upsale.statuscode = (int)DML.Xrm.new_upsale.UpsaleStatus.Success;
            upsaleDAL.Update(upsale);
            XrmDataContext.SaveChanges();
        }

        private DML.Xrm.incident UpdateParentCaseData(DataModel.Xrm.incident incident, Guid upsaleRequestId, DataAccessLayer.Incident dal)
        {
            DML.Xrm.incident upsaleRequest = dal.Retrieve(upsaleRequestId);
            incident.new_domain = upsaleRequest.new_domain;
            incident.new_keyword = upsaleRequest.new_keyword;
            incident.new_url = upsaleRequest.new_url;
            incident.new_controlname = upsaleRequest.new_controlname;
            incident.new_placeinwebsite = upsaleRequest.new_placeinwebsite;
            incident.new_pagename = upsaleRequest.new_pagename;
            incident.new_sitetitle = upsaleRequest.new_sitetitle;
            incident.new_ip = upsaleRequest.new_ip;
            incident.new_browser = upsaleRequest.new_browser;
            incident.new_browserversion = upsaleRequest.new_browserversion;
            incident.new_exposureid = upsaleRequest.new_exposureid;
            if (upsaleRequest.new_toolbaridid.HasValue)
                incident.new_toolbaridid = upsaleRequest.new_toolbaridid.Value;
            incident.new_country = upsaleRequest.new_country;
            incident.new_upsaleincidentid = upsaleRequestId;
            return upsaleRequest;
        }

        private DataModel.Xrm.new_region GetRegionIdAndCountry(out string country, IServiceRequest request)
        {
            DataAccessLayer.Region regionDAL = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DML.Xrm.new_region region = regionDAL.GetRegionByCode(request.RegionCode);
            country = request.Country;
            if (String.IsNullOrEmpty(country))
            {
                country = region.new_country;
                if (String.IsNullOrEmpty(country))
                {
                    country = GlobalConfigurations.DefaultCountry;
                }
            }
            return region;
        }

        private DML.Xrm.incident CreateNewIncident(IServiceRequest request, IServiceResponse response, eServiceType serviceType, DML.SqlHelper.ContactMinData consumer, Guid upsaleId, DataModel.Xrm.new_region region, string country)
        {
            isVideoLead = request is DataModel.ServiceRequestVideoLead;//request.ContactId != Guid.Empty;
            DML.Xrm.incident incident = new NoProblem.Core.DataModel.Xrm.incident();
         //   incident.statuscode
            incident.customerid = new Microsoft.Crm.Sdk.Customer("contact", consumer.Id);
            incident.new_telephone1 = consumer.MobilePhone;
            incident.new_exposureid = request.ExposureId;//.ToString();
            incident.new_type = request.Type;
            incident.new_apptype = request.AppType;
            incident.new_desktopappcamefrom = request.DesktopAppCameFrom;
            incident.new_regionid = region.new_regionid;
            incident.new_country = country;
            if (request.Budget.HasValue)
            {
                incident.new_budget = (int)request.Budget.Value;
            }
            InsertCaseTypeCodeToIncident(serviceType, incident);
            DML.Xrm.new_primaryexpertise primary = InsertExpertiseToIncident(request, incident);
            InsertTitleToIncident(incident, primary);
            InsertCallTimesToIncident(request, serviceType, incident);
            if (request.RequestDescription == null)
            {
                request.RequestDescription = string.Empty;
            }
            else if (request.RequestDescription.Length > 500)
            {
                request.RequestDescription = request.RequestDescription.Substring(0, 500);
            }
            bool badWordFound = CheckForBadWords(request, serviceType);
            InsertStatusCodeToIncident(response, incident, badWordFound);
            incident.description = FilterSpecialChars(FilterXss(request.RequestDescription).Trim());
            incident.new_webdescription = incident.description;
            bool isManualUpsale = false;
            Guid lastIncidentId = Guid.Empty;
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            bool isUpsale = (serviceType != eServiceType.SupplierSpecified && request.UpsaleId != Guid.Empty);
            if (isUpsale)
            {
                DML.Xrm.incident upsaleRequest = null;
                if (request.UpsaleRequestId != Guid.Empty)
                {
                    upsaleRequest = UpdateParentCaseData(incident, request.UpsaleRequestId, incidentDal);
                }
                if (!(request is DataModel.ServiceRequest && ((DataModel.ServiceRequest)request).IsAutoUpsale))// is manual upsale
                {
                    isManualUpsale = true;
                    DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
                    bool isUseSameInUpsale;
                    Guid lastCaseOriginId;
                    if (upsaleRequest == null)
                    {
                        lastCaseOriginId = upsaleDal.GetLastCaseOriginIdAndIsUseSameInUpsale(request.UpsaleId, out isUseSameInUpsale, ref lastIncidentId);
                        upsaleRequest = UpdateParentCaseData(incident, lastIncidentId, incidentDal);
                    }
                    else
                    {
                        lastCaseOriginId = upsaleRequest.new_originid.Value;
                        isUseSameInUpsale = upsaleRequest.new_new_origin_incident.new_usesameinupsale.HasValue && upsaleRequest.new_new_origin_incident.new_usesameinupsale.Value;
                        lastIncidentId = upsaleRequest.incidentid;
                    }
                    if (isUseSameInUpsale)
                    {
                        request.OriginId = lastCaseOriginId;
                    }
                    else
                    {
                        DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
                        request.OriginId = originDal.GetUpsaleOriginId();
                    }
                }
                incident.new_isupsale = true;
            }
            incident.new_originid = request.OriginId;
            incident.new_leadidtoken = request.LeadIdToken;
            InsertNumberOfRequiredSupplierToIncident(request, serviceType, incident);
            incident.new_siteid = request.SiteId;
            AffiliatesManager affiliateManager = new AffiliatesManager(XrmDataContext);
            if (!string.IsNullOrEmpty(request.ToolbarId))
            {
                incident.new_toolbaridid = affiliateManager.GetCreateToolbarId(request.ToolbarId, request.OriginId);
            }
            incident.new_isdirectnumber = false;
            if (!isUpsale)
            {
                incident.new_browser = request.Browser;
                incident.new_browserversion = request.BrowserVersion;
                incident.new_ip = request.IP;
                incident.new_placeinwebsite = request.PlaceInWebSite;
                incident.new_pagename = request.PageName;
                if (!string.IsNullOrEmpty(request.Url)
                && request.Url.Length > 999)
                    request.Url = request.Url.Substring(0, 999);
                incident.new_url = request.Url;
                incident.new_controlname = request.ControlName;
                incident.new_keyword = request.Keyword;
                if (!string.IsNullOrEmpty(request.SiteTitle)
                && request.SiteTitle.Length > 99)
                    request.SiteTitle = request.SiteTitle.Substring(0, 99);
                incident.new_sitetitle = request.SiteTitle;
                if (string.IsNullOrEmpty(request.Domain) == false)
                {
                    incident.new_domain = request.Domain;
                    //incident.new_websiteid = affiliateManager.GetCreateWebSiteId(request.Domain, request.OriginId);
                }
            }
            incident.new_sessionid = request.SessionId;
            if (serviceType == eServiceType.SupplierSpecified)
            {
                incident.new_isfromlisting = true;
            }
            else
            {
                incident.new_isfromlisting = false;
            }
            if (!isVideoLead)// avoid as duplicate
            {
                AffiliatePaymentResolver affiliatePaymentResolver = new AffiliatePaymentResolver(XrmDataContext);
                affiliatePaymentResolver.ResolveAffiliatePayment(response, incident, badWordFound, isUpsale);
            }
            incident.new_upsaleid = upsaleId;
            if (request is DataModel.ServiceRequest)
            {
                incident.new_subtype = ((DataModel.ServiceRequest)request).SubType;
            }
            if (request is DataModel.ServiceRequestVideoLead)
            {
                DataModel.ServiceRequestVideoLead srvd = (DataModel.ServiceRequestVideoLead)request;
                incident.new_videourl = srvd.VideroLeadUrl;
                incident.new_previewvideopicurl = srvd.PreviewPicUrl;
                incident.new_videoduration = srvd.VideoDuration;
                incident.new_relaunchdate = DateTime.UtcNow;
                incident.new_reviewstatus = (int)NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus.WaitnigForReview;
                if (incident.statuscode == (int)NoProblem.Core.DataModel.Xrm.incident.Status.NEW)
                {
                    if (string.IsNullOrEmpty(consumer.MobilePhone))
                    {
                        incident.statuscode = (int)NoProblem.Core.DataModel.Xrm.incident.Status.WATING_FOR_PHONE_VALIDATION;
                        response.Status = StatusCode.WaitingForPhoneValidation;
                    }
                    else if (srvd.IsPrivate)
                        incident.statuscode = (int)NoProblem.Core.DataModel.Xrm.incident.Status.PRIVATE;                   
                    else
                        incident.statuscode = (int)NoProblem.Core.DataModel.Xrm.incident.Status.IN_REVIEW;
                }
            }
            if (!isVideoLead)
            {
                HeadingRules.IHeadingRule rule = HeadingRules.HeadingRulesFactory.GetRuleInstance(request, primary, XrmDataContext);
                if (rule != null)
                {
                    bool continueRequest = rule.ApplyRules(request, response, incident, isManualUpsale, lastIncidentId);
                    if (!continueRequest)
                    {
                        return null;
                    }
                }
            }
            
            incidentDal.Create(incident);
            XrmDataContext.SaveChanges();

            if (isVideoLead)
            {
                if (incident.statuscode.Value == (int)NoProblem.Core.DataModel.Xrm.incident.Status.PRIVATE)
                {
                    NoProblem.Core.DataAccessLayer.IncidentPrivateServiceProvider ipsp = new DataAccessLayer.IncidentPrivateServiceProvider(XrmDataContext);
                    ipsp.AddIncidentPrivateServiceProvider(incident.incidentid, ((DataModel.ServiceRequestVideoLead)request).FavoriteSupplierIds);
                }
                SendNewVideoLeadNotification(incident);
            }
             
            if (response.Status == StatusCode.BadWord)
            {
                CreateDescriptionValidationLogForBadWord(request, incident, primary, region);
            }

            return incident;
        }
        
        private void CreateDescriptionValidationLogForBadWord(IServiceRequest request, DML.Xrm.incident incident, DML.Xrm.new_primaryexpertise primary, DML.Xrm.new_region region)
        {
            DescriptionValidationLogRequest logRequest = new DescriptionValidationLogRequest();
            logRequest.Description = incident.description;
            logRequest.Heading = primary.new_code;
            logRequest.Keyword = incident.new_keyword;
            logRequest.OriginId = incident.new_originid.Value;
            logRequest.Phone = request.ContactPhoneNumber;
            logRequest.QualityCheckName = enumQualityCheckName.Warn_badword;
            logRequest.Region = region.new_name;
            logRequest.Session = request.SessionId;
            logRequest.Url = request.Url;
            logRequest.Flavor = request.ControlName;
            logRequest.Event = "request submitted";
            logRequest.ZipCodeCheckName = "OK";
            DescriptionValidationLogger logger = new DescriptionValidationLogger(XrmDataContext);
            logger.LogValidation(logRequest);
        }

        private void SendServiceSavedSMS(NoProblem.Core.DataModel.Xrm.incident incident)
        {
            try
            {
                //we send null in the ctor becuase xrmDataContext is not a thread safe object, so we need a new one for this thread.
                Notifications notificationsManager = new Notifications(null);
                var method = notificationsManager.GetNotificationMethod(NotificationConfigurationKeys.NOTIFICATION_CUSTOMER_NEW_SERVICE_REQUEST);
                if (method != enumNotificationMethod.NONE && method != enumNotificationMethod.ONLY_MAIL)
                {
                    string body, subject;
                    notificationsManager.InstatiateTemplate(TemplateNames.CUSTOMER_NEW_SERVICE_REQUEST, out subject, out body);
                    string nameOfServiceProvider = incident.new_new_primaryexpertise_incident.new_ivrname;
                    if (string.IsNullOrEmpty(nameOfServiceProvider))
                    {
                        nameOfServiceProvider = "a service provider";
                    }
                    body = string.Format(body, nameOfServiceProvider.ToLower(), FoneApiBL.TelephonesUtils.MakeUpCallerIdForCustomer(incident.new_telephone1));
                    DataAccessLayer.Contact contactDal = new NoProblem.Core.DataAccessLayer.Contact(notificationsManager.XrmDataContext);
                    var customersList = contactDal.FindConsumersFast(null, incident.customerid.Value);
                    if (customersList.Count > 0)
                    {
                        string phone = customersList.First().Phone;
                        notificationsManager.SendSMSTemplate(body, phone, incident.new_country);
                    }
                    //notificationsManager.NotifyCustomer(enumCustomerNotificationTypes.NEW_SERVICE_REQUEST, incident.customerid.Value, incident.incidentid.ToString(), "incident", incident.incidentid.ToString(), "incident");
                }

                {
                    //Send email to the team:
                    DML.Xrm.DataContext localDataContext = notificationsManager.XrmDataContext;//this method runs async.
                    DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(localDataContext);
                    string recipientEmail = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.NEW_REQUEST_ALERT_EMAIL);
                    string subject = string.Empty;
                    string body = string.Empty;
                    notificationsManager.InstatiateTemplate(DML.TemplateNames.NEW_REQUEST_ALERT, incident.incidentid, "incident", out subject, out body);
                    notificationsManager.SendEmail(recipientEmail, body, subject, configDal);

                    //send to slack (uncomment when paying for slack so we don't waste all our trial messages)
                    //Slack.SlackNotifier slack = new Slack.SlackNotifier(subject + "\n" + body, Slack.SlackNotifier.eSlackChannel.new_request);
                    //slack.Start();
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ServiceRequestManager.SendServiceSavedSMS (async method)");
            }
        }

        private void SendServiceRequestAsynchronously(Guid incidentId, IServiceResponse response, IServiceRequest request, eServiceType serviceType)
        {
            ServiceRequest.SendServiceRequestThread thread = new SendServiceRequestThread(incidentId, response, request, serviceType);
            thread.Start();
        }

        internal void SendServiceRequest(DML.Xrm.incident incident, IServiceResponse response, IServiceRequest request, eServiceType serviceType)
        {
            SendServiceRequest(incident, response, request, serviceType, false);
        }

        internal void SendServiceRequest(DML.Xrm.incident incident, IServiceResponse response, IServiceRequest request, eServiceType serviceType, bool noAar)
        {
            if (request is DataModel.ServiceRequest && ((DataModel.ServiceRequest)request).PreSoldToAdvertiserId != Guid.Empty)//is a presold request (like 1800 dentist iframe form)
            {
                HandlePreSoldRequest(incident, request);
                return;
            }
            if (!noAar && IsAarTestCategory(incident.new_new_primaryexpertise_incident))
            {
                AAR.AarManager manager = new NoProblem.Core.BusinessLogic.AAR.AarManager(XrmDataContext);
                bool usingAar = manager.ContinueServiceRequest(incident.incidentid);
                if (usingAar)
                    return;
            }
            List<DML.Xrm.new_incidentaccount> incidentAccountList;
            List<DML.AdvertiserRankings.SupplierEntry> suppliersList = GetSuppliers(incident, serviceType);
            if (suppliersList.Count == 0)
            {
                SetIncidentStatusToNoSuppliers(incident, response);
                return;
            }
            if (incident.casetypecode != (int)DML.Xrm.incident.CaseTypeCode.SupplierSpecified)
            {
                incidentAccountList = CreateIncidentAccounts(incident, suppliersList);
            }
            else
            {
                SupplierServiceRequest supplierSpecificRequest = request as SupplierServiceRequest;
                bool supplierAvailable = suppliersList.Exists(x => x.SupplierId == supplierSpecificRequest.SupplierId);
                if (supplierAvailable)
                {
                    DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                    DML.Xrm.account supplier = accountRepositoryDal.Retrieve(supplierSpecificRequest.SupplierId);
                    incidentAccountList = CreateIncidentAccounts(incident, supplier);
                }
                else
                {
                    SetIncidentStatusToNoSuppliers(incident, response);
                    return;
                }
            }

            DataAccessLayer.PrimaryExpertise primaryDAL = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            DML.Xrm.new_primaryexpertise primary = primaryDAL.Retrieve(incident.new_primaryexpertiseid.Value);
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            incidentDal.Update(incident);
            XrmDataContext.SaveChanges();
            BiddingManagerThread bidManager = new BiddingManagerThread(incident.incidentid);
            bidManager.Start();
        }

        private bool IsAarTestCategory(DML.Xrm.new_primaryexpertise category)
        {
            return category.new_aar.IsTrue();
        }

        private void HandlePreSoldRequest(DML.Xrm.incident incident, IServiceRequest request)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(((DataModel.ServiceRequest)request).PreSoldToAdvertiserId);
            string preSoldMessage = ((DataModel.ServiceRequest)request).PreSoldMessage;
            DataAccessLayer.IncidentAccount incAccDal = new DataAccessLayer.IncidentAccount(XrmDataContext);
            var incAccs = CreateIncidentAccounts(incident, supplier, incAccDal);
            var incAcc = incAccs.First();
            incAcc.new_rejectedreason = preSoldMessage;
            incAcc.new_referred = true;
            if (((DataModel.ServiceRequest)request).PreSoldPrice.HasValue && ((DataModel.ServiceRequest)request).PreSoldPrice.Value > 0)
            {
                BiddingManager.UpdateCallPrice(supplier, incAcc, ((DataModel.ServiceRequest)request).PreSoldPrice.Value);
            }
            CloseIncidentAccountAfterLeadBuyer(incAcc, incident, new DataAccessLayer.Incident(XrmDataContext), incAccDal, false);
            incident.statuscode = (int)DML.Xrm.incident.Status.WORK_DONE;
            incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
            ServiceRequestCloser closer = new ServiceRequestCloser(XrmDataContext);
            closer.CloseRequest(incident);
        }

        private void ContinueRequestToChannel(DML.Xrm.incident incident, List<DML.Xrm.new_incidentaccount> incidentAccountList, DML.Xrm.new_channel.ChannelCode eChannelCode)
        {
            //get channel manager by the channel.
            Channels.ChannelManager channelManager = Channels.ChannelManagerFactory.GetChannelManager(eChannelCode, XrmDataContext);
            SendRequestToChannelDelegate asyncSender = new SendRequestToChannelDelegate(channelManager.SendRequestToChannel);
            asyncSender.BeginInvoke(incident, incidentAccountList, null, null);
        }

        

        private void SellLeadsToLeadBuyers(
            DML.Xrm.incident incident,
            DataAccessLayer.Incident incidentDal,
            bool soldAtLeastOnce)
        {
            DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            List<DML.Xrm.new_incidentaccount> incidentAccountList = GetSortedIncidentAccountsList(incident.incidentid, incidentAccountDal);
            bool swapUpPAPI = GiveHigherPriorityToHigherPayingPhoneApi(incidentAccountList);
            DataAccessLayer.AccountExpertise acExDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            bool waitForBrokerCall = false;
            for (int i = 0; i < incidentAccountList.Count && incident.new_requiredaccountno > incident.new_ordered_providers; i++)
            {
                if (!soldAtLeastOnce)
                {
                    decimal topExclusivePrice = -1;
                    int topExclusiveIndex = -1;
                    for (int j = 0; j < incidentAccountList.Count; j++)
                    {
                        if (incidentAccountList[j].statuscode != (int)DML.Xrm.new_incidentaccount.Status.LOST
                            && incidentAccountList[j].new_account_new_incidentaccount.new_isexclusivebroker.HasValue
                            && incidentAccountList[j].new_account_new_incidentaccount.new_isexclusivebroker.Value)
                        {
                            topExclusiveIndex = j;
                            topExclusivePrice = GetPriceToUse(incidentAccountList[j], swapUpPAPI);
                            break;
                        }
                    }
                    decimal sigma = 0;
                    int topSigmaIndex = -1;
                    int count = 0;
                    for (int j = 0; j < incidentAccountList.Count && count < incident.new_requiredaccountno; j++)
                    {
                        if (incidentAccountList[j].statuscode == (int)DataModel.Xrm.new_incidentaccount.Status.LOST)
                        {
                            continue;
                        }
                        var sup = incidentAccountList[j].new_account_new_incidentaccount;
                        bool isExclusive = sup.new_isexclusivebroker.HasValue && sup.new_isexclusivebroker.Value;
                        if (!isExclusive)
                        {
                            count++;
                            sigma += GetPriceToUse(incidentAccountList[j], swapUpPAPI);
                            if (topSigmaIndex < 0)
                            {
                                topSigmaIndex = j;
                            }
                        }
                    }

                    if (sigma < topExclusivePrice)//sigma loses
                    {
                        if (i != topExclusiveIndex)
                        {
                            i = topExclusiveIndex;
                        }
                    }
                    else//sigma won
                    {
                        if (i == topExclusiveIndex)
                        {
                            i = topSigmaIndex;
                        }
                        /*
                        if(incident.casetypecode == (int)DML.Xrm.incident.CaseTypeCode.Video)
                            SendPushNotificationsToNextWinners(incidentAccountList, i, incident, incidentAccountDal);
                         * */
                    }
                }

                var supplier = incidentAccountList[i].new_account_new_incidentaccount;

                bool isExclusiveBroker = supplier.new_isexclusivebroker.HasValue && supplier.new_isexclusivebroker.Value;
                if (isExclusiveBroker & soldAtLeastOnce)
                {
                    continue;
                }
                var acEx = acExDal.GetByAccountAndPrimaryExpertise(supplier.accountid, incident.new_primaryexpertiseid.Value);
                var buyerAns = SendLeadToBuyer(incident, incidentAccountList[i], acEx, supplier, incidentDal, incidentAccountList[i].new_pingid);
                if (buyerAns != null)
                {
                    incidentAccountList[i].new_referred = !buyerAns.FailedInnerValidation;
                    if (buyerAns.IsInCall)
                    {
                        incidentAccountDal.Update(incidentAccountList[i]);
                        XrmDataContext.SaveChanges();
                        waitForBrokerCall = true;
                        break;
                    }
                    else if (buyerAns.IsSold)
                    {
                        incidentAccountList[i].new_dialercallid = buyerAns.BrokerId;
                        incidentAccountList[i].new_rejectedreason = buyerAns.Reason;
                        CloseIncidentAccountAfterLeadBuyer(incidentAccountList[i], incident, incidentDal, incidentAccountDal, false);
                        soldAtLeastOnce = true;
                        if (isExclusiveBroker)
                        {
                            break;
                        }
                    }
                    else
                    {
                        incidentAccountList[i].new_isrejected = buyerAns.MarkAsRejected;
                        incidentAccountList[i].new_dialercallid = buyerAns.BrokerId;
                        incidentAccountList[i].new_rejectedreason = buyerAns.Reason;
                        MarkIncidentAccountAsLostAfterLeadBuyerFailed(incidentAccountDal, incidentAccountList[i]);
                    }
                }
                else
                {
                    incidentAccountList[i].new_rejectedreason = "Broker not configured correctly";
                    MarkIncidentAccountAsLostAfterLeadBuyerFailed(incidentAccountDal, incidentAccountList[i]);
                }
            }
            if (!waitForBrokerCall)
            {
                StopAll(incident, incidentDal, false);
            }
        }
        
        
        

        private List<DML.Xrm.new_incidentaccount> GetSortedIncidentAccountsList(Guid incidentId, DataAccessLayer.IncidentAccount incidentAccountDal)
        {
            List<DML.Xrm.new_incidentaccount> incidentAccountList = GetNonDoneIncidentAccounts(incidentId, incidentAccountDal);
            incidentAccountList.Sort(CompareIncidentAccountBySellingOrder);
            RandomizeEqualities(incidentAccountList);
            return incidentAccountList;
        }
        
        private static List<DML.Xrm.new_incidentaccount> GetNonDoneIncidentAccounts(Guid incidentId, DataAccessLayer.IncidentAccount incidentAccountDal)
        {
            List<Guid> guids = incidentAccountDal.GetNonDoneIncidentAccount(incidentId);
            List<DML.Xrm.new_incidentaccount> incidentAccountList = new List<NoProblem.Core.DataModel.Xrm.new_incidentaccount>();
            foreach (var g in guids)
            {
                DML.Xrm.new_incidentaccount ia = incidentAccountDal.Retrieve(g);
                incidentAccountList.Add(ia);
            }
            return incidentAccountList;
        }
        /*
        private void SendPushNotificationsToNextWinners(List<DML.Xrm.new_incidentaccount> incidentAccountList, int topIndex, DML.Xrm.incident incident, DataAccessLayer.IncidentAccount iaDal)
        {
            try
            {
                int count = 0;
                for (int j = 0; j < incidentAccountList.Count && count < incident.new_requiredaccountno; j++)
                {
                    if (incidentAccountList[j].statuscode == (int)DataModel.Xrm.new_incidentaccount.Status.LOST)
                    {
                        continue;
                    }
                    var sup = incidentAccountList[j].new_account_new_incidentaccount;
                    bool isExclusive = sup.new_isexclusivebroker.HasValue && sup.new_isexclusivebroker.Value;
                    if (!isExclusive)
                    {
                        count++;
                        if (j != topIndex)
                        {
                            SetEvaluatingStatusToStep2(incidentAccountList[j], iaDal);
                            SendWinnerPushNotification(incidentAccountList[j].new_account_new_incidentaccount, incidentAccountList[j].new_incidentaccountid);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SendPushNotificationsToNextWinners");
            }
        }
        */
        private void SendPushNotificationsToNextWinners(DML.Xrm.new_incidentaccount incidentAccount, DML.Xrm.incident incident, DataAccessLayer.IncidentAccount iaDal)
        {
            try
            {
          //      var sup = incidentAccount.new_account_new_incidentaccount;
         //       bool isExclusive = sup.new_isexclusivebroker.HasValue && sup.new_isexclusivebroker.Value;
        //        if (!isExclusive)
      //          {
                    
                    SetEvaluatingStatusToStep2(incidentAccount, iaDal);
                    SendWinnerPushNotification(incidentAccount.new_account_new_incidentaccount, incidentAccount.new_incidentaccountid);
                    
     //           }
                
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SendPushNotificationsToNextWinners");
            }
        }
        private void SetEvaluatingStatusToStep2(DML.Xrm.new_incidentaccount incidentaccount, DataAccessLayer.IncidentAccount iaDal)
        {
            incidentaccount.new_evaluatingstep2 = true;
            iaDal.Update(incidentaccount);
            iaDal.XrmDataContext.SaveChanges();
        }

        private void SendWinnerPushNotification(DML.Xrm.account account, Guid incidentAccountId)
        {
            PushNotifications.NpPushNotifier pushNotifier = new PushNotifications.NpPushNotifier(XrmDataContext);
            pushNotifier.SendPushNotification(account.accountid, BuildWinnerPushData(incidentAccountId), PushNotifications.ePushTypes.CONGRATULATIONS_WON_LEAD);
        }

        private Dictionary<string, string> BuildWinnerPushData(Guid incidentAccountId)
        {
            Dictionary<string, string> retVal = new Dictionary<string, string>()
                { 
                    { "id", incidentAccountId.ToString() }
                };
            return retVal;
        }

        private bool GiveHigherPriorityToHigherPayingPhoneApi(List<DML.Xrm.new_incidentaccount> incidentAccountList)
        {
            //check there are at least two suppliers
            if (incidentAccountList.Count < 2)
                return false;
            DataModel.Xrm.account firstSupplier = incidentAccountList.First().new_account_new_incidentaccount;
            //check is first one is already phone api
            if (firstSupplier.IsPhoneApi())
                return false;
            decimal priceOfFirst = incidentAccountList.First().new_potentialincidentprice.Value;
            int index = 0;
            bool swapUp = false;
            for (int i = 1; i < incidentAccountList.Count; i++)
            {
                if (incidentAccountList[i].new_potentialincidentprice.Value > priceOfFirst
                    && incidentAccountList[i].new_account_new_incidentaccount.IsPhoneApi())
                {
                    swapUp = true;
                    index = i;
                    priceOfFirst = incidentAccountList[i].new_potentialincidentprice.Value;
                }
            }
            if (swapUp)
            {
                var prioritized = incidentAccountList[index];
                incidentAccountList.RemoveAt(index);
                incidentAccountList.Insert(0, prioritized);
            }
            return swapUp;
        }

        private void RandomizeEqualities(List<NoProblem.Core.DataModel.Xrm.new_incidentaccount> incidentAccountList)
        {
            for (int i = 0; i < incidentAccountList.Count - 1; i++)
            {
                var current = incidentAccountList[i];
                var next = incidentAccountList[i + 1];
                if (CompareIncidentAccountBySellingOrder(current, next) == 0)
                {
                    if (random.Next(2) == 1)
                    {
                        incidentAccountList[i] = next;
                        incidentAccountList[i + 1] = current;
                    }
                }
            }
        }

        private int CompareIncidentAccountBySellingOrder(DataModel.Xrm.new_incidentaccount x, DataModel.Xrm.new_incidentaccount y)
        {
            if ((Object)x == (Object)y)
                return 0;
            decimal xPrice = GetPriceToUse(x);
            decimal yPrice = GetPriceToUse(y);
            int retVal = (xPrice.CompareTo(yPrice)) * -1;
            if (retVal == 0)
            {
                bool xIsReseller = (x.new_account_new_incidentaccount.new_isreseller.HasValue && x.new_account_new_incidentaccount.new_isreseller.Value);
                bool yIsReseller = (y.new_account_new_incidentaccount.new_isreseller.HasValue && y.new_account_new_incidentaccount.new_isreseller.Value);
                if (!xIsReseller && yIsReseller)
                    retVal = -1;
                else if (!yIsReseller && xIsReseller)
                    retVal = 1;
            }
            return retVal;
        }

        private static decimal GetPriceToUse(DataModel.Xrm.new_incidentaccount ia)
        {
            return GetPriceToUse(ia, false);
        }

        private static decimal GetPriceToUse(DataModel.Xrm.new_incidentaccount ia, bool useRealPricesOnly)
        {
            decimal price;
            if (ia.new_isdefaultpricechanged.HasValue && ia.new_isdefaultpricechanged.Value)
            {
                price = ia.new_potentialincidentprice.Value;
            }
            else
            {
                price = ia.new_considerasprice.Value;
            }
            if (!useRealPricesOnly)
            {
                decimal? billingRate = ia.new_account_new_incidentaccount.new_billingrate;
                if (billingRate.HasValue)
                {
                    price = price * billingRate.Value;
                }
            }
            return price;
        }

        private LeadBuyers.SendLeadResponse SendLeadToBuyer(
            DataModel.Xrm.incident incident,
            DataModel.Xrm.new_incidentaccount incAcc,
            DataModel.Xrm.new_accountexpertise acExp,
            DataModel.Xrm.account supplier,
            DataAccessLayer.Incident incidentDal,
            string pingId
            )
        {
            NoProblem.Core.BusinessLogic.LeadBuyers.SendLeadResponse buyerAns = null;
            if (supplier.new_isleadbuyer.HasValue && supplier.new_isleadbuyer.Value)
            {
                LeadBuyers.ILeadBuyer leadBuyer = LeadBuyers.LeadBuyerFactory.GetInstance(supplier.new_leadbuyername);
                if (leadBuyer != null)
                {
                    buyerAns = leadBuyer.SendLead(incident, acExp, supplier, incAcc, pingId);
                }
            }
            else
            {
                FoneApiBL.Handlers.JonaCallHandler handler = new NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.JonaCallHandler();
                bool rejectEnabled = IsRejectEnabledForJonaCall(incident, acExp);
                buyerAns = handler.StartCall(incident, incAcc, supplier, rejectEnabled);
            }
            return buyerAns;
        }
       

        private void SetIncidentStatusToNoSuppliers(DML.Xrm.incident incident, IServiceResponse response)
        {
            DataAccessLayer.Incident incidentDAL = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            response.Status = StatusCode.NoSuppliers;
            incident.statuscode = (int)DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER;
            incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
            ServiceRequestCloser closer = new ServiceRequestCloser(XrmDataContext);
            closer.CloseRequest(incident);
        }

        private List<DML.Xrm.new_incidentaccount> CreateIncidentAccounts(NoProblem.Core.DataModel.Xrm.incident incident, List<DML.AdvertiserRankings.SupplierEntry> suppliersTable)
        {
            List<DML.Xrm.new_incidentaccount> incidentAccountList = new List<NoProblem.Core.DataModel.Xrm.new_incidentaccount>();
            int foundSupplierCount = suppliersTable.Count;
            DataAccessLayer.ConfigurationSettings configDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string multiplierStr = configDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.AUCTION_SUPPLIERS_MULTIPLIER);
            int multiplier = int.Parse(multiplierStr);
            int suppliersAmountMultiplied = incident.new_requiredaccountno.Value * multiplier;
            int suppliersToCreateIncAcc = suppliersAmountMultiplied < foundSupplierCount ? suppliersAmountMultiplied : foundSupplierCount;
            DataAccessLayer.IncidentAccount incidentAccountDAL = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            for (int i = 0; i < suppliersToCreateIncAcc; i++)
            {
                Guid accountId = suppliersTable[i].SupplierId;
                DML.Xrm.new_incidentaccount incidentAccount = new NoProblem.Core.DataModel.Xrm.new_incidentaccount();
                incidentAccount.new_accountid = accountId;
                incidentAccount.new_incidentid = incident.incidentid;
                incidentAccount.new_potentialincidentprice = suppliersTable[i].IncidentPrice;
                incidentAccount.new_considerasprice = suppliersTable[i].ConsiderAsPrice;
                incidentAccount.new_predefinedprice = suppliersTable[i].IncidentPrice;
                incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.MATCH;
                incidentAccount.new_isjobdone = null;
                incidentAccount.new_isansweredbymachine = null;
                incidentAccountDAL.Create(incidentAccount);
                XrmDataContext.SaveChanges();
                incidentAccountList.Add(incidentAccount);
            }
            return incidentAccountList;
        }

        private List<DML.AdvertiserRankings.SupplierEntry> GetSuppliers(DML.Xrm.incident incident, eServiceType serviceType)
        {
            //TODO: if destop app case, then we need to get only local advs???
            FindSupplierParams param = new FindSupplierParams();
            if (incident.new_isupsale.HasValue && incident.new_isupsale.Value)//is upsale
            {
                DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                param.Incidents = incidentDal.GetIncidentsByUpsaleId(incident.new_upsaleid.Value);
            }
            else if (serviceType != eServiceType.SupplierSpecified)
            {
                DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string configVal = configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.NEW_INCIDENT_TIME_PASS);
                int passedDays = int.Parse(configVal);
                if (passedDays > 0)
                {
                    //int publisherTimeZoneCode = int.Parse(configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.PUBLISHER_TIME_ZONE));
                    DateTime localDate = FixDateToLocal(DateTime.Now).Date; // ConvertUtils.GetLocalFromUtc(XrmDataContext, DateTime.Now, publisherTimeZoneCode).Date;
                    DateTime midNightLocal = FixDateToUtcFromLocal(localDate); // ConvertUtils.GetUtcFromLocal(XrmDataContext, localDate, publisherTimeZoneCode);
                    DateTime dateReady = midNightLocal.AddDays((passedDays - 1) * -1);
                    DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                    param.Incidents = incidentDal.GetIncidentsByTimePass(incident.new_primaryexpertiseid.Value, incident.customerid.Value, dateReady, Guid.Empty);
                }
            }
            if (param.Incidents == null)
            {
                param.Incidents = new List<Guid>();
            }
            DataAccessLayer.Region regionDAL = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DML.Xrm.new_region region = regionDAL.Retrieve(incident.new_regionid.Value);
            param.AreaCode = region.new_code;
            param.AreaLevel = region.new_level.Value;
            string expertiseCode;
            int expertiseLevel;
            if (incident.new_secondaryexpertiseid.HasValue)
            {
                expertiseLevel = 2;
                DataAccessLayer.SecondaryExpertise secondaryDAL = new NoProblem.Core.DataAccessLayer.SecondaryExpertise(XrmDataContext);
                DML.Xrm.new_secondaryexpertise secondary = secondaryDAL.Retrieve(incident.new_secondaryexpertiseid.Value);
                expertiseCode = secondary.new_code;
            }
            else
            {
                expertiseLevel = 1;
                DataAccessLayer.PrimaryExpertise primaryDAL = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
                DML.Xrm.new_primaryexpertise primary = primaryDAL.Retrieve(incident.new_primaryexpertiseid.Value);
                expertiseCode = primary.new_code;
            }
            param.ExpertiseLevel = expertiseLevel;
            param.ExpertiseCode = expertiseCode;
            param.OriginId = incident.new_originid.Value;
            param.Availability = incident.new_preferedcalltime.Value;
            param.IsWithNoTimeNorCash = false;
            //If the case comes from the organic desktop app then we want only locals (no partners/apis).
            param.ExcludePartners = incident.new_apptype == (int)DataModel.Consumer.ServiceRequestEnums.eAppType.DeskApp;
            SqlHelper Helper = new SqlHelper(ConfigurationManager.AppSettings["updateConnectionString"]);
            DataSet searchSupplierResult = Helper.GetSuppliers(param);
            DataTable AvailableSuppliersTable = searchSupplierResult.Tables[0];
            List<DML.AdvertiserRankings.SupplierEntry> suppliersList = null;
            if (AvailableSuppliersTable.Columns[0].ColumnName == "ERROR")
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSuppliers returned with Error in SendServiceRequest for incidentId = {0}, error = {1}", incident.incidentid.ToString(), AvailableSuppliersTable.Rows[0]["ERROR"].ToString());
                suppliersList = new List<NoProblem.Core.DataModel.AdvertiserRankings.SupplierEntry>();
            }
            else
            {
                AdvertiserRankingsManager rankingManager = new AdvertiserRankingsManager(XrmDataContext);
                suppliersList = rankingManager.SortSuppliersByRanking(AvailableSuppliersTable);
            }
            return suppliersList;
        }

        private void InsertCaseTypeCodeToIncident(eServiceType serviceType, DML.Xrm.incident incident)
        {
            if (isVideoLead)
            {
                incident.casetypecode = (int)DML.Xrm.incident.CaseTypeCode.Video;
            }
            else if (serviceType == eServiceType.Normal)
            {

                incident.casetypecode = (int)DML.Xrm.incident.CaseTypeCode.Request;
            }
            else if (serviceType == eServiceType.Scheduled)
            {
                incident.casetypecode = (int)DML.Xrm.incident.CaseTypeCode.ScheduledRequest;
            }
            else
            {
                incident.casetypecode = (int)DML.Xrm.incident.CaseTypeCode.SupplierSpecified;
            }
        }

        private void InsertNumberOfRequiredSupplierToIncident(IServiceRequest request, eServiceType serviceType, DML.Xrm.incident incident)
        {
            if (serviceType != eServiceType.SupplierSpecified)
            {
                incident.new_requiredaccountno = request.NumOfSuppliers;
            }
            else
            {
                incident.new_requiredaccountno = 1;
            }
            incident.new_ordered_providers = 0;
        }

        private void InsertStatusCodeToIncident(IServiceResponse response, DML.Xrm.incident incident, bool badWordFound)
        {
            if (response.Status == StatusCode.BlackList)
            {
                incident.statuscode = (int)DML.Xrm.incident.Status.BLACKLIST;
                incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
            }
            else if (badWordFound)
            {
                response.Status = StatusCode.BadWord;
                incident.statuscode = (int)DML.Xrm.incident.Status.BADWORD;
                incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
            }
            else
            {
                incident.statuscode = (int)DML.Xrm.incident.Status.NEW;
                if (isVideoLead)
                    incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.WAITING_FOR_UPDATE;
                else
                    incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.DELIVERED_REQUEST;
            }
        }
        private decimal GetIncidentWinningPrice(DML.Xrm.incident incident)
        {
            DML.XrmDataContext.ClearCache("new_incidentaccount");
            decimal price = incident.new_winningprice.HasValue ? incident.new_winningprice.Value : 0;
            foreach (var inciAcc in incident.new_incident_new_incidentaccount)
            {
                if ((inciAcc.statuscode == (int)DML.Xrm.new_incidentaccount.Status.CLOSE ||
                    inciAcc.statuscode == (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS)
                    && inciAcc.new_potentialincidentprice.HasValue && 
                    inciAcc.new_potentialincidentprice.Value > price)
                {
                    price = inciAcc.new_potentialincidentprice.Value;
                }
            }
            return price;
        }
        private bool CheckForBadWords(IServiceRequest request, eServiceType serviceType)
        {
            bool badWordFound = false;
            BadWordManager badWordManager = new BadWordManager(XrmDataContext);
            badWordFound = badWordManager.CheckForBadWords(request.RequestDescription);
            return badWordFound;
        }

        private void InsertCallTimesToIncident(IServiceRequest request, eServiceType serviceType, DML.Xrm.incident incident)
        {
            if (serviceType == eServiceType.Scheduled)
            {
                incident.new_preferedcalltime = request.PrefferedCallTime;
            }
            else
            {
                incident.new_preferedcalltime = DateTime.Now;
            }
            incident.new_callendtime = incident.new_preferedcalltime.Value.AddHours(2);
        }

        private void InsertTitleToIncident(DML.Xrm.incident incident, DML.Xrm.new_primaryexpertise primary)
        {
            if (isVideoLead)
            {
                incident.title = "Video lead: " + primary.new_name;
            }
            else
            {
                DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                incident.title = string.Format(configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.SELF_SERVICE_INCIDENT_TITLE), primary.new_name);
            }
        }

        private DML.Xrm.new_primaryexpertise InsertExpertiseToIncident(IServiceRequest request, DML.Xrm.incident incident)
        {
            DataAccessLayer.PrimaryExpertise primaryDAL = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            DML.Xrm.new_primaryexpertise primary = primaryDAL.GetPrimaryExpertiseByCode(request.ExpertiseCode);
            incident.new_primaryexpertiseid = primary.new_primaryexpertiseid;
            return primary;
        }

        private Guid GetPrimaryExpertiseId(string expertiseCode)
        {
            DataAccessLayer.PrimaryExpertise primaryDAL = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            return primaryDAL.GetPrimaryExpertiseIdByCode(expertiseCode);
        }

        private eServiceType ResolveServiceType(IServiceRequest request, out IServiceResponse response)
        {
            eServiceType serviceType;
            if (request is DataModel.ServiceRequest || request is DataModel.ServiceRequestVideoLead)
            {
                serviceType = eServiceType.Normal;
                response = new DML.ServiceResponse();
            }
            else if (request is DML.Consumer.ScheduledServiceRequest)
            {
                serviceType = eServiceType.Scheduled;
                response = new DML.Consumer.ScheduledServiceResponse();
            }
            else
            {
                serviceType = eServiceType.SupplierSpecified;
                response = new DML.Consumer.SupplierServiceResponse();
            }
            return serviceType;
        }

        #endregion

        #region Mobile

        

        private void SendServiceRequestVideoPhoneApp(DML.Xrm.incident incident)
        {            
            List<Guid> SuppliersIds = GetAppPhoneSuppliers(incident);
            if (SuppliersIds.Count == 0)
            {
                ExecuteAar(incident.incidentid);
                return;             
                
            }

            List<DML.Xrm.new_incidentaccount> incidentAccountList;

            incidentAccountList = CreateIncidentAccounts(incident, SuppliersIds);

            BiddingManager bidManager = new BiddingManager(XrmDataContext);
            bidManager.StartBidding(incident.incidentid);

            ExecuteScheduleAar(incident.incidentid);

        }
        private void SendServiceRequestPrivateVideoPhoneApp(DML.Xrm.incident incident)
        {
            NoProblem.Core.DataAccessLayer.IncidentPrivateServiceProvider ipsp = new DataAccessLayer.IncidentPrivateServiceProvider(XrmDataContext);
            List<Guid> SuppliersIds = ipsp.GetPrivateSuppliers(incident.incidentid);
            if (SuppliersIds == null || SuppliersIds.Count == 0)
                return;
            List<DML.Xrm.new_incidentaccount> incidentAccountList = CreateIncidentAccounts(incident, SuppliersIds);

            BiddingManager bidManager = new BiddingManager(XrmDataContext);
            bidManager.StartBidding(incident.incidentid);


        }
        private List<Guid> GetAppPhoneSuppliers(DML.Xrm.incident incident)
        {
            return GetAppPhoneSuppliers(incident.incidentid, incident.new_regionid.Value, incident.new_primaryexpertiseid.Value);
        }
        public List<Guid> GetAppPhoneSuppliers(Guid incidentId, Guid regionId, Guid expertiseId)
        {
            List<Guid> list = new List<Guid>();
            string command = "EXEC [dbo].[GetSuppliersPhoneApp] @PrimaryExpertiseId, @RegionId,	@IncidentId";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.CommandTimeout = 60;
                    conn.Open();
                    cmd.Parameters.AddWithValue("@PrimaryExpertiseId", expertiseId);
                    cmd.Parameters.AddWithValue("@RegionId", regionId);
                    cmd.Parameters.AddWithValue("@IncidentId", incidentId);
                    //   cmd.Parameters.AddWithValue("@CustomerPhone", incident.new_telephone1);                    
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        list.Add((Guid)reader["AccountId"]);
                    }
                    conn.Close();
                }
            }
            return list;
        }

        internal void ContinueRequestAfterMobileBidding(Guid incidentId)
        {
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentId);
            SellMobileLeadsAfterBidding(incident, incidentDal);
        }

        private void SellMobileLeadsAfterBidding(
          DML.Xrm.incident incident,
          DataAccessLayer.Incident incidentDal)
        {
            DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            List<DML.Xrm.new_incidentaccount> incidentAccountList = GetSortedIncidentAccountsListMobileAfterBidding(incident.incidentid, incidentAccountDal);
        //    bool swapUpPAPI = GiveHigherPriorityToHigherPayingPhoneApi(incidentAccountList);
            
            //      bool waitForBrokerCall = false;
            DML.Xrm.incident.Status incidentStatus;
            lock (LockExecuteMobileAuction)
            {
                if (IsMobileIncidentOpen(incident.incidentid, out incidentStatus))
                {
                    if (incidentStatus == DML.Xrm.incident.Status.NEW || incidentStatus == DML.Xrm.incident.Status.WAITING)
                    {
                        incident.statuscode = (int)DML.Xrm.incident.Status.AFTER_AUCTION;
                        incidentDal.Update(incident);
                        XrmDataContext.SaveChanges();
                        for (int i = 0; i < incidentAccountList.Count && incident.new_requiredaccountno > incident.new_ordered_providers; i++)
                        {
                            try
                            {
                                SendWInMobileLead(incidentAccountList[i], incidentAccountDal, incident, incidentDal);
                            }
                            catch(Exception exc)
                            {
                                LogUtils.MyHandle.HandleException(exc, "Exception in SellMobileLeadsAfterBidding incidentAccountId:{0}", incidentAccountList[i].new_incidentaccountid);
                            }
                        }
                    }
                   
                   
                    /*
                    if (incident.new_requiredaccountno <= incident.new_ordered_providers)
                        CloseMobileIncident(incident, incidentDal, false);
                     */
                }
            }
           
        }
        internal void SendWInMobileLead(Guid incidentAccountId, Guid incidentId, SetBidForLeadRequest request)
        {
            DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DML.Xrm.new_incidentaccount incidentaccount = incidentAccountDal.Retrieve(incidentAccountId);
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
            DML.Xrm.incident incident = incidentDal.Retrieve(incidentId);
            SendWInMobileLead(incidentaccount, incidentAccountDal, incident, incidentDal);
        }
        internal void SendWInMobileLead(DML.Xrm.new_incidentaccount incidentaccount, DataAccessLayer.IncidentAccount incidentAccountDal,
            DML.Xrm.incident incident, DataAccessLayer.Incident incidentDal)
        {
     //       DataAccessLayer.AccountExpertise acExDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
     //       var supplier = incidentaccount.new_account_new_incidentaccount;
            SendPushNotificationsToNextWinners(incidentaccount, incident, incidentAccountDal);
    //        var acEx = acExDal.GetByAccountAndPrimaryExpertise(supplier.accountid, incident.new_primaryexpertiseid.Value);
     //       var buyerAns = CreateCallMobileLeadToBuyer(incident, incidentaccount, acEx, supplier, incidentDal, incidentaccount.new_pingid);
            CloseIncidentAccountAfterLeadBuyer(incidentaccount, incident, incidentDal, incidentAccountDal, false);
            IsMobileIncidentOpen(incident.incidentid);
            ChargeAccount(incidentaccount, incidentAccountDal);
            ExecuteNextMobileCall(incident, incidentDal);
        }
        public void ExecuteNextMobileCall(DML.Xrm.incident incident, DataAccessLayer.Incident incidentDal)
        {
            DataAccessLayer.IncidentAccount incidentaccountDal = new DataAccessLayer.IncidentAccount(XrmDataContext);
            DML.Xrm.new_incidentaccount incidentaccount = incidentDal.GetNextToCall(incidentaccountDal, incident.incidentid);
            if (incidentaccount == null)
                return;
            incidentaccountDal.SetMobileCallProcess(incidentaccount.new_incidentaccountid, DML.Xrm.new_incidentaccount.CallProcess.InConversation);
            DataAccessLayer.AccountExpertise acExDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            var supplier = incidentaccount.new_account_new_incidentaccount;
            var acEx = acExDal.GetByAccountAndPrimaryExpertise(supplier.accountid, incident.new_primaryexpertiseid.Value);
            var buyerAns = CreateCallMobileLeadToBuyer(incident, incidentaccount, acEx, supplier, incidentDal, incidentaccount.new_pingid);
        }

        private void ChargeAccount(DML.Xrm.new_incidentaccount incidentaccount, DataAccessLayer.IncidentAccount incidentAccountDal)
        {
            string leadNumber = incidentAccountDal.GetTicketNumber(incidentaccount.new_incidentaccountid);
            Balance.BalanceManager balanceManager = new NoProblem.Core.BusinessLogic.Balance.BalanceManager(XrmDataContext);
            balanceManager.ChargeLead(incidentaccount.new_accountid.Value, (incidentaccount.new_potentialincidentprice.Value * -1), NoProblem.Core.DataModel.Xrm.new_balancerow.Action.Call,
                incidentaccount.new_incidentaccountid, leadNumber);
        }
        private LeadBuyers.SendLeadResponse CreateCallMobileLeadToBuyer(
           DataModel.Xrm.incident incident,
           DataModel.Xrm.new_incidentaccount incAcc,
           DataModel.Xrm.new_accountexpertise acExp,
           DataModel.Xrm.account supplier,
           DataAccessLayer.Incident incidentDal,
           string pingId
           )
        {

            FoneApiBL.Handlers.ClipCall.ClipCallHandler handler = new FoneApiBL.Handlers.ClipCall.ClipCallHandler();
      //      bool rejectEnabled = IsRejectEnabledForJonaCall(incident, acExp);
            NoProblem.Core.BusinessLogic.LeadBuyers.SendLeadResponse buyerAns = handler.StartCall(incident, incAcc, supplier);//, rejectEnabled);

            return buyerAns;
        }
        /*
        private void ChargeMobileLead(DML.Xrm.new_incidentaccount incidentaccount)
        {
            DataModel.Xrm.account.RechargeTypeCode rechargeType;
            int dayOfMonth;
            int rechargeBonusPercent;
            int numberOfPayments;
            string userAgent;
            string userIP;
            string userAcceptLanguage;
            string userRemoteHost;
            decimal cashBalance;
            string subscriptionPlan;
            dal.GetRechargeData(incidentaccount.new_accountid.Value, out rechargeAmount, out rechargeType, out dayOfMonth, out rechargeBonusPercent, 
                out numberOfPayments, out userIP, out userAgent, out userAcceptLanguage, out userRemoteHost, out cashBalance, out subscriptionPlan);

            DateTime tokenDate;
            string ccToken, cvv2, ccOwnerId, ccOwnerName, last4digits, chargingCompanyContractId, chargingCompanyAccountNumber,
                ccType, billingAddress, ccExpDate;
            decimal cashBalance;
            DataAccessLayer.SupplierPricing spDal = new DataAccessLayer.SupplierPricing(XrmDataContext);
            bool found = spDal.FindCreditCardInfo(incidentaccount.new_accountid.Value, out ccToken, out cvv2, out ccOwnerId, out ccOwnerName, 
                out last4digits, out tokenDate, 
                out chargingCompanyContractId, out chargingCompanyAccountNumber, out ccType, out ccExpDate, out billingAddress);
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            string solekName = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.SOLEK);
            CreditCardSolek.Solek solek = CreditCardSolek.SolekFactory.GetSolek(solekName);
           
            string solkAnswer, transactionConfirmationNumber;
            solek.RechargeSupplier(ccToken, cvv2, ccOwnerId, amountToChargeCC, amountToChargeCC, 1, out solekAnswer, out transactionConfirmationNumber, last4digits, ccType, chargingCompanyAccountNumber, chargingCompanyContractId,
                userIP, userAgent, userRemoteHost, userAcceptLanguage, out needsAsyncAproval, ccExpDate, ccOwnerName, null, null, DataModel.Xrm.new_paymentmethod.ePaymentMethod.CreditCard, null, false);
        }
         * */
        /*
        internal void CloseIncident(DML.Xrm.incident incident,
          DataAccessLayer.Incident incidentDal)
        {
            DataAccessLayer.IncidentAccount incidentAccountDal = new DataAccessLayer.IncidentAccount(XrmDataContext);
            List<Guid> listIncidentAccountId = GetIncidentAccountOpen(incident.incidentid);
      //      List<DML.Xrm.new_incidentaccount> incidentAccountList = new List<DML.Xrm.new_incidentaccount>();
            foreach (var g in listIncidentAccountId)
            {
                DML.Xrm.new_incidentaccount ia = incidentAccountDal.Retrieve(g);
        //        incidentAccountList.Add(ia);
                if (ia.new_biddone.HasValue && ia.new_biddone.Value)
                    ia.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
                else
                    ia.statuscode = (int)DML.Xrm.new_incidentaccount.Status.MISSED;
                incidentAccountDal.Update(ia);
                XrmDataContext.SaveChanges();
            }
            incident.statuscode = (incident.new_ordered_providers == 0) ? (int)DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER
                : (int)DML.Xrm.incident.Status.WORK_DONE;
            incidentDal.Update(incident);
            XrmDataContext.SaveChanges();

        }
         * */
        private List<DML.Xrm.new_incidentaccount> GetSortedIncidentAccountsListMobileAfterBidding(Guid incidentId, DataAccessLayer.IncidentAccount incidentAccountDal)
        {
            List<DML.Xrm.new_incidentaccount> incidentAccountList = GetIncidentAccountsListMobileAfterBidding(incidentId, incidentAccountDal);
            incidentAccountList.Sort(CompareIncidentAccountBySellingOrder);
            RandomizeEqualities(incidentAccountList);
            /*
            for (int i = 0; i < incidentAccountList.Count; i++)
            {
                incidentAccountList[i].new_mobilecallorder = i;
               // incidentAccountList[i].new_callprocess = (int)DML.Xrm.new_incidentaccount.CallProcess.WaitForCall;
                incidentAccountDal.Update(incidentAccountList[i]);
            }
            XrmDataContext.SaveChanges();
             * */
            return incidentAccountList;
        }
        private static List<DML.Xrm.new_incidentaccount> GetIncidentAccountsListMobileAfterBidding(Guid incidentId, DataAccessLayer.IncidentAccount incidentAccountDal)
        {
            List<Guid> guids = incidentAccountDal.GetIncidentAccountWaitingAfterBiddingMobile(incidentId);
            List<DML.Xrm.new_incidentaccount> incidentAccountList = new List<NoProblem.Core.DataModel.Xrm.new_incidentaccount>();
            foreach (var g in guids)
            {
                DML.Xrm.new_incidentaccount ia = incidentAccountDal.Retrieve(g);
                incidentAccountList.Add(ia);
            }
            return incidentAccountList;
        }
        internal void CloseMobileIncident(Guid incidentId, NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.TypeExecuteUser ClosedBy)
        {
            DataAccessLayer.Incident incidentDal  = new DataAccessLayer.Incident(XrmDataContext);
            DML.Xrm.incident incident = incidentDal.Retrieve(incidentId);
            CloseMobileIncident(incident, incidentDal, ClosedBy);
        }
        internal void CloseMobileIncident(DML.Xrm.incident incident, NoProblem.Core.DataAccessLayer.Incident incidentDal, 
            NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.TypeExecuteUser ClosedBy)
        {
            //close incident 

            DataAccessLayer.IncidentAccount incidentAccountDAL = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            List<Guid> guids = incidentAccountDAL.GetNonDoneMobileIncidentAccount(incident.incidentid);
            foreach (var id in guids)
            {
                DataModel.Xrm.new_incidentaccount ia = new NoProblem.Core.DataModel.Xrm.new_incidentaccount();
                ia = incidentAccountDAL.Retrieve(id);
                CloseMobileIncidentAccount(incidentAccountDAL, ia);
            }

            incident.new_winningprice = GetIncidentWinningPrice(incident);
            if (incident.statuscode != (int)DML.Xrm.incident.Status.PRIVATE)
            {
                incident.statuscode = (incident.new_ordered_providers == 0) ? (int)DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER :
                    (int)DML.Xrm.incident.Status.WORK_DONE;
            }
            incident.new_dialerstatus = ClosedBy == VideoRequests.TypeExecuteUser.Customer ? (int)DML.Xrm.incident.DialerStatus.REQUEST_CANCELED :
                (int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
            incidentDal.Update(incident);
            XrmDataContext.SaveChanges();
            NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.RelaunchIncidentLog ril =
                new VideoRequests.RelaunchIncidentLog(incident.incidentid, false, ClosedBy, null);
            ril.Start();
            if(ClosedBy == VideoRequests.TypeExecuteUser.System && (!incident.new_ordered_providers.HasValue || incident.new_ordered_providers.Value == 0))
            {
                NoProblem.Core.BusinessLogic.ClipCall.RequestSystemPushNotificationNofulfillment unFulfillmentPushNoti = new ClipCall.RequestSystemPushNotificationNofulfillment(incident.customerid.Value);
                unFulfillmentPushNoti.SendAsyncNotification();
            }
            /*
            ServiceRequestCloser closer = new ServiceRequestCloser(incidentDal.XrmDataContext);
            closer.CloseRequest(incident, incidentDal);
             * */
        }
        private void CloseMobileIncidentAccount(
           DataAccessLayer.IncidentAccount incidentAccountDal,
           DML.Xrm.new_incidentaccount ia)
        {
            ia.statuscode = ia.statuscode == (int)DataModel.Xrm.new_incidentaccount.Status.IN_AUCTION ? (int)DataModel.Xrm.new_incidentaccount.Status.LOST
                : (int)DataModel.Xrm.new_incidentaccount.Status.MISSED;
            incidentAccountDal.Update(ia);
            XrmDataContext.SaveChanges();
        }
        public List<Guid> GetIncidentAccountOpen(Guid incidentId)
        {
            List<Guid> retVal = new List<Guid>();
            string query =
                @"SELECT new_incidentaccountid
FROM new_incidentaccount ia with(nolock)
where ia.new_incidentid = @incidentId
and ia.statuscode in(@WaitingStatus, @Match)";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", incidentId);
                    cmd.Parameters.AddWithValue("@WaitingStatus", (int)DML.Xrm.new_incidentaccount.Status.Waiting);
                    cmd.Parameters.AddWithValue("@Match", (int)DML.Xrm.new_incidentaccount.Status.MATCH);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        retVal.Add((Guid)reader["new_incidentaccountid"]);
                    }
                    conn.Close();
                }
            }

            return retVal;
        } 
        internal bool IsMobileIncidentOpen(Guid IncidentId)
        {
            DML.Xrm.incident.Status IncidentStatus;
            return IsMobileIncidentOpen(IncidentId, out IncidentStatus);
        }
        internal bool IsMobileIncidentOpen(Guid IncidentId, out DML.Xrm.incident.Status IncidentStatus)
        {
            string command = @"[dbo].[IsMobileIncidentOpen]";
            bool _isOpen, _toClosedIncident;
            //and new_requiredaccountno < new_ordered_providers";//new, waiting, after bidding
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@incidentId", IncidentId);
                    SqlParameter isOpen = new SqlParameter("@isOpen", SqlDbType.Bit);
                    isOpen.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(isOpen);
                    SqlParameter toClosedAllIncidentAccount = new SqlParameter("@toClosedIncident", SqlDbType.Bit);
                    toClosedAllIncidentAccount.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(toClosedAllIncidentAccount);
                    conn.Open();
                    object o = cmd.ExecuteScalar();
                    IncidentStatus = (o == DBNull.Value) ? DML.Xrm.incident.Status.WORK_DONE : (DML.Xrm.incident.Status)((int)o);
                    _isOpen = (bool)cmd.Parameters["@isOpen"].Value;
                    _toClosedIncident = (bool)cmd.Parameters["@toClosedIncident"].Value;
                    conn.Close();
                }
            }
            if (_toClosedIncident)
                CloseMobileIncident(IncidentId, VideoRequests.TypeExecuteUser.System);
            return _isOpen;
        }
        private void ExecuteScheduleAar(Guid incidentId)
        {
            TimerContainer timerContainer = new TimerContainer();
            ttsTimersHolder.Add(timerContainer);
            timerContainer.incidentId = incidentId;
            timerContainer.Timer = new Timer(
                new TimerCallback(MobileScheduleAarTimeout),
                timerContainer,
                (MOBILE_AAR_TIMEOUT * 1000),
                System.Threading.Timeout.Infinite);
        }
        private void MobileScheduleAarTimeout(object timerContainerObj)
        {
            TimerContainer timerContainer = (TimerContainer)timerContainerObj;
            timerContainer.Timer.Dispose();
            ttsTimersHolder.Remove(timerContainer);
          //  ServiceRequest.ServiceRequestManager srManager = new ServiceRequestManager(XrmDataContext);
            this.ExecuteAar(timerContainer.incidentId);
        }
        internal Guid CreateLeadForAar(DataModel.Xrm.account supplier, Guid incidentId)
        {
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
            DataModel.Xrm.incident _incident = incidentDal.Retrieve(incidentId);
            List<DataModel.Xrm.new_incidentaccount> list = CreateIncidentAccounts(_incident, supplier);
            if (list.Count != 1)
                return Guid.Empty;
            DataModel.Xrm.new_incidentaccount _incidentAccount = list[0];
            DataAccessLayer.IncidentAccount incidentAccountDal = new DataAccessLayer.IncidentAccount(XrmDataContext);
            BiddingManager bm = new BiddingManager(XrmDataContext);
            bm.UpdateMobileBid(incidentAccountDal, _incidentAccount, 0, -1, null, null, null);
            SendWInMobileLead(_incidentAccount, incidentAccountDal, _incident, incidentDal);
            return _incidentAccount.new_incidentaccountid;
        }
        internal void ExecuteAar(Guid incidentId)
        {
            if (!MOBILE_APP_USE_AAR)
                return;
            if (!IsMobileIncidentOpen(incidentId))
                return;
            if (IsUsedAar(incidentId))
                return;
            UpdateUsedAar(incidentId, true);
            AAR.AarManager am = new AAR.AarManager(XrmDataContext);
            try
            {
                am.CreateTextMessagesForServiceRequest(incidentId, null);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Failed to ExecuteAar. incidentId = {0}", incidentId.ToString());
                UpdateUsedAar(incidentId, false);
                return;
            }
            
            
        }
        private void UpdateUsedAar(Guid incidentId, bool UsedAar)
        {
            string command = @"
UPDATE dbo.IncidentExtensionBase
	SET New_UsedAAR = @UsedAar
	WHERE IncidentId = @incidentId";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@UsedAar", UsedAar);
                    cmd.Parameters.AddWithValue("@incidentId", incidentId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
        internal DML.ClipCall.Request.eRequestsStatus RelaunchIncident(Guid incidentId, NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.TypeExecuteUser typeExecuteUser,
            NoProblem.Core.DataModel.ClipCall.Request.RelaunchChangeValues changeValues)
        {
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
            DML.Xrm.incident incident = incidentDal.Retrieve(incidentId);
            bool isPassedReview = incident.new_reviewstatus.Value == (int)NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus.Passed;
            incident.statuscode = isPassedReview ? (int)DML.Xrm.incident.Status.NEW : (int)DML.Xrm.incident.Status.IN_REVIEW;
            incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.RELAUNCH_REQUEST;
            incident.new_relaunchdate = DateTime.UtcNow;
            incident.new_startauctionon = null;
            incident.new_requiredaccountno = incident.new_ordered_providers.HasValue ? (incident.new_ordered_providers.Value + NUMBER_PROVIDERS_MOBILE) :
                NUMBER_PROVIDERS_MOBILE;
            incident.new_usedaar = false;
            incidentDal.Update(incident);
            XrmDataContext.SaveChanges();
            NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.RelaunchIncidentLog ril =
                new NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests.RelaunchIncidentLog(incident.incidentid, true, typeExecuteUser, changeValues);
            ril.Start();
            if (isPassedReview)
                SendServiceRequestVideoPhoneApp(incident);           

            return isPassedReview ? DML.ClipCall.Request.eRequestsStatus.LIVE : DML.ClipCall.Request.eRequestsStatus.REVIEW;
             
            
        }
        private void SendNewVideoLeadNotification(DML.Xrm.incident incident)
        {
            NoProblem.Core.BusinessLogic.ClipCall.CreateMobileSystemNotification noti = new NoProblem.Core.BusinessLogic.ClipCall.CreateMobileSystemNotification();
            noti.SendNewVideoLeadNotification(incident.new_new_primaryexpertise_incident.new_name,
                incident.new_new_region_incident.new_englishname, incident.new_videourl, incident.new_telephone1);
            NoProblem.Core.BusinessLogic.ClipCall.RequestSystemPushNotificationUncomfortableTime pushNoti = 
                new ClipCall.RequestSystemPushNotificationUncomfortableTime(incident.new_regionid.Value, incident.customerid.Value, incident.new_primaryexpertiseid.Value);
            pushNoti.SendAsyncNotification();
        }
        internal NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus GetIncidentReviewStatus(Guid incidentId)
        {
            NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus result;
             string command = @"
SELECT ISNULL(New_reviewStatus, 1)
FROM dbo.IncidentExtensionBase
WHERE IncidentId = @incidentId";
             using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
             {
                 using (SqlCommand cmd = new SqlCommand(command, conn))
                 {
                     conn.Open();
                     cmd.Parameters.AddWithValue("@incidentId", incidentId);
                     result = (DML.Xrm.incident.IncidentReviewStatus)((int)cmd.ExecuteScalar());
                     conn.Close();
                 }
             }
             return result;

        }
        internal NoProblem.Core.DataModel.ClipCall.Request.ReviewRejectConfirmResponse ConfirmRejectIncidentReview(Guid incidentId, bool toConfirm)
        {
            NoProblem.Core.DataModel.ClipCall.Request.ReviewRejectConfirmResponse _response = new DML.ClipCall.Request.ReviewRejectConfirmResponse();
            NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus reviewStatus = GetIncidentReviewStatus(incidentId);
            if(reviewStatus != DML.Xrm.incident.IncidentReviewStatus.WaitnigForReview)
            {
                _response.IsSuccess = false;
                _response.message = DML.ClipCall.Request.ReviewRejectConfirmResponse.eMessage.ALREADY_REVIEWED;
                return _response;
            }
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
            DML.Xrm.incident _incident = incidentDal.Retrieve(incidentId);
            /*
            string command = @"
UPDATE dbo.IncidentExtensionBase 
SET New_reviewStatus = @reviewStatus
WHERE IncidentId = @incidentId";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@reviewStatus", (toConfirm) ? (int)NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus.Passed :
                        (int)NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus.Failed);
                    cmd.Parameters.AddWithValue("@incidentId", incidentId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
             * */
            _incident.new_reviewstatus = (toConfirm) ? (int)NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus.Passed :
                        (int)NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus.Failed;
            if (_incident.statuscode == (int)NoProblem.Core.DataModel.Xrm.incident.Status.PRIVATE)
            {
                ConfirmRejectPrivateIncidentReview(incidentDal, _incident, toConfirm);
                incidentDal.Update(_incident);
                XrmDataContext.SaveChanges();
                _response.IsSuccess = true;
                _response.message = DML.ClipCall.Request.ReviewRejectConfirmResponse.eMessage.OK;
                return _response;
            }
            if(!IsMobileIncidentOpen(incidentId))
            {
                _response.IsSuccess = true;
                _response.message = DML.ClipCall.Request.ReviewRejectConfirmResponse.eMessage.OK;
                incidentDal.Update(_incident);
                XrmDataContext.SaveChanges();
                return _response;
            }           
            if(toConfirm)
            {
                bool hasPhoneNumber = !string.IsNullOrEmpty(_incident.incident_customer_contacts.mobilephone);//contactDal.HasPhoneNumber(_incident.incident_customer_contacts.mobilephone.customerid.Value);
                _incident.statuscode = hasPhoneNumber ? (int)NoProblem.Core.DataModel.Xrm.incident.Status.NEW
                    : (int)NoProblem.Core.DataModel.Xrm.incident.Status.WATING_FOR_PHONE_VALIDATION;
                incidentDal.Update(_incident);
                XrmDataContext.SaveChanges();
                if (hasPhoneNumber)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
                    {
                        DML.Xrm.incident _incidentIn = (DML.Xrm.incident)state;
                        SendServiceRequestVideoPhoneApp(_incidentIn);
                    }), _incident);
                }
            }
            else
            {
                CloseMobileIncident(incidentId, VideoRequests.TypeExecuteUser.Publisher);
                incidentDal.Update(_incident);
                XrmDataContext.SaveChanges();
            }
            
            _response.IsSuccess = true;
            _response.message = DML.ClipCall.Request.ReviewRejectConfirmResponse.eMessage.OK;
            return _response;

        }
        private void ConfirmRejectPrivateIncidentReview(DataAccessLayer.Incident incidentDal, DML.Xrm.incident _incident, bool toConfirm)
        {
            if (!toConfirm)
                return;
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
            {
                DML.Xrm.incident _incidentIn = (DML.Xrm.incident)state;
                SendServiceRequestPrivateVideoPhoneApp(_incidentIn);
            }), _incident);
            
        }
        internal void ExecuteIncidentWaitingForPhoneValidation(Guid customerId)
        {
            List<Guid> incidentList = new List<Guid>();
            string command = @"SELECT IncidentId
FROM dbo.Incident WITH(NOLOCK)
WHERE ContactId = @customerId
	and StatusCode = @statusCode";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@customerId", customerId);
                    cmd.Parameters.AddWithValue("@statusCode", (int)NoProblem.Core.DataModel.Xrm.incident.Status.WATING_FOR_PHONE_VALIDATION);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        incidentList.Add((Guid)reader["IncidentId"]);
                    }
                    conn.Close();
                }
            }
            foreach(Guid incidentId in incidentList)
            {
                DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
                DML.Xrm.incident _incident = incidentDal.Retrieve(incidentId);
                if (_incident.new_reviewstatus == (int)NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus.Passed)
                {
                    _incident.statuscode = (int)NoProblem.Core.DataModel.Xrm.incident.Status.NEW;
                    _incident.new_relaunchdate = DateTime.Now;
                    incidentDal.Update(_incident);
                    XrmDataContext.SaveChanges();
                    SendServiceRequestVideoPhoneApp(_incident);
                }
                else if(_incident.new_reviewstatus == (int)NoProblem.Core.DataModel.Xrm.incident.IncidentReviewStatus.WaitnigForReview)
                {
                    _incident.statuscode = (int)NoProblem.Core.DataModel.Xrm.incident.Status.IN_REVIEW;
                    incidentDal.Update(_incident);
                    XrmDataContext.SaveChanges();
                }
            }
        }
        internal bool IsUsedAar(Guid incidentId)
        {
            string command = @"
SELECT ISNULL(i.New_UsedAAR, 0) 
from dbo.Incident i WITH(NOLOCK)
where i.DeletionStateCode = 0
	and i.CaseTypeCode = 200004 --Mobile video
	and i.StatusCode in (2, 200000, 200022)
	and i.IncidentId = @incidentId";
            bool result;
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@incidentId", incidentId);
                    object obj = cmd.ExecuteScalar();
                    result = (obj == null || obj == DBNull.Value) ? false : (bool)obj;
                    conn.Close();
                }
            }
            return result;
        }
        private class TimerContainer
        {
            public Timer Timer { get; set; }
            public Guid incidentId { get; set; }
            public DateTime TimerStartTime { get; set; }
        }
        
        #endregion


    }
}
