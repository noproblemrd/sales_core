﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    internal class BiddingManagerThread : Runnable
    {
        public BiddingManagerThread(Guid incidentId)
        {
            this.incidentId = incidentId;
        }

        Guid incidentId;

        protected override void Run()
        {
            try
            {
                BiddingManager bidManager = new BiddingManager(XrmDataContext);
                bidManager.StartBidding(incidentId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in BiddingManagerThread.Run");
            }
        }
    }
}
