﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class AutoUpsaleManager : XrmUserBase
    {
        #region Ctor
        public AutoUpsaleManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
            originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            primaryExpertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
        }
        #endregion

        #region Fields

        DataAccessLayer.ConfigurationSettings configDal;
        DataAccessLayer.Upsale upsaleDal;
        DataAccessLayer.Origin originDal;
        DataAccessLayer.PrimaryExpertise primaryExpertiseDal;
        DataAccessLayer.Incident incidentDal;
        DataAccessLayer.Region regionDal;
        DataAccessLayer.AccountRepository accountRepositoryDal;

        #endregion

        #region Public Methods

        public void StartAutoUpsales()
        {
            Thread tr = new Thread(
                delegate()
                {
                    try
                    {
                        StartAutoUpsalesPrivate();
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception in async method AutoUpsaleManager.StartAutoUpsalesPrivate");
                    }
                });
            tr.Start();
        }

        #endregion

        #region Private Methods

        private void StartAutoUpsalesPrivate()
        {
            if (!IsRunable())
            {
                return;
            }
            int hoursBack = GetAutoUpsaleHoursBack();
            Guid userId = accountRepositoryDal.GetAutoUpsaleUserId();
            Guid upsaleOriginId = originDal.GetAutoUpsaleOriginId();
            List<Guid> sentSuppliersList = new List<Guid>();
            int autoUpsalesSent = 0;
            int totalUpsales;
            var upsales = upsaleDal.GetAllOpenUpsales(DateTime.Now.AddHours(hoursBack * -1), DateTime.Now.AddHours(-2), Guid.Empty, 1, -1, out totalUpsales, false);
            foreach (var upsale in upsales)
            {
                try
                {
                    if (autoUpsalesSent >= 10)
                    {
                        break;
                    }
                    DoAutoUpsale(userId, upsaleOriginId, sentSuppliersList, upsale, ref autoUpsalesSent);
                }
                catch (ThreadAbortException threadAbortException)
                {
                    LogUtils.MyHandle.HandleException(threadAbortException, "ThreadAbortException caught and Thread.ResetAbort() called.");
                    Thread.ResetAbort();
                    XrmDataContext.ClearChanges();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception doing auto upsale for upsaleId {0}", upsale.UpsaleId);
                }
            }
        }

        private void DoAutoUpsale(Guid userId, Guid upsaleOriginId, List<Guid> sentSuppliersList, NoProblem.Core.DataModel.Upsale.UpsaleData upsale, ref int autoUpsalesSent)
        {
            List<Guid> incidentsIds = incidentDal.GetIncidentsByUpsaleId(upsale.UpsaleId);
            foreach (var inciId in incidentsIds)
            {
                //check if incident has region
                var incident = incidentDal.Retrieve(inciId);
                if (!incident.new_regionid.HasValue)
                {
                    continue;
                }
                if (incident.statuscode == (int)DataModel.Xrm.incident.Status.BADWORD
                    || incident.statuscode == (int)DataModel.Xrm.incident.Status.BLACKLIST)
                {
                    break;
                }
                if (incident.statuscode == (int)DataModel.Xrm.incident.Status.OnAar
                    || incident.statuscode == (int)DataModel.Xrm.incident.Status.Scraping
                    || incident.statuscode == (int)DataModel.Xrm.incident.Status.WAITING)
                {
                    continue;
                }
                //check if the inicent was stopped manually. if yes, continue to next upsale becuase probably a bad consumer.
                if (incident.new_isstoppedmanually.HasValue && incident.new_isstoppedmanually.Value)
                {
                    break;
                }
                if (incident.new_isresearch.HasValue && incident.new_isresearch.Value)
                {
                    break;
                }
                //check wanted to given difference
                int difference = incident.new_requiredaccountno.Value - incident.new_ordered_providers.Value;
                if (difference <= 0)
                {
                    continue;
                }
                if (IsNotToPayAffiliate(incident))
                {
                    continue;
                }
                //get expertise and region data.
                int expertiseLevel = GetExpertiseLevel(upsale.ExpertiseCode);
                int regionLevel;
                string regionCode = GetRegionCode(incident, out regionLevel);
                //find available suppliers
                List<Guid> foundSuppliersList = new List<Guid>();
                int numberOfSuppliers = FindSuppliers(upsaleOriginId, upsale, expertiseLevel, regionLevel, regionCode, foundSuppliersList);
                if (numberOfSuppliers == 0)
                {
                    continue;
                }
                //Check suppliers are not in sent list.
                var intersection = foundSuppliersList.Intersect(sentSuppliersList);
                if (intersection.Count() > 0)
                {
                    continue;
                }
                numberOfSuppliers = Math.Min(numberOfSuppliers, difference);
                //Check upsale is still open.
                var upsaleXrm = upsaleDal.Retrieve(upsale.UpsaleId);
                if (upsaleXrm.statuscode != (int)DataModel.Xrm.new_upsale.UpsaleStatus.Open)
                {
                    break;
                }
                //send request
                //SendServiceRequest(userId, upsaleOriginId, upsale, incident, expertiseLevel, regionLevel, regionCode, numberOfSuppliers);
                InitiateAutoUpsaleCallBack(incident.incidentid, upsale.UpsaleId, incident.new_telephone1);
                //add supplier to sent list
                sentSuppliersList.AddRange(foundSuppliersList);
                //increase sent count
                autoUpsalesSent++;
                break;
            }
        }

        private bool IsNotToPayAffiliate(NoProblem.Core.DataModel.Xrm.incident incident)
        {
            bool retVal = false;
            DataAccessLayer.AffiliatePayStatus dal = new NoProblem.Core.DataAccessLayer.AffiliatePayStatus(XrmDataContext);
            if (incident.new_affiliatepaystatusid.HasValue)
            {
                var payStatus = dal.Retrieve(incident.new_affiliatepaystatusid.Value);
                if (!payStatus.new_istopay.HasValue || !payStatus.new_istopay.Value)
                {
                    retVal = true;
                }
            }
            return retVal;
        }

        private bool IsRunable()
        {
            bool retVal = true;
            string enabled = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUTO_UPSALE_ENABLED);
            if (enabled != "1")
            {
                retVal = false;
            }
            if (retVal)
            {
                BusinessClosures.ClosuresManager closures = new NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager(XrmDataContext);
                bool isClosureNow = closures.IsNowClosure(NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager.ClosureOptions.WeeklyAndClosureDates);
                if (isClosureNow)
                {
                    retVal = false;
                }
            }
            return retVal;
        }

        private int GetAutoUpsaleHoursBack()
        {
            string hoursBackStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUTO_UPSALE_HOURS_BACK);
            int hoursBack;
            if (int.TryParse(hoursBackStr, out hoursBack) == false)
            {
                hoursBack = 24;
            }
            return hoursBack;
        }

        //We used this before we had the call back feature
        //private void SendServiceRequest(Guid userId, Guid upsaleOriginId, NoProblem.Core.DataModel.Upsale.UpsaleData upsale, NoProblem.Core.DataModel.Xrm.incident incident, int expertiseLevel, int regionLevel, string regionCode, int numberOfSuppliers)
        //{
        //    Thread tr = new Thread(delegate()
        //    {
        //        ServiceRequestManager ssManager = new ServiceRequestManager(null);
        //        DataModel.ServiceRequest request = new NoProblem.Core.DataModel.ServiceRequest();
        //        request.ContactPhoneNumber = upsale.ConsumerPhone;
        //        request.ExpertiseCode = upsale.ExpertiseCode;
        //        request.ExpertiseType = expertiseLevel;
        //        request.NumOfSuppliers = numberOfSuppliers;
        //        request.OriginId = upsaleOriginId;
        //        request.RegionCode = regionCode;
        //        request.RegionLevel = regionLevel;
        //        request.RequestDescription = incident.description;
        //        request.UpsaleId = upsale.UpsaleId;
        //        request.UserId = userId;
        //        ssManager.CreateService(request);
        //    });
        //    tr.Start();
        //}

        private int FindSuppliers(Guid upsaleOriginId, NoProblem.Core.DataModel.Upsale.UpsaleData upsale, int expertiseLevel, int regionLevel, string regionCode, List<Guid> foundSuppliersList)
        {
            Supplier supplierLogic = new Supplier(XrmDataContext, null);
            var suppliersXml = supplierLogic.SearchSiteSuppliers(string.Empty, upsale.ExpertiseCode, expertiseLevel, regionCode, regionLevel, upsaleOriginId.ToString(), -1, upsale.UpsaleId, -1, -1, false, false, true, null);
            var supplierElements = suppliersXml.Elements("Supplier");
            int count = supplierElements.Count();
            foreach (var element in supplierElements)
            {
                foundSuppliersList.Add(new Guid(element.Attribute("SupplierId").Value));
            }
            return count;
        }

        private string GetRegionCode(NoProblem.Core.DataModel.Xrm.incident incident, out int regionLevel)
        {
            var region = regionDal.Retrieve(incident.new_regionid.Value);
            regionLevel = region.new_level.Value;
            return region.new_code;
        }

        private void Dispatch(Guid id)
        {
            var incident = incidentDal.Retrieve(id);
            ServiceRequestManager srManager = new ServiceRequestManager(XrmDataContext);
            DataModel.ServiceRequest request = new NoProblem.Core.DataModel.ServiceRequest();

        }

        private int GetExpertiseLevel(string code)
        {
            int retVal;
            var primary = primaryExpertiseDal.GetPrimaryExpertiseByCode(code);
            if (primary != null)
            {
                retVal = 1;
            }
            else
            {
                retVal = 2;
            }
            return retVal;
        }

        public void InitiateAutoUpsaleCallBack(Guid incidentId, Guid upsaleId, string consumerPhone)
        {
            CallBack.AutoUpsaleCallBackManager callBackManager = new NoProblem.Core.BusinessLogic.ServiceRequest.CallBack.AutoUpsaleCallBackManager(XrmDataContext);
            callBackManager.InitiateAutoUpsaleCallBack(incidentId, upsaleId, consumerPhone);
        }

        #endregion
    }
}
