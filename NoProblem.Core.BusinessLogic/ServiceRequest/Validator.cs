﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using NoProblem.Core.DataModel;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    internal class Validator : ValidatorBase
    {
        private static DataAccessLayer.ConfigurationSettings configs = new DataAccessLayer.ConfigurationSettings(null);
        private static DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(null);
        private static DataAccessLayer.PrimaryExpertise primaryDAL = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(null);
        private static DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
        private static Guid unknownOriginId;

        static Validator()
        {
            string id = configs.GetConfigurationSettingValue(DataModel.ConfigurationKeys.UNKNOWN_ORIGIN_ID);
            id.TryParseToGuid(out unknownOriginId);
        }

        internal static Result ValidateServiceRequest(IServiceRequest request)
        {
            Result result = new Result();
            if (request.ContactId == Guid.Empty)
                CheckCustomer(request, result);
            CheckExpertise(request, result);
            CheckNumOfSuppliers(request, result);
            CheckOrigin(request);
            CheckRegionCode(request, result);
            CheckRegionLevel(request, result);
            return result;
        }

        private static void CheckCustomer(IServiceRequest request, Result result)
        {
            if (string.IsNullOrEmpty(request.ContactPhoneNumber))
            {
                AddFailureValidation(result, "ContactPhoneNumber");
            }
            else
            {
                string phoneToCheck = request.ContactPhoneNumber;
                if (phoneToCheck.StartsWith("+"))
                {
                    phoneToCheck = phoneToCheck.Substring(1);
                }
                for (int i = 0; i < phoneToCheck.Length; i++)
                {
                    if (char.IsDigit(phoneToCheck, i) == false)
                    {
                        AddFailureValidation(result, "ContactPhoneNumber must be only digits");
                        break;
                    }
                }
            }
        }

        private static void CheckNumOfSuppliers(IServiceRequest request, Result result)
        {
            if (request.NumOfSuppliers < 1 && (request is DataModel.Consumer.SupplierServiceRequest) == false)
            {
                AddFailureValidation(result, "NumOfSuppliers");
            }
        }

        private static void CheckOrigin(IServiceRequest request)
        {
            if (!originDal.IsExists(request.OriginId))
            {
                request.OriginId = unknownOriginId;
            }
        }

        private static void CheckExpertise(IServiceRequest request, Result result)
        {
            if (string.IsNullOrEmpty(request.ExpertiseCode))
            {
                AddFailureValidation(result, "ExpertiseCode");
            }
            else if (!primaryDAL.IsExists(request.ExpertiseCode))
            {
                AddFailureValidation(result, "ExpertiseCode does not exist");
            }
        }

        private static void CheckRegionLevel(IServiceRequest request, Result result)
        {
            if (request.RegionLevel < 1)
            {
                AddFailureValidation(result, "RegionLevel");
            }
        }

        private static void CheckRegionCode(IServiceRequest request, Result result)
        {
            if (string.IsNullOrEmpty(request.RegionCode))
            {
                AddFailureValidation(result, "RegionCode");
            }
            else
            {
                if (regDal.IsExists(request.RegionCode) == false)
                {
                    if (request.RegionLevel == 3 && request.RegionCode.Length < 4)
                    {
                        string prefix = string.Empty;
                        int zerosMissing = 4 - request.RegionCode.Length;
                        for (int i = 0; i < zerosMissing; i++)
                        {
                            prefix = prefix + "0";
                        }
                        request.RegionCode = prefix + request.RegionCode;
                    }
                    if (regDal.IsExists(request.RegionCode) == false)
                    {
                        AddFailureValidation(result, "RegionCode does not exist");
                    }
                }
            }
        }
    }
}
