﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class CustomerAfterCaseService : XrmUserBase
    {
        public CustomerAfterCaseService(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public void SendAfterCaseEmail(Guid incidentId, string email)
        {
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
            UpdateContactEmail(incidentId, email, incidentDal);
            Dictionary<Guid, AdvertiserFeedback> feedbacks = GetRecordingsAndAccountIds(incidentDal, incidentId);
            if (feedbacks.Count == 0)
                return;
            GetTipAnswers(incidentId, incidentDal, feedbacks);
            var sorted = feedbacks.Values.OrderByDescending(x => x.PositiveTips).ToArray();
            Dictionary<string, string> vars = BuildEmailVars(sorted);
            MandrillSender sender = new MandrillSender();
            sender.SendTemplate(email, null, DataModel.MandrillTemplates.E_CUSTOMER_POST_LEAD_DESKTOP_APP, vars);
        }

        private static Dictionary<string, string> BuildEmailVars(AdvertiserFeedback[] sorted)
        {
            Dictionary<string, string> vars = new Dictionary<string, string>();
            int i;
            for (i = 0; i < sorted.Count() && i < 3; i++)
            {
                vars.Add("NPVAR_LISTING" + (i + 1), (i + 1) + ". " + sorted[i].Name + " - " + sorted[i].PositiveTips + " of " + sorted[i].TotalTips + "<img src=\"http://noproblem.me/deskApp/images/happy_B.png\" style=\"height:20px; width:20px\" /> ");
                vars.Add("NPVAR_RECORDING" + (i + 1), String.IsNullOrEmpty(sorted[i].Recording) ? "  Sorry, no recording available." : "  <a href=\"" + sorted[i].Recording + "\">Listen back to your call.</a>");
            }

            while (i < 3)
            {
                vars.Add("NPVAR_LISTING" + (i + 1), String.Empty);
                vars.Add("NPVAR_RECORDING" + (i + 1), String.Empty);
                i++;
            }
            return vars;
        }

        private void GetTipAnswers(Guid incidentId, DataAccessLayer.Incident incidentDal, Dictionary<Guid, AdvertiserFeedback> feedbacks)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var item in feedbacks)
            {
                builder.Append("'").Append(item.Key.ToString()).Append("'").Append(",");
            }
            builder.Remove(builder.Length - 1, 1);
            string queryReady = queryGetTipAnswers.Replace("*|accountids|*", builder.ToString());
            DataAccessLayer.Incident.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@incidentId", incidentId);
            var reader = incidentDal.ExecuteReader(queryReady, parameters);
            foreach (var row in reader)
            {
                Guid accountId = (Guid)row["New_AccountId"];
                int count = (int)row["count"];
                bool isPositive = (bool)row["New_PositiveFeedback"];
                var f = feedbacks[accountId];
                f.TotalTips += count;
                if (isPositive)
                    f.PositiveTips += count;
            }
        }

        private Dictionary<Guid, AdvertiserFeedback> GetRecordingsAndAccountIds(DataAccessLayer.Incident incidentDal, Guid incidentId)
        {
            Dictionary<Guid, AdvertiserFeedback> feedbacks = new Dictionary<Guid, AdvertiserFeedback>();
            DataAccessLayer.Incident.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@incidentId", incidentId);
            var reader = incidentDal.ExecuteReader(queryGetRecordingsAndAccountIds, parameters);
            foreach (var row in reader)
            {
                AdvertiserFeedback f = new AdvertiserFeedback();
                f.Name = Convert.ToString(row["new_accountidname"]);
                bool customerDisallowedRecording = row["New_Customerdisallowedrecording"] != DBNull.Value ? (bool)row["New_Customerdisallowedrecording"] : false;
                if (!customerDisallowedRecording)
                    f.Recording = Convert.ToString(row["new_recordfilelocation"]);
                f.IsYelpSupplier = (bool)row["isYelp"];
                feedbacks.Add((Guid)row["new_accountid"], f);
            }
            return feedbacks;
        }

        private void UpdateContactEmail(Guid incidentId, string email, DataAccessLayer.Incident incidentDal)
        {
            Guid contactId = (from inc in incidentDal.All
                              where inc.incidentid == incidentId
                              select inc.customerid.Value).First();
            DataAccessLayer.Contact contactDal = new DataAccessLayer.Contact(XrmDataContext);
            DataModel.Xrm.contact contact = new DataModel.Xrm.contact(XrmDataContext);
            contact.contactid = contactId;
            contact.emailaddress1 = email;
            contactDal.Update(contact);
            XrmDataContext.SaveChanges();
        }

        private class AdvertiserFeedback
        {
            public string Name { get; set; }
            public int TotalTips { get; set; }
            public int PositiveTips { get; set; }
            public string Recording { get; set; }
            public bool IsYelpSupplier { get; set; }
        }

        private const string queryGetRecordingsAndAccountIds =
            @"
select new_recordfilelocation, new_accountidname, new_accountid, New_Customerdisallowedrecording, cast(0 as bit) as 'isYelp'
from new_incidentaccount with(nolock)
where new_incidentid = @incidentId
and statuscode = 10
union
select New_Recording, New_YelpSupplierIdName, New_YelpSupplierId, cast(0 as bit), cast(1 as bit)
from New_yelpaarcall with(nolock)
where New_IncidentId = @incidentId
and New_SuccessfulCall = 1
";

        private const string queryGetTipAnswers =
            @"
select New_PositiveFeedback, New_AccountId, COUNT(*) count
from New_customertipadvertiserresponse
where (New_AccountId in ( *|accountids|* ) or New_YelpSupplierId in ( *|accountids|* ))
and New_IncidentId = @incidentId
group by New_PositiveFeedback, New_AccountId
";
    }
}
