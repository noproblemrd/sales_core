﻿using System;
using System.Threading;
using Effect.Crm.Logs;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.CallBack
{
    internal class AutoUpsaleCallBackManager : XrmUserBase
    {
        #region Consts and Fields

        DataAccessLayer.AutoUpsaleCallBackSettings settingsDal;

        internal const string END_CALL = "endCall";
        internal const string ARE_YOU_SURE = "areYouSure";
        internal const string REPEAT = "repeat";

        private const string NOTHING = "NOTHING";

        private const string CONSULTATION_TEXT_KEY = "CONSULTATION_TEXT";
        private string _consultationText;
        private string ConsultationText
        {
            get
            {
                if (string.IsNullOrEmpty(_consultationText))
                {
                    _consultationText = settingsDal.GetConfigurationSettingValue(CONSULTATION_TEXT_KEY);
                }
                return _consultationText;
            }
        }

        private const string ARE_YOU_SURE_TEXT_KEY = "ARE_YOU_SURE_TEXT";
        private string _areYouSureText;
        private string AreYouSureText
        {
            get
            {
                if (string.IsNullOrEmpty(_areYouSureText))
                {
                    _areYouSureText = settingsDal.GetConfigurationSettingValue(ARE_YOU_SURE_TEXT_KEY);
                }
                return _areYouSureText;
            }
        }

        private const string CONFIRMED_TEXT_KEY = "CONFIRMED_TEXT";
        private string _confirmedText;
        private string ConfirmedText
        {
            get
            {
                if (string.IsNullOrEmpty(_confirmedText))
                {
                    _confirmedText = settingsDal.GetConfigurationSettingValue(CONFIRMED_TEXT_KEY);
                }
                return _confirmedText;
            }
        }

        private const string MISTAKE_TEXT_KEY = "MISTAKE_TEXT";
        private string _mistakeText;
        private string MistakeText
        {
            get
            {
                if (string.IsNullOrEmpty(_mistakeText))
                {
                    _mistakeText = settingsDal.GetConfigurationSettingValue(MISTAKE_TEXT_KEY);
                }
                return _mistakeText;
            }
        }

        private const string INVALID_OPTION_TEXT_KEY = "INVALID_OPTION_TEXT";
        private string _invalidOptionText;
        private string InvalidOptionText
        {
            get
            {
                if (string.IsNullOrEmpty(_invalidOptionText))
                {
                    _invalidOptionText = settingsDal.GetConfigurationSettingValue(INVALID_OPTION_TEXT_KEY);
                }
                return _invalidOptionText;
            }
        }

        private const string CANCEL_CONFIRMED_TEXT_KEY = "CANCEL_CONFIRMED_TEXT";
        private string _cancelConfirmedText;
        private string CancelConfirmedText
        {
            get
            {
                if (string.IsNullOrEmpty(_cancelConfirmedText))
                {
                    _cancelConfirmedText = settingsDal.GetConfigurationSettingValue(CANCEL_CONFIRMED_TEXT_KEY);
                }
                return _cancelConfirmedText;
            }
        }

        #endregion

        #region Ctor
        public AutoUpsaleCallBackManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            settingsDal = new NoProblem.Core.DataAccessLayer.AutoUpsaleCallBackSettings(XrmDataContext);
        }
        #endregion

        internal void InitiateAutoUpsaleCallBack(Guid incidentId, Guid upsaleId, string consumerPhone)
        {
            DataAccessLayer.AutoUpsaleCallBack callBackDal = new NoProblem.Core.DataAccessLayer.AutoUpsaleCallBack(XrmDataContext);
            DML.Xrm.new_autoupsalecallback callBack = new NoProblem.Core.DataModel.Xrm.new_autoupsalecallback();
            callBack.new_incidentid = incidentId;
            callBack.new_tophone = consumerPhone;
            callBack.new_upsaleid = upsaleId;
            callBack.new_confirmed = null;
            callBackDal.Create(callBack);
            XrmDataContext.SaveChanges();
            //DataAccessLayer.PrimaryExpertise headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            //string headingIvrName = headingDal.GetIvrPluralNameFromIncidentId(incidentId);
            //if (string.IsNullOrEmpty(headingIvrName))
            //{
            //   headingIvrName = "service providers";
            //}
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentId);
            string headingCode = incident.new_new_primaryexpertise_incident.new_code;
            TwilioBL.TwilioCallsExecuter executer = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioCallsExecuter(XrmDataContext);
            string callSid = executer.InitiateAutoUpsaleCallBackCall(consumerPhone, callBack.new_autoupsalecallbackid, headingCode);
            if (string.IsNullOrEmpty(callSid))
            {
                LogUtils.MyHandle.WriteToLog("InitiateAutoUpsaleCallBack was not able to execute call. callSid was null.");
                HandleStatusEventDelegate del = new HandleStatusEventDelegate(HandleStatusEventAsync);
                del.BeginInvoke(callBack.new_autoupsalecallbackid, "null", "failed", null, "call_creation_failure", null, "callsid_is_null", null, false, HandleStatuEventAsyncCallBack, del);
            }
            else
            {
                callBack.new_callsid = callSid;
                callBackDal.Update(callBack);
                XrmDataContext.SaveChanges();
            }
        }

        internal string HandleMainConsultation(string id, string callSid, string callStatus, string headingIvrName)
        {
            //string txt = GetConsultationText(headingIvrName);
            string txt = null;
            HandleStatusEventDelegate del = new HandleStatusEventDelegate(HandleStatusEventAsync);
            del.BeginInvoke(new Guid(id), callSid, callStatus, null, "MainConsultation", null, null, null, null, HandleStatuEventAsyncCallBack, del);
            return txt;
        }

        private string GetConsultationText(string headingIvrName)
        {
            string retVal = ConsultationText;
            retVal = string.Format(retVal, headingIvrName);
            return retVal;
        }

        private string GetAreYouSureText(string headingIvrName)
        {
            string retVal = AreYouSureText;
            retVal = string.Format(retVal, headingIvrName);
            return retVal;
        }

        internal bool HandleMainMenuClicks(string digits, string id, string headingIvrName, out string txt)
        {
            txt = null;
            string retVal = string.Empty;
            bool isConfirmed = digits == "1";
            //bool? isConfirmed = null;
            //switch (digits)
            //{
            //    case "1":
            //        txt = ConfirmedText;
            //        retVal = END_CALL;
            //        isConfirmed = true;
            //        break;
            //    case "3":
            //        txt = MistakeText;
            //        retVal = END_CALL;
            //        isConfirmed = false;
            //        break;
            //    case "8":
            //        txt = GetAreYouSureText(headingIvrName);
            //        retVal = ARE_YOU_SURE;
            //        break;
            //    case "9":
            //        retVal = REPEAT;
            //        txt = GetConsultationText(headingIvrName);
            //        break;
            //    case NOTHING:
            //        txt = string.Empty;
            //        retVal = END_CALL;
            //        isConfirmed = false;
            //        break;
            //    default:
            //        txt = InvalidOptionText + " " + GetConsultationText(headingIvrName);
            //        retVal = REPEAT;
            //        break;
            //}
            HandleStatusEventDelegate del = new HandleStatusEventDelegate(HandleStatusEventAsync);
            del.BeginInvoke(new Guid(id), null, null, digits, "MainMenuClicks", null, null, null, isConfirmed, HandleStatuEventAsyncCallBack, del);
            return isConfirmed;
        }

        internal string HandleSecondaryMenuClicks(string digits, string id, string headingIvrName, out string txt)
        {
            string retVal;
            switch (digits)
            {
                case "4":
                    txt = CancelConfirmedText;
                    retVal = END_CALL;
                    break;
                case NOTHING:
                    retVal = END_CALL;
                    txt = string.Empty;
                    break;
                default:
                    txt = GetConsultationText(headingIvrName);
                    retVal = REPEAT;
                    break;
            }
            HandleStatusEventDelegate del = new HandleStatusEventDelegate(HandleStatusEventAsync);
            bool? isConfirmed = null;
            if (retVal == END_CALL)
                isConfirmed = false;
            del.BeginInvoke(new Guid(id), null, null, digits, "SecondaryMenuClicks", null, null, null, isConfirmed, HandleStatuEventAsyncCallBack, del);
            return retVal;
        }

        internal void StatusCallBack(string callDuration, string callSid, string from, string to, string callStatus, string id)
        {
            HandleStatusEventDelegate del = new HandleStatusEventDelegate(HandleStatusEventAsync);
            del.BeginInvoke(new Guid(id), callSid, callStatus, null, "StatusCallBack", callDuration, null, null, null, HandleStatuEventAsyncCallBack, del);
        }

        internal void FallBack(string callSid, string callStatus, string id, string errorCode, string errorUrl)
        {
            HandleStatusEventDelegate del = new HandleStatusEventDelegate(HandleStatusEventAsync);
            del.BeginInvoke(new Guid(id), callSid, callStatus, null, "FallBack", null, errorCode, errorUrl, false, HandleStatuEventAsyncCallBack, del);
        }

        private delegate void HandleStatusEventDelegate(Guid id, string callSid, string callStatus, string digits, string eventType, string callDuration, string errorCode, string errorUrl, bool? isConfirmed);

        private void HandleStatusEventAsync(Guid id, string callSid, string callStatus, string digits, string eventType, string callDuration, string errorCode, string errorUrl, bool? isConfirmed)
        {
            DML.Xrm.new_autoupsalecallbackstatusevent statusEvent = new NoProblem.Core.DataModel.Xrm.new_autoupsalecallbackstatusevent();
            statusEvent.new_autoupsalecallbackid = id;
            statusEvent.new_callsid = callSid;
            statusEvent.new_callstatus = callStatus;
            statusEvent.new_digits = digits;
            statusEvent.new_eventtype = eventType;
            statusEvent.new_callduration = callDuration;
            statusEvent.new_errorcode = errorCode;
            if (!string.IsNullOrEmpty(errorUrl))
            {
                if (errorUrl.Length > 500)
                    errorUrl = errorUrl.Substring(0, 500);
                statusEvent.new_errorurl = errorUrl;
            }
            DataAccessLayer.AutoUpsaleCallBackStatusEvent statusEventDal = new NoProblem.Core.DataAccessLayer.AutoUpsaleCallBackStatusEvent(null);
            statusEventDal.Create(statusEvent);
            statusEventDal.XrmDataContext.SaveChanges();
            if (isConfirmed.HasValue)
            {

                DataAccessLayer.AutoUpsaleCallBack callBackDal = new NoProblem.Core.DataAccessLayer.AutoUpsaleCallBack(statusEventDal.XrmDataContext);
                DML.Xrm.new_autoupsalecallback callBack = callBackDal.Retrieve(id);
                if (!callBack.new_confirmed.HasValue)
                {
                    callBack.new_confirmed = isConfirmed.Value;
                    callBackDal.Update(callBack);
                    statusEventDal.XrmDataContext.SaveChanges();
                    if (isConfirmed.Value)
                    {
                        SendServiceRequest(callBack, statusEventDal.XrmDataContext);
                    }
                    else
                    {
                        MarkUpsaleAsLost(callBack, statusEventDal.XrmDataContext);
                    }
                }
            }
        }

        private void HandleStatuEventAsyncCallBack(IAsyncResult result)
        {
            try
            {
                ((HandleStatusEventDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AutoUpsaleCallBackManager.HandleStatusEventAsync");
            }
        }

        private void SendServiceRequest(DML.Xrm.new_autoupsalecallback callBack, DML.Xrm.DataContext localContext)
        {
            var incident = callBack.new_incident_autoupsalecallback;
            var consumerPhone = incident.new_telephone1;
            var expertiseCode = incident.new_new_primaryexpertise_incident.new_code;
            var expertiseLevel = 1;
            var numberOfSuppliers = incident.new_requiredaccountno.Value - incident.new_ordered_providers.Value;
            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(localContext);
            bool? useSameInUpsale = incident.new_new_origin_incident.new_usesameinupsale.HasValue && incident.new_new_origin_incident.new_usesameinupsale.Value;
            Guid originId;
            if (useSameInUpsale.HasValue && useSameInUpsale.Value)
            {
                originId = incident.new_originid.Value;
            }
            else
            {
                originId = originDal.GetAutoUpsaleOriginId();
            }
            var regionCode = incident.new_new_region_incident.new_code;
            var regionLevel = incident.new_new_region_incident.new_level.Value;
            var description = incident.description;
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(localContext);
            Guid userId = accountRepositoryDal.GetAutoUpsaleUserId();
            var upsaleId = callBack.new_upsaleid.Value;
            DataModel.ServiceRequest request = new NoProblem.Core.DataModel.ServiceRequest();
            request.ContactPhoneNumber = consumerPhone;
            request.ExpertiseCode = expertiseCode;
            request.ExpertiseType = expertiseLevel;
            request.NumOfSuppliers = numberOfSuppliers;
            request.OriginId = originId;
            request.RegionCode = regionCode;
            request.RegionLevel = regionLevel;
            request.RequestDescription = description;
            request.UpsaleId = upsaleId;
            request.UserId = userId;
            request.IsAutoUpsale = true;
            request.SubType = incident.new_subtype;
            request.UpsaleRequestId = incident.incidentid;
            if (incident.new_budget.HasValue)
            {
                request.Budget = (DataModel.Consumer.eBudget)incident.new_budget.Value;
            }
            AddSpecialHeadingsData(incident, request);
            Thread tr = new Thread(new ParameterizedThreadStart(obj =>
            {
                try
                {
                    ServiceRequestManager srManager = new ServiceRequestManager(null);
                    srManager.CreateService((DataModel.ServiceRequest)obj);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception sending async service request from AutoUpsaleCallBackManager");
                }
            }));
            tr.Start(request);
        }

        private void AddSpecialHeadingsData(NoProblem.Core.DataModel.Xrm.incident incident, DataModel.ServiceRequest request)
        {
            AddMoverData(incident, request);
            AddPersonalInjuryAttorneyData(incident, request);
            AddCriminalLawAttorneyData(incident, request);
            AddDuiAttorneyData(incident, request);
            AddDivorceAttorneyData(incident, request);
            AddBankruptcyAttorneyData(incident, request);
        }

        private void AddDivorceAttorneyData(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.ServiceRequest request)
        {
            if (incident.new_divorceattorneycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestDivorceAttorneyData requestData = new NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData();
                var oldData = incident.new_divorceattorneycasedata_incident;
                requestData.BeenFiled = oldData.new_hasadivorcecasebeenfiledincourt.HasValue ? oldData.new_hasadivorcecasebeenfiledincourt.Value : false;
                if (oldData.new_howmanychildren.HasValue)
                {
                    requestData.Children = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceChildren)((int)oldData.new_howmanychildren.Value);
                }
                else
                {
                    requestData.Children = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceChildren.Two;
                }
                if (oldData.new_howareyoufinancing.HasValue)
                {
                    requestData.Financing = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceFinancing)((int)oldData.new_howareyoufinancing.Value);
                }
                else
                {
                    requestData.Financing = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceFinancing.IWillDiscussPaymentOptionsWithMyAttorney;
                }
                if (oldData.new_yourannualincome.HasValue)
                {
                    requestData.Income = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceIncome)((int)oldData.new_yourannualincome.Value);
                }
                else
                {
                    requestData.Income = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceIncome.LessThan10K;
                }
                if (oldData.new_spouseannualincome.HasValue)
                {
                    requestData.SpouseIncome = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceIncome)((int)oldData.new_spouseannualincome.Value);
                }
                else
                {
                    requestData.SpouseIncome = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceIncome.LessThan10K;
                }
                requestData.Live = oldData.new_doyoucurrentlylivewithyourspouse.HasValue ? oldData.new_doyoucurrentlylivewithyourspouse.Value : false;
                if (oldData.new_ownedproperty.HasValue)
                {
                    requestData.Property = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceProperty)((int)oldData.new_ownedproperty.Value);
                }
                else
                {
                    requestData.Property = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceProperty.Other;
                }
                requestData.Represented = oldData.new_areyoucurrentlyrepresented.HasValue ? oldData.new_areyoucurrentlyrepresented.Value : false;
                if (oldData.new_howsoonareyoulookingtofile.HasValue)
                {
                    requestData.Soon = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceHowSoon)((int)oldData.new_howsoonareyoulookingtofile.Value);
                }
                else
                {
                    requestData.Soon = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eDivorceHowSoon.Immediately;
                }
                if (oldData.new_termsofdivorceagreedby.HasValue)
                {
                    requestData.Terms = (NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eTermsOfDivorce)((int)oldData.new_termsofdivorceagreedby.Value);
                }
                else
                {
                    requestData.Terms = NoProblem.Core.DataModel.Consumer.ServiceRequestDivorceAttorneyData.eTermsOfDivorce.WeAgreeOnSomeButNotAll;
                }
                request.DivorceAttorneyData = requestData;
            }
        }

        private void AddBankruptcyAttorneyData(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.ServiceRequest request)
        {
            if (incident.new_bankruptcyattorneycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestBankruptcyAttorneyData requestData = new NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData();
                var oldData = incident.new_bankruptcyattorneycasedata_incident;
                requestData.AffordAttorney = oldData.new_canyouaffordanattorney.HasValue ? oldData.new_canyouaffordanattorney.Value : true;
                requestData.Assets = oldData.new_haveadditionalassets.HasValue ? oldData.new_haveadditionalassets.Value : false;
                requestData.BehindAutomobile = oldData.new_behindinautomobilepayments.HasValue ? oldData.new_behindinautomobilepayments.Value : true;
                requestData.BehindRealEstate = oldData.new_behindinrealestatepayments.HasValue ? oldData.new_behindinrealestatepayments.Value : true;
                if (oldData.new_whichbillsdoyouhave.HasValue)
                {
                    requestData.Bills = (DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eBillsBankruptcy)((int)oldData.new_whichbillsdoyouhave.Value);
                }
                else
                {
                    requestData.Bills = NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eBillsBankruptcy.Other;
                }
                if (oldData.new_whyconsideringbankruptcy.HasValue)
                {
                    requestData.Considering = (NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eConsideringBankruptcy)((int)oldData.new_whyconsideringbankruptcy.Value);
                }
                else
                {
                    requestData.Considering = NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eConsideringBankruptcy.Other;
                }
                if (oldData.new_estimatetotalmonthlyexpenses.HasValue)
                {
                    requestData.Expenses = (NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eMoneyEstimateBankruptcy)((int)oldData.new_estimatetotalmonthlyexpenses.Value);
                }
                else
                {
                    requestData.Expenses = NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eMoneyEstimateBankruptcy.LessThan3K;
                }
                if (oldData.new_estimatetotalmonthlyincome.HasValue)
                {
                    requestData.Income = (NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eMoneyEstimateBankruptcy)((int)oldData.new_estimatetotalmonthlyincome.Value);
                }
                else
                {
                    requestData.Income = NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eMoneyEstimateBankruptcy.LessThan3K;
                }
                if (oldData.new_typesofincome.HasValue)
                {
                    requestData.IncomeTypes = (NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eTypesOfIncomeBankruptcy)((int)oldData.new_typesofincome.Value);
                }
                else
                {
                    requestData.IncomeTypes = NoProblem.Core.DataModel.Consumer.ServiceRequestBankruptcyAttorneyData.eTypesOfIncomeBankruptcy.Other;
                }
                request.BankruptcyAttorneyData = requestData;
            }
        }

        private void AddDuiAttorneyData(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.ServiceRequest request)
        {
            if (incident.new_duiattorneycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestDuiAttorneyData requestData = new NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData();
                var oldData = incident.new_duiattorneycasedata_incident;
                requestData.AffordAttorney = oldData.new_canyouaffordanattorney.HasValue ? oldData.new_canyouaffordanattorney.Value : true;
                if (oldData.new_areyourepresented.HasValue)
                {
                    switch ((DML.Xrm.new_duiattorneycasedata.AreYouRepresented)oldData.new_areyourepresented.Value)
                    {
                        case NoProblem.Core.DataModel.Xrm.new_duiattorneycasedata.AreYouRepresented.Yes:
                            requestData.Represented = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiAreYouRepresented.Yes;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_duiattorneycasedata.AreYouRepresented.YesButLookingForNewRepresentation:
                            requestData.Represented = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiAreYouRepresented.YesButLookingForNewRepresentation;
                            break;
                        default:
                            requestData.Represented = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiAreYouRepresented.No;
                            break;
                    }
                }
                else
                {
                    requestData.Represented = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiAreYouRepresented.No;
                }
                if (oldData.new_statusofthedui.HasValue)
                {
                    switch ((DML.Xrm.new_duiattorneycasedata.StatusOfTheDui)oldData.new_statusofthedui.Value)
                    {
                        case NoProblem.Core.DataModel.Xrm.new_duiattorneycasedata.StatusOfTheDui.GotDuiAndHaveCourtDate:
                            requestData.Status = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiStatusOfTheDui.GotDuiAndHaveCourtDate;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_duiattorneycasedata.StatusOfTheDui.WantPastDuiRemoved:
                            requestData.Status = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiStatusOfTheDui.WantPastDuiRemoved;
                            break;
                        default:
                            requestData.Status = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiStatusOfTheDui.GotDuiButNoCourtDate;
                            break;
                    }
                }
                else
                {
                    requestData.Status = NoProblem.Core.DataModel.Consumer.ServiceRequestDuiAttorneyData.eDuiStatusOfTheDui.GotDuiButNoCourtDate;
                }
                request.DuiAttorneyData = requestData;
            }
        }

        private void AddCriminalLawAttorneyData(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.ServiceRequest request)
        {
            if (incident.new_criminallawattorneycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestCriminalLawAttorneyData requestData = new NoProblem.Core.DataModel.Consumer.ServiceRequestCriminalLawAttorneyData();
                var oldData = incident.new_criminallawattorneycasedata_incident;
                requestData.CurrentCharges = oldData.new_doyouhavecurrentchargesagainstyou.HasValue ? oldData.new_doyouhavecurrentchargesagainstyou.Value : true;
                requestData.AffordAttorney = oldData.new_canyouaffordanattorney.HasValue ? oldData.new_canyouaffordanattorney.Value : true;
                request.CriminalLawAttorneyData = requestData;
            }
        }

        private void AddPersonalInjuryAttorneyData(NoProblem.Core.DataModel.Xrm.incident incident, DataModel.ServiceRequest request)
        {
            if (incident.new_personalinjurycasedataid.HasValue)
            {
                DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData piData = new NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData();
                var oldData = incident.new_personalinjurycasedata_incident;
                if (oldData.new_accidentresult.HasValue)
                {
                    switch ((DML.Xrm.new_personalinjurycasedata.AccidentResult)oldData.new_accidentresult.Value)
                    {
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.hospitalization:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.hospitalization;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.missedWork:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.missedWork;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.noneOfTheAbove:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.noneOfTheAbove;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.surgery:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.surgery;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.wrongfulDeath:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.wrongfulDeath;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata.AccidentResult.medicalTreatment:
                            piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.medicalTreatment;
                            break;
                    }
                }
                else
                {
                    piData.AccidentResult = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAccidentResult.medicalTreatment;
                }
                if (oldData.new_doesanyonehasinsurance.HasValue)
                {
                    switch (oldData.new_doesanyonehasinsurance.Value)
                    {
                        case (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.Yes:
                            piData.DoesAnyoneHasVehicleInsurance = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAnyoneHasInsurance.Yes;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.No:
                            piData.DoesAnyoneHasVehicleInsurance = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAnyoneHasInsurance.No;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.DontKnow:
                            piData.DoesAnyoneHasVehicleInsurance = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAnyoneHasInsurance.DontKnow;
                            break;
                    }
                }
                else
                {
                    piData.DoesAnyoneHasVehicleInsurance = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eAnyoneHasInsurance.Yes;
                }
                if (oldData.new_medicalbills.HasValue)
                {
                    switch (oldData.new_medicalbills.Value)
                    {
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.dontKnow:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.dontKnow;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e10kTo25k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.e10kTo25k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e1kTo5k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.e1kTo5k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e25kTo100k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.e25kTo100k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e5kTo10k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.e5kTo10k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.lessThan1k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.lessThan1k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.moreThan100k:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.moreThan100k;
                            break;
                        case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.noMedicalBills:
                            piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.noMedicalBills;
                            break;
                    }
                }
                else
                {
                    piData.EstimatedMedicalBills = NoProblem.Core.DataModel.Consumer.ServiceRequestPersonalInjuryAttorneyData.eEstimatedMedicalBills.dontKnow;
                }
                bool atLeastOneMarked = false;
                if (oldData.new_whiplash.HasValue && oldData.new_whiplash.Value)
                {
                    piData.Whiplash = true;
                    atLeastOneMarked = true;
                }
                if (oldData.new_brokenbones.HasValue && oldData.new_brokenbones.Value)
                {
                    piData.BrokenBones = true;
                    atLeastOneMarked = true;
                }
                if (oldData.new_lostlimb.HasValue && oldData.new_lostlimb.Value)
                {
                    piData.LostLimb = true;
                    atLeastOneMarked = true;
                }
                if (oldData.new_spinalcordinjuryorparalysis.HasValue && oldData.new_spinalcordinjuryorparalysis.Value)
                {
                    piData.SpinalCordInjuryOrParalysis = true;
                    atLeastOneMarked = true;
                }
                if (oldData.new_braininjury.HasValue && oldData.new_braininjury.Value)
                {
                    piData.BrainInjury = true;
                    atLeastOneMarked = true;
                }
                if (oldData.new_lossoflife.HasValue && oldData.new_lossoflife.Value)
                {
                    piData.LossOfLife = true;
                    atLeastOneMarked = true;
                }
                if ((oldData.new_other.HasValue && oldData.new_other.Value) || !atLeastOneMarked)
                {
                    piData.Other = true;
                }
                piData.CurrentlyRepresented = oldData.new_currentlyrepresented.HasValue ? oldData.new_currentlyrepresented.Value : false;
                piData.IsYourFault = oldData.new_isyourfault.HasValue ? oldData.new_isyourfault.Value : false;
                request.PersonalInjuryAttorneyData = piData;
            }
        }

        private void AddMoverData(NoProblem.Core.DataModel.Xrm.incident incident, DataModel.ServiceRequest request)
        {
            bool needsMoverData = false;
            DataModel.Consumer.ServiceRequestMoverData moverData = new NoProblem.Core.DataModel.Consumer.ServiceRequestMoverData();
            if (incident.new_movedate.HasValue)
            {
                needsMoverData = true;
                moverData.MoveDate = incident.new_movedate;
            }
            if (!string.IsNullOrEmpty(incident.new_movetozipcode))
            {
                needsMoverData = true;
                moverData.MoveToZipCode = incident.new_movetozipcode;
            }
            if (!string.IsNullOrEmpty(incident.new_movesize))
            {
                needsMoverData = true;
                moverData.MoveSize = (DataModel.Consumer.ServiceRequestMoverData.eMoveSize)Enum.Parse(typeof(DataModel.Consumer.ServiceRequestMoverData.eMoveSize), incident.new_movesize);
            }
            if (!string.IsNullOrEmpty(incident.new_movetype))
            {
                needsMoverData = true;
                moverData.MoveType = (DataModel.Consumer.ServiceRequestMoverData.eMoveType)Enum.Parse(typeof(DataModel.Consumer.ServiceRequestMoverData.eMoveType), incident.new_movetype);
            }
            if (needsMoverData)
            {
                request.MoverData = moverData;
            }
        }

        private void MarkUpsaleAsLost(DML.Xrm.new_autoupsalecallback callBack, DML.Xrm.DataContext localContext)
        {
            DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(localContext);
            DataModel.Xrm.new_upsale upsale = new NoProblem.Core.DataModel.Xrm.new_upsale(localContext);
            upsale.new_upsaleid = callBack.new_upsaleid.Value;
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(localContext);
            Guid userId = accountRepositoryDal.GetAutoUpsaleUserId();
            upsale.new_userid = userId;
            upsale.statuscode = (int)DML.Xrm.new_upsale.UpsaleStatus.Lost;
            upsaleDal.Update(upsale);
            localContext.SaveChanges();
        }
    }
}
