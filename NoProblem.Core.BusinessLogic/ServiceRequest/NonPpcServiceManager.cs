﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Consumer;
using DML = NoProblem.Core.DataModel;
using System.Globalization;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class NonPpcServiceManager : XrmUserBase
    {
        #region Ctor
        public NonPpcServiceManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        public NonPpcServiceResponse CreateNonPpcService(NonPpcServiceRequest request)
        {
            NonPpcServiceResponse response = new NonPpcServiceResponse();
            response.Status = StatusCode.Success;
            if (IsBlackList(request.ContactPhoneNumber) || IsDirectNumber(request.ContactPhoneNumber))
            {
                response.Status = StatusCode.BlackList;
            }
            else
            {
                DML.Xrm.new_nonppcrequest nonPpcRequest = CreateNonPpcRequestEntry(request);
                SendRequestToDialer(nonPpcRequest);
                response.ServiceRequestId = nonPpcRequest.new_nonppcrequestid.ToString();
            }
            return response;
        }
        /*
            public string CreateC2cReport(string command, string date, string format, string fields)
            {
                //if (command != "rptgenerate")
                //{
                //    return "Unknown command";
                //}
                DateTime d = DateTime.ParseExact(date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                DateTime from = d.Date;
                DateTime to = from.AddDays(1);

                string query =
                    @"select new_nonppcrequestid, statuscode, new_consumerphone, 
                        new_advertisercallstart, new_consumercallstart, new_callend
                        , new_advertiserphone1
                    from new_nonppcrequest with(nolock)
                    where deletionstatecode = 0 and createdon between @from and @to
                    order by createdon
                    ";

                List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
                parameters.Add(new KeyValuePair<string, object>("@from", from));
                parameters.Add(new KeyValuePair<string, object>("@to", to));
                DataAccessLayer.NonPpcRequest dal = new NoProblem.Core.DataAccessLayer.NonPpcRequest(XrmDataContext);
                var reader = dal.ExecuteReader(query, parameters);
                StringBuilder builder = new StringBuilder();
                string separator = ";";
                foreach (var row in reader)
                {
                    Guid id = (Guid)row["new_nonppcrequestid"];
                    DateTime? customerStartTime = null;
                    if (row["new_consumercallstart"] != DBNull.Value)
                    {
                        customerStartTime = (DateTime)row["new_consumercallstart"];
                    }
                    DateTime? advStartTime = null;
                    if (row["new_advertisercallstart"] != DBNull.Value)
                    {
                        advStartTime = (DateTime)row["new_advertisercallstart"];
                    }
                    DateTime? endTime = null;
                    if (row["new_callend"] != DBNull.Value)
                    {
                        endTime = (DateTime)row["new_callend"];
                    }
                    string customerPhone = Convert.ToString(row["new_consumerphone"]);
                    string advPhone = Convert.ToString(row["new_advertiserphone1"]);
                    DML.Xrm.new_nonppcrequest.Status status = (DML.Xrm.new_nonppcrequest.Status)((int)row["statuscode"]);
                    int endReason = -1;
                    switch (status)
                    {
                        case NoProblem.Core.DataModel.Xrm.new_nonppcrequest.Status.Close:
                            endReason = 0;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_nonppcrequest.Status.ConsumerDidntAnswer:
                            endReason = 2;
                            break;
                        case NoProblem.Core.DataModel.Xrm.new_nonppcrequest.Status.Lost:
                            endReason = 3;
                            break;
                    }
                    builder.Append(id.ToString()).Append(separator);
                    builder.Append("2").Append(separator);//2 is advertiser (receiver)
                    if (advStartTime.HasValue)
                    {
                        builder.Append(advStartTime.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    builder.Append(separator);
                    if (advStartTime.HasValue && endTime.HasValue)
                    {
                        builder.Append(endTime.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    builder.Append(separator);
                    builder.Append(endReason == 0 ? 1 : endReason).Append(separator);
                    builder.Append(advPhone).AppendLine();

                    builder.Append(id.ToString()).Append(separator);
                    builder.Append("1").Append(separator);//1 is customer (initiator)
                    if (customerStartTime.HasValue)
                    {
                        builder.Append(customerStartTime.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    builder.Append(separator);
                    if (customerStartTime.HasValue && endTime.HasValue)
                    {
                        builder.Append(endTime.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    builder.Append(separator);
                    builder.Append(endReason).Append(separator);
                    builder.Append(customerPhone).AppendLine();
                }
                return builder.ToString();
            }
            */
        public List<DML.Reports.Response.C2CData> CreateC2cReport(DateTime date)
        {
            DateTime from = date.Date;
            DateTime to = from.AddDays(1);

            string query =
                @"select new_nonppcrequestid, statuscode, new_consumerphone, 
                    new_advertisercallstart, new_consumercallstart, new_callend
                    , new_advertiserphone1, createdon
                from new_nonppcrequest with(nolock)
                where deletionstatecode = 0 and createdon between @from and @to
                order by createdon
                ";

            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@from", from));
            parameters.Add(new KeyValuePair<string, object>("@to", to));
            DataAccessLayer.NonPpcRequest dal = new NoProblem.Core.DataAccessLayer.NonPpcRequest(XrmDataContext);
            var reader = dal.ExecuteReader(query, parameters);
            List<DML.Reports.Response.C2CData> list = new List<NoProblem.Core.DataModel.Reports.Response.C2CData>();
            foreach (var row in reader)
            {
                Guid id = (Guid)row["new_nonppcrequestid"];
                DateTime createdOn = (DateTime)row["createdon"];
                DateTime? customerStartTime = null;
                if (row["new_consumercallstart"] != DBNull.Value)
                {
                    customerStartTime = (DateTime)row["new_consumercallstart"];
                }
                DateTime? advStartTime = null;
                if (row["new_advertisercallstart"] != DBNull.Value)
                {
                    advStartTime = (DateTime)row["new_advertisercallstart"];
                }
                DateTime? endTime = null;
                if (row["new_callend"] != DBNull.Value)
                {
                    endTime = (DateTime)row["new_callend"];
                }
                string customerPhone = Convert.ToString(row["new_consumerphone"]);
                string advPhone = Convert.ToString(row["new_advertiserphone1"]);
                DML.Xrm.new_nonppcrequest.Status status = (DML.Xrm.new_nonppcrequest.Status)((int)row["statuscode"]);
                int endReason = -1;
                switch (status)
                {
                    case NoProblem.Core.DataModel.Xrm.new_nonppcrequest.Status.Close:
                        endReason = 0;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_nonppcrequest.Status.ConsumerDidntAnswer:
                        endReason = 2;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_nonppcrequest.Status.Lost:
                        endReason = 3;
                        break;
                }
                DML.Reports.Response.C2CData _c2c = new NoProblem.Core.DataModel.Reports.Response.C2CData();
                _c2c.EntryCreatedOn = createdOn;
                _c2c.RequestId = id;
                _c2c.AdvertiserCallStart = advStartTime;
                if (_c2c.AdvertiserCallStart.HasValue)
                {
                    _c2c.AdvertiserCallEnd = endTime;
                }
                else
                {
                    _c2c.AdvertiserCallStart = createdOn;
                    _c2c.AdvertiserCallEnd = createdOn;
                }
                _c2c.EndReasonAdvertiser = (endReason == 0 ? 1 : endReason);
                _c2c.AdvertiserPhone = advPhone;               
                _c2c.ConsumerCallStart = customerStartTime;
                if (_c2c.ConsumerCallStart.HasValue)
                {
                    _c2c.ConsumerCallEnd = endTime;
                }
                else
                {
                    _c2c.ConsumerCallStart = createdOn;
                    _c2c.ConsumerCallEnd = createdOn;
                }
                _c2c.EndReasonConsumer = endReason;
                _c2c.CustomerPhone = customerPhone;              
                list.Add(_c2c);
            }
            return list;
        }
        #endregion

        #region Private Methods

        private bool IsBlackList(string phone)
        {
            bool retVal = false;
            DataAccessLayer.Contact contactDAL = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            DML.Xrm.contact contact = contactDAL.GetContactByPhoneNumber(phone);
            if (contact != null && contact.new_blaklist.HasValue == true && contact.new_blaklist.Value == true)
            {
                retVal = true;
            }
            return retVal;
        }

        private bool IsDirectNumber(string number)
        {
            DataAccessLayer.DirectNumber directNumberDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            bool isDirectNumber = directNumberDal.IsDirectNumber(number);
            return isDirectNumber;
        }

        private void SendRequestToDialer(DML.Xrm.new_nonppcrequest nonPpcRequest)
        {
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //List<inc2.NPProvider> providersList = new List<NoProblem.Core.BusinessLogic.inc2.NPProvider>();
            //inc2.NPProvider provider = new NoProblem.Core.BusinessLogic.inc2.NPProvider();
            DataAccessLayer.ConfigurationSettings settingsDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string prefix = settingsDAL.GetConfigurationSettingValue(ConfigurationKeys.COUNTRY_PHONE_PREFIX);
            string internalPrefix = settingsDAL.GetConfigurationSettingValue(ConfigurationKeys.INTERNAL_PHONE_PREFIX);
            //List<string> phoneList = new List<string>();
            //phoneList.Add(Phones.AppendPhonePrefix(prefix, internalPrefix, nonPpcRequest.new_advertiserphone1));
            //if (string.IsNullOrEmpty(nonPpcRequest.new_advertiserphone2) == false)
            //{
            //    phoneList.Add(Phones.AppendPhonePrefix(prefix, internalPrefix, nonPpcRequest.new_advertiserphone2));
            //}
            //provider.phoneNumbers = phoneList.ToArray();
            //provider.providerUniqueId = nonPpcRequest.new_advertiserphone1;
            //provider.recording = true;
            //provider.recordingSpecified = true;
            //providersList.Add(provider);
            //string startTime = DateTime.Now.AddMinutes(-10).ToString("dd/MM/yyyy HH:mm");
            //string endTime = DateTime.Now.AddHours(2).ToString("dd/MM/yyyy HH:mm");
            string consumerPhone = Phones.Deprecated_AppendPhonePrefix(prefix, internalPrefix, nonPpcRequest.new_consumerphone);
            int numOfRetries = int.Parse(settingsDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.SUPPLIER_ATTEMPTED_TRIES));
            int retryDelay = int.Parse(settingsDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.ATTEMPTED_TRIES_INTERVAL));
            //string callBackUrl = ConfigurationManager.AppSettings["DialerCallbackUrl"];
            //string requestId = "{" + nonPpcRequest.new_nonppcrequestid + "}1";
            //inc2.NPService inc2Service = new NoProblem.Core.BusinessLogic.inc2.NPService();
            //inc2Service.Url = ConfigurationManager.AppSettings["inc2Url"];
            //int supplierAmountToCall = 1;
            string advertiserPhone = Phones.Deprecated_AppendPhonePrefix(prefix, internalPrefix, nonPpcRequest.new_advertiserphone1);
            string dialerResult = string.Empty;
            try
            {
                //LogUtils.MyHandle.WriteToLog(8, "Sending request to dialer with params: requestId = {0}, consumerPhone = {1}, numofsuppliers = {2}, startTime = {3}, endTime = {4}, numOfRetries = {5}, retryDelay = {6}, callBackUrl = {7}.\n\r"
                //    , requestId, consumerPhone, supplierAmountToCall, startTime, endTime, numOfRetries, retryDelay, callBackUrl);
                //dialerResult = inc2Service.initServiceRequest(requestId, "", consumerPhone, providersList.ToArray(),
                //     supplierAmountToCall, startTime, endTime, numOfRetries, retryDelay, callBackUrl, string.Empty, false);
                AsteriskBL.AsteriskCallsExecuter executer = new NoProblem.Core.BusinessLogic.AsteriskBL.AsteriskCallsExecuter(XrmDataContext);
                string dialerId = executer.InitiateC2c(consumerPhone, advertiserPhone, true, string.Empty, false, nonPpcRequest.new_nonppcrequestid, string.Empty, true);
                if (string.IsNullOrEmpty(dialerId))
                {
                    nonPpcRequest.statuscode = (int)DML.Xrm.new_nonppcrequest.Status.Error;
                }
                else
                {
                    nonPpcRequest.statuscode = (int)DML.Xrm.new_nonppcrequest.Status.Calling;
                    nonPpcRequest.new_recordid = dialerId;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception sending request to dialer. requestId = {0}", nonPpcRequest.new_nonppcrequestid.ToString());
                nonPpcRequest.statuscode = (int)DML.Xrm.new_nonppcrequest.Status.Error;
            }
            //string[] dialerResultSplit = dialerResult.Split(',');
            //if (dialerResultSplit[0].Trim() == "1")
            //{
            //    LogUtils.MyHandle.WriteToLog("init service request to dialer return with error. requestId = {0}", nonPpcRequest.new_nonppcrequestid.ToString());
            //    nonPpcRequest.statuscode = (int)DML.Xrm.new_nonppcrequest.Status.Error;
            //}
            DataAccessLayer.NonPpcRequest nonPpcRequestDal = new NoProblem.Core.DataAccessLayer.NonPpcRequest(XrmDataContext);
            nonPpcRequestDal.Update(nonPpcRequest);
            XrmDataContext.SaveChanges();
        }

        private DML.Xrm.new_nonppcrequest CreateNonPpcRequestEntry(NonPpcServiceRequest request)
        {
            DataAccessLayer.NonPpcRequest nonPpcRequestDAL = new NoProblem.Core.DataAccessLayer.NonPpcRequest(XrmDataContext);
            DML.Xrm.new_nonppcrequest nonPpcRequest = new NoProblem.Core.DataModel.Xrm.new_nonppcrequest();
            nonPpcRequest.new_consumerphone = request.ContactPhoneNumber;
            nonPpcRequest.new_advertisername = request.AdvertiserName;
            nonPpcRequest.new_advertiserid = request.AdvertiserId;
            nonPpcRequest.new_advertiserphone1 = request.AdvertiserPhoneNumber;
            nonPpcRequest.new_advertiserphone2 = request.AdvertiserPhoneNumber2;
            nonPpcRequest.new_calltime = DateTime.Now;
            nonPpcRequest.statuscode = (int)DML.Xrm.new_nonppcrequest.Status.New;
            nonPpcRequest.new_callstatus = "QUEUED";
            nonPpcRequestDAL.Create(nonPpcRequest);
            XrmDataContext.SaveChanges();
            return nonPpcRequest;
        }

        #endregion
    }
}
