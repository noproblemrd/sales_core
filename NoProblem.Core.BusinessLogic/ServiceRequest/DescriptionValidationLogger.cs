﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class DescriptionValidationLogger : XrmUserBase
    {
        #region Ctor
        public DescriptionValidationLogger(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        public void LogValidation(DataModel.DescriptionValidationLogRequest request)
        {
            DataModel.Xrm.new_descriptionvalidationlog log = new NoProblem.Core.DataModel.Xrm.new_descriptionvalidationlog();
            log.new_heading = request.Heading;
            log.new_keyword = request.Keyword;
            log.new_phone = request.Phone;
            log.new_qualitycheckname = request.QualityCheckName.ToString();
            log.new_region = request.Region;
            log.new_session = request.Session;
            string url = request.Url;
            if (!string.IsNullOrEmpty(url) && url.Length > 1000)
            {
                url = url.Substring(0, 1000);
            }
            log.new_url = url;
            string description = request.Description;
            if (!string.IsNullOrEmpty(description) && description.Length > 200)
            {
                description = description.Substring(0, 200);
            }
            log.new_description = description;
            if (request.OriginId != Guid.Empty)
            {
                log.new_originid = request.OriginId;
            }
            log.new_event = request.Event;
            log.new_zipcodequalitycheckname = request.ZipCodeCheckName;
            log.new_flavor = request.Flavor;
            log.new_step = request.Step;
            log.new_control = request.Control;
            DataAccessLayer.DescriptionValidationLog dal = new NoProblem.Core.DataAccessLayer.DescriptionValidationLog(XrmDataContext);
            dal.Create(log);
            XrmDataContext.SaveChanges();
        }
    }
}
