﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    public class MobileBidManager
    {


        public bool ValidBid { get; private set; }
        public string FaildMessage { get; private set; }
        public bool IsLiveLead { get; private set; }
        public bool IsMakeBid { get; private set; }
        public Guid IncidentAccountId { get; private set; }
        public Guid IncidentId { get; private set; }
        public DataModel.Xrm.new_incidentaccount.Status incidentAccountStatus { get; private set; }
        public DataModel.Xrm.incident.Status incidentStatus { get; private set; }
        public DataModel.Xrm.account.SupplierStatus supplierStatus { get; private set; }

        public MobileBidManager(DataModel.SupplierModel.Mobile.SetBidForLeadRequest request)
        {
            IncidentAccountId = request.IncidentAccountId;
            string command = @"SELECT ia.statuscode IncidentAccountStatus, ia.New_BidDone, ia.new_incidentid, acc.New_status AccountStatus
FROM dbo.new_incidentaccount ia with(nolock)	
	INNER JOIN dbo.AccountExtensionBase acc on ia.new_accountid = acc.AccountId
where ia.DeletionStateCode = 0
	and ia.New_incidentaccountId = @IncidentAccountId";
      //      NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status incidentaccountStatus;
     //       bool IsMakeBid = false;
    //        Guid incidentId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@IncidentAccountId", request.IncidentAccountId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        incidentAccountStatus = (DataModel.Xrm.new_incidentaccount.Status)((int)reader["IncidentAccountStatus"]);
                        IsMakeBid = reader["New_BidDone"] == DBNull.Value ? false : (bool)reader["New_BidDone"];
                        supplierStatus = (DataModel.Xrm.account.SupplierStatus)((int)reader["AccountStatus"]);
                        IncidentId = (Guid)reader["new_incidentid"];
                    }
                    else                    
                        incidentAccountStatus = DataModel.Xrm.new_incidentaccount.Status.ERROR;
                    conn.Close();
                }
            }
    //        if (incidentAccountStatus == DataModel.Xrm.new_incidentaccount.Status.ERROR)
    //            return;
            if (IsMakeBid)
            {
                ValidBid = false;
                FaildMessage =  NoProblem.Core.DataModel.SupplierModel.Mobile.SetBidForLeadResponse.BidForLeadResponseMessage.DUPLICATE;
                return;
            }
            if(request.Price == 0)
            {
                /*
                if(supplierStatus != DataModel.Xrm.account.SupplierStatus.Trial)
                 */
                NoProblem.Core.DataAccessLayer.AccountRepository AccRep = new DataAccessLayer.AccountRepository(null);
                if(!AccRep.HaveFreeMobileLead(request.SupplierId))
                {
                    ValidBid = false;
                    FaildMessage = NoProblem.Core.DataModel.SupplierModel.Mobile.SetBidForLeadResponse.BidForLeadResponseMessage.DONT_HAVE_FREE_LEAD;
                    return;
                }
            }
            switch (incidentAccountStatus)
            {
                case (DataModel.Xrm.new_incidentaccount.Status.MATCH):
                case (DataModel.Xrm.new_incidentaccount.Status.IN_AUCTION):
                case (DataModel.Xrm.new_incidentaccount.Status.Waiting):
                    break;
                case (DataModel.Xrm.new_incidentaccount.Status.ERROR):
                    FaildMessage = DataModel.SupplierModel.Mobile.SetBidForLeadResponse.BidForLeadResponseMessage.INCIDENT_NOT_FOUND;
                    ValidBid = false;
                    return;
                default:
                    FaildMessage = DataModel.SupplierModel.Mobile.SetBidForLeadResponse.BidForLeadResponseMessage.MISSED;
                    ValidBid = false;
                    return;

            }
            
            DataModel.Xrm.incident.Status _incidentStatus;
            ServiceRequestManager srm = new ServiceRequestManager(null);
            if (!srm.IsMobileIncidentOpen(IncidentId, out _incidentStatus))
            {
                ValidBid = false;
                FaildMessage = DataModel.SupplierModel.Mobile.SetBidForLeadResponse.BidForLeadResponseMessage.MISSED;
                return;
            }
            incidentStatus = _incidentStatus;
            ValidBid = true;
        }

    }
}
