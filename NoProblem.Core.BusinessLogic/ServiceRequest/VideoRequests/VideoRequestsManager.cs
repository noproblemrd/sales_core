﻿
using NoProblem.Core.DataModel.Consumer.VideoRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.Consumer.ClipCall.Interfaces;
using System.Configuration;
using NoProblem.Core.DataAccessLayer;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests
{
    public class VideoRequestsManager : XrmUserBase
    {
        private static readonly Guid DEFAULT_REGION_ID;
        private static readonly string DEFAULT_COUNTRY;
        public static readonly Guid DEFAULT_EXPERTISE_ID;
        public static readonly string DEFAULT_EXPERTISE_NAME;
        private static Guid mobileOriginId;

        static VideoRequestsManager()
        {
            mobileOriginId = new Guid(ConfigurationManager.AppSettings["mobileOriginId"]);
            DEFAULT_EXPERTISE_ID = new Guid(ConfigurationManager.AppSettings["videoChatDefaultCategory"]);
            string country;
            DEFAULT_REGION_ID = GetDefaultRegion(out country);
            DEFAULT_COUNTRY = country;
            DEFAULT_EXPERTISE_NAME = GetCategoryName(DEFAULT_EXPERTISE_ID);
        }
        static Guid GetDefaultRegion(out string country)
        {
            country = null;
            Guid regionId = Guid.Empty;
            string command = @"
select top 1 New_regionId, New_Country
from dbo.New_region with(nolock)
where DeletionStateCode = 0 and New_Level is null and new_parentregionid is null";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if(reader.Read())
                        {
                            regionId = (Guid)reader["New_regionId"];
                            country = (string)reader["New_Country"];
                        }
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed to GetDefaultRegion");

            }
            return regionId;
        }
        static string GetCategoryName(Guid expertiseId)
        {
            string categoryName = null;
            string command = @"
SELECT New_name
FROM dbo.New_primaryexpertiseExtensionBase with(nolock)
WHERE New_primaryexpertiseId = @expertiseId";
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@expertiseId", expertiseId);
                        categoryName = (string)cmd.ExecuteScalar();
                        
                        conn.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed to GetCategoryName expertiseId = {0}", expertiseId);

            }
            return categoryName;
        }

        public VideoRequestsManager(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            
        }
        /*
        private Guid GetMobileOriginId()
        {
            return new Guid("35779B9E-759D-DF11-92C8-A4BADB37A26F");
        }
         * */

        public NoProblem.Core.DataModel.Consumer.CreateNewVideoLeadResponse CreateNewVideoRequest(NewVideoRequest request)
        {
            NoProblem.Core.DataModel.Consumer.CreateNewVideoLeadResponse _response = new DataModel.Consumer.CreateNewVideoLeadResponse();
            _response.UpdateCache = false;
            if(request.CategoryId == Guid.Empty)
            {
                if (string.IsNullOrEmpty(request.CategoryName))
                {
                    _response.LeadId = Guid.Empty;
                    _response.leadStatus = DataModel.Consumer.CreateNewVideoLeadResponse.eLeadStatus.Failed;
                    return _response;
                }
                Guid CategoryId = CheckIfCategoryExists(request.CategoryName);

                if(CategoryId == Guid.Empty)
                {
                    request.CategoryId = CreateNewCategory(request.CategoryName);
                    _response.UpdateCache = true;
                }
                else
                    request.CategoryId = CategoryId;
            }

           if(!ValidateVideoLeadRequest(request))
           {
               throw new Exception("Failed to create new video lead");
           }
           int numberProvider;
           if (request.IsPrivate)
           {
               if (request.FavoriteSupplierIds == null || request.FavoriteSupplierIds.Length == 0)
               {
                   _response.LeadId = Guid.Empty;
                   _response.leadStatus = DataModel.Consumer.CreateNewVideoLeadResponse.eLeadStatus.NotFavorite;
                   return _response;
               }
               FavoriteServiceProvider favoriteProvider = new FavoriteServiceProvider(XrmDataContext);
               foreach (Guid supplierId in request.FavoriteSupplierIds)
               {
                   if (!favoriteProvider.IsFavoriteSupplier(request.CustomerId, supplierId))
                   {
                       _response.LeadId = Guid.Empty;
                       _response.leadStatus = DataModel.Consumer.CreateNewVideoLeadResponse.eLeadStatus.NotFavorite;
                       return _response;
                   }
               }
               numberProvider = request.FavoriteSupplierIds.Length;
           }
           else
               numberProvider = ServiceRequestManager.NUMBER_PROVIDERS_MOBILE;
            ServiceRequest.ServiceRequestManager srManager = new ServiceRequestManager(XrmDataContext);
            var result = srManager.CreateServiceWithZipcodeUSA(
                new DataModel.ServiceRequestVideoLead()
                {
                    ContactId = request.CustomerId,
                    RegionCode = request.ZipCode,
                    ExpertiseCode = GetCodeFromCategoryId(request.CategoryId),
                    OriginId = mobileOriginId,
                    NumOfSuppliers = numberProvider,
                    VideroLeadUrl = request.VideoLeadUrl,
                    PreviewPicUrl = request.PreviewPicUrl,
                    VideoDuration = request.VideoDuration,
                    IsPrivate = request.IsPrivate,
                    FavoriteSupplierIds = request.FavoriteSupplierIds
                }
                );
            if (result.Status != DataModel.StatusCode.Success)
            {
                throw new Exception("Failed to create new video lead");
            }
            _response.LeadId = new Guid(result.ServiceRequestId);
            return _response;
        }
        private bool ValidateVideoLeadRequest(NewVideoRequest request)
        {
            if (request.CategoryId == Guid.Empty || request.CustomerId == Guid.Empty || string.IsNullOrEmpty(request.VideoLeadUrl) || string.IsNullOrEmpty(request.ZipCode))
                return false;
            return true;
        }
        private Guid CreateNewCategory(string CategoryName)
        {
            DataAccessLayer.PrimaryExpertise peDal = new DataAccessLayer.PrimaryExpertise(XrmDataContext);
            NoProblem.Core.DataModel.Xrm.new_primaryexpertise primaryExpertise = new DataModel.Xrm.new_primaryexpertise();
            primaryExpertise.new_name = CategoryName;
            primaryExpertise.new_code = GeneratePrimaryExpertiseCode();
            primaryExpertise.new_minprice = 5;
            primaryExpertise.new_channelid = GetCallChannel();
            peDal.Create(primaryExpertise);
            XrmDataContext.SaveChanges();
            /* remove on 20-10-2015
            DataAccessLayer.SlikerKeyword skDal = new DataAccessLayer.SlikerKeyword(XrmDataContext);
            NoProblem.Core.DataModel.Xrm.new_sliderkeyword sliderKeyword = new DataModel.Xrm.new_sliderkeyword();
            sliderKeyword.new_keyword = CategoryName.ToLower();
            sliderKeyword.new_primaryexpertiseid = primaryExpertise.new_primaryexpertiseid;
            skDal.Create(sliderKeyword);
            XrmDataContext.SaveChanges();
            */
            return primaryExpertise.new_primaryexpertiseid;
        }
        private Guid GetCallChannel()
        {
            Guid ChannelId = Guid.Empty;
            string command = "select New_channelId from new_channel WHERE New_Code = @code";
            int int_code = (int)NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Call;
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@code", int_code);
                    ChannelId = (Guid)cmd.ExecuteScalar();
                    conn.Close();
                
                }
            }
            return ChannelId;
        }
        private Guid CheckIfCategoryExists(string CategoryName)
        {
            string command = "EXEC [dbo].[GetIfExistPrimaryExpertise] @keyword";
            Guid _id = Guid.Empty;
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@keyword", CategoryName);
                    object o = cmd.ExecuteScalar();
                    _id = o == DBNull.Value ? Guid.Empty : (Guid)o;
                    conn.Close();
                    cmd.Dispose();
                }
            }
            return _id;
        }
        private string GeneratePrimaryExpertiseCode()
        {
            string command = "SELECT COUNT(*) FROM [dbo].[New_primaryexpertiseExtensionBase] WITH(NOLOCK) WHERE New_code = @code";
            bool IsUnique = false;
            int num = -1;
            while(!IsUnique)
            {
                Random rnd = new Random();
                num = rnd.Next(100000);
                using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@code", num.ToString());
                        IsUnique = (int)cmd.ExecuteScalar() == 0;
                        conn.Close();
                        cmd.Dispose();
                    }
                }
                
            }
            return num.ToString();
        }
        private string GetCodeFromCategoryId(Guid categoryId)
        {
            DataAccessLayer.PrimaryExpertise peDal = new DataAccessLayer.PrimaryExpertise(XrmDataContext);
            return peDal.GetCode(categoryId);
        }
        private Guid GetRegionIdAndCountry(out string country, Guid supplierId)
        {
            country = string.Empty;
            Guid regionId = Guid.Empty;
            string command = @"
SELECT TOP 1 R.New_regionId, R.New_Country
FROM dbo.New_coverarea CA with(nolock)
	INNER JOIN dbo.New_regionExtensionBase R with(nolock) on CA.New_SelectedArea3Id = R.New_regionId
WHERE CA.DeletionStateCode = 0 and CA.New_AccountId = @accountId";
            using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@accountId", supplierId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        regionId = (Guid)reader["New_regionId"];
                        country = reader["New_Country"] == DBNull.Value ? GlobalConfigurations.DefaultCountry : (string)reader["New_Country"];

                    }
                    reader.Dispose();
                    conn.Close();
                    cmd.Dispose();
                }
                conn.Dispose();
            }
            if(regionId == Guid.Empty)
            {
                command = "SELECT TOP 1 New_regionId, New_Country FROM dbo.New_region with(nolock) WHERE DeletionStateCode = 0 and New_Level = 4";
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@accountId", supplierId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            regionId = (Guid)reader["New_regionId"];
                            country = reader["New_Country"] == DBNull.Value ? GlobalConfigurations.DefaultCountry : (string)reader["New_Country"];

                        }
                        
                        conn.Close();
                    }
                }
            }
            return regionId;
        }
        /*
         * Guid CategoryId { get; set; }
        string ZipCode { get; set; }
        Guid CustomerId { get; set; }
        string VideoLeadUrl { get; set; }
        string PreviewPicUrl { get; set; }
        int VideoDuration { get; set; }
        string CategoryName { get; set; }
        Guid GetRegionId { get; set; }
        string country { get; set; }
        string CustomerPhone { get; set; }
         * */
        public void CreateNewVideoChat(Guid invitationId)
        {
            string command = @"
SELECT category.new_primaryexpertiseid CategoryId, category.New_name CategoryName, coverarea.New_SelectedArea2Id RegionId,
	acc.AccountId, vc.New_ContactId, con.MobilePhone, coverarea.New_Country
FROM dbo.New_videochatinvitationExtensionBase vc WITH(NOLOCK)
	inner join dbo.Contact con WITH(NOLOCK) on con.ContactId = vc.New_ContactId
	inner join  dbo.Account acc WITH(NOLOCK) on vc.New_AccountId = acc.AccountId
	OUTER apply
	(
		SELECT TOP 1 c.new_primaryexpertiseid, c.New_name
		FROM dbo.New_accountexpertise ce WITH(NOLOCK)
			inner join dbo.New_primaryexpertiseExtensionBase c WITH(NOLOCK) on ce.new_primaryexpertiseid = c.New_primaryexpertiseId
		WHERE ce.new_accountid = acc.AccountId and ce.DeletionStateCode = 0
	) category
	OUTER apply
	(
		SELECT TOP 1 ce.New_SelectedArea2Id, r.New_Country
		FROM dbo.New_coverarea ce WITH(NOLOCK)
			inner join dbo.New_regionExtensionBase r WITH(NOLOCK) on r.New_regionId = ce.New_SelectedArea2Id
		WHERE New_AccountId = acc.AccountId and DeletionStateCode = 0 and statuscode = 1
	) coverarea
WHERE vc.New_videochatinvitationId = @invitationId";

            NoProblem.Core.DataModel.Consumer.ClipCall.LeadRequestVideoChat videoChatRequest = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@invitationId", invitationId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            videoChatRequest = new DataModel.Consumer.ClipCall.LeadRequestVideoChat(invitationId);
                            videoChatRequest.CategoryId = reader["CategoryId"] == DBNull.Value ? Guid.Empty : (Guid)reader["CategoryId"];
                            videoChatRequest.CategoryName = reader["CategoryName"] == DBNull.Value ? null : (string)reader["CategoryName"];
                            videoChatRequest.CustomerId = (Guid)reader["New_ContactId"];
                            videoChatRequest.RegionId = reader["RegionId"] == DBNull.Value ? Guid.Empty: new Guid((string)reader["RegionId"]);
                            videoChatRequest.AccountId = (Guid)reader["AccountId"];
                            videoChatRequest.CustomerPhone = (string)reader["MobilePhone"];
                            videoChatRequest.country = reader["New_Country"] == DBNull.Value ? GlobalConfigurations.DefaultCountry : (string)reader["New_Country"];
                        }
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Failed to CreateNewVideoChat get details. invitationId = {0}", invitationId);
                return;
            }
            if(videoChatRequest == null)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(new Exception("didn't find any details"), "Failed to CreateNewVideoChat didn't find details. invitationId = {0}", invitationId);
                return;
            }
            AddFavoriteServiceProvider(videoChatRequest.CustomerId, videoChatRequest.AccountId);
            if(videoChatRequest.RegionId == Guid.Empty)
            {
                videoChatRequest.RegionId = DEFAULT_REGION_ID;
                videoChatRequest.country = DEFAULT_COUNTRY;
            }
            if(videoChatRequest.CategoryId == Guid.Empty)
            {
                videoChatRequest.CategoryId = DEFAULT_EXPERTISE_ID;
                videoChatRequest.CategoryName = DEFAULT_EXPERTISE_NAME;
            }
            NoProblem.Core.DataModel.Xrm.incident _incident = CreateNewVideoChatIncident(videoChatRequest);
            Guid incidentaccountid = CreateIncidentAccount(_incident, videoChatRequest);
            NoProblem.Core.DataAccessLayer.VideoChatInvitation chatInvitation = new DataAccessLayer.VideoChatInvitation();
            chatInvitation.UpdateVideoChatInvitation(invitationId, incidentaccountid);//, archiveId);

        }
        private void AddFavoriteServiceProvider(Guid customerId, Guid accountId)
        {
            NoProblem.Core.DataAccessLayer.FavoriteServiceProvider favoriteDal = new DataAccessLayer.FavoriteServiceProvider(null);
            favoriteDal.AddFavoriteServiceProvider(customerId, accountId);
        }
        private NoProblem.Core.DataModel.Xrm.incident CreateNewVideoChatIncident(NoProblem.Core.DataModel.Consumer.ClipCall.Interfaces.ILeadRequest request)
        {
            NoProblem.Core.DataModel.Xrm.incident incident = new NoProblem.Core.DataModel.Xrm.incident();
         //   incident.statuscode
            incident.customerid = new Microsoft.Crm.Sdk.Customer("contact", request.CustomerId);
            incident.new_telephone1 = request.CustomerPhone;
            incident.new_regionid = request.RegionId;
            incident.new_country = request.country;
            if (request is NoProblem.Core.DataModel.Consumer.ClipCall.LeadRequestVideoChat)
            {
                 incident.statuscode = (int)NoProblem.Core.DataModel.Xrm.incident.Status.PRIVATE;
                 incident.new_dialerstatus = (int)NoProblem.Core.DataModel.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
                incident.casetypecode = (int)NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode.VideoChat;
                incident.new_requiredaccountno = 1;            
                incident.new_ordered_providers = 1;
            }
            else
                return null;
            incident.new_primaryexpertiseid = request.CategoryId;
            incident.title = GetTitleForIncident(request);
            incident.new_preferedcalltime = DateTime.Now;
            incident.new_relaunchdate = DateTime.UtcNow;
                        
            incident.new_originid = request.OriginId;
            incident.new_isdirectnumber = false;
            incident.new_originid = mobileOriginId;

            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            incidentDal.Create(incident);
            XrmDataContext.SaveChanges();
            
    //            SendNewVideoLeadNotification(incident);
             
            

            return incident;
        }
        private Guid CreateIncidentAccount(NoProblem.Core.DataModel.Xrm.incident incident, ILeadRequest request)
        {
            DataAccessLayer.IncidentAccount incidentAccountDAL = new DataAccessLayer.IncidentAccount(XrmDataContext);
            NoProblem.Core.DataModel.Xrm.new_incidentaccount incidentAccount = new NoProblem.Core.DataModel.Xrm.new_incidentaccount();
            DataAccessLayer.AccountExpertise accountExpertiseDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
     //       DML.Xrm.new_accountexpertise accExpertise = accountExpertiseDal.GetByAccountAndPrimaryExpertise(supplier.accountid, incident.new_primaryexpertiseid.Value);
            incidentAccount.new_accountid = request.AccountId;
            incidentAccount.new_incidentid = incident.incidentid;
            incidentAccount.new_potentialincidentprice = 0;
            incidentAccount.new_considerasprice = 0;
            incidentAccount.new_predefinedprice = 0;
            incidentAccount.statuscode = (int)NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSE;
            incidentAccount.new_tocharge = true;
            incidentAccount.new_isjobdone = null;
            incidentAccount.new_isansweredbymachine = null;
            incidentAccountDAL.Create(incidentAccount);
            XrmDataContext.SaveChanges();
            return incidentAccount.new_incidentaccountid;
        }
        private string GetTitleForIncident(ILeadRequest request)
        {
            if (request is NoProblem.Core.DataModel.Consumer.ClipCall.LeadRequestVideoChat)
                return "VideoChat lead: " + request.CategoryName;
            return "Video lead: " + request.CategoryName;
            
        }
        /*
        private bool CheckInvitationDetails(invitationId)
        {

        }
         */
           
        
    }
}
