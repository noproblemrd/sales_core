﻿using NoProblem.Core.DataModel.ClipCall.Request;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;


namespace NoProblem.Core.BusinessLogic.ServiceRequest.VideoRequests
{
    public enum TypeExecuteUser
    {
        Customer = 1,
        Publisher = 2,
        System = 3
    }
    public class RelaunchIncidentLog : Runnable
    {

        //DML.Xrm.incident incident;
        Guid incidentId;
        bool IsRelaunch;
        TypeExecuteUser UserType;
        RelaunchChangeValues changeValues;
        public RelaunchIncidentLog(Guid incidentId, bool IsRelaunch, TypeExecuteUser UserType)
        {
            this.incidentId = incidentId;
            this.IsRelaunch = IsRelaunch;
            this.UserType = UserType;
        }
        public RelaunchIncidentLog(Guid incidentId, bool IsRelaunch, TypeExecuteUser UserType,
            RelaunchChangeValues changeValues)
            : this(incidentId, IsRelaunch, UserType)
        {
            this.changeValues = changeValues;
        }
        
        protected override void Run()
        {
            string command = @"
INSERT INTO dbo.IncidentRelaunchLog
	(IncidentId,  NewRegion, OldRegion,
	NewCategory, OldCategory, IsRelaunch, UserType)
VALUES (@IncidentId, @NewRegion, @OldRegion,
	@NewCategory, @OldCategory, @IsRelaunch, @UserType)";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@IncidentId", incidentId);
                    cmd.Parameters.AddWithValue("@IsRelaunch", IsRelaunch);
                    cmd.Parameters.AddWithValue("@UserType", (int)UserType);
                    if (changeValues != null)
                    {
                        cmd.Parameters.AddWithValue("@NewRegion", changeValues.NewRegionId);
                        cmd.Parameters.AddWithValue("@OldRegion", changeValues.OldRegionId);
                        cmd.Parameters.AddWithValue("@NewCategory", changeValues.NewExpertiseId);
                        cmd.Parameters.AddWithValue("@OldCategory", changeValues.OldExpertiseId);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@NewRegion", DBNull.Value);
                        cmd.Parameters.AddWithValue("@OldRegion", DBNull.Value);
                        cmd.Parameters.AddWithValue("@OldCategory", DBNull.Value);
                        cmd.Parameters.AddWithValue("@NewCategory", DBNull.Value);
                    }
                    
                    
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
        

        
    }
}
