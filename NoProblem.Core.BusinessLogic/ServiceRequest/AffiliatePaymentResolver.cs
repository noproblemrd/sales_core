﻿using NoProblem.Core.BusinessLogic.Origins;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Consumer.Interfaces;
using System;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    internal class AffiliatePaymentResolver : XrmUserBase
    {
        private DataAccessLayer.AffiliatePayStatusReason payReasonsDal;
        private AffiliatesManager affiliateManager;
        private DataAccessLayer.Incident incidentDal;

        public AffiliatePaymentResolver(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            payReasonsDal = new DataAccessLayer.AffiliatePayStatusReason(XrmDataContext);
            affiliateManager = new AffiliatesManager(XrmDataContext);
            incidentDal = new DataAccessLayer.Incident(XrmDataContext);
        }

        internal void ResolveAffiliatePayment(IServiceResponse response, DML.Xrm.incident incident, bool badWordFound, bool isUpsale)
        {
            Guid payStatusId;
            Guid payReasonId;
            incident.new_showaffiliate = true;
            if (response.Status == StatusCode.BlackList)
            {
                payReasonId = payReasonsDal.GetBlackListPayStatus(out payStatusId);
            }
            else if (badWordFound)
            {
                payReasonId = payReasonsDal.GetBadWordPayStatus(out payStatusId);
            }
            else if (CheckIfIsDuplicate(incident))
            {
                incident.statuscode = (int)DML.Xrm.incident.Status.DUPLICATE;
                response.Status = StatusCode.Duplicate;
                incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
                payReasonId = payReasonsDal.GetDuplicatePayStatus(out payStatusId);
            }
            else
            {
                bool showAffilite = affiliateManager.CalculateAffiliatePayStatus(out payStatusId, out payReasonId, incident.new_primaryexpertiseid.Value, incident.new_originid.Value, incident.customerid.Value, false, incidentDal, isUpsale, incident.new_regionid.Value, false);
                incident.new_showaffiliate = showAffilite;// if false, this means that no payment because of interval.
            }
            incident.new_affiliatepaystatusid = payStatusId;
            incident.new_affiliatepaystatusreasonid = payReasonId;
        }

        internal void ResolveAffiliatePayment(DML.Xrm.incident incident)
        {
            DataModel.ServiceResponse response = new ServiceResponse();
            ResolveAffiliatePayment(response, incident, false, false);
        }

        private bool CheckIfIsDuplicate(NoProblem.Core.DataModel.Xrm.incident incident)
        {
            bool retVal = false;
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string secsStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.CHECK_CASE_IS_DUPLICATE_SECONDS);
            int seconds;
            bool parsed = int.TryParse(secsStr, out seconds);
            if (!parsed || seconds == 0)
            {
                return retVal;
            }
            retVal = incidentDal.CheckIsDuplicate(incident.customerid.Value, incident.new_primaryexpertiseid.Value, incident.new_regionid.Value, incident.new_originid.Value, seconds);
            return retVal;
        }
    }
}
