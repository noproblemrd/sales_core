﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.CustomerTips
{
    internal class TipStatistics
    {
        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();

        internal List<DataModel.Pusher.CaseFlow.AdvertiserData.TipPercent> GetTipsPercentages(Guid accountId, Guid incidentId)
        {
            List<DataModel.Pusher.CaseFlow.AdvertiserData.TipPercent> retVal =
                new List<DataModel.Pusher.CaseFlow.AdvertiserData.TipPercent>();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@supplierId", accountId);
            parameters.Add("@incidentId", incidentId);
            var reader = dal.ExecuteReader(feedBackQuery, parameters);
            Dictionary<Guid, PositiveAndTotal> dict = new Dictionary<Guid, PositiveAndTotal>();
            foreach (var row in reader)
            {
                Guid tipId = (Guid)row["tipId"];
                bool isPositiveRow = (bool)row["isPositive"];
                int count = (int)row["count"];
                if (!dict.ContainsKey(tipId))
                    dict.Add(tipId, new PositiveAndTotal());
                dict[tipId].AddToTotal(count);
                if (isPositiveRow)
                    dict[tipId].SetPositive(count);
            }
            foreach (var item in dict)
            {
                var tipPercent = new DataModel.Pusher.CaseFlow.AdvertiserData.TipPercent();
                tipPercent.TipId = item.Key;
                if (dict[item.Key].Total >= 3)
                {
                    tipPercent.Show = true;
                    tipPercent.Percent = (double)dict[item.Key].Positive / (double)dict[item.Key].Total;
                }
                else
                {
                    tipPercent.Show = false;
                }
                retVal.Add(tipPercent);
            }
            return retVal;
        }

        private class PositiveAndTotal
        {
            public void SetPositive(int n)
            {
                Positive = n;
            }

            public void AddToTotal(int n)
            {
                Total += n;
            }

            public int Positive { get; private set; }
            public int Total { get; private set; }
        }

        private const string feedBackQuery =
    @"
select count(*) count, res.New_CustomerTipId tipId, New_PositiveFeedback isPositive
from incident inc with(nolock)
join new_customertip tip with(nolock) on inc.new_primaryexpertiseid = tip.new_primaryexpertiseid
join New_customertipadvertiserresponse res with(nolock) on tip.New_customertipId = res.New_CustomerTipId
where 
res.New_AccountId = @supplierId
and inc.IncidentId = @incidentId
group by res.New_CustomerTipId, New_PositiveFeedback
";
    }
}
