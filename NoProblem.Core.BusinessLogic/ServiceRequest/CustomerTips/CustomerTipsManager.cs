﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest.CustomerTips
{
    public class CustomerTipsManager : XrmUserBase
    {
        public CustomerTipsManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public IEnumerable<DataModel.Consumer.Tips.TipBasicData> GetTipsForIncidentId(Guid incidentId)
        {
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
            Guid categoryId = (from inc in incidentDal.All
                               where inc.incidentid == incidentId
                               select inc.new_primaryexpertiseid.Value).First();
            DataAccessLayer.CustomerTip tipDal = new DataAccessLayer.CustomerTip(XrmDataContext);
            return tipDal.GetByCategoryId(categoryId);
        }

        public void SetCustomerTipFeedback(DataModel.Consumer.Tips.TipFeedback feedback)
        {
            DataAccessLayer.CustomerTipAdveritserResponse dal = new DataAccessLayer.CustomerTipAdveritserResponse(XrmDataContext);
            DataModel.Xrm.new_customertipadvertiserresponse entity = DataModel.EntityFactories.CustomerTipAdveritserResponseFactory.Build(feedback.TipId, feedback.SupplierId, feedback.IncidentId, feedback.IsPositiveFeedback, feedback.IsYelpSupplier);
            dal.Create(entity);
            XrmDataContext.SaveChanges();
        }
    }
}
