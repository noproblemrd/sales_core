﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ServiceRequest
{
    internal class ServiceRequestCloser : XrmUserBase
    {
        public ServiceRequestCloser(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
        }

        DataAccessLayer.ConfigurationSettings config;

        private static Guid _toPayId = Guid.Empty;
        private Guid ToPayId
        {
            get
            {
                if (_toPayId == Guid.Empty)
                {
                    DataAccessLayer.AffiliatePayStatus payStatusDal = new NoProblem.Core.DataAccessLayer.AffiliatePayStatus(XrmDataContext);
                    _toPayId = payStatusDal.GetToPayStatusId();
                }
                return _toPayId;
            }
        }

        private static Guid _notToPayId = Guid.Empty;
        private Guid NotToPayId
        {
            get
            {
                if (_notToPayId == Guid.Empty)
                {
                    DataAccessLayer.AffiliatePayStatus payStatusDal = new NoProblem.Core.DataAccessLayer.AffiliatePayStatus(XrmDataContext);
                    _notToPayId = payStatusDal.GetNotToPayStatusId();
                }
                return _notToPayId;
            }
        }

        private static Guid _notForSaleReasonId = Guid.Empty;
        private Guid NotForSaleReasonId
        {
            get
            {
                if (_notForSaleReasonId == Guid.Empty)
                {
                    DataAccessLayer.AffiliatePayStatusReason payReasonsDal = new NoProblem.Core.DataAccessLayer.AffiliatePayStatusReason(XrmDataContext);
                    Guid statusId;
                    _notForSaleReasonId = payReasonsDal.GetNotForSalePayStatus(out statusId);
                }
                return _notForSaleReasonId;
            }
        }

        private static Guid _defaultReasonId = Guid.Empty;
        private Guid DefaultReasonId
        {
            get
            {
                if (_defaultReasonId == Guid.Empty)
                {
                    DataAccessLayer.AffiliatePayStatusReason payReasonsDal = new NoProblem.Core.DataAccessLayer.AffiliatePayStatusReason(XrmDataContext);
                    Guid statusId;
                    _defaultReasonId = payReasonsDal.GetDefault(out statusId);
                }
                return _defaultReasonId;
            }
        }

        public void CloseRequest(Guid incidentId)
        {
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = dal.Retrieve(incidentId);
            CloseRequest(incident, dal);
        }

        public void CloseRequest(DataModel.Xrm.incident incident)
        {
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            CloseRequest(incident, dal);
        }

        public void CloseRequest(DataModel.Xrm.incident incident, DataAccessLayer.Incident dal)
        {
            if (IsConfigurationEnabled())
            {
                ChangeStatusesAsNeeded(incident, dal);
            }
            SaveChanges(incident, dal);
            AutoResubmission.ResubmissionScheduler scheduler = new AutoResubmission.ResubmissionScheduler(incident.incidentid);
            scheduler.Start();
        }

        private bool IsConfigurationEnabled()
        {
            return config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.NOT_FOR_SALE_DONT_PAY_AFFILIATES) == 1.ToString();
        }

        private void ChangeStatusesAsNeeded(DataModel.Xrm.incident incident, DataAccessLayer.Incident dal)
        {
            if (!dal.HasBillableCalls(incident.incidentid))
            {
                //if is yes to pay, make not to pay with reason not for sale.
                if (incident.new_affiliatepaystatusid.Value == ToPayId)
                {
                    incident.new_affiliatepaystatusid = NotToPayId;
                    incident.new_affiliatepaystatusreasonid = NotForSaleReasonId;
                }
            }
            else if (incident.new_upsaleincidentid.HasValue)
            {
                //if parent request is not to pay because is not for sale, update it to yes to pay.
                var parentIncidentId = dal.Retrieve(incident.new_upsaleincidentid.Value);
                if (parentIncidentId.new_affiliatepaystatusreasonid.Value == NotForSaleReasonId)
                {
                    parentIncidentId.new_affiliatepaystatusid = ToPayId;
                    parentIncidentId.new_affiliatepaystatusreasonid = DefaultReasonId;
                    dal.Update(parentIncidentId);
                }
            }
        }

        private void SaveChanges(DataModel.Xrm.incident incident, DataAccessLayer.Incident dal)
        {
            dal.Update(incident);
            XrmDataContext.SaveChanges();
        }
    }
}
