﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataAccessLayer.Bases;
using NoProblem.Core.DataModel.Consumer;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.BusinessLogic.AddOns
{
    public class AddOnsManager : XrmUserBase
    {
        #region Ctor
        public AddOnsManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        public void HandleAddOnAction(Guid originId, List<string> macAddresses, string ip, bool isInstallation, string appType,
            bool? isDuplicate, string subId, string Country, bool isUpdater)
        {
            ValidateArguments(originId, macAddresses);
            DataAccessLayer.AddOnMACAddress macDal = new NoProblem.Core.DataAccessLayer.AddOnMACAddress(XrmDataContext);
            Dictionary<DataModel.Xrm.new_addonmacaddress, bool> macEntriesLst = new Dictionary<DataModel.Xrm.new_addonmacaddress, bool>();//if the bool = true then created else existed.
            bool allNew = FindMacAddresses(macAddresses, macDal, macEntriesLst);
            DataModel.Xrm.new_addonuser user = FindAndUpdateAddOnUser(originId, isInstallation, macEntriesLst, allNew, appType);
            UpdateMacAddressesWithUser(macDal, macEntriesLst, user);
            CreateAddOnAction(originId, ip, isInstallation, user.new_addonuserid, appType, isDuplicate, subId, Country, isUpdater);
        }

        public void DeskAppLog(DataModel.Consumer.DeskAppLogRequest request)
        {
            DeskAppLoggerThread tr = new DeskAppLoggerThread(request);
            tr.Start();
        }

        #endregion

        #region Private Methods

        private void UpdateMacAddressesWithUser(DataAccessLayer.AddOnMACAddress macDal, Dictionary<DataModel.Xrm.new_addonmacaddress, bool> macEntriesLst, DataModel.Xrm.new_addonuser user)
        {
            bool needUpdate = false;
            foreach (var item in macEntriesLst)
            {
                if (!item.Key.new_addonuserid.HasValue)
                {
                    item.Key.new_addonuserid = user.new_addonuserid;
                    macDal.Update(item.Key);
                    needUpdate = true;
                }
            }
            if (needUpdate)
                XrmDataContext.SaveChanges();
        }

        private DataModel.Xrm.new_addonuser FindAndUpdateAddOnUser(Guid originId, bool isInstallation, Dictionary<DataModel.Xrm.new_addonmacaddress, bool> macEntriesLst, bool allNew, string appType)
        {
            DataAccessLayer.AddOnUser userDal = new NoProblem.Core.DataAccessLayer.AddOnUser(XrmDataContext);

            DataModel.Xrm.new_addonuser user = null;
            if (allNew)
            {
                user = CreateAddOnUser(originId, isInstallation, userDal, appType);
            }
            else
            {
                Guid userId = Guid.Empty;
                foreach (var item in macEntriesLst)
                {
                    if (!item.Value && item.Key.new_addonuserid.HasValue)
                    {
                        userId = item.Key.new_addonuserid.Value;
                        break;
                    }
                }
                if (userId != Guid.Empty)
                {
                    user = userDal.Retrieve(userId);
                }
            }

            if (user == null)
            {
                user = CreateAddOnUser(originId, isInstallation, userDal, appType);
            }
            else
            {
                if (isInstallation && !user.new_isinstalled.Value)
                {
                    user.new_originid = originId;
                }
                user.new_isinstalled = isInstallation;
                user.new_apptype = appType;
                userDal.Update(user);
                XrmDataContext.SaveChanges();
            }
            return user;
        }

        private void ValidateArguments(Guid originId, List<string> macAddresses)
        {
            if (macAddresses == null || macAddresses.Count == 0)
            {
                throw new ArgumentException("macAddresses cannot be null nor empty", "macAddressses");
            }
            if (originId == Guid.Empty)
            {
                throw new ArgumentException("originId cannot be empty", "originId");
            }
        }

        private void CreateAddOnAction(Guid originId, string ip, bool isInstallation, Guid userId, string appType, bool? isDuplicate, string subId, string Country, bool isUpdater)
        {
            if (!isUpdater)
            {
                CreateNormalAddOnAction(originId, ip, isInstallation, userId, appType, isDuplicate, subId, Country);
            }
            else
            {
                CreateUpdaterAddOnAction(originId, ip, isInstallation, userId, appType, isDuplicate, subId, Country);
            }
        }

        private void CreateUpdaterAddOnAction(Guid originId, string ip, bool isInstallation, Guid userId, string appType, bool? isDuplicate, string subId, string Country)
        {
            DataAccessLayer.AddOnActionByUpdater actionDal = new NoProblem.Core.DataAccessLayer.AddOnActionByUpdater(XrmDataContext);
            DataModel.Xrm.new_addonactionbyupdater action = new NoProblem.Core.DataModel.Xrm.new_addonactionbyupdater();
            action.new_ip = ip;
            action.new_addonuserid = userId;
            action.new_originid = originId;
            action.new_isinstallation = isInstallation;
            action.new_apptype = appType;
            action.new_isduplicate = isDuplicate;
            action.new_subid = subId;
            action.new_country = Country;
            actionDal.Create(action);
            XrmDataContext.SaveChanges();
        }

        private void CreateNormalAddOnAction(Guid originId, string ip, bool isInstallation, Guid userId, string appType, bool? isDuplicate, string subId, string Country)
        {
            DataAccessLayer.AddOnAction actionDal = new NoProblem.Core.DataAccessLayer.AddOnAction(XrmDataContext);
            DataModel.Xrm.new_addonaction action = new NoProblem.Core.DataModel.Xrm.new_addonaction();
            action.new_ip = ip;
            action.new_addonuserid = userId;
            action.new_originid = originId;
            action.new_isinstallation = isInstallation;
            action.new_apptype = appType;
            action.new_isduplicate = isDuplicate;
            action.new_subid = subId;
            action.new_country = Country;
            actionDal.Create(action);
            XrmDataContext.SaveChanges();
        }

        private bool FindMacAddresses(List<string> macAddresses, DataAccessLayer.AddOnMACAddress macDal, Dictionary<DataModel.Xrm.new_addonmacaddress, bool> macEntriesLst)
        {
            bool allNew = true;
            foreach (var item in macAddresses)
            {
                DataModel.Xrm.new_addonmacaddress mac = macDal.FindMacAddressEntry(item);
                if (mac == null)
                {
                    mac = new NoProblem.Core.DataModel.Xrm.new_addonmacaddress();
                    mac.new_name = item;
                    macDal.Create(mac);
                    XrmDataContext.SaveChanges();
                    macEntriesLst.Add(mac, true);
                }
                else
                {
                    macEntriesLst.Add(mac, false);
                    allNew = false;
                }
            }
            return allNew;
        }

        private DataModel.Xrm.new_addonuser CreateAddOnUser(Guid originId, bool isInstallation, DataAccessLayer.AddOnUser userDal, string appType)
        {
            DataModel.Xrm.new_addonuser user = new NoProblem.Core.DataModel.Xrm.new_addonuser();
            user.new_isinstalled = isInstallation;
            user.new_originid = originId;
            user.new_apptype = appType;
            userDal.Create(user);
            XrmDataContext.SaveChanges();
            return user;
        }

        #endregion

        public void ReportGhostInstallation(ReportInstallationRequest request)
        {
            var entity = new new_ghostinstallations
            {
                new_appname = request.ApplicationName,
                new_ipaddress = request.IpAddress
            };

            var dbContext = DataModel.XrmDataContext.Create();
            var repository = new Repository(dbContext);
            repository.Create(entity);

        }
    }
}
