﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.AddOns
{
    public class DeskAppLoggerThread : Runnable
    {
        DataModel.Consumer.DeskAppLogRequest request;
        DataAccessLayer.AddOnMACAddress addonIdDal;

        public DeskAppLoggerThread(DataModel.Consumer.DeskAppLogRequest request)
        {
            this.request = request;
            addonIdDal = new DataAccessLayer.AddOnMACAddress(XrmDataContext);
        }

        protected override void Run()
        {
            //find userid in mac addresses table
            var query = from m in addonIdDal.All
                        where m.new_name == request.UserId
                        select new { m.new_addonuserid };
            var found = query.FirstOrDefault();
            //if not found create user and then create  userid in mac addresses table
            Guid userId;
            if (found == null)
            {
                userId = CreateUser();
            }
            else
            {
                userId = found.new_addonuserid.Value;
            }
            //save log entry and attach to user
            CreateLogEntry(userId);
        }

        private Guid CreateUser()
        {
            var user = DataModel.EntityFactories.AddOnUserFactory.Create(true, request.OriginId, request.AppType);
            DataAccessLayer.AddOnUser addonUserDal = new DataAccessLayer.AddOnUser(XrmDataContext);
            addonUserDal.Create(user);
            XrmDataContext.SaveChanges();
            var id = DataModel.EntityFactories.AddOnMacAddressFactory.Create(request.UserId, user.new_addonuserid);
            addonIdDal.Create(id);
            XrmDataContext.SaveChanges();
            return user.new_addonuserid;
        }

        private void CreateLogEntry(Guid userId)
        {
            var logEntry = DataModel.EntityFactories.DeskAppLogEntryFactory.Create(request, userId);
            DataAccessLayer.DeksAppLogEntry dal = new DataAccessLayer.DeksAppLogEntry(XrmDataContext);
            dal.Create(logEntry);
            XrmDataContext.SaveChanges();
            //save installed programs  
            //SaveInstalledPrograms(logEntry);
        }

        private void SaveInstalledPrograms(DataModel.Xrm.new_deskapplogentry logEntry)
        {

            Newtonsoft.Json.Linq.JArray installedPrograms;
            try
            {
                installedPrograms = ((Newtonsoft.Json.Linq.JArray)Newtonsoft.Json.JsonConvert.DeserializeObject(request.InstalledPrograms));
            }
            catch (Exception)
            {
                return;
            }
            DataAccessLayer.UserInstalledProgram uipDal = new DataAccessLayer.UserInstalledProgram(XrmDataContext);
            int count = 0;
            foreach (string programName in installedPrograms)
            {
                count++;
                DataModel.Xrm.new_userinstalledprogram p = DataModel.EntityFactories.UserInstalledProgramFactory.Create(programName, logEntry.new_deskapplogentryid);
                uipDal.Create(p);
                if (count % 10 == 0)
                {
                    SaveInstalledPrograms();
                }
            }
            SaveInstalledPrograms();
        }

        private void SaveInstalledPrograms()
        {
            try
            {
                XrmDataContext.SaveChanges();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Failed to save installed programs");
                XrmDataContext.ClearChanges();
            }
        }
    }
}
