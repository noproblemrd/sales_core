﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.CallPricePickerTool
{
    internal class CallPricePickerToolManager
    {
        private const string ONE = "1";
        private static bool? _isToolEnabled;
        private static bool IsToolEnabled
        {
            get
            {
                if (!_isToolEnabled.HasValue)
                {
                    DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
                    _isToolEnabled = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.CALL_PRICE_PICKER_TOOL_ENABLED) == ONE;
                }
                return _isToolEnabled.Value;
            }
        }

        public static void SaveStats(Guid accountId, Guid incidentAccountId)
        {
            if (IsToolEnabled)
            {
                StatsSaver saver = new StatsSaver(accountId, incidentAccountId);
                saver.SaveStats();
            }
        }
    }
}
