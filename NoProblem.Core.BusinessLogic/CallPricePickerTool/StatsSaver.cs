﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.CallPricePickerTool
{
    internal class StatsSaver : XrmUserBase
    {
        Thread thread;
        Guid accountId;
        Guid incidentAccountId;

        public StatsSaver(Guid accountId, Guid incidentAccountId)
            : base(null)
        {
            thread = new Thread(new ThreadStart(Run));
            this.accountId = accountId;
            this.incidentAccountId = incidentAccountId;
        }

        public void SaveStats()
        {
            thread.Start();
        }

        private void Run()
        {
            try
            {
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                var supplier = accountRepositoryDal.Retrieve(accountId);
                if (supplier.new_dapazstatus.HasValue && supplier.new_dapazstatus.Value != (int)DataModel.Xrm.account.DapazStatus.PPA)
                {
                    return;
                }
                DataAccessLayer.IncidentAccount callDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                var call = callDal.Retrieve(incidentAccountId);
                decimal price = call.new_potentialincidentprice.Value;
                string bizId = supplier.new_bizid;
                string headingCode = call.new_incident_new_incidentaccount.new_new_primaryexpertise_incident.new_englishname;
                int place = PlaceCalculator.Instance.GetBestPlace(bizId, headingCode);
                if (place > 0)
                {
                    DataAccessLayer.CallPricePickerToolStats statsDal = new NoProblem.Core.DataAccessLayer.CallPricePickerToolStats(XrmDataContext);
                    DataModel.Xrm.new_callpricepickertoolstats stat = new NoProblem.Core.DataModel.Xrm.new_callpricepickertoolstats();
                    stat.new_place = place;
                    stat.new_price = price;
                    stat.new_headingcode = headingCode;
                    statsDal.Create(stat);
                    XrmDataContext.SaveChanges();
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in async method NoProblem.Core.BusinessLogic.CallPricePickerTool.StatsSaver.Run");
            }
        }
    }
}
