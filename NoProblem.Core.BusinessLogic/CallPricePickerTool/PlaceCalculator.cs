﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml;

namespace NoProblem.Core.BusinessLogic.CallPricePickerTool
{
    public class PlaceCalculator
    {
        private PlaceCalculator()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            xmlLocation = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.DAPAZ_PPA_SYNC_DIR) + "PpaSync.xml";
            updateDataSyncRoot = new object();
            UpdateData();
        }

        private static volatile PlaceCalculator _instance;
        private static object syncRoot = new Object();

        public static PlaceCalculator Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new PlaceCalculator();
                        }
                    }
                }
                return _instance;
            }
        }

        private string xmlLocation;
        private object updateDataSyncRoot;

        private DataModel.Dapaz.PpaSync.PpaSyncData data;
        private DateTime dataUpdatedOn;

        private void UpdateData()
        {
            using (XmlReader xmlReader = XmlReader.Create(xmlLocation))
            {
                XmlSerializer ser = new XmlSerializer(typeof(DataModel.Dapaz.PpaSync.PpaSyncData));
                data = null;
                data = ser.Deserialize(xmlReader) as DataModel.Dapaz.PpaSync.PpaSyncData;
            }
            dataUpdatedOn = DateTime.Now;
        }

        public int GetBestPlace(string bizId, string headingCode)
        {
            if (dataUpdatedOn.AddHours(1) < DateTime.Now)
            {
                lock (updateDataSyncRoot)
                {
                    if (dataUpdatedOn.AddHours(1) < DateTime.Now)
                    {
                        UpdateData();
                    }
                }
            }
            var nodes =
                from node in data.CategoryRegionList
                where
                node.CategoryCode == headingCode
                &&
                node.AccountList.Contains(
                    new NoProblem.Core.DataModel.Dapaz.PpaSync.PpaSyncAccountData()
                    {
                        AccountNumberDapaz = bizId
                    })
                select node.AccountList.First(x => x.AccountNumberDapaz == bizId);
            if (nodes.Count() > 0)
            {
                return nodes.Min(x => x.OrderPriority);
            }
            else
            {
                return -1;
            }
        }
    }
}
