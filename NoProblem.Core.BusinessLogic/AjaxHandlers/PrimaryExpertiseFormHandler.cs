﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace NoProblem.Core.BusinessLogic.AjaxHandlers
{
    public class PrimaryExpertiseFormHandler : XrmUserBase
    {
        public PrimaryExpertiseFormHandler()
            : base(null)
        {

        }

        public DataModel.AjaxHandlersContainers.AccountValidationForDidSliderResponse ValidateAccountForDidSlider(Guid accountId, Guid headingId)
        {
            DataModel.AjaxHandlersContainers.AccountValidationForDidSliderResponse response = new NoProblem.Core.DataModel.AjaxHandlersContainers.AccountValidationForDidSliderResponse();
            DataAccessLayer.AccountExpertise dal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            var accExp = dal.GetByAccountAndPrimaryExpertise(accountId, headingId);
            if (accExp == null || accExp.statecode != DataAccessLayer.AccountExpertise.ACTIVE)
            {
                response.Success = false;
                response.Message = "BAD ACCOUNT: 'This account does not work in this heading'.";
            }
            else
            {
                DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                var directNumbers = dnDal.GetAllDirectNumbersOfAccountExpertise(accExp.new_accountexpertiseid);
                if (directNumbers.Count() == 0)
                {
                    response.Success = false;
                    response.Message = "BAD ACCOUNT: 'This account does not have any DIDs attached'.";
                }
                else
                {
                    response.Message = string.Format("GREAT! This account has {0} DIDs attached.", directNumbers.Count());
                }
            }
            return response;
        }
    }
}
