﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Deskapp;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.ConsumerDesktop
{
    public class ConsumerManager : XrmUserBase
    {
        #region Ctor
        public ConsumerManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion
        const int PIN_CODE_LENGTH = 4;
        /*
        const string ConsumerSignUpDesktopQuery = @"
SELECT con.ContactId, contactinvited.New_PinCode
FROM dbo.ContactExtensionBase con
	INNER JOIN dbo.ContactBase conb on con.ContactId = conb.ContactId
	left join 
	(
		SELECT TOP 1 c_inv_e.New_ContactInvited_ContactId, c_inv_e.New_PinCode 
		FROM dbo.New_contactinvitedExtensionBase c_inv_e 
			INNER JOIN dbo.New_contactinvitedBase c_inv on c_inv_e.New_contactinvitedId = c_inv.New_contactinvitedId
		WHERE c_inv.CreatedOn >= DATEADD(DAY, -1, GETDATE())
        ORDER BY c_inv.CreatedOn desc
	) contactinvited on contactinvited.New_ContactInvited_ContactId = con.ContactId
WHERE conb.deletionstatecode = 0 AND conb.MobilePhone = @phone";
         * */
        const string ConsumerSignUpDesktopQuery = @"
SELECT ContactId
FROM dbo.ContactBase 	
WHERE deletionstatecode = 0 AND MobilePhone = @phone";
        const string UpdateContactQuery = @"
UPDATE dbo.ContactExtensionBase
SET New_DesktopRegisterIP = @ip, New_DesktopRegisterBrowser = @browser, New_DesktopUserUniqueId = @UserId,
    New_DesktopRegisterCountry = @country, New_DesktopRegisterDate = GETDATE()
WHERE ContactId = @ContactId";
        public ConsumerSignUpDesktopResponse ConsumerSignUpDesktop(ConsumerSignUpDesktopRequest request)
        {
            ConsumerSignUpDesktopResponse response = new ConsumerSignUpDesktopResponse();
            //      bool HasConsumer = false;
            //   Guid ContactId = Guid.Empty;
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ConsumerSignUpDesktopQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@phone", request.phone);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        response.ConsumerId = (Guid)reader["ContactId"];
                        /*
                        if (reader["New_PinCode"] != DBNull.Value) 
                        { 
                            response.PinCode = (string)reader["New_PinCode"];                           
                            response.Status = SendSmsPincode(request.phone, response.PinCode);
                            return response;
                        }
                         * */
                        //         HasConsumer = true;                        
                    }
                    conn.Close();
                }
            }
            if (response.ConsumerId == Guid.Empty)
            {
                DataModel.Xrm.contact _contact = new NoProblem.Core.DataModel.Xrm.contact(XrmDataContext);
                DataAccessLayer.Contact con_dal = new DataAccessLayer.Contact(XrmDataContext);
                _contact.mobilephone = request.phone;
                _contact.new_desktopregistercountry = request.Country;
                _contact.new_desktopregisterbrowser = request.Browser;
                _contact.new_desktopregisterip = request.IP;
                _contact.new_desktopuseruniqueid = request.UserUniqueId.ToString();
                con_dal.Create(_contact);
                XrmDataContext.SaveChanges();
                response.ConsumerId = _contact.contactid;
                //            DataAccessLayer.Contact _contact_dal = null;
            }
            else
            {
                /*
                DataAccessLayer.Contact con_dal = new DataAccessLayer.Contact(XrmDataContext);
                DataModel.Xrm.contact _contact = con_dal.Retrieve(response.ConsumerId);
                _contact.new_desktopregistercountry = request.Country;
                _contact.new_desktopregisterbrowser = request.Browser;
                _contact.new_desktopregisterip = request.IP;
                con_dal.Update(_contact);
                XrmDataContext.SaveChanges();
                 * */
                using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(UpdateContactQuery, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@ip", request.IP);
                        cmd.Parameters.AddWithValue("@browser", request.Browser);
                        cmd.Parameters.AddWithValue("@country", request.Country);
                        cmd.Parameters.AddWithValue("@ContactId", response.ConsumerId);
                        cmd.Parameters.AddWithValue("@UserId", request.UserUniqueId.ToString());
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            response.PinCode = GetPinDigitCode(PIN_CODE_LENGTH);
            DataModel.Xrm.new_contactinvited _invited = new DataModel.Xrm.new_contactinvited(XrmDataContext);
            _invited.new_contactinvited_contactid = response.ConsumerId;
            _invited.new_pincode = response.PinCode;
            DataAccessLayer.ContactInvited contact_invited = new DataAccessLayer.ContactInvited(XrmDataContext);
            contact_invited.Create(_invited);
            XrmDataContext.SaveChanges();
            response.Status = SendSmsPincode(request.phone, response.PinCode);
            return response;
        }
        private eConsumerSignUpDesktopStatus SendSmsPincode(string phone, string PinCode)
        {
            bool IsSent = BusinessLogic.APIs.MobileAPI.PhoneValidator.SendPhoneValidationSms(phone, PinCode);
            return IsSent ? eConsumerSignUpDesktopStatus.OK : eConsumerSignUpDesktopStatus.SmsFaildToSend;
        }
        private string GetPinDigitCode(int DigitNumber)
        {
            Random rnd = new Random();
            string result = string.Empty;
            for (int i = 0; i < DigitNumber; i++)
                result += rnd.Next(10).ToString();
            return result;
        }
        const string ConsumerRegisterPincodeQuery = @"
DECLARE @ContactId uniqueidentifier
SET @ContactId = NULL

SELECT @ContactId = con.ContactId
FROM dbo.ContactExtensionBase con
	INNER JOIN dbo.ContactBase conb on con.ContactId = conb.ContactId
	left join 
	(
		SELECT top 1 c_inv_e.New_ContactInvited_ContactId, c_inv_e.New_PinCode 
		FROM dbo.New_contactinvitedExtensionBase c_inv_e 
			INNER JOIN dbo.New_contactinvitedBase c_inv on c_inv_e.New_contactinvitedId = c_inv.New_contactinvitedId
		WHERE c_inv.CreatedOn >= DATEADD(DAY, -1, GETDATE())
		ORDER BY c_inv.CreatedOn desc
	) contactinvited on contactinvited.New_ContactInvited_ContactId = con.ContactId
WHERE conb.deletionstatecode = 0 AND conb.MobilePhone = @phone AND contactinvited.New_PinCode = @PinCode

print @ContactId
IF(@ContactId IS NOT NULL)	
BEGIN
	UPDATE dbo.ContactExtensionBase
	SET New_DesktopRegisterIsApproved = (1), New_DesktopAppSecondRecordLeft = 1800
	WHERE ContactId = @ContactId
END
SELECT @ContactId";
        public Guid ConsumerRegisterPincode(ConsumerRegisterPincodeRequest request)
        {
            Guid result;
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ConsumerRegisterPincodeQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@phone", request.phone);
                    cmd.Parameters.AddWithValue("@PinCode", request.PinCode);
                    object obj = cmd.ExecuteScalar();

                    result = obj == DBNull.Value ? Guid.Empty : (Guid)obj;
                    conn.Close();
                }
            }
            return result;
        }
        const string GetSecondRecordLeftQuery = @"
SELECT [New_DesktopAppSecondRecordLeft]
  FROM [dbo].[ContactExtensionBase]
  WHERE ContactId = @ContactId";
        public int GetSecondRecordLeft(Guid ContactId)
        {
            int result = 0;
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GetSecondRecordLeftQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@ContactId", ContactId);
                    object obj = cmd.ExecuteScalar();

                    result = obj == DBNull.Value ? 0 : (int)obj;
                    conn.Close();
                }
            }
            return result;
        }

        public void CustomerDisallowesRecording(Guid incidentId, Guid supplierId)
        {
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@incidentId", incidentId);
            parameters.Add("@supplierId", supplierId);
            dal.ExecuteNonQuery(updateCustomerDisallowesRecordingSql, parameters);
        }

        private const string updateCustomerDisallowesRecordingSql =
            @"
update New_incidentaccount
set New_Customerdisallowedrecording = 1
where new_accountid = @supplierId
and new_incidentid = @incidentId
";
    }
}
