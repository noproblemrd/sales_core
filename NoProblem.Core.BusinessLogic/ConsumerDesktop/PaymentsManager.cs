﻿using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Consumer.DesktopApp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ConsumerDesktop
{
    public class PaymentsManager : XrmUserBase
    {
        public PaymentsManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private const string US = "US";        

        public bool AddPayment(CustomerPaymentInDesktopApp payment)
        {
            CreditCardSolek.Solek solek = new CreditCardSolek.Plimus();
            string shopperId = CreateShopper(payment, solek);
            if (String.IsNullOrEmpty(shopperId))
            {
                LogUtils.MyHandle.WriteToLog("Failed to charge customer from desktop app. Failed to create shopperId. contactId = {0}", payment.ContactId);
                return false;
            }
            string transactionNumber, solekAnswer;
            bool ok = ChargeShopper(payment, solek, shopperId, out transactionNumber, out solekAnswer);
            if (ok)
            {
                SaveCharge(payment, shopperId, transactionNumber);
                return true;
            }
            else
            {
                LogUtils.MyHandle.WriteToLog("Failed to charge customer from desktop app. contactId = {0}. solek answer = {1}. shopperId = {2}. amount = {3}", payment.ContactId, solekAnswer, shopperId, payment.AmountDollars);
                return false;
            }
        }

        private void SaveCharge(CustomerPaymentInDesktopApp payment, string shopperId, string transactionNumber)
        {
            DataModel.Xrm.new_customerdesktopappcharge charge = new DataModel.Xrm.new_customerdesktopappcharge(XrmDataContext)
            {
                new_amounttocharge = payment.AmountDollars,
                new_billingaddress = payment.BillingAddress,
                new_billingcity = payment.BillingCity,
                new_billingfirstname = payment.BillingFirstName,
                new_billinglastname = payment.BillingLastName,
                new_billingphone = payment.BillingPhone,
                new_billingstate = payment.BillingState,
                new_billingzipcode = payment.BillingZip,
                new_contactid = payment.ContactId,
                new_last4digits = payment.Last4Digits,
                new_orderid = transactionNumber,
                new_waitingtobechecked = true,
                new_waitstartdate = DateTime.Now,
                new_shopperid = shopperId,
                new_creditcardtype = payment.CreditCardType,
                new_expirationmonth = payment.ExpirationMonth,
                new_expirationyear = payment.ExpirationYear,
                new_useragent = payment.UserAgent,
                new_userip = payment.UserIP,
                new_userhost = payment.UserHost
            };
            DataAccessLayer.CustomerDesktopAppCharge chargeDal = new DataAccessLayer.CustomerDesktopAppCharge(XrmDataContext);
            chargeDal.Create(charge);
            XrmDataContext.SaveChanges();
        }

        private static bool ChargeShopper(CustomerPaymentInDesktopApp payment, CreditCardSolek.Solek solek, string shopperId, out string transactionNumber, out string solekAnswer)
        {
            bool needsAsyncApproval;
            bool ok = solek.RechargeSupplier(null, null, null, payment.AmountDollars, payment.AmountDollars, 1, out solekAnswer, out transactionNumber, payment.Last4Digits, payment.CreditCardType, shopperId, NoProblem.Core.BusinessLogic.CreditCardSolek.PlimusContracts.desktopCustomerContractId, payment.UserIP, payment.UserAgent, payment.UserHost, null, out needsAsyncApproval, null, null, null, null, DataModel.Xrm.new_paymentmethod.ePaymentMethod.CreditCard, null, false);
            return ok;
        }

        private static string CreateShopper(CustomerPaymentInDesktopApp payment, CreditCardSolek.Solek solek)
        {
            string shopperId = solek.CreateShopper(
                new CreditCardSolek.Solek.CreateShopperData()
                {
                    BillingCity = payment.BillingCity,
                    BillingFirstName = payment.BillingFirstName,
                    BillingLastName = payment.BillingLastName,
                    BillingState = payment.BillingState,
                    BillingStreetAddress = payment.BillingAddress,
                    BillingZip = payment.BillingZip,
                    CreditCardType = payment.CreditCardType,
                    EncryptedCardNumber = payment.EncryptedCreditCardNumber,
                    EncryptedCVV = payment.EcryptedCvv,
                    ExpirationMonth = payment.ExpirationMonth.ToString(),
                    ExpirationYear = payment.ExpirationYear,
                    IP = payment.UserIP,
                    ShopperCity = payment.BillingCity,
                    ShopperCompanyName = String.Empty,
                    ShopperEmail = payment.BillingEmail,
                    ShopperFax = String.Empty,
                    ShopperFirstName = payment.BillingFirstName,
                    ShopperLastName = payment.BillingLastName,
                    ShopperPhone = payment.BillingPhone,
                    ShopperState = payment.BillingState,
                    ShopperStreetAddress = payment.BillingAddress,
                    ShopperZip = payment.BillingZip,
                });
            return shopperId;
        }
    }
}
