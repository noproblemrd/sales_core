﻿using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Pusher.DesktopApp.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.ConsumerDesktop
{
    public class PendingChargesChecker : XrmUserBase
    {
        private static Timer timer;
        private const int ONE_MINUTE = 1000 * 60;
        private const int TEN_SECONDS = 1000 * 10;
        private static CreditCardSolek.Plimus plimus = new CreditCardSolek.Plimus();
        private static DataAccessLayer.Generic genericDal = new DataAccessLayer.Generic();
        private DataAccessLayer.CustomerDesktopAppCharge chargeDal;

        private PendingChargesChecker()
            : base(null)
        {
            chargeDal = new DataAccessLayer.CustomerDesktopAppCharge(XrmDataContext);
        }

        public static void OnAppStart()
        {
            timer = new Timer(
                new TimerCallback(CallBack),
                null,
                ONE_MINUTE,
                System.Threading.Timeout.Infinite);
        }

        private static void CallBack(object status)
        {
            try
            {
                PendingChargesChecker instance = new PendingChargesChecker();
                instance.Work();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ConsumerDesktop.PendingChargesChecker.CallBack");
            }
            finally
            {
                timer = new Timer(
                new TimerCallback(CallBack),
                null,
                TEN_SECONDS,
                System.Threading.Timeout.Infinite);
            }
        }

        private void Work()
        {
            PendingPair pair = FindPendingCallCharge();
            while (pair != null)
            {
                CheckCharge(pair);
                pair = FindPendingCallCharge();
            }
        }

        private void CheckCharge(PendingPair pair)
        {
            string orderStatusReason;
            CreditCardSolek.InvoiceDataContainer invoiceDataContainer;
            DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode orderStatus;
            try
            {
                orderStatus = plimus.CheckPendingRecharge(pair.Transaction, out orderStatusReason, out invoiceDataContainer);
            }
            catch
            {
                orderStatus = DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Pending;
                orderStatusReason = null;
                invoiceDataContainer = null;
            }
            if (orderStatus == NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Accepted)
            {
                HandleAccepted(pair, orderStatus, orderStatusReason, invoiceDataContainer);
            }
            else if (orderStatus == NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Cancelled)
            {
                HandleCancelled(pair, orderStatus, orderStatusReason);
            }
            else
            {
                HandleStillPending(pair);
            }
        }

        private void HandleStillPending(PendingPair pair)
        {
            DataAccessLayer.CustomerDesktopAppCharge dal = new DataAccessLayer.CustomerDesktopAppCharge(XrmDataContext);
            DataModel.Xrm.new_customerdesktopappcharge entity = new DataModel.Xrm.new_customerdesktopappcharge();
            entity.new_customerdesktopappchargeid = pair.PendingChargeId;
            entity.new_waitingtobechecked = true;
            entity.new_waitstartdate = DateTime.Now;
            dal.Update(entity);
            XrmDataContext.SaveChanges();
        }

        private void HandleCancelled(PendingPair pair, DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode orderStatus, string orderStatusReason)
        {
            UpdateLeadChargeAfterCheckingDone(pair.PendingChargeId, DataModel.Xrm.new_leadcreditcardcharge.SolekStatus.Cancelled, orderStatusReason, null);
            var charge = (
                    from x in chargeDal.All
                    where x.new_customerdesktopappchargeid == pair.PendingChargeId
                    select new
                    {
                        ContactId = x.new_contactid.Value
                    }
                ).First();
            SendPusherWithResult(charge.ContactId, orderStatus);
        }

        private void HandleAccepted(PendingPair pair, DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode orderStatus, string orderStatusReason, CreditCardSolek.InvoiceDataContainer invoiceDataContainer)
        {
            var charge = (
                    from x in chargeDal.All
                    where x.new_customerdesktopappchargeid == pair.PendingChargeId
                    select new
                    {
                        Amount = x.new_amounttocharge.Value,
                        ContactId = x.new_contactid.Value
                    }
                ).First();
            UpdateLeadChargeAfterCheckingDone(pair.PendingChargeId, DataModel.Xrm.new_leadcreditcardcharge.SolekStatus.Accepted, null, invoiceDataContainer);
            UpdateContactsMinutesLeft(charge.ContactId, charge.Amount);
            SendPusherWithResult(charge.ContactId, orderStatus);
        }

        private void SendPusherWithResult(Guid contactId, DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode orderStatus)
        {
            CustomerPaymentScriptBuilder builder = new CustomerPaymentScriptBuilder();
            string js = builder.BuildPaymentResultDataScript(orderStatus == DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Accepted);
            Pusher.DesktopApp.PusherDesktopAppInjectJsEvent pusherEvent = new Pusher.DesktopApp.PusherDesktopAppInjectJsEvent(contactId.ToString(), js);
            Pusher.PusherEventSender.TriggerEvent(pusherEvent);
        }

        private void UpdateContactsMinutesLeft(Guid contactId, decimal amountPaid)
        {
            DataAccessLayer.Contact contactDal = new DataAccessLayer.Contact(XrmDataContext);
            int secondsLeft = (from c in contactDal.All
                               where c.contactid == contactId
                               select c.new_desktopappsecondrecordleft.HasValue ? c.new_desktopappsecondrecordleft.Value : 0).First();
            DataModel.Xrm.contact contact = new DataModel.Xrm.contact(XrmDataContext);
            contact.contactid = contactId;
            contact.new_desktopappsecondrecordleft = secondsLeft + GetSecondsFromAmountPaid(amountPaid);
            contactDal.Update(contact);
            XrmDataContext.SaveChanges();
        }

        private int GetSecondsFromAmountPaid(decimal amountPaid)
        {
            int rounded = decimal.ToInt32(amountPaid);
            int minutesGranted;
            switch (rounded)
            {
                case 10:
                    minutesGranted = 10;
                    break;
                case 15:
                    minutesGranted = 20;
                    break;
                case 20:
                    minutesGranted = 30;
                    break;
                default:
                    minutesGranted = rounded;
                    break;
            }
            return minutesGranted * 60;
        }

        private void UpdateLeadChargeAfterCheckingDone(
            Guid chargeID,
            DataModel.Xrm.new_leadcreditcardcharge.SolekStatus status,
            string reason,
            CreditCardSolek.InvoiceDataContainer invoiceDataContainer)
        {
            if (invoiceDataContainer == null)
                invoiceDataContainer = new CreditCardSolek.InvoiceDataContainer();
            DataModel.Xrm.new_customerdesktopappcharge entity = new DataModel.Xrm.new_customerdesktopappcharge();
            entity.new_customerdesktopappchargeid = chargeID;
            entity.new_solekstatus = status.ToString();
            if (reason != null && reason.Length > 4000)
                reason = reason.Substring(0, 4000);
            entity.new_failreason = reason;
            entity.new_invoicenumber = invoiceDataContainer.InvoiceNumber;
            entity.new_invoiceurl = invoiceDataContainer.InvoiceUrl;
            entity.new_billingaddress = invoiceDataContainer.BillingAddress1 + " " + invoiceDataContainer.BillingAddress2;
            if (entity.new_billingaddress.Length > 100)
                entity.new_billingaddress = entity.new_billingaddress.Substring(0, 100);
            entity.new_billingcity = invoiceDataContainer.BillingCity;
            entity.new_billingstate = invoiceDataContainer.BillingState;
            entity.new_billingzipcode = invoiceDataContainer.BillingZip;
            entity.new_billingfirstname = invoiceDataContainer.BillingFirstName;
            entity.new_billinglastname = invoiceDataContainer.BillingLastName;
            entity.new_billingphone = invoiceDataContainer.BillingPhone;
            chargeDal.Update(entity);
            XrmDataContext.SaveChanges();
        }

        private class PendingPair
        {
            public Guid PendingChargeId { get; set; }
            public string Transaction { get; set; }
        }

        private PendingPair FindPendingCallCharge()
        {
            var reader = genericDal.ExecuteReader(findSqlString);
            var found = reader.FirstOrDefault();
            if (found == null)
                return null;
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@id", (Guid)found["new_customerdesktopappchargeid"]);
            int rowsUpdated = genericDal.ExecuteNonQuery(updateSqlString, parameters);
            if (rowsUpdated == 0)
                return FindPendingCallCharge();
            else
                return new PendingPair { PendingChargeId = (Guid)found["new_customerdesktopappchargeid"], Transaction = Convert.ToString(found["new_orderid"]) };
        }

        private const string findSqlString =
            @"select top 1 new_customerdesktopappchargeid, new_orderid from new_customerdesktopappcharge where deletionstatecode = 0 and new_waitingtobechecked = 1 and new_waitstartdate < dateadd(second, -10, getdate())";

        private const string updateSqlString =
            @"update new_customerdesktopappcharge set new_waitingtobechecked = 0 where new_customerdesktopappchargeid = @id and new_waitingtobechecked = 1";
    }
}
