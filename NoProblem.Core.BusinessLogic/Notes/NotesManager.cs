﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Notes
{
    public class NotesManager : XrmUserBase
    {
        #region Ctor
        public NotesManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Public Methods

        public List<DataModel.Notes.NoteData> GetNotes(Guid regardingObjectId, DataModel.Notes.NoteType type)
        {
            List<DataModel.Notes.NoteData> notes = null;
            switch (type)
            {
                case NoProblem.Core.DataModel.Notes.NoteType.Case:
                    DataAccessLayer.CaseNote caseNoteDal = new NoProblem.Core.DataAccessLayer.CaseNote(XrmDataContext);
                    notes = caseNoteDal.GetNotesByCaseId(regardingObjectId);
                    break;
                case NoProblem.Core.DataModel.Notes.NoteType.HelpDesk:
                    DataAccessLayer.HdNote hdNoteDal = new NoProblem.Core.DataAccessLayer.HdNote(XrmDataContext);
                    notes = hdNoteDal.GetNotesByEntryId(regardingObjectId);
                    break;
                case NoProblem.Core.DataModel.Notes.NoteType.SupplierPricing:
                    DataAccessLayer.SupplierPricingNote spNoteDal = new NoProblem.Core.DataAccessLayer.SupplierPricingNote(XrmDataContext);
                    notes = spNoteDal.GetNotesByEntryId(regardingObjectId);
                    break;
            }
            foreach (var item in notes)
            {
                item.CreatedOn = FixDateToLocal(item.CreatedOn);
            }
            return notes;
        }

        public Guid CreateNote(DataModel.Notes.NoteData note, DataModel.Notes.NoteType type)
        {
            switch (type)
            {
                case NoProblem.Core.DataModel.Notes.NoteType.Case:
                    return CreateCaseNote(note);
                case NoProblem.Core.DataModel.Notes.NoteType.HelpDesk:
                    return CreateHdNote(note);
                case NoProblem.Core.DataModel.Notes.NoteType.SupplierPricing:
                    return CreateSupplierPricingNote(note);
            }
            return Guid.Empty;
        }
        
        #endregion

        #region Private Methods

        private Guid CreateSupplierPricingNote(NoProblem.Core.DataModel.Notes.NoteData note)
        {
            DataModel.Xrm.new_supplierpricingnote noteXrm = new NoProblem.Core.DataModel.Xrm.new_supplierpricingnote();
            noteXrm.new_file = note.File;
            noteXrm.new_description = note.Description;
            noteXrm.new_userid = note.CreatedById;
            noteXrm.new_supplierpricingid = note.RegargingObjectId;
            DataAccessLayer.SupplierPricingNote dal = new NoProblem.Core.DataAccessLayer.SupplierPricingNote(XrmDataContext);
            dal.Create(noteXrm);
            XrmDataContext.SaveChanges();
            return noteXrm.new_supplierpricingnoteid;
        } 

        private Guid CreateCaseNote(DataModel.Notes.NoteData note)
        {
            DataModel.Xrm.new_casenote noteXrm = new NoProblem.Core.DataModel.Xrm.new_casenote();
            noteXrm.new_file = note.File;
            noteXrm.new_description = note.Description;
            noteXrm.new_userid = note.CreatedById;
            noteXrm.new_incidentid = note.RegargingObjectId;
            DataAccessLayer.CaseNote dal = new NoProblem.Core.DataAccessLayer.CaseNote(XrmDataContext);
            dal.Create(noteXrm);
            XrmDataContext.SaveChanges();
            return noteXrm.new_casenoteid;
        }

        private Guid CreateHdNote(DataModel.Notes.NoteData noteData)
        {
            DataModel.Xrm.new_helpdesknote note = new NoProblem.Core.DataModel.Xrm.new_helpdesknote();
            note.new_description = noteData.Description;
            note.new_helpdeskentryid = noteData.RegargingObjectId;
            note.new_userid = noteData.CreatedById;
            note.new_file = noteData.File;
            DataAccessLayer.HdNote dal = new NoProblem.Core.DataAccessLayer.HdNote(XrmDataContext);
            dal.Create(note);
            XrmDataContext.SaveChanges();
            return note.new_helpdesknoteid;
        } 

        #endregion
    }
}
