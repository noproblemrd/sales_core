﻿using NoProblem.Core.BusinessLogic.PushNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.OneCallUtilities
{
    public class NotificationServiceManager
    {
        NotificationServiceManagerRequest pushRequest;
        public NotificationServiceManager(NotificationServiceManagerRequest pushRequest)
        {
            this.pushRequest = pushRequest;
        }
        public bool SendPushNotification()
        {
            IPushServer pushServer = PushServerFactory.GetPushServer(pushRequest.OS, pushRequest.isDebug);

            NpPushResult pushResult = pushServer.Send(pushRequest.GetPushRequest());
            return (pushResult.IsSuccess);
             //   SetSentNotification(ad.AccountId);
        }
    }
    public class NotificationServiceManagerRequest
    {
        public string DeviceId { get; set; }
        public bool isDebug { get; set; }
       // public Dictionary<string, string> Data { get; set; }
        public List<KeyValuePairString> data { get; set; }
     //   public KeyValuePair<string, string> data { get; set; }
        public ePushTypes PushType { get; set; }
        public string OS { get; set; }
        public PushRequest GetPushRequest()
        {
            PushRequest pushRequest = new PushRequest();
            Dictionary<string, string> newData = new Dictionary<string, string>();
            foreach (KeyValuePairString kvp in data)
                newData.Add(kvp.Key, kvp.Value);
            pushRequest.Data = newData;
            pushRequest.DeviceId = this.DeviceId;
            pushRequest.PushType = this.PushType;
            return pushRequest;
        }
        public class KeyValuePairString
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }
    }
}
