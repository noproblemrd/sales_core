﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.OnecallUtilities;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.OneCallUtilities
{
    internal class AdvertiserLoader : XrmUserBase
    {
        #region Ctor
        internal AdvertiserLoader(NoProblem.Core.DataModel.Xrm.DataContext xrmDataContext, LoadAdvertisersRequest request)
            : base(xrmDataContext)
        {
            this.request = request;
            GetOriginId();
            GetRegionId();
            LoadFieldsWithRequest(request);
        }

        #endregion

        #region Fields

        LoadAdvertisersRequest request;
        int suppliersToCreate;
        string namePrefix;
        int nameIndex;
        int trueNumber;
        int virtualNumber;
        Guid expertiseId;
        int expertiseIndex;
        Guid originId;
        Guid regionId;

        #endregion

        #region Internal Methods

        internal LoadAdvertisersResponse LoadAdvertisers()
        {
            for (int i = 0; i < suppliersToCreate; i++, nameIndex++, trueNumber++, virtualNumber++)
            {
                if ((i % 4 == 0 && request.AdvertisersType == eAdvertisersType.Auction) || i == 0)
                {
                    expertiseId = CreateNewExpertise();
                }
                DML.Xrm.account account = new DML.Xrm.account();

                account.new_cashbalance = 1000000;
                account.new_availablecashbalance = 1000000;

                account.new_incidentprice = new Random().Next(1, 100);

                account.new_incidentpricechangeable = true;

                account.name = namePrefix + "_" + nameIndex;

                account.new_status = (int)DML.Xrm.account.SupplierStatus.Approved;

                account.new_recordcalls = true;

                account.telephone1 = trueNumber.ToString();

                account.new_stageinregistration = (int)DataModel.Xrm.account.StageInRegistration.PAID;

                XrmDataContext.AddToaccounts(account);
                XrmDataContext.SaveChanges();

                DML.Xrm.new_accountexpertise accExpertise = new DML.Xrm.new_accountexpertise();
                accExpertise.new_primaryexpertiseid = expertiseId;
                accExpertise.new_accountid = account.accountid;
                XrmDataContext.AddTonew_accountexpertises(accExpertise);
                XrmDataContext.SaveChanges();

                if (request.AdvertisersType == eAdvertisersType.DirectNumbers)
                {
                    DML.Xrm.new_directnumber directNumber = new DML.Xrm.new_directnumber();
                    directNumber.new_number = virtualNumber.ToString();
                    directNumber.new_originid = originId;
                    directNumber.new_accountexpertiseid = accExpertise.new_accountexpertiseid;
                    XrmDataContext.AddTonew_directnumbers(directNumber);
                }

                for (int j = 1; j <= 7; j++)
                {
                    DML.Xrm.new_availability av = new DML.Xrm.new_availability();
                    av.new_day = j;
                    av.new_from = "00:00";
                    av.new_to = "23:59";
                    av.new_accountid = account.accountid;
                    XrmDataContext.AddTonew_availabilitynew_availabilities(av);
                }

                DML.Xrm.new_regionaccountexpertise regionAccount = new DML.Xrm.new_regionaccountexpertise();
                regionAccount.new_accountid = account.accountid;
                regionAccount.new_regionstate = (int)DML.Xrm.new_regionaccountexpertise.RegionState.Working;
                regionAccount.new_regionid = regionId;
                XrmDataContext.AddTonew_regionaccountexpertises(regionAccount);

                XrmDataContext.SaveChanges();
            }

            LoadAdvertisersResponse response = new LoadAdvertisersResponse();
            return response;
        }

        #endregion

        #region Private Methods

        private void GetOriginId()
        {
            DML.Xrm.new_origin origin = XrmDataContext.new_origins.FirstOrDefault();
            if (origin != null)
            {
                originId = origin.new_originid;
            }
            else
            {
                throw new Exception("No origins in the system");
            }
        }

        private void GetRegionId()
        {
            DML.Xrm.new_region reg = XrmDataContext.new_regions.SingleOrDefault(x => x.new_name == "World");
            if (reg != null)
            {
                regionId = reg.new_regionid;
            }
            else
            {
                throw new Exception("Please create a 'World' region");
            }
        }

        private Guid CreateNewExpertise()
        {
            DML.Xrm.new_primaryexpertise expertise = new DML.Xrm.new_primaryexpertise();
            expertise.new_name = "expertise_" + expertiseIndex;
            expertise.new_code = expertiseIndex.ToString();
            expertiseIndex++;
            expertise.new_isauction = true;
            XrmDataContext.AddTonew_primaryexpertises(expertise);
            XrmDataContext.SaveChanges();
            return expertise.new_primaryexpertiseid;
        }

        private void LoadFieldsWithRequest(LoadAdvertisersRequest request)
        {
            suppliersToCreate = request.NumberOfAdvertisersToCreate;
            namePrefix = request.NamesPrefix;
            nameIndex = request.FirstIndexForNames;
            trueNumber = int.Parse(request.TruePhoneNumbersStart);
            virtualNumber = int.Parse(request.VirtualNumbersStart);
            expertiseIndex = request.ExpertiseIndex;
        }

        #endregion
    }
}
