﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using Effect.Crm.SDKProviders;
using Effect.Crm.Logs;
using Microsoft.Crm.Sdk;
using Effect.Crm.Convertor;
using NoProblem.Core.DataModel.OnecallUtilities;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Asterisk;
using NoProblem.Core.DataModel.Jona;

namespace NoProblem.Core.BusinessLogic.OneCallUtilities
{
    public class OneCallUtilities : XrmUserBase
    {
        #region Ctor
        public OneCallUtilities(DML.Xrm.DataContext xrmDataContext) :
            base(xrmDataContext)
        {

        }
        #endregion

        public LoadAdvertisersResponse LoadAdvertisers(LoadAdvertisersRequest request)
        {
            AdvertiserLoader loader = new AdvertiserLoader(XrmDataContext, request);
            LoadAdvertisersResponse response = loader.LoadAdvertisers();
            return response;
        }

        public void ClearCache()
        {
            DataAccessLayer.CacheManager.Instance.ClearCache();
        }

        public string stamTest()
        {
            return "You are: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            //List<string> s = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(@"[""hello""]");

            //BusinessLogic.Balance.TenLeadsCashBackGiver giver = new Balance.TenLeadsCashBackGiver(null);
            //giver.OnLeadCharge(new Guid("9E63EE28-E56A-48BB-99D6-D720A526D095"));
            //DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            //DataModel.Xrm.incident incident = incidentDal.Retrieve(new Guid("CC73151D-5E9B-4B2C-AE31-EBCA726CE38F"));
            
            //DataAccessLayer.PrimaryExpertise headingDal = new DataAccessLayer.PrimaryExpertise(XrmDataContext);
            //decimal? minBrokerBid = incident.new_new_primaryexpertise_incident.new_minimumbidpriceforbrokers;

            //if (!minBrokerBid.HasValue)
            //    minBrokerBid = 0;
            
            //NoProblem.Core.DataModel.Dapaz.PpaOnlineSync.AccountSyncData d = new DataModel.Dapaz.PpaOnlineSync.AccountSyncData();
            //var lst = new System.Collections.Generic.List<DataModel.Dapaz.PpaOnlineSync.AccountSyncData.CategoryData>();
            //var numbers1 = new List<DataModel.Dapaz.PpaOnlineSync.AccountSyncData.VirtualNumberData>();
            //numbers1.Add(new DML.Dapaz.PpaOnlineSync.AccountSyncData.VirtualNumberData()
            //{
            //    Number = "0723212435",
            //    OriginCode = "dSite"
            //});
            //numbers1.Add(new DML.Dapaz.PpaOnlineSync.AccountSyncData.VirtualNumberData()
            //{
            //    Number = "0723212440",
            //    OriginCode = "mobileSite"
            //});
            //lst.Add(
            //    new DataModel.Dapaz.PpaOnlineSync.AccountSyncData.CategoryData()
            //    {
            //        Code = "35840",
            //        CallPrice = new decimal(4.5),
            //        IsActive = true,
            //        VirtualNumbers = numbers1
            //    });

            //var numbers2 = new List<DataModel.Dapaz.PpaOnlineSync.AccountSyncData.VirtualNumberData>();
            //numbers2.Add(new DML.Dapaz.PpaOnlineSync.AccountSyncData.VirtualNumberData()
            //{
            //    Number = "0723212400",
            //    OriginCode = "dSite"
            //});
            //numbers2.Add(new DML.Dapaz.PpaOnlineSync.AccountSyncData.VirtualNumberData()
            //{
            //    Number = "0723212401",
            //    OriginCode = "mobileSite"
            //});
            //lst.Add(
            //    new DataModel.Dapaz.PpaOnlineSync.AccountSyncData.CategoryData()
            //    {
            //        Code = "13380",
            //        CallPrice = 6,
            //        VirtualNumbers = numbers2,
            //        IsActive = true
            //    });
            //lst.Add(
            //    new DataModel.Dapaz.PpaOnlineSync.AccountSyncData.CategoryData()
            //    {
            //        Code = "13381",
            //        IsActive = false
            //    });

            //d.Categories = lst;
            //d.CountCalls = 14;
            //d.AccountNumberNP = "34028";
            //d.AccountNumberDapaz = "80136418";

            //List<DataModel.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData> mekLst = new List<DML.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData>();

            //mekLst.Add(
            //    new DML.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData()
            //    {
            //        IsActive = true,
            //        MekomonId = "16"
            //    });

            //mekLst.Add(
            //    new DML.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData()
            //    {
            //        IsActive = true,
            //        MekomonId = "31"
            //    });

            //mekLst.Add(
            //    new DML.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData()
            //    {
            //        IsActive = true,
            //        MekomonId = "27"
            //    });

            //mekLst.Add(
            //    new DML.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData()
            //    {
            //        IsActive = false,
            //        MekomonId = "69"
            //    });            

            //d.Mekomonim = mekLst;

            //d.LogoUrl = "http://gallery.ppcnp.com/Gallery/images/logos/b827db4d-5ea8-44b4-8330-1cbf04f7905d.jpg";
            //d.Website = "http://www.noproblem.co.il/professional/לירן-_שירותי_ניקיון_ואחזקה.aspx";
            //d.Email = "stamemail@gmail.com";

            
            //d.SundayFrom = "00:00";
            //d.SundayTo = "23:59";
            //d.MondayFrom = "00:00";
            //d.MondayTo = "23:59";
            //d.TuesdayFrom = "00:00";
            //d.TuesdayTo = "23:59";
            //d.WednesdayFrom = "09:00";
            //d.WednesdayTo = "18:00";

            //string s = Newtonsoft.Json.JsonConvert.SerializeObject(d);
            //return s;
        }
    }
}
