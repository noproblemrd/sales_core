﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: Effect.Onecall.BusinessLogic.CrmServiceUserBase
//  File: CrmServiceUserBase.cs
//  Description: Base class for all classes that use CrmService
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using Effect.Crm.SDKProviders;
using Effect.Crm.SDKProviders.CrmServiceSdk;

namespace NoProblem.Core.BusinessLogic
{
    public class CrmServiceUserBase
    {
        private CrmService _crmService;
        public CrmService CrmService
        {
            get
            {
                if (_crmService == null)
                {
                    _crmService = ProviderUtils.GetCrmService();
                }
                return _crmService;
            }
            private set { _crmService = value; }
        }

         public CrmServiceUserBase(CrmService crmService)
        {
            if (crmService != null)
            {
                CrmService = crmService;
            }
            //else
            //{
            //    CrmService = ProviderUtils.GetCrmService();
            //}      
        }
    }
}
