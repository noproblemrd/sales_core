﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: Effect.Onecall.BusinessLogic.XrmUserBase
//  File: XrmUserBase.cs
//  Description: Base class for all classes that use XrmDataContext.
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using DML = NoProblem.Core.DataModel;
using System;
using Microsoft.Crm.SdkTypeProxy;
using System.Text.RegularExpressions;
using System.Linq;
using System.ServiceModel;

namespace NoProblem.Core.BusinessLogic
{
    public class XrmUserBase
    {
        private DML.Xrm.DataContext _xrmDataContext;
        public DML.Xrm.DataContext XrmDataContext
        {
            get
            {
                if (_xrmDataContext == null)
                {
                    _xrmDataContext = DataModel.XrmDataContext.Create();
                }
                return _xrmDataContext;
            }
            private set { _xrmDataContext = value; }
        }

        public XrmUserBase(DML.Xrm.DataContext xrmContext)
        {
            if (xrmContext != null)
            {
                XrmDataContext = xrmContext;
            }
        }

        protected const string DATETIMEFORMAT = "yyyy-MM-dd HH:mm:ss";
        protected const string DATETIMEFORMATNOHOUR = "yyyy-MM-dd";

        /// <summary>
        /// Calculates the local datetime of the publisher.
        /// </summary>
        /// <param name="utcDate"></param>
        /// <returns></returns>
        protected DateTime FixDateToLocal(DateTime utcDate)
        {
            DataAccessLayer.ConfigurationSettings configDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            //int publisherTimeZone = int.Parse(configDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE));
            //DateTime localDate = FixDateToLocal(utcDate, publisherTimeZone);
            string timeZoneStandardName = configDAL.GetPublisherTimeZoneStandardName();
            DateTime localDate = FixDateToLocal(utcDate, timeZoneStandardName);
            return localDate;
        }

        /// <summary>
        /// Calculates the local datetime in the given time zone.
        /// </summary>
        /// <param name="utcDate"></param>
        /// <param name="timeZone"></param>
        /// <returns></returns>
        protected DateTime FixDateToLocal(DateTime utcDate, int timeZone)
        {
            //DateTime localDate = Utils.ConvertUtils.GetLocalFromUtc(XrmDataContext, utcDate, timeZone);
            DataAccessLayer.ConfigurationSettings configDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string timeZoneStandardName = configDAL.GetTimeZoneStandardName(timeZone);
            DateTime localDate = FixDateToLocal(utcDate, timeZoneStandardName);
            return localDate;
        }

        protected DateTime FixDateToLocal(DateTime utcDate, string timeZoneStandardName)
        {
            DateTime localDate = Utils.ConvertUtils.GetLocalFromUtc(utcDate, timeZoneStandardName);
            return localDate;
        }

        protected DateTime FixDateToUtcFromLocal(DateTime localTime)
        {
            DataAccessLayer.ConfigurationSettings configDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            //int publisherTimeZone = int.Parse(configDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE));
            string timeZoneStandardName = configDAL.GetPublisherTimeZoneStandardName();
            DateTime utcTime = FixDateToUtcFromLocal(localTime, timeZoneStandardName);
            return utcTime;
        }

        protected DateTime FixDateToUtcFromLocal(DateTime localTime, int timeZone)
        {
            //DateTime utcDate = Utils.ConvertUtils.GetUtcFromLocalTime(XrmDataContext, localTime, timeZone);
            DataAccessLayer.ConfigurationSettings configDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string timeZoneStandardName = configDAL.GetTimeZoneStandardName(timeZone);
            DateTime utcTime = FixDateToUtcFromLocal(localTime, timeZoneStandardName);
            return utcTime;
        }

        protected DateTime FixDateToUtcFromLocal(DateTime localTime, string timeZoneStandardName)
        {
            DateTime utcTime = Utils.ConvertUtils.GetUtcFromLocal(localTime, timeZoneStandardName);
            return utcTime;
        }

        protected virtual void FixDatesToUtcFullDays(ref DateTime fromDate, ref DateTime toDate)
        {
            DataAccessLayer.ConfigurationSettings configDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            //int publisherTimeZone = int.Parse(configDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE));
            string timeZoneStandardName = configDAL.GetPublisherTimeZoneStandardName();
            DateTime fromOnlyDate = fromDate.Date;
            DateTime toDateOnly = toDate.Date.AddDays(1);
            //fromDate = Utils.ConvertUtils.GetUtcFromLocalTime(XrmDataContext, fromOnlyDate, publisherTimeZone);
            //toDate = Utils.ConvertUtils.GetUtcFromLocalTime(XrmDataContext, toDateOnly, publisherTimeZone);
            fromDate = Utils.ConvertUtils.GetUtcFromLocal(fromOnlyDate, timeZoneStandardName);
            toDate = Utils.ConvertUtils.GetUtcFromLocal(toDateOnly, timeZoneStandardName);
        }

        protected virtual void FixDatesToUtcFullDaysNotIncludingToDate(ref DateTime fromDate, ref DateTime toDate)
        {
            DataAccessLayer.ConfigurationSettings configDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            //int publisherTimeZone = int.Parse(configDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE));
            string timeZoneStandardName = configDAL.GetPublisherTimeZoneStandardName();
            DateTime fromOnlyDate = fromDate.Date;
            DateTime toDateOnly = toDate.Date;
            //fromDate = Utils.ConvertUtils.GetUtcFromLocalTime(XrmDataContext, fromOnlyDate, publisherTimeZone);
            //toDate = Utils.ConvertUtils.GetUtcFromLocalTime(XrmDataContext, toDateOnly, publisherTimeZone);
            fromDate = Utils.ConvertUtils.GetUtcFromLocal(fromOnlyDate, timeZoneStandardName);
            toDate = Utils.ConvertUtils.GetUtcFromLocal(toDateOnly, timeZoneStandardName);
        }

        protected void FixDatesToFullDays(ref DateTime fromDate, ref DateTime toDate)
        {
            fromDate = fromDate.Date;
            toDate = toDate.Date.AddDays(1);
        }        
    }
}
