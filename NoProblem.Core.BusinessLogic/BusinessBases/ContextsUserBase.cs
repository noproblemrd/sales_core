﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Class: Effect.Onecall.BusinessLogic.ContextsUserBase
//  File: ContextsUserBase.cs
//  Description: Base class for all classes that use XrmDataContext and CrmService
//  Author: Alain Adler
//  Company: Onecall
//  CreatedOn: 21/11/10
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using Effect.Crm.SDKProviders;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic
{
    public class ContextsUserBase : XrmUserBase
    {
        private CrmService _crmService;
        public CrmService CrmService
        {
            get
            {
                if (_crmService == null)
                {
                    _crmService = ProviderUtils.GetCrmService();
                }
                return _crmService;
            }
            set
            {
                _crmService = value;
            }
        }

        public ContextsUserBase(DML.Xrm.DataContext xrmDataContext, CrmService crmService)
            : base(xrmDataContext)
        {
            if (crmService != null)
            {
                CrmService = crmService;
            }
        }
    }
}
