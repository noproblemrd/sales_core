﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Regions;
using System.Xml.Linq;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using System.Data.SqlClient;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Regions
{
    public class RegionsManager : XrmUserBase
    {
        #region Ctor
        public RegionsManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        public List<DML.Regions.RegionCodeNamePair> GetRegionsByParentCode(string parentCode)
        {
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            IEnumerable<DML.Xrm.new_region> regions = null;
            if (string.IsNullOrEmpty(parentCode))
            {
                regions = regionDal.GetRegionsByLevel(1);
            }
            else
            {
                DML.Xrm.new_region parent = regionDal.GetRegionByCode(parentCode);
                if (parent == null)
                {
                    throw new Exception("Invalid parentCode. Send null or empty string for first level, or a valid code for child regions.");
                }
                regions = regionDal.GetChildren(parent.new_regionid);
            }
            List<DML.Regions.RegionCodeNamePair> retVal = new List<RegionCodeNamePair>();
            foreach (var reg in regions)
            {
                RegionCodeNamePair pair = new RegionCodeNamePair();
                pair.Code = reg.new_code;
                pair.Name = reg.new_name;
                retVal.Add(pair);
            }
            return retVal;
        }

        public void UpdateSupplierRegionsInTree(DML.Regions.UpdateSupplierRegionsRequest request)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("UpdateSupplierRegionsInTree started. supplierId = ").Append(request.SupplierId.ToString()).Append(" isFromAAR = ").AppendLine(request.IsFromAAR.ToString());
            builder.AppendLine("regions list:");
            foreach (var item in request.RegionDataList)
            {
                builder.Append("    regionId = ").Append(item.RegionId.ToString()).Append(" isDirty = ").Append(item.IsDirty).Append(" level = ").Append(item.Level).Append(" state = ").AppendLine(item.RegionState.ToString());
            }
            LogUtils.MyHandle.WriteToLog(builder.ToString());
            string oldMekomonim = GetCurrentMekomonim(request.SupplierId);
            RegionsTreeManager treeManager = new RegionsTreeManager(XrmDataContext);
            treeManager.UpdateSupplierRegions(request);
            Dapaz.PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(request.SupplierId);
            inserter.InsertToStats();
            Dapaz.PotentialStats.SupplierMekomonimUpdater updater = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.SupplierMekomonimUpdater(request.SupplierId, null, oldMekomonim);
            updater.UpdateMekomonim(false);
            if (Dapaz.PpaOnlineSync.SyncConfiguration.IsEnabled)
            {
                NoProblem.Core.BusinessLogic.Dapaz.PpaOnlineSync.Syncher syncher = new NoProblem.Core.BusinessLogic.Dapaz.PpaOnlineSync.Syncher(request.SupplierId, oldMekomonim);
                syncher.Start();
            }
        }

        private string GetCurrentMekomonim(Guid supplierId)
        {
            string oldMekomonim = null;
            if (Dapaz.PpaOnlineSync.SyncConfiguration.IsEnabled)
            {
                DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
                var supplier = accountRepositoryDal.Retrieve(supplierId);
                oldMekomonim = supplier.new_mekomonim;
            }
            return oldMekomonim;
        }

        public DML.Regions.GetSupplierRegionsResponse GetSupplierRegionsInTree(DML.Regions.GetSupplierRegionsRequest request)
        {
            RegionsTreeManager treeManager = new RegionsTreeManager(XrmDataContext);
            return treeManager.GetSupplierRegions(request.SupplierId, request.ParentRegionId);
        }

        public GetRegionsInLevelByAncestorResponse GetRegionsInLevelByAncestor(GetRegionsInLevelByAncestorRequest request)
        {
            DataAccessLayer.Region regionDAL = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DML.Xrm.new_region ancestor = regionDAL.Retrieve(request.AncestorId);
            int currentLevel = ancestor.new_level.Value;
            List<DML.Xrm.new_region> currentRegions = new List<NoProblem.Core.DataModel.Xrm.new_region>();
            currentRegions.Add(ancestor);
            while (currentLevel < request.Level)
            {
                IEnumerable<DML.Xrm.new_region> children = GetNextLevelRegions(currentRegions, regionDAL);
                currentRegions = children.ToList<DML.Xrm.new_region>();
                currentLevel++;
            }
            List<DML.Regions.RegionData> regionDataList = new List<DML.Regions.RegionData>();
            foreach (DML.Xrm.new_region region in currentRegions)
            {
                DML.Regions.RegionData data = new RegionData();
                data.RegionName = region.new_name;
                data.RegionId = region.new_regionid;
                regionDataList.Add(data);
            }
            GetRegionsInLevelByAncestorResponse response = new GetRegionsInLevelByAncestorResponse();
            response.RegionDataList = regionDataList;
            return response;
        }

        public DML.Regions.GetSupplierRegionsSearchResponse GetSupplierRegionsSearchInTree(GetSupplierRegionsSearchRequest request)
        {
            RegionsTreeManager treeManager = new RegionsTreeManager(XrmDataContext);
            return treeManager.GetSupplierRegionsSearch(request.SupplierId, request.RegionName);
        }

        public bool IsSupplierWorksInRegion(DML.Xrm.account supplier, DML.SqlHelper.RegionMinData region, IEnumerable<DML.SqlHelper.RegionMinData> regionParents, DataAccessLayer.RegionAccount regionAccountDal)
        {
            bool retVal = true;
            bool remove = false;
            bool parentWithNotWorkingChildFound = false;
            bool found = false;
            foreach (var regAcc in supplier.new_account_new_regionaccountexpertise)
            {
                if (regAcc.new_regionid.HasValue == false)
                {
                    continue;
                }
                DML.SqlHelper.RegionMinData parentFound = regionParents.FirstOrDefault(x => x.Id == regAcc.new_regionid.Value);
                if (regAcc.new_regionid.Value == region.Id || parentFound != null)
                {
                    if (regAcc.new_regionstate == (int)DML.Xrm.new_regionaccountexpertise.RegionState.NotWorking)
                    {
                        remove = true;
                        break;
                    }
                    else
                    {
                        if (parentFound == null)
                        {
                            if (regAcc.new_regionstate == (int)DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild)
                            {
                                remove = true;
                                break;
                            }
                            else
                            {
                                found = true;
                                break;
                            }
                        }
                        else
                        {
                            if (regAcc.new_regionstate == (int)DML.Xrm.new_regionaccountexpertise.RegionState.Working)
                            {
                                found = true;
                                break;
                            }
                            else
                            {
                                parentWithNotWorkingChildFound = true;
                            }
                        }
                    }
                }
            }
            if ((!found && !parentWithNotWorkingChildFound) || remove)
            {
                retVal = false;
            }
            return retVal;
        }

        public bool IsSupplierWorksInRegion(Guid supplierId, Guid regionId)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(supplierId);
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var region = regionDal.GetRegionMinDataById(regionId);
            //DML.Xrm.new_region region = regionDal.Retrieve(regionId);
            IEnumerable<DML.SqlHelper.RegionMinData> parents = regionDal.GetParents(region);
            return IsSupplierWorksInRegion(supplier, region, parents, new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext));
        }

        public List<NoProblem.Core.DataModel.Xrm.new_region> GetSupplierWorkingRegions(NoProblem.Core.DataModel.Xrm.account supplier)
        {
            List<DML.Xrm.new_region> workingRegions = new List<NoProblem.Core.DataModel.Xrm.new_region>();
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            IEnumerable<DML.Xrm.new_region> level1 = regionDal.GetRegionsByLevel(1);
            AddWorkingRegionsToList(level1, supplier.accountid, workingRegions, regionDal, new DataAccessLayer.RegionAccount(XrmDataContext));
            return workingRegions;
        }
        public List<NoProblem.Core.DataModel.Regions.RegionData> GetAllRegionByParentAndLevel(int LevelToGet, Guid RegionParentId)
        {
            List<NoProblem.Core.DataModel.Regions.RegionData> list = new List<NoProblem.Core.DataModel.Regions.RegionData>();
            string command = "EXEC [dbo].[GetAllRegionByParentAndLevel] @LevelToGet, @RegionParentId";
            string connectionString = NoProblem.Core.DataAccessLayer.Generic.GetConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@LevelToGet", LevelToGet);
                    cmd.Parameters.AddWithValue("@RegionParentId", RegionParentId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        NoProblem.Core.DataModel.Regions.RegionData data = new NoProblem.Core.DataModel.Regions.RegionData();
                    //    data.Code = reader["new_code"] == DBNull.Value ? string.Empty : (string)reader["new_code"];
                    //    data.EnglishName = reader["new_englishname"] == DBNull.Value ? string.Empty : (string)reader["new_englishname"];
                        data.RegionId = (Guid)reader["new_regionid"];
                   //     data.Level = reader["New_Level"] == DBNull.Value ? -1 : (int)reader["New_Level"];
                        data.RegionName = reader["new_name"] == DBNull.Value ? string.Empty : (string)reader["new_name"];
                        list.Add(data);
                    }
                    conn.Close();
                }
            }
            return list;

        }
        public List<NoProblem.Core.DataModel.Regions.RegionData> GetAllRegionByParentAndLevel(int LevelToGet)
        {
            List<NoProblem.Core.DataModel.Regions.RegionData> list = new List<NoProblem.Core.DataModel.Regions.RegionData>();
            string command = @"select r.new_name, r.new_regionid
                                from dbo.New_regionExtensionBase r with (nolock)
	                                inner join dbo.New_regionBase rb  with (nolock) on r.New_regionId = rb.New_regionId
                                where r.new_level = @level 
	                                and rb.deletionstatecode = 0";
            string connectionString = NoProblem.Core.DataAccessLayer.Generic.GetConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@level", LevelToGet);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        NoProblem.Core.DataModel.Regions.RegionData data = new NoProblem.Core.DataModel.Regions.RegionData();
                        //    data.Code = reader["new_code"] == DBNull.Value ? string.Empty : (string)reader["new_code"];
                        //    data.EnglishName = reader["new_englishname"] == DBNull.Value ? string.Empty : (string)reader["new_englishname"];
                        data.RegionId = (Guid)reader["new_regionid"];
                        //     data.Level = reader["New_Level"] == DBNull.Value ? -1 : (int)reader["New_Level"];
                        data.RegionName = reader["new_name"] == DBNull.Value ? string.Empty : (string)reader["new_name"];
                        list.Add(data);
                    }
                    conn.Close();
                }
            }
            return list;

        }

        #endregion

        #region Private Methods

        private void AddWorkingRegionsToList(IEnumerable<DML.Xrm.new_region> regions, Guid supplierId, List<DML.Xrm.new_region> workingRegions, DataAccessLayer.Region regionDal, DataAccessLayer.RegionAccount regionAccountDal)
        {
            foreach (var reg in regions)
            {
                bool works = IsSupplierWorksInRegion(supplierId, reg.new_regionid);
                if (works)
                {
                    workingRegions.Add(reg);
                }
                else
                {
                    DML.Xrm.new_regionaccountexpertise regAcc = regionAccountDal.GetByRegionIdAndAccountId(reg.new_regionid, supplierId);
                    if (regAcc != null && regAcc.new_regionstate == (int)DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild)
                    {
                        IEnumerable<DML.Xrm.new_region> children = regionDal.GetChildren(reg.new_regionid);
                        AddWorkingRegionsToList(children, supplierId, workingRegions, regionDal, regionAccountDal);
                    }
                }
            }
        }

        private IEnumerable<DML.Xrm.new_region> GetNextLevelRegions(IEnumerable<DML.Xrm.new_region> parentRegions, DataAccessLayer.Region regionDAL)
        {
            List<DML.Xrm.new_region> foundRegions = new List<NoProblem.Core.DataModel.Xrm.new_region>();
            foreach (DML.Xrm.new_region region in parentRegions)
            {
                IEnumerable<DML.Xrm.new_region> children = regionDAL.GetChildren(region.new_regionid);
                foundRegions.AddRange(children);
            }
            return foundRegions;
        }

        #endregion

        #region LoadRegionsFromXml

        public void LoadRegoinFromXml(string xml)
        {
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.SystemUser systemUserAccess = new NoProblem.Core.DataAccessLayer.SystemUser(XrmDataContext);
            DML.Xrm.systemuser currentUser = systemUserAccess.GetCurrentUser();

            XDocument xdoc = XDocument.Load(xml);
            foreach (XElement elem in xdoc.Elements().Elements("hood"))
            {
                string code = elem.Attribute("id").Value;
                string _name = elem.Element("name").Value;
                string alt_name = (elem.Element("alternatenames").HasElements) ? elem.Element("alternatenames").Element("alternatename").Value : string.Empty;
                string municipality = elem.Element("municipality").Value;
                string county = elem.Element("county").Value;
                string region = elem.Element("region").Value;
                string polygons = elem.Element("geometry").Value;
                List<KeyValuePair<decimal, decimal>> list = GetPolygons(polygons);
                List<string> zipList = new List<string>();
                if (elem.Element("postalcodes").HasElements)
                {
                    foreach (XElement xel in elem.Element("postalcodes").Elements("code"))
                    {
                        zipList.Add(xel.Value);
                    }
                }

                Guid level1Id = dal.GetRegionByNameWithParent(region, Guid.Empty);
                if (level1Id == Guid.Empty)
                {
                    DynamicEntity entity = CreateDynamicRegion(region, 1, Guid.Empty, code + "001");
                    level1Id = dal.AddRecordToCRM(entity, currentUser);
                }

                Guid level2Id = dal.GetRegionByNameWithParent(county, level1Id);
                if (level2Id == Guid.Empty)
                {
                    DynamicEntity entity = CreateDynamicRegion(county, 2, level1Id, code + "002");
                    level2Id = dal.AddRecordToCRM(entity, currentUser);
                }

                Guid level3Id = dal.GetRegionByNameWithParent(municipality, level2Id);
                if (level3Id == Guid.Empty)
                {
                    DynamicEntity entity = CreateDynamicRegion(municipality, 3, level2Id, code + "003");
                    level3Id = dal.AddRecordToCRM(entity, currentUser);
                }

                Guid hoodId = dal.GetRegionIdByCode(code + "004");
                if (hoodId == Guid.Empty)
                {
                    DynamicEntity hoodEntity = CreateDynamicRegion(_name, 4, level3Id, code + "004");
                    hoodId = dal.AddRecordToCRM(hoodEntity, currentUser);

                    foreach (var zip in zipList)
                    {
                        var zipEntity = CreateDynamicZip(zip, hoodId);
                        XrmDataContext.AddTonew_zipcodes(zipEntity);
                    }
                    XrmDataContext.SaveChanges();

                    foreach (var coor in list)
                    {
                        DynamicEntity coorEntity = CreateDinamicCoordinate(coor.Key, coor.Value, hoodId);
                        dal.AddRecordToCRM(coorEntity, currentUser);
                    }

                }
            }
        }

        private DynamicEntity CreateDinamicCoordinate(decimal longitude, decimal latitude, Guid hoodId)
        {
            Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity dynamicRegAcc =
               Effect.Crm.Convertor.ConvertorCRMParams.GetNewDynamicEntity("new_coordinate");
            CrmDecimal longi = new CrmDecimal();
            longi.IsNull = false;
            longi.Value = longitude;
            Utils.DynamicEntityUtils.SetPropertyValue(dynamicRegAcc, "new_longitude",
                longi, typeof(CrmDecimalProperty));
            //Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
            //    dynamicRegAcc,
            //    "new_longitude",
            //    //Effect.Crm.Convertor.ConvertorCRMParams.GetCrmDecimal(longitude, false),
            //    longi,
            //    typeof(Effect.Crm.SDKProviders.CrmServiceSdk.CrmDecimalProperty)
            //    );
            CrmDecimal latit = new CrmDecimal();
            latit.IsNull = false;
            latit.Value = latitude;
            Utils.DynamicEntityUtils.SetPropertyValue(dynamicRegAcc, "new_latitude",
                latit, typeof(CrmDecimalProperty));
            //Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
            //    dynamicRegAcc,
            //    "new_latitude",
            //    //Effect.Crm.Convertor.ConvertorCRMParams.GetCrmDecimal(latitude, false),
            //    latit,
            //    typeof(Effect.Crm.SDKProviders.CrmServiceSdk.CrmDecimalProperty)
            //    );
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_regionid",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("new_region", hoodId),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                );
            return dynamicRegAcc;
        }

        private DML.Xrm.new_zipcode CreateDynamicZip(string zip, Guid hoodId)
        {
            DML.Xrm.new_zipcode zipEntity = new NoProblem.Core.DataModel.Xrm.new_zipcode();
            zipEntity.new_name = zip;
            zipEntity.new_regionid = hoodId;
            return zipEntity;
        }

        private DynamicEntity CreateDynamicRegion(string name, int level, Guid parentId, string code)
        {
            Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity dynamicRegAcc =
               Effect.Crm.Convertor.ConvertorCRMParams.GetNewDynamicEntity("new_region");
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_name",
                name,
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.StringProperty));
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_englishname",
                name,
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.StringProperty));
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_level",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmNumber(level, false),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.CrmNumberProperty)
                );
            if (parentId != Guid.Empty)
            {
                Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                    dynamicRegAcc,
                    "new_parentregionid",
                    Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("new_region", parentId),
                    typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                    );
            }
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_code",
                code,
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.StringProperty));
            return dynamicRegAcc;
        }

        private List<KeyValuePair<decimal, decimal>> GetPolygons(string polygons)
        {
            List<KeyValuePair<decimal, decimal>> list = new List<KeyValuePair<decimal, decimal>>();
            string cleanP = polygons.Replace("POLYGON((", "").Replace("))", "");
            string[] Polygons = cleanP.Split(',');
            foreach (string str in Polygons)
            {
                string[] strs = str.Split(' ');
                list.Add(new KeyValuePair<decimal, decimal>(decimal.Parse(strs[0]), decimal.Parse(strs[1])));
            }
            return list;
        }

        #endregion
    }
}
