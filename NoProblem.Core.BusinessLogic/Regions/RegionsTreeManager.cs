﻿using System;
using System.Collections.Generic;
using System.Linq;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Regions
{
    internal class RegionsTreeManager : XrmUserBase
    {
        #region Ctor
        internal RegionsTreeManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
        }
        #endregion

        #region Internal Methods

        internal void UpdateSupplierRegions(DML.Regions.UpdateSupplierRegionsRequest request)
        {
            //PrintRequestToLog(request);
            DataAccessLayer.RegionAccount regionAccountAccess = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
            DataAccessLayer.SystemUser systemUserAccess = new NoProblem.Core.DataAccessLayer.SystemUser(XrmDataContext);
            DML.Xrm.systemuser currentUser = systemUserAccess.GetCurrentUser();
            IEnumerable<DML.Regions.RegionDataForUpdate> cleanData = request.RegionDataList.Where(x => x.IsDirty == false);
            List<DML.Regions.RegionDataForUpdate> dirtyData = request.RegionDataList.Where(x => x.IsDirty == true).ToList();
            IEnumerable<DML.Xrm.new_regionaccountexpertise> currentRegAccs = regionAccountAccess.GetForTreeUpdate(request.SupplierId, cleanData);
            foreach (DML.Xrm.new_regionaccountexpertise regAcc in currentRegAccs)
            {
                if (regAcc.new_regionid.HasValue == false)
                {
                    continue;
                }
                DML.Regions.RegionDataForUpdate regData = dirtyData.SingleOrDefault(d => d.RegionId == regAcc.new_regionid.Value);
                if (regData != null)//it is in the new regions so check that the status is correct, if not, update it.
                {
                    if (regAcc.new_regionstate.Value != (int)regData.RegionState)
                    {
                        regAcc.new_regionstate = (int)regData.RegionState;
                        //Effect.Crm.DB.DataBaseUtils.DirectUpdateInstance("new_regionaccountexpertise",
                        //    regAcc.new_regionaccountexpertiseid,
                        //    new string[] { "new_regionstate" },
                        //    new object[] { (int)regData.RegionState });
                        string directSqlUpdate = "update new_regionaccountexpertise set new_regionstate = @state where new_regionaccountexpertiseid = @id";
                        DataAccessLayer.RegionAccount.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
                        parameters.Add("@id", regAcc.new_regionaccountexpertiseid);
                        parameters.Add("@state", (int)regData.RegionState);
                        regionAccountAccess.ExecuteNonQuery(directSqlUpdate, parameters);
                    }
                }
                else//it is not in the new regions so delete it.
                {
                    //Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_regionaccountexpertise",
                    //    "new_regionaccountexpertiseid = '" + regAcc.new_regionaccountexpertiseid.ToString() + "'");

                    string deleteSql = "update new_regionaccountexpertise set DeletionStateCode = 2 where new_regionaccountexpertiseid = '" + regAcc.new_regionaccountexpertiseid.ToString() + "'";
                    regionAccountAccess.ExecuteNonQuery(deleteSql);
                }
                //remove from the list the region that we just took care of.
                dirtyData.Remove(regData);
            }
            //take care of all the regions that were not removed from the list int the last iteration (new regions).
            foreach (DML.Regions.RegionDataForUpdate regData in dirtyData)
            {
                Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity dynamicRegAcc = MakeDynamicRegionAccountFromRegionDataForUpdate(regData, request.SupplierId);
                regionAccountAccess.AddRecordToCRM(dynamicRegAcc, currentUser);
            }
            if (!request.IsFromAAR)
            {
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                //var stageInRegistration = accountDAL.GetStageInRegistration(request.SupplierId);
                //if (stageInRegistration < (int)NoProblem.Core.DataModel.Xrm.account.StageInRegistration.CITIES)
                //{
                    accountRepositoryDal.SetSupplierRegistrationStage(request.SupplierId, NoProblem.Core.DataModel.Xrm.account.StageInRegistration.CITIES);
                //}
                //else if (accountDAL.GetStageInRegistration(request.SupplierId) == (int)DML.Xrm.account.StageInRegistration.PAID)
                //{
                //    SupplierManager supManager = new SupplierManager(XrmDataContext);
                //    var supStatus = supManager.GetSupplierStatus(request.SupplierId);
                //    DirectNumbersAssigner assigner = new DirectNumbersAssigner();
                //    if (supStatus.SupplierStatus == NoProblem.Core.DataModel.SupplierModel.SupplierStatus.Available)
                //    {
                //        //assigner.DetachAndReassignNumbersToSupplier(request.SupplierId);
                //    }
                //    else
                //    {
                //        //assigner.DetachAllDirectNumbersFromSupplier(request.SupplierId);
                //    }
                //}
            }            
        }

        internal DML.Regions.GetSupplierRegionsResponse GetSupplierRegions(Guid supplierId, Guid parentRegionId)
        {
            DataAccessLayer.Region regionAccess = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            IEnumerable<DML.Xrm.new_region> regions = null;
            bool isLevelOne = parentRegionId == Guid.Empty;
            if (isLevelOne)
            {
                regions = regionAccess.GetRegionsByLevel(1);
            }
            else
            {
                regions = regionAccess.GetChildren(parentRegionId);
            }
            DataAccessLayer.RegionAccount regionAccountAccess = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
            List<DML.Regions.RegionDataForRetrieve> dataList = new List<NoProblem.Core.DataModel.Regions.RegionDataForRetrieve>();
            foreach (DML.Xrm.new_region reg in regions)
            {
                DML.Regions.RegionDataForRetrieve data = new NoProblem.Core.DataModel.Regions.RegionDataForRetrieve();
                data.RegionId = reg.new_regionid;
                data.RegionName = reg.new_name;
                DML.Xrm.new_regionaccountexpertise currentRegAcc = regionAccountAccess.GetByRegionIdAndAccountId(reg.new_regionid, supplierId);
                if (currentRegAcc != null)
                {
                    data.RegionState = (DML.Xrm.new_regionaccountexpertise.RegionState)Enum.Parse(typeof(DML.Xrm.new_regionaccountexpertise.RegionState), currentRegAcc.new_regionstate.Value.ToString());
                }
                else
                {
                    if (isLevelOne)
                    {
                        data.RegionState = NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.NotWorking;
                    }
                    else
                    {
                        data.RegionState = NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.Working;
                    }
                }
                dataList.Add(data);
            }
            DML.Regions.GetSupplierRegionsResponse response = new DML.Regions.GetSupplierRegionsResponse();
            dataList = dataList.OrderBy(x => x.RegionName).ToList();
            response.RegionDataList = dataList;
            if (isLevelOne)
            {
                response.StageInRegistration = GetStageInRegistration(supplierId);
            }
            return response;
        }

        internal DML.Regions.GetSupplierRegionsSearchResponse GetSupplierRegionsSearch(Guid supplierId, string regionName)
        {
            DML.Regions.GetSupplierRegionsSearchResponse response = new NoProblem.Core.DataModel.Regions.GetSupplierRegionsSearchResponse();
            List<DML.Regions.RegionDataForSearch> retVal = CreateFirstLevelTree(supplierId);
            response.RegionDataList = retVal;
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            IEnumerable<DML.Xrm.new_region> regionsFound = regionDal.SearchRegions(regionName);
            foreach (var reg in regionsFound)
            {
                if (reg.new_level == 1)
                {
                    continue;
                }
                AddToRegionsTree(supplierId, retVal, regionDal, reg);
            }
            return response;
        }

        #endregion

        #region Private Methods

        private List<DML.Regions.RegionDataForSearch> CreateFirstLevelTree(Guid supplierId)
        {
            DML.Regions.GetSupplierRegionsResponse level1Response = GetSupplierRegions(supplierId, Guid.Empty);
            List<DML.Regions.RegionDataForSearch> retVal = new List<NoProblem.Core.DataModel.Regions.RegionDataForSearch>();
            foreach (var reg in level1Response.RegionDataList)
            {
                DML.Regions.RegionDataForSearch data = new NoProblem.Core.DataModel.Regions.RegionDataForSearch();
                data.RegionId = reg.RegionId;
                data.RegionName = reg.RegionName;
                data.RegionState = reg.RegionState;
                retVal.Add(data);
            }
            return retVal;
        }

        private void AddToRegionsTree(Guid supplierId, List<DML.Regions.RegionDataForSearch> retVal, DataAccessLayer.Region regionDal, NoProblem.Core.DataModel.Xrm.new_region reg)
        {
            IEnumerable<DML.Xrm.new_region> parents = regionDal.GetParents(reg);
            DML.Xrm.new_region currentParent = parents.First(x => x.new_level == 1);
            var currentParentReady = retVal.First(x => x.RegionId == currentParent.new_regionid);
            int currentLevel = 2;
            while (currentLevel <= reg.new_level.Value)
            {
                if (currentParentReady.Children == null)
                {
                    var childrenResponse = GetSupplierRegions(supplierId, currentParentReady.RegionId);
                    currentParentReady.Children = (from child in childrenResponse.RegionDataList
                                                   select new DML.Regions.RegionDataForSearch()
                                                   {
                                                       RegionId = child.RegionId,
                                                       RegionName = child.RegionName,
                                                       RegionState =
                                                            (currentParentReady.RegionState ==
                                                            DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild ?
                                                            child.RegionState : currentParentReady.RegionState)
                                                   }).ToList();
                }
                currentParent = parents.FirstOrDefault(x => x.new_level == currentLevel);
                if (currentParent != null)
                {
                    currentParentReady = currentParentReady.Children.First(x => x.RegionId == currentParent.new_regionid);
                }
                currentLevel++;
            }
        }

        private int GetStageInRegistration(Guid supplierId)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(supplierId);
            return supplier.new_stageinregistration.HasValue ? supplier.new_stageinregistration.Value : 0;
        }

        private void PrintRequestToLog(NoProblem.Core.DataModel.Regions.UpdateSupplierRegionsRequest request)
        {
            foreach (DML.Regions.RegionDataForUpdate data in request.RegionDataList)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.WriteToLog(8, "regionId = {0}, state = {1}, level = {2}, idDirry = {3}, parent1 = {4},   parent2 = {5}, parent3 = {6}, parent4 = {7}, parent5 = {8},",
                    data.RegionId, data.RegionState, data.Level, data.IsDirty, data.Parent1, data.Parent2, data.Parent3, data.Parent4, data.Parent5);
            }
        }

        private Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity MakeDynamicRegionAccountFromRegionDataForUpdate(DML.Regions.RegionDataForUpdate regionData, Guid supplierId)
        {
            Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity dynamicRegAcc =
                Effect.Crm.Convertor.ConvertorCRMParams.GetNewDynamicEntity("new_regionaccountexpertise");
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_regionstate",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmPicklist((int)regionData.RegionState),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.PicklistProperty));
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_accountid",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("account", supplierId),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                );
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_regionid",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("new_region", regionData.RegionId),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                );
            if (regionData.Parent1 != Guid.Empty)
            {
                Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                    dynamicRegAcc,
                    "new_parent1id",
                    Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("new_region", regionData.Parent1),
                    typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                    );
            }
            if (regionData.Parent2 != Guid.Empty)
            {
                Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                    dynamicRegAcc,
                    "new_parent2id",
                    Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("new_region", regionData.Parent2),
                    typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                    );
            }
            if (regionData.Parent3 != Guid.Empty)
            {
                Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                    dynamicRegAcc,
                    "new_parent3id",
                    Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("new_region", regionData.Parent3),
                    typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                    );
            }
            if (regionData.Parent4 != Guid.Empty)
            {
                Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                    dynamicRegAcc,
                    "new_parent4id",
                    Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("new_region", regionData.Parent4),
                    typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                    );
            }
            if (regionData.Parent5 != Guid.Empty)
            {
                Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                    dynamicRegAcc,
                    "new_parent5id",
                    Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("new_region", regionData.Parent5),
                    typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                    );
            }
            return dynamicRegAcc;
        }

        #endregion
    }
}
