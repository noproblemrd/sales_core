﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.OrganicDesktopApp
{
    public class CallAndRecordManager : XrmUserBase
    {
        string ThisPhone;
        int SecondRecordLeft;
        const string QueryGetContactPhone = @"
SELECT c.MobilePhone, ce.New_DesktopAppSecondRecordLeft
FROM dbo.ContactBase c WITH(NOLOCK) 
	inner join dbo.ContactExtensionBase ce WITH(NOLOCK) on c.ContactId = ce.ContactId
WHERE c.ContactId = @ContactId"; // ADD record minutes left
        public CallAndRecordManager(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {

        }

        public NoProblem.Core.DataModel.Deskapp.CallRecordResponse StartCall(NoProblem.Core.DataModel.Deskapp.CallRecordRequest request)
        {
            NoProblem.Core.DataModel.Deskapp.CallRecordResponse response = new DataModel.Deskapp.CallRecordResponse();
            if(!SetContactPhoneData(request.ContactId))
            {
                response.ResponseStatus = DataModel.Deskapp.eCallRecordResponseStatus.ContactIdNotFound;
                return response;
            }
            if(string.IsNullOrEmpty(ThisPhone))
            {
                response.ResponseStatus = DataModel.Deskapp.eCallRecordResponseStatus.ThereIsNotContactPhoneNumber;
                return response;
            }
            if(SecondRecordLeft < 10)
            {
                response.ResponseStatus = DataModel.Deskapp.eCallRecordResponseStatus.NoSecondRecordLeft;
                return response;
            }
            FoneApiBL.Handlers.Click2Call.DesktopAppCallAndRecord.DesktopAppCallAndRecordCallManager callManager
                = new FoneApiBL.Handlers.Click2Call.DesktopAppCallAndRecord.DesktopAppCallAndRecordCallManager(ThisPhone, SecondRecordLeft, request);
            bool IsStartCallSuccesfully = callManager.StartCall();
            if (IsStartCallSuccesfully)
            {
                response.ResponseStatus = DataModel.Deskapp.eCallRecordResponseStatus.StartSuccessfully;
                response.CallId = callManager.GetCallId;
            }
            else
                response.ResponseStatus = DataModel.Deskapp.eCallRecordResponseStatus.Faild;
            return response;
        }
        private bool SetContactPhoneData(Guid ContactId)
        {
            bool IfFoundContact = true;
            using(SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand(QueryGetContactPhone, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@ContactId", ContactId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        ThisPhone = reader["MobilePhone"] == DBNull.Value ? null : (string)reader["MobilePhone"];
                        SecondRecordLeft = reader["New_DesktopAppSecondRecordLeft"] == DBNull.Value ? 0 : (int)reader["New_DesktopAppSecondRecordLeft"];
                    }
                    else
                        IfFoundContact = false;
                    conn.Close();
                }
            }
            return IfFoundContact;
        }
        
    }
}
