﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.OrganicDesktopApp
{
    public class EmailSenderAfterCallAndRecord : XrmUserBase
    {
        public EmailSenderAfterCallAndRecord(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public bool SendEmail(Guid callId, string email)
        {
            DataAccessLayer.deskappprivatecallrecord dal = new DataAccessLayer.deskappprivatecallrecord(XrmDataContext);
            var recordingObj = (from c in dal.All
                                where c.new_deskappprivatecallrecordid == callId
                                select new { Recording = c.new_recordurl }).FirstOrDefault();
            if (recordingObj == null || String.IsNullOrEmpty(recordingObj.Recording))
            {
                return false;
            }

            Dictionary<string, string> vars = new Dictionary<string, string>();
            vars.Add("NPVAR_RECORDING", recordingObj.Recording);

            MandrillSender sender = new MandrillSender();
            bool sentOk = sender.SendTemplate(email, null, DataModel.MandrillTemplates.E_CUSTOMER_POST_CALL_AND_RECORD_DESKTOP_APP, vars);
            return sentOk;
        }
    }
}
