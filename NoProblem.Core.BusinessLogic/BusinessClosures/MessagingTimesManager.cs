﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.BusinessClosures
{
    internal class MessagingTimesManager : XrmUserBase
    {
        #region Ctor
        internal MessagingTimesManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Internal Methods

        internal void UpdateMessagingTimes(List<DataModel.BusinessClosures.MessagingTimeData> messagingTimes)
        {
            DataAccessLayer.Availability dal = new NoProblem.Core.DataAccessLayer.Availability(XrmDataContext);
            dal.DeleteAllMessagingTimes();
            foreach (var item in messagingTimes)
            {
                DataModel.Xrm.new_availability availability = new NoProblem.Core.DataModel.Xrm.new_availability(XrmDataContext);
                availability.new_day = (int)item.DayOfWeek;
                availability.new_ismessagingtime = true;
                availability.new_from = item.FromHour;
                availability.new_to = item.ToHour;
                dal.Create(availability);
            }
            XrmDataContext.SaveChanges();
        }

        internal List<DataModel.BusinessClosures.MessagingTimeData> GetMessagingTimes()
        {
            DataAccessLayer.Availability dal = new NoProblem.Core.DataAccessLayer.Availability(XrmDataContext);
            List<DataModel.BusinessClosures.MessagingTimeData> data = dal.GetMessagingTimes();
            return data;
        } 

        #endregion
    }
}
