﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Consumer;

namespace NoProblem.Core.BusinessLogic.BusinessClosures
{
    public class ClosuresManager : XrmUserBase
    {
        #region Ctor
        public ClosuresManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Members

        internal enum ClosureOptions
        {
            ClosureDatesOnly,
            WeeklyAndClosureDates
        }

        #endregion

        #region Public Methods

        public Guid UpsertClosureDate(Guid closureDateId, string name, DateTime fromDate, DateTime toDate)
        {
            ClosureDatesManager manager = new ClosureDatesManager(XrmDataContext);
            return manager.UpsertClosureDate(closureDateId, name, fromDate, toDate);
        }

        public void DeleteClosureDates(List<Guid> ids)
        {
            ClosureDatesManager manager = new ClosureDatesManager(XrmDataContext);
            manager.DeleteClosureDates(ids);
        }

        public List<DataModel.BusinessClosures.ClosureDateData> GetClosureDates()
        {
            ClosureDatesManager manager = new ClosureDatesManager(XrmDataContext);
            return manager.GetClosureDates();
        }

        public void UpdateMessagingTimes(List<DataModel.BusinessClosures.MessagingTimeData> messagingTimes)
        {
            MessagingTimesManager manager = new MessagingTimesManager(XrmDataContext);
            manager.UpdateMessagingTimes(messagingTimes);
        }

        public List<DataModel.BusinessClosures.MessagingTimeData> GetMessagingTimes()
        {
            MessagingTimesManager manager = new MessagingTimesManager(XrmDataContext);
            return manager.GetMessagingTimes();
        }

        #endregion

        #region Internal Methods

        internal bool IsNowClosure(ClosureOptions options)
        {
            return IsTimeClosure(options, DateTime.Now);
        }

        internal bool IsTimeClosure(ClosureOptions options, DateTime time)
        {
            DateTime localTime = FixDateToLocal(time);
            return IsTimeClosureWithLocalTime(options, localTime);
        }

        internal bool IsTimeClosure(ClosureOptions options, DateTime time, int timeZoneCode)
        {
            DateTime localTime = FixDateToLocal(time, timeZoneCode);
            return IsTimeClosureWithLocalTime(options, localTime);
        }        

        //internal int GetMinutesToNextPoliteHour(DateTime time, int? timeZoneCode = null)
        //{
        //    DateTime localTime;
        //    if (timeZoneCode.HasValue)
        //        localTime = FixDateToLocal(time, timeZoneCode.Value);
        //    else
        //        localTime = FixDateToLocal(time);
        //    DataAccessLayer.Availability avDal = new DataAccessLayer.Availability(XrmDataContext);
        //    var messagingTimes = avDal.GetMessagingTimes();
        //    var relevantDay = messagingTimes.FirstOrDefault(x => (int)x.DayOfWeek == ((int)localTime.DayOfWeek) + 1);
        //    int retVal = -1;
        //    if (relevantDay != null)
        //    {
        //        int fromHour = TranslateStringHourToInt(relevantDay.FromHour);
        //        int toHour = TranslateStringHourToInt(relevantDay.ToHour);
        //        if (fromHour <= localTime.Hour && toHour > localTime.Hour)
        //        {
        //            retVal = 0;
        //        }
        //        else if (fromHour > localTime.Hour)
        //        {

        //        }
        //    }
        //    return retVal;
        //}

        #endregion        

        private bool IsTimeClosureWithLocalTime(ClosureOptions options, DateTime localTime)
        {
            DataAccessLayer.ClosureDate closuresDal = new NoProblem.Core.DataAccessLayer.ClosureDate(XrmDataContext);
            bool isClosure = closuresDal.IsInClosure(localTime);
            if (!isClosure && options == ClosureOptions.WeeklyAndClosureDates)
            {
                DataAccessLayer.Availability availabilityDal = new NoProblem.Core.DataAccessLayer.Availability(XrmDataContext);
                isClosure = !availabilityDal.IsInMessagingTime(localTime);
            }
            return isClosure;
        }
    }
}
