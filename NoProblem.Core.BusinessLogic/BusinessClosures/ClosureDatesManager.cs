﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.BusinessClosures
{
    internal class ClosureDatesManager : XrmUserBase
    {
        #region Ctor
        internal ClosureDatesManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Internal Methds

        internal Guid UpsertClosureDate(Guid closureDateId, string name, DateTime fromDate, DateTime toDate)
        {
            DataAccessLayer.ClosureDate dal = new NoProblem.Core.DataAccessLayer.ClosureDate(XrmDataContext);
            DataModel.Xrm.new_closuredate closure = new NoProblem.Core.DataModel.Xrm.new_closuredate(XrmDataContext);
            closure.new_from = fromDate;
            closure.new_to = toDate;
            closure.new_name = name;
            if (closureDateId == Guid.Empty)
            {
                dal.Create(closure);
            }
            else
            {
                closure.new_closuredateid = closureDateId;
                dal.Update(closure);
            }
            XrmDataContext.SaveChanges();
            return closure.new_closuredateid;
        }

        internal void DeleteClosureDates(List<Guid> ids)
        {
            DataAccessLayer.ClosureDate dal = new NoProblem.Core.DataAccessLayer.ClosureDate(XrmDataContext);
            foreach (var id in ids)
            {
                DataModel.Xrm.new_closuredate closure = new NoProblem.Core.DataModel.Xrm.new_closuredate(XrmDataContext);
                closure.new_closuredateid = id;
                dal.Delete(closure);
            }
            XrmDataContext.SaveChanges();
        }

        internal List<DataModel.BusinessClosures.ClosureDateData> GetClosureDates()
        {
            DataAccessLayer.ClosureDate dal = new NoProblem.Core.DataAccessLayer.ClosureDate(XrmDataContext);
            List<DataModel.BusinessClosures.ClosureDateData> data = dal.GetClosureDates();
            return data;
        } 

        #endregion
    }
}
