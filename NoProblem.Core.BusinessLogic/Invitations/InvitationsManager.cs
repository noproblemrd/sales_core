﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.AdvertiserDashboards.Invitations;
using DML = NoProblem.Core.DataModel;
using Effect.Crm.Logs;
using System.Web;

namespace NoProblem.Core.BusinessLogic.Invitations
{
    public class InvitationsManager : XrmUserBase
    {
        #region Ctor
        public InvitationsManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Public Methods

        public void RequestMoreInvites(Guid supplierId)
        {
            DataModel.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_requestedmoreinvites = true;
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        public static void GiveMoreInvites(object obj)
        {
            try
            {
                LogUtils.MyHandle.WriteToLog("GiveMoreInvites started");
                InvitationsManager manager = new InvitationsManager(null);
                manager.GiveMoreInvites();
                LogUtils.MyHandle.WriteToLog("GiveMoreInvites ended");
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GiveMoreInvites");
            }
        }

        public static void CheckWaitingForInvites(object obj)
        {
            try
            {
                LogUtils.MyHandle.WriteToLog("CheckWaitingForInvites started");
                InvitationsManager manager = new InvitationsManager(null);
                manager.CheckWaitingForInvites();
                LogUtils.MyHandle.WriteToLog("CheckWaitingForInvites ended");
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckWaitingForInvites");
            }
        }

        public InvitationData GetInvitationsData(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = dal.Retrieve(supplierId);
            InvitationData data = new InvitationData();
            data.InvitationsLeft = supplier.new_invitesleft.HasValue ? supplier.new_invitesleft.Value : 3;
            if (data.InvitationsLeft == 0)
            {
                data.RequestedMoreInvites = supplier.new_requestedmoreinvites.HasValue && supplier.new_requestedmoreinvites.Value;
            }
            data.Email = supplier.emailaddress1;
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            int successBonus;
            if (!int.TryParse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_SUCCESS_BONUS), out successBonus))
            {
                successBonus = 25;
            }
            data.InvitationSuccessBonus = successBonus;
            int lifeTimeBonus;
            if (!int.TryParse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_LIFETIME_BONUS), out lifeTimeBonus))
            {
                lifeTimeBonus = 1;
            }
            data.InvitationLifetimeBonus = lifeTimeBonus;
            data.JustGotMoreInvites = supplier.new_justgotmoreinvites.HasValue && supplier.new_justgotmoreinvites.Value;
            if (data.JustGotMoreInvites)
            {
                supplier.new_justgotmoreinvites = false;
                dal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
            return data;
        }

        public CreateInvitationResponse CreateInvitation(CreateInvitationRequest request)
        {
            if (string.IsNullOrEmpty(request.InvitedEmail))
            {
                throw new ArgumentException("Email cannot be null nor empty");
            }
            CreateInvitationResponse response = new CreateInvitationResponse();

            DataAccessLayer.Invitation dal = new NoProblem.Core.DataAccessLayer.Invitation(XrmDataContext);
            DataModel.Xrm.account supplier;
            bool valid = ValidateInvitation(request.SupplierId, request.InvitedEmail, response, dal, out supplier);
            if (valid)
            {
                DML.Xrm.new_invitation invitation = new NoProblem.Core.DataModel.Xrm.new_invitation();
                invitation.statuscode = (int)DML.Xrm.new_invitation.InvitationStatus.Pending;
                invitation.new_invitedemail = request.InvitedEmail;
                invitation.new_accountregisteredid = request.SupplierId;
                string message = request.PersonalMessage;
                if (message.Length > 300)
                {
                    message = message.Substring(0, 300);
                }
                invitation.new_personalmessage = message;
                ReferralCodeCreator referralCodeCreator = new ReferralCodeCreator(XrmDataContext);
                string code = referralCodeCreator.GetReferralCode();
                invitation.new_referralcode = code;
                dal.Create(invitation);
                XrmDataContext.SaveChanges();

                bool emailSent = SendInvitationEmails(invitation, request.PersonalMessage);
                if (emailSent)
                {
                    response.InvitationsLeft--;
                    DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                    supplier.new_invitesleft = response.InvitationsLeft;
                    accountRepositoryDal.Update(supplier);
                    XrmDataContext.SaveChanges();
                }
                else
                {
                    dal.Delete(invitation);
                    XrmDataContext.SaveChanges();
                    response.Status = CreateInvitationResponse.CreateInvitationStatus.EmailFailed;
                }
            }
            else if (response.Status == CreateInvitationResponse.CreateInvitationStatus.NeedsEmailVerification)
            {
                MandrillSender sender = new MandrillSender();
                var vars = new Dictionary<string, string>();
                vars.Add("NPVAR_AD_NAME", supplier.name);
                DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string url = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.EMAIL_VERIFICATION_URL);
                vars.Add("NPVAR_AD_EMAILVERIFICATION", url + "?verification=" + HttpUtility.UrlEncode(supplier.accountid.ToString()) + "&friendemail=" + HttpUtility.UrlEncode(request.InvitedEmail) + "&verify=" + HttpUtility.UrlEncode(Utils.CeaserCypherRot47.Rot47(supplier.emailaddress1)));
                DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
                string subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_AD_EmailVerification);
                bool sent = sender.SendTemplate(supplier.emailaddress1, subject, DataModel.MandrillTemplates.E_AD_EmailVerification, vars);
                if (!sent)
                {
                    response.Status = CreateInvitationResponse.CreateInvitationStatus.EmailFailed;
                }
            }
            return response;
        }

        public bool VerifyEmail(string verification, string verify)
        {
            bool isVerified = false;
            Guid supplierId;
            try
            {
                supplierId = new Guid(verification);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "verification was not a valid Guid in VerifyEmail");
                return isVerified;
            }
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = dal.Retrieve(supplierId);
            if (supplier != null)
            {
                string givenEmail = Utils.CeaserCypherRot47.Rot47(verify);
                if (givenEmail == supplier.emailaddress1)
                {
                    supplier.new_isemailverified = true;
                    dal.Update(supplier);
                    XrmDataContext.SaveChanges();
                    isVerified = true;
                }
            }
            return isVerified;
        }

        #endregion

        #region Internal Methods

        internal static void HandleInvitationsAndEmailsOnRegistrationFinished(Guid supplierId, string email)
        {
            InvitationsManager manager = new InvitationsManager(null);
            HandleInvitationsAndEmailsOnRegistrationFinishedDelegate del = new HandleInvitationsAndEmailsOnRegistrationFinishedDelegate(manager.HandleInvitationsAndEmailsOnRegistrationFinishedAsync);
            del.BeginInvoke(supplierId, email, manager.HandleInvitationsAndEmailsOnRegistrationFinishedAsyncCallback, del);
        }

        internal static void CreditTheSupplier(Guid inviteeId, CreditType type, int paymentAmount)
        {
            InvitationsManager manager = new InvitationsManager(null);
            CreditTheSupplierDelegate del = new CreditTheSupplierDelegate(manager.CreditTheSupplierAsync);
            del.BeginInvoke(inviteeId, type, paymentAmount, manager.CreditTheSupplierAsyncCallback, del);
        }

        internal enum CreditType
        {
            FirstPayment,
            LifeTime
        }

        #endregion

        #region Private Methods

        private void GiveMoreInvites()
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            string nonQuery =
                @"update accountextensionbase 
                    set new_requestedmoreinvites = 0,
                    new_invitesleft = 3,
                    new_justgotmoreinvites = 1
                    where new_requestedmoreinvites = 1";
            dal.ExecuteNonQuery(nonQuery);
        }

        private void CheckWaitingForInvites()
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            string query =
                @"select accountid, emailaddress1, name, new_cashbalance from account with(nolock)
                where deletionstatecode = 0 and new_waitingforinvitation = 1 and DATEADD(day, 2, new_finishedwebregistrationon) < getdate()";
            var reader = dal.ExecuteReader(query);
            foreach (var row in reader)
            {
                try
                {
                    Guid supplierId = (Guid)row["accountid"];
                    string email = Convert.ToString(row["emailaddress1"]);
                    string name = Convert.ToString(row["name"]);
                    decimal balance = row["new_cashbalance"] != DBNull.Value ? (decimal)row["new_cashbalance"] : 0;
                    if (string.IsNullOrEmpty(email))
                    {
                        continue;
                    }
                    ApproveInvitationRequest(supplierId, email, balance, name, dal);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in CheckWaitingForInvites in the foreach loop. continueing to next supplier");
                }
            }
        }

        private void ApproveInvitationRequest(Guid supplierId, string email, decimal balance, string name, DataAccessLayer.AccountRepository dal)
        {
            MandrillSender sender = new MandrillSender();

            Dictionary<string, string> vars = new Dictionary<string, string>();
            vars.Add("NPVAR_AD_NAME", name);
            bool sent;
            DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
            if (balance > 0)
            {
                vars.Add("NPVAR_CURRENTBALANCE", string.Format("{0:0.##}", balance));
                string subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_Requester_LoginCodeAAR);
                sent = sender.SendTemplate(email, subject, DataModel.MandrillTemplates.E_Requester_LoginCodeAAR, vars);
            }
            else
            {
                string subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_Requester_LoginCode);
                sent = sender.SendTemplate(email, subject, DataModel.MandrillTemplates.E_Requester_LoginCode, vars);
            }
            if (sent)
            {
                DataModel.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
                supplier.accountid = supplierId;
                supplier.new_waitingforinvitation = false;
                dal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
        }

        delegate void HandleInvitationsAndEmailsOnRegistrationFinishedDelegate(Guid supplierId, string email);

        private void HandleInvitationsAndEmailsOnRegistrationFinishedAsync(Guid supplierId, string email)
        {
            try
            {
                MandrillSender sender = new MandrillSender();
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                var supplier = accountRepositoryDal.Retrieve(supplierId);
                DataAccessLayer.Invitation dal = new NoProblem.Core.DataAccessLayer.Invitation(XrmDataContext);
                var invitation = dal.FindLinkedInvitation(supplierId);
                if (invitation != null)//supplier was already linked in registration, if no is requester
                {
                    bool isEmailInvitedChangedFromLinked = false;
                    invitation.statuscode = (int)DML.Xrm.new_invitation.InvitationStatus.Success;
                    if (invitation.new_invitedemail != email)
                    {
                        invitation.new_invitedemailchangedto = email;
                        //to check if the email he changed to was invited by a different advertiser
                        isEmailInvitedChangedFromLinked = true;
                    }
                    invitation.new_successon = DateTime.Now;
                    dal.Update(invitation);
                    XrmDataContext.SaveChanges();
                    if (isEmailInvitedChangedFromLinked)
                    {
                        CheckIfChangedEmailWasInvitedByOther(invitation);
                    }

                    //send email after registration to inviter and invitee
                    DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
                    string subject;
                    Dictionary<string, string> vars = new Dictionary<string, string>();
                    if (supplier.new_cashbalance.HasValue
                        && supplier.new_cashbalance.Value > 0)
                    {
                        //send welcome email and tell him we gave him free bonus.
                        vars.Add("NPVAR_INVITEE_NAME", supplier.name);
                        vars.Add("NPVAR_CURRENTBALANCE", string.Format("{0:0.##}", supplier.new_cashbalance.Value));
                        subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_Invitee_InviteeRegisteredAAR);
                        sender.SendTemplate(supplier.emailaddress1, subject, DataModel.MandrillTemplates.E_Invitee_InviteeRegisteredAAR, vars);
                    }
                    else//has no bonus
                    {
                        vars.Add("NPVAR_INVITEE_NAME", supplier.name);
                        subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_Invitee_InviteeRegistered);
                        sender.SendTemplate(supplier.emailaddress1, subject, DataModel.MandrillTemplates.E_Invitee_InviteeRegistered, vars);
                    }
                    //send email to inviter
                    vars = new Dictionary<string, string>();
                    vars.Add("NPVAR_AD_NAME", invitation.new_accountregistered_invitation.name);
                    vars.Add("NPVAR_INVITEE_NAME", supplier.name);
                    DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                    vars.Add("NPVAR_1STBONUS", config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_SUCCESS_BONUS));
                    vars.Add("NPVAR_4EVERBONUS", config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_LIFETIME_BONUS));
                    subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_AD_InviteeRegistered);
                    subject = string.Format(subject, supplier.name);
                    sender.SendTemplate(invitation.new_accountregistered_invitation.emailaddress1, subject, DataModel.MandrillTemplates.E_AD_InviteeRegistered, vars);
                }
                else //no invitation. send email.
                {
                    DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);                    
                    if (!supplier.new_waitingforinvitation.HasValue || !supplier.new_waitingforinvitation.Value)
                    {
                        //is itc so he doesn't wait for approval, send email welcoming him.
                        string subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_AD_Reg_ITC);
                        sender.SendTemplate(supplier.emailaddress1, subject, DataModel.MandrillTemplates.E_AD_Reg_ITC, new Dictionary<string, string>());
                    }
                    else
                    {
                        //has to wait for approval email.
                        Dictionary<string, string> vars = new Dictionary<string, string>();
                        vars.Add("NPVAR_AD_NAME", supplier.name);
                        string subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_Requester_WaitingList);
                        sender.SendTemplate(supplier.emailaddress1, subject, DataModel.MandrillTemplates.E_Requester_WaitingList, vars);
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in HandleInvitationsAndEmailsOnRegistrationFinishedAsync. supplierId = {0}, email = {1}", supplierId, email);
            }
        }

        private void CheckIfChangedEmailWasInvitedByOther(DML.Xrm.new_invitation invitation)
        {
            CheckIfChangedEmailWasInvitedByOtherDelegate del = new CheckIfChangedEmailWasInvitedByOtherDelegate(CheckIfChangedEmailWasInvitedByOtherAsync);
            del.BeginInvoke(invitation, CheckIfChangedEmailWasInvitedByOtherAsyncCallback, del);
        }

        delegate void CheckIfChangedEmailWasInvitedByOtherDelegate(DML.Xrm.new_invitation invitation);

        private void CheckIfChangedEmailWasInvitedByOtherAsync(DML.Xrm.new_invitation invitation)
        {
            DataAccessLayer.Invitation dal = new NoProblem.Core.DataAccessLayer.Invitation(null);
            var lostInvitation = dal.FindPendingInvitationByEmail(invitation.new_invitedemailchangedto);
            if (lostInvitation != null)
            {
                lostInvitation.new_loston = DateTime.Now;
                lostInvitation.statuscode = (int)DML.Xrm.new_invitation.InvitationStatus.Lost;
                dal.Update(lostInvitation);
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(dal.XrmDataContext);
                var supplierLoser = lostInvitation.new_accountregistered_invitation;
                supplierLoser.new_invitesleft = supplierLoser.new_invitesleft.Value + 1;
                accountRepositoryDal.Update(supplierLoser);
                dal.XrmDataContext.SaveChanges();
                SendLostInvitationEmail(lostInvitation, supplierLoser);
            }
        }

        /// <summary>
        /// Sends email to advertiser that lost an invitation to someone else. Use local context only.
        /// </summary>
        /// <param name="lostInvitation"></param>
        /// <param name="invitation"></param>
        private void SendLostInvitationEmail(NoProblem.Core.DataModel.Xrm.new_invitation lostInvitation, DataModel.Xrm.account loserSupplier)
        {
            MandrillSender sender = new MandrillSender();
            Dictionary<string, string> vars = new Dictionary<string, string>();
            vars.Add("NPVAR_AD_NAME", loserSupplier.name);
            vars.Add("NPVAR_INVITEE_EMAIL", lostInvitation.new_invitedemail);
            DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
            string subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_AD_InviteeLost);
            subject = string.Format(subject, lostInvitation.new_invitedemail);
            sender.SendTemplate(loserSupplier.emailaddress1, subject, DataModel.MandrillTemplates.E_AD_InviteeLost, vars);
        }

        private void CheckIfChangedEmailWasInvitedByOtherAsyncCallback(IAsyncResult result)
        {
            try
            {
                ((CheckIfChangedEmailWasInvitedByOtherDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckIfChangedEmailWasInvitedByOtherAsyncCallback");
            }
        }

        delegate void CreditTheSupplierDelegate(Guid inviteeId, CreditType type, int paymentAmount);

        private void CreditTheSupplierAsync(Guid inviteeId, CreditType type, int paymentAmount)
        {
            DataAccessLayer.Invitation invitationDal = new NoProblem.Core.DataAccessLayer.Invitation(XrmDataContext);
            DataModel.Xrm.new_invitation invitation = invitationDal.FindInviteeSuccessInvitation(inviteeId);
            if (invitation == null)
                return;
            Guid supplierId = invitation.new_accountregisteredid.Value;
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            string voucherNumber = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AAR_VOUCHER_NUMBER);
            Guid voucherReasonId = new Guid(configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AAR_VOUCHER_REASON_ID));
            int bonus = 0;
            if (type == CreditType.FirstPayment)
            {
                bonus = int.Parse(configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_SUCCESS_BONUS));
            }
            else//lifetime bonus
            {
                if (paymentAmount <= 0)
                    return;
                int percent = int.Parse(configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_LIFETIME_BONUS));
                bonus = (int)Math.Ceiling((double)paymentAmount / 100 * (double)percent);
            }
            SupplierPricing.SupplierPricingManager pricing = new NoProblem.Core.BusinessLogic.SupplierPricing.SupplierPricingManager(configDal.XrmDataContext);
            var pricingResponse = pricing.CreateSupplierPricing(
                new NoProblem.Core.DataModel.SupplierPricing.CreateSupplierPricingRequest()
                {
                    BonusAmount = 0,
                    PaymentAmount = bonus,
                    PaymentMethod = NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.Voucher,
                    SupplierId = supplierId,
                    VoucherNumber = voucherNumber,
                    VoucherReasonId = voucherReasonId,
                    BonusType = NoProblem.Core.DataModel.Xrm.new_supplierpricing.BonusType.None,
                    Action = DataModel.Xrm.new_balancerow.Action.CreditEarned,
                    Description = invitation.new_accountinvited_invitation.name
                });
            if (pricingResponse.DepositStatus != NoProblem.Core.DataModel.SupplierPricing.eDepositStatus.OK)
            {
                LogUtils.MyHandle.WriteToLog("Unable to give invitation bonus money to {0}. DepositStatus = {1}", supplierId, pricingResponse.DepositStatus);
            }
            else
            {
                if (type == CreditType.FirstPayment)
                {
                    SendFirstPaymentToInviterEmail(supplierId, invitation, configDal);
                }
            }
        }

        private void SendFirstPaymentToInviterEmail(Guid registeredId, DataModel.Xrm.new_invitation invitation, DataAccessLayer.ConfigurationSettings configDal)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var registered = dal.Retrieve(registeredId);
            Dictionary<string, string> vars = new Dictionary<string, string>();
            vars.Add("NPVAR_AD_NAME", registered.name);
            vars.Add("NPVAR_INVITEE_NAME", invitation.new_accountinvited_invitation.name);
            vars.Add("NPVAR_1STBONUS", configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_SUCCESS_BONUS));
            vars.Add("NPVAR_4EVERBONUS", configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_LIFETIME_BONUS));
            vars.Add("NPVAR_CURRENTBALANCE", string.Format("{0:0.##}", registered.new_cashbalance.Value));
            MandrillSender sender = new MandrillSender();
            DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
            string subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_AD_Deposit);
            sender.SendTemplate(registered.emailaddress1, subject, DataModel.MandrillTemplates.E_AD_Deposit, vars);
        }

        private void CreditTheSupplierAsyncCallback(IAsyncResult result)
        {
            try
            {
                ((CreditTheSupplierDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CreditTheSupplierAsyncCallback");
            }
        }

        private void HandleInvitationsAndEmailsOnRegistrationFinishedAsyncCallback(IAsyncResult result)
        {
            try
            {
                ((HandleInvitationsAndEmailsOnRegistrationFinishedDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in HandleInvitationsAndEmailsOnRegistrationFinishedAsyncCallback");
            }
        }

        private bool SendInvitationEmails(NoProblem.Core.DataModel.Xrm.new_invitation invitation, string personalMessage)
        {
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            //to ad (inviter):
            string adName = invitation.new_accountregistered_invitation.name;
            string inviteeEmail = invitation.new_invitedemail;
            string referralCode = invitation.new_referralcode;
            string firstBonus = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_SUCCESS_BONUS);
            string lifeTimeBonus = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_LIFETIME_BONUS);
            MandrillSender sender = new MandrillSender();
            Dictionary<string, string> vars = new Dictionary<string, string>();

            //to invitee:
            string templateName;
            vars.Add("NPVAR_AD_NAME", adName);
            if (!string.IsNullOrEmpty(personalMessage))//has personal message
            {
                vars.Add("NPVAR_AD_PERSONALMESSAGE", personalMessage);
                templateName = DataModel.MandrillTemplates.E_Invitee_InviteSent;
            }
            else
            {
                templateName = DataModel.MandrillTemplates.E_Invitee_InviteSent_NoMessage;
            }
            vars.Add("NPVAR_REFERRALCODE", invitation.new_referralcode);
            DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
            string subject = subjectsConfig.GetEmailSubject(templateName);
            subject = string.Format(subject, adName);
            bool sent = sender.SendTemplate(
                invitation.new_invitedemail,
                subject,
                templateName,
                vars);

            if (sent)
            {
                //to invitor
                vars = new Dictionary<string, string>();
                vars.Add("NPVAR_AD_NAME", adName);
                vars.Add("NPVAR_INVITEE_EMAIL", inviteeEmail);
                vars.Add("NPVAR_REFERRALCODE", referralCode);
                vars.Add("NPVAR_1STBONUS", firstBonus);
                vars.Add("NPVAR_4EVERBONUS", lifeTimeBonus);
                subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_AD_InviteSent);
                sender.SendTemplate(
                    invitation.new_accountregistered_invitation.emailaddress1,
                    subject,
                    DataModel.MandrillTemplates.E_AD_InviteSent,
                    vars);
            }

            return sent;
        }

        private bool ValidateInvitation(Guid supplierId, string invitedEmail, CreateInvitationResponse response, DataAccessLayer.Invitation dal, out DataModel.Xrm.account supplier)
        {
            bool valid = true;
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            supplier = accountRepositoryDal.Retrieve(supplierId);
            int invitationsLeft = supplier.new_invitesleft.HasValue ? supplier.new_invitesleft.Value : 3;
            response.InvitationsLeft = invitationsLeft;
            if (invitationsLeft <= 0)
            {
                response.Status = CreateInvitationResponse.CreateInvitationStatus.NoInvitationsLeft;
                valid = false;
                return valid;
            }
            if (!supplier.new_isemailverified.HasValue || !supplier.new_isemailverified.Value)
            {
                response.Status = CreateInvitationResponse.CreateInvitationStatus.NeedsEmailVerification;
                return false;
            }
            bool isExists = accountRepositoryDal.IsEmailDuplicate(invitedEmail, Guid.Empty);
            if (isExists)
            {
                response.Status = CreateInvitationResponse.CreateInvitationStatus.AleradyRegistered;
                valid = false;
                return valid;
            }
            bool isInvited = dal.IsAlreadyInvited(invitedEmail);
            if (isInvited)
            {
                response.Status = CreateInvitationResponse.CreateInvitationStatus.AlreadyInvited;
                valid = false;
                return valid;
            }
            return valid;
        }

        #endregion
    }
}
