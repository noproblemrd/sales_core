﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace NoProblem.Core.BusinessLogic.Invitations
{
    internal class ReferralCodeCreator : XrmUserBase
    {
        public ReferralCodeCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public string GetReferralCode()
        {
            return GetReferralCodeIter().ToString("X6");
        }

        private int GetReferralCodeIter()
        {
            string query =
                @"select top 1 
                new_nextvalue
                from new_autonumber
                where
                new_entityname = 'referralcode'";

            DataAccessLayer.Invitation dal = new NoProblem.Core.DataAccessLayer.Invitation(XrmDataContext);
            int referralCodeNumeric = (int)dal.ExecuteScalar(query);

            int nextValue = CalculateNextValue(referralCodeNumeric);

            string update =
                @"update new_autonumber set new_nextvalue = @nextValue 
                  where new_entityname = 'referralcode' and new_nextvalue = @referralCode";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@referralCode", referralCodeNumeric));
            parameters.Add(new KeyValuePair<string, object>("@nextValue", nextValue));
            int rowsAffected = dal.ExecuteNonQuery(update, parameters);

            if (rowsAffected == 0)
            {
                referralCodeNumeric = GetReferralCodeIter();
            }
            return referralCodeNumeric;
        }

        /// <summary>
        /// Gives all the number between 0 to 0xffffff - 1
        /// </summary>
        /// <param name="referralCode"></param>
        /// <returns></returns>
        private int CalculateNextValue(int referralCodeNumeric)
        {
            int increment = 1019071;//big prime number
            int n = 0xffffff;//max number (something like 16 millions)
            int seed = referralCodeNumeric;
            int nextValue = (seed + increment) % n;
            return nextValue;
        }
    }
}
