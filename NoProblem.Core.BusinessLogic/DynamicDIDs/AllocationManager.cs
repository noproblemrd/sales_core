﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.DynamicDIDs
{
    public class AllocationManager : XrmUserBase
    {
        #region Ctor
        public AllocationManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Delegates
        delegate void CreateAllocationFailureDelegate(Guid webSiteId, Guid headingId, string realPhone);
        delegate void CreateExposureDelegate(NoProblem.Core.DataModel.Xrm.new_ddidallocation allocation, NoProblem.Core.DataModel.DynamicDIDs.AllocationRequest request);
        #endregion

        #region Public Methods

        public List<DML.DynamicDIDs.AllocationResponse> AllocateMultiple(List<DML.DynamicDIDs.AllocationRequest> request)
        {
            List<DML.DynamicDIDs.AllocationResponse> retVal = new List<NoProblem.Core.DataModel.DynamicDIDs.AllocationResponse>();
            foreach (var item in request)
            {
                var itemResponse = Allocate(item);
                retVal.Add(itemResponse);
            }
            return retVal;
        }

        public DataModel.DynamicDIDs.AllocationResponse Allocate(DataModel.DynamicDIDs.AllocationRequest request)
        {
            DML.DynamicDIDs.AllocationResponse response = new NoProblem.Core.DataModel.DynamicDIDs.AllocationResponse(request.RealPhone);
            if (string.IsNullOrEmpty(request.RealPhone))
            {
                response.NotMappedReason = "bad-real-phone";
                response.DID = null;
                LogUtils.MyHandle.WriteToLog("RealPhone was null or empty in method AllocationManager.Allocate from originId {0}", request.OriginId);
                return response;
            }
            Origins.AffiliatesManager affilateManager = new Origins.AffiliatesManager(XrmDataContext);
            Guid webSiteId = affilateManager.GetCreateWebSiteId(request.WebSite, request.OriginId);
            DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            Guid headingId = expertiseDal.GetPrimaryExpertiseIdByCode(request.HeadingCode);
            if (headingId == Guid.Empty)
            {
                response.NotMappedReason = "bad-heading-code";
                response.DID = null;
                LogUtils.MyHandle.WriteToLog("Non existing heading code received in method AllocationManager.Allocate, HeadingCode {0}, originId {1}", request.HeadingCode, request.OriginId);
                return response;
            }
            DataAccessLayer.ConfigurationSettings configSettings = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string sessionTimeStr = configSettings.GetConfigurationSettingValue(DML.ConfigurationKeys.DDID_SESSION_TIME);
            int sessionTime;
            if (!int.TryParse(sessionTimeStr, out sessionTime) || sessionTime < 1)
            {
                sessionTime = 15;
            }
            DataAccessLayer.DDIDAllocation allocationDal = new NoProblem.Core.DataAccessLayer.DDIDAllocation(XrmDataContext);
            DataModel.Xrm.new_ddidallocation allocation = allocationDal.FindExistingAllocation(request.RealPhone, headingId, webSiteId);
            if (allocation == null)
            {
                allocation = CreateAllocation(request.RealPhone, headingId, webSiteId, sessionTime, allocationDal);
            }
            else
            {
                allocation.new_expireson = DateTime.Now.AddMinutes(sessionTime);
                allocationDal.Update(allocation);
                XrmDataContext.SaveChanges();
            }
            if (allocation == null)
            {
                CreateAllocationFailureDelegate del = new CreateAllocationFailureDelegate(CreateAllocationFailure);
                del.BeginInvoke(webSiteId, headingId, request.RealPhone, null, null);
                response.DID = request.RealPhone;
                response.NotMappedReason = "no-free-dids";
                LogUtils.MyHandle.WriteToLog("No more free dids for headingCode {0}", request.HeadingCode);
            }
            else
            {
                CreateExposureDelegate del = new CreateExposureDelegate(CreateExposure);
                del.BeginInvoke(allocation, request, null, null);
                response.IsMapped = true;
                response.DID = allocation.new_dynamicdid_ddidallocation.new_number;
            }
            return response;
        }

        public void CleanAllocations()
        {
            DataAccessLayer.DDIDAllocation dal = new NoProblem.Core.DataAccessLayer.DDIDAllocation(XrmDataContext);
            dal.CleanAllocations();
        }

        #endregion

        #region Private Methods

        private NoProblem.Core.DataModel.Xrm.new_ddidallocation CreateAllocation(string realPhone, Guid headingId, Guid webSiteId, int sessionTime, DataAccessLayer.DDIDAllocation allocationDal)
        {
            DataModel.Xrm.new_ddidallocation allocation = new NoProblem.Core.DataModel.Xrm.new_ddidallocation();
            allocation.new_expireson = DateTime.Now.AddMinutes(sessionTime);
            allocation.new_websiteid = webSiteId;
            allocation.new_primaryexpertiseid = headingId;
            allocation.new_realphone = realPhone;
            allocation.new_lockcount = 0;
            DataAccessLayer.DynamicDID dynamicDidDal = new NoProblem.Core.DataAccessLayer.DynamicDID(XrmDataContext);
            Guid dynamicDidId = dynamicDidDal.FindFreeDynamicDID(headingId);
            if (dynamicDidId != Guid.Empty)
            {
                allocation.new_dynamicdidid = dynamicDidId;
                allocationDal.Create(allocation);
                XrmDataContext.SaveChanges();
            }
            else
            {
                allocation = null;
            }
            return allocation;
        }

        private void CreateAllocationFailure(Guid webSiteId, Guid headingId, string realPhone)
        {
            try
            {
                DataAccessLayer.DDIDAllocationFailure dal = new NoProblem.Core.DataAccessLayer.DDIDAllocationFailure(null);
                DataModel.Xrm.new_ddidallocatoinfailure failure = new NoProblem.Core.DataModel.Xrm.new_ddidallocatoinfailure();
                failure.new_realphone = realPhone;
                failure.new_primaryexpertiseid = headingId;
                failure.new_websiteid = webSiteId;
                dal.Create(failure);
                dal.XrmDataContext.SaveChanges();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateAllocationFailure. webSiteId:{0}, headingId:{1}, realPhone:{2}", webSiteId, headingId, realPhone);
            }
        }

        private void CreateExposure(NoProblem.Core.DataModel.Xrm.new_ddidallocation allocation, NoProblem.Core.DataModel.DynamicDIDs.AllocationRequest request)
        {
            try
            {               
                
                DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(null);
                DataModel.Xrm.DataContext localAsyncContext = regionDal.XrmDataContext;
                DML.SqlHelper.RegionMinData regionData = null;
                if (!string.IsNullOrEmpty(request.RegionCode))
                {
                    regionData = regionDal.GetRegionMinDataByCode(request.RegionCode);
                }
                DataModel.Xrm.new_ddidexposure exposure = new NoProblem.Core.DataModel.Xrm.new_ddidexposure();
                exposure.new_primaryexpertiseid = allocation.new_primaryexpertiseid;
                exposure.new_realphone = allocation.new_realphone;
                exposure.new_dynamicdid = allocation.new_dynamicdid_ddidallocation.new_number;
                exposure.new_dynamicdidid = allocation.new_dynamicdidid;
                exposure.new_expireson = allocation.new_expireson;
                exposure.new_originname = allocation.new_website_ddidallocation.new_origin_website.new_name;
                exposure.new_primaryexpertisename = allocation.new_primaryexpertise_ddidallocation.new_name;
                if (regionData != null)
                {
                    exposure.new_regionid = regionData.Id;
                    exposure.new_regionlevel = regionData.Level;
                    exposure.new_regionname = regionData.Name;
                }
                exposure.new_url = request.Url;
                exposure.new_websiteid = allocation.new_websiteid;
                exposure.new_websitename = allocation.new_website_ddidallocation.new_name;
                DataAccessLayer.DDIDExposure exposureDal = new NoProblem.Core.DataAccessLayer.DDIDExposure(localAsyncContext);
                exposureDal.Create(exposure);
                localAsyncContext.SaveChanges();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateExposure. webSiteId:{0}, headingId:{1}, realPhone:{2}", allocation.new_websiteid, allocation.new_primaryexpertiseid, request.RealPhone);
            }
        }

        #endregion
    }
}
