﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Utils
{
    internal class WavToMp3Converter : XrmUserBase
    {
        public WavToMp3Converter(DataModel.Xrm.DataContext xrmDataContext):
            base(xrmDataContext)
        {
            configs = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
        }

        private DataAccessLayer.ConfigurationSettings configs;

        public bool ConvertWavMP3(string file)
        {
            bool retVal = false;
            try
            {
                string lame = configs.GetConfigurationSettingValue(DataModel.ConfigurationKeys.LAME);
                if (!string.IsNullOrEmpty(lame))
                {
                    string outfile = "-b 32 --resample 22.05 -m m \"" + file + "\" \"" + file.Replace(".wav", ".mp3") + "\"";
                    System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
                    psi.FileName = "\"" + lame + "\"";
                    psi.Arguments = outfile;
                    psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;
                    System.Diagnostics.Process p = System.Diagnostics.Process.Start(psi);
                    retVal = true;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ConvertWavMP3");
                retVal = false;
            }
            return retVal;
        }
    }
}
