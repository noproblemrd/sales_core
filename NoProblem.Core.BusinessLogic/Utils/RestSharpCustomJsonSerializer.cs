﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Utils
{
    class RestSharpCustomJsonSerializer : RestSharp.Serializers.ISerializer
    {
        public RestSharpCustomJsonSerializer()
        {
            ContentType = "application/json";
        }

        #region ISerializer Members

        public string ContentType
        {
            get;
            set;
        }

        public string DateFormat
        {
            get;
            set;
        }

        public string Namespace
        {
            get;
            set;
        }

        public string RootElement
        {
            get;
            set;
        }

        public string Serialize(object obj)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            return json;
        }

        #endregion
    }
}
