using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Effect.Crm.Configuration;
using NoProblem.Core.DataModel;
using DML = NoProblem.Core.DataModel;
using System.Xml.Serialization;
using System.IO;

namespace NoProblem.Core.BusinessLogic.Utils
{
    /// <summary>
    /// This class as beed deprecated use the OneCallDBLayer for sql services
    /// </summary>
    public class SqlHelper
    {
        string _connString = String.Empty;

        public SqlHelper(string connString)
        {
            _connString = connString;
        }

        public DataSet GetSuppliers(FindSupplierParams searchParams)
        {
            //return GetSuppliersOldStyle(searchParams);
            return GetSuppliersFromStoredProcedure(searchParams);
        }

        private DataSet GetSuppliersFromStoredProcedure(FindSupplierParams searchParams)
        {
            DataSet dsSuppliers = new DataSet();
            using (SqlCommand cmdFindSuppliers = new SqlCommand())
            {
                AddParamsToCommandForStoredProcedure(searchParams, cmdFindSuppliers);
                StringBuilder incidentsParam = new StringBuilder();
                DataTable table = new DataTable();
                table.Columns.Add("id", typeof(Guid));
                if (searchParams.Incidents.Count > 0)
                {
                    for (int i = 0; i < searchParams.Incidents.Count; i++)
                    {
                        table.Rows.Add(searchParams.Incidents[i]);
                    }
                }
                cmdFindSuppliers.Parameters.AddWithValue("@upsales", table);
                cmdFindSuppliers.CommandText = "dbo.GetSuppliers";
                using (SqlConnection conn = new SqlConnection(ConfigurationUtils.GetConfigurationItem("updateConnectionString")))
                {
                    cmdFindSuppliers.Connection = conn;
                    cmdFindSuppliers.CommandTimeout = 120;
                    cmdFindSuppliers.CommandType = CommandType.StoredProcedure;
                    using (SqlDataAdapter dtFindSuppliers = new SqlDataAdapter(cmdFindSuppliers))
                    {
                        dtFindSuppliers.Fill(dsSuppliers);
                    }
                }
            }
            if (dsSuppliers.Tables[0].Columns[0].ColumnName != "ERROR" && dsSuppliers.Tables[0].Rows.Count == 0)
            {
                dsSuppliers.Tables[0].Columns.Clear();
                dsSuppliers.Tables[0].Columns.Add("ERROR");
                DataRow dr = dsSuppliers.Tables[0].NewRow();
                dr[0] = Constants.CONST_ERROR_NOT_AVAILABLE_TODAY;
                dsSuppliers.Tables[0].Rows.Add(dr);
            }
            if (dsSuppliers.Tables[0].Columns[0].ColumnName == "ERROR")//zero sups
            {
                dsSuppliers.Tables[0].Columns.Add("ERRORDESC");
                dsSuppliers.Tables[0].Rows[0][1] = "ERROR";
            }
            return dsSuppliers;
        }

        private DataSet GetSuppliersOldStyle(FindSupplierParams searchParams)
        {
            DataSet dsSuppliers = new DataSet();
            using (SqlCommand cmdFindSuppliers = new SqlCommand())
            {
                AddParamsToCommand(searchParams, cmdFindSuppliers);

                StringBuilder incidentsParam = new StringBuilder();
                int incidentsCount = searchParams.Incidents.Count;
                if (incidentsCount > 0)
                {
                    for (int i = 0; i < incidentsCount; i++)
                    {
                        incidentsParam.Append("'").Append(searchParams.Incidents[i].ToString()).Append("'");
                        if (i < incidentsCount - 1)
                        {
                            incidentsParam.Append(",");
                        }
                    }
                }
                else
                {
                    incidentsParam.Append("'").Append(Guid.Empty.ToString()).Append("'");
                }

                cmdFindSuppliers.CommandText = string.Format(findSuppliersCommand, incidentsParam.ToString());
                using (SqlConnection conn = new SqlConnection(ConfigurationUtils.GetConfigurationItem("updateConnectionString")))
                {
                    cmdFindSuppliers.Connection = conn;
                    cmdFindSuppliers.CommandTimeout = 90;
                    using (SqlDataAdapter dtFindSuppliers = new SqlDataAdapter(cmdFindSuppliers))
                    {
                        dtFindSuppliers.Fill(dsSuppliers);
                    }
                }
            }
            if (dsSuppliers.Tables[0].Columns[0].ColumnName != "ERROR" && dsSuppliers.Tables[0].Rows.Count == 0)
            {
                dsSuppliers.Tables[0].Columns.Clear();
                dsSuppliers.Tables[0].Columns.Add("ERROR");
                DataRow dr = dsSuppliers.Tables[0].NewRow();
                dr[0] = Constants.CONST_ERROR_NOT_AVAILABLE_TODAY;
                dsSuppliers.Tables[0].Rows.Add(dr);
            }
            if (dsSuppliers.Tables[0].Columns[0].ColumnName == "ERROR")//zero sups
            {
                dsSuppliers.Tables[0].Columns.Add("ERRORDESC");
                dsSuppliers.Tables[0].Rows[0][1] = "ERROR";
            }
            return dsSuppliers;
        }

        private void AddParamsToCommandForStoredProcedure(FindSupplierParams searchParams, SqlCommand cmdFindSuppliers)
        {
            /*
             @OriginId  uniqueidentifier = NULL, 
@PrimaryExpertiseCode nvarchar(100) = '2137',
@isAar  bit = 0,
@IsIncludingTrial bit = 0,
@RegionLevel int = 4, zip code level 
@Regioncode nvarchar(100) = '79379', 
@Availability datetime =NULL , 
@DayOfWeek int = 2, 
@DirectNumberRegionId uniqueidentifier = '00000000-0000-0000-0000-000000000000', 
@isWithNoTimeNorCash bit = 0,
@maxTrialAttempts int = 20,
@upsales IdsTable READONLY,
@excludePartners bit = 0 */
            cmdFindSuppliers.Parameters.AddWithValue("@PrimaryExpertiseCode", searchParams.ExpertiseCode);
            cmdFindSuppliers.Parameters.AddWithValue("@RegionCode", searchParams.AreaCode);
            cmdFindSuppliers.Parameters.AddWithValue("@RegionLevel", searchParams.AreaLevel);

            if (searchParams.DirectNumberRegionId != Guid.Empty)
            {
                cmdFindSuppliers.Parameters.AddWithValue("@DirectNumberRegionid", searchParams.DirectNumberRegionId);
            }
            else
            {
                cmdFindSuppliers.Parameters.AddWithValue("@DirectNumberRegionid", DBNull.Value);
            }

            cmdFindSuppliers.Parameters.Add("@Availability", SqlDbType.DateTime);
            cmdFindSuppliers.Parameters["@Availability"].Value = searchParams.Availability;

            cmdFindSuppliers.Parameters.Add("@DayOfWeek", SqlDbType.Int);
            cmdFindSuppliers.Parameters["@DayOfWeek"].Value = ((int)searchParams.Availability.DayOfWeek) + 1; // Adding one, because in the CRM the days numbering begins with 1 and not 0

            cmdFindSuppliers.Parameters.AddWithValue("@OriginId", DBNull.Value);
            if (searchParams.OriginId != Guid.Empty)
                cmdFindSuppliers.Parameters["@OriginId"].Value = searchParams.OriginId;

            cmdFindSuppliers.Parameters.AddWithValue("@isWithNoTimeNorCash", searchParams.IsWithNoTimeNorCash);
            cmdFindSuppliers.Parameters.AddWithValue("@isAar", searchParams.IsAar);
            cmdFindSuppliers.Parameters.AddWithValue("@maxTrialAttempts", searchParams.MaxTrialAttempts <= 0 ? int.MaxValue : searchParams.MaxTrialAttempts);
            cmdFindSuppliers.Parameters.AddWithValue("@IsIncludingTrial", searchParams.IsIncludingTrial);
            cmdFindSuppliers.Parameters.AddWithValue("@excludePartners", searchParams.ExcludePartners);
        }

        private void AddParamsToCommand(FindSupplierParams searchParams, SqlCommand cmdFindSuppliers)
        {
            if (searchParams.ExpertiseLevel == (int)enumExpertiseType.Primary)
            {
                cmdFindSuppliers.Parameters.AddWithValue("@PrimaryExpertiseCode", searchParams.ExpertiseCode);
                cmdFindSuppliers.Parameters.AddWithValue("@SecondaryExpertiseCode", DBNull.Value);
            }
            else
            {
                cmdFindSuppliers.Parameters.AddWithValue("@PrimaryExpertiseCode", DBNull.Value);
                cmdFindSuppliers.Parameters.AddWithValue("@SecondaryExpertiseCode", searchParams.ExpertiseCode);
            }

            if (searchParams.AreaLevel == (int)enumServiceAreaType.ZipCode)
            {
                cmdFindSuppliers.Parameters.AddWithValue("@ZipCode", searchParams.AreaCode);
                cmdFindSuppliers.Parameters.AddWithValue("@RegionCode", DBNull.Value);
                cmdFindSuppliers.Parameters.AddWithValue("@RegionLevel", DBNull.Value);
            }
            else
            {
                cmdFindSuppliers.Parameters.AddWithValue("@ZipCode", DBNull.Value);
                cmdFindSuppliers.Parameters.AddWithValue("@RegionCode", searchParams.AreaCode);
                cmdFindSuppliers.Parameters.AddWithValue("@RegionLevel", searchParams.AreaLevel);
            }

            if (searchParams.DirectNumberRegionId != Guid.Empty)
            {
                cmdFindSuppliers.Parameters.AddWithValue("@DirectNumberRegionid", searchParams.DirectNumberRegionId);
            }
            else
            {
                cmdFindSuppliers.Parameters.AddWithValue("@DirectNumberRegionid", DBNull.Value);
            }

            cmdFindSuppliers.Parameters.Add("@Availability", SqlDbType.DateTime);
            cmdFindSuppliers.Parameters["@Availability"].Value = searchParams.Availability;

            cmdFindSuppliers.Parameters.Add("@DayOfWeek", SqlDbType.Int);
            cmdFindSuppliers.Parameters["@DayOfWeek"].Value = ((int)searchParams.Availability.DayOfWeek) + 1; // Adding one, because in the CRM the days numbering begins with 1 and not 0

            cmdFindSuppliers.Parameters.AddWithValue("@OriginId", DBNull.Value);
            if (searchParams.OriginId != Guid.Empty)
                cmdFindSuppliers.Parameters["@OriginId"].Value = searchParams.OriginId;

            cmdFindSuppliers.Parameters.AddWithValue("@isWithNoTimeNorCash", searchParams.IsWithNoTimeNorCash);
            cmdFindSuppliers.Parameters.AddWithValue("@isAar", searchParams.IsAar);
            cmdFindSuppliers.Parameters.AddWithValue("@maxTrialAttempts", searchParams.MaxTrialAttempts <= 0 ? int.MaxValue : searchParams.MaxTrialAttempts);
            cmdFindSuppliers.Parameters.AddWithValue("@IsIncludingTrial", searchParams.IsIncludingTrial);
        }

        public string GetIncidentPackageListAsXml()
        {
            string retPackages = "";
            string query = @"SELECT new_pricingpackageid AS PackageId,new_name AS Name,new_freepricingunitamount AS FreeAmount,
new_pricingunitamount AS PaidAmount
FROM new_pricingpackage IncidentPackage with (nolock)
WHERE deletionstatecode = 0 AND statecode = 0
FOR XML AUTO";

            using (SqlConnection con = new SqlConnection(_connString))
            {
                using (SqlCommand comSel = new SqlCommand(query, con))
                {
                    con.Open();
                    object ret = comSel.ExecuteScalar();
                    retPackages = @"<IncidentPackages>" + ret.ToString() + "</IncidentPackages>";
                }
            }
            return retPackages;
        }

        #region super queries

        #region normal

        private const string findSuppliersCommand = @"

IF(@OriginId IS NULL)
BEGIN
	SELECT TOP 1 @OriginId = new_originid FROM new_origin with (nolock) WHERE new_isdirectnumber = 1
END
--declare @PrimaryExpertiseCode nvarchar(4)
--set @PrimaryExpertiseCode = '2137'
DECLARE @IsAuction bit
DECLARE @PrimaryExpertiseId uniqueidentifier
SELECT @PrimaryExpertiseId = new_primaryexpertiseid,@IsAuction = new_isauction
FROM New_primaryexpertiseExtensionBase with (nolock)
WHERE new_code = @PrimaryExpertiseCode

---------------------------------------------------------------------------------

create table #TempMatchPrimaryExpertise
	(New_AccountExpertiseId uniqueidentifier primary key, hasCash bit, accountId uniqueidentifier)
	
	create index idx_nu_na_tempmatch on #TempMatchPrimaryExpertise(accountId)include(New_AccountExpertiseId,hasCash)

/*** check for suppliers that match to @PrimaryExpertiseId ****/
insert into #TempMatchPrimaryExpertise (New_AccountExpertiseId, hasCash, accountId)
SELECT tempAccounts.New_AccountExpertiseId, tempAccounts.hasCash, tempAccounts.new_accountid
FROM 


(SELECT ae.New_AccountExpertiseId
, CASE WHEN ae.new_incidentprice <= Account.new_availablecashbalance THEN 1 ELSE 0 END AS hasCash
, ae.new_accountid, ae.new_incidentprice
FROM Account with (nolock)
INNER JOIN New_AccountExpertise ae with (nolock) ON ae.New_accountId = Account.AccountId AND 
	ae.deletionstatecode = 0 AND ae.statecode = 0 AND ae.new_primaryexpertiseid = @PrimaryExpertiseId
WHERE 	
(Account.telephone1 IS NOT NULL) AND Account.AccountId NOT IN
	(SELECT New_accountId FROM New_incidentaccount WHERE New_incidentId in ( {0} ) 
	AND (statuscode in (12, 10, 13) or @isAar = 1) AND deletionstatecode = 0)
    and (isnull(Account.new_firstpaymentneeded, 0)= 0)
    and (isnull(Account.new_waitingforinvitation, 0) = 0)
    AND (isnull(Account.new_dapazstatus, 1) = 1)
	AND ((Account.new_status = 1 and @isAar = 0) or (@IsIncludingTrial = 1 and (Account.new_status = 1 or (Account.new_status = 4 and ((Account.new_trialstatusreason = 3 and (new_trialattempts < @maxTrialAttempts or new_trialattempts is null)) or Account.new_trialstatusreason = 4) ))) or (@isAar = 1 and Account.new_status = 4 and ((Account.new_trialstatusreason = 3 and (new_trialattempts < @maxTrialAttempts or new_trialattempts is null)) or Account.new_trialstatusreason = 4)))
) tempAccounts 
--order by tempAccounts.New_IncidentPrice desc




IF @@ROWCOUNT = 0
begin
	select -1 ERROR
	return
end

----------------------------------------------------------------------------------------

create table #TempMatchCity
	(New_AccountExpertiseId uniqueidentifier primary key, hasCash bit)

create table #tmpRegionHirarchy ( RegionLevel int, RegionId uniqueidentifier, ParentRegionId uniqueidentifier );
create index idx_region on #tmpRegionHirarchy(RegionId) ;

WITH RegionHirarchy(RegionLevel,RegionId,ParentRegionId)
AS
(
	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
	FROM new_region with(nolock)
	WHERE new_region.new_level = @RegionLevel AND new_code = @RegionCode and new_region.deletionstatecode = 0
	UNION ALL
	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
	FROM new_region with(nolock)
	INNER JOIN RegionHirarchy ON RegionHirarchy.ParentRegionId = new_regionid
)
insert into #tmpRegionHirarchy
	select RegionLevel,  RegionId,  ParentRegionId from RegionHirarchy
	
	create table #tmpJoinning ( AccountExpertiseId uniqueidentifier, RegionId uniqueidentifier, RegionState int, hasCash bit)
	insert into #tmpJoinning
			select #TempMatchPrimaryExpertise.New_AccountExpertiseId , regaccount.new_regionid, regaccount.New_regionstate, #TempMatchPrimaryExpertise.hasCash
	from #TempMatchPrimaryExpertise
	INNER JOIN(
	select a.new_regionid, a.New_regionstate, a.new_accountid, b.deletionstatecode
	from dbo.New_regionaccountexpertiseExtensionBase a
	join dbo.New_regionaccountexpertiseBase b
	on a.New_regionaccountexpertiseId = b.New_regionaccountexpertiseId
	where b.deletionstatecode = 0)
	
	

 --new_regionaccountexpertise \
 
 regaccount  ON regaccount.new_accountid = #TempMatchPrimaryExpertise.accountId --AND 	regaccount.deletionstatecode = 0 
INNER JOIN #tmpRegionHirarchy ON #tmpRegionHirarchy.RegionId = regaccount.new_regionid	 
	
	create table #tmpNotGoodCity (accountexpertise uniqueidentifier)
	insert into #tmpNotGoodCity
	select #tmpJoinning.AccountExpertiseId
	from #tmpJoinning	
	where
     (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel = @RegionLevel) and #tmpJoinning.Regionstate != 1)
     or (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel != @RegionLevel) and #tmpJoinning.Regionstate = 2)
		
insert into #TempMatchCity (New_AccountExpertiseId, hasCash)
SELECT distinct #tmpJoinning.AccountExpertiseId, #tmpJoinning.hasCash
FROM #tmpJoinning
where
	#tmpJoinning.AccountexpertiseId not in ( select accountexpertise from #tmpNotGoodCity )

IF @@ROWCOUNT = 0
begin
	select -4 ERROR
	return
end	

------------------------------------------------------------------

create table #TempAvailabilityDate
	(New_AccountId uniqueidentifier,New_AccountExpertiseId uniqueidentifier,New_PrimaryExpertiseId uniqueidentifier, isAvailable bit, hasCash bit)

/*** check for suppliers that match to @AvailabilityDate ****/
insert into #TempAvailabilityDate 
SELECT distinct New_AccountExpertiseExtensionBase.New_accountId,#TempMatchCity.New_AccountExpertiseId,New_AccountExpertiseExtensionBase.new_primaryexpertiseid
,case when sum(CASE WHEN 
    (account.new_unavailablefrom > @Availability OR account.new_unavailableto < @Availability OR
	(account.new_unavailablefrom IS NULL AND account.new_unavailableto IS NULL))
    AND
    (new_from <= CONVERT(char(8),@Availability,108) AND new_to >= CONVERT(char(8),@Availability,108))
THEN 1 ELSE 0 END) > 0 then 1 else 0 end as isAvailable
,#TempMatchCity.hasCash
FROM #TempMatchCity
INNER JOIN New_AccountExpertiseExtensionBase with (nolock)
			ON New_AccountExpertiseExtensionBase.New_AccountExpertiseId = #TempMatchCity.New_AccountExpertiseId
LEFT JOIN new_availability with (nolock) ON (new_availability.new_accountid = New_AccountExpertiseExtensionBase.new_accountid
		AND new_availability.deletionstatecode = 0 AND new_day = @DayOfWeek)
INNER JOIN account with (nolock) ON account.accountid = New_AccountExpertiseExtensionBase.new_accountid 
group by 
New_AccountExpertiseExtensionBase.New_accountId,#TempMatchCity.New_AccountExpertiseId,New_AccountExpertiseExtensionBase.new_primaryexpertiseid
,#TempMatchCity.hasCash
IF @@ROWCOUNT = 0
begin
	select -5 ERROR
	return
end	

------------------------------------------------------------------

/*** return match suppliers by social fairness order ****/
SELECT	DISTINCT Account.AccountId
		,Account.accountnumber
		,Account.[Name]
		,Account.new_unavailablefrom
		,Account.new_unavailableto
		,Account.new_sumsurvey
		,Account.new_sumassistancerequests
		,Account.numberofemployees
		,Account.new_description
		,Account.emailaddress1
		,Account.telephone1
		,Account.telephone2
        ,Account.new_trialstatusreason
        ,Account.new_trialattempts
        ,Account.new_trialcallsanswered
        ,Account.new_stageintrialregistration
        ,Account.new_availablecashbalance
        ,Account.new_initialtrialbudget
		,new_accountexpertise.new_certificate
		,New_incidentaccount.CreatedOn AS IncidentAccountsCreatedOn
		,Account.CreatedOn AS AccountCreatedOn
        ,case when MatchedSuppliers.isAvailable = 1 and MatchedSuppliers.hasCash = 1
            then new_directnumber.new_number else '0' end as new_directnumber
		,@IsAuction AS IsIncidentAuction
		,MatchedSuppliers.New_AccountExpertiseId
        ,new_accountexpertise.new_incidentprice
        ,isAvailable
        ,new_accountexpertise.new_primaryexpertiseid as PrimaryExpertiseId
FROM	#TempAvailabilityDate MatchedSuppliers		
		INNER JOIN new_accountexpertise with (nolock) ON new_accountexpertise.new_accountexpertiseid = MatchedSuppliers.new_accountexpertiseid
		INNER JOIN Account with (nolock) ON MatchedSuppliers.new_accountid = Account.accountid		
		LEFT OUTER JOIN new_directnumber with (nolock) ON new_directnumber.new_accountexpertiseid = MatchedSuppliers.New_AccountExpertiseId AND
						new_directnumber.new_originid = @OriginId AND new_directnumber.deletionstatecode = 0 AND new_directnumber.statecode = 0
                        and (new_directnumber.new_regionid = @DirectNumberRegionId or @DirectNumberRegionId is null)
		LEFT JOIN (SELECT   New_accountId,max(CreatedOn)AS  CreatedOn
					FROM	New_incidentaccount with (nolock)
					GROUP BY New_accountId
					) AS New_incidentaccount
			ON New_incidentaccount.New_accountId = MatchedSuppliers.New_accountId
where 
((isAvailable = 1 and hasCash = 1)
or 
@isWithNoTimeNorCash = 1)
Order By new_accountexpertise.new_incidentprice DESC,New_incidentaccount.CreatedOn ASC
";

        #endregion

        #region old query
        // This is the old query

        //        private const string findSuppliersCommand = @" 
        //IF(@OriginId IS NULL)
        //BEGIN
        //	SELECT TOP 1 @OriginId = new_originid FROM new_origin with (nolock) WHERE new_isdirectnumber = 1
        //END
        //
        //DECLARE @IsAuction bit
        //DECLARE @PrimaryExpertiseId uniqueidentifier
        //DECLARE @SecondaryExpertiseCount int
        //
        //IF(@PrimaryExpertiseCode IS NULL)
        //BEGIN
        //	SELECT @PrimaryExpertiseCode = pe.new_code,@SecondaryExpertiseCount = COUNT(*)
        //	FROM new_primaryexpertise pe with (nolock)
        //	INNER JOIN new_secondaryexpertise se with (nolock) ON pe.new_primaryexpertiseid = se.new_primaryexpertiseid AND se.new_code = @SecondaryExpertiseCode
        //	GROUP BY pe.new_code
        //END
        //ELSE
        //BEGIN
        //	SELECT @SecondaryExpertiseCount = COUNT(*)
        //	FROM new_primaryexpertise pe with (nolock)
        //	INNER JOIN new_secondaryexpertise se with (nolock) ON pe.new_primaryexpertiseid = se.new_primaryexpertiseid
        //	WHERE pe.new_code = @PrimaryExpertiseCode
        //END
        //
        //SELECT @PrimaryExpertiseId = new_primaryexpertiseid,@IsAuction = new_isauction
        //FROM new_primaryexpertise with (nolock)
        //WHERE new_code = @PrimaryExpertiseCode
        //
        //---------------------------------------------------------------------------------
        //
        //create table #TempMatchPrimaryExpertise
        //	(New_AccountExpertiseId uniqueidentifier primary key, hasCash bit, accountId uniqueidentifier)
        //
        ///*** check for suppliers that match to @PrimaryExpertiseId ****/
        //insert into #TempMatchPrimaryExpertise (New_AccountExpertiseId, hasCash, accountId)
        //SELECT tempAccounts.New_AccountExpertiseId, tempAccounts.hasCash, tempAccounts.new_accountid
        //FROM 
        //(SELECT ae.New_AccountExpertiseId--,COUNT(*) AS SecondaryAmount
        //, CASE WHEN ae.new_incidentprice <= Account.new_availablecashbalance THEN 1 ELSE 0 END AS hasCash
        //, ae.new_accountid
        //FROM Account with (nolock)
        //INNER JOIN New_AccountExpertise ae with (nolock) ON ae.New_accountId = Account.AccountId AND 
        //	ae.deletionstatecode = 0 AND ae.statecode = 0 AND ae.new_primaryexpertiseid = @PrimaryExpertiseId
        //WHERE 	
        //(Account.telephone1 IS NOT NULL) AND Account.AccountId NOT IN
        //	(SELECT New_accountId FROM New_incidentaccount WHERE New_incidentId in ( {0} ) AND (statuscode = 12 OR statuscode = 10 OR statuscode = 13 or @isAar = 1) AND deletionstatecode = 0)
        //    and (Account.new_firstpaymentneeded is null or Account.new_firstpaymentneeded = 0)
        //    and (Account.new_waitingforinvitation is null or Account.new_waitingforinvitation = 0)
        //	AND ((Account.new_status = 1 and @isAar = 0) or (@IsIncludingTrial = 1 and (Account.new_status = 1 or (Account.new_status = 4 and ((Account.new_trialstatusreason = 3 and (new_trialattempts < @maxTrialAttempts or new_trialattempts is null)) or Account.new_trialstatusreason = 4) ))) or (@isAar = 1 and Account.new_status = 4 and ((Account.new_trialstatusreason = 3 and (new_trialattempts < @maxTrialAttempts or new_trialattempts is null)) or Account.new_trialstatusreason = 4)))
        //) tempAccounts 
        //
        //IF @@ROWCOUNT = 0
        //begin
        //	select -1 ERROR
        //	return
        //end
        //
        //----------------------------------------------------------------------------------------
        //
        //create table #TempMatchCity
        //	(New_AccountExpertiseId uniqueidentifier primary key, hasCash bit)
        //
        //IF (@ZipCode IS NOT NULL) BEGIN
        //	--SELECT @RegionCode = new_region.new_code, @RegionLevel = new_region.new_level
        //	--FROM new_zipcode with (nolock)
        //	--INNER JOIN new_region with (nolock) ON new_region.new_regionid = new_zipcode.new_regionid
        //	--WHERE new_zipcode.new_name = @ZipCode and new_zipcode.deletionstatecode = 0  and new_region.deletionstatecode = 0
        //WITH RegionHirarchy(RegionLevel,RegionId,ParentRegionId)
        //AS
        //(
        //	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
        //	FROM new_region with(nolock)
        //	WHERE new_region.deletionstatecode = 0 and new_region.new_regionId in (
        //				SELECT new_zipcode.new_regionId
        //				FROM new_zipcode
        //				INNER JOIN new_region ON new_region.new_regionid = new_zipcode.new_regionid
        //				WHERE new_zipcode.new_name = @ZipCode)
        //	UNION ALL
        //	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
        //	FROM new_region with(nolock)
        //	INNER JOIN RegionHirarchy ON RegionHirarchy.ParentRegionId = new_regionid
        //)
        //insert into #TempMatchCity (New_AccountExpertiseId, hasCash)
        //SELECT distinct ae.new_accountexpertiseid, #TempMatchPrimaryExpertise.hasCash
        //FROM #TempMatchPrimaryExpertise
        //INNER JOIN new_accountexpertise ae with (nolock) ON ae.new_accountexpertiseid = #TempMatchPrimaryExpertise.new_accountexpertiseid
        //
        //INNER JOIN new_regionaccountexpertise regaccount with (nolock) ON regaccount.new_accountid = ae.new_accountid AND 
        //	regaccount.deletionstatecode = 0 AND (regaccount.new_regionstate != 2)
        //INNER JOIN RegionHirarchy ON RegionHirarchy.RegionId = regaccount.new_regionid	
        //
        //IF @@ROWCOUNT = 0
        //begin
        //	select -4 ERROR
        //	return
        //end	
        //END
        //ELSE BEGIN
        //
        //create table #tmpRegionHirarchy ( RegionLevel int, RegionId uniqueidentifier, ParentRegionId uniqueidentifier );
        //WITH RegionHirarchy(RegionLevel,RegionId,ParentRegionId)
        //AS
        //(
        //	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
        //	FROM new_region with(nolock)
        //	WHERE new_region.new_level = @RegionLevel AND new_code = @RegionCode and new_region.deletionstatecode = 0
        //	UNION ALL
        //	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
        //	FROM new_region with(nolock)
        //	INNER JOIN RegionHirarchy ON RegionHirarchy.ParentRegionId = new_regionid
        //)
        //insert into #tmpRegionHirarchy
        //	select RegionLevel,  RegionId,  ParentRegionId from RegionHirarchy
        //	
        //	create table #tmpJoinning ( AccountExpertiseId uniqueidentifier, RegionId uniqueidentifier, RegionState int, hasCash bit)
        //	insert into #tmpJoinning
        //			select #TempMatchPrimaryExpertise.New_AccountExpertiseId , regaccount.new_regionid, regaccount.New_regionstate, #TempMatchPrimaryExpertise.hasCash
        //	from #TempMatchPrimaryExpertise
        //INNER JOIN new_regionaccountexpertise regaccount with (nolock) ON regaccount.new_accountid = #TempMatchPrimaryExpertise.accountId AND 
        //	regaccount.deletionstatecode = 0 
        //INNER JOIN #tmpRegionHirarchy ON #tmpRegionHirarchy.RegionId = regaccount.new_regionid	 
        //	
        //	create table #tmpNotGoodCity (accountexpertise uniqueidentifier)
        //	insert into #tmpNotGoodCity
        //	select #tmpJoinning.AccountExpertiseId
        //	from #tmpJoinning	
        //	where
        //     (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel = @RegionLevel) and #tmpJoinning.Regionstate != 1)
        //     or (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel != @RegionLevel) and #tmpJoinning.Regionstate = 2)
        //		
        //insert into #TempMatchCity (New_AccountExpertiseId, hasCash)
        //SELECT distinct #tmpJoinning.AccountExpertiseId, #tmpJoinning.hasCash
        //FROM #tmpJoinning
        //where
        //	#tmpJoinning.AccountexpertiseId not in ( select accountexpertise from #tmpNotGoodCity )
        //
        //IF @@ROWCOUNT = 0
        //begin
        //	select -4 ERROR
        //	return
        //end	
        //END
        //
        //------------------------------------------------------------------
        //
        //
        //create table #TempAvailabilityDate
        //	(New_AccountId uniqueidentifier,New_AccountExpertiseId uniqueidentifier,New_PrimaryExpertiseId uniqueidentifier, isAvailable bit, hasCash bit)
        //
        ///*** check for suppliers that match to @AvailabilityDate ****/
        //insert into #TempAvailabilityDate 
        //SELECT distinct New_AccountExpertiseExtensionBase.New_accountId,#TempMatchCity.New_AccountExpertiseId,New_AccountExpertiseExtensionBase.new_primaryexpertiseid
        //,case when sum(CASE WHEN 
        //    (account.new_unavailablefrom > @Availability OR account.new_unavailableto < @Availability OR
        //	(account.new_unavailablefrom IS NULL AND account.new_unavailableto IS NULL))
        //    AND
        //    (new_from <= CONVERT(char(8),@Availability,108) AND new_to >= CONVERT(char(8),@Availability,108))
        //THEN 1 ELSE 0 END) > 0 then 1 else 0 end as isAvailable
        //,#TempMatchCity.hasCash
        //FROM #TempMatchCity
        //INNER JOIN New_AccountExpertiseExtensionBase with (nolock)
        //			ON New_AccountExpertiseExtensionBase.New_AccountExpertiseId = #TempMatchCity.New_AccountExpertiseId
        //LEFT JOIN new_availability with (nolock) ON (new_availability.new_accountid = New_AccountExpertiseExtensionBase.new_accountid
        //		AND new_availability.deletionstatecode = 0 AND new_day = @DayOfWeek)
        //INNER JOIN account with (nolock) ON account.accountid = New_AccountExpertiseExtensionBase.new_accountid 
        //group by 
        //New_AccountExpertiseExtensionBase.New_accountId,#TempMatchCity.New_AccountExpertiseId,New_AccountExpertiseExtensionBase.new_primaryexpertiseid
        //,#TempMatchCity.hasCash
        //IF @@ROWCOUNT = 0
        //begin
        //	select -5 ERROR
        //	return
        //end	
        //
        //------------------------------------------------------------------
        //
        ///*** return match suppliers by social fairness order ****/
        //SELECT	DISTINCT Account.AccountId
        //		,Account.accountnumber
        //		,Account.[Name]
        //		,sp.new_pricingmethodidName		
        //		,sp.new_incidentpriceincludefactor
        //		,Account.new_unavailablefrom
        //		,Account.new_unavailableto
        //		,Account.new_sumsurvey
        //		,Account.new_sumassistancerequests
        //		,Account.numberofemployees
        //		,Account.new_description
        //		,Account.emailaddress1
        //		,Account.telephone1
        //		,Account.telephone2
        //        ,Account.new_trialstatusreason
        //        ,Account.new_trialattempts
        //        ,Account.new_trialcallsanswered
        //        ,Account.new_stageintrialregistration
        //        ,Account.new_availablecashbalance
        //        ,Account.new_initialtrialbudget
        //		,new_accountexpertise.new_certificate
        //		,New_incidentaccount.CreatedOn AS IncidentAccountsCreatedOn
        //		,Account.CreatedOn AS AccountCreatedOn
        //        ,case when MatchedSuppliers.isAvailable = 1 and MatchedSuppliers.hasCash = 1
        //            then new_directnumber.new_number else '0' end as new_directnumber
        //		,@IsAuction AS IsIncidentAuction
        //		,sp.new_isauction AS IsSupplierAuction
        //		,MatchedSuppliers.New_AccountExpertiseId
        //        ,new_accountexpertise.new_incidentprice
        //        ,isAvailable
        //        ,new_accountexpertise.new_primaryexpertiseid as PrimaryExpertiseId
        //FROM	#TempAvailabilityDate MatchedSuppliers		
        //		INNER JOIN new_accountexpertise with (nolock) ON new_accountexpertise.new_accountexpertiseid = MatchedSuppliers.new_accountexpertiseid
        //		INNER JOIN Account with (nolock) ON MatchedSuppliers.new_accountid = Account.accountid AND account.deletionstatecode = 0		
        //		LEFT JOIN (SELECT sp.new_pricingmethodidName,sp.new_accountid,
        //							new_incidentpriceincludefactor,pm.new_isauction
        //					FROM new_supplierPricing sp with (nolock)
        //					INNER JOIN new_accountexpertise_new_supplierpricing aesp with (nolock) ON aesp.new_supplierPricingid = sp.new_supplierPricingid
        //					INNER JOIN new_accountexpertise ae with (nolock) ON ae.new_accountexpertiseid = aesp.new_accountexpertiseid
        //					INNER JOIN new_pricingmethod pm with (nolock) ON pm.new_pricingmethodid = sp.new_pricingmethodid
        //					WHERE ae.new_primaryexpertiseid = @PrimaryExpertiseId ) sp 
        //			ON sp.new_accountid = Account.AccountId
        //		LEFT OUTER JOIN new_directnumber with (nolock) ON new_directnumber.new_accountexpertiseid = MatchedSuppliers.New_AccountExpertiseId AND
        //						new_directnumber.new_originid = @OriginId AND new_directnumber.deletionstatecode = 0 AND new_directnumber.statecode = 0
        //                        and (new_directnumber.new_regionid = @DirectNumberRegionId or @DirectNumberRegionId is null)
        //		LEFT JOIN (SELECT   New_accountId,New_accountexpertiseId,max(CreatedOn)AS  CreatedOn
        //					FROM	New_incidentaccount with (nolock)
        //					GROUP BY New_accountId,New_accountexpertiseId
        //					) AS New_incidentaccount
        //			ON New_incidentaccount.New_accountId = MatchedSuppliers.New_accountId
        //			and New_incidentaccount.New_accountexpertiseId = MatchedSuppliers.New_AccountExpertiseId
        //where 
        //((isAvailable = 1 and hasCash = 1)
        //or 
        //@isWithNoTimeNorCash = 1)
        //Order By new_accountexpertise.new_incidentprice DESC,New_incidentaccount.CreatedOn ASC";

        #endregion

        #endregion
    }
}