﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Utils
{
    public static class CoreAppExtensionMethod
    {
        public static bool TryParseToGuid(this string id, out Guid guid)
        {
            try
            {
                guid = new Guid(id);
            }
            catch (Exception)
            {
                guid = Guid.Empty;
                return false;
            }
            return true;
        }

        public static bool IsTrue(this bool? b)
        {
            return b.HasValue && b.Value;
        }
    }
}
