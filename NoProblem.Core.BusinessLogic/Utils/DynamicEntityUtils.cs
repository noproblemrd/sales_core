﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using System.Reflection;
using Effect.Crm.Convertor;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Utils
{
    class DynamicEntityUtils
    {
        #region GetDynamicEntity
        public static DynamicEntity GetDynamicEntity(CrmService service, string entityTypeName, Guid instanceID)
        {
            BusinessEntity[] entities;
            switch (entityTypeName)
            {
                case "task":
                case "phonecall":
                case "fax":
                case "email":
                case "letter":
                case "appointment":
                case "serviceappointment":
                case "campaignresponse":
                    entities = Effect.Crm.Methods.MethodsUtils.FindEntities(service, entityTypeName, new string[] { "activityid" }, new Object[] { instanceID }, "", "", true);
                    break;
                default:
                    entities = Effect.Crm.Methods.MethodsUtils.FindEntities(service, entityTypeName, new string[] { entityTypeName + "id" }, new Object[] { instanceID }, "", "", true);
                    break;
            }
            if (entities != null && entities.Length > 0)
                return (DynamicEntity)entities[0];
            return null;
        }
        #endregion

        #region GetDynamicEntity
        public static DynamicEntity[] GetDynamicEntity(CrmService service, string entityTypeName, string[] criterias, object[] values)
        {
            Effect.Crm.Logs.LogUtils.MyHandle.WriteToLog(8, "GetDynamicEntity entity={0}", entityTypeName);
            BusinessEntity[] entities = Effect.Crm.Methods.MethodsUtils.FindEntities(service, entityTypeName, criterias, values, "", "", true);

            if (entities != null && entities.Length > 0)
            {
                List<DynamicEntity> list = new List<DynamicEntity>();
                foreach (BusinessEntity entity in entities)
                {
                    list.Add((DynamicEntity)entity);
                }

                return list.ToArray();
            }
            return null;
        }
        #endregion

        #region GetProperty
        public static Property GetProperty(DynamicEntity entity, string propertyName)
        {
            if (entity == null)
                return null;
            for (int i = 0; i < entity.Properties.Length; i++)
                if (entity.Properties[i].Name == propertyName)
                    return entity.Properties[i];
            return null;
        }
        #endregion

        #region GetNew
        public static DynamicEntity GetNew(string entityTypeName)
        {
            DynamicEntity rslt = new DynamicEntity();
            rslt.Name = entityTypeName;
            rslt.Properties = new Property[] { };
            return rslt;
        }
        #endregion



        #region CloseLack
        public static void SetStateDynamicEntity(CrmService pService, Guid entityId, string entityname, string state, int status)
        {
            try
            {
                SetStateDynamicEntityRequest req = new SetStateDynamicEntityRequest();
                Moniker mon = new Moniker();
                mon.Id = entityId;
                mon.Name = entityname;
                req.Entity = mon;
                req.State = state;
                req.Status = status;
                SetStateDynamicEntityResponse resp = (SetStateDynamicEntityResponse)pService.Execute(req);
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "Exception in SetStateDynamicEntity");
                //Effect.Crm.EntityLogs.LogEntityUtils.MyHandle.HandleInvokingMethodException(ex, "SetStateDynamicEntity", new object[] { entityId, entityname });
            }
        }
        #endregion

        #region SetPropertyValue
        public static void SetPropertyValue(DynamicEntity entity, string propertyName, object propertyValue, Type propertyType)
        {
            if (entity == null)
                return;
            Property property = GetProperty(entity, propertyName);
            if (property != null)
                //property.GetType().GetField("Value").SetValue(property, propertyValue);
                SetValueFromProperty(property, propertyValue);
            else
            {
                property = (Property)Activator.CreateInstance(propertyType.Assembly.FullName, propertyType.FullName).Unwrap();
                SetValueFromProperty(property, propertyValue);
                //property.GetType().GetField("Value").SetValue(property, propertyValue);

                property.Name = propertyName;
                List<Property> list = new List<Property>();
                for (int i = 0; i < entity.Properties.Length; i++)
                    list.Add(entity.Properties[i]);
                list.Add(property);
                entity.Properties = list.ToArray();
            }
        }
        #endregion

        #region GetPropertyValue
        public static object GetPropertyValue(DynamicEntity entity, string propertyName)
        {
            if (entity == null || propertyName == null)
                return null;
            Property property = GetProperty(entity, propertyName.ToLower());
            if (property == null)
                return null;
            return GetValueFromProperty(property);
        }
        #endregion

        #region GetDynamicPropertyFieldFromBisinessEntity
        public static object GetDynamicPropertyFieldFromBisinessEntity(FieldInfo field, BusinessEntity entity)
        {
            switch (field.FieldType.Name)
            {
                case "String":
                    StringProperty stringProperty = new StringProperty();
                    stringProperty.Name = field.Name;
                    stringProperty.Value = (string)field.GetValue(entity);
                    return stringProperty;
                case "Lookup":
                    LookupProperty lookup = new LookupProperty();
                    lookup.Name = field.Name;
                    lookup.Value = (Lookup)field.GetValue(entity);
                    return lookup;
                case "Key":
                    KeyProperty key = new KeyProperty();
                    key.Name = field.Name;
                    key.Value = (Key)field.GetValue(entity);
                    return key;
                case "CrmNumber":
                    CrmNumberProperty number = new CrmNumberProperty();
                    number.Name = field.Name;
                    number.Value = (CrmNumber)field.GetValue(entity);
                    return number;
                case "CrmBoolean":
                    CrmBooleanProperty boolean = new CrmBooleanProperty();
                    boolean.Name = field.Name;
                    boolean.Value = (CrmBoolean)field.GetValue(entity);
                    return boolean;
                case "CrmDateTime":
                    CrmDateTimeProperty dateTime = new CrmDateTimeProperty();
                    dateTime.Name = field.Name;
                    dateTime.Value = (CrmDateTime)field.GetValue(entity);
                    return dateTime;
                case "Picklist":
                    PicklistProperty picklist = new PicklistProperty();
                    picklist.Name = field.Name;
                    picklist.Value = (Picklist)field.GetValue(entity);
                    return picklist;
                case "Customer":
                    CustomerProperty customer = new CustomerProperty();
                    customer.Name = field.Name;
                    customer.Value = (Customer)field.GetValue(entity);
                    return customer;
                case "Owner":
                    OwnerProperty owner = new OwnerProperty();
                    owner.Name = field.Name;
                    owner.Value = (Owner)field.GetValue(entity);
                    return owner;
                case "CrmMoney":
                    CrmMoneyProperty money = new CrmMoneyProperty();
                    money.Name = field.Name;
                    money.Value = (CrmMoney)field.GetValue(entity);
                    return money;
                case "CrmFloat":
                    CrmFloatProperty floatProperty = new CrmFloatProperty();
                    floatProperty.Name = field.Name;
                    floatProperty.Value = (CrmFloat)field.GetValue(entity);
                    return floatProperty;
                case "CrmDecimal":
                    CrmDecimalProperty decimalProperty = new CrmDecimalProperty();
                    decimalProperty.Name = field.Name;
                    decimalProperty.Value = (CrmDecimal)field.GetValue(entity);
                    return decimalProperty;
                case "EntityNameReference":
                    EntityNameReferenceProperty entityName = new EntityNameReferenceProperty();
                    entityName.Name = field.Name;
                    entityName.Value = (EntityNameReference)field.GetValue(entity);
                    return entityName;
                case "Status":
                    StatusProperty status = new StatusProperty();
                    status.Name = field.Name;
                    status.Value = (Status)field.GetValue(entity);
                    return status;
                case "State":
                    StateProperty state = new StateProperty();
                    state.Name = field.Name;
                    state.Value = (string)field.GetValue(entity);
                    return state;
            }
            return null;
        }
        #endregion

        #region ConvertEntityToDynamic
        public static DynamicEntity ConvertEntityToDynamic(BusinessEntity entity)
        {
            DynamicEntity rslt = new DynamicEntity();
            rslt.Name = entity.GetType().Name;
            List<Property> list = new List<Property>();
            FieldInfo[] fields = entity.GetType().GetFields();
            for (int i = 0; i < fields.Length; i++)
                list.Add((Property)GetDynamicPropertyFieldFromBisinessEntity(fields[i], entity));
            rslt.Properties = list.ToArray();
            return rslt;
        }
        #endregion

        #region SetValueFromProperty
        public static void SetValueFromProperty(Property property, object val)
        {
            if (property == null)
                return;
            switch (property.GetType().Name)
            {
                case "StringProperty":
                    ((StringProperty)property).Value = (string)val;
                    break;
                case "LookupProperty":
                    ((LookupProperty)property).Value = (Lookup)val;
                    break;
                case "PicklistProperty":
                    ((PicklistProperty)property).Value = (Picklist)val;
                    break;
                case "CustomerProperty":
                    ((CustomerProperty)property).Value = (Customer)val;
                    break;
                case "CrmNumberProperty":
                    ((CrmNumberProperty)property).Value = (CrmNumber)val;
                    break;
                case "CrmFloatProperty":
                    ((CrmFloatProperty)property).Value = (CrmFloat)val;
                    break;
                case "CrmMoneyProperty":
                    ((CrmMoneyProperty)property).Value = (CrmMoney)val;
                    break;
                case "CrmBooleanProperty":
                    ((CrmBooleanProperty)property).Value = (CrmBoolean)val;
                    break;
                case "CrmDateTimeProperty":
                    ((CrmDateTimeProperty)property).Value = (CrmDateTime)val;
                    break;
                case "StatusProperty":
                    ((StatusProperty)property).Value = (Status)val;
                    break;
                case "StateProperty":
                    ((StateProperty)property).Value = (string)val;
                    break;
                case "KeyProperty":
                    ((KeyProperty)property).Value = (Key)val;
                    break;
                case "OwnerProperty":
                    ((OwnerProperty)property).Value = (Owner)val;
                    break;
                case "CrmDecimalProperty":
                    ((CrmDecimalProperty)property).Value = (CrmDecimal)val;
                    break;
            }
        }
        #endregion

        #region GetValueFromProperty
        public static object GetValueFromProperty(Property property)
        {
            if (property == null)
                return null;
            switch (property.GetType().Name)
            {
                case "DynamicEntityArrayProperty":
                    return ((DynamicEntityArrayProperty)property).Value;
                case "StringProperty":
                    return ((StringProperty)property).Value;
                case "LookupProperty":
                    return ((LookupProperty)property).Value;
                case "PicklistProperty":
                    return ((PicklistProperty)property).Value;
                case "CustomerProperty":
                    return ((CustomerProperty)property).Value;
                case "CrmNumberProperty":
                    return ((CrmNumberProperty)property).Value;
                case "CrmFloatProperty":
                    return ((CrmFloatProperty)property).Value;
                case "CrmMoneyProperty":
                    return ((CrmMoneyProperty)property).Value;
                case "CrmBooleanProperty":
                    return ((CrmBooleanProperty)property).Value;
                case "CrmDateTimeProperty":
                    return ((CrmDateTimeProperty)property).Value;
                case "StatusProperty":
                    return ((StatusProperty)property).Value;
                case "StateProperty":
                    return ((StateProperty)property).Value;
                case "KeyProperty":
                    return ((KeyProperty)property).Value;
                case "OwnerProperty":
                    return ((OwnerProperty)property).Value;
                case "CrmDecimalProperty":
                    return ((CrmDecimalProperty)property).Value;
            }
            return null;
        }
        #endregion

        #region GetPropertyStringValue
        public static string GetPropertyStringValue(DynamicEntity entity, string propertyName)
        {
            string res = null;
            Property property = GetProperty(entity, propertyName);
            if (property == null)
                return res;
            switch (property.GetType().Name)
            {
                case "DynamicEntityArrayProperty":
                    res = ((DynamicEntityArrayProperty)property).Value.ToString();
                    break;
                case "StringProperty":
                    res = ((StringProperty)property).Value;
                    break;
                case "LookupProperty":
                    Lookup lookup = ((LookupProperty)property).Value;
                    res = (lookup == null || lookup.IsNull) ? null : lookup.Value.ToString();
                    break;
                case "PicklistProperty":
                    Picklist picklist = ((PicklistProperty)property).Value;
                    res = (picklist == null || picklist.IsNull) ? null : picklist.Value.ToString();
                    break;
                case "CustomerProperty":
                    Customer customer = ((CustomerProperty)property).Value;
                    res = (customer == null || customer.IsNull) ? null : customer.Value.ToString();
                    break;
                case "CrmNumberProperty":
                    CrmNumber crmNumber = ((CrmNumberProperty)property).Value;
                    res = (crmNumber == null || crmNumber.IsNull) ? null : crmNumber.Value.ToString();
                    break;
                case "CrmFloatProperty":
                    CrmFloat crmFloat = ((CrmFloatProperty)property).Value;
                    res = (crmFloat == null || crmFloat.IsNull) ? null : crmFloat.Value.ToString();
                    break;
                case "CrmMoneyProperty":
                    CrmMoney crmMoney = ((CrmMoneyProperty)property).Value;
                    res = (crmMoney == null || crmMoney.IsNull) ? null : crmMoney.Value.ToString();
                    break;
                case "CrmBooleanProperty":
                    CrmBoolean crmBoolean = ((CrmBooleanProperty)property).Value;
                    res = (crmBoolean == null || crmBoolean.IsNull) ? null : crmBoolean.Value.ToString();
                    break;
                case "CrmDateTimeProperty":
                    CrmDateTime crmDateTime = ((CrmDateTimeProperty)property).Value;
                    res = (crmDateTime == null || crmDateTime.IsNull) ? null : crmDateTime.Value;
                    break;
                case "StatusProperty":
                    Status status = ((StatusProperty)property).Value;
                    res = (status == null || status.IsNull) ? null : status.name;
                    break;
                case "StateProperty":
                    res = ((StateProperty)property).Name;
                    break;
                case "KeyProperty":
                    Key key = ((KeyProperty)property).Value;
                    res = key == null ? null : key.Value.ToString();
                    break;
                case "OwnerProperty":
                    Owner owner = ((OwnerProperty)property).Value;
                    res = (owner == null || owner.IsNull) ? null : owner.Value.ToString();
                    break;
            }
            return res;
        }
        #endregion

        #region GetPropertyDatetimeValue
        public static DateTime GetPropertyDatetimeValue(DynamicEntity entity, string propertyName)
        {
            DateTime res = DateTime.MinValue;
            Property property = GetProperty(entity, propertyName);
            if (property == null)
                return res;
            switch (property.GetType().Name)
            {
                case "StringProperty":
                    res = Effect.Crm.Convertor.ConvertorUtils.ToDateTime(((StringProperty)property).Value);
                    break;
                case "CrmDateTimeProperty":
                    CrmDateTime crmDateTime = ((CrmDateTimeProperty)property).Value;
                    res = (crmDateTime == null || crmDateTime.IsNull) ? DateTime.MinValue : ConvertorUtils.ToDateTime(crmDateTime);
                    break;
            }
            return res;
        }
        #endregion

        #region GetPropertyBooleanValue
        public static bool GetPropertyBooleanValue(DynamicEntity entity, string propertyName)
        {
            bool res = false;
            Property property = GetProperty(entity, propertyName);
            if (property == null)
                return res;
            switch (property.GetType().Name)
            {
                case "StringProperty":
                    res = ConvertorUtils.ToBoolean(((StringProperty)property).Value);
                    break;
                case "CrmNumberProperty":
                    CrmNumber crmNumber = ((CrmNumberProperty)property).Value;
                    res = (crmNumber == null || crmNumber.IsNull) ? false : ConvertorUtils.ToBoolean(crmNumber.Value);
                    break;
                case "CrmFloatProperty":
                    CrmFloat crmFloat = ((CrmFloatProperty)property).Value;
                    res = (crmFloat == null || crmFloat.IsNull) ? false : ConvertorUtils.ToBoolean(crmFloat.Value);
                    break;
                case "CrmMoneyProperty":
                    CrmMoney crmMoney = ((CrmMoneyProperty)property).Value;
                    res = (crmMoney == null || crmMoney.IsNull) ? false : ConvertorUtils.ToBoolean(crmMoney.Value);
                    break;
                case "CrmBooleanProperty":
                    CrmBoolean crmBoolean = ((CrmBooleanProperty)property).Value;
                    res = (crmBoolean == null || crmBoolean.IsNull) ? false : crmBoolean.Value;
                    break;
            }
            return res;
        }
        #endregion

        #region GetPropertyFloatValue
        public static float GetPropertyFloatValue(DynamicEntity entity, string propertyName)
        {
            float res = 0;
            Property property = GetProperty(entity, propertyName);
            if (property == null)
                return res;
            switch (property.GetType().Name)
            {
                case "StringProperty":
                    res = ConvertorUtils.ToFloat(((StringProperty)property).Value);
                    break;
                case "CrmNumberProperty":
                    CrmNumber crmNumber = ((CrmNumberProperty)property).Value;
                    res = (crmNumber == null || crmNumber.IsNull) ? 0 : ConvertorUtils.ToFloat(crmNumber.Value);
                    break;
                case "CrmFloatProperty":
                    CrmFloat crmFloat = ((CrmFloatProperty)property).Value;
                    res = (crmFloat == null || crmFloat.IsNull) ? 0 : (float)crmFloat.Value;
                    break;
                case "CrmMoneyProperty":
                    CrmMoney crmMoney = ((CrmMoneyProperty)property).Value;
                    res = (crmMoney == null || crmMoney.IsNull) ? 0 : ConvertorUtils.ToFloat(crmMoney.Value);
                    break;
            }
            return res;
        }
        #endregion

        #region GetPropertyDecimalValue
        public static Decimal GetPropertyDecimalValue(DynamicEntity entity, string propertyName)
        {
            Decimal res = 0M;
            Property property = GetProperty(entity, propertyName);
            if (property == null)
                return res;
            switch (property.GetType().Name)
            {
                case "StringProperty":
                    res = ConvertorUtils.ToDecimal(((StringProperty)property).Value);
                    break;
                case "CrmNumberProperty":
                    CrmNumber crmNumber = ((CrmNumberProperty)property).Value;
                    res = (crmNumber == null || crmNumber.IsNull) ? 0M : ConvertorUtils.ToDecimal(crmNumber.Value);
                    break;
                case "CrmFloatProperty":
                    CrmFloat crmFloat = ((CrmFloatProperty)property).Value;
                    res = (crmFloat == null || crmFloat.IsNull) ? 0M : ConvertorUtils.ToDecimal(crmFloat.Value);
                    break;
                case "CrmMoneyProperty":
                    CrmMoney crmMoney = ((CrmMoneyProperty)property).Value;
                    res = (crmMoney == null || crmMoney.IsNull) ? 0M : crmMoney.Value;
                    break;
            }
            return res;
        }
        #endregion

        #region GetPropertyGuidValue
        public static Guid GetPropertyGuidValue(DynamicEntity entity, string propertyName)
        {
            Guid res = Guid.Empty;
            Property property = GetProperty(entity, propertyName);
            if (property == null)
                return res;
            switch (property.GetType().Name)
            {
                case "StringProperty":
                    res = ConvertorUtils.ToGuid(((StringProperty)property).Value);
                    break;
                case "LookupProperty":
                    Lookup lookup = ((LookupProperty)property).Value;
                    res = (lookup == null || lookup.IsNull) ? Guid.Empty : lookup.Value;
                    break;
                case "CustomerProperty":
                    Customer customer = ((CustomerProperty)property).Value;
                    res = (customer == null || customer.IsNull) ? Guid.Empty : customer.Value;
                    break;
                case "KeyProperty":
                    Key key = ((KeyProperty)property).Value;
                    res = key == null ? Guid.Empty : key.Value;
                    break;
                case "OwnerProperty":
                    Owner owner = ((OwnerProperty)property).Value;
                    res = (owner == null || owner.IsNull) ? Guid.Empty : owner.Value;
                    break;
            }
            return res;
        }
        #endregion

        #region GetPropertyIntValue
        public static int GetPropertyIntValue(DynamicEntity entity, string propertyName)
        {
            int res = 0;
            Property property = GetProperty(entity, propertyName);
            if (property == null)
                return res;
            switch (property.GetType().Name)
            {
                case "StringProperty":
                    res = ConvertorUtils.ToInt(((StringProperty)property).Value);
                    break;
                case "PicklistProperty":
                    Picklist picklist = ((PicklistProperty)property).Value;
                    res = (picklist == null || picklist.IsNull) ? -1 : picklist.Value;
                    break;
                case "CrmNumberProperty":
                    CrmNumber crmNumber = ((CrmNumberProperty)property).Value;
                    res = (crmNumber == null || crmNumber.IsNull) ? 0 : crmNumber.Value;
                    break;
                case "CrmFloatProperty":
                    CrmFloat crmFloat = ((CrmFloatProperty)property).Value;
                    res = (crmFloat == null || crmFloat.IsNull) ? 0 : ConvertorUtils.ToInt(crmFloat.Value);
                    break;
                case "CrmMoneyProperty":
                    CrmMoney crmMoney = ((CrmMoneyProperty)property).Value;
                    res = (crmMoney == null || crmMoney.IsNull) ? 0 : ConvertorUtils.ToInt(crmMoney.Value);
                    break;
            }
            return res;
        }
        #endregion

        #region GetCrmLookup
        public static Lookup GetCrmLookup(string entityTypeName, Guid value)
        {
            Lookup rslt = new Lookup();
            if (value == Guid.Empty)
            {
                rslt.IsNull = rslt.IsNullSpecified = true;
            }
            else
            {
                rslt.type = entityTypeName;
                rslt.Value = value;
            }
            return rslt;
        }
        #endregion
    }
}
