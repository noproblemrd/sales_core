﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.Utils
{
    public class DateLocalFixer : XrmUserBase
    {
        public DateLocalFixer()
            : base(null)
        {

        }

        public void FillLocalDates()
        {
            DataAccessLayer.Generic dal = new NoProblem.Core.DataAccessLayer.Generic();
//            string getTablesQuery =
//                @"SELECT Table_name
//                FROM INFORMATION_SCHEMA.Columns
//                where
//                column_name = 'new_createdonlocal'
//                and
//                Table_name not like '%Filtered%'
//                and
//                Table_name not like '%Extension%'
//                and 
//                Table_name != 'tbl_broker_calls'
//                and
//                Table_name != '_Flavor_Half_Controls_Top_2'";
            List<string> tables = new List<string>();
            //var tablesReader = dal.ExecuteReader(getTablesQuery);
            //foreach (var row in tablesReader)
            //{
            //    tables.Add((string)row["Table_name"]);
            //}
            tables.Add("New_upsale");
            tables.Add("Incident");
            tables.Add("New_incidentaccount");
            string getDataQuery =
                @"select {0}id, createdon from {0} with(nolock)
                where deletionstatecode = 0 and new_createdonlocal is null";
            string updateQuery =
                @"update {0} set new_createdonlocal = @localDate
                where {0}id = @id";
            foreach (var tableName in tables)
            {
                Dictionary<Guid, DateTime> data = new Dictionary<Guid, DateTime>();
                string queryReady = string.Format(getDataQuery, tableName);
                var dataReader = dal.ExecuteReader(queryReady);
                foreach (var row in dataReader)
                {
                    data.Add((Guid)row[tableName + "id"], (DateTime)row["createdon"]);
                }
                string updateQueryReady = string.Format(updateQuery, tableName);
                foreach (var item in data)
                {
                    DateTime fixedDate = FixDateToLocal(item.Value);
                    List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
                    parameters.Add(new KeyValuePair<string, object>("@localDate", fixedDate));
                    parameters.Add(new KeyValuePair<string, object>("@id", item.Key));
                    dal.ExecuteNonQuery(updateQueryReady, parameters);
                }
            }
        }
    }
}
