﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Utils
{
    public class BalanceFixer : XrmUserBase
    {
        #region Ctor
        public BalanceFixer(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        private static Timer timer;

        public static void SetTimerOnAppStart()
        {
            TimeSpan firstInvokeSpan = FindFirstInvokeSpan();
            timer = new Timer(
                    StartBalanceFixerTimerCallback,
                    null,
                    firstInvokeSpan,
                    new TimeSpan(1, 0, 0, 0));
        }

        private static TimeSpan FindFirstInvokeSpan()
        {
            BalanceFixer fixer = new BalanceFixer(null);
            var localNow = fixer.FixDateToLocal(DateTime.Now);
            var d = new DateTime(localNow.Year, localNow.Month, localNow.Day, 3, 0, 0);
            if (localNow.Hour >= 3)
            {
                d = d.AddDays(1);
            }
            DateTime utcToRun = fixer.FixDateToUtcFromLocal(d);
            return utcToRun - DateTime.Now;
        }

        private static void StartBalanceFixerTimerCallback(object obj)
        {
            try
            {
                BalanceFixer fixer = new BalanceFixer(null);
                fixer.Fix();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in StartBalanceFixerTimerCallback");
            }
        }

        private void Fix()
        {
            string query =
                    @"update account set new_availablecashbalance = new_cashbalance
                    where deletionstatecode = 0 and new_availablecashbalance != new_cashbalance";
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            int rowsChanged = dal.ExecuteNonQuery(query);
            LogUtils.MyHandle.WriteToLog("Fixer, rows changed = {0}", rowsChanged);
        }
    }
}
