﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace NoProblem.Core.BusinessLogic.Utils
{
    public class RegionUtils
    {
        private readonly string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];

        public List<DataModel.SqlHelper.RegionMinData> GetParentRegions(List<Guid> regions, int maxLevel)
        {
            StringBuilder regionList = new StringBuilder();
            for (int i = 0; i < regions.Count; i++)
            {
                if (i > 0)
                {
                    regionList.Append(",");
                }
                regionList.Append("'");
                regionList.Append(regions.ElementAt<Guid>(i).ToString());
                regionList.Append("'");
            }

            string getRegionsQuery = @"WITH HirarchyRegions(RegionLevel,RegionId,ParentRegionId)
AS
(
	SELECT new_region.new_level,new_region.new_regionid,new_parentregionid
	FROM new_region with(nolock)
	WHERE new_region.new_regionid IN ({0})
	UNION ALL
	SELECT new_region.new_level,new_region.new_regionid,new_parentregionid
	FROM new_region with(nolock)
	INNER JOIN HirarchyRegions ON HirarchyRegions.ParentRegionId = new_regionid
)
SELECT distinct RegionId, RegionLevel FROM HirarchyRegions
where
RegionLevel != {1}";
            List<DataModel.SqlHelper.RegionMinData> parentRegions = new List<DataModel.SqlHelper.RegionMinData>();

            if (regionList.ToString() != string.Empty)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand(string.Format(getRegionsQuery, regionList.ToString(), maxLevel), connection))
                    {
                        command.Connection.Open();
                        using (SqlDataReader getRegions = command.ExecuteReader())
                        {
                            while (getRegions.Read())
                            {
                                DataModel.SqlHelper.RegionMinData data = new NoProblem.Core.DataModel.SqlHelper.RegionMinData();
                                data.Id = (Guid)getRegions["RegionId"];
                                data.Level = (int)getRegions["RegionLevel"];
                                parentRegions.Add(data);
                                //parentRegions.Add((Guid)getRegions["RegionId"]);
                            }
                        }
                    }
                }
            }
            return parentRegions;
        }

        public XmlNodeList GetParentRegionsXml(string RegionList)
        {
            string getRegionsQuery = @"WITH HirarchyRegions(RegionLevel,RegionId,ParentRegionId)
AS
(
	SELECT new_region.new_level,new_region.new_regionid,new_parentregionid
	FROM new_region with(nolock)
	WHERE new_region.new_regionid IN ({0})
	UNION ALL
	SELECT new_region.new_level,new_region.new_regionid,new_parentregionid
	FROM new_region with(nolock)
	INNER JOIN HirarchyRegions ON HirarchyRegions.ParentRegionId = new_regionid
)
SELECT RegionId FROM HirarchyRegions";

            XmlDocument regionContainer = new XmlDocument();
            XmlElement regions = regionContainer.CreateElement("Regions");

            regionContainer.AppendChild(regions);

            if (RegionList != "")
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand(string.Format(getRegionsQuery, RegionList), connection))
                    {
                        command.Connection.Open();
                        using (SqlDataReader getRegions = command.ExecuteReader())
                        {
                            while (getRegions.Read())
                            {
                                XmlElement region = regionContainer.CreateElement("Region");
                                regions.AppendChild(region);
                                XmlAttribute id = regionContainer.CreateAttribute("ID");

                                region.Attributes.Append(id);
                                id.InnerText = getRegions["RegionId"].ToString();
                            }
                        }
                    }
                }
            }

            return regionContainer.SelectNodes("//Region");
        }

        public string GetRegions(int RegionLevel)
        {
            StringWriter xmlText = new StringWriter();
            XmlTextWriter xmlFormater = new XmlTextWriter(xmlText);
            xmlFormater.WriteStartElement("Areas");

            xmlFormater.WriteRaw(GetCitiesNoRootElement(RegionLevel));

            xmlFormater.WriteEndElement(); // Closes the Areas tag

            return xmlText.ToString();
        }

        public string GetCitiesNoRootElement(int RegionLevel)
        {
            string query = String.Format(
                   @"select new_name,new_englishname,new_regionid,new_code from new_region with (nolock)
                 where new_level = {0} and deletionstatecode = 0", RegionLevel);

            StringWriter xmlText = new StringWriter();
            XmlTextWriter xmlFormater = new XmlTextWriter(xmlText);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            xmlFormater.WriteStartElement("Area");
                            xmlFormater.WriteAttributeString("RegionId", reader["new_regionid"].ToString());
                            xmlFormater.WriteAttributeString("RegionCode", reader["new_code"].ToString());
                            xmlFormater.WriteAttributeString("Name", reader["new_name"].ToString());
                            xmlFormater.WriteAttributeString("EnglishName", reader["new_englishname"].ToString());
                            xmlFormater.WriteEndElement();
                        }
                    }
                }
            }
            return xmlText.ToString();
        }

        public IEnumerable<GetAreasInRadiusContainer> GetAreasInRadiusOO(decimal longitude, decimal latitude, decimal radiusInMiles)
        {
            string query =
                @"
CREATE TABLE #Temp
	(
		Id uniqueidentifier,
		Name nvarchar(256)
	)
	INSERT INTO #Temp
	SELECT DISTINCT r.new_regionid, r.new_name
	FROM new_region r
		INNER JOIN new_coordinate p
		on (p.new_regionid = r.new_regionid)

	WHERE @earthRadius * ACos( Cos(RADIANS(New_latitude)) * Cos(RADIANS(@latitude)) * Cos(RADIANS(@longitude) - RADIANS(New_longitude)) + Sin(RADIANS(New_latitude)) * Sin(RADIANS(@latitude))) < @radius
	ORDER BY r.new_name

	if (@@RowCount = 0) 
	BEGIN
		INSERT INTO #Temp
		SELECT DISTINCT r.new_regionid, r.new_name
		FROM new_region r
			INNER JOIN new_coordinate p
			on (p.new_regionid = r.new_regionid)

		WHERE  
			p.new_regionid IN 
			(
				SELECT new_regionid
				FROM new_coordinate
				WHERE @earthRadius * ACos( Cos(RADIANS(New_latitude)) * Cos(RADIANS(@latitude)) * Cos(RADIANS(@longitude) - RADIANS(New_longitude)) + Sin(RADIANS(New_latitude)) * Sin(RADIANS(@latitude))) = 
				(
					SELECT MIN(@earthRadius * ACos( Cos(RADIANS(New_latitude)) * Cos(RADIANS(@latitude)) * Cos(RADIANS(@longitude) - RADIANS(New_longitude)) + Sin(RADIANS(New_latitude)) * Sin(RADIANS(@latitude))))
					FROM new_coordinate
				)
			)
		ORDER BY r.new_name
	END
	SELECT * FROM #Temp
	DROP TABLE #Temp";

                        
            List<GetAreasInRadiusContainer> retVal = new List<GetAreasInRadiusContainer>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@radius", radiusInMiles);
                    command.Parameters.AddWithValue("@latitude", latitude);
                    command.Parameters.AddWithValue("@longitude", longitude);
                    command.Parameters.AddWithValue("@earthRadius", DataModel.Constants.EARTH_RADIUS_MILES);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GetAreasInRadiusContainer data = new GetAreasInRadiusContainer()
                            {
                                Name = Convert.ToString(reader["Name"]),
                                RegionId = (Guid)reader["Id"]
                            };
                            retVal.Add(data);
                        }
                    }
                }
            }
            return retVal;
        }

        public string GetAreasInRadius(decimal longitude, decimal latitude, decimal radiusInMiles)
        {
            IEnumerable<GetAreasInRadiusContainer> data = GetAreasInRadiusOO(longitude, latitude, radiusInMiles);
            StringWriter xmlText = new StringWriter();
            XmlTextWriter xmlFormater = new XmlTextWriter(xmlText);
            xmlFormater.WriteStartElement("Root");
            foreach (var item in data)
            {
                    xmlFormater.WriteStartElement("Area");
                    xmlFormater.WriteAttributeString("RegionId", item.RegionId.ToString());
                    xmlFormater.WriteAttributeString("Name", item.Name);
                    xmlFormater.WriteEndElement();
            }
            xmlFormater.WriteEndElement();
            return xmlText.ToString();
        }

        public class GetAreasInRadiusContainer
        {
            public Guid RegionId { get; set; }
            public string Name { get; set; }
        }       
    }
}
