﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Utils
{
   public static class FormatUtils
   {
      /// <summary>
      /// This function formats the decimal value as the default formatin: #,###.00
      /// </summary>
      /// <param name="Value"></param>
      /// <returns></returns>
      public static string Format(decimal Value)
      {
         return Value.ToString("#,###.00");
      }
   }
}
