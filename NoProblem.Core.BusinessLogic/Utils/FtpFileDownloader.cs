﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;

namespace NoProblem.Core.BusinessLogic.Utils
{
    internal class FtpFileDownloader
    {
        public FtpFileDownloader(string userName, string password)
        {
            this.userName = userName;
            this.password = password;
        }

        string userName;
        string password;

        public Stream DownloadFile(string url)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
            request.KeepAlive = false;
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential(userName, password);
            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        using (MemoryStream stream = new MemoryStream())
                        {
                            using (StreamWriter writer = new StreamWriter(stream))
                            {
                                writer.Write(reader.ReadToEnd());
                                writer.Flush();
                                return new MemoryStream(stream.ToArray(), false /*writable*/);
                            }
                        }
                    }                
                }
            }
        }
    }
}
