﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Utils
{
    internal class FtpFileUploader
    {
        private string urlBase;
        private string userName;
        private string password;
        private const string ftpPathDelimiter = "/";

        internal FtpFileUploader(string urlBase, string userName, string password)
        {
            this.urlBase = urlBase;
            this.userName = userName;
            this.password = password;
        }

        internal bool CreateDirectory(string directoryName)
        {
            bool retVal = true;
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(urlBase + ftpPathDelimiter + directoryName);
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(userName, password);
                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                    if (resp.StatusCode != FtpStatusCode.PathnameCreated)
                    {
                        retVal = false;
                        LogUtils.MyHandle.WriteToLog("Failed to create directoy in ftp server. directoryName = {0}, urlBase = {1}", directoryName, urlBase);
                    }
                }
            }
            catch (Exception exc)
            {
                retVal = false;
                LogUtils.MyHandle.HandleException(exc, "CreateDirectory failed with expception. DirectoryName = {0}, urlBase = {1}.", directoryName, urlBase);
            }
            return retVal;
        }

        internal bool UploadFile(FileStream file, string fileName, string directoryName)
        {
            bool retVal = true;
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(urlBase + ftpPathDelimiter + directoryName + ftpPathDelimiter + fileName);
                request.KeepAlive = false;
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(userName, password);
                using (Stream ftpStream = request.GetRequestStream())
                {
                    int length = 1024;
                    byte[] buffer = new byte[length];
                    int bytesRead = 0;
                    do
                    {
                        bytesRead = file.Read(buffer, 0, length);
                        ftpStream.Write(buffer, 0, bytesRead);
                    }
                    while (bytesRead != 0);
                }
            }
            catch (Exception exc)
            {
                retVal = false;
                LogUtils.MyHandle.HandleException(exc, "UploadFile failed with expception. FileName = {0}, DirectoryName = {1}, urlBase = {2}.", fileName, directoryName, urlBase);
            }
            return retVal;
        }
    }
}
