﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Utils
{
    internal class LoginUtils
    {
        public static string GenerateNewPassword()
        {
            StringBuilder passBuilder = new StringBuilder();
            Random passGenerator = new Random();

            for (int i = 0; i < 6; i++)
            {
                int digit = passGenerator.Next(10);
                passBuilder.Append(digit);
            }

            return passBuilder.ToString();
        }

        public static bool ValidatePassword(string password)
        {
            bool retVal = false;
            if (password.Length >= 6)
            {
                retVal = true;
            }
            return retVal;
        }
    }
}
