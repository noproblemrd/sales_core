﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using Effect.Crm.Convertor;
using Effect.Crm.Methods;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using System.Configuration;

namespace NoProblem.Core.BusinessLogic.Utils
{
    public class EntityUtils : CrmServiceUserBase
    {
        #region Ctor
        public EntityUtils(CrmService crmService)
            : base(crmService)
        {

        }
        #endregion

        private readonly string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];

        public void WriteEntityLogRecord(string EntityName, string EntityId, string UserName, string FieldName, string PreValue, string PostValue)
        {
            DynamicEntity logEntity = ConvertorCRMParams.GetNewDynamicEntity("new_generallog");

            ConvertorCRMParams.SetDynamicEntityPropertyValue(logEntity, "new_" + EntityName + "id",
               ConvertorCRMParams.GetCrmLookup(EntityName, new Guid(EntityId)), typeof(LookupProperty));

            ConvertorCRMParams.SetDynamicEntityPropertyValue(logEntity, "new_username", UserName, typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(logEntity, "new_fieldname", FieldName, typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(logEntity, "new_previoustextvalue", PreValue, typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(logEntity, "new_currenttextvalue", PostValue, typeof(StringProperty));
            MethodsUtils.CreateEntity(CrmService, logEntity);
        }

        public string GetEntityLogs(string entityName, string entityId)
        {
            string query = "SELECT * FROM new_generallog with (nolock) WHERE new_{0}id = '{1}' AND deletionstatecode = 0 and statecode = 0";
            StringWriter xmlWriter = new StringWriter();
            XmlTextWriter xmlFormater = new XmlTextWriter(xmlWriter);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(string.Format(query, entityName, entityId), connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader logs = command.ExecuteReader())
                    {
                        xmlFormater.WriteStartElement("Logs");

                        while (logs.Read())
                        {
                            xmlFormater.WriteStartElement("Log");
                            xmlFormater.WriteElementString("UserName", logs["new_username"].ToString());
                            xmlFormater.WriteElementString("FieldName", logs["new_fieldname"].ToString());
                            xmlFormater.WriteElementString("PreviousValue", logs["new_previoustextvalue"].ToString());
                            xmlFormater.WriteElementString("CurrentValue", logs["new_currenttextvalue"].ToString());
                            xmlFormater.WriteEndElement();
                        }
                        xmlFormater.WriteEndElement();
                    }
                }
            }
            return xmlWriter.ToString();
        }

        public void ManyToManyAssign(string RelationshipName, string Entity1Name, Guid Entity1Id, string Entity2Name, Guid Entity2Id)
        {
            Moniker Moniker1 = new Moniker();
            Moniker1.Id = Entity1Id;
            Moniker1.Name = Entity1Name;

            Moniker Moniker2 = new Moniker();
            Moniker2.Id = Entity2Id;
            Moniker2.Name = Entity2Name;

            AssociateEntitiesRequest request = new AssociateEntitiesRequest();
            // Set the ID of Moniker1 to the ID of the lead.        
            request.Moniker1 = Moniker1;

            // Set the ID of Moniker2 to the ID of the contact.        
            request.Moniker2 = Moniker2;

            // Set the relationship name to associate on.
            request.RelationshipName = RelationshipName;

            // Execute the request.        
            CrmService.Execute(request);
        }

        public SetStateDynamicEntityResponse SetEntityState(string EntityName, Guid EntityId, string StateCodeName)
        {
            SetStateDynamicEntityRequest request = new SetStateDynamicEntityRequest();
            request.Entity = new Moniker();
            request.Entity.Id = EntityId;
            request.Entity.Name = EntityName;
            request.State = StateCodeName;
            request.Status = -1;
            return (SetStateDynamicEntityResponse)MethodsUtils.ExecuteService(CrmService, request);
        }

        public string GetInstanceNameByGuid(string entityName, Guid entityId)
        {
            string retVal = "";
            string query = "SELECT new_name FROM {0} with (nolock) WHERE {0}id = '{1}'";
            string queryReady = string.Format(query, entityName, entityId);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryReady, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader getName = command.ExecuteReader())
                    {
                        if (getName.Read())
                        {
                            retVal = getName["new_name"].ToString();
                        }
                    }
                }
            }
            return retVal;
        }
    }
}
