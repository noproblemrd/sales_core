﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Utils
{
    public class ZeroDurationNotifier
    {
        //singleton
        private static ZeroDurationNotifier instance = new ZeroDurationNotifier();

        private readonly HashSet<ZeroDurationOcurrence> data;
        private int countToCleanUp;
        private DataAccessLayer.ConfigurationSettings configs;

        private const string SUBJECT = "ZERO DURATION CALLS ALERT";
        private const string BODY = "{0} calls with Zero duration recorded during the last 60 min.";

        private readonly List<string> emails;

        private ZeroDurationNotifier()
        {
            data = new HashSet<ZeroDurationOcurrence>();
            countToCleanUp = 0;
            configs = new DataAccessLayer.ConfigurationSettings(null);
            string emailsConfig = configs.GetConfigurationSettingValue(DataModel.ConfigurationKeys.ZERO_DURATION_NOTIFIER_EMAILS);
            string[] splited = emailsConfig.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            emails = new List<string>();
            foreach (string item in splited)
            {
                emails.Add(item);
            }
        }

        private void RegisterEvent(Guid id)
        {
            lock (data)
            {
                if (data.Add(new ZeroDurationOcurrence { IncidentAccountId = id, Time = DateTime.Now }))
                {
                    DateTime lastHour = DateTime.Now.AddHours(-1);
                    int count = data.Count(x => x.Time > lastHour);
                    if (NeedsNotification(count))
                    {
                        Notify(count);
                        CleanUp(true);
                    }
                    else if (countToCleanUp >= 10)
                    {
                        CleanUp(false);
                    }
                    else
                    {
                        countToCleanUp++;
                    }
                }
            }
        }

        private bool NeedsNotification(int count)
        {
            int notifyCount;
            return int.TryParse(configs.GetConfigurationSettingValue(DataModel.ConfigurationKeys.ZERO_DURATION_NOTIFIER_NOTIFY_COUNT), out notifyCount)
                && notifyCount > 0
                && notifyCount <= count;
        }

        private void Notify(int count)
        {
            EmailSender sender = new EmailSender(count);
            sender.Start();
            Slack.SlackNotifier notifier = new Slack.SlackNotifier(String.Format(BODY, count));
            notifier.Start();
        }

        private void CleanUp(bool deleteAll)
        {
            if (deleteAll)
            {
                data.Clear();
            }
            else
            {
                DateTime lastHour = DateTime.Now.AddHours(-1);
                data.RemoveWhere(x => x.Time < lastHour);
            }
            countToCleanUp = 0;
        }

        public static void ZeroDurationEvent(Guid incidentAccountId)
        {
            Worker worker = new Worker(incidentAccountId);
            worker.Start();
        }

        private class Worker : Runnable
        {
            Guid id;

            public Worker(Guid id)
            {
                this.id = id;
            }

            protected override void Run()
            {
                ZeroDurationNotifier.instance.RegisterEvent(id);
            }
        }

        private class ZeroDurationOcurrence
        {
            public DateTime Time { get; set; }
            public Guid IncidentAccountId { get; set; }

            public override bool Equals(object obj)
            {
                return ((ZeroDurationOcurrence)obj).IncidentAccountId.Equals(this.IncidentAccountId);
            }

            public override int GetHashCode()
            {
                return IncidentAccountId.GetHashCode();
            }
        }

        private class EmailSender : Runnable
        {
            int count;

            public EmailSender(int count)
            {
                this.count = count;
            }

            protected override void Run()
            {
                Notifications notifier = new Notifications(XrmDataContext);
                notifier.SendEmail(ZeroDurationNotifier.instance.emails,
                    String.Format(BODY, count),
                    SUBJECT);
            }
        }
    }
}
