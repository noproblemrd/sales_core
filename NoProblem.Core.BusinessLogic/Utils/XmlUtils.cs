﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace NoProblem.Core.BusinessLogic.Utils
{
   public static class XmlUtils
   {
      public static string GetValue(XmlNode Element)
      {
         if (Element != null)
            return Element.InnerText;

         return "";
      }

      public static string WrapWithCDATA(string XmlValue)
      {
         return string.Format("<![CDATA[{0}]]>", XmlValue);
      }
   }
}
