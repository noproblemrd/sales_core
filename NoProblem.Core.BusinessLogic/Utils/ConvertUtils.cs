﻿using System;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using DML = NoProblem.Core.DataModel;
using System.Linq;

namespace NoProblem.Core.BusinessLogic.Utils
{
    public static class ConvertUtils
    {
        public static string FormatCrmDateTimeString(string dateTimeString, string format)
        {
            DateTime parsedTime = DateTime.Parse(dateTimeString);

            return parsedTime.ToString(format);
        }

        public static string FormatCrmDateTimeString(string dateTimeString)
        {
            string format = "dd/MM/yyyy HH:mm";

            return FormatCrmDateTimeString(dateTimeString, format);
        }

        public static string[] ConvertObjectArrayToStringArray(object[] items)
        {
            string[] ret = new string[items.Length];
            for (int i = 0; i < items.Length; i++)
            {
                ret[i] = items[i] as string;
            }
            return ret;
        }

        public static int GetDayNumberFromName(string DayName)
        {
            int dayOfWeek = -1;
            if (DayName.ToLower() == DayOfWeek.Sunday.ToString().ToLower())
            {
                dayOfWeek = (int)DayOfWeek.Sunday;
            }
            else if (DayName.ToLower() == DayOfWeek.Monday.ToString().ToLower())
            {
                dayOfWeek = (int)DayOfWeek.Monday;
            }
            else if (DayName.ToLower() == DayOfWeek.Tuesday.ToString().ToLower())
            {
                dayOfWeek = (int)DayOfWeek.Tuesday;
            }
            else if (DayName.ToLower() == DayOfWeek.Wednesday.ToString().ToLower())
            {
                dayOfWeek = (int)DayOfWeek.Wednesday;
            }
            else if (DayName.ToLower() == DayOfWeek.Thursday.ToString().ToLower())
            {
                dayOfWeek = (int)DayOfWeek.Thursday;
            }
            else if (DayName.ToLower() == DayOfWeek.Friday.ToString().ToLower())
            {
                dayOfWeek = (int)DayOfWeek.Friday;
            }
            else if (DayName.ToLower() == DayOfWeek.Saturday.ToString().ToLower())
            {
                dayOfWeek = (int)DayOfWeek.Saturday;
            }

            dayOfWeek++;

            return dayOfWeek;

        }

        public static int GetNextDay(int Day)
        {
            return (Day % 7) + 1;
        }

        public static int GetPreviousDay(int Day)
        {
            return (Day + 5) % 7 + 1;
        }

        //public static DateTime GetUtcFromLocal(CrmService pService, DateTime localTime, int localTimeCode)
        //{
        //    return GetUtcFromLocal(pService, localTime.ToString("MM/dd/yyyy HH:mm:ss"), localTimeCode);
        //}

        //public static DateTime GetUtcFromLocal(CrmService pService, string localTime, int localTimeCode)
        //{
        //    UtcTimeFromLocalTimeRequest request = new UtcTimeFromLocalTimeRequest();
        //    request.LocalTime = new CrmDateTime();
        //    request.LocalTime.Value = localTime;
        //    request.TimeZoneCode = localTimeCode;
        //    UtcTimeFromLocalTimeResponse response = (UtcTimeFromLocalTimeResponse)pService.Execute(request);
        //    return DateTime.Parse(response.UtcTime.Value);
        //}

        //public static DateTime GetUtcFromLocal(DML.Xrm.DataContext xrmDataContext, DateTime localTime, int localTimeCode)
        //{
        //    DateTime retVal = new DateTime();
        //    Microsoft.Crm.SdkTypeProxy.UtcTimeFromLocalTimeRequest request = new Microsoft.Crm.SdkTypeProxy.UtcTimeFromLocalTimeRequest();
        //    request.LocalTime = new Microsoft.Crm.Sdk.CrmDateTime();
        //    request.LocalTime.Value = localTime.ToString("MM/dd/yyyy HH:mm:ss");
        //    request.TimeZoneCode = localTimeCode;
        //    using (var service = xrmDataContext.CreateService())
        //    {
        //        var response = service.Execute(request) as Microsoft.Crm.SdkTypeProxy.UtcTimeFromLocalTimeResponse;
        //        retVal = response.UtcTime.UniversalTime;
        //    }
        //    return retVal;
        //}

        //public static DateTime GetLocalFromUtc(CrmService pService, DateTime utcTime, int localTimeCode)
        //{
        //    string utcTimeString = utcTime.ToString("MM/dd/yyyy HH:mm:ss");
        //    DateTime retVal = GetLocalFromUtc(pService, utcTimeString, localTimeCode);
        //    return retVal;
        //}

        //public static DateTime GetLocalFromUtc(CrmService pService, string utcTime, int localTimeCode)
        //{
        //    LocalTimeFromUtcTimeRequest request = new LocalTimeFromUtcTimeRequest();
        //    request.UtcTime = new CrmDateTime();
        //    request.UtcTime.Value = utcTime;
        //    request.TimeZoneCode = localTimeCode;
        //    LocalTimeFromUtcTimeResponse response = (LocalTimeFromUtcTimeResponse)pService.Execute(request);
        //    return DateTime.Parse(response.LocalTime.Value.Split('+')[0]);
        //}

        //public static DateTime GetLocalFromUtc(DML.Xrm.DataContext xrmDataContext, DateTime utcTime, int localTimeCode)
        //{
        //    DateTime retVal = new DateTime();
        //    Microsoft.Crm.SdkTypeProxy.LocalTimeFromUtcTimeRequest request = new Microsoft.Crm.SdkTypeProxy.LocalTimeFromUtcTimeRequest(); //new LocalTimeFromUtcTimeRequest();
        //    request.UtcTime = new Microsoft.Crm.Sdk.CrmDateTime();  //new CrmDateTime();
        //    request.UtcTime.Value = utcTime.ToString("MM/dd/yyyy HH:mm:ss");
        //    request.TimeZoneCode = localTimeCode;
        //    using (Microsoft.Xrm.Client.Services.IOrganizationService service = xrmDataContext.CreateService())
        //    {
        //        var response = service.Execute(request) as Microsoft.Crm.SdkTypeProxy.LocalTimeFromUtcTimeResponse;
        //        //string[] splitted = response.LocalTime.Value.Split('+');
        //        //string dateLocal = splitted[0];
        //        string dateLocal = response.LocalTime.Value.Substring(0, 19);
        //        retVal = DateTime.Parse(dateLocal);           
        //    }
        //    return retVal;
        //}

        /// <summary>
        /// Converts the given utc datetime to the given time zone according to the given time zone name. This is fast (does not use Crm context).
        /// </summary>
        /// <param name="utcTime"></param>
        /// <param name="localTimeZoneStandardName"></param>
        /// <returns></returns>
        public static DateTime GetLocalFromUtc(DateTime utcTime, string localTimeZoneStandardName)
        {
            var timeZoneInfo = GetTimeZoneInfo(localTimeZoneStandardName);
            DateTime local = utcTime;
            local = DateTime.SpecifyKind(local, DateTimeKind.Unspecified);
            local = TimeZoneInfo.ConvertTimeFromUtc(local, timeZoneInfo);
            return local;
        }
        public static DateTime GetLocalFromUtc(DateTime utcDate, int timeZone)
        {
            //DateTime localDate = Utils.ConvertUtils.GetLocalFromUtc(XrmDataContext, utcDate, timeZone);
            DataAccessLayer.ConfigurationSettings configDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            string timeZoneStandardName = configDAL.GetTimeZoneStandardName(timeZone);
            DateTime localDate = GetLocalFromUtc(utcDate, timeZoneStandardName);
            return localDate;
        }
        public static DateTime GetLocalFromUtcByZipcode(DateTime utcDate, Guid regionId)
        {
            NoProblem.Core.DataAccessLayer.Region reg = new DataAccessLayer.Region(null);
            int timeZoneCode = reg.GetTimeZoneCode(regionId);
            if(timeZoneCode < 0)
            {
                NoProblem.Core.DataAccessLayer.ConfigurationSettings conSetting = new DataAccessLayer.ConfigurationSettings(null);
                string localTimeZoneStandardName = conSetting.GetPublisherTimeZoneStandardName();
                return GetLocalFromUtc(utcDate, localTimeZoneStandardName);
            }
            return GetLocalFromUtc(utcDate, timeZoneCode);
        }

        /// <summary>
        /// Converts the given datetime to UTC according to the given time zone name. This is fast (does not use Crm context).
        /// </summary>
        /// <param name="localTime"></param>
        /// <param name="localTimeZoneStandardName"></param>
        /// <returns></returns>
        public static DateTime GetUtcFromLocal(DateTime localTime, string localTimeZoneStandardName)
        {
            var timeZoneInfo = GetTimeZoneInfo(localTimeZoneStandardName);
            DateTime utcTime = localTime;
            utcTime = DateTime.SpecifyKind(utcTime, DateTimeKind.Unspecified);
            utcTime = TimeZoneInfo.ConvertTimeToUtc(utcTime, timeZoneInfo);
            return utcTime;
        }

        private static TimeZoneInfo GetTimeZoneInfo(string timeZoneStandardName)
        {
            var timeZoneInfo =
               (from info in TimeZoneInfo.GetSystemTimeZones()
                where string.Equals(info.StandardName, timeZoneStandardName, StringComparison.InvariantCultureIgnoreCase)
                select info).First();
            return timeZoneInfo;
        }

        /// <summary>
        /// Check if the passed object is null or DBNull and returns Param1 as string or the DefaultValue in case of Null
        /// </summary>
        /// <param name="Param1">The object to inspect</param>
        /// <param name="DefaultValue">The value to be returned in case of null</param>
        public static string IsNull(object Param1, string DefaultValue)
        {
            if (Param1 == null || Param1 == DBNull.Value)
                return DefaultValue;

            return Param1.ToString();
        }

        /// <summary>
        /// Check if the passed object is null or DBNull and returns Param1 as string or an empty string in case of Null
        /// </summary>
        /// <param name="Param1">The object to inspect</param>
        public static string IsNull(object Param1)
        {
            if (Param1 == null || Param1 == DBNull.Value)
                return "";

            return Param1.ToString();
        }

        public static int TranslateHourStringToInt(string hourString)
        {
            if (hourString.StartsWith("23:59"))
            {
                return 24;
            }
            string hourPart = hourString.Split(':')[0];
            return int.Parse(hourPart);
        }

        public static DayOfWeek GetNextDay(DayOfWeek day)
        {
            return (DayOfWeek)(((int)day + 1) % 7);
        }

        public static DayOfWeek GetPreviousDay(DayOfWeek day)
        {
            return (DayOfWeek)(((int)day + 6) % 7);
        }
    }
}
