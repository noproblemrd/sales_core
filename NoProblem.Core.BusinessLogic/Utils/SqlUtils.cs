﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Utils
{
   public static class SqlUtils
   {
      public static string EscapeCharacters(string TextToEscape)
      {
         return TextToEscape.Replace("'", "''");
      }
   }
}
