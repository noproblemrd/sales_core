﻿using System;
using System.Text.RegularExpressions;
using Effect.Crm.Convertor;
using Effect.Crm.Logs;
using Effect.Crm.Methods;
using Effect.Crm.SDKProviders.CrmServiceSdk;

namespace NoProblem.Core.BusinessLogic.Utils
{
   public static class GeneralUtils
   {
       public static bool IsNumericValue(string str)
       {
           bool isNumber = true;
           for (int i = 0; i < str.Length; i++)
           {
               if (char.IsDigit(str, i) == false)
               {
                   isNumber = false;
                   break;
               }
           }
           return isNumber;
       }

       public static bool IsGUID(string expression)
       {
           if (expression != null)
           {
               Regex guidRegEx = new Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");
               return guidRegEx.IsMatch(expression);
           }
           return false;
       }

      public static void CreateDaysAvailability(CrmService pService,Guid AccountId, string FromTime, string ToTime, int Day)
      {
         LogUtils.MyHandle.WriteToLog(8, "CreateDaysAvailability - AccountId: {0}, FromTime: {1}, ToTime: {2}, Day: {3}", AccountId, FromTime, ToTime, Day);
          DynamicEntity daysAvailability = ConvertorCRMParams.GetNewDynamicEntity("new_availability");

          ConvertorCRMParams.SetDynamicEntityPropertyValue(daysAvailability, "new_accountid",
              ConvertorCRMParams.GetCrmLookup("account",AccountId), typeof(LookupProperty));

          DateTime from = DateTime.Parse(FromTime);
          DateTime to = DateTime.Parse(ToTime);
          string toTimeStr = ToTime;

          // if the to is smaller the the from then we need to split the request to two days one from the FromTime to 23:59
          // and another one from 00:00 to ToTime
          if (to < from)
          {
              CreateDaysAvailability(pService, AccountId, "00:00", ToTime, ConvertUtils.GetNextDay(Day));
              toTimeStr = "23:59";
          }

          ConvertorCRMParams.SetDynamicEntityPropertyValue(daysAvailability, "new_from",
            FromTime, typeof(StringProperty));

          ConvertorCRMParams.SetDynamicEntityPropertyValue(daysAvailability, "new_to",
              toTimeStr, typeof(StringProperty));

          ConvertorCRMParams.SetDynamicEntityPropertyValue(daysAvailability, "new_day",
              ConvertorCRMParams.GetCrmPicklist(Day), typeof(PicklistProperty));

          MethodsUtils.CreateEntity(pService, daysAvailability);
      }
   }
}
