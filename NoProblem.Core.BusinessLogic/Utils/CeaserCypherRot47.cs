﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Utils
{
    class CeaserCypherRot47
    {
        private static char Rot47(char chr)
        {
            if (chr == ' ') return ' ';
            int ascii = chr;
            ascii += 47;
            if (ascii > 126) ascii -= 94;
            if (ascii < 33) ascii += 94;
            return (char)ascii;
        }

        public static string Rot47(string str)
        {
            string RetStr = "";
            foreach (char c in str.ToCharArray())
                RetStr += Rot47(c).ToString();
            return RetStr;
        }
    }
}
