﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Utils.Csv
{
    public class CsvRow : List<string>
    {
        public string LineText { get; set; }
    }
}
