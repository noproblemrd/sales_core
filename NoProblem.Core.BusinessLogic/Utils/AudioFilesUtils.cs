﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Utils
{
    public class AudioFilesUtils
    {
        public static List<int> NumberToAudioFilesPartition(int number)
        {
            if (number <= 0 || number > 10999)
                return null;
            int nDigits = 1 + Convert.ToInt32(Math.Floor(Math.Log10(number)));
            int[] digits = new int[nDigits];
            for (int i = nDigits - 1; i >= 0; i--)
            {
                digits[i] = number % 10;
                number = number / 10;
            }

            List<int> retVal = new List<int>();

            if (nDigits == 1)
            {
                retVal.Add(digits[0]);
            }
            else
            {
                if (digits[nDigits - 2] == 1)
                {
                    retVal.Add(((digits[nDigits - 2] * 10) + digits[nDigits - 1]));
                }
                else
                {
                    int n1 = digits[nDigits - 2] * 10;
                    if (n1 > 0)
                        retVal.Add(n1);
                    int n2 = digits[nDigits - 1];
                    if (n2 > 0)
                        retVal.Add(n2);
                }

                for (int i = nDigits - 3; i >= 0; i--)
                {
                    int n = (int)(digits[i] * Math.Pow(10, nDigits - 1 - i));
                    if (n > 0)
                        retVal.Insert(0, n);
                }
            }
            return retVal;
        }
    }
}
