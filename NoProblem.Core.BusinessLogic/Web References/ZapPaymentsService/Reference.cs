﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4984
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 2.0.50727.4984.
// 
#pragma warning disable 1591

namespace NoProblem.Core.BusinessLogic.ZapPaymentsService {
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.4927")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="NPCreateInvoiceERPBinding", Namespace="http://www.d.co.il/NPCreateInvoiceERP")]
    public partial class NPCreateInvoiceERP : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback processOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public NPCreateInvoiceERP() {
            this.Url = global::NoProblem.Core.BusinessLogic.Properties.Settings.Default.NoProblem_Core_BusinessLogic_ZapPaymentsService_NPCreateInvoiceERP;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event processCompletedEventHandler processCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("process", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("NPCreateInvoiceERPProcessResponse", Namespace="http://www.d.co.il/NPCreateInvoiceERP")]
        public NPCreateInvoiceERPProcessResponse process([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.d.co.il/NPCreateInvoiceERP")] NPCreateInvoiceERPProcessRequest NPCreateInvoiceERPProcessRequest) {
            object[] results = this.Invoke("process", new object[] {
                        NPCreateInvoiceERPProcessRequest});
            return ((NPCreateInvoiceERPProcessResponse)(results[0]));
        }
        
        /// <remarks/>
        public void processAsync(NPCreateInvoiceERPProcessRequest NPCreateInvoiceERPProcessRequest) {
            this.processAsync(NPCreateInvoiceERPProcessRequest, null);
        }
        
        /// <remarks/>
        public void processAsync(NPCreateInvoiceERPProcessRequest NPCreateInvoiceERPProcessRequest, object userState) {
            if ((this.processOperationCompleted == null)) {
                this.processOperationCompleted = new System.Threading.SendOrPostCallback(this.OnprocessOperationCompleted);
            }
            this.InvokeAsync("process", new object[] {
                        NPCreateInvoiceERPProcessRequest}, this.processOperationCompleted, userState);
        }
        
        private void OnprocessOperationCompleted(object arg) {
            if ((this.processCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.processCompleted(this, new processCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4927")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.d.co.il/NPCreateInvoiceERP")]
    public partial class NPCreateInvoiceERPProcessRequest {
        
        private string loginUserField;
        
        private System.Nullable<NPCreateInvoiceERPProcessRequestProcessMode> processModeField;
        
        private bool processModeFieldSpecified;
        
        private NPOrder_Type nPOrderField;
        
        /// <remarks/>
        public string LoginUser {
            get {
                return this.loginUserField;
            }
            set {
                this.loginUserField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<NPCreateInvoiceERPProcessRequestProcessMode> ProcessMode {
            get {
                return this.processModeField;
            }
            set {
                this.processModeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ProcessModeSpecified {
            get {
                return this.processModeFieldSpecified;
            }
            set {
                this.processModeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public NPOrder_Type NPOrder {
            get {
                return this.nPOrderField;
            }
            set {
                this.nPOrderField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4927")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.d.co.il/NPCreateInvoiceERP")]
    public enum NPCreateInvoiceERPProcessRequestProcessMode {
        
        /// <remarks/>
        ONLINE,
        
        /// <remarks/>
        BATCH,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4927")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.d.co.il/NPCreateInvoiceERP")]
    public partial class NPOrder_Type {
        
        private NPOrderHeaderRec_Type nPOrderHeaderRecField;
        
        private OrderItem_Type[] listOfOrderItemField;
        
        /// <remarks/>
        public NPOrderHeaderRec_Type NPOrderHeaderRec {
            get {
                return this.nPOrderHeaderRecField;
            }
            set {
                this.nPOrderHeaderRecField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("OrderItem", IsNullable=false)]
        public OrderItem_Type[] ListOfOrderItem {
            get {
                return this.listOfOrderItemField;
            }
            set {
                this.listOfOrderItemField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4927")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.d.co.il/NPCreateInvoiceERP")]
    public partial class NPOrderHeaderRec_Type {
        
        private string customerNoField;
        
        private string customerEmailField;
        
        private string taxRegistrationNumField;
        
        private string salesPersonField;
        
        private string invoiceDateField;
        
        private decimal invoiceTotalAmtWithVATField;
        
        private decimal invoiceTotalAmtNoVATField;
        
        private string invoiceCommentsField;
        
        private NPOrderHeaderRec_TypePaymentType paymentTypeField;
        
        private string ccIdField;
        
        private string ccExpDateField;
        
        private string ccOwnerIdField;
        
        private string ccOwnerNameField;
        
        private string ccCompanyNameField;
        
        private string trxUniqueIdentifierField;
        
        /// <remarks/>
        public string CustomerNo {
            get {
                return this.customerNoField;
            }
            set {
                this.customerNoField = value;
            }
        }
        
        /// <remarks/>
        public string CustomerEmail {
            get {
                return this.customerEmailField;
            }
            set {
                this.customerEmailField = value;
            }
        }
        
        /// <remarks/>
        public string TaxRegistrationNum {
            get {
                return this.taxRegistrationNumField;
            }
            set {
                this.taxRegistrationNumField = value;
            }
        }
        
        /// <remarks/>
        public string SalesPerson {
            get {
                return this.salesPersonField;
            }
            set {
                this.salesPersonField = value;
            }
        }
        
        /// <remarks/>
        public string InvoiceDate {
            get {
                return this.invoiceDateField;
            }
            set {
                this.invoiceDateField = value;
            }
        }
        
        /// <remarks/>
        public decimal InvoiceTotalAmtWithVAT {
            get {
                return this.invoiceTotalAmtWithVATField;
            }
            set {
                this.invoiceTotalAmtWithVATField = value;
            }
        }
        
        /// <remarks/>
        public decimal InvoiceTotalAmtNoVAT {
            get {
                return this.invoiceTotalAmtNoVATField;
            }
            set {
                this.invoiceTotalAmtNoVATField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string InvoiceComments {
            get {
                return this.invoiceCommentsField;
            }
            set {
                this.invoiceCommentsField = value;
            }
        }
        
        /// <remarks/>
        public NPOrderHeaderRec_TypePaymentType PaymentType {
            get {
                return this.paymentTypeField;
            }
            set {
                this.paymentTypeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CcId {
            get {
                return this.ccIdField;
            }
            set {
                this.ccIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CcExpDate {
            get {
                return this.ccExpDateField;
            }
            set {
                this.ccExpDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CcOwnerId {
            get {
                return this.ccOwnerIdField;
            }
            set {
                this.ccOwnerIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CcOwnerName {
            get {
                return this.ccOwnerNameField;
            }
            set {
                this.ccOwnerNameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CcCompanyName {
            get {
                return this.ccCompanyNameField;
            }
            set {
                this.ccCompanyNameField = value;
            }
        }
        
        /// <remarks/>
        public string TrxUniqueIdentifier {
            get {
                return this.trxUniqueIdentifierField;
            }
            set {
                this.trxUniqueIdentifierField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4927")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.d.co.il/NPCreateInvoiceERP")]
    public enum NPOrderHeaderRec_TypePaymentType {
        
        /// <remarks/>
        CC,
        
        /// <remarks/>
        TRANSFER,
        
        /// <remarks/>
        CASH,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4927")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.d.co.il/NPCreateInvoiceERP")]
    public partial class OrderItem_Type {
        
        private string lineNumField;
        
        private string itemDescriptionField;
        
        private string itemCodeField;
        
        private string itemTypeField;
        
        private decimal quantityField;
        
        private bool quantityFieldSpecified;
        
        private decimal itemPriceNoVatField;
        
        private bool itemPriceNoVatFieldSpecified;
        
        private string lineCommentsField;
        
        private string lineDiscountField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
        public string LineNum {
            get {
                return this.lineNumField;
            }
            set {
                this.lineNumField = value;
            }
        }
        
        /// <remarks/>
        public string ItemDescription {
            get {
                return this.itemDescriptionField;
            }
            set {
                this.itemDescriptionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ItemCode {
            get {
                return this.itemCodeField;
            }
            set {
                this.itemCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ItemType {
            get {
                return this.itemTypeField;
            }
            set {
                this.itemTypeField = value;
            }
        }
        
        /// <remarks/>
        public decimal Quantity {
            get {
                return this.quantityField;
            }
            set {
                this.quantityField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantitySpecified {
            get {
                return this.quantityFieldSpecified;
            }
            set {
                this.quantityFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public decimal ItemPriceNoVat {
            get {
                return this.itemPriceNoVatField;
            }
            set {
                this.itemPriceNoVatField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ItemPriceNoVatSpecified {
            get {
                return this.itemPriceNoVatFieldSpecified;
            }
            set {
                this.itemPriceNoVatFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string LineComments {
            get {
                return this.lineCommentsField;
            }
            set {
                this.lineCommentsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string LineDiscount {
            get {
                return this.lineDiscountField;
            }
            set {
                this.lineDiscountField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4927")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.d.co.il/NPCreateInvoiceERP")]
    public partial class NPCreateInvoiceERPProcessResponse {
        
        private string resultField;
        
        private string errorDescField;
        
        private string cgStatusField;
        
        private string cgStatusDescField;
        
        private string cgAuthorizationNumField;
        
        /// <remarks/>
        public string Result {
            get {
                return this.resultField;
            }
            set {
                this.resultField = value;
            }
        }
        
        /// <remarks/>
        public string ErrorDesc {
            get {
                return this.errorDescField;
            }
            set {
                this.errorDescField = value;
            }
        }
        
        /// <remarks/>
        public string CgStatus {
            get {
                return this.cgStatusField;
            }
            set {
                this.cgStatusField = value;
            }
        }
        
        /// <remarks/>
        public string CgStatusDesc {
            get {
                return this.cgStatusDescField;
            }
            set {
                this.cgStatusDescField = value;
            }
        }
        
        /// <remarks/>
        public string CgAuthorizationNum {
            get {
                return this.cgAuthorizationNumField;
            }
            set {
                this.cgAuthorizationNumField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.4927")]
    public delegate void processCompletedEventHandler(object sender, processCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.4927")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class processCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal processCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public NPCreateInvoiceERPProcessResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((NPCreateInvoiceERPProcessResponse)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591