﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL
{
    internal class HandlerFactory
    {
        private readonly Dictionary<string, Func<IFoneApiHandler>> _handlers =
                            new Dictionary<string, Func<IFoneApiHandler>>();

        public IFoneApiHandler CreateInstance(string handlerName)
        {
            return _handlers[handlerName]();
        }

        public void RegisterHandler<T>(string handlerName)
            where T : IFoneApiHandler, new()
        {
            _handlers.Add(handlerName,
                  delegate
                  {
                      var handlerInstance = new T();
                      return handlerInstance;
                  });
        }
    }
}
