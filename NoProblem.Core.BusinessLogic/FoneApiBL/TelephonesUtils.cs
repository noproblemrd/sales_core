﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL
{
    public class TelephonesUtils
    {
        private const string ENDING_FOR_MADE_UP_NUMBER = "54321";

        public static string MakeUpCallerIdForCustomer(string customerPhone)
        {
            if (customerPhone.Length > 5)
            {
                return customerPhone.Substring(0, customerPhone.Length - 5) + ENDING_FOR_MADE_UP_NUMBER;
            }
            return customerPhone;  
        }
    }
}
