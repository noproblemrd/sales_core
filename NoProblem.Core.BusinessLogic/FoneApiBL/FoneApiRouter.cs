﻿using System;
using System.Collections.Generic;
using FoneApiWrapper.CallBacks;
using NoProblem.Core.BusinessLogic.FoneApiBL.Handlers;
using NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call;

namespace NoProblem.Core.BusinessLogic.FoneApiBL
{
    public sealed class FoneApiRouter
    {
        private static volatile FoneApiRouter instance;
        private static object syncRoot = new Object();
        private static Dictionary<string, Func<IFoneApiHandler>> InstanceCreateCache = new Dictionary<string, Func<IFoneApiHandler>>();
        private static HandlerFactory factory;
        internal const string FAPI_NPHANDLER = "fapi_nph";

        public static FoneApiRouter Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new FoneApiRouter();
                    }
                }
                return instance;
            }
        }

        public CallBackResponse Route(FapiArgs args)
        {
            string handlerName;
            if (!args.TryGetValue(FAPI_NPHANDLER, out handlerName))
            {
                handlerName = HandlersNames.INCOMING_CALLS_HANDLER;
            }
            IFoneApiHandler handler = factory.CreateInstance(handlerName);
            return handler.HandleCallBack(args);
        }

        private FoneApiRouter()
        {
            factory = new HandlerFactory();
            factory.RegisterHandler<IncomingCallsHandler>(HandlersNames.INCOMING_CALLS_HANDLER);
            factory.RegisterHandler<PhoneApiHandler>(HandlersNames.PHONE_API_HANDLER);
            factory.RegisterHandler<JonaCallHandler>(HandlersNames.JONA_CALL_HANDLER);
            factory.RegisterHandler<BidCallHandler>(HandlersNames.BID_CALL_HANDLER);
            factory.RegisterHandler<NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.BidCall.BidCall2014.BidCall2014Handler>(HandlersNames.BID_CALL_2014_HANDLER);
            factory.RegisterHandler<Click2CallHandler>(HandlersNames.CLICK_2_CALL_HANDLER);
            factory.RegisterHandler<NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.PinCall.PinCallHandler>(HandlersNames.PIN_CALL_HANDLER);
            factory.RegisterHandler<NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.AarTextMessage.AarTextMessageHandler>(HandlersNames.AAR_TEXT_MESSAGE_HANDLER);
            factory.RegisterHandler<NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call.AarSupplier.Click2CallAarHandler>(HandlersNames.CLICK_2_CALL_AAR_HANDLER);
            factory.RegisterHandler<NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call.ConnectAndRecord.C2CHanlerCustomerConnectAndRecord>(HandlersNames.CLICK_2_CALL_CUSTOMER_CONNECT);
            factory.RegisterHandler<NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.ClipCall.ClipCallHandler>(HandlersNames.CLIP_CALL_HANDLER);

        }



        internal static class HandlersNames
        {
            public const string INCOMING_CALLS_HANDLER = "IncomingCallsHandler";
            public const string PHONE_API_HANDLER = "PhoneApiHandler";
            public const string JONA_CALL_HANDLER = "JonaCallHandler";
            public const string BID_CALL_HANDLER = "BidCallHandler";
            public const string BID_CALL_2014_HANDLER = "BidCall2014Handler";
            public const string CLICK_2_CALL_HANDLER = "C2CHandler";
            public const string PIN_CALL_HANDLER = "PinCallHandler";
            public const string AAR_TEXT_MESSAGE_HANDLER = "AarTextMessageHandler";
            public const string CLICK_2_CALL_AAR_HANDLER = "C2CAarHandler";
            public const string CLICK_2_CALL_CUSTOMER_CONNECT = "C2CHanlerCustomerConnectAndRecord";
            public const string CLIP_CALL_HANDLER = "ClipCallHandler";
        }
    }
}
