﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Effect.Crm.Logs;
using FoneApiWrapper.CallBacks;

namespace NoProblem.Core.BusinessLogic.FoneApiBL
{
    internal abstract class FoneApiHandlerBase : XrmUserBase, IFoneApiHandler
    {
        public FoneApiHandlerBase()
            : base(null)
        {
            phonesConfigDal = new NoProblem.Core.DataAccessLayer.AsteriskConfiguration(XrmDataContext);
            configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
        }

        protected DataAccessLayer.AsteriskConfiguration phonesConfigDal;
        protected DataAccessLayer.ConfigurationSettings configDal;

        protected static readonly string returnAddress = ConfigurationManager.AppSettings["FoneApiReturnAddress"];
        protected const int appId = 0;
        protected const int _defaultDialTimeOut = 70;
        protected static readonly int _defaultLimitCallDuration = 60 * 60 * 5;//5 hours
        protected const string TERMINATION_KEY_POUND = "#";
        protected const int _defaultDtmfTimeout = 10;

        protected const string _FAPI_SIDE = "fapi_s";
        protected const string _CUSTOMER = "cust";
        protected const string _ADVERTISER = "adv";
        protected const string _HEB_PROMPT_START = "heb";
        private string _fapiKey, _fapiSecret;
        private string _audioFilesUrlBase;
        private int? _successfulCallMinLength;
        private string _apiUrlBase;
        private string _trunk;
        private string _internationalExtension, _countryCode;
        private int? _limitCallDurationSeconds;

        #region IFoneApiHandler Members

        public FoneApiWrapper.CallBacks.CallBackResponse HandleCallBack(FapiArgs args)
        {
            string phoneEvent = args[ArgsTypes.FAPI_EVENT];
            FoneApiWrapper.CallBacks.CallBackResponse retVal = null;
            //LogEventReceived(args);
            switch (phoneEvent)
            {
                case EventTypes.INCOMING_CALL:
                    retVal = IncomingCall(args);
                    break;
                case EventTypes.HANGUP:
                    Hangup(args);
                    break;
                case EventTypes.CALL_ANSWERED:
                    retVal = CallAnswered(args);
                    break;
                case EventTypes.PLAY_FILE_COMPLETE:
                    retVal = PlayFileComplete(args);
                    break;
                case EventTypes.PLAY_AND_GET_DTMF_COMPLETE:
                case EventTypes.GET_DTMF_COMPLETE:
                    retVal = GetDtmfComplete(args);
                    break;
                case EventTypes.BRIDGE_END:
                    BridgeEnd(args);
                    break;
                case EventTypes.DIAL_COMPLETE:
                    retVal = DialComplete(args);
                    break;
            }
            //LogResponseJson(args, retVal);
            return retVal;
        }

        #endregion

        protected virtual CallBackResponse IncomingCall(FapiArgs args)
        {
            return null;
        }

        protected virtual void BridgeEnd(FapiArgs args)
        {
        }

        protected virtual CallBackResponse GetDtmfComplete(FapiArgs args)
        {
            return null;
        }

        protected virtual CallBackResponse PlayFileComplete(FapiArgs args)
        {
            return null;
        }

        protected virtual CallBackResponse CallAnswered(FapiArgs args)
        {
            return null;
        }

        protected virtual CallBackResponse DialComplete(FapiArgs args)
        {
            return null;
        }

        protected virtual void Hangup(FapiArgs args)
        {
        }

        protected string Trunk
        {
            get
            {
                if (string.IsNullOrEmpty(_trunk))
                {
                    _trunk = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.FONEAPI_TRUNK);
                }
                return _trunk;
            }
        }

        protected string ApiUrlBase
        {
            get
            {
                if (string.IsNullOrEmpty(_apiUrlBase))
                {
                    _apiUrlBase = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.FONEAPI_URL_BASE);
                }
                return _apiUrlBase;
            }
        }

        protected int SuccessfulCallMinLength
        {
            get
            {
                if (!_successfulCallMinLength.HasValue)
                {
                    string tmpStr = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.SUCCESSFUL_CALL_MIN_LENGTH);
                    int tmpInt;
                    if (int.TryParse(tmpStr, out tmpInt))
                    {
                        _successfulCallMinLength = tmpInt;
                    }
                    else
                    {
                        _successfulCallMinLength = -1;
                    }
                }
                return _successfulCallMinLength.Value;
            }
        }

        protected int LimitCallDurationSeconds
        {
            get
            {
                if (!_limitCallDurationSeconds.HasValue)
                {
                    string limitStr = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.CALL_LIMIT);
                    int tmp;
                    int.TryParse(limitStr, out tmp);
                    if (tmp > 0)
                    {
                        _limitCallDurationSeconds = tmp;
                    }
                    else
                    {
                        _limitCallDurationSeconds = _defaultLimitCallDuration;
                    }
                }
                return _limitCallDurationSeconds.Value;
            }
        }

        protected string FapiKey
        {
            get
            {
                if (string.IsNullOrEmpty(_fapiKey))
                {
                    _fapiKey = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.FAPI_KEY);
                }
                return _fapiKey;
            }
        }

        protected string FapiSecret
        {
            get
            {
                if (string.IsNullOrEmpty(_fapiSecret))
                {
                    _fapiSecret = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.FAPI_SECRET);
                }
                return _fapiSecret;
            }
        }

        protected string AudioFilesUrlBase
        {
            get
            {
                if (string.IsNullOrEmpty(_audioFilesUrlBase))
                {
                    _audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
                }
                return _audioFilesUrlBase;
            }
        }

        private string InternationalExtension
        {
            get
            {
                if (string.IsNullOrEmpty(_internationalExtension))
                {
                    _internationalExtension = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.INTERNATIONAL_EXTENSION);
                }
                return _internationalExtension;
            }
        }

        private string CountryCode
        {
            get
            {
                if (string.IsNullOrEmpty(_countryCode))
                {
                    _countryCode = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.COUNTRY_CODE);
                }
                return _countryCode;
            }
        }

        private string _localAreaCode;
        private string LocalAreaCode
        {
            get
            {
                if (string.IsNullOrEmpty(_localAreaCode))
                {
                    _localAreaCode = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.LOCAL_AREA_CODE);
                }
                return _localAreaCode;
            }
        }

        private string _internalCountryPrefix;
        private string InternalCountryPrefix
        {
            get
            {
                if (string.IsNullOrEmpty(_internalCountryPrefix))
                {
                    _internalCountryPrefix = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.INTERNAL_COUNTRY_PREFIX);
                }
                return _internalCountryPrefix;
            }
        }

        private string _cid;
        protected string Cid
        {
            get
            {
                if (string.IsNullOrEmpty(_cid))
                {
                    _cid = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.CID);
                }
                return _cid;
            }
        }
        private string _clipcall_cid;
        protected string clipcall_cid
        {
            get
            {
                if (string.IsNullOrEmpty(_clipcall_cid))
                {
                    _clipcall_cid = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.CLIP_CALL_CID);
                }
                return _clipcall_cid;
            }
        }

        protected string MakeUpCallerIdForCustomer(string customerPhone)
        {
            return TelephonesUtils.MakeUpCallerIdForCustomer(customerPhone);
        }

        protected string NormalizePhone(string phoneNumber, string phoneCountry)
        {
#if DEBUG
            return Dialer.PhoneToDialNormalizer.NormalizePhone(phoneNumber, "");
#else
            return Dialer.PhoneToDialNormalizer.NormalizePhone(phoneNumber, phoneCountry);
#endif
        }

        protected string NormalizeIncomingDidPhone(string phoneNumber)
        {

            string retVal;
            if (Trunk == "012")
            {
                if (phoneNumber.Length > 10)
                    retVal = phoneNumber.Substring(phoneNumber.Length - 10, 10);
                else if (!phoneNumber.StartsWith("0"))
                    retVal = "0" + phoneNumber;
                else
                    retVal = phoneNumber;
            }
            else
            {
                /*
                if (phoneNumber.Length > 10)
                    retVal = phoneNumber.Substring(phoneNumber.Length - 10, 10);
                else
                 * */
                    retVal = phoneNumber;
            }
            return retVal;
        }

        protected void LogEventReceived(FapiArgs args)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Event log: ").Append(args[FoneApiWrapper.CallBacks.ArgsTypes.FAPI_EVENT]).AppendLine("---------------------------------------------------------------");
            foreach (var item in args)
            {
                builder.AppendLine(string.Format("{0} = {1}", item.Key, item.Value));
            }
            LogUtils.MyHandle.WriteToLog(builder.ToString());
        }

        protected void LogResponseJson(FapiArgs args, FoneApiWrapper.CallBacks.CallBackResponse response)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Response log for event: ").Append(args[FoneApiWrapper.CallBacks.ArgsTypes.FAPI_EVENT]).Append(" call id: ").Append(args[FoneApiWrapper.CallBacks.ArgsTypes.FAPI_CALL_ID]).AppendLine("-------------------------------------------------------");
            if (response != null)
            {
                builder.Append("Json: ").AppendLine(response.ToString());
            }
            LogUtils.MyHandle.WriteToLog(builder.ToString());
        }

        protected void CreateCDR(string callStatus, string callSid, string callDuration, string toPhone, string name, DataModel.Xrm.new_calldetailrecord.Type type, Guid incidentAccountId)
        {
            DataModel.Xrm.new_calldetailrecord cdr = new NoProblem.Core.DataModel.Xrm.new_calldetailrecord();
            cdr.new_type = (int)type;
            cdr.new_trunk = Trunk;
            cdr.new_tophone = toPhone;
            cdr.new_name = name;
            cdr.new_callstatus = callStatus;
            cdr.new_dialercallid = callSid;
            int duration;
            int.TryParse(callDuration, out duration);
            cdr.new_duration = duration;
            cdr.new_incidentaccountid = incidentAccountId;
            cdr.new_timeofcall = DateTime.Now;
            DataAccessLayer.CallDetailRecord dal = new NoProblem.Core.DataAccessLayer.CallDetailRecord(null);
            dal.Create(cdr);
            dal.XrmDataContext.SaveChanges();
            if (duration == 0)
            {
                Utils.ZeroDurationNotifier.ZeroDurationEvent(incidentAccountId);
            }
        }
        protected bool GetDigits(FapiArgs args, out string digits)
        {
            return args.TryGetValue(ArgsTypes.FAPI_DIGITS, out digits);
        }
        protected bool ValidatePhoneNumber(string digit)
        {
            string regexPattern = @"^\d{10}$";
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(regexPattern);
            return regex.IsMatch(digit);
        }
    }
}
