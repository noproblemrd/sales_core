﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call
{
    enum eActionAfterDtmf
    {
        Connect, Repeat, Hangup, HangupWithPrompt
    }
}
