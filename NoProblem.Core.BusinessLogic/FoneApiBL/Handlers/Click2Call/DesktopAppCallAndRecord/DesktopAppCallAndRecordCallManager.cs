﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call.DesktopAppCallAndRecord
{
    internal class DesktopAppCallAndRecordCallManager : Click2CallManagersBase
    {
        private Guid ContactId;
        private string userPhone;
        private string targetPhone;
        int SecondRecordLeft;
        bool IsBlockCallerId;
        DateTime CallDate;
        int DiffBetweenUtcToLocalTime_minute;
        Guid CallId;
        Pusher.DesktopApp.Builders.PrivateCallRecorderScriptBuilder ScriptPusherBuilder;
        int aliceCallDurationSeconds = 0;
        Click2CallHandler handler;
        System.Timers.Timer TimerHangup;
        bool IsTimerHangup = false;

        private string PusherChannel
        {
            get { return this.CallId.ToString(); }
        }
        public Guid GetCallId
        {
            get
            {
                return CallId;
            }
        }
        public DesktopAppCallAndRecordCallManager(string userPhone, int SecondRecordLeft, NoProblem.Core.DataModel.Deskapp.CallRecordRequest request)
        {
            this.userPhone = userPhone;
            this.SecondRecordLeft = SecondRecordLeft;
            this.targetPhone = request.BobPhone;
            this.IsBlockCallerId = request.BlockCallerId;
            this.DiffBetweenUtcToLocalTime_minute = request.TimezoneOffset;
            this.ContactId = request.ContactId;
            this.CallDate = DateTime.Now;
            this.CallId = CreateNewDeskAppPrivateCall();
            this.ScriptPusherBuilder = new Pusher.DesktopApp.Builders.PrivateCallRecorderScriptBuilder();
        }

        public bool StartCall()
        {
            //   PusherServer.IGetResult<object> result = NoProblem.Core.BusinessLogic.Pusher.PusherEventSender.getchannel(PusherChannel);
            //   PusherServer.IGetResult<PusherServer.ChannelsList> result2 = NoProblem.Core.BusinessLogic.Pusher.PusherEventSender.getchannels();
            handler = new Click2CallHandler();
            bool IsStartCall = handler.StartCall(this);

            return IsStartCall;
            /*
            bool IsStartCall = handler.StartCall(this);
       //     bool IsStartCall = true;
            if(IsStartCall)
            {
                Pusher.DesktopApp.PusherDesktopAppInjectJsEvent ev = new Pusher.DesktopApp.PusherDesktopAppInjectJsEvent(PusherChannel, "alert('hi');");
                Pusher.PusherEventSender.TriggerEvent(ev);
            }
            return IsStartCall;
       //     return handler.StartCall(this);
             * */
        }
        private void CallTimerHangup()
        {
            // Create a timer with a two second interval.
            TimerHangup = new System.Timers.Timer((SecondRecordLeft) * 1000);// 5 seconds spare for different
            // Hook up the Elapsed event for the timer. 
            TimerHangup.Elapsed += OnTimerHangupEvent;
            TimerHangup.AutoReset = false;
            TimerHangup.Enabled = true;
        }

        private void OnTimerHangupEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            IsTimerHangup = true;
            handler.HangupCallByManager(this);
        }
        public override void AliceCdr(string hangupStatus, string duration, string callId)
        {
            aliceCallDurationSeconds = int.Parse(duration);
        }

        public override void DailingToBobStarted()
        {

            try
            {
                //      PusherServer.IGetResult<PusherServer.ChannelsList> result2 = NoProblem.Core.BusinessLogic.Pusher.PusherEventSender.getchannels();
                string js = ScriptPusherBuilder.BuildPrivateRecordBobCallingDataScript();
                Pusher.DesktopApp.PusherDesktopAppInjectJsEvent pusherEvent = new Pusher.DesktopApp.PusherDesktopAppInjectJsEvent(PusherChannel, js);
                Pusher.PusherEventSender.TriggerEvent(pusherEvent);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception sending pusher to desktop app after bidding done");
            }
        }
        public override void TwoSidesConnected()
        {

            try
            {
                string js = ScriptPusherBuilder.BuildPrivateRecordTwoSidesConnectedDataScript();
                Pusher.DesktopApp.PusherDesktopAppInjectJsEvent pusherEvent = new Pusher.DesktopApp.PusherDesktopAppInjectJsEvent(PusherChannel, js);
                Pusher.PusherEventSender.TriggerEvent(pusherEvent);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception sending pusher to desktop app after bidding done");
            }
        }
        public override void AliceAnswered()
        {
            CallTimerHangup();
            try
            {
                string js = ScriptPusherBuilder.BuildPrivateRecordAliceAnsweredDataScript();
                Pusher.DesktopApp.PusherDesktopAppInjectJsEvent pusherEvent = new Pusher.DesktopApp.PusherDesktopAppInjectJsEvent(PusherChannel, js);
                Pusher.PusherEventSender.TriggerEvent(pusherEvent);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception sending pusher to desktop app after bidding done");
            }
        }
        /*
        protected override void Hangup(FapiArgs args)
        {
            try
            {
                string js = ScriptPusherBuilder.BuildPrivateRecordHangUpDataScript();
                Pusher.DesktopApp.PusherDesktopAppInjectJsEvent pusherEvent = new Pusher.DesktopApp.PusherDesktopAppInjectJsEvent(PusherChannel, js);
                Pusher.PusherEventSender.TriggerEvent(pusherEvent);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception sending pusher to desktop app after bidding done");
            }
        }
        */
        public override List<string> GetAlicesPrompt()
        {
            return new List<string>() { AudioFilesUrlBase + "/callandrecord/call_and_record.wav" };
        }

        public override string GetAlicesPhone()
        {
            return userPhone;
        }

        public override string GetBobsPhone()
        {
            return targetPhone;
        }
        public override bool ShouldRecordCall()
        {
            return true;
        }
        const string UpdateDeskAppPrivateCallCommand = @"
UPDATE dbo.New_deskappprivatecallrecordExtensionBase
SET New_RecordURL = @RecordURL, New_DurationSeconds = @DurationSeconds
WHERE New_deskappprivatecallrecordId = @CallId";
        const string UpdateContactSecondLeftQuery = @"
DECLARE @seconds int
SELECT @seconds = ISNULL(New_DesktopAppSecondRecordLeft, 0)
FROM dbo.ContactExtensionBase
WHERE ContactId = @ContactId

SET @duration = @seconds - @duration

IF(@duration < 0)
	SET @duration = 0

UPDATE dbo.ContactExtensionBase
SET New_DesktopAppSecondRecordLeft = @duration
WHERE ContactId = @ContactId";
        public override void ContinueProcess(string recordingUrl, int duration)
        {
            /*
             *  case ('EndOfCall')://End of regular call
                    break;
                case ('NoOneAnswer')://
                    break;
                case ('TimeOver')
             * */
            if (TimerHangup != null)
                TimerHangup.Stop();
            string Message;
            if (aliceCallDurationSeconds > 0)
            {
                Message = "EndOfCall";
                try
                {
                    using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand(UpdateDeskAppPrivateCallCommand, conn))
                        {
                            cmd.Parameters.AddWithValue("@RecordURL", recordingUrl != null ? recordingUrl : string.Empty);
                            cmd.Parameters.AddWithValue("@DurationSeconds", aliceCallDurationSeconds);
                            cmd.Parameters.AddWithValue("@CallId", this.CallId);
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                        }
                        using (SqlCommand cmd = new SqlCommand(UpdateContactSecondLeftQuery, conn))
                        {
                            cmd.Parameters.AddWithValue("@ContactId", ContactId);
                            cmd.Parameters.AddWithValue("@duration", aliceCallDurationSeconds);
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                        }
                        conn.Close();
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Faild UPDATE New_deskappprivatecallrecordExtensionBase / ContactExtensionBase // CallId={0}", this.CallId);
                }
            }
            else
                Message = "NoOneAnswer";
            try
            {
                if (IsTimerHangup)
                    Message = "TimeOver";
                string js = ScriptPusherBuilder.BuildPrivateRecordHangUpDataScript(Message);
                Pusher.DesktopApp.PusherDesktopAppInjectJsEvent pusherEvent = new Pusher.DesktopApp.PusherDesktopAppInjectJsEvent(PusherChannel, js);
                Pusher.PusherEventSender.TriggerEvent(pusherEvent);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception sending pusher to desktop app after bidding done");
            }

        }
        public override string GetCallerIdForBob()
        {
            if (IsBlockCallerId)
                return null;
            return GetAlicesPhone();
        }
        private Guid CreateNewDeskAppPrivateCall()
        {
            NoProblem.Core.DataAccessLayer.deskappprivatecallrecord deskapp_dal = new NoProblem.Core.DataAccessLayer.deskappprivatecallrecord(XrmDataContext);
            DataModel.Xrm.new_deskappprivatecallrecord _new_deskapp = new DataModel.Xrm.new_deskappprivatecallrecord(XrmDataContext);

            _new_deskapp.new_calltophone = targetPhone;
            _new_deskapp.new_blockcallerid = IsBlockCallerId;
            _new_deskapp.new_contactphone = userPhone;
            _new_deskapp.new_durationrecordsecondleft = this.SecondRecordLeft;
            //     _new_deskapp.new_durationseconds = duration;
            //      _new_deskapp.new_recordurl = recordingUrl;
            _new_deskapp.new_calldate = CallDate;
            _new_deskapp.new_diffbetweenutctolocaltime_minute = DiffBetweenUtcToLocalTime_minute;
            _new_deskapp.new_contactid = ContactId;
            deskapp_dal.Create(_new_deskapp);
            XrmDataContext.SaveChanges();
            return _new_deskapp.new_deskappprivatecallrecordid;
        }

        private const int FOUR_HOURS = 1000 * 60 * 60 * 4;

        public override int GetLimitCallDurationSeconds()
        {
            return FOUR_HOURS;
        }
    }
}
