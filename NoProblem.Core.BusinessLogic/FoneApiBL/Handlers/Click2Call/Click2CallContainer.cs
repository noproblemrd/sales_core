﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call
{
    class Click2CallContainer
    {
        public IClick2CallManager CallManager { get; set; }

        public string AlicesCallId { get; set; }

        private volatile string bobsCallId;
        public string BobsCallId
        {
            get
            {
                return bobsCallId;
            }
            set
            {
                bobsCallId = value;
            }
        }

        private volatile bool closedAndProcessContinued = false;
        public bool ClosedAndProcessContinued
        {
            get
            {
                return closedAndProcessContinued;
            }
            set
            {
                closedAndProcessContinued = value;
            }
        }
    }
}
