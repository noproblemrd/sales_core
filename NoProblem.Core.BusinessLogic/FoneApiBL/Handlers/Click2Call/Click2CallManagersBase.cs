﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call
{
    abstract class Click2CallManagersBase : FoneApiHandlerBase, IClick2CallManager
    {
        public abstract List<string> GetAlicesPrompt();

        public virtual string GetBobsPrompt()
        {
            return null;
        }

        public virtual eActionAfterDtmf ActionAfterAlicePressedDtmf(string digits)
        {
            return eActionAfterDtmf.Connect;
        }

        public virtual void AliceCdr(string hangupStatus, string duration, string callId) { }

        public virtual void BobCdr(string hangupStatus, string duration, string callId) { }

        public virtual bool ShouldRecordCall()
        {
            return false;
        }

        public virtual void ContinueProcess(string recordingUrl, int duration) { }

        public virtual void ContinueProcess()
        {
            ContinueProcess(null, 0);
        }

        public abstract string GetAlicesPhone();

        public abstract string GetBobsPhone();

        public virtual void DailingToBobStarted() { }

        public virtual void TwoSidesConnected() { }

        public virtual string GetCallerIdForBob()
        {
            return GetAlicesPhone();
        }

        public virtual string GetCallerIdForAlice()
        {
            return Cid;
        }

        public virtual void AliceAnswered() { }

        public virtual string GetPromptForAliceHangup() { return null; }

        public virtual int GetLimitCallDurationSeconds()
        {
            return LimitCallDurationSeconds;
        }
    }
}
