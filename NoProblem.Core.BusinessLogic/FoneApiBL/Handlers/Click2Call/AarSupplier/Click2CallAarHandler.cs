﻿using FoneApiWrapper;
using FoneApiWrapper.CallBacks;
using FoneApiWrapper.Commands;
using NoProblem.Core.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call.AarSupplier
{
    internal class Click2CallAarHandler : Click2CallHandler
    {
        protected override string _HANDLERNAME
        {
            get
            {
                return FoneApiBL.FoneApiRouter.HandlersNames.CLICK_2_CALL_AAR_HANDLER;
            }
        }

        protected override CallBackResponse CallAnsweredByAlice(FapiArgs args)
        {
            Click2CallContainer container = GetcontainersHolderByAliceId[args[ArgsTypes.FAPI_CALL_ID]];
            int callDurationLimitSeconds = container.CallManager.GetLimitCallDurationSeconds();
            CallBackResponse response = new CallBackResponse(callDurationLimitSeconds);
            container.CallManager.AliceAnswered();
            DialToBob(container, response);
            return response;
        }

        
    }
}
