﻿using FoneApiWrapper;
using FoneApiWrapper.CallBacks;
using FoneApiWrapper.Commands;
using NoProblem.Core.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call
{
    internal class Click2CallHandler : FoneApiHandlerBase
    {
        
        private const string ALICE = "A";
        private const string BOB = "B";
        private static Dictionary<string, Click2CallContainer> containersHolderByAliceId = new Dictionary<string, Click2CallContainer>();
        private static Dictionary<string, Click2CallContainer> containersHolderByBobId = new Dictionary<string, Click2CallContainer>();

        protected virtual string _HANDLERNAME
        {
            get
            {
                return  FoneApiBL.FoneApiRouter.HandlersNames.CLICK_2_CALL_HANDLER;
            }
        }
        internal bool StartCall(
            IClick2CallManager callManager
            )
        {
            FoneApiClient client = new FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
            var call = client.Dial(eDialDestinationType.number,
                  NormalizePhone(callManager.GetAlicesPhone(), GlobalConfigurations.DefaultCountry),
                  returnAddress,
                  appId,
                  returnAddress,
                  _defaultDialTimeOut,
                  callManager.GetCallerIdForAlice(),
                  null,
                  null,
                  Trunk,
                  null,
                  new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_FAPI_SIDE, ALICE)
            );
            if (call.status == 0)
            {
                Click2CallContainer container = new Click2CallContainer();
                containersHolderByAliceId.Add(call.callId, container);
                container.AlicesCallId = call.callId;
                container.CallManager = callManager;
                return true;
            }
            else
            {
                callManager.ContinueProcess();
                return false;
            }
        }

        internal void HangupCallByManager(IClick2CallManager callManager)
        {
            var container = containersHolderByAliceId.FirstOrDefault(x => x.Value.CallManager == callManager);
            if (container.Value == null)
                return;
            FoneApiClient client = new FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
            client.Hangup(container.Value.AlicesCallId);
        }

        protected override FoneApiWrapper.CallBacks.CallBackResponse CallAnswered(FapiArgs args)
        {
            CallBackResponse retVal = null;
            if (args[_FAPI_SIDE] == ALICE)
            {
                retVal = CallAnsweredByAlice(args);
            }
            else
            {
                retVal = CallAnsweredByBob(args);
            }
            return retVal;
        }

        private CallBackResponse CallAnsweredByBob(FapiArgs args)
        {
            Click2CallContainer container = containersHolderByBobId[args[ArgsTypes.FAPI_CALL_ID]];
            int callDurationLimitSeconds = container.CallManager.GetLimitCallDurationSeconds();
            CallBackResponse response = new CallBackResponse(callDurationLimitSeconds);
            string prompt = container.CallManager.GetBobsPrompt();
            if (!String.IsNullOrEmpty(prompt))
            {
                response.PlayFile(prompt, null);
            }
            if (container.CallManager.ShouldRecordCall())
                response.RecordStart();
            container.CallManager.TwoSidesConnected();
            response.BridgeTo(container.AlicesCallId, returnAddress, new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME)
            );
            return response;
        }

        protected  virtual CallBackResponse CallAnsweredByAlice(FapiArgs args)
        {
            Click2CallContainer container = containersHolderByAliceId[args[ArgsTypes.FAPI_CALL_ID]];
            int callDurationLimitSeconds = container.CallManager.GetLimitCallDurationSeconds();
            CallBackResponse response = new CallBackResponse(callDurationLimitSeconds);
            container.CallManager.AliceAnswered();
            List<string> prompts = container.CallManager.GetAlicesPrompt();
            for (int i = 0; i < prompts.Count - 1; i++)
            {
                response.PlayFile(prompts[i], null);
            }
            response.FlushDtmfBuffer();
            response.PlayAndGetDtmf(prompts[prompts.Count - 1], returnAddress, 1, "#", 20, new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME));
            return response;
        }

        protected override CallBackResponse GetDtmfComplete(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            Click2CallContainer container = containersHolderByAliceId[args[ArgsTypes.FAPI_CALL_ID]];
            string digits;
            if (args.TryGetValue(ArgsTypes.FAPI_DIGITS, out digits))//something was pressed
            {
                //connect
                eActionAfterDtmf action = container.CallManager.ActionAfterAlicePressedDtmf(digits);
                switch (action)
                {
                    case eActionAfterDtmf.Connect:
                        DialToBob(container, response);
                        return response;
                    case eActionAfterDtmf.Repeat:
                        return CallAnsweredByAlice(args);
                    case eActionAfterDtmf.HangupWithPrompt:
                        string prompt = container.CallManager.GetPromptForAliceHangup();
                        if (!String.IsNullOrEmpty(prompt))
                        {
                            response.PlayFile(prompt, null);
                        }
                        break;
                }
            }
            //nothing pressed or action is hangup:
            response.Hangup();
            return response;
        }

        protected void DialToBob(Click2CallContainer container, CallBackResponse alicesCallResponse)
        {
            container.CallManager.DailingToBobStarted();
            FoneApiClient client = new FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
            var call = client.Dial(eDialDestinationType.number,
                  NormalizePhone(container.CallManager.GetBobsPhone(), GlobalConfigurations.DefaultCountry),
                  returnAddress,
                  appId,
                  returnAddress,
                  _defaultDialTimeOut,
                  container.CallManager.GetCallerIdForBob(),
                  null,
                  null,
                  Trunk,
                  container.AlicesCallId,
                  new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_FAPI_SIDE, BOB)
            );
            if (call.status != 0)
            {
                alicesCallResponse.Hangup();
                container.ClosedAndProcessContinued = true;
                container.CallManager.ContinueProcess();
            }
            else
            {
                container.BobsCallId = call.callId;
                containersHolderByBobId.Add(call.callId, container);
                /*
                alicesCallResponse.PlayFile(AudioFilesUrlBase + AudioFilesConsts.FAKE_RING, returnAddress,
                    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME)
                  );
                 * */
                PlayFakeRing(alicesCallResponse);
            }
        }
        protected void PlayFakeRing(CallBackResponse response)
        {
            response.PlayFile(AudioFilesUrlBase + AudioFilesConsts.FAKE_RING, returnAddress,
                    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME)
                  );
        }
        protected override CallBackResponse PlayFileComplete(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            response.PlayFile(AudioFilesUrlBase + AudioFilesConsts.FAKE_RING, returnAddress,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME)
            );
            return response;
        }

        protected override void Hangup(FapiArgs args)
        {
            if (args[_FAPI_SIDE] == ALICE)
            {
                HangupAlice(args);
            }
            else
            {
                HangupBob(args);
            }
        }

        private void HangupAlice(FapiArgs args)
        {
            string callId = args[ArgsTypes.FAPI_CALL_ID];
            string duration = args[ArgsTypes.FAPI_CALL_DURATION];
            string hangupStatus = args[ArgsTypes.FAPI_HANGUP_CAUSE];
            Click2CallContainer container = containersHolderByAliceId[callId];
            container.CallManager.AliceCdr(hangupStatus, duration, callId);
            string bobsCallId = container.BobsCallId;
            if (!string.IsNullOrEmpty(bobsCallId))
            {
                FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
                client.Hangup(bobsCallId);
            }
            else if (duration == 0.ToString())
            {
                container.CallManager.ContinueProcess();
            }
            else//check that the process is really going forward in some other thread, if not, continue here. sometimes when the call is very short (2 seconds) the "dtmf complete" event is not fired so this takes care of this edge case.
            {
                int counter = 0;
                while (!container.ClosedAndProcessContinued && counter < 3)
                {
                    System.Threading.Thread.Sleep(1000);
                    counter++;
                }
                if (!container.ClosedAndProcessContinued)
                {
                    container.CallManager.ContinueProcess();
                }
            }
            containersHolderByAliceId.Remove(callId);
        }

        private void HangupBob(FapiArgs args)
        {
            string duration = args[ArgsTypes.FAPI_CALL_DURATION];
            string hangupStatus = args[ArgsTypes.FAPI_HANGUP_CAUSE];
            string callId = args[ArgsTypes.FAPI_CALL_ID];
            Click2CallContainer container = containersHolderByBobId[callId];
            container.CallManager.BobCdr(hangupStatus, duration, callId);
            container.ClosedAndProcessContinued = true;
            string recordingUrl;
            args.TryGetValue(ArgsTypes.FAPI_RECORDING_URL, out recordingUrl);
            int durationInt;
            int.TryParse(duration, out durationInt);
            container.CallManager.ContinueProcess(recordingUrl, durationInt);
            FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
            client.Hangup(container.AlicesCallId);
            containersHolderByBobId.Remove(callId);
        }

        protected Dictionary<string, Click2CallContainer> GetcontainersHolderByAliceId
        {
            get { return containersHolderByAliceId; }
        }
    }
}
