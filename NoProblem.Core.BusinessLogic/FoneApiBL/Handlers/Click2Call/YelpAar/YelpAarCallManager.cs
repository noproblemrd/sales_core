﻿using NoProblem.Core.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call.YelpAar
{
    internal class YelpAarCallManager : Click2CallManagersBase
    {
        private Guid yelpAarCallId;
        private string supplierPhone;
        private string customerPhone;
        private string headingCode;

        private int callDurationSupplier;
        private string callIdSupplier;
        private string endStatusSupplier;
        private string callIdCustomer;
        private string endStatusCustomer;

        public YelpAarCallManager(Guid yelpAarCallId, string supplierPhone, string customerPhone, string headingCode)
        {
            this.yelpAarCallId = yelpAarCallId;
            this.supplierPhone = supplierPhone;
            this.customerPhone = customerPhone;
            this.headingCode = headingCode;
        }

        public bool StartCall()
        {
            Click2CallHandler handler = new Click2CallHandler();
            return handler.StartCall(this);
        }

        public override bool ShouldRecordCall()
        {
            return true;
        }

        public override List<string> GetAlicesPrompt()
        {
            //TODO: get real recording for AAR call. Here we have to do flavors probably.
            List<string> prompts = new List<string>();
            prompts.Add(AudioFilesUrlBase + "/aar/flavor_short_a/before_category.wav");
            prompts.Add(AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.MICHAEL + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.WAV_FILE_EXTENSION);
            prompts.Add(AudioFilesUrlBase + "/aar/flavor_short_a/after_category.wav");
            return prompts;
        }

        public override string GetAlicesPhone()
        {
            return supplierPhone;
        }

        public override string GetBobsPhone()
        {
            return customerPhone;
        }

        public override string GetCallerIdForAlice()
        {
            return customerPhone;
        }

        public override eActionAfterDtmf ActionAfterAlicePressedDtmf(string digits)
        {
            AAR.DtmfPressedThread t = new AAR.DtmfPressedThread(yelpAarCallId, digits);
            t.Start();
            switch (digits)
            {
                case "1":
                    return eActionAfterDtmf.Connect;
                case "3":
                    return eActionAfterDtmf.HangupWithPrompt;
                default:
                    return eActionAfterDtmf.Repeat;
            }
        }

        public override void AliceCdr(string hangupStatus, string duration, string callId)
        {
            int durationInt;
            int.TryParse(duration, out durationInt);
            callDurationSupplier = durationInt;
            callIdSupplier = callId;
            endStatusSupplier = hangupStatus;
        }

        public override void BobCdr(string hangupStatus, string duration, string callId)
        {
            callIdCustomer = callId;
            endStatusCustomer = hangupStatus;
        }

        public override void ContinueProcess(string recordingUrl, int duration)
        {
            AAR.AarManager manager = new AAR.AarManager(null);
            if (callIdSupplier == null)
                System.Threading.Thread.Sleep(1000);
            manager.CallEnded(yelpAarCallId, recordingUrl, duration, callDurationSupplier, endStatusCustomer, endStatusSupplier, callIdCustomer, callIdSupplier);
        }

        public override string GetPromptForAliceHangup()
        {
            return AudioFilesUrlBase + "/aar/flavor_short_a/remove_me.wav";
        }
    }
}
