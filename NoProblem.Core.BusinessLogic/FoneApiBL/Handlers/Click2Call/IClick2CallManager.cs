﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call
{
    interface IClick2CallManager
    {
        List<string> GetAlicesPrompt();
        string GetBobsPrompt();
        eActionAfterDtmf ActionAfterAlicePressedDtmf(string digits);
        void AliceCdr(string hangupStatus, string duration, string callId);
        void BobCdr(string hangupStatus, string duration, string callId);
        bool ShouldRecordCall();
        void ContinueProcess(string recordingUrl, int duration);
        void ContinueProcess();
        string GetAlicesPhone();
        string GetBobsPhone();
        void DailingToBobStarted();
        void TwoSidesConnected();
        string GetCallerIdForBob();
        string GetCallerIdForAlice();
        /***/
        void AliceAnswered();
        string GetPromptForAliceHangup();
        int GetLimitCallDurationSeconds();
    }
}
