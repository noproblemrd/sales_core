﻿using FoneApiWrapper;
using FoneApiWrapper.CallBacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call.ConnectAndRecord
{
    internal class C2CHanlerCustomerConnectAndRecord : Click2CallHandler
    {
        const string IS_PROMPT_VALUE = "1";
        const string IS_PROMPT_KEY = "npp1";
        protected override string _HANDLERNAME
        {
            get
            {
                return FoneApiBL.FoneApiRouter.HandlersNames.CLICK_2_CALL_CUSTOMER_CONNECT;
            }
        }
        protected  override CallBackResponse CallAnsweredByAlice(FapiArgs args)
        {
            Click2CallContainer container = GetcontainersHolderByAliceId[args[ArgsTypes.FAPI_CALL_ID]];
            int callDurationLimitSeconds = container.CallManager.GetLimitCallDurationSeconds();
            CallBackResponse response = new CallBackResponse(callDurationLimitSeconds);
            container.CallManager.AliceAnswered();
            List<string> prompts = container.CallManager.GetAlicesPrompt();
            for (int i = 0; i < prompts.Count - 1; i++)
            {
                response.PlayFile(prompts[i], null);
            }
            response.PlayFile(prompts[prompts.Count - 1], returnAddress, new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME)
                ,new CustomParam(IS_PROMPT_KEY, IS_PROMPT_VALUE));
       //     response.FlushDtmfBuffer();

            
     //       response.PlayAndGetDtmf(prompts[prompts.Count - 1], returnAddress, 1, "#", 20, new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME));
            return response;
        }
        protected override CallBackResponse PlayFileComplete(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            if (args.ContainsKey(IS_PROMPT_KEY) && args[IS_PROMPT_KEY] == IS_PROMPT_VALUE)
            {
                Click2CallContainer container = GetcontainersHolderByAliceId[args[ArgsTypes.FAPI_CALL_ID]];
                DialToBob(container, response);
            }
            else
                PlayFakeRing(response);
            return response;
        }
        
    }
}
