﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.Click2Call.ConnectAndRecord
{
    class ConnectAndRecordCallManager : Click2CallManagersBase
    {
        private Guid incidentAccountId;
        private string supplierPhone;
        private string customerPhone;
        internal delegate void CallEndedWithRecording(string recordingUrl, int duration);
        internal event CallEndedWithRecording OnCallEndedWithRecording;

        public ConnectAndRecordCallManager(Guid incidentAccountId, string supplierPhone, string customerPhone)
        {
            this.incidentAccountId = incidentAccountId;
            this.supplierPhone = supplierPhone;
            this.customerPhone = customerPhone;
        }

        public bool StartCall()
        {
            Click2CallHandler handler = new Click2CallHandler();
            return handler.StartCall(this);
        }

        public override bool ShouldRecordCall()
        {
            return true;
        }

        public override void ContinueProcess(string recordingUrl, int duration)
        {
            if (!String.IsNullOrEmpty(recordingUrl) && OnCallEndedWithRecording != null)
            {
                OnCallEndedWithRecording(recordingUrl, duration);
            }
        }

        public override string GetAlicesPhone()
        {
            return supplierPhone;
        }

        public override string GetBobsPhone()
        {
            return customerPhone;
        }
        public override string GetCallerIdForAlice()
        {
            return clipcall_cid;
        }
        public override void AliceCdr(string hangupStatus, string duration, string callId)
        {
            CreateCDR(hangupStatus, callId, duration, GetAlicesPhone(), "Connect And Record Adv Side", DataModel.Xrm.new_calldetailrecord.Type.jona_advertiser, incidentAccountId);
        }

        public override void BobCdr(string hangupStatus, string duration, string callId)
        {
            CreateCDR(hangupStatus, callId, duration, GetBobsPhone(), "Connect And Record Cust Side", DataModel.Xrm.new_calldetailrecord.Type.jona_consumer, incidentAccountId);
        }

        public override List<string> GetAlicesPrompt()
        {
            return new List<string>()
            {
                AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.MOBILE + AudioFilesConsts.SLASH +
                "AdvertiserPrompt.wav"
            };
        }

        private string _promptsClass;
        private string PromptsClass
        {
            get
            {
                if (String.IsNullOrEmpty(_promptsClass))
                {
                    _promptsClass = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.PROMPTS_CLASS);
                }
                return _promptsClass;
            }
        }
    }
}
