﻿using FoneApiWrapper;
using FoneApiWrapper.CallBacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading;
using System.Web;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.AarTextMessage
{
    internal class AarTextMessageHandler : FoneApiHandlerBase
    {
        /*
        private enum eDtmfStep
        {
            STEP1 = 1,
            PHONE_OK = 2,
            ALTHERNATIVE_PHONE = 3
        }
         * */
        private enum ePromptStep
        {
            STEP1 = 1,
            THANKS = 2,
            ENTER_ALTHERNATIVE_PHONE = 3,
            SENDING_TO = 4,
            CONFIRMED_PHONE = 5
        }
        private const int SECONDS_WAIT_AFTER_PROMPT = 5;
        private const int PROMPT_LOOP = 3;
        private const string _HANDLERNAME = FoneApiBL.FoneApiRouter.HandlersNames.AAR_TEXT_MESSAGE_HANDLER;
        private const string AAR_ID = "npp1";
        private const string DTMF_STEP = "npp2";
    //    private const string ZIPCODE = "npp3";
   //     private const string CATEGORY_NAME = "npp4";
        private const string SUPPLIER_PHONE = "npp5";
 //       private const string CONSUMER_PHONE = "npp6";
        private const string LOOP_INDEX = "npp7";
        private const string REDIAL_TIMES = "npp8";
        private const string AAR_INCIDENT_ID = "npp9";
        //private const string LOOP_INDEX = "npp0";

        private const int SHORT_SUPPLIER_NAME_LENGTH = 15;
        private const int SUPER_SHORT_SUPPLIER_NAME_LENGTH = 10;
        private const int DOUBLE_SHORT_SUPPLIER_NAME_LENGTH = 20;
        private const int SHORT_CATEGORY_NAME_LENGTH = 25;

        private static readonly string AarDestinationPage;
        private static readonly string DownloadMobileAppDestination;

        private NoProblem.Core.DataAccessLayer.Aar.AarRecord AarPromptStep1;

        static AarTextMessageHandler()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            AarDestinationPage = configDal.GetConfigurationSettingValue( NoProblem.Core.DataModel.ConfigurationKeys.AAR_VIDEO_DESTINATION_PAGE);
            DownloadMobileAppDestination = configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.DOWNLOAD_MOBILE_APP_DESTINATION);
        }
        /*
        public static void UpdateAarPromptStep1()
        {
         //   DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
         //   AarPromptStep1 = configDal.GetConfigurationSettingValueFromDB(NoProblem.Core.DataModel.ConfigurationKeys.AAR_VIDEOLEAD_PROMPT_TEST_STEP1);
            AarPromptStep1 = new NoProblem.Core.DataAccessLayer.Aar.AarRecord();
        }
         * */
        public AarTextMessageHandler() 
        { 

        }
        public AarTextMessageHandler(NoProblem.Core.DataAccessLayer.Aar.AarRecord AarPromptStep1) 
        {
            this.AarPromptStep1 = AarPromptStep1;
        }
        void SetAarPrompt(FapiArgs args)
        {
            string str = args[AAR_INCIDENT_ID];
            Guid AarIncidentId = new Guid(str);
            AarPromptStep1 = new DataAccessLayer.Aar.AarRecord(AarIncidentId);
        }
        internal bool StartCall(Guid yelpAarCallId, string supplierPhone, string customerPhone
            ,Guid AarIncidentId)//, string zipcode, string CategoryName, )
         {
      //       if (!GlobalConfigurations.IsProductionUsa())
      //           supplierPhone = "0543388914";
             customerPhone = CramblePhone(customerPhone);
             FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
             var call = client.Dial(FoneApiWrapper.Commands.eDialDestinationType.number,
                   NormalizePhone(supplierPhone, GlobalConfigurations.DefaultCountry),
                   returnAddress,
                   appId,
                   returnAddress,
                   _defaultDialTimeOut,
                   customerPhone,
                   null,
                   null,
                   Trunk,
                   null,
                  
                   new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                   new CustomParam(AAR_ID, yelpAarCallId.ToString()),
    //               new CustomParam(ZIPCODE, zipcode),
  //                  new CustomParam(CATEGORY_NAME, CategoryName),
                    new CustomParam(SUPPLIER_PHONE, supplierPhone),
 //                   new CustomParam(CONSUMER_PHONE, customerPhone)
                    new CustomParam(AAR_INCIDENT_ID, AarIncidentId.ToString())
                    /*
                   new CustomParam(_NPP_INCIDENTACCOUNT, incidentAccountId.ToString()),
                   new CustomParam(_NPP_MAXPRICE, decimal.ToInt32(decimal.Floor(maxPrice)).ToString()),
                   new CustomParam(_NPP_MINPRICE, decimal.ToInt32(decimal.Floor(minPrice)).ToString()),
                   new CustomParam(_NPP_TTSFILEURL, ttsFileUrl),
                   new CustomParam(_NPP_SUPPLIERPHONE, supplierPhone)
                    * */
             );
             if (call.status != 0)
             {
              //   MarkBidDoneThread tr = new MarkBidDoneThread(incidentAccountId);
              //   tr.Start();
                 return false;
             }
             return true;
         }
        protected override FoneApiWrapper.CallBacks.CallBackResponse CallAnswered(FapiArgs args)
        {
            SetAarPrompt(args);
            return ExecPromptStep1(args);
        }
        private bool CheckAndGetLoopNumber(FapiArgs args, out int LoopIndex)
        {
            if (args.ContainsKey(LOOP_INDEX))
            {
                LoopIndex = int.Parse(args[LOOP_INDEX]);
            }
            else
                LoopIndex = 1;

            return (LoopIndex <= PROMPT_LOOP);           
        }
        private FoneApiWrapper.CallBacks.CallBackResponse ExecPromptStep1(FapiArgs args)
        {

           
             int LoopIndex;
             CallBackResponse response = new CallBackResponse();
            if(!CheckAndGetLoopNumber(args, out LoopIndex))
            {
                response.Hangup();
                return response;
            }
             /*
              if (args.ContainsKey(LOOP_INDEX))
              {
                  LoopIndex = int.Parse(args[LOOP_INDEX]);
              }
              else
                  LoopIndex = 1;
            
             CallBackResponse response = new CallBackResponse();
             if (LoopIndex > PROMPT_LOOP)
             {
                 response.Hangup();
                 return response;
             }
             */
            response.PlayAndGetDtmf(
                AudioFilesUrlBase + AudioFilesConsts.GetAarFolder + AarPromptStep1.AarRecordFiles[(int)ePromptStep.STEP1].FileName,
                returnAddress,
                1,
                TERMINATION_KEY_POUND,
                SECONDS_WAIT_AFTER_PROMPT,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                   new CustomParam(AAR_ID, args[AAR_ID]),
                   new CustomParam(DTMF_STEP, ((int)ePromptStep.STEP1).ToString()),
                   new CustomParam(LOOP_INDEX, (++LoopIndex).ToString()),
                   new CustomParam(SUPPLIER_PHONE, args[SUPPLIER_PHONE]),
                   new CustomParam(AAR_INCIDENT_ID, args[AAR_INCIDENT_ID])


            );
            return response;
        }
        protected override CallBackResponse GetDtmfComplete(FapiArgs args)
        {
            SetAarPrompt(args);
            CallBackResponse response = new CallBackResponse();
            ePromptStep _step = GetPromptStep(args);
            switch(_step)
            {
                case (ePromptStep.STEP1):
                    return GetDtmfInterested(args);
                case(ePromptStep.THANKS):
                    response.Hangup();
                    return response;
                case (ePromptStep.ENTER_ALTHERNATIVE_PHONE):
                    return GetAlthernativePhoneNumber(args);
                case (ePromptStep.CONFIRMED_PHONE):
                    return GetConfirmedPhoneNumber(args);
                    
            }
            response.Hangup();
            return response;
        }
        

        private CallBackResponse GetConfirmedPhoneNumber(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            string digits;
            if (!GetDigits(args, out digits))//something was pressed
                digits = "1";
            if (digits == "2")
                return GetAlthernativePhoneNumber(args);
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
            {
                Guid AarCallId = new Guid(args[AAR_ID]);
                Guid AarIncidentId = new Guid(args[AAR_INCIDENT_ID]);
                SendTextMessage(AarIncidentId, AarCallId);
            }));
            return ExecThankYou(args);

        }

        private CallBackResponse GetAlthernativePhoneNumber(FapiArgs args)
        {
            
            string digits;
            if (!GetDigits(args, out digits) || !ValidatePhoneNumber(digits))//something was pressed
                return ExecPromptPressPhoneNumber(args);     
            
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
            {
                Guid AarCallId = new Guid(args[AAR_ID]);
                Guid AarIncidentId = new Guid(args[AAR_INCIDENT_ID]);
                SetAlthernativePhone(digits, AarCallId);
       //         SendTextMessage(AarIncidentId, AarCallId, digits);
            }));

            return ExecYourPhoneNumberIs(args, digits);
         

        }
        private CallBackResponse ExecYourPhoneNumberIs(FapiArgs args, string phone_number)
        {
            CallBackResponse response = new CallBackResponse();
            response.PlayFile(
              AudioFilesUrlBase + AudioFilesConsts.GetAarFolder + AarPromptStep1.AarRecordFiles[(int)ePromptStep.SENDING_TO].FileName, null);
            foreach (char digit in phone_number)
            {
                response.PlayFile(
                    AudioFilesUrlBase + AudioFilesConsts.GetAarFolder + AarPromptStep1.NumberFolder + AudioFilesConsts.SLASH + digit + "n" + AudioFilesConsts.WAV_FILE_EXTENSION,
                    null);
            }
            response.FlushDtmfBuffer();
            response.PlayAndGetDtmf(AudioFilesUrlBase + AudioFilesConsts.GetAarFolder + AarPromptStep1.AarRecordFiles[(int)ePromptStep.CONFIRMED_PHONE].FileName,
                returnAddress,
               1,
               TERMINATION_KEY_POUND,
               SECONDS_WAIT_AFTER_PROMPT,
               new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(AAR_ID, args[AAR_ID]),
                  new CustomParam(DTMF_STEP, ((int)ePromptStep.CONFIRMED_PHONE).ToString()),
                  new CustomParam(SUPPLIER_PHONE, args[SUPPLIER_PHONE]),
                //      new CustomParam(LOOP_INDEX, (++LoopIndex).ToString()),
                  new CustomParam(AAR_INCIDENT_ID, args[AAR_INCIDENT_ID])


           );

            return response;
        }
        private CallBackResponse GetDtmfInterested(FapiArgs args)
        {
            
            string digits;
            if (!GetDigits(args, out digits))//something was pressed
                return ExecPromptStep1(args);
            Guid yelpAarCallId = new Guid(args[AAR_ID]);
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
            {
                SetPressedDtmf(digits, yelpAarCallId);
                switch (digits)
                {
                    case ("3"):
                        SetDontCallMe(yelpAarCallId);
                        break;
                        /*
                    case ("1"):
                        SendTextMessage(new Guid(args[AAR_INCIDENT_ID]), yelpAarCallId);
                        break;
                         */
                }
               
            }));
            switch (digits)
            {                
                case ("8"):
                    return ExecPromptStep1(args);
                case("2"):
                    args[LOOP_INDEX] = "0";
                    return ExecPromptPressPhoneNumber(args);
                case("1"):
                    args[LOOP_INDEX] = "0";
                    //return ExecThankYou(args);
                    return ExecYourPhoneNumberIs(args, args[SUPPLIER_PHONE]);
            }
            CallBackResponse response = new CallBackResponse();
            response.Hangup();
            return response;
        }

        private CallBackResponse ExecThankYou(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            response.PlayFile(AudioFilesUrlBase + AudioFilesConsts.GetAarFolder + AarPromptStep1.AarRecordFiles[(int)ePromptStep.THANKS].FileName, returnAddress,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                   new CustomParam(AAR_ID, args[AAR_ID]),
                   new CustomParam(SUPPLIER_PHONE, args[SUPPLIER_PHONE]),
                   new CustomParam(DTMF_STEP, ((int)ePromptStep.THANKS).ToString()),
                   new CustomParam(AAR_INCIDENT_ID, args[AAR_INCIDENT_ID]));
            return response;
        }

        private CallBackResponse ExecPromptPressPhoneNumber(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            int LoopIndex;
            if(!CheckAndGetLoopNumber(args, out LoopIndex))
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
                {
                    Guid AarCallId = new Guid(args[AAR_ID]);
                    Guid AarIncidentId = new Guid(args[AAR_INCIDENT_ID]);
                    SendTextMessage(AarIncidentId, AarCallId);
                }));
                response.Hangup();
                return response;
            }
            
            response.PlayAndGetDtmf(
                AudioFilesUrlBase + AudioFilesConsts.GetAarFolder + AarPromptStep1.AarRecordFiles[(int)ePromptStep.ENTER_ALTHERNATIVE_PHONE].FileName,
                returnAddress,
                10,
                TERMINATION_KEY_POUND,
                SECONDS_WAIT_AFTER_PROMPT,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                   new CustomParam(AAR_ID, args[AAR_ID]),
                   new CustomParam(SUPPLIER_PHONE, args[SUPPLIER_PHONE]),
                   new CustomParam(DTMF_STEP, ((int)ePromptStep.ENTER_ALTHERNATIVE_PHONE).ToString()),
                   new CustomParam(LOOP_INDEX, (++LoopIndex).ToString()),
                   new CustomParam(AAR_INCIDENT_ID, args[AAR_INCIDENT_ID])


            );
            return response;
        }
        private ePromptStep GetPromptStep(FapiArgs args)
        {
            return (ePromptStep)int.Parse(args[DTMF_STEP]);
        }
        private const string SetDontCallMeQuery = @"
UPDATE S
SET S.New_DontCallMe = 1
FROM dbo.New_yelpsupplierExtensionBase S
	INNER JOIN dbo.New_yelpaarcallExtensionBase C on S.New_yelpsupplierId = C.New_YelpSupplierId
WHERE C.New_yelpaarcallId = @yelpAarCallId";
        private void SetDontCallMe(Guid yelpAarCallId)
        {
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(SetDontCallMeQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@yelpAarCallId", yelpAarCallId);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }               
                conn.Close();
            }
        }
        private const string SetPressedDtmfQuery = @"
UPDATE dbo.New_yelpaarcallExtensionBase
SET New_DTMFpressed = @DtmfPressed
    {IsSuccess}
WHERE New_yelpaarcallId = @yelpAarCallId";
        private const string SetPressedDtmfQueryIsSuccess = @",New_SuccessfulCall = 1";
        private const string UpdateSuccessfulCallQuery = @"
UPDATE S
SET S.New_SuccessfulCallCount = ISNULL(S.New_SuccessfulCallCount, 0) + 1
FROM dbo.New_yelpsupplierExtensionBase S
	INNER JOIN dbo.New_yelpaarcallExtensionBase C on S.New_yelpsupplierId = C.New_YelpSupplierId
WHERE C.New_yelpaarcallId = @yelpAarCallId";

        private void SetPressedDtmf(string digit, Guid yelpAarCallId)
        {
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                string _query = (digit == "1") ? SetPressedDtmfQuery.Replace("{IsSuccess}", SetPressedDtmfQueryIsSuccess) :
                    SetPressedDtmfQuery.Replace("{IsSuccess}", string.Empty);
                using (SqlCommand cmd = new SqlCommand(_query, conn))
                {
                    cmd.Parameters.AddWithValue("@DtmfPressed", digit);
                    cmd.Parameters.AddWithValue("@yelpAarCallId", yelpAarCallId);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                if (digit == "1")
                {
                    using (SqlCommand cmd = new SqlCommand(UpdateSuccessfulCallQuery, conn))
                    {
                        cmd.Parameters.AddWithValue("@yelpAarCallId", yelpAarCallId);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
                conn.Close();
            }
        }
        private const string SetHangupQuery = @"
UPDATE dbo.New_yelpaarcallExtensionBase
SET New_CallLengthSupplier = @CallDuration
	,New_CallEndStatusSupplier = @CallStatus
WHERE New_yelpaarcallId = @yelpaarcallId
";
        private const string UpdateHangupSupplierCallQuery = @"
UPDATE S
SET S.New_CallCount = ISNULL(S.New_CallCount, 0) + 1   
FROM dbo.New_yelpsupplierExtensionBase S
	INNER JOIN dbo.New_yelpaarcallExtensionBase C on S.New_yelpsupplierId = C.New_YelpSupplierId
WHERE C.New_yelpaarcallId = @yelpAarCallId";
     //   private const string UpdateHangupSupplierCallQueryIsSuccess = @",S.New_SuccessfulCallCount = ISNULL(S.New_SuccessfulCallCount, 0) + 1";
        protected override void Hangup(FapiArgs args)
        {
            SetAarPrompt(args);
            int duration;
            if(!int.TryParse(args[ArgsTypes.FAPI_CALL_DURATION], out duration))
                duration = 0;
            Guid YelpAarCallId = new Guid(args[AAR_ID]);
            Guid AarIncidentId = new Guid(args[AAR_INCIDENT_ID]);
            SetHangupData(duration, args[ArgsTypes.FAPI_HANGUP_CAUSE], YelpAarCallId);
            string DtmfPressed = GetDTMFpressed(YelpAarCallId);
            if (DtmfPressed == "1" || DtmfPressed == "2")
                SendTextMessage(AarIncidentId, YelpAarCallId);
            if (ToRedail(args, duration, DtmfPressed))
            {
                NoProblem.Core.BusinessLogic.AAR.AarManager am = new AAR.AarManager(null);
                am.CreateTextMessagesForNoAnswer(YelpAarCallId, AarIncidentId);
            }
        }
        bool ToRedail(FapiArgs args, int duration, string DtmfPressed)
        {
            if (!HangupCauses.IsValidHangup(args[ArgsTypes.FAPI_HANGUP_CAUSE]) || !string.IsNullOrEmpty(DtmfPressed))
                return false;
            if (duration < 3)
                return true;            
            int NoAnswer = (AarPromptStep1.AarRecordFiles[(int)ePromptStep.STEP1].Duration) * PROMPT_LOOP;
            return duration >= NoAnswer;
        }
       
        private string GetDTMFpressed(Guid yelpAarCallId)
        {
            string DtmfPressed = "";
            string command = @"
SELECT New_DTMFpressed
FROM dbo.New_yelpaarcallExtensionBase WITH(NOLOCK)
WHERE New_yelpaarcallId = @YelpAarCallId";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@YelpAarCallId", yelpAarCallId);
                    object o = cmd.ExecuteScalar();
                    DtmfPressed = (o == DBNull.Value) ? "" : (string)o;
                }
                conn.Close();
            }
           // return !string.IsNullOrEmpty(DtmfPress);
            return DtmfPressed;
        }

        private void SetHangupData(int CallDuration, string CallStatus, Guid yelpAarCallId)
        {
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(SetHangupQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@CallDuration", CallDuration);
                    cmd.Parameters.AddWithValue("@CallStatus", CallStatus);
                    cmd.Parameters.AddWithValue("@yelpAarCallId", yelpAarCallId);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
          //      string _query = (CallDuration > 0) ? UpdateHangupSupplierCallQuery.Replace("{IsSuccess}", UpdateHangupSupplierCallQueryIsSuccess) :
          //          UpdateHangupSupplierCallQuery.Replace("{IsSuccess}", string.Empty);
                
                using (SqlCommand cmd = new SqlCommand(UpdateHangupSupplierCallQuery, conn))
                {                        
                    cmd.Parameters.AddWithValue("@yelpAarCallId", yelpAarCallId);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                
                conn.Close();
            }
        }
        /*
        private void SendTextMessage(string SupplierPhone, string zipcode, string CategoryName, string yelpAarCallId)
        {
            Notifications noti = new Notifications(null);
            Bitly.BitlyDriver bd = new Bitly.BitlyDriver(null);
            string url = bd.ShortenUrl(AarDestinationPage + yelpAarCallId);
            noti.SendSMSTemplate(string.Format(TextMessageNotification, CategoryName, zipcode, url), SupplierPhone, GlobalConfigurations.DefaultCountry);

        }
         * */
        /*
        internal bool SendMissedLead(Guid accountId, Guid incidentId)
        {
            string message = "{0} Your ClipCall lead was taken :( Click {1} to install the app and claim your free lead. To remove reply: remov";
            Account acc = GetAccountDetailsforTextMessage(accountId, incidentId);
            if (acc == null)
                return false;
            GoogleShorten.GoogleShortenUrl gsu = new GoogleShorten.GoogleShortenUrl(null);
            string url = gsu.ShortenUrl(DownloadMobileAppDestination + acc.CallId.ToString());
            if (string.IsNullOrEmpty(url))
                return false;
            message = string.Format(message, acc.name, url);
            return _SendTextMessage(acc.phone, message);

        }
         * */
        /*
        internal bool SendAfterRequestDone(Guid accountId)
        {
            string message = @"{0} you like the ClipCall lead? Keep’m coming by installing the app {1} To remove reply:remove";
            Account acc = GetAccountDetailsforTextMessage(accountId);
            if (acc == null)
                return false;
            GoogleShorten.GoogleShortenUrl gsu = new GoogleShorten.GoogleShortenUrl(null);
            string url = gsu.ShortenUrl(DownloadMobileAppDestination + accountId.ToString());
            if (string.IsNullOrEmpty(url))
                return false;
            message = string.Format(message, acc.name, url);
            return _SendTextMessage(acc.phone, message);
        }
         * */
        public bool SendAfterCall(Guid accountId, Guid incidentId, bool IsSuccess)
        {
          //string messageUnSuccess = @"{0} We were unable to connect you to {1}, but the lead is yours. Install the app to get more leads {2} To remove reply:remove";
          string messageUnSuccess = @"{0} We were unable to connect you to the customer. You can still contact them and other new leads by installing the app {1} To remove reply:remove";
            string messageSuccess = @"{0} Did you like the ClipCall lead from {1}? Keep’m coming by installing the app {2} To remove reply:remove";
            //M & MOVER We were unable to connect you to 6145543456, but the lead is yours. Install the app to get more leads. bit.ly/xxxxxxx To remove reply: remove
            
            Account acc = GetAccountDetailsforTextMessage(accountId, incidentId);
            if (acc == null)
                return false;
            /*
            GoogleShorten.GoogleShortenUrl gsu = new GoogleShorten.GoogleShortenUrl(null);
            string url = gsu.ShortenUrl(DownloadMobileAppDestination + acc.CallId.ToString());
             * */
            NoProblem.Core.BusinessLogic.GoogleShorten.FactoryShortenUrl shortenUrl = 
                new GoogleShorten.FactoryShortenUrl(DownloadMobileAppDestination + acc.CallId.ToString());
            string url = shortenUrl.ShortenUrl();
            if (string.IsNullOrEmpty(url))
                return false;
            
            int ShortName = IsSuccess ? DOUBLE_SHORT_SUPPLIER_NAME_LENGTH : SUPER_SHORT_SUPPLIER_NAME_LENGTH;
            string message = IsSuccess ? string.Format(messageSuccess, GetShortName(acc.name, ShortName), acc.CustomerPhone, url) : 
                string.Format(messageUnSuccess, GetShortName(acc.name, ShortName), url);
      //      message = string.Format(message, GetShortName(acc.name, ShortName), acc.CustomerPhone, url);
            return _SendTextMessage(acc.phone, message);

        }
        
        
        internal bool SendTextMessage(Guid AarIncidentId, Guid yelpAarCallId)//, string AlthernativePhone = null)
        {
            string command = "EXEC [dbo].[GetAarCallDetailsForTextMessage] @AarIncidentId, @AarCallId";
            string SupplierName = "", CategoryName = "", zipcode = "", SupplierPhone = "", AlthernativePhone = "", shorCategorytName = "";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@AarIncidentId", AarIncidentId);
                    cmd.Parameters.AddWithValue("@AarCallId", yelpAarCallId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        SupplierName = (string)reader["Supplier"];
                        CategoryName = (string)reader["Expertise"];
                        shorCategorytName = reader["New_shortName"] == DBNull.Value ? "" : (string)reader["New_shortName"];
                        zipcode = (string)reader["Zipcoe"];
                        SupplierPhone = (string)reader["Phone"];
                        AlthernativePhone = reader["AlthernativePhone"] == DBNull.Value ? "" : (string)reader["AlthernativePhone"];
                    }
                    else
                        return false;
                    conn.Close();
                }
            }
            shorCategorytName = string.IsNullOrEmpty(shorCategorytName) ? CategoryName : shorCategorytName;
            string phone = string.IsNullOrEmpty(AlthernativePhone) ? SupplierPhone : AlthernativePhone;
       /*     
            Bitly.BitlyDriver bd = new Bitly.BitlyDriver(null);
            string url = bd.ShortenUrl(AarDestinationPage + yelpAarCallId);
         */    
#if DEBUG
            if ("ISRAEL" == GlobalConfigurations.DefaultCountry && !phone.StartsWith("0"))
                return false;
#endif
            /*
            GoogleShorten.GoogleShortenUrl gsu = new GoogleShorten.GoogleShortenUrl(null);
            string url = gsu.ShortenUrl(AarDestinationPage + yelpAarCallId);
             * */
            NoProblem.Core.BusinessLogic.GoogleShorten.FactoryShortenUrl shortenUrl = 
                new GoogleShorten.FactoryShortenUrl(AarDestinationPage + yelpAarCallId);
            string url = shortenUrl.ShortenUrl();
            if (string.IsNullOrEmpty(url))
                return false;
            /*
            int _indx = url.IndexOf("http://");
            if (_indx > -1)
                _indx += "http://".Length;
             *  url = url.Substring(_indx, url.Length - _indx);
             */
          //  url = url.Replace("http://", "");

     //       string text_message = rnd.Next(2) == 0 ? TextMessageNotificationV5 : TextMessageNotificationV6;
            string text_message = TextMessageNotificationV7;
            /*
            if (!noti.SendSMSTemplate(string.Format(text_message, GetShortName(SupplierName, SHORT_SUPPLIER_NAME_LENGTH),
                GetShortName(shorCategorytName, SHORT_CATEGORY_NAME_LENGTH), url), phone, GlobalConfigurations.DefaultCountry))
               return false;
             * */
            if (!_SendTextMessage(phone, string.Format(text_message, GetShortName(SupplierName, SHORT_SUPPLIER_NAME_LENGTH),
                GetShortName(shorCategorytName, SHORT_CATEGORY_NAME_LENGTH), url)))
                return false;
            
   //         string url = "http://localhost:51700/tm/ + yelpAarCallId;
  //          noti.SendSMSTemplate(string.Format(TextMessageNotification, SupplierName, CategoryName, zipcode, url), SupplierPhone, GlobalConfigurations.DefaultCountry);
 //           noti.SendSMSTemplate(string.Format(TextMessageNotification, url), phone, GlobalConfigurations.DefaultCountry);
            SetTextMessage(yelpAarCallId, text_message);
            return true;

        }
   //     private static Random rnd = new Random(DateTime.Now.Millisecond);
  //      private const string TextMessageNotificationV1 = "We have a customer requesting the services of a {0} in your zip code. View a video of this lead here: {1}. To remove reply: remove";
      //  private const string TextMessageNotificationV2 = "Want to see a video from a customer looking for a {0} in your area? click to view: {1}";
    //    private const string TextMessageNotificationV3 = "Watch a video lead of a customer looking for a {0} in your zip code: {1}";
  //      private const string TextMessageNotificationV4 = "{0}? We have a customer requesting your services in your zip code. View a video of this lead: {1}. To remove reply: remove";
     //   private const string TextMessageNotification = "Hello {0}, We have a free video lead for {1} in {2}. {3} watch now.";
    //    private const string TextMessageNotificationV5 = "{0} a customer is looking for a {1} in your zip code. See a video of this lead: {2} To remove reply: remove";

   //     private const string TextMessageNotificationV6 = "{0} watch a video lead of a customer looking for a {1} in your zip code: {2} To remove reply: remove";
        private const string TextMessageNotificationV7 = @"{0}, watch a video from a customer looking for a {1} in your area right now: {2} To remove reply: remove";

        private const string TextMessageNotification = "To see your new lead from VideoLeadPro: {0}";
        private bool _SendTextMessage(string phone, string message)
        {
            Notifications noti = new Notifications(null);
#if DEBUG            
            if ("ISRAEL" == GlobalConfigurations.DefaultCountry && !phone.StartsWith("0"))
                return false;
#endif
            return noti.SendSMSTemplate(message, phone, GlobalConfigurations.DefaultCountry);
                
        }
        private const string SetTextMessageQuery = @"
UPDATE dbo.New_yelpaarcallExtensionBase 
SET New_TextMessage = @TextMessage
WHERE New_yelpaarcallId = @YelpAarCallId";
        private void SetTextMessage(Guid yelpAarCallId, string text_message)
        {
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(SetTextMessageQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@TextMessage", text_message);
                    cmd.Parameters.AddWithValue("@YelpAarCallId", yelpAarCallId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
        public bool SendMissedConnectMeToCustomer(Guid AarCallId)
        {
            string message = "{0} Your ClipCall lead was taken :( Click {1} to install the app and claim your free lead. To remove reply: remove";
            string command = @"SELECT S.New_name, S.New_Phone, S.New_AlthernativePhone
FROM dbo.New_yelpsupplierExtensionBase S
	INNER JOIN dbo.New_yelpaarcallExtensionBase C on C.New_YelpSupplierId = S.New_yelpsupplierId
WHERE C.New_yelpaarcallId = @AarCallId";

            string SupplierName = "", SupplierPhone = "", AlthernativePhone = "";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@AarSupplier", AarCallId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        SupplierName = (string)reader["New_name"];
                        SupplierPhone = (string)reader["New_Phone"];
                        AlthernativePhone = reader["New_AlthernativePhone"] == DBNull.Value ? "" : (string)reader["AlthernativePhone"];
                    }
                    else
                        return false;
                    conn.Close();
                }
            }
            string phone = string.IsNullOrEmpty(AlthernativePhone) ? SupplierPhone : AlthernativePhone;
            /*
            GoogleShorten.GoogleShortenUrl gsu = new GoogleShorten.GoogleShortenUrl(null);
            string url = gsu.ShortenUrl(DownloadMobileAppDestination + AarCallId.ToString());
             * */
            NoProblem.Core.BusinessLogic.GoogleShorten.FactoryShortenUrl shortenUrl = new GoogleShorten.FactoryShortenUrl(DownloadMobileAppDestination + AarCallId.ToString());
            string url = shortenUrl.ShortenUrl();
            if (string.IsNullOrEmpty(url))
                return false;
            string _message = string.Format(message, GetShortName(SupplierName, SHORT_CATEGORY_NAME_LENGTH), url);
            return _SendTextMessage(phone, _message);
        }
        private string GetShortName(string SupplierName, int length)
        {
            if (string.IsNullOrEmpty(SupplierName))
                return string.Empty;
            if (SupplierName.Length <= length)
                return SupplierName;
            return SupplierName.Substring(0, (length -2)) + "..";
        }
        private const string SetAlthernativePhoneQuery = @"
UPDATE S 
SET S.New_AlthernativePhone = @AlthernativePhone
FROM dbo.New_yelpsupplierExtensionBase S
	INNER JOIN dbo.New_yelpaarcallExtensionBase C on S.New_yelpsupplierId = C.New_YelpSupplierId
WHERE C.New_yelpaarcallId = @YelpAarCallId";
        private void SetAlthernativePhone(string AlthernativePhone, Guid YelpAarCallId)
        {
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(SetAlthernativePhoneQuery, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@AlthernativePhone", AlthernativePhone);
                    cmd.Parameters.AddWithValue("@YelpAarCallId", YelpAarCallId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
        private string CramblePhone(string phoneNumber)
        {
            if (phoneNumber.Length < 4)
                return phoneNumber;
            string result = phoneNumber.Substring(0, phoneNumber.Length - 3);
            for (int i = phoneNumber.Length - 3; i < phoneNumber.Length; i++)
            {
                int c = (int)(phoneNumber[i]);
                c = (c == 57) ? 48 : c + 1;
                result += (char)c;
            }
            return result;
        }
        private Account GetAccountDetailsforTextMessage(Guid AccountId, Guid IncidentId)
        {
            Account acc = null;
            string command = @"SELECT Telephone1, Name, New_FullName
FROM dbo.Account WITH(NOLOCK)
WHERE AccountId = @accountId";
            string commandCallId = @"
select top 1 C.New_yelpaarcallId, inc.New_telephone1 CustomerPhone
from dbo.New_yelpaarcall C WITH(NOLOCK)
	inner join dbo.New_yelpsupplierExtensionBase S WITH(NOLOCK) on S.New_yelpsupplierId = C.New_YelpSupplierId
    inner join dbo.IncidentExtensionBase inc WITH(NOLOCK) on inc.IncidentId = C.New_IncidentId
where C.New_IncidentId = @IncidentId
	AND S.New_AccountId = @AccountId
order by C.CreatedOn desc";
            using(SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                   
                    cmd.Parameters.AddWithValue("@accountId", AccountId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        acc = new Account();
                        acc.phone = (string)reader["Telephone1"];
                        string _name = reader["New_FullName"] == DBNull.Value ? (reader["Name"] == DBNull.Value ? string.Empty : (string)reader["New_FullName"])
                            : (string)reader["New_FullName"];
                        acc.name = _name;
                    }                    
                    reader.Dispose();
                    cmd.Dispose();
                    
                }
                if (acc != null)
                {
                    using (SqlCommand cmd = new SqlCommand(commandCallId, conn))
                    {
                        cmd.Parameters.AddWithValue("@accountId", AccountId);
                        cmd.Parameters.AddWithValue("@IncidentId", IncidentId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if(reader.Read())
                        {
                            acc.CallId = (Guid)reader["New_yelpaarcallId"];
                            acc.CustomerPhone = reader["CustomerPhone"] == DBNull.Value ? string.Empty : (string)reader["CustomerPhone"];
                        }
                        
                    }
                }
                conn.Close();
            }
            return acc;
        }
        private class Account
        {
            public string name { get; set; }
            public string phone { get; set; }
            public Guid CallId { get; set; }
            public string CustomerPhone { get; set; }
        }
    }
}
