﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.IncomingCalls
{
    internal class IncomingCallsContainer
    {
        public string IncomingCallId { get; set; }
        public string OutgoingCallId { get; set; }
        public string CustomerPhone { get; set; }
        public string AdvertiserPhone { get; set; }
        public string VirtualNumber { get; set; }
        public bool Reroute { get; set; }
        public bool RerouteNow { get; set; }
        public List<Guid> ContactedAdvertisers { get; set; }
        public Guid RerouteHeadingId { get; set; }
        public Guid RerouteRegionId { get; set; }
        public Guid RerouteOriginId { get; set; }
        public bool IsReroutedCall { get; set; }
        public DataModel.Xrm.account.DapazStatus RerouteCurrentSupplierDapazStatus { get; set; }

        public IncomingCallsContainer()
        {
            ContactedAdvertisers = new List<Guid>();
        }
    }
}
