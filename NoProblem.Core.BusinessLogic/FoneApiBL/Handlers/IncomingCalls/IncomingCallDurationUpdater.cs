﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.IncomingCalls
{
    internal class IncomingCallDurationUpdater : Runnable
    {
        private string callId;
        private int duration;

        public IncomingCallDurationUpdater(string callId, int duration)
        {
            this.callId = callId;
            this.duration = duration;
        }

        protected override void Run()
        {
            try
            {
                DataAccessLayer.VnStatus dal = new NoProblem.Core.DataAccessLayer.VnStatus(XrmDataContext);
                dal.UpdateIncomingCallDuration(callId, duration);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.IncomingCalls.IncomingCallDurationUpdater.Run");
            }
        }
    }
}
