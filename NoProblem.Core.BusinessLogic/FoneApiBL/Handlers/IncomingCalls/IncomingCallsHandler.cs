﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Effect.Crm.Logs;
using FoneApiWrapper;
using FoneApiWrapper.CallBacks;
using NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.IncomingCalls;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers
{
    internal class IncomingCallsHandler : FoneApiHandlerBase
    {
        private static SynchronizedCollection<IncomingCallsContainer> onAirCallsListForIn = new SynchronizedCollection<IncomingCallsContainer>();
        private static SynchronizedCollection<IncomingCallsContainer> onAirCallsListForOut = new SynchronizedCollection<IncomingCallsContainer>();
        private static SynchronizedCollection<TimerContainer> timersHolder = new SynchronizedCollection<TimerContainer>();

        private const string _HANDLERNAME = FoneApiRouter.HandlersNames.INCOMING_CALLS_HANDLER;
        private const string _ANTIFRAUDBLOCKED = "AntiFraudBlocked";
        private const string _BLACKLIST = "Blacklist";
        private const string _FAPI_INCOMINGCALLID = "npp1";
        private const string _FAPI_IVRADVERTISERANNOUNCE = "npp2";
        private const string _FAPI_REROUTING = "npp3";
        private const string _FAPI_RECORDTHISCALL = "npp4";
        private const string _OUTCALLFAILED = "OutCallFailed";
        private const string _LOG_MESSAGE_FOR_VN_NOT_FOUND = "FoneApi: VnRequest not found in DB. IvrSession: {0}. IvrStatus: {1}";
        private const string _FAPI_ISUNMAPPED = "npp5";
        private const string _FAPI_LIMITCALLDURATION = "npp6";
        private const string _NPP_CUSTOM = "npp7";
        private const int _rerouteDialTimeOut = 30;
        private const string NO_POSSIBLE_REROUTE_FOUND = "NO_POSSIBLE_REROUTE_FOUND";
        private const string DIDNT_PRESS_ONE = "DIDNT_PRESS_ONE";

        protected override CallBackResponse IncomingCall(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            response.Answer();
            string callId = args[ArgsTypes.FAPI_CALL_ID];
            string didNumber = args[ArgsTypes.FAPI_DID_NUMBER];
            didNumber = NormalizeIncomingDidPhone(didNumber);
            string customerCallerId = args[ArgsTypes.FAPI_CALLER_ID_NUMBER];
            string sourceUrl = args[ArgsTypes.FAPI_CALL_SOURCE_ADDRESS];

            Dialer.IncomingCallsAntiFraud fraudAnalist = new NoProblem.Core.BusinessLogic.Dialer.IncomingCallsAntiFraud(XrmDataContext);
            bool passed = fraudAnalist.CheckAntiFraud(customerCallerId);
            if (!passed)
            {
                SaveVnRequest(callId, didNumber, customerCallerId, sourceUrl, false, true, _ANTIFRAUDBLOCKED, null, null, null, false, Guid.Empty);
                response.Hangup();
                return response;
            }

            Phones phones = new Phones(XrmDataContext, null);
            var dnQueryResponse = phones.GetPhoneNumber(callId, didNumber, customerCallerId);
            LogUtils.MyHandle.WriteToLog(1, Newtonsoft.Json.JsonConvert.SerializeObject(dnQueryResponse));

            if (dnQueryResponse.IsBlacklisted)
            {
                //handle black listed customer in incoming calls.
                SaveVnRequest(callId, didNumber, customerCallerId, sourceUrl, false, false, _BLACKLIST, dnQueryResponse.CallType, dnQueryResponse.TruePhoneNumber.FirstOrDefault(), null, dnQueryResponse.IsRecording, dnQueryResponse.ProviderId);
                response.Hangup();
            }
            else if (dnQueryResponse.IsAvailable)
            {
                string callerAudioFile;
                if (!string.IsNullOrEmpty(dnQueryResponse.IvrCallerAnnounceAdvertiserSpecific))//specific caller prompt file at supplier level:
                {
                    
                    //play caller announce
                    callerAudioFile = AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.INCOMING_CALLS + AudioFilesConsts.SLASH + dnQueryResponse.IvrCallerAnnounceAdvertiserSpecific;
                }
                else if (!string.IsNullOrEmpty(dnQueryResponse.IvrCallerAnnounce))//general caller prompt file for the environment:
                {
                    //play caller announce
                    callerAudioFile = AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.INCOMING_CALLS + AudioFilesConsts.SLASH + dnQueryResponse.IvrCallerAnnounce + AudioFilesConsts.WAV_FILE_EXTENSION;
                }
                else//no caller prompt, so he heards a ringing tone:
                {
                    //play fake ring
                    callerAudioFile = AudioFilesUrlBase + AudioFilesConsts.FAKE_RING;
                }
                response.PlayFile(
                        callerAudioFile,
                        returnAddress,
                        new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                        new CustomParam(_FAPI_INCOMINGCALLID, callId),
                        new CustomParam(_FAPI_SIDE, _CUSTOMER));

                //execute second call
                List<CustomParam> cpList = new List<CustomParam>(6);
                cpList.Add(new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME));
                cpList.Add(new CustomParam(_FAPI_INCOMINGCALLID, callId));
                cpList.Add(new CustomParam(_FAPI_IVRADVERTISERANNOUNCE, dnQueryResponse.IvrAdvertiserAnnounce == null ? string.Empty : dnQueryResponse.IvrAdvertiserAnnounce));
                cpList.Add(new CustomParam(_FAPI_RECORDTHISCALL, dnQueryResponse.IsRecording.ToString()));
                cpList.Add(new CustomParam(_FAPI_LIMITCALLDURATION, dnQueryResponse.LimitCallDurationSeconds.ToString()));
                if (!String.IsNullOrEmpty(dnQueryResponse.CustomField))
                {
                    cpList.Add(new CustomParam(_NPP_CUSTOM, dnQueryResponse.CustomField));
                }
                FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
                var advCall = client.Dial(
                    FoneApiWrapper.Commands.eDialDestinationType.number,
                    NormalizePhone(dnQueryResponse.TruePhoneNumber.First(), dnQueryResponse.SupplierCountry),
                    returnAddress,
                    appId,
                    returnAddress,
                    dnQueryResponse.Reroute ? _rerouteDialTimeOut : _defaultDialTimeOut,
                    dnQueryResponse.DisplayPhoneNumber,
                    null,
                    null,
                    Trunk,
                    callId,
                    cpList.ToArray());
                if (advCall.status != 0)
                {
                    //handle outgoing call bad status in incoming calls.
                    response = new CallBackResponse();//clear any prior actions.
                    response.Answer();
                    response.Hangup();
                    SaveVnRequest(callId, didNumber, customerCallerId, sourceUrl, false, false, _OUTCALLFAILED, dnQueryResponse.CallType, dnQueryResponse.TruePhoneNumber.FirstOrDefault(), advCall.callId, dnQueryResponse.IsRecording, dnQueryResponse.ProviderId);
                    LogUtils.MyHandle.WriteToLog("Failed to make outgoing call after incoming call in foneapi. incomingCallId: {0}. outgoingCallStatus: {1}. outgoingCallErrorMsg: {2}", callId, advCall.status, advCall.errorMsg);
                }
                else
                {
                    CreateOnAirCallsManagementDbEntry(callId, advCall.callId);
                    IncomingCallsContainer container = new IncomingCallsContainer();
                    container.OutgoingCallId = advCall.callId;
                    container.IncomingCallId = callId;
                    onAirCallsListForIn.Add(container);
                    container.AdvertiserPhone = dnQueryResponse.TruePhoneNumber.First();
                    container.CustomerPhone = customerCallerId;
                    container.VirtualNumber = didNumber;
                    container.Reroute = dnQueryResponse.Reroute;
                    container.ContactedAdvertisers.Add(dnQueryResponse.ProviderId);
                    container.RerouteHeadingId = dnQueryResponse.RerouteHeadingId;
                    container.RerouteRegionId = dnQueryResponse.RerouteRegionId;
                    container.RerouteOriginId = dnQueryResponse.RerouteOriginId;
                    onAirCallsListForOut.Add(container);

                    //create vnrequest (maybe async).
                    SaveVnRequest(callId, didNumber, customerCallerId, sourceUrl, true, false, dnQueryResponse.MappingStatus, dnQueryResponse.CallType, dnQueryResponse.TruePhoneNumber.FirstOrDefault(), advCall.callId, dnQueryResponse.IsRecording, dnQueryResponse.ProviderId);
                }
            }
            else
            {
                //handle no mapping in incoming calls.
                SaveVnRequest(callId, didNumber, customerCallerId, sourceUrl, false, false, dnQueryResponse.MappingStatus, dnQueryResponse.CallType, dnQueryResponse.TruePhoneNumber.FirstOrDefault(), null, dnQueryResponse.IsRecording, dnQueryResponse.ProviderId);
                if (dnQueryResponse.TruePhoneNumber.Count > 0)
                {
                    List<CustomParam> cpList = new List<CustomParam>(2);
                    cpList.Add(new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME));
                    cpList.Add(new CustomParam(_FAPI_ISUNMAPPED, true.ToString()));
                    response.Dial(
                        NormalizePhone(dnQueryResponse.TruePhoneNumber.First(), dnQueryResponse.SupplierCountry),
                        dnQueryResponse.DisplayPhoneNumber,
                        returnAddress,
                        false,
                        Trunk,
                        cpList.ToArray()
                        );
                }
                else
                {
                    response = new CallBackResponse();
                    response.Hangup();
                }
            }

            return response;
        }

        private void CreateOnAirCallsManagementDbEntry(string incomingCallId, string outgoingCallId)
        {
            DataAccessLayer.FoneApiOnAirDidCallsManagement dal = new NoProblem.Core.DataAccessLayer.FoneApiOnAirDidCallsManagement();
            dal.InsertCall(incomingCallId, outgoingCallId);
        }

        private void SaveVnRequest(
            string callId,
            string didNumber,
            string customerCallerId,
            string sourceUrl,
            bool doReport,
            bool antiFraudBlocked,
            string mappingStatus,
            string callType,
            string target,
            string outCallId,
            bool recordCall,
            Guid supplierId)
        {
            DataAccessLayer.VnRequest dal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
            DataModel.Xrm.new_vnrequest entity = new NoProblem.Core.DataModel.Xrm.new_vnrequest();
            entity.new_ivrsession = callId;
            entity.new_ivrinbound = didNumber;
            entity.new_ivrcallerid = customerCallerId;
            entity.new_ivrsourceurl = sourceUrl;
            entity.new_doreport = doReport;
            entity.new_antifraudblocked = antiFraudBlocked;
            entity.new_target1 = target;
            entity.new_mappingstatus = mappingStatus;
            entity.new_calltype = callType;
            entity.new_outcallid = outCallId;
            entity.new_recordcall = recordCall;
            if (supplierId != Guid.Empty)
            {
                entity.new_accountid = supplierId;
            }
            dal.Create(entity);
            XrmDataContext.SaveChanges();
        }

        protected override CallBackResponse CallAnswered(FapiArgs args)
        {
            string callId = args[ArgsTypes.FAPI_CALL_ID];
            string incomingCallId = args[_FAPI_INCOMINGCALLID];
            string advertiserAnnounce = args[_FAPI_IVRADVERTISERANNOUNCE];
            int limitCallDuration;
            string limitStr;
            if (args.TryGetValue(_FAPI_LIMITCALLDURATION, out limitStr))
            {
                if (!int.TryParse(limitStr, out limitCallDuration))
                    limitCallDuration = _defaultLimitCallDuration;
                else if (limitCallDuration <= 0)
                    limitCallDuration = _defaultLimitCallDuration;
            }
            else
            {
                limitCallDuration = _defaultLimitCallDuration;
            }
            CallBackResponse response = new CallBackResponse(limitCallDuration);
            if (string.IsNullOrEmpty(advertiserAnnounce))
            {
                if (args[_FAPI_RECORDTHISCALL] == true.ToString())
                    response.RecordStart();
                response.BridgeTo(incomingCallId, returnAddress, new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME));
            }
            else
            {
                string callerAudioFile = AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.INCOMING_CALLS + AudioFilesConsts.SLASH + advertiserAnnounce + AudioFilesConsts.WAV_FILE_EXTENSION;
                response.PlayFile(callerAudioFile, returnAddress,
                    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                    new CustomParam(_FAPI_INCOMINGCALLID, incomingCallId),
                    new CustomParam(_FAPI_SIDE, _ADVERTISER),
                    new CustomParam(_FAPI_RECORDTHISCALL, args[_FAPI_RECORDTHISCALL]));
            }
            return response;
        }

        protected override void Hangup(FapiArgs args)
        {
            string callId = args[ArgsTypes.FAPI_CALL_ID];
            string hangupCause = args[ArgsTypes.FAPI_HANGUP_CAUSE];
            int duration = 0;
            string durationStr;
            if (args.TryGetValue(ArgsTypes.FAPI_CALL_DURATION, out durationStr))
            {
                int.TryParse(durationStr, out duration);
            }
            string isUnmapped;
            if (args.TryGetValue(_FAPI_ISUNMAPPED, out isUnmapped))//is call to moked from unmapped
            {
                CreateVnStatus(callId, hangupCause, duration);
            }
            else//is normal call to mapped advertiser
            {
                bool isIncomingCall = true;
                string incomingCallId;
                if (args.TryGetValue(_FAPI_INCOMINGCALLID, out incomingCallId))
                    isIncomingCall = false;
                if (isIncomingCall)
                {
                    IncomingCallsContainer outContainer;
                    lock (onAirCallsListForOut.SyncRoot)
                    {
                        outContainer = onAirCallsListForOut.FirstOrDefault(x => x.IncomingCallId == callId);
                    }
                    if (outContainer != null)
                    {
                        outContainer.Reroute = false;
                    }
                    IncomingCallsContainer container;
                    lock (onAirCallsListForIn.SyncRoot)
                    {
                        container = onAirCallsListForIn.FirstOrDefault(x => x.IncomingCallId == callId);
                    }
                    if (container != null)
                    {
                        onAirCallsListForIn.Remove(container);
                    }
                    //get outgoing call and hang it up.                    
                    HangupOutgoingCall(callId);
                    UpdateIncomingCallDurationInVnStatus(callId, duration);
                }
                else//is outgoing call
                {
                    IncomingCallsContainer container;
                    lock (onAirCallsListForOut.SyncRoot)
                    {
                        container = onAirCallsListForOut.FirstOrDefault(x => x.OutgoingCallId == callId);
                    }
                    if (container != null)
                    {
                        //do reporting according to hang up causes.
                        int reasonCode;

                        if (duration == 0)
                        {
                            if (container.Reroute)
                            {
                                RerouteCall(container);
                                return;
                            }
                            reasonCode = 1;
                        }
                        else
                        {
                            if (container.IsReroutedCall && duration <= 10 && container.RerouteCurrentSupplierDapazStatus == NoProblem.Core.DataModel.Xrm.account.DapazStatus.PPA)
                            {
                                HangUpCallAfterRerouteDone(callId, container, true);
                                return;
                            }
                            reasonCode = 0;
                        }

                        var vnRequestEntity = CreateVnStatus(incomingCallId, hangupCause, duration);

                        if (vnRequestEntity != null)
                        {
                            if (reasonCode == 0)
                            {
                                vnRequestEntity.new_successfulcall = true;
                            }
                            if (container.IsReroutedCall)
                            {
                                vnRequestEntity.new_originaltarget = vnRequestEntity.new_target1;
                                vnRequestEntity.new_accountoriginalid = vnRequestEntity.new_accountid;
                                vnRequestEntity.new_accountid = container.ContactedAdvertisers.Last();
                                vnRequestEntity.new_target1 = container.AdvertiserPhone;
                                vnRequestEntity.new_usedrerouting = true;
                                vnRequestEntity.new_primaryexpertisererouteid = container.RerouteHeadingId;
                                vnRequestEntity.new_regionrerouteid = container.RerouteRegionId;
                            }
                            DataAccessLayer.VnRequest vnDal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
                            vnDal.Update(vnRequestEntity);
                            XrmDataContext.SaveChanges();
                        }

                        onAirCallsListForOut.Remove(container);
                        FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
                        client.Hangup(incomingCallId);

                        if (duration < SuccessfulCallMinLength)
                            reasonCode = 1;

                        string recordingUrl;
                        args.TryGetValue(ArgsTypes.FAPI_RECORDING_URL, out recordingUrl);

                        string custom;
                        args.TryGetValue(_NPP_CUSTOM, out custom);

                        Phones phones = new Phones(XrmDataContext, null);
                        phones.DirectNumberContactedAsync(
                            container.CustomerPhone,
                            container.VirtualNumber,
                            reasonCode,
                            incomingCallId,
                            duration,
                            incomingCallId,
                            recordingUrl,
                            container.IsReroutedCall,
                            container.ContactedAdvertisers.Last(),
                            container.RerouteHeadingId,
                            container.RerouteOriginId,
                            custom);
                    }
                }
            }
        }

        private void HangupOutgoingCall(string incomingCallId)
        {
            OutgoingCallsHangupThread hangupThread = new OutgoingCallsHangupThread(incomingCallId, ApiUrlBase, FapiKey, FapiSecret);
            hangupThread.Start();
        }

        private void ExecuteRerouteCall(IncomingCallsContainer container)
        {
            Thread tr = new Thread(
                new ParameterizedThreadStart(
                    delegate(object containerObj)
                    {
                        Thread.Sleep(500);
                        IncomingCallsContainer containerLocal = (IncomingCallsContainer)containerObj;
                        try
                        {
                            DataAccessLayer.FoneApiOnAirDidCallsManagement dal = new NoProblem.Core.DataAccessLayer.FoneApiOnAirDidCallsManagement();
                            bool found = dal.Find(containerLocal.IncomingCallId);
                            if (found)
                            {
                                Dialer.ReroutingManager rerouteManager = new NoProblem.Core.BusinessLogic.Dialer.ReroutingManager(null);
                                DataModel.Xrm.account.DapazStatus dStatusOfNext;
                                DataModel.DialerModel.RerouteGetNextSupplierResponse nextSupplierData = rerouteManager.GetNextSupplier(containerLocal, out dStatusOfNext, false);
                                if (nextSupplierData.SupplierId != Guid.Empty)
                                {
                                    List<CustomParam> cpList = new List<CustomParam>(4);
                                    cpList.Add(new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME));
                                    cpList.Add(new CustomParam(_FAPI_INCOMINGCALLID, containerLocal.IncomingCallId));
                                    cpList.Add(new CustomParam(_FAPI_IVRADVERTISERANNOUNCE, nextSupplierData.IvrAdvertiserAnnounce == null ? string.Empty : nextSupplierData.IvrAdvertiserAnnounce));
                                    cpList.Add(new CustomParam(_FAPI_RECORDTHISCALL, nextSupplierData.RecordCall.ToString()));
                                    FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
                                    var advCall = client.Dial(
                                        FoneApiWrapper.Commands.eDialDestinationType.number,
                                        NormalizePhone(nextSupplierData.TargetPhoneNumber, null),//This is only for Israel so I'm letting the system take the default country.
                                        returnAddress,
                                        appId,
                                        returnAddress,
                                        _rerouteDialTimeOut,
                                        nextSupplierData.DisplayPhoneNumber,
                                        null,
                                        null,
                                        Trunk,
                                        containerLocal.IncomingCallId,
                                        cpList.ToArray());
                                    if (advCall.status != 0)
                                    {
                                        HangUpCallAfterRerouteDone(containerLocal.IncomingCallId, containerLocal, true);
                                    }
                                    else
                                    {
                                        dal.UpdateOutgoingCallId(containerLocal.IncomingCallId, advCall.callId);
                                        containerLocal.OutgoingCallId = advCall.callId;
                                        containerLocal.AdvertiserPhone = nextSupplierData.TargetPhoneNumber;
                                        containerLocal.IsReroutedCall = true;
                                        containerLocal.RerouteCurrentSupplierDapazStatus = dStatusOfNext;
                                        //create reroute entry connected to vnrequest
                                        CreateVnRerouteEntry(containerLocal, dStatusOfNext);
                                    }
                                }
                                else
                                {
                                    HangUpCallAfterRerouteDone(containerLocal.IncomingCallId, containerLocal, true);
                                }
                            }
                            else
                            {
                                HangUpCallAfterRerouteDone(containerLocal.IncomingCallId, containerLocal, true);
                            }
                        }
                        catch (Exception exc)
                        {
                            LogUtils.MyHandle.HandleException(exc, "Exception in RerouteCall async method. IncomingCallId = {0}. OutgoingCallId = {1}.", containerLocal.IncomingCallId, containerLocal.OutgoingCallId);
                            HangUpCallAfterRerouteDone(containerLocal.IncomingCallId, containerLocal, true);
                        }
                    }));
            tr.Start(container);
        }

        private void RerouteCall(IncomingCallsContainer container)
        {
            container.RerouteNow = true;
        }

        private void CreateVnRerouteEntry(IncomingCallsContainer container, DataModel.Xrm.account.DapazStatus dStatus)
        {
            DataAccessLayer.VnRequest vnRequestDal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
            var requestEntity = vnRequestDal.GetVnRequestByIvrSession(container.IncomingCallId);
            if (requestEntity != null)
            {
                DataModel.Xrm.new_vnreroute vnReroute = new NoProblem.Core.DataModel.Xrm.new_vnreroute();
                vnReroute.new_accountid = container.ContactedAdvertisers.Last();
                vnReroute.new_dapazstatus = (int)dStatus;
                vnReroute.new_targetphone = container.AdvertiserPhone;
                vnReroute.new_vnrequestid = requestEntity.new_vnrequestid;
                DataAccessLayer.VnReroute dal = new NoProblem.Core.DataAccessLayer.VnReroute(XrmDataContext);
                dal.Create(vnReroute);
                XrmDataContext.SaveChanges();
            }
        }

        private void HangUpCallAfterRerouteDone(string callId, IncomingCallsContainer container, bool hangupTheCall)
        {
            HangUpCallAfterRerouteDone(callId, container, hangupTheCall, false);
        }

        private void HangUpCallAfterRerouteDone(string callId, IncomingCallsContainer container, bool hangupTheCall, bool didntPressOne)
        {
            var vnRequestEntity = CreateVnStatus(container.IncomingCallId, didntPressOne ? DIDNT_PRESS_ONE : NO_POSSIBLE_REROUTE_FOUND, 0);
            if (container.IsReroutedCall)
            {
                vnRequestEntity.new_usedrerouting = true;
                vnRequestEntity.new_primaryexpertisererouteid = container.RerouteHeadingId;
                vnRequestEntity.new_regionrerouteid = container.RerouteRegionId;
                vnRequestEntity.new_originaltarget = vnRequestEntity.new_target1;
                vnRequestEntity.new_accountoriginalid = vnRequestEntity.new_accountid;
                DataAccessLayer.VnRequest dal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
                dal.Update(vnRequestEntity);
                XrmDataContext.SaveChanges();
            }
            if (hangupTheCall)
            {
                FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
                client.Hangup(callId);
            }
            Phones phones = new Phones(null, null);
            phones.DirectNumberContactedAsync(
                container.CustomerPhone,
                container.VirtualNumber,
                1,
                container.IncomingCallId,
                0,
                container.IncomingCallId,
                null,
                /*
                container.IsReroutedCall,
                container.ContactedAdvertisers.Last(),
                container.RerouteHeadingId,
                container.RerouteOriginId
                */
                false, null, null, null, null);
        }

        private void UpdateIncomingCallDurationInVnStatus(string callId, int duration)
        {
            TimerContainer container = new TimerContainer();
            container.CallId = callId;
            container.Duration = duration;
            System.Threading.Timer timer = new System.Threading.Timer(UpdateIncomingCallDurationInVnStatusTimerCallBack, container, 7500, System.Threading.Timeout.Infinite);
            container.Timer = timer;
            timersHolder.Add(container);
        }

        private class TimerContainer
        {
            public System.Threading.Timer Timer { get; set; }
            public int Duration { get; set; }
            public string CallId { get; set; }
        }

        private static void UpdateIncomingCallDurationInVnStatusTimerCallBack(object obj)
        {
            try
            {
                TimerContainer container = (TimerContainer)obj;
                IncomingCallDurationUpdater updater = new IncomingCallDurationUpdater(container.CallId, container.Duration);
                updater.Start();
                timersHolder.Remove(container);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateIncomingCallDurationInVnStatusTimerCallBack");
            }
        }

        private DataModel.Xrm.new_vnrequest CreateVnStatus(string callId, string hangupCause, int duration)
        {
            DataAccessLayer.VnRequest vnRequestDal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
            var requestEntity = vnRequestDal.GetVnRequestByIvrSession(callId);
            if (requestEntity == null)
            {
                LogUtils.MyHandle.WriteToLog(string.Format(_LOG_MESSAGE_FOR_VN_NOT_FOUND, callId, hangupCause));
            }
            else
            {
                DataAccessLayer.VnStatus vnStatusDal = new NoProblem.Core.DataAccessLayer.VnStatus(XrmDataContext);
                DataModel.Xrm.new_vnstatus statusEntity = new NoProblem.Core.DataModel.Xrm.new_vnstatus();
                statusEntity.new_vnrequestid = requestEntity.new_vnrequestid;
                statusEntity.new_ivrsession = callId;
                statusEntity.new_ivrstatus = hangupCause;
                statusEntity.new_advertisercallduration = duration;
                vnStatusDal.Create(statusEntity);
                XrmDataContext.SaveChanges();
            }
            return requestEntity;
        }

        protected override CallBackResponse PlayFileComplete(FapiArgs args)
        {
            string callId = args[ArgsTypes.FAPI_CALL_ID];
            string incomingCallId = args[_FAPI_INCOMINGCALLID];
            string side = args[_FAPI_SIDE];
            CallBackResponse response = new CallBackResponse();
            if (side == _ADVERTISER)
            {
                if (args[_FAPI_RECORDTHISCALL] == true.ToString())
                    response.RecordStart();
                response.BridgeTo(incomingCallId, returnAddress, new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME));
            }
            else
            {
                IncomingCallsContainer container;
                lock (onAirCallsListForIn.SyncRoot)
                {
                    container = onAirCallsListForIn.FirstOrDefault(x => x.IncomingCallId == callId);
                }
                if (container != null && container.RerouteNow)
                {
                    Dialer.ReroutingManager rerouteManager = new NoProblem.Core.BusinessLogic.Dialer.ReroutingManager(null);
                    DataModel.Xrm.account.DapazStatus dStatusOfNext;
                    DataModel.DialerModel.RerouteGetNextSupplierResponse nextSupplierData = rerouteManager.GetNextSupplier(container, out dStatusOfNext, true);
                    if (nextSupplierData.SupplierId != Guid.Empty)
                    {
                        response.PlayAndGetDtmf(
                            AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.INCOMING_CALLS + "/wanttoreroute2.wav",
                            returnAddress,
                            1,
                            TERMINATION_KEY_POUND,
                            _defaultDtmfTimeout,
                            new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                            new CustomParam(_FAPI_INCOMINGCALLID, callId)
                            );
                    }
                    else
                    {
                        HangUpCallAfterRerouteDone(container.IncomingCallId, container, false);
                        response.Hangup();
                    }
                    //string callerAudioFile = AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.INCOMING_CALLS + "/reroutingyou.wav";
                    //response.PlayFile(callerAudioFile, returnAddress,
                    //    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                    //    new CustomParam(_FAPI_INCOMINGCALLID, callId),
                    //    new CustomParam(_FAPI_SIDE, _CUSTOMER),
                    //    new CustomParam(_FAPI_REROUTING, true.ToString()));
                    container.RerouteNow = false;
                }
                else
                {
                    string callerAudioFile = AudioFilesUrlBase + AudioFilesConsts.FAKE_RING;
                    response.PlayFile(callerAudioFile, returnAddress,
                        new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                        new CustomParam(_FAPI_INCOMINGCALLID, callId),
                        new CustomParam(_FAPI_SIDE, _CUSTOMER));
                    //string s;
                    //if (args.TryGetValue(_FAPI_REROUTING, out s))
                    //{
                    //    ExecuteRerouteCall(container);
                    //}
                }
            }
            return response;
        }

        protected override CallBackResponse GetDtmfComplete(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            string callId = args[ArgsTypes.FAPI_CALL_ID];
            IncomingCallsContainer container;
            lock (onAirCallsListForIn.SyncRoot)
            {
                container = onAirCallsListForIn.FirstOrDefault(x => x.IncomingCallId == callId);
            }
            if (container != null)
            {
                string digits;
                if (args.TryGetValue(ArgsTypes.FAPI_DIGITS, out digits))
                {
                    ExecuteRerouteCall(container);
                    string callerAudioFile = AudioFilesUrlBase + AudioFilesConsts.FAKE_RING;
                    response.PlayFile(callerAudioFile, returnAddress,
                        new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                        new CustomParam(_FAPI_INCOMINGCALLID, callId),
                        new CustomParam(_FAPI_SIDE, _CUSTOMER));
                }
                else
                {
                    HangUpCallAfterRerouteDone(container.IncomingCallId, container, false, true);
                    response.Hangup();
                }
            }
            else
            {
                HangUpCallAfterRerouteDone(container.IncomingCallId, container, false, true);
                response.Hangup();
            }
            return response;
        }
    }
}
