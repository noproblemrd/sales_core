﻿using System;
using Effect.Crm.Logs;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.IncomingCalls
{
    class OutgoingCallsHangupThread : Runnable
    {
        public OutgoingCallsHangupThread(string incomingCallId, string apiUrlBase, string fapiKey, string fapiSecret)
            : base(System.Threading.ThreadPriority.Highest)
        {
            this.incomingCallId = incomingCallId;
            this.apiUrlBase = apiUrlBase;
            this.fapiKey = fapiKey;
            this.fapiSecret = fapiSecret;
        }

        private string incomingCallId;
        private string apiUrlBase;
        private string fapiKey;
        private string fapiSecret;
        private static DataAccessLayer.FoneApiOnAirDidCallsManagement dal = new NoProblem.Core.DataAccessLayer.FoneApiOnAirDidCallsManagement();

        protected override void Run()
        {
            try
            {
                string outgoingCallId = FindOutgoingCallId(incomingCallId);
                if (outgoingCallId != null)
                {
                    FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(apiUrlBase, fapiKey, fapiSecret);
                    client.Hangup(outgoingCallId);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.IncomingCalls.OutgoingCallsHangupThread.Run");
            }
        }

        private string FindOutgoingCallId(string incomingCallId)
        {
            string outgoingCallId = null;
            for (int i = 0; i < 3; i++)
            {
                outgoingCallId = dal.FindAndDelete(incomingCallId);
                if (outgoingCallId != null)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
            return outgoingCallId;
        }
    }
}
