﻿using NoProblem.Core.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.ClipCall
{
    public class ClipCallTracing
    {
        private const string _FAPI_INCIDENT_ACCOUNT_ID = "npp3";

        private Guid incidentAccountId;
        private NoProblem.Core.DataModel.Xrm.new_incidentaccount.CallTracing callTracing;
        FapiArgs args;
        public ClipCallTracing(Guid incidentAccountId, NoProblem.Core.DataModel.Xrm.new_incidentaccount.CallTracing callTracing)
        {
            this.incidentAccountId = incidentAccountId;
            this.callTracing = callTracing;
        }
        public ClipCallTracing(FapiArgs args, NoProblem.Core.DataModel.Xrm.new_incidentaccount.CallTracing callTracing)
        {
            this.args = args;
            this.callTracing = callTracing;
        }
        public void ExecuteAsync()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate(object state)
            {
                ClipCallTracing _this = (ClipCallTracing)state;
                _this.Exec();
            }), this);
        }
        public void Exec()
        {
            if(incidentAccountId == Guid.Empty)
            {
                if (args == null || !args.ContainsKey(_FAPI_INCIDENT_ACCOUNT_ID))
                    return;
                incidentAccountId = new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]);
            }
            IncidentAccount ia = new IncidentAccount(null);
            ia.SetCallTracing(incidentAccountId, callTracing);
        }
    }
}
