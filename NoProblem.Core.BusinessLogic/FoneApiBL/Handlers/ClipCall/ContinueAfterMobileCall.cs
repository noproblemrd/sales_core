﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.ClipCall
{
    internal class ContinueAfterMobileCall : Runnable
    {
        public ContinueAfterMobileCall(Guid incidentId, Guid incidentAccountId, eEndStatus endStatus, string duration, string recording)
        {
            this.incidentId = incidentId;
            this.incidentAccountId = incidentAccountId;
            this.endStatus = endStatus;
            this.duration = duration;
            this.recording = recording;
            int.TryParse(duration, out durationInt);
        }

        Guid incidentId;
        Guid incidentAccountId;
        eEndStatus endStatus;
        string duration;
        string recording;
        int durationInt;
        protected override void Run()
        {
            ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(XrmDataContext);
            DataAccessLayer.IncidentAccount incAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            var incAcc = incAccDal.Retrieve(incidentAccountId);
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentId);
        /*
            if (!string.IsNullOrEmpty(recording))
            {
                incAccDal.UpdateRecordFileLocation(incidentAccountId, recording, durationInt);
            }
         * */
            incAccDal.UpdateRecordFileLocation(incidentAccountId, recording, durationInt, endStatus.ToString());
            incAccDal.SetMobileCallProcess(incidentAccountId, DataModel.Xrm.new_incidentaccount.CallProcess.CallDone);
            NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager srm = new ServiceRequest.ServiceRequestManager(XrmDataContext);
            srm.ExecuteNextMobileCall(incident, incidentDal);
            
            DataAccessLayer.AccountRepository accDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var acc = accDal.Retrieve(incAcc.new_accountid.Value);
            if (!acc.new_havevideo.HasValue || !acc.new_havevideo.Value)
            {
                AarTextMessage.AarTextMessageHandler atm = new AarTextMessage.AarTextMessageHandler();
                atm.SendAfterCall(incAcc.new_accountid.Value, incAcc.new_incidentid.Value, durationInt > 1);
            }
            
        }
        internal enum eEndStatus
        {
            Rejected,
            AdvertiserCallFailed,
            AdvertiserDidntPressDtmf,
            AdvertiserDidntAnswer,
            AdvertiserGeneralHungUp,
            CustomerCallFailed,
            CustomerDidntAnswer,
            CustomerGeneralHungUp,
            CallOk
        }
    }
}
