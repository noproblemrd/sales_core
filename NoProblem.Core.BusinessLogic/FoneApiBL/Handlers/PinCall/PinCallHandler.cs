﻿using FoneApiWrapper;
using FoneApiWrapper.Commands;
using NoProblem.Core.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.PinCall
{
    internal class PinCallHandler : FoneApiHandlerBase
    {
        private const string _HANDLERNAME = FoneApiRouter.HandlersNames.PIN_CALL_HANDLER;
        private const string PIN_KEY = "npp1";

        public bool StartCall(string phone, string pin)
        {
            FoneApiClient client = new FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
            var call = client.Dial(FoneApiWrapper.Commands.eDialDestinationType.number,
                  NormalizePhone(phone, GlobalConfigurations.DefaultCountry),
                  returnAddress,
                  appId,
                  returnAddress,
                  _defaultDialTimeOut,
                  Cid,
                  null,
                  null,
                  Trunk,
                  null,
                  new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(PIN_KEY, pin)
            );
            return call.status == 0;
        }

        protected override FoneApiWrapper.CallBacks.CallBackResponse CallAnswered(FapiArgs args)
        {
            string pin = args[PIN_KEY];
            FoneApiWrapper.CallBacks.CallBackResponse response =
                new FoneApiWrapper.CallBacks.CallBackResponse();
            for (int i = 0; i < 3; i++)
            {
            //    response.Say("Your pin code is", FoneApiWrapper.CallBacks.Actions.eVoice.female, FoneApiWrapper.CallBacks.Actions.eLanguage.en);
                response.PlayFile(
                        AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.MOBILE + AudioFilesConsts.SLASH + "YourPinCode.wav",
                        null);
                SayPin(pin, response);
            }
            response.Hangup();
            return response;
        }

        private void SayPin(string pin, FoneApiWrapper.CallBacks.CallBackResponse response)
        {
            foreach (char c in pin)
            {
                response.PlayFile(
                        AudioFilesUrlBase + AudioFilesConsts.GetAarFolder + "numberf" + AudioFilesConsts.SLASH + c + "n" + AudioFilesConsts.WAV_FILE_EXTENSION,
                        //AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHIELA + AudioFilesConsts.SLASH + AudioFilesConsts.NATURAL_NUMBERS_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + c.ToString() + "n" + AudioFilesConsts.WAV_FILE_EXTENSION,
                        null);
            }
        }
    }
}
