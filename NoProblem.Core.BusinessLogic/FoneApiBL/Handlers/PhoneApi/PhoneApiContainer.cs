﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.PhoneApi
{
    internal class PhoneApiContainer
    {
        public PhoneApiContainer()
        {
            voiceMail = false;
        }

        public string CustomerCallId { get; set; }

        private volatile string advertiserCallId;
        public string AdvertiserCallId
        {
            get
            {
                return advertiserCallId;
            }
            set
            {
                advertiserCallId = value;
            }
        }

        private volatile bool voiceMail;
        public bool VoiceMail
        {
            get
            {
                return voiceMail;
            }
            set
            {
                voiceMail = value;
            }
        }
    }
}
