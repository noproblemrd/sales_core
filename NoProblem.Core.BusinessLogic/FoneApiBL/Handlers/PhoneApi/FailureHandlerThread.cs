﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.PhoneApi
{
    internal class FailureHandlerThread : Runnable
    {
        public FailureHandlerThread(Guid incidentId, Guid incidentAccountId, string reason)
            : this(incidentId, incidentAccountId, reason, false)
        {

        }

        public FailureHandlerThread(Guid incidentId, Guid incidentAccountId, string reason, bool isCustomersFault)
        {
            this.incidentId = incidentId;
            this.incidentAccountId = incidentAccountId;
            this.reason = reason;
            this.isCustomersFault = isCustomersFault;
        }

        private Guid incidentId;
        private Guid incidentAccountId;
        private string reason;
        private bool isCustomersFault;

        protected override void Run()
        {
            try
            {
                ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(XrmDataContext);
                DataAccessLayer.IncidentAccount incAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                var incAcc = incAccDal.Retrieve(incidentAccountId);
                incAcc.new_rejectedreason = reason;
                incAcc.new_lostoncustomersfault = isCustomersFault;
                manager.MarkIncidentAccountAsLostAfterLeadBuyerFailed(incAccDal, incAcc);
                manager.ContinueRequestAfterBrokerCall(false, incidentId, false);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in PhoneApi.FailureHandlerThread.Run");
            }
        }
    }
}
