﻿using System;
using System.Collections.Generic;
using Effect.Crm.Logs;
using FoneApiWrapper;
using FoneApiWrapper.CallBacks;
using NoProblem.Core.BusinessLogic.LeadBuyers;
using NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.PhoneConnection;
using NoProblem.Core.DataModel;
using NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.PhoneApi;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers
{
    internal class PhoneApiHandler : FoneApiHandlerBase
    {
        private const string _HANDLERNAME = FoneApiBL.FoneApiRouter.HandlersNames.PHONE_API_HANDLER;// "PhoneApiHandler";
        private const string _FAPI_CALL_PARAMS = "npp0";
        private const string _FAPI_INCIDENT_ID = "npp1";
        private const string _FAPI_INCIDENT_ACCOUNT_ID = "npp2";
        private const string _FAPI_ADVERTISER_PH0NE_NORMALIZED = "npp3";
        private const string _FAPI_CUSTOMER_PHONE = "npp4";
        private const string _FAPI_HEADING_CODE = "npp5";
        private const string _FAPI_DISABLE_PRESS_ONE = "npp6";
        private const string _FAPI_DTMF_PROMPT_INDEX = "npp7";
        private const string _FAPI_CUSTOMER_CALL_ID = "npp8";
        private const string _FAPI_LIMITCALLDURATION = "npp9";
        private const string _FAPI_MINIMUMCALLDURATION = "npp10";
        private const string _FAPI_PROMPTINDEX = "npp11";

        private static Dictionary<string, PhoneApi.PhoneApiContainer> containersHolder = new Dictionary<string, NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.PhoneApi.PhoneApiContainer>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="incident"></param>
        /// <param name="incAcc"></param>
        /// <param name="accExp"></param>
        /// <param name="advertiserPhone"></param>
        /// <param name="customerPhone"></param>
        /// <param name="limitCallDuration">The limit of the call duration in seconds</param>
        /// <param name="minimumCallDuration">Billable call duration in seconds</param>
        /// <returns></returns>
        internal SendLeadResponse StartCall(
           DataModel.Xrm.incident incident,
           DataModel.Xrm.new_incidentaccount incAcc,
           DataModel.Xrm.new_accountexpertise accExp,
           string advertiserPhone, string advertiserPhoneCountry, string customerPhone,
           int? limitCallDuration,
           int? minimumCallDuration
           )
        {
            var heading = accExp.new_new_primaryexpertise_new_accountexpertise;
            bool disablePressOne = false;
            disablePressOne = (heading.new_disablepressoneinphoneapi.HasValue && heading.new_disablepressoneinphoneapi.Value);
            int limitCallDurationlocal;
            if (limitCallDuration.HasValue && limitCallDuration.Value > 0)
            {
                limitCallDurationlocal = limitCallDuration.Value;
            }
            else
            {
                limitCallDurationlocal = _defaultLimitCallDuration;
            }
            int minimumCallDurationLocal = minimumCallDuration.HasValue ? minimumCallDuration.Value : -1;
            SendLeadResponse retVal = new SendLeadResponse();
            FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
            string customerPhoneNormalized = NormalizePhone(customerPhone, incident.new_country);
            var callSid = client.Dial(FoneApiWrapper.Commands.eDialDestinationType.number,
                  customerPhoneNormalized,
                  returnAddress,
                  appId,
                  returnAddress,
                  _defaultDialTimeOut,
                  MakeUpCallerIdForCustomer(customerPhoneNormalized),
                  null,
                  null,
                  Trunk,
                  null,
                  new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_FAPI_SIDE, _CUSTOMER),
                  new CustomParam(_FAPI_INCIDENT_ID, incident.incidentid.ToString()),
                  new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, incAcc.new_incidentaccountid.ToString()),
                  new CustomParam(_FAPI_ADVERTISER_PH0NE_NORMALIZED, NormalizePhone(advertiserPhone, advertiserPhoneCountry)),
                  new CustomParam(_FAPI_HEADING_CODE, accExp.new_new_primaryexpertise_new_accountexpertise.new_code),
                  new CustomParam(_FAPI_CUSTOMER_PHONE, customerPhone),
                  new CustomParam(_FAPI_DISABLE_PRESS_ONE, disablePressOne.ToString()),
                  new CustomParam(_FAPI_LIMITCALLDURATION, limitCallDurationlocal.ToString()),
                  new CustomParam(_FAPI_MINIMUMCALLDURATION, minimumCallDurationLocal.ToString())
            );

            if (callSid.status == 0)
            {
                retVal.IsInCall = true;
                incAcc.new_dialercallid = callSid.callId;
                DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                dal.Update(incAcc);
                XrmDataContext.SaveChanges();
                PhoneApi.PhoneApiContainer container = new NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.PhoneApi.PhoneApiContainer();
                container.CustomerCallId = callSid.callId;
                containersHolder.Add(callSid.callId, container);
            }
            else
            {
                retVal.Reason = "Failed to initiate call";
            }
            return retVal;
        }

        protected override CallBackResponse CallAnswered(FapiArgs args)
        {
            string side = args[_FAPI_SIDE];
            if (side == _CUSTOMER)
            {
                return CallAnsweredCustomer(args);
            }
            else
            {
                return CallAnsweredAdvetiser(args);
            }
        }

        private CallBackResponse CallAnsweredCustomer(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            string headingCode = args[_FAPI_HEADING_CODE];
            bool disablePressOne = true.ToString() == args[_FAPI_DISABLE_PRESS_ONE];
            if (disablePressOne)
            {
                //response.PlayFile(AudioFilesUrlBase + "/brokercall/brokercallpressone-a.wav", string.Empty);
                //response.PlayFile(AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHARON + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.WAV_FILE_EXTENSION, string.Empty);
                //response.PlayFile(AudioFilesUrlBase + "/brokercall/phone_API_nopressone_Part02.wav", string.Empty);
                ConnectCustomerToAdvertiser(response, args, true);
            }
            else
            {
                response.PlayAndGetDtmf(AudioFilesUrlBase + "/brokercall/flavor_a/PROMPT_A-01.wav", returnAddress, 1, "#", 0,
                     new CustomParam(_FAPI_DTMF_PROMPT_INDEX, 0.ToString()),
                     new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                     new CustomParam(_FAPI_HEADING_CODE, args[_FAPI_HEADING_CODE]),
                     new CustomParam(_FAPI_INCIDENT_ID, args[_FAPI_INCIDENT_ID]),
                     new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, args[_FAPI_INCIDENT_ACCOUNT_ID]),
                     new CustomParam(_FAPI_ADVERTISER_PH0NE_NORMALIZED, args[_FAPI_ADVERTISER_PH0NE_NORMALIZED]),
                     new CustomParam(_FAPI_CUSTOMER_PHONE, args[_FAPI_CUSTOMER_PHONE]),
                     new CustomParam(_FAPI_LIMITCALLDURATION, args[_FAPI_LIMITCALLDURATION]),
                     new CustomParam(_FAPI_MINIMUMCALLDURATION, args[_FAPI_MINIMUMCALLDURATION])
                );
            }
            return response;
        }

        private CallBackResponse CallAnsweredAdvetiser(FapiArgs args)
        {
            int limitCallDuration;
            if (!int.TryParse(args[_FAPI_LIMITCALLDURATION], out limitCallDuration))
                limitCallDuration = _defaultLimitCallDuration;
            else if (limitCallDuration <= 0)
                limitCallDuration = _defaultLimitCallDuration;
            CallBackResponse response = new CallBackResponse(limitCallDuration);
            response.RecordStart();
            response.BridgeTo(args[_FAPI_CUSTOMER_CALL_ID], string.Empty);
            return response;
        }

        protected override CallBackResponse GetDtmfComplete(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            string digits;
            if (args.TryGetValue(ArgsTypes.FAPI_DIGITS, out digits))//something was pressed
            {
                ConnectCustomerToAdvertiser(response, args);
                SaveDTMFPressed(digits, args[_FAPI_INCIDENT_ACCOUNT_ID]);
            }
            else
            {
                //play next file...
                string nextFile = null;
                int promptIndex = int.Parse(args[_FAPI_DTMF_PROMPT_INDEX]);
                switch (promptIndex)
                {
                    case 0:
                        {
                            string headingCode = args[_FAPI_HEADING_CODE];
                            nextFile = AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHARON + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.WAV_FILE_EXTENSION;
                        }
                        break;
                    case 1:
                        {
                            nextFile = AudioFilesUrlBase + "/brokercall/flavor_a/PROMPT_A-02.wav";
                        }
                        break;
                    case 2:
                        {
                            nextFile = AudioFilesUrlBase + "/brokercall/flavor_a/PROMPT_A-01.wav";
                        }
                        break;
                    case 3:
                        {
                            string headingCode = args[_FAPI_HEADING_CODE];
                            nextFile = AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHARON + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.WAV_FILE_EXTENSION;
                        }
                        break;
                    case 4:
                        {
                            nextFile = AudioFilesUrlBase + "/brokercall/flavor_a/PROMPT_A-02.wav";
                        }
                        break;
                    case 5:
                        {
                            nextFile = AudioFilesUrlBase + "/brokercall/flavor_a/PROMPT_A-01.wav";
                        }
                        break;
                    case 6:
                        {
                            string headingCode = args[_FAPI_HEADING_CODE];
                            nextFile = AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHARON + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.WAV_FILE_EXTENSION;
                        }
                        break;
                    case 7:
                        {
                            nextFile = AudioFilesUrlBase + "/brokercall/flavor_a/PROMPT_A-02.wav";
                        }
                        break;
                }
                if (nextFile != null)
                {
                    response.PlayAndGetDtmf(nextFile, returnAddress, 1, "#", promptIndex == 7 ? 1 : 0,
                     new CustomParam(_FAPI_DTMF_PROMPT_INDEX, (++promptIndex).ToString()),
                     new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                     new CustomParam(_FAPI_HEADING_CODE, args[_FAPI_HEADING_CODE]),
                     new CustomParam(_FAPI_INCIDENT_ID, args[_FAPI_INCIDENT_ID]),
                     new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, args[_FAPI_INCIDENT_ACCOUNT_ID]),
                     new CustomParam(_FAPI_ADVERTISER_PH0NE_NORMALIZED, args[_FAPI_ADVERTISER_PH0NE_NORMALIZED]),
                     new CustomParam(_FAPI_CUSTOMER_PHONE, args[_FAPI_CUSTOMER_PHONE]),
                     new CustomParam(_FAPI_LIMITCALLDURATION, args[_FAPI_LIMITCALLDURATION]),
                     new CustomParam(_FAPI_MINIMUMCALLDURATION, args[_FAPI_MINIMUMCALLDURATION])
                    );
                }
                else//if out of files then failed to press one...
                {
                    PhoneApi.PhoneApiContainer container = containersHolder[args[ArgsTypes.FAPI_CALL_ID]];
                    container.VoiceMail = true;
                    response.Hangup();
                    //Guid incidentId = new Guid(args[_FAPI_INCIDENT_ID]);
                    //Guid incidentAccountId = new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]);
                    ////string reason = "Customer didn't press one";
                    //string reason = "Probable voicemail";
                    //FailureHandlerThread failureThread = new FailureHandlerThread(incidentId, incidentAccountId, reason);
                    //failureThread.Start();
                }
            }
            return response;
        }

        private void SaveDTMFPressed(string digits, string incidentAccountId)
        {
            DTMFSaver saver = new DTMFSaver(digits, incidentAccountId);
            saver.Start();
        }

        private void ConnectCustomerToAdvertiser(CallBackResponse response, FapiArgs args)
        {
            ConnectCustomerToAdvertiser(response, args, false);
        }

        private void ConnectCustomerToAdvertiser(CallBackResponse response, FapiArgs args, bool disablePressOne)
        {
            ExecuteCallToAdvertiser(args);
            if (!disablePressOne)
            {
                response.PlayFile(AudioFilesUrlBase + "/brokercall/flavor_a/PROMPT_A-03.wav",
                    returnAddress,
                    new CustomParam(_FAPI_SIDE, _CUSTOMER),
                    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME));
            }
            else
            {
                response.PlayFile(
                    AudioFilesUrlBase + "/brokercall/flavor_c/PROMPT_C-01.wav",
                    returnAddress,
                    new CustomParam(_FAPI_SIDE, _CUSTOMER),
                    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                    new CustomParam(_FAPI_PROMPTINDEX, 1.ToString()),
                    new CustomParam(_FAPI_HEADING_CODE, args[_FAPI_HEADING_CODE])
                    );
            }
        }

        private void ExecuteCallToAdvertiser(FapiArgs args)
        {
            //execute call async
            AdvertiserCallExecuterThread tr = new AdvertiserCallExecuterThread(args, args[_FAPI_ADVERTISER_PH0NE_NORMALIZED], ApiUrlBase, FapiKey, FapiSecret, args[_FAPI_CUSTOMER_PHONE], Trunk);
            tr.Start();
        }

        protected override void Hangup(FapiArgs args)
        {
            if (args[_FAPI_SIDE] == _CUSTOMER)
            {
                HangupCustomerCall(args);
            }
            else
            {
                HangupAdvertiserCall(args);
            }
        }

        private void HangupAdvertiserCall(FapiArgs args)
        {
            Guid incidentId = new Guid(args[_FAPI_INCIDENT_ID]);
            Guid incAccId = new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]);
            string callDuration = args[ArgsTypes.FAPI_CALL_DURATION];
            string hangupStatus = args[ArgsTypes.FAPI_HANGUP_CAUSE];
            CreateCDR(hangupStatus, args[ArgsTypes.FAPI_CALL_ID], callDuration, args[_FAPI_ADVERTISER_PH0NE_NORMALIZED], "phone api advertiser Call", NoProblem.Core.DataModel.Xrm.new_calldetailrecord.Type.jona_advertiser, incAccId);

            if (hangupStatus == FoneApiWrapper.CallBacks.HangupCauses.NO_ANSWER ||
                hangupStatus == FoneApiWrapper.CallBacks.HangupCauses.NO_USER_RESPONSE ||
                hangupStatus == FoneApiWrapper.CallBacks.HangupCauses.USER_BUSY)
            {
                FailureHandlerThread failureThread = new FailureHandlerThread(incidentId, incAccId, "advertiser did not answer");
                failureThread.Start();
            }
            else
            {
                DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                var incAcc = incidentAccountDal.Retrieve(incAccId);
                int duration;
                int.TryParse(callDuration, out duration);
                string recording;
                if (args.TryGetValue(ArgsTypes.FAPI_RECORDING_URL, out recording))
                {
                    incAcc.new_recordfilelocation = recording;
                    incAcc.new_recordduration = duration;
                    incidentAccountDal.Update(incAcc);
                    XrmDataContext.SaveChanges();
                }
                int minSecs;
                int.TryParse(args[_FAPI_MINIMUMCALLDURATION], out minSecs);
                if (duration < minSecs)
                {
                    FailureHandlerThread failureThread = new FailureHandlerThread(incidentId, incAccId,
                        string.Format("Call to advertiser was too short. call duration = {0}. minimun required duration = {1}", duration, minSecs));
                    failureThread.Start();
                }
                else
                {
                    //success
                    SuccessHandlerThread successThread = new SuccessHandlerThread(incidentId, incAcc, incAcc.new_account_new_incidentaccount);
                    successThread.Start();
                }
            }
        }

        private void HangupCustomerCall(FapiArgs args)
        {
            string customerCallId = args[ArgsTypes.FAPI_CALL_ID];
            string duration = args[ArgsTypes.FAPI_CALL_DURATION];
            string hangupStatus = args[ArgsTypes.FAPI_HANGUP_CAUSE];
            CreateCDR(hangupStatus, customerCallId, duration, args[_FAPI_CUSTOMER_PHONE], "phone api customer side", NoProblem.Core.DataModel.Xrm.new_calldetailrecord.Type.jona_consumer, new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]));
            PhoneApi.PhoneApiContainer container = null;
            container = containersHolder[customerCallId];
            string advertiserCallId = container.AdvertiserCallId;
            if (!string.IsNullOrEmpty(advertiserCallId))
            {
                FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
                client.Hangup(advertiserCallId);
            }
            else if (duration == 0.ToString())
            {

                CloseCallFromCustomerSide(args, "Customer did not answer");
            }
            else
            {
                Thread.Sleep(500);
                CloseCallFromCustomerSide(args, !container.VoiceMail ? "Customer didn't press one" : "Probable voicemail");
            }
            containersHolder.Remove(customerCallId);
        }

        /// <summary>
        /// To be called when customer did not answer.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="pressedOne"></param>
        private void CloseCallFromCustomerSide(FapiArgs args, string reason)
        {
            Guid incidentId = new Guid(args[_FAPI_INCIDENT_ID]);
            Guid incidentAccountId = new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]);
            FailureHandlerThread failureThread = new FailureHandlerThread(incidentId, incidentAccountId, reason, true);
            failureThread.Start();
            string to = args[_FAPI_CUSTOMER_PHONE];
            PhoneConnectionSmsSender smsSender = new PhoneConnectionSmsSender();
            smsSender.Send(incidentAccountId, incidentId, to);
        }

        protected override CallBackResponse PlayFileComplete(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            if (args[_FAPI_SIDE] == _CUSTOMER)
            {
                string indexStrValue;
                if (args.TryGetValue(_FAPI_PROMPTINDEX, out indexStrValue))
                {
                    int index = int.Parse(indexStrValue);
                    if (index == 9)
                    {
                        response.Hangup();
                    }
                    else
                    {
                        string file;
                        int modulu = index % 3;
                        if (modulu == 0)
                        {
                            file = AudioFilesUrlBase + "/brokercall/flavor_c/PROMPT_C-01.wav";
                        }
                        else if (modulu == 1)
                        {
                            string headingCode = args[_FAPI_HEADING_CODE];
                            file = AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHARON + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.WAV_FILE_EXTENSION;
                        }
                        else
                        {
                            file = AudioFilesUrlBase + "/brokercall/flavor_c/PROMPT_C-02music.wav";
                        }
                        response.PlayFile(file,
                            returnAddress,
                            new CustomParam(_FAPI_SIDE, _CUSTOMER),
                            new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                            new CustomParam(_FAPI_PROMPTINDEX, (++index).ToString()),
                            new CustomParam(_FAPI_HEADING_CODE, args[_FAPI_HEADING_CODE])
                            );
                    }
                }
                else
                {
                    response.PlayFile(AudioFilesUrlBase + "/brokercall/flavor_a/PROMPT_A-03.wav",
                        returnAddress,
                        new CustomParam(_FAPI_SIDE, _CUSTOMER),
                        new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME));
                }
            }
            else
            {
                response.Hangup();
            }
            return response;
        }

        #region Advertiser Call Thread Classs

        private class AdvertiserCallExecuterThread : Runnable
        {
            public AdvertiserCallExecuterThread(FapiArgs args, string advertiserPhoneNormailzed, string apiUrlBase, string fapiKey, string fapiSecret, string outgoingPhoneNumber, string trunk)
                : base(System.Threading.ThreadPriority.Highest)
            {
                this.args = args;
                this.advertiserPhoneNormailzed = advertiserPhoneNormailzed;
                this.apiUrlBase = apiUrlBase;
                this.fapiKey = fapiKey;
                this.fapiSecret = fapiSecret;
                this.outgoingPhoneNumber = outgoingPhoneNumber;
                this.trunk = trunk;
            }

            private FapiArgs args;
            private string advertiserPhoneNormailzed;
            private string apiUrlBase;
            private string fapiKey;
            private string fapiSecret;
            private string outgoingPhoneNumber;
            private string trunk;

            protected override void Run()
            {
                try
                {
                    FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(apiUrlBase, fapiKey, fapiSecret);
                    var callSid = client.Dial(FoneApiWrapper.Commands.eDialDestinationType.number,
                          advertiserPhoneNormailzed,
                          returnAddress,
                          appId,
                          returnAddress,
                          _defaultDialTimeOut,
                          outgoingPhoneNumber,
                          null,
                          null,
                          trunk,
                          args[ArgsTypes.FAPI_CALL_ID],
                          new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                          new CustomParam(_FAPI_SIDE, _ADVERTISER),
                          new CustomParam(_FAPI_INCIDENT_ID, args[_FAPI_INCIDENT_ID]),
                          new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, args[_FAPI_INCIDENT_ACCOUNT_ID]),
                          new CustomParam(_FAPI_ADVERTISER_PH0NE_NORMALIZED, args[_FAPI_ADVERTISER_PH0NE_NORMALIZED]),
                          new CustomParam(_FAPI_CUSTOMER_PHONE, args[_FAPI_CUSTOMER_PHONE]),
                          new CustomParam(_FAPI_CUSTOMER_CALL_ID, args[ArgsTypes.FAPI_CALL_ID]),
                          new CustomParam(_FAPI_LIMITCALLDURATION, args[_FAPI_LIMITCALLDURATION]),
                          new CustomParam(_FAPI_MINIMUMCALLDURATION, args[_FAPI_MINIMUMCALLDURATION])
                    );
                    if (callSid.status == 0)
                    {
                        PhoneApi.PhoneApiContainer container = null;
                        container = containersHolder[args[ArgsTypes.FAPI_CALL_ID]];
                        container.AdvertiserCallId = callSid.callId;
                    }
                    else
                    {
                        Guid incidentId = new Guid(args[_FAPI_INCIDENT_ID]);
                        Guid incidentAccountId = new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]);
                        string reason = "Failed to execute call to advertiser";
                        FailureHandlerThread failureThread = new FailureHandlerThread(incidentId, incidentAccountId, reason);
                        failureThread.Start();
                        client.Hangup(args[ArgsTypes.FAPI_CALL_ID]);
                        LogUtils.MyHandle.WriteToLog("Failed to execute call to advertiser in phone api. status = {0}. errorMsg = {1}", callSid.status, callSid.errorMsg);
                    }
                }
                catch (Exception exc)
                {
                    FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(apiUrlBase, fapiKey, fapiSecret);
                    client.Hangup(args[ArgsTypes.FAPI_CALL_ID]);
                    LogUtils.MyHandle.HandleException(exc, "Exception executing call to advertiser in phone api.");
                }
            }
        }

        #endregion
    }
}
