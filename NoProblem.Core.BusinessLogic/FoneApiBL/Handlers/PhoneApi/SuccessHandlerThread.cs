﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.PhoneApi
{
    internal class SuccessHandlerThread : Runnable
    {
        public SuccessHandlerThread(Guid incidentId, DataModel.Xrm.new_incidentaccount incAcc, DataModel.Xrm.account supplier)
        {
            this.incidentId = incidentId;
            this.incAcc = incAcc;
            this.supplier = supplier;
        }

        private Guid incidentId;
        private DataModel.Xrm.new_incidentaccount incAcc;
        private DataModel.Xrm.account supplier;

        protected override void Run()
        {
            try
            {
                DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(XrmDataContext);
                DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                var incident = incidentDal.Retrieve(incidentId);
                manager.CloseIncidentAccountAfterLeadBuyer(incAcc, incident, incidentDal, incidentAccountDal, false);
                bool isExclusive = supplier.new_isexclusivebroker.HasValue && supplier.new_isexclusivebroker.Value;
                manager.ContinueRequestAfterBrokerCall(true, incidentId, isExclusive);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in PhoneApi.SuccessHandlerThread.Run");
            }
        }
    }
}
