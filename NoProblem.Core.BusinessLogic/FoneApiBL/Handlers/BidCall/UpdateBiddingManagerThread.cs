﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.ServiceRequest;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.BidCall
{
    internal class UpdateBiddingManagerThread : Runnable
    {
        Guid incidentAccountId;
        decimal newPrice;
        bool priceChanged;
        bool rejected;
        BiddingManager biddingManager;

        public UpdateBiddingManagerThread(Guid incidentAccountId, decimal newPrice, bool priceChanged, bool rejected)
        {
            this.incidentAccountId = incidentAccountId;
            this.newPrice = newPrice;
            this.priceChanged = priceChanged;
            this.rejected = rejected;
            biddingManager = new BiddingManager(XrmDataContext);
        }

        protected override void Run()
        {
            try
            {
                biddingManager.UpdateBidCallEnd(incidentAccountId, newPrice, priceChanged, rejected);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in UpdateBiddingManagerThread.Run");
            }
        }
    }
}
