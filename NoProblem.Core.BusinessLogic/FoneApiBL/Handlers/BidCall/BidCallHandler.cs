﻿using System;
using FoneApiWrapper;
using FoneApiWrapper.CallBacks;
using NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.BidCall;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers
{
    internal class BidCallHandler : FoneApiHandlerBase, IBidCallHandler
    {
        //private static TtsFileCreator ttsFileCreator = new TtsFileCreator();

        private string _promptsClass;
        private string PromptsClass
        {
            get
            {
                if (String.IsNullOrEmpty(_promptsClass))
                {
                    _promptsClass = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.PROMPTS_CLASS);
                }
                return _promptsClass;
            }
        }

        private const string _HANDLERNAME = FoneApiBL.FoneApiRouter.HandlersNames.BID_CALL_HANDLER;
        private const string _FAPI_INCIDENTACCOUNTID = "npp1";
        private const string _FAPI_MAXPRICE = "npp2";
        private const string _FAPI_MINPRICE = "npp3";
        private const string _FAPI_REJECTENABLED = "npp4";
        private const string _FAPI_TTSFILEURL = "npp5";
        private const string _FAPI_BIDTYPE = "npp6";
        private const string _FAPI_FIRSTNUMBER = "npp7";
        private const string _FAPI_NEWPRICE = "npp8";
        private const string _FAPI_FAILCOUNT = "npp9";
        private const string _FAPI_ADVERTISER_PHONE = "npp10";
        private const string INITIAL_BID = "1";
        private const string COMPLETION_BID = "2";
        private const string WANT_TO_CHANGE = "3";
        private const string REENTER = "4";
        private const string WANT_TO_REVERT = "5";


        public bool StartCall(
            string ttsFileUrl,
            Guid incidentAccountId,
            decimal maxPrice,
            decimal minPrice,
            bool rejectEnabled,
            string supplierPhone,
            string phoneCountry)
        {
            FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
            var call = client.Dial(FoneApiWrapper.Commands.eDialDestinationType.number,
                  NormalizePhone(supplierPhone, phoneCountry),
                  returnAddress,
                  appId,
                  returnAddress,
                  _defaultDialTimeOut,
                  Cid,
                  null,
                  null,
                  Trunk,
                  null,
                  new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_FAPI_INCIDENTACCOUNTID, incidentAccountId.ToString()),
                  new CustomParam(_FAPI_MAXPRICE, decimal.ToInt32(decimal.Floor(maxPrice)).ToString()),
                  new CustomParam(_FAPI_MINPRICE, decimal.ToInt32(decimal.Floor(minPrice)).ToString()),
                  new CustomParam(_FAPI_REJECTENABLED, rejectEnabled.ToString()),
                  new CustomParam(_FAPI_TTSFILEURL, ttsFileUrl),
                  new CustomParam(_FAPI_ADVERTISER_PHONE, supplierPhone)
            );
            if (call.status != 0)
            {
                MarkBidDoneThread tr = new MarkBidDoneThread(incidentAccountId);
                tr.Start();
                return false;
            }
            return true;
        }

        protected override CallBackResponse CallAnswered(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            bool rejectEnabled = bool.Parse(args[_FAPI_REJECTENABLED]);
            response.PlayFile(AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/prompt.wav", null);
            TtsAndBidToResponse(args, response, rejectEnabled);
            return response;
        }

        protected override FoneApiWrapper.CallBacks.CallBackResponse GetDtmfComplete(FapiArgs args)
        {
            CallBackResponse response = null;
            string bidType = args[_FAPI_BIDTYPE];
            if (bidType == INITIAL_BID)
            {
                response = GetDtmfCompleteInitialBid(args);
            }
            else if (bidType == COMPLETION_BID)
            {
                response = GetDtmfCompleteCompletionBid(args);
            }
            else if (bidType == WANT_TO_CHANGE)
            {
                response = GetDtmfCompleteWantToChange(args);
            }
            else if (bidType == REENTER)
            {
                response = GetDtmfCompleteReenter(args);
            }
            else if (bidType == WANT_TO_REVERT)
            {
                response = GetDtmfCompleteWantToRevert(args);
            }
            return response;
        }

        protected override void Hangup(FapiArgs args)
        {
            Guid incidentAccountId = new Guid(args[_FAPI_INCIDENTACCOUNTID]);
            MarkBidDoneThread tr = new MarkBidDoneThread(incidentAccountId);
            tr.Start();
            CreateCDR(args[ArgsTypes.FAPI_HANGUP_CAUSE], args[ArgsTypes.FAPI_CALL_ID], args[ArgsTypes.FAPI_CALL_DURATION], args[_FAPI_ADVERTISER_PHONE], "bid call", NoProblem.Core.DataModel.Xrm.new_calldetailrecord.Type.tts, incidentAccountId);
        }

        private void TtsAndBidToResponse(FapiArgs args, CallBackResponse response, bool rejectEnabled)
        {
            response.PlayFile(args[_FAPI_TTSFILEURL], null);
            string fileToPlay;
            if (rejectEnabled)
                fileToPlay = AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/placebid_with_reject.wav";
            else
                fileToPlay = AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/placebid.wav";
            BidInitialToResponse(response, fileToPlay, args, 0);
        }

        private void BidInitialToResponse(CallBackResponse response, string fileToPlay, FapiArgs args, int failCount)
        {
            response.FlushDtmfBuffer();
            response.PlayAndGetDtmf(fileToPlay, returnAddress, 1, "#", 30,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                new CustomParam(_FAPI_INCIDENTACCOUNTID, args[_FAPI_INCIDENTACCOUNTID]),
                new CustomParam(_FAPI_MAXPRICE, args[_FAPI_MAXPRICE]),
                new CustomParam(_FAPI_MINPRICE, args[_FAPI_MINPRICE]),
                new CustomParam(_FAPI_REJECTENABLED, args[_FAPI_REJECTENABLED]),
                new CustomParam(_FAPI_TTSFILEURL, args[_FAPI_TTSFILEURL]),
                new CustomParam(_FAPI_BIDTYPE, INITIAL_BID),
                new CustomParam(_FAPI_FAILCOUNT, failCount.ToString())
              );
        }

        private CallBackResponse GetDtmfCompleteCompletionBid(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            string firstNumber = args[_FAPI_FIRSTNUMBER];
            string digits;
            int newPrice;
            if (args.TryGetValue(ArgsTypes.FAPI_DIGITS, out digits))//something was pressed
            {
                string newPriceStr = firstNumber + digits;
                if (!int.TryParse(newPriceStr, out newPrice))
                {
                    ErrorOnInitial(args, response, "reprompt");
                    return response;
                }
            }
            else
            {
                newPrice = int.Parse(firstNumber);
            }
            int minPrice = int.Parse(args[_FAPI_MINPRICE]);
            if (newPrice < minPrice)
            {
                ErrorOnInitial(args, response, "reprompt_below");
            }
            else if (newPrice > int.Parse(args[_FAPI_MAXPRICE]))
            {
                ErrorOnInitial(args, response, "reprompt");
            }
            else
            {
                ReadNewPriceToResponse(response, newPrice);
                response.FlushDtmfBuffer();
                response.PlayAndGetDtmf(
                    AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/validate2.wav",
                    returnAddress, 1, "", 30,
                    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                    new CustomParam(_FAPI_INCIDENTACCOUNTID, args[_FAPI_INCIDENTACCOUNTID]),
                    new CustomParam(_FAPI_MAXPRICE, args[_FAPI_MAXPRICE]),
                    new CustomParam(_FAPI_MINPRICE, args[_FAPI_MINPRICE]),
                    new CustomParam(_FAPI_REJECTENABLED, args[_FAPI_REJECTENABLED]),
                    new CustomParam(_FAPI_BIDTYPE, WANT_TO_CHANGE),
                    new CustomParam(_FAPI_NEWPRICE, newPrice.ToString())
                );
            }
            return response;
        }

        private void ReadNewPriceToResponse(CallBackResponse response, int newPrice)
        {
            response.PlayFile(AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/validate1.wav", null);
            if (PromptsClass.StartsWith(_HEB_PROMPT_START))
            {
                if (newPrice > 0 && newPrice <= 1000)
                {
                    response.PlayFile(
                        AudioFilesUrlBase + AudioFilesConsts.SLASH + PromptsClass + AudioFilesConsts.SLASH + AudioFilesConsts.NATURAL_NUMBERS_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + "he-IL-Ahron-" + newPrice.ToString() + AudioFilesConsts.WAV_FILE_EXTENSION,
                        null);
                }
            }
            else
            {
                var numbersFileList = Utils.AudioFilesUtils.NumberToAudioFilesPartition(newPrice);
                if (numbersFileList != null)
                {
                    foreach (int filePartition in numbersFileList)
                    {
                        response.PlayFile(
                            AudioFilesUrlBase + AudioFilesConsts.SLASH + PromptsClass + AudioFilesConsts.SLASH + AudioFilesConsts.NATURAL_NUMBERS_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + filePartition.ToString() + "n" + AudioFilesConsts.WAV_FILE_EXTENSION,
                            null);
                    }
                }
            }
        }

        private CallBackResponse GetDtmfCompleteInitialBid(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            string digits;
            if (args.TryGetValue(ArgsTypes.FAPI_DIGITS, out digits))//something was pressed
            {
                bool rejectEnabled = bool.Parse(args[_FAPI_REJECTENABLED]);
                char firstChar = digits[0];
                if (firstChar == '*')//replay tts 
                {
                    TtsAndBidToResponse(args, response, rejectEnabled);
                }
                else if (firstChar == '0')
                {
                    if (rejectEnabled)
                    {
                        UpdateBiddingManagerThread tr = new UpdateBiddingManagerThread(new Guid(args[_FAPI_INCIDENTACCOUNTID]), 0, false, true);
                        tr.Start();
                        response.PlayFile(AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/reject.wav", null);
                        response.Hangup();
                    }
                    else
                    {
                        ErrorOnInitial(args, response, "reprompt_below");
                    }
                }
                else if (firstChar == '#')
                {
                    ErrorOnInitial(args, response, "reprompt");
                }
                else
                {
                    int newPrice;
                    if (int.TryParse(firstChar.ToString(), out newPrice))
                    {
                        response.GetDtmf(20, "#", 30, returnAddress,
                            new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                            new CustomParam(_FAPI_INCIDENTACCOUNTID, args[_FAPI_INCIDENTACCOUNTID]),
                            new CustomParam(_FAPI_MAXPRICE, args[_FAPI_MAXPRICE]),
                            new CustomParam(_FAPI_MINPRICE, args[_FAPI_MINPRICE]),
                            new CustomParam(_FAPI_REJECTENABLED, args[_FAPI_REJECTENABLED]),
                            new CustomParam(_FAPI_TTSFILEURL, args[_FAPI_TTSFILEURL]),
                            new CustomParam(_FAPI_BIDTYPE, COMPLETION_BID),
                            new CustomParam(_FAPI_FIRSTNUMBER, newPrice.ToString()),
                            new CustomParam(_FAPI_FAILCOUNT, args[_FAPI_FAILCOUNT])
                          );
                    }
                    else
                    {
                        ErrorOnInitial(args, response, "reprompt");
                    }
                }
            }
            else//nothing pressed
            {
                response.PlayFile(AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/default.wav", null);
                response.Hangup();
            }
            return response;
        }

        private void ErrorOnInitial(FapiArgs args, CallBackResponse response, string fileName)
        {
            int failCount = int.Parse(args[_FAPI_FAILCOUNT]);
            if (failCount > 0)
            {
                FailToResponse(args, response);
            }
            else
            {
                BidInitialToResponse(response, AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/" + fileName + AudioFilesConsts.WAV_FILE_EXTENSION, args, ++failCount);
            }
        }

        private void FailToResponse(FapiArgs args, CallBackResponse response)
        {
            response.PlayFile(AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/fail.wav", null);
            response.Hangup();
        }

        private CallBackResponse GetDtmfCompleteWantToRevert(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            string digits;
            bool wantsToRevert = false;
            if (args.TryGetValue(ArgsTypes.FAPI_DIGITS, out digits))//something was pressed
            {
                wantsToRevert = digits == 1.ToString();
            }
            if (wantsToRevert)
            {
                response.PlayFile(AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/default.wav", null);
                response.Hangup();
            }
            else
            {
                response.Hangup();
                UpdateBiddingManagerThread tr = new UpdateBiddingManagerThread(new Guid(args[_FAPI_INCIDENTACCOUNTID]), decimal.Parse(args[_FAPI_NEWPRICE]), true, false);
                tr.Start();
            }
            return response;
        }

        private CallBackResponse GetDtmfCompleteReenter(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            string digits;
            int newPrice;
            if (args.TryGetValue(ArgsTypes.FAPI_DIGITS, out digits) && int.TryParse(digits, out newPrice))//something was pressed
            {
                int minPrice = int.Parse(args[_FAPI_MINPRICE]);
                int maxPrice = int.Parse(args[_FAPI_MAXPRICE]);
                if (newPrice < minPrice || newPrice > maxPrice)
                {
                    FailToResponse(args, response);
                }
                else
                {
                    ReadNewPriceToResponse(response, newPrice);
                    response.FlushDtmfBuffer();
                    response.PlayAndGetDtmf(
                        AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/validate3.wav",
                        returnAddress, 1, "", 30,
                        new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                        new CustomParam(_FAPI_INCIDENTACCOUNTID, args[_FAPI_INCIDENTACCOUNTID]),
                        new CustomParam(_FAPI_MAXPRICE, args[_FAPI_MAXPRICE]),
                        new CustomParam(_FAPI_MINPRICE, args[_FAPI_MINPRICE]),
                        new CustomParam(_FAPI_BIDTYPE, WANT_TO_REVERT),
                        new CustomParam(_FAPI_NEWPRICE, newPrice.ToString())
                    );
                }
            }
            else
            {
                FailToResponse(args, response);
            }
            return response;
        }

        private CallBackResponse GetDtmfCompleteWantToChange(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            string digits;
            bool wantsToChange = false;
            if (args.TryGetValue(ArgsTypes.FAPI_DIGITS, out digits))//something was pressed
            {
                wantsToChange = digits == 1.ToString();
            }
            if (wantsToChange)
            {
                response.FlushDtmfBuffer();
                response.PlayAndGetDtmf(
                    AudioFilesUrlBase + "/bidcall/" + PromptsClass + "/reenter.wav",
                    returnAddress, 20, "#", 30,
                    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                    new CustomParam(_FAPI_INCIDENTACCOUNTID, args[_FAPI_INCIDENTACCOUNTID]),
                    new CustomParam(_FAPI_MAXPRICE, args[_FAPI_MAXPRICE]),
                    new CustomParam(_FAPI_MINPRICE, args[_FAPI_MINPRICE]),
                    new CustomParam(_FAPI_BIDTYPE, REENTER)
                );
            }
            else
            {
                response.Hangup();
                UpdateBiddingManagerThread tr = new UpdateBiddingManagerThread(new Guid(args[_FAPI_INCIDENTACCOUNTID]), decimal.Parse(args[_FAPI_NEWPRICE]), true, false);
                tr.Start();
            }
            return response;
        }
    }
}
