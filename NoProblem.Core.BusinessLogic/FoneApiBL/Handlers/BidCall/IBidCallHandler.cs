﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.BidCall
{
    internal interface IBidCallHandler
    {
        bool StartCall(
            string ttsFileUrl,
            Guid incidentAccountId,
            decimal maxPrice,
            decimal minPrice,
            bool rejectEnabled,
            string supplierPhone,
            string phoneCountry);
    }
}
