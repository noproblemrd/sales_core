﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoneApiWrapper;
using FoneApiWrapper.CallBacks;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.BidCall.BidCall2014
{
    internal class BidCall2014Handler : FoneApiHandlerBase, IBidCallHandler
    {
        private const string _HANDLERNAME = FoneApiBL.FoneApiRouter.HandlersNames.BID_CALL_2014_HANDLER;

        private const string AUDIO_FILES_DIRECTORY = "/bidcall2014/";

        private const string _NPP_INCIDENTACCOUNT = "npp1";
        private const string _NPP_MAXPRICE = "npp2";
        private const string _NPP_MINPRICE = "npp3";
        private const string _NPP_TTSFILEURL = "npp4";
        private const string _NPP_SUPPLIERPHONE = "npp5";
        private const string _NPP_DTMF_STEP = "npp6";
        private const string _NPP_NEWPRICE = "npp7";

        private static class DtmfSteps
        {
            internal const string INITIAL_PROMPT = "1";
            internal const string TTS_DESCRIPTION = "2";
            internal const string MAIN_MENU = "3";
            internal const string PRICE = "4";
            internal const string CONFIRMATION = "5";
        }

        private static class AudioFilesNames
        {
            internal const string LEAD_ACCEPTED = "lead_accepted.wav";
            internal const string PLACEBID = "placebid.wav";
            internal const string PLACEBID_SETPRICE = "placebid_setprice.wav";
            internal const string PROMPT = "prompt.wav";
            internal const string REPROMPT_ABOVE = "reprompt_above.wav";
            internal const string REPROMPT_BELOW1 = "reprompt_below1.wav";
            internal const string REPROMPT_BELOW2 = "reprompt_below2.wav";
            internal const string VALID_OPTION = "valid_option.wav";
            internal const string VALIDATE1 = "validate1.wav";
            internal const string VALIDATE2 = "validate2.wav";
        }

        #region IBidCallHandler Members

        public bool StartCall(string ttsFileUrl, Guid incidentAccountId, decimal maxPrice, decimal minPrice, bool rejectEnabled, string supplierPhone, string phoneCountry)
        {
            return StartCall(ttsFileUrl, incidentAccountId, maxPrice, minPrice, supplierPhone, phoneCountry);
        }

        #endregion

        internal bool StartCall(
            string ttsFileUrl,
            Guid incidentAccountId,
            decimal maxPrice,
            decimal minPrice,
            string supplierPhone,
            string phoneCountry)
        {
            FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
            var call = client.Dial(FoneApiWrapper.Commands.eDialDestinationType.number,
                  NormalizePhone(supplierPhone, phoneCountry),
                  returnAddress,
                  appId,
                  returnAddress,
                  _defaultDialTimeOut,
                  Cid,
                  null,
                  null,
                  Trunk,
                  null,
                  new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_NPP_INCIDENTACCOUNT, incidentAccountId.ToString()),
                  new CustomParam(_NPP_MAXPRICE, decimal.ToInt32(decimal.Floor(maxPrice)).ToString()),
                  new CustomParam(_NPP_MINPRICE, decimal.ToInt32(decimal.Floor(minPrice)).ToString()),
                  new CustomParam(_NPP_TTSFILEURL, ttsFileUrl),
                  new CustomParam(_NPP_SUPPLIERPHONE, supplierPhone)
            );
            if (call.status != 0)
            {
                MarkBidDoneThread tr = new MarkBidDoneThread(incidentAccountId);
                tr.Start();
                return false;
            }
            return true;
        }

        protected override FoneApiWrapper.CallBacks.CallBackResponse CallAnswered(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            response.PlayAndGetDtmf(
                AudioFilesUrlBase + AUDIO_FILES_DIRECTORY + AudioFilesNames.PROMPT,
                returnAddress,
                1,
                TERMINATION_KEY_POUND,
                0,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_NPP_INCIDENTACCOUNT, args[_NPP_INCIDENTACCOUNT]),
                  new CustomParam(_NPP_MAXPRICE, args[_NPP_MAXPRICE]),
                  new CustomParam(_NPP_MINPRICE, args[_NPP_MINPRICE]),
                  new CustomParam(_NPP_TTSFILEURL, args[_NPP_TTSFILEURL]),
                  new CustomParam(_NPP_SUPPLIERPHONE, args[_NPP_SUPPLIERPHONE]),
                  new CustomParam(_NPP_DTMF_STEP, DtmfSteps.INITIAL_PROMPT)
            );
            return response;
        }

        protected override CallBackResponse GetDtmfComplete(FapiArgs args)
        {
            switch (args[_NPP_DTMF_STEP])
            {
                case DtmfSteps.INITIAL_PROMPT:
                case DtmfSteps.TTS_DESCRIPTION:
                case DtmfSteps.MAIN_MENU:
                    return GetDtmfComplete_InitialMenu(args);
                case DtmfSteps.PRICE:
                    return GetDtmfComplete_Price(args);
                case DtmfSteps.CONFIRMATION:
                    return GetDtmfComplete_Confirmation(args);
            }
            return null;
        }

        protected override void Hangup(FapiArgs args)
        {
            Guid incidentAccountId = new Guid(args[_NPP_INCIDENTACCOUNT]);
            MarkBidDoneThread tr = new MarkBidDoneThread(incidentAccountId);
            tr.Start();
            CreateCDR(args[ArgsTypes.FAPI_HANGUP_CAUSE], args[ArgsTypes.FAPI_CALL_ID], args[ArgsTypes.FAPI_CALL_DURATION], args[_NPP_SUPPLIERPHONE], "bid call 2014", NoProblem.Core.DataModel.Xrm.new_calldetailrecord.Type.tts, incidentAccountId);
        }

        private CallBackResponse GetDtmfComplete_Confirmation(FapiArgs args)
        {
            string digits;
            if (GetDigits(args, out digits))//something was pressed
            {
                switch (digits)
                {
                    case "1":
                        return LeadAccepted(args, true, decimal.Parse(args[_NPP_NEWPRICE]));
                    case "2":
                        return EnterNewPrice(args);
                    default:
                        return PleasePressAValidOption(args);
                }
            }
            else//nothing was pressed
            {
                return TimeoutDtmf(args);
            }
        }

        private CallBackResponse GetDtmfComplete_Price(FapiArgs args)
        {
            string digits;
            if (GetDigits(args, out digits))//something was pressed
            {
                int newPrice;
                if (int.TryParse(digits, out newPrice))
                {
                    if (newPrice > int.Parse(args[_NPP_MAXPRICE]))
                    {
                        return PlayUnableToAcceptBid(args);
                    }
                    else if (newPrice < int.Parse(args[_NPP_MINPRICE]))
                    {
                        return PlayBidTooLow(args);
                    }
                    else
                    {
                        return ConfirmBid(args, newPrice);
                    }
                }
                else
                {
                    return PlayUnableToAcceptBid(args);
                }
            }
            else//nothing was pressed
            {
                return TimeoutDtmf(args);
            }
        }

        private CallBackResponse ConfirmBid(FapiArgs args, int newPrice)
        {
            return ConfirmBid(args, newPrice, null);
        }

        private CallBackResponse ConfirmBid(FapiArgs args, int newPrice, CallBackResponse response)
        {
            if (response == null)
                response = new CallBackResponse();
            response.PlayFile(AudioFilesUrlBase + AUDIO_FILES_DIRECTORY + AudioFilesNames.VALIDATE1, null);
            var numbersFileList = Utils.AudioFilesUtils.NumberToAudioFilesPartition(newPrice);
            if (numbersFileList != null)
            {
                foreach (int filePartition in numbersFileList)
                {
                    response.PlayFile(
                        AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHIELA + AudioFilesConsts.SLASH + AudioFilesConsts.NATURAL_NUMBERS_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + filePartition.ToString() + "n" + AudioFilesConsts.WAV_FILE_EXTENSION,
                        null);
                }
            }
            response.FlushDtmfBuffer();
            response.PlayAndGetDtmf(
                AudioFilesUrlBase + AUDIO_FILES_DIRECTORY + AudioFilesNames.VALIDATE2,
                returnAddress,
                1,
                TERMINATION_KEY_POUND,
                _defaultDtmfTimeout,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_NPP_INCIDENTACCOUNT, args[_NPP_INCIDENTACCOUNT]),
                  new CustomParam(_NPP_MAXPRICE, args[_NPP_MAXPRICE]),
                  new CustomParam(_NPP_MINPRICE, args[_NPP_MINPRICE]),
                  new CustomParam(_NPP_SUPPLIERPHONE, args[_NPP_SUPPLIERPHONE]),
                  new CustomParam(_NPP_DTMF_STEP, DtmfSteps.CONFIRMATION),
                  new CustomParam(_NPP_NEWPRICE, newPrice.ToString())
            );
            return response;
        }

        private CallBackResponse PlayBidTooLow(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            response.PlayFile(AudioFilesUrlBase + AUDIO_FILES_DIRECTORY + AudioFilesNames.REPROMPT_BELOW1, null);
            var numbersFileList = Utils.AudioFilesUtils.NumberToAudioFilesPartition(int.Parse(args[_NPP_MINPRICE]));
            if (numbersFileList != null)
            {
                foreach (int filePartition in numbersFileList)
                {
                    response.PlayFile(
                        AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHIELA + AudioFilesConsts.SLASH + AudioFilesConsts.NATURAL_NUMBERS_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + filePartition.ToString() + "n" + AudioFilesConsts.WAV_FILE_EXTENSION,
                        null);
                }
            }
            response.PlayFile(AudioFilesUrlBase + AUDIO_FILES_DIRECTORY + AudioFilesNames.REPROMPT_BELOW2, null);
            return EnterNewPrice(args, response);
        }

        private CallBackResponse PlayUnableToAcceptBid(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            response.PlayFile(AudioFilesUrlBase + AUDIO_FILES_DIRECTORY + AudioFilesNames.REPROMPT_ABOVE, null);
            return EnterNewPrice(args, response);
        }

        private CallBackResponse GetDtmfComplete_InitialMenu(FapiArgs args)
        {
            string digits;
            if (GetDigits(args, out digits))//something was pressed
            {
                switch (digits)
                {
                    case "1":
                        return LeadAccepted(args, false, 0);
                    case "2":
                        return EnterNewPrice(args);
                    case "3":
                        return PlayTtsDescription(args);
                    case "0":
                        return PlayMainMenu(args);
                    default:
                        return PleasePressAValidOption(args);
                }
            }
            else//nothing was pressed
            {
                switch (args[_NPP_DTMF_STEP])
                {
                    case DtmfSteps.INITIAL_PROMPT:
                        return PlayTtsDescription(args);
                    case DtmfSteps.TTS_DESCRIPTION:
                        return PlayMainMenu(args);
                    case DtmfSteps.MAIN_MENU:
                        return TimeoutDtmf(args);
                    default:
                        return null;
                }
            }
        }

        private CallBackResponse TimeoutDtmf(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            response.Hangup();
            return response;
        }

        private CallBackResponse PleasePressAValidOption(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            response.PlayFile(AudioFilesUrlBase + AUDIO_FILES_DIRECTORY + AudioFilesNames.VALID_OPTION, null);
            string newPriceStr;
            if (args.TryGetValue(_NPP_NEWPRICE, out newPriceStr))
            {
                return ConfirmBid(args, int.Parse(newPriceStr), response);
            }
            else
            {
                return PlayMainMenu(args, response);
            }
        }

        private CallBackResponse PlayMainMenu(FapiArgs args)
        {
            return PlayMainMenu(args, null);
        }

        private CallBackResponse PlayMainMenu(FapiArgs args, CallBackResponse response)
        {
            if (response == null)
                response = new CallBackResponse();
            response.PlayAndGetDtmf(
                AudioFilesUrlBase + AUDIO_FILES_DIRECTORY + AudioFilesNames.PLACEBID,
                returnAddress,
                1,
                TERMINATION_KEY_POUND,
                _defaultDtmfTimeout,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_NPP_INCIDENTACCOUNT, args[_NPP_INCIDENTACCOUNT]),
                  new CustomParam(_NPP_MAXPRICE, args[_NPP_MAXPRICE]),
                  new CustomParam(_NPP_MINPRICE, args[_NPP_MINPRICE]),
                  new CustomParam(_NPP_TTSFILEURL, args[_NPP_TTSFILEURL]),
                  new CustomParam(_NPP_SUPPLIERPHONE, args[_NPP_SUPPLIERPHONE]),
                  new CustomParam(_NPP_DTMF_STEP, DtmfSteps.MAIN_MENU)
            );
            return response;
        }

        private CallBackResponse EnterNewPrice(FapiArgs args)
        {
            return EnterNewPrice(args, null);
        }

        private CallBackResponse EnterNewPrice(FapiArgs args, CallBackResponse response)
        {
            if (response == null)
                response = new CallBackResponse();
            response.FlushDtmfBuffer();
            response.PlayAndGetDtmf(
                AudioFilesUrlBase + AUDIO_FILES_DIRECTORY + AudioFilesNames.PLACEBID_SETPRICE,
                returnAddress,
                10,
                TERMINATION_KEY_POUND,
                _defaultDtmfTimeout,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_NPP_INCIDENTACCOUNT, args[_NPP_INCIDENTACCOUNT]),
                  new CustomParam(_NPP_MAXPRICE, args[_NPP_MAXPRICE]),
                  new CustomParam(_NPP_MINPRICE, args[_NPP_MINPRICE]),
                  new CustomParam(_NPP_SUPPLIERPHONE, args[_NPP_SUPPLIERPHONE]),
                  new CustomParam(_NPP_DTMF_STEP, DtmfSteps.PRICE)
            );
            return response;
        }

        private CallBackResponse LeadAccepted(FapiArgs args, bool priceChanged, decimal newPrice)
        {
            UpdateBiddingManagerThread tr = new UpdateBiddingManagerThread(new Guid(args[_NPP_INCIDENTACCOUNT]), newPrice, priceChanged, false);
            tr.Start();
            CallBackResponse response = new CallBackResponse();
            response.PlayFile(AudioFilesUrlBase + AUDIO_FILES_DIRECTORY + AudioFilesNames.LEAD_ACCEPTED, null);
            response.Hangup();
            return response;
        }

        private CallBackResponse PlayTtsDescription(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            response.PlayAndGetDtmf(args[_NPP_TTSFILEURL],
                returnAddress,
                1,
                TERMINATION_KEY_POUND,
                0,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_NPP_INCIDENTACCOUNT, args[_NPP_INCIDENTACCOUNT]),
                  new CustomParam(_NPP_MAXPRICE, args[_NPP_MAXPRICE]),
                  new CustomParam(_NPP_MINPRICE, args[_NPP_MINPRICE]),
                  new CustomParam(_NPP_TTSFILEURL, args[_NPP_TTSFILEURL]),
                  new CustomParam(_NPP_SUPPLIERPHONE, args[_NPP_SUPPLIERPHONE]),
                  new CustomParam(_NPP_DTMF_STEP, DtmfSteps.TTS_DESCRIPTION)
            );
            return response;
        }

             
    }
}
