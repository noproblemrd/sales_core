﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.ServiceRequest;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.BidCall
{
    internal class MarkBidDoneThread : Runnable
    {
        Guid incidentAccountId;
        BiddingManager biddingManager;

        public MarkBidDoneThread(Guid incidentAccountId)
        {
            this.incidentAccountId = incidentAccountId;
            biddingManager = new BiddingManager(XrmDataContext);
        }

        protected override void Run()
        {
            biddingManager.MarkBidDone(incidentAccountId);
        }
    }
}
