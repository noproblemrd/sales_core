﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.JonaCall
{
    internal class CallStatusChangeHandler : Runnable
    {
        private Guid incidentAccountId;
        private Guid incidentId;
        private eStatus status;

        public enum eStatus
        {
            callingCustomer,
            customerAnswered
        }

        public CallStatusChangeHandler(Guid incidentAccountId, Guid incidentId, eStatus status)
        {
            this.incidentAccountId = incidentAccountId;
            this.incidentId = incidentId;
            this.status = status;
        }

        protected override void Run()
        {
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(XrmDataContext);
            var incidentQuery = (from inc in incidentDal.All
                                 where inc.incidentid == incidentId
                                 select new { AppType = inc.new_apptype }).First();
            DataAccessLayer.IncidentAccount dal = new DataAccessLayer.IncidentAccount(XrmDataContext);
            if (incidentQuery.AppType == (int)DataModel.Consumer.ServiceRequestEnums.eAppType.DeskApp)
            {
                SendPusherEvent(dal);
            }
            if (status == eStatus.callingCustomer)
                SetIncidentAccountStatus(dal);
        }

        private void SendPusherEvent(DataAccessLayer.IncidentAccount dal)
        {
            var accountId = (from ia in dal.All
                             where ia.new_incidentaccountid == incidentAccountId
                             select ia.new_accountid.Value).First();
            DataModel.Pusher.CaseFlow.CallStatusContainer data = new DataModel.Pusher.CaseFlow.CallStatusContainer() { SupplierId = accountId };
            string js;
            if (status == eStatus.callingCustomer)
            {
                js = new Pusher.DesktopApp.Builders.CaseFlowScriptsBuilder().BuildCallingCustomerScript(data);
            }
            else
            {
                js = new Pusher.DesktopApp.Builders.CaseFlowScriptsBuilder().BuildCustomerConnectedScript(data);
            }
            Pusher.DesktopApp.PusherDesktopAppInjectJsEvent jsEvent = new Pusher.DesktopApp.PusherDesktopAppInjectJsEvent(incidentId.ToString(), js);
            Pusher.PusherEventSender.TriggerEvent(jsEvent);
        }

        private void SetIncidentAccountStatus(DataAccessLayer.IncidentAccount dal)
        {
            DataModel.Xrm.new_incidentaccount ia = new DataModel.Xrm.new_incidentaccount(XrmDataContext);
            ia.new_incidentaccountid = incidentAccountId;
            ia.statuscode = (int)DataModel.Xrm.new_incidentaccount.Status.IN_CONVERSATION;
            dal.Update(ia);
            XrmDataContext.SaveChanges();
        }
    }
}
