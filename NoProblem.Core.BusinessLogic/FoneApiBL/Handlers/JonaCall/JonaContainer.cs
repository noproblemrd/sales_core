﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.JonaCall
{
    internal class JonaContainer
    {
        public string AdvertiserCallId { get; set; }

        private volatile string customerCallId;
        public string CustomerCallId
        {
            get
            {
                return customerCallId;
            }
            set
            {
                customerCallId = value;
            }
        }

        public bool RecordCall { get; set; }

        private volatile bool closedAndProcessContinued = false;
        public bool ClosedAndProcessContinued
        {
            get
            {
                return closedAndProcessContinued;
            }
            set
            {
                closedAndProcessContinued = value;
            }
        }
    }
}
