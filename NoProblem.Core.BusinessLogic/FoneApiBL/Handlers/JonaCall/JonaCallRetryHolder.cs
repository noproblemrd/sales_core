﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.JonaCall
{
    internal class JonaCallRetryHolder
    {
        private JonaCallRetryHolder()
        {
            timers = new SynchronizedCollection<RetryContainer>();
        }

        private const int timerDelayMillisDefault = 60 * 1000;
        private int TimerDelayMillis
        {
            get
            {
                DataAccessLayer.AsteriskConfiguration phonesConfig = new NoProblem.Core.DataAccessLayer.AsteriskConfiguration(null);
                int seconds;
                if (!int.TryParse(phonesConfig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.DELAY_BETWEEN_JONA_RETRIES_SECONDS), out seconds))
                {
                    return timerDelayMillisDefault;
                }
                else if (seconds < 10)
                {
                    return timerDelayMillisDefault;
                }
                else
                {
                    return seconds * 1000;
                }
            }
        }

        private static volatile JonaCallRetryHolder _instance;
        private static readonly object instanceSync = new object();

        private System.Collections.Generic.SynchronizedCollection<RetryContainer> timers;

        public static JonaCallRetryHolder Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (instanceSync)
                    {
                        if (_instance == null)
                        {
                            _instance = new JonaCallRetryHolder();
                        }
                    }
                }
                return _instance;
            }
        }

        public void AddRetry(Guid incidentAccountId)
        {
            RetryContainer container = new RetryContainer();
            timers.Add(container);
            container.IncidentAccountId = incidentAccountId;
            Timer timer = new Timer(TimerCallBack, container, TimerDelayMillis, System.Threading.Timeout.Infinite);
            container.Timer = timer;
        }

        private void TimerCallBack(object containerObj)
        {
            RetryContainer container = (RetryContainer)containerObj;
            try
            {
                DataAccessLayer.IncidentAccount incAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(null);
                DataModel.Xrm.new_incidentaccount incAcc = incAccDal.Retrieve(container.IncidentAccountId);
                DataModel.Xrm.account supplier = incAcc.new_account_new_incidentaccount;
                DataModel.Xrm.incident incident = incAcc.new_incident_new_incidentaccount;
                DataAccessLayer.AccountExpertise acExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(incAccDal.XrmDataContext);
                DataModel.Xrm.new_accountexpertise acEx = acExpDal.GetByAccountAndPrimaryExpertise(supplier.accountid, incident.new_primaryexpertiseid.Value);
                bool rejectEnabled = ServiceRequest.ServiceRequestManager.IsRejectEnabledForJonaCall(incident, acEx);
                JonaCallHandler callHandler = new JonaCallHandler();
                callHandler.StartCall(incident, incAcc, supplier, rejectEnabled);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in JonaCallRetryHolder.TimerCallBack");                
            }
            finally
            {
                timers.Remove(container);
                container.Timer.Dispose();
            }
        }
    }
}
