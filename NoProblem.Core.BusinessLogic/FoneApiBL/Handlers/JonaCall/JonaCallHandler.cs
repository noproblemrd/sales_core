﻿using System;
using System.Collections.Generic;
using Effect.Crm.Logs;
using FoneApiWrapper;
using FoneApiWrapper.CallBacks;
using FoneApiWrapper.Commands;
using NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.JonaCall;
using NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers
{
    internal class JonaCallHandler : FoneApiHandlerBase
    {
        private const string _HANDLERNAME = FoneApiBL.FoneApiRouter.HandlersNames.JONA_CALL_HANDLER;
        private const string _FAPI_DTMFIDX = "npp1";
        private const string _FAPI_INCIDENT_ID = "npp2";
        private const string _FAPI_INCIDENT_ACCOUNT_ID = "npp3";
        private const string _FAPI_CUSTOMER_PHONE_NORMALIZED = "npp4";
        private const string _FAPI_PLAY_TTS = "npp5";
        private const string _FAPI_TTS_URL = "npp6";
        private const string _FAPI_ADVERTISERCALLID = "npp7";
        private const string _FAPI_ADVERTISER_PHONE = "npp8";
        private const string _FAPI_HEADING_CODE = "npp9";

        private static TtsFileCreator ttsFileCreator = new TtsFileCreator();
        private static Dictionary<string, JonaContainer> containersHolder = new Dictionary<string, JonaContainer>();

        private string _promptsClass;
        private string PromptsClass
        {
            get
            {
                if (String.IsNullOrEmpty(_promptsClass))
                {
                    _promptsClass = phonesConfigDal.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.PROMPTS_CLASS);
                }
                return _promptsClass;
            }
        }

        internal NoProblem.Core.BusinessLogic.LeadBuyers.SendLeadResponse StartCall(
            NoProblem.Core.DataModel.Xrm.incident incident,
            NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc,
            NoProblem.Core.DataModel.Xrm.account supplier,
            bool rejectEnabled)
        {
            var retVal = new NoProblem.Core.BusinessLogic.LeadBuyers.SendLeadResponse();
            bool playTts = false;
            string ttsFileUrl = null;
            if (rejectEnabled)
            {
                playTts = !String.IsNullOrEmpty(incident.description);
                if (playTts)
                {
                    try
                    {
                        ttsFileUrl = ttsFileCreator.GetTtsFileUrl(incident.incidentid.ToString(), incident.description);
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception creating tts file in JonaCallHandler. Executing call without TTS (and without reject option).");
                        ttsFileUrl = null;
                        playTts = false;
                    }
                }
                if (String.IsNullOrEmpty(ttsFileUrl))
                {
                    playTts = false;
                }
            }

            FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
            var call = client.Dial(FoneApiWrapper.Commands.eDialDestinationType.number,
                  NormalizePhone(supplier.telephone1, supplier.new_country),
                  returnAddress,
                  appId,
                  returnAddress,
                  _defaultDialTimeOut,
                  Cid,
                  null,
                  null,
                  Trunk,
                  null,
                  new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_FAPI_SIDE, _ADVERTISER),
                  new CustomParam(_FAPI_INCIDENT_ID, incident.incidentid.ToString()),
                  new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, incAcc.new_incidentaccountid.ToString()),
                  new CustomParam(_FAPI_CUSTOMER_PHONE_NORMALIZED, NormalizePhone(incident.new_telephone1, incident.new_country)),
                  new CustomParam(_FAPI_PLAY_TTS, playTts.ToString()),
                  new CustomParam(_FAPI_TTS_URL, ttsFileUrl),
                  new CustomParam(_FAPI_ADVERTISER_PHONE, supplier.telephone1),
                  new CustomParam(_FAPI_HEADING_CODE, incident.new_new_primaryexpertise_incident.new_code)
            );
            retVal.IsInCall = true;
            if (call.status == 0)
            {
                JonaContainer container = new JonaContainer();
                containersHolder.Add(call.callId, container);
                container.RecordCall = supplier.new_recordcalls.HasValue && supplier.new_recordcalls.Value;
                container.AdvertiserCallId = call.callId;
                incAcc.new_dialercallid = call.callId;
                DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                dal.Update(incAcc);
                XrmDataContext.SaveChanges();
            }
            else
            {
                ContinueProcessThread continueThread = new ContinueProcessThread(
                        incident.incidentid,
                        incAcc.new_incidentaccountid,
                        ContinueProcessThread.eEndStatus.NotOkBlameAdvertiser,
                        0.ToString(),
                        null);
                continueThread.Start();
            }
            return retVal;
        }

        protected override FoneApiWrapper.CallBacks.CallBackResponse CallAnswered(FapiArgs args)
        {
            CallBackResponse retVal = null;
            if (args[_FAPI_SIDE] == _ADVERTISER)
            {
                retVal = CallAnsweredAdvertiser(args);
            }
            else
            {
                retVal = CallAnsweredCostumer(args);
            }
            return retVal;
        }

        protected override CallBackResponse GetDtmfComplete(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            string digits;
            if (args.TryGetValue(ArgsTypes.FAPI_DIGITS, out digits))//something was pressed
            {
                if (args[_FAPI_PLAY_TTS] == true.ToString())
                {
                    if (digits == "*")
                    {
                        //replay
                        response.PlayAndGetDtmf(args[_FAPI_TTS_URL], returnAddress, 1, "#", 0,
                            new CustomParam(_FAPI_DTMFIDX, 1.ToString()),
                            new CustomParam(_FAPI_SIDE, _ADVERTISER),
                            new CustomParam(_FAPI_INCIDENT_ID, args[_FAPI_INCIDENT_ID]),
                            new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, args[_FAPI_INCIDENT_ACCOUNT_ID]),
                            new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                            new CustomParam(_FAPI_CUSTOMER_PHONE_NORMALIZED, args[_FAPI_CUSTOMER_PHONE_NORMALIZED]),
                            new CustomParam(_FAPI_PLAY_TTS, args[_FAPI_PLAY_TTS]),
                            new CustomParam(_FAPI_TTS_URL, args[_FAPI_TTS_URL]),
                            new CustomParam(_FAPI_HEADING_CODE, args[_FAPI_HEADING_CODE])
                        );
                    }
                    else if (digits == 0.ToString())
                    {
                        //reject
                        response.PlayFile(AudioFilesUrlBase + "/jonacall/" + PromptsClass + "/reject.wav", String.Empty);
                        response.Hangup();
                        ContinueProcessThread continueThread = new ContinueProcessThread(
                            new Guid(args[_FAPI_INCIDENT_ID]),
                            new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]),
                            ContinueProcessThread.eEndStatus.Rejected,
                            0.ToString(),
                            null);
                        continueThread.Start();
                        var container = containersHolder[args[ArgsTypes.FAPI_CALL_ID]];
                        container.ClosedAndProcessContinued = true;
                    }
                    else
                    {
                        //connect
                        DialToCustomer(args[_FAPI_CUSTOMER_PHONE_NORMALIZED], args[_FAPI_INCIDENT_ID], args[_FAPI_INCIDENT_ACCOUNT_ID], args[ArgsTypes.FAPI_CALL_ID], args[_FAPI_HEADING_CODE], response);
                    }
                }
                else//no tts, only option is connect when something is pressed.
                {
                    //connect
                    DialToCustomer(args[_FAPI_CUSTOMER_PHONE_NORMALIZED], args[_FAPI_INCIDENT_ID], args[_FAPI_INCIDENT_ACCOUNT_ID], args[ArgsTypes.FAPI_CALL_ID], args[_FAPI_HEADING_CODE], response);
                }
            }
            else//nothing pressed
            {
                string idx = args[_FAPI_DTMFIDX];
                if (idx == 0.ToString())
                {
                    //play tts
                    response.PlayAndGetDtmf(args[_FAPI_TTS_URL], returnAddress, 1, "#", 0,
                        new CustomParam(_FAPI_DTMFIDX, 1.ToString()),
                        new CustomParam(_FAPI_SIDE, _ADVERTISER),
                        new CustomParam(_FAPI_INCIDENT_ID, args[_FAPI_INCIDENT_ID]),
                        new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, args[_FAPI_INCIDENT_ACCOUNT_ID]),
                        new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                        new CustomParam(_FAPI_CUSTOMER_PHONE_NORMALIZED, args[_FAPI_CUSTOMER_PHONE_NORMALIZED]),
                        new CustomParam(_FAPI_PLAY_TTS, args[_FAPI_PLAY_TTS]),
                        new CustomParam(_FAPI_TTS_URL, args[_FAPI_TTS_URL]),
                        new CustomParam(_FAPI_HEADING_CODE, args[_FAPI_HEADING_CODE])
                    );
                }
                else if (idx == 1.ToString())
                {
                    response.PlayAndGetDtmf(AudioFilesUrlBase + "/jonacall/" + PromptsClass + "/welcome_posttext.wav", returnAddress, 1, "#", 20,
                        new CustomParam(_FAPI_DTMFIDX, 2.ToString()),
                        new CustomParam(_FAPI_SIDE, _ADVERTISER),
                        new CustomParam(_FAPI_INCIDENT_ID, args[_FAPI_INCIDENT_ID]),
                        new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, args[_FAPI_INCIDENT_ACCOUNT_ID]),
                        new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                        new CustomParam(_FAPI_CUSTOMER_PHONE_NORMALIZED, args[_FAPI_CUSTOMER_PHONE_NORMALIZED]),
                        new CustomParam(_FAPI_PLAY_TTS, args[_FAPI_PLAY_TTS]),
                        new CustomParam(_FAPI_TTS_URL, args[_FAPI_TTS_URL]),
                        new CustomParam(_FAPI_HEADING_CODE, args[_FAPI_HEADING_CODE])
                    );
                }
                else
                {
                    response.Hangup();
                    //failed to press anyting, we need to charge by sms
                    ContinueProcessThread continueThread = new ContinueProcessThread(
                        new Guid(args[_FAPI_INCIDENT_ID]),
                        new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]),
                        ContinueProcessThread.eEndStatus.NotOkBlameAdvertiser,
                        0.ToString(),
                        null);
                    continueThread.Start();
                    var container = containersHolder[args[ArgsTypes.FAPI_CALL_ID]];
                    container.ClosedAndProcessContinued = true;
                }
            }
            return response;
        }

        protected override CallBackResponse PlayFileComplete(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse();
            response.PlayFile(AudioFilesUrlBase + AudioFilesConsts.FAKE_RING, returnAddress,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME)
            );
            return response;
        }

        protected override void Hangup(FapiArgs args)
        {
            if (args[_FAPI_SIDE] == _ADVERTISER)
            {
                HangupAdvertiser(args);
            }
            else
            {
                HangupCustomer(args);
            }
        }

        private void SetCallStatusToConnected(FapiArgs args)
        {
            try
            {
                string incidentId = args[_FAPI_INCIDENT_ID];
                string iaId = args[_FAPI_INCIDENT_ACCOUNT_ID];
                CallStatusChangeHandler handler = new CallStatusChangeHandler(new Guid(iaId), new Guid(incidentId), CallStatusChangeHandler.eStatus.customerAnswered);
                handler.Start();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SetCallStatusToConnected failed with exception");
            }
        }

        private CallBackResponse CallAnsweredCostumer(FapiArgs args)
        {
            SetCallStatusToConnected(args);
            CallBackResponse response = new CallBackResponse(LimitCallDurationSeconds);
            string advertiserCallId = args[_FAPI_ADVERTISERCALLID];
            JonaContainer container = containersHolder[advertiserCallId];
            if (container.RecordCall && GlobalConfigurations.IsUsaSystem)
            {
                response.PlayFile(AudioFilesUrlBase + "/jonacall/" + AudioFilesConsts.SHARON + "/Customer_call_recording-02A.wav", null);
                string headingCode = args[_FAPI_HEADING_CODE];
                string headingFile = AudioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHARON + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.WAV_FILE_EXTENSION;
                response.PlayFile(headingFile, null);
                response.PlayFile(AudioFilesUrlBase + "/jonacall/" + AudioFilesConsts.SHARON + "/Customer_call_recording-02B.wav", null);
                response.RecordStart();
            }
            response.BridgeTo(advertiserCallId, returnAddress,
                new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME)
            );
            return response;
        }

        private FoneApiWrapper.CallBacks.CallBackResponse CallAnsweredAdvertiser(FapiArgs args)
        {
            CallBackResponse response = new CallBackResponse(LimitCallDurationSeconds);
            if (args[_FAPI_PLAY_TTS] == true.ToString())
            {
                response.PlayAndGetDtmf(AudioFilesUrlBase + "/jonacall/" + PromptsClass + "/welcome_pretext.wav", returnAddress, 1, "#", 0,
                    new CustomParam(_FAPI_DTMFIDX, 0.ToString()),
                    new CustomParam(_FAPI_SIDE, _ADVERTISER),
                    new CustomParam(_FAPI_INCIDENT_ID, args[_FAPI_INCIDENT_ID]),
                    new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, args[_FAPI_INCIDENT_ACCOUNT_ID]),
                    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                    new CustomParam(_FAPI_CUSTOMER_PHONE_NORMALIZED, args[_FAPI_CUSTOMER_PHONE_NORMALIZED]),
                    new CustomParam(_FAPI_PLAY_TTS, args[_FAPI_PLAY_TTS]),
                    new CustomParam(_FAPI_TTS_URL, args[_FAPI_TTS_URL]),
                    new CustomParam(_FAPI_HEADING_CODE, args[_FAPI_HEADING_CODE])
                    );
            }
            else
            {
                response.PlayAndGetDtmf(AudioFilesUrlBase + "/jonacall/" + PromptsClass + "/welcome.wav", returnAddress, 1, "#", 20,
                    new CustomParam(_FAPI_DTMFIDX, 2.ToString()),
                    new CustomParam(_FAPI_SIDE, _ADVERTISER),
                    new CustomParam(_FAPI_INCIDENT_ID, args[_FAPI_INCIDENT_ID]),
                    new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, args[_FAPI_INCIDENT_ACCOUNT_ID]),
                    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                    new CustomParam(_FAPI_CUSTOMER_PHONE_NORMALIZED, args[_FAPI_CUSTOMER_PHONE_NORMALIZED]),
                    new CustomParam(_FAPI_PLAY_TTS, args[_FAPI_PLAY_TTS]),
                    new CustomParam(_FAPI_HEADING_CODE, args[_FAPI_HEADING_CODE])
                    );
            }
            return response;
        }

        private void DialToCustomer(string customerPhoneNormalized, string incidentId, string incidentAccountId, string advertiserCallId, string headingCode, CallBackResponse response)
        {
            SetCallToInConversation(incidentAccountId, incidentId);
            FoneApiClient client = new FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
            var call = client.Dial(FoneApiWrapper.Commands.eDialDestinationType.number,
                  customerPhoneNormalized,
                  returnAddress,
                  appId,
                  returnAddress,
                  _defaultDialTimeOut,
                  MakeUpCallerIdForCustomer(customerPhoneNormalized),
                  null,
                  null,
                  Trunk,
                  advertiserCallId,
                  new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME),
                  new CustomParam(_FAPI_SIDE, _CUSTOMER),
                  new CustomParam(_FAPI_INCIDENT_ID, incidentId),
                  new CustomParam(_FAPI_INCIDENT_ACCOUNT_ID, incidentAccountId),
                  new CustomParam(_FAPI_ADVERTISERCALLID, advertiserCallId),
                  new CustomParam(_FAPI_CUSTOMER_PHONE_NORMALIZED, customerPhoneNormalized),
                  new CustomParam(_FAPI_HEADING_CODE, headingCode)
            );
            if (call.status != 0)
            {
                response.Hangup();
                ContinueProcessThread continueThread = new ContinueProcessThread(
                        new Guid(incidentId),
                        new Guid(incidentAccountId),
                        ContinueProcessThread.eEndStatus.NotOkBlameCustomer,
                        0.ToString(),
                        null);
                continueThread.Start();
                var container = containersHolder[advertiserCallId];
                container.ClosedAndProcessContinued = true;
            }
            else
            {
                JonaContainer container = containersHolder[advertiserCallId];
                container.CustomerCallId = call.callId;
                response.PlayFile(AudioFilesUrlBase + AudioFilesConsts.FAKE_RING, returnAddress,
                    new CustomParam(FoneApiRouter.FAPI_NPHANDLER, _HANDLERNAME)
                  );
            }
        }

        private void SetCallToInConversation(string incidentAccountId, string incidentId)
        {
            CallStatusChangeHandler handler = new CallStatusChangeHandler(new Guid(incidentAccountId), new Guid(incidentId), CallStatusChangeHandler.eStatus.callingCustomer);
            handler.Start();
        }

        private void HangupAdvertiser(FapiArgs args)
        {
            string advertiserCallId = args[ArgsTypes.FAPI_CALL_ID];
            string duration = args[ArgsTypes.FAPI_CALL_DURATION];
            string hangupStatus = args[ArgsTypes.FAPI_HANGUP_CAUSE];
            CreateCDR(hangupStatus, advertiserCallId, duration, args[_FAPI_ADVERTISER_PHONE], "jona advertiser side", NoProblem.Core.DataModel.Xrm.new_calldetailrecord.Type.jona_advertiser, new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]));
            JonaContainer container = containersHolder[advertiserCallId];
            string customerCallId = container.CustomerCallId;
            if (!string.IsNullOrEmpty(customerCallId))
            {
                FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
                client.Hangup(customerCallId);
            }
            else if (duration == 0.ToString())
            {
                ContinueProcessThread tr = new ContinueProcessThread(
                    new Guid(args[_FAPI_INCIDENT_ID]),
                    new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]),
                    ContinueProcessThread.eEndStatus.NotOkBlameAdvertiser,
                    duration,
                    null);
                tr.Start();
            }
            else//check that the process is really going forward in some other thread, if not, continue here. sometimes when the call is very short (2 seconds) the "dtmf complete" event is not fired so this takes care of this edge case.
            {
                int counter = 0;
                while (!container.ClosedAndProcessContinued && counter < 3)
                {
                    System.Threading.Thread.Sleep(1000);
                    counter++;
                }
                if (!container.ClosedAndProcessContinued)
                {
                    ContinueProcessThread tr = new ContinueProcessThread(
                    new Guid(args[_FAPI_INCIDENT_ID]),
                    new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]),
                    ContinueProcessThread.eEndStatus.NotOkBlameAdvertiser,
                    duration,
                    null);
                    tr.Start();
                }
            }
            containersHolder.Remove(advertiserCallId);
        }

        private void HangupCustomer(FapiArgs args)
        {
            Guid incidentId = new Guid(args[_FAPI_INCIDENT_ID]);
            Guid incAccId = new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]);
            string callDuration = args[ArgsTypes.FAPI_CALL_DURATION];
            string hangupStatus = args[ArgsTypes.FAPI_HANGUP_CAUSE];
            CreateCDR(hangupStatus, args[ArgsTypes.FAPI_CALL_ID], callDuration, args[_FAPI_CUSTOMER_PHONE_NORMALIZED], "jona customer side", NoProblem.Core.DataModel.Xrm.new_calldetailrecord.Type.jona_consumer, incAccId);
            if (callDuration == 0.ToString())
            {
                ContinueProcessThread tr = new ContinueProcessThread(
                    new Guid(args[_FAPI_INCIDENT_ID]),
                    new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]),
                    hangupStatus == FoneApiWrapper.CallBacks.HangupCauses.NORMAL_CLEARING ? ContinueProcessThread.eEndStatus.NotOkBlameAdvertiser : ContinueProcessThread.eEndStatus.NotOkBlameCustomer,
                    0.ToString(),
                    null);
                tr.Start();
                FoneApiWrapper.Commands.FoneApiClient client = new FoneApiWrapper.Commands.FoneApiClient(ApiUrlBase, FapiKey, FapiSecret);
                client.Hangup(args[_FAPI_ADVERTISERCALLID]);
            }
            else
            {
                string recordingUrl;
                args.TryGetValue(ArgsTypes.FAPI_RECORDING_URL, out recordingUrl);
                ContinueProcessThread tr = new ContinueProcessThread(
                    new Guid(args[_FAPI_INCIDENT_ID]),
                    new Guid(args[_FAPI_INCIDENT_ACCOUNT_ID]),
                    ContinueProcessThread.eEndStatus.CallOk,
                    callDuration,
                    recordingUrl);
                tr.Start();
            }
        }
    }
}
