﻿using System;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.JonaCall
{
    internal class ContinueProcessThread : Runnable
    {
        public ContinueProcessThread(Guid incidentId, Guid incidentAccountId, eEndStatus endStatus, string duration, string recording)
        {
            this.incidentId = incidentId;
            this.incidentAccountId = incidentAccountId;
            this.endStatus = endStatus;
            this.duration = duration;
            this.recording = recording;
            int.TryParse(duration, out durationInt);
        }

        Guid incidentId;
        Guid incidentAccountId;
        eEndStatus endStatus;
        string duration;
        string recording;
        int durationInt;

        private const int maxFaultsAllowedDefault = 1;
        private int MaxFaultsAllowed
        {
            get
            {
                DataAccessLayer.AsteriskConfiguration phonesConfig = new NoProblem.Core.DataAccessLayer.AsteriskConfiguration(XrmDataContext);
                int faults;
                if (!int.TryParse(phonesConfig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.MAX_FAULTS_ALLOWED_JONA), out faults))
                {
                    return maxFaultsAllowedDefault;
                }
                else if (faults < 0)
                {
                    return maxFaultsAllowedDefault;
                }
                else
                {
                    return faults;
                }
            }
        }

        private int? minCallDurationToCharge;
        private int MinCallDurationToCharge
        {
            get
            {
                if (!minCallDurationToCharge.HasValue)
                {
                    DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                    string minCallLengthStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.MINIMAL_CALL_LENGTH_TO_CHARGE);
                    int x;
                    int.TryParse(minCallLengthStr, out x);
                    minCallDurationToCharge = x;
                }
                return minCallDurationToCharge.Value;
            }
        }

        protected override void Run()
        {
            try
            {
                ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(XrmDataContext);
                DataAccessLayer.IncidentAccount incAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                var incAcc = incAccDal.Retrieve(incidentAccountId);
                DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                var incident = incidentDal.Retrieve(incidentId);
                bool sold = false;
                if (incident.casetypecode == (int)DataModel.Xrm.incident.CaseTypeCode.Video)
                {
                    if (recording != null)
                    {
                        incAccDal.UpdateRecordFileLocation(incidentAccountId, recording, durationInt);
                    }
                }
                else if (GlobalConfigurations.IsUsaSystem)
                {
                    
                    manager.CloseIncidentAccountAfterLeadBuyer(incAcc, incident, incidentDal, incAccDal, false);
                    sold = true;
                    if (recording != null)
                    {
                        incAccDal.UpdateRecordFileLocation(incidentAccountId, recording, durationInt);
                    }
                }
                else
                {
                    switch (endStatus)
                    {
                        case eEndStatus.CallOk:
                            {
                                bool isLengthOk = true;
                                if (MinCallDurationToCharge > 0 || incAcc.new_account_new_incidentaccount.new_mincallduration.HasValue)
                                {
                                    int dur;
                                    if (int.TryParse(duration, out dur))
                                    {
                                        if (dur < MinCallDurationToCharge || dur < (incAcc.new_account_new_incidentaccount.new_mincallduration.HasValue ? incAcc.new_account_new_incidentaccount.new_mincallduration.Value : -1))
                                        {
                                            isLengthOk = false;
                                        }
                                    }
                                }
                      //          DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                     //           var incident = incidentDal.Retrieve(incidentId);
                                if (isLengthOk)
                                {
                                    manager.CloseIncidentAccountAfterLeadBuyer(incAcc, incident, incidentDal, incAccDal, false);
                                    sold = true;
                                }
                                else
                                {
                                    sold = manager.InviteBySms(incAcc, incident, incidentDal, incAccDal);
                                }
                                if (recording != null)
                                {
                                    incAccDal.UpdateRecordFileLocation(incidentAccountId, recording, durationInt);
                                }
                            }
                            break;
                        case eEndStatus.Rejected:
                            {
                                manager.RejectCall(incAccDal, incAcc);
                            }
                            break;
                        case eEndStatus.NotOkBlameAdvertiser:
                            {
                                bool retry = incAcc.new_advertiserfaults < MaxFaultsAllowed;
                                if (retry)
                                {
                                    incAcc.new_advertiserfaults = incAcc.new_advertiserfaults + 1;
                                    incAccDal.Update(incAcc);
                                    XrmDataContext.SaveChanges();
                                    RetryCall(incAcc.new_incidentaccountid);
                                    return;
                                }
                                sold = InviteBySms(manager, incAccDal, incAcc);
                            }
                            break;
                        case eEndStatus.NotOkBlameCustomer:
                            {
                      //          DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                      //          var incident = incidentDal.Retrieve(incidentId);
                                bool retry = incident.new_customerfaults < MaxFaultsAllowed;
                                if (retry)
                                {
                                    incident.new_customerfaults = incident.new_customerfaults + 1;
                                    incidentDal.Update(incident);
                                    XrmDataContext.SaveChanges();
                                    RetryCall(incAcc.new_incidentaccountid);
                                }
                                else//bad customer, stop all
                                {
                                    incAcc.new_lostoncustomersfault = true;
                                    manager.MarkIncidentAccountAsLostAfterLeadBuyerFailed(incAccDal, incAcc);
                                    manager.StopAll(incident, incidentDal, true);
                                }
                            }
                            return;
                    }
                }
                manager.ContinueRequestAfterBrokerCall(sold, incidentId, false);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in JonaCall.ContinueProcessThread.Run. Attempting to continue process...");
                try
                {
                    ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(XrmDataContext);
                    manager.ContinueRequestAfterBrokerCall(false, incidentId, false);
                }
                catch (Exception exc2)
                {
                    LogUtils.MyHandle.HandleException(exc2, "Exception in JonaCall.ContinueProcessThread.Run after attempting to continue the process.");
                }
            }
        }

        private bool InviteBySms(ServiceRequest.ServiceRequestManager manager, DataAccessLayer.IncidentAccount incAccDal, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc)
        {
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentId);
            bool sold = manager.InviteBySms(incAcc, incident, incidentDal, incAccDal);
            return sold;
        }

        private void RetryCall(Guid incAccId)
        {
            JonaCallRetryHolder.Instance.AddRetry(incAccId);
        }

        internal enum eEndStatus
        {
            Rejected,
            NotOkBlameAdvertiser,
            NotOkBlameCustomer,
            CallOk
        }
    }
}
