﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.JonaCall
{
    internal class RetryContainer
    {
        public Timer Timer { get; set; }
        public Guid IncidentAccountId { get; set; }
    }
}
