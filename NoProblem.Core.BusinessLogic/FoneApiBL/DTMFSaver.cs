﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.FoneApiBL
{
    class DTMFSaver : Runnable
    {
        string dtmf;
        string incidentAccountId;

        public DTMFSaver(string dtmf, string incidentAccountId)
        {
            this.dtmf = dtmf;
            this.incidentAccountId = incidentAccountId;
        }

        protected override void Run()
        {
            DataAccessLayer.IncidentAccount dal = new DataAccessLayer.IncidentAccount(XrmDataContext);
            DataModel.Xrm.new_incidentaccount ia = new DataModel.Xrm.new_incidentaccount(XrmDataContext);
            ia.new_dtmfpressed = dtmf;
            ia.new_incidentaccountid = new Guid(incidentAccountId);
            dal.Update(ia);
            XrmDataContext.SaveChanges();
        }
    }
}
