﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoneApiWrapper.CallBacks;

namespace NoProblem.Core.BusinessLogic.FoneApiBL
{
    interface IFoneApiHandler
    {
        FoneApiWrapper.CallBacks.CallBackResponse HandleCallBack(FapiArgs args);
    }
}
