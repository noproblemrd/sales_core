﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Audit;

namespace NoProblem.Core.BusinessLogic.Audit
{
    public class AuditManager : XrmUserBase
    {
        #region Ctor
        public AuditManager()
            : base(null)
        {

        }
        #endregion

        #region Delegates

        private delegate void CreateAuditEntriesDelegate(List<DML.Audit.AuditEntry> auditEntries); 

        #endregion

        #region Public Methods

        public void CreateAudit(List<DML.Audit.AuditEntry> auditEntries)
        {
            CreateAuditEntriesDelegate createAsync = new CreateAuditEntriesDelegate(CreateAuditEntries);
            createAsync.BeginInvoke(auditEntries, null, null);
        }

        public AuditReportResponse AuditReport(AuditReportRequest request)
        {
            AuditReportResponse response = new AuditReportResponse();
            DateTime fromDate = request.FromDate;
            DateTime toDate = request.ToDate;
            FixDatesToUtcFullDays(ref fromDate, ref toDate);
            DataAccessLayer.AuditEntry auditDAL = new NoProblem.Core.DataAccessLayer.AuditEntry(XrmDataContext);
            IEnumerable<DML.Xrm.new_auditentry> entries = auditDAL.GetAuditEntriesByDatesAndSupplier(fromDate, toDate, request.SupplierId);
            if (string.IsNullOrEmpty(request.PageId) == false)
            {
                entries = entries.Where(ent => ent.new_pageid == request.PageId);
            }
            List<DML.Audit.AuditEntry> entriesList = new List<AuditEntry>(entries.Count());
            foreach (DML.Xrm.new_auditentry entry in entries)
            {
                AuditEntry data = new AuditEntry();
                data.AdvertiseId = entry.new_advertiserid.HasValue ? entry.new_advertiserid.Value : Guid.Empty;
                data.AdvertiserName = entry.new_advertisername;
                data.AuditStatus = (DML.Xrm.new_auditentry.Status)entry.statuscode.Value;
                data.FieldId = entry.new_fieldid;
                data.FieldName = entry.new_fieldname;
                data.NewValue = entry.new_newvalue;
                data.OldValue = entry.new_oldvalue;
                data.PageId = entry.new_pageid;
                data.UserId = entry.new_userid.HasValue ? entry.new_userid.Value : Guid.Empty;
                data.UserName = entry.new_username;
                data.CreatedOn = FixDateToLocal(entry.createdon.Value);
                data.PageName = entry.new_pagename;
                entriesList.Add(data);
            }
            response.AuditEntries = entriesList;
            return response;
        }

        public void LogFlavorUsability(LogFlavorUsabilityRequest request)
        {
            DataModel.Xrm.new_flavorusabilitylog log = new NoProblem.Core.DataModel.Xrm.new_flavorusabilitylog();
            log.new_flavor = request.Flavor;
            log.new_action = request.Action;
            DataAccessLayer.FlavorUsabilityLog dal = new NoProblem.Core.DataAccessLayer.FlavorUsabilityLog(XrmDataContext);
            dal.Create(log);
            XrmDataContext.SaveChanges();
        }

        #endregion

        #region Private Methods

        private void CreateAuditEntries(List<DML.Audit.AuditEntry> auditEntries)
        {
            try
            {
                if (auditEntries != null)
                {
                    DataAccessLayer.AuditEntry auditDAL = new NoProblem.Core.DataAccessLayer.AuditEntry(XrmDataContext);
                    foreach (DML.Audit.AuditEntry entry in auditEntries)
                    {
                        try
                        {
                            CreateEntry(entry, auditDAL);
                        }
                        catch (Exception exc)
                        {
                            LogUtils.MyHandle.HandleException(exc, "Exception creating audit entry");
                        }
                    }
                    XrmDataContext.SaveChanges();
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception saving the audit entries");
            }
        } 

        private void CreateEntry(DML.Audit.AuditEntry entry, DataAccessLayer.AuditEntry auditDAL)
        {
            DML.Xrm.new_auditentry audit = new NoProblem.Core.DataModel.Xrm.new_auditentry();
            audit.statuscode = (int)entry.AuditStatus;
            if (entry.UserId != Guid.Empty)
            {
                audit.new_userid = entry.UserId;
            }
            audit.new_username = entry.UserName;
            audit.new_advertiserid = entry.AdvertiseId;
            audit.new_advertisername = entry.AdvertiserName;
            audit.new_fieldid = entry.FieldId;
            audit.new_fieldname = entry.FieldName;
            audit.new_pageid = entry.PageId;
            audit.new_oldvalue = entry.OldValue;
            audit.new_newvalue = entry.NewValue;
            audit.new_pagename = entry.PageName;
            auditDAL.Create(audit);
        } 

        #endregion
    }
}
