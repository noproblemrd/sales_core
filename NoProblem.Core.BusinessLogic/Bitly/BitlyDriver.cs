﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Bitly
{
    internal class BitlyDriver //: XrmUserBase
    {
        #region static
        private const string baseUrl = "https://api-ssl.bitly.com";
        private const string shortenResource = "v3/shorten";
        private readonly static string _apiToken;
        static BitlyDriver()
        {
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            _apiToken = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.BITLY_API_TOKEN);
        }
        #endregion
        /*
        public BitlyDriver(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {

        }
         * */
        public BitlyDriver() { }
        
        string ApiToken
        {
            get
            {
                /*
                if (string.IsNullOrEmpty(_apiToken))
                {
                    DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                    _apiToken = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.BITLY_API_TOKEN);
                }
                 * */
                return _apiToken;
            }
        }

        public string ShortenUrl(string longUrl)
        {
            string shortUrl = null;
            try
            {
                RestSharp.RestClient client = new RestSharp.RestClient(baseUrl);
                RestSharp.RestRequest request = new RestSharp.RestRequest(shortenResource, RestSharp.Method.GET);
                request.Parameters.Add(new RestSharp.Parameter() { Name = "access_token", Value = ApiToken, Type = RestSharp.ParameterType.GetOrPost });
                request.Parameters.Add(new RestSharp.Parameter() { Name = "longUrl", Value = System.Web.HttpUtility.HtmlEncode(longUrl), Type = RestSharp.ParameterType.GetOrPost });
                RestSharp.RestResponse response = (RestSharp.RestResponse)client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    ShortenResponse shortenResponse = null;
                    try
                    {
                        shortenResponse = JsonConvert.DeserializeObject<ShortenResponse>(response.Content);
                    }
                    catch (Exception)
                    {
                        var shortenResponseOnError = JsonConvert.DeserializeObject<ShortenReponseOnError>(response.Content);
                        shortenResponse = new ShortenResponse();
                        shortenResponse.StatusCode = shortenResponseOnError.StatusCode;
                        shortenResponse.StatusTxt = shortenResponseOnError.StatusTxt;
                    }
                    if (shortenResponse.StatusCode == 200)
                    {
                        shortUrl = shortenResponse.Data.Url;
                    }
                    else
                    {
                        LogUtils.MyHandle.WriteToLog("Unable to shorten url with bitly. statusCode: {0}, statusTxt: {1}", shortenResponse.StatusCode, shortenResponse.StatusTxt);
                    }
                }
                else
                {
                    LogUtils.MyHandle.WriteToLog("Unable to shorten url with bitly. HTTP protocol error.  httpStatusCode: {0}, content: {1}", response.StatusCode, response.Content);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc,"Exception shortening url with bitly. longUrl: {0}", longUrl);
            }
            return shortUrl;
        }
    }
}
