﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace NoProblem.Core.BusinessLogic.Bitly
{
    [JsonObject]
    internal class ShortenResponse
    {
        [JsonProperty("data")]
        public ShortenResponseData Data { get; set; }
        [JsonProperty("status_code")]
        public int StatusCode { get; set; }
        [JsonProperty("status_txt")]
        public string StatusTxt { get; set; }
    }

    [JsonObject]
    internal class ShortenReponseOnError
    {
        [JsonProperty("status_code")]
        public int StatusCode { get; set; }
        [JsonProperty("status_txt")]
        public string StatusTxt { get; set; }
    }

    internal class ShortenResponseData
    {
        [JsonProperty("long_url")]
        public string LongUrl { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("hash")]
        public string Hash { get; set; }
        [JsonProperty("new_hash")]
        public int NewHash { get; set; }
        [JsonProperty("global_hash")]
        public string GlobalHash { get; set; }
    }
}
