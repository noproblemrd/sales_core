﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using NoProblem.Core.DataModel.ReportsWizard;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ReportsWizard.Creators
{
    internal class AdvertisersReportCreator : WizardCreatorsBase
    {
        #region Ctor
        internal AdvertisersReportCreator(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal override List<List<string>> Create(WizardReportRequest request)
        {
            StringBuilder select = new StringBuilder("select distinct acc.accountid ");
            foreach (var col in request.Columns)
            {
                select.Append(",");
                select.Append(ColumnToString(col, false));
            }
            select.Append(" ");
            StringBuilder from = new StringBuilder(" from account acc ");
            StringBuilder where = new StringBuilder(" where acc.deletionstatecode = 0 and acc.statecode = 0 and (acc.new_securitylevel is null or acc.new_securitylevel = 0) and acc.new_affiliateoriginid is null ");
            int regionsCount = 0;
            Guid regionId = Guid.Empty;
            bool regionEquals = false;
            foreach (var fil in request.Filters)
            {
                AddFilterTextToQuery(from, where, ref regionId, fil, ref regionsCount, ref regionEquals);
            }
            if (string.IsNullOrEmpty(request.OrderBy) == false)
            {
                InsertOrderBy(request.OrderBy, request.Descending, where, ColumnToString);
            }
            DataAccessLayer.ReportsWizardDal dal = new NoProblem.Core.DataAccessLayer.ReportsWizardDal();
            string query = select.ToString() + from.ToString() + where.ToString();
            DataTable table = dal.ExecuteSql(query);

            if (regionId != Guid.Empty)
            {
                RemoveSuppliersByRegion(regionId, table, regionEquals);
            }
            if (regionsCount > 1)
            {
                return new List<List<string>>();
            }
            string groupBy = string.Empty;
            if (string.IsNullOrEmpty(request.GroupBy) == false)
            {
                groupBy = ColumnToString(request.GroupBy, true);
            }
            List<List<string>> data = CreateReturnValue(request.Columns.Count, table, groupBy);
            return data;
        }

        #endregion

        #region Private Methods

        private void AddFilterTextToQuery(StringBuilder from, StringBuilder where, ref Guid regionId, DML.ReportsWizard.Filter fil, ref int regionsCount, ref bool regionEquals)
        {
            bool skip = false;
            bool closeParentesis = false;
            where.Append(" and ");
            switch (fil.FilterField)
            {
                case "Balance":
                    where.Append(" acc.new_cashbalance ");
                    break;
                case "CreatedOn":
                    if (fil.Operator == "NotEquals")
                    {
                        DoNotEqualsDateFilter(where, fil, ref skip, "acc.createdon");
                    }
                    else
                    {
                        where.Append(" acc.createdon ");
                    }
                    break;
                case "UnavailableFrom":
                    if (fil.Operator == "NotEquals")
                    {
                        DoNotEqualsDateFilter(where, fil, ref skip, "acc.new_unavailablefrom");
                    }
                    else
                    {
                        where.Append(" (acc.New_unavailableto > GETUTCDATE() or acc.New_unavailableto is null) and acc.new_unavailablefrom ");
                    }
                    break;
                case "UnavailableTo":
                    if (fil.Operator == "NotEquals")
                    {
                        DoNotEqualsDateFilter(where, fil, ref skip, "acc.new_unavailableto");
                    }
                    else
                    {
                        where.Append(" (acc.New_unavailableto > GETUTCDATE() or acc.New_unavailableto is null) and acc.new_unavailableto ");
                    }
                    break;
                case "Heading":
                    from.Append(" join new_accountexpertise ace with (nolock) on ace.new_accountid = acc.accountid and ace.deletionstatecode = 0 and ace.statecode = 0");
                    where.Append(" ace.new_primaryexpertiseidname ");
                    break;
                case "NumberOfCalls":
                    where.Append(" (select count(*) from new_incidentaccount with (nolock) where new_accountid = acc.accountid) ");
                    break;
                case "Region":
                    regionId = GetRegionId((string)fil.Value);
                    where.Remove(where.Length - 5, 4);
                    if (fil.Operator == "Equals")
                    {
                        regionEquals = true;
                    }
                    else
                    {
                        regionEquals = false;
                    }
                    skip = true;
                    regionsCount++;
                    break;
                case "AdvertiserStatus":
                    HandleAdvertiserStatus(fil, where);
                    skip = true;
                    break;
                case "AccountManager":
                    if (fil.Operator == "NotEquals")
                    {
                        where.Append(" (acc.new_manageridname is null or acc.new_manageridname ");
                        closeParentesis = true;
                    }
                    else
                    {
                        where.Append(" acc.new_manageridname ");
                    }
                    break;
                case "UnavailabilityReason":
                    where.Append(" (acc.New_unavailableto > GETUTCDATE() or acc.New_unavailableto is null) and acc.new_unavailabilityreasonid ");
                    break;
            }
            if (!skip)
            {
                where.Append(GetOperatorSymbol(fil.Operator, fil.Value));
                where.Append(" ");
                where.Append(GetValueString(fil.Value, fil.Operator));
                where.Append(" ");
            }
            if (closeParentesis)
            {
                where.Append(" ) ");
            }
        }

        private void HandleAdvertiserStatus(Filter fil, StringBuilder where)
        {
            if (fil.Operator == "NotEquals")
            {
                where.Append(" not( ");
            }
            else
            {
                where.Append(" ( ");
            }
            if ((string)fil.Value == "Candidate")
            {
                where.Append(" acc.new_status = 2 ");
            }
            else if ((string)fil.Value == "Inactive")
            {
                where.Append(" acc.new_status = 3 ");
            }
            else if ((string)fil.Value == "FreshlyImported")
            {
                where.Append(" acc.new_status = 4 and acc.new_trialstatusreason = 1 ");
            }
            else if ((string)fil.Value == "Machine")
            {
                where.Append(" acc.new_status = 4 and acc.new_trialstatusreason = 2 ");
            }
            else if ((string)fil.Value == "Accessible")
            {
                where.Append(" acc.new_status = 4 and acc.new_trialstatusreason = 3 ");
            }
            else if ((string)fil.Value == "ITC")
            {
                where.Append(" acc.new_status = 4 and acc.new_trialstatusreason = 4 ");
            }
            else if ((string)fil.Value == "RemoveMe")
            {
                where.Append(" acc.new_status = 4 and acc.new_trialstatusreason = 5 ");
            }
            else if ((string)fil.Value == "Expired")
            {
                where.Append(" acc.new_status = 4 and acc.new_trialstatusreason = 6 ");
            }
            else if ((string)fil.Value == "InRegistration")
            {
                where.Append(" acc.new_status = 4 and acc.new_trialstatusreason = 7 ");
            }
            else if ((string)fil.Value == "DoNotCallList")
            {
                where.Append(" acc.new_status = 4 and acc.new_trialstatusreason = 8 ");
            }
            else
            {
                where.Append(@" acc.new_status = 1 ");//active
                if ((string)fil.Value == "Available")
                {
                    where.Append(@" and
               (acc.New_unavailabilityreasonId is null or
                (acc.New_unavailableto is not null and acc.New_unavailableto < GETUTCDATE())
                or (acc.new_unavailablefrom is not null and acc.New_unavailablefrom > GETUTCDATE())) ");
                }
                else//unavailable
                {
                    where.Append(@" and 
                        not((acc.New_unavailabilityreasonId is null or
                (acc.New_unavailableto is not null and acc.New_unavailableto < GETUTCDATE())
                or (acc.new_unavailablefrom is not null and acc.New_unavailablefrom > GETUTCDATE()))) ");
                }
            }
            where.Append(" ) ");
        }

        private Guid GetRegionId(string regionName)
        {
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DML.Xrm.new_region region = regionDal.GetRegionByName(regionName);
            return region.new_regionid;
        }

        private Guid RemoveSuppliersByRegion(Guid regionId, DataTable table, bool equals)
        {
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.RegionAccount regionAccountDal = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
            Regions.RegionsManager regionsManager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(XrmDataContext);
            DML.SqlHelper.RegionMinData region = regionDal.GetRegionMinDataById(regionId);
            IEnumerable<DML.SqlHelper.RegionMinData> parents = regionDal.GetParents(region);
            for (int i = table.Rows.Count - 1; i >= 0; i--)
            {
                var row = table.Rows[i];
                Guid accountId = (Guid)row[0];
                DML.Xrm.account supplier = accountRepositoryDal.Retrieve(accountId);
                bool works = regionsManager.IsSupplierWorksInRegion(supplier, region, parents, regionAccountDal);
                if ((equals && !works) || (!equals && works))
                {
                    table.Rows.Remove(row);
                }
            }
            return regionId;
        }

        private string ColumnToString(string column, bool isOrderBy)
        {
            switch (column)
            {
                case "Balance":
                    return isOrderBy ? "new_cashbalance" : "acc.new_cashbalance";
                case "CompanyId":
                    return isOrderBy ? "accountnumber" : "acc.accountnumber";
                case "CompanyName":
                    return isOrderBy ? "name" : "acc.name, acc.accountid";
                case "ContactPersonName":
                    return isOrderBy ? "fullname" : "acc.new_firstname + ' ' + acc.new_lastname as fullname";
                case "Email":
                    return isOrderBy ? "emailaddress1" : "acc.emailaddress1";
                case "MainPhone":
                    return isOrderBy ? "telephone1" : "acc.telephone1";
                case "Status":
                    if (isOrderBy == false)
                    {
                        return @" case acc.new_status when 2 then 'Candidate' when 3 then 'Inactive' 
                                    when 1 then 
                                   (case when 
                                    (acc.New_unavailabilityreasonId is null or
                                    (acc.New_unavailableto is not null and acc.New_unavailableto < GETUTCDATE())
                                    or (acc.new_unavailablefrom is not null and acc.New_unavailablefrom > GETUTCDATE()))
                                    then 'Available' else 'Unavailable' end)
                                else 
                                    (case acc.new_trialstatusreason when 1 then 'Freshly Imported'
                                        when 2 then 'Machine' when 3 then 'Accesible' when 4 then 'ITC'
                                        when 5 then 'Remove Me' when 6 then 'Expired' when 7 then 'In Registration'
                                        when 8 then 'Do Not Call List' else '' end)
                                end as Status";
                    }
                    else
                    {
                        return "Status";
                    }
                case "AccountManager":
                    return isOrderBy ? "new_manageridname" : "acc.new_manageridname";
                case "UnavailabilityReason":
                    return isOrderBy ? "new_unavailabilityreasonidname" : "acc.new_unavailabilityreasonidname";
                case "UnavailableFrom":
                    return isOrderBy ? "new_unavailablefrom" : " case when (acc.New_unavailableto > GETUTCDATE() or acc.New_unavailableto is null) then acc.new_unavailablefrom else null end as new_unavailablefrom ";
                case "UnavailableTo":
                    return isOrderBy ? "new_unavailableto" : " case when (acc.New_unavailableto > GETUTCDATE() or acc.New_unavailableto is null) then acc.new_unavailableto else null end as new_unavailableto ";
                case "Custom2":
                    return isOrderBy ? "new_custom2" : "acc.new_custom2";
                case "IncidentPrice":
                    return isOrderBy ? "'IncidentPrice'" : @"(select cast(cast(new_incidentprice as int) as nvarchar(10)) + ', ' as 'data()' 
                            from new_accountexpertise
                            where deletionstatecode = 0 and statecode = 0
                            and new_accountid = accountid
                            order by new_primaryexpertiseidname
                            for xml path('')) as 'IncidentPrice'";
                case "Heading":
                    return isOrderBy ? "'Heading'" : @"(select new_primaryexpertiseidname + ', ' as 'data()' 
                            from new_accountexpertise
                            where deletionstatecode = 0 and statecode = 0
                            and new_accountid = accountid
                            order by new_primaryexpertiseidname
                            for xml path('')) as 'Heading'";
                case "VirtualNumber":
                    return isOrderBy ? "'VirtualNumber'" : @"(select dn.new_number + ', ' as 'data()' 
                            from new_accountexpertise ae
                            join new_directnumber dn on ae.new_accountexpertiseid = dn.new_accountexpertiseid
                            where ae.deletionstatecode = 0 and ae.statecode = 0
                            and ae.new_accountid = accountid
                            order by ae.new_primaryexpertiseidname
                            for xml path('')) as 'VirtualNumber'";
            }
            return "";
        }

        #endregion
    }
}
