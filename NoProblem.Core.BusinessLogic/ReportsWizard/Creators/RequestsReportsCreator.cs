﻿using System.Collections.Generic;
using System.Data;
using System.Text;
using NoProblem.Core.DataModel.ReportsWizard;

namespace NoProblem.Core.BusinessLogic.ReportsWizard.Creators
{
    internal class RequestsReportsCreator : WizardCreatorsBase
    {
        #region Ctor
        internal RequestsReportsCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal override List<List<string>> Create(WizardReportRequest request)
        {
            StringBuilder select = new StringBuilder("select inc.incidentid ");
            foreach (var col in request.Columns)
            {
                select.Append(",");
                select.Append(ColumnToString(col, false));
            }
            select.Append(" ");
            StringBuilder from = new StringBuilder(" from incident inc with (nolock) ");
            StringBuilder where = new StringBuilder(" where inc.deletionstatecode = 0 and inc.statecode = 0");
            foreach (var fil in request.Filters)
            {
                AddFilterTextToQuery(where, fil);
            }
            if (string.IsNullOrEmpty(request.OrderBy) == false)
            {
                InsertOrderBy(request.OrderBy, request.Descending, where, ColumnToString);
            }
            DataAccessLayer.ReportsWizardDal dal = new NoProblem.Core.DataAccessLayer.ReportsWizardDal();
            string query = select.ToString() + from.ToString() + where.ToString();
            DataTable table = dal.ExecuteSql(query);
            string groupBy = string.Empty;
            if (string.IsNullOrEmpty(request.GroupBy) == false)
            {
                groupBy = ColumnToString(request.GroupBy, true);
            }
            List<List<string>> data = CreateReturnValue(request.Columns.Count, table, groupBy);
            return data;
        }

        #endregion

        #region Private Methods

        private void AddFilterTextToQuery(StringBuilder where, NoProblem.Core.DataModel.ReportsWizard.Filter fil)
        {
            bool skip = false;
            where.Append(" and ");
            switch (fil.FilterField)
            {
                case "CreatedOn":
                    if (fil.Operator == "NotEquals")
                    {
                        DoNotEqualsDateFilter(where, fil, ref skip, "inc.createdon");
                    }
                    else
                    {
                        where.Append(" inc.createdon ");
                    }
                    break;
                case "Heading":
                    where.Append(" inc.new_primaryexpertiseidname ");
                    break;
                case "NumOfPayingAdvertisers":
                    where.Append(@" ( select count(*) from
	                                new_incidentaccount with (nolock) where
	                                new_incidentid = inc.incidentid
                                    and deletionstatecode = 0
	                                and (statuscode = 10 or statuscode = 12 or statuscode = 13) ) ");
                    break;
                case "NumOfRequestedAdvertisers":
                    where.Append(" inc.new_requiredaccountno ");
                    break;
                case "Region":
                    where.Append(" inc.new_regionidname ");
                    break;
                case "Revenue":
                    where.Append(@" ( select sum(new_potentialincidentprice) from
	                                    new_incidentaccount inac with (nolock) where
	                                    inac.new_incidentid = inc.incidentid
                                        and inac.deletionstatecode = 0
	                                    and (inac.statuscode = 10 or inac.statuscode = 12 or inac.statuscode = 13) ) ");
                    break;
                case "PageName":
                    where.Append(" inc.new_pagename ");
                    break;
                case "PlaceInWebSite":
                    where.Append(" inc.new_placeinwebsite ");
                    break;
                case "ControlName":
                    where.Append(" inc.new_controlname ");
                    break;
                case "Domain":
                    where.Append(" SUBSTRING(inc.new_url, 0, CHARINDEX('/', inc.new_url, CHARINDEX('/', inc.new_url, CHARINDEX('/', inc.new_url)+1)+1)) ");
                    break;    
                case "Url":
                    where.Append(" inc.new_url ");
                    break;
                case "Keyword":
                    where.Append(" inc.new_keyword ");
                    break;
            }
            if (!skip)
            {
                where.Append(GetOperatorSymbol(fil.Operator, fil.Value));
                where.Append(" ");
                where.Append(GetValueString(fil.Value, fil.Operator));
                where.Append(" ");
            }
        }

        private string ColumnToString(string column, bool isOrderBy)
        {
            switch (column)
            {
                case "Keyword":
                    return isOrderBy ? "new_keyword" : "inc.new_keyword";
                case "PageName":
                    return isOrderBy ? "new_pagename" : "inc.new_pagename";
                case "PlaceInWebSite":
                    return isOrderBy ? "new_placeinwebsite" : "inc.new_placeinwebsite";
                case "ControlName":
                    return isOrderBy ? "new_controlname" : "inc.new_controlname";
                case "Domain":
                    return isOrderBy ? "SUBSTRING(new_url, 0, CHARINDEX('/', new_url, CHARINDEX('/', new_url, CHARINDEX('/', new_url)+1)+1))" : "SUBSTRING(inc.new_url, 0, CHARINDEX('/', inc.new_url, CHARINDEX('/', inc.new_url, CHARINDEX('/', inc.new_url)+1)+1))";
                case "Url":
                    return isOrderBy ? "new_url" : "inc.new_url";
                case "ConsumerPhoneNumber":
                    return isOrderBy ? "customeridname" : "inc.customeridname, inc.customerid";
                case "CreatedOn":
                    return isOrderBy ? "createdon" : "inc.createdon";
                case "RequestId":
                    return isOrderBy ? "ticketnumber" : "inc.ticketnumber";
                case "NumOfPayingAdvertisers":
                    if (isOrderBy)
                    {
                        return " PayingAdvertisers ";
                    }
                    else
                    {
                        return @" ( select count(*) from new_incidentaccount inac with (nolock) where
	                            inac.new_incidentid = incidentid 
                                and inac.deletionstatecode = 0
	                            and (inac.statuscode = 10 or inac.statuscode = 12 or inac.statuscode = 13))
                                as PayingAdvertisers ";
                    }
                case "NumOfRequestedAdvertisers":
                    return isOrderBy ? "new_requiredaccountno" : "inc.new_requiredaccountno";
                case "Revenue":
                    if (isOrderBy)
                    {
                        return " Revenue ";
                    }
                    else
                    {
                        return @" ( select sum(new_potentialincidentprice) from new_incidentaccount inac with (nolock) where
	                            inac.new_incidentid = incidentid 
                                and inac.deletionstatecode = 0
	                            and (inac.statuscode = 10 or inac.statuscode = 12 or inac.statuscode = 13)) 
                                as Revenue ";
                    }
                case "Status":
                    if (isOrderBy)
                    {
                        return " Status ";
                    }
                    else
                    {
                        return @" case statuscode 
                                when 1 then 'Finished'
                                when 2 then 'In Progress'
                                when 200000 then 'New'
                                when 200002 then 'No Available Suppliers'
                                when 200013 then 'Black List'
                                when 200014 then 'Bad Word'
                                when 200015 then 'Invalid Consumer Phone'
                                when 200007 then 'Customer Not Available'
                                else 'Other'
                                end as Status ";
                    }
            }
            return "";
        }

        #endregion
    }
}
