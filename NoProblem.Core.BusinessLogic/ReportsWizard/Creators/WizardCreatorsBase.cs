﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NoProblem.Core.BusinessLogic.ReportsWizard.Creators
{
    internal abstract class WizardCreatorsBase : XrmUserBase
    {
        #region Ctor
        internal WizardCreatorsBase(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Abstract Methods

        internal abstract List<List<string>> Create(DataModel.ReportsWizard.WizardReportRequest request);

        #endregion

        #region Protected Methods

        protected void InsertOrderBy(string orderBy, bool descending, StringBuilder where, Func<string, bool, string> columnToStringFunc)
        {
            where.Append(" order by ");
            where.Append(columnToStringFunc(orderBy, true));
            if (descending)
            {
                where.Append(" desc ");
            }
            else
            {
                where.Append(" asc ");
            }
        }

        protected List<List<string>> CreateReturnValue(int columnsCount, DataTable table, string groupBy)
        {
            List<List<string>> data = null;
            if (string.IsNullOrEmpty(groupBy))
            {
                data = CreateNormalReturnValue(columnsCount, table);
            }
            else
            {
                data = CreateGroupByReturnValue(table, groupBy);
            }
            return data;
        }        

        protected string GetValueString(object value, string oper)
        {
            string retVal = string.Empty;
            if (value is DateTime)
            {
                DateTime date = (DateTime)value;
                if (oper == "StartAt")
                {
                    date = date.Date;
                    date = FixDateToUtcFromLocal(date);
                    retVal = date.ToString(DATETIMEFORMAT);
                }
                else if (oper == "EndBy")
                {
                    date = date.Date.AddDays(1);
                    date = FixDateToUtcFromLocal(date);
                    retVal = date.ToString(DATETIMEFORMAT);
                }
                else if (oper == "Equals")
                {
                    DateTime dateFrom = date;
                    DateTime dateTo = date;
                    FixDatesToUtcFullDays(ref dateFrom, ref dateTo);
                    retVal = dateFrom.ToString(DATETIMEFORMAT) + "' and '" + dateTo.ToString(DATETIMEFORMAT);
                }
                retVal = "'" + retVal + "'";
            }
            else if(value is bool)
            {
                bool b = (bool)value;
                retVal = (b ? "1" : "0");
                retVal = "'" + retVal + "'";
            }
            else
            {
                retVal = "N'" + value.ToString() + "'";
            }
            return retVal;
        }

        protected string GetOperatorSymbol(string oper, object value)
        {
            switch (oper)
            {
                case "BiggerThan":
                    return ">";
                case "EndBy":
                    return "<=";
                case "Equals":
                    if (value is DateTime)
                    {
                        return "between";
                    }
                    return "=";
                case "NotEquals":
                    return "!=";
                case "SmallerThan":
                    return "<";
                case "StartAt":
                    return ">=";
                case "Contains":
                    return "like";
            }
            return "=";
        }

        protected void DoNotEqualsDateFilter(StringBuilder where, DataModel.ReportsWizard.Filter fil, ref bool skip, string column)
        {
            skip = true;
            DateTime dateTo = (DateTime)fil.Value;
            DateTime dateFrom = dateTo;
            FixDatesToUtcFullDays(ref dateTo, ref dateFrom);
            where.Append(" ( ");
            where.Append(column);
            where.Append(" < '");
            where.Append(dateTo.ToString(DATETIMEFORMAT));
            where.Append("' or ");
            where.Append(column);
            where.Append(" > '");
            where.Append(dateFrom.ToString(DATETIMEFORMAT));
            where.Append("' ) ");
        }

        #endregion

        #region Private Methods

        private List<List<string>> CreateGroupByReturnValue(DataTable table, string groupBy)
        {
            List<List<string>> data = new List<List<string>>();
            var col = table.Columns[groupBy];
            int columnIndex = col.Ordinal;
            Type columnType = col.DataType;
            bool isClickable = table.Columns.Count == 3;
            int countIndex = isClickable ? 2 : 1;
            int one = 1;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                var row = table.Rows[i];
                string valueString = GetDataValueString(table, row, columnIndex, true);
                List<string> existing = data.FirstOrDefault(x => x[0] == valueString);
                if (existing == null)
                {
                    List<string> rowData = new List<string>();
                    rowData.Add(valueString);
                    if (isClickable)
                    {
                        rowData.Add(row[columnIndex + 1].ToString());
                    }
                    rowData.Add(one.ToString());                    
                    data.Add(rowData);
                }
                else
                {
                    int existingCount = int.Parse(existing[countIndex]);
                    existing[countIndex] = (existingCount + 1).ToString();
                }
            }
            data = data.OrderBy(x => x[0]).ToList();
            return data;
        }

        private List<List<string>> CreateNormalReturnValue(int columnsCount, DataTable table)
        {
            List<List<string>> data = new List<List<string>>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                List<string> rowData = new List<string>();
                var row = table.Rows[i];
                for (int j = 1; j < table.Columns.Count; j++)
                {
                    string valueString = GetDataValueString(table, row, j, false);
                    rowData.Add(valueString);
                }
                data.Add(rowData);
            }
            return data;
        }

        private string GetDataValueString(DataTable table, DataRow row, int j, bool isGroupBy)
        {
            string valueString = string.Empty;
            Type columnDataType = table.Columns[j].DataType;
            if (columnDataType == typeof(DateTime))
            {
                object scalar = row[j];
                if (scalar != DBNull.Value)
                {
                    DateTime date = (DateTime)scalar;
                    date = FixDateToLocal(date);
                    if (isGroupBy)
                    {
                        date = date.Date;
                        valueString = date.ToString(DATETIMEFORMATNOHOUR);
                    }
                    else
                    {
                        valueString = date.ToString(DATETIMEFORMAT);
                    }
                }
                else
                {
                    valueString = string.Empty;
                }
            }
            else if (columnDataType == typeof(decimal)
                || columnDataType == typeof(double)
                || columnDataType == typeof(float))
            {
                valueString = string.Format("{0:0.##}", row[j]);
            }
            else
            {
                valueString = row[j].ToString();
            }
            return valueString;
        }

        #endregion
    }
}
