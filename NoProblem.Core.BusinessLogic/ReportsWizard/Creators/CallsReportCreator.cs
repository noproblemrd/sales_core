﻿using System.Collections.Generic;
using System.Data;
using System.Text;
using System;

namespace NoProblem.Core.BusinessLogic.ReportsWizard.Creators
{
    internal class CallsReportCreator : WizardCreatorsBase
    {
        #region Ctor
        internal CallsReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal override List<List<string>> Create(DataModel.ReportsWizard.WizardReportRequest request)
        {
            StringBuilder select = new StringBuilder("select inac.new_incidentaccountid ");
            foreach (var col in request.Columns)
            {
                select.Append(",");
                select.Append(ColumnToString(col, false));
            }
            select.Append(" ");
            StringBuilder from = new StringBuilder(" from new_incidentaccount inac with (nolock) join incident inc with (nolock) on inc.incidentid = inac.new_incidentid and inc.deletionstatecode = 0 join contact cont with (nolock) on cont.contactid = inc.customerid and cont.deletionstatecode = 0 left join new_calldetailrecord cdr with(nolock) on cdr.new_incidentaccountid = inac.new_incidentaccountid ");
            StringBuilder where = new StringBuilder(" where inac.deletionstatecode = 0 and inac.statecode = 0");
            foreach (var fil in request.Filters)
            {
                AddFilterTextToQuery(where, fil);
            }
            if (string.IsNullOrEmpty(request.OrderBy) == false)
            {
                InsertOrderBy(request.OrderBy, request.Descending, where, ColumnToString);
            }
            DataAccessLayer.ReportsWizardDal dal = new NoProblem.Core.DataAccessLayer.ReportsWizardDal();
            string query = select.ToString() + from.ToString() + where.ToString();
            DataTable table = dal.ExecuteSql(query);
            string groupBy = string.Empty;
            if (string.IsNullOrEmpty(request.GroupBy) == false)
            {
                groupBy = ColumnToString(request.GroupBy, true);
            }
            List<List<string>> data = CreateReturnValue(request.Columns.Count, table, groupBy);
            return data;
        }



        #endregion

        #region Private Methods

        private void AddFilterTextToQuery(StringBuilder where, NoProblem.Core.DataModel.ReportsWizard.Filter fil)
        {
            where.Append(" and ");
            bool skip = false;
            switch (fil.FilterField)
            {
                case "Advertiser":
                    where.Append(" inac.new_accountidname ");
                    break;
                case "CreatedOn":
                    if (fil.Operator == "NotEquals")
                    {
                        DoNotEqualsDateFilter(where, fil, ref skip, "inac.createdon");
                    }
                    else
                    {
                        where.Append(" inac.createdon ");
                    }
                    break;
                case "Heading":
                    where.Append(" inc.new_primaryexpertiseidname ");
                    break;
                case "PricePerCall":
                    where.Append(" inac.new_potentialincidentprice ");
                    break;
                case "Region":
                    where.Append(" inc.new_regionidname ");
                    break;
                case "CallStatus":
                    DoStatusWhere(where, fil, ref skip);
                    break;
                case "PreDefinedPPC":
                    where.Append(" inac.new_predefinedprice ");
                    break;
                case "PriceChanged":
                    where.Append(" inac.new_isdefaultpricechanged ");
                    break;
                case "Duration":
                    where.Append(" cdr.new_type = 3  and cdr.new_duration ");
                    break;
            }
            if (!skip)
            {
                where.Append(GetOperatorSymbol(fil.Operator, fil.Value));
                where.Append(" ");
                where.Append(GetValueString(fil.Value, fil.Operator));
                where.Append(" ");
            }
        }        
        
        private void DoStatusWhere(StringBuilder where, NoProblem.Core.DataModel.ReportsWizard.Filter fil, ref bool skip)
        {
            string[] useValue;
            if (fil.Value.ToString() == "WON")
            {
                useValue = new string[] { "10", "12", "13" };
            }
            else
            {
                useValue = new string[] { "1", "4", "14", "15", "16", "17" };
            }
            where.Append(" ( ");
            for (int i = 0; i < useValue.Length; i++)
            {
                where.Append(" inac.statuscode ");
                if (fil.Operator == "Equals")
                {
                    where.Append(" = ");
                }
                else
                {
                    where.Append(" != ");
                }
                where.Append(useValue[i]);
                if (i < useValue.Length - 1)
                {
                    if (fil.Operator == "Equals")
                    {
                        where.Append(" or ");
                    }
                    else
                    {
                        where.Append(" and ");
                    }
                }
            }
            where.Append(" ) ");
            skip = true;
        }

        private string ColumnToString(string column, bool isOrderBy)
        {
            switch (column)
            {
                case "Advertiser":
                    return (isOrderBy ? "new_accountidname" : "inac.new_accountidname, inac.new_accountid");
                case "ConsumerPhoneNumber":
                    return (isOrderBy ? "mobilephone" : "cont.mobilephone, cont.contactid");
                case "CreatedOn":
                    return (isOrderBy ? "createdon" : "inac.createdon");
                case "PricePerCall":
                    return (isOrderBy ? "new_potentialincidentprice" : "inac.new_potentialincidentprice");
                case "RequestId":
                    return (isOrderBy ? "ticketnumber" :"inc.ticketnumber");
                case "Status":
                    if (isOrderBy)
                    {
                        return "inacstatuscode";
                    }
                    else
                    {
                        return @"case inac.statuscode 
                            when  10 then 'WON'
                            when 12 then 'WON'
                            when 13 then 'WON'
                            else 'LOST'
                            end as inacstatuscode";
                    }
                case "PreDefinedPPC":
                    return isOrderBy ? "new_predefinedprice" : "inac.new_predefinedprice";
                case "PriceChanged":
                    return isOrderBy ? "new_isdefaultpricechanged" : "inac.new_isdefaultpricechanged";
                case "Duration":
                    return isOrderBy ? "duration" : "cdr.new_duration as duration";
            }
            return "";
        }

        #endregion
    }
}
