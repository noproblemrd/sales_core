﻿using System.Collections.Generic;
using System.Data;
using System.Text;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.ReportsWizard.Creators
{
    internal class DepositsReportCreator : WizardCreatorsBase
    {
        #region Ctor
        internal DepositsReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Internal Methods

        internal override List<List<string>> Create(DML.ReportsWizard.WizardReportRequest request)
        {
            StringBuilder select = new StringBuilder("select sp.new_supplierpricingid ");
            foreach (var col in request.Columns)
            {
                select.Append(",");
                select.Append(ColumnToString(col, false));
            }
            select.Append(" ");
            StringBuilder from = new StringBuilder(" from new_supplierpricing sp with (nolock) ");
            StringBuilder where = new StringBuilder(" where sp.deletionstatecode = 0 and sp.statecode = 0");
            foreach (var fil in request.Filters)
            {
                AddFilterTextToQuery(where, fil);
            }
            if (string.IsNullOrEmpty(request.OrderBy) == false)
            {
                InsertOrderBy(request.OrderBy, request.Descending, where, ColumnToString);
            }
            DataAccessLayer.ReportsWizardDal dal = new NoProblem.Core.DataAccessLayer.ReportsWizardDal();
            string query = select.ToString() + from.ToString() + where.ToString();
            DataTable table = dal.ExecuteSql(query);
            string groupBy = string.Empty;
            if (string.IsNullOrEmpty(request.GroupBy) == false)
            {
                groupBy = ColumnToString(request.GroupBy, true);
            }
            List<List<string>> data = CreateReturnValue(request.Columns.Count, table, groupBy);
            return data;
        }

        #endregion

        #region Private Methods

        private void AddFilterTextToQuery(StringBuilder where, NoProblem.Core.DataModel.ReportsWizard.Filter fil)
        {
            bool skip = false;
            where.Append(" and ");
            switch (fil.FilterField)
            {
                case "Advertiser":
                    where.Append(" sp.new_accountidname ");
                    break;
                case "Amount":
                    where.Append(" sp.new_total ");
                    break;
                case "CreatedOn":
                    if (fil.Operator == "NotEquals")
                    {
                        DoNotEqualsDateFilter(where, fil, ref skip, "sp.createdon");
                    }
                    else
                    {
                        where.Append(" sp.createdon ");
                    }
                    break;
            }
            if (!skip)
            {
                where.Append(GetOperatorSymbol(fil.Operator, fil.Value));
                where.Append(" ");
                where.Append(GetValueString(fil.Value, fil.Operator));
                where.Append(" ");
            }
        }

        private string ColumnToString(string column, bool isOrderBy)
        {
            switch (column)
            {
                case "Advertiser":
                    return isOrderBy ? "new_accountidname" : "sp.new_accountidname, sp.new_accountid";
                case "Amount":
                    return isOrderBy ? "new_total" : "sp.new_total";
                case "CreatedOn":
                    return isOrderBy ? "createdon" : "sp.createdon";
            }
            return "";
        }

        #endregion
    }
}
