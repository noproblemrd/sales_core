﻿using NoProblem.Core.DataModel.ReportsWizard;

namespace NoProblem.Core.BusinessLogic.ReportsWizard
{
    internal class ReportCreatorFactory
    {
        internal static NoProblem.Core.BusinessLogic.ReportsWizard.Creators.WizardCreatorsBase GetCreator(NoProblem.Core.DataModel.ReportsWizard.Issues issue, NoProblem.Core.DataModel.Xrm.DataContext XrmDataContext)
        {
            Creators.WizardCreatorsBase creator = null;
            switch (issue)
            {
                case NoProblem.Core.DataModel.ReportsWizard.Issues.Advertisers:
                    creator = new Creators.AdvertisersReportCreator(XrmDataContext);
                    break;
                case NoProblem.Core.DataModel.ReportsWizard.Issues.Calls:
                    creator = new Creators.CallsReportCreator(XrmDataContext);
                    break;
                case NoProblem.Core.DataModel.ReportsWizard.Issues.Deposits:
                    creator = new Creators.DepositsReportCreator(XrmDataContext);
                    break;
                case NoProblem.Core.DataModel.ReportsWizard.Issues.Requests:
                    creator = new Creators.RequestsReportsCreator(XrmDataContext);
                    break;
            }
            return creator;
        }
    }
}
