﻿using System.Collections.Generic;
using NoProblem.Core.BusinessLogic.ReportsWizard.Creators;
using NoProblem.Core.DataModel.ReportsWizard;
using DML = NoProblem.Core.DataModel;
using Effect.Crm.Logs;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ReportsWizard
{
    public class WizardManager : XrmUserBase
    {
        #region Ctor
        public WizardManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        public List<List<string>> CreateReport(WizardReportRequest request)
        {
            LogRequest(request);
            WizardCreatorsBase creator = ReportCreatorFactory.GetCreator(request.Issue, XrmDataContext);
            return creator.Create(request);
        }

        private void LogRequest(WizardReportRequest request)
        {
            StringBuilder buff = new StringBuilder("Report Wizard Params: ");
            buff.Append("Issue: ");
            buff.Append(request.Issue.ToString());
            buff.Append(" Filters:[");
            foreach (var fil in request.Filters)
            {
                buff.Append("Fil[")
                    .Append(fil.FilterField)
                    .Append(",")
                    .Append(fil.Operator)
                    .Append(",")
                    .Append(fil.Value.ToString())
                    .Append("] ");
            }
            buff.Append("] ");
            buff.Append("Columns:[");
            foreach (var col in request.Columns)
            {
                buff.Append(col)
                    .Append(",");
            }
            buff.Append("] ");
            buff.Append("Order By: ").Append(request.OrderBy);
            buff.Append(" Descenting: ").Append(request.Descending.ToString());
            LogUtils.MyHandle.WriteToLog(8, "{0}", buff.ToString());
        }
    }
}
