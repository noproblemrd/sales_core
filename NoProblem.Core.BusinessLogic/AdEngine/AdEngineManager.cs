﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AdEngine
{
    public class AdEngineManager : XrmUserBase
    {
        public AdEngineManager()
            : this(null)
        {

        }

        public AdEngineManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<DataModel.AdEngine.AdEngineCampaignData> GetAllCampaigns()
        {
            DataAccessLayer.AdEngineCampaign dal = new DataAccessLayer.AdEngineCampaign(XrmDataContext);
            return dal.GetAll();
        }

        public List<Guid> GetOriginsWithAdEngineEnabled()
        {
            DataAccessLayer.Origin originDal = new DataAccessLayer.Origin(XrmDataContext);
            var query = from or in originDal.All
                        where or.new_adengineenabled == true
                        select or.new_originid;
            return query.ToList();
        }
    }
}
