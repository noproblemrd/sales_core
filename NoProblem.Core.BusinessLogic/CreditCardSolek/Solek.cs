﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.CreditCardSolek
{
    /// <summary>
    /// Abstract class that represents a credit card charger.
    /// </summary>
    internal abstract class Solek
    {
        internal abstract bool RechargeSupplier(string token, string cvv2, string creditCardOwnerId, decimal chargeAmount, decimal chargeAmountWithVat, int numberOfPayments, out string solekAnswer, out string transactionConfirmationNumber, string ccLast4Digits, string ccType, string chargingCompanyAccountNumber, string chargingCompanyContractId, string userIP, string userAgent, string userRemoteHost, string userAcceptLanguage, out bool needsAsyncApproval, string ccExpDate, string ccOwnerName, DataModel.Xrm.new_supplierpricing newSupplierPricing, DataModel.Xrm.account supplier, DataModel.Xrm.new_paymentmethod.ePaymentMethod paymentMethod, List<DataModel.Xrm.new_paymentcomponent> payComponents, bool isBatch);

        internal virtual string CreateShopper(CreateShopperData data)
        {
            throw new NotImplementedException();
        }

        internal virtual DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode CheckPendingRecharge(string orderId, out string orderStatusReason, out InvoiceDataContainer invoiceDataContainer)
        {
            orderStatusReason = "Accepted";
            invoiceDataContainer = null;
            return NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Accepted;
        }

        public class CreateShopperData
        {
            private const string ZERO = "0";
            private const string US = "US";

            public string IP { get; set; }
            public string ShopperFirstName { get; set; }
            public string ShopperLastName { get; set; }
            public string ShopperEmail { get; set; }
            public string ShopperCompanyName { get; set; }
            public string ShopperStreetAddress { get; set; }
            public string ShopperCity { get; set; }
            public string ShopperZip { get; set; }
            public string ShopperState { get; set; }
            public string ShopperCountry
            {
                get
                {
                    return US;
                }
            }
            public string ShopperPhone { get; set; }
            public string ShopperFax { get; set; }
            public string BillingFirstName { get; set; }
            public string BillingLastName { get; set; }
            public string BillingStreetAddress { get; set; }
            public string BillingCity { get; set; }
            public string BillingZip { get; set; }
            public string BillingState { get; set; }
            public string BillingCountry
            {
                get
                {
                    return US;
                }
            }
            /// <summary>
            /// Encrypted in client side
            /// </summary>
            public string EncryptedCardNumber { get; set; }
            /// <summary>
            /// Three digits in back of card encrypted in client side.
            /// </summary>
            public string EncryptedCVV { get; set; }
            /// <summary>
            /// VISA, MASTERCARD, etc...
            /// </summary>
            public string CreditCardType { get; set; }
            /// <summary>
            /// Two digits. For example: "09" for September
            /// </summary>
            public string ExpirationMonth
            {
                get
                {
                    return expirationMonth;
                }
                set
                {
                    if (value.Length == 1)
                    {
                        expirationMonth = ZERO + value;
                    }
                    expirationMonth = value;
                }
            }
            private string expirationMonth;

            /// <summary>
            /// Four digits. For example: 2017
            /// </summary>
            public int ExpirationYear { get; set; }
            public string Subscription { get; set; }
            public DataModel.Xrm.account.SupplierStatus SupplierStatus { get; set; }
            public bool IsMobileSupplier { get; set; }
        }
    }
}
