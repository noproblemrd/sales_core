﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.CreditCardSolek
{
    internal class Pelecard : Solek
    {
        private string username;
        private string password;
        private string shop;
        private string terminal;

        public Pelecard()
        {
            username = ConfigurationManager.AppSettings["SolekUsername"];
            password = ConfigurationManager.AppSettings["SolekPassword"];
            shop = ConfigurationManager.AppSettings["SolekShop"];
            terminal = ConfigurationManager.AppSettings["SolekTerminal"];
        }

        internal override bool RechargeSupplier(string token, string cvv2, string creditCardOwnerId, decimal chargeAmount, decimal chargeAmountWithVat, int numberOfPayments, out string solekAnswer, out string transactionConfirmationNumber, string ccLast4Digits, string ccType, string chargingCompanyAccountNumber, string chargingCompanyContractId, string userIP, string userAgent, string userRemoteHost, string userAcceptLanguage, out bool needsAsyncApproval, string ccExpDate, string ccOwnerName, DataModel.Xrm.new_supplierpricing newSupplierPricing, DataModel.Xrm.account supplier, DataModel.Xrm.new_paymentmethod.ePaymentMethod paymentMethod, List<DataModel.Xrm.new_paymentcomponent> payComponents, bool isBatch)
        {
            needsAsyncApproval = false;
            LogUtils.MyHandle.WriteToLog(8, "Trying to charge through Pelecard. Amount: {0}, token: {1}, cvv2: {2}, ownerId: {3}", chargeAmount, token, cvv2, creditCardOwnerId);
            PelecardReference.ITrxWebService client = new NoProblem.Core.BusinessLogic.PelecardReference.ITrxWebService();
            int agorot = decimal.ToInt32(chargeAmount * 100); // *100 because the amount must be in agorot.
            string amountToSend = agorot.ToString();
            if (creditCardOwnerId == null)
                creditCardOwnerId = string.Empty;
            if (cvv2 == null)
                cvv2 = string.Empty;
            string ans;
            if (numberOfPayments == 1)
            {
                ans = client.DebitRegularType(username, password, terminal, shop, string.Empty, string.Empty, token, amountToSend, "1", cvv2, creditCardOwnerId, string.Empty, string.Empty);
            }
            else
            {
                ans = client.DebitPaymntsType(username, password, terminal, shop, string.Empty, string.Empty, token, chargeAmount.ToString(), "1", numberOfPayments.ToString(), string.Empty, cvv2, creditCardOwnerId, string.Empty, string.Empty);
            }
            LogUtils.MyHandle.WriteToLog(8, "Pelecard answered: {0}", ans);
            bool retVal = false;
            transactionConfirmationNumber = string.Empty;
            solekAnswer = ans.Substring(0, 3);
            if (solekAnswer == "000")
            {
                retVal = true;
                if (ans.Length > 77)
                    transactionConfirmationNumber = ans.Substring(70, 7);
            }
            return retVal;
        }
    }
}
