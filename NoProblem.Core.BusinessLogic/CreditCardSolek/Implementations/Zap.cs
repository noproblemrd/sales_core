﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Net;

namespace NoProblem.Core.BusinessLogic.CreditCardSolek
{
    internal class Zap : Solek
    {
        private const string OK_CC_RESPONSE = "000";
        private const string OK_RESULT_RESPONSE = "OK";
        private const string SOLEK_ANSWER_FORMAT = "CgStatus = {0}. CgStatusDesc = {1}. Result = {3}. ErrorDesc = {2}.";
        private const string HEADING_PPA_CHARGE = "HeadingPPACharge";
        private const string CALL_BALANCE_CHARGE = "CallBalanceCharge";
        private const string DATE_TIME_FORMAT = "yyyy-MM-ddTHH:mm:ss";
        internal const string DONT_NOTIFY_CODE = "XXX_DONT_NOTIFY_XXX";

        private static DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);

        private static string _url;
        private string Url
        {
            get
            {
                if (string.IsNullOrEmpty(_url))
                {
                    _url = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.ZAP_PAYMENTS_SYSTEM_URL);
                }
                return _url;
            }
        }

        private static string _loginPrefix;
        private string LoginPrefix
        {
            get
            {
                if (string.IsNullOrEmpty(_loginPrefix))
                {
                    _loginPrefix = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.ZAP_PAYMENTS_SYSTEM_LOGIN_PREFIX);
                }
                return _loginPrefix;
            }
        }

        internal override bool RechargeSupplier(string token, string cvv2, string creditCardOwnerId, decimal chargeAmount, decimal chargeAmountWithVat, int numberOfPayments, out string solekAnswer, out string transactionConfirmationNumber, string ccLast4Digits, string ccType, string chargingCompanyAccountNumber, string chargingCompanyContractId, string userIP, string userAgent, string userRemoteHost, string userAcceptLanguage, out bool needsAsyncApproval, string ccExpDate, string ccOwnerName, DataModel.Xrm.new_supplierpricing newSupplierPricing, DataModel.Xrm.account supplier, DataModel.Xrm.new_paymentmethod.ePaymentMethod paymentMethod, List<DataModel.Xrm.new_paymentcomponent> payComponents, bool isBatch)
        {
            transactionConfirmationNumber = "";
            needsAsyncApproval = false;
            if (chargeAmount == 0
                && (!supplier.new_isdollaraccount.HasValue || !supplier.new_isdollaraccount.Value))
            {
                solekAnswer = DONT_NOTIFY_CODE;
                return false;
            }
            solekAnswer = "";            
            bool retVal = false;
            try
            {
                ZapPaymentsService.NPCreateInvoiceERP client = new ZapPaymentsService.NPCreateInvoiceERP();
                client.Url = Url;

                ServicePointManager.DefaultConnectionLimit = 50;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                ServicePointManager.MaxServicePointIdleTime = 1000 * 5 * 1;
                ServicePointManager.UseNagleAlgorithm = true;
                ServicePointManager.Expect100Continue = false;

                //This for page doesn't have certification 
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);


                ZapPaymentsService.NPOrder_Type orderType = new ZapPaymentsService.NPOrder_Type();
                orderType.NPOrderHeaderRec = new ZapPaymentsService.NPOrderHeaderRec_Type();

                switch (paymentMethod)
                {
                    case NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.Cash:
                        {
                            orderType.NPOrderHeaderRec.PaymentType = NoProblem.Core.BusinessLogic.ZapPaymentsService.NPOrderHeaderRec_TypePaymentType.CASH;
                            break;
                        }
                    case NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.CreditCard:
                        {
                            orderType.NPOrderHeaderRec.CcCompanyName = ccType;
                            orderType.NPOrderHeaderRec.CcExpDate = ccExpDate;
                            orderType.NPOrderHeaderRec.CcId = token;
                            orderType.NPOrderHeaderRec.CcOwnerId = creditCardOwnerId;
                            orderType.NPOrderHeaderRec.CcOwnerName = ccOwnerName;
                            orderType.NPOrderHeaderRec.PaymentType = NoProblem.Core.BusinessLogic.ZapPaymentsService.NPOrderHeaderRec_TypePaymentType.CC;
                            break;
                        }
                    case NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.WireTransfer:
                        {
                            orderType.NPOrderHeaderRec.PaymentType = NoProblem.Core.BusinessLogic.ZapPaymentsService.NPOrderHeaderRec_TypePaymentType.TRANSFER;
                            break;
                        }
                    default:
                        {
                            solekAnswer = "Payment method not supported by Zap payments system. Method received = " + paymentMethod;
                            throw new Exception("Payment method not supported by Zap payments system. Method received = " + paymentMethod);
                        }
                }

                orderType.NPOrderHeaderRec.TrxUniqueIdentifier = chargingCompanyContractId;

                orderType.NPOrderHeaderRec.CustomerEmail = supplier.emailaddress1;
                orderType.NPOrderHeaderRec.CustomerNo = supplier.new_bizid.Trim();
                orderType.NPOrderHeaderRec.InvoiceComments = string.Empty;
                orderType.NPOrderHeaderRec.InvoiceDate = DateTime.Now.ToString(DATE_TIME_FORMAT);
                orderType.NPOrderHeaderRec.InvoiceTotalAmtWithVAT = chargeAmountWithVat;
                orderType.NPOrderHeaderRec.InvoiceTotalAmtNoVAT = chargeAmount;
                DataModel.Xrm.account user;
                orderType.NPOrderHeaderRec.SalesPerson = FindSalesPersonZapId(newSupplierPricing, supplier, out user);
                orderType.NPOrderHeaderRec.TaxRegistrationNum = supplier.new_accountgovernmentid;

                List<ZapPaymentsService.OrderItem_Type> listOfOrderedItems = new List<ZapPaymentsService.OrderItem_Type>();
                int lineCount = 1;
                foreach (var spc in payComponents)
                {
                    ZapPaymentsService.OrderItem_Type rowItem = CreateOrderItem(spc, lineCount++);
                    listOfOrderedItems.Add(rowItem);
                }
                orderType.ListOfOrderItem = listOfOrderedItems.ToArray();

                var request = new ZapPaymentsService.NPCreateInvoiceERPProcessRequest();
                request.LoginUser = CreateLoginUser(user, supplier);
                request.NPOrder = orderType;

                CheckNullFields(request, newSupplierPricing);

                request.ProcessModeSpecified = true;
                ZapPaymentsService.NPCreateInvoiceERPProcessRequestProcessMode processMode;
                if (isBatch)
                {
                    processMode = ZapPaymentsService.NPCreateInvoiceERPProcessRequestProcessMode.BATCH;
                }
                else
                {
                    processMode = ZapPaymentsService.NPCreateInvoiceERPProcessRequestProcessMode.ONLINE;
                }
                request.ProcessMode = processMode;

                var ans = client.process(request);

                solekAnswer = string.Format(SOLEK_ANSWER_FORMAT, ans.CgStatus, ans.CgStatusDesc, ans.ErrorDesc, ans.Result);

                if (ans.Result == OK_RESULT_RESPONSE)
                {
                    if (paymentMethod == NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.CreditCard)
                    {
                        transactionConfirmationNumber = ans.CgAuthorizationNum;
                        if (ans.CgStatus == OK_CC_RESPONSE)
                        {
                            retVal = true;
                        }
                    }
                    else
                    {
                        retVal = true;
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.WriteToLog("Exception in NoProblem.Core.BusinessLogic.CreditCardSolek.Implementations.Zap.RechargeSupplier ExceptionMessage = " + exc.Message);
            }
            return retVal;
        }

        private static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private void CheckNullFields(NoProblem.Core.BusinessLogic.ZapPaymentsService.NPCreateInvoiceERPProcessRequest request, DataModel.Xrm.new_supplierpricing newSupplierPricing)
        {
            if (request.NPOrder.NPOrderHeaderRec.CcId == null)
            {
                request.NPOrder.NPOrderHeaderRec.CcId = string.Empty;
            }
            if (string.IsNullOrEmpty(request.NPOrder.NPOrderHeaderRec.TrxUniqueIdentifier))
            {
                string newTrxId = Guid.NewGuid().ToString();
                request.NPOrder.NPOrderHeaderRec.TrxUniqueIdentifier = newTrxId;
                newSupplierPricing.new_chargingcompanycontractid = newTrxId;
            }
            if (request.NPOrder.NPOrderHeaderRec.CustomerEmail == null)
            {
                request.NPOrder.NPOrderHeaderRec.CustomerEmail = string.Empty;
            }
            if (request.NPOrder.NPOrderHeaderRec.CustomerNo == null)
            {
                request.NPOrder.NPOrderHeaderRec.CustomerNo = string.Empty;
            }
            if (request.NPOrder.NPOrderHeaderRec.SalesPerson == null)
            {
                request.NPOrder.NPOrderHeaderRec.SalesPerson = string.Empty;
            }
        }

        private ZapPaymentsService.OrderItem_Type CreateOrderItem(NoProblem.Core.DataModel.Xrm.new_paymentcomponent spc, int lineCount)
        {
            ZapPaymentsService.OrderItem_Type rowItem = new NoProblem.Core.BusinessLogic.ZapPaymentsService.OrderItem_Type();
            rowItem.LineNum = lineCount.ToString();
            if (spc.new_type == (int)DataModel.Xrm.new_paymentcomponent.ePaymentComponentType.MonthlyFeePayment)
            {
                rowItem.ItemDescription = HEADING_PPA_CHARGE;
            }
            else
            {
                rowItem.ItemDescription = CALL_BALANCE_CHARGE;
            }
            rowItem.ItemPriceNoVat = spc.new_currentpaymentamount.Value;
            rowItem.ItemPriceNoVatSpecified = true;
            rowItem.Quantity = 1;
            rowItem.QuantitySpecified = true;
            return rowItem;
        }

        private string CreateLoginUser(DataModel.Xrm.account user, DataModel.Xrm.account supplier)
        {
            string login;
            string userEmail;
            if (user != null)
            {
                userEmail = user.emailaddress1;
            }
            else
            {
                userEmail = supplier.emailaddress1;
            }
            login = LoginPrefix + userEmail;
            return login;
        }

        private string FindSalesPersonZapId(DataModel.Xrm.new_supplierpricing newSupplierPricing, DataModel.Xrm.account supplier, out DataModel.Xrm.account user)
        {
            string salesPersonZapId = null;
            user = null;
            if (newSupplierPricing.new_userid.HasValue && newSupplierPricing.new_userid.Value != Guid.Empty && newSupplierPricing.new_userid.Value != supplier.accountid)
            {
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(null);
                user = dal.Retrieve(newSupplierPricing.new_userid.Value);
                if (!string.IsNullOrEmpty(user.new_userbizid))
                {
                    salesPersonZapId = user.new_userbizid;
                }
            }
            if (salesPersonZapId == null)
            {
                if (supplier.new_managerid.HasValue && !string.IsNullOrEmpty(supplier.new_account_account_account.new_userbizid))
                {
                    salesPersonZapId = supplier.new_account_account_account.new_userbizid;
                }
            }
            return salesPersonZapId;
        }
    }
}
