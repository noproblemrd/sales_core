﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Configuration;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Effect.Crm.Logs;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.CreditCardSolek
{
    /// <summary>
    /// Must configure the checkbox in plimus like this: "I agree to the <a href=http://www2.noproblemppc.com/LegalAdvTerms.htm target=_new >Terms of service</a>"
    /// </summary>
    internal class Plimus : Solek
    {

        #region Fields

        private string username;
        private string password;
        private string url;
        private string urlCreateShopper;
        private string shop;
        private static XNamespace xmlNamespace = "http://ws.plimus.com";

        #region recharge XML structure

        private static readonly string xmlStructureRecharge =
            "<order xmlns=\"http://ws.plimus.com\">" +
            @"<soft-descriptor>NoProblem</soft-descriptor>
            <ordering-shopper>
                <shopper-id>{0}</shopper-id>
                <credit-card>
                    <card-last-four-digits>{1}</card-last-four-digits>
                    <card-type>{2}</card-type>
                </credit-card>
                <web-info>
                    <ip>{3}</ip>
                    <remote-host>{4}</remote-host>
                    <user-agent>{5}</user-agent>
                </web-info>
            </ordering-shopper>
            <cart>
                <cart-item>
                    <sku>
                        <sku-id>{6}</sku-id>
                        <sku-charge-price>
                            <charge-type>initial</charge-type>
                            <amount>{7}</amount>
                            <currency>USD</currency>
                        </sku-charge-price>
                    </sku>
                    <quantity>1</quantity>
                    <sku-parameter>
                        <param-name>ifRecharge</param-name>
                        <param-value>TRUE</param-value>
                    </sku-parameter>
                    <sku-parameter>
                        <param-name>I agree to the &lt;a href=http://www2.noproblemppc.com/LegalAdvTerms.htm target=_new &gt;Terms of service&lt;/a&gt;</param-name>
                        <param-value>Y</param-value>
                    </sku-parameter>
                </cart-item>
                <coupons>
                    <coupon></coupon>
                </coupons>
                <cdod>false</cdod>
            </cart>
            <expected-total-price>
                <amount>{7}</amount>
                <currency>USD</currency>
            </expected-total-price>
            <affiliate-id></affiliate-id>
            </order>";

        private static readonly string xmlStructureRechargeDesktopCustomerRecordCall =
            "<order xmlns=\"http://ws.plimus.com\">" +
            @"<soft-descriptor>NoProblem</soft-descriptor>
            <ordering-shopper>
                <shopper-id>{0}</shopper-id>
                <credit-card>
                    <card-last-four-digits>{1}</card-last-four-digits>
                    <card-type>{2}</card-type>
                </credit-card>
                <web-info>
                    <ip>{3}</ip>
                    <remote-host>{4}</remote-host>
                    <user-agent>{5}</user-agent>
                </web-info>
            </ordering-shopper>
            <cart>
                <cart-item>
                    <sku>
                        <sku-id>{6}</sku-id>
                        <sku-charge-price>
                            <charge-type>initial</charge-type>
                            <amount>{7}</amount>
                            <currency>USD</currency>
                        </sku-charge-price>
                    </sku>
                    <quantity>1</quantity>
                </cart-item>
                <coupons>
                    <coupon></coupon>
                </coupons>
                <cdod>false</cdod>
            </cart>
            <expected-total-price>
                <amount>{7}</amount>
                <currency>USD</currency>
            </expected-total-price>
            <affiliate-id></affiliate-id>
            </order>";


        #endregion

        #region create shopper XML structure

        private static readonly string xmlStructureCreateShopper =
            "<shopper xmlns=\"http://ws.plimus.com\">" +
  @"<web-info>
    <ip>{0}</ip>
  </web-info>
  <shopper-info>
    <shopper-currency>USD</shopper-currency>
    <store-id>{1}</store-id>
    <locale>en</locale>
    <shopper-contact-info>
      <title></title>
      <first-name>{2}</first-name>
      <last-name>{3}</last-name>
      <email>{4}</email>
      <company-name>{5}</company-name>
      <address1>{6}</address1>
      <city>{7}</city>
      <zip>{8}</zip>
      <state>{9}</state>
      <country>{10}</country>
      <phone>{11}</phone>
      <fax>{12}</fax>
    </shopper-contact-info>
    <payment-info>
      <credit-cards-info>
        <credit-card-info>
          <billing-contact-info>
            <first-name>{13}</first-name>
            <last-name>{14}</last-name>
            <address1>{15}</address1>
            <city>{16}</city>
            <zip>{17}</zip>
            <state>{18}</state>
            <country>{19}</country>
          </billing-contact-info>
          <credit-card>
            <encrypted-card-number>{20}</encrypted-card-number>
            <encrypted-security-code>{21}</encrypted-security-code>
            <card-type>{22}</card-type>
            <expiration-month>{23}</expiration-month>
            <expiration-year>{24}</expiration-year>
          </credit-card>
        </credit-card-info>
      </credit-cards-info>
    </payment-info>
  </shopper-info>
</shopper>";

        #endregion

        #endregion

        #region Ctor
        public Plimus()
        {
            username = ConfigurationManager.AppSettings["SolekUsername"];
            password = ConfigurationManager.AppSettings["SolekPassword"];
            url = ConfigurationManager.AppSettings["SolekUrl"];
            urlCreateShopper = ConfigurationManager.AppSettings["SolekCreateShopperUrl"];
            shop = ConfigurationManager.AppSettings["SolekShop"];
        }
        #endregion

        #region Create Shopper Methods

        internal override string CreateShopper(Solek.CreateShopperData data)
        {
            string shopperId = null;
            string xmlRequest =
                string.Format(xmlStructureCreateShopper,
                   data.IP,
                   shop,
                   data.ShopperFirstName,
                   data.ShopperLastName,
                   data.ShopperEmail,
                   data.ShopperCompanyName,
                   data.ShopperStreetAddress,
                   data.ShopperCity,
                   data.ShopperZip,
                   data.ShopperState,
                   data.ShopperCountry,
                   data.ShopperPhone,
                   data.ShopperFax,
                   data.BillingFirstName,
                   data.BillingLastName,
                   data.BillingStreetAddress,
                   data.BillingCity,
                   data.BillingZip,
                   data.BillingState,
                   data.BillingCountry,
                   data.EncryptedCardNumber,
                   data.EncryptedCVV,
                   data.CreditCardType,
                   data.ExpirationMonth,
                   data.ExpirationYear
                   );
            LogUtils.MyHandle.WriteToLog(9, "xmlRequest = " + xmlRequest);
            RestSharp.IRestClient client = new RestSharp.RestClient(urlCreateShopper);
            RestSharp.IRestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            string usernamePassword = username + ":" + password;
            request.AddHeader("Authorization", "Basic " + Convert.ToBase64String(new ASCIIEncoding().GetBytes(usernamePassword)));
            request.AddParameter("application/xml", xmlRequest, RestSharp.ParameterType.RequestBody);
            RestSharp.IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.Created)
            {
                string locationUrl = response.Headers.First(x => x.Name == "Location").Value.ToString();
                int lastSlashIndex = locationUrl.LastIndexOf("/");
                shopperId = locationUrl.Substring(lastSlashIndex + 1);
            }
            else
            {
                LogUtils.MyHandle.WriteToLog(8, "Plimus answered: status={0}, body={1}", response.StatusCode, response.Content);
            }
            return shopperId;
        }

        #endregion

        #region Recharge Methods

        internal override bool RechargeSupplier(
            string token,
            string cvv2,
            string creditCardOwnerId,
            decimal chargeAmount,
            decimal chargeAmountWithVat,
            int numberOfPayments,
            out string SolekAnswer,
            out string transactionConfirmationNumber,
            string ccLast4Digits,
            string ccType,
            string ChargingCompanyAccountNumber,
            string ChargingCompanyContractId,
            string userIP,
            string userAgent,
            string userRemoteHost,
            string userAcceptLanguage,
            out bool needsAsyncApproval,
            string ccExpDate,
            string ccOwnerName,
            DataModel.Xrm.new_supplierpricing newSupplierPricing,
            DataModel.Xrm.account supplier,
            DataModel.Xrm.new_paymentmethod.ePaymentMethod paymentMethod,
            List<DataModel.Xrm.new_paymentcomponent> payComponents,
            bool isBatch)
        {
            LogUtils.MyHandle.WriteToLog(8, "Trying to charge through Plimus. Amount: {0}, ccLast4Digits: {1}, ccType: {2}, ChargingCompanyAccountNumber: {3}, ChargingCompanyContractId: {4}", chargeAmount, ccLast4Digits, ccType, ChargingCompanyAccountNumber, ChargingCompanyContractId);
            bool retVal = true;
            needsAsyncApproval = true;
            string xmlToUse;
            if (ChargingCompanyContractId == NoProblem.Core.BusinessLogic.CreditCardSolek.PlimusContracts.desktopCustomerContractId)
            {
                xmlToUse = xmlStructureRechargeDesktopCustomerRecordCall;
            }
            else
            {
                xmlToUse = xmlStructureRecharge;
            }

           string xmlRequest =
                string.Format(xmlToUse,
                    ChargingCompanyAccountNumber,
                    ccLast4Digits,
                    ccType,
                    userIP,
                    userRemoteHost,
                    userAgent,
                    ChargingCompanyContractId,
                    decimal.ToInt32(chargeAmount));
            LogUtils.MyHandle.WriteToLog(9, "xmlRequest = " + xmlRequest);
            WebRequest request = WebRequest.Create(url);
            string usernamePassword = username + ":" + password;
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(new ASCIIEncoding().GetBytes(usernamePassword)));
            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/xml";
            byte[] postBuffer = Encoding.UTF8.GetBytes(xmlRequest);
            request.ContentLength = postBuffer.Length;
            using (Stream RequestStream = request.GetRequestStream())
            {
                RequestStream.Write(postBuffer, 0, postBuffer.Length);
            }
            string result;
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        result = sr.ReadToEnd();
                    }
                }
                XDocument doc = XDocument.Parse(result);
                transactionConfirmationNumber = doc.Root.Element(xmlNamespace + "order-id").Value;
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    result = httpResponse.StatusCode.ToString();
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        string text = sr.ReadToEnd();
                        result += " \n" + text;
                    }
                }
                transactionConfirmationNumber = "";
                retVal = false;
            }
            LogUtils.MyHandle.WriteToLog(8, "Plimus answered: {0}", result);
            SolekAnswer = result;
            return retVal;
        }

        internal override NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode CheckPendingRecharge(string orderId, out string orderStatusReason, out InvoiceDataContainer invoiceDataContainer)
        {
            invoiceDataContainer = new InvoiceDataContainer();
            if (!url.EndsWith("/"))
            {
                url += "/";
            }
            string requestUrl = url + orderId;
            LogUtils.MyHandle.WriteToLog(9, "Checking pending payment in plimus: " + requestUrl);
            WebRequest request = WebRequest.Create(requestUrl);
            string usernamePassword = username + ":" + password;
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(new ASCIIEncoding().GetBytes(usernamePassword)));
            request.Method = WebRequestMethods.Http.Get;
            request.ContentType = "application/xml";
            string result;
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        result = sr.ReadToEnd();
                    }
                }
                orderStatusReason = ParseXmlResponse(invoiceDataContainer, result);
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    result = httpResponse.StatusCode.ToString();
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        string text = sr.ReadToEnd();
                        result += " \n" + text;
                    }
                    orderStatusReason = "";
                    LogUtils.MyHandle.HandleException(e, "Exception in CheckPendingRecharge web service to plimus. Recharge is still pending. Result = {0}", result);
                    throw e;
                }
            }
            LogUtils.MyHandle.WriteToLog(8, "Plimus answered (when no error): {0}", orderStatusReason);
            DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode retVal = NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Pending;
            if (orderStatusReason == "Approved")
            {
                retVal = NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Accepted;
            }
            else if (orderStatusReason == "Declined")
            {
                retVal = NoProblem.Core.DataModel.Xrm.new_pendingsupplierpricing.OrderStatusCode.Cancelled;
            }
            return retVal;
        }

        private static string ParseXmlResponse(InvoiceDataContainer invoiceDataContainer, string result)
        {
            string orderStatusReason;
            XDocument doc = XDocument.Parse(result);
            var invoiceElement = doc.Root.
                Element(xmlNamespace + "post-sale-info").
                Element(xmlNamespace + "invoices").
                Element(xmlNamespace + "invoice");
            var financialElement = invoiceElement.
                Element(xmlNamespace + "financial-transactions").
                Element(xmlNamespace + "financial-transaction");
            orderStatusReason = financialElement.
                Element(xmlNamespace + "status").Value;
            var invoiceUrlElement = invoiceElement.
                Element(xmlNamespace + "url");
            if (invoiceUrlElement != null)
                invoiceDataContainer.InvoiceUrl = invoiceUrlElement.Value;
            var invoiceNumberElement = invoiceElement.
                Element(xmlNamespace + "invoice-id");
            if (invoiceNumberElement != null)
                invoiceDataContainer.InvoiceNumber = invoiceNumberElement.Value;
            var contInfoElement = financialElement.
                Element(xmlNamespace + "invoice-contact-info");
            if (contInfoElement != null)
            {
                var fNameElement = contInfoElement.Element(xmlNamespace + "first-name");
                if (fNameElement != null)
                    invoiceDataContainer.BillingFirstName = fNameElement.Value;
                var lNameElement = contInfoElement.Element(xmlNamespace + "last-name");
                if (lNameElement != null)
                    invoiceDataContainer.BillingLastName = lNameElement.Value;
                var emailElement = contInfoElement.Element(xmlNamespace + "email");
                if (emailElement != null)
                    invoiceDataContainer.BillingEmail = emailElement.Value;
                var stateElement = contInfoElement.Element(xmlNamespace + "state");
                if (stateElement != null)
                    invoiceDataContainer.BillingState = stateElement.Value;
                var countryElement = contInfoElement.Element(xmlNamespace + "country");
                if (countryElement != null)
                    invoiceDataContainer.BillingCountry = countryElement.Value;
                var phoneElement = contInfoElement.Element(xmlNamespace + "phone");
                if (phoneElement != null)
                    invoiceDataContainer.BillingPhone = phoneElement.Value;
                var address1Element = contInfoElement.Element(xmlNamespace + "address1");
                if (address1Element != null)
                    invoiceDataContainer.BillingAddress1 = address1Element.Value;
                var address2Element = contInfoElement.Element(xmlNamespace + "address2");
                if (address2Element != null)
                    invoiceDataContainer.BillingAddress2 = address2Element.Value;
                var cityElement = contInfoElement.Element(xmlNamespace + "city");
                if (cityElement != null)
                    invoiceDataContainer.BillingCity = cityElement.Value;
                var zipElement = contInfoElement.Element(xmlNamespace + "zip");
                if (zipElement != null)
                    invoiceDataContainer.BillingZip = zipElement.Value;
            }
            return orderStatusReason;
        }

        #endregion
    }


    [XmlRoot("order", Namespace = "http://ws.plimus.com", IsNullable = false)]
    public class PlimusOrder
    {
        [XmlElement("soft-description")]
        public string SoftDescription { get; set; }
        [XmlElement("ordering-shopper")]
        public OrderingShopper OrderingShopper { get; set; }
        [XmlElement("cart")]
        public Cart Cart { get; set; }
    }

    public class OrderingShopper
    {
        [XmlElement(ElementName = "shopper-id")]
        public string ShopperId { get; set; }
        [XmlElement(ElementName = "credit-card")]
        public CreditCardRef CreditCard { get; set; }
         [XmlElement(ElementName = "web-info")]
        public WebInfo WebInfo { get; set; }
    }

    public class CreditCardRef
    {
        [XmlElement(ElementName = "card-last-four-digits")]
        public string LastFourDigits { get; set; }
        [XmlElement(ElementName = "card-type")]
        public string Type { get; set; }
    }

    public class WebInfo
    {
        [XmlElement(ElementName = "ip")]
        public string Ip { get; set; }
        [XmlElement(ElementName = "remote-host")]
        public string RemoteHost { get; set; }
        [XmlElement(ElementName = "user-agent")]
        public string UserAgent { get; set; }
    }

    public class Cart
    {
        [XmlElement(ElementName = "cart-item")]
        public CartItem Item { get; set; }
    }

    public class CartItem
    {
        [XmlElement(ElementName = "sku")]
        public Sku Sku { get; set; }
        [XmlElement(ElementName = "quantity")]
        public int Quantity { get; set; }

        [XmlElement(ElementName = "sku-parameter")]
        public List<SkuParameter> Parameters { get; set; }
    }

    public class Sku
    {
        [XmlElement(ElementName = "sku-id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "sku-charge-price")]
        public SkuChargePrice ChargePrice { get; set; }
    }

    public class SkuChargePrice
    {
        [XmlElement(ElementName = "charge-type")]
        public string ChargeType { get; set; }
        [XmlElement(ElementName = "amount")]
        public decimal Amount { get; set; }
        [XmlElement(ElementName = "currency")]
        public string Currency { get; set; }
    }

    public class SkuParameter
    {
        [XmlElement(ElementName = "param-name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "param-value")]
        public string Value { get; set; }
    }
}
