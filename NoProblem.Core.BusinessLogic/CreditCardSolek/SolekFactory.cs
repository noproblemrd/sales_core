﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.CreditCardSolek
{
    internal class SolekFactory
    {
        /// <summary>
        /// Creates an instance of the relevant solek for the environment.
        /// </summary>
        /// <param name="solekName">The name of the solek</param>
        /// <returns>Instance of Solek or null if no solek in the environment or the name is not found.</returns>
        internal static Solek GetSolek(string solekName)
        {
            switch (solekName.ToLower())
            {
                case "pelecard":
                    return new Pelecard();
                case "plimus":
                    return new Plimus();
                case "zap":
                    return new Zap();
            }
            return null;
        }
    }
}
