﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.CreditCardSolek
{
    internal class PlimusContracts
    {
        public static readonly string desktopCustomerContractId = ConfigurationManager.AppSettings["SolekDesktopCustomerCallRecorderContractId"];
        public static readonly string leadBoosterContractId = ConfigurationManager.AppSettings["SolekLeadBoosterContractId"];
        public static readonly string featuredListingContractId = ConfigurationManager.AppSettings["SolekFeaturedListingContractId"];

        public static readonly string ClipCallContractId = ConfigurationManager.AppSettings["SolekClipCallContractId"];
    }
}
