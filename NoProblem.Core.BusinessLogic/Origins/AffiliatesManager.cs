﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.PublisherPortal;
using NoProblem.Core.DataModel;
using NoProblem.Core.BusinessLogic.Utils;
using System.Configuration;
using System.Data.SqlClient;
namespace NoProblem.Core.BusinessLogic.Origins
{
    public class AffiliatesManager : XrmUserBase
    {
        public AffiliatesManager(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
         
        }

        public List<AffiliateUserData> GetAllAffiliateUsers()
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            List<AffiliateUserData> retVal = dal.GetAllAffiliateUsers();
            return retVal;
        }

        public List<GuidStringPair> GetAllOrigins(bool getSystemOrigins)
        {
            DataAccessLayer.Origin dal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            List<GuidStringPair> retVal = dal.GetAllOrigins(getSystemOrigins, false).ToList();
            return retVal;
        }

        public Guid GetCreateWebSiteId(string name, Guid originId)
        {
            Guid retVal = Guid.Empty;
            if (string.IsNullOrEmpty(name) == false && originId != Guid.Empty)
            {
                DataAccessLayer.WebSite dal = new NoProblem.Core.DataAccessLayer.WebSite(XrmDataContext);
                retVal = dal.GetWebSiteByName(name, originId);
                if (retVal == Guid.Empty)
                {
                    var webSite = new DataModel.Xrm.new_website();
                    webSite.new_name = name;
                    webSite.new_originid = originId;
                    dal.Create(webSite);
                    XrmDataContext.SaveChanges();
                    retVal = webSite.new_websiteid;
                }
            }
            return retVal;
        }

        public Guid GetCreateToolbarId(string toolbarIdName, Guid originId)
        {
            Guid retVal = Guid.Empty;
            if (string.IsNullOrEmpty(toolbarIdName) == false && originId != Guid.Empty)
            {
                DataAccessLayer.ToolbarId dal = new NoProblem.Core.DataAccessLayer.ToolbarId(XrmDataContext);
                retVal = dal.GetToolbarIdByName(toolbarIdName, originId);
                if (retVal == Guid.Empty)
                {
                    var toolbarIdEntry = new DataModel.Xrm.new_toolbarid();
                    toolbarIdEntry.new_name = toolbarIdName;
                    toolbarIdEntry.new_originid = originId;
                    dal.Create(toolbarIdEntry);
                    XrmDataContext.SaveChanges();
                    retVal = toolbarIdEntry.new_toolbaridid;
                }
            }
            return retVal;
        }

        public HashSet<OriginsWebSitesData> GetAllWebSites()
        {
            DataAccessLayer.WebSite dal = new NoProblem.Core.DataAccessLayer.WebSite(XrmDataContext);
            HashSet<OriginsWebSitesData> retVal = dal.GetAllWebSites();
            return retVal;
        }

        public List<List<GuidStringPair>> GetAllPayStatusAndReasons()
        {
            DataAccessLayer.AffiliatePayStatus dal = new NoProblem.Core.DataAccessLayer.AffiliatePayStatus(XrmDataContext);
            List<List<GuidStringPair>> retVal = dal.GetAll();
            return retVal;
        }

        public void UpdateAffiliatePayStatus(Guid caseId, Guid payStatusId, Guid payReasonId)
        {
            DataModel.Xrm.incident incident = new NoProblem.Core.DataModel.Xrm.incident(XrmDataContext);
            incident.incidentid = caseId;
            incident.new_affiliatepaystatusid = payStatusId;
            incident.new_affiliatepaystatusreasonid = payReasonId;
            DataAccessLayer.Incident dal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            dal.Update(incident);
            XrmDataContext.SaveChanges();
        }

        public Guid GetDefaultOrigin()
        {
            DataAccessLayer.Origin dal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            Guid retVal = dal.GetDefaultOrigin();
            return retVal;
        }

        internal bool CalculateAffiliatePayStatus(out Guid payStatusId, out Guid payStatusReasonId, Guid expertiseId, Guid origindId, Guid contactId, bool isBlockedNumberContact, DataAccessLayer.Incident incidentDal, bool isUpsale, Guid regionId, bool isCallCenterCase)
        {
            bool useDefault = true;
            payStatusReasonId = Guid.Empty;
            payStatusId = Guid.Empty;
            DataAccessLayer.AffiliatePayStatusReason payReasonsDal = new NoProblem.Core.DataAccessLayer.AffiliatePayStatusReason(XrmDataContext);
            if (isCallCenterCase)
            {
                useDefault = false;
                payStatusReasonId = payReasonsDal.GetCallCenterReason(out payStatusId);
            }
            else if (isUpsale == false && isBlockedNumberContact == false)
            {
                DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string configVal = configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.AFFILIATE_PAY_TIME_PASS);
                int passedDays = int.Parse(configVal);
                if (passedDays > 0)
                {
                    //int publisherTimeZoneCode = int.Parse(configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.PUBLISHER_TIME_ZONE));
                    DateTime localDate = FixDateToLocal(DateTime.Now).Date; // ConvertUtils.GetLocalFromUtc(XrmDataContext, DateTime.Now, publisherTimeZoneCode).Date;
                    DateTime midNightLocal = FixDateToUtcFromLocal(localDate); //ConvertUtils.GetUtcFromLocal(XrmDataContext, localDate, publisherTimeZoneCode);
                    DateTime dateReady = midNightLocal.AddDays((passedDays - 1) * -1);
                    bool wasPayed = incidentDal.IsExistsInInterval(expertiseId, contactId, origindId, dateReady, regionId);
                    if (wasPayed)
                    {
                        useDefault = false;
                        payStatusReasonId = payReasonsDal.GetIntervalPayStatus(out payStatusId);
                    }
                }
            }
            if (useDefault)
            {
                payStatusReasonId = payReasonsDal.GetDefault(out payStatusId);
            }
            return useDefault;
        }
        public List<GuidStringPair> GetOriginBroughtByName()
        {
            List<GuidStringPair> list = new List<GuidStringPair>();
            string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
            string command = @"SELECT New_originId,
                                  New_SliderBroughtByName
                              FROM dbo.New_originExtensionBase
                              WHERE ISNULL(New_SliderBroughtByName, '') <> ''";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        GuidStringPair gsp = new GuidStringPair((string)reader["New_SliderBroughtByName"], (Guid)reader["New_originId"]);
                        list.Add(gsp);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        public List<Guid> GetNoProblemSliderOrigins()
        {
            List<Guid> list = new List<Guid>();
            string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
            string command = @"SELECT New_originId
                                FROM dbo.New_originExtensionBase
                                WHERE ISNULL(New_ShowNoProblemInSlider, 0) = 1";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        list.Add((Guid)reader["New_originId"]);
                    }
                    conn.Close();
                }
            }
            return list;
        }
        public List<Guid> GetDistributionOrigins()
        {
            List<Guid> list = new List<Guid>();
            string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
            string command = @"SELECT ORG.New_originId
                                FROM dbo.New_originExtensionBase ORG_E WITH(NOLOCK)
	                                INNER JOIN dbo.New_originBase ORG WITH(NOLOCK) on ORG_E.New_originId = ORG.New_originId
                                WHERE ORG.DeletionStateCode = 0
	                                and ISNULL(ORG_E.New_IsDistribution, 0) = 1";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        list.Add((Guid)reader["New_originId"]);
                    }
                    conn.Close();
                }
            }
            return list;
        }
         
    }
}
