﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Effect.Crm.Logs;
using DML = NoProblem.Core.DataModel;
using System.Collections.Specialized;
using System.Web;

namespace NoProblem.Core.BusinessLogic.Origins
{
    public class WibiyaManager : XrmUserBase
    {
        #region Ctor
        public WibiyaManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Private Fields

        private delegate void SendCashOutAlertDelegate(DML.Xrm.new_wibiyacashout cashOut);

        #endregion

        #region Public Methods

        public void SaveHeadings(Guid originId, List<Guid> headings)
        {
            DataAccessLayer.Origin dal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            DML.Xrm.new_origin origin = new NoProblem.Core.DataModel.Xrm.new_origin(XrmDataContext);
            origin.new_originid = originId;
            RemoveOldHeadings(origin);
            AddNewSelectedHeadings(headings, origin);
        }

        public List<Guid> GetHeadingsForConfiguration(Guid originId)
        {
            DataAccessLayer.Origin dal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            var origin = dal.Retrieve(originId);
            List<Guid> retVal = new List<Guid>();
            foreach (var item in origin.new_origin_primaryexpertise)
            {
                retVal.Add(item.new_primaryexpertiseid);
            }
            return retVal;
        }

        public List<Guid> GetHeadingsForWidget(Guid originId)
        {
            List<Guid> retVal;
            try
            {
                retVal = GetHeadingsForConfiguration(originId);
                CompleteHeadingsListWithDefault(retVal);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaManager.GetHeadingsForWidget. Returning default headings.");
                retVal = GetDefaultHeadings();
            }
            return retVal;
        }

        public List<Guid> GetDefaultHeadings()
        {
            List<Guid> retVal = new List<Guid>();
            DataAccessLayer.WibiyaConfiguration dal = new NoProblem.Core.DataAccessLayer.WibiyaConfiguration(XrmDataContext);
            var configuration = dal.GetConfiguration();
            if (configuration.new_heading1id.HasValue)
            {
                retVal.Add(configuration.new_heading1id.Value);
            }
            if (configuration.new_heading2id.HasValue)
            {
                retVal.Add(configuration.new_heading2id.Value);
            }
            if (configuration.new_heading3id.HasValue)
            {
                retVal.Add(configuration.new_heading3id.Value);
            }
            if (configuration.new_heading4id.HasValue)
            {
                retVal.Add(configuration.new_heading4id.Value);
            }
            if (configuration.new_heading5id.HasValue)
            {
                retVal.Add(configuration.new_heading5id.Value);
            }
            if (configuration.new_heading6id.HasValue)
            {
                retVal.Add(configuration.new_heading6id.Value);
            }
            return retVal;
        }

        public DML.Origins.WibiyaLoginResponse Login(string email, string password, bool updateXmlInWibiya, string wibiyaToken, string siteId)
        {
            DataAccessLayer.Origin dal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            string firstName;
            Guid originId = dal.GetOriginIdInLogin(email, password, out firstName);
            DML.Origins.WibiyaLoginResponse response = new NoProblem.Core.DataModel.Origins.WibiyaLoginResponse();
            response.OriginId = originId;
            response.FirstName = firstName;
            if (updateXmlInWibiya & originId != Guid.Empty)
            {
                if (string.IsNullOrEmpty(wibiyaToken) || string.IsNullOrEmpty(siteId))
                {
                    throw new ArgumentException("If updateXmlInWibiya is True, then wibiyaToken and siteId most be not null nor empty.", "wibiyaToken or siteId");
                }
                var configuration = GetWibiyaConfiguration();
                UpdateOriginIdInWibiya(originId, wibiyaToken, siteId, configuration.new_apptype, configuration.new_appname, configuration.new_srcurl, configuration.new_xml);
            }
            return response;
        }

        public DML.Origins.WibiyaSignUpStatus SignUp(string firstName, string lastName, string email, string webSiteUrl, string businessName, string wibiyaToken, string siteId)
        {
            DataAccessLayer.Origin dal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            Guid existing = dal.GetOriginIdByEmail(email);
            if (existing != Guid.Empty)
            {
                return NoProblem.Core.DataModel.Origins.WibiyaSignUpStatus.AlreadyExists;
            }
            DML.Xrm.new_origin origin = new NoProblem.Core.DataModel.Xrm.new_origin();
            origin.new_name = string.IsNullOrEmpty(businessName) ? firstName + "_" + lastName : businessName;
            origin.new_firstname = firstName;
            origin.new_lastname = lastName;
            origin.new_email = email;
            origin.new_websiteurl = webSiteUrl;
            origin.new_password = Utils.LoginUtils.GenerateNewPassword();
            var configuration = GetWibiyaConfiguration();
            origin.new_parentoriginid = configuration.new_wibiyaoriginid.Value;
            dal.Create(origin);
            XrmDataContext.SaveChanges();
            DML.Origins.WibiyaSignUpStatus retVal = NoProblem.Core.DataModel.Origins.WibiyaSignUpStatus.OK;
            bool wibiyaUpdated = UpdateOriginIdInWibiya(origin.new_originid, wibiyaToken, siteId, configuration.new_apptype, configuration.new_appname, configuration.new_srcurl, configuration.new_xml);
            if (wibiyaUpdated)
            {
                bool emailSent = SendEmailWithPassword(origin);
                if (!emailSent)
                {
                    retVal = NoProblem.Core.DataModel.Origins.WibiyaSignUpStatus.EmailWithPasswordFailed;
                }
            }
            else
            {
                retVal = NoProblem.Core.DataModel.Origins.WibiyaSignUpStatus.CouldNotUpdateOriginIdInWibiya;
                dal.Delete(origin);
                XrmDataContext.SaveChanges();
            }
            return retVal;
        }

        public DML.Origins.WibiyaIncomingFundsData GetIncomingFundsData(Guid originId)
        {
            return CalculateIncomingFundsData(originId);
        }

        public Guid DoCashOut(Guid originId, string payPalEmail, bool rememberPayPalEmail, string currency)
        {
            DML.Origins.WibiyaIncomingFundsData data = CalculateIncomingFundsData(originId);
            if (data.CashEarned < 50)
            {
                throw new Exception(string.Format("Cannot cash out if cash earned is less than 50. Chash earned = {0}", data.CashEarned));
            }
            DML.Xrm.new_wibiyacashout cashOut = new NoProblem.Core.DataModel.Xrm.new_wibiyacashout();
            cashOut.statuscode = (int)DML.Xrm.new_wibiyacashout.WibiyaCashOutStatusCode.WaitingApproval;
            cashOut.new_cashamount = data.CashEarned;
            cashOut.new_currency = currency;
            cashOut.new_exposures = data.Views;
            cashOut.new_name = "Wibiya Cash Out";
            cashOut.new_originid = originId;
            cashOut.new_paypalemailaddress = payPalEmail;
            cashOut.new_rememberpaypalaccount = rememberPayPalEmail;
            cashOut.new_requests = data.Requests;
            DataAccessLayer.WibiyaCashOut cashOutDal = new NoProblem.Core.DataAccessLayer.WibiyaCashOut(XrmDataContext);
            cashOutDal.Create(cashOut);
            XrmDataContext.SaveChanges();
            SendCashOutAlertDelegate del = new SendCashOutAlertDelegate(SendCashOutAlert);
            del.BeginInvoke(cashOut, null, null);
            return cashOut.new_wibiyacashoutid;
        }

        #endregion

        #region Private Methods

        private NoProblem.Core.DataModel.Xrm.new_wibiyaconfiguration GetWibiyaConfiguration()
        {
            DataAccessLayer.WibiyaConfiguration wibiyaConfigDal = new NoProblem.Core.DataAccessLayer.WibiyaConfiguration(XrmDataContext);
            var configuration = wibiyaConfigDal.GetConfiguration();
            if (configuration == null)
            {
                throw new Exception("Wibiya configuration not found. Please create configuration.");
            }
            return configuration;
        }

        private bool UpdateOriginIdInWibiya(Guid originId, string wibiyaToken, string siteId, string appType, string appName, string srcUrl, string xml)
        {
            xml = xml.Replace("{0}", wibiyaToken);
            xml = xml.Replace("{1}", appType);
            xml = xml.Replace("{2}", appName);
            xml = xml.Replace("{3}", originId.ToString());
            xml = xml.Replace("{4}", siteId);
            xml = xml.Replace("{5}", srcUrl);
            string post_data = xml;
            //string post_data = string.Format(xml, wibiyaToken, appType, appName, originId, siteId, srcUrl);
            // 0 - token, 1 - apptype, 2 - appname, 3 - originid, 4 - siteid, 5 - srcurl.
            LogUtils.MyHandle.WriteToLog(8, "Sending to wibiya: " + post_data);
            string uri = "https://api.wibiya.com/Handlers/App.php";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            string parameters = "msg=" + HttpUtility.UrlEncode(post_data);
            request.ContentLength = parameters.Length;
            using (StreamWriter sw = new StreamWriter(request.GetRequestStream()))
            {
                sw.Write(parameters);
            }
            string result;
            bool allOk = true;
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        result = sr.ReadToEnd();
                    }
                }
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    result = httpResponse.StatusCode.ToString();
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        string text = sr.ReadToEnd();
                        result += " \n" + text;
                        allOk = false;
                        LogUtils.MyHandle.HandleException(e, "WebException in WibiyaManager.UpdateOriginIdInWibiya. result = {0}", result);
                    }
                }
            }
            if (allOk)
            {
                //                <response>
                //                  <header>
                //                      <result>10001</result>
                //                      <resultMessage>Operation performed successfuly</resultMessage>
                //                  </header>
                //                </response>
                try
                {
                    XElement doc = XElement.Parse(result);
                    if (doc.Element("header").Element("result").Value != "10001")
                    {
                        allOk = false;
                        throw new Exception(string.Format("Wibiya result was not 10001. result = {0}", result));
                    }
                }
                catch (Exception exc)
                {
                    allOk = false;
                    LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaManager.UpdateOriginIdInWibiya. Result not OK. result = {0}", result);
                }
            }
            return allOk;
        }

        private void SendCashOutAlert(DML.Xrm.new_wibiyacashout cashOut)
        {
            try
            {
                Notifications notifier = new Notifications(null);
                DML.Xrm.DataContext localContext = notifier.XrmDataContext;
                string body, subject;
                notifier.InstatiateTemplate(DML.TemplateNames.WIBIYA_CASH_OUT_ALERT, out subject, out body);
                DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(localContext);
                var origin = originDal.Retrieve(cashOut.new_originid.Value);
                body = string.Format(body, origin.new_name, origin.new_firstname, origin.new_email, origin.new_originid, cashOut.new_requests.Value, cashOut.new_exposures.Value, cashOut.new_cashamount.Value, cashOut.new_paypalemailaddress, cashOut.new_wibiyacashoutid, cashOut.new_currency, cashOut.createdon.Value.ToShortDateString());
                DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(localContext);
                string toEmail = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.WIBIYA_CASH_OUT_ALERT_EMAIL);
                bool sent = notifier.SendEmail(toEmail, body, subject, configDal);
                if (!sent)
                {
                    LogUtils.MyHandle.WriteToLog("Wibiya cash out alert was not sent. WibiyaCashOutId = {0}", cashOut.new_wibiyacashoutid);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WibiyaManager.SendCashOutAlert. WibiyaCashOutId = {0}", cashOut.new_wibiyacashoutid);
            }
        }

        private DML.Origins.WibiyaIncomingFundsData CalculateIncomingFundsData(Guid originId)
        {
            NoProblem.Core.DataModel.Origins.WibiyaIncomingFundsData response = new NoProblem.Core.DataModel.Origins.WibiyaIncomingFundsData();
            DataAccessLayer.WibiyaCashOut cashOutDal = new NoProblem.Core.DataAccessLayer.WibiyaCashOut(XrmDataContext);
            string payPalEmail;
            bool rememberPayPalAccount;
            DateTime? lastCashOutDate = cashOutDal.GetLastCashOutData(originId, out payPalEmail, out rememberPayPalAccount);
            if (rememberPayPalAccount)
            {
                response.PayPalEmail = payPalEmail;
            }
            else
            {
                response.PayPalEmail = string.Empty;
            }
            response.LastCashOut = lastCashOutDate;
            DataAccessLayer.Origin dal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            int requests = dal.GetBillableRequestsNumberFromDate(originId, lastCashOutDate);
            response.Requests = requests;
            DataAccessLayer.Exposure exposureDal = new NoProblem.Core.DataAccessLayer.Exposure(XrmDataContext);
            int exposures = exposureDal.GetExposuresCountFromOriginFromDate(originId, lastCashOutDate);
            response.Views = exposures;
            response.ConversionRate = exposures > 0 ? (double)requests / (double)exposures : 0;
            DataAccessLayer.WibiyaConfiguration wibiyaConfigurationDal = new NoProblem.Core.DataAccessLayer.WibiyaConfiguration(XrmDataContext);
            var configuration = wibiyaConfigurationDal.GetConfiguration();
            int cashEarned = 0;
            if (configuration.new_priceperrequest.HasValue && configuration.new_priceperrequest.Value > 0)
            {
                cashEarned = (int)(configuration.new_priceperrequest.Value * requests);
            }
            response.CashEarned = cashEarned;
            return response;
        }

        private void CompleteHeadingsListWithDefault(List<Guid> lst)
        {
            if (lst.Count() < 6)
            {
                var defaultHeadings = GetDefaultHeadings();
                int i = 0;
                while (lst.Count() < 6 && defaultHeadings.Count > i)
                {
                    var current = defaultHeadings[i];
                    if (!lst.Contains(current))
                    {
                        lst.Add(current);
                    }
                    i++;
                }
            }
        }

        private void AddNewSelectedHeadings(List<Guid> headings, DML.Xrm.new_origin origin)
        {
            foreach (var head in headings)
            {
                DML.Xrm.new_primaryexpertise heading = new NoProblem.Core.DataModel.Xrm.new_primaryexpertise(XrmDataContext);
                heading.new_primaryexpertiseid = head;
                XrmDataContext.AddLink(origin, "new_origin_primaryexpertise", heading);
            }
            XrmDataContext.SaveChanges();
        }

        private void RemoveOldHeadings(DML.Xrm.new_origin origin)
        {
            foreach (var item in origin.new_origin_primaryexpertise)
            {
                XrmDataContext.DeleteLink(origin, "new_origin_primaryexpertise", item);
            }
            XrmDataContext.SaveChanges();
        }

        private bool SendEmailWithPassword(NoProblem.Core.DataModel.Xrm.new_origin origin)
        {
            Notifications notifier = new Notifications(XrmDataContext);
            string toEmail = origin.new_email;
            string body, subject;
            notifier.InstatiateTemplate(DML.TemplateNames.WIBIYA_EMAIL_CONFIRMATION, out subject, out body);
            body = string.Format(body, origin.new_firstname, origin.new_password);
            bool emailSent = notifier.SendEmail(toEmail, body, subject);
            return emailSent;
        }

        #endregion
    }
}
