﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Origins.InjectionsCockpit;
using NoProblem.Core.DataModel.Injection;

namespace NoProblem.Core.BusinessLogic.Origins.InjectionsCockpit
{
    public class InjectionsManager : XrmUserBase
    {
        DataAccessLayer.Injection dal;

        public InjectionsManager(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            dal = new DataAccessLayer.Injection(XrmDataContext);
        }

        public InjectionData GetInjectionDetails(Guid injectionId)
        {
            var query = (from inj in dal.All
                         where inj.new_injectionid == injectionId
                         select new InjectionData
                          {
                              Name = inj.new_name,
                              ScriptBefore = inj.new_scriptbefore,
                              ScriptUrl = inj.new_url,
                              BlockWithUs = inj.new_blockwithus.HasValue ? inj.new_blockwithus.Value : false,
                              ScriptElementId = inj.new_scriptelementid
                          }
                          ).FirstOrDefault();
            query.InjectionId = injectionId;
            return query;
        }

        public Guid UpsertInjectionDetails(InjectionData data)
        {
            DataModel.Xrm.new_injection injection = DataModel.EntityFactories.InjectionFactory.Build(data.Name, data.ScriptBefore, data.ScriptUrl, 
                data.BlockWithUs, data.ScriptElementId);
            if (data.InjectionId == Guid.Empty)
            {
                dal.Create(injection);
            }
            else
            {
                injection.new_injectionid = data.InjectionId;
                dal.Update(injection);
            }
            XrmDataContext.SaveChanges();
            return injection.new_injectionid;
        }
    }
}
