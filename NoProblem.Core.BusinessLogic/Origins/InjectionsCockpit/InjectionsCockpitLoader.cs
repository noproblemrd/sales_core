﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Origins.InjectionsCockpit;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.BusinessLogic.Origins.InjectionsCockpit
{
    public class InjectionsCockpitLoader
    {
        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();

        public List<InjectionWithDAUs> GetInjectionsAndDAUs()
        {
            List<InjectionWithDAUs> retVal = new List<InjectionWithDAUs>();
            var reader = dal.ExecuteReader(queryGetInjectionsAndDAUs);
            foreach (var row in reader)
            {
                InjectionWithDAUs data = new InjectionWithDAUs();
                data.InjecitonId = row["id"] == DBNull.Value ? Guid.Empty : (Guid)row["id"];
                data.InjectionName = Convert.ToString(row["name"]);
                data.DAUs = (int)row["daus"];
                retVal.Add(data);
            }
            return retVal;
        }
        
        private const string queryGetInjectionsAndDAUs =
            @"
declare @maxDate datetime
select @maxDate = MAX(date) from [DynamicNoProblem].[dbo].[Exposure]

SELECT
      isnull(SUM([Visitor]), 0) daus
      ,inj.New_injectionId id
      ,inj.New_name name
  FROM [DynamicNoProblem].[dbo].[Exposure] e
      inner join DausInjection di on  di.DausId = e.Id
      right join New_injection inj on inj.New_injectionId = di.InjectionId and isnull(Date, @maxDate) = @maxDate
      where inj.deletionstatecode = 0
      group by 
      inj.New_injectionId
      ,inj.New_name
    ORDER BY inj.New_name
";

        /*
        private const string queryGetInjectionsAndDAUs = @"
declare @maxDate datetime
select @maxDate = MAX(date) from [DynamicNoProblem].[dbo].[Exposure]

SELECT injection.daus
    ,injection.id
    ,injection.name
FROM(
SELECT isnull(SUM([Visitor]), 0) daus
    ,null id
    ,'Ad Engine Popup' name
FROM [DynamicNoProblem].[dbo].[Exposure] e
    right join dbo.New_origin o on o.New_originId = e.OriginId and isnull(Date, @maxDate) = @maxDate
WHERE o.DeletionStateCode = 0
    and New_AdEngineEnabled = 1
UNION ALL
SELECT
  isnull(SUM([Visitor]), 0) daus
  ,inj.New_injectionId id
  ,inj.New_name name
FROM [DynamicNoProblem].[dbo].[Exposure] e
  inner join DausInjection di on  di.DausId = e.Id
  right join New_injection inj on inj.New_injectionId = di.InjectionId and isnull(Date, @maxDate) = @maxDate
  where inj.deletionstatecode = 0
  group by 
  inj.New_injectionId
  ,inj.New_name) injection

ORDER BY injection.name";
         * * */
    }
}
