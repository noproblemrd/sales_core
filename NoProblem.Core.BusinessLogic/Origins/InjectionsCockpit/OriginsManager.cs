﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Origins.InjectionsCockpit;

namespace NoProblem.Core.BusinessLogic.Origins.InjectionsCockpit
{
    public class OriginsManager : XrmUserBase
    {
        private DataAccessLayer.Origin dal;

        public OriginsManager(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            dal = new DataAccessLayer.Origin(XrmDataContext);
        }

        public OriginData GetOriginDetails(Guid originId)
        {
            var query = (from or in dal.All
                         where or.new_originid == originId
                         select new OriginData
                         {
                             Name = or.new_name,
                             UseSameInUpsale = or.new_usesameinupsale,
                             IsInFinancialDashboard = or.new_isinfinancialdashboard,
                             SliderBroughtToYouByName = or.new_sliderbroughtbyname,
                             IncludeCampaign = or.new_includecampaign,
                             ShowNoProblemInSlider = or.new_shownoprobleminslider,
                             IsDistribution = or.new_isdistribution,
                             IsApiSeller = or.new_isapiseller,
                             CostPerRequest = or.new_costperrequest,
                             CostPerInstall = or.new_costofinstallation,
                             RevenueShare = or.new_revenueshare,
                             AdEngineEnabled = or.new_adengineenabled,
                             SendInstallationReport = or.new_sendinstallationdailyemail,
                             EmailToSendInstallationReport = or.new_installationdailyemail,
                             Factor = or.new_factor.HasValue ? or.new_factor : 1,
                             MaxFactor = or.new_maxfactor.HasValue ? or.new_maxfactor : 1,
                             EmailToSendMaxFactorReport = or.new_maxfactordailyemail,
                             SendMaxFactorReport = or.new_maxfactorsenddailyemail
                         }).FirstOrDefault();
            query.Id = originId;
            return query;
        }

        public Guid UpsertOriginDetails(OriginData data)
        {
            DataModel.Xrm.new_origin origin = DataModel.EntityFactories.OriginFactory.Build(data);
            if (data.Id == Guid.Empty)
            {
                dal.Create(origin);
            }
            else
            {
                origin.new_originid = data.Id;
                dal.Update(origin);
            }
            XrmDataContext.SaveChanges();
            return origin.new_originid;
        }
    }
}
