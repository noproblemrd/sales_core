﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.PublisherPortal;
using NoProblem.Core.DataModel.Injection;

namespace NoProblem.Core.BusinessLogic.Origins.InjectionsCockpit
{
    public class ConnectionsManager : XrmUserBase
    {
        private DataAccessLayer.InjectionOrigin dal;

        public ConnectionsManager(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {
            dal = new DataAccessLayer.InjectionOrigin(XrmDataContext);
        }

        public List<InjectionOriginData> GetOriginsByInjection(Guid injectionId)
        {
            List<InjectionOriginData> retVal = new List<InjectionOriginData>();
            var query = from con in dal.All
                        where con.new_injectionid == injectionId
                        select new
                        {
                            OriginId = con.new_originid.Value,
                            OriginName = con.new_origin_injectionorigin.new_name,
                            HitCounter = con.new_hitcounter.HasValue ? con.new_hitcounter.Value : 0,
                            DayCounter = con.new_daycounter.HasValue ? con.new_daycounter.Value : 0
                        };
            foreach (var item in query)
            {
                InjectionOriginData d = new InjectionOriginData();
                d.OriginId = item.OriginId;
                d.OriginName = item.OriginName;
                d.DayCounter = item.DayCounter;
                d.HitCounter = item.HitCounter;
                d.InjectionId = injectionId;
                retVal.Add(d);
            }
            return retVal;
        }

        public List<InjectionOriginData> GetInjectionsByOrigin(Guid originId)
        {
            List<InjectionOriginData> retVal = new List<InjectionOriginData>();
            var query = from con in dal.All
                        where con.new_originid == originId
                        select new
                        {
                            InjectionId = con.new_injectionid.Value,
                            InjectionName = con.new_injection_injectionorigin.new_name,
                            HitCounter = con.new_hitcounter.HasValue ? con.new_hitcounter.Value : 0,
                            DayCounter = con.new_daycounter.HasValue ? con.new_daycounter.Value : 0
                        };
            foreach (var item in query)
            {
                InjectionOriginData d = new InjectionOriginData();
                d.InjectionId = item.InjectionId;
                d.InjectionName = item.InjectionName;
                d.OriginId = originId;
                d.HitCounter = item.HitCounter;
                d.DayCounter = item.DayCounter;
                retVal.Add(d);
            }
            return retVal;
        }

        public void UpdateConnectionsByOrigin(Guid originId, List<InjectionOriginData> injectionIds)
        {
            var current = GetInjectionOriginsByOrigin(originId);
            var givenIds = injectionIds.Select(x => x.InjectionId);

            foreach (var item in current)
            {
                if (!givenIds.Contains(item.new_injectionid.Value))
                {
                    dal.Delete(item);
                }
            }
            XrmDataContext.SaveChanges();

            foreach (InjectionOriginData givenData in injectionIds)
            {
                bool contains = false;
                foreach (var relation in current)
                {
                    if (relation.new_injectionid.Value == givenData.InjectionId)
                    {
                        contains = true;
                        UpdateCounters(givenData, relation);
                        break;
                    }
                }
                if (!contains)
                {
                    DataModel.Xrm.new_injectionorigin newRelation = new DataModel.Xrm.new_injectionorigin(XrmDataContext);
                    newRelation.new_injectionid = givenData.InjectionId;
                    newRelation.new_originid = originId;
                    newRelation.new_hitcounter = givenData.HitCounter;
                    newRelation.new_daycounter = givenData.DayCounter;
                    dal.Create(newRelation);
                }
            }
            XrmDataContext.SaveChanges();
        }

        public void UpdateConnectionsByInjection(Guid injectionId, List<InjectionOriginData> originIds)
        {
            var current = GetInjectionOriginsByInjection(injectionId);
            var givenIds = originIds.Select(x => x.OriginId);

            foreach (var item in current)
            {
                if (!givenIds.Contains(item.new_originid.Value))
                {
                    dal.Delete(item);
                }
            }
            XrmDataContext.SaveChanges();

            foreach (InjectionOriginData givenData in originIds)
            {
                bool contains = false;
                foreach (var relation in current)
                {
                    if (relation.new_originid.Value == givenData.OriginId)
                    {
                        contains = true;
                        UpdateCounters(givenData, relation);
                        break;
                    }
                }
                if (!contains)
                {
                    DataModel.Xrm.new_injectionorigin newRelation = new DataModel.Xrm.new_injectionorigin(XrmDataContext);
                    newRelation.new_injectionid = injectionId;
                    newRelation.new_originid = givenData.OriginId;
                    newRelation.new_daycounter = givenData.DayCounter;
                    newRelation.new_hitcounter = givenData.HitCounter;
                    dal.Create(newRelation);
                }
            }
            XrmDataContext.SaveChanges();
        }

        private void UpdateCounters(InjectionOriginData givenData, DataModel.Xrm.new_injectionorigin relation)
        {
            if (relation.new_hitcounter != givenData.HitCounter
                || relation.new_daycounter != givenData.DayCounter)
            {
                relation.new_daycounter = givenData.DayCounter;
                relation.new_hitcounter = givenData.HitCounter;
                dal.Update(relation);
            }
        }

        private List<DataModel.Xrm.new_injectionorigin> GetInjectionOriginsByOrigin(Guid originId)
        {
            List<DataModel.Xrm.new_injectionorigin> retVal = new List<DataModel.Xrm.new_injectionorigin>();
            var query = from conn in dal.All
                        where conn.new_originid == originId
                        select new { conn.new_injectionoriginid, conn.new_injectionid, conn.new_hitcounter, conn.new_daycounter };
            foreach (var item in query)
            {
                retVal.Add(
                    new DataModel.Xrm.new_injectionorigin(XrmDataContext)
                    {
                        new_injectionoriginid = item.new_injectionoriginid,
                        new_injectionid = item.new_injectionid,
                        new_daycounter = item.new_daycounter,
                        new_hitcounter = item.new_hitcounter
                    });
            }
            return retVal;
        }

        private List<DataModel.Xrm.new_injectionorigin> GetInjectionOriginsByInjection(Guid injectionId)
        {
            List<DataModel.Xrm.new_injectionorigin> retVal = new List<DataModel.Xrm.new_injectionorigin>();
            var query = from conn in dal.All
                        where conn.new_injectionid == injectionId
                        select new { conn.new_injectionoriginid, conn.new_originid };
            foreach (var item in query)
            {
                retVal.Add(
                    new DataModel.Xrm.new_injectionorigin(XrmDataContext)
                    {
                        new_injectionoriginid = item.new_injectionoriginid,
                        new_originid = item.new_originid
                    });
            }
            return retVal;
        }
    }
}
