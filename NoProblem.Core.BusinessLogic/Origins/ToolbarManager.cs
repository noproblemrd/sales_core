﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NoProblem.Core.DataModel.PublisherPortal;
using NoProblem.Core.DataModel;
using NoProblem.Core.BusinessLogic.Utils;


namespace NoProblem.Core.BusinessLogic.Origins
{
    public class ToolbarManager : XrmUserBase
    {
        public ToolbarManager(DataModel.Xrm.DataContext xrmDataContext)
            :base(xrmDataContext)
        {
         
        }
        public List<DataModel.PublisherPortal.GuidStringPair> GetToolbarid(Guid OriginId)
        {
            DataAccessLayer.ToolbarId dal = new NoProblem.Core.DataAccessLayer.ToolbarId(XrmDataContext);
            return dal.GetToolbaridByOrigin(OriginId);
        }
    }
}
