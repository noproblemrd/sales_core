﻿using System;
using System.Collections.Generic;
using System.Linq;
using Effect.Crm.Logs;
using DML = NoProblem.Core.DataModel;
using System.Text;
using NoProblem.Core.BusinessLogic.Utils.Csv;

namespace NoProblem.Core.BusinessLogic.DataExport
{
    internal class DataRetriever : XrmUserBase
    {
        #region Ctor
        internal DataRetriever(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        private string separator = " / ";

        #region Internal Methods

        internal List<CsvRow> GetCandidatesData(IEnumerable<DML.Xrm.account> candidatesList)
        {
            List<CsvRow> rows = new List<CsvRow>();
            CsvRow columns = new CsvRow();
            columns.Add("ID");
            columns.Add("Company_name");
            columns.Add("NIP");
            columns.Add("Telephone");
            columns.Add("Email");
            columns.Add("Address");
            columns.Add("Headings");
            columns.Add("Regions");
            columns.Add("Origin");
            rows.Add(columns);
            foreach (var candidate in candidatesList)
            {
                try
                {
                    CsvRow row = new CsvRow();
                    row.Add(GetStringValue(candidate.accountnumber));
                    row.Add(GetStringValue(candidate.name));
                    row.Add(GetStringValue(candidate.new_accountgovernmentid));
                    row.Add(GetStringValue(candidate.telephone1));
                    row.Add(GetStringValue(candidate.emailaddress1));
                    row.Add(GetStringValue(candidate.address1_line1 + " " + candidate.address1_line2 + " " + candidate.address1_city + " " + candidate.address1_county));
                    row.Add(GetAllSuppierHeadings(candidate));
                    row.Add(GetSupplierLevel1Regions(candidate));
                    row.Add(GetOrigin(candidate));
                    rows.Add(row);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to add row in Candidates Info data");
                }
            }
            return rows;
        }        

        internal List<CsvRow> GetAdvertiserGeneralData(IEnumerable<DML.Xrm.account> suppliersList)
        {
            List<CsvRow> rows = new List<CsvRow>();
            CsvRow columns = new CsvRow();
            columns.Add("NP_id");
            columns.Add("Biz_id");
            columns.Add("ID");
            columns.Add("Company_name");
            columns.Add("Contact_name");
            columns.Add("Mobile_phone");
            columns.Add("Email");
            columns.Add("Province");
            columns.Add("City");
            columns.Add("Street");
            columns.Add("House_number");
            columns.Add("Postcode");
            columns.Add("Web_Address");
            columns.Add("Other_phone");
            columns.Add("Balance");
            columns.Add("Sunday");
            columns.Add("Monday");
            columns.Add("Tuesday");
            columns.Add("Wednesday");
            columns.Add("Thursday");
            columns.Add("Friday");
            columns.Add("Saturday");
            columns.Add("Origin");
            rows.Add(columns);
            foreach (var supplier in suppliersList)
            {
                try
                {
                    CsvRow row = new CsvRow();
                    row.Add(supplier.accountid.ToString());
                    row.Add(GetStringValue(supplier.new_bizid));
                    row.Add(GetStringValue(supplier.new_accountgovernmentid));
                    row.Add(GetStringValue(supplier.name));
                    row.Add(supplier.new_firstname + " " + supplier.new_lastname);
                    row.Add(GetStringValue(supplier.telephone1));
                    row.Add(GetStringValue(supplier.emailaddress1));
                    row.Add(GetStringValue(supplier.address1_county));
                    row.Add(GetStringValue(supplier.address1_city));
                    row.Add(GetStringValue(supplier.address1_line1));
                    row.Add(GetStringValue(supplier.address1_line2));
                    row.Add(GetStringValue(supplier.address1_postalcode));
                    row.Add(GetStringValue(supplier.websiteurl));
                    row.Add(GetStringValue(supplier.telephone2));
                    row.Add(supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value.ToString() : "");
                    row.Add(supplier.new_status.HasValue ? (supplier.new_status.Value == (int)DML.Xrm.account.SupplierStatus.Approved ? "Approved" : "Inactive") : "");
                    row.Add(IsNotWorkingDay(supplier.new_sundayavailablefrom, supplier.new_sundayavailableto) ? string.Empty : supplier.new_sundayavailablefrom + " - " + supplier.new_sundayavailableto);
                    row.Add(IsNotWorkingDay(supplier.new_mondayavailablefrom, supplier.new_mondayavailableto) ? string.Empty : supplier.new_mondayavailablefrom + " - " + supplier.new_mondayavailableto);
                    row.Add(IsNotWorkingDay(supplier.new_tuesdayavailablefrom, supplier.new_tuesdayavailableto) ? string.Empty : supplier.new_tuesdayavailablefrom + " - " + supplier.new_tuesdayavailableto);
                    row.Add(IsNotWorkingDay(supplier.new_wednesdayavailablefrom, supplier.new_wednesdayavailableto) ? string.Empty : supplier.new_wednesdayavailablefrom + " - " + supplier.new_wednesdayavailableto);
                    row.Add(IsNotWorkingDay(supplier.new_thursdayavailablefrom, supplier.new_thursdayavailableto) ? string.Empty : supplier.new_thursdayavailablefrom + " - " + supplier.new_thursdayavailableto);
                    row.Add(IsNotWorkingDay(supplier.new_fridayavailablefrom, supplier.new_fridayavailableto) ? string.Empty : supplier.new_fridayavailablefrom + " - " + supplier.new_fridayavailableto);
                    row.Add(IsNotWorkingDay(supplier.new_saturdayavailablefrom, supplier.new_saturdayavailableto) ? string.Empty : supplier.new_saturdayavailablefrom + " - " + supplier.new_saturdayavailableto);
                    row.Add(GetOrigin(supplier));
                    rows.Add(row);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to add row in Advertisers Info data");
                }
            }
            return rows;
        }

        internal List<CsvRow> GetDepositData()
        {
            List<CsvRow> rows = new List<CsvRow>();
            CsvRow columns = new CsvRow();
            columns.Add("Biz_id");
            columns.Add("Date");
            columns.Add("Amount");
            columns.Add("Deposit_id");
            columns.Add("Transaction_id");
            rows.Add(columns);
            DataAccessLayer.SupplierPricing suppPricingDal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            IEnumerable<DML.Xrm.new_supplierpricing> pricings = suppPricingDal.GetAllSupplierPricings();
            pricings = from pri in pricings
                       where
                       string.IsNullOrEmpty(pri.new_account_new_supplierpricing.new_bizid) == false
                       &&
                       IsExported(pri.new_isexported) == false
                       &&
                       pri.new_voucherid.HasValue == false
                       select pri;
            foreach (var pri in pricings)
            {
                try
                {
                    CsvRow row = new CsvRow();
                    row.Add(pri.new_account_new_supplierpricing.new_bizid);
                    row.Add(FixDateToLocal(pri.createdon.Value).ToString());
                    row.Add(pri.new_total.Value.ToString());
                    row.Add(GetStringValue(pri.new_name));
                    if (string.IsNullOrEmpty(pri.new_transactionid) && pri.new_userid.HasValue && pri.new_user_supplierpricing != null)
                    {
                        row.Add("manually " + pri.new_user_supplierpricing.name);
                    }
                    else
                    {
                        row.Add(GetStringValue(pri.new_transactionid));
                    }
                    rows.Add(row);
                    pri.new_isexported = true;
                    suppPricingDal.Update(pri);
                    XrmDataContext.SaveChanges();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to add row in Deposits data");
                }
            }
            return rows;
        }

        internal List<CsvRow> GetCallsData()
        {
            List<CsvRow> rows = new List<CsvRow>();
            CsvRow columns = new CsvRow();
            columns.Add("Biz_id");
            columns.Add("Date");
            columns.Add("Price");
            rows.Add(columns);
            DataAccessLayer.IncidentAccount incAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            IEnumerable<DML.Xrm.new_incidentaccount> calls = incAccDal.GetAllCalls();
            calls = from call in calls
                    where
                    string.IsNullOrEmpty(call.new_account_new_incidentaccount.new_bizid) == false
                    &&
                    IsExported(call.new_iscallexported) == false
                    select call;
            foreach (var call in calls)
            {
                try
                {
                    CsvRow row = new CsvRow();
                    row.Add(call.new_account_new_incidentaccount.new_bizid);
                    row.Add(FixDateToLocal(call.createdon.Value).ToString());
                    row.Add(call.new_potentialincidentprice.Value.ToString());
                    rows.Add(row);
                    call.new_iscallexported = true;
                    incAccDal.Update(call);
                    XrmDataContext.SaveChanges();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to add row in Calls data");
                }
            }
            return rows;
        }

        internal List<CsvRow> GetHeadingData(IEnumerable<DML.Xrm.account> suppliersList)
        {
            List<CsvRow> rows = new List<CsvRow>();
            CsvRow columns = new CsvRow();
            columns.Add("Biz_id");
            columns.Add("Heading_id");
            columns.Add("Price");
            columns.Add("Bidding");
            rows.Add(columns);
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            foreach (var supplier in suppliersList)
            {
                IEnumerable<DML.Xrm.new_accountexpertise> accExps = accExpDal.GetActiveByAccount(supplier.accountid);
                foreach (var accExp in accExps)
                {
                    try
                    {
                        CsvRow row = new CsvRow();
                        row.Add(supplier.new_bizid);
                        row.Add(accExp.new_new_primaryexpertise_new_accountexpertise.new_code);
                        row.Add(accExp.new_incidentprice.HasValue ? accExp.new_incidentprice.Value.ToString() : "0");
                        row.Add(accExp.new_incidentpricechangeable.HasValue && accExp.new_incidentpricechangeable.Value ? "True" : "False");
                        rows.Add(row);
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Failed to add row in Headings data");
                    }
                }
            }
            return rows;
        }

        internal List<CsvRow> GetRefundsData()
        {
            List<CsvRow> rows = new List<CsvRow>();
            CsvRow columns = new CsvRow();
            columns.Add("Biz_id");
            columns.Add("Date");
            columns.Add("Amount");
            rows.Add(columns);
            DataAccessLayer.IncidentAccount inciAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            IEnumerable<DML.Xrm.new_incidentaccount> refunds = inciAccDal.GetRefundsByRefundStatus(DML.Xrm.new_incidentaccount.RefundSatus.Approved);
            refunds = from item in refunds
                      where
                      string.IsNullOrEmpty(item.new_account_new_incidentaccount.new_bizid) == false
                      &&
                      IsExported(item.new_isrefundexported) == false
                      select item;
            foreach (var refu in refunds)
            {
                try
                {
                    CsvRow row = new CsvRow();
                    row.Add(refu.new_account_new_incidentaccount.new_bizid);
                    row.Add(refu.new_refundprocesseddate.HasValue ? FixDateToLocal(refu.new_refundprocesseddate.Value).ToString() : "");
                    row.Add(refu.new_potentialincidentprice.Value.ToString());
                    rows.Add(row);
                    refu.new_isrefundexported = true;
                    inciAccDal.Update(refu);
                    XrmDataContext.SaveChanges();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to add row in Refunds data");
                }
            }
            return rows;
        }

        internal List<CsvRow> GetRegionsData(IEnumerable<DML.Xrm.account> suppliersList)
        {
            List<CsvRow> rows = new List<CsvRow>();
            CsvRow columns = new CsvRow();
            columns.Add("Biz_id");
            columns.Add("Region_id");
            rows.Add(columns);
            foreach (var supplier in suppliersList)
            {
                try
                {
                    List<CsvRow> supplierRows = GetSupplierRegionsRows(supplier);
                    rows.AddRange(supplierRows);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to add supplier rows in Region data");
                }
            }
            return rows;
        }

        internal List<CsvRow> GetRequestsData()
        {
            List<CsvRow> rows = new List<CsvRow>();
            CsvRow columns = new CsvRow();
            columns.Add("Request_id");
            columns.Add("Heading_id");
            columns.Add("Description");
            rows.Add(columns);
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            IEnumerable<DML.Xrm.incident> incidents = incidentDal.GetValidFinishedIncidents();
            incidents = incidents.Where(x => IsExported(x.new_isexported) == false);
            incidents = incidents.Where(x => x.new_primaryexpertiseid.HasValue);
            foreach (var inci in incidents)
            {
                try
                {
                    CsvRow row = new CsvRow();
                    row.Add(GetStringValue(inci.ticketnumber));
                    row.Add(GetStringValue(inci.new_new_primaryexpertise_incident.new_code));
                    row.Add(GetStringValue(inci.description));
                    rows.Add(row);
                    inci.new_isexported = true;
                    incidentDal.Update(inci);
                    XrmDataContext.SaveChanges();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to add row in Requests data");
                }
            }
            return rows;
        }

        #endregion

        #region Private Methods

        private string GetOrigin(NoProblem.Core.DataModel.Xrm.account supplier)
        {
            string retVal = string.Empty;
            if (supplier.new_originid.HasValue)
            {
                retVal = supplier.new_origin_account.new_name;
            }
            return retVal;
        }

        private bool IsNotWorkingDay(string from, string to)
        {
            return 
                (from == "1:00" && to == "1:01")
                ||
                string.IsNullOrEmpty(from)
                ||
                string.IsNullOrEmpty(to);
        }

        private string GetAllSuppierHeadings(NoProblem.Core.DataModel.Xrm.account supplier)
        {
            StringBuilder builder = new StringBuilder(string.Empty);
            DataAccessLayer.AccountExpertise dal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            var headings = dal.GetActiveHeadingNamesForAccount(supplier.accountid);
            for (int i = 0; i < headings.Count; i++)
            {
                builder.Append(headings[i]);
                if (i < headings.Count - 1)
                {
                    builder.Append(separator);
                }
            }
            return builder.ToString();
        }

        private string GetSupplierLevel1Regions(DataModel.Xrm.account supplier)
        {
            StringBuilder builder = new StringBuilder(string.Empty);
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            List<string> regions = dal.GetLevel1RegionNamesForAccount(supplier.accountid);
            for (int i = 0; i < regions.Count; i++)
            {
                builder.Append(regions[i]);
                if (i < regions.Count - 1)
                {
                    builder.Append(separator);
                }
            }
            return builder.ToString();
        }

        private bool IsExported(bool? exported)
        {
            bool retVal = false;
            if (exported.HasValue && exported.Value == true)
            {
                retVal = true;
            }
            return retVal;
        }

        private List<CsvRow> GetSupplierRegionsRows(NoProblem.Core.DataModel.Xrm.account supplier)
        {
            Regions.RegionsManager regionManager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(XrmDataContext);
            IEnumerable<DML.Xrm.new_region> regions = regionManager.GetSupplierWorkingRegions(supplier);
            List<CsvRow> rows = new List<CsvRow>();
            foreach (var reg in regions)
            {
                CsvRow row = new CsvRow();
                row.Add(supplier.new_bizid);
                row.Add(GetStringValue(reg.new_code));
                rows.Add(row);
            }
            return rows;
        }

        private string GetStringValue(string str)
        {
            string retVal = str;
            if (string.IsNullOrEmpty(str))
            {
                retVal = "";
            }
            return retVal;
        }

        #endregion
    }
}
