﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DML = NoProblem.Core.DataModel;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils.Csv;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic.DataExport
{
    public class DataExportManager : XrmUserBase
    {
        #region Fields and consts

        private const string localPathBase = "D:\\TempExportData";
        private readonly string localDirectory;
        private readonly string urlBase;
        private readonly string directoryName;
        private readonly string userName;
        private readonly string password;
        private const string fileNameTimeFormat = "dd-MM-yyyy_HH-mm_UCT";
        private const string directoryNameBase = "NoProblemDataExport_";
        private const string fileExtension = ".txt";
        private const string localPathDelimiter = "\\";
        private const string GeneralInfoFileNameBase = "Advertisers_";
        private const string DepositsFileNameBase = "Deposits_";
        private const string CallsFileNameBase = "Calls_";
        private const string HeadingsFileNameBase = "Headings_";
        private const string RegionsFileNameBase = "Regions_";
        private const string RefundsFileNameBase = "Refunds_";
        private const string RequestsFileNameBase = "Requests_";
        private const string CandidateFileNameBase = "Candidates_";

        #endregion

        #region Delegates
        private delegate void ExportAsync();
        #endregion

        #region Ctor
        public DataExportManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            DataAccessLayer.ConfigurationSettings settingsDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(this.XrmDataContext);
            urlBase = settingsDal.GetConfigurationSettingValue(DML.ConfigurationKeys.FTP_URL_BASE);
            userName = settingsDal.GetConfigurationSettingValue(DML.ConfigurationKeys.FTP_USERNAME);
            password = settingsDal.GetConfigurationSettingValue(DML.ConfigurationKeys.FTP_PASSWORD);
            directoryName = directoryNameBase + DateTime.Now.ToString(fileNameTimeFormat);
            localDirectory = System.Configuration.ConfigurationManager.AppSettings["OrganizationName"];
            CreateLocalDirectory();
        }
        #endregion

        #region Public Methods

        public void Export()
        {
            ExportAsync asyncExport = new ExportAsync(ExportPrivate);
            asyncExport.BeginInvoke(null, null);            
        }

        public void UpdateSupplierBizId(NoProblem.Core.DataModel.OnecallUtilities.UpdateSupplierBizIdRequest request)
        {
            if (string.IsNullOrEmpty(request.Biz_Id) || request.NP_Id == Guid.Empty)
            {
                throw new Exception("One of the parameters is missing");
            }
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.GetByBizId(request.Biz_Id);
            if (supplier != null)
            {
                throw new Exception("Duplicate Biz_id");
            }
            supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
            supplier.accountid = request.NP_Id;
            supplier.new_bizid = request.Biz_Id;
            accountRepositoryDal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        #endregion

        #region Private Methods

        private void ExportPrivate()
        {
            try
            {
                FtpFileUploader uploader = new FtpFileUploader(urlBase, userName, password);
                bool directoryCreated = uploader.CreateDirectory(directoryName);
                if (directoryCreated)
                {
                    DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                    IEnumerable<DML.Xrm.account> suppliersList = accountRepositoryDal.GetSuppliersThatCompletedRegistration();
                    DataRetriever retriever = new DataRetriever(XrmDataContext);
                    List<CsvRow> rows = retriever.GetAdvertiserGeneralData(suppliersList);
                    CreateAndSendFile(rows, GeneralInfoFileNameBase);

                    suppliersList = from supplier in suppliersList
                                    where
                                    string.IsNullOrEmpty(supplier.new_bizid) == false
                                    select supplier;

                    rows = retriever.GetDepositData();
                    CreateAndSendFile(rows, DepositsFileNameBase);
                    rows = retriever.GetCallsData();
                    CreateAndSendFile(rows, CallsFileNameBase);
                    rows = retriever.GetHeadingData(suppliersList);
                    CreateAndSendFile(rows, HeadingsFileNameBase);
                    rows = retriever.GetRefundsData();
                    CreateAndSendFile(rows, RefundsFileNameBase);
                    rows = retriever.GetRequestsData();
                    CreateAndSendFile(rows, RequestsFileNameBase);
                    rows = retriever.GetRegionsData(suppliersList);
                    CreateAndSendFile(rows, RegionsFileNameBase);
                    IEnumerable<DML.Xrm.account> candidateList = accountRepositoryDal.GetSuppliersByStatus(DML.Xrm.account.SupplierStatus.Candidate);
                    rows = retriever.GetCandidatesData(candidateList);
                    CreateAndSendFile(rows, CandidateFileNameBase);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "ExportPrivate failed with exception.");
            }
        }

        private void CreateAndSendFile(List<CsvRow> rows, string fileNameBase)
        {
            string fileName = fileNameBase + DateTime.Now.ToString(fileNameTimeFormat) + fileExtension;
            string filePath = localPathBase + localPathDelimiter + localDirectory + localPathDelimiter + fileName;
            using (FileStream file = File.Create(filePath))
            {
                using (CsvFileWriter writer = new CsvFileWriter(file))
                {
                    foreach (var row in rows)
                    {
                        writer.WriteRow(row);
                    }
                }
            }
            using (FileStream file = File.Open(filePath, FileMode.Open))
            {
                FtpFileUploader uploader = new FtpFileUploader(urlBase, userName, password);
                uploader.UploadFile(file, fileName, directoryName);
            }
        }

        private void CreateLocalDirectory()
        {
            if (!Directory.Exists(localPathBase))
            {
                Directory.CreateDirectory(localPathBase);
            }
            if (!Directory.Exists(localPathBase + localPathDelimiter + localDirectory))
            {
                Directory.CreateDirectory(localPathBase + localPathDelimiter + localDirectory);
            }
        }

        #endregion
    }
}
