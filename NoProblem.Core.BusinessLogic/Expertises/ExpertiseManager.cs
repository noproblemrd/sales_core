﻿using System;
using System.Collections.Generic;
using System.Linq;
using NoProblem.Core.DataModel.Expertise;
using DML = NoProblem.Core.DataModel;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Expertises
{
    public class ExpertiseManager : XrmUserBase
    {
        #region Ctor
        public ExpertiseManager(DML.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        #region Contst
        private const string ACTIVE = "Active";
        #endregion

        #region Public Methods

        public bool VerifyCategoryCodeExists(string code)
        {
            DataAccessLayer.PrimaryExpertise dal = new DataAccessLayer.PrimaryExpertise(XrmDataContext);
            Guid categoryId = (from cat in dal.All
                               where cat.new_code == code
                               select cat.new_primaryexpertiseid).FirstOrDefault();
            return categoryId != Guid.Empty;
        }

        public DML.Xrm.new_channel.ChannelCode GetChannelForHeading(string headingCode, int headingLevel)
        {
            DML.Xrm.new_channel.ChannelCode retVal = NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Call;
            if (headingLevel == 1)
            {
                DataAccessLayer.PrimaryExpertise priExpsertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
                retVal = priExpsertiseDal.GetChannelCodeFromPrimaryExpertise(headingCode);
            }
            else if (headingLevel == 2)
            {
                DataAccessLayer.SecondaryExpertise secExpDal = new NoProblem.Core.DataAccessLayer.SecondaryExpertise(XrmDataContext);
                retVal = secExpDal.GetChannelCodeFromSecondaryExpertise(headingCode);
            }
            return retVal;
        }

        public int GetRankingInExpertise(Guid expertiseId, decimal price, Guid supplierId)
        {
            DataAccessLayer.AccountExpertise accExperDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            int rank = accExperDal.GetRanking(expertiseId, supplierId, price);
            return rank;
        }

        public GetSupplierPricesResponse GetSupplierPrices(GetSupplierPricesRequest request)
        {
            GetSupplierPricesResponse response = new GetSupplierPricesResponse();
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(request.SupplierId);
            response.StageInRegistration = supplier.new_stageinregistration.HasValue ? supplier.new_stageinregistration.Value : 1;
            List<ExpertiseContainer> dataList = new List<ExpertiseContainer>();
            DataAccessLayer.AccountExpertise accountExpertiseDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            IEnumerable<DML.Xrm.new_accountexpertise> accExps = accountExpertiseDal.GetActiveByAccount(request.SupplierId);
            DataAccessLayer.PredefinedPrice preDefinedPriceDal = new NoProblem.Core.DataAccessLayer.PredefinedPrice(XrmDataContext);
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            bool dontUseAvgAsMinPrice = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.DONT_USE_AVERAGE_AS_MIN_PRICE) == "1";
            bool noMinPriceLimit = supplier.new_nominpricelimit.HasValue ? supplier.new_nominpricelimit.Value : false;
            foreach (var accExp in accExps)
            {
                if (accExp.new_primaryexpertiseid.HasValue == false)
                {
                    continue;
                }
                ExpertiseContainer data = CreateExpertiseContainer(expertiseDal, preDefinedPriceDal, accExp, dontUseAvgAsMinPrice, noMinPriceLimit);
                dataList.Add(data);
            }
            response.Expertises = dataList.OrderBy(x => x.ExpertiseName).ToList();
            response.RankingNotification = supplier.new_rankingnotification.HasValue ? supplier.new_rankingnotification.Value : 0;
            return response;
        }

        public UpdateSupplierPricesResponse UpdateSupplierPrices(UpdateSupplierPricesRequest request)
        {
            bool isUsaSystem = GlobalConfigurations.IsUsaSystem;
            if (!isUsaSystem)
            {
                DataAccessLayer.SupplierPricing suppPricingDal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
                var supplierPricing = suppPricingDal.GetLastSupplierPricing(request.SupplierId);
                if (supplierPricing != null && supplierPricing.new_renew.HasValue && supplierPricing.new_renew.Value && supplierPricing.new_rechargeamount.HasValue)
                {
                    if (request.Expertises.Exists(x => x.Price > supplierPricing.new_rechargeamount.Value))
                    {
                        return new UpdateSupplierPricesResponse()
                        {
                            RechargePrice = supplierPricing.new_rechargeamount.Value,
                            Status = UpdateSupplierPricesResponse.UpdateSupplierPricesStatus.PriceOverRechargePrice
                        };
                    }
                }
            }
            DataAccessLayer.AccountExpertise accountExpertiseDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(request.SupplierId);
            bool checkUnavailability = false;
            if (!isUsaSystem)
            {
                if (supplier.new_unavailabilityreasonid.HasValue)//check if supplier is unavailable because of no money
                {
                    DataAccessLayer.UnavailabilityReason reasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
                    DML.Xrm.new_unavailabilityreason reason = reasonDal.Retrieve(supplier.new_unavailabilityreasonid.Value);
                    if (reason.new_ispricingended.HasValue && reason.new_ispricingended.Value == true)
                    {
                        checkUnavailability = true;
                    }
                }
            }
            if (supplier.new_notifypricechange.HasValue && supplier.new_notifypricechange.Value)
            {
                StringBuilder builder = new StringBuilder("<p>").Append(supplier.name).Append(" changed prices: ").Append("<\\p>");
                foreach (var item in request.Expertises)
                {
                    DML.Xrm.new_accountexpertise accExp = accountExpertiseDal.Retrieve(item.Id);
                    if (!accExp.new_incidentprice.HasValue || (accExp.new_incidentprice.HasValue && accExp.new_incidentprice.Value != item.Price))
                    {
                        decimal targetPrice = -1;
                        int canWin = -1;
                        if (accExp.new_new_primaryexpertise_new_accountexpertise.new_targetprice.HasValue)
                        {
                            targetPrice = accExp.new_new_primaryexpertise_new_accountexpertise.new_targetprice.Value;
                            canWin = 0;
                            for (int i = 0; i < 21; i++)
                            {
                                int amountToAdd = (int)Math.Ceiling((double)targetPrice * (double)i / 100);
                                if (item.Price >= targetPrice + amountToAdd)
                                    canWin++;
                                if (item.Price >= targetPrice - amountToAdd)
                                    canWin++;
                            }
                        }

                        builder.Append("<p>").Append(accExp.new_new_primaryexpertise_new_accountexpertise.new_name)
                            .Append(": From ").Append(accExp.new_incidentprice.HasValue ? string.Format("{0:0.##}", accExp.new_incidentprice.Value) : "0")
                            .Append(" to ").Append(string.Format("{0:0.##}", item.Price)).Append(".<br />")
                            .Append(" Current target price: ").Append(targetPrice != -1 ? string.Format("{0:0.##}", targetPrice) : "No target price").Append(".<br />");
                        if (canWin != -1)
                        {
                            builder.Append(" Probability of win: ").Append(String.Format("{0:0.####}", (double)canWin / (double)42)).Append(" (if no other partner is higher)");
                        }
                        builder.Append("<\\p>");
                    }
                    accExp.new_incidentprice = item.Price;
                    accExp.new_incidentpricechangeable = item.IsBid;
                    accountExpertiseDal.Update(accExp);
                    //check if the new incident price is lower that the balance, if true, put supplier to available.
                    CheckAvailabilityWithIncidentPriceChange(supplier, checkUnavailability, item, accountRepositoryDal);
                }
                Notifications notifier = new Notifications(XrmDataContext);
                DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string emails = config.GetConfigurationSettingValue(DML.ConfigurationKeys.PRICE_CHANGE_NOTIFY_EMAILS);
                if (!string.IsNullOrEmpty(emails))
                {
                    string[] spit = emails.Split(';');
                    List<string> emailsLst = new List<string>();
                    foreach (var item in spit)
                    {
                        emailsLst.Add(item);
                    }
                    notifier.SendEmail(emailsLst, builder.ToString(), supplier.name + " changed prices");
                }
            }
            else
            {
                foreach (var item in request.Expertises)
                {
                    DML.Xrm.new_accountexpertise accExp = new NoProblem.Core.DataModel.Xrm.new_accountexpertise(XrmDataContext);
                    accExp.new_accountexpertiseid = item.Id;
                    accExp.new_incidentprice = item.Price;
                    accExp.new_incidentpricechangeable = isUsaSystem ? true : item.IsBid;
                    accountExpertiseDal.Update(accExp);
                    //check if the new incident price is lower that the balance, if true, put supplier to available.
                    CheckAvailabilityWithIncidentPriceChange(supplier, checkUnavailability, item, accountRepositoryDal);
                }
            }
            supplier.new_rankingnotification = request.RankingNotification;
            accountRepositoryDal.Update(supplier);
            XrmDataContext.SaveChanges();
            if (!isUsaSystem)
            {
                //check if the supplier increased his incident prices and by doint it, took himself out of availability.
                CheckAvailabilityWhenIncidentPricesIncreased(request, accountRepositoryDal, supplier);
            }
            else
            {
                BusinessLogic.AdvertiserDashboards.GetStartedTasksManager manager = new NoProblem.Core.BusinessLogic.AdvertiserDashboards.GetStartedTasksManager(XrmDataContext);
                manager.CrossOutGetStartedTask(request.SupplierId, NoProblem.Core.DataModel.AdvertiserDashboards.GetStartedTaskType.default_price);
            }
            accountRepositoryDal.SetSupplierRegistrationStage(request.SupplierId, DML.Xrm.account.StageInRegistration.PAYMENT);
            //check if ranking notification are needed (if the supplier is updating and not first time in this page).
            if (supplier.new_stageinregistration == (int)DML.Xrm.account.StageInRegistration.PAID)
            {
                RankingNotifications rankingNoti = new RankingNotifications(supplier.accountid);
                rankingNoti.NotifyOtherSuppliers();
            }
            if (Dapaz.PpaOnlineSync.SyncConfiguration.IsEnabled)
            {
                Dapaz.PpaOnlineSync.Syncher syncher = new Dapaz.PpaOnlineSync.Syncher(request.SupplierId);
                syncher.Start();
            }
            return new UpdateSupplierPricesResponse() { Status = UpdateSupplierPricesResponse.UpdateSupplierPricesStatus.OK };
        }

        public List<ExpertiseWithIsActive> GetAllExpertisesWithHasSuppliers()
        {
            DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            List<ExpertiseWithIsActive> dataList = expertiseDal.GetAllActive();
            foreach (var expertise in dataList)
            {
                bool isActive = accExpDal.IsActiveWithPrimaryExpertise(expertise.Id);
                expertise.IsActive = isActive;
            }
            return dataList;
        }

        public bool IsHeadingForAuction(string headingCode, int headingLevel)
        {
            if (headingLevel == 1)
            {
                DataAccessLayer.PrimaryExpertise priExpsertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
                return priExpsertiseDal.IsAuction(headingCode);
            }
            else if (headingLevel == 2)
            {
                DataAccessLayer.SecondaryExpertise secExpDal = new NoProblem.Core.DataAccessLayer.SecondaryExpertise(XrmDataContext);
                return secExpDal.IsAuction(headingCode);
            }
            return true;
        }

        /// <summary>
        /// Returns headings with suppliers (Trial, Approved).
        /// </summary>
        /// <returns></returns>
        public List<DML.Expertise.ExpertiseWithIsActive> GetHeadingsWithSuppliers()
        {
            DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            List<DML.Expertise.ExpertiseWithIsActive> retVal = dal.GetHeadingsWithSuppliers();
            return retVal;
        }

        public List<DataModel.Expertise.KeywordWithCategory> GetAllKeywordsWithCategories()
        {
        //    return DataAccessLayer.CacheManager.Instance.Get<List<DataModel.Expertise.KeywordWithCategory>>("NoProblem.Core.BusinessLogic.Expertises.ExpertiseManager", "GetAllKeywordsWithCategories", GetAllKeywordsWithCategoriesPrivate, DataAccessLayer.CacheManager.ONE_DAY);
            return GetAllKeywordsWithCategoriesPrivate();
        }

        private List<DataModel.Expertise.KeywordWithCategory> GetAllKeywordsWithCategoriesPrivate()
        {
            List<DataModel.Expertise.KeywordWithCategory> retVal = new List<KeywordWithCategory>();
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            var reader = dal.ExecuteReader(getAllCategoriesWithKeywordsQuery);
            foreach (var row in reader)
            {
                KeywordWithCategory kwc = new KeywordWithCategory();
                kwc.CategoryId = (Guid)row["new_primaryexpertiseid"];
                kwc.CategoryName = Convert.ToString(row["new_primaryexpertiseidname"]);
                kwc.Keyword = Convert.ToString(row["new_keyword"]);
                retVal.Add(kwc);
            }
            return retVal;
        }

        private const string
            getAllCategoriesWithKeywordsQuery =
@"
select new_keyword, new_primaryexpertiseid, new_primaryexpertiseidname
from new_sliderkeyword with(nolock)
where statecode = 0 and DeletionStateCode = 0 and new_primaryexpertiseid is not null and new_keyword is not null
order by new_keyword
";
        #endregion

        #region Private Methods

        private ExpertiseContainer CreateExpertiseContainer(DataAccessLayer.PrimaryExpertise expertiseDal, DataAccessLayer.PredefinedPrice preDefinedPriceDal, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, bool dontUseAvgAsMinPrice, bool noMinPriceLimit)
        {
            ExpertiseContainer data = new ExpertiseContainer();
            DML.Xrm.new_primaryexpertise expertise = expertiseDal.Retrieve(accExp.new_primaryexpertiseid.Value);
            data.Id = accExp.new_accountexpertiseid;
            data.ExpertiseId = expertise.new_primaryexpertiseid;
            data.ExpertiseName = expertise.new_name;
            bool expertiseIsAuction = expertise.new_isauction.HasValue ? expertise.new_isauction.Value : true;
            data.ExpertiseIsAuction = expertiseIsAuction;
            if (noMinPriceLimit)
            {
                data.MinimumPrice = 1;
            }
            else
            {
                data.MinimumPrice = expertise.new_minprice.HasValue ? expertise.new_minprice.Value : 1;
            }
            data.Price = accExp.new_incidentprice.HasValue ? accExp.new_incidentprice.Value : new decimal?();
            if (data.Price.HasValue)
            {
                data.Ranking = GetRankingInExpertise(expertise.new_primaryexpertiseid, data.Price.Value, accExp.new_accountid.Value);
            }
            else
            {
                data.Ranking = null;
            }
            if (expertiseIsAuction)
            {
                if (data.Ranking != null)//is not a new expertise.
                {
                    data.IsBid = accExp.new_incidentpricechangeable.HasValue ? accExp.new_incidentpricechangeable.Value : true;
                }
                else// is a new expertise.
                {
                    data.IsBid = true;
                }
            }
            else//expertise is not for auctions.
            {
                data.IsBid = false;
            }
            return data;
        }

        /// <summary>
        /// We check if the new incident prices take the supplier out of availability because of not enough money.
        /// This can happen if the supplier increased all his incident prices to be higher than his balance.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="accountRepositoryDal"></param>
        /// <param name="supplier"></param>
        private void CheckAvailabilityWhenIncidentPricesIncreased(UpdateSupplierPricesRequest request, DataAccessLayer.AccountRepository accountRepositoryDal, DML.Xrm.account supplier)
        {
            if (supplier.new_cashbalance.HasValue
                && request.Expertises.Count > 0)
            {
                decimal balance = supplier.new_cashbalance.Value;
                decimal minPrice = decimal.MaxValue;
                foreach (var item in request.Expertises)
                {
                    if (item.Price.HasValue && minPrice > item.Price)
                    {
                        minPrice = item.Price.Value;
                    }
                }
                if (minPrice != decimal.MaxValue && minPrice > balance)
                {
                    //take supplier out of availability
                    DataAccessLayer.UnavailabilityReason reasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
                    DML.Xrm.new_unavailabilityreason reason = reasonDal.GetPricingEndedReason();
                    supplier.new_unavailabilityreasonid = reason.new_unavailabilityreasonid;
                    supplier.new_unavailablefrom = DateTime.Now.AddMinutes(-10);
                    int timezone;
                    if (supplier.address1_utcoffset.HasValue)
                    {
                        timezone = supplier.address1_utcoffset.Value;
                    }
                    else
                    {
                        DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                        timezone = int.Parse(configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE));
                    }
                    DateTime localDate = FixDateToLocal(DateTime.Now.AddMinutes(-10), timezone); //Utils.ConvertUtils.GetLocalFromUtc(XrmDataContext, DateTime.Now.AddMinutes(-10), timezone);
                    supplier.new_unavailablelocalfrom = localDate;
                    supplier.new_unavailablelocalto = null;
                    supplier.new_unavailableto = null;
                    accountRepositoryDal.Update(supplier);
                    XrmDataContext.SaveChanges();
                }
            }
        }

        /// <summary>
        /// If the supplier is unavailable because of not enough money. We check if the new incident price puts him back in availability.
        /// This can happen if he lowered the price under his balance.
        /// </summary>
        /// <param name="supplier"></param>
        /// <param name="checkUnavailability"></param>
        /// <param name="item"></param>
        /// <param name="accountRepositoryDal"></param>
        private void CheckAvailabilityWithIncidentPriceChange(DML.Xrm.account supplier, bool checkUnavailability, ExpertiseContainer item, DataAccessLayer.AccountRepository accountRepositoryDal)
        {
            if (checkUnavailability
                && supplier.new_cashbalance.HasValue
                && supplier.new_cashbalance.Value > 0
                && item.Price.HasValue)
            {
                if (supplier.new_cashbalance.Value >= item.Price.Value)
                {
                    supplier.new_unavailabilityreasonid = null;
                    supplier.new_unavailablefrom = null;
                    supplier.new_unavailablelocalfrom = null;
                    supplier.new_unavailablelocalto = null;
                    supplier.new_unavailableto = null;
                    accountRepositoryDal.Update(supplier);
                }
            }
        }

        #endregion
    }
}
