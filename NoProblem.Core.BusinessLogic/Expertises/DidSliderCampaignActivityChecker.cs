﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Expertise.Campaigns;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Expertises
{
    public class DidSliderCampaignActivityChecker
    {
        public List<HeadingIdAndIsAvailable> CheckAvailability(List<Guid> headings)
        {           
            List<HeadingIdAndIsAvailable> retVal = new List<HeadingIdAndIsAvailable>();
            DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(null);          
            foreach (var item in headings)
            {
                bool isSomeoneAvailable = NoProblem.Core.BusinessLogic.Dialer.RealTimeRouting.RealTimeRouter.IsSomeoneAvailable(item);
                retVal.Add(new HeadingIdAndIsAvailable() { HeadingId = item, IsSomeoneAvailable = isSomeoneAvailable });            
            }
            return retVal;
        }

        public List<EmailAndIsAvailable> CheckSpecialAdvertisersAvailability(List<string> advertiserEmails)
        {
            List<EmailAndIsAvailable> retVal = new List<EmailAndIsAvailable>();
            string emails = String.Join(",", advertiserEmails.ToArray());
            emails = "'" + emails.Replace(",", "','") + "'";
            string query = String.Format(querySecials, emails);
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            var reader = dal.ExecuteReader(query);
            List<string> availables = new List<string>();
            foreach (var item in reader)
            {
                availables.Add((string)item["EMailAddress1"]);                    
            }
            foreach (var item in advertiserEmails)
            {
                var pair = new EmailAndIsAvailable();
                pair.Email = item;
                pair.IsAvailable = availables.Contains(item);
                retVal.Add(pair);
            }
            return retVal;
        }

        private const string querySecials =
            @" 
declare @Availability Datetime = getdate()
declare @DayOfWeek int =  datepart(DW,getdate())

select
Account.EMailAddress1
from 
Account with(nolock)
 left JOIN new_availability with (nolock) ON (new_availability.new_accountid = account.accountid
 AND new_availability.deletionstatecode = 0 AND new_day = @DayOfWeek )
WHERE    
		(account.new_unavailablefrom > @Availability OR account.new_unavailableto < @Availability OR
		(account.new_unavailablefrom IS NULL AND account.new_unavailableto IS NULL))   
		and Account.new_status = 1 
		and EMailAddress1 in ( {0} )
		
group by Account.EMailAddress1

		having 
		(CASE WHEN 
	SUM(CASE WHEN 					
 (new_from <= CONVERT(char(8), @Availability,108) AND new_to >= CONVERT(char(8), @Availability ,108))
					THEN 1 
					ELSE 0 END) > 0 
 THEN 1  ELSE 0 END) = 1
";
    }
}
