﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Expertise;
using System.Data.SqlClient;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Expertises
{
    public class CampaignsDataRetriever
    {
        public DataModel.Expertise.Campaigns.PhoneDidSliderCampaignData GetPhoneDidData(Guid ExpertiseId)
        {
            string query = @"
                    select                    
                    new_code
                    ,pe.new_primaryexpertiseid
                    ,isnull(new_campaignmode, 1) as new_campaignmode
                    ,isnull(new_postsubmissionlandingpage, 1) as new_postsubmissionlandingpage
                    ,isnull(new_titleoptionsforphonedidslider, 1) as new_titleoptionsforphonedidslider
                    ,new_phonedidsliderothertitlename
                    ,isnull(new_secondtitleoptionsforphonedidslider, 1) as new_secondtitleoptionsforphonedidslider
                    ,new_phonedidsliderothersecondtitlename
                    ,new_didsliderimage
                    from new_primaryexpertiseextensionbase pe
                    where pe.New_primaryexpertiseId = @ExpertiseId";
            DML.Expertise.Campaigns.PhoneDidSliderCampaignData pd = null;              
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.AddWithValue("@ExpertiseId", ExpertiseId);
                    SqlDataReader reader = cmd.ExecuteReader();
                   
                    if (reader.Read())
                    {
                        pd = new NoProblem.Core.DataModel.Expertise.Campaigns.PhoneDidSliderCampaignData();

                        pd.UseTitleOther = (int)reader["new_titleoptionsforphonedidslider"] == (int)DML.Xrm.new_primaryexpertise.ePhoneDidOtherTitleOptions.Other;
                        pd.TitleOther = reader["new_phonedidsliderothertitlename"] == DBNull.Value ? null : (string)reader["new_phonedidsliderothertitlename"];
                        pd.UseTitleOtherSecond = (int)reader["new_secondtitleoptionsforphonedidslider"] == (int)DML.Xrm.new_primaryexpertise.ePhoneDidOtherSecondTitleOptions.Other;
                        pd.TitleOtherSecond = reader["new_phonedidsliderothersecondtitlename"] == DBNull.Value ? null : (string)reader["new_phonedidsliderothersecondtitlename"];

                        pd.Image = reader["new_didsliderimage"] == DBNull.Value ? string.Empty : (string)reader["new_didsliderimage"];                   
                           
                    }
                }
                conn.Close();
            }
            return pd;

        }
        public HashSet<DML.Expertise.Campaigns.CampaignData> GetAllCampaignsData()
        {
            return DataAccessLayer.CacheManager.Instance.Get<HashSet<DML.Expertise.Campaigns.CampaignData>>("Campaigns", "GetAllCampaignsData", GetAllCampaignsDataMethod);
        }
        public List<Guid> GetOriginsBlockCampaigns()
        {
            List<Guid> list = new List<Guid>();
            string command = @"SELECT New_originId
                                FROM dbo.New_originExtensionBase with(nolock)
                                WHERE ISNULL(New_IncludeCampaign, 0) = 0";
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        list.Add((Guid)reader["New_originId"]);
                    }
                }
                conn.Close();
            }
            return list;
        }
        private HashSet<DML.Expertise.Campaigns.CampaignData> GetAllCampaignsDataMethod()
        {
            DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(null);
            HashSet<DML.Expertise.Campaigns.CampaignData> retVal = new HashSet<DML.Expertise.Campaigns.CampaignData>();
            var reader = dal.ExecuteReader(queryForGetAllCampaignData);
            foreach (var row in reader)
            {
                try
                {
                    DML.Expertise.Campaigns.CampaignData campaign = new NoProblem.Core.DataModel.Expertise.Campaigns.CampaignData();
                    campaign.HeadingId = (Guid)row["new_primaryexpertiseid"];
                    campaign.Campaign = (DML.Xrm.new_primaryexpertise.eCampaignMode)((int)row["new_campaignmode"]);
                    if (campaign.Campaign == NoProblem.Core.DataModel.Xrm.new_primaryexpertise.eCampaignMode.PhoneDidSlider
                        || campaign.Campaign == DML.Xrm.new_primaryexpertise.eCampaignMode.FormAndDid)
                    {
                        bool isRealTimeRouting = row["new_realtimerouting"] != DBNull.Value ? (bool)row["new_realtimerouting"] : false;
                        if (!isRealTimeRouting && !IsActiveAndNotOnVacation(row))
                        {
                            continue;
                        }
                    }
                    bool isNew = retVal.Add(campaign);
                    if (isNew)
                    {
                        campaign.HeadingCode = Convert.ToString(row["new_code"]);
                        campaign.HeadingName = Convert.ToString(row["new_name"]);
                        campaign.Banner = new NoProblem.Core.DataModel.Expertise.Campaigns.BannerCampaignData();
                        campaign.Banner.DestinationPage = Convert.ToString(row["new_cjdestinationpage"]);
                        campaign.UseBannerCampaignsLandingPage = (int)row["new_postsubmissionlandingpage"] == (int)DML.Xrm.new_primaryexpertise.ePostSubmissionLandingPage.BannerCampaignsLandingPage;
                        if (campaign.UseBannerCampaignsLandingPage && string.IsNullOrEmpty(campaign.Banner.DestinationPage))
                        {
                            campaign.UseBannerCampaignsLandingPage = false;
                        }
                        if (campaign.Campaign == NoProblem.Core.DataModel.Xrm.new_primaryexpertise.eCampaignMode.BannerCampaign)
                        {
                            AddBannerExclusiveData(row, campaign);
                        }
                        else if (campaign.Campaign == NoProblem.Core.DataModel.Xrm.new_primaryexpertise.eCampaignMode.PhoneDidSlider
                            || campaign.Campaign == DML.Xrm.new_primaryexpertise.eCampaignMode.FormAndDid)
                        {
                            AddDidSliderExclusiveData(row, campaign);
                        }
                    }
                    else if (campaign.Campaign == NoProblem.Core.DataModel.Xrm.new_primaryexpertise.eCampaignMode.PhoneDidSlider
                        || campaign.Campaign == DML.Xrm.new_primaryexpertise.eCampaignMode.FormAndDid)
                    {
                        campaign = retVal.First(x => x.HeadingId == campaign.HeadingId);
                        AddDidNumber(row, campaign);
                    }
                }
                catch (Exception exc)
                {
                    Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, "Exception retrieving campaign for some heading. Catching the exception and continuing to next heading.");
                }
            }
            return retVal;
        }

        private bool IsActiveAndNotOnVacation(Dictionary<string, object> row)
        {
            bool isAccountActive = row["new_status"] != null && row["new_status"] != DBNull.Value && (int)row["new_status"] == (int)DataModel.Xrm.account.SupplierStatus.Approved;
            if (!isAccountActive)
            {
                return false;
            }
            bool hasInavailabilityReason = row["new_unavailabilityreasonid"] != DBNull.Value;
            if (hasInavailabilityReason)
            {
                DateTime? from = null;
                DateTime? to = null;
                if (row["new_unavailablefrom"] != DBNull.Value)
                {
                    from = (DateTime)row["new_unavailablefrom"];
                }
                if (row["new_unavailableto"] != DBNull.Value)
                {
                    to = (DateTime)row["new_unavailableto"];
                }
                DateTime now = DateTime.Now;
                if (from.HasValue)
                {
                    if (from.Value < DateTime.Now)
                    {
                        if (!to.HasValue || to.Value > DateTime.Now)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private void AddDidSliderExclusiveData(Dictionary<string, object> row, DML.Expertise.Campaigns.CampaignData campaign)
        {
            campaign.PhoneDid = new NoProblem.Core.DataModel.Expertise.Campaigns.PhoneDidSliderCampaignData();
            campaign.PhoneDid.UseTitleOther = (int)row["new_titleoptionsforphonedidslider"] == (int)DML.Xrm.new_primaryexpertise.ePhoneDidOtherTitleOptions.Other;
            campaign.PhoneDid.TitleOther = Convert.ToString(row["new_phonedidsliderothertitlename"]);
            campaign.PhoneDid.UseTitleOtherSecond = (int)row["new_secondtitleoptionsforphonedidslider"] == (int)DML.Xrm.new_primaryexpertise.ePhoneDidOtherSecondTitleOptions.Other;
            campaign.PhoneDid.TitleOtherSecond = Convert.ToString(row["new_phonedidsliderothersecondtitlename"]);
            AddDidNumber(row, campaign);
            string timeZoneName = Convert.ToString(row["StandardName"]);
            if (string.IsNullOrEmpty(timeZoneName))
            {
                DataAccessLayer.ConfigurationSettings configDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
                campaign.PhoneDid.TimeZoneStandardName = configDAL.GetPublisherTimeZoneStandardName();
            }
            else
            {
                campaign.PhoneDid.TimeZoneStandardName = timeZoneName;
            }
            campaign.PhoneDid.Image = Convert.ToString(row["new_didsliderimage"]);
            campaign.PhoneDid.IsRealTimeRouting = row["new_realtimerouting"] != DBNull.Value ? (bool)row["new_realtimerouting"] : false;
            if (!campaign.PhoneDid.IsRealTimeRouting)
            {
                DoWorkHours(campaign, row);
            }
        }

        private static void AddBannerExclusiveData(Dictionary<string, object> row, DML.Expertise.Campaigns.CampaignData campaign)
        {
            campaign.Banner.BannerHeight = row["new_cjbannerheight"] != DBNull.Value ? (int)row["new_cjbannerheight"] : 300;
            campaign.Banner.BannerWidth = row["new_cjbannerwidth"] != DBNull.Value ? (int)row["new_cjbannerwidth"] : 300;
            campaign.Banner.HtmlCode = Convert.ToString(row["new_cjbannerhtmlcode"]);
            campaign.Banner.Title = Convert.ToString(row["new_cjtitleonsliderwindow"]);
        }

        private static void AddDidNumber(Dictionary<string, object> row, DML.Expertise.Campaigns.CampaignData campaign)
        {
            string number = Convert.ToString(row["new_number"]);
            Guid originId = row["new_originid"] != DBNull.Value ? (Guid)row["new_originid"] : Guid.Empty;
            if (!string.IsNullOrEmpty(number) && originId != Guid.Empty)
            {
                campaign.PhoneDid.DIDs.Add(new DataModel.PublisherPortal.GuidStringPair(number, originId));
            }
        }

        private void DoWorkHours(DML.Expertise.Campaigns.CampaignData campaign, Dictionary<string, object> row)
        {
            DoHourOfOneDay(campaign, row, "new_sundayavailablefrom", "new_sundayavailableto", DayOfWeek.Sunday);
            DoHourOfOneDay(campaign, row, "new_mondayavailablefrom", "new_mondayavailableto", DayOfWeek.Monday);
            DoHourOfOneDay(campaign, row, "new_tuesdayavailablefrom", "new_tuesdayavailableto", DayOfWeek.Tuesday);
            DoHourOfOneDay(campaign, row, "new_wednesdayavailablefrom", "new_wednesdayavailableto", DayOfWeek.Wednesday);
            DoHourOfOneDay(campaign, row, "new_thursdayavailablefrom", "new_thursdayavailableto", DayOfWeek.Thursday);
            DoHourOfOneDay(campaign, row, "new_fridayavailablefrom", "new_fridayavailableto", DayOfWeek.Friday);
            DoHourOfOneDay(campaign, row, "new_saturdayavailablefrom", "new_saturdayavailableto", DayOfWeek.Saturday);
        }

        private void DoHourOfOneDay(DML.Expertise.Campaigns.CampaignData campaign, Dictionary<string, object> row, string fromColumnName, string toColumnName, DayOfWeek dayOfWeek)
        {
            string from = Convert.ToString(row[fromColumnName]);
            string to = Convert.ToString(row[toColumnName]);
            var hoursData = GetHorkHoursData(dayOfWeek, from, to);
            if (hoursData != null)
                campaign.PhoneDid.WorkingHoursData.Add(hoursData);
        }

        private DML.Expertise.Campaigns.WorkingHoursData GetHorkHoursData(DayOfWeek dayOfWeek, string fromStr, string toStr)
        {
            DML.Expertise.Campaigns.WorkingHoursData hoursData = null;
            if (!string.IsNullOrEmpty(fromStr) & !string.IsNullOrEmpty(toStr) & fromStr != "1:00" & toStr != "1:01")
            {
                hoursData = new NoProblem.Core.DataModel.Expertise.Campaigns.WorkingHoursData();
                hoursData.DayOfWeek = dayOfWeek;
                int from = int.Parse(fromStr.Substring(0, 2));
                int to;
                if (toStr.StartsWith("23:59"))
                {
                    to = 24;
                }
                else
                {
                    to = int.Parse(toStr.Substring(0, 2));
                }
                hoursData.From = from;
                hoursData.To = to;
            }
            return hoursData;
        }

        private const string queryForGetAllCampaignData =
            @"
select 
new_cjtitleonsliderwindow
,new_cjbannerheight
,new_cjbannerwidth
,new_cjbannerhtmlcode
,new_cjdestinationpage
,pe.new_name
,new_code
,pe.new_primaryexpertiseid
,isnull(new_campaignmode, 1) as new_campaignmode
,isnull(new_postsubmissionlandingpage, 1) as new_postsubmissionlandingpage
,isnull(new_titleoptionsforphonedidslider, 1) as new_titleoptionsforphonedidslider
,new_phonedidsliderothertitlename
,isnull(new_secondtitleoptionsforphonedidslider, 1) as new_secondtitleoptionsforphonedidslider
,new_phonedidsliderothersecondtitlename
,new_didsliderimage
,dn.new_number
,dn.new_originid
,tzd.StandardName
,acc.new_sundayavailablefrom
,acc.new_sundayavailableto
,acc.new_mondayavailablefrom
,acc.new_mondayavailableto
,acc.new_tuesdayavailablefrom
,acc.new_tuesdayavailableto
,acc.new_wednesdayavailablefrom
,acc.new_wednesdayavailableto
,acc.new_thursdayavailablefrom
,acc.new_thursdayavailableto
,acc.new_fridayavailablefrom
,acc.new_fridayavailableto
,acc.new_saturdayavailablefrom
,acc.new_saturdayavailableto
,acc.new_status
,acc.new_unavailabilityreasonid
,acc.new_unavailablefrom
,acc.new_unavailableto
,dn.new_realtimerouting
from new_primaryexpertiseextensionbase pe
left join account acc on accountid = pe.new_accountid
left join new_accountexpertise ae on ae.new_primaryexpertiseid = pe.new_primaryexpertiseid and ae.new_accountid = pe.new_accountid and ae.statecode = 0 and ae.deletionstatecode = 0
left join new_directnumber dn on dn.new_accountexpertiseid = ae.new_accountexpertiseid and dn.deletionstatecode = 0
left join TimeZoneDefinition tzd on tzd.TimeZoneCode = acc.address1_utcoffset
where
isnull(new_campaignmode, 1) > 1
or new_postsubmissionlandingpage = 2
";
        /*
        private const string QueryPhonedidByCountryExpertise = @"
SELECT deb.New_number, deb.new_originid
FROM dbo.New_directnumberBase db WITH(NOLOCK)
	INNER JOIN dbo.New_directnumberExtensionBase deb WITH(NOLOCK) on db.New_directnumberId = deb.New_directnumberId
WHERE db.DeletionStateCode = 0
	AND db.statuscode = 1
	AND deb.New_Country = @country
	AND deb.new_primaryexpertiseid = @ExpertiseId";
        */
        private const string QueryPhonedidByCountryExpertise = @"
SELECT deb.New_number, deb.new_originid, deb.New_Country, deb.new_primaryexpertiseid, ex.New_code
FROM dbo.New_directnumberBase db WITH(NOLOCK)
	INNER JOIN dbo.New_directnumberExtensionBase deb WITH(NOLOCK) on db.New_directnumberId = deb.New_directnumberId
	INNER JOIN dbo.New_primaryexpertiseExtensionBase ex WITH(NOLOCK) on ex.New_primaryexpertiseId = deb.new_primaryexpertiseid
WHERE db.DeletionStateCode = 0
	AND db.statuscode = 1
	AND deb.New_Country <> 'UNITED STATES'";

        public List<DML.Expertise.Campaigns.PhonedidOutsideUsResponse> GetPhoneDidOutsideUS()
        {
            List<DML.Expertise.Campaigns.PhonedidOutsideUsResponse> list = new List<DML.Expertise.Campaigns.PhonedidOutsideUsResponse>();
            using (SqlConnection conn = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(QueryPhonedidByCountryExpertise, conn))
                {
                   
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DML.Expertise.Campaigns.PhonedidOutsideUsResponse data = new DML.Expertise.Campaigns.PhonedidOutsideUsResponse();
                        data.number = (string)reader["New_number"];
                        data.OriginId = (Guid)reader["new_originid"];
                        data.Country = (string)reader["New_Country"];
                        data.ExpertiseCode = (string)reader["New_code"];
                        data.ExpertiseId = (Guid)reader["new_primaryexpertiseid"];
                        list.Add(data);
                    }
                }
                conn.Close();
            }
            return list;
        }
    }
}
