﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.SupplierModel;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic
{
    internal class DirectNumbersAssigner : XrmUserBase
    {
        #region Ctor
        internal DirectNumbersAssigner()
            : base(null)// this class' mehtods are async so we create new context for it.
        {
        }
        #endregion

        #region Internal Methods

        internal void DetachAndReassignNumbersToSupplier(Guid supplierId)
        {
            Thread t1 = new Thread(delegate()
            {
                try
                {
                    if (IsAutoDirectNumbersDisabled(supplierId))
                    {
                        return;
                    }
                    DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                    DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                    IEnumerable<Guid> accExps = accExpDal.GetAllIdsByAccount(supplierId);
                    foreach (var id in accExps)
                    {
                        dnDal.DetachDirectNumbersFromAccountExpertise(id, false);
                    }
                    DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                    string dnsHaveRegion = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.DIRECT_NUMBERS_HAVE_REGION);
                    if (dnsHaveRegion == "1")
                    {
                        AssignWithRegion(supplierId);
                    }
                    else
                    {
                        AssignNormal(supplierId);
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in DetachAndReassignNumbersToSupplier. supplierId = {0}", supplierId);
                }
            });
            t1.Start();
        }

        internal void AssignDirectNumbersByOriginId(Guid originId)
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string dnsHaveRegion = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.DIRECT_NUMBERS_HAVE_REGION);
            if (dnsHaveRegion == "1")
            {
                AssignByOriginIdWithRegion(originId);
            }
            else
            {
                AssingByOriginIdNormal(originId);
            }
        }

        internal void AssignDirectNumbersToSuppliers()
        {
            DataAccessLayer.Origin dal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            var originIds = dal.GetAllDirectNumbersOrigins();
            foreach (var id in originIds)
            {
                AssignDirectNumbersByOriginId(id);
            }
        }

        internal void AssignDirectNumbersToSupplier(Guid supplierId)
        {
            Thread t1 = new Thread(delegate()
            {
                try
                {
                    //if (IsAutoDirectNumbersDisabled(supplierId))
                    //{
                    //    return;
                    //}
                    DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                    string dnsHaveRegion = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.DIRECT_NUMBERS_HAVE_REGION);
                    if (dnsHaveRegion == "1")
                    {
                        AssignWithRegion(supplierId);
                    }
                    else
                    {
                        AssignNormal(supplierId);
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in AssignDirectNumbersToSupplier. supplierId = {0}", supplierId);
                }
            });
            t1.Start();
        }

        internal void DetachAllDirectNumbersFromSupplier(Guid supplierId, bool forceDetach)
        {
            Thread t1 = new Thread(delegate()
            {
                try
                {
                    if (!forceDetach && IsAutoDirectNumbersDisabled(supplierId))
                    {
                        return;
                    }
                    DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                    DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                    IEnumerable<Guid> accExps = accExpDal.GetAllIdsByAccount(supplierId);
                    foreach (var id in accExps)
                    {
                        dnDal.DetachDirectNumbersFromAccountExpertise(id, true);
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in DetachAllDirectNumbersFromSupplier. supplierId = {0}", supplierId);
                }
            });
            t1.Start();
        }

        #endregion

        #region Private Methods

        private bool IsAutoDirectNumbersDisabled(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            return dal.IsAutoDirectNumbersDisabled(supplierId);
        }

        private void AssignWithRegion(Guid supplierId)
        {
            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            IEnumerable<Guid> origins = originDal.GetAllDirectNumbersOrigins();
            int originsCount = origins.Count();
            DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            IEnumerable<Guid> regions = regionDal.GetLevel1RegionIdsForAccount(supplierId);
            foreach (var regId in regions)
            {
                List<Guid> freeDirectNumbers = dnDal.GetFreeDirectNumbersInRegion(regId);
                for (int i = 0; i < originsCount && freeDirectNumbers.Count > 0; i++)
                {
                    Guid originId = origins.ElementAt(i);
                    IEnumerable<Guid> accExps = originDal.GetAccountExpertisesThatNeedDirectNumber(supplierId, originId, regId);
                    int accExpCount = accExps.Count();
                    for (int j = 0; j < accExpCount && freeDirectNumbers.Count > 0; j++)
                    {
                        DML.Xrm.new_directnumber dn = new NoProblem.Core.DataModel.Xrm.new_directnumber(XrmDataContext);
                        dn.new_originid = originId;
                        dn.new_accountexpertiseid = accExps.ElementAt(j);
                        dn.new_regionid = regId;
                        dn.new_directnumberid = freeDirectNumbers.ElementAt(0);
                        freeDirectNumbers.RemoveAt(0);
                        dnDal.Update(dn);
                    }
                }
                XrmDataContext.SaveChanges();
            }
        }

        private void AssignNormal(Guid supplierId)
        {
            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            IEnumerable<Guid> origins = originDal.GetAllDirectNumbersOrigins();
            int originsCount = origins.Count();
            DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            lock (DataAccessLayer.DirectNumber.lockObject)
            {
                List<Guid> freeDirectNumbers = dnDal.GetFreeDirectNumbers();
                for (int i = 0; i < originsCount && freeDirectNumbers.Count > 0; i++)
                {
                    Guid originId = origins.ElementAt(i);
                    IEnumerable<Guid> accExps = originDal.GetAccountExpertisesThatNeedDirectNumber(supplierId, originId);
                    int accExpCount = accExps.Count();
                    for (int j = 0; j < accExpCount && freeDirectNumbers.Count > 0; j++)
                    {
                        DML.Xrm.new_directnumber dn = new NoProblem.Core.DataModel.Xrm.new_directnumber(XrmDataContext);
                        dn.new_originid = originId;
                        dn.new_accountexpertiseid = accExps.ElementAt(j);
                        dn.new_directnumberid = freeDirectNumbers.ElementAt(0);
                        freeDirectNumbers.RemoveAt(0);
                        dnDal.Update(dn);
                    }
                }
                XrmDataContext.SaveChanges();
            }
        }

        private void AssignByOriginIdWithRegion(Guid originId)
        {
            DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            IEnumerable<Guid> regionsIds = regDal.GetRegionIdsByLevel(1);
            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            foreach (var regId in regionsIds)
            {
                IEnumerable<Guid> accExps = originDal.GetAllAccountExpertisesThatNeedDirectNumber(originId, regId);
                List<Guid> freeDirectNumbers = dnDal.GetFreeDirectNumbersInRegion(regId);
                int dnsCount = freeDirectNumbers.Count;
                int accExpCount = accExps.Count();
                for (int i = 0; i < dnsCount && i < accExpCount; i++)
                {
                    DML.Xrm.new_directnumber dn = new NoProblem.Core.DataModel.Xrm.new_directnumber(XrmDataContext);
                    dn.new_originid = originId;
                    dn.new_accountexpertiseid = accExps.ElementAt(i);
                    dn.new_directnumberid = freeDirectNumbers.ElementAt(i);
                    dnDal.Update(dn);
                    XrmDataContext.SaveChanges();
                }
            }
        }

        private void AssingByOriginIdNormal(Guid originId)
        {
            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            IEnumerable<Guid> accExps = originDal.GetAllAccountExpertisesThatNeedDirectNumber(originId);
            DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            lock (DataAccessLayer.DirectNumber.lockObject)
            {
                List<Guid> freeDirectNumbers = dnDal.GetFreeDirectNumbers();
                int dnsCount = freeDirectNumbers.Count;
                int accExpCount = accExps.Count();
                for (int i = 0; i < dnsCount && i < accExpCount; i++)
                {
                    DML.Xrm.new_directnumber dn = new NoProblem.Core.DataModel.Xrm.new_directnumber(XrmDataContext);
                    dn.new_originid = originId;
                    dn.new_accountexpertiseid = accExps.ElementAt(i);
                    dn.new_directnumberid = freeDirectNumbers.ElementAt(i);
                    dnDal.Update(dn);
                    XrmDataContext.SaveChanges();
                }
            }
        }

        #endregion        
    }
}
