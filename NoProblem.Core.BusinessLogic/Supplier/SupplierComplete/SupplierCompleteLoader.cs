﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel;
using System.Xml.Linq;
using NoProblem.Core.DataModel;
using System.Net;
using System.IO;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.SqlHelper;

namespace NoProblem.Core.BusinessLogic.SupplierLogic.SupplierComplete
{
    internal class SupplierCompleteLoader : XrmUserBase
    {
        #region Fields

        BusinessLogic.SupplierManager supplierManager; 

        #endregion

        #region Ctor

        public SupplierCompleteLoader(BusinessLogic.SupplierManager supplierManager)
            : base(supplierManager.XrmDataContext)
        {
            this.supplierManager = supplierManager;
        } 

        #endregion

        #region Internal Methods
        
        internal Result<Guid> SaveSupplierData(string supplierXml, string callBackUrl, string customParam)
        {
            Result<Guid> result = new Result<Guid>();
            Validator.ValidateSupplierXml(supplierXml, callBackUrl, result);
            if (result.Type == Result.eResultType.Success)
            {
                DataModel.Xrm.new_supplierinsertdata data = new NoProblem.Core.DataModel.Xrm.new_supplierinsertdata();
                data.new_callbackurl = callBackUrl;
                data.new_supplierxml = supplierXml;
                data.new_customparam = customParam;
                DataAccessLayer.SupplierInsertData dal = new NoProblem.Core.DataAccessLayer.SupplierInsertData(XrmDataContext);
                dal.Create(data);
                XrmDataContext.SaveChanges();
                result.Value = data.new_supplierinsertdataid;
            }
            return result;
        }

        internal void CreateSupplier(DataModel.Xrm.new_supplierinsertdata data)
        {
            var supplier = ParseXmltoSupplierObject(data.new_supplierxml);
            Result<Guid> result = new Result<Guid>();
            //General info:
            DoGeneralInfo(supplier, result);
            Dictionary<Guid, int> headPricesDic = null;
            if (supplier.IsUpdate || result.Type == Result.eResultType.Success)
            {
                //Headings:
                headPricesDic = DoHeadings(supplier, result);
            }
            if (supplier.IsUpdate || result.Type == Result.eResultType.Success)
            {
                //regions:            
                DoRegions(supplier, result);
            }
            if (supplier.IsUpdate || result.Type == Result.eResultType.Success)
            {
                //work hours
                DoHours(supplier, result);
            }
            if (supplier.IsUpdate || result.Type == Result.eResultType.Success)
            {
                //credits:
                DoCredits(supplier, headPricesDic, result);
            }
            if (result.Type == Result.eResultType.Success && !supplier.IsUpdate)
            {
                //money:
                DoDeposit(supplier, result);
            }
            if (result.Type == Result.eResultType.Failure)
            {
                if (!supplier.IsUpdate)
                {
                    DeleteAll(supplier);
                }
                data.statuscode = (int)DataModel.Xrm.new_supplierinsertdata.SupplierInsertDataStatusCode.Failed;
                data.new_failurereason = result.ToString();
            }
            else
            {
                data.statuscode = (int)DataModel.Xrm.new_supplierinsertdata.SupplierInsertDataStatusCode.Done;
                data.new_accountid = supplier.SupplierId;
            }
            //Send call back:
            try
            {
                WebRequest request = WebRequest.Create(data.new_callbackurl);
                request.Timeout = 30000;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                string postData;
                if (supplier.IsUpdate || result.Type == Result.eResultType.Success)
                {
                    postData = "requestId=" + data.new_supplierinsertdataid.ToString() + "&advertiserId=" + supplier.SupplierId.ToString() + "&status=" + result.Type.ToString() + "&messages=" + result.ToString() + "&customParam=" + data.new_customparam;
                }
                else
                {
                    postData = "requestId=" + data.new_supplierinsertdataid.ToString() + "&advertiserId=0&status=" + result.Type.ToString() + "&messages=" + result.ToString() + "&customParam=" + data.new_customparam;
                }
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] bytes = encoding.GetBytes(postData);
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        string str = sr.ReadToEnd();
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Could not send supplier insert call back");
                data.new_failurereason = "Could not send supplier insert call back" + exc.Message;
            }
            DataAccessLayer.SupplierInsertData dal = new NoProblem.Core.DataAccessLayer.SupplierInsertData(XrmDataContext);
            dal.Update(data);
            XrmDataContext.SaveChanges();
        }

        #endregion

        #region Private Regions

        private void DeleteAll(NoProblem.Core.DataModel.SupplierModel.SupplierComplete.SupplierComplete supplier)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplerXrm = accountRepositoryDal.Retrieve(supplier.SupplierId);
            if (supplerXrm != null)
            {
                DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                var accExps = accExpDal.GetAllIdsByAccount(supplerXrm.accountid);
                foreach (var acExpId in accExps)
                {
                    DataModel.Xrm.new_accountexpertise acExp = new NoProblem.Core.DataModel.Xrm.new_accountexpertise(XrmDataContext);
                    acExp.new_accountexpertiseid = acExpId;
                    accExpDal.Delete(acExp);
                }

                DataAccessLayer.RegionAccount regionAccountDal = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
                IEnumerable<Guid> regAccs = regionAccountDal.GetByAccountId(supplerXrm.accountid);
                foreach (var regAcId in regAccs)
                {
                    DataModel.Xrm.new_regionaccountexpertise regAc = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise(XrmDataContext);
                    regAc.new_regionaccountexpertiseid = regAcId;
                    regionAccountDal.Delete(regAc);
                }

                DataAccessLayer.Availability availabilityDal = new NoProblem.Core.DataAccessLayer.Availability(XrmDataContext);
                IEnumerable<Guid> daysAvs = availabilityDal.GetByAccountId(supplerXrm.accountid);
                foreach (var dayId in daysAvs)
                {
                    DataModel.Xrm.new_availability day = new NoProblem.Core.DataModel.Xrm.new_availability(XrmDataContext);
                    day.new_availabilityid = dayId;
                    availabilityDal.Delete(day);
                }

                accountRepositoryDal.Delete(supplerXrm);
                XrmDataContext.SaveChanges();
            }
        }

        private DataModel.SupplierModel.SupplierComplete.SupplierComplete ParseXmltoSupplierObject(string supplierXml)
        {
            DataModel.SupplierModel.SupplierComplete.SupplierComplete supplier =
                new NoProblem.Core.DataModel.SupplierModel.SupplierComplete.SupplierComplete();
            XElement xml = XElement.Parse(supplierXml);
            if (xml.Attribute("Method").Value == "Update")
            {
                supplier.IsUpdate = true;
                supplier.SupplierId = new Guid(xml.Attribute("Id").Value);
            }
            XElement nameElmt = xml.Element("Name");
            supplier.Name = nameElmt.Value;
            XElement phoneElmt = xml.Element("Phone");
            supplier.Phone = phoneElmt.Value;
            supplier.Headings = new List<NoProblem.Core.DataModel.SupplierModel.SupplierComplete.HeadingComplete>();
            XElement headsElmt = xml.Element("Headings");
            foreach (var item in headsElmt.Elements("Heading"))
            {
                string code = item.Value;
                string priceStr = item.Attribute("Price").Value;
                supplier.Headings.Add(new NoProblem.Core.DataModel.SupplierModel.SupplierComplete.HeadingComplete()
                {
                    Code = code,
                    Price = int.Parse(priceStr)
                });
            }
            supplier.Regions = new List<NoProblem.Core.DataModel.SupplierModel.SupplierComplete.RegionComplete>();
            XElement regsElmt = xml.Element("Regions");
            foreach (var item in regsElmt.Elements("Region"))
            {
                string code = item.Value;
                string levelStr = item.Attribute("Level").Value;
                supplier.Regions.Add(new NoProblem.Core.DataModel.SupplierModel.SupplierComplete.RegionComplete()
                {
                    Code = code,
                    Level = int.Parse(levelStr)
                });
            }
            supplier.DaysAvailability = new List<NoProblem.Core.DataModel.SupplierModel.SupplierComplete.DayHoursComplete>();
            XElement daysElmt = xml.Element("Availability");
            foreach (var item in daysElmt.Elements("Day"))
            {
                DayOfWeek day = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), item.Value);
                string from = item.Attribute("FromHour").Value;
                string to = item.Attribute("ToHour").Value;
                supplier.DaysAvailability.Add(new NoProblem.Core.DataModel.SupplierModel.SupplierComplete.DayHoursComplete()
                {
                    DayOfWeek = day,
                    FromHour = int.Parse(from),
                    ToHour = int.Parse(to)
                });
            }
            XElement balanceElmt = xml.Element("Balance");
            if (balanceElmt != null)
            {
                supplier.PaymentAmount = int.Parse(balanceElmt.Value);
            }
            return supplier;
        }

        private void DoGeneralInfo(DataModel.SupplierModel.SupplierComplete.SupplierComplete supplier, Result<Guid> result)
        {
            UpsertAdvertiserRequest upsertReq = new UpsertAdvertiserRequest();
            if (supplier.IsUpdate)
            {
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                var exists = dal.Retrieve(supplier.SupplierId);
                if (exists == null)
                {
                    result.Type = Result.eResultType.Failure;
                    result.Messages.Add(string.Format("No advertiser was found with id {0}", supplier.SupplierId));
                    return;
                }
                upsertReq.SupplierId = supplier.SupplierId;
            }            
            upsertReq.Company = supplier.Name;
            upsertReq.ContactPhone = supplier.Phone;            
            upsertReq.Email = supplier.Name + "_" + supplier.Phone + "@noproblemppc.com";
            
            var upsertResponse = supplierManager.UpsertAdvertiser(upsertReq);
            if (upsertResponse.Status != UpsertAdvertiserStatus.Success)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add("An advertiser with this name and phone already exists.");
            }
            supplier.SupplierId = upsertResponse.SupplierId;
            result.Value = supplier.SupplierId;
        }

        private Dictionary<Guid, int> DoHeadings(DataModel.SupplierModel.SupplierComplete.SupplierComplete supplier, Result<Guid> result)
        {
            XElement expRequestXml = new XElement("SupplierExpertise");
            expRequestXml.Add(new XAttribute("SiteId", ""));
            expRequestXml.Add(new XAttribute("SupplierId", supplier.SupplierId));

            Dictionary<Guid, int> headPricesDic = new Dictionary<Guid, int>();
            DataAccessLayer.PrimaryExpertise headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            foreach (var item in supplier.Headings)
            {
                //find heading from code
                var heading = headingDal.GetPrimaryExpertiseByCode(item.Code);
                if (heading != null)
                {
                    headPricesDic.Add(heading.new_primaryexpertiseid, item.Price);
                    expRequestXml.Add(
                            new XElement("PrimaryExpertise",
                                new XAttribute("ID", heading.new_primaryexpertiseid),
                                new XAttribute("Certificate", false))
                                );
                }
                else
                {
                    result.Type = Result.eResultType.Failure;
                    result.Messages.Add(string.Format("Heading with code {0} does not exist.", item.Code));
                    break;
                }
            }
            if (result.Type == Result.eResultType.Success)
            {
                string expertiseReq = expRequestXml.ToString();
                Supplier supplierLogic = new Supplier(XrmDataContext, null);
                string expertiseResult = supplierLogic.CreateSupplierExpertise(expertiseReq, false, Guid.Empty);
                if (expertiseResult.StartsWith("<SupplierExpertise><Status>Failure"))
                {
                    result.Type = Result.eResultType.Failure;
                    result.Messages.Add(string.Format("Not all headings could be saved. Recharge amount too low."));
                }
            }
            return headPricesDic;
        }

        private void DoRegions(DataModel.SupplierModel.SupplierComplete.SupplierComplete supplier, Result<Guid> result)
        {
            NoProblem.Core.BusinessLogic.Regions.RegionsManager regionsManager = new NoProblem.Core.BusinessLogic.Regions.RegionsManager(XrmDataContext);
            var regionsRequest =
                new NoProblem.Core.DataModel.Regions.UpdateSupplierRegionsRequest()
                {
                    SupplierId = supplier.SupplierId,
                    RegionDataList = new List<NoProblem.Core.DataModel.Regions.RegionDataForUpdate>()
                };
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            List<RegionMinData> workingList = new List<RegionMinData>();
            List<RegionMinData> notWorkingList = new List<RegionMinData>();
            List<RegionMinData> partialList = new List<RegionMinData>();
            foreach (var item in supplier.Regions)
            {
                if (item.Level == 1)
                {
                    var regionData = dal.GetRegionMinDataByCode(item.Code);
                    if (regionData.Id != Guid.Empty)
                    {
                        if (regionData.Level != 1)
                        {
                            result.Type = Result.eResultType.Failure;
                            result.Messages.Add(string.Format("Region with code {0} is not level 1", item.Code));
                            break;
                        }
                        regionsRequest.RegionDataList.Add(new NoProblem.Core.DataModel.Regions.RegionDataForUpdate()
                        {
                            Level = 1,
                            IsDirty = true,
                            RegionId = regionData.Id,
                            RegionState = NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.Working,
                            Parent1 = regionData.Id
                        });
                        workingList.Add(regionData);
                    }
                    else
                    {
                        result.Type = Result.eResultType.Failure;
                        result.Messages.Add(string.Format("Region with code {0} does not exist", item.Code));
                        break;
                    }
                }
                else
                {
                    var regionData = dal.GetRegionMinDataByCode(item.Code);
                    if (item.Level != regionData.Level)
                    {
                        result.Type = Result.eResultType.Failure;
                        result.Messages.Add(string.Format("Region with code {0} is not level {1}", item.Code, item.Level));
                        break;
                    }
                    workingList.Add(regionData);
                    if (partialList.Contains(regionData))
                        partialList.Remove(regionData);
                    if (notWorkingList.Contains(regionData))
                        notWorkingList.Remove(regionData);
                    var parents = dal.GetParents(regionData);
                    foreach (var dad in parents)
                    {
                        if (notWorkingList.Contains(dad))
                        {
                            notWorkingList.Remove(dad);
                        }
                        if (!partialList.Contains(dad))
                        {
                            partialList.Add(dad);
                        }
                    }
                    var brothers = dal.GetBrothers(regionData.Id);
                    foreach (var bro in brothers)
                    {
                        RegionMinData broData = new RegionMinData()
                        {
                            Id = bro.new_regionid,
                            Level = bro.new_level.Value,
                            ParentId = bro.new_parentregionid.Value
                        };
                        if (!workingList.Contains(broData) && !partialList.Contains(broData))
                        {
                            notWorkingList.Add(broData);
                        }
                    }
                }
            }
            foreach (var item in partialList)
            {
                regionsRequest.RegionDataList.Add(new NoProblem.Core.DataModel.Regions.RegionDataForUpdate()
                {
                    Level = item.Level,
                    IsDirty = true,
                    RegionId = item.Id,
                    RegionState = NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild
                });
                var bros = dal.GetBrothers(item.Id);
                foreach (var bro in bros)
                {
                    RegionMinData broData = new RegionMinData()
                    {
                        Id = bro.new_regionid,
                        Level = bro.new_level.Value,
                        ParentId = bro.new_parentregionid.Value
                    };
                    if (!workingList.Contains(broData) && !partialList.Contains(broData))
                    {
                        notWorkingList.Add(broData);
                    }
                }
            }
            foreach (var item in notWorkingList)
            {
                regionsRequest.RegionDataList.Add(new NoProblem.Core.DataModel.Regions.RegionDataForUpdate()
                {
                    Level = item.Level,
                    IsDirty = true,
                    RegionId = item.Id,
                    RegionState = NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.NotWorking
                });
            }
            if (result.Type == Result.eResultType.Success)
            {
                regionsManager.UpdateSupplierRegionsInTree(regionsRequest);
            }
        }

        private void DoHours(DataModel.SupplierModel.SupplierComplete.SupplierComplete supplier, Result<Guid> result)
        {
            XElement xml = new XElement("SupplierAvailability");
            xml.Add(new XAttribute("SiteId", ""));
            xml.Add(new XAttribute("SupplierId", supplier.SupplierId));
            XElement availability = new XElement("Availability");
            bool[] daysDone = new bool[7];
            foreach (var item in supplier.DaysAvailability)
            {
                if (daysDone[(int)item.DayOfWeek])
                {
                    continue;
                }
                string fromStr = item.FromHour.ToString();
                if (fromStr.Length == 1)
                {
                    fromStr = "0" + fromStr;
                }
                string toStr;
                if (item.ToHour == 24)
                {
                    toStr = "23:59";
                }
                else
                {
                    toStr = item.ToHour.ToString();
                    if (toStr.Length == 1)
                    {
                        toStr = "0" + toStr;
                    }
                    toStr += ":00";
                }
                string day = item.DayOfWeek.ToString().ToLower();
                availability.Add(
                    new XElement("Day",
                        new XAttribute("Name", day),
                        new XElement("FromTime", fromStr + ":00"),
                        new XElement("TillTime", toStr))
                    );
                daysDone[(int)item.DayOfWeek] = true;
            }
            for (int i = 0; i < 7; i++)
            {
                if (daysDone[i])
                {
                    continue;
                }
                DayOfWeek day = (DayOfWeek)i;
                availability.Add(
                    new XElement("Day",
                        new XAttribute("Name", day.ToString().ToLower()),
                        new XElement("FromTime", "1:00"),
                        new XElement("TillTime", "1:01"))
                    );
            }
            xml.Add(availability);
            Supplier supplierLogic = new Supplier(XrmDataContext, null);
            supplierLogic.SetSupplierAvailability(xml.ToString(), false);
        }

        private void DoCredits(DataModel.SupplierModel.SupplierComplete.SupplierComplete supplier, Dictionary<Guid, int> headPricesDic, Result<Guid> result)
        {
            NoProblem.Core.BusinessLogic.Expertises.ExpertiseManager headingsManager =
                new NoProblem.Core.BusinessLogic.Expertises.ExpertiseManager(XrmDataContext);

            var supplierPrices = headingsManager.GetSupplierPrices(new NoProblem.Core.DataModel.Expertise.GetSupplierPricesRequest()
            {
                SupplierId = supplier.SupplierId
            });
            var creditsRequest =
                new NoProblem.Core.DataModel.Expertise.UpdateSupplierPricesRequest()
                {
                    SupplierId = supplier.SupplierId,
                    Expertises = new List<NoProblem.Core.DataModel.Expertise.ExpertiseContainer>()
                };
            foreach (var item in supplierPrices.Expertises)
            {
                var givenPrice = headPricesDic[item.ExpertiseId];
                if (givenPrice < item.MinimumPrice)
                {
                    result.Type = Result.eResultType.Failure;
                    result.Messages.Add(string.Format("Price to low for heading {0}, minimum price is {1}, given price is {2}.", item.ExpertiseName, item.MinimumPrice, givenPrice));
                }
                else
                {
                    item.Price = givenPrice;
                }
                creditsRequest.Expertises.Add(item);
            }
            var creditsResponse = headingsManager.UpdateSupplierPrices(creditsRequest);
            if (creditsResponse.Status == NoProblem.Core.DataModel.Expertise.UpdateSupplierPricesResponse.UpdateSupplierPricesStatus.PriceOverRechargePrice)
            {
                result.Type = Result.eResultType.Failure;
                result.Messages.Add("Heading prices could not be updated. Recharge amount too low.");
            }
        }

        private void DoDeposit(DataModel.SupplierModel.SupplierComplete.SupplierComplete supplier, Result<Guid> result)
        {
            if (supplier.PaymentAmount > 0)
            {
                SupplierPricing.SupplierPricingManager depositsManager = new NoProblem.Core.BusinessLogic.SupplierPricing.SupplierPricingManager(XrmDataContext);
                var depositResponse = depositsManager.CreateSupplierPricing(
                    new NoProblem.Core.DataModel.SupplierPricing.CreateSupplierPricingRequest()
                    {
                        SupplierId = supplier.SupplierId,
                        BonusType = NoProblem.Core.DataModel.Xrm.new_supplierpricing.BonusType.None,
                        BonusAmount = 0,
                        ExtraBonus = 0,
                        PaymentMethod = NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.Cash,
                        PaymentAmount = supplier.PaymentAmount,
                        Action = DataModel.Xrm.new_balancerow.Action.Deposit
                    });
                if (depositResponse.DepositStatus != NoProblem.Core.DataModel.SupplierPricing.eDepositStatus.OK)
                {
                    result.Type = Result.eResultType.Failure;
                    result.Messages.Add(string.Format("Money was not deposited in the advertisers account. DepositStatus: {0}", depositResponse.DepositStatus));
                }
            }
        }

        #endregion
    }
}
