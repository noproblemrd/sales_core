﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.SupplierLogic.SupplierComplete
{
    internal class Validator : ValidatorBase
    {
        internal static void ValidateSupplierXml(string xmlString, string url, Result result)
        {
            XElement xml;
            try
            {
                xml = XElement.Parse(xmlString);
            }
            catch
            {
                AddFailureValidation(result, "Could not parse xml");
                return;
            }

            bool isUpdate = ValidateMainAttributes(result, xml);

            ValidateGeneralInfo(result, xml);

            ValidateHeadings(result, xml);

            ValidateRegions(result, xml);

            ValidateAvailability(result, xml);

            ValidateBalance(result, xml, isUpdate);

            try
            {
                Uri uri = new Uri(url);
            }
            catch
            {
                AddFailureValidation(result, "Invalid call back url");
            }
        }

        private static bool ValidateMainAttributes(Result result, XElement xml)
        {
            bool isUpdate = false;
            XAttribute methodAttr = xml.Attribute("Method");
            if (methodAttr == null)
            {
                AddFailureValidation(result, "Method attribute is mandatory");
            }
            else
            {
                if (methodAttr.Value != "Create" && methodAttr.Value != "Update")
                {
                    AddFailureValidation(result, "Method must be 'Create' or 'Update'");
                }
                if (methodAttr.Value == "Update")
                {
                    isUpdate = true;
                    XAttribute idAttr = xml.Attribute("Id");
                    if (idAttr == null)
                    {
                        AddFailureValidation(result, "Id attribute is mandatory when Method is Update");
                    }
                    else
                    {
                        try
                        {
                            new Guid(idAttr.Value);
                        }
                        catch
                        {
                            AddFailureValidation(result, "Id must be a valid Guid");
                        }
                    }
                }
            }
            return isUpdate;
        }

        private static void ValidateBalance(Result result, XElement xml, bool isUpdate)
        {
            XElement balanceElmt = xml.Element("Balance");
            if (balanceElmt != null)
            {
                if (isUpdate)
                {
                    AddFailureValidation(result, "Balance element not allowed in Update");
                }
                int balance;
                bool parsed = int.TryParse(balanceElmt.Value, out balance);
                if (!parsed)
                {
                    AddFailureValidation(result, "Balance value must be an integer");
                }
                else if (balance <= 0)
                {
                    AddFailureValidation(result, "Balance must be larger than zero");
                }
            }
        }

        private static void ValidateAvailability(Result result, XElement xml)
        {
            XElement daysElmt = xml.Element("Availability");
            if (daysElmt == null)
            {
                AddFailureValidation(result, "Availability element is mandatory");
            }
            else
            {
                IEnumerable<XElement> allDays = daysElmt.Elements("Day");
                if (allDays.Count() == 0)
                {
                    AddFailureValidation(result, "At least one day must be provided");
                }
                foreach (var item in allDays)
                {
                    try
                    {
                        DayOfWeek day = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), item.Value);
                    }
                    catch
                    {
                        AddFailureValidation(result, "The value of day must be Sunday, Monday, Tuesday, Wednesday, Thursday, Friday or Saturday");
                    }
                    XAttribute fromAttribute = item.Attribute("FromHour");
                    XAttribute toAttribute = item.Attribute("ToHour");
                    if (fromAttribute == null || toAttribute == null)
                    {
                        AddFailureValidation(result, "FromHour and ToHour attributes must me provided for every day element");
                    }
                    else
                    {
                        string fromStr = item.Attribute("FromHour").Value;
                        string toStr = item.Attribute("ToHour").Value;
                        int from;
                        bool parsedFrom = int.TryParse(fromStr, out from);
                        int to;
                        bool parsedTo = int.TryParse(toStr, out to);
                        if (!parsedFrom || !parsedTo)
                        {
                            AddFailureValidation(result, "FromHour and ToHour values must be integers");
                        }
                        else
                        {
                            if (from >= to)
                            {
                                AddFailureValidation(result, "FromHour must be smaller than ToHour");
                            }
                            if (from > 23 || from < 0 || to < 1 || to > 24)
                            {
                                AddFailureValidation(result, "FromHour must be from 0 to 23, ToHour must be from 1 to 24");
                            }
                        }
                    }
                }
            }
        }

        private static void ValidateRegions(Result result, XElement xml)
        {
            XElement regsElmt = xml.Element("Regions");
            if (regsElmt == null)
            {
                AddFailureValidation(result, "Regions element is mandatory");
            }
            else
            {
                IEnumerable<XElement> allRegions = regsElmt.Elements("Region");
                if (allRegions.Count() == 0)
                {
                    AddFailureValidation(result, "At least one region must be provided");
                }
                foreach (var item in allRegions)
                {
                    if (string.IsNullOrEmpty(item.Value))
                    {
                        AddFailureValidation(result, "All region must have a region code as value");
                    }
                    XAttribute levelAttribute = item.Attribute("Level");
                    if (levelAttribute == null)
                    {
                        AddFailureValidation(result, "Level attribute must be provided for every region.");
                    }
                    else
                    {
                        int x;
                        bool parsed = int.TryParse(levelAttribute.Value, out x);
                        if (!parsed)
                        {
                            AddFailureValidation(result, "Level must be an integer.");
                        }
                    }
                }
            }
        }

        private static void ValidateGeneralInfo(Result result, XElement xml)
        {
            XElement nameElmt = xml.Element("Name");
            if (nameElmt == null || string.IsNullOrEmpty(nameElmt.Value))
            {
                AddFailureValidation(result, "Name is mandatory");
            }
            XElement phoneElmt = xml.Element("Phone");
            if (phoneElmt == null)
            {
                AddFailureValidation(result, "Phone is mandatory");
            }
            else if (string.IsNullOrEmpty(phoneElmt.Value))
            {
                AddFailureValidation(result, "Phone is mandatory");
            }
            else
            {
                foreach (var letter in phoneElmt.Value)
                {
                    if (!Char.IsDigit(letter))
                    {
                        AddFailureValidation(result, "Phone can only contain digits");
                        break;
                    }
                }
            }
        }

        private static void ValidateHeadings(Result result, XElement xml)
        {
            XElement headsElmt = xml.Element("Headings");
            if (headsElmt == null)
            {
                AddFailureValidation(result, "Headings element is mandatory");
            }
            else
            {
                IEnumerable<XElement> allHeads = headsElmt.Elements("Heading");
                if (allHeads.Count() == 0)
                {
                    AddFailureValidation(result, "At least one heading must be provided");
                }
                foreach (var item in allHeads)
                {
                    if (string.IsNullOrEmpty(item.Value))
                    {
                        AddFailureValidation(result, "Every heading element must have a heading code as value.");
                    }
                    XAttribute priceAttribute = item.Attribute("Price");
                    if (priceAttribute == null)
                    {
                        AddFailureValidation(result, "Price attribute must be provided for every heading.");
                    }
                    else
                    {
                        int x;
                        bool parsed = int.TryParse(priceAttribute.Value, out x);
                        if (!parsed)
                        {
                            AddFailureValidation(result, "Price must be an integer.");
                        }
                    }
                }
            }
        }
    }
}
