﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic
{
    internal class RankingNotifications : XrmUserBase
    {
        private Guid supplierId;

        internal RankingNotifications(Guid supplierId)
            :base(null)//null because we want to use a new data context, because this class's operations are async (and data context object in not thread safe).
        {
            this.supplierId = supplierId;
        }

        internal void NotifyOtherSuppliers()
        {
            Thread t1 = new Thread(delegate()
            {
                try
                {
                    LogUtils.MyHandle.WriteToLog(8, "Ranking Notifications started.");
                    NotifyOthers();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Ranking Notifications failed with exception.");
                }
            });
            t1.Start();
        }

        private void NotifyOthers()
        {
            Notifications notifications = new Notifications(XrmDataContext);
            var notificationMethod = notifications.GetNotificationMethod(DataModel.NotificationConfigurationKeys.NOTIFICATION_RANK_LOW);
            if (notificationMethod == NoProblem.Core.DataModel.enumNotificationMethod.NONE)
            {
                return;
            }
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            List<DataModel.SqlHelper.RankingNotificationSupplier> toNotify = dal.GetSuppliersForRankingNotification(supplierId);
            List<Guid> doneSuppliers = new List<Guid>();
            Expertises.ExpertiseManager expertiseManager = new NoProblem.Core.BusinessLogic.Expertises.ExpertiseManager(XrmDataContext);
            foreach (var item in toNotify)
            {
                if (doneSuppliers.Contains(item.SupplierId))
                {
                    continue;
                }
                int rank = expertiseManager.GetRankingInExpertise(item.ExpertiseId, item.IncidentPrice, item.SupplierId);
                if (rank > item.RankingNotification)
                {
                    bool notified = notifications.NotifySupplier(NoProblem.Core.DataModel.enumSupplierNotificationTypes.RANK_LOW,
                        item.SupplierId, item.SupplierId.ToString(), "account",
                        item.SupplierId.ToString(), "account");
                    if (notified)
                    {
                        doneSuppliers.Add(item.SupplierId);
                    }
                }
            }
        }
    }
}
