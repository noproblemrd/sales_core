﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Effect.Crm.Configuration;
using Effect.Crm.Convertor;
using Effect.Crm.Logs;
using Effect.Crm.Methods;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using NoProblem.Core.BusinessLogic.PublisherPortal;
using NoProblem.Core.BusinessLogic.Utils;
using NoProblem.Core.DataModel.AdvertiserPortal;
using NoProblem.Core.DataModel.AdvertiserRankings;
using NoProblem.Core.DataModel.PublisherPortal;
using DML = NoProblem.Core.DataModel;
using System.Threading;

namespace NoProblem.Core.BusinessLogic
{
    public partial class Supplier : ContextsUserBase
    {
        #region Ctor

        public Supplier(DML.Xrm.DataContext xrmDataContext, CrmService crmService)
            : base(xrmDataContext, crmService)
        {
        }

        #endregion

        private static readonly string cachePrefix = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();
        private readonly string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];

        public delegate void Action<T1, T2>(T1 t1, T2 t2);
        private delegate void CreateExposureDelegate(XElement suppliersXml, string SiteId, string ExpertiseCode, int ExpertiseLevel, string ServiceAreaCode, int ServiceAreaLevel, string OriginId,
            string controlName, string domain, string pageName, string placeInWebSite, string sessionId, string url);
        private delegate void CreateExposureDelegateStr(string suppliersXml, string SiteId, string ExpertiseCode, int ExpertiseLevel, string ServiceAreaCode, int ServiceAreaLevel, string OriginId,
           string controlName, string domain, string pageName, string placeInWebSite, string sessionId, string url);
        public string connStr = ConfigurationManager.AppSettings["updateConnectionString"];

        public string GetAllExpertises(string supplierIdStr)
        {
            Guid supplierId = new Guid(supplierIdStr);
            PublisherPortalManager publisherUtils = new PublisherPortalManager(XrmDataContext, CrmService);
            string allExpertiseStr = publisherUtils.GetExpertise("", "");
            XDocument allExpertiseDoc = XDocument.Parse(allExpertiseStr);
            XElement expertisesElement = allExpertiseDoc.Element("Expertise");
            IEnumerable<XElement> allPrimaries = expertisesElement.Elements();

            foreach (XElement node in allPrimaries)
            {
                node.SetAttributeValue("Selected", "False");
                node.SetAttributeValue("Certificate", "False");
                IEnumerable<XElement> secondaryElements = node.Elements();
                foreach (XElement subNode in secondaryElements)
                {
                    subNode.SetAttributeValue("Selected", "False");
                }
            }

            if (supplierId != Guid.Empty)
            {
                string supplierDetailsStr = GetSupplierDetails(supplierId.ToString());
                XDocument supplierDetailsDoc = XDocument.Parse(supplierDetailsStr);
                allExpertiseDoc.Element("Expertise").SetAttributeValue("RegistrationStage", supplierDetailsDoc.Element("Supplier").Element("Details").Attribute("RegistrationStage").Value);
                IEnumerable<XElement> supplierPrimaries = supplierDetailsDoc.Element("Supplier").Element("Expertise").Elements();
                foreach (XElement node in supplierPrimaries)
                {
                    string id = node.Attribute("ID").Value;
                    foreach (XElement nodeInAll in allPrimaries)
                    {
                        if (nodeInAll.Attribute("ID").Value == id)
                        {
                            nodeInAll.SetAttributeValue("Selected", "True");
                            if (node.Attribute("Certificate").Value == "True")
                            {
                                nodeInAll.SetAttributeValue("Certificate", "True");
                            }
                            else
                            {
                                nodeInAll.SetAttributeValue("Certificate", "False");
                            }
                            IEnumerable<XElement> supplierSecondaries = node.Elements();
                            foreach (XElement subNode in supplierSecondaries)
                            {
                                string subId = subNode.Attribute("ID").Value;
                                IEnumerable<XElement> subNodesInAll = nodeInAll.Elements();
                                foreach (XElement subInAll in subNodesInAll)
                                {
                                    if (subInAll.Attribute("ID").Value == subId)
                                    {
                                        subInAll.SetAttributeValue("Selected", "True");
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
            return allExpertiseDoc.ToString();
        }

        #region Login Methods

        public string SupplierSignUp(string email, string cellphone, string originCode)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            Guid supplierId = Guid.Empty;
            if (accountRepositoryDal.IsSupplierExists(email))
            {
                return "<SupplierSignUp><Error>Email allready exists</Error></SupplierSignUp>";
            }
            // Get a new password
            string password = LoginUtils.GenerateNewPassword();
            // Create the supplier in the CRM
            DynamicEntity supplier = ConvertorCRMParams.GetNewDynamicEntity("account");
            ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_password", password, typeof(StringProperty));
            //ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "accountnumber", "***", typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "name", "***", typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_status", ConvertorCRMParams.GetCrmPicklist((int)DML.Xrm.account.SupplierStatus.Candidate), typeof(PicklistProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "emailaddress1", email, typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "telephone1", cellphone, typeof(StringProperty));
            if (string.IsNullOrEmpty(originCode) == false)
            {
                DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
                Guid originId = originDal.GetOriginIdByCode(originCode);
                if (originId != Guid.Empty)
                {
                    ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_originid", ConvertorCRMParams.GetCrmLookup("new_origin", originId), typeof(LookupProperty));
                }
            }
            //by Yoav
            ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_incidentpricechangeable", ConvertorCRMParams.GetCrmBoolean(true), typeof(CrmBooleanProperty));
            supplierId = MethodsUtils.CreateEntity(CrmService, supplier);
            //Send email and sms to the supplier with the password
            Notifications notificationsLogic = new Notifications(XrmDataContext);
            notificationsLogic.NotifySupplier(DML.enumSupplierNotificationTypes.SUPPLIER_SIGNUP, supplierId, supplierId.ToString(), "account", supplierId.ToString(), "account");
            return "<SupplierSignUp><Status>" + supplierId.ToString() + "</Status></SupplierSignUp>";
        }

        public DataModel.PublisherPortal.UserLoginResponse UserLoginForAffiliates(string Email, string Password)
        {
            UserLoginResponse response = new UserLoginResponse();
            string query = "SELECT new_affiliateoriginid,accountid,name,new_cashbalance,new_securitylevel,new_stageinregistration, new_status FROM account with (nolock) WHERE deletionstatecode = 0 AND emailaddress1 = @email AND new_password = @password";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@email", Email);
                    command.Parameters.AddWithValue("@password", Password);
                    using (SqlDataReader details = command.ExecuteReader())
                    {
                        if (details.Read())
                        {
                            string statusStr = details["new_status"].ToString();
                            int status = 3;//temporarily mark as inactive
                            bool statusParsed = int.TryParse(statusStr, out status);
                            if (!statusParsed || status == (int)DML.Xrm.account.SupplierStatus.Inactive)
                            {
                                response.UserLoginStatus = eUserLoginStatus.UserInactive;
                            }
                            else
                            {
                                response.UserId = new Guid(details["accountid"].ToString());
                                response.Name = details["name"].ToString();
                                string balanceStr = details["new_cashbalance"].ToString();
                                decimal balanceDec;
                                bool balanceParseGood = decimal.TryParse(balanceStr, out balanceDec);
                                if (balanceParseGood)
                                {
                                    response.Balance = balanceDec;
                                }
                                else
                                {
                                    response.Balance = 0;
                                }
                                string securityStr = details["new_securitylevel"].ToString();
                                int securityInt;
                                bool securityParseGood = int.TryParse(securityStr, out securityInt);
                                if (securityParseGood)
                                {
                                    response.SecurityLevel = securityInt;
                                }
                                else
                                {
                                    response.SecurityLevel = -1;
                                }
                                Guid affiliateOrigin = details["new_affiliateoriginid"] != DBNull.Value ? (Guid)details["new_affiliateoriginid"] : Guid.Empty;
                                response.AffiliateOrigin = affiliateOrigin;
                                string regStageStr = details["new_stageinregistration"].ToString();
                                int regStageInt;
                                bool regStagePargeGood = int.TryParse(regStageStr, out regStageInt);
                                if (regStagePargeGood)
                                {
                                    response.StageInRegistration = regStageInt;
                                }
                                else
                                {
                                    response.StageInRegistration = 0;
                                }
                                response.Site = "1";
                                response.UserLoginStatus = eUserLoginStatus.OK;
                            }
                        }
                        else
                        {
                            response.UserLoginStatus = eUserLoginStatus.NotFound;
                        }
                    }
                }
            }
            LogUtils.MyHandle.WriteToLog(8, "UserLogin- Response: UserId: {0}, Name: {1}, Balance: {2}, SecurityLevel: {3}, UserLoginStatus: {4}",
                response.UserId.ToString(), response.Name, response.Balance.ToString(), response.SecurityLevel.ToString(), response.UserLoginStatus.ToString());
            return response;
        }

        public DataModel.PublisherPortal.UserLoginResponse UserLogin(string Email, string Password)
        {
            UserLoginResponse response = new UserLoginResponse();
            string query = "SELECT new_affiliateoriginid,accountid,name,new_cashbalance,new_securitylevel,new_stageinregistration, new_canhearallrecordings, new_status FROM account with (nolock) WHERE deletionstatecode = 0 AND emailaddress1 = @email AND new_password = @password";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@email", Email);
                    command.Parameters.AddWithValue("@password", Password);
                    using (SqlDataReader details = command.ExecuteReader())
                    {
                        if (details.Read())
                        {
                            string statusStr = details["new_status"].ToString();
                            int status = 3;//temporarily mark as inactive
                            bool statusParsed = int.TryParse(statusStr, out status);
                            if (!statusParsed || status == (int)DML.Xrm.account.SupplierStatus.Inactive)
                            {
                                response.UserLoginStatus = eUserLoginStatus.UserInactive;
                            }
                            else
                            {
                                response.UserId = new Guid(details["accountid"].ToString());
                                response.Name = details["name"].ToString();
                                string balanceStr = details["new_cashbalance"].ToString();
                                decimal balanceDec;
                                bool balanceParseGood = decimal.TryParse(balanceStr, out balanceDec);
                                if (balanceParseGood)
                                {
                                    response.Balance = balanceDec;
                                }
                                else
                                {
                                    response.Balance = 0;
                                }
                                string securityStr = details["new_securitylevel"].ToString();
                                int securityInt;
                                bool securityParseGood = int.TryParse(securityStr, out securityInt);
                                if (securityParseGood)
                                {
                                    response.SecurityLevel = securityInt;
                                }
                                else
                                {
                                    response.SecurityLevel = -1;
                                }
                                Guid affiliateOrigin = details["new_affiliateoriginid"] != DBNull.Value ? (Guid)details["new_affiliateoriginid"] : Guid.Empty;
                                response.AffiliateOrigin = affiliateOrigin;
                                string regStageStr = details["new_stageinregistration"].ToString();
                                int regStageInt;
                                bool regStagePargeGood = int.TryParse(regStageStr, out regStageInt);
                                if (regStagePargeGood)
                                {
                                    response.StageInRegistration = regStageInt;
                                }
                                else
                                {
                                    response.StageInRegistration = 0;
                                }
                                response.CanListenToRecords = details["new_canhearallrecordings"] == DBNull.Value ? false : (bool)details["new_canhearallrecordings"];
                                response.Site = "1";
                                response.UserLoginStatus = eUserLoginStatus.OK;
                            }
                        }
                        else
                        {
                            response.UserLoginStatus = eUserLoginStatus.NotFound;
                        }
                        if (response.AffiliateOrigin != Guid.Empty)
                        {
                            response.UserLoginStatus = eUserLoginStatus.NotFound;
                            response.Balance = 0;
                            response.Name = "";
                            response.SecurityLevel = 0;
                            response.UserId = Guid.Empty;
                        }
                    }
                }
            }
            LogUtils.MyHandle.WriteToLog(8, "UserLogin- Response: UserId: {0}, Name: {1}, Balance: {2}, SecurityLevel: {3}, UserLoginStatus: {4}",
                response.UserId.ToString(), response.Name, response.Balance.ToString(), response.SecurityLevel.ToString(), response.UserLoginStatus.ToString());
            return response;
        }

        public ResetPasswordResponse ResetPassword(ResetPasswordRequest request)
        {
            ResetPasswordResponse response = new ResetPasswordResponse();
            DataAccessLayer.AccountRepository accountRepositoryAccess = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account account = accountRepositoryAccess.GetAccountByPhoneAndEmail(request.PhoneNumber, request.Email);
            if (account != null)
            {
                string password = LoginUtils.GenerateNewPassword();
                account.new_password = password;
                accountRepositoryAccess.Update(account);
                XrmDataContext.SaveChanges();
                Notifications notificationLogic = new Notifications(XrmDataContext);
                notificationLogic.NotifySupplier(DML.enumSupplierNotificationTypes.SUPPLIER_SIGNUP, account.accountid, account.accountid.ToString(), "account", account.accountid.ToString(), "account");
                response.ResetPasswordStatus = ResetPasswordResponseStatus.OK;
            }
            else
            {
                response.ResetPasswordStatus = ResetPasswordResponseStatus.UserNotFound;
            }
            return response;
        }

        public string ChangePasswordByEmail(string email, string oldPassword, string newPassword)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.GetSupplierByEmail(email);
            if (supplier == null)
            {
                return "<ChangePassword><Error>The supplier does not exist</Error></ChangePassword>";
            }
            string retVal = ChangePassword(supplier.accountid.ToString(), oldPassword, newPassword);
            return retVal;
        }

        public string ChangePassword(string supplierId, string oldPassword, string newPassword)
        {
            if (!LoginUtils.ValidatePassword(newPassword))
                return "<ChangePassword><Error>Password does not fit the requirements of the organization</Error></ChangePassword>";

            Guid supplierIdGuid = new Guid(supplierId);

            //Verify the old password
            //object isSupplierExists = Effect.Crm.DB.DataBaseUtils.GetValueFromSqlString("SELECT '1' FROM account with (nolock) WHERE accountid = '{0}' AND new_password = '{1}'", SupplierId, OldPassword);
            string sqlGetValue = "SELECT '1' FROM account with (nolock) WHERE accountid = @id AND new_password = @oldPassword";
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList checkParameters = new DataAccessLayer.Generic.SqlParametersList();
            checkParameters.Add("@id", supplierIdGuid);
            checkParameters.Add("@oldPassword", oldPassword);
            object scalar = dal.ExecuteScalar(sqlGetValue, checkParameters);
            bool oldPasswordOk = (scalar != null && scalar != DBNull.Value);

            //Update the password
            //if (!ConvertorUtils.ToBoolean(isSupplierExists))
            if (!oldPasswordOk)
            {
                return "<ChangePassword><Error>The supplier does not exist</Error></ChangePassword>";
            }
            else
            {
                // Update the supplier in the CRM
                //Effect.Crm.DB.DataBaseUtils.DirectUpdateInstance("account", new Guid(SupplierId), new string[] { "new_password" }, new object[] { NewPassword });
                string sqlUpdate = "update account set new_password = @newPassword where accountid = @id";
                DataAccessLayer.Generic.SqlParametersList updateParameters = new DataAccessLayer.Generic.SqlParametersList();
                updateParameters.Add("@id", supplierIdGuid);
                updateParameters.Add("@newPassword", newPassword);
                dal.ExecuteNonQuery(sqlUpdate, updateParameters);
            }

            return "<ChangePassword><Status>Success</Status></ChangePassword>";
        }
        #endregion

        #region Searching Methods

        public string SearchSiteSuppliersExpanded(string siteId, string expertiseCode, int expertiseLevel, string serviceAreaCode, int serviceAreaLevel, string originId, int maxResults, Guid upsaleId, int pageNumber, int pageSize, bool withNoTimeNorCash, bool sortByHasDirectNumber, bool isIncludingTrial, bool isDondil)
        {
            XElement suppliersXml = SearchSiteSuppliers(siteId, expertiseCode, expertiseLevel, serviceAreaCode, serviceAreaLevel, originId, maxResults, upsaleId, pageNumber, pageSize, withNoTimeNorCash, sortByHasDirectNumber, isIncludingTrial, null);
            IEnumerable<XElement> elements = suppliersXml.Elements("Supplier");
            List<string> supplierIds = new List<string>();
            foreach (var item in elements)
            {
                string guidStr = item.Attribute("SupplierId").Value;
                supplierIds.Add(guidStr);
            }
            List<DML.SqlHelper.AccountExpandedFields> expandedFields = null;
            string expertiseName = string.Empty;
            if (expertiseLevel == 1)
            {
                DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
                expertiseName = expertiseDal.GetPrimaryExpertiseNameByCode(expertiseCode);
            }
            else
            {
                DataAccessLayer.SecondaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.SecondaryExpertise(XrmDataContext);
                expertiseName = expertiseDal.GetSecondaryExpertiseNameByCode(expertiseCode);
            }
            suppliersXml.SetAttributeValue("ExpertiseName", expertiseName);
            if (supplierIds.Count > 0)
            {
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                expandedFields = accountRepositoryDal.GetAccountsExpandedFields(supplierIds, isDondil);
            }
            foreach (var item in elements)
            {
                Guid supplierId = new Guid(item.Attribute("SupplierId").Value);
                DML.SqlHelper.AccountExpandedFields fields = expandedFields.First(x => x.SupplierId == supplierId);
                item.SetAttributeValue("Country", fields.Country);
                item.SetAttributeValue("State", fields.State);
                item.SetAttributeValue("City", fields.City);
                item.SetAttributeValue("District", fields.District);
                item.SetAttributeValue("Street", fields.Street);
                item.SetAttributeValue("StreetNumber", fields.StreetNumber);
                item.SetAttributeValue("ZipCode", fields.ZipCode);
                item.SetAttributeValue("Likes", fields.Likes);
                item.SetAttributeValue("Dislikes", fields.Dislikes);
                item.SetAttributeValue("Crm3Url", fields.Crm3Url);
                if (isDondil)
                {
                    item.Attribute("DirectNumber").Value = fields.RealPhone;
                }
            }
            string retVal = suppliersXml.ToString();
            return retVal;
        }

        public XElement SearchSiteSuppliersDapazPoc(string headingCodeDapaz, string mekomonCode, Guid originId)
        {
            return DataAccessLayer.CacheManager.Instance.Get<XElement>(cachePrefix + ".SearchSiteSuppliersDapazPoc", headingCodeDapaz + ";" + mekomonCode + ";" + originId.ToString(), SearchSiteSuppliersDapazPocPrivateMethod, 15);
        }

        private XElement SearchSiteSuppliersDapazPocPrivateMethod(string cacheKey)
        {
            string[] split = cacheKey.Split(';');
            string headingCodeDapaz = split[0];
            string mekomonCode = split[1];
            string originId = split[2];
            DataAccessLayer.PrimaryExpertise headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            string headingCode = headingDal.GetPrimaryExpertiseCodeByEnglishName(headingCodeDapaz);
            XElement result = SearchSiteSuppliers(null, headingCode, 1, mekomonCode, 2, originId, -1, Guid.Empty, -1, -1, false, false, false, DateTime.Now.AddMinutes(30));
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            foreach (XElement item in result.Elements("Supplier"))
            {
                string bizId = accountRepositoryDal.GetBizId(item.Attribute("SupplierId").Value);
                item.SetAttributeValue("BizId", bizId);
            }
            return result;
        }

        public XElement SearchSiteSuppliers(string siteId, string expertiseCode, int expertiseLevel, string serviceAreaCode, int serviceAreaLevel, string originId, int maxResults, Guid upsaleId, int pageNumber, int pageSize, bool withNoTimeNorCash, bool sortByHasDirectNumbers, bool isIncludingTrial, DateTime? availabilityTime)
        {
            DML.FindSupplierParams findParams = new DML.FindSupplierParams();
            findParams.ExpertiseCode = expertiseCode;
            findParams.ExpertiseLevel = expertiseLevel;
            findParams.AreaLevel = serviceAreaLevel;
            findParams.AreaCode = serviceAreaCode;
            if (string.IsNullOrEmpty(originId) == false)
            {
                findParams.OriginId = new Guid(originId);
            }

            if (upsaleId != Guid.Empty)
            {
                DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                List<Guid> incidentsIds = incidentDal.GetIncidentsIdsByUpsale(upsaleId);
                findParams.Incidents = incidentsIds;
            }
            else
            {
                findParams.Incidents = new List<Guid>();
            }
            if (availabilityTime.HasValue)
            {
                findParams.Availability = availabilityTime.Value;
            }
            else
            {
                findParams.Availability = DateTime.Now;
            }
            findParams.IsWithNoTimeNorCash = withNoTimeNorCash;
            findParams.IsIncludingTrial = isIncludingTrial;
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string dnHaveRegion = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.DIRECT_NUMBERS_HAVE_REGION);
            if (dnHaveRegion == "1")
            {
                DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
                Guid regLevel1Id = regionDal.GetLevel1Parent(serviceAreaCode, serviceAreaLevel);
                findParams.DirectNumberRegionId = regLevel1Id;
            }

            SqlHelper helper = new SqlHelper(ConfigurationUtils.GetConfigurationItem("ConnectionString"));

            //Get results from database
            DataSet AvailableSuppliers = helper.GetSuppliers(findParams);

            //Get the result table
            DataTable AvailableSuppliersTable = AvailableSuppliers.Tables[0];
            bool noSuppliers = false;
            //Check if error occur in GetSuppliers/SQL - 
            if (AvailableSuppliersTable.Columns[0].ColumnName == "ERROR")
            {
                LogUtils.MyHandle.WriteToLog(8, "SearchSiteSuppliers failed with Error (no suppliers found) : {0}", AvailableSuppliersTable.Rows[0]["ERROR"].ToString());
                //return "<Suppliers><Error>Failed</Error></Suppliers>";
                noSuppliers = true;
            }
            int numOfPages = 0;
            IEnumerable<SupplierEntry> sortedSuppliers = null;
            if (noSuppliers == false)
            {
                AdvertiserRankingsManager rankingsManager = new AdvertiserRankingsManager(XrmDataContext);
                List<SupplierEntry> sortedSuppliersList = rankingsManager.SortSuppliersByRanking(AvailableSuppliersTable);
                if (maxResults > 0 && maxResults < sortedSuppliersList.Count)
                {
                    sortedSuppliersList.RemoveRange(maxResults, sortedSuppliersList.Count - maxResults);
                }
                if (sortByHasDirectNumbers)
                {
                    sortedSuppliersList = SortByHasDirectNumbers(sortedSuppliersList);
                }
                sortedSuppliers = sortedSuppliersList;
                numOfPages = 1;
                if (pageNumber > 0 && pageSize > 0)//paging
                {
                    sortedSuppliers = sortedSuppliersList.Skip(pageSize * (pageNumber - 1)).Take(pageSize);
                    numOfPages = (int)Math.Ceiling(((double)sortedSuppliersList.Count()) / ((double)pageSize));
                }
            }
            DataAccessLayer.ConfigurationSettings configSettings = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string extensionNumber = configSettings.GetConfigurationSettingValue(DML.ConfigurationKeys.EXTENSION_NUMBER);
            XElement root = new XElement("Suppliers");
            root.Add(new XAttribute("ExtensionNumber", extensionNumber));
            root.Add(new XAttribute("NumberOfPages", numOfPages));
            if (noSuppliers == false)
            {
                foreach (SupplierEntry supplierEntry in sortedSuppliers)
                {
                    XElement supplierNode = new XElement("Supplier");
                    string accountid = supplierEntry.SupplierId.ToString();
                    supplierNode.Add(new XAttribute("SupplierId", accountid));
                    supplierNode.Add(new XAttribute("SupplierNumber", supplierEntry.SupplierNumber));
                    supplierNode.Add(new XAttribute("SupplierName", supplierEntry.SupplierName));
                    supplierNode.Add(new XAttribute("SumSurvey", supplierEntry.SumSurvey));
                    supplierNode.Add(new XAttribute("SumAssistanceRequests", supplierEntry.SumAssistanceRequests));
                    supplierNode.Add(new XAttribute("NumberOfEmployees", supplierEntry.NumberOfEmployees));
                    supplierNode.Add(new XAttribute("ShortDescription", supplierEntry.ShortDescription));
                    supplierNode.Add(new XAttribute("Certificate", supplierEntry.Certificate));
                    supplierNode.Add(new XAttribute("DirectNumber", supplierEntry.DirectNumber));
                    root.Add(supplierNode);
                }
            }
            return root;
        }

        private List<SupplierEntry> SortByHasDirectNumbers(List<SupplierEntry> sortedSuppliersList)
        {
            var notAvailable = sortedSuppliersList.Where(x => x.DirectNumber == "0");
            var tempNotAvailable = new List<SupplierEntry>(notAvailable);
            sortedSuppliersList.RemoveAll(x => x.DirectNumber == "0");

            var noDn = sortedSuppliersList.Where(x => x.DirectNumber == "");
            var tempNoDNumber = new List<SupplierEntry>(noDn);
            sortedSuppliersList.RemoveAll(x => string.IsNullOrEmpty(x.DirectNumber));

            sortedSuppliersList.AddRange(tempNoDNumber);
            sortedSuppliersList.AddRange(tempNotAvailable);

            return sortedSuppliersList;
        }

        public XElement SearchSiteSuppliersWithExposure(string siteId, string expertiseCode, int expertiseLevel, string serviceAreaCode, int serviceAreaLevel, string originId, int maxResults, Guid upsaleId, int pageNumber, int pageSize, bool withNoTimeNorCash, bool sortByHasDirectNumber
            , string controlName, string domain, string pageName, string placeInWebSite, string sessionId, string url)
        {
            XElement suppliersXml = SearchSiteSuppliers(siteId, expertiseCode, expertiseLevel, serviceAreaCode, serviceAreaLevel, originId, maxResults, upsaleId, pageNumber, pageSize, withNoTimeNorCash, sortByHasDirectNumber, false, null);
            CreateExposureDelegate exposureAsync = new CreateExposureDelegate(CreateExposureFromSearchAsync);
            exposureAsync.BeginInvoke(suppliersXml, siteId, expertiseCode, expertiseLevel, serviceAreaCode, serviceAreaLevel, originId,
                    controlName, domain, pageName, placeInWebSite, sessionId, url, null, null);
            return suppliersXml;
        }

        public string SearchSiteSuppliersExpandedWithExposure(string siteId, string expertiseCode, int expertiseLevel, string serviceAreaCode, int serviceAreaLevel, string originId, int maxResults, Guid upsaleId, int pageNumber, int pageSize, bool withNoTimeNorCash, bool sortByHasDirectNumber,
            string controlName, string domain, string pageName, string placeInWebSite, string sessionId, string url)
        {
            string retVal = SearchSiteSuppliersExpanded(siteId, expertiseCode, expertiseLevel, serviceAreaCode, serviceAreaLevel, originId, maxResults, upsaleId, pageNumber, pageSize, withNoTimeNorCash, sortByHasDirectNumber, false, false);
            CreateExposureDelegateStr exposureAsyncStr = new CreateExposureDelegateStr(CreateExposureFromSearchAsyncStr);
            exposureAsyncStr.BeginInvoke(retVal, siteId, expertiseCode, expertiseLevel, serviceAreaCode, serviceAreaLevel, originId,
                    controlName, domain, pageName, placeInWebSite, sessionId, url, null, null);
            return retVal;
        }

        public string SearchSiteSuppliersExpandedWithExposureWithZipcodeUSA(string siteId, string expertiseCode, int expertiseLevel, string serviceAreaCode, int serviceAreaLevel, string originId, int maxResults, Guid upsaleId, int pageNumber, int pageSize, bool withNoTimeNorCash, bool sortByHasDirectNumber,
            string controlName, string domain, string pageName, string placeInWebSite, string sessionId, string url)
        {
            DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            serviceAreaCode = regDal.GetCodeByNameAndLevel(serviceAreaCode, 4);
            serviceAreaLevel = 4;
            string retVal = SearchSiteSuppliersExpanded(siteId, expertiseCode, expertiseLevel, serviceAreaCode, serviceAreaLevel, originId, maxResults, upsaleId, pageNumber, pageSize, withNoTimeNorCash, sortByHasDirectNumber, false, false);
            CreateExposureDelegateStr exposureAsyncStr = new CreateExposureDelegateStr(CreateExposureFromSearchAsyncStr);
            exposureAsyncStr.BeginInvoke(retVal, siteId, expertiseCode, expertiseLevel, serviceAreaCode, serviceAreaLevel, originId,
                    controlName, domain, pageName, placeInWebSite, sessionId, url, null, null);
            return retVal;
        }

        private void CreateExposureFromSearchAsync(XElement suppliersXml, string SiteId, string ExpertiseCode, int ExpertiseLevel, string ServiceAreaCode, int ServiceAreaLevel, string OriginId,
            string controlName, string domain, string pageName, string placeInWebSite, string sessionId, string url)
        {
            try
            {
                IEnumerable<XElement> elements = suppliersXml.Elements("Supplier");
                int supplierCount = elements.Count();
                Dashboard.ConvertionsManager convertionManager = new NoProblem.Core.BusinessLogic.Dashboard.ConvertionsManager(null);
                DataModel.Dashboard.CreateExposureRequest exposureRequest = new NoProblem.Core.DataModel.Dashboard.CreateExposureRequest()
                {
                    ControlName = controlName,
                    Domain = domain,
                    HeadingCode = ExpertiseCode,
                    HeadingLevel = ExpertiseLevel,
                    OriginId = new Guid(OriginId),
                    PageName = pageName,
                    PlaceInWebSite = placeInWebSite,
                    RegionCode = ServiceAreaCode,
                    RegionLevel = ServiceAreaLevel,
                    SessionId = sessionId,
                    SiteId = SiteId,
                    Type = NoProblem.Core.DataModel.Xrm.new_exposure.ExposureType.Listing,
                    Url = url,
                    NumAdvertisersReturned = supplierCount
                };
                convertionManager.CreateExposure(exposureRequest);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateExposureFromSearchAsync");
            }
        }

        private void CreateExposureFromSearchAsyncStr(string suppliersXmlStr, string SiteId, string ExpertiseCode, int ExpertiseLevel, string ServiceAreaCode, int ServiceAreaLevel, string OriginId,
            string controlName, string domain, string pageName, string placeInWebSite, string sessionId, string url)
        {
            try
            {
                XElement suppliersXml = XElement.Parse(suppliersXmlStr);
                CreateExposureFromSearchAsync(suppliersXml, SiteId, ExpertiseCode, ExpertiseLevel, ServiceAreaCode, ServiceAreaLevel, OriginId,
                    controlName, domain, pageName, placeInWebSite, sessionId, url);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateExposureFromSearchAsync");
            }
        }

        public List<DML.SupplierModel.ExpertiseData> GetSupplierCount(string expertiseName, string areaCode, int areaLevel)
        {
            List<DML.SupplierModel.ExpertiseData> dataList = new List<NoProblem.Core.DataModel.SupplierModel.ExpertiseData>();
            DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            IEnumerable<DML.Xrm.new_primaryexpertise> expertises = expertiseDal.GetPrimaryExpertiseByPartOfName(expertiseName);
            foreach (var exp in expertises)
            {
                DML.SupplierModel.ExpertiseData data = new NoProblem.Core.DataModel.SupplierModel.ExpertiseData();
                data.Code = exp.new_code;
                data.Level = 1;
                data.ExpertiseName = exp.new_name;
                XElement doc = SearchSiteSuppliers("", exp.new_code, 1, areaCode, areaLevel, "", -1, Guid.Empty, 0, 0, false, false, false, null);
                IEnumerable<XElement> elements = doc.Elements("Supplier");
                int count = elements.Count();
                if (count == 0)
                {
                    doc = SearchSiteSuppliers("", exp.new_code, 1, areaCode, areaLevel, "", -1, Guid.Empty, 0, 0, true, false, false, null);
                    elements = doc.Elements("Supplier");
                    if (elements.Count() == 0)
                    {
                        count = -1;
                    }
                }
                data.SupplierCount = count;
                dataList.Add(data);
            }
            return dataList;
        }

        #endregion

        public string GetPricingMethods()
        {
            DataAccessLayer.PricingMethod dal = new NoProblem.Core.DataAccessLayer.PricingMethod(XrmDataContext);
            return dal.GetPricingMethods();
        }

        public string GetSupplierDetails(string supplierId)
        {
            StringWriter xmlText = new StringWriter();
            XmlTextWriter xmlConstructor = new XmlTextWriter(xmlText);

            string accountQuery = @"SELECT description as LongDescription,name,accountnumber,numberofemployees,
new_complaints,new_weightedscore,new_surveygradecounter,new_certificate2,new_description as ShortDescription,
ISNULL(new_sumassistancerequests,0) AS new_sumassistancerequests,new_videourl as VideoUrl,emailaddress1,new_firstname,new_lastname,telephone1,
fax,address1_country,address1_stateorprovince,address1_city,address1_county,address1_line1,address1_line2,address1_postalcode,
new_accountgovernmentid,websiteurl,telephone2,address1_utcoffset,ISNULL(new_stageinregistration,0) AS new_stageinregistration, new_publisherranking, new_recordcalls,
new_address1_stateorprovince_regionid as stateid, new_address1_city_regionid as cityid, new_address1_county_regionid as countyid, new_address1_line1_regionid as streetid
,new_custom1,new_custom2,new_custom3,new_custom4,new_custom5,new_bizid
FROM account with (nolock) WHERE accountid = '{0}' AND account.deletionstatecode = 0";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(string.Format(accountQuery, supplierId), connection))
                {
                    using (SqlDataReader accountDetails = command.ExecuteReader())
                    {
                        xmlConstructor.WriteStartElement("Supplier");
                        if (accountDetails.Read())
                        {
                            xmlConstructor.WriteStartElement("Details");
                            xmlConstructor.WriteAttributeString("LongDescription", accountDetails["LongDescription"].ToString());
                            xmlConstructor.WriteAttributeString("Name", accountDetails["name"].ToString());
                            xmlConstructor.WriteAttributeString("AccountNumber", accountDetails["accountnumber"].ToString());
                            xmlConstructor.WriteAttributeString("NumberOfEmployees", accountDetails["numberofemployees"].ToString());
                            xmlConstructor.WriteAttributeString("Complaints", accountDetails["new_complaints"].ToString());
                            xmlConstructor.WriteAttributeString("WeightedScore", accountDetails["new_weightedscore"].ToString());
                            xmlConstructor.WriteAttributeString("SurveyGradeCounter", accountDetails["new_surveygradecounter"].ToString());
                            xmlConstructor.WriteAttributeString("Certificate2", accountDetails["new_certificate2"].ToString());
                            xmlConstructor.WriteAttributeString("ShortDescription", accountDetails["ShortDescription"].ToString());
                            xmlConstructor.WriteAttributeString("SumAssistanceRequests", accountDetails["new_sumassistancerequests"].ToString());
                            xmlConstructor.WriteAttributeString("VideoUrl", accountDetails["VideoUrl"].ToString());
                            xmlConstructor.WriteAttributeString("Email", accountDetails["emailaddress1"].ToString());
                            xmlConstructor.WriteAttributeString("FirstName", accountDetails["new_firstname"].ToString());
                            xmlConstructor.WriteAttributeString("LastName", accountDetails["new_lastname"].ToString());
                            xmlConstructor.WriteAttributeString("ContactPhone", accountDetails["telephone1"].ToString());
                            xmlConstructor.WriteAttributeString("Company", accountDetails["name"].ToString());
                            xmlConstructor.WriteAttributeString("CompanyPhone", accountDetails["telephone2"].ToString());
                            xmlConstructor.WriteAttributeString("Fax", accountDetails["Fax"].ToString());
                            xmlConstructor.WriteAttributeString("CountryName", accountDetails["address1_country"].ToString());
                            xmlConstructor.WriteAttributeString("StateName", accountDetails["address1_stateorprovince"].ToString());
                            xmlConstructor.WriteAttributeString("StateId", accountDetails["stateid"].ToString());
                            xmlConstructor.WriteAttributeString("CityName", accountDetails["address1_city"].ToString());
                            xmlConstructor.WriteAttributeString("CityId", accountDetails["cityid"].ToString());
                            xmlConstructor.WriteAttributeString("DistrictName", accountDetails["address1_county"].ToString());
                            xmlConstructor.WriteAttributeString("DistrictId", accountDetails["countyid"].ToString());
                            xmlConstructor.WriteAttributeString("StreetName", accountDetails["address1_line1"].ToString());
                            xmlConstructor.WriteAttributeString("StreetId", accountDetails["streetid"].ToString());
                            xmlConstructor.WriteAttributeString("HouseNum", accountDetails["address1_line2"].ToString());
                            xmlConstructor.WriteAttributeString("Zipcode", accountDetails["address1_postalcode"].ToString());
                            xmlConstructor.WriteAttributeString("TimeZone", accountDetails["address1_utcoffset"].ToString());
                            xmlConstructor.WriteAttributeString("CompanyId", accountDetails["new_accountgovernmentid"].ToString());
                            xmlConstructor.WriteAttributeString("CompanySite", accountDetails["websiteurl"].ToString());
                            xmlConstructor.WriteAttributeString("RegistrationStage", accountDetails["new_stageinregistration"].ToString());
                            xmlConstructor.WriteAttributeString("PublisherRanking", accountDetails["new_publisherranking"].ToString());
                            xmlConstructor.WriteAttributeString("AllowRecordings", Convert.IsDBNull(accountDetails["new_recordcalls"]) ? false.ToString() : accountDetails["new_recordcalls"].ToString());
                            xmlConstructor.WriteAttributeString("BizId", accountDetails["new_bizid"].ToString());
                            for (int i = 1; i <= 5; i++)
                            {
                                xmlConstructor.WriteAttributeString("Custom" + i, accountDetails["new_custom" + i].ToString());
                            }
                            xmlConstructor.WriteEndElement();
                        }
                    }
                }

                string expertiseQuery = @"SELECT ae.new_primaryexpertiseid as PrimaryExpertiseId,
						   ae.new_primaryexpertiseidname as PrimaryExpertiseName,
						   se.new_secondaryexpertiseid as SecondaryExpertiseId,
						   se.new_name as SecondaryExpertiseName,ae.new_certificate
						FROM new_accountexpertise ae with (nolock)
						LEFT JOIN new_secondaryexpertise_new_accountexpertise seRelation with (nolock) ON seRelation.new_accountexpertiseid = ae.new_accountexpertiseid
						LEFT JOIN new_secondaryexpertise se with (nolock) ON se.new_secondaryexpertiseid = seRelation.new_secondaryexpertiseid
						WHERE ae.new_accountid = '{0}' AND ae.deletionstatecode = 0 AND ae.statecode = 0
						ORDER BY PrimaryExpertiseName,SecondaryExpertiseName";

                string previousPrimary = "";

                using (SqlCommand command = new SqlCommand(string.Format(expertiseQuery, supplierId), connection))
                {
                    using (SqlDataReader expertiseReader = command.ExecuteReader())
                    {
                        xmlConstructor.WriteStartElement("Expertise");
                        while (expertiseReader.Read())
                        {
                            if (previousPrimary != expertiseReader["PrimaryExpertiseName"].ToString())
                            {
                                if (previousPrimary != "")
                                    xmlConstructor.WriteEndElement();

                                xmlConstructor.WriteStartElement("Primary");
                                xmlConstructor.WriteAttributeString("ID", expertiseReader["PrimaryExpertiseId"].ToString());
                                xmlConstructor.WriteAttributeString("Name", expertiseReader["PrimaryExpertiseName"].ToString());
                                xmlConstructor.WriteAttributeString("Certificate", expertiseReader["new_certificate"].ToString());

                                previousPrimary = expertiseReader["PrimaryExpertiseName"].ToString();
                            }
                            if (expertiseReader["PrimaryExpertiseId"] != DBNull.Value)
                            {
                                if (expertiseReader["SecondaryExpertiseId"] != DBNull.Value)
                                {
                                    xmlConstructor.WriteStartElement("Secondary");
                                    xmlConstructor.WriteAttributeString("ID", expertiseReader["SecondaryExpertiseId"].ToString());
                                    xmlConstructor.WriteValue(expertiseReader["SecondaryExpertiseName"].ToString());
                                    xmlConstructor.WriteEndElement();
                                }
                            }
                        }
                    }
                }
                // Close The last primary Tag
                if (previousPrimary != "")
                    xmlConstructor.WriteEndElement();
            }
            xmlConstructor.WriteEndElement();
            xmlConstructor.WriteEndElement();
            return xmlText.ToString();
        }

        [Obsolete("Use SupplierManager.UpsertAdvertiser instead.")]
        public string UpsertSupplier(string Request)
        {
            /*
             * <InternetSupplier SiteId="" SupplierId="Guid">
             *	<Email></Email>
             *  <FirstName></FirstName>
             *  <LastName></LastName>
             *  <ContactPhone></ContactPhone>
             *  <Company></Company>
             *  <CompanyPhone></CompanyPhone>
             *  <NumberOfEmployees></NumberOfEmployees>
             *  <Fax></Fax>
             *  <Country></Country>
             *  <StateName></StateName>
             *  <CityName></CityName>
             *  <DistrictName></DistrictName>
             *  <StreetName></StreetName>
             *  <HouseNum></HouseNum>
             *  <Zipcode></Zipcode>
             *  <TimeZone></TimeZone>
             *  <CompanyId></CompanyId>
             *  <CompanySite></CompanySite>
             *  <VideoUrl></VideoUrl>
             *  <LongDescription></LongDescription>
             *  <ShortDescription></ShortDescription>
             * </InternetSupplier> 
             */

            XmlDocument supplierRequest = new XmlDocument();
            supplierRequest.LoadXml(Request);
            DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            Guid accountId = Guid.Empty;
            XmlAttribute supplierId = supplierRequest.DocumentElement.Attributes["SupplierId"];
            if (supplierId != null && supplierId.InnerText != "")
                accountId = new Guid(supplierId.InnerText);
            DynamicEntity supplier = ConvertorCRMParams.GetNewDynamicEntity("account"); ;
            DML.Xrm.account accountXrm = null;
            DataAccessLayer.AccountRepository accountRepository = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            if (accountId != Guid.Empty)
            {
                accountXrm = accountRepository.Retrieve(accountId);
            }
            // if this is a new account and the email exist then return an error
            if (supplierRequest.DocumentElement["Email"] != null)
            {
                string newEmail = supplierRequest.DocumentElement["Email"].InnerText;
                if (accountId != Guid.Empty)
                {
                    if (accountXrm.emailaddress1 != newEmail && accountRepository.IsSupplierExists(newEmail))
                    {
                        return "<InternetSupplier><Error>Email allready exists</Error></InternetSupplier>";
                    }
                }
                else
                {
                    if (accountRepository.IsSupplierExists(newEmail))
                        return "<InternetSupplier><Error>Email allready exists</Error></InternetSupplier>";
                }
            }

            //Check if phone already exists (removed 17/5/11 by Alain, Shaip's request).
            //if (supplierRequest.DocumentElement["ContactPhone"] != null)
            //{
            //    string newPhone = supplierRequest.DocumentElement["ContactPhone"].InnerText;
            //    if (accountId != Guid.Empty)
            //    {
            //        if (accountXrm.telephone1 != newPhone && SupplierUtils.Current.IsPhoneExists(newPhone))
            //        {
            //            return "<InternetSupplier><Error>Phone allready exists</Error></InternetSupplier>";
            //        }
            //    }
            //    else
            //    {
            //        if (SupplierUtils.Current.IsPhoneExists(newPhone))
            //            return "<InternetSupplier><Error>Phone allready exists</Error></InternetSupplier>";
            //    }
            //}

            #region Fill all the properties

            if (accountId != Guid.Empty)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "accountid",
                   ConvertorCRMParams.GetCrmKey(new Guid(supplierId.InnerText)), typeof(KeyProperty));

            if (supplierRequest.DocumentElement["Email"] != null)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "emailaddress1",
                   supplierRequest.DocumentElement["Email"].InnerText, typeof(StringProperty));
            }
            if (supplierRequest.DocumentElement["VideoUrl"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_videourl",
                   supplierRequest.DocumentElement["VideoUrl"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["LongDescription"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "description",
                   supplierRequest.DocumentElement["LongDescription"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["ShortDescription"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_description",
                   supplierRequest.DocumentElement["ShortDescription"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["FirstName"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_firstname",
                   supplierRequest.DocumentElement["FirstName"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["LastName"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_lastname",
                   supplierRequest.DocumentElement["LastName"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["FirstName"] != null || supplierRequest.DocumentElement["LastName"] != null)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_fullname",
                     string.Format("{0} {1}",
                     (supplierRequest.DocumentElement["FirstName"] != null ? supplierRequest.DocumentElement["FirstName"].InnerText : ""),
                     (supplierRequest.DocumentElement["LastName"] != null ? supplierRequest.DocumentElement["LastName"].InnerText : "")), typeof(StringProperty));

                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_fullnamereverse",
                     string.Format("{0} {1}",
                     (supplierRequest.DocumentElement["LastName"] != null ? supplierRequest.DocumentElement["LastName"].InnerText : ""),
                     (supplierRequest.DocumentElement["FirstName"] != null ? supplierRequest.DocumentElement["FirstName"].InnerText : "")), typeof(StringProperty));
            }

            if (supplierRequest.DocumentElement["ContactPhone"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "telephone1",
                   supplierRequest.DocumentElement["ContactPhone"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["NumberOfEmployees"] != null)
            {
                if (supplierRequest.DocumentElement["NumberOfEmployees"].InnerText != "")
                    ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "numberofemployees",
                       ConvertorCRMParams.GetCrmNumber(int.Parse(supplierRequest.DocumentElement["NumberOfEmployees"].InnerText), false), typeof(CrmNumberProperty));
                else
                    ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "numberofemployees",
                       ConvertorCRMParams.GetCrmNumber(0, true), typeof(CrmNumberProperty));
            }

            if (supplierRequest.DocumentElement["Company"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "name",
                   System.Web.HttpUtility.HtmlDecode(supplierRequest.DocumentElement["Company"].InnerText), typeof(StringProperty));

            if (supplierRequest.DocumentElement["CompanyPhone"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "telephone2",
                   supplierRequest.DocumentElement["CompanyPhone"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["Fax"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "fax",
                   supplierRequest.DocumentElement["Fax"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["CountryName"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "address1_country",
                   supplierRequest.DocumentElement["CountryName"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["StateName"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "address1_stateorprovince",
                   supplierRequest.DocumentElement["StateName"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["StateId"] != null)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_address1_stateorprovince_regionid",
                   ConvertorCRMParams.GetCrmLookup("new_region", new Guid(supplierRequest.DocumentElement["StateId"].InnerText)), typeof(LookupProperty));
            }

            if (supplierRequest.DocumentElement["CityName"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "address1_city",
                   supplierRequest.DocumentElement["CityName"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["CityId"] != null)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_address1_city_regionid",
                   ConvertorCRMParams.GetCrmLookup("new_region", new Guid(supplierRequest.DocumentElement["CityId"].InnerText)), typeof(LookupProperty));
            }

            if (supplierRequest.DocumentElement["DistrictName"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "address1_county",
                   supplierRequest.DocumentElement["DistrictName"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["DistrictId"] != null)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_address1_county_regionid",
                   ConvertorCRMParams.GetCrmLookup("new_region", new Guid(supplierRequest.DocumentElement["DistrictId"].InnerText)), typeof(LookupProperty));
            }

            if (supplierRequest.DocumentElement["StreetName"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "address1_line1",
                   supplierRequest.DocumentElement["StreetName"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["StreetId"] != null)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_address1_line1_regionid",
                  ConvertorCRMParams.GetCrmLookup("new_region", new Guid(supplierRequest.DocumentElement["StreetId"].InnerText)), typeof(LookupProperty));
            }

            if (supplierRequest.DocumentElement["HouseNum"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "address1_line2",
                   supplierRequest.DocumentElement["HouseNum"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["Zipcode"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "address1_postalcode",
                   supplierRequest.DocumentElement["Zipcode"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["TimeZone"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "address1_utcoffset",
                    ConvertorCRMParams.GetCrmNumber(int.Parse(supplierRequest.DocumentElement["TimeZone"].InnerText), false), typeof(CrmNumberProperty));
            else if (accountId == Guid.Empty)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "address1_utcoffset",
                 ConvertorCRMParams.GetCrmNumber(int.Parse(configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE)), false), typeof(CrmNumberProperty));


            if (supplierRequest.DocumentElement["CompanyId"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_accountgovernmentid",
                   supplierRequest.DocumentElement["CompanyId"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["CompanySite"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "websiteurl",
                   supplierRequest.DocumentElement["CompanySite"].InnerText, typeof(StringProperty));

            if (supplierRequest.DocumentElement["PublisherRanking"] != null)
            {
                int ranking = int.Parse(supplierRequest.DocumentElement["PublisherRanking"].InnerText);
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_publisherranking",
                    ConvertorCRMParams.GetCrmNumber(ranking, false), typeof(CrmNumberProperty));
            }
            int securityLevel = 0;
            if (supplierRequest.DocumentElement["SecurityLevel"] != null)
            {
                securityLevel = int.Parse(supplierRequest.DocumentElement["SecurityLevel"].InnerText);
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_securitylevel",
                    ConvertorCRMParams.GetCrmPicklist(securityLevel), typeof(PicklistProperty));
            }

            for (int i = 1; i <= 5; i++)
            {
                if (supplierRequest.DocumentElement["Custom" + i] != null)
                {
                    string custom = supplierRequest.DocumentElement["Custom" + i].InnerText;
                    ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_custom" + i,
                        custom, typeof(StringProperty));
                }
            }

            if (supplierRequest.DocumentElement["AllowRecordings"] != null)
            {
                bool allowRecordings = false;
                string allowStr = supplierRequest.DocumentElement["AllowRecordings"].InnerText;
                if (allowStr == "1")
                {
                    allowRecordings = true;
                }
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_recordcalls",
                        ConvertorCRMParams.GetCrmBoolean(allowRecordings), typeof(CrmBooleanProperty));
            }


            #endregion

            if (accountId != Guid.Empty)
                MethodsUtils.UpdateEntity(CrmService, supplier);
            else
            {
                string publisherTimeZoneCode = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE);
                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "address1_utcoffset",
                    ConvertorCRMParams.GetCrmNumber(int.Parse(publisherTimeZoneCode), false), typeof(CrmNumberProperty));

                if (securityLevel > 0)
                {
                    string password = LoginUtils.GenerateNewPassword();
                    ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_password", password, typeof(StringProperty));
                    ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_status",
                        ConvertorCRMParams.GetCrmPicklist((int)DML.Xrm.account.SupplierStatus.Approved), typeof(PicklistProperty));
                }
                //by Yoav
                else
                    ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_incidentpricechangeable",
                         ConvertorCRMParams.GetCrmBoolean(true), typeof(CrmBooleanProperty));
                accountId = MethodsUtils.CreateEntity(CrmService, supplier);
                if (securityLevel > 0)
                {
                    Notifications notificationLogic = new Notifications(XrmDataContext);
                    notificationLogic.NotifySupplier(DML.enumSupplierNotificationTypes.SUPPLIER_SIGNUP, accountId, accountId.ToString(), "account", accountId.ToString(), "account");
                }
            }

            // Adding the stage in case its the first time
            accountRepository.SetSupplierRegistrationStage(accountId, DML.Xrm.account.StageInRegistration.GENERAL_INFO);

            return string.Format("<InternetSupplier><Status>Success</Status><SupplierId>{0}</SupplierId></InternetSupplier>", accountId);
        }

        public string GetSupplierAvailability(string supplierId)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            string query = @"SELECT new_sundayavailablefrom,new_sundayavailableto,new_mondayavailablefrom,new_mondayavailableto,
		 new_tuesdayavailablefrom,new_tuesdayavailableto,new_wednesdayavailablefrom,new_wednesdayavailableto,new_thursdayavailablefrom,
		 new_thursdayavailableto,new_fridayavailablefrom,new_fridayavailableto,new_saturdayavailablefrom,new_saturdayavailableto,
		 new_unavailablelocalfrom,new_unavailablelocalto,new_unavailableto,new_unavailabilityreason.new_unavailabilityreasonid,new_unavailabilityreasonidname,  ISNULL(new_stageinregistration,0) AS new_stageinregistration
		 FROM account with (nolock)
		 LEFT JOIN new_unavailabilityreason with (nolock) ON new_unavailabilityreason.deletionstatecode = 0 AND new_unavailabilityreason.new_unavailabilityreasonid = account.new_unavailabilityreasonid
		 WHERE accountid = '{0}' AND account.deletionstatecode = 0";
            StringWriter xmlText = new StringWriter();
            XmlTextWriter xmlConstructor = new XmlTextWriter(xmlText);
            xmlConstructor.WriteStartElement("SupplierAvailability");
            xmlConstructor.WriteAttributeString("SupplierId", supplierId);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(string.Format(query, supplierId), connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            xmlConstructor.WriteAttributeString("RegistrationStage", reader["new_stageinregistration"].ToString());

                            xmlConstructor.WriteStartElement("Availability");

                            xmlConstructor.WriteStartElement("Day");
                            xmlConstructor.WriteAttributeString("Name", "Sunday");
                            xmlConstructor.WriteElementString("FromTime", reader["new_sundayavailablefrom"].ToString());
                            xmlConstructor.WriteElementString("TillTime", reader["new_sundayavailableto"].ToString());
                            xmlConstructor.WriteEndElement();

                            xmlConstructor.WriteStartElement("Day");
                            xmlConstructor.WriteAttributeString("Name", "Monday");
                            xmlConstructor.WriteElementString("FromTime", reader["new_mondayavailablefrom"].ToString());
                            xmlConstructor.WriteElementString("TillTime", reader["new_mondayavailableto"].ToString());
                            xmlConstructor.WriteEndElement();

                            xmlConstructor.WriteStartElement("Day");
                            xmlConstructor.WriteAttributeString("Name", "Tuesday");
                            xmlConstructor.WriteElementString("FromTime", reader["new_tuesdayavailablefrom"].ToString());
                            xmlConstructor.WriteElementString("TillTime", reader["new_tuesdayavailableto"].ToString());
                            xmlConstructor.WriteEndElement();

                            xmlConstructor.WriteStartElement("Day");
                            xmlConstructor.WriteAttributeString("Name", "Wednesday");
                            xmlConstructor.WriteElementString("FromTime", reader["new_wednesdayavailablefrom"].ToString());
                            xmlConstructor.WriteElementString("TillTime", reader["new_wednesdayavailableto"].ToString());
                            xmlConstructor.WriteEndElement();

                            xmlConstructor.WriteStartElement("Day");
                            xmlConstructor.WriteAttributeString("Name", "Thursday");
                            xmlConstructor.WriteElementString("FromTime", reader["new_thursdayavailablefrom"].ToString());
                            xmlConstructor.WriteElementString("TillTime", reader["new_thursdayavailableto"].ToString());
                            xmlConstructor.WriteEndElement();

                            xmlConstructor.WriteStartElement("Day");
                            xmlConstructor.WriteAttributeString("Name", "Friday");
                            xmlConstructor.WriteElementString("FromTime", reader["new_fridayavailablefrom"].ToString());
                            xmlConstructor.WriteElementString("TillTime", reader["new_fridayavailableto"].ToString());
                            xmlConstructor.WriteEndElement();

                            xmlConstructor.WriteStartElement("Day");
                            xmlConstructor.WriteAttributeString("Name", "Saturday");
                            xmlConstructor.WriteElementString("FromTime", reader["new_saturdayavailablefrom"].ToString());
                            xmlConstructor.WriteElementString("TillTime", reader["new_saturdayavailableto"].ToString());
                            xmlConstructor.WriteEndElement();

                            // End of Availability
                            xmlConstructor.WriteEndElement();
                            string unavailableToStr = reader["new_unavailableto"].ToString();
                            bool removeUnavailability = false;
                            //Check if the supplier had unavailability and it is already over, if true, nullify unavailability field.
                            if (!string.IsNullOrEmpty(unavailableToStr))
                            {
                                DateTime unavailableTo = DateTime.Parse(unavailableToStr);
                                if (unavailableTo.CompareTo(DateTime.Now) < 0)//unavailableTo is earlier than now.
                                {
                                    removeUnavailability = true;
                                    NullifyUnavailabilityFields(new Guid(supplierId));
                                }
                            }
                            xmlConstructor.WriteStartElement("Unavailability");
                            xmlConstructor.WriteElementString("StartDate", removeUnavailability ? string.Empty : reader["new_unavailablelocalfrom"].ToString());
                            xmlConstructor.WriteElementString("EndDate", removeUnavailability ? string.Empty : reader["new_unavailablelocalto"].ToString());
                            xmlConstructor.WriteStartElement("Reason");
                            xmlConstructor.WriteAttributeString("ID", removeUnavailability ? string.Empty : reader["new_unavailabilityreasonid"].ToString());
                            xmlConstructor.WriteValue(removeUnavailability ? string.Empty : reader["new_unavailabilityreasonidname"].ToString());
                            xmlConstructor.WriteEndElement();
                            // End of Unavailability
                            xmlConstructor.WriteEndElement();
                        }
                    }
                }
            }
            xmlConstructor.WriteEndElement();
            return xmlText.ToString();
        }

        private void NullifyUnavailabilityFields(Guid accountId)
        {
            DataAccessLayer.AccountRepository accountRepositoryAccess = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryAccess.Retrieve(accountId);
            supplier.new_unavailabilityreasonid = null;
            supplier.new_unavailablefrom = null;
            supplier.new_unavailablelocalfrom = null;
            supplier.new_unavailablelocalto = null;
            supplier.new_unavailableto = null;
            accountRepositoryAccess.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        public string SetSupplierAvailability(string Request, bool isFromAAR)
        {
            /*
             * <SupplierAvailability SiteId="" SupplierId="" >
             *    <Availability>
             *	      <Day Name="sunday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *	      </Day>
             *	      <Day Name="monday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *	      </Day>
             *       <Day Name="tuesday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *       </Day>
             *       <Day Name="wednesday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *       </Day>
             *       <Day Name="thursday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *       </Day>
             *       <Day Name="friday">
             *		      <FromTime>14:00</FromTime>
             *		      <TillTime>17:00</TillTime>
             *       </Day>
             *       <Day Name="saturday">
             *		      <FromTime>14:00</FromTime>
             *    		<TillTime>17:00</TillTime>
             *       </Day>
             *    </Availability>
             *    <Unavailability>
             *       <StartDate>05/26/2010</StartDate>
             *       <EndDate></EndDate>
             *       <ReasonId></ReasonId>
             *    </Unavailability>
             * </SupplierAvailability>
             */

            string retVal = "";
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(Request);

            XmlAttribute supplierId = xDoc.DocumentElement.Attributes["SupplierId"];
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
            if (supplierId != null && supplierId.InnerText != "")
            {
                Guid accountId = new Guid(supplierId.InnerText);
                XmlNodeList availabilities = xDoc.SelectNodes("//Availability/Day");

                DynamicEntity supplier = ConvertorCRMParams.GetNewDynamicEntity("account");
                //object oTimeZone = DataBaseUtils.GetValueFromSqlString("select address1_utcoffset from account with (nolock) where accountid = '{0}'", accountId);
                DataAccessLayer.AccountRepository accDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
                parameters.Add(new KeyValuePair<string, object>("@accountId", accountId));
                object oTimeZone = accDal.ExecuteScalar("select address1_utcoffset from account with (nolock) where accountid = @accountId", parameters);
                int timeZone = oTimeZone == DBNull.Value ? 0 : int.Parse(oTimeZone.ToString());

                //Effect.Crm.DB.DataBaseUtils.DirectUpdateInstances("new_availability", new string[] { "deletionstatecode" }, new object[] { 2 }, "new_accountid = '{0}'", accountId);
                string sqlUpdate = "update new_availability set deletionstatecode = 2 where new_accountid = @id";
                DataAccessLayer.Generic.SqlParametersList parametersUpdate = new DataAccessLayer.Generic.SqlParametersList();
                parametersUpdate.Add("@id", accountId);
                accDal.ExecuteNonQuery(sqlUpdate, parametersUpdate);

                for (int i = 0; i < availabilities.Count; i++)
                {
                    string dayName = availabilities[i].Attributes["Name"].InnerText;
                    string fromFieldname = "new_" + dayName + "availablefrom";
                    string toFieldname = "new_" + dayName + "availableto";

                    ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, fromFieldname,
                       availabilities[i].SelectSingleNode("FromTime").InnerText, typeof(StringProperty));

                    ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, toFieldname,
                       availabilities[i].SelectSingleNode("TillTime").InnerText, typeof(StringProperty));

                    DateTime fromTime = DateTime.Parse(availabilities[i].SelectSingleNode("FromTime").InnerText);
                    DateTime fromTimeUtc = FixDateToUtcFromLocal(fromTime, timeZone); //ConvertUtils.GetUtcFromLocal(CrmService, fromTime.ToString("MM/dd/yyyy HH:mm:ss"), timeZone);
                    int day = ConvertUtils.GetDayNumberFromName(dayName);

                    // if by changing to utc format we moved a day in the calendar
                    if (fromTime.Date > fromTimeUtc.Date)
                        day = ConvertUtils.GetPreviousDay(day);
                    else if (fromTime.Date < fromTimeUtc.Date)
                        day = ConvertUtils.GetNextDay(day);

                    //GeneralUtils.CreateDaysAvailability(CrmService, accountId,
                    //    fromTimeUtc.ToString("HH:mm"),
                    //    ConvertUtils.GetUtcFromLocal(CrmService, availabilities[i].SelectSingleNode("TillTime").InnerText, timeZone).ToString("HH:mm"),                        
                    //    day);

                    //GeneralUtils.CreateDaysAvailability(CrmService, accountId,
                    //    fromTimeUtc.ToString("HH:mm"),
                    //    FixDateToUtcFromLocal(DateTime.ParseExact(availabilities[i].SelectSingleNode("TillTime").InnerText, "HH:mm", culture), timeZone).ToString("HH:mm"),
                    //    day);

                    GeneralUtils.CreateDaysAvailability(CrmService, accountId,
                        fromTimeUtc.ToString("HH:mm"),
                        FixDateToUtcFromLocal(DateTime.Parse(availabilities[i].SelectSingleNode("TillTime").InnerText), timeZone).ToString("HH:mm"),
                        day);
                }

                ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "accountid",
                      ConvertorCRMParams.GetCrmKey(accountId), typeof(KeyProperty));


                XmlNode Unavailability = xDoc.DocumentElement["Unavailability"];
                //Set the unavailability if exists
                if (Unavailability != null)
                {
                    // Handle the start date
                    if (Unavailability["StartDate"] != null)
                    {
                        CrmDateTime startDate = null;
                        CrmDateTime startDateLocal = null;
                        if (Unavailability["StartDate"].InnerText == "")
                        {
                            startDate = new CrmDateTime();
                            startDate.IsNullSpecified = startDate.IsNull = true;

                            startDateLocal = new CrmDateTime();
                            startDateLocal.IsNullSpecified = startDateLocal.IsNull = true;
                        }
                        else
                        {
                            DateTime localStartDateTime = DateTime.Parse(Unavailability["StartDate"].InnerText + " 00:00:00", culture);
                            //startDate = ConvertorCRMParams.GetCrmDateTime(ConvertUtils.GetUtcFromLocal(CrmService, Unavailability["StartDate"].InnerText + " 00:00:00", timeZone));
                            startDate = ConvertorCRMParams.GetCrmDateTime(FixDateToUtcFromLocal(localStartDateTime, timeZone));
                            startDateLocal = ConvertorCRMParams.GetCrmDateTime(localStartDateTime);
                        }
                        ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_unavailablefrom",
                              startDate, typeof(CrmDateTimeProperty));

                        ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_unavailablelocalfrom",
                          startDateLocal, typeof(CrmDateTimeProperty));
                    }

                    // Handle the end date
                    if (Unavailability["EndDate"] != null)
                    {
                        CrmDateTime endDate = null;
                        CrmDateTime endDateLocal = null;
                        if (Unavailability["EndDate"].InnerText == "")
                        {
                            endDate = new CrmDateTime();
                            endDate.IsNullSpecified = endDate.IsNull = true;

                            endDateLocal = new CrmDateTime();
                            endDateLocal.IsNullSpecified = endDateLocal.IsNull = true;
                        }
                        else
                        {
                            DateTime localEndDateTime = DateTime.Parse(Unavailability["EndDate"].InnerText + " 23:59:00", culture);
                            //endDate = ConvertorCRMParams.GetCrmDateTime(ConvertUtils.GetUtcFromLocal(CrmService, Unavailability["EndDate"].InnerText + " 23:59:00", timeZone));
                            endDate = ConvertorCRMParams.GetCrmDateTime(FixDateToUtcFromLocal(localEndDateTime, timeZone));
                            endDateLocal = ConvertorCRMParams.GetCrmDateTime(localEndDateTime);
                        }

                        ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_unavailableto",
                              endDate, typeof(CrmDateTimeProperty));

                        ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_unavailablelocalto",
                          endDateLocal, typeof(CrmDateTimeProperty));
                    }

                    // handle the reason id
                    if (Unavailability["ReasonId"] != null)
                    {
                        Lookup unavailabilityReason = null;
                        if (Unavailability["ReasonId"].InnerText == "")
                        {
                            unavailabilityReason = new Lookup();
                            unavailabilityReason.IsNull = unavailabilityReason.IsNullSpecified = true;
                        }
                        else
                        {
                            unavailabilityReason = ConvertorCRMParams.GetCrmLookup("new_unavailabilityreason",
                               new Guid(Unavailability["ReasonId"].InnerText));
                        }

                        ConvertorCRMParams.SetDynamicEntityPropertyValue(supplier, "new_unavailabilityreasonid",
                           unavailabilityReason, typeof(LookupProperty));
                    }
                }

                CrmService.Update(supplier);

                // Adding the stage 
                //if (!isFromAAR)
                //{
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                accountRepositoryDal.SetSupplierRegistrationStage(new Guid(supplierId.InnerText), DML.Xrm.account.StageInRegistration.WORKS_HOUR);
                //}
                retVal = "<SupplierAvailability><Status>Success</Status></SupplierAvailability>";
                if (Dapaz.PpaOnlineSync.SyncConfiguration.IsEnabled)
                {
                    Dapaz.PpaOnlineSync.Syncher syncher = new Dapaz.PpaOnlineSync.Syncher(accountId);
                    syncher.Start();
                }
            }

            return retVal;
        }

        public string GetSupplierExpertise(string SiteId, string SupplierId)
        {            
            Guid supplierIdGuid = new Guid(SupplierId);
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();            
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@supplierId", supplierIdGuid);
            var reader = dal.ExecuteReader(getSupplierExpertise_query, parameters);
            XElement root = new XElement("SupplierExpertise");
            string plan = null;
            string stage = null;
            foreach (var row in reader)
            {
                XElement innerElement = new XElement("PrimaryExpertise");
                innerElement.SetAttributeValue("Name", Convert.ToString(row["PrimaryExpertiseName"]));
                innerElement.SetAttributeValue("ID", row["PrimaryExpertiseId"].ToString());
                innerElement.SetAttributeValue("Certificate", row["new_certificate"] != DBNull.Value ? row["new_certificate"].ToString() : false.ToString());
                root.Add(innerElement);
                if (plan == null)
                    plan = Convert.ToString(row["new_subscriptionplan"]);
                if (stage == null)
                    stage = row["new_stageinregistration"] != DBNull.Value ? row["new_stageinregistration"].ToString() : 0.ToString();
            }
            root.SetAttributeValue("RegistrationStage", stage);
            root.SetAttributeValue("SubscriptionPlan", plan);
            return root.ToString();
        }

        private const string getSupplierExpertise_query =
            @"
SELECT ae.new_primaryexpertiseid as PrimaryExpertiseId,
	ae.new_primaryexpertiseidname as PrimaryExpertiseName                           
    ,ae.new_certificate
    ,acc.new_subscriptionplan
    ,acc.new_stageinregistration
FROM new_accountexpertise ae with (nolock)
JOIN account acc with(nolock) on ae.new_accountid = acc.accountid						
WHERE ae.new_accountid = @supplierId AND ae.deletionstatecode = 0 AND ae.statecode = 0
ORDER BY PrimaryExpertiseName
";

        public string CreateSupplierExpertise(string Request, bool isFromAAR, Guid userId)
        {
            /* Request Formation
             * <SupplierExpertise SiteId="" SupplierId="">
             *	<PrimaryExpertise ID="" Certificate="">
             *	   <SecondaryExpertise>ID</SecondaryExpertise>
             *	   <SecondaryExpertise>ID</SecondaryExpertise>
             *  </PrimaryExpertise>
             *	<PrimaryExpertise ID="" Certificate="">
             *	   <SecondaryExpertise>ID</SecondaryExpertise>
             *  </PrimaryExpertise>
             * </SupplierExpertise>
             */

            XmlDocument parser = new XmlDocument();
            parser.LoadXml(Request);
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            string supplierIdStr = Utils.XmlUtils.GetValue(parser.DocumentElement.Attributes["SupplierId"]);
            Guid supplierId = new Guid(supplierIdStr);
            Dictionary<string, DML.ExpertiseContainer> accountExpertise = accountRepositoryDal.GetAccountExpertise(supplierId);

            DataAccessLayer.SupplierPricing supPricingDal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            var supplierPricing = supPricingDal.GetLastSupplierPricing(supplierId);
            int rechargeAmount =
                (supplierPricing != null
                && supplierPricing.new_renew.HasValue
                && supplierPricing.new_renew.Value
                && supplierPricing.new_rechargeamount.HasValue) ? supplierPricing.new_rechargeamount.Value
                : -1;
            Guid badExpertise = Guid.Empty;
            string badPrice = string.Empty;
            XmlNodeList expertises = parser.SelectNodes("//PrimaryExpertise");
            EntityUtils entityUtils = new EntityUtils(CrmService);
            int stageInRegistration = accountRepositoryDal.GetStageInRegistration(supplierId);
            var supplierDbStatus = accountRepositoryDal.GetSupplierDataBaseStatus(supplierId);
            int expertiseCount = 0;
            int dailyBudgetSumForAvg = 0;
            bool changed = false;
            DataAccessLayer.PrimaryExpertise expertiseDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            // Go all over the primary expertise in the request
            List<Guid> listIfMultipleExists = new List<Guid>();
            foreach (XmlNode experties in expertises)
            {
                string Certificate = experties.Attributes["Certificate"].InnerText;
                string primaryExpertiseId = experties.Attributes["ID"].InnerText.ToLower();
                Guid expertiseIdGuid = new Guid(primaryExpertiseId);
                if (listIfMultipleExists.Contains(expertiseIdGuid))
                    continue;
                listIfMultipleExists.Add(expertiseIdGuid);
                if (stageInRegistration < (int)DML.Xrm.account.StageInRegistration.EXPERTISE &&
                    supplierDbStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Trial)
                {
                    int budget = expertiseDal.GetDailyCapacityCount(expertiseIdGuid);
                    if (budget > 0)
                    {
                        dailyBudgetSumForAvg += budget;
                        expertiseCount++;
                    }
                }
                bool newlyCreated = false;
                Guid expertiseid = Guid.Empty;
                LogUtils.MyHandle.WriteToLog(8, "Xml primaryExpertiseId: {0}", primaryExpertiseId);

                // Check if the primary expertise is created in the CRM
                if (!accountExpertise.ContainsKey(primaryExpertiseId))
                {
                    newlyCreated = true;
                    DynamicEntity expertiesEntity = ConvertorCRMParams.GetNewDynamicEntity("new_accountexpertise");

                    ConvertorCRMParams.SetDynamicEntityPropertyValue(expertiesEntity, "new_accountid",
                       ConvertorCRMParams.GetCrmLookup("account", supplierId), typeof(LookupProperty));

                    ConvertorCRMParams.SetDynamicEntityPropertyValue(expertiesEntity, "new_primaryexpertiseid",
                       ConvertorCRMParams.GetCrmLookup("new_primaryexpertise", expertiseIdGuid), typeof(LookupProperty));

                    ConvertorCRMParams.SetDynamicEntityPropertyValue(expertiesEntity, "new_certificate",
                       ConvertorCRMParams.GetCrmBoolean(ConvertorUtils.ToBoolean(Certificate)), typeof(CrmBooleanProperty));

                    decimal defaultPrice = FindDefaultPriceForExpertise(expertiseDal, expertiseIdGuid, isFromAAR, supplierDbStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Trial);

                    DynamicEntityUtils.SetPropertyValue(expertiesEntity, "new_incidentprice",
                        ConvertorCRMParams.GetCrmMoney(defaultPrice), typeof(CrmMoneyProperty));

                    if (GlobalConfigurations.IsUsaSystem || rechargeAmount == -1 || rechargeAmount >= defaultPrice)
                    {
                        expertiseid = MethodsUtils.CreateEntity(CrmService, expertiesEntity);
                        DML.ExpertiseContainer tempExpertise = new DML.ExpertiseContainer();
                        tempExpertise.IsActive = true;
                        tempExpertise.AllreadyHandled = true;
                        tempExpertise.AccountExpertiseId = expertiseid;
                        tempExpertise.PrimaryExpertiseId = new Guid(primaryExpertiseId);
                        tempExpertise.SecondaryExpertiseIds = new List<Guid>();
                        accountExpertise.Add(primaryExpertiseId, tempExpertise);
                        changed = true;
                    }
                    else
                    {
                        badExpertise = new Guid(primaryExpertiseId);
                        badPrice = string.Format("{0:0}", defaultPrice);
                        continue;
                    }
                }
                else
                {
                    var container = accountExpertise[primaryExpertiseId];
                    expertiseid = container.AccountExpertiseId;

                    if (GlobalConfigurations.IsUsaSystem || rechargeAmount == -1 || rechargeAmount >= container.IncidentPrice)
                    {
                        //Effect.Crm.DB.DataBaseUtils.DirectUpdateInstance("new_accountexpertise", expertiseid, new string[] { "new_certificate" }, new object[] { ConvertorUtils.ToBoolean(Certificate) });
                        string sqlUpdateDirect = "update new_accountexpertise set new_certificate = @cert where new_accountexpertiseid = @id";
                        DataAccessLayer.Generic.SqlParametersList updateCertParameters = new DataAccessLayer.Generic.SqlParametersList();
                        updateCertParameters.Add("@id", expertiseid);
                        updateCertParameters.Add("@cert", ConvertorUtils.ToBoolean(Certificate));
                        expertiseDal.ExecuteNonQuery(sqlUpdateDirect, updateCertParameters);

                        if (((DML.ExpertiseContainer)accountExpertise[primaryExpertiseId]).IsActive == false)
                        {
                            //Reactivate
                            entityUtils.SetEntityState("new_accountexpertise", expertiseid, "Active");
                            changed = true;
                        }

                        ((DML.ExpertiseContainer)accountExpertise[primaryExpertiseId]).AllreadyHandled = true;
                    }
                    else
                    {
                        badPrice = string.Format("{0:0}", container.IncidentPrice);
                        badExpertise = new Guid(primaryExpertiseId);
                        ((DML.ExpertiseContainer)accountExpertise[primaryExpertiseId]).AllreadyHandled = true;
                        continue;
                    }
                }

                XmlNodeList secondaryExpertiseList = experties.SelectNodes("./SecondaryExpertise");

                foreach (XmlNode secondaryExpertise in secondaryExpertiseList)
                {
                    string secondaryExpertiseId = secondaryExpertise.InnerText;

                    bool createSecondary = newlyCreated;
                    LogUtils.MyHandle.WriteToLog(8, "Xml secondaryExpertiseId: {0}", secondaryExpertiseId);

                    // If the secondary expertise is not in the list the we want to add it to the crm
                    if (accountExpertise.ContainsKey(primaryExpertiseId))
                    {
                        // if the secondary is in the list and in the xml then we remove it from the list to avoid deletion later on
                        if (((DML.ExpertiseContainer)accountExpertise[primaryExpertiseId]).SecondaryExpertiseIds.Contains(new Guid(secondaryExpertiseId)))
                            ((DML.ExpertiseContainer)accountExpertise[primaryExpertiseId]).SecondaryExpertiseIds.Remove(new Guid(secondaryExpertiseId));
                        else
                            createSecondary = true;
                    }

                    if (createSecondary)
                    {
                        entityUtils.ManyToManyAssign("new_secondaryexpertise_new_accountexpertise",
                           "new_accountexpertise", expertiseid, "new_secondaryexpertise", new Guid(secondaryExpertiseId));
                    }
                }
            }

            foreach (DML.ExpertiseContainer currExpertise in accountExpertise.Values)
            {
                LogUtils.MyHandle.WriteToLog(8, "PrimaryId: {0},IsHandled: {1},IsActive: {2}", currExpertise.PrimaryExpertiseId, currExpertise.AllreadyHandled, currExpertise.IsActive);
                if (currExpertise.AllreadyHandled == false)
                {
                    if (currExpertise.IsActive)
                    {
                        LogUtils.MyHandle.WriteToLog(8, "Disabling AccountExpertiseId: {0}", currExpertise.AccountExpertiseId);
                        entityUtils.SetEntityState("new_accountexpertise", currExpertise.AccountExpertiseId, "Inactive");
                        changed = true;
                        //detach direct number:
                        DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                        dnDal.DetachDirectNumbersFromAccountExpertise(currExpertise.AccountExpertiseId, false);
                    }
                }
                //else
                //{
                //    // Remove all the secondary expertise assigned to that primary expertise that were left untouched (they weren't in the xml)
                //    LogUtils.MyHandle.WriteToLog(8, "Delete all Secondaries");
                //    for (int i = 0; i < currExpertise.SecondaryExpertiseIds.Count; i++)
                //    {
                //        Guid secondaryExpertiseId = currExpertise.SecondaryExpertiseIds[i];
                //        LogUtils.MyHandle.WriteToLog(8, "Delete Secondary: {0}", secondaryExpertiseId);
                //        Effect.Crm.DB.DataBaseUtils.ExecuteSqlString("DELETE FROM new_secondaryexpertise_new_accountexpertise WHERE new_secondaryexpertiseid = '{0}' AND new_accountexpertiseid = '{1}'", secondaryExpertiseId, currExpertise.AccountExpertiseId);                       
                //    }
                //}
            }

            if (stageInRegistration == (int)DML.Xrm.account.StageInRegistration.PAID)
            {
                //SupplierManager supManager = new SupplierManager(XrmDataContext);
                //var supplierStatus = supManager.GetSupplierStatus(supplierId);
                //if (supplierStatus.SupplierStatus == NoProblem.Core.DataModel.SupplierModel.SupplierStatus.Available)
                //{
                DirectNumbersAssigner assigner = new DirectNumbersAssigner();
                assigner.AssignDirectNumbersToSupplier(supplierId);
                //}
            }
            else
            {
                if (!isFromAAR)// Adding the stage 
                {
                    accountRepositoryDal.SetSupplierRegistrationStage(supplierId, DML.Xrm.account.StageInRegistration.EXPERTISE);
                    accountRepositoryDal.SetSupplierTrialRegistrationStage(supplierId, DML.Xrm.account.StageInTrialRegistration.EnteredHeading);
                }
                if (supplierDbStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Trial)
                {
                    //Set daily budget if is trail supplier.  
                    if (expertiseCount > 0)
                    {
                        int budget = dailyBudgetSumForAvg / expertiseCount;
                        DailyBudgetManager budgetManager = new DailyBudgetManager(XrmDataContext);
                        budgetManager.UpdateDailyBudget(supplierId, budget);
                    }
                }
            }

            //check unavailability:
            CheckNeedsAvailabilityChange(supplierId);
            Dapaz.PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(supplierId);
            inserter.InsertToStats();
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            int? currentStep = (from acc in accountRepositoryDal.All
                                where acc.accountid == supplierId
                                select acc.new_stepinregistration2014).First();
            if (badExpertise != Guid.Empty)
            {
                string expertiseName = expertiseDal.GetPrimaryExpertiseNameById(badExpertise);
                return "<SupplierExpertise><Status>Failure;BadExpertise=" + expertiseName + ";DefaultPrice=" + badPrice + ";RechargePrice=" + rechargeAmount + "</Status><Step>" + (currentStep.HasValue ? currentStep.Value : (int)DML.Xrm.account.StepInRegistration2014.BusinessDetails_Category_4) + "</Step></SupplierExpertise>";
            }
            else
            {
                if (changed && userId != Guid.Empty)//this makes sure the request comes from the screen and not some other process. and that a change was made.
                {
                    //SendOrder:
                    new Dapaz.Orders.OrdersSender(userId, NoProblem.Core.BusinessLogic.Dapaz.Orders.eOrderType.HeadingsChange, supplierId).SendOrder();
                }
                if (Dapaz.PpaOnlineSync.SyncConfiguration.IsEnabled)
                {
                    Dapaz.PpaOnlineSync.Syncher syncher = new Dapaz.PpaOnlineSync.Syncher(supplierId);
                    syncher.Start();
                }
                //update step in registration:
                if (!currentStep.HasValue || currentStep.Value < (int)DML.Xrm.account.StepInRegistration2014.BusinessDetails_Area_5)
                {
                    DataModel.Xrm.account supplier = new DataModel.Xrm.account();
                    supplier.accountid = supplierId;
                    supplier.new_stepinregistration2014 = (int)DML.Xrm.account.StepInRegistration2014.BusinessDetails_Area_5;
                    accountRepositoryDal.Update(supplier);
                    accountRepositoryDal.XrmDataContext.SaveChanges();
                    currentStep = (int)DML.Xrm.account.StepInRegistration2014.BusinessDetails_Area_5;
                }
                NoProblem.Core.BusinessLogic.SEODataServices.SEOSupplierDataRetriever.ClearSupplierPageDataCache(supplierId);
                return "<SupplierExpertise><Status>Success</Status><Step>" + currentStep + "</Step></SupplierExpertise>";
            }
        }

        private decimal FindDefaultPriceForExpertise(DataAccessLayer.PrimaryExpertise expertiseDal, Guid expertiseIdGuid, bool isFromAAR, bool isTrial)
        {
            decimal defaultPrice = 1;
            if (isFromAAR || isTrial)
            {
                decimal expertiseDefaultPrice = expertiseDal.GetDefaultPrice(expertiseIdGuid);
                defaultPrice = expertiseDefaultPrice;
            }
            else
            {
                var expertise = expertiseDal.Retrieve(expertiseIdGuid);
                DataAccessLayer.PredefinedPrice preDefinedPricesDal = new NoProblem.Core.DataAccessLayer.PredefinedPrice(XrmDataContext);
                List<NoProblem.Core.DataModel.Reports.PreDefinedPriceData> dataList = new List<NoProblem.Core.DataModel.Reports.PreDefinedPriceData>();
                NoProblem.Core.DataModel.Reports.PreDefinedPriceData data = new NoProblem.Core.DataModel.Reports.PreDefinedPriceData();
                data.Date = DateTime.Now;
                dataList.Add(data);
                preDefinedPricesDal.GetPreDefinedPrice(dataList, expertise.new_code);
                var preDefinedAverage = Math.Ceiling(dataList.ElementAt(0).PreDefinedPrice);
                if (preDefinedAverage == 0)//no average calculated today, use heading's min price if there is one.
                {
                    decimal minimumPrice = expertiseDal.GetMinimumPrice(expertiseIdGuid);
                    defaultPrice = minimumPrice;
                }
                else
                {
                    defaultPrice = preDefinedAverage;
                }
            }
            return defaultPrice;
        }

        private void CheckNeedsAvailabilityChange(Guid supplierId)
        {
            if (GlobalConfigurations.IsUsaSystem)
                return;
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = accountRepositoryDal.Retrieve(supplierId);
            if (supplier.new_stageinregistration < (int)DML.Xrm.account.StageInRegistration.PAID || !supplier.new_cashbalance.HasValue)
            {
                return;
            }
            decimal minPrice, maxPrice;
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            accExpDal.GetSupplierMinAndMaxPrices(supplierId, out maxPrice, out minPrice);
            if (supplier.new_cashbalance.Value < minPrice)
            {
                DataAccessLayer.UnavailabilityReason reasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
                DML.Xrm.new_unavailabilityreason reason = reasonDal.GetPricingEndedReason();
                supplier.new_unavailabilityreasonid = reason.new_unavailabilityreasonid;
                supplier.new_unavailablefrom = DateTime.Now;
                supplier.new_unavailableto = null;
                supplier.new_unavailablelocalto = null;
                if (supplier.address1_utcoffset.HasValue)
                {
                    supplier.new_unavailablelocalfrom = FixDateToLocal(DateTime.Now, supplier.address1_utcoffset.Value);
                }
                else
                {
                    supplier.new_unavailablelocalfrom = FixDateToLocal(DateTime.Now);
                }
                accountRepositoryDal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
            else if (supplier.new_unavailabilityreasonid.HasValue)
            {
                DataAccessLayer.UnavailabilityReason reasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
                DML.Xrm.new_unavailabilityreason reason = reasonDal.GetPricingEndedReason();
                if (supplier.new_unavailabilityreasonid.Value == reason.new_unavailabilityreasonid)
                {
                    supplier.new_unavailablefrom = null;
                    supplier.new_unavailablelocalfrom = null;
                    supplier.new_unavailableto = null;
                    supplier.new_unavailablelocalto = null;
                    supplier.new_unavailabilityreasonid = null;
                    accountRepositoryDal.Update(supplier);
                    XrmDataContext.SaveChanges();
                }
            }
        }

        public string GetSupplierComments(string supplierId)
        {
            string query = @"select new_suppliernoteid,new_suppliernote.new_userid, account.name as accountName,new_suppliernote.new_description,new_suppliernote.createdon,new_suppliernote.new_file
FROM new_suppliernote with (nolock)
Inner Join Account with (nolock) on account.accountid = new_suppliernote.new_userid
WHERE new_supplierid = '{0}'
ORDER BY new_suppliernote.createdon DESC";
            StringWriter xmlWriter = new StringWriter();
            XmlTextWriter xmlFormater = new XmlTextWriter(xmlWriter);
            xmlFormater.WriteStartElement("SupplierComments");
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(string.Format(query, supplierId), connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader annotationReader = command.ExecuteReader())
                    {
                        while (annotationReader.Read())
                        {
                            xmlFormater.WriteStartElement("SupplierComment");
                            xmlFormater.WriteElementString("CommentId", annotationReader["new_suppliernoteid"].ToString());
                            xmlFormater.WriteElementString("CreatedBy", annotationReader["accountName"].ToString());
                            xmlFormater.WriteElementString("Text", annotationReader["new_description"].ToString());
                            xmlFormater.WriteElementString("CreatedOn", FixDateToLocal((DateTime)annotationReader["createdon"]).ToString());
                            xmlFormater.WriteElementString("File", ConvertorUtils.ToString(annotationReader["new_file"], string.Empty));
                            xmlFormater.WriteEndElement();
                        }
                    }
                }
            }
            xmlFormater.WriteEndElement();
            return xmlWriter.ToString();
        }

        public string UpsertSupplierComment(string Request)
        {
            /*
             * <SupplierComment ID="">
             *    <UserId></UserId>
             *    <SupplierId></SupplierId>
             *    <Text>Some Text</Text>
             *    <File>some file name</File>
             * </SupplierComment>
             */

            XmlDocument parser = new XmlDocument();
            parser.LoadXml(Request);

            DynamicEntity comment = ConvertorCRMParams.GetNewDynamicEntity("new_suppliernote");

            XmlAttribute commentId = parser.DocumentElement.Attributes["ID"];
            bool isUpdateRequest = false;

            if (commentId != null && commentId.InnerText != "")
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(comment, "new_suppliernoteid",
                   ConvertorCRMParams.GetCrmKey(new Guid(commentId.InnerText)), typeof(KeyProperty));

                isUpdateRequest = true;
            }

            if (parser.DocumentElement["UserId"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(comment, "new_userid",
                   ConvertorCRMParams.GetCrmLookup("account", new Guid(parser.DocumentElement["UserId"].InnerText)), typeof(LookupProperty));

            if (parser.DocumentElement["SupplierId"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(comment, "new_supplierid",
                   ConvertorCRMParams.GetCrmLookup("account", new Guid(parser.DocumentElement["SupplierId"].InnerText)), typeof(LookupProperty));

            if (parser.DocumentElement["Text"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(comment, "new_description",
                   parser.DocumentElement["Text"].InnerText, typeof(StringProperty));

            if (parser.DocumentElement["File"] != null)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(comment, "new_file",
                    parser.DocumentElement["File"].InnerText, typeof(StringProperty));

            if (isUpdateRequest)
                MethodsUtils.UpdateEntity(CrmService, comment);
            else
                MethodsUtils.CreateEntity(CrmService, comment);

            return "<SupplierComment><Status>Success</Status></SupplierComment>";
        }

        //protected override void FixDatesToUtcFullDays(ref DateTime fromDate, ref DateTime toDate)
        //{
        //    DataAccessLayer.ConfigurationSettings configDAL = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
        //    int publisherTimeZone = int.Parse(configDAL.GetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE));
        //    FixDatesToUtcFullDays(ref fromDate, ref toDate, publisherTimeZone);
        //}

        //private void FixDatesToUtcFullDays(ref DateTime fromDate, ref DateTime toDate, int timeZone)
        //{
        //    DateTime fromOnlyDate = fromDate.Date;
        //    DateTime toDateOnly = toDate.Date.AddDays(1);
        //    fromDate = Utils.ConvertUtils.GetUtcFromLocal(XrmDataContext, fromOnlyDate, timeZone);
        //    toDate = Utils.ConvertUtils.GetUtcFromLocal(XrmDataContext, toDateOnly, timeZone);
        //}

        //[Obsolete("User ReportsManager.MyCallsReport instead")]
        public string GetSuppliersIncidents(string publisherId, string supplierIdStr, DateTime fromDate, DateTime toDate, string incidentNumber, DML.IncidentWinStatus status)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            int timeZone = 0;
            //bool isPublisher = false;
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            Guid supplierId = new Guid(supplierIdStr);
            var supplier = accountRepositoryDal.Retrieve(supplierId);
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            if (publisherId != null && !string.IsNullOrEmpty(publisherId))
            {
                // isPublisher = true;
                timeZone = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.PUBLISHER_TIME_ZONE));
            }
            else
            {
                timeZone = supplier.address1_utcoffset.HasValue ? supplier.address1_utcoffset.Value : -1;
                if (timeZone == -1)
                {
                    timeZone = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.PUBLISHER_TIME_ZONE));
                }
            }
            //bool isOverRefundPercentage = supplier.new_isrefundblocked.HasValue ? supplier.new_isrefundblocked.Value : false;
            //string daysLimitStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.REFUND_DAYS_BACK_LIMIT);
            //int daysLimit;
            //if (int.TryParse(daysLimitStr, out daysLimit) == false)
            //{
            //    daysLimit = 365;
            //}
            //string recordStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.RECORD_CALLS);
            //bool recordCalls = recordStr == "1";
            //DataAccessLayer.Channel channelDal = new NoProblem.Core.DataAccessLayer.Channel(XrmDataContext);
            //Guid callChannelId = channelDal.GetChannelIdByCode(NoProblem.Core.DataModel.Xrm.new_channel.ChannelCode.Call);
            DateTime fromDateUtc = fromDate;
            DateTime toDateUtc = toDate;
            FixDatesToFullDays(ref fromDateUtc, ref toDateUtc);
            fromDateUtc = FixDateToUtcFromLocal(fromDateUtc, timeZone);
            toDateUtc = FixDateToUtcFromLocal(toDateUtc, timeZone);
            //FixDatesToUtcFullDays(ref fromDateUtc, ref toDateUtc, timeZone);

            string sql = @"SELECT fiac.new_incidentaccountid,fiac.new_recordfilelocation
				,fiac.new_tocharge,fiac.new_refundstatus,finc.ticketnumber
				--,finc.createdon
				,dbo.fn_UTCToLocalTimeByTimeZoneCode(finc.createdon, @timeZoneCode) createdonfixed
				,finc.title,fiac.statuscode
				,finc.new_primaryexpertiseidname,fiac.new_channelid
				FROM incident finc with (nolock)
				INNER JOIN new_incidentaccount fiac with (nolock) on fiac.new_incidentid = finc.incidentid and new_accountid = @AccountId
				WHERE 
			   fiac.deletionstatecode = 0
				  AND
			   finc.deletionstatecode = 0
				  AND
			   finc.statuscode != 2";//2 is waiting. We don't want the incident to appear if it is still in process.

            if (incidentNumber == null || incidentNumber == "")
                sql += " AND finc.createdon BETWEEN @dt0 AND @dt1 ";
            else
                sql += " AND finc.ticketnumber = @ticketnumber";


            if (status == DML.IncidentWinStatus.Lose)
            {
                sql += " AND not(fiac.statuscode = 10 or fiac.statuscode = 12 or fiac.statuscode = 13) ";
            }
            else if (status == DML.IncidentWinStatus.Win)
            {
                sql += " AND (fiac.statuscode = 10 or fiac.statuscode = 12 or fiac.statuscode = 13) ";
            }

            sql += " AND finc.deletionstatecode = 0 ORDER BY finc.createdon DESC";

            string retVal = string.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Connection.Open();
                    cmd.Parameters.AddWithValue("@AccountId", supplierId);
                    cmd.Parameters.AddWithValue("@timeZoneCode", timeZone);
                    if (incidentNumber == null || incidentNumber == "")
                    {
                        cmd.Parameters.AddWithValue("@dt0", fromDateUtc);
                        cmd.Parameters.AddWithValue("@dt1", toDateUtc);
                    }
                    else
                        cmd.Parameters.AddWithValue("@ticketnumber", incidentNumber);
                    using (SqlDataReader incidents = cmd.ExecuteReader())
                    {
                        StringWriter xmlText = new StringWriter();
                        XmlTextWriter xmlFormatter = new XmlTextWriter(xmlText);
                        xmlFormatter.WriteStartElement("Incidents");
                        xmlFormatter.WriteAttributeString("RecordsCalls", supplier.new_recordcalls.HasValue ? supplier.new_recordcalls.Value.ToString() : false.ToString());
                        while (incidents.Read())
                        {
                            xmlFormatter.WriteStartElement("Incident");
                            Guid incidentAccountId = (Guid)incidents["new_incidentaccountid"];
                            xmlFormatter.WriteAttributeString("CallId", incidentAccountId.ToString());
                            object caseNumber = incidents["ticketnumber"];
                            xmlFormatter.WriteAttributeString("TicketNumber", caseNumber.ToString());
                            //DateTime createdon = (DateTime)incidents["createdon"];
                            //DateTime createdonFixed = FixDateToLocal(createdon, timeZone);
                            DateTime createdonFixed = (DateTime)incidents["createdonfixed"];
                            xmlFormatter.WriteAttributeString("CreatedOn", string.Format("{0:g}", createdonFixed));
                            object primaryExpertise = incidents["new_primaryexpertiseidname"];
                            xmlFormatter.WriteAttributeString("PrimaryExpertise", primaryExpertise.ToString());
                            xmlFormatter.WriteAttributeString("Title", incidents["title"].ToString());
                            object recordId = incidents["new_recordfilelocation"];
                            bool wasRefunded = false;
                            bool toCharge = (bool)incidents["new_tocharge"];
                            int statusCode = (int)incidents["statuscode"];
                            DML.Xrm.new_incidentaccount.Status iaStatus = (DML.Xrm.new_incidentaccount.Status)statusCode;
                            Guid channelId = incidents["new_channelid"] == DBNull.Value ? Guid.Empty : (Guid)incidents["new_channelid"];
                            if (toCharge)
                            {
                                string refundStatusStr = null;
                                if (incidents["new_refundstatus"] != DBNull.Value)
                                {
                                    refundStatusStr = incidents["new_refundstatus"].ToString();
                                }
                                string refundStatusValue = "true";
                                if (refundStatusStr != null)
                                {
                                    DML.Xrm.new_incidentaccount.RefundSatus refundStatus = (DML.Xrm.new_incidentaccount.RefundSatus)Enum.Parse(typeof(DML.Xrm.new_incidentaccount.RefundSatus), refundStatusStr);
                                    if (refundStatus == DML.Xrm.new_incidentaccount.RefundSatus.Pending)
                                    {
                                        refundStatusValue = refundStatus.ToString();
                                    }
                                }
                                //if (!isPublisher && refundStatusValue == "true")//if is advertiser user and was charged for this call (and hasn't asked for refund yet).
                                //{
                                //    if (isOverRefundPercentage)
                                //    {
                                //        refundStatusValue = "OverPercentageLimit";
                                //    }
                                //    else if (createdon.Date <= DateTime.Now.Date.AddDays(daysLimit * -1))//if it is an old call
                                //    {
                                //        refundStatusValue = "OverDaysLimit";
                                //    }
                                //    else if (iaStatus == NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.INVITED_BY_SMS//if it was invited or a channel that is not call.
                                //        || (channelId != Guid.Empty && channelId != callChannelId))
                                //    {
                                //        refundStatusValue = "NoPhoneConnection";
                                //    }
                                //    else if (recordCalls && (recordId == null || recordId == DBNull.Value))//if the organization has recordings but there is no recording for this call.
                                //    {
                                //        refundStatusValue = "NoRecording";
                                //    }
                                //}
                                xmlFormatter.WriteAttributeString("RefundStatus", refundStatusValue);
                            }
                            else
                            {
                                wasRefunded = true;
                                xmlFormatter.WriteAttributeString("RefundStatus", "false");
                            }

                            bool won = false;
                            if (iaStatus == DML.Xrm.new_incidentaccount.Status.CLOSE ||
                                iaStatus == DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS ||
                                iaStatus == DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS)
                            {
                                won = true;
                            }
                            xmlFormatter.WriteAttributeString("Win", won.ToString());

                            //               if (won && !wasRefunded)
                            //              {
                            if (recordId != null && recordId != DBNull.Value)
                            {
                                xmlFormatter.WriteAttributeString("RecordLocation", recordId.ToString());
                            }
                            //         }
                            xmlFormatter.WriteEndElement(); // End of Incident
                        }
                        xmlFormatter.WriteEndElement(); // End of Incidents
                        retVal = xmlText.ToString();
                    }
                }
            }
            return retVal;
        }

        public void AssignDirectNumbersToAccount(Guid AccountId)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlTransaction trans = conn.BeginTransaction("AssignDirectNumbers"))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        try
                        {
                            cmd.Transaction = trans;
                            cmd.Connection = conn;
                            cmd.CommandType = CommandType.Text;

                            //Select the needed amount of free direct numbers
                            cmd.CommandText = "SELECT new_originid FROM new_origin with (nolock) WHERE new_isdirectnumber = 1";
                            DataTable origins = new DataTable();
                            using (SqlDataAdapter originLoader = new SqlDataAdapter())
                            {
                                originLoader.SelectCommand = cmd;
                                originLoader.Fill(origins);
                            }
                            cmd.CommandText = string.Format(
             @"SELECT new_accountexpertiseid
FROM new_accountexpertise with(nolock)
WHERE deletionstatecode = 0 AND statecode = 0 
AND new_accountid = '{0}'
And new_accountexpertiseid not in(
select new_accountexpertiseid
from new_directnumber with(nolock)
where deletionstatecode = 0 and new_accountexpertiseid is not null
)", AccountId);

                            DataTable accountExpertise = new DataTable();
                            using (SqlDataAdapter accountExpertiseLoader = new SqlDataAdapter())
                            {
                                accountExpertiseLoader.SelectCommand = cmd;
                                accountExpertiseLoader.Fill(accountExpertise);
                            }
                            //Select the needed amount of free direct numbers
                            cmd.CommandText = "SELECT TOP " + accountExpertise.Rows.Count * origins.Rows.Count + " new_number FROM new_directnumberextensionbase (UPDLOCK) WHERE new_accountexpertiseid IS NULL";
                            DataTable availablenumbers = new DataTable();
                            using (SqlDataAdapter loader = new SqlDataAdapter())
                            {
                                loader.SelectCommand = cmd;
                                loader.Fill(availablenumbers);
                            }
                            int availableNumbersCounter = 0;
                            if (availablenumbers.Rows.Count == (accountExpertise.Rows.Count * origins.Rows.Count))
                            {
                                DataAccessLayer.DirectNumber directNumberDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                                for (int j = 0; j < origins.Rows.Count; j++)
                                {
                                    // Assign each available number to the account and primary expertise
                                    foreach (DataRow currAccountExpertise in accountExpertise.Rows)
                                    {
                                        DML.Xrm.new_directnumber directNumber = directNumberDal.GetDirectNumberByNumber(availablenumbers.Rows[availableNumbersCounter]["new_number"].ToString());
                                        directNumber.new_accountexpertiseid = new Guid(currAccountExpertise["new_accountexpertiseid"].ToString());
                                        directNumber.new_originid = new Guid(origins.Rows[j]["new_originid"].ToString());
                                        directNumberDal.Update(directNumber);

                                        //cmd.CommandText = "UPDATE new_directnumberextensionbase SET new_accountexpertiseid = @accountexpertiseid,new_originid = @originid WHERE new_number = @number";
                                        //cmd.Parameters.AddWithValue("@accountexpertiseid", currAccountExpertise["new_accountexpertiseid"]);
                                        //cmd.Parameters.AddWithValue("@number", availablenumbers.Rows[availableNumbersCounter]["new_number"]);
                                        //cmd.Parameters.AddWithValue("@originid", origins.Rows[j]["new_originid"]);

                                        //cmd.ExecuteNonQuery();
                                        //cmd.Parameters.Clear();
                                        availableNumbersCounter++;
                                    }
                                }

                            }

                            trans.Commit();
                            XrmDataContext.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            throw ex;
                        }
                    }
                }
            }
        }

        public string GetRegions(int RegionLevel)
        {
            RegionUtils regionUtil = new RegionUtils();
            return regionUtil.GetRegions(RegionLevel);
        }

        public string GetAccountExpertiseIncidentAmount(string accountId)
        {
            //            string query = @"SELECT ISNULL(count(incidentid),0)
            //FROM incident with (nolock)
            //INNER JOIN new_accountexpertise ae with (nolock) ON ae.new_accountid = '{0}' AND 
            //	incident.new_primaryexpertiseid = ae.new_primaryexpertiseid
            //INNER JOIN new_regionaccountexpertise rae with (nolock) ON rae.new_accountid = '{0}' AND 
            //	incident.new_regionid = rae.new_regionid";


            //FROM incident inc
            //INNER JOIN new_accountexpertise ae ON ae.new_primaryexpertiseid = inc.new_primaryexpertiseid AND
            //   (ae.new_secondaryexpertiseid IS NULL OR ae.new_secondaryexpertiseid = inc.new_secondaryexpertiseid) AND
            //   ae.new_accountid = '{0}'
            //INNER JOIN new_servicearea sa ON sa.new_accountexpertiseid = ae.new_accountexpertiseid AND
            //   (sa.new_cityidname = inc.new_address1city OR sa.new_cityid IS NULL)
            //INNER JOIN new_city city ON city.new_name = inc.new_address1city AND city.new_citygroupid = sa.new_citiesgroupid";
            //WHERE inc.createdon > DATEADD(day,-30,GETDATE())";

            //object ret = Effect.Crm.DB.DataBaseUtils.GetValueFromSqlString(query, AccountId);

            string query = @"SELECT ISNULL(count(incidentid),0)
FROM incident with (nolock)
INNER JOIN new_accountexpertise ae with (nolock) ON ae.new_accountid = @id AND 
	incident.new_primaryexpertiseid = ae.new_primaryexpertiseid
INNER JOIN new_regionaccountexpertise rae with (nolock) ON rae.new_accountid = @id AND 
	incident.new_regionid = rae.new_regionid";


            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@id", new Guid(accountId));
            object ret = dal.ExecuteScalar(query, parameters);

            string retAmount = @"<IncidentAmount>" + ret.ToString() + "</IncidentAmount>";

            return retAmount;
        }

        /// <summary>
        /// Sets the PublisherTimeZone configuration setting.
        /// </summary>
        /// <param name="timeZoneCode"></param>
        public void SetUserTimeZone(int timeZoneCode)
        {
            DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            configSettingsAccess.SetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE, timeZoneCode.ToString());
        }
    }
}
