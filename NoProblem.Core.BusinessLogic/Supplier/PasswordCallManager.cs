﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.TwilioBL;
using NoProblem.Core.DataModel;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.SupplierBL
{
    public class PasswordCallManager : TwilioHandlerBase
    {
        public PasswordCallManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public bool GetPasswordByPhone(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = dal.Retrieve(supplierId);
            TwilioBL.TwilioCallsExecuter executer = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioCallsExecuter(XrmDataContext);
            bool ok = executer.InitatePasswordCall(supplier.telephone1, supplierId);
            return ok;
        }

        public string CallAnswered(string id)
        {
            Guid supplierId = new Guid(id);
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            string password = dal.GetPassword(supplierId);
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string audioFilesUrlBase = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
            response.BeginGather(new
            {
                action = twilioCallBackUrlBase + "/PasswordCallClickHandler?id=" + id + "&SiteId=" + siteId,
                timeout = 5,
                method = "GET",
                numDigits = 1,
                finishOnKey = string.Empty
            });
            response.Play(audioFilesUrlBase + "/passwordcall/pre.mp3");
            foreach (char c in password)
            {
                    response.Play(audioFilesUrlBase +
                        AudioFilesConsts.SLASH + AudioFilesConsts.NATURAL_NUMBERS_RECORDINGS_FOLDER +
                        AudioFilesConsts.SLASH + c + "n" + AudioFilesConsts.MP3_FILE_EXTENSION);
            }
            response.Play(audioFilesUrlBase + "/passwordcall/post.mp3");
            response.EndGather();
            response.Redirect(twilioCallBackUrlBase + "/PasswordCallClickHandler?id=" + id + "&SiteId=" + siteId + "&Digits=NOTHING", "GET");
            return response.ToString();
        }

        public string ClickHandler(string id, string digits)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            if (digits == "1")
            {
                response.Redirect(twilioCallBackUrlBase + "/PasswordCallAnswered?id=" + id + "&SiteId=" + siteId, "GET");
            }
            else
            {
                response.Hangup();
                if (digits == "8")
                {
                    SaveMistakeDelegate del = new SaveMistakeDelegate(SaveMistake);
                    del.BeginInvoke(id, SaveMistakeAsyncCallback, del);
                }
            }
            return response.ToString();
        }

        delegate void SaveMistakeDelegate(string id);

        private void SaveMistake(string id)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(null);
            var supplier = dal.Retrieve(new Guid(id));
            DataAccessLayer.PasswordCallMistake mistakeDal = new DataAccessLayer.PasswordCallMistake(dal.XrmDataContext);
            DataModel.Xrm.new_passwordcallmistake mistake = new NoProblem.Core.DataModel.Xrm.new_passwordcallmistake();
            mistake.new_name = supplier.telephone1;
            mistake.new_accountid = supplier.accountid;
            mistakeDal.Create(mistake);
            dal.XrmDataContext.SaveChanges();
        }

        private void SaveMistakeAsyncCallback(IAsyncResult result)
        {
            try
            {
                ((SaveMistakeDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SaveMistakeAsyncCallback");
            }
        }
    }
}
