﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils;
using DML = NoProblem.Core.DataModel;
using System.Text;
using System.Threading;

namespace NoProblem.Core.BusinessLogic
{
    public partial class Supplier
    {
        public bool SetSupplierWorkAreas(Guid supplierId, string xmlAreas, string fullAddress, string name, string email, bool updateNameAndEmail, string userIdentity, bool isFromAdvRegistration)
        {
            //Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_regionaccountexpertise",
            //                        "new_accountid = '" + supplierId.ToString() + "'");

            StringBuilder builder = new StringBuilder();
            builder.Append("SetSupplierWorkAreas started. supplierId = ").Append(supplierId.ToString()).Append(" xmlAreas = ").Append(xmlAreas).Append(" fullAddress = ")
                .Append(fullAddress).Append(" name  =").Append(name).Append(" email = ").Append(email).Append(" updateNameAndEmail = ").Append(updateNameAndEmail.ToString())
                .Append(" userIdentity = ").Append(userIdentity).Append(" isFromAdvRegistration = ").AppendLine(isFromAdvRegistration.ToString());
            LogUtils.MyHandle.WriteToLog(builder.ToString());


            string deleteSql = "update new_regionaccountexpertise set DeletionStateCode = 2 where new_accountid = '" + supplierId.ToString() + "'";
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            dal.ExecuteNonQuery(deleteSql);

            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(xmlAreas);

            XmlNodeList xRegionslist = xDoc.SelectNodes("Areas/Area");
            XmlNodeList xRegionsNotSelected = xDoc.SelectNodes("Areas/AreaNotSelected");

            XmlNode xDetails = xDoc.SelectSingleNode("Areas");
            string latitude = xDetails.Attributes["Latitude"].Value;
            string longitude = xDetails.Attributes["Longitude"].Value;
            string radius = xDetails.Attributes["Radius"].Value;
            SupplierManager supManager = new SupplierManager(XrmDataContext);
            var upsertRequest = new NoProblem.Core.DataModel.SupplierModel.UpsertAdvertiserRequest()
            {
                SupplierId = supplierId,
                FullAddress = fullAddress
            };
            if (updateNameAndEmail)
            {
                upsertRequest.Email = email;
                upsertRequest.Company = name;
            }
            var upsertAns = supManager.UpsertAdvertiser(upsertRequest);
            if (upsertAns.Status == NoProblem.Core.DataModel.SupplierModel.UpsertAdvertiserStatus.EmailDuplicate)
            {
                return false;
            }
            string updateQurty = "update account set new_latitude = @latitude, new_longitude = @longitude, new_radiusnew = @radius where accountid= @supplierId";
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                using (SqlCommand comm = new SqlCommand(updateQurty, conn))
                {
                    conn.Open();
                    comm.Parameters.AddWithValue("@latitude", latitude);
                    comm.Parameters.AddWithValue("@longitude", longitude);
                    comm.Parameters.AddWithValue("@radius", radius);
                    comm.Parameters.AddWithValue("@supplierId", supplierId);
                    comm.ExecuteNonQuery();
                }
            }
            DataAccessLayer.ConfigurationSettings settingsDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            int maxLevel = int.Parse(settingsDal.GetConfigurationSettingValue(DML.ConfigurationKeys.MAX_REGION_LEVEL));
            List<Guid> regionsSelected = new List<Guid>();
            foreach (XmlNode region in xRegionslist)
            {
                regionsSelected.Add(new Guid(region.Attributes["RegionId"].InnerText));
            }
            List<Guid> regionsNotSelected = new List<Guid>();
            foreach (XmlNode region in xRegionsNotSelected)
            {
                regionsNotSelected.Add(new Guid(region.Attributes["RegionId"].InnerText));
            }
            List<Guid> brothers = new List<Guid>();
            DataAccessLayer.Region regionAccess = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            foreach (Guid id in regionsSelected)
            {
                IEnumerable<Guid> innerBrothers = regionAccess.GetBrothersIds(id);
                foreach (Guid broId in innerBrothers)
                {
                    if (!regionsSelected.Contains<Guid>(broId) && !regionsNotSelected.Contains<Guid>(broId) && !brothers.Contains<Guid>(broId))
                    {
                        brothers.Add(broId);
                    }
                }
            }
            foreach (Guid id in regionsNotSelected)
            {
                IEnumerable<Guid> innerBrothers = regionAccess.GetBrothersIds(id);
                foreach (Guid broId in innerBrothers)
                {
                    if (!regionsSelected.Contains<Guid>(broId) && !regionsNotSelected.Contains<Guid>(broId) && !brothers.Contains<Guid>(broId))
                    {
                        brothers.Add(broId);
                    }
                }
            }
            List<Guid> allMaxLevel = new List<Guid>(regionsSelected);
            allMaxLevel.AddRange(regionsNotSelected);
            allMaxLevel.AddRange(brothers);

            RegionUtils regionUtil = new RegionUtils();
            List<DML.SqlHelper.RegionMinData> parentRegions = regionUtil.GetParentRegions(allMaxLevel, maxLevel);
            if (maxLevel == 1)
            {
                HandleRegions(supplierId, regionsSelected, maxLevel, true);
            }
            else
            {
                for (int i = 1; i < maxLevel; i++)
                {
                    IEnumerable<Guid> regionsAtLevel = parentRegions.Where(x => x.Level == i).Select(x => x.Id);
                    if (i == 1)
                    {
                        HandleRegions(supplierId, regionsAtLevel, i, true);
                    }
                    else
                    {
                        List<Guid> brosAtLevel = new List<Guid>();
                        foreach (var r in regionsAtLevel)
                        {
                            IEnumerable<Guid> bros = regionAccess.GetBrothersIds(r);
                            foreach (Guid bId in bros)
                            {
                                if (!brosAtLevel.Contains(bId) && !regionsAtLevel.Contains(bId))
                                {
                                    brosAtLevel.Add(bId);
                                }
                            }
                        }
                        HandleRegions(supplierId, brosAtLevel, i, false);
                    }
                }
                //last level:
                List<Guid> lastLevelNotWithBros = new List<Guid>(regionsNotSelected);
                lastLevelNotWithBros.AddRange(brothers);
                HandleRegions(supplierId, lastLevelNotWithBros, maxLevel, false);
            }
            DoAfterLocationSavedUpdates(supplierId, userIdentity, isFromAdvRegistration, email);
            return true;
        }

        private void DoAfterLocationSavedUpdates(Guid supplierId, string userIdentity, bool isFromAdvRegistration, string email)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            accountRepositoryDal.SetSupplierRegistrationStage(supplierId, NoProblem.Core.DataModel.Xrm.account.StageInRegistration.PAID);//we want everything open for this "in trial" accounts.
            accountRepositoryDal.SetSupplierTrialRegistrationStage(supplierId, NoProblem.Core.DataModel.Xrm.account.StageInTrialRegistration.EnteredLocation);
            if (isFromAdvRegistration)
            {
                Invitations.InvitationsManager.HandleInvitationsAndEmailsOnRegistrationFinished(supplierId, email);
            }
            System.Threading.Thread tr = new System.Threading.Thread(delegate()
            {
                try
                {
                    var supplier = accountRepositoryDal.Retrieve(supplierId);
                    RegionUtils regionUtils = new RegionUtils();
                    string areasInRadius = regionUtils.GetAreasInRadius(decimal.Parse(supplier.new_longitude), decimal.Parse(supplier.new_latitude), new decimal(0.01));
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(areasInRadius);
                    XmlNodeList xRegionslist = xDoc.SelectNodes("Root/Area");
                    string idStr = xRegionslist[0].Attributes["RegionId"].InnerText;
                    Guid regionId = new Guid(idStr);
                    DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
                    var regData = regionDal.GetRegionMinDataById(regionId);
                    var parents = regionDal.GetParents(regData);
                    var level1Parent = parents.First(x => x.Level == 1);
                    bool updateNeeded = false;
                    bool timeZoneChanged = false;
                    if (level1Parent.TimeZoneCode != -1 && supplier.address1_utcoffset != level1Parent.TimeZoneCode)
                    {
                        supplier.address1_utcoffset = level1Parent.TimeZoneCode;
                        updateNeeded = true;
                        timeZoneChanged = true;
                    }
                    var status = (DML.Xrm.account.SupplierStatus)supplier.new_status;
                    bool finishedRegistrationNow = false;
                    if (isFromAdvRegistration && status == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Trial)
                    {
                        if (supplier.new_finishedwebregistrationon.HasValue == false)
                        {
                            supplier.new_finishedwebregistrationon = DateTime.Now;
                            supplier.new_finishedwebregistratrionwithstatusreason = supplier.new_trialstatusreason.Value;
                            finishedRegistrationNow = true;
                            if (supplier.new_isfromaar.HasValue && supplier.new_isfromaar.Value)
                            {
                                supplier.new_createdonoverride = DateTime.Now;
                            }
                            if (!string.IsNullOrEmpty(userIdentity))
                            {
                                supplier.new_useridentityatwebregistrationfinish = userIdentity;
                            }
                        }
                        if (supplier.new_trialstatusreason.HasValue == false ||
                            supplier.new_trialstatusreason.Value != (int)DML.Xrm.account.TrialStatusReasonCode.InTrial)
                        {
                            supplier.new_trialstatusreason = (int)DML.Xrm.account.TrialStatusReasonCode.InTrial;
                            supplier.new_trialstatusreasonmodifiedon = DateTime.Now;
                        }
                        supplier.new_status = (int)DML.Xrm.account.SupplierStatus.Approved;
                        updateNeeded = true;
                    }
                    if (updateNeeded)
                    {
                        accountRepositoryDal.Update(supplier);
                        XrmDataContext.SaveChanges();
                        if (finishedRegistrationNow)
                        {
                            SendAfterRegistrationNotifications(supplierId);
                        }
                        if (timeZoneChanged)
                        {
                            XElement xml = new XElement("SupplierAvailability");
                            xml.Add(new XAttribute("SiteId", ""));
                            xml.Add(new XAttribute("SupplierId", supplier.accountid));
                            XElement availability = new XElement("Availability");
                            availability.Add(
                                new XElement("Day",
                                    new XAttribute("Name", "sunday"),
                                    new XElement("FromTime", supplier.new_sundayavailablefrom),
                                    new XElement("TillTime", supplier.new_sundayavailableto)),
                                new XElement("Day",
                                    new XAttribute("Name", "monday"),
                                    new XElement("FromTime", supplier.new_mondayavailablefrom),
                                    new XElement("TillTime", supplier.new_mondayavailableto)),
                                new XElement("Day",
                                    new XAttribute("Name", "tuesday"),
                                    new XElement("FromTime", supplier.new_tuesdayavailablefrom),
                                    new XElement("TillTime", supplier.new_tuesdayavailableto)),
                                new XElement("Day",
                                    new XAttribute("Name", "wednesday"),
                                    new XElement("FromTime", supplier.new_wednesdayavailablefrom),
                                    new XElement("TillTime", supplier.new_wednesdayavailableto)),
                                new XElement("Day",
                                    new XAttribute("Name", "thursday"),
                                    new XElement("FromTime", supplier.new_thursdayavailablefrom),
                                    new XElement("TillTime", supplier.new_thursdayavailableto)),
                                new XElement("Day",
                                    new XAttribute("Name", "friday"),
                                    new XElement("FromTime", supplier.new_fridayavailablefrom),
                                    new XElement("TillTime", supplier.new_fridayavailableto)),
                                new XElement("Day",
                                    new XAttribute("Name", "saturday"),
                                    new XElement("FromTime", supplier.new_saturdayavailablefrom),
                                    new XElement("TillTime", supplier.new_saturdayavailableto))
                                );
                            xml.Add(availability);
                            BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(XrmDataContext, null);
                            supplierLogic.SetSupplierAvailability(xml.ToString(), true);
                        }
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in DoAfterLocationSavedUpdates async part.");
                }
            });
            tr.Start();
        }

        private void SendAfterRegistrationNotifications(Guid supplierId)
        {
            Thread tr = new Thread(new ParameterizedThreadStart(delegate(object supplierIdObj)
            {
                try
                {
                    Guid id = (Guid)supplierIdObj;
                    Notifications notifier = new Notifications(null);
                    DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(notifier.XrmDataContext);
                    string recipientEmail = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.REGISTRATION_FINISHED_ALERT_EMAIL);
                    string subject = string.Empty;
                    string body = string.Empty;
                    notifier.InstatiateTemplate(DML.TemplateNames.REGISTRATION_FINISHED_ALERT, id, "account", out subject, out body);
                    DataAccessLayer.AccountExpertise dal = new NoProblem.Core.DataAccessLayer.AccountExpertise(notifier.XrmDataContext);
                    var headings = dal.GetActiveHeadingNamesForAccount(id);
                    StringBuilder strBuilder = new StringBuilder(" ");
                    foreach (var item in headings)
                    {
                        strBuilder.Append(item).Append(" ");
                    }
                    body = string.Format(body, strBuilder.ToString());
                    notifier.SendEmail(recipientEmail, body, subject, configDal);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in SendAfterRegistrationNotifications");
                }
            }));
            tr.Start(supplierId);
        }

        public string GetCitiesByCoordinate(Guid supplierId, decimal radius, decimal longitude, decimal latitude)
        {
            /*
              <WorkArea SiteId="" SupplierId="">
                  <Details>
                      <Radius></Radius>
                      <Latitude></Latitude>
                      <Longitude></Longitude>
                  </Details>
              </WorkArea>
             */
            //LogUtils.MyHandle.WriteToLog(8, "GetCitiesByCoordinate: Request- {0}", Request);
            //XmlDocument requestDoc = new XmlDocument();
            //requestDoc.LoadXml(Request);
            //string supplierId = Utils.XmlUtils.GetValue(requestDoc.DocumentElement.Attributes["SupplierId"]);
            //string radius = "";
            //string longitude = "";
            //string latitude = "";
            //if (requestDoc.DocumentElement["Details"] != null)
            //{
            //    radius = Utils.XmlUtils.GetValue(requestDoc.DocumentElement["Details"]["Radius"]);
            //    latitude = Utils.XmlUtils.GetValue(requestDoc.DocumentElement["Details"]["Latitude"]);
            //    longitude = Utils.XmlUtils.GetValue(requestDoc.DocumentElement["Details"]["Longitude"]);
            //}            
            string query = "select new_stageinregistration from account with (nolock) where accountid='{0}'";
            object result;
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                using (SqlCommand comm = new SqlCommand(String.Format(query, supplierId), conn))
                {
                    conn.Open();
                    result = comm.ExecuteScalar();
                }
            }
            StringWriter xmlText = new StringWriter();
            XmlTextWriter xmlFormater = new XmlTextWriter(xmlText);
            xmlFormater.WriteStartElement("Areas");
            xmlFormater.WriteAttributeString("RegistrationStage", result != null ? result.ToString() : "");
            xmlFormater.WriteAttributeString("Longitude", longitude.ToString());
            xmlFormater.WriteAttributeString("Latitude", latitude.ToString());
            xmlFormater.WriteAttributeString("Radius", radius.ToString());
            RegionUtils regionUtil = new RegionUtils();
            string xmlAreas = "";
            if (radius > 0)
            {
                //DataAccessLayer.ConfigurationSettings settingsDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                //string maxLevel = settingsDal.GetConfigurationSettingValue(DML.ConfigurationKeys.MAX_REGION_LEVEL);
                string xmlAreasStr = regionUtil.GetAreasInRadius(longitude, latitude, radius);
                TextReader reader = new StringReader(xmlAreasStr);
                XElement root = XElement.Load(reader);
                foreach (XElement area in root.Elements())
                {
                    xmlAreas += area.ToString();
                }
            }
            else
            {
                xmlAreas = regionUtil.GetCitiesNoRootElement((int)Math.Abs(radius));
            }
            xmlFormater.WriteRaw(xmlAreas);
            xmlFormater.WriteEndElement(); // Closes the Areas tag
            return xmlText.ToString();
        }

        public bool SetCitiesByCoordinate(Guid supplierId, decimal radius, decimal longitude, decimal latitude, string fullAddress, string name, string email, Guid regionId, bool allRegions, bool updateNameAndEmail, string userIdentity, bool isFromAdvRegistration, ref bool invitationApproved)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            bool waitingForInvitation = accountRepositoryDal.IsWaitingForInvitation(supplierId);
            invitationApproved = !waitingForInvitation;
            if (regionId == Guid.Empty && allRegions == false)
            {
                string xmlAreas = GetCitiesByCoordinate(supplierId, radius, longitude, latitude);
                return SetSupplierWorkAreas(supplierId, xmlAreas, fullAddress, name, email, updateNameAndEmail, userIdentity, isFromAdvRegistration);
            }
            else
            {
                SupplierManager supManager = new SupplierManager(XrmDataContext);
                var upsertRequest = new NoProblem.Core.DataModel.SupplierModel.UpsertAdvertiserRequest()
                {
                    SupplierId = supplierId,
                    FullAddress = fullAddress
                };
                if (updateNameAndEmail)
                {
                    upsertRequest.Email = email;
                    upsertRequest.Company = name;
                }
                var upsertAns = supManager.UpsertAdvertiser(upsertRequest);
                if (upsertAns.Status == NoProblem.Core.DataModel.SupplierModel.UpsertAdvertiserStatus.EmailDuplicate)
                {
                    return false;
                }
                string updateQurty = "update account set new_latitude = @latitude, new_longitude = @longitude, new_radiusnew = @radius where accountid = @supplierId";
                using (SqlConnection conn = new SqlConnection(connStr))
                {
                    using (SqlCommand comm = new SqlCommand(updateQurty, conn))
                    {
                        conn.Open();
                        comm.Parameters.AddWithValue("@latitude", latitude);
                        comm.Parameters.AddWithValue("@longitude", longitude);
                        comm.Parameters.AddWithValue("@radius", allRegions ? "ALL" : regionId.ToString());
                        comm.Parameters.AddWithValue("@supplierId", supplierId);
                        comm.ExecuteNonQuery();
                    }
                }
                if (allRegions)
                {
                    DoSupplierWorksInAll(supplierId);
                }
                else
                {
                    DoSupplierWorksInSingleRegion(supplierId, regionId);
                }
                DoAfterLocationSavedUpdates(supplierId, userIdentity, isFromAdvRegistration, email);
                return true;
            }
        }

        private void DoSupplierWorksInAll(Guid supplierId)
        {
            //Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_regionaccountexpertise",
            //                        "new_accountid = '" + supplierId.ToString() + "'");
            string sqlDelete = "update new_regionaccountexpertise set DeletionStateCode = 2 where new_accountid = '" + supplierId.ToString() + "'";
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            regionDal.ExecuteNonQuery(sqlDelete);
            var all1 = regionDal.GetRegionsByLevel(1);
            DataAccessLayer.RegionAccount regAccDal = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
            foreach (var item in all1)
            {
                DataModel.Xrm.new_regionaccountexpertise regAcc = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
                regAcc.new_regionid = item.new_regionid;
                regAcc.new_accountid = supplierId;
                regAcc.new_regionstate = (int)DML.Xrm.new_regionaccountexpertise.RegionState.Working;
                regAccDal.Create(regAcc);
            }
            XrmDataContext.SaveChanges();
        }

        private void DoSupplierWorksInSingleRegion(Guid supplierId, Guid regionId)
        {
            //Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_regionaccountexpertise",
            //                        "new_accountid = '" + supplierId.ToString() + "'");
            string sqlDelete = "update new_regionaccountexpertise set DeletionStateCode = 2 where new_accountid = '" + supplierId.ToString() + "'";
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            regionDal.ExecuteNonQuery(sqlDelete);
            var region = regionDal.Retrieve(regionId);
            DataAccessLayer.RegionAccount regAccDal = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
            if (region.new_level == 1)
            {
                NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise ra = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
                ra.new_accountid = supplierId;
                ra.new_regionid = regionId;
                ra.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.Working;
                regAccDal.Create(ra);
                XrmDataContext.SaveChanges();
                return;
            }

            var brothers = XrmDataContext.new_regions.Where(x => x.new_parentregionid.Value == region.new_parentregionid && x.new_regionid != regionId);

            foreach (var bor in brothers)
            {
                NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise regAcc = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
                regAcc.new_accountid = supplierId;
                regAcc.new_regionid = bor.new_regionid;
                regAcc.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.NotWorking;
                regAccDal.Create(regAcc);
            }

            NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise regAccParent = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
            regAccParent.new_accountid = supplierId;
            regAccParent.new_regionid = region.new_parentregionid.Value;
            if (brothers.Count() > 0)
            {
                regAccParent.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild;
                regAccDal.Create(regAccParent);
            }
            else
            {
                if (region.new_level == 2)
                {
                    regAccParent.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.Working;
                    regAccDal.Create(regAccParent);
                }
            }
            XrmDataContext.SaveChanges();

            if (region.new_level == 2)
            {
                return;
            }
            //given is level 3:
            var parent = XrmDataContext.new_regions.First(x => x.new_regionid == region.new_parentregionid.Value);
            brothers = XrmDataContext.new_regions.Where(x => x.new_parentregionid.Value == parent.new_parentregionid && x.new_regionid != parent.new_regionid);

            foreach (var bor in brothers)
            {
                NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise regAcc = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
                regAcc.new_accountid = supplierId;
                regAcc.new_regionid = bor.new_regionid;
                regAcc.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.NotWorking;
                regAccDal.Create(regAcc);
            }

            NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise regAccSaba = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
            regAccSaba.new_accountid = supplierId;
            regAccSaba.new_regionid = parent.new_parentregionid.Value;
            if (brothers.Count() > 0)
            {
                regAccSaba.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild;
            }
            else
            {
                regAccSaba.new_regionstate = (int)NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.Working;
            }
            regAccDal.Create(regAccSaba);
            XrmDataContext.SaveChanges();
            return;
        }

        public string GetWorkArea(string supplierIdStr)
        {
            Guid supplierId = new Guid(supplierIdStr);
            DataAccessLayer.AccountRepository accountRepositoryAccess = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryAccess.Retrieve(supplierId);
            string longitude = string.IsNullOrEmpty(supplier.new_longitude) == false ? supplier.new_longitude : "0";
            string latitude = string.IsNullOrEmpty(supplier.new_latitude) == false ? supplier.new_latitude : "0";
            string radius = string.IsNullOrEmpty(supplier.new_radiusnew) == false ? supplier.new_radiusnew : "0";
            string fullAddress = string.IsNullOrEmpty(supplier.new_fulladdress) ? string.Empty : supplier.new_fulladdress;
            string email = string.IsNullOrEmpty(supplier.emailaddress1) ? string.Empty : supplier.emailaddress1;
            if (string.IsNullOrEmpty(email))
            {
                DataAccessLayer.Invitation invitationsDal = new NoProblem.Core.DataAccessLayer.Invitation(XrmDataContext);
                var invitation = invitationsDal.FindLinkedInvitation(supplierId);
                if (invitation != null)
                {
                    email = invitation.new_invitedemail;
                }
            }
            XElement selectedAreas = new XElement("SelectedAreas");
            // XElement notSelectedAreas = new XElement("NotSelectedAreas");
            bool isMoreSelected = false;
            if (string.IsNullOrEmpty(fullAddress) == false)
            {
                if (radius == "ALL")
                {
                    isMoreSelected = true;
                    XElement root = new XElement("Area");
                    root.Add(new XAttribute("RegionId", "ALL"));
                    root.Add(new XAttribute("Name", "ALL"));
                    root.Add(new XAttribute("Level", "ALL"));
                    root.Add(new XAttribute("Selected", true));
                    selectedAreas.Add(root);
                    var moreOps = GetMoreMapData(decimal.Parse(longitude), decimal.Parse(latitude));
                    foreach (var item in moreOps)
                    {
                        XElement more = new XElement("Area");
                        more.Add(new XAttribute("RegionId", item.Id));
                        more.Add(new XAttribute("Name", item.Name));
                        more.Add(new XAttribute("Level", item.Level));
                        more.Add(new XAttribute("Selected", false));
                        selectedAreas.Add(more);
                    }
                }
                else if (Utils.GeneralUtils.IsGUID(radius))
                {
                    isMoreSelected = true;
                    DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
                    var region = regionDal.GetRegionMinDataById(new Guid(radius));
                    XElement root = new XElement("Area");
                    root.Add(new XAttribute("RegionId", radius));
                    root.Add(new XAttribute("Name", region.Name));
                    root.Add(new XAttribute("Level", region.Level));
                    root.Add(new XAttribute("Selected", true));
                    selectedAreas.Add(root);
                    var moreOps = GetMoreMapData(decimal.Parse(longitude), decimal.Parse(latitude));
                    var otherMore = moreOps.Where(x => x.Level != region.Level);
                    foreach (var other in otherMore)
                    {
                        XElement more = new XElement("Area");
                        more.Add(new XAttribute("RegionId", other.Id));
                        more.Add(new XAttribute("Name", other.Name));
                        more.Add(new XAttribute("Level", other.Level));
                        more.Add(new XAttribute("Selected", false));
                        selectedAreas.Add(more);
                    }
                }
                /*
            else
            {
                DataAccessLayer.ConfigurationSettings settingsDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string maxLevelStr = settingsDal.GetConfigurationSettingValue(DML.ConfigurationKeys.MAX_REGION_LEVEL);
                int earthRadius = DML.Constants.EARTH_RADIUS_KM;
                //string distanceUnit = settingsDal.GetConfigurationSettingValue(DML.ConfigurationKeys.DISTANCE_UNIT);
                //if (distanceUnit.ToUpper() == "KM")
                //{
                //    earthRadius = DML.Constants.EARTH_RADIUS_KM;
                //}
                RegionUtils regionUtils = new RegionUtils();
                string areasInRadius = regionUtils.GetAreasInRadius(decimal.Parse(longitude), decimal.Parse(latitude), decimal.Parse(radius), earthRadius);
                TextReader reader = new StringReader(areasInRadius);
                XElement areasElement = XElement.Load(reader);
                //DataModel.XrmDataContext.ClearCache("new_regionaccountexpertise");
                //IEnumerable<DML.Xrm.new_regionaccountexpertise> regionAccounts = supplier.new_account_new_regionaccountexpertise;
                DataAccessLayer.RegionAccount regionAccountAccess = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
                var regionAccounts = regionAccountAccess.GetMinDataByAccountId(supplierId);
                //List<DML.Xrm.new_region> connectedRegionsMaxLevel = new List<DML.Xrm.new_region>();
                List<Guid> connectedRegionsMaxLevel = new List<Guid>();
                //DataAccessLayer.Region regionAccess = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
                int maxLevel = int.Parse(maxLevelStr);
                foreach (var regionAcc in regionAccounts)
                {
                    if (regionAcc.RegionState == DML.Xrm.new_regionaccountexpertise.RegionState.NotWorking)
                    {
                        //DML.Xrm.new_region r = regionAccess.Retrieve(regionAcc.RegionId);
                        if (regionAcc.Level == maxLevel)
                        {
                            connectedRegionsMaxLevel.Add(regionAcc.RegionId);
                        }
                    }
                }
                foreach (XElement area in areasElement.Elements())
                {
                    Guid areaId = new Guid(area.Attribute("RegionId").Value);
                    bool found = false;
                    foreach (Guid r in connectedRegionsMaxLevel)
                    {
                        if (r == areaId)
                        {
                            notSelectedAreas.Add(area);
                            found = true;
                            break;
                        }
                    }
                    if (found == false)
                    {
                        selectedAreas.Add(area);
                    }
                }
            }
                 * */
            }
            XElement mainElement = new XElement("Areas",
                new XAttribute("IsMoreSelected", isMoreSelected),
                new XAttribute("RegistrationStage", supplier.new_stageinregistration.HasValue ? supplier.new_stageinregistration.Value.ToString() : ""),
                new XAttribute("Longitude", longitude),
                new XAttribute("Latitude", latitude),
                new XAttribute("Radius", radius),
                new XAttribute("FullAddress", fullAddress),
                new XAttribute("Email", email),
                selectedAreas//,
                // notSelectedAreas
                );
            return mainElement.ToString();
        }

        public List<DataModel.SqlHelper.RegionMinData> GetMoreMapData(decimal longitude, decimal latitude)
        {
            RegionUtils regionUtils = new RegionUtils();
            string areasInRadius = regionUtils.GetAreasInRadius(longitude, latitude, new decimal(0.01));
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(areasInRadius);
            XmlNodeList xRegionslist = xDoc.SelectNodes("Root/Area");
            string idStr = xRegionslist[0].Attributes["RegionId"].InnerText;
            Guid regionId = new Guid(idStr);
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var regData = regionDal.GetRegionMinDataById(regionId);
            var parents = regionDal.GetParents(regData);
            List<DataModel.SqlHelper.RegionMinData> retVal = new List<NoProblem.Core.DataModel.SqlHelper.RegionMinData>();
            foreach (var item in parents)
            {
                if (item.Level == 1 || item.Level == 2 || item.Level == 3)
                {
                    retVal.Add(item);
                    if (item.Level == 3)
                    {
                        item.NumberOfZipcodes = SupplierBL.Registration2014.CoverAreas.MapUtils.GetNumberOfZipcodes(item.Id);
                    }
                }
            }
            return retVal;
        }

        /// <summary>
        /// </summary>
        /// <param name="RegionLevel"></param>
        /// <param name="SupplierId"></param>
        /// <param name="SelectedRecords">a XML formated request filled with the region the supplier has chosen</param>
        /// <returns></returns>
        public string HandleAdvertiserRegions(int RegionLevel, Guid AdvertiserId, string Prefix, string SelectedRegions)
        {
            // <Regions Level="">
            //     <Region ID="" Selected=""/>
            //     <Region ID="" Selected=""/>
            //</Regions>

            StringBuilder builder = new StringBuilder();
            builder.Append("HandleAdvertiserRegions started. supplierId = ").Append(AdvertiserId.ToString()).Append(" prefix = ").Append(Prefix).Append(" regionLevel = ").Append(RegionLevel.ToString()).Append(" selected regions = ").AppendLine(SelectedRegions);
            LogUtils.MyHandle.WriteToLog(builder.ToString());

            if (SelectedRegions != "")
            {
                XmlDocument regions = new XmlDocument();
                regions.LoadXml(SelectedRegions);

                string selectedLevel = regions.DocumentElement.Attributes["Level"].InnerText;
                int level = 0;

                if (selectedLevel != "")
                    level = int.Parse(selectedLevel);

                if (selectedLevel == "1")
                {
                    XmlNodeList regionList = regions.SelectNodes("//Region[@Selected='True']");

                    HandleRegions(AdvertiserId, regionList, level, true);
                }
                else
                {
                    XmlNodeList regionList = regions.SelectNodes("//Region[@Selected='False']");

                    HandleRegions(AdvertiserId, regionList, level, false);
                }
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                accountRepositoryDal.SetSupplierRegistrationStage(AdvertiserId, DML.Xrm.account.StageInRegistration.CITIES);
            }

            //get the regions with the requested level
            string retXml = GetConnectedRegions(AdvertiserId, RegionLevel, Prefix);

            return retXml;
        }

        private void CreateRegionAccountExpertises(Guid supplierId, XmlNodeList regions, HybridDictionary accountRegions, int level)
        {
            DataAccessLayer.SystemUser systemUserAccess = new NoProblem.Core.DataAccessLayer.SystemUser(XrmDataContext);
            DML.Xrm.systemuser currentUser = systemUserAccess.GetCurrentUser();
            DataAccessLayer.RegionAccount regionAccountAccess = new DataAccessLayer.RegionAccount(XrmDataContext);
            DataAccessLayer.Region regionAccess = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            foreach (XmlNode region in regions)
            {
                string regionIdStr = region.Attributes["ID"].InnerText;
                Guid regionId = new Guid(regionIdStr);
                if (accountRegions.Contains(regionIdStr))
                {
                    if (level > 1)
                    {
                        var reAcc = regionAccountAccess.GetIdByRegionIdAndAccountId(regionId, supplierId);
                        //reAcc.new_regionstate = (int)DML.Xrm.new_regionaccountexpertise.RegionState.NotWorking;
                        //regionAccountAccess.Update(reAcc);
                        //Effect.Crm.DB.DataBaseUtils.DirectUpdateInstance("new_regionaccountexpertise",
                        //    reAcc.Id,
                        //    new string[] { "new_regionstate" },
                        //    new object[] { (int)DML.Xrm.new_regionaccountexpertise.RegionState.NotWorking });
                        string sqlUpdate = "update new_regionaccountexpertise set new_regionstate = @state where new_regionaccountexpertiseid = @id";
                        DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
                        parameters.Add("@id", reAcc.Id);
                        parameters.Add("@state", (int)DML.Xrm.new_regionaccountexpertise.RegionState.NotWorking);
                        regionAccess.ExecuteNonQuery(sqlUpdate, parameters);
                        var childrenIds = regionAccess.GetChildrenIds(regionId);
                        foreach (var childId in childrenIds)
                        {
                            var childReAcc = regionAccountAccess.GetIdByRegionIdAndAccountId(childId, supplierId);
                            if (childReAcc != null)
                            {
                                //Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_regionaccountexpertise",
                                //    "new_regionaccountexpertiseid = '" + childReAcc.Id.ToString() + "'");
                                string sqlDelete = "update new_regionaccountexpertise set DeletionStateCode = 2 where new_regionaccountexpertiseid = '" + childReAcc.Id.ToString() + "'";
                                regionAccess.ExecuteNonQuery(sqlDelete);
                            }
                        }
                    }
                    accountRegions.Remove(regionIdStr);
                }
                else
                {
                    //DML.Xrm.new_regionaccountexpertise regionAccount = new DML.Xrm.new_regionaccountexpertise();
                    //regionAccount.new_accountid = supplierId;
                    //regionAccount.new_regionid = new Guid(regionIdStr);
                    DML.Xrm.new_regionaccountexpertise.RegionState state;
                    if (level == 1)
                    {
                        state = DML.Xrm.new_regionaccountexpertise.RegionState.Working;
                    }
                    else
                    {
                        state = DML.Xrm.new_regionaccountexpertise.RegionState.NotWorking;
                    }

                    //regionAccountAccess.Create(regionAccount);
                    Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity dynamicRegAcc = MakeDynamicRegionAccount(state, regionId, supplierId);
                    regionAccountAccess.AddRecordToCRM(dynamicRegAcc, currentUser);
                }
            }
            //XrmDataContext.SaveChanges();
        }

        private Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity MakeDynamicRegionAccount(DML.Xrm.new_regionaccountexpertise.RegionState state, Guid regionId, Guid supplierId)
        {
            Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity dynamicRegAcc =
                Effect.Crm.Convertor.ConvertorCRMParams.GetNewDynamicEntity("new_regionaccountexpertise");
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_regionstate",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmPicklist((int)state),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.PicklistProperty));
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_accountid",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("account", supplierId),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                );
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_regionid",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("new_region", regionId),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                );
            return dynamicRegAcc;
        }

        //private void HandleRegions(Guid advertiserId, IEnumerable<DML.SqlHelper.RegionMinData> regions, int level, bool selected)
        //{
        //    // <Regions Level="">
        //    //     <Region ID="" Selected=""/>
        //    //     <Region ID="" Selected=""/>
        //    //</Regions>
        //    XElement element = new XElement("Regions");
        //    foreach (var region in regions)
        //    {
        //        XElement regElement = new XElement("Region",
        //            new XAttribute("ID", region.Id.ToString()));
        //        element.Add(regElement);
        //    }
        //    XmlDocument doc = new XmlDocument();
        //    doc.LoadXml(element.ToString());
        //    XmlNodeList nodeList = doc.SelectNodes("//Region");
        //    HandleRegions(advertiserId, nodeList, level, selected);
        //}

        private void HandleRegions(Guid advertiserId, IEnumerable<Guid> regionsId, int level, bool selected)
        {
            XElement element = new XElement("Regions");
            foreach (Guid id in regionsId)
            {
                XElement regElement = new XElement("Region",
                    new XAttribute("ID", id.ToString()));
                element.Add(regElement);
            }
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(element.ToString());
            XmlNodeList nodeList = doc.SelectNodes("//Region");
            HandleRegions(advertiserId, nodeList, level, selected);
        }

        private void HandleRegions(Guid advertiserId, XmlNodeList regions, int level, bool selected)
        {
            string getAccountRegionsQuery =
   @"SELECT new_region.new_regionid,new_region.new_parentregionid 
FROM new_regionaccountexpertise with (nolock)
INNER JOIN new_region with (nolock) ON new_region.new_regionid = new_regionaccountexpertise.new_regionid AND new_region.new_level = @level
WHERE new_accountid = @accountId AND new_regionaccountexpertise.deletionstatecode = 0";

            HybridDictionary accountRegions = new HybridDictionary();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(getAccountRegionsQuery, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@accountId", advertiserId);
                    command.Parameters.AddWithValue("@level", level);
                    using (SqlDataReader getAccountRegions = command.ExecuteReader())
                    {
                        while (getAccountRegions.Read())
                        {
                            // Add the region to the list
                            DML.RegionMap region = new DML.RegionMap();
                            region.Handled = false;
                            region.RegionId = (Guid)getAccountRegions["new_regionid"];
                            if (getAccountRegions["new_parentregionid"] != DBNull.Value)
                            {
                                region.ParentRegionId = (Guid)getAccountRegions["new_parentregionid"];
                            }
                            accountRegions.Add(getAccountRegions["new_regionid"].ToString().ToLower(), region);
                        }
                    }
                }
            }
            CreateRegionAccountExpertises(advertiserId, regions, accountRegions, level);

            DataAccessLayer.Region regionAccess = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.RegionAccount regionAccountAccess = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
            // DataAccessLayer.Account accountAccess = new NoProblem.Core.DataAccessLayer.Account(XrmDataContext);

            if (level > 1)
            {
                RegionUtils regionUtils = new RegionUtils();
                foreach (string regionIdStr in accountRegions.Keys)
                {
                    Guid regionId = new Guid(regionIdStr);
                    var reAcc = regionAccountAccess.GetIdByRegionIdAndAccountId(regionId, advertiserId);
                    if (reAcc.RegionState == DML.Xrm.new_regionaccountexpertise.RegionState.NotWorking)
                    {
                        DeleteRegionAccountRecursive(reAcc, regionAccess, regionAccountAccess);
                    }
                }
            }
            DataAccessLayer.SystemUser systemUserAccess = new NoProblem.Core.DataAccessLayer.SystemUser(XrmDataContext);
            DML.Xrm.systemuser currentUser = systemUserAccess.GetCurrentUser();
            //DML.Xrm.account accountXrm = accountAccess.Retrieve(advertiserId);
            int currentLevel = level;
            while (currentLevel > 1)
            {
                List<Guid> parentRegionIds = new List<Guid>();
                var parentsAccountExpertise = regionAccountAccess.GetByAccountIdAndLevel(advertiserId, currentLevel);
                foreach (var reAcc in parentsAccountExpertise)
                {
                    //var reg = regionAccess.Retrieve(reAcc.RegionId);
                    Guid parentId = regionAccess.GetParentId(reAcc.RegionId);
                    if (parentId != Guid.Empty && !parentRegionIds.Contains(parentId))
                    {
                        parentRegionIds.Add(parentId);
                    }
                }
                foreach (Guid parentId in parentRegionIds)
                {
                    var regAcc = regionAccountAccess.GetIdByRegionIdAndAccountId(parentId, advertiserId);
                    if (regAcc == null)
                    {
                        //regAcc = new DML.Xrm.new_regionaccountexpertise();
                        //regAcc.new_regionstate = (int)DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild;
                        //regAcc.new_accountid = advertiserId;
                        //regAcc.new_regionid = parentId;
                        var dynamicRegAcc = MakeDynamicRegionAccount(DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild, parentId, advertiserId);
                        //regionAccountAccess.Create(regAcc);
                        regionAccountAccess.AddRecordToCRM(dynamicRegAcc, currentUser);
                    }
                    else
                    {
                        //regAcc.new_regionstate = (int)DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild;
                        //regionAccountAccess.Update(regAcc);
                        //Effect.Crm.DB.DataBaseUtils.DirectUpdateInstance("new_regionaccountexpertise",
                        //    regAcc.Id,
                        //    new string[] { "new_regionstate" },
                        //    new object[] { (int)DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild });

                        string sqlDirectUpdate = "update new_regionaccountexpertise set new_regionstate = @state where new_regionaccountexpertiseid = @id";
                        DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
                        parameters.Add("@id", regAcc.Id);
                        parameters.Add("@state", (int)DML.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild);
                        regionAccess.ExecuteNonQuery(sqlDirectUpdate, parameters);
                    }
                }
                //XrmDataContext.SaveChanges();
                currentLevel--;
            }
            int tempLevel = level;
            while (tempLevel > 1)
            {
                //IEnumerable<DML.Xrm.new_regionaccountexpertise> regAccsParents = regionAccountAccess.GetByAccountIdAndLevel(advertiserId, tempLevel - 1);
                var regAccsParents = regionAccountAccess.GetByAccountIdAndLevel(advertiserId, tempLevel - 1);
                //IEnumerable<DML.Xrm.new_regionaccountexpertise> regAccsThisLevel = regionAccountAccess.GetByAccountIdAndLevel(advertiserId, tempLevel);
                var regAccsThisLevel = regionAccountAccess.GetByAccountIdAndLevel(advertiserId, tempLevel);
                foreach (var regAcc in regAccsParents)
                {
                    if (regAcc.RegionState == DML.Xrm.new_regionaccountexpertise.RegionState.NotWorking)
                    {
                        continue;
                    }
                    //DML.Xrm.new_region current = regionAccess.Retrieve(regAcc.new_regionid.Value);
                    //IEnumerable<DML.Xrm.new_region> children = current.new_new_region_new_region;
                    var childrenIds = regionAccess.GetChildrenIds(regAcc.RegionId);
                    int childCount = childrenIds.Count();
                    if (childCount != 0)
                    {
                        foreach (Guid childId in childrenIds)
                        {
                            var rea = regAccsThisLevel.SingleOrDefault(x => x.RegionId == childId);
                            if (rea == null)
                            {
                                childCount--;
                            }
                        }
                        if (childCount == 0)
                        {
                            if (tempLevel == 2)
                            {
                                //regAcc.new_regionstate = (int)DML.Xrm.new_regionaccountexpertise.RegionState.Working;
                                //regionAccountAccess.Update(regAcc);
                                //Effect.Crm.DB.DataBaseUtils.DirectUpdateInstance("new_regionaccountexpertise",
                                //    regAcc.Id,
                                //    new string[] { "new_regionstate" },
                                //    new object[] { (int)DML.Xrm.new_regionaccountexpertise.RegionState.Working });
                                string sqlDirectUpdate = "update new_regionaccountexpertise set new_regionstate = @state where new_regionaccountexpertiseid = @id";
                                DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
                                parameters.Add("@id", regAcc.Id);
                                parameters.Add("@state", (int)DML.Xrm.new_regionaccountexpertise.RegionState.Working);
                                regionAccess.ExecuteNonQuery(sqlDirectUpdate, parameters);
                            }
                            else
                            {
                                //regionAccountAccess.Delete(regAcc);
                                //Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_regionaccountexpertise",
                                //    "new_regionaccountexpertiseid = '" + regAcc.Id.ToString() + "'");
                                regionAccess.ExecuteNonQuery("update new_regionaccountexpertise set DeletionStateCode = 2 where new_regionaccountexpertiseid = '" + regAcc.Id.ToString() + "'");
                            }
                        }
                    }
                }
                //XrmDataContext.SaveChanges();
                tempLevel--;
            }
        }

        private void DeleteRegionAccountRecursive(DML.Regions.RegionAccountMinData regionAccount, DataAccessLayer.Region regionAccess, DataAccessLayer.RegionAccount regionAccountAccess)
        {
            List<Guid> toDelete = new List<Guid>();
            DeleteRegionAccountRecursive(regionAccount, regionAccess, regionAccountAccess, toDelete);
            if (toDelete.Count > 0)
            {
                StringBuilder where = new StringBuilder("new_regionaccountexpertiseid in ( ");
                int count = toDelete.Count;
                for (int i = 0; i < count; i++)
                {
                    where.Append("'").Append(toDelete[i].ToString()).Append("'");
                    if (i < count - 1)
                    {
                        where.Append(",");
                    }
                }
                where.Append(" ) ");
                //Effect.Crm.DB.DataBaseUtils.DeleteEntities("new_regionaccountexpertise", where.ToString());
                regionAccess.ExecuteNonQuery("update new_regionaccountexpertise set DeletionStateCode = 2 where " + where.ToString());
            }
        }

        private void DeleteRegionAccountRecursive(DML.Regions.RegionAccountMinData regionAccount, DataAccessLayer.Region regionAccess, DataAccessLayer.RegionAccount regionAccountAccess, List<Guid> toDelete)
        {
            //if (regionAccount == null)
            //{
            //    return;
            //}
            Guid regionId = regionAccount.RegionId;
            Guid accountId = regionAccount.SupplierId;
            IEnumerable<Guid> children = regionAccess.GetChildrenIds(regionId);
            foreach (Guid child in children)
            {
                var reAcc = regionAccountAccess.GetIdByRegionIdAndAccountId(child, accountId);
                if (reAcc != null)
                {
                    toDelete.Add(reAcc.Id);
                    DeleteRegionAccountRecursive(reAcc, regionAccess, regionAccountAccess, toDelete);
                }
            }
            //regionAccountAccess.Delete(regionAccount);
            //XrmDataContext.SaveChanges();            
        }

        public string GetConnectedRegions(Guid AdvertiserId, int Level, string Prefix)
        {
            string getRegionsQuery = @"declare @SupplierId uniqueidentifier
declare @Level int
declare @Prefix nvarchar(100)
declare @table table (RegionLevel int,RegionId uniqueidentifier,ParentRegionId uniqueidentifier,RegionState int)

set @SupplierId = '{0}'
set @Level = {1}
set @Prefix = N'{2}%';

WITH HirarchyRegions(RegionLevel,RegionId,ParentRegionId)
AS
(
	SELECT new_region.new_level,new_region.new_regionid,new_parentregionid
	FROM new_region with(nolock)
	INNER JOIN new_regionaccountexpertise rae ON rae.new_regionid = new_region.new_regionid AND 
		new_accountid = @SupplierId AND rae.deletionstatecode = 0 AND new_parentregionid IS NULL
	WHERE new_region.new_parentregionid IS NULL AND new_region.deletionstatecode = 0
	UNION ALL
	SELECT new_region.new_level,new_region.new_regionid,new_parentregionid
	FROM new_region with(nolock)
	INNER JOIN HirarchyRegions ON HirarchyRegions.RegionId = new_parentregionid	
)

insert into @table 
select HirarchyRegions.* ,rae.new_regionstate
from HirarchyRegions
LEFT JOIN new_regionaccountexpertise rae ON rae.new_regionid = HirarchyRegions.RegionId 
AND new_accountid = @SupplierId AND rae.deletionstatecode = 0;


WITH SelectiveRegions(RegionLevel,RegionId,ParentRegionId,RegionState)
AS
(
	select *
	from  @table
	WHERE ParentRegionId IS NULL
	UNION ALL
	select allRegions.*
	from  @table allRegions
	INNER JOIN SelectiveRegions ON SelectiveRegions.RegionId = allRegions.ParentRegionId AND 
		(allRegions.RegionState != 2 OR allRegions.RegionState IS NULL OR (allRegions.RegionState = 2 AND allRegions.RegionLevel =@Level))
)

SELECT new_region.new_regionid,new_region.new_name,
CASE 
WHEN rae.new_regionstate = 2 THEN 'False'
ELSE 'True'
END AS ConnectedToAdvertiser,
CASE 
WHEN children.ChildCount IS NULL THEN 0
ELSE 1
END AS HasChildRegions,
parent.new_name AS ParentName
FROM SelectiveRegions
INNER JOIN new_region ON new_region.new_regionid = RegionId
LEFT JOIN (SELECT new_parentregionid,COUNT(*) AS ChildCount FROM new_region GROUP BY new_parentregionid) children ON
   children.new_parentregionid = new_region.new_regionid
LEFT JOIN new_regionaccountexpertise rae ON rae.new_regionid = SelectiveRegions.RegionId AND new_accountid = @SupplierId AND rae.deletionstatecode = 0
LEFT JOIN new_region parent ON parent.new_regionid = new_region.new_parentregionid
WHERE RegionLevel = @Level AND new_region.new_name LIKE @Prefix AND new_region.deletionstatecode = 0
UNION ALL
SELECT new_region.new_regionid, new_region.new_name,'False' AS ConnectedToAdvertiser,
CASE 
WHEN children.ChildCount IS NULL THEN 0
ELSE 1
END AS HasChildRegions,parent.new_name AS ParentName
FROM new_region
LEFT JOIN new_regionaccountexpertise ON new_region.new_regionid = new_regionaccountexpertise.new_regionid AND
	new_regionaccountexpertise.new_accountid = @SupplierId AND new_regionaccountexpertise.deletionstatecode = 0
LEFT JOIN (SELECT new_parentregionid,COUNT(*) AS ChildCount FROM new_region GROUP BY new_parentregionid) children ON
   children.new_parentregionid = new_region.new_regionid
LEFT JOIN new_region parent ON parent.new_regionid = new_region.new_parentregionid
WHERE new_region.deletionstatecode = 0 AND  new_region.new_level = @Level AND new_region.new_parentregionid IS NULL AND new_regionaccountexpertise.new_regionaccountexpertiseid IS NULL AND new_region.new_name LIKE @Prefix
ORDER BY ParentName,new_name";

            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            int stage = accountRepositoryDal.GetStageInRegistration(AdvertiserId);

            StringWriter xmlText = new StringWriter();
            XmlTextWriter xmlFormater = new XmlTextWriter(xmlText);

            xmlFormater.WriteStartElement("Regions");
            xmlFormater.WriteAttributeString("RegistrationStage", stage.ToString());
            xmlFormater.WriteAttributeString("Status", "Success");

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string getRegionsQueryReady = string.Format(getRegionsQuery, AdvertiserId, Level, Prefix);
                using (SqlCommand com = new SqlCommand(getRegionsQueryReady, connection))
                {
                    com.Connection.Open();
                    com.CommandTimeout = 300000;
                    using (SqlDataReader getRegions = com.ExecuteReader())
                    {
                        while (getRegions.Read())
                        {
                            xmlFormater.WriteStartElement("Region");
                            xmlFormater.WriteAttributeString("ID", getRegions["new_regionid"].ToString());
                            xmlFormater.WriteAttributeString("Name", getRegions["new_name"].ToString());
                            xmlFormater.WriteAttributeString("ConnectedToAdvertiser", getRegions["ConnectedToAdvertiser"].ToString());
                            xmlFormater.WriteAttributeString("HasChildRegions", getRegions["HasChildRegions"].ToString());
                            xmlFormater.WriteAttributeString("ParentName", getRegions["ParentName"].ToString());
                            xmlFormater.WriteEndElement();
                        }
                    }
                }
            }
            xmlFormater.WriteEndElement();
            return xmlText.ToString();
        }

        public DML.ExpertiseAndRegionsContainer GetRelationIfExist(List<DML.ExpertiseAndRegionsContainer> Relations, Guid RegionId)
        {
            foreach (DML.ExpertiseAndRegionsContainer aec in Relations)
            {
                if (aec.RegionId == RegionId)
                    return aec;
            }
            return null;
        }

        public void SetUnCompletedExpertiseAndRegionsAsInActive(List<DML.ExpertiseAndRegionsContainer> Relations)
        {
            EntityUtils entityUtils = new EntityUtils(CrmService);
            foreach (DML.ExpertiseAndRegionsContainer aec in Relations)
            {
                if (aec.IsCompleted == false)
                {
                    entityUtils.SetEntityState("new_regionaccountexpertise", aec.RelationId, "Inactive");
                }
            }
        }

        public string GetAllTimeZones()
        {
            string selectQuery = "select TimeZoneCode,UserInterfaceName from TimeZoneDefinition with (nolock) order by TimeZoneCode";
            StringWriter sWriter = new StringWriter();
            XmlTextWriter xmlWriter = new XmlTextWriter(sWriter);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(selectQuery, connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        xmlWriter.WriteStartElement("TimeZones");
                        while (reader.Read())
                        {
                            xmlWriter.WriteStartElement("TimeZone");
                            xmlWriter.WriteAttributeString("TimeZoneCode", reader["TimeZoneCode"].ToString());
                            xmlWriter.WriteAttributeString("TimeZoneName", reader["UserInterfaceName"].ToString());
                            xmlWriter.WriteEndElement();
                        }
                        xmlWriter.WriteEndElement();
                    }
                }
            }
            return sWriter.ToString();
        }

        public string GetUserTimeZone(Guid userId)
        {
            string timeZoneCode = "";
            string getTimeZoneQuery = "SELECT address1_utcoffset FROM account with (nolock) WHERE accountid = '{0}'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(string.Format(getTimeZoneQuery, userId), connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader getTimeZone = command.ExecuteReader())
                    {
                        if (getTimeZone.Read())
                        {
                            timeZoneCode = getTimeZone["address1_utcoffset"].ToString();
                        }
                    }
                }
            }
            return string.Format("<GetUserTimeZone><TimeZoneCode>{0}</TimeZoneCode></GetUserTimeZone>", timeZoneCode);
        }

    }
}
