﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic.SupplierBL
{
    public class CallRecordingsManager : XrmUserBase
    {
        public CallRecordingsManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<DataModel.SupplierModel.Recording> GetRecordingsForLead(Guid supplierId, Guid incidentAccountId)
        {
            List<DataModel.SupplierModel.Recording> retVal = new List<DataModel.SupplierModel.Recording>();

            DataAccessLayer.IncidentAccount dal = new DataAccessLayer.IncidentAccount(XrmDataContext);
            var query = from ia in dal.All
                        where ia.new_incidentaccountid == incidentAccountId
                        && ia.new_accountid == supplierId
                        select new { MainRecordingUrl = ia.new_recordfilelocation, CreatedOn = ia.createdon.Value, CustomerDissallowedRecording = ia.new_customerdisallowedrecording };
            var found = query.FirstOrDefault();
            if (found != null)
            {
                DataAccessLayer.CallRecording recordingsDal = new DataAccessLayer.CallRecording(XrmDataContext);
                var recordingsQuery = from rec in recordingsDal.All
                                      where rec.new_incidentaccountid == incidentAccountId
                                      select new { Url = rec.new_url, CreatedOn = rec.createdon.Value }
                                      ;
                var list = recordingsQuery.ToList();
                foreach (var rec in list.OrderByDescending(x => x.CreatedOn))
                {
                    retVal.Add(
                        new DataModel.SupplierModel.Recording()
                        {
                            Url = rec.Url,
                            CreatedOn = rec.CreatedOn
                        }
                        );
                }
                if (!String.IsNullOrEmpty(found.MainRecordingUrl) && !found.CustomerDissallowedRecording.IsTrue())
                {
                    retVal.Add(new DataModel.SupplierModel.Recording() { CreatedOn = found.CreatedOn, Url = found.MainRecordingUrl });
                }
            }
            return retVal;
        }

        public string GetFilesLocation(Guid supplierId, Guid incidentAccountId)
        {
            return @"\\c70415wcfil01fs\Web\TTSfiles\QA2";
        }

        public bool PostNewCallRecording(Guid supplierId, Guid incidentAccountId, string file)
        {
            return true;
        }
    }
}
