﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.TwilioBL;

namespace NoProblem.Core.BusinessLogic.SupplierBL.ExpiredIvr
{
    public class ExpiredIvrCallHandler : TwilioHandlerBase
    {
        public ExpiredIvrCallHandler(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        internal void DoExpiredCall(Guid supplierId)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = accountRepositoryDal.Retrieve(supplierId);
            if (supplier.new_status != (int)DataModel.Xrm.account.SupplierStatus.Trial
                || supplier.new_trialstatusreason != (int)DataModel.Xrm.account.TrialStatusReasonCode.Expired
                || supplier.new_expiredcallscount >= 4)
            {
                return;
            }
            supplier.new_expiredcallscount = supplier.new_expiredcallscount.HasValue ? supplier.new_expiredcallscount.Value + 1 : 1;
            accountRepositoryDal.Update(supplier);
            DataModel.Xrm.new_expiredcall call = new NoProblem.Core.DataModel.Xrm.new_expiredcall();
            call.new_accountid = supplierId;
            DataAccessLayer.ExpiredCall dal = new NoProblem.Core.DataAccessLayer.ExpiredCall(XrmDataContext);
            dal.Create(call);
            XrmDataContext.SaveChanges();
            TwilioBL.TwilioCallsExecuter executer = new TwilioCallsExecuter(XrmDataContext);
            string callStatus;
            string callSid = executer.InitiateCall(supplier.telephone1, "callId", call.new_expiredcallid.ToString(), "ExpiredCallAnswered", "ExpiredCallCallback", null, false, true, null, out callStatus);
            if (string.IsNullOrEmpty(callSid) || callStatus == "failed")
            {
                ExpiredIvr.ExpiredIvrManager manager = new ExpiredIvrManager(XrmDataContext);
                DateTime? next = manager.CalculateTimeForExpiredCall(supplier);
                supplier.new_nextexpiredcallon = next;
                accountRepositoryDal.Update(supplier);
            }
            if (!string.IsNullOrEmpty(callSid))
            {
                call.new_callsid = callSid;
                call.new_callstatus = callStatus;
                dal.Update(call);
            }
            XrmDataContext.SaveChanges();
        }

        public string ExpiredCallAnswered(string callIdStr, string callStatus, bool again)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            Guid callId = new Guid(callIdStr);
            if (!again)
            {
                DataAccessLayer.ExpiredCall dal = new NoProblem.Core.DataAccessLayer.ExpiredCall(XrmDataContext);
                DataModel.Xrm.new_expiredcall call = new NoProblem.Core.DataModel.Xrm.new_expiredcall(XrmDataContext);
                call.new_expiredcallid = callId;
                call.new_callstart = DateTime.Now;
                call.new_callstatus = callStatus;
                dal.Update(call);
                XrmDataContext.SaveChanges();
            }
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string audioFilesUrlBase = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
            response.BeginGather(new
            {
                action = twilioCallBackUrlBase + "/ExpiredCallClickHandler?callId=" + callIdStr + "&SiteId=" + siteId,
                timeout = 5,
                method = "GET",
                numDigits = 1,
                finishOnKey = string.Empty
            });
            response.Play(audioFilesUrlBase + "/expiredcall/32.mp3");
            response.Play(audioFilesUrlBase + "/expiredcall/33.mp3");
            response.EndGather();
            response.Redirect(twilioCallBackUrlBase + "/ExpiredCallClickHandler?callId=" + callIdStr + "&SiteId=" + siteId + "&Digits=NOTHING", "GET");
            return response.ToString();
        }

        public string ExpiredCallClickHandler(string callIdStr, string digits)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            DataAccessLayer.ExpiredCall dal = new NoProblem.Core.DataAccessLayer.ExpiredCall(XrmDataContext);
            var call = dal.Retrieve(new Guid(callIdStr));
            call.new_dtmf = digits;
            dal.Update(call);
            XrmDataContext.SaveChanges();
            if (digits == "3")
            {
                DataModel.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
                supplier.accountid = call.new_accountid.Value;
                supplier.new_trialstatusreason = (int)DataModel.Xrm.account.TrialStatusReasonCode.RemoveMe;
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                accountRepositoryDal.Update(supplier);
                XrmDataContext.SaveChanges();
                response.Hangup();
            }
            else if (digits == "9")
            {
                DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string phoneToCall = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.CALL_CENTER_PHONE);
                response.Dial(phoneToCall,
                        new
                        {
                            record = "true",
                            action = twilioCallBackUrlBase + "/ExpiredRepresentativeCallback?callId=" + callIdStr + "&SiteId=" + siteId,
                            method = "GET",
                            timeout = 30
                        });
            }
            else if (digits == "0")
            {
                response.Redirect(twilioCallBackUrlBase + "/ExpiredCallAnswered?callId=" + callIdStr + "&SiteId=" + siteId + "&again=true", "GET");
            }
            else
            {
                response.Hangup();
            }

            return response.ToString();
        }

        public void ExpiredCallCallback(string callStatus, string callIdStr)
        {
            DataAccessLayer.ExpiredCall dal = new NoProblem.Core.DataAccessLayer.ExpiredCall(XrmDataContext);
            var call = dal.Retrieve(new Guid(callIdStr));
            call.new_callstatus = callStatus;
            call.new_callend = DateTime.Now;
            dal.Update(call);
            XrmDataContext.SaveChanges();
            if (call.new_dtmf != "3")
            {
                var supplier = call.new_account_expiredcall;
                ExpiredIvrManager manager = new ExpiredIvrManager(XrmDataContext);
                DateTime? d = manager.CalculateTimeForExpiredCall(supplier);
                supplier.new_nextexpiredcallon = d;
                DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                accountRepositoryDal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
        }

        public void ExpiredRepresentativeCallback(string recording, string callIdStr)
        {
            if (!string.IsNullOrEmpty(recording))
            {
                DataAccessLayer.ExpiredCall dal = new NoProblem.Core.DataAccessLayer.ExpiredCall(XrmDataContext);
                DataModel.Xrm.new_expiredcall call = new NoProblem.Core.DataModel.Xrm.new_expiredcall(XrmDataContext);
                call.new_expiredcallid = new Guid(callIdStr);
                call.new_representativerecording = recording;
                dal.Update(call);
                XrmDataContext.SaveChanges();
            }
        }
    }
}
