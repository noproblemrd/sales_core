﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.SupplierBL.ExpiredIvr
{
    public class ExpiredIvrManager : XrmUserBase
    {
        public ExpiredIvrManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private static SynchronizedCollection<TimerConatiner> timersHolder = new SynchronizedCollection<TimerConatiner>();

        internal DateTime? CalculateTimeForExpiredCall(NoProblem.Core.DataModel.Xrm.account supplier)
        {
            return CalculateTimeForExpiredCall(supplier, DateTime.Now);
        }

        internal DateTime? CalculateTimeForExpiredCall(NoProblem.Core.DataModel.Xrm.account supplier, DateTime lastCallDate)
        {
            return CalculateTimeForExpiredCall(supplier, lastCallDate, 0);
        }

        private DateTime? CalculateTimeForExpiredCall(NoProblem.Core.DataModel.Xrm.account supplier, DateTime lastCallDate, int tries)
        {
            if (tries > 4)
            {
                return null;
            }
            DateTime? retVal;
            if (supplier.new_expiredcallscount.HasValue && supplier.new_expiredcallscount.Value > 0)
            {
                if (supplier.new_expiredcallscount.Value >= 4)
                {
                    return null;
                }
                retVal = lastCallDate.AddDays(7);
            }
            else
            {
                int timeZone;
                if (supplier.address1_utcoffset.HasValue)
                {
                    timeZone = supplier.address1_utcoffset.Value;
                }
                else
                {
                    DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                    timeZone = int.Parse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PUBLISHER_TIME_ZONE));
                }

                DateTime current = FixDateToLocal(DateTime.Now, timeZone);
                int count = 0;
                bool found = false;
                while (count < 6 && !found)
                {
                    count++;
                    current = current.AddDays(1);
                    int to;
                    int from;
                    GetFromToHours(supplier, current, out to, out from);
                    if (current.Hour >= from && current.Hour <= to)
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    current = lastCallDate.AddDays(7);
                }
                else
                {
                    current = lastCallDate.AddDays(count);
                }
                retVal = current;
            }
            BusinessClosures.ClosuresManager closures = new NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager(XrmDataContext);
            bool isClosure = closures.IsTimeClosure(NoProblem.Core.BusinessLogic.BusinessClosures.ClosuresManager.ClosureOptions.ClosureDatesOnly, retVal.Value);
            if (isClosure)
            {
                retVal = CalculateTimeForExpiredCall(supplier, retVal.Value, tries + 1);
            }
            if (retVal.HasValue)
            {
                SetTimerForCall(retVal.Value, supplier.accountid);
            }
            return retVal;
        }

        private void SetTimerForCall(DateTime nextCallTime, Guid supplierId)
        {
            TimeSpan span = nextCallTime - DateTime.Now;
            TimerConatiner container = new TimerConatiner();
            Timer timer = new Timer(
                ExpiredIvrManager.TimerCallback,
                container,
                span,
                new TimeSpan(0, 0, 0, 0, -1));
            container.Timer = timer;
            container.SupplierId = supplierId;
            timersHolder.Add(container);
        }

        private void GetFromToHours(NoProblem.Core.DataModel.Xrm.account supplier, DateTime d, out int to, out int from)
        {
            string fromStr = string.Empty;
            string toStr = string.Empty;
            switch (d.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    fromStr = supplier.new_sundayavailablefrom;
                    toStr = supplier.new_sundayavailableto;
                    break;
                case DayOfWeek.Monday:
                    fromStr = supplier.new_mondayavailablefrom;
                    toStr = supplier.new_mondayavailableto;
                    break;
                case DayOfWeek.Tuesday:
                    fromStr = supplier.new_tuesdayavailablefrom;
                    toStr = supplier.new_tuesdayavailableto;
                    break;
                case DayOfWeek.Wednesday:
                    fromStr = supplier.new_wednesdayavailablefrom;
                    toStr = supplier.new_wednesdayavailableto;
                    break;
                case DayOfWeek.Thursday:
                    fromStr = supplier.new_thursdayavailablefrom;
                    toStr = supplier.new_thursdayavailableto;
                    break;
                case DayOfWeek.Friday:
                    fromStr = supplier.new_fridayavailablefrom;
                    toStr = supplier.new_fridayavailableto;
                    break;
                case DayOfWeek.Saturday:
                    fromStr = supplier.new_saturdayavailablefrom;
                    toStr = supplier.new_saturdayavailableto;
                    break;
            }
            if (fromStr.Length < 5)
            {
                from = -1;
                to = -1;
            }
            else
            {
                if (toStr.StartsWith("23:59"))
                {
                    to = 24;
                }
                else
                {
                    to = int.Parse(toStr.Split(':')[0]);
                }
                from = int.Parse(fromStr.Split(':')[0]);
            }
        }

        private static void TimerCallback(object obj)
        {
            try
            {
                TimerConatiner container = (TimerConatiner)obj;
                timersHolder.Remove(container);
                ExpiredIvrCallHandler manager = new ExpiredIvrCallHandler(null);
                manager.DoExpiredCall(container.SupplierId);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ExpiredIvrManager.TimerCallback");
            }
        }

        private class TimerConatiner
        {
            public Timer Timer { get; set; }
            public Guid SupplierId { get; set; }
        }

        public void SetTimersOnAppStart()
        {
            string query =
                @"select new_nextexpiredcallon, accountid from account with(nolock)
                where deletionstatecode = 0 and new_nextexpiredcallon > @now and (new_expiredcallscount < 4 or new_expiredcallscount is null)
                and new_status = @trial and new_trialstatusreason = @expired";
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@now", DateTime.Now));
            parameters.Add(new KeyValuePair<string, object>("@trial", (int)DataModel.Xrm.account.SupplierStatus.Trial));
            parameters.Add(new KeyValuePair<string, object>("@expired", (int)DataModel.Xrm.account.TrialStatusReasonCode.Expired));
            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                try
                {
                    Guid supplierId = (Guid)row["accountid"];
                    DateTime dateTime = (DateTime)row["new_nextexpiredcallon"];
                    TimerConatiner container = new TimerConatiner();
                    container.SupplierId = supplierId;
                    Timer timer = new Timer(
                        ExpiredIvrManager.TimerCallback,
                        container,
                        dateTime - DateTime.Now,
                        new TimeSpan(0, 0, 0, 0, -1));
                    container.Timer = timer;
                    timersHolder.Add(container);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception in ExpiredIvrManager.SetTimersOnAppStart in the foreach loop. Advancing to next supplier");
                }
            }
        }
    }
}
