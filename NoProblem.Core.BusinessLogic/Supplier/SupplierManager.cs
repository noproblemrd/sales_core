﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using Effect.Crm.Convertor;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils;
using NoProblem.Core.DataModel.SupplierModel;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic
{
	public class SupplierManager : XrmUserBase
	{
		#region Ctor
		public SupplierManager(DataModel.Xrm.DataContext xrmDataContext)
			: base(xrmDataContext)
		{

		}
		#endregion

		private readonly string connectionString = ConfigurationManager.AppSettings["updateConnectionString"];

		#region Public Methods

		#region Suppliers by API

		public NoProblem.Core.DataModel.Result<Guid> UpsertAdvertiserApi(string supplierXml, string callBackUrl, string customParam)
		{
			NoProblem.Core.BusinessLogic.SupplierLogic.SupplierComplete.SupplierCompleteLoader loader =
				new NoProblem.Core.BusinessLogic.SupplierLogic.SupplierComplete.SupplierCompleteLoader(this);
			return loader.SaveSupplierData(supplierXml, callBackUrl, customParam);
		}

		public void CreateSavedSuppliers()
		{
			DataAccessLayer.SupplierInsertData dal = new NoProblem.Core.DataAccessLayer.SupplierInsertData(XrmDataContext);
			IEnumerable<DML.Xrm.new_supplierinsertdata> lst = dal.GetNewSuppliersToCreate();
			NoProblem.Core.BusinessLogic.SupplierLogic.SupplierComplete.SupplierCompleteLoader loader =
				new NoProblem.Core.BusinessLogic.SupplierLogic.SupplierComplete.SupplierCompleteLoader(this);
			foreach (var item in lst)
			{
				try
				{
					loader.CreateSupplier(item);
				}
				catch (Exception exc)
				{
					LogUtils.MyHandle.HandleException(exc, "Exception in CreateSavedSuppliers while handling supplierInsertData with id {0}", item.new_supplierinsertdataid);
				}
			}
		}

		#endregion

		public MiniSiteSupplierData GetMiniSiteSupplierData(string supplier)
		{
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			MiniSiteSupplierData retVal = dal.GetMiniSiteSupplierData(Guid.Empty, string.Empty, supplier);
			return retVal;
		}

		public MiniSiteSupplierData GetMiniSiteSupplierData(Guid supplierId, string originCode)
		{
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			MiniSiteSupplierData retVal = dal.GetMiniSiteSupplierData(supplierId, originCode, string.Empty);
			return retVal;
		}

		public AdvertiserGeneralInfo GetSupplierGeneralInfo(Guid supplierId)
		{
			string accountQuery = @"SELECT description as LongDescription,name,accountnumber,numberofemployees,
new_complaints,new_weightedscore,new_surveygradecounter,new_certificate2,new_description as ShortDescription,
ISNULL(new_sumassistancerequests,0) AS new_sumassistancerequests,new_videourl as VideoUrl,emailaddress1,new_firstname,new_lastname,telephone1,
fax,address1_country,address1_stateorprovince,address1_city,address1_county,address1_line1,address1_line2,address1_postalcode,
new_accountgovernmentid,websiteurl,telephone2,address1_utcoffset,ISNULL(new_stageinregistration,0) AS new_stageinregistration, new_publisherranking, new_recordcalls,
new_address1_stateorprovince_regionid as stateid, new_address1_city_regionid as cityid, new_address1_county_regionid as countyid, new_address1_line1_regionid as streetid
,new_custom1,new_custom2,new_custom3,new_custom4,new_custom5,new_password,new_managerid,new_bizid,new_dapazstatus,new_fundsstatus,telephone3,New_DoNotSendSMS
FROM account with (nolock) WHERE accountid = @id AND account.deletionstatecode = 0";
			AdvertiserGeneralInfo retVal = new AdvertiserGeneralInfo();
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				using (SqlCommand command = new SqlCommand(accountQuery, connection))
				{
					command.Connection.Open();
					command.Parameters.AddWithValue("@id", supplierId);
					using (SqlDataReader accountDetails = command.ExecuteReader())
					{
						if (accountDetails.Read())
						{
							retVal.LongDescription = ConvertorUtils.ToString(accountDetails["LongDescription"], string.Empty);
							retVal.Name = ConvertorUtils.ToString(accountDetails["name"], string.Empty);
							retVal.AccountNumber = ConvertorUtils.ToString(accountDetails["accountnumber"], string.Empty);
							retVal.NumberOfEmployees = ConvertorUtils.ToInt(accountDetails["numberofemployees"]);
							retVal.Complaints = ConvertorUtils.ToInt(accountDetails["new_complaints"]);
							retVal.WeightedScore = ConvertorUtils.ToInt(accountDetails["new_weightedscore"]);
							retVal.SurveyGradeCounter = ConvertorUtils.ToInt(accountDetails["new_surveygradecounter"]);
							retVal.Certificate2 = ConvertorUtils.ToString(accountDetails["new_certificate2"], string.Empty);
							retVal.ShortDescription = ConvertorUtils.ToString(accountDetails["ShortDescription"], string.Empty);
							retVal.SumAssistanceRequests = ConvertorUtils.ToInt(accountDetails["new_sumassistancerequests"]);
							retVal.VideoUrl = ConvertorUtils.ToString(accountDetails["VideoUrl"], string.Empty);
							retVal.Email = ConvertorUtils.ToString(accountDetails["emailaddress1"], string.Empty);
							retVal.FirstName = ConvertorUtils.ToString(accountDetails["new_firstname"], string.Empty);
							retVal.LastName = ConvertorUtils.ToString(accountDetails["new_lastname"], string.Empty);
							retVal.ContactPhone = ConvertorUtils.ToString(accountDetails["telephone1"], string.Empty);
							retVal.CompanyPhone = ConvertorUtils.ToString(accountDetails["telephone2"], string.Empty);
							retVal.Fax = ConvertorUtils.ToString(accountDetails["Fax"], string.Empty);
							retVal.CountryName = ConvertorUtils.ToString(accountDetails["address1_country"], string.Empty);
							retVal.StateName = ConvertorUtils.ToString(accountDetails["address1_stateorprovince"], string.Empty);
							retVal.StateId = ConvertorUtils.ToGuid(accountDetails["stateid"]);
							retVal.CityName = ConvertorUtils.ToString(accountDetails["address1_city"], string.Empty);
							retVal.CityId = ConvertorUtils.ToGuid(accountDetails["cityid"]);
							retVal.DistrictName = ConvertorUtils.ToString(accountDetails["address1_county"], string.Empty);
							retVal.DistrictId = ConvertorUtils.ToGuid(accountDetails["countyid"]);
							retVal.StreetName = ConvertorUtils.ToString(accountDetails["address1_line1"], string.Empty);
							retVal.StreetId = ConvertorUtils.ToGuid(accountDetails["streetid"]);
							retVal.HouseNum = ConvertorUtils.ToString(accountDetails["address1_line2"], string.Empty);
							retVal.Zipcode = ConvertorUtils.ToString(accountDetails["address1_postalcode"], string.Empty);
							retVal.TimeZone = ConvertorUtils.ToInt(accountDetails["address1_utcoffset"]);
							retVal.CompanyId = ConvertorUtils.ToString(accountDetails["new_accountgovernmentid"], string.Empty);
							retVal.CompanySite = ConvertorUtils.ToString(accountDetails["websiteurl"], string.Empty);
							retVal.RegistrationStage = ConvertorUtils.ToInt(accountDetails["new_stageinregistration"]);
							retVal.PublisherRanking = ConvertorUtils.ToInt(accountDetails["new_publisherranking"]);
							retVal.AllowRecordings = accountDetails["new_recordcalls"] == null ? false : ConvertorUtils.ToBoolean(accountDetails["new_recordcalls"]);
							retVal.Custom1 = ConvertorUtils.ToString(accountDetails["new_custom1"], string.Empty);
							retVal.Custom2 = ConvertorUtils.ToString(accountDetails["new_custom2"], string.Empty);
							retVal.Custom3 = ConvertorUtils.ToString(accountDetails["new_custom3"], string.Empty);
							retVal.Custom4 = ConvertorUtils.ToString(accountDetails["new_custom4"], string.Empty);
							retVal.Custom5 = ConvertorUtils.ToString(accountDetails["new_custom5"], string.Empty);
							retVal.Password = ConvertorUtils.ToString(accountDetails["new_password"], string.Empty);
							retVal.ManagerId = ConvertorUtils.ToGuid(accountDetails["new_managerid"]);
							retVal.BizId = ConvertorUtils.ToString(accountDetails["new_bizid"], string.Empty);
							retVal.SmsNumber = (accountDetails["telephone3"] == DBNull.Value) ? string.Empty : (string)accountDetails["telephone3"];
							retVal.DoNotSendSMS = (accountDetails["New_DoNotSendSMS"] == DBNull.Value) ? false : (bool)accountDetails["New_DoNotSendSMS"];
							if (accountDetails["new_dapazstatus"] != DBNull.Value)
							{
								var dapazStatus = (DataModel.Xrm.account.DapazStatus)(int)accountDetails["new_dapazstatus"];
								retVal.IsRarahQuote = dapazStatus == NoProblem.Core.DataModel.Xrm.account.DapazStatus.RarahQuote;
								retVal.DapazStatus = (NoProblem.Core.DataModel.Xrm.account.DapazStatus)((int)accountDetails["new_dapazstatus"]);
							}
							else
							{
								retVal.DapazStatus = NoProblem.Core.DataModel.Xrm.account.DapazStatus.PPA;
							}
							if (accountDetails["new_fundsstatus"] != DBNull.Value)
							{
								retVal.FundsStatus = (DataModel.Xrm.account.FundsStatus)((int)accountDetails["new_fundsstatus"]);
							}
							else
							{
								retVal.FundsStatus = NoProblem.Core.DataModel.Xrm.account.FundsStatus.Normal;
							}
						}
					}
				}
			}
			return retVal;
		}

		public UpsertAdvertiserResponse UpsertAdvertiser(UpsertAdvertiserRequest request)
		{
			UpsertAdvertiserResponse response = new UpsertAdvertiserResponse();
			response.Status = UpsertAdvertiserStatus.Success;
			DataAccessLayer.ConfigurationSettings settingsDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
			DataAccessLayer.AccountRepository accountRepository = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			DML.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
			bool isUpdate = request.SupplierId != Guid.Empty;
			if (isUpdate)
			{
				supplier.accountid = request.SupplierId;
				DataAccessLayer.AccountRepository dal2 = new NoProblem.Core.DataAccessLayer.AccountRepository(null);
				var oldEmail = dal2.GetEmail(request.SupplierId);
				if (oldEmail != request.Email)
				{
					supplier.new_isemailverified = false;
				}
			}
            bool isDuplicate = CheckDuplicates(request, response, settingsDal, accountRepository);
			if (isDuplicate)
			{
				return response;
			}
			if (IsRealNumberAVirtualNumber(request, response))
			{
				return response;
			}

			InsertPropertiesToSupplier(request, supplier);

			//if (request.IsFromAdvertiserPortal)
			//{
			//    supplier.new_accountcompleteness_mydetails = true;
			//}

			if (isUpdate == false)
			{
				if ((supplier.new_securitylevel.HasValue && supplier.new_securitylevel.Value > 0)
					|| supplier.new_affiliateoriginid.HasValue
					)//if is publisher user or affiliate user.
				{
					string password = LoginUtils.GenerateNewPassword();
					supplier.new_password = password;
					supplier.new_status = (int)DML.Xrm.account.SupplierStatus.Approved;
				}
				else//is is supplier, give default origin.
				{
					DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
					Guid originId = originDal.GetDefaultOriginId();
					if (originId != Guid.Empty)
					{
						supplier.new_originid = originId;
					}
					if (request.IsFromAAR)
					{
						supplier.new_isfromaar = true;
						supplier.new_status = (int)DML.Xrm.account.SupplierStatus.Trial;
						supplier.new_trialstatusreason = (int)DML.Xrm.account.TrialStatusReasonCode.Accessible;
						supplier.new_trialstatusreasonmodifiedon = DateTime.Now;
					}
					supplier.new_isexemptofppamonthlypayment = true;
				}
				if (request.TimeZone.HasValue == false)
				{
					string publisherTimeZoneCode = settingsDal.GetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE);
					supplier.address1_utcoffset = int.Parse(publisherTimeZoneCode);
				}
			}

			if (isUpdate)
			{
                accountRepository.Update(supplier);
			}
			else
			{
                accountRepository.Create(supplier);
			}
			XrmDataContext.SaveChanges();
			if (isUpdate)
			{
				SendStupidEmail(request, settingsDal);
				if (request.TimeZone.HasValue)
				{
					RefreshWorkHours(request.SupplierId);
				}
			}
			if (isUpdate == false && supplier.new_securitylevel.HasValue && supplier.new_securitylevel.Value > 0)//creation of user (no supplier).
			{
				Notifications notificationLogic = new Notifications(XrmDataContext);
				notificationLogic.NotifySupplier(DML.enumSupplierNotificationTypes.SUPPLIER_SIGNUP, supplier.accountid, supplier.accountid.ToString(), "account", supplier.accountid.ToString(), "account");
			}
			//Update stage in registration:
			DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			accountRepositoryDal.SetSupplierRegistrationStage(supplier.accountid, DML.Xrm.account.StageInRegistration.GENERAL_INFO);
			response.SupplierId = supplier.accountid;
			if (response.Status == UpsertAdvertiserStatus.Success)
			{
				if (isUpdate && request.NewDapazStatus.HasValue)
				{
					var supplierWithAllFields = accountRepositoryDal.Retrieve(supplier.accountid);
					Dapaz.DapazStatusChanger changer = new NoProblem.Core.BusinessLogic.Dapaz.DapazStatusChanger(XrmDataContext);
					changer.ChangeStatus(supplierWithAllFields, request.NewDapazStatus.Value);
				}
				Dapaz.PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(response.SupplierId);
				inserter.InsertToStats();
			}
			return response;
		}

		private bool IsRealNumberAVirtualNumber(UpsertAdvertiserRequest request, UpsertAdvertiserResponse response)
		{
			DataAccessLayer.DirectNumber dal = new DataAccessLayer.DirectNumber(XrmDataContext);
			if (dal.IsDirectNumber(request.ContactPhone) || dal.IsDirectNumber(request.CompanyPhone) || dal.IsDirectNumber(request.SmsNumber) || dal.IsDirectNumber(request.Fax))
			{
				response.Status = UpsertAdvertiserStatus.PhoneDuplicate;
				return true;
			}
			return false;
		}

		private void SendStupidEmail(UpsertAdvertiserRequest request, DataAccessLayer.ConfigurationSettings settingsDal)
		{
			string stupidEmailTo = settingsDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.STUPID_EMAIL_TO);
			if (!string.IsNullOrEmpty(stupidEmailTo))
			{
				Thread tr = new Thread(new ParameterizedThreadStart(SendStupidEmail));
				tr.Start(new object[] { stupidEmailTo, request });
			}
		}

		private void SendStupidEmail(object obj)
		{
			try
			{
				string stupidEmailTo = (string)((object[])obj)[0];
				UpsertAdvertiserRequest upsertRequest = (UpsertAdvertiserRequest)((object[])obj)[1];
				StringBuilder bodyBuilder = new StringBuilder();
				Type requestType = typeof(UpsertAdvertiserRequest);
				var properties = requestType.GetProperties();
				bool send = false;
				foreach (var property in properties)
				{
					if (property.Name == "SupplierId"
						|| property.Name == "AuditOn"
						|| property.Name == "AuditEntries"
						|| property.Name == "IsFromAAR"
						|| property.Name == "IsFromAdvertiserPortal"
						|| property.Name == "FullAddress"
						|| property.Name == "CanHearAllRecordings"
						|| property.Name == "Password"
						|| property.Name.StartsWith("Custom")
						|| property.Name == "SecurityLevel"
					)
					{
						continue;
					}
					if (property.PropertyType == typeof(Guid?) || property.PropertyType == typeof(Guid))
					{
						continue;
					}
					object objPropertyValue = property.GetValue(upsertRequest, null);
					if (objPropertyValue != null)
					{
						bodyBuilder.Append(property.Name).Append(" = ");
						bodyBuilder.AppendLine(objPropertyValue.ToString());
						bodyBuilder.Append("<br />");
						send = true;
					}
				}
				if (send)
				{
					Notifications notifier = new Notifications(null);
					notifier.SendEmail(stupidEmailTo, bodyBuilder.ToString(), "Changes in Customer Details - " + GetBizId(upsertRequest));
				}
			}
			catch (Exception exc)
			{
				LogUtils.MyHandle.HandleException(exc, "Exception in SendStupidEmail(object obj)");
			}
		}

		private string GetBizId(UpsertAdvertiserRequest upsertRequest)
		{
			if (upsertRequest.BizId != null)
			{
				return upsertRequest.BizId;
			}
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(null);
			return dal.Retrieve(upsertRequest.SupplierId).new_bizid;
		}

		public DML.SupplierModel.MiniSiteData GetMiniSiteData(Guid supplierId)
		{
			DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			DML.SupplierModel.MiniSiteData data = accountRepositoryDal.GetMiniSiteData(supplierId);
			return data;
		}

		public void UpdateJobDone(UpdateJobDoneRequest request)
		{
			DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
			var call = dal.Retrieve(request.CallId);
			if (request.IsJobDone.HasValue)
			{
				if (call.new_isjobdone.HasValue == false)
				{
					dal.AddToSumSurvey(request.CallId, false);
				}
				call.new_isjobdone = request.IsJobDone.Value;
			}
			else
			{
				if (call.new_isjobdone.HasValue)
				{
					dal.AddToSumSurvey(request.CallId, true);
				}
				call.new_isjobdone = null;
			}
			dal.Update(call);
			XrmDataContext.SaveChanges();
		}

		public SupplierIncidentData GetSupplierIncidentData(Guid callId)
		{
			DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
			SupplierIncidentDataExpanded data = dal.GetSupplierIncidentData(callId);
			data.refundProcessState = RefundProcessState.OK;
			int daysLimit = 0;
			if (data.IsRefundBlocked)
			{
				data.refundProcessState = RefundProcessState.OverPercentageLimit;
			}
			else
			{
				DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
				string daysLimitStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.REFUND_DAYS_BACK_LIMIT);
				if (int.TryParse(daysLimitStr, out daysLimit) == false)
				{
					daysLimit = 365;
				}
				if (data.CreatedOn.Date < DateTime.Now.Date.AddDays(daysLimit * -1))
				{
					data.refundProcessState = RefundProcessState.OverDaysLimit;
				}
				//else if (data.IsInvitedBySMS || !data.IsCallChannel)
				//{
				//    data.refundProcessState = RefundProcessState.NoPhoneConnection;
				//}
				else if (!data.HasRecording)
				{
					string recordStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.RECORD_CALLS);
					bool recordCalls = recordStr == "1";
					if (recordCalls)
					{
						data.refundProcessState = RefundProcessState.NoRecording;
					}
				}
				//else
				//{
				//    var status = GetSupplierStatus(data.SupplierId);
				//    if (status.SupplierStatus != SupplierStatus.Available)
				//    {
				//        data.refundProcessState = RefundProcessState.NotAvailable;
				//    }
				//}
			}
			SupplierIncidentData retVal = new SupplierIncidentData(data);
			retVal.RefundDaysBackLimit = daysLimit;
			return retVal;
		}

		public DML.SupplierModel.SupplierStatus ActivateSupplier(Guid supplierId, Guid userId)
		{
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			var supplier = dal.Retrieve(supplierId);
			int stageInt = supplier.new_stageinregistration.HasValue ? supplier.new_stageinregistration.Value : 0;
			bool inactiveWithMoney = supplier.new_isinactivewithmoney.HasValue && supplier.new_isinactivewithmoney.Value;
			DML.Xrm.account.SupplierStatus newStatus = NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Approved;
			if (stageInt < (int)DML.Xrm.account.StageInRegistration.PAID)
			{
				newStatus = NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Candidate;
			}
			SetSupplierDbStatus(supplier, newStatus, Guid.Empty, dal);
			if (newStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Approved)
			{
				RankingNotifications rankingNotify = new RankingNotifications(supplierId);
				rankingNotify.NotifyOtherSuppliers();
			}
			var retVal = GetSupplierStatus(supplierId, newStatus, dal);
			if (retVal.SupplierStatus == SupplierStatus.Available)
			{
				DirectNumbersAssigner assigner = new DirectNumbersAssigner();
				assigner.AssignDirectNumbersToSupplier(supplierId);
			}
			Supplierns.SupplierStatusChangeAuditer auditer = new NoProblem.Core.BusinessLogic.Supplierns.SupplierStatusChangeAuditer(XrmDataContext);
			auditer.AuditSupplierStatusChange(
				supplier,
				inactiveWithMoney ? NoProblem.Core.BusinessLogic.Supplierns.SupplierStatusChangeAuditer.AuditStatusValues.InactiveWithMoney : NoProblem.Core.BusinessLogic.Supplierns.SupplierStatusChangeAuditer.AuditStatusValues.Inactive,
				NoProblem.Core.BusinessLogic.Supplierns.SupplierStatusChangeAuditer.AuditStatusValues.Active,
				userId,
				dal);
			if (Dapaz.PpaOnlineSync.SyncConfiguration.IsEnabled)
			{
				Dapaz.PpaOnlineSync.Syncher syncher = new Dapaz.PpaOnlineSync.Syncher(supplierId);
				syncher.Start();
			}
			return retVal.SupplierStatus;
		}

		public DeactivateSupplierResponse DeactivateSupplier(Guid supplierId, Guid inactivityReasonId, Guid userId)
		{
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			//if is trial, don't allow deactivation (temp).
			//var dbStatus = dal.GetSupplierDataBaseStatus(supplierId);
			//if (dbStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Trial)
			//{
			//    return;
			//}
			if (inactivityReasonId == Guid.Empty)
			{
				DataAccessLayer.InactivityReason inactivityReasonsDal = new NoProblem.Core.DataAccessLayer.InactivityReason(XrmDataContext);
				inactivityReasonId = inactivityReasonsDal.GetRequestedByApiReasonId();
			}
			bool isInactiveWithMoney = SetSupplierDbStatus(supplierId, NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Inactive, inactivityReasonId, dal);
			new Dapaz.Orders.OrdersSender(userId, NoProblem.Core.BusinessLogic.Dapaz.Orders.eOrderType.Deactivation, supplierId).SendOrder();
			//DirectNumbersAssigner assigner = new DirectNumbersAssigner();
			//assigner.DetachAllDirectNumbersFromSupplier(supplierId);
			DeactivateSupplierResponse response = new DeactivateSupplierResponse();
			response.IsInactiveWithMoney = isInactiveWithMoney;
			Supplierns.SupplierStatusChangeAuditer auditer = new NoProblem.Core.BusinessLogic.Supplierns.SupplierStatusChangeAuditer(XrmDataContext);
			auditer.AuditSupplierStatusChange(
				supplierId,
				NoProblem.Core.BusinessLogic.Supplierns.SupplierStatusChangeAuditer.AuditStatusValues.Active,
				isInactiveWithMoney ? NoProblem.Core.BusinessLogic.Supplierns.SupplierStatusChangeAuditer.AuditStatusValues.InactiveWithMoney : NoProblem.Core.BusinessLogic.Supplierns.SupplierStatusChangeAuditer.AuditStatusValues.Inactive,
				userId,
				dal);
			if (Dapaz.PpaOnlineSync.SyncConfiguration.IsEnabled)
			{
				Dapaz.PpaOnlineSync.Syncher syncher = new Dapaz.PpaOnlineSync.Syncher(supplierId);
				syncher.Start();
			}
			return response;
		}

		public SearchSuppliersResponse SearchSuppliers(DML.SupplierModel.SearchSuppliersRequest request)
		{
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			var suppliers = dal.SearchSuppliers(request.SearchValue);
			var pagesSsuppliers = suppliers.Skip(request.PageSize * (request.PageNumber - 1)).Take(request.PageSize);
			SearchSuppliersResponse response = new SearchSuppliersResponse();
			response.Rows = new List<SupplierSearchRow>();
			foreach (var sup in pagesSsuppliers)
			{
				sup.Key.SupplierStatus = GetSupplierStatus(sup.Key.SupplierId, sup.Value, dal).SupplierStatus;
				var supplier = dal.Retrieve(sup.Key.SupplierId);
				sup.Key.CashBalance = supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value : 0;
				sup.Key.DapazStatus = supplier.new_dapazstatus.HasValue ? (DML.Xrm.account.DapazStatus)supplier.new_dapazstatus.Value : DML.Xrm.account.DapazStatus.PPA;
				sup.Key.IsInactiveWithMoney = supplier.new_isinactivewithmoney.Value;
				response.Rows.Add(sup.Key);
			}
			response.TotalPages = (int)Math.Ceiling(((double)suppliers.Count) / ((double)request.PageSize));
			response.TotalRows = suppliers.Count;
			response.CurrentPage = request.PageNumber;
			return response;
		}

		public DML.SupplierModel.SupplierStatusContainer GetSupplierStatus(Guid supplierId)
		{
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			DML.Xrm.account.SupplierStatus dbStatus = dal.GetSupplierDataBaseStatus(supplierId);
			var container = GetSupplierStatus(supplierId, dbStatus, dal);
			container.IsAvailableHours = IsAvailableHours(supplierId);
			var supplier = dal.Retrieve(supplierId);
			container.UpdatedDefaultPrices = supplier.new_getstartedtask_defaultprice.HasValue && supplier.new_getstartedtask_defaultprice.Value;
			container.PaymentMethodFailed = supplier.new_paymentmethodfailed.Value;
			container.EmailJustVerified = supplier.new_emailjustverified.HasValue && supplier.new_emailjustverified.Value;
			if (container.EmailJustVerified)
			{
				SupplierBL.ResetEmailJustVerifiedThread tr = new SupplierBL.ResetEmailJustVerifiedThread(supplierId);
				tr.Start();
			}
			if (container.PaymentMethodFailed)
				container.CanGoOnDuty = false;
			container.Balance = supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value : 0;
			container.IsInactiveWithMoney = supplier.new_isinactivewithmoney.Value;
			return container;
		}

		public List<DataModel.Tables.TableRowData> GetAllInactivityReasons()
		{
			DataAccessLayer.InactivityReason dal = new NoProblem.Core.DataAccessLayer.InactivityReason(XrmDataContext);
			return dal.GetAll();
		}

		public void GiveDirectNumbersToSuppliers(Guid originId)
		{
			DirectNumbersAssigner assigner = new DirectNumbersAssigner();
			assigner.AssignDirectNumbersByOriginId(originId);
		}

		public void GiveDirectNumbersToSuppliers()
		{
			DirectNumbersAssigner assigner = new DirectNumbersAssigner();
			assigner.AssignDirectNumbersToSuppliers();
		}

		public bool IsPublisherUser(Guid userId)
		{
			bool retVal = false;
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			int securityLevel = dal.GetSecurityLevel(userId);
			if (securityLevel > 0)
			{
				retVal = true;
			}
			return retVal;
		}

		public string GetPublisherUserName(Guid UserId)
		{
			string command = @"
					SELECT TOP 1 [Name]
					  FROM [dbo].[AccountBase] ab
						inner join [dbo].[AccountExtensionBase] aeb on aeb.AccountId = ab.AccountId
						  where [New_securitylevel] is not null and ab.AccountId = @UserId";
			string _connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
			string name = null;
			using (SqlConnection conn = new SqlConnection(_connectionString))
			{
				conn.Open();
				using (SqlCommand cmd = new SqlCommand(command, conn))
				{
					cmd.Parameters.AddWithValue("@UserId", UserId);
					SqlDataReader reader = cmd.ExecuteReader();
					if (reader.Read())
					{
						name = (string)reader["Name"];
					}
				}
				conn.Close();
			}
			return name;
		}

		public void TurnOnRecordings(Guid supplierId)
		{
			ChangeRecordingSetting(supplierId, true);
		}

		public void TurnOffRecordings(Guid supplierId)
		{
			ChangeRecordingSetting(supplierId, false);
		}

		private void ChangeRecordingSetting(Guid supplierId, bool on)
		{
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			DML.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
			supplier.accountid = supplierId;
			supplier.new_recordcalls = on;
			dal.Update(supplier);
			XrmDataContext.SaveChanges();
		}

		public bool SendSignupNotification(Guid supplierId)
		{
			Notifications notificationsLogic = new Notifications(XrmDataContext);
			bool sent = notificationsLogic.NotifySupplier(DML.enumSupplierNotificationTypes.SUPPLIER_SIGNUP, supplierId, supplierId.ToString(), "account", supplierId.ToString(), "account");
			return sent;
		}

		public SupplierSignUpResponse SupplierSignUp(SupplierSignUpRequest request)
		{
			SupplierSignUpResponse response = new SupplierSignUpResponse();
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			DML.Xrm.account supplier = dal.GetSupplierByPhone(request.MobilePhone);
			DataModel.Xrm.new_invitation invitation = null;
			DataAccessLayer.Invitation invitationDal = null;
			if (supplier == null)
			{
				if (!string.IsNullOrEmpty(request.Email))
				{
					if (dal.IsEmailDuplicate(request.Email, Guid.Empty))
					{
						response.SignUpStatus = SupplierSignUpStatus.EmailDuplicate;
						return response;
					}
				}
				else//is with referral code
				{
					if (string.IsNullOrEmpty(request.ReferralCode))
					{
						response.SignUpStatus = SupplierSignUpStatus.ReferralCodeNotFound;
						return response;
					}
					if (request.ReferralCode != "ThEkNiGhTrIdEr")
					{
						invitationDal = new NoProblem.Core.DataAccessLayer.Invitation(XrmDataContext);
						invitation = invitationDal.FindInvitationbyReferralCode(request.ReferralCode);
						if (invitation == null)
						{
							response.SignUpStatus = SupplierSignUpStatus.ReferralCodeNotFound;
							return response;
						}
						if (invitation.statuscode != (int)DataModel.Xrm.new_invitation.InvitationStatus.Pending)
						{
							response.SignUpStatus = SupplierSignUpStatus.ReferralCodeAlreadyUsed;
							return response;
						}
					}
				}
				response.SupplierId = SignUpNewSupplier(request.MobilePhone, dal, request.Email);
				response.SignUpStatus = SupplierSignUpStatus.OkIsNew;
				if (invitation != null)
				{
					invitation.statuscode = (int)DataModel.Xrm.new_invitation.InvitationStatus.Linked;
					invitation.new_linkedon = DateTime.Now;
					invitation.new_accountinvitedid = response.SupplierId;
					invitationDal.Update(invitation);
					supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
					supplier.emailaddress1 = invitation.new_invitedemail;
					supplier.accountid = response.SupplierId;
					supplier.new_referralcode = request.ReferralCode;
					dal.Update(supplier);
					XrmDataContext.SaveChanges();
				}
				else if (request.ReferralCode == "ThEkNiGhTrIdEr")
				{
					supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
					supplier.accountid = response.SupplierId;
					supplier.new_referralcode = request.ReferralCode;
					supplier.new_firstpaymentneeded = false;
					dal.Update(supplier);
					XrmDataContext.SaveChanges();
				}
			}
			else//supplier != null
			{
				response.SupplierId = supplier.accountid;
				if (supplier.new_status == (int)DML.Xrm.account.SupplierStatus.Trial && (supplier.new_stageintrialregistration.HasValue == false || supplier.new_stageintrialregistration.Value < (int)DML.Xrm.account.StageInTrialRegistration.EnteredLocation))
				{
					if (!string.IsNullOrEmpty(request.Email))//is requester
					{
						if (dal.IsEmailDuplicate(request.Email, supplier.accountid))
						{
							response.SignUpStatus = SupplierSignUpStatus.EmailDuplicate;
							return response;
						}
						supplier.new_firstpaymentneeded = false;

						if (supplier.new_isfromaar.HasValue
							&& supplier.new_isfromaar.Value
							&&
							(supplier.new_trialstatusreason == (int)DataModel.Xrm.account.TrialStatusReasonCode.InTrial
							|| supplier.new_trialstatusreason == (int)DataModel.Xrm.account.TrialStatusReasonCode.Expired)
							)
						{
							supplier.new_waitingforinvitation = false;
						}
						else
						{
							supplier.new_waitingforinvitation = true;
						}
					}
					else//is with referral code
					{
						invitation = CheckReferralCode(response, supplier, request.ReferralCode);
						if (invitation == null && request.ReferralCode != "ThEkNiGhTrIdEr")
						{
							return response;
						}
						supplier.new_firstpaymentneeded = true;
						supplier.new_waitingforinvitation = false;
						if (request.ReferralCode != "ThEkNiGhTrIdEr")
						{
							supplier.emailaddress1 = invitation.new_invitedemail;
						}
						else
						{
							supplier.new_firstpaymentneeded = false;
						}
						supplier.new_referralcode = request.ReferralCode;
					}
					if (supplier.new_isfromaar.HasValue && supplier.new_isfromaar.Value)
					{
						response.SignUpStatus = SupplierSignUpStatus.OkIsAar;
						supplier.new_firstpaymentneeded = false;
					}
					else
					{
						response.SignUpStatus = SupplierSignUpStatus.OkIsNew;
					}
					//Send email and sms to the supplier with the password
					if (string.IsNullOrEmpty(supplier.new_password))
					{
						string password = LoginUtils.GenerateNewPassword();
						supplier.new_password = password;
					}
					dal.Update(supplier);
					XrmDataContext.SaveChanges();
					SendSignupNotification(supplier.accountid);
				}
				else
				{
					response.SignUpStatus = SupplierSignUpStatus.TransferToLogin;
				}
			}
			dal.SetSupplierTrialRegistrationStage(response.SupplierId, NoProblem.Core.DataModel.Xrm.account.StageInTrialRegistration.EnteredMobile);
			return response;
		}

		public CheckEmailInRegistrationRequestResponse CheckEmailInRegistrationRequest(string email)
		{
			CheckEmailInRegistrationRequestResponse response = new CheckEmailInRegistrationRequestResponse();
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			var supplier = dal.GetSupplierByEmail(email);
			if (supplier == null)
			{
				response.EmailCheckResult = CheckEmailInRegistrationRequestResponse.eEmailCheck.Continue;
			}
			else if (supplier.new_status == (int)DML.Xrm.account.SupplierStatus.Trial && (supplier.new_stageintrialregistration.HasValue == false || supplier.new_stageintrialregistration.Value < (int)DML.Xrm.account.StageInTrialRegistration.EnteredCode))
			{
				response.EmailCheckResult = CheckEmailInRegistrationRequestResponse.eEmailCheck.Continue;
			}
			else
			{
				response.EmailCheckResult = CheckEmailInRegistrationRequestResponse.eEmailCheck.Login;
			}
			return response;
		}

		/// <summary>
		/// releases the email and referral code
		/// </summary>
		/// <param name="supplierId"></param>
		public void ChangePhoneInRegistration(Guid supplierId)
		{
			DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			DataModel.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
			supplier.accountid = supplierId;
			supplier.emailaddress1 = null;
			accountRepositoryDal.Update(supplier);
			DataAccessLayer.Invitation inviteDal = new NoProblem.Core.DataAccessLayer.Invitation(XrmDataContext);
			var invitation = inviteDal.FindLinkedInvitation(supplierId);
			if (invitation != null)
			{
				invitation.new_accountinvitedid = null;
				invitation.new_linkedon = null;
				invitation.statuscode = (int)DataModel.Xrm.new_invitation.InvitationStatus.Pending;
				inviteDal.Update(invitation);
			}
			XrmDataContext.SaveChanges();
		}

		private DataModel.Xrm.new_invitation CheckReferralCode(SupplierSignUpResponse response, DML.Xrm.account supplier, string referralCode)
		{
			if (string.IsNullOrEmpty(referralCode))
			{
				response.SignUpStatus = SupplierSignUpStatus.ReferralCodeNotFound;
				return null;
			}
			if (referralCode == "ThEkNiGhTrIdEr")
			{
				return null;
			}
			DataAccessLayer.Invitation invitationDal = new NoProblem.Core.DataAccessLayer.Invitation(XrmDataContext);
			var invitation = invitationDal.FindInvitationbyReferralCode(referralCode);
			if (invitation == null)
			{
				response.SignUpStatus = SupplierSignUpStatus.ReferralCodeNotFound;
				return null;
			}
			if (invitation.statuscode != (int)DataModel.Xrm.new_invitation.InvitationStatus.Pending
				&& invitation.new_accountinvitedid != supplier.accountid)
			{
				response.SignUpStatus = SupplierSignUpStatus.ReferralCodeAlreadyUsed;
				return null;
			}
			var oldInvitation = invitationDal.FindLinkedInvitation(supplier.accountid);
			if (oldInvitation != null && oldInvitation.new_invitationid != invitation.new_invitationid)
			{
				oldInvitation.statuscode = (int)DataModel.Xrm.new_invitation.InvitationStatus.Pending;
				oldInvitation.new_linkedon = null;
				oldInvitation.new_accountinvitedid = null;
				invitationDal.Update(oldInvitation);
				XrmDataContext.SaveChanges();
			}
			if (invitation.statuscode == (int)DataModel.Xrm.new_invitation.InvitationStatus.Pending)
			{
				invitation.statuscode = (int)DataModel.Xrm.new_invitation.InvitationStatus.Linked;
				invitation.new_linkedon = DateTime.Now;
				invitation.new_accountinvitedid = supplier.accountid;
				invitationDal.Update(invitation);
				XrmDataContext.SaveChanges();
			}
			return invitation;
		}

		public ApprovePasswordResponse ApprovePassword(ApprovePasswordRequest request)
		{
			ApprovePasswordResponse response = new ApprovePasswordResponse();
			if (string.IsNullOrEmpty(request.Password))
			{
				response.IsApproved = false;
			}
			else
			{
				DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
				var supplier = dal.Retrieve(request.SupplierId);
				bool isApproved = supplier.new_password == request.Password;
				if (isApproved)
				{
					response.IsApproved = true;
					if (supplier.new_trialstatusreason.HasValue)
					{
						response.TrialStatusReasonCode = (DML.Xrm.account.TrialStatusReasonCode)supplier.new_trialstatusreason.Value;
					}
					if (supplier.new_cashbalance.HasValue)
					{
						response.Balance = supplier.new_cashbalance.Value;
					}
					if (!string.IsNullOrEmpty(request.Email))
					{
						supplier.emailaddress1 = request.Email;
						dal.Update(supplier);
						XrmDataContext.SaveChanges();
					}
					dal.SetSupplierTrialRegistrationStage(request.SupplierId, NoProblem.Core.DataModel.Xrm.account.StageInTrialRegistration.EnteredCode);
				}
				else
				{
					response.IsApproved = false;
				}
			}
			return response;
		}

		public NoProblem.Core.DataModel.PublisherPortal.UserLoginResponse AdvertiserLogin(string phone, string password)
		{
			DML.PublisherPortal.UserLoginResponse response = new NoProblem.Core.DataModel.PublisherPortal.UserLoginResponse();
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			var supplier = dal.GetSupplierByPhone(phone);
			if (supplier == null)
			{
				response.UserLoginStatus = NoProblem.Core.DataModel.PublisherPortal.eUserLoginStatus.NotFound;
			}
			else if (supplier.new_status == (int)DML.Xrm.account.SupplierStatus.Trial
				&& (supplier.new_stageintrialregistration.HasValue == false || supplier.new_stageintrialregistration.Value < (int)DML.Xrm.account.StageInTrialRegistration.EnteredLocation))
			{
				if (password != supplier.new_password)
				{
					response.UserLoginStatus = NoProblem.Core.DataModel.PublisherPortal.eUserLoginStatus.BadPassword;
				}
				else
				{
					if (supplier.new_stageintrialregistration.HasValue == false
						|| supplier.new_stageintrialregistration.Value == (int)DML.Xrm.account.StageInTrialRegistration.EnteredMobile)
					{
						response.UserLoginStatus = NoProblem.Core.DataModel.PublisherPortal.eUserLoginStatus.RegistrationNotFinishedStart;
					}
					else if (supplier.new_stageintrialregistration.Value == (int)DML.Xrm.account.StageInTrialRegistration.EnteredCode)
					{
						response.UserLoginStatus = NoProblem.Core.DataModel.PublisherPortal.eUserLoginStatus.RegistrationNotFinishedHeading;
					}
					else
					{
						response.UserLoginStatus = NoProblem.Core.DataModel.PublisherPortal.eUserLoginStatus.RegistrationNotFinishedMap;
					}
					response.Balance = supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value : 0;
					response.Name = supplier.name;
					response.SecurityLevel = supplier.new_securitylevel.HasValue ? supplier.new_securitylevel.Value : 0;
					response.StageInRegistration = supplier.new_stageinregistration.HasValue ? supplier.new_stageinregistration.Value : 0;
					response.UserId = supplier.accountid;
				}
			}
			else if (password != supplier.new_password)
			{
				response.UserLoginStatus = NoProblem.Core.DataModel.PublisherPortal.eUserLoginStatus.BadPassword;
			}
			else if (supplier.new_status == (int)DML.Xrm.account.SupplierStatus.Inactive)
			{
				response.UserLoginStatus = NoProblem.Core.DataModel.PublisherPortal.eUserLoginStatus.UserInactive;
			}
			else if (supplier.new_waitingforinvitation.HasValue
				&& supplier.new_waitingforinvitation.Value)
			{
				response.UserLoginStatus = NoProblem.Core.DataModel.PublisherPortal.eUserLoginStatus.WaitingForInvitation;
			}
			else
			{
				response.Balance = supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value : 0;
				response.Name = supplier.name;
				response.SecurityLevel = supplier.new_securitylevel.HasValue ? supplier.new_securitylevel.Value : 0;
				response.StageInRegistration = supplier.new_stageinregistration.HasValue ? supplier.new_stageinregistration.Value : 0;
				response.UserId = supplier.accountid;
				response.UserLoginStatus = NoProblem.Core.DataModel.PublisherPortal.eUserLoginStatus.OK;
			}
			return response;
		}

		public bool IsAdvertiserExists(string phone)
		{
			bool retVal = false;
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			var supplier = dal.GetSupplierByPhone(phone);
			if (supplier != null
				&& (supplier.new_securitylevel.HasValue == false || supplier.new_securitylevel.Value == 0)
				&& supplier.new_affiliateoriginid.HasValue == false
				&& (supplier.new_status != (int)DML.Xrm.account.SupplierStatus.Inactive)
				&& !(supplier.new_status == (int)DML.Xrm.account.SupplierStatus.Trial && supplier.new_stageintrialregistration.HasValue && supplier.new_stageintrialregistration.Value < (int)DML.Xrm.account.StageInTrialRegistration.EnteredLocation)
				)
			{
				retVal = true;
			}
			return retVal;
		}

		public void SendPasswordReminder(string phone, bool useEmail)
		{
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			var supplier = dal.GetSupplierByPhone(phone);
			if (supplier == null
				|| (supplier.new_securitylevel.HasValue && supplier.new_securitylevel.Value > 0)
				|| (supplier.new_affiliateoriginid.HasValue)
				|| (supplier.new_status == (int)DML.Xrm.account.SupplierStatus.Inactive)
				|| (supplier.new_status == (int)DML.Xrm.account.SupplierStatus.Trial && supplier.new_stageintrialregistration.HasValue && supplier.new_stageintrialregistration.Value < (int)DML.Xrm.account.StageInTrialRegistration.EnteredLocation)
				)
			{
				throw new Exception("Supplier not found or inactive or didn't finish basic registration");
			}
			Notifications notifier = new Notifications(XrmDataContext);
			var sent = notifier.NotifySupplier(
				DML.TemplateNames.PASSWORD_REMINDER_SMS,
				DML.TemplateNames.PASSWORD_REMINDER_EMAIL,
				!useEmail,
				useEmail,
				supplier.accountid,
				supplier.accountid,
				"account",
				supplier.accountid,
				"account");
		}

		public bool MakeAvailable(Guid supplierId)
		{
			bool retVal = false;
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			var supplier = dal.Retrieve(supplierId);
			if (supplier.new_unavailabilityreasonid.HasValue)
			{
				DataAccessLayer.UnavailabilityReason reasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
				var reason = reasonDal.Retrieve(supplier.new_unavailabilityreasonid.Value);
				if (!((reason.new_isoverrefundpercent.HasValue && reason.new_isoverrefundpercent.Value)
					|| (reason.new_ispricingended.HasValue && reason.new_ispricingended.Value)))
				{
					supplier.new_unavailabilityreasonid = null;
					supplier.new_unavailablefrom = null;
					supplier.new_unavailablelocalfrom = null;
					supplier.new_unavailableto = null;
					supplier.new_unavailablelocalto = null;
					dal.Update(supplier);
					XrmDataContext.SaveChanges();
					retVal = true;
				}
			}
			return retVal;
		}

		public string GetRecordingPathByRecordid(string recordId)
		{
			string retVal = null;
			DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
			var incAcc = dal.GetIncidentAccountByRecordId(recordId);
			if (incAcc != null)
			{
				retVal = incAcc.new_recordfilelocation;
			}
			return retVal;
		}

		#endregion

		#region Private Methods

		private void RefreshWorkHours(Guid supplierId)
		{
			Thread tr = new Thread(delegate()
			{
				DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(null);
				var supplier = dal.Retrieve(supplierId);
				if (string.IsNullOrEmpty(supplier.new_sundayavailablefrom))
				{
					return;
				}
				XElement xml = new XElement("SupplierAvailability");
				xml.Add(new XAttribute("SiteId", ""));
				xml.Add(new XAttribute("SupplierId", supplier.accountid));
				XElement availability = new XElement("Availability");
				availability.Add(
					new XElement("Day",
						new XAttribute("Name", "sunday"),
						new XElement("FromTime", supplier.new_sundayavailablefrom),
						new XElement("TillTime", supplier.new_sundayavailableto)),
					new XElement("Day",
						new XAttribute("Name", "monday"),
						new XElement("FromTime", supplier.new_mondayavailablefrom),
						new XElement("TillTime", supplier.new_mondayavailableto)),
					new XElement("Day",
						new XAttribute("Name", "tuesday"),
						new XElement("FromTime", supplier.new_tuesdayavailablefrom),
						new XElement("TillTime", supplier.new_tuesdayavailableto)),
					new XElement("Day",
						new XAttribute("Name", "wednesday"),
						new XElement("FromTime", supplier.new_wednesdayavailablefrom),
						new XElement("TillTime", supplier.new_wednesdayavailableto)),
					new XElement("Day",
						new XAttribute("Name", "thursday"),
						new XElement("FromTime", supplier.new_thursdayavailablefrom),
						new XElement("TillTime", supplier.new_thursdayavailableto)),
					new XElement("Day",
						new XAttribute("Name", "friday"),
						new XElement("FromTime", supplier.new_fridayavailablefrom),
						new XElement("TillTime", supplier.new_fridayavailableto)),
					new XElement("Day",
						new XAttribute("Name", "saturday"),
						new XElement("FromTime", supplier.new_saturdayavailablefrom),
						new XElement("TillTime", supplier.new_saturdayavailableto))
					);
				xml.Add(availability);
				BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(dal.XrmDataContext, null);
				supplierLogic.SetSupplierAvailability(xml.ToString(), true);
			});
			tr.Start();
		}

		private Guid SignUpNewSupplier(string mobilePhone, DataAccessLayer.AccountRepository dal, string email)
		{
			DML.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account();
			supplier.telephone1 = mobilePhone;
			string password = LoginUtils.GenerateNewPassword();
			supplier.new_password = password;
			supplier.new_status = (int)DML.Xrm.account.SupplierStatus.Trial;
			supplier.new_trialstatusreason = (int)DML.Xrm.account.TrialStatusReasonCode.InRegistration;
			supplier.new_trialstatusreasonmodifiedon = DateTime.Now;
			supplier.name = "***";
			if (!string.IsNullOrEmpty(email))
			{
				supplier.new_waitingforinvitation = true;
			}
			else//is with referral
			{
				supplier.new_firstpaymentneeded = true;
			}
			DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
			string publisherTimeZoneCode = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.PUBLISHER_TIME_ZONE);
			supplier.address1_utcoffset = int.Parse(publisherTimeZoneCode);
			supplier.new_recordcalls = true;
			supplier.new_initialtrialbudget = 0;
			supplier.new_isfromaar = false;
			dal.Create(supplier);
			XrmDataContext.SaveChanges();
			//Send email and sms to the supplier with the password
			SendSignupNotification(supplier.accountid);
			Thread tr = new Thread(delegate()
			{
				try
				{
					XElement xml = new XElement("SupplierAvailability");
					xml.Add(new XAttribute("SiteId", ""));
					xml.Add(new XAttribute("SupplierId", supplier.accountid));
					XElement availability = new XElement("Availability");
					availability.Add(
						new XElement("Day",
							new XAttribute("Name", "sunday"),
							new XElement("FromTime", "1:00"),
							new XElement("TillTime", "1:01")),
						new XElement("Day",
							new XAttribute("Name", "monday"),
							new XElement("FromTime", "08:00"),
							new XElement("TillTime", "17:00")),
						new XElement("Day",
							new XAttribute("Name", "tuesday"),
							new XElement("FromTime", "08:00"),
							new XElement("TillTime", "17:00")),
						new XElement("Day",
							new XAttribute("Name", "wednesday"),
							new XElement("FromTime", "08:00"),
							new XElement("TillTime", "17:00")),
						new XElement("Day",
							new XAttribute("Name", "thursday"),
							new XElement("FromTime", "08:00"),
							new XElement("TillTime", "17:00")),
						new XElement("Day",
							new XAttribute("Name", "friday"),
							new XElement("FromTime", "08:00"),
							new XElement("TillTime", "17:00")),
						new XElement("Day",
							new XAttribute("Name", "saturday"),
							new XElement("FromTime", "1:00"),
							new XElement("TillTime", "1:01"))
						);
					xml.Add(availability);
					BusinessLogic.Supplier supplierLogic = new NoProblem.Core.BusinessLogic.Supplier(XrmDataContext, null);
					supplierLogic.SetSupplierAvailability(xml.ToString(), true);
				}
				catch (Exception exc)
				{
					LogUtils.MyHandle.HandleException(exc, "Exception in SignUpNewSupplier async part. SupplierId = {0}", supplier.accountid);
				}
			});
			tr.Start();
			return supplier.accountid;
		}

		private bool SetSupplierDbStatus(Guid supplierId, DML.Xrm.account.SupplierStatus newStatus, Guid inactivityReasonId, DataAccessLayer.AccountRepository dal)
		{
			return SetSupplierDbStatus(dal.Retrieve(supplierId), newStatus, inactivityReasonId, dal);
		}

		private bool SetSupplierDbStatus(DataModel.Xrm.account supplier, DML.Xrm.account.SupplierStatus newStatus, Guid inactivityReasonId, DataAccessLayer.AccountRepository dal)
		{
			bool isInactiveWithMoney = false;
			supplier.new_status = (int)newStatus;
			if (newStatus == DML.Xrm.account.SupplierStatus.Inactive && inactivityReasonId != Guid.Empty)
			{
				supplier.new_inactivityreasonid = inactivityReasonId;
				DataAccessLayer.InactivityReason inactivityReasonDal = new NoProblem.Core.DataAccessLayer.InactivityReason(XrmDataContext);
				Guid inactiveWithMoneyReasonId = inactivityReasonDal.GetReasonIdForInactiveWithMoney();
				if (inactiveWithMoneyReasonId == inactivityReasonId)
				{
					decimal minPrice, maxPrice;
					DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
					accExpDal.GetSupplierMinAndMaxPrices(supplier.accountid, out maxPrice, out minPrice);
					if (supplier.new_cashbalance >= maxPrice)
					{
						supplier.new_status = (int)DML.Xrm.account.SupplierStatus.Approved;
						supplier.new_isinactivewithmoney = true;
						isInactiveWithMoney = true;
					}
					else
					{
						supplier.new_cashbalance = 0;
						supplier.new_availablecashbalance = 0;
						supplier.new_isinactivewithmoney = false;
					}
				}
			}
			else if (newStatus != DML.Xrm.account.SupplierStatus.Inactive)
			{
				supplier.new_inactivityreasonid = null;
				supplier.new_isinactivewithmoney = false;
			}
			dal.Update(supplier);
			XrmDataContext.SaveChanges();
			return isInactiveWithMoney;
		}

		private SupplierStatusContainer GetSupplierStatus(Guid supplierId, DML.Xrm.account.SupplierStatus dbSupplierStatus, DataAccessLayer.AccountRepository dal)
		{
			SupplierStatusContainer retVal = new SupplierStatusContainer();
			retVal.CanGoOnDuty = true;
			retVal.SupplierStatus = SupplierStatus.Inactive;
			if (dbSupplierStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Approved
				|| dbSupplierStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Trial
				|| dbSupplierStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Rarah)
			{
				Guid unavailabilityReasonId;
				bool isAvailable = dal.IsAvailable(supplierId, out unavailabilityReasonId);
				retVal.SupplierStatus = SupplierStatus.Unavailable;
				if (isAvailable)
				{
					if (dbSupplierStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Approved)
					{
						retVal.SupplierStatus = SupplierStatus.Available;
					}
					else if (dbSupplierStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Trial)
					{
						retVal.SupplierStatus = SupplierStatus.Trial;
					}
					else
					{
						retVal.SupplierStatus = SupplierStatus.Rarah;
					}
				}
				else
				{
					DataAccessLayer.UnavailabilityReason reasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
					var reason = reasonDal.Retrieve(unavailabilityReasonId);
					if ((reason.new_isoverrefundpercent.HasValue && reason.new_isoverrefundpercent.Value)
						|| (reason.new_ispricingended.HasValue && reason.new_ispricingended.Value)
						)
					{
						retVal.CanGoOnDuty = false;
					}
					retVal.UnavailabilityReasonId = unavailabilityReasonId;
					retVal.UnavailabilityReasonName = reason.new_name;
					if (reason.new_ispricingended.HasValue && reason.new_ispricingended.Value)
					{
						retVal.UnavailavilityType = UnavailavilityTypes.OutOfMoney;
					}
					else if (reason.new_isdailybudget.HasValue && reason.new_isdailybudget.Value)
					{
						retVal.UnavailavilityType = UnavailavilityTypes.DailyBudget;
					}
					else if (reason.new_isoverrefundpercent.HasValue && reason.new_isoverrefundpercent.Value)
					{
						retVal.UnavailavilityType = UnavailavilityTypes.OverRefundPercent;
						var supplier = dal.Retrieve(supplierId);
						if (supplier.new_unavailableto.HasValue)
						{
							retVal.DaysToEndOfOverRefundPercent = (supplier.new_unavailableto.Value - DateTime.Now).Days + 1;
						}
					}
					else
					{
						retVal.UnavailavilityType = UnavailavilityTypes.UserCreated;
					}
				}
			}
			else if (dbSupplierStatus == NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Candidate)
			{
				retVal.SupplierStatus = SupplierStatus.Candidate;
			}
			return retVal;
		}

		private void InsertPropertiesToSupplier(UpsertAdvertiserRequest request, DML.Xrm.account supplier)
		{
			Type requestType = typeof(UpsertAdvertiserRequest);
			Type accountType = typeof(DML.Xrm.account);
			var accountProperties = accountType.GetProperties();
			var properties = requestType.GetProperties();
			foreach (var property in properties)
			{
				if (property.Name == "SupplierId" || property.Name == "AuditOn" || property.Name == "AuditEntries" || property.Name == "IsFromAAR" || property.Name == "IsFromAdvertiserPortal" || property.Name == "NewDapazStatus")
				{
					continue;
				}
				if (property.Name == "FundsStatus")
				{
					if (request.FundsStatus == 0)
						request.FundsStatus = DataModel.Xrm.account.FundsStatus.Normal;
					supplier.new_fundsstatus = (int)request.FundsStatus;
					continue;
				}
				object obj = property.GetValue(request, null);
				if (obj != null)
				{
					var attributes = property.GetCustomAttributes(typeof(CrmFieldAttribute), true);
					var attr = (CrmFieldAttribute)attributes.First();
					string crmFieldName = attr.field;
					var accountProperty = accountProperties.First(x => x.Name == crmFieldName);
					if (property.PropertyType == typeof(Guid?) || property.PropertyType == typeof(Guid))
					{
						if (((Guid)obj) == Guid.Empty)
						{
							obj = null;
						}
					}
					accountProperty.SetValue(supplier, obj, null);
				}
			}
		}

		private bool CheckDuplicates(UpsertAdvertiserRequest request, UpsertAdvertiserResponse response, DataAccessLayer.ConfigurationSettings settingsDal, DataAccessLayer.AccountRepository accountRepositoryDal)
		{
			bool isDuplicate = false;
			bool checkDuplicate = true;
			string checkStr = settingsDal.GetConfigurationSettingValue(DML.ConfigurationKeys.CHECK_SUPPLIER_DUPLICATES);
			if (string.IsNullOrEmpty(checkStr) == false)
			{
				checkDuplicate = checkStr != "0";
			}
			if (checkDuplicate)
			{
				if (string.IsNullOrEmpty(request.Email) == false)
				{
					//check email not duplicate.
					isDuplicate = accountRepositoryDal.IsEmailDuplicate(request.Email, request.SupplierId);
					if (isDuplicate)
					{
						response.Status = UpsertAdvertiserStatus.EmailDuplicate;
						isDuplicate = true;
					}
				}
				//check biz id duplicate:
				if (!isDuplicate & !string.IsNullOrEmpty(request.BizId))
				{
					if (request.SupplierId == Guid.Empty)
					{
						if (accountRepositoryDal.ExistsByBizId(request.BizId))
						{
							response.Status = UpsertAdvertiserStatus.BizIdDuplicate;
							isDuplicate = true;
						}
					}
					else
					{
						if (accountRepositoryDal.ExistsByBizIdExcludingThisId(request.SupplierId, request.BizId))
						{
							response.Status = UpsertAdvertiserStatus.BizIdDuplicate;
							isDuplicate = true;
						}
					}
				}
			}
			if (!isDuplicate)
			{
				string checkPhoneStr = settingsDal.GetConfigurationSettingValue(DML.ConfigurationKeys.CHECK_PHONE_DUPLICATE);
				bool checkPhoneDuplicate = true;
				if (!string.IsNullOrEmpty(checkPhoneStr))
				{
					checkPhoneDuplicate = checkPhoneStr != "0";
				}
				if (checkPhoneDuplicate)
				{
					if (!string.IsNullOrEmpty(request.ContactPhone))
					{
						//check phone not duplicate.
						if (accountRepositoryDal.IsPhoneDuplicate(request.ContactPhone, request.SupplierId))
						{
							response.Status = UpsertAdvertiserStatus.PhoneDuplicate;
							isDuplicate = true;
						}
					}
				}
			}
			return isDuplicate;
		}

		private bool IsAvailableHours(Guid supplierId)
		{
			DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
			bool isAvailable = dal.IsAvailableHours(supplierId);
			return isAvailable;
		}

		#endregion
	}
}
