﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic
{
    public class DailyBudgetManager : XrmUserBase
    {
        #region Ctor

        public DailyBudgetManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        #endregion

        #region Public Methods

        public bool CheckDailyBudgetIsReached(Guid supplierId, bool isTrialSupplier)
        {
            bool retVal = false;
            try
            {
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                DateTime nowLocal = FixDateToLocal(DateTime.Now);
                DateTime dateStartUtc = FixDateToUtcFromLocal(nowLocal.Date);
                bool isOver = dal.IsOverDailyBugdet(supplierId, dateStartUtc, isTrialSupplier);
                if (isOver)
                {
                    retVal = true;
                    MakeSupplierUnavailable(supplierId, dal, nowLocal, dateStartUtc);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CheckDailyBudget for supplier {0}", supplierId);
            }
            return retVal;
        }

        public int? GetDailybudget(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var budget = dal.Retrieve(supplierId).new_dailybudget;
            return budget;
        }

        public void UpdateDailyBudget(Guid supplierId, int? budget)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            {
                DataModel.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
                supplier.accountid = supplierId;
                supplier.new_dailybudget = budget;
                dal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
            DateTime nowLocal = FixDateToLocal(DateTime.Now);
            DateTime dateStartUtc = FixDateToUtcFromLocal(nowLocal.Date);
            bool isOver = dal.IsOverDailyBugdet(supplierId, dateStartUtc, false);
            if (isOver)
            {
                MakeSupplierUnavailable(supplierId, dal, nowLocal, dateStartUtc);
            }
            else
            {
                var supplier = dal.Retrieve(supplierId);
                if (supplier.new_unavailabilityreasonid.HasValue)
                {
                    DataAccessLayer.UnavailabilityReason unaReasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
                    DataModel.Xrm.new_unavailabilityreason reason = unaReasonDal.GetDailyBudgetReason();
                    if (supplier.new_unavailabilityreasonid.Value == reason.new_unavailabilityreasonid)
                    {
                        supplier.new_unavailabilityreasonid = null;
                        supplier.new_unavailablefrom = null;
                        supplier.new_unavailablelocalfrom = null;
                        supplier.new_unavailableto = null;
                        supplier.new_unavailablelocalto = null;
                        dal.Update(supplier);
                        XrmDataContext.SaveChanges();
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        private void MakeSupplierUnavailable(Guid supplierId, DataAccessLayer.AccountRepository dal, DateTime nowLocal, DateTime dateStartUtc)
        {
            DateTime endDateUtc = dateStartUtc.AddDays(1).AddMinutes(-1);
            DataModel.Xrm.account supplier = dal.Retrieve(supplierId);
            if (supplier.new_unavailabilityreasonid.HasValue 
                && (supplier.new_unavailableto.HasValue == false
                    || supplier.new_unavailableto.Value >= endDateUtc)
                && (supplier.new_unavailablefrom.Value <= dateStartUtc)
                )
            {
                return;
            }
            DataAccessLayer.UnavailabilityReason unaReasonDal = new NoProblem.Core.DataAccessLayer.UnavailabilityReason(XrmDataContext);
            DataModel.Xrm.new_unavailabilityreason reason = unaReasonDal.GetDailyBudgetReason();
            supplier.new_unavailablefrom = DateTime.Now;
            supplier.new_unavailablelocalfrom = nowLocal;
            DateTime localEnd = nowLocal.Date.AddDays(1).AddMinutes(-1);
            supplier.new_unavailablelocalto = localEnd;
            supplier.new_unavailableto = endDateUtc;
            supplier.new_unavailabilityreasonid = reason.new_unavailabilityreasonid;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        #endregion

    }
}
