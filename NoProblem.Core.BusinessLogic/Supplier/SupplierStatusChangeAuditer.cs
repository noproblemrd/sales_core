﻿using System;
using System.Collections.Generic;

namespace NoProblem.Core.BusinessLogic.Supplierns
{
    internal class SupplierStatusChangeAuditer : XrmUserBase
    {
        public SupplierStatusChangeAuditer(DataModel.Xrm.DataContext xrmDataContext) :
            base(xrmDataContext)
        {

        }

        private const string FIELD_ID = "STATUS_FIELD";
        private const string FIELD_NAME = "status";
        private const string PAGE_ID = "STATUS_CHANGE_ID";
        private const string PAGE_NAME = "STATUS_CHANGE";

        public void AuditSupplierStatusChange(Guid supplierId, AuditStatusValues oldValue, AuditStatusValues newValue, Guid userId, DataAccessLayer.AccountRepository dal)
        {
            var supplier = dal.Retrieve(supplierId);
            AuditSupplierStatusChange(supplier, oldValue, newValue, userId, dal);
        }

        public void AuditSupplierStatusChange(NoProblem.Core.DataModel.Xrm.account supplier, AuditStatusValues oldValue, AuditStatusValues newValue, Guid userId, DataAccessLayer.AccountRepository dal)
        {
            Audit.AuditManager audit = new NoProblem.Core.BusinessLogic.Audit.AuditManager();
            List<DataModel.Audit.AuditEntry> auditList = new List<NoProblem.Core.DataModel.Audit.AuditEntry>();
            var auditEntry = new NoProblem.Core.DataModel.Audit.AuditEntry();
            auditEntry.AdvertiseId = supplier.accountid;
            auditEntry.AdvertiserName = supplier.name;
            auditEntry.AuditStatus = NoProblem.Core.DataModel.Xrm.new_auditentry.Status.Update;
            auditEntry.CreatedOn = DateTime.Now;
            auditEntry.FieldId = FIELD_ID;
            auditEntry.FieldName = FIELD_NAME;
            auditEntry.NewValue = newValue.ToString();
            auditEntry.OldValue = oldValue.ToString();
            auditEntry.PageId = PAGE_ID;
            auditEntry.PageName = PAGE_NAME;
            auditEntry.UserId = userId;
            var user = dal.Retrieve(userId);
            auditEntry.UserName = user.name;
            auditList.Add(auditEntry);
            audit.CreateAudit(auditList);
        }

        public enum AuditStatusValues
        {
            Active,
            Inactive,
            InactiveWithMoney
        }
    }
}
