﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierBL
{
    internal class SignUpNotifier : Runnable
    {
        private DataModel.Xrm.account supplier;
        private DataAccessLayer.ConfigurationSettings config;

        public SignUpNotifier(DataModel.Xrm.account supplier)
        {
            this.supplier = supplier;
            config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
        }

        protected override void Run()
        {
            SendSMS();
        }        

        private void SendSMS()
        {
            Notifications notificationsManager = new Notifications(XrmDataContext);
            notificationsManager.NotifySupplier(DataModel.enumSupplierNotificationTypes.AFTER_FIRST_PAYMENT, supplier.accountid, supplier.accountid.ToString(), "account", supplier.accountid.ToString(), "account");
        }
    }
}
