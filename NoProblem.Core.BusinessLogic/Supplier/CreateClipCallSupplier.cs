﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierBL
{
    public class CreateClipCallSupplier : XrmUserBase
    {
        private const string LATITIDUE_LONGITUDE_FORMAT = "{0:0.######}";
         #region Ctor

        public CreateClipCallSupplier(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        #endregion

        public DataModel.Xrm.account CreateClipCallSupplierFromAar(Guid AarSupplierId)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplierXrm = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
            string command = @"
SELECT New_name, New_Rating YelpRating, New_Phone, New_AlthernativePhone, New_AccountId
	,New_Address, New_Latitude, New_Longitude, New_GoogleRating, New_yelpUrl, New_googleUrl
    ,New_yelpReviewCount, New_yelpBusinessIcon, New_googleReviewCount, New_googleBusinessIcon
    ,New_citygridId, New_citygridBusinessIcon, New_citygridRating, New_citygridReviewCount, New_citygridUrl
    ,New_YPBusinessIcon, New_YPId, New_YPRating, New_YPReviewCount, New_YPUrl
FROM dbo.New_yelpsupplierExtensionBase
WHERE New_yelpsupplierId = @AarSupplierId";
            using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    
                    cmd.Parameters.AddWithValue("@AarSupplierId", AarSupplierId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["New_AccountId"] == DBNull.Value)
                        {
                            supplierXrm.new_fullname = (string)reader["New_name"];
                            supplierXrm.name = supplierXrm.new_fullname;
                            if (reader["YelpRating"] != DBNull.Value)
                                supplierXrm.new_yelprating = (double)((decimal)reader["YelpRating"]);
                            if (reader["New_AlthernativePhone"] != DBNull.Value)
                            {
                                supplierXrm.telephone1 = (string)reader["New_AlthernativePhone"];
                                supplierXrm.telephone2 = (string)reader["New_Phone"];
                            }
                            else
                                supplierXrm.telephone1 = (string)reader["New_Phone"];
                            if (reader["New_Latitude"] != DBNull.Value)
                                supplierXrm.new_latitude = string.Format(LATITIDUE_LONGITUDE_FORMAT, (decimal)reader["New_Latitude"]);
                            if (reader["New_Longitude"] != DBNull.Value)
                                supplierXrm.new_longitude = string.Format(LATITIDUE_LONGITUDE_FORMAT, (decimal)reader["New_Longitude"]);
                            if (reader["New_GoogleRating"] != DBNull.Value)
                                supplierXrm.new_googlerating = (double)((decimal)reader["New_GoogleRating"]);
                            if (reader["New_yelpUrl"] != DBNull.Value)
                                supplierXrm.new_yelpurl = (string)reader["New_yelpUrl"];
                            if (reader["New_googleUrl"] != DBNull.Value)
                                supplierXrm.new_googleurl = (string)reader["New_googleUrl"];
                            if (reader["New_Address"] != DBNull.Value)
                                supplierXrm.new_fulladdress = (string)reader["New_Address"];
                            if (reader["New_yelpReviewCount"] != DBNull.Value)
                                supplierXrm.new_yelpreviewcount = (int)reader["New_yelpReviewCount"];
                            if (reader["New_yelpBusinessIcon"] != DBNull.Value)
                                supplierXrm.new_yelpiconurl = (string)reader["New_yelpBusinessIcon"];
                            if (reader["New_googleReviewCount"] != DBNull.Value)
                                supplierXrm.new_googlereviewcount = (int)reader["New_googleReviewCount"];
                            if (reader["New_googleBusinessIcon"] != DBNull.Value)
                                supplierXrm.new_googleiconurl = (string)reader["New_googleBusinessIcon"];

                            if (reader["New_citygridId"] != DBNull.Value)
                                supplierXrm.new_citygridid = (string)reader["New_citygridId"];
                            if (reader["New_citygridBusinessIcon"] != DBNull.Value)
                                supplierXrm.new_citygridbusinessicon = (string)reader["New_citygridBusinessIcon"];
                            if (reader["New_citygridRating"] != DBNull.Value)
                                supplierXrm.new_citygridrating = (decimal)reader["New_citygridRating"];
                            if (reader["New_citygridReviewCount"] != DBNull.Value)
                                supplierXrm.new_citygridreviewcount = (int)reader["New_citygridReviewCount"];
                            if (reader["New_citygridUrl"] != DBNull.Value)
                                supplierXrm.new_citygridurl = (string)reader["New_citygridUrl"];

                            if (reader["New_YPBusinessIcon"] != DBNull.Value)
                                supplierXrm.new_ypbusinessicon = (string)reader["New_YPBusinessIcon"];
                            if (reader["New_YPId"] != DBNull.Value)
                                supplierXrm.new_ypid = (string)reader["New_YPId"];
                            if (reader["New_YPRating"] != DBNull.Value)
                                supplierXrm.new_yprating = (decimal)reader["New_YPRating"];
                            if (reader["New_YPReviewCount"] != DBNull.Value)
                                supplierXrm.new_ypreviewcount = (int)reader["New_YPReviewCount"];
                            if (reader["New_YPUrl"] != DBNull.Value)
                                supplierXrm.new_ypurl = (string)reader["New_YPUrl"];

                            supplierXrm.new_havevideo = false;
                            supplierXrm.new_ismobile = true;
                            supplierXrm.new_isfromaar = true;
                            //   supplierXrm.timezoneruleversionnumber
                            supplierXrm.new_recordcalls = true;
                            string password = Utils.LoginUtils.GenerateNewPassword();
                            supplierXrm.new_password = password;
                            supplierXrm.new_stepinregistration2014 = (int)DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_General_3;
                            supplierXrm.new_status = (int)DataModel.Xrm.account.SupplierStatus.Trial;
                            supplierXrm.new_subscriptionplan = SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION;                            
                        }
                        else
                        {
                            supplierXrm = accountRepositoryDal.Retrieve((Guid)reader["New_AccountId"]);
                            conn.Close();
                            return supplierXrm;
                        }
                    }
                    else
                        return null;
                    reader.Dispose();
                    cmd.Dispose();
                    accountRepositoryDal.Create(supplierXrm);
                    XrmDataContext.SaveChanges();
                }
                command = @"SELECT s.PrimaryExpertiseId, ex.new_defaultprice
FROM dbo.AarSupplierExpertise s
	INNER JOIN dbo.New_primaryexpertiseExtensionBase ex on s.PrimaryExpertiseId = ex.New_primaryexpertiseId
WHERE s.YelpSupplierId = @AarSupplierId";
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@AarSupplierId", AarSupplierId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        DataAccessLayer.AccountExpertise accountExpertiseDal = new DataAccessLayer.AccountExpertise(XrmDataContext);
                        DataModel.Xrm.new_accountexpertise accountExpertise = new DataModel.Xrm.new_accountexpertise(XrmDataContext);
                        accountExpertise.new_accountid = supplierXrm.accountid;
                        accountExpertise.new_primaryexpertiseid = (Guid)reader["PrimaryExpertiseId"];
                        accountExpertise.new_incidentprice = reader["new_defaultprice"] == DBNull.Value ? 5m : (decimal)reader["new_defaultprice"];
                        accountExpertiseDal.Create(accountExpertise);
                    }
                    reader.Dispose();
                    cmd.Dispose();
                }
                command = @"SELECT distinct RH.Lvl2, RH.Lvl1
  FROM [dbo].[AarSupplierRegion] R
	INNER JOIN dbo.Region_Hierarchy RH on R.RegionId = RH.Lvl3
WHERE YelpSupplierId = @AarSupplierId";
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@AarSupplierId", AarSupplierId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    Guid RegionLvl2, RegionLvl1;
                    while(reader.Read())
                    {
                        RegionLvl2 = (Guid)reader["Lvl2"];
                        RegionLvl1 = (Guid)reader["Lvl1"];
                        DataAccessLayer.RegionAccount regionAccountDal = new DataAccessLayer.RegionAccount(XrmDataContext);
                        DataModel.Xrm.new_regionaccountexpertise regionAccount = new DataModel.Xrm.new_regionaccountexpertise(XrmDataContext);
                        regionAccount.new_accountid = supplierXrm.accountid;
                        regionAccount.new_regionid = RegionLvl2;
                        regionAccount.new_regionstate = (int)DataModel.Xrm.new_regionaccountexpertise.RegionState.Working;
                        regionAccountDal.Create(regionAccount);
                    }
                    reader.Dispose();
                    cmd.Dispose();
                }

                command = "UPDATE dbo.New_yelpsupplierExtensionBase SET New_AccountId = @AccountId WHERE New_yelpsupplierId = @AarSupplierId";
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    cmd.Parameters.AddWithValue("@AccountId", supplierXrm.accountid);
                    cmd.Parameters.AddWithValue("@AarSupplierId", AarSupplierId);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            XrmDataContext.SaveChanges();
            
            return supplierXrm;

        }
    }
}
