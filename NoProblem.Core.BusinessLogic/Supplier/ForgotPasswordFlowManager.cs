﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL
{
    public class ForgotPasswordFlowManager : XrmUserBase
    {
        private const string FORGOT_PASSWORD_QUERY_STRING_PARAM_NAME = "?code=";

        public ForgotPasswordFlowManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public bool SendResetEmail(string email)
        {
            //search for account with email in db
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.emailaddress1 == email
                        select new { acc.name, acc.accountid };
            var found = query.FirstOrDefault();
            //if found, create reset password request and send email with url (with token in the query string), return true
            if (found != null)
            {
                Guid supplierId = found.accountid;
                string token = SaveResetPasswordRequest(supplierId);
                SendEmailToSupplier(email, token, found.name);
                return true;
            }
            else//if not found, return false
            {
                return false;
            }
        }

        public void NewPasswordEntered(string password, string token)
        {
            Guid supplierId;
            Guid resetPasswordRequestId = RetrieveTokenData(token, out supplierId);
            if (resetPasswordRequestId == Guid.Empty)
            {
                throw new Exception("Reset password token is invalid.");
            }
            if (!Utils.LoginUtils.ValidatePassword(password))
            {
                throw new Exception("New password is invalid.");
            }
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_password = password;
            DataAccessLayer.AccountRepository accDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            accDal.Update(supplier);
            DataModel.Xrm.new_resetpasswordrequest resetPasswordRequest = new DataModel.Xrm.new_resetpasswordrequest(XrmDataContext);
            resetPasswordRequest.new_resetpasswordrequestid = resetPasswordRequestId;
            resetPasswordRequest.new_isused = true;
            DataAccessLayer.ResetPasswordRequest rprDal = new DataAccessLayer.ResetPasswordRequest(XrmDataContext);
            rprDal.Update(resetPasswordRequest);
            XrmDataContext.SaveChanges();
        }

        public bool CheckResetPasswordToken(string token)
        {
            if (String.IsNullOrEmpty(token))
            {
                throw new ArgumentException("token is invalid", "token");
            }
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@token", token);
            object scalar = dal.ExecuteScalar(checkTokenQuery, parameters);
            return scalar != null;
        }

        private Guid RetrieveTokenData(string token, out Guid supplierId)
        {
            if (String.IsNullOrEmpty(token))
            {
                throw new ArgumentException("token is invalid", "token");
            }
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@token", token);
            var reader = dal.ExecuteReader(retrieveTokenDataQuery, parameters);
            Guid resetPasswordRequestId = Guid.Empty;
            supplierId = Guid.Empty;
            foreach (var row in reader)
            {
                resetPasswordRequestId = (Guid)row["new_resetpasswordrequestid"];
                supplierId = (Guid)row["new_accountid"];
                break;
            }
            return resetPasswordRequestId;
        }
        
        private void SendEmailToSupplier(string email, string token, string name)
        {
            MandrillSender sender = new MandrillSender();
            DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
            string subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_AD_ForgotPassword);
            Dictionary<string, string> vars = new Dictionary<string, string>();
            string helloText = String.IsNullOrEmpty(name) ? email : name;
            vars.Add("NPVAR_HELLO", helloText);
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string url = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.FORGOT_PASSWORD_URL);
            url = url + FORGOT_PASSWORD_QUERY_STRING_PARAM_NAME + token;
            vars.Add("NPVAR_URL", url);
            bool sent = sender.SendTemplate(email, subject, DataModel.MandrillTemplates.E_AD_ForgotPassword, vars);
            if (!sent)
            {
                throw new Exception("The forgot password email could not be sent.");
            }
        }

        private string SaveResetPasswordRequest(Guid supplierId)
        {
            DataModel.Xrm.new_resetpasswordrequest entity = DataModel.EntityFactories.ResetPasswordRequestFactory.Create(supplierId);
            DataAccessLayer.ResetPasswordRequest rprDal = new DataAccessLayer.ResetPasswordRequest(XrmDataContext);
            rprDal.Create(entity);
            XrmDataContext.SaveChanges();
            return entity.new_token;
        }

        private const string checkTokenQuery =
    @"
declare @baseDate datetime = dateadd(day, -1, getdate())
select top 1 1
from new_resetpasswordrequest r
where 
New_Token = @token
and New_IsUsed = 0
and createdon > @baseDate
";

        private const string retrieveTokenDataQuery =
    @"
declare @baseDate datetime = dateadd(day, -1, getdate())
select top 1 new_resetpasswordrequestid, new_accountid
from new_resetpasswordrequest r
where 
New_Token = @token
and New_IsUsed = 0
and createdon > @baseDate
";
    }
}
