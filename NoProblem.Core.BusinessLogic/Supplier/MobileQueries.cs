﻿using NoProblem.Core.DataModel.SupplierModel.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL
{
    public class MobileQueries
    {
        private static readonly decimal MOBILE_DEFAULT_MIN_PRICE;
        private static readonly decimal MOBILE_DEFAULT_MED_PRICE;
        private static readonly decimal MOBILE_DEFAULT_MAX_PRICE;
        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();

        static MobileQueries()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            //    MOBILE_BID_TIMEOUT = int.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_BID_TIMEOUT));
            MOBILE_DEFAULT_MIN_PRICE = decimal.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_DEFAULT_MIN_PRICE));
            MOBILE_DEFAULT_MED_PRICE = decimal.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_DEFAULT_MED_PRICE));
            MOBILE_DEFAULT_MAX_PRICE = decimal.Parse(configDal.GetConfigurationSettingValue(NoProblem.Core.DataModel.ConfigurationKeys.MOBILE_DEFAULT_MAX_PRICE));
        }

        public LeadsListResponse GetSupplierLeads(DataModel.SupplierModel.Mobile.LeadsListRequest request)
        {
            LeadsListResponse response = new LeadsListResponse();
            var list = ExecuteLeadsListQuery(request.SupplierId, request.supplierLeadStatus);
            IEnumerable<LeadsListResponse.LeadsListLeadEntry> retValList;
            /*
             * when 16 then 'LOST'
when 10 then 'WON'
when 1 then 'NEW'
when 14 then 'EVALUATING'
when 19 then 'MISSED'
when 20 then 'REJECT'
             * REFUNDED

            if (request.supplierLeadStatus == LeadsListRequest.SupplierLeadStatus.NEW)
            {
                retValList = list.Where(x => x.Status == "NEW" || );
            }
            else if (request.supplierLeadStatus == LeadsListRequest.SupplierLeadStatus.WIN)
            {
                retValList = list.Where(x => x.Status == "WON");
            }
            else//onlyLost
            {
                retValList = list.Where(x => x.Status == "LOST");
            }
             * */
            retValList = list.Skip(request.PageSize * request.PageNumber).Take(request.PageSize);
            response.Leads = retValList.ToList();
            return response;
        }
        /*
        private List<LeadsListResponse.LeadsListLeadEntry> GetLeadsList(Guid supplierId)
        {
            /*
            return DataAccessLayer.CacheManager.Instance.Get<List<LeadsListResponse.LeadsListLeadEntry>>("MobileQueries_leadslist", supplierId.ToString(), ExecuteLeadsListQuery, DataAccessLayer.CacheManager.ONE_MINUTE * 2);
             * * /
            return ExecuteLeadsListQuery(supplierId);
        }
    */

        private List<LeadsListResponse.LeadsListLeadEntry> ExecuteLeadsListQuery(Guid supplierId, LeadsListRequest.SupplierLeadStatus suplierLeadStatus)
        {
            string _where = string.Empty;
            switch(suplierLeadStatus)
            {
                case(LeadsListRequest.SupplierLeadStatus.WIN):
                    _where = " and ia.statuscode = " + (int)NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSE +
                        " and ISNULL(ia.new_refundstatus, -1) <> " + (int)NoProblem.Core.DataModel.Xrm.new_incidentaccount.RefundSatus.Approved;
                    break;
                case(LeadsListRequest.SupplierLeadStatus.LOST):
                    _where = " and (ia.statuscode in (" + (int)NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.LOST + "," +
                        (int)NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.MISSED + "," + (int)NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.REJECT + ")" +
                        " or ia.new_refundstatus = " + (int)NoProblem.Core.DataModel.Xrm.new_incidentaccount.RefundSatus.Approved + ")";
                    break;
                case (LeadsListRequest.SupplierLeadStatus.NEW):
                    _where = " and ia.statuscode in (" + (int)NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.MATCH + "," +
                        (int)NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.IN_AUCTION + ") " +
                        "and inc.StatusCode in (" + (int)NoProblem.Core.DataModel.Xrm.incident.Status.NEW + "," + (int)NoProblem.Core.DataModel.Xrm.incident.Status.WAITING +
                        "," + (int)NoProblem.Core.DataModel.Xrm.incident.Status.AFTER_AUCTION + ") ";
                    break;
                    
            }
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("supplierId", supplierId);
            var reader = dal.ExecuteReader(getLeadsQuery.Replace("{where}", _where), parameters);
            List<LeadsListResponse.LeadsListLeadEntry> list = new List<LeadsListResponse.LeadsListLeadEntry>();
            foreach (var row in reader)
            {
                bool refunded = (row["new_refundstatus"] != DBNull.Value && (DataModel.Xrm.new_incidentaccount.RefundSatus)((int)row["new_refundstatus"]) == DataModel.Xrm.new_incidentaccount.RefundSatus.Approved);
                LeadsListResponse.LeadsListLeadEntry leadEntry = new LeadsListResponse.LeadsListLeadEntry();
                leadEntry.CategoryName = (string)row["category"];
                leadEntry.CreatedOn = (DateTime)row["CreatedOn"];
                leadEntry.Region = (string)row["region"];
                leadEntry.IncidentAccountId = (Guid)row["new_incidentaccountid"];
                leadEntry.VideoUrl = row["New_videoUrl"] == DBNull.Value ? string.Empty : (string)row["New_videoUrl"];
                leadEntry.CustomerProfileImage = row["new_profileimageurl"] != DBNull.Value ? (string)row["new_profileimageurl"] : null;
                leadEntry.PreviewVideoImageUrl = row["New_PreviewVideoPicUrl"] == DBNull.Value ? string.Empty : (string)row["New_PreviewVideoPicUrl"];
                leadEntry.Videoduration = row["New_VideoDuration"] == DBNull.Value ? 0 : (int)row["New_VideoDuration"];
                NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status incidentAccountStatus = (DataModel.Xrm.new_incidentaccount.Status)((int)row["statuscode"]);
                NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode incidentType = (DataModel.Xrm.incident.CaseTypeCode)((int)row["CaseTypeCode"]);
                string customerPhone = row["MobilePhone"] == DBNull.Value ? null : (string)row["MobilePhone"];
                string customerName = row["FirstName"] == DBNull.Value ? null : (string)row["FirstName"];
                string wonTitle;
                leadEntry.Status = GetIncidentAccountStatus(incidentAccountStatus, incidentType, customerName, customerPhone, out wonTitle);
                leadEntry.WonTitle = wonTitle;
                list.Add(leadEntry);                   
            }
            return list;
        }
        /*
        public LeadsListResponse GetSupplierLeadsV2(DataModel.SupplierModel.Mobile.LeadsListRequestV2 request)
        {
            LeadsListResponse response = new LeadsListResponse();
            var list = ExecuteLeadsListQuery(request.SupplierId);
            IEnumerable<LeadsListResponse.LeadsListLeadEntry> retValList;
          
            retValList = list.Skip(request.PageSize * request.PageNumber).Take(request.PageSize);
            response.Leads = retValList.ToList();
            return response;
        }
         * */
        /*
        private List<LeadsListResponse.LeadsListLeadEntry> GetLeadsList(Guid supplierId)
        {
            /*
            return DataAccessLayer.CacheManager.Instance.Get<List<LeadsListResponse.LeadsListLeadEntry>>("MobileQueries_leadslist", supplierId.ToString(), ExecuteLeadsListQuery, DataAccessLayer.CacheManager.ONE_MINUTE * 2);
             * * /
            return ExecuteLeadsListQuery(supplierId);
        }
    */
        /*
        private List<LeadsListResponse.LeadsListLeadEntry> ExecuteLeadsListQuery(Guid supplierId)
        {
            string _where = string.Empty;
           
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("supplierId", supplierId);
            var reader = dal.ExecuteReader(getLeadsQuery.Replace("{where}", _where), parameters);
            List<LeadsListResponse.LeadsListLeadEntry> list = new List<LeadsListResponse.LeadsListLeadEntry>();
            foreach (var row in reader)
            {
                bool refunded = (row["new_refundstatus"] != DBNull.Value && (DataModel.Xrm.new_incidentaccount.RefundSatus)((int)row["new_refundstatus"]) == DataModel.Xrm.new_incidentaccount.RefundSatus.Approved);
                LeadsListResponse.LeadsListLeadEntry leadEntry = new LeadsListResponse.LeadsListLeadEntry();
                leadEntry.CategoryName = (string)row["category"];
                leadEntry.CreatedOn = (DateTime)row["CreatedOn"];
                leadEntry.Region = (string)row["region"];
                leadEntry.IncidentAccountId = (Guid)row["new_incidentaccountid"];
                leadEntry.VideoUrl = row["New_videoUrl"] == DBNull.Value ? string.Empty : (string)row["New_videoUrl"];
                leadEntry.CustomerProfileImage = row["new_profileimageurl"] != DBNull.Value ? (string)row["new_profileimageurl"] : null;
                leadEntry.PreviewVideoImageUrl = row["New_PreviewVideoPicUrl"] == DBNull.Value ? string.Empty : (string)row["New_PreviewVideoPicUrl"];
                leadEntry.Videoduration = row["New_VideoDuration"] == DBNull.Value ? 0 : (int)row["New_VideoDuration"];
                NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status incidentAccountStatus = (DataModel.Xrm.new_incidentaccount.Status)((int)row["statuscode"]);
                NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode incidentType = (DataModel.Xrm.incident.CaseTypeCode)((int)row["CaseTypeCode"]);
                string customerPhone = row["MobilePhone"] == DBNull.Value ? null : (string)row["MobilePhone"];
                string customerName = row["FirstName"] == DBNull.Value ? null : (string)row["FirstName"];
                leadEntry.Status = GetIncidentAccountStatus(incidentAccountStatus, incidentType, customerName, customerPhone);
                list.Add(leadEntry);
            }
            return list;
        }*/
        /*
        private string GetIncidentAccountStatus(NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status incidentAccountStatus)
        {
            switch (incidentAccountStatus)
            {
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.LOST):
                    return "LOST";
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSE):
                    return "WON";
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.MATCH):
                    return "NEW";
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.IN_AUCTION):
                    return "EVALUATING";
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.MISSED):
                    return "MISSED";
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.REJECT):
                    return "REJECT";
            }
            return "LOST";
        }
         * */
        private string GetIncidentAccountStatus(NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status incidentAccountStatus,
            NoProblem.Core.DataModel.Xrm.incident.CaseTypeCode incidentType, string customerName, string customerPhone, out string wonTitle)
        {
            /*
            case ia.statuscode 
when 16 then 'LOST'
when 10 then 'WON'
when 1 then 'NEW'
when 14 then 'EVALUATING'
when 19 then 'MISSED'
when 20 then 'REJECT'
else 'LOST' 
end as 'status'
             * */
            if (incidentType == DataModel.Xrm.incident.CaseTypeCode.VideoChat)
            {
                wonTitle = GetWonStatus(customerName, customerPhone);
                return "WON";
            }
            wonTitle = null;
            switch(incidentAccountStatus)
            {
                case(NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.LOST):
                    return "LOST";
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.CLOSE):
                    wonTitle = GetWonStatus(customerName,customerPhone);
                    return "WON";
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.MATCH):
                    return "NEW";
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.IN_AUCTION):
                    return "EVALUATING";
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.MISSED):
                    return "MISSED";
                case (NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.REJECT):
                    return "REJECT";
            }
            return "LOST";
        }
        private string GetWonStatus(string customerName, string customerPhone)
        {
            if (!string.IsNullOrEmpty(customerName))
                return customerName;
            if (string.IsNullOrEmpty(customerPhone))
                return "CONNECTED";
            return NoProblem.Core.BusinessLogic.Dialer.PhoneUtility.GetPhoneDisplay(customerPhone);
        }
        private decimal SetupLostBy(decimal diff)
        {
            if (diff < 1)
                return 1;
            else
                return diff;
        }
        private const string getLeadsQuery = @"
select top 500 ia.CreatedOn
,ia.statuscode 
, ia.new_incidentaccountid 
, ia.new_refundstatus
, R.New_name region
, ex.New_name category
,inc.New_VideoDuration
,inc.New_PreviewVideoPicUrl
,inc.New_videoUrl
,c.new_profileimageurl
,c.MobilePhone
,c.FirstName
,inc.CaseTypeCode
from dbo.incident inc with(nolock)
inner join dbo.new_incidentaccount ia with(nolock) on ia.new_incidentid = inc.incidentid
inner join dbo.New_regionExtensionBase R with(nolock) on R.New_regionId = inc.new_regionid
inner join Contact c with(nolock) on c.ContactId = inc.CustomerId
inner join dbo.New_primaryexpertiseExtensionBase ex with(nolock) on ex.New_primaryexpertiseId = inc.new_primaryexpertiseid
where inc.DeletionStateCode = 0 and ia.DeletionStateCode = 0
	and ia.new_accountid = @supplierId
    {where}
order by ia.CreatedOn desc";
        /*
        private const string getLeadsQuery = @"
select top 500 ia.CreatedOn
, case ia.statuscode 
when 16 then 'LOST'
when 10 then 'WON'
when 1 then 'NEW'
when 14 then 'EVALUATING'
when 19 then 'MISSED'
when 20 then 'REJECT'
else 'LOST' 
end as 'status'
, ia.new_incidentaccountid 
, ia.new_refundstatus
, R.New_name region
, ex.New_name category
,inc.New_VideoDuration
,inc.New_PreviewVideoPicUrl
,inc.New_videoUrl
,c.new_profileimageurl
,c.MobilePhone
,c.FirstName
,inc.CaseTypeCode
from dbo.incident inc with(nolock)
inner join dbo.new_incidentaccount ia with(nolock) on ia.new_incidentid = inc.incidentid
inner join dbo.New_regionExtensionBase R with(nolock) on R.New_regionId = inc.new_regionid
inner join Contact c with(nolock) on c.ContactId = inc.CustomerId
inner join dbo.New_primaryexpertiseExtensionBase ex with(nolock) on ex.New_primaryexpertiseId = inc.new_primaryexpertiseid
where inc.DeletionStateCode = 0 and ia.DeletionStateCode = 0
	and ia.new_accountid = @supplierId
    {where}
order by ia.CreatedOn desc";
       */

        public GetLeadForMobileResponse GetLead(Guid supplierId, Guid IncidentAccountId)
        {
            GetLeadForMobileResponse response = new GetLeadForMobileResponse();
            DataAccessLayer.Generic.SqlParametersList parameters =
                new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            parameters.Add("@IncidentAccountId", IncidentAccountId);
            var reader = dal.ExecuteReader(getSingleLeadQuery, parameters);
            foreach (var row in reader)
            {
                bool refunded = (row["new_refundstatus"] != DBNull.Value && (DataModel.Xrm.new_incidentaccount.RefundSatus)((int)row["new_refundstatus"]) == DataModel.Xrm.new_incidentaccount.RefundSatus.Approved);

                response.Category = new SupplierCategory();
                int accountStatus = (int)row["AccountStatus"];
                if ((int)row["statuscode"] == (int)NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.MATCH)
                {
                    response.Category.MinPrice = row["categoryMinPrice"] != DBNull.Value ? (decimal)Convert.ToDouble(row["categoryMinPrice"]) : MOBILE_DEFAULT_MIN_PRICE;
                    response.Category.MediumPrice = row["categoryMediumPrice"] != DBNull.Value ? (decimal)Convert.ToDouble(row["categoryMediumPrice"]) : MOBILE_DEFAULT_MED_PRICE;
                    response.Category.MaxPrice = row["categoryMaxPrice"] != DBNull.Value ? (decimal)Convert.ToDouble(row["categoryMaxPrice"]) : MOBILE_DEFAULT_MAX_PRICE;
      //              response.IsFree = accountStatus == (int)NoProblem.Core.DataModel.Xrm.account.SupplierStatus.Trial;
                    NoProblem.Core.DataAccessLayer.AccountRepository accDal = new DataAccessLayer.AccountRepository(null);
                    response.IsFree = accDal.HaveFreeMobileLead(supplierId);
                }
                response.Category.Name = Convert.ToString(row["category"]);
                /*
                response.Category = new SupplierCategory
                {
                    MaxPrice = categoryMaxPrice,
                    MediumPrice = categoryMediumPrice,
                    MinPrice = categoryMinPrice,
                    Name = categoryName
                };
                 * */
          //      response.Description = Convert.ToString(row["description"]);
                string phone = Convert.ToString(row["phoneNumber"]);
                response.Status = refunded ? "REFUNDED" : Convert.ToString(row["status"]);
                response.Price = row["price"] != DBNull.Value ? (decimal)row["price"] : 0;
                response.VideoUrl = row["New_videoUrl"] != DBNull.Value ? (string)row["New_videoUrl"] : string.Empty;
                response.PreviewVideoImageUrl = row["New_PreviewVideoPicUrl"] != DBNull.Value ? (string)row["New_PreviewVideoPicUrl"] : string.Empty;
                response.Videoduration = row["New_VideoDuration"] != DBNull.Value ? (int)row["New_VideoDuration"] : 0;
                response.ZipCode = row["zipCode"] != DBNull.Value ? (string)row["zipCode"] : string.Empty;
                response.CustomerProfileImage = row["new_profileimageurl"] != DBNull.Value ? (string)row["new_profileimageurl"] : null;
                bool toCharge = row["toCharge"] != DBNull.Value ? (bool)row["toCharge"] : false;
                int? refundStatus = row["new_refundstatus"] != DBNull.Value ? (int)row["new_refundstatus"] : (int?)null;
                if (!toCharge)
                {
                    if (phone != null && phone.Length >= 6)
                        phone = phone.Substring(0, 6) + "...";
                    else
                        phone = "-";
                    response.CustomerName = "-";
                    if (response.Status == "LOST")
                    {
                        response.LostReason = GetLostReason(row, response.Price);
                    }
                    else if (response.Status == "EVALUATING")
                    {
                        /*
                        if (row["evaluating_step_2"] != DBNull.Value && (bool)row["evaluating_step_2"])
                        {
                            response.EvaluatingText = "You are among the winning pros. We will be connecting you soon. Please be available.";
                        }
                        else
                        {
                            response.EvaluatingText = "Thank you! We are evaluating. You will get notified asap";
                        }
                         * */
                        response.EvaluatingText = "You're among the top pros for this job. We'll be connecting you ASAP. Please be available to accept the call.";
                    }
                }
                else//tocharge
                {
                    response.CustomerName = Convert.ToString(row["edited_cust_name"]);
                    if (String.IsNullOrEmpty(response.CustomerName))
                    {
                        string _name = row["customerName"] == DBNull.Value ? null : (string)row["customerName"];
                        /*
                        response.CustomerName = Convert.ToString(row["customerName"]);
                        if (String.IsNullOrEmpty(response.CustomerName))
                            response.CustomerName = "-";
                         */
                        response.CustomerName = _name;// GetCustomerName(_name);
                    }
                    response.Address = Convert.ToString(row["edited_address"]);
                }
                if (String.IsNullOrEmpty(response.Address))
                {
                    /*
                    string fullRegionNameWithSemicolons = Convert.ToString(row["full_region_name"]);
                    string[] splittedRegionName = fullRegionNameWithSemicolons.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    response.Address = splittedRegionName[1] + ", " + splittedRegionName[3];
                     */
                    response.Address = GetResponseAddress(Convert.ToString(row["full_region_name"]));
                }
                response.PhoneNumber = phone;
                response.CreatedOn = (DateTime)row["createdon"];
                bool customerDisallowedRecording = false;
                RecordingInLead ril_first = null;
                if (response.Status == "WON")
                {
                    string mainRecordingUrl = Convert.ToString(row["main_recording"]);
                    customerDisallowedRecording = row["customerdisallowedrecording"] != DBNull.Value ? (bool)row["customerdisallowedrecording"] : false;
                    if (!String.IsNullOrEmpty(mainRecordingUrl) && !customerDisallowedRecording)
                    {
                        int duration = row["main_recording_duration"] != DBNull.Value ? (int)row["main_recording_duration"] : 0;
                        /*
                        response.Recordings.Add(new RecordingInLead()
                        {
                            CreatedOn = response.CreatedOn,
                            Url = mainRecordingUrl,
                            Duration = duration
                        });
                         * */
                        ril_first = new RecordingInLead()
                        {
                            CreatedOn = response.CreatedOn,
                            Url = mainRecordingUrl,
                            Duration = duration
                        };
                    }
                    DataAccessLayer.CallRecording recordingsDal = new DataAccessLayer.CallRecording(null);
                    var recordings = from rec in recordingsDal.All
                                     where rec.new_incidentaccountid == IncidentAccountId
                                     select new RecordingInLead { CreatedOn = rec.createdon.Value, Duration = rec.new_duration.HasValue ? rec.new_duration.Value : 0, Url = rec.new_url };
              //      var recordingsOrdered = recordings.ToList().OrderByDescending(x => x.CreatedOn);//doing ToList before because XRM does not support sorting.
                    List<RecordingInLead> ListRecording = recordings.ToList();
                    if(ril_first != null)
                        ListRecording.Add(ril_first);
                    var recordingsOrdered = ListRecording.OrderByDescending(x => x.CreatedOn);
                    foreach (var item in recordingsOrdered)
                    {
                        response.Recordings.Add(item);
                    }
                }
                response.CanRefund = !refundStatus.HasValue;
           //     response.ShowLinkToSettings = toCharge && !response.CanRefund && !RecordCallsAreEnabled(row) && !customerDisallowedRecording;
                response.IncidentAccountId = IncidentAccountId;//(Guid)row["IncidentId"];
                break;
            }
            return response;
        }
        public string GetResponseAddress(string fullRegionNameWithSemicolons)
        {
            string[] splittedRegionName = fullRegionNameWithSemicolons.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            string result = null;
            if (splittedRegionName.Length > 3)
                result = splittedRegionName[1] + ", " + splittedRegionName[3];
            else if (splittedRegionName.Length == 3)
                result = splittedRegionName[0] + ", " + splittedRegionName[2];
            else if (splittedRegionName.Length == 2)
                result = splittedRegionName[0] + ", " + splittedRegionName[1];
            else
                result = splittedRegionName[0];
            return result;
        }
        private string GetCustomerName(string name)
        {
            if (string.IsNullOrEmpty(name))
                return "-";
            if (name.Length < 5)
                return "-";
            return name.Substring(0, name.Length - 4) + "****";
        }
        private string GetLostReason(Dictionary<string, object> row, decimal myPrice)
        {
            bool leadAccepted = row["lead_accepted"] != DBNull.Value ? (bool)row["lead_accepted"] : false;
            if (leadAccepted)
            {
                decimal winningPrice = row["winning_price"] != DBNull.Value ? (decimal)row["winning_price"] : myPrice + 1;
                if (winningPrice <= myPrice)
                    winningPrice = myPrice + 1;
                return "Winning Bid: $" + winningPrice.ToString("0.##");
            }
            bool leadDeclined = row["lead_rejected"] != DBNull.Value ? (bool)row["lead_rejected"] : false;
            if (leadDeclined)
            {
                return "Declined";
            }
            return "Missed";
        }

        private bool RecordCallsAreEnabled(Dictionary<string, object> row)
        {
            return row["record_calls"] != DBNull.Value ? (bool)row["record_calls"] : false;
        }
        /*
         * hen 16 then 'LOST'
when 10 then 'WON'
when 1 then 'NEW'
when 14 then 'EVALUATING'
when 19 then 'MISSED'
when 20 then 'REJECT'*/
        private const string getSingleLeadQuery =
            @"
select top 1
inc.description
,inc.new_primaryexpertiseidname as 'category'
, ia.CreatedOn as 'createdon'
,ia.statuscode
, case ia.statuscode 
when 16 then 'LOST'
when 10 then 'WON'
when 1 then 'NEW'
when 14 then 'EVALUATING'
when 19 then 'MISSED'
when 20 then 'REJECT'
else 'LOST' 
end as 'status'
, inc.New_telephone1 as 'phoneNumber'
, ia.New_potentialincidentprice as 'price'
, reg.new_englishname as 'full_region_name'
, reg.New_name as 'zipCode'
, ia.new_tocharge as 'toCharge'
, ia.new_refundstatus
, c.fullname as 'customerName'
, ia.New_recordfilelocation as 'main_recording'
, ia.new_recordduration as 'main_recording_duration'
, ia.New_LeadAccepted as 'lead_accepted'
, ia.New_isrejected as 'lead_rejected'
, acc.new_recordcalls as 'record_calls'
, acc.new_status as 'AccountStatus'
, inc.new_winningprice as 'winning_price'
, inc.IncidentId
,inc.New_VideoDuration
,inc.New_PreviewVideoPicUrl
,inc.New_videoUrl
, ia.new_adveditedaddress as 'edited_address'
, ia.new_adveditedcustomername as 'edited_cust_name'
, ia.new_evaluatingstep2 as 'evaluating_step_2'
, ia.new_customerdisallowedrecording as 'customerdisallowedrecording'
, pe.New_mobileMaxPrice  as categoryMaxPrice
, pe.New_mobileMinPrice as categoryMinPrice
, pe.New_mobileMedPrice as categoryMediumPrice
,c.new_profileimageurl
from incident inc with(nolock)
join new_incidentaccount ia with(nolock) on inc.incidentid = ia.new_incidentid
join dbo.New_primaryexpertiseExtensionBase pe with(nolock) on pe.New_primaryexpertiseId = inc.new_primaryexpertiseid
join Contact c with(nolock) on c.ContactId = inc.CustomerId
join new_region reg with(nolock) on inc.new_regionid = reg.new_regionid
join account acc with(nolock) on acc.accountid = ia.new_accountid
where
ia.new_accountid = @supplierId
and ia.new_incidentaccountid = @IncidentAccountId
";
    }
}
