﻿using NoProblem.Core.DataModel.SupplierModel.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL
{
    public class UserAlertsManager : XrmUserBase
    {
        public UserAlertsManager(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {

        }

        private static class AlertTypesNames
        {
            public const string PAYMENT_FAILED = "PAYMENT_FAILED";
        }

        private static readonly Dictionary<string, string> alertTypes =
            new Dictionary<string, string>()
            {
                {AlertTypesNames.PAYMENT_FAILED, "Your last payment failed!"}
            };

        public GetUserAlertsResponse GetUserAlerts(Guid supplierId)
        {
            GetUserAlertsResponse response = new GetUserAlertsResponse();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var data = (from acc in dal.All
                        where acc.accountid == supplierId
                        select new { PaymentFailed = acc.new_paymentmethodfailed.HasValue ? acc.new_paymentmethodfailed.Value : false }
                                ).First();
            if (data.PaymentFailed)
            {
                response.Alerts.Add(new GetUserAlertsResponse.Alert() { Type = AlertTypesNames.PAYMENT_FAILED, Text = alertTypes[AlertTypesNames.PAYMENT_FAILED] });
            }
            return response;
        }
    }
}
