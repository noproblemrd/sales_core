﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic
{
    public class RealTimeRoutingErrorPrevention : XrmUserBase
    {
        public RealTimeRoutingErrorPrevention()
            : base(null)
        {
            dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string hoursBackStr = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.REALTIMEROUTINGERRORPREVENTION_HOURS_BACK);
            if (!int.TryParse(hoursBackStr, out hoursBack))
            {
                hoursBack = -1;
            }
            string dropAllowedStr = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.REALTIMEROUTINGERRORPREVENTION_DROP_ALLOWED);
            if (!decimal.TryParse(dropAllowedStr, out dropAllowed))
            {
                dropAllowed = new decimal(50);
            }
            string emailsConfig = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.REALTIMEROUTINGERRORPREVENTION_EMAILS);
            string[] splited = emailsConfig.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            emails = new List<string>();
            foreach (string item in splited)
            {
                emails.Add(item);
            }
        }

        private static Timer timer;
        private DataAccessLayer.AccountRepository dal;
        private int hoursBack;
        private decimal dropAllowed;
        private List<string> emails;

        public static void SetTimerOnAppStart()
        {
            if (timer == null)
            {
                timer = new Timer(
                    TimerCallback,
                    null,
                    CalculateFirstInvoke(),
                    new TimeSpan(0, 30, 0)
                    );
            }
        }

        private static TimeSpan CalculateFirstInvoke()
        {
            DateTime now = DateTime.Now;
            DateTime runOn;
            if (now.Minute < 30)
            {
                runOn = new DateTime(now.Year, now.Month, now.Day, now.Hour, 30, 0);
            }
            else
            {
                runOn = new DateTime(now.Year, now.Month, now.Day, now.Hour, 0, 0).AddHours(1);
            }
            return runOn - now;
        }

        private static void TimerCallback(object obj)
        {
            try
            {
                RealTimeRoutingErrorPrevention instance = new RealTimeRoutingErrorPrevention();
                instance.Check();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RealTimeRoutingErrorPrevention.TimerCallback.");
            }
        }

        public void Check()
        {
            if (hoursBack <= 0)
                return;
            IEnumerable<Guid> accountsToCheck = GetAccountsToCheck();
            foreach (var accountId in accountsToCheck)
            {
                try
                {
                    CheckAccount(accountId);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception checking account in RealTimeRoutingErrorPrevention. Going to next account. accountId = {0}", accountId);
                }
            }
        }

        private void CheckAccount(Guid accountId)
        {
            return;
            /* remove at 05-01-2016 to avoid close account
            decimal monthAccRate = GetLastMonthsAcceptanceRate(accountId);
            decimal currentAccRate = GetCurrentAcceptanceRate(accountId);
            if (monthAccRate < 0 || currentAccRate < 0)
                return;
            if (monthAccRate - dropAllowed > currentAccRate)
            {
                CloseAccount(accountId, monthAccRate, currentAccRate);
            }
             * */
        }

        private void CloseAccount(Guid accountId, decimal monthAccRate, decimal currentAccRate)
        {
            var account = dal.Retrieve(accountId);
            LogUtils.MyHandle.WriteToLog("Closing account {0} because of drop in acceptance rate. accountId = {1}", account.name, accountId);
            Notifications notifier = new Notifications(XrmDataContext);
            string body = String.Format(emailBody, account.name, dropAllowed, currentAccRate, hoursBack, monthAccRate);
            notifier.SendEmail(emails, body, String.Format(emailSubject, account.name));

            account.new_status = (int)DataModel.Xrm.account.SupplierStatus.Inactive;
            dal.Update(account);
            XrmDataContext.SaveChanges();

            Slack.SlackNotifier slack = new Slack.SlackNotifier(body);
            slack.Start();
        }

        private decimal GetCurrentAcceptanceRate(Guid accountId)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@accountId", accountId);
            parameters.Add("@hoursBack", hoursBack * -1);
            object scalar = dal.ExecuteScalar(queryGetCurrentAcceptanceRate, parameters);
            decimal retVal = -1;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (decimal)scalar;
            }
            return retVal;
        }

        private decimal GetLastMonthsAcceptanceRate(Guid accountId)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@accountId", accountId);
            object scalar = dal.ExecuteScalar(queryMonthAcceptanceRate, parameters);
            decimal retVal = -1;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (decimal)scalar;
            }
            return retVal;
        }

        private IEnumerable<Guid> GetAccountsToCheck()
        {
            var reader = dal.ExecuteReader(suppliersQuery);
            List<KeyValuePair<Guid, Guid>> pairs = new List<KeyValuePair<Guid, Guid>>();
            foreach (var row in reader)
            {
                pairs.Add(
                    new KeyValuePair<Guid, Guid>((Guid)row["accountid"], (Guid)row["new_primaryexpertiseid"]));
            }
            var groupedByHeading = pairs.GroupBy(x => x.Value);
            foreach (var item in groupedByHeading)
            {
                if (item.Count() == 1)
                {
                    pairs.RemoveAll(x => x.Value == item.Key);
                }
            }
            HashSet<Guid> retVal = new HashSet<Guid>();
            foreach (var item in pairs)
            {
                retVal.Add(item.Key);
            }
            return retVal;
        }

        private const string emailBody = "Account {0} has been closed because his acceptance rate has dropped more than {1}. Current acceptance rate = {2} (for the last {3} hours). Month's acceptance rate = {4}.";
        private const string emailSubject = "Account {0} has been closed because of drop in acceptance rate!!!";

        private const string queryGetCurrentAcceptanceRate =
            @"
declare @success decimal
declare @all decimal

select @success = COUNT(*)
from new_incidentaccount ia WITH (NOLOCK) 
where ia.statuscode in (10, 13)
and ia.createdon > DATEADD(HOUR, @hoursBack, getdate())
and ia.new_accountid = @accountId



select @all = COUNT(*)
from new_incidentaccount ia WITH (NOLOCK)
where (ia.statuscode in (10, 13) or (ia.statuscode = 16 and ( ia.New_recordid is not null or ia.New_DialerCallId is not null )))
and ia.createdon > DATEADD(HOUR, @hoursBack, getdate())
and ia.new_accountid = @accountId

if @all > 9
select @success / @all * 100, @all, @success
else
select cast(-1 as decimal)
";

        private const string queryMonthAcceptanceRate =
            @"
select (cast(sum(successCnt) as decimal) / cast(sum(allCnt) as decimal) * 100.0) as acceptanceRate from tbl_partners_acceptance_rate_aggregation
            where createdon > DATEADD(MONTH, -1, GETDATE())
            and tbl_partners_acceptance_rate_aggregation.accountId = @accountId
";

        private const string suppliersQuery =
            @"
select accountid, pe.new_primaryexpertiseid
from Account acc
join new_accountexpertise ae on ae.new_accountid = acc.AccountId
join new_primaryexpertise pe on pe.New_primaryexpertiseId = ae.new_primaryexpertiseid
where 
New_IsLeadBuyer = 1
and New_status = 1
and 
(acc.new_unavailablefrom > getdate() OR acc.new_unavailableto < getdate() OR
(acc.new_unavailablefrom IS NULL AND acc.new_unavailableto IS NULL)) 
and ae.new_incidentprice <= acc.new_availablecashbalance
and New_LeadBuyerName = 'phone'
and ae.new_primaryexpertiseid in (
select distinct new_primaryexpertiseid
from 
New_directnumber
where
New_Realtimerouting = 1
and DeletionStateCode = 0
)
and pe.new_campaignmode = 2
and ae.statecode = 0
and ae.DeletionStateCode = 0
and pe.DeletionStateCode = 0
and acc.DeletionStateCode = 0
order by ae.new_primaryexpertiseidName
";
    }
}
