﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.SupplierBL
{
    internal class BonusGiver : Runnable
    {
        private Guid supplierId;
        private DataAccessLayer.ConfigurationSettings config;
        private DataAccessLayer.AccountRepository accountRepositoryDal;

        public BonusGiver(Guid supplierId)
        {
            this.supplierId = supplierId;
            config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
            accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
        }

        protected override void Run()
        {
            GiveRegistrationBonusIfNeeded();
        }

        private void GiveRegistrationBonusIfNeeded()
        {
            if (NeedsBonus())
            {
                SupplierPricing.SupplierPricingManager spManager = new SupplierPricing.SupplierPricingManager(XrmDataContext);
                var spRequest = BuildRequest();
                var spResponse = spManager.CreateSupplierPricing(spRequest);
                if (spResponse.DepositStatus != NoProblem.Core.DataModel.SupplierPricing.eDepositStatus.OK)
                {
                    LogUtils.MyHandle.WriteToLog("Unable to give bonus money to {0}. DepositStatus = {1}", supplierId, spResponse.DepositStatus);
                }
            }
        }

        private bool NeedsBonus()
        {
            var query = from ac in accountRepositoryDal.All
                        where ac.accountid == supplierId
                        select new { ac.new_isemailverified, ac.new_status, ac.new_subscriptionplan };
            var found = query.FirstOrDefault();
            return 
                found != null 
                && found.new_isemailverified.HasValue 
                && found.new_isemailverified.Value 
                && found.new_status.HasValue 
                && found.new_status.Value == (int)DataModel.Xrm.account.SupplierStatus.Approved
                && found.new_subscriptionplan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING;
        }

        private DataModel.SupplierPricing.CreateSupplierPricingRequest BuildRequest()
        {
            var spRequest = new DataModel.SupplierPricing.CreateSupplierPricingRequest();
            spRequest.SupplierId = supplierId;
            spRequest.Action = DataModel.Xrm.new_balancerow.Action.BonusCredit;
            spRequest.BonusAmount = GetBonusAmount();
            spRequest.PaymentMethod = DataModel.Xrm.new_paymentmethod.ePaymentMethod.Voucher;
            string voucherNumber = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AAR_VOUCHER_NUMBER);
            Guid voucherReasonId = new Guid(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AAR_VOUCHER_REASON_ID));
            spRequest.VoucherNumber = voucherNumber;
            spRequest.VoucherReasonId = voucherReasonId;
            spRequest.BonusType = NoProblem.Core.DataModel.Xrm.new_supplierpricing.BonusType.None;
            return spRequest;
        }

        private int GetBonusAmount()
        {
            return GlobalConfigurations.GetRegistrationBonusAmount();            
        }
    }
}
