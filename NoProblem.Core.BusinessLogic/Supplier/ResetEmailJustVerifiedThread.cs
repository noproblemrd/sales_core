﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL
{
    internal class ResetEmailJustVerifiedThread : Runnable
    {
        private Guid supplierId;
        private DataAccessLayer.AccountRepository dal;

        public ResetEmailJustVerifiedThread(Guid supplierId)
        {
            this.supplierId = supplierId;
            dal = new DataAccessLayer.AccountRepository(XrmDataContext);
        }

        protected override void Run()
        {
            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.accountid = supplierId;
            supplier.new_emailjustverified = false;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }
    }
}
