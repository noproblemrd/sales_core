﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class NormalLogOnManager : LogOnManagerBase
    {
        public NormalLogOnManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
        }
        
        public LogOnResponse CreateUser(NormalLogOnRequest request)
        {
            ValidateRequest(request);
            var query = from acc in dal.All
                        where acc.emailaddress1 == request.Email
                        select new { Id = acc.accountid };
            if (query.FirstOrDefault() != null)
            {
                return new LogOnResponse()
                {
                    StatusCode = LogOnResponse.StatusCodes.ANOTHER_ACCOUNT_ALREADY_HAS_THIS_EMAIL
                };
            }
            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.emailaddress1 = request.Email;
            supplier.new_password = request.Password;
            supplier.new_recordcalls = true;
            int? step = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
            supplier.new_stepinregistration2014 = step;
            supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Candidate;
            if (request.ReferrerId != Guid.Empty)
            {
                supplier.new_referrerid = request.ReferrerId;
            }
            supplier.new_comefrom = request.ComeFrom;
            if (request.RegistrationHitId != Guid.Empty)
                supplier.new_registrationhitsid = request.RegistrationHitId.ToString();
            dal.Create(supplier);
            XrmDataContext.SaveChanges();
            SendEmailVerificationEmail(request.Email, request.Password, supplier.accountid, false, false, String.Empty);
            LogOnResponse response = new LogOnResponse();
            /*
            if (request.IsFromMobileApp)
            {
                step = MobileAppStepHandle(request, supplier.accountid, supplier.new_stepinregistration2014);
                string token = GetMobileToken(request, supplier.accountid);
                response.ApiToken = token;
            }
             * */
            response.Step = step.Value;
            response.SupplierId = supplier.accountid;
            return response;
        }
        
        
        public LogOnResponse LogIn(NormalLogOnRequest request)
        {
            ValidateRequest(request);
            var query = from acc in dal.All
                        where acc.emailaddress1 == request.Email
                        && acc.new_password == request.Password
                        && acc.new_securitylevel == null
                        select new
                        {
                            Id = acc.accountid,
                            Step = acc.new_stepinregistration2014,
                            Status = acc.new_status,
                            ContactName = acc.new_fullname,
                            Name = acc.name,
                            Balance = acc.new_cashbalance,
                            FacebookId = acc.new_facebookid,
                            GoogleId = acc.new_googleid,
                            IsLegacy = acc.new_legacyadvertiser2014.HasValue && acc.new_legacyadvertiser2014.Value,
                            LegacyStep = acc.new_steplegacy2014
                        };
            var foundSupplier = query.FirstOrDefault();
            if (foundSupplier == null)
            {
                return new LogOnResponse()
                {
                    StatusCode = LogOnResponse.StatusCodes.WRONG_EMAIL_PASSWORD_COMBINATION
                };
            }
            if (foundSupplier.Status == (int)DataModel.Xrm.account.SupplierStatus.Inactive)
            {
                return new LogOnResponse()
                {
                    StatusCode = LogOnResponse.StatusCodes.INACTIVE_ACCOUNT,
                    ContactName = foundSupplier.ContactName,
                    Name = foundSupplier.Name,
                    SupplierId = foundSupplier.Id
                };
            }
            int step;
            if (foundSupplier.Step.HasValue)
            {
                step = foundSupplier.Step.Value;
            }
            else//step has no value
            {
                step = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
            }
            var response = new LogOnResponse()
            {
                SupplierId = foundSupplier.Id,
                ContactName = foundSupplier.ContactName,
                Name = foundSupplier.Name,
                Balance = foundSupplier.Balance.HasValue ? foundSupplier.Balance.Value : 0,
                HasFacebook = !String.IsNullOrEmpty(foundSupplier.FacebookId),
                HasGoogle = !String.IsNullOrEmpty(foundSupplier.GoogleId)
            };
            if (request.IsFromMobileApp)
            {
                /*
                step = MobileAppStepHandle(request, foundSupplier.Id, step).Value;
                string token = GetMobileToken(request, foundSupplier.Id);
                response.ApiToken = token;
                 * */
            }
            else if (foundSupplier.IsLegacy && foundSupplier.LegacyStep != DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.FINISHED_3)
            {
                response.StatusCode = LogOnResponse.StatusCodes.SEND_TO_LEGACY_PROCESS;
            }
            response.Step = step;
            return response;
        }
        
        private static void ValidateRequest(NormalLogOnRequest request)
        {
            if (String.IsNullOrEmpty(request.Email))
                throw new ArgumentException("Email cannot be null nor empty.", "Email");
            if (String.IsNullOrEmpty(request.Password))
                throw new ArgumentException("Password cannot be null nort empty.", "Password");
        }
    }
}
