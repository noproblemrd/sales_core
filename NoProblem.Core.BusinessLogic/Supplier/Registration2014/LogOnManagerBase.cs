﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public abstract class LogOnManagerBase : XrmUserBase
    {
        protected DataAccessLayer.AccountRepository dal;

        public LogOnManagerBase(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            dal = new DataAccessLayer.AccountRepository(XrmDataContext);
        }

        protected void SendEmailVerificationEmail(string email, string password, Guid supplierId, bool isSocial, bool isFacebook, string contactName)
        {
            EmailVerificationEmailSender sender = new EmailVerificationEmailSender(email, password, supplierId, isSocial, isFacebook, contactName);
            sender.Start();
        }

        protected int? MobileAppStepHandle(LogOnRequest request, Guid supplierId, int? step)
        {
            if (request.IsFromMobileApp && step <= (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2)
            {
                ChoosePlanManager cpManager = new ChoosePlanManager(XrmDataContext);
                var cpResult = cpManager.ChoosePlan(new ChoosePlanRequest() { SupplierId = supplierId, PlanSelected = SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING });
                step = cpResult.Step;
            }
            return step;
        }
        /*
        protected string GetMobileToken(LogOnRequest request, Guid supplierId)
        {
            APIs.MobileAPI.Devices.DevicesManager devicesManager = new APIs.MobileAPI.Devices.DevicesManager(XrmDataContext);
            string token = devicesManager.DeviceLogin(supplierId, Guid.Empty, request.MobileLogOnData.DeviceName, request.MobileLogOnData.DeviceUId, request.MobileLogOnData.DeviceOS, request.MobileLogOnData.DeviceOSVersion, request.MobileLogOnData.NpAppVersion);
            return token;
        }
         * */
    }
}
