﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    internal class EmailVerificationEmailSender : Runnable
    {
        private string email;
        private string password;
        private Guid supplierId;
        private bool isFromGettingStarted;
        private bool isSocial;
        private bool isFacebook;
        private string contactName;
        private bool sentOk;

        public EmailVerificationEmailSender(string email, string password, Guid supplierId, bool isSocial, bool isFacebook, string contactName)
        {
            isFromGettingStarted = false;
            this.email = email;
            this.password = password;
            this.supplierId = supplierId;
            this.isSocial = isSocial;
            this.isFacebook = isFacebook;
            this.contactName = contactName;
        }

        public EmailVerificationEmailSender(Guid supplierId)
        {
            isFromGettingStarted = true;
            this.supplierId = supplierId;
        }

        internal bool SendSync()
        {
            Run();
            return sentOk;
        }

        protected override void Run()
        {
            if (isFromGettingStarted)
            {
                SendEmailFromGettingStarted();
            }
            else
            {
                SendEmailFromRegistration();
            }
        }

        private void SendEmailFromGettingStarted()
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new { Email = acc.emailaddress1, Name = acc.name };
            var found = query.FirstOrDefault();
            if (found == null)
                return;
            email = found.Email;
            Dictionary<string, string> vars = new Dictionary<string, string>();
            vars.Add("NPVAR_VERIFY_URL", GetVerificationUrl());
            vars.Add("NPVAR_AD_EMAIL", email);
            vars.Add("NPVAR_AD_NAME", found.Name);
            vars.Add("NPVAR_BONUS_AMOUNT", GlobalConfigurations.GetRegistrationBonusAmount().ToString());
            string templateName = DataModel.MandrillTemplates.E_AD_EmailVerification_Getting_Started;
            DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
            string subject = subjectsConfig.GetEmailSubject(templateName);
            MandrillSender sender = new MandrillSender();
            sentOk = sender.SendTemplate(email, subject, templateName, vars);
        }

        private void SendEmailFromRegistration()
        {
            //send through mailchimp with template.
            Dictionary<string, string> vars = new Dictionary<string, string>();
            vars.Add("NPVAR_VERIFY_URL", GetVerificationUrl());
            vars.Add("NPVAR_AD_EMAIL", email);
            string templateName;
            if (isSocial)
            {
                vars.Add("NPVAR_AD_NAME", contactName);
                vars.Add("NPVAR_AD_PASSWORD", password);
                vars.Add("NPVAR_SOCIAL_USED", isFacebook ? "Facebook" : "Google");
                templateName = DataModel.MandrillTemplates.E_AD_EmailVerificaiton_Social_Registration2014;
            }
            else
            {
                templateName = DataModel.MandrillTemplates.E_AD_EmailVerificaiton_No_Social_Registration2014;
            }
            DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
            string subject = subjectsConfig.GetEmailSubject(templateName);
            MandrillSender sender = new MandrillSender();
            sentOk = sender.SendTemplate(email, subject, templateName, vars);
        }

        private string GetVerificationUrl()
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string url = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.EMAIL_VERIFICATION_URL_REGISTRATION_2014);
            return url + "?id=" + supplierId.ToString();
        }
    }
}
