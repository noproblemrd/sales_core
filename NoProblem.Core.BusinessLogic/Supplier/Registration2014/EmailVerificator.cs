﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class EmailVerificator
    {
        public int EmailVerified(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(null);
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new { Step = acc.new_stepinregistration2014, IsVerified = acc.new_isemailverified, Status = acc.new_status };
            int step = -1;
            var found = query.FirstOrDefault();
            if (found != null)
            {
                step = found.Step.Value;
                if (!found.IsVerified.HasValue || !found.IsVerified.Value)
                {
                    DataModel.Xrm.account supplier = new DataModel.Xrm.account();
                    supplier.accountid = supplierId;
                    supplier.new_isemailverified = true;
                    supplier.new_emailjustverified = true;
                    dal.Update(supplier);
                    dal.XrmDataContext.SaveChanges();
                    SupplierBL.BonusGiver bonusGiver = new SupplierBL.BonusGiver(supplierId);
                    bonusGiver.Start();
                    
                }
            }
            return step;
        }

        public bool ResendEmailVerificationEmail(Guid supplierId)
        {
            EmailVerificationEmailSender sender = new EmailVerificationEmailSender(supplierId);
            return sender.SendSync();
        }
    }
}
