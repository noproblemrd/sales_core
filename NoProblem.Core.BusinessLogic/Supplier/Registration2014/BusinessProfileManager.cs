﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class BusinessProfileManager : XrmUserBase
    {
        public BusinessProfileManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public NoProblem.Core.DataModel.SupplierModel.Mobile.BusinessDescriptionResponse GetBusinessDescription(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var data = (from acc in dal.All
                        where acc.accountid == supplierId
                        select new { Description = acc.description, LicenseId = acc.new_licenseid }).FirstOrDefault();
            if (data == null)
                throw new ArgumentException("supplier with given id was not found", "supplierId");
            NoProblem.Core.DataModel.SupplierModel.Mobile.BusinessDescriptionResponse response = new DataModel.SupplierModel.Mobile.BusinessDescriptionResponse();
            response.description = data.Description != null ? data.Description : String.Empty;
            if (data.LicenseId != null)
                response.licenseId = data.LicenseId;
            return response;
        }

        public void UpdateBusinessDescription(NoProblem.Core.DataModel.SupplierModel.Mobile.UpdateBusinessDescriptionRequest request)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(XrmDataContext);
            supplier.accountid = request.supplierId;
            supplier.description = string.IsNullOrEmpty(request.description)?null:request.description;
            supplier.new_licenseid = string.IsNullOrEmpty(request.licenseId) ? null : request.licenseId;

            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        public GetBusinessProfileResponse GetBusinessProfile(Guid supplierId)
        {
            GetBusinessProfileResponse response = new GetBusinessProfileResponse();
            GetBusinessProfileBasicData(supplierId, response);
            GetBusinessProfileMedia(supplierId, response);
            return response;
        }

        public void SetBusinessProfileData(SetBusinessProfileDataRequest request)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == request.SupplierId
                        select new { FullAddress = acc.new_fulladdress, TimeZoneCode = acc.address1_utcoffset };
            var found = query.FirstOrDefault();
            if (found == null)
                throw new ArgumentException("Supplier with given Id was not found.", "supplierId");
            DataModel.Xrm.account entity = new DataModel.Xrm.account();
            entity.accountid = request.SupplierId;

            entity.name = request.Name;
            bool timeZoneChanged = false;
            if (found.FullAddress != request.FullAddress)
            {
                entity.new_fulladdress = request.FullAddress;
                entity.new_latitude = request.Latitude.ToString();
                entity.new_longitude = request.Longitude.ToString();
                int newTimeZoneCode = CoverAreas.MapUtils.GetTimeZoneCodeFromLatitudeAndLongitude(request.Latitude, request.Longitude, XrmDataContext);
                if (found.TimeZoneCode.Value != newTimeZoneCode)
                {
                    entity.address1_utcoffset = newTimeZoneCode;
                    timeZoneChanged = true;
                }
            }
            if (!String.IsNullOrEmpty(request.ContactPerson))
                entity.new_fullname = request.ContactPerson;
            if (!String.IsNullOrEmpty(request.Phone))
                entity.telephone1 = request.Phone;
            entity.fax = request.Fax;
            //entity.emailaddress1 = request.Email; //Email is the id, should not be changed that easily.
            entity.websiteurl = request.Website;
            entity.description = request.About;
            entity.numberofemployees = request.NumberOfEmployees;
            entity.new_accountcompleteness_mydetails = true;

            entity.new_paywithamex = request.PayWithAMEX;
            entity.new_paywithcash = request.PayWithCASH;
            entity.new_paywithmastercard = request.PayWithMASTERCARD;
            entity.new_paywithvisa = request.PayWithVISA;

            dal.Update(entity);
            XrmDataContext.SaveChanges();

            SaveVideos(request.SupplierId, request.Videos);

            if (timeZoneChanged)
            {
                WorkingHours.WorkingHoursManager whManager = new WorkingHours.WorkingHoursManager(XrmDataContext);
                whManager.OnTimeZoneChangedOrSummerClockChange(request.SupplierId, entity.address1_utcoffset.Value);
            }
            SEODataServices.SEOSupplierDataRetriever.ClearSupplierPageDataCache(request.SupplierId);
        }

        private void SaveVideos(Guid supplierId, List<string> videos)
        {
            DataAccessLayer.AccountMedia mediaDal = new DataAccessLayer.AccountMedia(XrmDataContext);
            var query = from m in mediaDal.All
                        where m.new_accountid == supplierId
                        && m.new_type == DataModel.Xrm.new_accountmedia.Types.VIDEO
                        select new { Id = m.new_accountmediaid, URI = m.new_uri };

            foreach (var c in query)
            {
                if (videos.Contains(c.URI))
                {
                    videos.Remove(c.URI);
                }
                else
                {
                    DataModel.Xrm.new_accountmedia entity = new DataModel.Xrm.new_accountmedia(XrmDataContext);
                    entity.new_accountmediaid = c.Id;
                    mediaDal.Delete(entity);
                }
            }

            foreach (var videoUrl in videos)
            {
                DataModel.Xrm.new_accountmedia entity = new DataModel.Xrm.new_accountmedia(XrmDataContext);
                entity.new_source = DataModel.Xrm.new_accountmedia.Source.YOUTUBE;
                entity.new_type = DataModel.Xrm.new_accountmedia.Types.VIDEO;
                entity.new_uri = videoUrl;
                entity.new_accountid = supplierId;
                mediaDal.Create(entity);
            }
            XrmDataContext.SaveChanges();
        }

        private void GetBusinessProfileMedia(Guid supplierId, GetBusinessProfileResponse response)
        {
            DataAccessLayer.AccountMedia mediaDal = new DataAccessLayer.AccountMedia(XrmDataContext);
            var mediaQuery = from media in mediaDal.All
                             where media.new_accountid == supplierId
                             select new
                             {
                                 URI = media.new_uri,
                                 Source = media.new_source,
                                 Type = media.new_type,
                                 IsLogo = media.new_islogo
                             };

            foreach (var item in mediaQuery)
            {
                if (item.Type == DataModel.Xrm.new_accountmedia.Types.PHOTO)
                {
                    if (item.IsLogo.HasValue && item.IsLogo.Value)
                    {
                        response.Logo = new MediaItem { URI = item.URI, Source = item.Source };
                    }
                    else
                    {
                        response.Photos.Add(new MediaItem { URI = item.URI, Source = item.Source });
                    }
                }
                else
                {
                    response.Videos.Add(item.URI);
                }
            }
        }

        private void GetBusinessProfileBasicData(Guid supplierId, GetBusinessProfileResponse response)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new
                        {
                            Name = acc.name,
                            FullAddress = acc.new_fulladdress,
                            ContactName = acc.new_fullname,
                            Phone = acc.telephone1,
                            Fax = acc.fax,
                            Email = acc.emailaddress1,
                            Website = acc.websiteurl,
                            Description = acc.description,
                            NumberOfEmployees = acc.numberofemployees,
                            Latitude = acc.new_latitude,
                            Longitude = acc.new_longitude,
                            PayWithAMEX = acc.new_paywithamex,
                            PayWithMASTERCARD = acc.new_paywithmastercard,
                            PayWithVISA = acc.new_paywithvisa,
                            PayWithCASH = acc.new_paywithcash
                        };
            var found = query.FirstOrDefault();
            if (found == null)
                throw new ArgumentException("Supplier with given Id was not found.", "supplierId");

            response.Name = found.Name;
            response.FullAddress = found.FullAddress;
            if (!String.IsNullOrEmpty(found.Latitude))
                response.Latitude = decimal.Parse(found.Latitude);
            if (!String.IsNullOrEmpty(found.Longitude))
                response.Longitude = decimal.Parse(found.Longitude);
            response.ContactPerson = found.ContactName;
            response.Phone = found.Phone;
            response.Fax = found.Fax;
            response.Email = found.Email;
            response.Website = found.Website;
            response.About = found.Description;
            response.NumberOfEmployees = found.NumberOfEmployees;
            response.PayWithAMEX = found.PayWithAMEX.HasValue ? found.PayWithAMEX.Value : false;
            response.PayWithMASTERCARD = found.PayWithMASTERCARD.HasValue ? found.PayWithMASTERCARD.Value : false;
            response.PayWithCASH = found.PayWithCASH.HasValue ? found.PayWithCASH.Value : false;
            response.PayWithVISA = found.PayWithVISA.HasValue ? found.PayWithVISA.Value : false;
        }
    }
}
