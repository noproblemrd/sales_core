﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CoverAreas
{
    class SpecificZipcodesMarker
    {
        List<Guid> working = new List<Guid>();
        List<Guid> partial = new List<Guid>();
        List<Guid> notWorking = new List<Guid>();
        Guid supplierId;
        HashSet<Guid> allWorkingZips;
        List<Hirarchy> workingHirarchy;
        DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
        DataModel.Xrm.systemuser currentUser;

        public SpecificZipcodesMarker(Guid supplierId, HashSet<Guid> allWorkingZips, DataModel.Xrm.DataContext xrmDataContext)
        {
            this.supplierId = supplierId;
            this.allWorkingZips = allWorkingZips;
            NoProblem.Core.DataAccessLayer.SystemUser systemUserDal = new NoProblem.Core.DataAccessLayer.SystemUser(xrmDataContext);
            currentUser = systemUserDal.GetCurrentUser();
        }

        internal void Mark()
        {
            BuildWorkingHirarcy();
            var groupByLvl1 = workingHirarchy.GroupBy(x => x.Lvl1);

            foreach (var lvl1 in groupByLvl1)
            {
                int workCount = lvl1.Count();
                int actualCount = GetChildCount(lvl1.Key, 1);
                if (actualCount == workCount)
                {
                    working.Add(lvl1.Key);
                }
                else
                {
                    partial.Add(lvl1.Key);
                    RecursivelyCheckChildren(lvl1, 2);
                }
            }
            SaveToDB();
        }

        private void SaveToDB()
        {
            StringBuilder sqlStringBuilder = new StringBuilder();
            DateTime now = DateTime.Now;
            int insertCount = 0;
            foreach (var regionId in working)
            {
                SaveToDbIteration(ref sqlStringBuilder, now, ref insertCount, regionId, DataModel.Xrm.new_regionaccountexpertise.RegionState.Working);
            }
            foreach (var regionId in partial)
            {
                SaveToDbIteration(ref sqlStringBuilder, now, ref insertCount, regionId, DataModel.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild);
            }
            foreach (var regionId in notWorking)
            {
                SaveToDbIteration(ref sqlStringBuilder, now, ref insertCount, regionId, DataModel.Xrm.new_regionaccountexpertise.RegionState.NotWorking);
            }
            if (sqlStringBuilder.Length > 0)
                dal.ExecuteNonQuery(sqlStringBuilder.ToString());
        }

        private void SaveToDbIteration(ref StringBuilder sqlStringBuilder, DateTime now, ref int insertCount, Guid regionId, DataModel.Xrm.new_regionaccountexpertise.RegionState state)
        {
            insertCount++;
            var entity = MakeDynamicRegionAccountEntity(supplierId, regionId, state);
            dal.CreateSqlInsertString(entity, currentUser, now, sqlStringBuilder);
            if (insertCount % 25 == 0)
            {
                dal.ExecuteNonQuery(sqlStringBuilder.ToString());
                sqlStringBuilder = new StringBuilder();
            }
        }



        private Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity MakeDynamicRegionAccountEntity(Guid supplierId, Guid regionId, DataModel.Xrm.new_regionaccountexpertise.RegionState state)
        {
            Effect.Crm.SDKProviders.CrmServiceSdk.DynamicEntity dynamicRegAcc =
                Effect.Crm.Convertor.ConvertorCRMParams.GetNewDynamicEntity("new_regionaccountexpertise");
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_regionstate",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmPicklist((int)state),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.PicklistProperty));
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_accountid",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("account", supplierId),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                );
            Effect.Crm.Convertor.ConvertorCRMParams.SetDynamicEntityPropertyValue(
                dynamicRegAcc,
                "new_regionid",
                Effect.Crm.Convertor.ConvertorCRMParams.GetCrmLookup("new_region", regionId),
                typeof(Effect.Crm.SDKProviders.CrmServiceSdk.LookupProperty)
                );
            return dynamicRegAcc;
        }

        private void RecursivelyCheckChildren(IGrouping<Guid, Hirarchy> parentGrouping, int currentLevel)
        {
            IEnumerable<IGrouping<Guid, Hirarchy>> groupByCurrent = GetCurrentGroupBy(parentGrouping, currentLevel);
            List<Guid> levelWorking = new List<Guid>();
            foreach (var group in groupByCurrent)
            {
                levelWorking.Add(group.Key);
                int workCount = group.Count();
                int actualCount = GetChildCount(group.Key, currentLevel);
                if (workCount < actualCount)
                {
                    partial.Add(group.Key);
                    RecursivelyCheckChildren(group, currentLevel + 1);
                }
            }
            List<Guid> complementOfLevelWorking = GetComplementOfLevel(levelWorking, currentLevel, parentGrouping.Key);
            notWorking.AddRange(complementOfLevelWorking);
        }

        private List<Guid> GetComplementOfLevel(List<Guid> levelWorkingIds, int level, Guid parentRegionId)
        {
            StringBuilder builder = new StringBuilder();
            List<Guid> complement = new List<Guid>();
            for (int i = 0; i < levelWorkingIds.Count; i++)
            {
                builder.Append("'").Append(levelWorkingIds.ElementAt(i).ToString()).Append("'");
                if (i < levelWorkingIds.Count - 1)
                    builder.Append(",");
            }
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@parentRegionId", parentRegionId);
            var reader = dal.ExecuteReader(String.Format(query_complement, level.ToString(), builder.ToString(), (level - 1).ToString()), parameters);
            foreach (var row in reader)
            {
                complement.Add((Guid)row.First().Value);
            }
            return complement;
        }

        private const string query_complement =
            @"
select distinct Lvl{0}
from RegionHierarchy
where Lvl{2} = @parentRegionId and Lvl{0} not in ( {1} )
";

        private IEnumerable<IGrouping<Guid, Hirarchy>> GetCurrentGroupBy(IGrouping<Guid, Hirarchy> parentGrouping, int currentLevel)
        {
            if (currentLevel == 2)
            {
                return parentGrouping.GroupBy(x => x.Lvl2);
            }
            else if (currentLevel == 3)
            {
                return parentGrouping.GroupBy(x => x.Lvl3);
            }
            else
            {
                return parentGrouping.GroupBy(x => x.Lvl4);
            }
        }

        private int GetChildCount(Guid regionId, int level)
        {
            if (level == 4)
                return 0;
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@regionId", regionId);
            int count = (int)dal.ExecuteScalar(String.Format(query_getChildCount, level.ToString()), parameters);
            return count;
        }

        private const string query_getChildCount =
            @"
select COUNT(*)
from RegionHierarchy
where Lvl{0} = @regionId
";

        private void BuildWorkingHirarcy()
        {
            workingHirarchy = new List<Hirarchy>();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < allWorkingZips.Count; i++)
            {
                builder.Append("'").Append(allWorkingZips.ElementAt(i).ToString()).Append("'");
                if (i < allWorkingZips.Count - 1)
                    builder.Append(",");
            }
            var reader = dal.ExecuteReader(String.Format(query_initial, builder.ToString()));
            foreach (var row in reader)
            {
                Hirarchy h = new Hirarchy();
                h.Lvl1 = (Guid)row["Lvl1"];
                h.Lvl2 = (Guid)row["Lvl2"];
                h.Lvl3 = (Guid)row["Lvl3"];
                h.Lvl4 = (Guid)row["Lvl4"];
                workingHirarchy.Add(h);
            }
        }

        private class Hirarchy
        {
            public Guid Lvl1 { get; set; }
            public Guid Lvl2 { get; set; }
            public Guid Lvl3 { get; set; }
            public Guid Lvl4 { get; set; }
        }

        private const string query_initial =
            @"
select Lvl1, Lvl2, Lvl3, Lvl4
from 
RegionHierarchy
where RegionHierarchy.Lvl4 
in
(
{0}
)";
    }
}
