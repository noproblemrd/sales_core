﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CoverAreas
{
    public class CoverAreasUpdater : XrmUserBase
    {
        private static Timer timer;
        private const int ONE_MIN = 1000 * 60;
        private const int FIVE_MINS = ONE_MIN * 5;

        public CoverAreasUpdater(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public static void OnApplicationStart()
        {
            timer = new Timer(TimerCallbackMethod, null, ONE_MIN, Timeout.Infinite);
        }

        private static void TimerCallbackMethod(object state)
        {
            try
            {
                CoverAreasUpdater updater = new CoverAreasUpdater(null);
                updater.UpdateAreas();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CoverAreasUpdater.UpdateAreas");
            }
            finally
            {
                timer = new Timer(TimerCallbackMethod, null, FIVE_MINS, Timeout.Infinite);
            }
        }

        public void UpdateAreas()
        {
            Guid supplierId = FindSupplierToUpdate();
            while (supplierId != Guid.Empty)
            {
                try
                {
                    UpdateSupplierCoverAreas(supplierId);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception updating cover areas of supplier with id {0}", supplierId);
                    MarkSupplier(supplierId, true);
                }
                supplierId = FindSupplierToUpdate();
            }
        }

        private void MarkSupplier(Guid supplierId, bool isDirty)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.accountid = supplierId;
            supplier.new_coverareasneedupdate = isDirty;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        private Guid FindSupplierToUpdate()
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.new_coverareasneedupdate.HasValue
                        && acc.new_coverareasneedupdate.Value
                        select new { acc.accountid };
            if (query.Count() == 0)
                return Guid.Empty;
            var found = query.FirstOrDefault();
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@id", found.accountid);
            int rowsUpdated = dal.ExecuteNonQuery(updateSqlString, parameters);
            if (rowsUpdated == 0)
                return FindSupplierToUpdate();
            else
                return found.accountid;
        }

        private const string updateSqlString =
            @"update account set new_coverareasneedupdate = 0 where accountid = @id and new_coverareasneedupdate = 1";

        public void UpdateSupplierCoverAreas(Guid supplierId)
        {
            HashSet<Guid> allZips;
            bool isAll = GetAllZipcodesInCoverAreas(supplierId, out allZips);
            DeleteAllCurrent(supplierId);
            if (isAll)
            {
                MarkAll(supplierId);
            }
            else
            {
                MarkSpecific(supplierId, allZips);
            }
        }

        private void DeleteAllCurrent(Guid supplierId)
        {
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.Region.SqlParametersList parameteres = new DataAccessLayer.Generic.SqlParametersList();
            parameteres.Add("@supplierId", supplierId);
            regionDal.ExecuteNonQuery(query_deleteAll, parameteres);
        }

        private bool GetAllZipcodesInCoverAreas(Guid supplierId, out HashSet<Guid> allZips)
        {
            allZips = new HashSet<Guid>();
            DataAccessLayer.CoverArea dal = new DataAccessLayer.CoverArea(XrmDataContext);
            var areas = from area in dal.All
                        where area.new_accountid == supplierId
                        && area.statecode == DataAccessLayer.CoverArea.ACTIVE
                        select new { Latitude = area.new_latitude, Longitude = area.new_longitude, Radius = area.new_radius };
            bool isAll = false;
            foreach (var area in areas)
            {
                string radiusStr = area.Radius;
                decimal latitude = area.Latitude.Value;
                decimal longitude = area.Longitude.Value;
                Guid regionId;
                List<Guid> zps;
                if (radiusStr.ToUpper() == "ALL")
                {
                    isAll = true;
                    break;
                }
                else if (radiusStr.TryParseToGuid(out regionId))
                {
                    zps = MapUtils.GetAllZipcodesInRegion(regionId);
                }
                else
                {
                    decimal radius = decimal.Parse(radiusStr);
                    zps = MapUtils.GetAllZipcodesInRadius(latitude, longitude, radius);
                }
                foreach (var zip in zps)
                {
                    allZips.Add(zip);
                }
            }
            return isAll;
        }

        private void MarkSpecific(Guid supplierId, HashSet<Guid> allZips)
        {
            SpecificZipcodesMarker specificMarker = new SpecificZipcodesMarker(supplierId, allZips, XrmDataContext);
            specificMarker.Mark();
        }

        private void MarkAll(Guid supplierId)
        {
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            var all1 = from reg in regionDal.All
                       where reg.new_level == 1
                       && reg.new_country == "UNITED STATES"
                       select new { RegionId = reg.new_regionid };
            DataAccessLayer.RegionAccount regAccDal = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
            foreach (var item in all1)
            {
                DataModel.Xrm.new_regionaccountexpertise regAcc = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
                regAcc.new_regionid = item.RegionId;
                regAcc.new_accountid = supplierId;
                regAcc.new_regionstate = (int)DataModel.Xrm.new_regionaccountexpertise.RegionState.Working;
                regAccDal.Create(regAcc);
            }
            XrmDataContext.SaveChanges();
        }

        private const string query_deleteAll =
            @"update new_regionaccountexpertise set DeletionStateCode = 2 where new_accountid = @supplierId";
    }
}
