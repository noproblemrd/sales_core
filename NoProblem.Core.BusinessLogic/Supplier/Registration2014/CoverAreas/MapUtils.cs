﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.Utils;
using System.Xml;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CoverAreas
{
    internal class MapUtils
    {
        private const string CACHE_PREFIX = "cache_map_utils";

        internal static int GetTimeZoneCodeFromLatitudeAndLongitude(decimal latitude, decimal longitude, DataModel.Xrm.DataContext xrmDataContext)
        {
            RegionUtils regionUtils = new RegionUtils();
            string areasInRadius = regionUtils.GetAreasInRadius(longitude, latitude, new decimal(0.01));
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(areasInRadius);
            XmlNodeList xRegionslist = xDoc.SelectNodes("Root/Area");
            string idStr = xRegionslist[0].Attributes["RegionId"].InnerText;
            Guid regionId = new Guid(idStr);
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(xrmDataContext);
            var regData = regionDal.GetRegionMinDataById(regionId);
            var parents = regionDal.GetParents(regData);
            var level1Parent = parents.First(x => x.Level == 1);
            int timeZoneCode = level1Parent.TimeZoneCode;
            if (timeZoneCode == -1)
                timeZoneCode = WorkingHours.WorkingHoursConstants.DEFAULT_TIME_ZONE_CODE;
            return timeZoneCode;
        }

        internal static List<Guid> GetAllZipcodesInRadius(decimal latitude, decimal longitude, decimal radius)
        {
            List<Guid> retVal = new List<Guid>();
            RegionUtils regionUtils = new RegionUtils();
            string areasInRadius = regionUtils.GetAreasInRadius(longitude, latitude, radius);
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(areasInRadius);
            XmlNodeList xRegionslist = xDoc.SelectNodes("Root/Area");
            foreach (XmlNode item in xRegionslist)
            {
                retVal.Add(new Guid(item.Attributes["RegionId"].InnerText));
            }
            return retVal;
        }

        internal static List<DataModel.SqlHelper.RegionMinData> GetMoreMapData(decimal longitude, decimal latitude)
        {
            RegionUtils regionUtils = new RegionUtils();
            string areasInRadius = regionUtils.GetAreasInRadius(longitude, latitude, new decimal(0.01));
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(areasInRadius);
            XmlNodeList xRegionslist = xDoc.SelectNodes("Root/Area");
            string idStr = xRegionslist[0].Attributes["RegionId"].InnerText;
            Guid regionId = new Guid(idStr);
            DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(null);
            var regData = regionDal.GetRegionMinDataById(regionId);
            var parents = regionDal.GetParents(regData);
            List<DataModel.SqlHelper.RegionMinData> retVal = new List<NoProblem.Core.DataModel.SqlHelper.RegionMinData>();
            foreach (var item in parents)
            {
                if (item.Level == 1 || item.Level == 2 || item.Level == 3)
                {
                    retVal.Add(item);
                }
            }
            return retVal;
        }

        internal static int GetNumberOfZipcodes(Guid regionId)
        {
            var wrapper = DataAccessLayer.CacheManager.Instance.Get<NumberOfZipcodesCacheWrapper>(CACHE_PREFIX + "_GetNumberOfZipcodes", regionId.ToString(), GetNumberOfZipcodesPrivateByRegionId, DataAccessLayer.CacheManager.ONE_YEAR);
            return wrapper.NumberOfZipcodes;
        }

        internal static int GetNumberOfZipcodes(decimal latitude, decimal longitude, decimal radius)
        {
            string cacheKey = longitude + ";" + latitude + ";" + radius;
            var wrapper = DataAccessLayer.CacheManager.Instance.Get<NumberOfZipcodesCacheWrapper>(CACHE_PREFIX + "_GetNumberOfZipcodes", cacheKey, GetNumberOfZipcodesPrivateByRadius, DataAccessLayer.CacheManager.ONE_YEAR);
            return wrapper.NumberOfZipcodes;
        }

        private const int longitudeIndex = 0;
        private const int latitudeIndex = 1;
        private const int radiusIndex = 2;

        private static NumberOfZipcodesCacheWrapper GetNumberOfZipcodesPrivateByRadius(string cacheKey)//(decimal latitude, decimal longitude, decimal radius)
        {
            string[] split = cacheKey.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            decimal longitude = decimal.Parse(split[longitudeIndex]);
            decimal latitude = decimal.Parse(split[latitudeIndex]);
            decimal radius = decimal.Parse(split[radiusIndex]);
            RegionUtils regionUtils = new RegionUtils();
            string areasInRadius = regionUtils.GetAreasInRadius(longitude, latitude, radius);
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(areasInRadius);
            XmlNodeList xRegionslist = xDoc.SelectNodes("Root/Area");
            NumberOfZipcodesCacheWrapper wrapper = new NumberOfZipcodesCacheWrapper();
            wrapper.NumberOfZipcodes = xRegionslist.Count;
            return wrapper;
        }

        private static NumberOfZipcodesCacheWrapper GetNumberOfZipcodesPrivateByRegionId(string regionId)
        {
            Guid id = new Guid(regionId);
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@regionId", id);
            object scalar = dal.ExecuteScalar(numberOfZipcodesQuery, parameters);
            int retVal = 0;
            if (scalar != null)
            {
                retVal = (int)scalar;
            }
            NumberOfZipcodesCacheWrapper wrapper = new NumberOfZipcodesCacheWrapper();
            wrapper.NumberOfZipcodes = retVal;
            return wrapper;
        }

        private const string numberOfZipcodesQuery =
            @"
select COUNT(*)
from new_region r1
join new_region r2 on r1.New_regionId = r2.new_parentregionid
left join New_region r3 on r2.New_regionId = r3.new_parentregionid
left join New_region r4 on r3.New_regionId = r4.new_parentregionid
where r1.New_regionId = @regionId
";

        public class NumberOfZipcodesCacheWrapper
        {
            public int NumberOfZipcodes { get; set; }
        }

        internal static List<Guid> GetAllZipcodesInRegion(Guid regionId)
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<Guid>>(CACHE_PREFIX + "_GetAllZipcodesInRegion", regionId.ToString(), GetAllZipcodesInRegionPrivate, DataAccessLayer.CacheManager.ONE_YEAR);
        }

        private static List<Guid> GetAllZipcodesInRegionPrivate(string regionId)
        {

            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@regionId", new Guid(regionId));
            var reader = dal.ExecuteReader(query_GetAllZipcodesInRegion, parameters);
            List<Guid> retVal = new List<Guid>();
            foreach (var row in reader)
            {
                retVal.Add((Guid)row["Lvl4"]);
            }
            return retVal;
        }

        private const string query_GetAllZipcodesInRegion =
            @"
SELECT Lvl4   
FROM RegionHierarchy  
where 
Lvl1 = @regionId
or Lvl2 = @regionId
or Lvl3 = @regionId
";
    }
}
