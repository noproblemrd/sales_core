﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;
using System.IO;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.CoverAreas
{
    public class CoverAreaManager : XrmUserBase
    {
        const decimal LatitudeNationwideSelected = 39.9997329712m;
        const decimal LongitudeNationwideSelected = -98.6785049438m;
        const string USA = "USA";
        private const string UNITED_STATES = "United States";
        internal const string ALL = "ALL";
        private const string DEFAULT_RADIUS = "30";
        private static readonly GetCoverAreaResponse.SelectedArea nationwideSelectedArea =
            new GetCoverAreaResponse.SelectedArea()
            {
                Level = ALL,
                Name = UNITED_STATES,
                RegionId = ALL
            };

        public CoverAreaManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        public GetCoverAreaResponse SetBussinessAddress(SetBussinessAddressRequest request)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = accountRepositoryDal.Retrieve(request.SupplierId);
            //   supplier.accountid = request.SupplierId;
            decimal _latitude = request.Latitude;
            decimal _longitude = request.Longitude;
            supplier.new_fulladdress = request.BusinessAddress;
            supplier.new_latitude = _latitude.ToString();
            supplier.new_longitude = _longitude.ToString();
            accountRepositoryDal.Update(supplier);
            XrmDataContext.SaveChanges();
            GetCoverAreaResponse response = new GetCoverAreaResponse();
            response.AreasList.Add(GetDefaultCoverArea(request.SupplierId));
            response.SubscriptionPlan = String.Empty;
         //   GetCoverAreaRequest gcar = new GetCoverAreaRequest() { SupplierId = request.SupplierId };
         //   return GetCoverArea(gcar);
            //GetCoverAreaResponse response = new GetCoverAreaResponse();
            return response;
        }
        public GetCoverAreaResponse GetCoverArea(GetCoverAreaRequest request)
        {
            ValidateRequest(request);

            GetCoverAreaResponse response = new GetCoverAreaResponse();

            DataAccessLayer.CoverArea coverAreaDal = new DataAccessLayer.CoverArea(XrmDataContext);
            var query = from area in coverAreaDal.All
                        where area.new_accountid == request.SupplierId
                        && area.statecode == DataAccessLayer.CoverArea.ACTIVE
                        orderby area.new_order
                        select new
                        {
                            area.new_latitude,
                            area.new_longitude,
                            area.new_fulladdress,
                            area.new_numberofzipcodes,
                            area.new_radius,
                            area.new_selectedarea1id,
                            area.new_selectedarea1name,
                            area.new_selectedarea2id,
                            area.new_selectedarea2name,
                            area.new_selectedarea3id,
                            area.new_selectedarea3name,
                            area.new_order
                        };

            if (query.Count() == 0)
            {
                //if the user has no areas then use his address as default
                var defaultCoverAreaManager = GetDefaultCoverArea(request.SupplierId);
                if (defaultCoverAreaManager != null)
                    response.AreasList.Add(defaultCoverAreaManager);
                else
                    response.AreasList.Add(GetDefaultCoverArea());

            }
            else
            {
                foreach (var item in query)
                {
                    var area = new GetCoverAreaResponse.AreaNode()
                    {
                        FullAddress = item.new_fulladdress,
                        Latitude = item.new_latitude.Value,
                        Longitude = item.new_longitude.Value,
                        Radius = item.new_radius,
                        NumberOfZipcodes = item.new_numberofzipcodes.Value,
                        Order = item.new_order.Value
                    };
                    area.SelectedAreas.Add(new GetCoverAreaResponse.SelectedArea() { Level = 3.ToString(), RegionId = item.new_selectedarea3id, Name = item.new_selectedarea3name });
                    area.SelectedAreas.Add(new GetCoverAreaResponse.SelectedArea() { Level = 2.ToString(), RegionId = item.new_selectedarea2id, Name = item.new_selectedarea2name });
                    area.SelectedAreas.Add(new GetCoverAreaResponse.SelectedArea() { Level = 1.ToString(), RegionId = item.new_selectedarea1id, Name = item.new_selectedarea1name });
                    area.SelectedAreas.Add(nationwideSelectedArea);

                    response.AreasList.Add(area);
                }
            }
            DataAccessLayer.AccountRepository accDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var getPlanQuery = from acc in accDal.All
                               where acc.accountid == request.SupplierId
                               select new { Plan = acc.new_subscriptionplan };
            if (getPlanQuery.FirstOrDefault() != null)
            {
                response.SubscriptionPlan = getPlanQuery.First().Plan;
            }
            if (response.SubscriptionPlan == null)
                response.SubscriptionPlan = String.Empty;
            return response;
        }

        public SetCoverAreasResponse SetCoverArea(SetCoverAreasRequest request)
        {
            ValidateRequest(request);
            DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = accountRepositoryDal.Retrieve(request.SupplierId);
         //   supplier.accountid = request.SupplierId;
            decimal _latitude = request.Areas[0].Latitude;
            decimal _longitude = request.Areas[0].Longitude;
            supplier.new_fulladdress = request.Areas[0].Address;
            supplier.new_latitude = _latitude.ToString();
            supplier.new_longitude = _longitude.ToString();
   //         dalAccount.Update(supplier);

            DataAccessLayer.CoverArea dal = new DataAccessLayer.CoverArea(XrmDataContext);
            SetCoverAreasResponse response = new SetCoverAreasResponse();

            //retrieve existing cover areas
            var existingAreasQuery = from ca in dal.All
                                     where ca.new_accountid == request.SupplierId
                                     && ca.statecode == DataAccessLayer.CoverArea.ACTIVE
                                     select new DataModel.Xrm.new_coverarea
                                     {
                                         new_coverareaid = ca.new_coverareaid,
                                         new_fulladdress = ca.new_fulladdress,
                                         new_radius = ca.new_radius,
                                         new_latitude = ca.new_latitude,
                                         new_longitude = ca.new_longitude,
                                         new_order = ca.new_order
                                     };

            var existingAreas = existingAreasQuery.ToArray();

            //create objects for new cover areas
            List<DataModel.Xrm.new_coverarea> newAreas = new List<DataModel.Xrm.new_coverarea>();
            foreach (var item in request.Areas)
            {
                DataModel.Xrm.new_coverarea area = new DataModel.Xrm.new_coverarea();
                area.new_accountid = request.SupplierId;
                area.new_fulladdress = item.Address;
                area.new_latitude = item.Latitude;
                area.new_longitude = item.Longitude;
                area.new_radius = item.Radius;
                area.new_numberofzipcodes = item.NumberOfZipcodes;
                if (String.IsNullOrEmpty(item.SelectedArea1Id)
                    || String.IsNullOrEmpty(item.SelectedArea2Id)
                    || String.IsNullOrEmpty(item.SelectedArea3Id)
                    )//is from mobile
                {
                    var defaultAreaObject = CreateDefaultCoverAreaObject(item.Longitude, item.Latitude, item.Address);
                    var level1 = defaultAreaObject.SelectedAreas.Where(x => x.Level == 1.ToString()).First();
                    var level2 = defaultAreaObject.SelectedAreas.Where(x => x.Level == 2.ToString()).First();
                    var level3 = defaultAreaObject.SelectedAreas.Where(x => x.Level == 3.ToString()).First();
                    item.SelectedArea1Id = level1.RegionId;
                    item.SelectedArea1Name = level1.Name;
                    item.SelectedArea2Id = level2.RegionId;
                    item.SelectedArea2Name = level2.Name;
                    item.SelectedArea3Id = level3.RegionId;
                    item.SelectedArea3Name = level3.Name;
                }
                area.new_selectedarea1name = item.SelectedArea1Name;
                area.new_selectedarea1id = item.SelectedArea1Id;
                area.new_selectedarea2name = item.SelectedArea2Name;
                area.new_selectedarea2id = item.SelectedArea2Id;
                area.new_selectedarea3name = item.SelectedArea3Name;
                area.new_selectedarea3id = item.SelectedArea3Id;
                area.new_order = item.Order;
                newAreas.Add(area);
            }

            bool changedAreas = false;
            //check if new areas have been added.
            foreach (var newA in newAreas)
            {
                if (!existingAreas.Contains(newA))
                {
                    changedAreas = true;
                    break;
                }
            }

            if (!changedAreas)
            {
                //check if existing areas have been deleted.
                foreach (var existingA in existingAreas)
                {
                    if (!newAreas.Contains(existingA))
                    {
                        changedAreas = true;
                        break;
                    }
                }
            }

            //if any change was made, redo the cover areas.
            if (changedAreas)
            {
                //create new areas
                foreach (var newA in newAreas)
                {
                    dal.Create(newA);
                }
                XrmDataContext.SaveChanges();
                //set state of old areas to disabled.
                foreach (var existingA in existingAreas)
                {
                    dal.SetState(((Microsoft.Xrm.Client.ICrmEntity)existingA).LogicalName, existingA.new_coverareaid, false);
                }
            }

            
            var query = from acc in accountRepositoryDal.All
                        where acc.accountid == request.SupplierId
                        select new { Step = acc.new_stepinregistration2014 };
            var currentStepFound = query.FirstOrDefault();
            int? step = currentStepFound.Step;

            if (!step.HasValue || step.Value <= (int)DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_Area_5 || changedAreas)
            {
             //   DataModel.Xrm.account supplier = accountRepositoryDal.Retrieve(request.SupplierId);// new DataModel.Xrm.account();
                //supplier.accountid = request.SupplierId;
                if (!step.HasValue || step.Value <= (int)DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_Area_5)
                {
                    step = (int)DataModel.Xrm.account.StepInRegistration2014.Checkout_6;
                    supplier.new_stepinregistration2014 = step;
                    //Send system notification - Advertiser registration - finished 3rd screen (area) 
                    NoProblem.Core.BusinessLogic.ClipCall.CreateMobileSystemNotification noti = new NoProblem.Core.BusinessLogic.ClipCall.CreateMobileSystemNotification();
                    noti.SendAdvertiserRegistration(supplier.name, supplier.telephone1);
                }
                if (changedAreas)
                {
                    supplier.new_coverareasneedupdate = true;
                }
                //accountRepositoryDal.Update(supplier);
                //XrmDataContext.SaveChanges();
            }
            accountRepositoryDal.Update(supplier);
            XrmDataContext.SaveChanges();
            response.Step = step.Value;
            accountRepositoryDal.SetSupplierRegistrationStage(request.SupplierId, NoProblem.Core.DataModel.Xrm.account.StageInRegistration.CITIES);
            TimeZoneAndDefaultHoursSetter setter = new TimeZoneAndDefaultHoursSetter(supplier.accountid, _latitude, _longitude);
            setter.Start();
            return response;
        }

        public int GetNumberOfZipcodes(Guid regionId)
        {
            return MapUtils.GetNumberOfZipcodes(regionId);
        }

        public int GetNumberOfZipcodes(decimal latitude, decimal longitude, decimal radius)
        {
            return MapUtils.GetNumberOfZipcodes(latitude, longitude, radius);
        }
        private GetCoverAreaResponse.AreaNode GetDefaultCoverArea()
        {
            var area = CreateDefaultCoverAreaObject(LongitudeNationwideSelected, LatitudeNationwideSelected, USA);
            return area;
        }
        private GetCoverAreaResponse.AreaNode GetDefaultCoverArea(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new { Latitude = acc.new_latitude, Longitude = acc.new_longitude, FullAddress = acc.new_fulladdress };
            var foundSupplierData = query.FirstOrDefault();
            if (foundSupplierData == null)
                throw new ArgumentException("There is no supplier with the provided SupplierId", "SupplierId");
            if (String.IsNullOrEmpty(foundSupplierData.Latitude) || String.IsNullOrEmpty(foundSupplierData.Longitude) || String.IsNullOrEmpty(foundSupplierData.FullAddress))
            {
                return null;
            }
            decimal longitude = decimal.Parse(foundSupplierData.Longitude);
            decimal latitude = decimal.Parse(foundSupplierData.Latitude);
            string fullAddress = foundSupplierData.FullAddress;
            var area = CreateDefaultCoverAreaObject(longitude, latitude, fullAddress);
            return area;
        }

        public static GetCoverAreaResponse.AreaNode CreateDefaultCoverAreaObject(decimal longitude, decimal latitude, string fullAddress)
        {
            var moreMapData = MapUtils.GetMoreMapData(longitude, latitude);
            var cityId = moreMapData.FirstOrDefault(x => x.Level == 3).Id;
            var area =
                new GetCoverAreaResponse.AreaNode()
                {
                    FullAddress = fullAddress,
                    Latitude = latitude,
                    Longitude = longitude,
                    Radius = DEFAULT_RADIUS//cityId.ToString()
                };
            area.NumberOfZipcodes = MapUtils.GetNumberOfZipcodes(cityId);
            moreMapData = moreMapData.OrderByDescending(x => x.Level).ToList();
            foreach (var x in moreMapData)
            {
                area.SelectedAreas.Add(
                    new GetCoverAreaResponse.SelectedArea()
                    {
                        Level = x.Level.ToString(),
                        Name = x.Name,
                        RegionId = x.Id.ToString()
                    });
            }
            area.SelectedAreas.Add(nationwideSelectedArea);
            return area;
        }

        private void ValidateRequest(RegistrationRequest request)
        {
            if (request.SupplierId == Guid.Empty)
                throw new ArgumentException("SupplierId must be provided", "SupplierId");
        }

        private void ValidateRequest(SetCoverAreasRequest request)
        {
            ValidateRequest((RegistrationRequest)request);
            if (request.Areas == null || request.Areas.Count == 0)
                throw new ArgumentException("Areas cannot be empty", "Areas");
        }
    }
}
