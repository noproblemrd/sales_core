﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class LogoManager
    {
        static readonly string siteConnectionString;
        static readonly string professionalLogosWeb;
        static LogoManager()
        {
            siteConnectionString = ConfigurationManager.AppSettings["siteConnectionString"];
            professionalLogosWeb = ConfigurationManager.AppSettings["professionalLogosWeb"];
        }
        public NoProblem.Core.DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoResponse
            UpdateSupplierLogo(DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoUploadRequest uploadRequest)
        {
            try
            {
                /*
                string command = "EXEC dbo.SetAdvertiser @UserId, @SiteId, @LogoFile";
                using (SqlConnection conn = new SqlConnection(siteConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        cmd.Parameters.AddWithValue("@UserId", uploadRequest.SupplierId);
                        cmd.Parameters.AddWithValue("@SiteId", uploadRequest.SiteId);
                        cmd.Parameters.AddWithValue("@LogoFile", uploadRequest.ImageUrl);
                        cmd.ExecuteScalar();                        
                        cmd.Dispose();
                    }
                    conn.Close();
                    conn.Dispose();
                }
                 * */
                string command = "UPDATE [dbo].[AccountExtensionBase] SET new_businessimageurl = @imageUrl WHERE AccountId = @accountId";
                using (SqlConnection conn = new System.Data.SqlClient.SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {
                    
                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@imageUrl", uploadRequest.ImageUrl);
                        cmd.Parameters.AddWithValue("@accountId", uploadRequest.SupplierId);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                throw exc;
            }
            return new DataModel.SupplierModel.Registration2014.Mobile.SupplierLogoResponse() 
            { 
      //          ImageUrl = GetLogoImageUrl(uploadRequest.SupplierId, uploadRequest.ImageUrl) 
                ImageUrl = uploadRequest.ImageUrl
            };
        }
        /*
        public string GetLogoImageUrl(Guid supplierId, string logoFileName)
        {
            if (string.IsNullOrEmpty(logoFileName))
                return null;
            return professionalLogosWeb + supplierId.ToString() + @"/" + logoFileName;
        }
         * */
        public string GetLogoImageUrl(Guid supplierId)
        {
            string imageUrl = null;
            try
            {
                string command = "SELECT new_businessimageurl FROM [dbo].[AccountExtensionBase] WHERE AccountId = @accountId";
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@accountId", supplierId);
                        object obj = cmd.ExecuteScalar();
                        if (obj != null && obj != DBNull.Value)
                            imageUrl = (string)obj;
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("GetLogoImageUrl failed with exception. request: SupplierId={0}", supplierId));
                return null;
            }
         //   return GetLogoImageUrl(supplierId, imageUrl);
            return imageUrl;
        }
    
    }
}
