﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;
using Effect.Crm.Logs;
using System.Configuration;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class PaymentMethodManager : XrmUserBase
    {
        public PaymentMethodManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
        }

        DataAccessLayer.ConfigurationSettings configDal;
        private const string ZERO = "0";

        public GetPaymentMethodDataResponse GetPaymentMethodData(Guid supplierId)
        {
            var spDal = new DataAccessLayer.SupplierPricing(XrmDataContext);
            var ccData = spDal.FindCreditCardInfo(supplierId);
            DataAccessLayer.AccountRepository acc = new DataAccessLayer.AccountRepository(XrmDataContext);
            bool IsActive = acc.IsActiveMobileAccount(supplierId);
            if (ccData == null)
                return new GetPaymentMethodDataResponse
                {                    
                    IsActive = IsActive
                };            
            
            var response = new GetPaymentMethodDataResponse
            {
                Data = new GetPaymentMethodDataResponseInner()
                {
                    Last4Digits = ccData.last4digits,
                    ExpDate = ccData.ccExpDate,
                    CcOwnerName = ccData.ccOwnerName,
                    BillingAddress = ccData.billingAddress,
                    CreditCardType = ccData.ccType,
                    Email = ccData.ccOwnerEmail,
                    Phone = ccData.ccOwnerPhone,
                    BillingCity = ccData.BillingCity,
                    BillingState = ccData.BillingState,
                    BillingZip = ccData.BillingZip
                },
                IsActive = IsActive
                
            };
            return response;
        }

        public bool CreatePaymentMethod(DataModel.APIs.MobileApi.CreatePaymentMethodRequest request)
        {

            string solekName = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.SOLEK);
            if (string.IsNullOrEmpty(solekName))
            {
                LogUtils.MyHandle.WriteToLog("CreatePaymentMethod did nothing becuase there is no solek in the environment");
                return false;//auto recharge disabled in environment.
            }
            CreditCardSolek.Solek solek = CreditCardSolek.SolekFactory.GetSolek(solekName);
            DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var shopperData = CreateShopperDataObject(request, accountRepositoryDal);
            string shopperId = solek.CreateShopper(shopperData);
            if (String.IsNullOrEmpty(shopperId))
            {
                LogUtils.MyHandle.WriteToLog("Failed creating shopper id for supplier: " + request.BillingFirstName + request.BillingLastName);
                return false;
            }

            var pricingManager = new SupplierPricing.SupplierPricingManager(XrmDataContext);
            var pricingRequest = new DataModel.SupplierPricing.CreateSupplierPricingRequest()
            {
                BillingAddress = request.BillingAddress,
                BillingCity = request.BillingCity,
                BillingEmail = shopperData.ShopperEmail,
                BillingPhone = shopperData.ShopperPhone,
                BillingState = request.BillingState,
                BillingZip = request.BillingZip,
                ChargingCompany = solekName.ToLower(),
                ChargingCompanyAccountNumber = shopperId,
                ChargingCompanyContractId = GetChargingCompanyContractId(shopperData.Subscription),
                CreditCardExpDate = FixExpirationMonth(request.ExpirationMonth) + request.ExpirationYear.ToString().Substring(2),
                CreditCardOwnerName = request.BillingFirstName + " " + request.BillingLastName,
                CreditCardType = request.CreditCardType,
                Last4DigitsCC = request.Last4Digits,
                PaymentMethod = DataModel.Xrm.new_paymentmethod.ePaymentMethod.CreditCard,
                UserAcceptLanguage = String.Empty,
                UserAgent = GlobalConfigurations.IsDevEnvironment ? "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0" : request.UserAgent,
                UserIP = shopperData.IP,
                UserRemoteHost = GlobalConfigurations.IsDevEnvironment ? "70-9-127-225.pools.spcsdns.net" : request.UserHost,
                SupplierId = request.SupplierId,
                CreatedByUserId = request.SupplierId
            };
           if (shopperData.SupplierStatus == DataModel.Xrm.account.SupplierStatus.Candidate)
            {
                pricingRequest.IsAutoRenew = true;
                pricingRequest.RechargeAmount = GetRechargeAmount();
                pricingRequest.RechargeDayOfMonth = DateTime.Now.Day;
                pricingRequest.RechargeTypeCode = DataModel.Xrm.account.RechargeTypeCode.Monthly;
                pricingRequest.UpdateRechargeFields = true;
            }
            var pricingResult = pricingManager.CreateSupplierPricing(pricingRequest);
            return pricingResult.DepositStatus == DataModel.SupplierPricing.eDepositStatus.OK;
        }

        private string GetChargingCompanyContractId(string subscription)
        {
            if (subscription == SubscriptionPlansContract.Plans.FEATURED_LISTING)
                return CreditCardSolek.PlimusContracts.featuredListingContractId;


            if (subscription == SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION)
                return CreditCardSolek.PlimusContracts.ClipCallContractId;


              //default
            return CreditCardSolek.PlimusContracts.leadBoosterContractId;
        }

        private int GetRechargeAmount()
        {
            string costStr = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.FEATURED_LISTING_SUBSCRIPTION_COST);
            int cost;
            if (!int.TryParse(costStr, out cost))
            {
                cost = 4;
            }
            return cost;
        }

        private CreditCardSolek.Solek.CreateShopperData CreateShopperDataObject(DataModel.APIs.MobileApi.CreatePaymentMethodRequest request, 
            DataAccessLayer.AccountRepository accountRepositoryDal)
        {
            var supplier = (from acc in accountRepositoryDal.All
                            where acc.accountid == request.SupplierId
                            select new { Status = acc.new_status.Value, Subscription = acc.new_subscriptionplan, Phone = acc.telephone1, 
                                Address = acc.address1_line1, CompanyName = acc.name, FirstName = acc.new_firstname, LastName = acc.new_lastname, 
                                Email = acc.emailaddress1, IsMobile = acc.new_ismobile }).First();
            var shopperData = new CreditCardSolek.Solek.CreateShopperData()
            {
                BillingCity = request.BillingCity,
                BillingFirstName = request.BillingFirstName,
                BillingLastName = request.BillingLastName,
                BillingState = request.BillingState,
                BillingStreetAddress = request.BillingAddress,
                BillingZip = request.BillingZip,
                CreditCardType = request.CreditCardType,
                EncryptedCardNumber = request.EncryptedCreditCardNumber,
                EncryptedCVV = request.EcryptedCvv,
                ExpirationMonth = request.ExpirationMonth.ToString(),
                ExpirationYear = request.ExpirationYear,
                IP = GlobalConfigurations.IsDevEnvironment ? "62.219.121.253" : request.UserIP,
                ShopperStreetAddress = String.IsNullOrEmpty(supplier.Address) ? request.BillingAddress : supplier.Address,
                ShopperCity = request.BillingCity,
                ShopperCompanyName = supplier.CompanyName,
                ShopperEmail = request.BillingEmail,
                ShopperFax = supplier.Phone,
                ShopperFirstName = String.IsNullOrEmpty(supplier.FirstName) ? request.BillingFirstName : supplier.FirstName,
                ShopperLastName = String.IsNullOrEmpty(supplier.LastName) ? request.BillingLastName : supplier.LastName,
                ShopperPhone = supplier.Phone,
                ShopperState = request.BillingState,
                ShopperZip = request.BillingZip,
                Subscription = String.IsNullOrEmpty(supplier.Subscription) ? DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING : supplier.Subscription,
                SupplierStatus = (DataModel.Xrm.account.SupplierStatus)supplier.Status,
                IsMobileSupplier = supplier.IsMobile.HasValue ? supplier.IsMobile.Value : false
            };
            return shopperData;
        }

        private string FixExpirationMonth(int month)
        {
            string monthStr = month.ToString();
            if (monthStr.Length == 1)
            {
                monthStr = ZERO + monthStr;
            }
            return monthStr;
        }
    }
}
