﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class CheckoutManager : XrmUserBase
    {
        public CheckoutManager(DataModel.Xrm.DataContext xrmDataContext) 
            : base(xrmDataContext)
        {

        }

        public string GetPlan(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new { Plan = acc.new_subscriptionplan };
            var planObj = query.FirstOrDefault();
            if (planObj == null)
                throw new ArgumentException("No supplier found with the provided supplierId", "supplierId");
            return planObj.Plan;
        }

        public void CheckoutButtonClicked(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_checkoutbuttonclicked = true;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }
    }
}
