﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class FriendEmailInviter : XrmUserBase
    {
        public FriendEmailInviter(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public void SendInvitationEmails(SendInvitationEmailsRequest request)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = (from acc in dal.All
                            where acc.accountid == request.SupplierId
                            select new { Name = acc.name }).FirstOrDefault();
            if (supplier == null)
            {
                throw new ArgumentException(String.Format("Supplier with id {0} does not exist", request.SupplierId), "SupplierId");
            }
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string urlBase = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.INVITATION_URL_BASE);
            string url = String.Format(urlBase, request.SupplierId);
            MandrillSender sender = new MandrillSender();
            Dictionary<string, Dictionary<string, string>> vars = new Dictionary<string, Dictionary<string, string>>();
            List<string> emails = new List<string>();
            foreach (var item in request.Friends)
            {
                Dictionary<string, string> innerVars = new Dictionary<string, string>();
                innerVars.Add("NPVAR_NEW_NAME", item.Name);
                vars.Add(item.Email, innerVars);
                emails.Add(item.Email);
            }
            Dictionary<string, string> globalVars = new Dictionary<string, string>();
            globalVars.Add("NPVAR_AD_NAME", supplier.Name);
            globalVars.Add("NPVAR_URL", url);
            DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
            string subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_Invitee_Invitation_2014);
            if (!String.IsNullOrEmpty(subject))
                subject = String.Format(subject, supplier.Name);
            sender.SendTemplate(emails, subject, DataModel.MandrillTemplates.E_Invitee_Invitation_2014, vars, globalVars);
        }
    }
}
