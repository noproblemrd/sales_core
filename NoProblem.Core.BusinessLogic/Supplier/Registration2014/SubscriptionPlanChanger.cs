﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class SubscriptionPlanChanger : XrmUserBase
    {
        public SubscriptionPlanChanger(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public void ChangeSubscriptionPlan(Guid supplierId, string newSubscriptionPlan)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var currentPlan = (from acc in dal.All where acc.accountid == supplierId select acc.new_subscriptionplan).FirstOrDefault();
            if (currentPlan == newSubscriptionPlan)
                return;
            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.accountid = supplierId;
            supplier.new_subscriptionplan = newSubscriptionPlan;
            supplier.new_participatesinservicerequests = newSubscriptionPlan == SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING;
            supplier.new_rechargetypecode = (int)DataModel.Xrm.account.RechargeTypeCode.Monthly;
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
            decimal subscriptionCost = ChoosePlanManager.GetSubscriptionPlanCost();
            supplier.new_rechargeamount = Convert.ToInt32(subscriptionCost);
            supplier.new_rechargeenabled = true;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
            if (newSubscriptionPlan == SubscriptionPlansContract.Plans.FEATURED_LISTING)
            {
                bool alreadyPaid = SupplierPricing.SubscriptionPlansUtils.AlreadyPaidThisMonth(supplierId, newSubscriptionPlan);
                if (!alreadyPaid)
                {
                    supplier.new_rechargedayofmonth = DateTime.Now.Day;
                    dal.Update(supplier);
                    XrmDataContext.SaveChanges();
                    SupplierPricing.SupplierPricingManager manager = new SupplierPricing.SupplierPricingManager(XrmDataContext);
                    manager.AutoRechargeSupplier(supplierId, false);
                }
            }
        }

        private static void ValidateRequest(Guid supplierId, string subscriptionPlan)
        {
            if (supplierId == Guid.Empty)
                throw new ArgumentException("SupplierId must be provided.", "SupplierId");
            if (subscriptionPlan != SubscriptionPlansContract.Plans.FEATURED_LISTING && subscriptionPlan != SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING)
                throw new ArgumentException(String.Format("Invalid plan. Plan can be {0} or {1}.", SubscriptionPlansContract.Plans.FEATURED_LISTING, SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING), "NewSubscriptionPlan");
        }
    }
}
