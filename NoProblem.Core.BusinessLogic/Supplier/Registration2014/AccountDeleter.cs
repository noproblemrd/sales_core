﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class AccountDeleter : XrmUserBase
    {
        public AccountDeleter(DataModel.Xrm.DataContext dataContext)
            : base(dataContext)
        {

        }

        public bool DeleteAccount(DeleteAccountRequest request)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == request.SupplierId
                        select new { Password = acc.new_password, Email = acc.emailaddress1, Name = acc.name };
            var found = query.FirstOrDefault();
            if (found == null)
            {
                throw new ArgumentException(String.Format("Supplier with id {0} was not found.", request.SupplierId), "SupplierId");
            }
            if (request.Password != found.Password)
            {
                return false;
            }
            UpdateSupplierToInactive(request, dal);
            SendEmail(found.Email, found.Name);
            return true;
        }

        private void SendEmail(string email, string name)
        {
            MandrillSender sender = new MandrillSender();
            DataAccessLayer.MandrillEmailSubjects subjectsConfig = new NoProblem.Core.DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
            string subject = subjectsConfig.GetEmailSubject(DataModel.MandrillTemplates.E_AD_Delete);
            Dictionary<string, string> vars = new Dictionary<string,string>();
            vars.Add("NPVAR_AD_NAME", name);
            sender.SendTemplate(email, subject, DataModel.MandrillTemplates.E_AD_Delete, vars);
        }

        private void UpdateSupplierToInactive(DeleteAccountRequest request, DataAccessLayer.AccountRepository dal)
        {
            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.accountid = request.SupplierId;
            supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Inactive;
            supplier.new_deletereason = request.Reason;
            string reasonOwnWords = request.ReasonInYourOwnWords;
            if (reasonOwnWords != null && reasonOwnWords.Length > 2000)
            {
                reasonOwnWords = reasonOwnWords.Substring(0, 2000);
            }
            supplier.new_deletereasonownwords = reasonOwnWords;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }
    }
}
