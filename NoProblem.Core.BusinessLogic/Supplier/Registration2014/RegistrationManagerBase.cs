﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class RegistrationManagerBase
    {
        public void CheckRegistrationStep(Guid supplierId)
        {

            try
            {
                string command = @"
SELECT A.Name, 
	(SELECT COUNT(*)
	FROM dbo.New_accountexpertise
	WHERE New_AccountId = A.AccountId and DeletionStateCode = 0 and statecode = 0) Categories,
	A.New_businessImageUrl,
	A.Description,
	(SELECT COUNT(*)
	FROM dbo.New_coverarea
	WHERE New_AccountId = A.AccountId and DeletionStateCode = 0 and statecode = 0) CoverArea
FROM dbo.Account A WITH(NOLOCK)
WHERE A.AccountId = @accountId
	and A.New_IsMobile = 1
	and A.New_havevideo = 1";
                using (SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand(command, conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@accountId", supplierId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        conn.Close();
                    }
                }
            }
            catch(Exception exc)
            {

            }
        }
    }
}
