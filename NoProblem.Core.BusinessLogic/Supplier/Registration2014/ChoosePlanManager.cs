﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class ChoosePlanManager : XrmUserBase
    {
        public ChoosePlanManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public ChoosePlanResponse ChoosePlan(ChoosePlanRequest request)
        {
            ValidateRequest(request);
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == request.SupplierId
                        select new { Step = acc.new_stepinregistration2014 };
            var currentStepFound = query.FirstOrDefault();
            if (currentStepFound == null)
                throw new Exception("Supplier with the given SupplierId was not found");

            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.accountid = request.SupplierId;
            supplier.new_subscriptionplan = request.PlanSelected;
            supplier.new_participatesinservicerequests = request.PlanSelected == SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING;

            int? step = currentStepFound.Step;
            if (!step.HasValue || step.Value <= (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2)
            {
                step = (int)DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_General_3;
                supplier.new_stepinregistration2014 = step;
            }

            dal.Update(supplier);
            XrmDataContext.SaveChanges();

            ChoosePlanResponse response = new ChoosePlanResponse();
            response.Step = step.Value;
            return response;
        }

        public static decimal GetSubscriptionPlanCost()
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(null);
            string configStr = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.FEATURED_LISTING_SUBSCRIPTION_COST);
            decimal cost;
            if (!decimal.TryParse(configStr, out cost))
            {
                cost = 4;
            }
            return cost;
        }

        public static decimal GetRegistrationBonus()
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(null);
            string registrationBonusStr = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.REGISTRATION_BONUS);
            decimal registrationBonus;
            if (!decimal.TryParse(registrationBonusStr, out registrationBonus))
            {
                registrationBonus = 30;
            }
            return registrationBonus;
        }

        public static SubscriptionCostAndBonus GetSubscriptionPlanCostAndRegistrationBonus()
        {
            SubscriptionCostAndBonus retVal = new SubscriptionCostAndBonus();
            retVal.Bonus = GetRegistrationBonus();
            retVal.Cost = GetSubscriptionPlanCost();
            return retVal;
        }

        public decimal GetSubscriptionPlanCost(Guid supplierId)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in accountRepositoryDal.All
                        where acc.accountid == supplierId
                        select new { acc.new_rechargeenabled, acc.new_rechargeamount, acc.new_rechargetypecode, acc.new_subscriptionplan, acc.new_isexemptofppamonthlypayment };
            var queryResult = query.FirstOrDefault();
            decimal retVal = 0;
            if (queryResult != null 
                && queryResult.new_rechargeenabled.HasValue && queryResult.new_rechargeenabled.Value
                && queryResult.new_rechargetypecode.HasValue && queryResult.new_rechargetypecode.Value == (int)DataModel.Xrm.account.RechargeTypeCode.Monthly
                && (!queryResult.new_isexemptofppamonthlypayment.HasValue || !queryResult.new_isexemptofppamonthlypayment.Value)
                && IsInNewPlans(queryResult.new_subscriptionplan)
                )
            {
                retVal = queryResult.new_rechargeamount.HasValue ? queryResult.new_rechargeamount.Value : 0;
            }
            return retVal;
        }

        private bool IsInNewPlans(string subscriptionPlan)
        {
            return
                subscriptionPlan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.FEATURED_LISTING
                ||
                subscriptionPlan == DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING;
        }

        private static void ValidateRequest(ChoosePlanRequest request)
        {
            if (request.SupplierId == Guid.Empty)
                throw new ArgumentException("SupplierId must be provided.", "SupplierId");
            if (request.PlanSelected != SubscriptionPlansContract.Plans.FEATURED_LISTING && request.PlanSelected != SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING)
                throw new ArgumentException(String.Format("Invalid plan. Plan can be {0} or {1}.", SubscriptionPlansContract.Plans.FEATURED_LISTING, SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING), "PlanSelected");
        }
    }
}
