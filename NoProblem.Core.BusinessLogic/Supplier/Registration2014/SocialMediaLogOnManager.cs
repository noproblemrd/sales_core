﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class SocialMediaLogOnManager : LogOnManagerBase
    {
        public SocialMediaLogOnManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
        }
        /*
        public LogOnResponse SocialMediaLogOn(SocialMediaLogOnRequest request)
        {
            if (String.IsNullOrEmpty(request.Email))
            {
                throw new ArgumentException("Email cannot be null nor empty.", "Email");
            }
            if (String.IsNullOrEmpty(request.FacebookId) && String.IsNullOrEmpty(request.GoogleId))
            {
                throw new ArgumentException("FacebookId or GoogleId must be provided. Both were null or empty.");
            }
            bool givenFacebookId = !String.IsNullOrEmpty(request.FacebookId);
            if (givenFacebookId)
            {
                var accountByFacebook = GetByFacebookId(request.FacebookId);
                if (accountByFacebook != null)//account found by facebook
                {
                    if (accountByFacebook.Status.Value == (int)DataModel.Xrm.account.SupplierStatus.Inactive)
                    {
                        return new LogOnResponse()
                        {
                            SupplierId = accountByFacebook.Id,
                            ContactName = request.ContactName,
                            Name = accountByFacebook.Name,
                            StatusCode = LogOnResponse.StatusCodes.INACTIVE_ACCOUNT
                        };
                    }

                    //account exists. return step.
                    int? step = accountByFacebook.Step;
                    if (!step.HasValue)
                    {
                        step = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
                    }
                    step = MobileAppStepHandle(request, accountByFacebook.Id, step);
                    var response = new LogOnResponse() { Step = step.Value, SupplierId = accountByFacebook.Id, ContactName = request.ContactName, Name = accountByFacebook.Name };
                    if (request.IsFromMobileApp)
                    {
                        string token = GetMobileToken(request, accountByFacebook.Id);
                        response.ApiToken = token;
                    }
                    else if (ShouldGoToLegacy(accountByFacebook))
                    {
                        response.StatusCode = LogOnResponse.StatusCodes.SEND_TO_LEGACY_PROCESS;
                    }
                    return response;
                }
            }
            bool givenGoogleId = !String.IsNullOrEmpty(request.GoogleId);
            if (givenGoogleId)
            {
                var accountByGoogle = GetByGoogleId(request.GoogleId);
                if (accountByGoogle != null)
                {
                    //account exits. return step.

                    if (accountByGoogle.Status.Value == (int)DataModel.Xrm.account.SupplierStatus.Inactive)
                    {
                        return new LogOnResponse()
                        {
                            SupplierId = accountByGoogle.Id,
                            ContactName = request.ContactName,
                            Name = accountByGoogle.Name,
                            StatusCode = LogOnResponse.StatusCodes.INACTIVE_ACCOUNT
                        };
                    }

                    int? step = accountByGoogle.Step;
                    if (!step.HasValue)
                    {
                        step = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
                    }
                    step = MobileAppStepHandle(request, accountByGoogle.Id, step);
                    var response = new LogOnResponse() { Step = step.Value, SupplierId = accountByGoogle.Id, ContactName = request.ContactName, Name = accountByGoogle.Name };
                    if (request.IsFromMobileApp)
                    {
                        string token = GetMobileToken(request, accountByGoogle.Id);
                        response.ApiToken = token;
                    }
                    else if (ShouldGoToLegacy(accountByGoogle))
                    {
                        response.StatusCode = LogOnResponse.StatusCodes.SEND_TO_LEGACY_PROCESS;
                    }
                    return response;
                }
            }
            //if reached here then not found neither by facebook nor google.
            var accountByEmail = GetByEmail(request.Email);
            if (accountByEmail != null)
            {
                //account found by email

                if (accountByEmail.Status.Value == (int)DataModel.Xrm.account.SupplierStatus.Inactive)
                {
                    return new LogOnResponse()
                    {
                        SupplierId = accountByEmail.Id,
                        ContactName = request.ContactName,
                        Name = accountByEmail.Name,
                        StatusCode = LogOnResponse.StatusCodes.INACTIVE_ACCOUNT
                    };
                }

                //If the account found by email has social ids but different than the once provided then we can't log in cause this email is used in different account.
                if (
                    (givenFacebookId && !String.IsNullOrEmpty(accountByEmail.FacebookId))
                    ||
                    (givenGoogleId && !String.IsNullOrEmpty(accountByEmail.GoogleId))
                    )
                {
                    //error: another account is already registered with this email. return error.
                    var retVal = new LogOnResponse();
                    retVal.StatusCode = LogOnResponse.StatusCodes.ANOTHER_ACCOUNT_ALREADY_HAS_THIS_EMAIL;
                    retVal.ContactName = request.ContactName;
                    retVal.Name = accountByEmail.Name;
                    retVal.SupplierId = accountByEmail.Id;
                    return retVal;
                }
                else
                {
                    //account found by email. update facebookId and/or googleId. return step.
                    UpdateSocialMediaIds(accountByEmail.Id, request.FacebookId, request.GoogleId);
                    var retVal = new LogOnResponse();
                    retVal.SupplierId = accountByEmail.Id;
                    retVal.Name = accountByEmail.Name;
                    retVal.ContactName = request.ContactName;
                    int? step = accountByEmail.Step;
                    if (!step.HasValue)
                    {
                        step = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
                    }
                    if (step.Value < (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2)
                    {
                        step = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
                        //update step to 2.
                        DataModel.Xrm.account supplierUpdate = new DataModel.Xrm.account();
                        supplierUpdate.accountid = accountByEmail.Id;
                        supplierUpdate.new_stepinregistration2014 = step;
                        dal.Update(supplierUpdate);
                        XrmDataContext.SaveChanges();
                    }
                    step = MobileAppStepHandle(request, accountByEmail.Id, step);
                    retVal.Step = step.Value;
                    if (request.IsFromMobileApp)
                    {
                        string token = GetMobileToken(request, accountByEmail.Id);
                        retVal.ApiToken = token;
                    }
                    else if (ShouldGoToLegacy(accountByEmail))
                        retVal.StatusCode = LogOnResponse.StatusCodes.SEND_TO_LEGACY_PROCESS;
                    return retVal;
                }
            }
            //if reached here then account is new (not found by email nor facebook nor google). Create account.
            DataModel.Xrm.account supplierNew = new DataModel.Xrm.account();
            supplierNew.new_googleid = request.GoogleId;
            supplierNew.new_facebookid = request.FacebookId;
            supplierNew.emailaddress1 = request.Email;
            supplierNew.new_recordcalls = true;
            string password = Utils.LoginUtils.GenerateNewPassword();
            supplierNew.new_password = password;
            supplierNew.new_stepinregistration2014 = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
            supplierNew.new_status = (int)DataModel.Xrm.account.SupplierStatus.Candidate;
            supplierNew.new_fullname = request.ContactName;
            if (request.ReferrerId != Guid.Empty)
            {
                supplierNew.new_referrerid = request.ReferrerId;
            }
            supplierNew.new_comefrom = request.ComeFrom;
            if (request.RegistrationHitId != Guid.Empty)
                supplierNew.new_registrationhitsid = request.RegistrationHitId.ToString();
            dal.Create(supplierNew);
            XrmDataContext.SaveChanges();
            int? stepNew = MobileAppStepHandle(request, supplierNew.accountid, supplierNew.new_stepinregistration2014);
            string apiToken = null;
            if (request.IsFromMobileApp)
            {
                string token = GetMobileToken(request, supplierNew.accountid);
                apiToken = token;
            }
            SendEmailVerificationEmail(request.Email, password, supplierNew.accountid, true, givenFacebookId, request.ContactName);
            return new LogOnResponse { Step = stepNew.Value, SupplierId = supplierNew.accountid, ContactName = request.ContactName, ApiToken = apiToken };
        }
        */
        private static bool ShouldGoToLegacy(MinAccountData account)
        {
            return account.IsLegacy && account.LegacyStep != DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.FINISHED_3;
        }

        private void UpdateSocialMediaIds(Guid supplierId, string facebookId, string googleId)
        {
            bool updateNeeded = false;
            DataModel.Xrm.account supplierUpdate = new DataModel.Xrm.account();
            if (!String.IsNullOrEmpty(googleId))
            {
                supplierUpdate.new_googleid = googleId;
                updateNeeded = true;
            }
            if (!String.IsNullOrEmpty(facebookId))
            {
                supplierUpdate.new_facebookid = facebookId;
                updateNeeded = true;
            }
            if (updateNeeded)
            {
                supplierUpdate.accountid = supplierId;
                dal.Update(supplierUpdate);
                XrmDataContext.SaveChanges();
            }
        }

        private MinAccountData GetByFacebookId(string facebookId)
        {
            var accountsByFacebookIdQuery = from acc in dal.All
                                            where acc.new_facebookid == facebookId
                                            select new MinAccountData
                                            {
                                                Email = acc.emailaddress1,
                                                FacebookId = acc.new_facebookid,
                                                GoogleId = acc.new_googleid,
                                                Id = acc.accountid,
                                                Step = acc.new_stepinregistration2014,
                                                Status = acc.new_status,
                                                Name = acc.name,
                                                IsLegacy = acc.new_legacyadvertiser2014.HasValue && acc.new_legacyadvertiser2014.Value,
                                                LegacyStep = acc.new_steplegacy2014
                                            };
            var accountByFacebookId = accountsByFacebookIdQuery.FirstOrDefault();
            return accountByFacebookId;
        }

        private MinAccountData GetByGoogleId(string googleId)
        {
            var accountsByGoogleIdQuery = from acc in dal.All
                                          where acc.new_googleid == googleId
                                          select new MinAccountData
                                          {
                                              Email = acc.emailaddress1,
                                              FacebookId = acc.new_facebookid,
                                              GoogleId = acc.new_googleid,
                                              Id = acc.accountid,
                                              Step = acc.new_stepinregistration2014,
                                              Status = acc.new_status,
                                              Name = acc.name,
                                              IsLegacy = acc.new_legacyadvertiser2014.HasValue && acc.new_legacyadvertiser2014.Value,
                                              LegacyStep = acc.new_steplegacy2014
                                          };
            var accountByGoogleId = accountsByGoogleIdQuery.FirstOrDefault();
            return accountByGoogleId;
        }

        private MinAccountData GetByEmail(string email)
        {
            var accountsByEmailQuery = from acc in dal.All
                                       where acc.emailaddress1 == email
                                       select new MinAccountData
                                       {
                                           Email = acc.emailaddress1,
                                           FacebookId = acc.new_facebookid,
                                           GoogleId = acc.new_googleid,
                                           Id = acc.accountid,
                                           Step = acc.new_stepinregistration2014,
                                           Status = acc.new_status,
                                           Name = acc.name,
                                           IsLegacy = acc.new_legacyadvertiser2014.HasValue && acc.new_legacyadvertiser2014.Value,
                                           LegacyStep = acc.new_steplegacy2014
                                       };
            var accountByEmail = accountsByEmailQuery.FirstOrDefault();
            return accountByEmail;
        }

        private class MinAccountData
        {
            public string Email { get; set; }
            public string FacebookId { get; set; }
            public string GoogleId { get; set; }
            public Guid Id { get; set; }
            public int? Step { get; set; }
            public int? Status { get; set; }
            public string Name { get; set; }
            public bool IsLegacy { get; set; }
            public string LegacyStep { get; set; }
        }
    }
}
