﻿using NoProblem.Core.DataModel.SupplierModel.Registration2014;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.Utils;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class SwitchToAdvertiserModeManager : XrmUserBase
    {
        DataAccessLayer.Contact contactDal;

        public SwitchToAdvertiserModeManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            contactDal = new DataAccessLayer.Contact(XrmDataContext);
        }

        public NoProblem.Core.DataModel.SupplierModel.Registration2014.LogOnResponse SwitchToAdvertiserMode(Guid customerId)
        {
            DataModel.SupplierModel.Registration2014.LogOnResponse retVal = new DataModel.SupplierModel.Registration2014.LogOnResponse();
            var data = FindAttachedSupplierId(customerId);
            if (!data.SupplierId.HasValue)
            {
                retVal.SupplierId = CreateNewMobileSupplier(data);
                retVal.Step = (int)DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_General_3;
                AttachAdvertiserToCustomer(retVal.SupplierId, customerId);
            }
            else//already attached advertiser
            {
                retVal.SupplierId = data.SupplierId.Value;
                retVal.Step = FindStepInRegistration(data.SupplierId.Value);
            }
            retVal.ApiToken = AttachSupplierToMobileDevice(retVal.SupplierId, customerId);
            return retVal;
        }

        private string AttachSupplierToMobileDevice(Guid supplierId, Guid customerId)
        {
            DataAccessLayer.MobileDevice devicesDal = new DataAccessLayer.MobileDevice(XrmDataContext);
            var query = from d in devicesDal.All
                        where d.new_contactid == customerId
                        select new { Id = d.new_mobiledeviceid, IsActive = d.new_isactive, IsAuth = d.new_isauthorized, Token = d.new_token };
            Guid deviceId = Guid.Empty;
            string token = null;
            foreach (var item in query)
            {
                if (item.IsActive.IsTrue() && item.IsAuth.IsTrue())
                {
                    deviceId = item.Id;
                    token = item.Token;
                    break;
                }
            }
            DataModel.Xrm.new_mobiledevice device = new DataModel.Xrm.new_mobiledevice(XrmDataContext);
            device.new_mobiledeviceid = deviceId;
            device.new_accountid = supplierId;
            devicesDal.Update(device);
            XrmDataContext.SaveChanges();
            return token;
        }

        private void AttachAdvertiserToCustomer(Guid supplierId, Guid customerId)
        {
            DataModel.Xrm.contact contact = new DataModel.Xrm.contact(XrmDataContext);
            contact.contactid = customerId;
            contact.new_accountid = supplierId;
            contactDal.Update(contact);
            XrmDataContext.SaveChanges();
        }

        private int FindStepInRegistration(Guid supplierId)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            return (from acc in accountRepositoryDal.All
                    where acc.accountid == supplierId
                    select acc.new_stepinregistration2014.Value).First();
        }

        private Guid CreateNewMobileSupplier(CustomerToSupplierData data)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplierNew = CheckAarSupplierAndCreate(data.Phone);
            if (supplierNew == null)
            {
                supplierNew = new DataModel.Xrm.account();
                supplierNew.new_recordcalls = true;
                string password = Utils.LoginUtils.GenerateNewPassword();
                supplierNew.new_password = password;
                supplierNew.new_stepinregistration2014 = (int)DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_General_3;
                supplierNew.new_status = (int)DataModel.Xrm.account.SupplierStatus.Trial;
                supplierNew.name = data.FullName;
                supplierNew.new_fullname = data.FullName;
                supplierNew.telephone1 = data.Phone;
                supplierNew.new_havevideo = true;
                supplierNew.new_ismobile = true;
                supplierNew.new_subscriptionplan = SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION;
                accountRepositoryDal.Create(supplierNew);
            }
            else
            {
                supplierNew.new_havevideo = true;
                supplierNew.new_ismobile = true;
                supplierNew.new_recordcalls = true;
                supplierNew.new_status = (int)DataModel.Xrm.account.SupplierStatus.Trial;
                supplierNew.new_subscriptionplan = SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION;
                accountRepositoryDal.Update(supplierNew);
            }
            
            XrmDataContext.SaveChanges();
            /*
            ChoosePlanManager cpManager = new ChoosePlanManager(XrmDataContext);
            cpManager.ChoosePlan(new ChoosePlanRequest() { SupplierId = supplierNew.accountid, PlanSelected = SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING });
             * */
            return supplierNew.accountid;
        }
        private DataModel.Xrm.account CheckAarSupplierAndCreate(string phone)
        {
            Guid AarSupplireId = Guid.Empty;
            string command = @"SELECT TOP 1 New_yelpsupplierId
FROM dbo.New_yelpsupplierExtensionBase
WHERE New_Phone = @phone";
            using(SqlConnection conn = new SqlConnection(NoProblem.Core.DataAccessLayer.Generic.GetConnectionString))
            {
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@phone", phone);
                    object o = cmd.ExecuteScalar();
                    if (o != null && o != DBNull.Value)
                        AarSupplireId = (Guid)o;
                    conn.Close();
                }
            }
            if (AarSupplireId == Guid.Empty)
                return null;
            NoProblem.Core.BusinessLogic.SupplierBL.CreateClipCallSupplier createSupplier = new SupplierBL.CreateClipCallSupplier(XrmDataContext);
            return createSupplier.CreateClipCallSupplierFromAar(AarSupplireId);
        }

        private CustomerToSupplierData FindAttachedSupplierId(Guid customerId)
        {
            var query = from c in contactDal.All
                        where c.contactid == customerId
                        select new { SupplierId = c.new_accountid, Fullname = c.fullname, Phone = c.mobilephone };

            if (query.Count() == 0)
                throw new ArgumentException("Customer with given id not found");
            var found = query.FirstOrDefault();
            return new CustomerToSupplierData()
            {
                SupplierId = found.SupplierId,
                FullName = found.Fullname,
                Phone = found.Phone
            };
        }

        private class CustomerToSupplierData
        {
            public Guid? SupplierId { get; set; }
            public string FullName { get; set; }
            public string Phone { get; set; }
        }
    }
}
