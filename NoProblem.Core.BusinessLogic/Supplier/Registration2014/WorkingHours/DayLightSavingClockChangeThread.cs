﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.WorkingHours
{
    public class DayLightSavingClockChangeThread : Runnable
    {
        private static DataAccessLayer.Generic genericDal = new DataAccessLayer.Generic();

        public DayLightSavingClockChangeThread()
            : base(System.Threading.ThreadPriority.BelowNormal)
        {

        }

        protected override void Run()
        {
            List<Account> accounts = new List<Account>();
            var reader = genericDal.ExecuteReader(query);
            foreach (var row in reader)
            {
                Guid accountId = (Guid)row["accountid"];
                int timeZoneCode = row["address1_utcoffset"] != DBNull.Value ? (int)row["address1_utcoffset"] : 35;
                accounts.Add(
                    new Account()
                    {
                        AccountId = accountId,
                        TimeZoneCode = timeZoneCode
                    });
            }
            WorkingHoursManager manager = new WorkingHoursManager(XrmDataContext);
            foreach (var item in accounts)
            {
                manager.OnTimeZoneChangedOrSummerClockChange(item.AccountId, item.TimeZoneCode);
            }
        }

        private struct Account
        {
            public Guid AccountId { get; set; }
            public int TimeZoneCode { get; set; }
        }

        private const string query =
            @"
select accountid, address1_utcoffset
from account with(nolock)
where new_status = 1
";
    }
}
