﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.WorkingHours
{
    internal static class WorkingHoursConstants
    {
        internal const int DEFAULT_START_HOUR = 8;
        internal const int DEFAULT_END_HOUR = 19;
        internal const int DEFAULT_TIME_ZONE_CODE = 35;//eastern
        private const int DEFAULT_START_HOUR_MOBILE = 24;
        private const int DEFAULT_END_HOUR_MOBILE = 23;
        private const int DEFAULT_END_MINUTES_MOBILE = 30;
        internal static readonly DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit defaultWorkingHourUnit;
        internal static readonly DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit mobileDefaultWorkingUnit;

        static WorkingHoursConstants()
        {
            defaultWorkingHourUnit = new DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit();
            defaultWorkingHourUnit.DaysOfWeek.Add(DayOfWeek.Monday);
            defaultWorkingHourUnit.DaysOfWeek.Add(DayOfWeek.Tuesday);
            defaultWorkingHourUnit.DaysOfWeek.Add(DayOfWeek.Wednesday);
            defaultWorkingHourUnit.DaysOfWeek.Add(DayOfWeek.Thursday);
            defaultWorkingHourUnit.DaysOfWeek.Add(DayOfWeek.Friday);
            defaultWorkingHourUnit.DaysOfWeek.Add(DayOfWeek.Saturday);
            defaultWorkingHourUnit.FromHour = DEFAULT_START_HOUR;
            defaultWorkingHourUnit.ToHour = DEFAULT_END_HOUR;
            defaultWorkingHourUnit.Order = 1;

            mobileDefaultWorkingUnit = new DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit();
            mobileDefaultWorkingUnit.DaysOfWeek.Add(DayOfWeek.Monday);
            mobileDefaultWorkingUnit.DaysOfWeek.Add(DayOfWeek.Tuesday);
            mobileDefaultWorkingUnit.DaysOfWeek.Add(DayOfWeek.Wednesday);
            mobileDefaultWorkingUnit.DaysOfWeek.Add(DayOfWeek.Thursday);
            mobileDefaultWorkingUnit.DaysOfWeek.Add(DayOfWeek.Friday);
            mobileDefaultWorkingUnit.DaysOfWeek.Add(DayOfWeek.Saturday);
            mobileDefaultWorkingUnit.DaysOfWeek.Add(DayOfWeek.Sunday);
            mobileDefaultWorkingUnit.FromHour = DEFAULT_START_HOUR_MOBILE;
            mobileDefaultWorkingUnit.ToHour = DEFAULT_END_HOUR_MOBILE;
            mobileDefaultWorkingUnit.ToMinute = DEFAULT_END_MINUTES_MOBILE;
            mobileDefaultWorkingUnit.AllDay = true;
            mobileDefaultWorkingUnit.Order = 1;
        }
    }
}
