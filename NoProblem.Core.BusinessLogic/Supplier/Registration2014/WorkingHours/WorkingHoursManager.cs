﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014.WorkingHours;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.WorkingHours
{
    public class WorkingHoursManager : XrmUserBase
    {
        private static readonly DateTime beginingOfDay = DateTime.Now.Date;
        private static readonly DateTime endOfDay = DateTime.Now.Date.AddSeconds(-1);

        public WorkingHoursManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public void OnDayLightSavingClockChange()
        {
            DayLightSavingClockChangeThread thread = new DayLightSavingClockChangeThread();
            thread.Start();
        }

        public GetWorkingHoursResponse GetWorkingHours(Guid supplierId)
        {
            GetWorkingHoursResponse response = new GetWorkingHoursResponse();

            DateTime? vacationFrom, vacationTo;
            GetVacationData(supplierId, out vacationFrom, out vacationTo);
            response.VacationFrom = vacationFrom;
            response.VacationTo = vacationTo;

            DataAccessLayer.WorkingHoursUnit dal = new DataAccessLayer.WorkingHoursUnit(XrmDataContext);
            //find existing units
            var query = from unit in dal.All
                        where unit.new_accountid == supplierId
                        && unit.statecode == DataAccessLayer.WorkingHoursUnit.ACTIVE
                        orderby unit.new_order
                        select
                        new DataModel.Xrm.new_workinghoursunit()
                        {
                            new_order = unit.new_order,
                            new_fromhour = unit.new_fromhour,
                            new_fromminute = unit.new_fromminute,
                            new_tohour = unit.new_tohour,
                            new_tominute = unit.new_tominute,
                            new_allday = unit.new_allday,
                            new_sunday = unit.new_sunday,
                            new_monday = unit.new_monday,
                            new_tuesday = unit.new_tuesday,
                            new_wednesday = unit.new_wednesday,
                            new_thursday = unit.new_thursday,
                            new_friday = unit.new_friday,
                            new_saturday = unit.new_saturday
                        };

            //if found units, return them
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit unit =
                        new WorkingHoursUnit(item);
                    response.Units.Add(unit);
                }
                return response;
            }

            //else (if no existing units) then check if there are times in old hours fields.
            List<DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit> newUnits = CreateUnitsFromOldDisplayfields(supplierId);

            //if found in old hours fields then create units accordingly, save them, update availability and return them
            if (newUnits.Count > 0)
            {
                foreach (var item in newUnits)
                {
                    dal.Create(item, supplierId);
                    response.Units.Add(item);
                }
                XrmDataContext.SaveChanges();
                return response;
            }

            //else (old hour fields are empty) then create default unit, save it, update availability and return it.
            response.Units.Add(WorkingHoursConstants.defaultWorkingHourUnit);
            dal.Create(WorkingHoursConstants.defaultWorkingHourUnit, supplierId);
            XrmDataContext.SaveChanges();
            UpdateAvailability(supplierId, response.Units);
            return response;
        }

        private void GetVacationData(Guid supplierId, out DateTime? vacationFrom, out DateTime? vacationTo)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            //var data = dal.All.Select(x => new { Id = x.accountid, From = x.new_unavailablelocalfrom, To = x.new_unavailablelocalto }).FirstOrDefault(x => x.Id == supplierId);
            var data = (from x in dal.All
                        where x.accountid == supplierId
                        select new { Id = x.accountid, From = x.new_unavailablelocalfrom, To = x.new_unavailablelocalto }).FirstOrDefault();
            if (data == null)
                throw new Exception(String.Format("Supplier with id {0} does not exist.", supplierId));
            vacationFrom = data.From;
            vacationTo = data.To;
            if (vacationTo.HasValue)
                vacationTo = vacationTo.Value.AddDays(-1);
        }

        public void SetWorkingHours(SetWorkingHoursRequest request)
        {
            SetVacation(request.SupplierId, request.VacationFrom, request.VacationTo);

            //deactivate old units
            DataAccessLayer.WorkingHoursUnit dal = new DataAccessLayer.WorkingHoursUnit(XrmDataContext);
            var query = from unit in dal.All
                        where unit.new_accountid == request.SupplierId
                        && unit.statecode == DataAccessLayer.WorkingHoursUnit.ACTIVE
                        select new DataModel.Xrm.new_workinghoursunit { new_workinghoursunitid = unit.new_workinghoursunitid };
            foreach (var unit in query)
            {
                dal.SetState(((Microsoft.Xrm.Client.ICrmEntity)unit).LogicalName, unit.new_workinghoursunitid, false);
            }
            //save new units
            foreach (var item in request.Units)
            {
                dal.Create(item, request.SupplierId);
            }
            XrmDataContext.SaveChanges();
            //update availability
            UpdateAvailability(request.SupplierId, request.Units);
            SEODataServices.SEOSupplierDataRetriever.ClearSupplierPageDataCache(request.SupplierId);
        }

        private void SetVacation(Guid supplierId, DateTime? vacationFrom, DateTime? vacationTo)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.accountid = supplierId;
            supplier.new_unavailablelocalfrom = vacationFrom;
            if (vacationTo.HasValue)
                vacationTo = vacationTo.Value.AddDays(1);
            supplier.new_unavailablelocalto = vacationTo;
            DateTime? fromUtc = null;
            DateTime? toUtc = null;
            if (vacationFrom.HasValue)
            {
                fromUtc = FixDateToUtcFromLocal(vacationFrom.Value);
                supplier.new_unavailabilityreasonid = GetDeaultReasonId();
            }
            else
            {
                supplier.new_unavailabilityreasonid = null;
            }
            if (vacationTo.HasValue)
            {
                toUtc = FixDateToUtcFromLocal(vacationTo.Value);
            }
            supplier.new_unavailablefrom = fromUtc;
            supplier.new_unavailableto = toUtc;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        private Guid GetDeaultReasonId()
        {
            DataAccessLayer.UnavailabilityReason dal = new DataAccessLayer.UnavailabilityReason(XrmDataContext);
            return dal.GetDefaultReasonId();
        }

        public void OnTimeZoneChangedOrSummerClockChange(Guid supplierId, int timeZoneCode)
        {
            var getWorkingHoursResponse = GetWorkingHours(supplierId);
            UpdateAvailability(supplierId, getWorkingHoursResponse.Units, timeZoneCode, new DataAccessLayer.Availability(XrmDataContext));
        }

        private List<WorkingHoursUnit> CreateUnitsFromOldDisplayfields(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = (from acc in dal.All
                            where acc.accountid == supplierId
                            select new
                            {
                                acc.new_sundayavailablefrom,
                                acc.new_sundayavailableto,
                                acc.new_mondayavailablefrom,
                                acc.new_mondayavailableto,
                                acc.new_tuesdayavailablefrom,
                                acc.new_tuesdayavailableto,
                                acc.new_wednesdayavailablefrom,
                                acc.new_wednesdayavailableto,
                                acc.new_thursdayavailablefrom,
                                acc.new_thursdayavailableto,
                                acc.new_fridayavailablefrom,
                                acc.new_fridayavailableto,
                                acc.new_saturdayavailablefrom,
                                acc.new_saturdayavailableto
                            }).First();
            List<WorkingHoursUnit> units = new List<WorkingHoursUnit>();
            AddDayFromHourStrings(supplier.new_sundayavailablefrom, supplier.new_sundayavailableto, DayOfWeek.Sunday, units);
            AddDayFromHourStrings(supplier.new_mondayavailablefrom, supplier.new_mondayavailableto, DayOfWeek.Monday, units);
            AddDayFromHourStrings(supplier.new_tuesdayavailablefrom, supplier.new_tuesdayavailableto, DayOfWeek.Tuesday, units);
            AddDayFromHourStrings(supplier.new_wednesdayavailablefrom, supplier.new_wednesdayavailableto, DayOfWeek.Wednesday, units);
            AddDayFromHourStrings(supplier.new_thursdayavailablefrom, supplier.new_thursdayavailableto, DayOfWeek.Thursday, units);
            AddDayFromHourStrings(supplier.new_fridayavailablefrom, supplier.new_fridayavailableto, DayOfWeek.Friday, units);
            AddDayFromHourStrings(supplier.new_saturdayavailablefrom, supplier.new_saturdayavailableto, DayOfWeek.Saturday, units);
            for (int i = 0; i < units.Count; i++)
            {
                units[i].Order = i + 1;
            }
            return units;
        }

        private void AddDayFromHourStrings(string from, string to, DayOfWeek dayOfWeek, List<WorkingHoursUnit> units)
        {
            if (!String.IsNullOrEmpty(from) && from.Length == 5
                && !String.IsNullOrEmpty(to) && to.Length == 5)
            {
                WorkingHoursUnit unit = new WorkingHoursUnit();
                string[] fromSplit = from.Split(':');
                string[] toSplit = to.Split(':');
                unit.FromHour = int.Parse(fromSplit[0]);
                unit.FromMinute = int.Parse(fromSplit[1]);
                unit.ToHour = int.Parse(toSplit[0]);
                unit.ToMinute = int.Parse(toSplit[1]);
                if (unit.ToMinute == 59)
                {
                    unit.ToHour = 24;
                    unit.ToMinute = 0;
                }
                if (unit.FromHour == 0
                    && unit.FromMinute == 0
                    && unit.ToHour == 24)
                {
                    unit.AllDay = true;
                }
                bool found = false;
                foreach (var item in units)
                {
                    if (item.Equals(unit))
                    {
                        item.DaysOfWeek.Add(dayOfWeek);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    unit.DaysOfWeek.Add(dayOfWeek);
                    units.Add(unit);
                }
            }
        }

        private void UpdateAvailability(Guid supplierId, List<DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit> units, int timeZoneCode, DataAccessLayer.Availability dal)
        {
            //delete old availability
            var deleteQuery = from unit in dal.All
                              where unit.new_accountid == supplierId
                              select new { Id = unit.new_availabilityid };
            try
            {
                foreach (var item in deleteQuery)
                {
                    DataModel.Xrm.new_availability entity = new DataModel.Xrm.new_availability(XrmDataContext);
                    entity.new_availabilityid = item.Id;
                    dal.Delete(entity);
                }
                XrmDataContext.SaveChanges();
            }
            catch(Exception exc)
            {
                Effect.Crm.Logs.LogUtils.MyHandle.HandleException(exc, string.Format("Exception in UpdateAvailability SupplierId:{0}", supplierId));
                XrmDataContext.ClearChanges();
            }

            foreach (var unit in units)
            {
                UpdateAvailability(supplierId, unit, dal, timeZoneCode);
            }
        }

        private void UpdateAvailability(Guid supplierId, List<DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit> units)
        {
            int timeZone = GetTimeZone(supplierId);
            DataAccessLayer.Availability dal = new DataAccessLayer.Availability(XrmDataContext);
            UpdateAvailability(supplierId, units, timeZone, dal);
        }

        private void UpdateAvailability(Guid supplierId, DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit unit, DataAccessLayer.Availability dal, int timeZone)
        {
            foreach (var day in unit.DaysOfWeek)
            {
                if (unit.AllDay)
                {
                    CreateSingleAvailability(day, 0, 0, 24, 0, timeZone, dal, supplierId);
                }
                else
                {
                    CreateSingleAvailability(day, unit.FromHour, unit.FromMinute, unit.ToHour, unit.ToMinute, timeZone, dal, supplierId);
                }
            }
        }

        private int GetTimeZone(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new { timezone = acc.address1_utcoffset };
            var found = query.First();
            if (found.timezone.HasValue)
                return found.timezone.Value;
            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.accountid = supplierId;
            supplier.address1_utcoffset = WorkingHours.WorkingHoursConstants.DEFAULT_TIME_ZONE_CODE;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
            return WorkingHours.WorkingHoursConstants.DEFAULT_TIME_ZONE_CODE;
        }

        private void CreateSingleAvailability(DayOfWeek dayOfWeek, int fromHour, int fromMinute, int toHour, int toMinute, int timeZone, DataAccessLayer.Availability dal, Guid supplierId)
        {
            DateTime fromTime = DateTime.Now.Date.AddHours(fromHour).AddMinutes(fromMinute);
            DateTime toTime;
            if (toHour == 24)
            {
                toTime = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
            }
            else
            {
                toTime = DateTime.Now.Date.AddHours(toHour).AddMinutes(toMinute);
            }
            DateTime fromTimeUtc = FixDateToUtcFromLocal(fromTime, timeZone);
            DateTime toTimeUtc = FixDateToUtcFromLocal(toTime, timeZone);

            // if by changing to utc format we moved a day in the calendar
            if (fromTime.Date > fromTimeUtc.Date)
            {
                dayOfWeek = NoProblem.Core.BusinessLogic.Utils.ConvertUtils.GetPreviousDay(dayOfWeek);
            }
            else if (fromTime.Date < fromTimeUtc.Date)
            {
                dayOfWeek = NoProblem.Core.BusinessLogic.Utils.ConvertUtils.GetNextDay(dayOfWeek);
            }

            if (fromTimeUtc.Date < toTimeUtc.Date)
            {
                dal.Create(supplierId, beginingOfDay, toTimeUtc, NoProblem.Core.BusinessLogic.Utils.ConvertUtils.GetNextDay(dayOfWeek));
                toTimeUtc = endOfDay;
            }

            dal.Create(supplierId, fromTimeUtc, toTimeUtc, dayOfWeek);

            XrmDataContext.SaveChanges();
        }
    }
}
