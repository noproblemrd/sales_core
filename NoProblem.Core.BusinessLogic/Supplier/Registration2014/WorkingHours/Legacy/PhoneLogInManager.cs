﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014.Legacy;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.Legacy
{
    /// <summary>
    /// Phone and password login for legacy advertisers.
    /// </summary>
    /// @Authors: Alain Adler
    public class PhoneLogInManager : LogOnManagerBase
    {
        public PhoneLogInManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public PhoneLogInResponse PhoneLogIn(PhoneLogInRequest request)
        {
            var query = from acc in dal.All
                        where acc.telephone1 == request.Phone && acc.new_password == request.Password && acc.new_securitylevel == null
                        select
                        new
                        {
                            ContactName = acc.new_fullname,
                            Name = acc.name,
                            Balance = acc.new_cashbalance.HasValue ? acc.new_cashbalance.Value : 0,
                            SubscriptionPlan = acc.new_subscriptionplan,
                            IsLegacy = acc.new_legacyadvertiser2014.HasValue && acc.new_legacyadvertiser2014.Value,
                            Step = acc.new_stepinregistration2014.HasValue ? acc.new_stepinregistration2014.Value : (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2,
                            StepLegacy = String.IsNullOrEmpty(acc.new_steplegacy2014) ? StepsLegacy.CONFIRM_EMAIL_1 : acc.new_steplegacy2014,
                            SupplierId = acc.accountid
                        };
            int count = query.Count();
            if (count == 0)
            {
                PhoneLogInResponse response = new PhoneLogInResponse();
                response.LegacyStatus = LegacyStatuses.PHONE_PASSWORD_COMBINATION_NOT_FOUND;
                return response;
            }
            if (count > 1)
            {
                PhoneLogInResponse response = new PhoneLogInResponse();
                response.LegacyStatus = LegacyStatuses.MORE_THAN_ONE_ACCOUNT_FOUND_SEND_TO_EMAIL_LOGIN;
                return response;
            }
            var found = query.First();
            string legacyStatus = LegacyStatusChecker.CheckLegacyStatus(
                new LegacyCheckerObject()
                {
                    IsLegacy = found.IsLegacy,
                    Step = found.Step,
                    StepLegacy = found.StepLegacy,
                    SupplierId = found.SupplierId
                });
            return new PhoneLogInResponse
            {
                LegacyStatus = legacyStatus,
                Step = found.Step,
                StepLegacy = found.StepLegacy,
                SupplierId = found.SupplierId,
                Name = found.Name,
                ContactName = found.ContactName,
                Balance = found.Balance,
                SubscriptionPlan = found.SubscriptionPlan
            };
        }
    }
}
