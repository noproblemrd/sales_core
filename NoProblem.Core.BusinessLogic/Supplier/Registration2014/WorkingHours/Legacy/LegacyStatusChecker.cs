﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014.Legacy;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.Legacy
{
    class LegacyStatusChecker
    {
        public static string CheckLegacyStatus(LegacyCheckerObject legacyCheckerObj)
        {
            string retVal;
            if (!legacyCheckerObj.IsLegacy)//not legacy
            {
                retVal = LegacyStatuses.NOT_LEGACY_SEND_TO_EMAIL_LOGIN;
            }
            else//is legacy adv
            {
                //if finished doing legacy steps then go to normal (if is paying will go to dashboard, if not to plan step).
                if (StepsLegacy.FINISHED_3 == legacyCheckerObj.StepLegacy)
                {
                    if (legacyCheckerObj.Step == (int)DataModel.Xrm.account.StepInRegistration2014.Dashboard_7)
                    {
                        retVal = LegacyStatuses.LEGACY_FINISHED_SEND_TO_EMAIL_LOGIN;
                    }
                    else
                    {
                        retVal = LegacyStatuses.SEND_TO_NORMAL_STEP;
                    }
                }
                else//if not finished, send to relevant legacy step.
                {
                    retVal = LegacyStatuses.SEND_TO_LEGACY_STEP;
                }
            }
            return retVal;
        }
    }
}
