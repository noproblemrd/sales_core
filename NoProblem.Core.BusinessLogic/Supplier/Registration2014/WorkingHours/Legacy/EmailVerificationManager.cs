﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014.Legacy;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.Legacy
{
    public class EmailVerificationManager : XrmUserBase
    {
        private DataAccessLayer.AccountRepository dal;

        public EmailVerificationManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            dal = new DataAccessLayer.AccountRepository(XrmDataContext);
        }

        public EmailVerifiyLegacyPreLoadResponse EmailVerifiyLegacyPreLoad(Guid supplierId)
        {
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new
                        {
                            Email = acc.emailaddress1,
                            StepLegacy = String.IsNullOrEmpty(acc.new_steplegacy2014) ? StepsLegacy.CONFIRM_EMAIL_1 : acc.new_steplegacy2014,
                            Step = acc.new_stepinregistration2014.HasValue ? acc.new_stepinregistration2014.Value : (int)DataModel.Xrm.account.StepInRegistration2014.VerifyYourEmail_1,
                            IsLegacy = acc.new_legacyadvertiser2014.HasValue ? acc.new_legacyadvertiser2014.Value : false,
                            ContactName = acc.new_fullname,
                            Name = acc.name,
                            Balance = acc.new_cashbalance.HasValue ? acc.new_cashbalance.Value : 0,
                            SubscriptionPlan = acc.new_subscriptionplan,
                        };
            var found = query.First();
            if (found == null)
                throw new ArgumentException("supplierId is invalid");
            string legacyStatus = LegacyStatusChecker.CheckLegacyStatus(
                new LegacyCheckerObject
                {
                    IsLegacy = found.IsLegacy,
                    Step = found.Step,
                    SupplierId = supplierId,
                    StepLegacy = found.StepLegacy
                });
            EmailVerifiyLegacyPreLoadResponse response = new EmailVerifiyLegacyPreLoadResponse();
            response.LegacyStatus = legacyStatus;
            response.Step = found.Step;
            response.StepLegacy = found.StepLegacy;
            response.Email = found.Email;
            response.Balance = found.Balance;
            response.ContactName = found.ContactName;
            response.Name = found.Name;
            response.SubscriptionPlan = found.SubscriptionPlan;
            return response;
        }

        public SendVerificationEmailResponse SendEmail(Guid supplierId, string email)
        {
            if (String.IsNullOrEmpty(email))
            {
                throw new ArgumentException("email cannot be null nor empty.");
            }
            SendVerificationEmailResponse response = new SendVerificationEmailResponse();
            //check that email is unique
            bool isUniqueEmail = IsUniqueEmail(supplierId, email);
            if (isUniqueEmail)
            {
                SaveEmail(supplierId, email);
            }
            else
            {
                response.SendEmailStatus = SendVerificationEmailResponse.SendEmailStatuses.DUPLICATE_EMAIL;
                return response;
            }
            bool sent = SendEmail(email, supplierId);
            if (sent)
            {
                UpdateStep(supplierId);
                response.SendEmailStatus = SendVerificationEmailResponse.SendEmailStatuses.SENT;
            }
            else
            {
                response.SendEmailStatus = SendVerificationEmailResponse.SendEmailStatuses.FAILED_TO_SEND;
            }
            return response;
        }

        public EmailVerifiedResponse EmailVerified(Guid supplierId)
        {
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select
                        new 
                        {
                            StepLegacy = acc.new_steplegacy2014,
                            IsLegacy = acc.new_legacyadvertiser2014.HasValue ? acc.new_legacyadvertiser2014.Value : false,
                            Step = acc.new_stepinregistration2014.HasValue ? acc.new_stepinregistration2014.Value : (int)DataModel.Xrm.account.StepInRegistration2014.VerifyYourEmail_1,
                            SupplierId = acc.accountid,
                            IsExemptOfMonthlyPayment = acc.new_isexemptofppamonthlypayment.HasValue ? acc.new_isexemptofppamonthlypayment.Value : false,
                            ContactName = acc.new_fullname,
                            Name = acc.name,
                            Balance = acc.new_cashbalance.HasValue ? acc.new_cashbalance.Value : 0,
                            SubscriptionPlan = acc.new_subscriptionplan,
                        };
            var found = query.First();
            if (found == null)
                throw new ArgumentException("supplierId is invalid.");
            LegacyCheckerObject legacyObj = new LegacyCheckerObject()
            {
                IsExemptOfMonthlyPayment  = found.IsExemptOfMonthlyPayment,
                IsLegacy = found.IsLegacy,
                Step = found.Step,
                StepLegacy = found.StepLegacy,
                SupplierId = found.SupplierId
            };
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(XrmDataContext);
            supplier.accountid = supplierId;
            if (String.IsNullOrEmpty(legacyObj.StepLegacy) || legacyObj.StepLegacy == StepsLegacy.CONFIRM_EMAIL_1)
            {
                if (!legacyObj.IsExemptOfMonthlyPayment)//is someone that never payed nor had bonus
                {
                    supplier.new_steplegacy2014 = StepsLegacy.FINISHED_3;
                    legacyObj.StepLegacy = StepsLegacy.FINISHED_3;
                }
                else
                {
                    supplier.new_steplegacy2014 = StepsLegacy.PAYMENT_METHOD_2;
                    legacyObj.StepLegacy = StepsLegacy.PAYMENT_METHOD_2;
                }
            }
            if (legacyObj.Step < (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2)
            {
                supplier.new_stepinregistration2014 = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
                legacyObj.Step = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
            }
            supplier.new_isemailverified = true;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
            //check legacy status checker and return step and status.
            string legacyStatus = LegacyStatusChecker.CheckLegacyStatus(legacyObj);
            EmailVerifiedResponse response = new EmailVerifiedResponse();
            response.LegacyStatus = legacyStatus;
            response.Step = legacyObj.Step;
            response.StepLegacy = legacyObj.StepLegacy;
            response.SupplierId = supplierId;
            response.Balance = found.Balance;
            response.ContactName = found.ContactName;
            response.Name = found.Name;
            response.SubscriptionPlan = found.SubscriptionPlan;
            return response;
        }

        private bool IsUniqueEmail(Guid supplierId, string email)
        {
            var query = from acc in dal.All
                        where acc.emailaddress1 == email && acc.accountid != supplierId
                        select new { acc.accountid };
            return query.Count() == 0;
        }

        private void UpdateStep(Guid supplierId)
        {
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select acc.new_steplegacy2014;
            string legacyStep = query.First();
            if (String.IsNullOrEmpty(legacyStep))
            {
                DataModel.Xrm.account supplier = new DataModel.Xrm.account(XrmDataContext);
                supplier.accountid = supplierId;
                supplier.new_steplegacy2014 = StepsLegacy.CONFIRM_EMAIL_1;
                dal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
        }

        private void SaveEmail(Guid supplierId, string email)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(XrmDataContext);
            supplier.accountid = supplierId;
            supplier.emailaddress1 = email;
            supplier.new_isemailverified = false;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        private bool SendEmail(string email, Guid supplierId)
        {
            MandrillSender sender = new MandrillSender();
            DataAccessLayer.MandrillEmailSubjects subjects = new DataAccessLayer.MandrillEmailSubjects(XrmDataContext);
            string subject = subjects.GetEmailSubject(DataModel.MandrillTemplates.E_AD_EmailVerificaiton_Legacy2014);
            Dictionary<string, string> vars = new Dictionary<string, string>();
            string url = GetVerificationUrl(supplierId);
            string name = GetSupplierCompanyName(supplierId);
            vars.Add("NPVAR_COMPANY_NAME", name);
            vars.Add("NPVAR_VERIFY_URL", url);
            return sender.SendTemplate(email, subject, DataModel.MandrillTemplates.E_AD_EmailVerificaiton_Legacy2014, vars);
        }

        private string GetVerificationUrl(Guid supplierId)
        {
            DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string url = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.EMAIL_VERIFICATION_URL_LEGACY_2014);
            url = url + "?id=" + supplierId.ToString();
            return url;
        }

        private string GetSupplierCompanyName(Guid supplierId)
        {
            var nameQuery = from acc in dal.All
                            where acc.accountid == supplierId
                            select acc.name;
            string name = nameQuery.FirstOrDefault();
            if (name == null)
            {
                name = String.Empty;
            }
            return name;
        }
    }
}
