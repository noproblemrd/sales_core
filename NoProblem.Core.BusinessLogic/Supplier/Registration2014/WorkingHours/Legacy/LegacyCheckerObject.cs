﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.Legacy
{
    class LegacyCheckerObject
    {
        public bool IsLegacy { get; set; }
        public int Step { get; set; }
        public string StepLegacy { get; set; }
        public Guid SupplierId { get; set; }
        public bool IsExemptOfMonthlyPayment { get; set; }
    }
}
