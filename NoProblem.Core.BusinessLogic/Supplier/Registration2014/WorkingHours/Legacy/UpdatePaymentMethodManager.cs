﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.SupplierModel.Registration2014.Legacy;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.Legacy
{
    public class UpdatePaymentMethodManager : XrmUserBase
    {
        private DataAccessLayer.AccountRepository dal;

        public UpdatePaymentMethodManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            dal = new DataAccessLayer.AccountRepository(XrmDataContext);
        }

        public UpdatePaymentMethodResponse UpdatePaymentMethodOnLoad(Guid supplierId)
        {
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new
                        {
                            Email = acc.emailaddress1,
                            SubcriptionPlan = acc.new_subscriptionplan,
                            ContactName = acc.new_fullname,
                            StepLegacy = String.IsNullOrEmpty(acc.new_steplegacy2014) ? StepsLegacy.CONFIRM_EMAIL_1 : acc.new_steplegacy2014,
                            Step = acc.new_stepinregistration2014.HasValue ? acc.new_stepinregistration2014.Value : (int)DataModel.Xrm.account.StepInRegistration2014.VerifyYourEmail_1,
                            IsLegacy = acc.new_legacyadvertiser2014.HasValue ? acc.new_legacyadvertiser2014.Value : false,
                            Name = acc.name,
                            Balance = acc.new_cashbalance.HasValue ? acc.new_cashbalance.Value : 0,
                        };
            var found = query.First();
            if (found == null)
                throw new ArgumentException("supplierId is invalid");
            string legacyStatus = LegacyStatusChecker.CheckLegacyStatus(
                new LegacyCheckerObject
                {
                    IsLegacy = found.IsLegacy,
                    Step = found.Step,
                    SupplierId = supplierId,
                    StepLegacy = found.StepLegacy
                });
            UpdatePaymentMethodResponse response = new UpdatePaymentMethodResponse();
            response.LegacyStatus = legacyStatus;
            response.Step = found.Step;
            response.StepLegacy = found.StepLegacy;
            response.Email = found.Email;
            response.ContactName = found.ContactName;
            response.SubscriptionPlan = found.SubcriptionPlan;
            response.Name = found.Name;
            response.Balance = found.Balance;
            return response;
        }
    }
}
