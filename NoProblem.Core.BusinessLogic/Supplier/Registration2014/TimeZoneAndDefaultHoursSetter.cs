﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.Utils;
using System.Xml;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    internal class TimeZoneAndDefaultHoursSetter : Runnable
    {
        private Guid supplierId;
        private decimal latitude;
        private decimal longitude;

        public TimeZoneAndDefaultHoursSetter(Guid supplierId, decimal latitude, decimal longitude)
        {
            this.supplierId = supplierId;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        protected override void Run()
        {
            int timeZoneCode = GetTimeZone();
            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.accountid = supplierId;
            supplier.address1_utcoffset = timeZoneCode;
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
            SetDefaultWorkHours();
        }

        private int GetTimeZone()
        {
            return CoverAreas.MapUtils.GetTimeZoneCodeFromLatitudeAndLongitude(latitude, longitude, XrmDataContext);
        }

        private void SetDefaultWorkHours()
        {
            WorkingHours.WorkingHoursManager hoursManager = new WorkingHours.WorkingHoursManager(XrmDataContext);
            var hoursRequest = new DataModel.SupplierModel.Registration2014.WorkingHours.SetWorkingHoursRequest();
            hoursRequest.SupplierId = supplierId;
            hoursRequest.Units.Add(GetDefualtWorkingUnit());
            hoursManager.SetWorkingHours(hoursRequest);
        }

        private DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit GetDefualtWorkingUnit()
        {
            if (IsMobile())
            {
                return WorkingHours.WorkingHoursConstants.mobileDefaultWorkingUnit;
            }
            return WorkingHours.WorkingHoursConstants.defaultWorkingHourUnit;

        }

        private bool IsMobile()
        {
            DataAccessLayer.MobileDevice dal = new DataAccessLayer.MobileDevice(XrmDataContext);
            var query = from device in dal.All
                        where device.new_isauthorized == true
                        && device.new_accountid == supplierId
                        select new { id = device.new_mobiledeviceid };
            return query.FirstOrDefault() != null;
        }
    }
}
