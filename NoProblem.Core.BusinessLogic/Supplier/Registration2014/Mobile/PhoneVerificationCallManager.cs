﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014.Mobile
{
    public class PhoneVerificationCallManager
    {
        public bool MakeCall(string phone, string pin)
        {
            FoneApiBL.Handlers.PinCall.PinCallHandler callHandler =
                new FoneApiBL.Handlers.PinCall.PinCallHandler();
            return callHandler.StartCall(phone, pin);
        }
    }
}
