﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.SupplierBL.Registration2014
{
    public class EnterBusinessDetailsManager : XrmUserBase
    {
        public EnterBusinessDetailsManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public GetBusinessDetailsResponse GetBusinessDetails(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new { Name = acc.name, Address = acc.new_fulladdress, Latitude = acc.new_latitude, Longitude = acc.new_longitude, 
                            Phone = acc.telephone1, Plan = acc.new_subscriptionplan, ContactPersionName = acc.new_fullname, 
                            Email = acc.emailaddress1, Website = acc.websiteurl, ReferralCode = acc.new_referralcode};
            var found = query.FirstOrDefault();
            if (found == null)
                throw new Exception("Supplier with the given SupplierId was not found");
            GetBusinessDetailsResponse response = new GetBusinessDetailsResponse();
            response.Name = found.Name;
            response.Phone = found.Phone;
            response.Address = found.Address;
            decimal lat, lon;
            decimal.TryParse(found.Latitude, out lat);
            decimal.TryParse(found.Longitude, out lon);
            response.Latitude = lat;
            response.Longitude = lon;
            response.SubscriptionPlan = found.Plan;
            response.ContactPersonName = found.ContactPersionName;
            response.Email = found.Email;
            response.Website = found.Website;
            response.ReferralCode = found.ReferralCode;
            return response;
        }
        public EnterBusinessDetailsResponse EnterBusinessDetails(EnterBusinessDetailsRequest request)
        {
            /*
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string BusinessAddress { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public string MobileNumber { get; set; }
             */
            ValidateRequest(request);
            EnterBusinessDetailsResponse response = new EnterBusinessDetailsResponse();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == request.SupplierId
                        select new { Step = acc.new_stepinregistration2014 };
            var currentStepFound = query.FirstOrDefault();
            if (currentStepFound == null)
                throw new Exception("Supplier with the given SupplierId was not found");

            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.accountid = request.SupplierId;

            supplier.name = request.Name;
            if (!string.IsNullOrEmpty(request.Address))
                supplier.new_fulladdress = request.Address;
            supplier.telephone1 = request.Phone;
            if (request.Latitude.HasValue)
                supplier.new_latitude = request.Latitude.Value.ToString();
            if (request.Longitude.HasValue)
                supplier.new_longitude = request.Longitude.Value.ToString();
            if (!string.IsNullOrEmpty(request.StreetAddress))
                supplier.address1_line1 = request.StreetAddress;
            if (!string.IsNullOrEmpty(request.City))
                supplier.address1_city = request.City;
            if (!string.IsNullOrEmpty(request.Country))
                supplier.address1_country = request.Country;           
            if (!string.IsNullOrEmpty(request.ContactPersonName))
            {
                supplier.new_fullname = request.ContactPersonName.Trim();
                string first, last;
                SplitName(request.ContactPersonName, out first, out last);
                supplier.new_firstname = first;
                supplier.new_lastname = last;
            }
            if (!string.IsNullOrEmpty(request.State))
                supplier.address1_stateorprovince = request.State;

            int? step = currentStepFound.Step;
            if (!step.HasValue || step.Value <= (int)DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_General_3)
            {
                step = (int)DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_Category_4;
                supplier.new_stepinregistration2014 = step;
            }

            dal.Update(supplier);
            XrmDataContext.SaveChanges();
            response.Step = step.Value;

            if (request.Latitude.HasValue && request.Longitude.HasValue)
            {
                TimeZoneAndDefaultHoursSetter setter = new TimeZoneAndDefaultHoursSetter(request.SupplierId, request.Latitude.Value, request.Longitude.Value);
                setter.Start();
            }

            return response;
        }
        public EnterBusinessDetailsResponse EnterBusinessDetailsV2(EnterBusinessDetailsRequest request)
        {
            /*
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string WebSite { get; set; }
        public string ReferralCode { get; set; }
             */
            ValidateRequest(request);
            EnterBusinessDetailsResponse response = new EnterBusinessDetailsResponse();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var query = from acc in dal.All
                        where acc.accountid == request.SupplierId
                        select new { Step = acc.new_stepinregistration2014 };
            var currentStepFound = query.FirstOrDefault();
            if (currentStepFound == null)
                throw new Exception("Supplier with the given SupplierId was not found");

            DataModel.Xrm.account supplier = new DataModel.Xrm.account();
            supplier.accountid = request.SupplierId;

            supplier.name = request.Name;
            supplier.telephone1 = request.Phone;
            supplier.emailaddress1 = request.Email;
            supplier.websiteurl = !string.IsNullOrEmpty(request.Website) ? request.Website : null;
            supplier.new_referralcode = !string.IsNullOrEmpty(request.ReferralCode) ? request.ReferralCode : null;
           
            int? step = currentStepFound.Step;
            if (!step.HasValue || step.Value <= (int)DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_General_3)
            {                
                step = (int)DataModel.Xrm.account.StepInRegistration2014.BusinessDetails_Category_4;
                supplier.new_stepinregistration2014 = step;                
            }

            dal.Update(supplier);
            XrmDataContext.SaveChanges();
            response.Step = step.Value;           

            return response;
        }
       
        private void SplitName(string fullName, out string first, out string last)
        {
            if (String.IsNullOrEmpty(fullName))
            {
                first = String.Empty;
                last = String.Empty;
            }
            else
            {
                fullName = fullName.Trim();
                int index = fullName.IndexOf(" ");
                if (index > 0)
                {
                    first = fullName.Substring(0, index).Trim();
                    last = fullName.Substring(index + 1).Trim();
                }
                else
                {
                    first = fullName;
                    last = String.Empty;
                }
            }
        }

        private static void ValidateRequest(EnterBusinessDetailsRequest request)
        {
            if (request.SupplierId == Guid.Empty)
                throw new ArgumentException("SupplierId must be provided.", "SupplierId");
            if (String.IsNullOrEmpty(request.Name))
                throw new ArgumentException("Name must be provided.", "Name");
            if (String.IsNullOrEmpty(request.Phone))
                throw new ArgumentException("Phone must be provided.", "Phone");
            /*
            if (String.IsNullOrEmpty(request.Address))
                throw new ArgumentException("Address must be provided.", "Address");
             * */
        }
    }
}
