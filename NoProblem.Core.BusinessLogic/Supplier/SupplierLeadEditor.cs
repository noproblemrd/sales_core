﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.SupplierBL
{
    public class SupplierLeadEditor : XrmUserBase
    {
        public SupplierLeadEditor(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            dal = new DataAccessLayer.IncidentAccount(XrmDataContext);
        }

        DataAccessLayer.IncidentAccount dal;

        public void UpdateLeadAddress(string newAddress, Guid incidentAccountId)
        {
            DataModel.Xrm.new_incidentaccount ia = new DataModel.Xrm.new_incidentaccount(XrmDataContext);
            ia.new_incidentaccountid = incidentAccountId;
            ia.new_adveditedaddress = newAddress;
            dal.Update(ia);
            XrmDataContext.SaveChanges();
        }

        public void UpdateLeadCustomerName(string newName, Guid incidentAccountId)
        {
            DataModel.Xrm.new_incidentaccount ia = new DataModel.Xrm.new_incidentaccount(XrmDataContext);
            ia.new_incidentaccountid = incidentAccountId;
            ia.new_adveditedcustomername = newName;
            dal.Update(ia);
            XrmDataContext.SaveChanges();
        }
    }
}
