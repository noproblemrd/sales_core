﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.DialerModel;
using System.Data;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    internal class ReroutingManager : XrmUserBase
    {
        public ReroutingManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        internal RerouteGetNextSupplierResponse GetNextSupplier(NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.IncomingCalls.IncomingCallsContainer container, out DataModel.Xrm.account.DapazStatus dapazStatus, bool isOnlyCheck)
        {
            dapazStatus = NoProblem.Core.DataModel.Xrm.account.DapazStatus.PPA;
            RerouteGetNextSupplierResponse response = new RerouteGetNextSupplierResponse();
            Guid regionId = container.RerouteRegionId;
            Guid headingId = container.RerouteHeadingId;
            List<Guid> contactedAdvs = container.ContactedAdvertisers;
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
            parameters.Add("@PrimaryExpertiseId", headingId);
            parameters.Add("@RegionId", regionId);
            DataTable table = new DataTable();
            table.Columns.Add("id", typeof(Guid));
            foreach (var item in contactedAdvs)
            {
                table.Rows.Add(item);
            }
            parameters.Add("@doneSuppliersIds", table);
            var reader = dal.ExecuteReader(queryStored, parameters, CommandType.StoredProcedure);
            if (reader.Count > 0)
            {
                var row = reader.First();
                response.SupplierId = (Guid)row["AccountId"];
                response.TargetPhoneNumber = Convert.ToString(row["telephone1"]);
                response.RecordCall = row["new_recordcalls"] != DBNull.Value ? (bool)row["new_recordcalls"] : false;
                DataModel.Xrm.account.DapazStatus dStatus = (NoProblem.Core.DataModel.Xrm.account.DapazStatus)(int)row["new_dapazstatus"];
                dapazStatus = dStatus;
                DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                response.IvrAdvertiserAnnounce = configSettingsAccess.GetConfigurationSettingValue(DataModel.ConfigurationKeys.IVR_ADVERTISER_ANNOUNCE);
                if (dapazStatus == NoProblem.Core.DataModel.Xrm.account.DapazStatus.RarahQuote && row["New_RarahQuoteLeft"] != DBNull.Value)
                {
                    int rarahCallsLeft = (int)row["New_RarahQuoteLeft"];
                    if (rarahCallsLeft > 0
                        && rarahCallsLeft <= 5)
                    {
                        string rarahAdvertiserAnnounceBase = configSettingsAccess.GetConfigurationSettingValue(DataModel.ConfigurationKeys.IVR_ADVERTISER_ANNOUNCE_RARAH_QUOTE);
                        response.IvrAdvertiserAnnounce = rarahAdvertiserAnnounceBase + rarahCallsLeft;
                    }
                }
                if (dStatus == NoProblem.Core.DataModel.Xrm.account.DapazStatus.PPA
                    || dStatus == NoProblem.Core.DataModel.Xrm.account.DapazStatus.RarahQuote)
                {
                    response.DisplayPhoneNumber = configSettingsAccess.GetConfigurationSettingValue(DataModel.ConfigurationKeys.ADVERTISER_DISPLAY_NUMBER);
                }
                else
                {
                    response.DisplayPhoneNumber = container.CustomerPhone;
                }
                if (!isOnlyCheck)
                    container.ContactedAdvertisers.Add(response.SupplierId);
            }
            return response;
        }

        #region getNextSupplierQuery

        private const string queryStored = "GetSupplierForRerouting";

        private const string query =
            @"
declare @Availability datetime
if @Availability is null 
begin
	set @Availability = getdate()
end

declare @DayOfWeek int
if @DayOfWeek is null
begin 
	set @DayOfWeek =  datepart(DW,getdate())
end 
--------------------------------------------------------------------------------- 
create table #TempMatchPrimaryExpertise
      (New_AccountExpertiseId uniqueidentifier, accountId uniqueidentifier, dapazStatus int)
     
      create index idx_nu_na_tempmatch on #TempMatchPrimaryExpertise(accountId)include(New_AccountExpertiseId,dapazStatus)
 
/*** check for suppliers that match to @PrimaryExpertiseId ****/
insert into #TempMatchPrimaryExpertise (New_AccountExpertiseId, accountId, dapazStatus)
SELECT tempAccounts.New_AccountExpertiseId, tempAccounts.accountid, tempAccounts.new_dapazstatus
FROM (SELECT isnull(ae.New_AccountExpertiseId, '00000000-0000-0000-0000-000000000000') New_AccountExpertiseId
, accountid, isnull(ae.new_incidentprice, 0) new_incidentprice
,isnull(Account.new_dapazstatus, 1) new_dapazstatus
FROM Account with (nolock)
LEFT JOIN New_AccountExpertise ae with (nolock) ON ae.New_accountId = Account.AccountId AND
      ae.deletionstatecode = 0 AND ae.statecode = 0 
WHERE  
		((account.new_unavailablefrom > @Availability OR account.new_unavailableto < @Availability OR
				(account.new_unavailablefrom IS NULL AND account.new_unavailableto IS NULL))
		and (ae.new_incidentprice <= Account.new_availablecashbalance) or Account.new_dapazstatus > 1)
		and (Account.telephone1 IS NOT NULL) 
		AND Account.AccountId NOT IN ({0})
      
    and (Account.new_firstpaymentneeded is null or Account.new_firstpaymentneeded = 0)
    and (Account.new_waitingforinvitation is null or Account.new_waitingforinvitation = 0)
    AND (Account.new_dapazstatus is null or Account.new_dapazstatus = 1 or Account.new_dapazstatus = 2 or Account.new_dapazstatus = 5)
    AND Account.new_status = 1

	AND (((Account.new_dapazstatus is null or Account.new_dapazstatus = 1) and ae.new_primaryexpertiseid = @PrimaryExpertiseId) 
		or 
		(Account.new_dapazstatus > 1 and Account.new_rarahheadingid = @PrimaryExpertiseId))
      
) tempAccounts
 
--select account.name, #TempMatchPrimaryExpertise.New_AccountExpertiseId, #TempMatchPrimaryExpertise.dapazStatus, #TempMatchPrimaryExpertise.accountId from #TempMatchPrimaryExpertise
--join Account on account.AccountId = #TempMatchPrimaryExpertise.accountId
------------------------------------------------------------------
 
create table #TempAvailabilityDate
      (New_AccountId uniqueidentifier,New_AccountExpertiseId uniqueidentifier,isAvailable bit, dapazStatus int)
 

/*** check for suppliers that match to @AvailabilityDate ****/
insert into #TempAvailabilityDate
SELECT distinct #TempMatchPrimaryExpertise.accountId,#TempMatchPrimaryExpertise.New_AccountExpertiseId  
,(
CASE WHEN 
	SUM(CASE WHEN 				
 (new_from <= CONVERT(char(8), @Availability ,108) AND new_to >= CONVERT(char(8), @Availability ,108))
					THEN 1 
					ELSE 0 END) > 0 
 THEN 1  ELSE 0 END
) as isAvailable
,#TempMatchPrimaryExpertise.dapazStatus
FROM #TempMatchPrimaryExpertise  
LEFT JOIN new_availability with (nolock) ON (new_availability.new_accountid = #TempMatchPrimaryExpertise.accountid
            AND new_availability.deletionstatecode = 0 AND new_day = @DayOfWeek )
group by #TempMatchPrimaryExpertise.accountId,#TempMatchPrimaryExpertise.New_AccountExpertiseId,#TempMatchPrimaryExpertise.dapazStatus

--select * from #TempAvailabilityDate

----------------------------------------------------------------------------------------
create table #TempMatchCity
      (New_AccountExpertiseId uniqueidentifier, accountid uniqueidentifier) 

declare @RegionLevel int
declare @Regioncode nvarchar(100)

select @RegionLevel = new_level, @Regioncode = new_code from new_region where
new_regionid = @RegionId

create table #tmpRegionHirarchy ( RegionLevel int, RegionId uniqueidentifier, ParentRegionId uniqueidentifier );
create index idx_region on #tmpRegionHirarchy(RegionId) ;
 
WITH RegionHirarchy(RegionLevel,RegionId,ParentRegionId)
AS
(
      SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
      FROM new_region with(nolock)
      WHERE new_region.new_level = @RegionLevel AND new_code = @Regioncode and new_region.deletionstatecode = 0
      UNION ALL
      SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
      FROM new_region with(nolock)
      INNER JOIN RegionHirarchy ON RegionHirarchy.ParentRegionId = new_regionid
)
insert into #tmpRegionHirarchy
      select RegionLevel,  RegionId,  ParentRegionId from RegionHirarchy
     
	 
      CREATE TABLE #tmpJoinning ( AccountExpertiseId uniqueidentifier, RegionId uniqueidentifier, RegionState int, accountid uniqueidentifier)
      insert into #tmpJoinning
                  select #TempAvailabilityDate.New_AccountExpertiseId , regaccount.new_regionid, regaccount.New_regionstate, regaccount.new_accountid
      from #TempAvailabilityDate
      INNER JOIN(
      select a.new_regionid, a.New_regionstate, a.new_accountid, b.deletionstatecode
      from dbo.New_regionaccountexpertiseExtensionBase a
      join dbo.New_regionaccountexpertiseBase b
      on a.New_regionaccountexpertiseId = b.New_regionaccountexpertiseId
      where b.deletionstatecode = 0) regaccount  ON regaccount.new_accountid = #TempAvailabilityDate.new_accountid
INNER JOIN #tmpRegionHirarchy ON #tmpRegionHirarchy.RegionId = regaccount.new_regionid   
    where #TempAvailabilityDate.isAvailable = 1
     
      CREATE TABLE #tmpNotGoodCity (accountexpertise uniqueidentifier)
      INSERT INTO #tmpNotGoodCity
      SELECT #tmpJoinning.AccountExpertiseId
      FROM #tmpJoinning
      WHERE
     (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel = @RegionLevel) and #tmpJoinning.Regionstate != 1)
     or (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel != @RegionLevel) and #tmpJoinning.Regionstate = 2)
           
INSERT INTO #TempMatchCity (New_AccountExpertiseId, accountid)
SELECT distinct AccountExpertiseId, accountid
FROM #tmpJoinning
WHERE #tmpJoinning.AccountexpertiseId not in ( select accountexpertise from #tmpNotGoodCity )
union
select #TempAvailabilityDate.New_AccountExpertiseId, #TempAvailabilityDate.New_AccountId
from #TempAvailabilityDate 
join accountextensionbase acc on acc.accountid = #TempAvailabilityDate.New_AccountId
where #TempAvailabilityDate.dapazStatus > 1 and acc.new_rarahregionid = @RegionId
 
------------------------------------------------------------------
 
/*** return match suppliers by social fairness order ****/
SELECT TOP 1 Account.AccountId
            ,Account.telephone1
            ,isnull(Account.new_dapazstatus, 1) new_dapazstatus
			,Account.new_recordcalls
FROM  #TempMatchCity MatchedSuppliers
            INNER JOIN Account with (nolock) ON MatchedSuppliers.accountid = Account.accountid
			LEFT JOIN new_accountexpertise with(nolock) ON new_accountexpertise.new_accountexpertiseid = MatchedSuppliers.New_AccountExpertiseId
Order By isnull(Account.new_dapazstatus, 1) ASC, isnull(new_accountexpertise.new_incidentprice, 0) DESC 

drop table #TempMatchPrimaryExpertise
drop table #TempAvailabilityDate
drop table #TempMatchCity
drop table #tmpRegionHirarchy
drop table #tmpJoinning
drop table #tmpNotGoodCity
";

        #endregion
    }
}
