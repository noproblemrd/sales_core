﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    public static class PhoneUtility
    {
        public static string GetPhoneDisplay(string phone)
        {
            StringBuilder result = new StringBuilder("+1 (", 20);
            for (int i = 0; i < phone.Length; i++)
            {
                if (i == 3)
                    result.Append(") ");
                else if (i == 6)
                    result.Append("-");
                result.Append(phone[i]);
            }
            return result.ToString();
        }
    }
}
