﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web.Services.Protocols;
using Effect.Crm.Configuration;
using Effect.Crm.Convertor;
using Effect.Crm.Logs;
using Effect.Crm.Methods;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using NoProblem.Core.BusinessLogic.Utils;
using NoProblem.Core.DataModel;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic
{
    public class Phones : ContextsUserBase
    {
        #region Ctor
        public Phones(DML.Xrm.DataContext xrmDataContext, CrmService crmService)
            : base(xrmDataContext, crmService)
        {
        }
        #endregion

        #region delegates

        private delegate void DirectNumberContactedDelegate(string customerPhoneNumber, string directPhoneNumber, int reasonCode, string callId, int duration, string dnQueryId, string recordingUrl, bool isReroutedCall, Guid? reroutedSupplierId, Guid? rerouteHeadingId, Guid? rerouteOriginId, string custom);
        private delegate void UpdateIncidentAccountDelegate(string callId, Guid incidentId, Guid accountId, bool isLengthOK);
        private delegate void SendSuccConnectionNotifications(string incidentId, string accountId);
        private delegate void OpenCallCenterCaseDelegate(Guid headingId, Guid originId, DML.SqlHelper.ContactMinData customer, string dnQueryId, bool isRarah, string directPhoneNumber, string country);

        #endregion

        #region Public Methods

        public string DirectNumberContactedAsync(string customerPhoneNumber, string directPhoneNumber, int reasonCode, string callId, int duration, string dnQueryId, string recordingUrl, bool isReroutedCall, Guid? reroutedSupplierId, Guid? rerouteHeadingId, Guid? rerouteOriginId, string custom)
        {
            DirectNumberContactedDelegate asyncCall = new DirectNumberContactedDelegate(DirectNumberContacted);
            asyncCall.BeginInvoke(customerPhoneNumber, directPhoneNumber, reasonCode, callId, duration, dnQueryId, recordingUrl, isReroutedCall, reroutedSupplierId, rerouteHeadingId, rerouteOriginId, custom, null, null);
            return "0,Success";
        }

        /// <summary>
        /// This function Handles the proccess of a new Direct number call
        /// </summary>
        /// <param name="customerPhoneNumber">The customer who called te Direct number</param>
        /// <param name="directNumber">The number to which the customer has called</param>
        /// <param name="hasAnswerd">Indicating whether the supplier answerd the phone call or not</param>
        public void DirectNumberContacted(string customerPhoneNumber, string directPhoneNumber, int reasonCode, string callId, int duration, string dnQueryId, string recordingUrl, bool isReroutedCall, Guid? reroutedSupplierId, Guid? rerouteHeadingId, Guid? rerouteOriginId, string custom)
        {
            try
            {
                Guid incidentID = Guid.Empty;
                Guid incidentaccountid = Guid.Empty;

                NoProblem.Core.BusinessLogic.Dialer.DirectCallContactedIdentificationHandler identifier = new Dialer.DirectCallContactedIdentificationHandler(XrmDataContext);
                Dialer.DirectCallContactedIdentificationHandler.DirectCallTypeContainer identityContainer =
                    identifier.IdentifyCallType(
                     directPhoneNumber,
                     dnQueryId,
                     isReroutedCall,
                     reroutedSupplierId,
                     rerouteHeadingId,
                     rerouteOriginId,
                     custom);

                var customer = GetContactByMobile(customerPhoneNumber, identityContainer.Country, identityContainer.CallType == Dialer.DirectCallContactedIdentificationHandler.eCallType.PingPhone);
                if (reasonCode == 0 & identityContainer.MinCallDuration > duration)
                {
                    reasonCode = 1;
                }
                if (identityContainer.IsTest)
                {
                    if (identityContainer.CallType == Dialer.DirectCallContactedIdentificationHandler.eCallType.PingPhone)
                    {
                        ExecuteCallbackToOrigin(reasonCode == 0, identityContainer, custom);
                    }
                    return;
                }
                Dialer.CreateIncidentContainer request = new Dialer.CreateIncidentContainer();
                request.CustomerData = customer;
                request.CustomerPhone = customerPhoneNumber;
                request.OriginId = identityContainer.OriginId;
                request.PrimaryExpertise = identityContainer.CategoryId;
                request.DnQueryId = dnQueryId;
                request.DapazStatus = identityContainer.DapazStatus;
                request.Country = identityContainer.Country;
                request.CallType = identityContainer.CallType;
                request.Custom = custom;
                request.VirtualNumber = directPhoneNumber;
                incidentID = GetIncidentID(request);
                incidentaccountid = CreateDNIncidentAccount(incidentID, identityContainer.SupplierId, reasonCode, callId, identityContainer.CategoryId, customer, duration, identityContainer.PriceOfCall, customer, null, identityContainer.DapazStatus, directPhoneNumber, recordingUrl);
                DataAccessLayer.IncidentAccount inciAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                var incidentAccount = inciAccDal.Retrieve(incidentaccountid);
                bool toCharge = incidentAccount.new_tocharge.HasValue && incidentAccount.new_tocharge.Value;
                if (toCharge)
                {
                    if (customer.IsBlockedNumberContact == false)
                    {
                        DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
                        var upsale = upsaleDal.GetUpsaleByIncidentId(incidentID);
                        if (upsale != null)
                        {
                            upsale.new_numofconnectedadvertisers =
                                upsale.new_numofconnectedadvertisers.HasValue ? upsale.new_numofconnectedadvertisers.Value + 1 : 1;
                            upsaleDal.Update(upsale);
                        }
                    }
                    DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                    DML.Xrm.incident incident = incidentDal.Retrieve(incidentID);
                    incident.new_ordered_providers += 1;
                    incidentDal.Update(incident);
                    XrmDataContext.SaveChanges();
                    if (customer.IsBlockedNumberContact == false && (!identityContainer.DapazStatus.HasValue || identityContainer.DapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.PPA))
                    {
                        bool refunded = CheckIncidentTimePass(incidentAccount, XrmDataContext);
                        if (refunded)
                            toCharge = false;
                    }
                    DailyBudgetManager budgetManager = new DailyBudgetManager(XrmDataContext);
                    budgetManager.CheckDailyBudgetIsReached(incidentAccount.new_accountid.Value, false);
                    if (Dapaz.PpaOnlineSync.SyncConfiguration.IsEnabled)
                    {
                        Dapaz.PpaOnlineSync.Syncher syncher = new Dapaz.PpaOnlineSync.Syncher(identityContainer.SupplierId);
                        syncher.Start();
                    }
                }
                else//not to charge.
                {
                    //This will make affiliate pay status to "not for sale".
                    NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestCloser closer = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestCloser(XrmDataContext);
                    closer.CloseRequest(incidentID);
                    //give back one to daily budget counter
                    NoProblem.Core.BusinessLogic.Dialer.CallEndSynchronizer endSync = new Dialer.CallEndSynchronizer(XrmDataContext);
                    endSync.UpsertDailyBudgetCounter(identityContainer.SupplierId);
                }
                Dialer.CdrManager cdrManager = new Dialer.CdrManager(XrmDataContext);
                cdrManager.CreateDNCdr(incidentaccountid, duration);

                ExecuteCallbackToOrigin(toCharge, identityContainer, custom);

                //Send email to the team:
                Notifications notificationsManager = new Notifications(XrmDataContext);
                DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string recipientEmail = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.NEW_REQUEST_ALERT_EMAIL);
                string subject = string.Empty;
                string body = string.Empty;
                notificationsManager.InstatiateTemplate(DML.TemplateNames.NEW_REQUEST_ALERT, incidentID, "incident", out subject, out body);
                notificationsManager.SendEmail(recipientEmail, body, subject, configDal);

                //send to slack (uncomment when paying slack so we don't waste all the free trial messages)
                //Slack.SlackNotifier slack = new Slack.SlackNotifier(subject + "\n" + body, Slack.SlackNotifier.eSlackChannel.new_request);
                //slack.Start();
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "Exception in async method DirectNumberContacted");
            }
        }

        private void ExecuteCallbackToOrigin(bool isBillable, Dialer.DirectCallContactedIdentificationHandler.DirectCallTypeContainer identityContainer, string custom)
        {
            if (identityContainer.CallType == Dialer.DirectCallContactedIdentificationHandler.eCallType.PingPhone)
            {
                APIs.SellersApi.PingPhone.CallbackSenderThread callback = new APIs.SellersApi.PingPhone.CallbackSenderThread(isBillable, custom);
                callback.Start();
            }
        }

        public string UpdateRecordingFileLocation(string callId, string fileLocation)
        {
            string retVal = string.Empty;
            DataAccessLayer.IncidentAccount inciAccAccess = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DML.Xrm.new_incidentaccount incidentAccount = inciAccAccess.GetIncidentAccountByRecordId(callId);
            if (incidentAccount != null)
            {
                incidentAccount.new_recordfilelocation = fileLocation;
                inciAccAccess.Update(incidentAccount);
                XrmDataContext.SaveChanges();
                retVal = "0,success";
            }
            else
            {
                DataAccessLayer.NonPpcRequest nonPpcDal = new NoProblem.Core.DataAccessLayer.NonPpcRequest(XrmDataContext);
                DML.Xrm.new_nonppcrequest nonPpc = nonPpcDal.GetNonPpcRequestByRecordId(callId);
                if (nonPpc != null)
                {
                    nonPpc.new_recordfilelocation = fileLocation;
                    nonPpcDal.Update(nonPpc);
                    XrmDataContext.SaveChanges();
                    retVal = "0,success";
                }
                else
                {
                    DataAccessLayer.DDIDCase ddidCaseDal = new NoProblem.Core.DataAccessLayer.DDIDCase(XrmDataContext);
                    bool isUpdated = ddidCaseDal.UpdateRecordFileLocation(callId, fileLocation);
                    if (isUpdated)
                    {
                        retVal = "0,success";
                    }
                    else
                    {
                        LogUtils.MyHandle.WriteToLog(1, "UpdateRecordingFileLocation - RecordId was not found. recordId = {0}, fileLocation = {1}", callId, fileLocation);
                        retVal = "1,record id not found";
                    }
                }
            }
            return retVal;
        }

        public bool ConsumerContactedSucc(string incidentIdStr, string accountIdStr, string callId, string duration)
        {
            Guid incidentId = new Guid(incidentIdStr);
            Guid accountId = new Guid(accountIdStr);
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            bool isLengthOK = true;
            string minLengthStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.MINIMAL_CALL_LENGTH_TO_CHARGE);
            int minLength;
            if (int.TryParse(minLengthStr, out minLength))
            {
                if (minLength > 0)
                {
                    LogUtils.MyHandle.WriteToLog(8, "ConsumerContactedSucc duration is {0}", duration);
                    int dur;
                    if (int.TryParse(duration, out dur))
                    {
                        if (dur < minLength)
                        {
                            isLengthOK = false;
                        }
                    }
                }
            }
            bool charged = UpdateIncidentAccountInConsumerContactedSucc(callId, incidentId, accountId, isLengthOK);
            //UpdateIncidentAccountDelegate updateAsync = new UpdateIncidentAccountDelegate(UpdateIncidentAccountInConsumerContactedSucc);
            //updateAsync.BeginInvoke(callId, incidentId, accountId, isLengthOK, null, null);
            //updateAsync.Invoke(callId, incidentId, accountId, isLengthOK);
            if (isLengthOK)
            {
                SendSuccConnectionNotifications sendAsync = new SendSuccConnectionNotifications(SendConsumerContactedSuccNotifications);
                sendAsync.BeginInvoke(incidentIdStr, accountIdStr, null, null);
            }
            return charged;
        }

        public void UpdateIncidentStatusCode(string incidentIdStr, int statusCodeSR, string cdrxml)
        {
            Guid incidentId = new Guid(incidentIdStr);
            Thread cdrThread = new Thread(delegate()
            {
                try
                {
                    LogUtils.MyHandle.WriteToLog(9, "CDR work started");
                    Dialer.CdrManager cdrManager = new Dialer.CdrManager(null);//null data context because data context is not thread safe.
                    cdrManager.CreateCdrs(cdrxml, incidentId, false);
                    LogUtils.MyHandle.WriteToLog(9, "CDR work ended");
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "cdrThread failed with exception in ServiceRequestCompleted.");
                }
            });
            cdrThread.Start();
            Thread mainWorkThread = new Thread(delegate()
            {
                try
                {
                    int statuscode = 0;
                    int dialerStatus = 0;
                    ServiceRequest.Incident incidentLogic = new ServiceRequest.Incident(XrmDataContext, CrmService);
                    Notifications notificationsManager = new Notifications(XrmDataContext);
                    switch (statusCodeSR)
                    {
                        // All the provider were contected
                        case 0:
                            {
                                statuscode = (int)DML.Xrm.incident.Status.WORK_DONE;
                                incidentLogic.UpdateMatchSuppliersToLose(incidentId);
                                break;
                            }
                        // didnt contected all the providers or any of them
                        case 1:
                            DML.enumNotificationMethod notifymethod = notificationsManager.GetNotificationMethod(DML.NotificationConfigurationKeys.NOTIFICATION_SUPPLIER_UNREACHED);
                            if (notifymethod != DML.enumNotificationMethod.NONE)
                            {
                                NotifyUnreachedSuppliers(incidentId);
                            }
                            statuscode = (int)DML.Xrm.incident.Status.WORK_DONE;
                            incidentLogic.UpdateMatchSuppliersToLose(incidentId);
                            break;
                        // Stop request recived
                        case 3:
                            {
                                dialerStatus = (int)DML.Xrm.incident.DialerStatus.REQUEST_CANCELED;
                                incidentLogic.UpdateMatchSuppliersToLose(incidentId);
                                break;
                            }
                        // Consumer is unavailable
                        case 2:
                            {
                                statuscode = (int)DML.Xrm.incident.Status.CUSTOMER_NOT_AVAILABLE_DIALER;
                                SendConsumerIsUnavailableSmses(incidentId);
                                incidentLogic.UpdateMatchSuppliersToLose(incidentId);
                                break;
                            }
                        // Some error
                        case 4:
                            {
                                dialerStatus = (int)DML.Xrm.incident.DialerStatus.ERROR;
                                ErrorHandler.ReportError(EntityName.incident, incidentIdStr, "0", "An Error has ocured in the dialer, callstatus:4.", "Inc2Handler", "UpdateIncidentStatusCode");
                                incidentLogic.UpdateMatchSuppliersToLose(incidentId);
                                break;
                            }
                        default:
                            {
                                ErrorHandler.ReportError(EntityName.incident, incidentIdStr, "0", "Status Code not supported. StatusCode:" + statusCodeSR, "Inc2Handler", "UpdateIncidentStatusCode");
                                incidentLogic.UpdateMatchSuppliersToLose(incidentId);
                                break;
                            }
                    }
                    GiveAvailableMoneyBackToErrorSuppliers(incidentId);
                    DataAccessLayer.Incident incidentDAL = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                    //DML.Xrm.incident inci = new NoProblem.Core.DataModel.Xrm.incident(XrmDataContext);
                    DML.Xrm.incident inci = incidentDAL.Retrieve(incidentId);
                    //inci.incidentid = incidentId;
                    if (dialerStatus == 0)
                    {
                        dialerStatus = (int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED;
                        notificationsManager.NotifyCustomer(DML.enumCustomerNotificationTypes.SUPPLIERS_CONTACTED, inci.customerid.Value, incidentId.ToString(), "incident", incidentId.ToString(), "incident");
                    }
                    //inci.new_dialerstatus = dialerStatus;
                    if (statuscode == 0)
                    {
                        statuscode = (int)DML.Xrm.incident.Status.WORK_DONE;
                    }
                    //inci.statuscode = statuscode;
                    //inci.new_winningprice = GetIncidentWinningPrice(inci);
                    //incidentDAL.Update(inci);
                    //XrmDataContext.SaveChanges();

                    //I changed this code to old crm mode code to see if this solved our strage bug that something the incident
                    //does not update itself but no exception is thrown.

                    decimal winningPrice = GetIncidentWinningPrice(inci);
                    DynamicEntity inciDynamic = Utils.DynamicEntityUtils.GetNew("incident");
                    Utils.DynamicEntityUtils.SetPropertyValue(inciDynamic, "incidentid", ConvertorCRMParams.GetCrmKey(incidentId), typeof(KeyProperty));
                    Utils.DynamicEntityUtils.SetPropertyValue(inciDynamic, "new_winningprice", ConvertorCRMParams.GetCrmMoney(winningPrice), typeof(CrmMoneyProperty));
                    MethodsUtils.UpdateEntity(CrmService, inciDynamic);
                    SendLoserNotifications(inci, winningPrice);

                    //AAR.AarManager aarManager = new NoProblem.Core.BusinessLogic.AAR.AarManager(XrmDataContext);
                    //bool usingAar = aarManager.ContinueServiceRequest(incidentId);
                    bool usingAar = false;
                    if (!usingAar)
                    {
                        Utils.DynamicEntityUtils.SetPropertyValue(inciDynamic, "statuscode", ConvertorCRMParams.GetCrmStatus(statuscode), typeof(StatusProperty));
                        Utils.DynamicEntityUtils.SetPropertyValue(inciDynamic, "new_dialerstatus", ConvertorCRMParams.GetCrmPicklist(dialerStatus), typeof(PicklistProperty));
                        MethodsUtils.UpdateEntity(CrmService, inciDynamic);
                        ServiceRequest.ServiceRequestCloser closer = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestCloser(null);
                        closer.CloseRequest(incidentId);
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "mainWorkThread failed with exception in ServiceRequestCompleted.");
                }
            });
            mainWorkThread.Start();
        }

        public DML.DNQueryResult GetPhoneNumber(string dNQueryId, string directPhoneNumber, string consumerPhoneNumber)
        {
            DML.DNQueryResult response = new DML.DNQueryResult();
            //check consumerPhoneNumber is not black listed:
            DML.SqlHelper.ContactMinData contact = GetContactByMobile(consumerPhoneNumber, null);
            if (contact.IsBlackList)
            {
                response.IsBlacklisted = true;
                response.DirectPhoneNumber = directPhoneNumber;
                return response;
            }
            /* DAPAZ
            if (!GlobalConfigurations.IsUsaSystem && IsDapazGoldenNumberAdvertiser(dNQueryId, directPhoneNumber, consumerPhoneNumber, response))
            {
                return response;
            }
            */
            DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            response.DirectPhoneNumber = directPhoneNumber;
            Guid originId = Guid.Empty;
            Guid headingId = Guid.Empty;
            string countryOfLead = GlobalConfigurations.DefaultCountry;
            string isRealTimeRoutingQuery = @"select top 1 new_originid as originid, new_realtimerouting as realtimerouting, new_primaryexpertiseid as realtimeroutingheadingid, new_country as country, new_ispingphoneincomingdid as isPingPhoneDid
				from new_directnumber with(nolock) where new_mappingnumber = @number and deletionstatecode = 0";
            DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            DataAccessLayer.DirectNumber.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_directnumber>.SqlParametersList();
            parameters.Add("@number", directPhoneNumber);
            var isReatTimeRoutingReader = dnDal.ExecuteReader(isRealTimeRoutingQuery, parameters);
            bool isRealTimeRouting = false;
            bool isPingPhoneApi = false;
            foreach (var row in isReatTimeRoutingReader)
            {
                isRealTimeRouting = row["realtimerouting"] != DBNull.Value ? (bool)row["realtimerouting"] : false;
                string convertedCountry = Convert.ToString(row["country"]);
                if (!String.IsNullOrEmpty(convertedCountry))
                    countryOfLead = convertedCountry;
                if (isRealTimeRouting)
                {
                    originId = (Guid)row["originid"];
                    headingId = (Guid)row["realtimeroutingheadingid"];
                }
                else
                {
                    isPingPhoneApi = row["isPingPhoneDid"] != DBNull.Value ? (bool)row["isPingPhoneDid"] : false;
                }
            }
            DataModel.Xrm.account.DapazStatus? dapazStatus = null;
            bool isLeadBuyer = false;
            int rarahQuoteLeft = -1;
            string incomingCallCallerPrompt = null;
            int limitCallDuration = -1;
            decimal pricePerCall = 0;
            if (isPingPhoneApi)
            {
                //LogUtils.MyHandle.WriteToLog(1, "isPingPhoneApi consumerPhoneNumber={0}", consumerPhoneNumber);
                HandlePingPhoneDid(consumerPhoneNumber, response, ref isLeadBuyer, ref incomingCallCallerPrompt, ref limitCallDuration, ref pricePerCall);
            }
            else if (!isRealTimeRouting)
            {
                HandleNormalDid(directPhoneNumber, response, ref originId, ref headingId, ref dapazStatus, ref isLeadBuyer, ref rarahQuoteLeft, ref incomingCallCallerPrompt, ref limitCallDuration, ref pricePerCall);
            }
            else//is real time routing
            {
                //Decide which who will get the call (partners with phone api).
                HandleRealTimeRouting(response, headingId, countryOfLead, ref isLeadBuyer, ref incomingCallCallerPrompt, ref limitCallDuration, ref pricePerCall);
            }
            if (isLeadBuyer)
            {
                response.DisplayPhoneNumber = consumerPhoneNumber;
            }
            else
            {
                response.DisplayPhoneNumber = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.ADVERTISER_DISPLAY_NUMBER);
            }
            // if the supplier is not available we need to route the call to the call center
            if (!response.IsAvailable)
            {
                response.DisplayPhoneNumber = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.CALL_CENTER_PHONE);
                response.TruePhoneNumber.Clear();
                response.TruePhoneNumber.Add(response.DisplayPhoneNumber);
                response.MappingStatus = "MappingFailed;SupplierNotActive";
                OpenCallCenterCaseDelegate del = new OpenCallCenterCaseDelegate(OpenCallCenterCaseAsync);
                del.BeginInvoke(headingId, originId, contact, dNQueryId, /*isRarah*/false, directPhoneNumber, countryOfLead, OpenCallCenterCaseAsyncCallBack, del);
            }
            else
            {
                if (dapazStatus.HasValue
                    && dapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.RarahQuote
                    && rarahQuoteLeft > 0
                    && rarahQuoteLeft <= 5)
                {
                    response.IvrCallerAnnounce = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.IVR_CALLER_ANNOUNCE_RARAH_QUOTE);
                    string rarahAdvertiserAnnounceBase = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.IVR_ADVERTISER_ANNOUNCE_RARAH_QUOTE);
                    response.IvrAdvertiserAnnounce = rarahAdvertiserAnnounceBase + rarahQuoteLeft;
                    response.DisplayPhoneNumber = consumerPhoneNumber;
                }
                else
                {
                    response.IvrAdvertiserAnnounce = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.IVR_ADVERTISER_ANNOUNCE);
                    response.IvrCallerAnnounce = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.IVR_CALLER_ANNOUNCE);
                }
                CallStartSynchronization(response.ProviderId, pricePerCall);
            }

            if (!string.IsNullOrEmpty(incomingCallCallerPrompt))
            {
                response.IvrCallerAnnounceAdvertiserSpecific = incomingCallCallerPrompt;
            }
            response.LimitCallDurationSeconds = limitCallDuration;
            return response;
        }

        private void HandlePingPhoneDid(string consumerPhoneNumber, DNQueryResult response, ref bool isLeadBuyer, ref string incomingCallCallerPrompt, ref int limitCallDuration, ref decimal pricePerCall)
        {
            APIs.SellersApi.PingPhone.IncomingCallHandler handler = new APIs.SellersApi.PingPhone.IncomingCallHandler(XrmDataContext);
            APIs.SellersApi.PingPhone.IncomingCallHandler.IncomingCallContainer container = handler.IncomingPhone(consumerPhoneNumber);
            isLeadBuyer = container.IsLeadBuyer;
            incomingCallCallerPrompt = container.IncomingCallCallerPrompt;
            limitCallDuration = container.LimitCallDuration;
            pricePerCall = container.PricePerCall;
            response.IsAvailable = container.IsAvailable;
            response.ProviderId = container.SupplierId;
            response.IsRecording = container.RecordCalls;
            response.TruePhoneNumber.Add(container.TruePhoneNumber);
            response.SupplierCountry = container.SupplierCountry;
            response.CustomField = container.Custom;
        }

        private static void HandleRealTimeRouting(DML.DNQueryResult response, Guid headingId, string country, ref bool isLeadBuyer, ref string incomingCallCallerPrompt, ref int limitCallDuration, ref decimal pricePerCall)
        {
            Dialer.RealTimeRouting.RealTimeRouter.RealTimeRouterResponse realTimeRouterData = Dialer.RealTimeRouting.RealTimeRouter.GetSupplierForRealTimeRouting(headingId, country);
            pricePerCall = realTimeRouterData.PricePerCall;
            isLeadBuyer = realTimeRouterData.IsLeadBuyer;
            incomingCallCallerPrompt = realTimeRouterData.IncomingCallCallerPrompt;
            limitCallDuration = realTimeRouterData.LimitCallDuration;
            pricePerCall = realTimeRouterData.PricePerCall;
            response.IsAvailable = realTimeRouterData.IsAvailable;
            response.ProviderId = realTimeRouterData.SupplierId;
            response.IsRecording = realTimeRouterData.RecordCalls;
            response.TruePhoneNumber.Add(realTimeRouterData.TruePhoneNumber);
            response.SupplierCountry = realTimeRouterData.SupplierCountry;
        }

        private void HandleNormalDid(string directPhoneNumber, DML.DNQueryResult response, ref Guid originId, ref Guid headingId, ref DataModel.Xrm.account.DapazStatus? dapazStatus, ref bool isLeadBuyer, ref int rarahQuoteLeft, ref string incomingCallCallerPrompt, ref int limitCallDuration, ref decimal pricePerCall)
        {
            response.IsBlacklisted = false;
            response.IsAvailable = false;
            string headingCode = string.Empty;
            try
            {
                string query = @"
				DECLARE @date date = dbo.fn_UTCToLocalTimeByTimeZoneCode(getdate(), (select new_value from new_configurationsetting where new_key = 'PublisherTimeZone'))
				declare @UtcNow datetime
			   set @UtcNow = GETUTCDATE()
			   declare @new_unavailableto datetime
			   declare @new_unavailablefrom datetime
			   declare @day int
			   declare @number varchar(20)
			   set @day = datepart (dw, @UtcNow)
			   set @number = '{0}'
			   SELECT telephone1,account.accountid,new_presentationphone,new_recordcalls,new_accountexpertise.new_primaryexpertiseid,pe.new_code,new_directnumber.new_originid
				--,new_region.new_code as regionCode,new_region.new_level,
			   ,CASE 
			   WHEN (account.new_unavailablefrom IS NULL AND account.new_unavailableto IS NULL) THEN 1
			   WHEN (@UtcNow > account.new_unavailableto) THEN 1
			   WHEN (@UtcNow < account.new_unavailablefrom) THEN 1
			   ELSE 0
			   END AS IsAvailable,
			   CASE 
				   WHEN new_availabilityid IS NULL THEN 0
				   ELSE 1
			   END AS IsWorkHours,
			   CASE
			   WHEN (new_accountexpertise.new_incidentprice <= account.new_availablecashbalance) THEN 1
			   ELSE 0
			   END AS HasAvailableMoney,
				CASE
				WHEN new_status = 1 THEN 1 WHEN new_status = 5 THEN 2 ELSE 0
				END AS Status
				,new_dapazstatus
				,new_rarahquoteleft
				,new_isleadbuyer
				,new_incomingcallcallerprompt
				,new_limitcallduration
				,new_accountexpertise.new_incidentprice
				,New_PaymentMethodFailed
				,account.new_country supplierCountry
			   FROM account with(nolock) 
			   INNER JOIN new_accountexpertise with(nolock) ON new_accountexpertise.new_accountid = account.accountid AND new_accountexpertise.deletionstatecode = 0 AND new_accountexpertise.statecode = 0
			   INNER JOIN new_primaryexpertise pe with(nolock) ON pe.new_primaryexpertiseid = new_accountexpertise.new_primaryexpertiseid
			   INNER JOIN new_directnumber with(nolock) ON new_directnumber.new_accountexpertiseid = new_accountexpertise.new_accountexpertiseid AND new_directnumber.deletionstatecode = 0
			   LEFT JOIN new_availability with (nolock) ON new_availability.new_accountid = account.accountid AND new_availability.deletionstatecode = 0 AND
				   new_availability.new_day = @day
				   AND convert(char(8), @UtcNow, 108) BETWEEN convert(char(8), new_availability.new_from, 108) AND convert(char(8), new_availability.new_to, 108)
			   LEFT JOIN (
					select Count , AccountId
					from DailyBudgetCounter 
					where Date = @date
				) counters	on counters.AccountId = Account.AccountId
			   WHERE new_mappingnumber = @number
			   and isnull(counters.Count, 0) < isnull(Account.New_DailyBudget, 99999)
";
                using (SqlConnection connection = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(string.Format(query, directPhoneNumber), connection))
                    {
                        command.Connection.Open();
                        using (SqlDataReader getPhoneDetails = command.ExecuteReader())
                        {
                            if (getPhoneDetails.Read())
                            {
                                if (getPhoneDetails["new_dapazstatus"] != DBNull.Value)
                                {
                                    dapazStatus = (DataModel.Xrm.account.DapazStatus)(int)getPhoneDetails["new_dapazstatus"];
                                }
                                if (getPhoneDetails["new_rarahquoteleft"] != DBNull.Value)
                                {
                                    rarahQuoteLeft = (int)getPhoneDetails["new_rarahquoteleft"];
                                }
                                headingCode = (string)getPhoneDetails["new_code"];
                                originId = (Guid)getPhoneDetails["new_originid"];
                                response.ProviderId = new Guid(getPhoneDetails["accountid"].ToString());
                                response.IsRecording = ConvertorUtils.ToBoolean(getPhoneDetails["new_recordcalls"]);
                                response.TruePhoneNumber.Add(Convert.ToString(getPhoneDetails["telephone1"]));
                                response.SupplierCountry = Convert.ToString(getPhoneDetails["supplierCountry"]);
                                headingId = (Guid)getPhoneDetails["new_primaryexpertiseid"];
                                incomingCallCallerPrompt = Convert.ToString(getPhoneDetails["new_incomingcallcallerprompt"]);
                                if (getPhoneDetails["new_limitcallduration"] != DBNull.Value)
                                {
                                    int limitLocal = (int)getPhoneDetails["new_limitcallduration"];
                                    if (limitLocal > 0)
                                    {
                                        limitCallDuration = limitLocal;
                                    }
                                }
                                if (getPhoneDetails["new_isleadbuyer"] != DBNull.Value)
                                {
                                    isLeadBuyer = (bool)getPhoneDetails["new_isleadbuyer"];
                                }
                                bool paymentMethodFailed = false;
                                if (getPhoneDetails["New_PaymentMethodFailed"] != DBNull.Value)
                                {
                                    paymentMethodFailed = (bool)getPhoneDetails["New_PaymentMethodFailed"];
                                }
                                bool havAvailableMoney = ConvertorUtils.ToBoolean(getPhoneDetails["HasAvailableMoney"]);
                                pricePerCall = (decimal)getPhoneDetails["new_incidentprice"];
                                if (ConvertorUtils.ToBoolean(getPhoneDetails["IsAvailable"]) &&
                                    ConvertorUtils.ToBoolean(getPhoneDetails["IsWorkHours"]) &&
                                    (GlobalConfigurations.IsUsaSystem ? !paymentMethodFailed : havAvailableMoney) &&
                                    (int)getPhoneDetails["Status"] > 0)
                                {
                                    response.IsAvailable = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtils.MyHandle.HandleException(ex, "GetPhoneNumber Failed with exception and returning default phone number");
            }
        }

        private void CallStartSynchronization(Guid supplierId, decimal pricePerCall)
        {
            NoProblem.Core.BusinessLogic.Dialer.CallStartSynchronizer synchronizer = new Dialer.CallStartSynchronizer(supplierId, pricePerCall);
            synchronizer.Start();
        }

        private bool IsDapazGoldenNumberAdvertiser(string dNQueryId, string directPhoneNumber, string consumerPhoneNumber, DNQueryResult response)
        {
            string query =
                @"select top 1 acc.telephone1, acc.new_status, acc.accountid, new_dapazstatus, new_rarahheadingid, new_rarahregionid, dn.new_originid from 
new_directnumber dn with(nolock)
join new_accountexpertise ae with(nolock) on ae.new_accountexpertiseid = dn.new_accountexpertiseid
join account acc with(nolock) on acc.accountid = ae.new_accountid
where acc.deletionstatecode = 0
and ae.deletionstatecode = 0 and ae.statecode = 0
and dn.deletionstatecode = 0
and ae.new_primaryexpertiseid = @genericHeading
and dn.new_mappingnumber = @virtualNumber";
            DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            Guid genericHeadingId = dal.GetGenericHeadingId();//environments other than israel should not have a generic heading to be  
            if (genericHeadingId == Guid.Empty)
            {
                return false;
            }
            DataAccessLayer.DirectNumber.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_directnumber>.SqlParametersList();
            parameters.Add("@genericHeading", genericHeadingId);
            parameters.Add("@virtualNumber", directPhoneNumber);
            var reader = dal.ExecuteReader(query, parameters);
            if (reader.Count > 0)
            {
                var row = reader.First();
                response.CallType = "GoldenNumberAdvertiser";
                DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                //give golden number advertisers the number of the customer as caller id
                response.DisplayPhoneNumber = consumerPhoneNumber;
                response.IsRecording = false;
                response.IsWaitForDigit = "0";
                response.ProviderId = (Guid)row["accountid"];
                response.IvrAdvertiserAnnounce = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.IVR_ADVERTISER_ANNOUNCE);
                response.IvrCallerAnnounce = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.IVR_CALLER_ANNOUNCE);
                bool isActive = row["new_status"] != DBNull.Value ? (int)row["new_status"] == 1 : false;
                DataModel.Xrm.account.DapazStatus dapazStatus = (DataModel.Xrm.account.DapazStatus)((int)row["new_dapazstatus"]);
                if (isActive && (dapazStatus == DML.Xrm.account.DapazStatus.RarahGold || dapazStatus == DML.Xrm.account.DapazStatus.FixGold))
                {
                    string realPhone = Convert.ToString(row["telephone1"]);
                    string countryPhonePrefix = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.COUNTRY_PHONE_PREFIX);
                    string internalPhonePrefix = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.INTERNAL_PHONE_PREFIX);
                    response.IsAvailable = true;
                    response.IsBlacklisted = false;
                    response.TruePhoneNumber.Add(realPhone);
                    response.MappingStatus = "MappingSuccess";
                    //reroute stuff:
                    if (configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.AUTO_REROUTE_ENABLED) == 1.ToString())
                    {
                        response.Reroute = dapazStatus == NoProblem.Core.DataModel.Xrm.account.DapazStatus.RarahGold;
                        if (response.Reroute && row["new_rarahheadingid"] != DBNull.Value && row["new_rarahregionid"] != DBNull.Value)
                        {
                            response.RerouteHeadingId = (Guid)row["new_rarahheadingid"];
                            response.RerouteRegionId = (Guid)row["new_rarahregionid"];
                            response.RerouteOriginId = (Guid)row["new_originid"];
                        }
                        else
                        {
                            response.Reroute = false;
                        }
                    }
                }
                else
                {
                    response.IsAvailable = false;
                    response.IsBlacklisted = false;
                    response.MappingStatus = "MappingFailed;SupplierNotActive";
                }
                return true;
            }
            return false;
        }

        public bool IsDirectNumber(string phoneNumber)
        {
            DataAccessLayer.DirectNumber dal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            return dal.IsDirectNumber(phoneNumber);
        }

        public string CleanPhoneNumber(string phone)
        {
            for (int i = phone.Length - 1; i >= 0; i--)
            {
                if (!char.IsDigit(phone, i))
                {
                    phone = phone.Remove(i, 1);
                }
            }
            return phone;
        }

        /// <summary>
        /// Do not use this for new code. Only for use for legacy code for Asterisk, Twilo and sending SMS messages. You use use Dialer.PhoneToDialNormalizer class
        /// </summary>
        /// <param name="countryPrefix"></param>
        /// <param name="internalPrefix"></param>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        [Obsolete("Do not use this for new code. Only for use for legacy code for Asterisk, Twilo and sending SMS messages. You use use Dialer.PhoneToDialNormalizer class")]
        public static string Deprecated_AppendPhonePrefix(string countryPrefix, string internalPrefix, string phoneNumber)
        {
            string retVal = string.Empty;
            if (phoneNumber.StartsWith("+"))
            {
                retVal = phoneNumber;
            }
            else
            {
                string phoneStart = phoneNumber.Substring(0, internalPrefix.Length);
                if (phoneStart == internalPrefix)
                {
                    retVal = countryPrefix + phoneNumber.Substring(internalPrefix.Length);
                }
                else
                {
                    retVal = countryPrefix + phoneNumber;
                }
            }
            return retVal;
        }

        public static bool IsMobilePhone(string phoneNumber)
        {
            return !string.IsNullOrEmpty(phoneNumber);
        }

        #endregion

        #region Private Methods

        private void OpenCallCenterCaseAsync(Guid headingId, Guid originId, DML.SqlHelper.ContactMinData customer, string dnQueryId, bool isRarah, string directPhoneNumber, string country)
        {
            if (originId != Guid.Empty && headingId != Guid.Empty)//if this are empty means the direct number is not connected to any supplier. We don't create a case for this because we don't know the heading or anything.
            {
                var createIncidentRequest = new Dialer.CreateIncidentContainer();
                createIncidentRequest.DnQueryId = dnQueryId;
                createIncidentRequest.CustomerData = customer;
                createIncidentRequest.CustomerPhone = customer.MobilePhone;
                createIncidentRequest.IsRarah = isRarah;
                createIncidentRequest.OriginId = originId;
                createIncidentRequest.PrimaryExpertise = headingId;
                createIncidentRequest.IsCallCenterCase = true;
                createIncidentRequest.Country = country;
                Phones phonesObject = new Phones(null, null);//create new context because this method runs async.
                Guid incidentId = phonesObject.GetIncidentID(createIncidentRequest);
            }
            else
            {
                DataAccessLayer.DynamicDID dynamicDidDal = new NoProblem.Core.DataAccessLayer.DynamicDID(null);
                var localXrmContext = dynamicDidDal.XrmDataContext;
                headingId = dynamicDidDal.GetHeadingIdFromNumber(directPhoneNumber);
                bool isDynamicDid = headingId != Guid.Empty;
                DML.Xrm.new_unmappeddnquery unmapped = new DML.Xrm.new_unmappeddnquery();
                unmapped.new_directphonenumber = directPhoneNumber;
                unmapped.new_contactid = customer.Id;
                unmapped.new_dnqueryid = dnQueryId;
                unmapped.new_isdynamicdid = isDynamicDid;
                if (isDynamicDid)
                {
                    unmapped.new_primaryexpertiseid = headingId;
                }
                DataAccessLayer.UnmappedDnQuery unmappedDal = new NoProblem.Core.DataAccessLayer.UnmappedDnQuery(localXrmContext);
                unmappedDal.Create(unmapped);
                localXrmContext.SaveChanges();
            }
        }

        private void OpenCallCenterCaseAsyncCallBack(IAsyncResult result)
        {
            try
            {
                ((OpenCallCenterCaseDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (SoapException soapExc)
            {
                LogUtils.MyHandle.HandleException(soapExc, "Exception (SoapException) in async method OpenCallCenterCase. InnerXml = {0}", soapExc.Detail.InnerXml);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in async method OpenCallCenterCase");
            }
        }

        private void SaveRarahDnTempCase(Guid rarahId, string rarahPhone, Guid alternateId, string alternatePhone, string dnQueryId)
        {
            Thread tr = new Thread(new ParameterizedThreadStart(SaveRarahDnTempCaseAsync));
            object[] arr = new object[5];
            arr[0] = rarahId;
            arr[1] = rarahPhone;
            arr[2] = alternateId;
            arr[3] = alternatePhone;
            arr[4] = dnQueryId;
            tr.Start(arr);
        }

        private void SaveRarahDnTempCaseAsync(object obj)
        {
            object[] arr = (object[])obj;
            Guid rarahId = (Guid)arr[0];
            string rarahPhone = (string)arr[1];
            Guid payingId = (Guid)arr[2];
            string payingPhone = (string)arr[3];
            string dnQueryId = (string)arr[4];
            DataAccessLayer.RarahDnTempCase dal = new NoProblem.Core.DataAccessLayer.RarahDnTempCase(null);
            DML.Xrm.new_rarahdntempcase tmpCase = new NoProblem.Core.DataModel.Xrm.new_rarahdntempcase();
            tmpCase.new_rarahid = rarahId;
            tmpCase.new_rarahphone = rarahPhone;
            tmpCase.new_payingid = payingId;
            tmpCase.new_payingphone = payingPhone;
            tmpCase.new_dnqueryid = dnQueryId;
            dal.Create(tmpCase);
            dal.XrmDataContext.SaveChanges();
        }

        private KeyValuePair<Guid, string> GetMoreSuppliers(string regionCode, int regionLevel, string headingCode, Guid originId)
        {
            FindSupplierParams param = new FindSupplierParams();
            param.AreaCode = regionCode;
            param.AreaLevel = regionLevel;
            param.IsIncludingTrial = false;
            param.IsWithNoTimeNorCash = false;
            param.IsAar = false;
            param.Incidents = new List<Guid>();
            param.OriginId = originId;
            param.ExpertiseLevel = 1;
            param.ExpertiseCode = headingCode;
            param.Availability = DateTime.Now;
            SqlHelper helper = new SqlHelper(ConfigurationManager.AppSettings["updateConnectionString"]);
            System.Data.DataSet searchSupplierResult = helper.GetSuppliers(param);
            System.Data.DataTable availableSuppliersTable = searchSupplierResult.Tables[0];
            List<DML.AdvertiserRankings.SupplierEntry> suppliersList = null;
            if (availableSuppliersTable.Columns[0].ColumnName == "ERROR")
            {
                LogUtils.MyHandle.WriteToLog(8, "GetSuppliers returned with Error in Phones.GetMoreSuppliers. error = {1}", availableSuppliersTable.Rows[0]["ERROR"].ToString());
            }
            else
            {
                AdvertiserRankingsManager rankingManager = new AdvertiserRankingsManager(XrmDataContext);
                suppliersList = rankingManager.SortSuppliersByRanking(availableSuppliersTable);
            }
            KeyValuePair<Guid, string> retVal;
            if (suppliersList != null && suppliersList.Count > 0)
            {
                var supplier = suppliersList.First();
                retVal = new KeyValuePair<Guid, string>(supplier.SupplierId, supplier.Telephone1);
            }
            else
            {
                retVal = new KeyValuePair<Guid, string>();
            }
            return retVal;
        }

        private void SendConsumerIsUnavailableSmses(Guid incidentId)
        {
            LogUtils.MyHandle.WriteToLog(9, "SendConsumerIsUnavailableSmses started");
            DataAccessLayer.IncidentAccount inciAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            IEnumerable<DML.Xrm.new_incidentaccount> calls = inciAccDal.GetIncidentAccountsByIncidentId(incidentId);
            calls = from c in calls
                    where (
                    c.statuscode == (int)DML.Xrm.new_incidentaccount.Status.IN_AUCTION
                    || c.statuscode == (int)DML.Xrm.new_incidentaccount.Status.Waiting)
                    select c;
            Notifications notifier = new Notifications(XrmDataContext);
            DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
            var upsale = upsaleDal.GetUpsaleByIncidentId(incidentId);
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            var incident = incidentDal.Retrieve(incidentId);
            bool upsaleChanged = false;
            foreach (var call in calls)
            {
                bool supplierNotified = notifier.NotifySupplier(DML.enumSupplierNotificationTypes.CONSUMER_UNAVAILABLE, call.new_accountid.Value, incidentId.ToString(), "incident", incidentId.ToString(), "incident");
                if (supplierNotified)
                {
                    call.statuscode = (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS;
                    call.new_tocharge = true;
                    inciAccDal.Update(call);
                    if (upsale != null)
                    {
                        upsale.new_numofconnectedadvertisers =
                            upsale.new_numofconnectedadvertisers.HasValue ? upsale.new_numofconnectedadvertisers.Value + 1 : 1;
                        incident.new_ordered_providers += 1;
                        upsaleChanged = true;
                    }
                    XrmDataContext.SaveChanges();
                    CheckIncidentTimePass(call, XrmDataContext);
                    DailyBudgetManager budgetManager = new DailyBudgetManager(XrmDataContext);
                    budgetManager.CheckDailyBudgetIsReached(call.new_accountid.Value, false);
                    notifier.NotifyCustomer(DML.enumCustomerNotificationTypes.CONSUMER_UNAVAILABLE, call.new_incident_new_incidentaccount.customerid.Value, call.new_accountid.Value.ToString(), "account", call.new_accountid.Value.ToString(), "account");
                }
            }
            if (upsaleChanged)
            {
                upsaleDal.Update(upsale);
                incidentDal.Update(incident);
            }
            XrmDataContext.SaveChanges();
            LogUtils.MyHandle.WriteToLog(9, "SendConsumerIsUnavailableSmses ended");
        }

        /// <summary>
        /// Will sent an sms to all losers of the incident that their price was lower than the winning price.
        /// </summary>
        /// <param name="inci"></param>
        private void SendLoserNotifications(NoProblem.Core.DataModel.Xrm.incident inci, decimal winningPrice)
        {
            Notifications notificator = new Notifications(XrmDataContext);
            foreach (var inciAcc in inci.new_incident_new_incidentaccount)
            {
                if (inciAcc.statuscode == (int)DML.Xrm.new_incidentaccount.Status.LOST && inciAcc.new_potentialincidentprice.Value < winningPrice)
                {
                    notificator.NotifySupplier(DML.enumSupplierNotificationTypes.SUPPLIER_LOSER, inciAcc.new_accountid.Value, inci.incidentid.ToString(), "incident", inci.incidentid.ToString(), "incident");
                }
            }
        }

        private decimal GetIncidentWinningPrice(DML.Xrm.incident incident)
        {
            DML.XrmDataContext.ClearCache("new_incidentaccount");
            decimal price = incident.new_winningprice.HasValue ? incident.new_winningprice.Value : 0;
            foreach (var inciAcc in incident.new_incident_new_incidentaccount)
            {
                if ((inciAcc.statuscode == (int)DML.Xrm.new_incidentaccount.Status.CLOSE ||
                    inciAcc.statuscode == (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS)
                    &&
                    inciAcc.new_potentialincidentprice.Value > price)
                {
                    price = inciAcc.new_potentialincidentprice.Value;
                }
            }
            return price;
        }

        /// <summary>
        /// This function checks if the phone number is equal to a value that represent a blocked number
        /// </summary>
        /// <param name="phoneNumber">The phone number to check</param>
        /// <returns></returns>
        internal static bool IsBlockedNumber(string phoneNumber)
        {
            if (phoneNumber == DML.Constants.BLOCKED_PHONE_NUMBER || Utils.GeneralUtils.IsNumericValue(phoneNumber) == false)
                return true;
            else
                return false;
        }

        private DML.SqlHelper.ContactMinData GetContactByMobile(string mobilephone, string country)
        {
            return GetContactByMobile(mobilephone, country, false);
        }

        /// <summary>
        /// This function retrives the ID of the contact by the supplied phone number. 
        /// if the phone number is Blocked then we return the ID of the anonymous contact
        /// if the phone number was not found in the CRM then we return the ID of the newly created anonymous contact.
        /// </summary>
        /// <param name="mobilephone">The contact's phone number</param>
        /// <returns>The ID of the contact</returns>
        private DML.SqlHelper.ContactMinData GetContactByMobile(string mobilephone, string country, bool isFromApi)
        {
            DML.SqlHelper.ContactMinData contact = null;
            DataAccessLayer.Contact contactDal = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
            if (IsBlockedNumber(mobilephone))
            {
                Guid contactId = contactDal.GetBlockedNumberContactId();
                contact = new NoProblem.Core.DataModel.SqlHelper.ContactMinData();
                contact.Id = contactId;
                contact.IsBlackList = false;
                contact.IsBlockedNumberContact = true;
                contact.MobilePhone = string.Empty;
            }
            else
            {
                contact = contactDal.GetContactMinDataByPhoneNumber(mobilephone);
                if (contact == null)
                {
                    DML.Xrm.contact anonymous = contactDal.CreateAnonymousContact(mobilephone, null, null, null, false, null, null, null, country, isFromApi, null, null, false);
                    contact = new NoProblem.Core.DataModel.SqlHelper.ContactMinData();
                    contact.Id = anonymous.contactid;
                    contact.IsBlackList = false;
                    contact.MobilePhone = mobilephone;
                    contact.IsBlockedNumberContact = false;
                }
                else//existing contact. update it if needed.
                {
                    DataModel.Xrm.contact c = new DML.Xrm.contact(XrmDataContext);
                    c.contactid = contact.Id;
                    bool updated = false;
                    if (!String.IsNullOrEmpty(country))
                    {
                        c.new_country = country;
                        updated = true;
                    }
                    if ((isFromApi && !c.new_isfromapi.IsTrue()) || (!isFromApi && c.new_isfromapi.IsTrue()))
                    {
                        c.new_isfromapi = isFromApi;
                        updated = true;
                    }
                    if (updated)
                    {
                        contactDal.Update(c);
                        XrmDataContext.SaveChanges();
                    }
                }
            }
            return contact;
        }

        private Guid GetUpsaleId(Guid consumerId, Guid expertiseId)
        {
            DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
            Guid primaryExptertiseId = expertiseId;
            Guid upsaleId = upsaleDal.FindOpenUpsale(consumerId, primaryExptertiseId);
            if (upsaleId == Guid.Empty)
            {
                DML.Xrm.new_upsale upsale = new NoProblem.Core.DataModel.Xrm.new_upsale();
                upsale.new_primaryexpertiseid = primaryExptertiseId;
                upsale.new_contactid = consumerId;
                upsale.new_numofrequestedadvertisers = 1;
                upsale.new_timetocall = FixDateToLocal(DateTime.Now);
                upsaleDal.Create(upsale);
                XrmDataContext.SaveChanges();
                upsaleId = upsale.new_upsaleid;
            }
            else
            {
                DML.Xrm.new_upsale upsale = upsaleDal.Retrieve(upsaleId);
                upsale.new_numofrequestedadvertisers += 1;
                upsaleDal.Update(upsale);
                XrmDataContext.SaveChanges();
            }
            return upsaleId;
        }

        private Guid GetIncidentID(Dialer.CreateIncidentContainer request)
        {
            Guid incidentID = Guid.Empty;
            DynamicEntity newincident = ConvertorCRMParams.GetNewDynamicEntity("incident");
            Utils.EntityUtils entityUtils = new Utils.EntityUtils(CrmService);
            string primaryExpertiseName = entityUtils.GetInstanceNameByGuid("new_primaryexpertise", request.PrimaryExpertise);
            DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string title = configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.DIRECT_NUMBER_INCIDENT_TITLE);
            title = string.Format(title, primaryExpertiseName);
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "title", title, typeof(StringProperty));
            bool isGoldenNumberAdvertiser =
                (request.DapazStatus.HasValue &&
                (request.DapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.FixGold
                || request.DapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.RarahGold
                || request.DapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.Fix
                || request.DapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.Rarah)
                );
            if (isGoldenNumberAdvertiser)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_isgoldennumbercall", ConvertorCRMParams.GetCrmBoolean(true), typeof(CrmBooleanProperty));
            }
            if (request.CustomerData.IsBlockedNumberContact == false && !isGoldenNumberAdvertiser)
            {
                Guid upsaleId = GetUpsaleId(request.CustomerData.Id, request.PrimaryExpertise);
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_upsaleid",
                    ConvertorCRMParams.GetCrmLookup("new_upsale", upsaleId), typeof(LookupProperty));
            }
            if (request.CallType == Dialer.DirectCallContactedIdentificationHandler.eCallType.PingPhone)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "casetypecode",
                    ConvertorCRMParams.GetCrmPicklist((int)DML.Xrm.incident.CaseTypeCode.PingPhone), typeof(PicklistProperty));
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_isfromlisting",
                    ConvertorCRMParams.GetCrmBoolean(false), typeof(CrmBooleanProperty));
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_pingphoneapientryid",
                    ConvertorCRMParams.GetCrmLookup("new_pingphoneapientry", APIs.SellersApi.PingPhone.CustomFieldManager.GetPingPhoneId(request.Custom)), typeof(LookupProperty));
            }
            else
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "casetypecode",
                    ConvertorCRMParams.GetCrmPicklist((int)DML.Xrm.incident.CaseTypeCode.DirectNumber), typeof(PicklistProperty));
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_isfromlisting",
                    ConvertorCRMParams.GetCrmBoolean(true), typeof(CrmBooleanProperty));
            }
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "customerid", ConvertorCRMParams.GetCrmCustomer("contact", request.CustomerData.Id), typeof(CustomerProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_durationcall", ConvertorCRMParams.GetCrmNumber(120, false), typeof(CrmNumberProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident,
                "new_telephone1",
                request.CustomerData.IsBlockedNumberContact ? configSettingsAccess.GetConfigurationSettingValue(DML.ConfigurationKeys.BLOCKED_NUMBER_NAME) : request.CustomerPhone,
                typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_requiredaccountno", ConvertorCRMParams.GetCrmNumber(1, false), typeof(CrmNumberProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_ordered_providers", ConvertorCRMParams.GetCrmNumber(0, false), typeof(CrmNumberProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_originid", ConvertorCRMParams.GetCrmLookup("new_origin", request.OriginId), typeof(LookupProperty));
            LogUtils.MyHandle.WriteToLog(8, "incident new_originid: {0}", request.OriginId);
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_dialerstatus", ConvertorCRMParams.GetCrmPicklist((int)DML.Xrm.incident.DialerStatus.REQUEST_COMPLETED), typeof(PicklistProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_primaryexpertiseid", ConvertorCRMParams.GetCrmLookup("new_primaryexpertise", request.PrimaryExpertise), typeof(LookupProperty));
            DataAccessLayer.Autonumber.AutonumberManager autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
            string nextNumber = autoNumber.GetNextNumber("incident");
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "ticketnumber", nextNumber, typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_country", request.Country, typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_dnqueryid", request.DnQueryId, typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_isdirectnumber",
                ConvertorCRMParams.GetCrmBoolean(true), typeof(CrmBooleanProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_israrahcase",
                ConvertorCRMParams.GetCrmBoolean(request.IsRarah), typeof(CrmBooleanProperty));
            if (request.IsCallCenterCase)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "statuscode",
                                ConvertorCRMParams.GetCrmStatus((int)DML.Xrm.incident.Status.NO_AVAILABLE_SUPPLIER), typeof(StatusProperty));
            }
            Origins.AffiliatesManager affiliatesManager = new Origins.AffiliatesManager(XrmDataContext);
            Guid payStatusId;
            Guid payReasonId;
            bool payAffiliate = affiliatesManager.CalculateAffiliatePayStatus(
                out payStatusId, out payReasonId, request.PrimaryExpertise, request.OriginId, request.CustomerData.Id, request.CustomerData.IsBlockedNumberContact, new DataAccessLayer.Incident(XrmDataContext), false, Guid.Empty, request.IsCallCenterCase);
            if (payAffiliate == false)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_showaffiliate",
                    ConvertorCRMParams.GetCrmBoolean(false), typeof(CrmBooleanProperty));
            }
            else
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_showaffiliate",
                    ConvertorCRMParams.GetCrmBoolean(true), typeof(CrmBooleanProperty));
            }
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_affiliatepaystatusid",
                ConvertorCRMParams.GetCrmLookup("new_affiliatepaystatus", payStatusId), typeof(LookupProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_affiliatepaystatusreasonid",
                ConvertorCRMParams.GetCrmLookup("new_affiliatepaystatusreason", payReasonId), typeof(LookupProperty));
            string sessionId;
            //perion dynamic did is used now for intext
            bool isPerionDynamic = FindPerionSessionId(request.VirtualNumber, out sessionId);
            if (isPerionDynamic)
            {
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_sessionid",
                    sessionId, typeof(StringProperty));
                //set type intext (7)
                ConvertorCRMParams.SetDynamicEntityPropertyValue(newincident, "new_type",
                    ConvertorCRMParams.GetCrmNumber(7, false), typeof(CrmNumberProperty));
            }
            incidentID = CrmService.Create(newincident);
            //if (isPerionDynamic)
            //{
            //    RetrieveDataFromMongoExposure(incidentID, sessionId, request.OriginId);
            //send sms ater request
            NoProblem.Core.BusinessLogic.SmsEngine.SmsAfterRequest sar = new SmsEngine.SmsAfterRequest(incidentID);
            sar.Start();
              
            return incidentID;
        }

        private void RetrieveDataFromMongoExposure(Guid incidentId, string perionSessionId, Guid originId)
        {
            Dialer.MongoExposures.RetrieveDataFromMongoExposureThread dataRetriever = new Dialer.MongoExposures.RetrieveDataFromMongoExposureThread(incidentId, perionSessionId, originId);
            dataRetriever.Start();
        }

        internal bool FindPerionSessionId(string virtualNumber, out string sessionId)
        {
            Dialer.PerionDDID.PerionDDIDManager manager = new Dialer.PerionDDID.PerionDDIDManager();
            sessionId = manager.GetSessionIdByNumber(virtualNumber);
            return sessionId != null;
        }

        /// <summary>
        /// This function creates a new incident account from the account and incident IDs
        /// </summary>
        /// <param name="incidentID">The conected incident's ID</param>
        /// <param name="accountID">The conected account's ID</param>
        /// <param name="hasAnswerd">a value indicating whether the account has answerd the phone call or not</param>
        /// <returns>The ID of the newly created incident account</returns>
        private Guid CreateDNIncidentAccount(Guid incidentID, Guid accountID, int reasonCode, string callId, Guid primaryExpertiseId, DML.SqlHelper.ContactMinData consumerData, int duration, decimal incidentPrice, DML.SqlHelper.ContactMinData customer, DML.Xrm.new_rarahdntempcase rarahCase, DataModel.Xrm.account.DapazStatus? dapazStatus, string virtualNumber, string recordingUrl)
        {
            DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DML.Xrm.new_incidentaccount incidentAccount = new NoProblem.Core.DataModel.Xrm.new_incidentaccount();
            incidentAccount.new_referred = true;
            incidentAccount.new_isjobdone = null;
            incidentAccount.new_isansweredbymachine = null;
            incidentAccount.new_incidentid = incidentID;
            incidentAccount.new_accountid = accountID;
            incidentAccount.new_isdirectnumber = true;
            incidentAccount.new_predefinedprice = incidentPrice;
            if (IsFreeCall(customer, accountID, dapazStatus))
            {
                incidentAccount.new_potentialincidentprice = 0;
            }
            else
            {
                incidentAccount.new_potentialincidentprice = incidentPrice;
            }
            bool isGoldenNumberAdvertiser =
                (dapazStatus.HasValue &&
                (dapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.FixGold
                || dapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.RarahGold
                || dapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.Fix
                || dapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.Rarah)
                );
            if (isGoldenNumberAdvertiser)
            {
                incidentAccount.new_isgoldennumbercall = true;
            }
            if (!string.IsNullOrEmpty(recordingUrl))
            {
                if (recordingUrl.Contains("twilio") && !recordingUrl.EndsWith(".mp3"))//the url is received from twilio without an extension and it gets a wav file, by adding .mp3 extension we get the mp3 file.
                {
                    recordingUrl = recordingUrl + ".mp3";
                }
                incidentAccount.new_recordfilelocation = recordingUrl;
                incidentAccount.new_recordduration = duration;
            }
            Guid instanceId = Guid.Empty;
            switch ((enumDirectNumberContactCode)reasonCode)
            {
                case enumDirectNumberContactCode.CONTACTED:
                    {
                        HandleDnContactedCase(incidentID, accountID, callId, primaryExpertiseId, duration, customer, rarahCase, incidentAccountDal, incidentAccount, ref instanceId, dapazStatus, virtualNumber);
                        break;
                    }
                case enumDirectNumberContactCode.SUPPLIER_UNREACHED:
                    {
                        HandleDnSupplierUnreachedCase(incidentID, accountID, callId, consumerData, incidentAccountDal, incidentAccount);
                        break;
                    }
                case enumDirectNumberContactCode.SUPPLIER_UNREACHED_AFTER_ALTERNATIVE:
                    {
                        HandleDnUnreachedAfterAlternativeCase(incidentID, accountID, callId, primaryExpertiseId, rarahCase, incidentAccountDal, incidentAccount, ref instanceId, incidentPrice);
                        break;
                    }
            }
            if (instanceId == Guid.Empty)
                instanceId = incidentAccount.new_incidentaccountid;
            return instanceId;
        }

        /// <summary>
        /// Free call is given to PPA accounts on the first two calendar months from their first payment on calls from blocked numbers (if enabled in the environment).
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="accountId"></param>
        /// <param name="dapazStatus"></param>
        /// <returns></returns>
        private bool IsFreeCall(DML.SqlHelper.ContactMinData customer, Guid accountId, DML.Xrm.account.DapazStatus? dapazStatus)
        {
            if (
                customer.IsBlockedNumberContact
                &&
                (!dapazStatus.HasValue || dapazStatus.Value == DML.Xrm.account.DapazStatus.PPA)
               )
            {
                DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(XrmDataContext);
                if (config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.FREE_BLOCKED_CALLS_FOR_TWO_MONTHS_ENABLED) == 1.ToString())
                {
                    DateTime? firstPaymentDate = GetFirstPayment(accountId);
                    if (firstPaymentDate.HasValue)
                    {
                        DateTime monthOfPayment = new DateTime(firstPaymentDate.Value.Year, firstPaymentDate.Value.Month, 1);
                        if (monthOfPayment.AddMonths(2) > DateTime.Now)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private DateTime? GetFirstPayment(Guid accountId)
        {
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@accountId", accountId);
            object scalar = dal.ExecuteScalar(getFirstPaymentQuery, parameters);
            DateTime? retVal = null;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (DateTime)scalar;
            }
            return retVal;
        }

        private const string getFirstPaymentQuery =
            @"
select min(createdon)
from New_supplierpricing with(nolock)
where
new_accountid = @accountId
and New_total > 0
";

        private void HandleDnUnreachedAfterAlternativeCase(Guid incidentID, Guid accountID, string callId, Guid primaryExpertiseId, DML.Xrm.new_rarahdntempcase rarahCase, DataAccessLayer.IncidentAccount incidentAccountDal, DML.Xrm.new_incidentaccount incidentAccount, ref Guid instanceId, decimal incidentPrice)
        {
            incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
            incidentAccountDal.Create(incidentAccount);
            XrmDataContext.SaveChanges();
            DML.Xrm.new_incidentaccount payingIncidentAccount = new NoProblem.Core.DataModel.Xrm.new_incidentaccount();
            payingIncidentAccount.new_accountid = rarahCase.new_payingid;
            payingIncidentAccount.new_incidentid = incidentID;
            payingIncidentAccount.new_isjobdone = null;
            //DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            //var accountExpertise = accExpDal.GetByAccountAndPrimaryExpertise(rarahCase.new_payingid.Value, primaryExpertiseId);
            decimal payingIncidentPrice = incidentPrice; //accountExpertise.new_incidentprice.Value;
            payingIncidentAccount.new_potentialincidentprice = payingIncidentPrice;
            payingIncidentAccount.new_predefinedprice = payingIncidentPrice;
            payingIncidentAccount.new_isdirectnumber = true;
            payingIncidentAccount.new_recordid = callId;
            payingIncidentAccount.new_isansweredbymachine = null;
            Notifications notifier = new Notifications(XrmDataContext);
            bool successfulSend = notifier.NotifySupplier(enumSupplierNotificationTypes.SUPPLIER_UNREACHED, rarahCase.new_payingid.Value, accountID.ToString(), "account", incidentID.ToString(), "incident");
            if (successfulSend)
            {
                payingIncidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS;
                payingIncidentAccount.new_tocharge = true;
            }
            else
            {
                payingIncidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
            }
            incidentAccountDal.Create(payingIncidentAccount);
            XrmDataContext.SaveChanges();
            instanceId = payingIncidentAccount.new_incidentaccountid;
        }

        private void HandleDnSupplierUnreachedCase(Guid incidentID, Guid accountID, string callId, DML.SqlHelper.ContactMinData consumerData, DataAccessLayer.IncidentAccount incidentAccountDal, DML.Xrm.new_incidentaccount incidentAccount)
        {
            incidentAccount.new_recordid = callId;
            Notifications notifier = new Notifications(XrmDataContext);
            if (consumerData.IsBlockedNumberContact == false)
            {
                notifier.NotifyCustomer(enumCustomerNotificationTypes.DIRECT_NUMBERS_UNRECHED,
                    consumerData.Id, accountID.ToString(), "account", accountID.ToString(), "account");
            }
            bool successfulSend = false;
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = accountRepositoryDal.Retrieve(accountID);
            if (supplier.new_dapazstatus.HasValue
                && (supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.FixGold
                    || supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.RarahGold
                    || supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.RarahQuote
                 ))
            {
                successfulSend = notifier.NotifySupplier(enumSupplierNotificationTypes.SUPPLIER_UNREACHED_GOLDEN_NUMBER_ADV, accountID, accountID.ToString(), "account", incidentID.ToString(), "incident");
            }
            else
            {
                successfulSend = notifier.NotifySupplier(enumSupplierNotificationTypes.SUPPLIER_UNREACHED, accountID, accountID.ToString(), "account", incidentID.ToString(), "incident");
            }
            if (successfulSend
                && (!supplier.new_dapazstatus.HasValue || supplier.new_dapazstatus.Value == (int)DML.Xrm.account.DapazStatus.PPA))
            {
                incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS;
                incidentAccount.new_tocharge = true;
            }
            else
            {
                incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
            }
            incidentAccountDal.Create(incidentAccount);
            XrmDataContext.SaveChanges();
        }

        private void HandleDnContactedCase(Guid incidentID, Guid accountID, string callId, Guid primaryExpertiseId, int duration, DML.SqlHelper.ContactMinData customer, DML.Xrm.new_rarahdntempcase rarahCase, DataAccessLayer.IncidentAccount incidentAccountDal, DML.Xrm.new_incidentaccount incidentAccount, ref Guid instanceId, DataModel.Xrm.account.DapazStatus? dapazStatus, string virtualNumber)
        {
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(accountID);
            DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DML.Xrm.incident incident = incidentDal.Retrieve(incidentID);
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            DML.Xrm.new_accountexpertise accExp = accExpDal.GetByAccountAndPrimaryExpertise(accountID, incident.new_primaryexpertiseid.Value);
            DML.Xrm.new_directnumber dirNumber = null;
            if (accExp != null)
            {
                DataAccessLayer.DirectNumber dirNumDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                dirNumber = dirNumDal.GetDirectNumberByAccountExpertise(accExp.new_accountexpertiseid);
            }
            string tempPhone = string.Empty;
            if (dirNumber == null)
            {
                tempPhone = supplier.telephone1;
            }
            else
            {
                tempPhone = dirNumber.new_number;
            }
            supplier.new_tempphone = tempPhone;
            accountRepositoryDal.Update(supplier);
            XrmDataContext.SaveChanges();
            //Telelistas specific feature start
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string minLengthStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.MINIMAL_CALL_LENGTH_TO_CHARGE);
            int minLength;
            bool isLenghtOK = true;
            if (int.TryParse(minLengthStr, out minLength))
            {
                if (minLength > 0 && duration < minLength)
                {
                    isLenghtOK = false;
                }
            }
            //Telelistas specific feature end
            if (isLenghtOK)//if not telelistas (configuration set) is goes always in here.
            {
                bool charge = true;
                bool isHotCallLong = false;
                bool isHotCallLast = false;
                if (dapazStatus.HasValue
                    && dapazStatus.Value == NoProblem.Core.DataModel.Xrm.account.DapazStatus.RarahQuote)
                {
                    if (IsRarahQuoteCallLongEnough(duration, configDal))
                    {
                        supplier.new_rarahquoteleft = supplier.new_rarahquoteleft.HasValue ? supplier.new_rarahquoteleft.Value - 1 : 0;
                        if (supplier.new_rarahquoteleft == 0)
                        {
                            isHotCallLast = true;
                            supplier.new_rarahquoteenddate = DateTime.Now;
                            Dapaz.PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(supplier.accountid);
                            inserter.InsertToStats();
                        }
                        accountRepositoryDal.Update(supplier);
                        XrmDataContext.SaveChanges();
                        string secondsStr = configDal.GetConfigurationSettingValue(DML.ConfigurationKeys.HOT_CALL_SECONDS);
                        int secondsForHotCall;
                        if (!int.TryParse(secondsStr, out secondsForHotCall))
                        {
                            secondsForHotCall = 90;
                        }
                        isHotCallLong = secondsForHotCall <= duration;
                    }
                    else
                    {
                        charge = false;
                    }
                }
                if (charge)
                {
                    incidentAccount.new_recordid = callId;
                    Notifications notificationManager = new Notifications(XrmDataContext);
                    notificationManager.NotifyCustomer(DML.enumCustomerNotificationTypes.SUPPLIER_INFO, customer.Id, accountID.ToString(), "account", accountID.ToString(), "account");
                    incidentAccount.new_tocharge = true;
                    incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS;
                    incidentAccount.new_pingid = virtualNumber;
                    incidentAccountDal.Create(incidentAccount);
                    XrmDataContext.SaveChanges();
                    if (isHotCallLast || isHotCallLong)
                    {
                        Dapaz.HotCallNotifier notifier = new Dapaz.HotCallNotifier(supplier.new_bizid, virtualNumber, callId, incidentAccount.new_incidentaccountid.ToString(), isHotCallLong, isHotCallLast, supplier.new_rarahquotestartdate.HasValue ? supplier.new_rarahquotestartdate.Value : DateTime.MinValue);
                        notifier.NotifyHotCallEvent();
                    }
                }
                else
                {
                    HandleDirectNumberCallToShort(callId, incidentAccountDal, incidentAccount);
                }
                //}
            }
            else//length not ok
            {
                HandleDirectNumberCallToShort(callId, incidentAccountDal, incidentAccount);
            }
        }

        private bool IsRarahQuoteCallLongEnough(int duration, DataAccessLayer.ConfigurationSettings config)
        {
            string successCallSecondsStr = config.GetConfigurationSettingValue(DML.ConfigurationKeys.RARAH_QUOTE_SUCCESS_CALL_SECONDS);
            int successCallSeconds;
            if (!int.TryParse(successCallSecondsStr, out successCallSeconds))
            {
                successCallSeconds = 30;
            }
            return (duration >= successCallSeconds);
        }

        private void HandleDirectNumberCallToShort(string callId, DataAccessLayer.IncidentAccount incidentAccountDal, DML.Xrm.new_incidentaccount incidentAccount)
        {
            incidentAccount.new_recordid = callId;
            incidentAccount.new_tocharge = false;
            incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
            incidentAccountDal.Create(incidentAccount);
            XrmDataContext.SaveChanges();
        }

        /// <summary>
        /// This function sets the incident account's statuscode to Close
        /// </summary>
        /// <param name="incidentAccountID">The ID of the incident account to close</param>
        private void CloseRequest(Guid incidentAccountID, string callId)
        {
            try
            {
                DML.Xrm.new_incidentaccount incidentAccount = new NoProblem.Core.DataModel.Xrm.new_incidentaccount(XrmDataContext);
                incidentAccount.new_incidentaccountid = incidentAccountID;
                incidentAccount.new_recordid = callId;
                incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS;
                DataAccessLayer.IncidentAccount inciAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                inciAccDal.Update(incidentAccount);
                XrmDataContext.SaveChanges();
            }
            catch (SoapException ex)
            {
                ErrorHandler.ReportError(EntityName.account, "", "", "Closing the incident account, failed with reason: " + ex.Detail.InnerText, "Inc2Dialer", "CloseRequest");
            }
            catch (Exception ex)
            {
                ErrorHandler.ReportError(EntityName.account, "", "", "Creating the incident account, failed with reason: " + ex.Message, "Inc2Dialer", "CloseRequest");
            }
        }

        private void GiveAvailableMoneyBackToErrorSuppliers(Guid incidentId)
        {
            LogUtils.MyHandle.WriteToLog(9, "GiveAvailableMoneyBackToErrorSuppliers started.");
            DataAccessLayer.Incident incidentDAL = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
            DML.Xrm.incident inci = incidentDAL.Retrieve(incidentId);
            int version = inci.new_inc2version.HasValue ? inci.new_inc2version.Value : 1;
            DataAccessLayer.IncidentAccount inciAccountDAL = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            IEnumerable<DML.Xrm.new_incidentaccount> inciAccounts = inciAccountDAL.GetIncidentAccountsByIncidentId(incidentId);
            Balance.BalanceManager balanceManager = new NoProblem.Core.BusinessLogic.Balance.BalanceManager(XrmDataContext);
            foreach (DML.Xrm.new_incidentaccount inciAcc in inciAccounts)
            {
                if (inciAcc.statuscode == (int)DML.Xrm.new_incidentaccount.Status.ERROR && inciAcc.new_version.HasValue && inciAcc.new_version.Value == version)
                {
                    balanceManager.AddToAvailableBalance(inciAcc.new_accountid.Value, inciAcc.new_potentialincidentprice.Value);
                }
            }
            LogUtils.MyHandle.WriteToLog(9, "GiveAvailableMoneyBackToErrorSuppliers ended.");
        }

        private bool UpdateIncidentAccountInConsumerContactedSucc(string callId, Guid incidentId, Guid accountId, bool isLengthOK)
        {
            bool charged = false;
            try
            {
                DataAccessLayer.IncidentAccount inciAccAccess = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                DML.Xrm.new_incidentaccount incidentAccount = inciAccAccess.GetIncidentAccountByIncidentAndAccount(incidentId, accountId);
                if (isLengthOK)
                {
                    incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.CLOSE;
                    incidentAccount.new_tocharge = true;
                    incidentAccount.new_recordid = callId;
                    inciAccAccess.Update(incidentAccount);
                    DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
                    DML.Xrm.new_upsale upsale = upsaleDal.GetUpsaleByIncidentId(incidentId);
                    if (upsale != null)
                    {
                        upsale.new_numofconnectedadvertisers =
                            (upsale.new_numofconnectedadvertisers.HasValue ? upsale.new_numofconnectedadvertisers.Value + 1 : 1);
                        upsaleDal.Update(upsale);
                    }
                    DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                    var incident = incidentDal.Retrieve(incidentId);
                    incident.new_ordered_providers += 1;
                    incidentDal.Update(incident);
                    XrmDataContext.SaveChanges();
                    bool refunded = CheckIncidentTimePass(incidentAccount, XrmDataContext);
                    DailyBudgetManager budgetManager = new DailyBudgetManager(XrmDataContext);
                    budgetManager.CheckDailyBudgetIsReached(accountId, false);
                    charged = !refunded;
                }
                else
                {
                    incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
                    incidentAccount.new_tocharge = false;
                    incidentAccount.new_recordid = callId;
                    inciAccAccess.Update(incidentAccount);
                    XrmDataContext.SaveChanges();
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "UpdateIncidentAccountInConsumerContactedSucc failed with exception.");
            }
            return charged;
        }

        private bool CheckIncidentTimePass(NoProblem.Core.DataModel.Xrm.new_incidentaccount incidentAccount, DML.Xrm.DataContext xrmDataContext)
        {
            bool refunded = false;
            DataAccessLayer.ConfigurationSettings configSettingsAccess = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(xrmDataContext);
            string configVal = configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.NEW_INCIDENT_TIME_PASS);
            int passedDays = int.Parse(configVal);
            if (passedDays > 0)
            {
                //int publisherTimeZoneCode = int.Parse(configSettingsAccess.GetConfigurationSettingValue(ConfigurationKeys.PUBLISHER_TIME_ZONE));
                DateTime localDate = FixDateToLocal(DateTime.Now).Date; //ConvertUtils.GetLocalFromUtc(xrmDataContext, DateTime.Now, publisherTimeZoneCode).Date;
                DateTime midNightLocal = FixDateToUtcFromLocal(localDate); //ConvertUtils.GetUtcFromLocal(xrmDataContext, localDate, publisherTimeZoneCode);
                DateTime dateReady = midNightLocal.AddDays((passedDays - 1) * -1);
                DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(xrmDataContext);
                var incident = incidentDal.Retrieve(incidentAccount.new_incidentid.Value);
                DataAccessLayer.Contact contactDal = new NoProblem.Core.DataAccessLayer.Contact(xrmDataContext);
                if (contactDal.GetBlockedNumberContactId() != incident.customerid.Value)
                {
                    bool wasCharged = incidentDal.WasChargedInTimePass(incident.new_primaryexpertiseid.Value, incident.customerid.Value, incidentAccount.new_accountid.Value, dateReady, incident.incidentid);
                    if (wasCharged)
                    {
                        Refunds.RefundsManager refunder = new NoProblem.Core.BusinessLogic.Refunds.RefundsManager(xrmDataContext);
                        refunder.RefundSupplier(incidentAccount, NoProblem.Core.DataModel.Xrm.new_balancerow.Action.Interval);
                        refunded = true;
                    }
                }
            }
            return refunded;
        }

        private void SendConsumerContactedSuccNotifications(string incidentIdStr, string accountIdStr)
        {
            try
            {
                Guid accountId = new Guid(accountIdStr);
                Guid incidentId = new Guid(incidentIdStr);
                Notifications notificationsManager = new Notifications(XrmDataContext);
                notificationsManager.NotifySupplier(DML.enumSupplierNotificationTypes.INCIDENT_INFO, accountId, incidentIdStr, "incident", incidentIdStr, "incident");

                //string getCustomerIdQuery = "SELECT customerid FROM incident with (nolock) WHERE incidentid = '{0}'";
                //object customerId = Effect.Crm.DB.DataBaseUtils.GetValueFromSqlString(getCustomerIdQuery, incidentId);
                string getCustomerIdQuery = "SELECT customerid FROM incident with (nolock) WHERE incidentid = @id";
                DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
                parameters.Add("@id", incidentId);
                DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
                object customerId = dal.ExecuteScalar(getCustomerIdQuery);

                if (customerId != null)
                {
                    DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                    DML.Xrm.account supplier = accountRepositoryDal.Retrieve(accountId);
                    DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                    DML.Xrm.incident incident = incidentDal.Retrieve(incidentId);
                    DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                    DML.Xrm.new_accountexpertise accExp = accExpDal.GetByAccountAndPrimaryExpertise(accountId, incident.new_primaryexpertiseid.Value);
                    DML.Xrm.new_directnumber dirNumber = null;
                    if (accExp != null)
                    {
                        DataAccessLayer.DirectNumber dirNumDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                        dirNumber = dirNumDal.GetDirectNumberByAccountExpertise(accExp.new_accountexpertiseid);
                    }
                    string tempPhone = string.Empty;
                    if (dirNumber == null)
                    {
                        tempPhone = supplier.telephone1;
                    }
                    else
                    {
                        tempPhone = dirNumber.new_number;
                    }
                    supplier.new_tempphone = tempPhone;
                    accountRepositoryDal.Update(supplier);
                    XrmDataContext.SaveChanges();
                    notificationsManager.NotifyCustomer(DML.enumCustomerNotificationTypes.SUPPLIER_INFO, (Guid)customerId, accountIdStr, "account", accountIdStr, "account");
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "SendConsumerContactedSuccNotifications failed with exception.");
            }
        }

        private void NotifyUnreachedSuppliers(Guid incidentID)
        {
            LogUtils.MyHandle.WriteToLog(9, "NotifyUnreachedSuppliers started");
            string query = @"SELECT account.accountid
			,incident.customerid,incident.new_requiredaccountno,new_incidentaccount.new_incidentaccountid 
		 FROM incident with(nolock) 
		 INNER JOIN new_incidentaccount with(nolock) ON new_incidentid = incidentid AND (new_incidentaccount.statuscode = 1 OR new_incidentaccount.statuscode = 14 OR new_incidentaccount.statuscode = 17 OR new_incidentaccount.statuscode = 18)
		 INNER JOIN account with(nolock) ON account.accountid = new_accountid
		 WHERE incidentid = @id order by new_incidentaccount.createdon";

            using (SqlConnection connection = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@id", incidentID);
                    using (SqlDataReader accounts = command.ExecuteReader())
                    {
                        DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                        int orderedProvidersCount = incidentDal.GetIncidentAccountSum(incidentID,
                          DML.Xrm.new_incidentaccount.Status.CLOSE,
                          DML.Xrm.new_incidentaccount.Status.CLOSED_BY_DIRECT_NUMBERS,
                          DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS);

                        int requiredaccountno = 0;

                        if (accounts.Read())
                        {
                            requiredaccountno = int.Parse(accounts["new_requiredaccountno"].ToString());

                            if (orderedProvidersCount < requiredaccountno)
                            {
                                DataAccessLayer.Upsale upsaleDal = new NoProblem.Core.DataAccessLayer.Upsale(XrmDataContext);
                                var upsale = upsaleDal.GetUpsaleByIncidentId(incidentID);
                                var incident = incidentDal.Retrieve(incidentID);
                                bool upsaleChanged = false;
                                do
                                {
                                    try
                                    {
                                        object oAccountId = accounts["accountid"];
                                        if (oAccountId != null)
                                        {
                                            Guid accountid = new Guid(oAccountId.ToString());
                                            bool supplierNotified = false;
                                            bool customerNotified = false;
                                            Notifications notifier = new Notifications(XrmDataContext);
                                            supplierNotified = notifier.NotifySupplier(enumSupplierNotificationTypes.SUPPLIER_UNREACHED, accountid, incidentID.ToString(), "incident", incidentID.ToString(), "incident");
                                            object customerid = accounts["customerid"];
                                            if (customerid != null && supplierNotified)
                                            {
                                                customerNotified = notifier.NotifyCustomer(enumCustomerNotificationTypes.SUPPLIER_UNREACHED, new Guid(customerid.ToString()), accountid.ToString(), "account", accountid.ToString(), "account");
                                            }
                                            object incidentaccount = accounts["new_incidentaccountid"];
                                            if (incidentaccount != null && supplierNotified)
                                            {
                                                Guid incidentaccountid = new Guid(incidentaccount.ToString());
                                                DataAccessLayer.IncidentAccount incidentAccountAccess = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                                                DML.Xrm.new_incidentaccount smsaccount = incidentAccountAccess.Retrieve(incidentaccountid);
                                                smsaccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.INVITED_BY_SMS;
                                                smsaccount.new_tocharge = true;
                                                incidentAccountAccess.Update(smsaccount);
                                                if (upsale != null)
                                                {
                                                    upsale.new_numofconnectedadvertisers =
                                                        upsale.new_numofconnectedadvertisers.HasValue ? upsale.new_numofconnectedadvertisers.Value + 1 : 1;
                                                    incident.new_ordered_providers += 1;
                                                    upsaleChanged = true;
                                                }
                                                XrmDataContext.SaveChanges();
                                                CheckIncidentTimePass(smsaccount, XrmDataContext);
                                                DailyBudgetManager budgetManager = new DailyBudgetManager(XrmDataContext);
                                                budgetManager.CheckDailyBudgetIsReached(smsaccount.new_accountid.Value, false);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LogUtils.MyHandle.HandleException(ex, "NotifyUnreachedSuppliers failed with exception");
                                    }
                                }
                                while (accounts.Read() && orderedProvidersCount < requiredaccountno);
                                if (upsaleChanged)
                                {
                                    upsaleDal.Update(upsale);
                                    incidentDal.Update(incident);
                                    XrmDataContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            LogUtils.MyHandle.WriteToLog(9, "NotifyUnreachedSuppliers ended");
        }

        #endregion
    }
}
