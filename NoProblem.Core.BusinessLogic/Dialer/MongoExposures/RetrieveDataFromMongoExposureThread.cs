﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using Effect.Crm.Logs;
using System.Configuration;

namespace NoProblem.Core.BusinessLogic.Dialer.MongoExposures
{
    internal class RetrieveDataFromMongoExposureThread : Runnable
    {
        private string sessionId;
        private Guid originId;
        private Guid incidentId;
        private const string EXPOSURES_COLLECTION = "Exposure";
        private const string EXPOSURES_COLLECTION_QA = "ExposureQA";
        private const string SITEID = "SiteId";

        public RetrieveDataFromMongoExposureThread(Guid incidentId, string perionSessionId, Guid originId)
        {
            sessionId = perionSessionId;
            this.originId = originId;
            this.incidentId = incidentId;
        }

        protected override void Run()
        {
            var collection = DataAccessLayer.MongoDBHelper.Instance.GetCollection(GetCollectionName());
            var query = collection.AsQueryable<ExposureModel>()
                .Where(x => x.OriginId == originId && x.RSI == sessionId)
                .OrderByDescending(x => x.Date);
            ExposureModel exposure = query.FirstOrDefault();
            if (exposure != null)
            {
                DataAccessLayer.Incident dal = new DataAccessLayer.Incident(XrmDataContext);
                DataModel.Xrm.incident incident = new DataModel.Xrm.incident(XrmDataContext);
                incident.incidentid = incidentId;
                incident.new_operatingsystem = exposure.OS;
                incident.new_country = exposure.Country;
                incident.new_domain = exposure.ClientDomain;
                incident.new_keyword = exposure.Keyword;
                Origins.AffiliatesManager affManager = new Origins.AffiliatesManager(XrmDataContext);
                Guid toolbarIdId = affManager.GetCreateToolbarId(exposure.ToolbarId, originId);
                if (toolbarIdId != Guid.Empty)
                    incident.new_toolbaridid = toolbarIdId;
                dal.Update(incident);
                XrmDataContext.SaveChanges();
            }
            else
            {
                LogUtils.MyHandle.WriteToLog("XSaver's (Perion) RSI not found in RetrieveDataFromMongoExposureThread. RSI = {0}. incidentId = {1}. originId = {2}.", sessionId, incidentId, originId);
            }
        }

        private string GetCollectionName()
        {
            if (ConfigurationManager.AppSettings.Get(SITEID) == DataModel.Constants.SiteIds.QA)
            {
                return EXPOSURES_COLLECTION_QA;
            }
            return EXPOSURES_COLLECTION;
        }
    }
}
