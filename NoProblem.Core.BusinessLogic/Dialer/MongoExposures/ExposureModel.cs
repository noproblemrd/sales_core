﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace NoProblem.Core.BusinessLogic.Dialer.MongoExposures
{
    [BsonIgnoreExtraElements]
    internal class ExposureModel
    {
        public Guid OriginId { get; set; }
        public string RSI { get; set; }
        public string Country { get; set; }
        public string OS { get; set; }
        public string ClientDomain { get; set; }
        public string Keyword { get; set; }
        public string ToolbarId { get; set; }
        public DateTime Date { get; set; }
        public MongoDB.Bson.ObjectId _id { get; set; }
    }
}
