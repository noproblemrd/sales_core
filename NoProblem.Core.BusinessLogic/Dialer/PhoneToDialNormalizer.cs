﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    public static class PhoneToDialNormalizer
    {
        private static DataAccessLayer.ConfigurationSettings config = new DataAccessLayer.ConfigurationSettings(null);

        /// <summary>
        /// Normlize phone according to trunk in FONEAPI_TRUNK config and given country. If country is null or empty, DEFAULT_COUNTRY will be used. You should try to pass the country of the supplier (account.new_country) if this is a supplier's phone.
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="country"></param>
        /// <returns></returns>
        public static string NormalizePhone(string phone, string country)
        {
            return NormalizePhone(phone, country, null);
        }

        /// <summary>
        /// If you don't know about trunks, please use overload method without trunk parameter. Normailze phone according to given country and trunk. If country is null or empty, DEFAULT_COUNTRY will be used. If trunk is null or empty, FONEAPI_TRUNK will be used.
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="country"></param>
        /// <param name="trunk"></param>
        /// <returns></returns>
        public static string NormalizePhone(string phone, string country, string trunk)
        {
            if (String.IsNullOrEmpty(phone))
                return phone;
            var data = DialerCountryTrunkConfiguration.GetConfiguration(country, trunk);
            if (phone.StartsWith(data.RemovePrefix))
                phone = phone.Substring(data.RemovePrefix.Length);
            return data.AddPrefix + phone;
        }
    }
}
