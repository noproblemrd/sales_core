﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    public class PhoneManager : XrmUserBase
    {
        #region Ctor
        public PhoneManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        public void ServiceRequestCompleted(string serviceRequestUniqueId, int reasonCode, string cdrxml)
        {
            //if (Utils.GeneralUtils.IsGUID(serviceRequestUniqueId))
            //{
                NoProblem.Core.BusinessLogic.Phones handler = new NoProblem.Core.BusinessLogic.Phones(XrmDataContext, null);
                handler.UpdateIncidentStatusCode(serviceRequestUniqueId, reasonCode, cdrxml);
            //}
            //else
            //{
            //    NonPpcPhoneManager manager = new NonPpcPhoneManager(XrmDataContext);
            //    manager.ServiceRequestCompleted(serviceRequestUniqueId, reasonCode, cdrxml);
            //}
        }

        public string CallStatus(string serviceRequestUniqueId, string providerUniqueId, int statusCode)
        {
            //if (Utils.GeneralUtils.IsGUID(serviceRequestUniqueId))
            //{
                NoProblem.Core.BusinessLogic.Dialer.CallManager callManager = new NoProblem.Core.BusinessLogic.Dialer.CallManager(null, null);
                return callManager.CallStatus(new Guid(serviceRequestUniqueId), new Guid(providerUniqueId), statusCode/*, session_id, siperrorcode, sipmessage*/);
            //}
            //else
            //{
            //    BusinessLogic.Dialer.NonPpcPhoneManager manager = new NoProblem.Core.BusinessLogic.Dialer.NonPpcPhoneManager(null);
            //    return manager.CallStatus(serviceRequestUniqueId, providerUniqueId, statusCode);
            //}
        }

        public bool ConsumerContactedSucc(string serviceRequestUniqueId, string providerUniqueId, string callId, string timeStamp, string duration)
        {
            //if (Utils.GeneralUtils.IsGUID(serviceRequestUniqueId))
            //{
                NoProblem.Core.BusinessLogic.Phones handler = new NoProblem.Core.BusinessLogic.Phones(null, null);
                return handler.ConsumerContactedSucc(serviceRequestUniqueId, providerUniqueId, callId, duration);
            //}
            //else
            //{
                //BusinessLogic.Dialer.NonPpcPhoneManager manager = new NoProblem.Core.BusinessLogic.Dialer.NonPpcPhoneManager(null);
                //manager.ConsumerContactedSucc(serviceRequestUniqueId, providerUniqueId, callId);
            //}
        }
    }
}
