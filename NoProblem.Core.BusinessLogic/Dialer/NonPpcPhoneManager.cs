﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel;
using DML = NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.Jona;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Asterisk;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    public class NonPpcPhoneManager : XrmUserBase
    {
        #region Ctor
        public NonPpcPhoneManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        internal void HandleCallStatus(DataModel.Asterisk.C2cStatusContainer statusContainer)
        {
            DataAccessLayer.NonPpcRequest dal = new NoProblem.Core.DataAccessLayer.NonPpcRequest(XrmDataContext);
            var call = dal.GetNonPpcRequestByRecordId(statusContainer.Session);
            if (call == null)
            {
                if (statusContainer.Status != "ack")
                {
                    throw new Exception("Call is null and status is not ack in HandleCallStatus");
                }
                return;
            }
            enumCallStatus currentStatus = (enumCallStatus)Enum.Parse(typeof(enumCallStatus), call.new_callstatus);
            enumCallStatus nextStatus = GetNextStatus(currentStatus, statusContainer);
            LogUtils.MyHandle.WriteToLog(8, "-- {0} => {1} --", currentStatus, nextStatus);
            if (currentStatus != nextStatus)
            {
                call.new_callstatus = nextStatus.ToString();
                dal.Update(call);
                XrmDataContext.SaveChanges();
                if (nextStatus == enumCallStatus.CALL_COMPLETED)
                {
                    call.new_callend = DateTime.Now;
                    if (call.new_consumercallstart.HasValue)
                    {
                        TimeSpan duration = call.new_callend.Value - call.new_consumercallstart.Value;
                        call.new_consumercallduration = (int)duration.TotalSeconds;
                    }
                    call.statuscode = (int)DML.Xrm.new_nonppcrequest.Status.Close;
                    dal.Update(call);
                    XrmDataContext.SaveChanges();
                }
                else
                {
                    if (nextStatus == enumCallStatus.CUSTOMER_BUSY)
                    {
                        call.statuscode = (int)DataModel.Xrm.new_nonppcrequest.Status.ConsumerDidntAnswer;
                        call.new_callend = DateTime.Now;
                        SendNoConnectionMessagesAsync(call);
                    }
                    else if (nextStatus == enumCallStatus.ADVERTISER_BUSY
                        || nextStatus == enumCallStatus.ADVERTISER_NOT_CONFIRMED)
                    {
                        call.statuscode = (int)DataModel.Xrm.new_nonppcrequest.Status.Lost;
                        call.new_callend = DateTime.Now;
                        SendNoConnectionMessagesAsync(call);
                    }
                    else if (nextStatus == enumCallStatus.CALL_INIT_TIMEOUT
                        || nextStatus == enumCallStatus.CALL_TERMINATED
                        || nextStatus == enumCallStatus.ERROR
                        || nextStatus == enumCallStatus.UNKNOWN)
                    {
                        if (call.statuscode == (int)DataModel.Xrm.new_nonppcrequest.Status.CallInProgress)
                        {
                            call.new_callend = DateTime.Now;
                            if (call.new_consumercallstart.HasValue)
                            {
                                TimeSpan duration = call.new_callend.Value - call.new_consumercallstart.Value;
                                call.new_consumercallduration = (int)duration.TotalSeconds;
                            }
                        }
                        else if (call.statuscode != (int)DataModel.Xrm.new_nonppcrequest.Status.Close)
                        {
                            call.new_callend = DateTime.Now;
                            call.statuscode = (int)DML.Xrm.new_nonppcrequest.Status.Error;
                        }
                        SendNoConnectionMessagesAsync(call);
                    }
                    else if (nextStatus == enumCallStatus.CALL_IN_PROGRESS)
                    {
                        //save start of consumer call.
                        call.new_consumercallstart = DateTime.Now;
                        call.statuscode = (int)DataModel.Xrm.new_nonppcrequest.Status.CallInProgress;
                    }
                    else if (nextStatus == enumCallStatus.ADVERTISER_ANSWERED)
                    {
                        //save start of call
                        call.new_advertisercallstart = DateTime.Now;
                        call.statuscode = (int)DataModel.Xrm.new_nonppcrequest.Status.AdvAnswered;
                    }
                    dal.Update(call);
                    XrmDataContext.SaveChanges();
                }
            }
        }

        private void SendNoConnectionMessagesAsync(NoProblem.Core.DataModel.Xrm.new_nonppcrequest call)
        {
            NonPpcPhoneManager manager = new NonPpcPhoneManager(null);
            SendNoConnectionMessagesDelegate del = new SendNoConnectionMessagesDelegate(manager.SendNoConnectionMessages);
            del.BeginInvoke(call.new_advertiserphone1, call.new_consumerphone, null, del);
        }

        delegate void SendNoConnectionMessagesDelegate(string advPhone, string custPhone);

        private void SendNoConnectionMessages(string advPhone, string custPhone)
        {
            Notifications notifier = new Notifications(XrmDataContext);
            string subject, body;
            if (custPhone.StartsWith("05"))
            {
                notifier.InstatiateTemplate(DataModel.TemplateNames.DAPAZ_C2C_CUSTOMER, out subject, out body);
                body = string.Format(body, advPhone);
                notifier.SendSMSTemplate(body, custPhone, GlobalConfigurations.DefaultCountry);
            }
            if (advPhone.StartsWith("05"))
            {
                notifier.InstatiateTemplate(DataModel.TemplateNames.DAPAZ_C2C_ADVERTISER, out subject, out body);
                body = string.Format(body, custPhone);
                notifier.SendSMSTemplate(body, advPhone, GlobalConfigurations.DefaultCountry);
            }
        }

        private void SendNoConnectionMessagesAsyncCallback(IAsyncResult result)
        {
            try
            {
                ((SendNoConnectionMessagesDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SendNoConnectionMessagesAsyncCallback");
            }
        }

        private enumCallStatus GetNextStatus(DataModel.Jona.enumCallStatus currentStatus, DataModel.Asterisk.C2cStatusContainer statusContainer)
        {
            enumCallStatus retStatus = enumCallStatus.UNKNOWN;
            string side = statusContainer.Side;
            string status = statusContainer.Status;
            if (status == C2cStatusCode.FAILED)
                currentStatus = enumCallStatus.ERROR;
            string action = side + status;
            switch (currentStatus)
            {
                case DataModel.Jona.enumCallStatus.QUEUED:
                    if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ACK)
                        retStatus = enumCallStatus.QUEUED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.BUSY)
                        retStatus = enumCallStatus.ADVERTISER_BUSY;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.NO_ANSWER)
                        retStatus = enumCallStatus.ADVERTISER_BUSY;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ANSWER)
                        retStatus = enumCallStatus.ADVERTISER_ANSWERED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.IN_PROGRESS)
                        retStatus = enumCallStatus.ADVERTISER_ANSWERED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.FAILED)
                        retStatus = enumCallStatus.ADVERTISER_NOT_CONFIRMED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.CANCEL)
                        retStatus = enumCallStatus.ADVERTISER_NOT_CONFIRMED;
                    else
                        retStatus = enumCallStatus.QUEUED;
                    break;
                case DataModel.Jona.enumCallStatus.ADVERTISER_ANSWERED:
                    if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.DONE || action == C2cStatusContainer.CON_SIDE + C2cStatusCode.IN_PROGRESS)
                        retStatus = enumCallStatus.ADVERTISER_NOT_CONFIRMED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ANSWER)
                        retStatus = enumCallStatus.ADVERTISER_ANSWERED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.APPROVED)
                        retStatus = enumCallStatus.ADVERTISER_CONFIRMED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ACK) //Found to occur when advertiser and consumer phone are the same 
                        retStatus = enumCallStatus.ADVERTISER_BUSY;
                    else if (action == C2cStatusContainer.ADV_SIDE + enumCallStatus.REJECTED)
                        retStatus = enumCallStatus.REJECTED;
                    break;
                case DataModel.Jona.enumCallStatus.ADVERTISER_CONFIRMED:
                    if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.IN_PROGRESS)
                        retStatus = enumCallStatus.CALL_IN_PROGRESS;
                    else if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.FAILED)
                        retStatus = enumCallStatus.CUSTOMER_BUSY; //Should be ERROR. But not supported in this version by UI.
                    else if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.BUSY)
                        retStatus = enumCallStatus.CUSTOMER_BUSY;
                    else if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.NO_ANSWER)
                        retStatus = enumCallStatus.CUSTOMER_BUSY;
                    else if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.NO_ANSWER)
                        retStatus = enumCallStatus.CUSTOMER_BUSY;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ANSWER)
                        retStatus = enumCallStatus.ADVERTISER_CONFIRMED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.DONE) //Probably because recevied before busy message
                        retStatus = enumCallStatus.CUSTOMER_BUSY; //retStatus = enumCallStatus.ADVERTISER_CONFIRMED;
                    else if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.CANCEL) //Consumer disconnected immediatly.
                        retStatus = enumCallStatus.CUSTOMER_BUSY;
                    break;
                case enumCallStatus.CALL_IN_PROGRESS:
                    if (action == C2cStatusContainer.CON_SIDE + C2cStatusCode.DONE || action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.DONE)
                        retStatus = enumCallStatus.CALL_COMPLETED;
                    else if (action == C2cStatusContainer.ADV_SIDE + C2cStatusCode.ANSWER)
                        retStatus = enumCallStatus.CALL_IN_PROGRESS;
                    break;
                case enumCallStatus.REJECTED:
                case enumCallStatus.CUSTOMER_BUSY:
                case enumCallStatus.ADVERTISER_BUSY:
                case enumCallStatus.CALL_COMPLETED:
                case enumCallStatus.ADVERTISER_NOT_CONFIRMED:
                case enumCallStatus.ERROR:
                    retStatus = currentStatus;
                    break;
            }

            if (retStatus == enumCallStatus.UNKNOWN)
            {
                LogUtils.MyHandle.WriteToLog(
                    "Unkwown status recevied from Linesip in NonPpcPhoneManager.GetNextStatus for session {0} current status: {1}, status from Atelis: {2}",
                    statusContainer.Session, currentStatus, action);
            }
            if (retStatus == enumCallStatus.ERROR)
            {
                LogUtils.MyHandle.WriteToLog(
                    "Received FAILED status from Linesip in NonPpcPhoneManager.GetNextStatus for session {0}. status received: {1}",
                    statusContainer.Session, action);
            }
            return retStatus;
        }
    }
}
