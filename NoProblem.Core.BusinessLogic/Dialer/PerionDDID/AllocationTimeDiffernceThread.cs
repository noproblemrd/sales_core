﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Dialer.PerionDDID
{
    internal class AllocationTimeDiffernceThread : Runnable
    {
        private int priorAllocationMinute;
        private const int MINUTES = 5;

        public AllocationTimeDiffernceThread(int minute)
        {
            this.priorAllocationMinute = minute;
        }

        protected override void Run()
        {
           // TimeSpan span = DateTime.Now - priorAllocationTime;
            if (this.priorAllocationMinute < MINUTES)
            {
                string message = String.Format("Perion DDID allocation time is less than {0} minutes, allocation time was {1} minutes", MINUTES, priorAllocationMinute);
                LogUtils.MyHandle.WriteToLog(message);
                Slack.SlackNotifier slack = new Slack.SlackNotifier(message);
                slack.Start();
            }
        }
    }
}