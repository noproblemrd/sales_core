﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic.Dialer.PerionDDID
{
    public class PerionDDIDManager
    {
        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
         public string GetNumber(string perionSessionId, Guid ExpertiseId)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@sessionId", perionSessionId);
            parameters.Add("@expertiseid", ExpertiseId);
            var reader = dal.ExecuteReader(queryGetNumber, parameters);
            if (reader.Count == 0)
                return null;
            var first = reader.First();
            if (first["number"] == DBNull.Value)            
                return null;
            string num = (string)first["number"];
            int? min = null;
            if (first["allocationMin"] != DBNull.Value)
                min = (int)first["allocationMin"];
            CheckAllocationTimeDifference(min);
            return num;
        }
        /*
        public string GetNumber(string perionSessionId)
        {
            string number = GetExistingNumber(perionSessionId);
            if (number == null)
            {
                number = AllocateNewNumber(perionSessionId);
            }
            return number;
        }
         * */

        internal string GetSessionIdByNumber(string virtualNumber)
        {
            if (String.IsNullOrEmpty(virtualNumber))
            {
                return null;
            }
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@number", virtualNumber);
            var reader = dal.ExecuteReader(queryGetSessionByNumber, parameters);
            if (reader.Count == 0)
                return null;
            var first = reader.First();
            if (first["New_PerionDDID"] == DBNull.Value || !((bool)first["New_PerionDDID"]))
            {
                return null;
            }
            string sessionId = Convert.ToString(first["New_PerionSessionID"]);
            if (String.IsNullOrEmpty(sessionId))
            {
                object scalar = dal.ExecuteScalar(queryGetRandomSessionId);
                sessionId = Convert.ToString(scalar);
            }
            return sessionId;
        }
        /*
        private string AllocateNewNumber(string perionSessionId)
        {
            //sessionId must be guid because it will be the numbers originid.
            Guid sessionGuid;
            if (!perionSessionId.TryParseToGuid(out sessionGuid))
            {
                return String.Empty;
            }
            string number = null;
            Guid id = Guid.Empty;
            DateTime? allocationTime = null;
            bool done = false;
            while (!done && GetFreeNumber(ref number, ref id, ref allocationTime))
            {
                done = ConfirmNumberAllocation(perionSessionId, id, allocationTime);
            }
            CheckAllocationTimeDifference(allocationTime);
            return number;
        }
        */
        private void CheckAllocationTimeDifference(int? priorAllocationTime)
        {
            if (!priorAllocationTime.HasValue)
                return;
            AllocationTimeDiffernceThread tr = new AllocationTimeDiffernceThread(priorAllocationTime.Value);
            tr.Start();
        }
        /*
        private static bool ConfirmNumberAllocation(string perionSessionId, Guid id, DateTime? allocationTime)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@sessionId", perionSessionId);
            parameters.Add("@dnid", id);
            bool done;
            try
            {
                int changedRows;
                if (allocationTime.HasValue)
                {
                    parameters.Add("@allocationTime", allocationTime.Value);
                    changedRows = dal.ExecuteNonQuery(updateAllocateNumber, parameters);
                }
                else
                {
                    changedRows = dal.ExecuteNonQuery(updateAllocateNumber_nulltime, parameters);
                }
                done = changedRows == 1;
            }
            catch (System.Data.SqlClient.SqlException)//perion sending trash exception
            {
                done = true;
                allocationTime = null;
            }
            return done;
        }
         * */
        /*
        private string GetExistingNumber(string sessionId)
        {
            string number = null;
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@sessionId", sessionId);
            object scalar = dal.ExecuteScalar(queryFindExistingNumber, parameters);
            if (scalar != DBNull.Value)
            {
                number = Convert.ToString(scalar);
            }
            return number;
        }
         * */
        /*
        private bool GetFreeNumber(ref string number, ref Guid id, ref DateTime? allocationTime)
        {
            var reader = dal.ExecuteReader(queryFindFreeNumber);
            var row = reader.First();
            number = Convert.ToString(row["new_number"]);
            id = (Guid)row["new_directnumberid"];
            if (row["new_allocationtime"] != DBNull.Value)
            {
                allocationTime = (DateTime)row["new_allocationtime"];
            }
            else
            {
                allocationTime = null;
            }
            return true;
        }
        */
        private const string queryGetSessionByNumber =
            @"
select top 1 New_PerionSessionID, New_PerionDDID
from new_directnumber
where DeletionStateCode = 0
and new_mappingnumber = @number
";

        private const string queryGetRandomSessionId =
            @"
select New_PerionSessionID
,ROW_NUMBER() over (order by New_PerionSessionID) as row
into #tmpp
from new_directnumber
where DeletionStateCode = 0
and New_PerionDDID = 1
and New_PerionSessionID is not null

SELECT top 1 New_PerionSessionID FROM #tmpp WHERE row >= RAND() * (SELECT MAX(row) FROM #tmpp)

drop table #tmpp
";
        /*
        private const string queryFindExistingNumber =
            @"
declare @number nvarchar(50)
declare @id uniqueidentifier
set @number = null
set @id = null

select top 1 @number = new_number, @id = New_directnumberId
from New_directnumber
where
new_perionddid = 1
and new_perionsessionid = @sessionId
and deletionstatecode = 0

if @id is not null
begin
	update New_directnumberExtensionBase
	set new_allocationtime = GETDATE()
	where New_directnumberId = @id	
end

select @number
";
        */
        /*
        private const string queryFindFreeNumber =
            @"
select top 1 new_directnumberid, new_number, new_allocationtime
from new_directnumber
where
new_perionddid = 1
and deletionstatecode = 0
order by new_allocationtime
";

         */
        /*
        private const string updateAllocateNumber =
            @"
update New_directnumberExtensionBase
set new_perionsessionid = @sessionId
, new_allocationtime = GETDATE()
, new_originid = @sessionId
where new_directnumberid = @dnid
and new_allocationtime = @allocationTime
";

        private const string updateAllocateNumber_nulltime =
            @"
update New_directnumberExtensionBase
set new_perionsessionid = @sessionId
, new_allocationtime = GETDATE()
, new_originid = @sessionId
where new_directnumberid = @dnid
and new_allocationtime is null
";
         * */
        private const string queryGetNumber = @"
DECLARE @number nvarchar(50)
DECLARE @id uniqueidentifier
DECLARE @allocationMin int
SET @number = NULL
SET @id = NULL
SET @allocationMin = NULL

SELECT top 1 @number = new_number, @id = New_directnumberId
FROM New_directnumber
WHERE
	new_perionddid = 1
	and deletionstatecode = 0
	and new_perionsessionid = @sessionId
	and new_primaryexpertiseid = @expertiseid   

if @id is not null
BEGIN
	UPDATE New_directnumberExtensionBase
	SET new_allocationtime = GETDATE()
	WHERE New_directnumberId = @id	
END
ELSE
BEGIN
	DECLARE @OriginId uniqueidentifier
	BEGIN TRY
		 SELECT @OriginId = CONVERT(uniqueidentifier, @sessionId)
	END TRY
	BEGIN CATCH
		SELECT @number number, @allocationMin allocationMin
		RETURN		 
	END CATCH
	IF(NOT EXISTS(
		SELECT *
		FROM dbo.New_primaryexpertiseBase WITH(NOLOCK)
		WHERE New_primaryexpertiseId = @expertiseid)
			OR
		NOT EXISTS(
		SELECT *
		FROM dbo.New_originBase WITH(NOLOCK)
		WHERE New_originId = @OriginId))
	BEGIN
		SELECT @number number, @allocationMin allocationMin
		RETURN
	END
		
		
	BEGIN TRANSACTION
		DECLARE @now datetime = GETDATE()
		SELECT top 1 @id = d.new_directnumberid, @number = d.new_number, @allocationMin = DATEDIFF(minute, d.new_allocationtime, @now)
		FROM New_directnumberExtensionBase d WITH (TABLOCKX, HOLDLOCK)
			INNER JOIN New_directnumberBase dbase WITH (TABLOCKX, HOLDLOCK) on d.New_directnumberId = dbase.New_directnumberId
		WHERE
			d.new_perionddid = 1
			and dbase.deletionstatecode = 0
		order by d.new_allocationtime 
		
		UPDATE New_directnumberExtensionBase
		SET new_allocationtime = @now
			,new_perionsessionid = @sessionId
			,new_primaryexpertiseid = @expertiseid
            ,new_originid = @OriginId
		WHERE New_directnumberId = @id	 
	COMMIT TRANSACTION
END

SELECT @number number, @allocationMin allocationMin";
    }
}
