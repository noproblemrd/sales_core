﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Effect.Crm.Convertor;
using Effect.Crm.Logs;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using DML = NoProblem.Core.DataModel;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    public class CdrManager : XrmUserBase
    {
        #region Ctor
        public CdrManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        } 
        #endregion

        #region Internal Methods

        internal void CreateDNCdr(Guid incidentAccountId, int duration)
        {
            DynamicEntity record = CreateRecordDynamicObject(
                false,
                DML.Xrm.new_calldetailrecord.Type.directnumber,
                DateTime.Now,
                "supplier's phone",
                duration,
                "unknown in direct numbers",
                incidentAccountId);
            DataAccessLayer.CallDetailRecord callDetailDal = new NoProblem.Core.DataAccessLayer.CallDetailRecord(XrmDataContext);
            callDetailDal.AddRecordToCRM(record);
            if (duration == 0)
            {
                Utils.ZeroDurationNotifier.ZeroDurationEvent(incidentAccountId);
            }
        }

        internal void CreateCdrs(string cdrxml, Guid requestEntityId, bool isNonPPC)
        {
            if (string.IsNullOrEmpty(cdrxml))
            {
                return;
            }
            DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DataAccessLayer.CallDetailRecord callDetailDal = new NoProblem.Core.DataAccessLayer.CallDetailRecord(XrmDataContext);
            DataAccessLayer.SystemUser systemUserDal = new NoProblem.Core.DataAccessLayer.SystemUser(XrmDataContext);
            DML.Xrm.systemuser currentUser = systemUserDal.GetCurrentUser();
            XDocument xDoc = XDocument.Parse(cdrxml);
            XElement root = xDoc.Element("cdrlist");
            IEnumerable<XElement> cdrList = root.Elements();
            foreach (var cdr in cdrList)
            {
                try
                {
                    string typeStr = cdr.Attribute("type").Value;
                    DML.Xrm.new_calldetailrecord.Type type =
                        (DML.Xrm.new_calldetailrecord.Type)Enum.Parse(
                        typeof(DML.Xrm.new_calldetailrecord.Type),
                        typeStr,
                        true);
                    DateTime timeStamp = DateTime.ParseExact(
                        cdr.Element("timestamp").Value,
                        "dd/MM/yyyy HH:mm:ss",
                        System.Globalization.CultureInfo.InvariantCulture);
                    string toPhone = cdr.Element("to").Value;
                    int duration = int.Parse(cdr.Element("duration").Value);
                    string trunk = cdr.Element("trunk").Value;
                    Guid supplierId = new Guid(cdr.Element("providerid").Value);
                    Guid callRecordId = GetCallRecordId(requestEntityId, isNonPPC, incidentAccountDal, supplierId);
                    DynamicEntity record = CreateRecordDynamicObject(isNonPPC, type, timeStamp, toPhone, duration, trunk, callRecordId);
                    callDetailDal.AddRecordToCRM(record, currentUser);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception was thrown while creating call detail record");
                }
            }
        } 

        #endregion

        #region Private Methods

        private DynamicEntity CreateRecordDynamicObject(bool isNonPPC, DML.Xrm.new_calldetailrecord.Type type, DateTime timeStamp, string toPhone, int duration, string trunk, Guid callRecordId)
        {
            DynamicEntity record = ConvertorCRMParams.GetNewDynamicEntity("new_calldetailrecord");
            ConvertorCRMParams.SetDynamicEntityPropertyValue(
                record,
                "new_type",
                ConvertorCRMParams.GetCrmPicklist((int)type),
                typeof(PicklistProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(
                record,
                "new_timeofcall",
                ConvertorCRMParams.GetCrmDateTime(timeStamp),
                typeof(CrmDateTimeProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(
                record,
                "new_tophone",
                toPhone,
                typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(
                record,
                "new_duration",
                ConvertorCRMParams.GetCrmNumber(duration, false),
                typeof(CrmNumberProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(
                record,
                "new_trunk",
                trunk,
                typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(
                record,
                "new_name",
                type.ToString(),
                typeof(StringProperty));
            ConvertorCRMParams.SetDynamicEntityPropertyValue(
                record,
                isNonPPC ? "new_nonppcrequestid" : "new_incidentaccountid",
                ConvertorCRMParams.GetCrmLookup(isNonPPC ? "new_nonppcrequest" : "new_incidentaccount", callRecordId),
                typeof(LookupProperty));
            return record;
        }

        private Guid GetCallRecordId(Guid requestEntityId, bool isNonPPC, DataAccessLayer.IncidentAccount incidentAccountDal, Guid supplierId)
        {
            Guid callRecordId;
            if (isNonPPC)
            {
                callRecordId = requestEntityId;
            }
            else
            {
                DML.Xrm.new_incidentaccount incidentAccount = incidentAccountDal.GetIncidentAccountByIncidentAndAccount(requestEntityId, supplierId);
                if (incidentAccount != null)
                {
                    callRecordId = incidentAccount.new_incidentaccountid;
                }
                else
                {
                    throw new Exception(string.Format("IncidentAccount record was not found for incidentId {0} and accountId {1}", requestEntityId, supplierId));
                }
            }
            return callRecordId;
        } 

        #endregion
    }
}
