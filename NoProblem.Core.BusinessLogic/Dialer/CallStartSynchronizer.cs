﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    class CallStartSynchronizer : Runnable
    {
        private Guid supplierId;
        private decimal pricePerCall;
        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();

        public CallStartSynchronizer(Guid supplierId, decimal pricePerCall)
        {
            this.supplierId = supplierId;
            this.pricePerCall = pricePerCall;
        }

        protected override void Run()
        {
            UpsertDailyBudgetCounter();
            UpdateAvailableCash();
        }

        private void UpdateAvailableCash()
        {
            Balance.BalanceManager m = new Balance.BalanceManager(XrmDataContext);
            m.AddToAvailableBalance(supplierId, pricePerCall * -1);
        }

        private void UpsertDailyBudgetCounter()
        {
            DateTime date = FixDateToLocal(DateTime.Now).Date;
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            parameters.Add("@date", date);
            dal.ExecuteNonQuery(updateCounterSql, parameters);
        }

        private const string updateCounterSql =
            @"
MERGE DailyBudgetCounter AS Target
USING (select @supplierId, convert(date, @date), 1) AS Source (AccountId, Date, Count)
ON (Target.AccountId = Source.AccountId AND Target.Date = Source.Date)
WHEN MATCHED THEN
    UPDATE SET Target.Count = Target.Count + 1
WHEN NOT MATCHED BY TARGET THEN
    INSERT (AccountId, Date, Count)
    VALUES (Source.AccountId, Source.Date, Source.Count)
;
";

    }
}
