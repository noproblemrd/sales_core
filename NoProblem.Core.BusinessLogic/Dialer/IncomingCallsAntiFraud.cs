﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    internal class IncomingCallsAntiFraud : XrmUserBase
    {
        public IncomingCallsAntiFraud(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public bool CheckAntiFraud(string customerCallerId)
        {
            return CheckAntiFraud(customerCallerId, new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext));
        }

        public bool CheckAntiFraud(string customerCallerId, DataAccessLayer.VnRequest dal)
        {
            bool isBlockedNumber = Phones.IsBlockedNumber(customerCallerId);
            var configs = GetAntiFraudConfigs(isBlockedNumber);
            int numberOfCalls = dal.GetNumberOfCalls(customerCallerId, DateTime.Now.AddSeconds(configs.SecondsBack * -1));
            bool passed = numberOfCalls + 1 < configs.NumberOfAttempts;
            return passed;
        }

        private AntiFraudConfigs GetAntiFraudConfigs(bool isBlockedNumber)
        {
            AntiFraudConfigs retVal = new AntiFraudConfigs();
            DataAccessLayer.AsteriskConfiguration configSettings = new NoProblem.Core.DataAccessLayer.AsteriskConfiguration(XrmDataContext);
            if (!int.TryParse(configSettings.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.VN_ANTI_FRAUD_SECONDS_BACK), out retVal.SecondsBack))
            {
                retVal.SecondsBack = 60;
            }
            if (isBlockedNumber)
            {
                if (!int.TryParse(configSettings.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.VN_ANTI_FRAUD_BLOCKED_NUMBER_ATTEMPTS), out retVal.NumberOfAttempts))
                {
                    retVal.NumberOfAttempts = 4;
                }
            }
            else
            {
                if (!int.TryParse(configSettings.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.VN_ANTI_FRAUD_NORMAL_NUMBER_ATTEMPTS), out retVal.NumberOfAttempts))
                {
                    retVal.NumberOfAttempts = 3;
                }
            }
            return retVal;
        }

        private struct AntiFraudConfigs
        {
            internal int SecondsBack;
            internal int NumberOfAttempts;
        }
    }
}
