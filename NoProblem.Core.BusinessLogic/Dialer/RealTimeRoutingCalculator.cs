﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dialer.RealTimeRouting
{
    internal class RealTimeRoutingCalculator
    {
        private static Random random = new Random();

        public static RealTimeRoutingItem GetWinnerFromSet(List<RealTimeRoutingItem> workingSet)
        {
            decimal sumOfRanks = workingSet.Sum(x => x.Rank);
            decimal topRank = workingSet.Max(x => x.Rank);

            foreach (var item in workingSet)
            {
                decimal weight = item.Rank / sumOfRanks;
                decimal distance = item.Rank / topRank;
                item.DistanceTimesWeight = weight * distance;
            }

            decimal normalizationBase = workingSet.Sum(x => x.DistanceTimesWeight);

            foreach (var item in workingSet)
            {
                item.NormalizedResult = item.DistanceTimesWeight / normalizationBase;
            }

            decimal dice = Convert.ToDecimal(random.NextDouble());

            decimal aggregation = 0;
            int winningIndex = 0;
            for (int i = 0; i < workingSet.Count; i++)
            {
                aggregation += workingSet[i].NormalizedResult;
                if (dice <= aggregation)
                {
                    winningIndex = i;
                    break;
                }
            }

            return workingSet[winningIndex];
        }
    }

    internal class RealTimeRoutingItem
    {
        public object SupplierId { get; set; }
        public object RecordCalls { get; set; }
        public object Phone { get; set; }
        public object IsLeadBuyer { get; set; }
        public object IncomingCallCallerPrompt { get; set; }
        public object LimitCallDuration { get; set; }
        public object PricePerCall { get; set; }
        public decimal Rank { get; set; }
        public decimal DistanceTimesWeight { get; set; }
        public decimal NormalizedResult { get; set; }
        public object Country { get; set; }
        public object BillableCallDuration { get; set; }
    }
}
