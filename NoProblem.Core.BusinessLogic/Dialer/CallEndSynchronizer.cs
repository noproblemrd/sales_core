﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    class CallEndSynchronizer : XrmUserBase
    {
        public CallEndSynchronizer(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();

        public void UpsertDailyBudgetCounter(Guid supplierId)
        {
            DateTime date = FixDateToLocal(DateTime.Now).Date;
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            parameters.Add("@date", date);
            dal.ExecuteNonQuery(updateCounterSql, parameters);
        }

        private string updateCounterSql = @"            
            update DailyBudgetCounter set Count = Count - 1 where AccountId = @supplierId and Date = convert(date, @date)
        ";
    }
}
