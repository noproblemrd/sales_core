﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CNSTS = NoProblem.Core.DataModel.Constants;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    public static class DialerCountryTrunkConfiguration
    {
        private static readonly Dictionary<CountryTrunkKey, CountryTrunkConfigurationContainer> data =
            new Dictionary<CountryTrunkKey, CountryTrunkConfigurationContainer>()
            {
                //XCASTLABS
                {
                    new CountryTrunkKey(
                        CNSTS.Countries.UNITED_STATES, 
                        CNSTS.Trunks.XCASTLABS ), 
                    new CountryTrunkConfigurationContainer(
                        String.Empty, 
                        String.Empty )
                },
                {
                    new CountryTrunkKey(
                        CNSTS.Countries.UNITED_KINGDOM, 
                        CNSTS.Trunks.XCASTLABS ), 
                    new CountryTrunkConfigurationContainer(
                        "01144", 
                        "0" )
                },
                {
                    new CountryTrunkKey(
                        CNSTS.Countries.ISRAEL, 
                        CNSTS.Trunks.XCASTLABS ), 
                    new CountryTrunkConfigurationContainer(
                        "011972", 
                        "0" )
                },
                //SIPUS
                {
                    new CountryTrunkKey(
                        CNSTS.Countries.UNITED_STATES, 
                        CNSTS.Trunks.SIPUS ), 
                    new CountryTrunkConfigurationContainer(
                        "1", 
                        String.Empty )
                },
                {
                    new CountryTrunkKey(
                        CNSTS.Countries.UNITED_KINGDOM, 
                        CNSTS.Trunks.SIPUS ), 
                    new CountryTrunkConfigurationContainer(
                        "01144", 
                        "0" )
                },
                {
                    new CountryTrunkKey(
                        CNSTS.Countries.ISRAEL, 
                        CNSTS.Trunks.SIPUS ), 
                    new CountryTrunkConfigurationContainer(
                        "011972", 
                        "0" )
                },
                //ISR_012
                {
                    new CountryTrunkKey(
                        CNSTS.Countries.UNITED_STATES, 
                        CNSTS.Trunks.ISR_012 ), 
                    new CountryTrunkConfigurationContainer(
                        "0121", 
                        String.Empty )
                },
                {
                    new CountryTrunkKey(
                        CNSTS.Countries.UNITED_KINGDOM, 
                        CNSTS.Trunks.ISR_012 ), 
                    new CountryTrunkConfigurationContainer( 
                        "01244", 
                        "0")
                },
                {
                    new CountryTrunkKey( 
                        CNSTS.Countries.ISRAEL, 
                        CNSTS.Trunks.ISR_012 ), 
                    new CountryTrunkConfigurationContainer( 
                        String.Empty, 
                        String.Empty )
                }
            };

        public static CountryTrunkConfigurationContainer GetConfiguration(string country, string trunk)
        {
            if (String.IsNullOrEmpty(trunk))
                trunk = GetConfiguredTrunk();
            if (String.IsNullOrEmpty(country))
                country = GlobalConfigurations.DefaultCountry;
            CountryTrunkKey key = new CountryTrunkKey(country, trunk);
            CountryTrunkConfigurationContainer configuration;
            if (!data.TryGetValue(key, out configuration))
            {
                configuration = new CountryTrunkConfigurationContainer(String.Empty, String.Empty);
            }
            return configuration;
        }

        private static string GetConfiguredTrunk()
        {            
            DataAccessLayer.AsteriskConfiguration phoneConfig = new DataAccessLayer.AsteriskConfiguration(null);
            return phoneConfig.GetConfigurationValue(DataModel.AsteriskConfigurationKeys.FONEAPI_TRUNK);
        }

        private class CountryTrunkKey
        {
            private string country;
            private string trunk;

            public CountryTrunkKey(string country, string trunk)
            {
                this.country = country;
                this.trunk = trunk;
            }

            public override int GetHashCode()
            {
                return country.GetHashCode() + trunk.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                if (!(obj is CountryTrunkKey))
                    return false;
                CountryTrunkKey other = (CountryTrunkKey)obj;
                return this.trunk == other.trunk && this.country == other.country;
            }
        }

        public class CountryTrunkConfigurationContainer
        {
            public CountryTrunkConfigurationContainer(string addPrefix, string removePrefix)
            {
                AddPrefix = addPrefix;
                RemovePrefix = removePrefix;
            }

            public string AddPrefix { get; private set; }
            public string RemovePrefix { get; private set; }
        }
    }
}
