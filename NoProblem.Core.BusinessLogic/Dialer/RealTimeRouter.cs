﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NoProblem.Core.BusinessLogic.Dialer.RealTimeRouting
{
    public static class RealTimeRouter
    {
        private const string getSupplierForRealTimeRouting_query =
           @"dbo.GetSupplierForRealTimeRouting";

        private static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();

        public class RealTimeRouterResponse
        {
            public bool IsLeadBuyer { get; set; }
            public string IncomingCallCallerPrompt { get; set; }
            public int LimitCallDuration { get; set; }
            public decimal PricePerCall { get; set; }
            public bool IsAvailable { get; set; }
            public Guid SupplierId { get; set; }
            public bool RecordCalls { get; set; }
            public string TruePhoneNumber { get; set; }
            public string SupplierCountry { get; set; }
            public int BillableCallDuration { get; set; }
        }

        public static bool IsSomeoneAvailable(Guid headingId)
        {
            DataAccessLayer.Reader reader = ExecuteQuery(headingId, null, null, Guid.Empty, false);
            return reader.Count > 0;
        }

        public static RealTimeRouterResponse GetSupplierForRealTimeRouting(Guid headingId, string country)
        {
            return GetSupplierForRealTimeRouting(headingId, country, null, Guid.Empty);
        }

        public static RealTimeRouterResponse GetSupplierForRealTimeRouting(Guid headingId, string country, IEnumerable<Guid> irrelevantBuyers, Guid level4RegionId)
        {
            RealTimeRouterResponse retVal = new RealTimeRouterResponse();
            DataAccessLayer.Reader reader = ExecuteQuery(headingId, country, irrelevantBuyers, level4RegionId);

            if (reader.Count == 0)
            {
                retVal.IsAvailable = false;
                return retVal;
            }

            List<Dialer.RealTimeRouting.RealTimeRoutingItem> workingSet = new List<Dialer.RealTimeRouting.RealTimeRoutingItem>();
            foreach (var row in reader)
            {
                Dialer.RealTimeRouting.RealTimeRoutingItem item = new Dialer.RealTimeRouting.RealTimeRoutingItem();
                item.SupplierId = row["supplierId"];
                item.RecordCalls = row["recordCalls"];
                item.Phone = row["phone"];
                item.IsLeadBuyer = row["isLeadBuyer"];
                item.IncomingCallCallerPrompt = row["incomingCallCallerPrompt"];
                item.LimitCallDuration = row["New_LimitCallDuration"];
                item.PricePerCall = row["new_incidentprice"];
                item.Rank = (decimal)row["rank"];
                item.Country = row["country"];
                item.BillableCallDuration = row["new_mincallduration"];
                workingSet.Add(item);
            }

            Dialer.RealTimeRouting.RealTimeRoutingItem winner = Dialer.RealTimeRouting.RealTimeRoutingCalculator.GetWinnerFromSet(workingSet);

            retVal.IsAvailable = true;
            retVal.SupplierId = (Guid)winner.SupplierId;
            retVal.RecordCalls = winner.RecordCalls != DBNull.Value ? (bool)winner.RecordCalls : false;
            retVal.TruePhoneNumber = Convert.ToString(winner.Phone);
            retVal.IsLeadBuyer = winner.IsLeadBuyer != DBNull.Value ? (bool)winner.IsLeadBuyer : false;
            retVal.IncomingCallCallerPrompt = Convert.ToString(winner.IncomingCallCallerPrompt);
            if (winner.LimitCallDuration != DBNull.Value)
            {
                int limitLocal = (int)winner.LimitCallDuration;
                if (limitLocal > 0)
                {
                    retVal.LimitCallDuration = limitLocal;
                }
            }
            if (winner.BillableCallDuration != DBNull.Value)
            {
                retVal.BillableCallDuration = (int)winner.BillableCallDuration;
            }
            retVal.PricePerCall = (decimal)winner.PricePerCall;
            retVal.SupplierCountry = Convert.ToString(winner.Country);
            return retVal;
        }

        private static DataAccessLayer.Reader ExecuteQuery(Guid headingId, string country, IEnumerable<Guid> irrelevantBuyers, Guid regionId)
        {
//#if DEBUG
 //           return ExecuteQuery(headingId, country, irrelevantBuyers, regionId, false);
//#else
            return ExecuteQuery(headingId, country, irrelevantBuyers, regionId, true);
//#endif
        }

        private static DataAccessLayer.Reader ExecuteQuery(Guid headingId, string country, IEnumerable<Guid> irrelevantBuyers, Guid regionId, bool checkCountry)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = PrepareSqlParameters(headingId, country, irrelevantBuyers, regionId, checkCountry);
            DataAccessLayer.Reader reader = dal.ExecuteReader(getSupplierForRealTimeRouting_query, parameters, System.Data.CommandType.StoredProcedure);
            return reader;
        }

        private static DataAccessLayer.Generic.SqlParametersList PrepareSqlParameters(Guid headingId, string country, IEnumerable<Guid> irrelevantBuyers, Guid regionId, bool checkCountry)
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@PrimaryExpertiseId", headingId);
            parameters.Add("@Country", String.IsNullOrEmpty(country) ? GlobalConfigurations.DefaultCountry : country);
            parameters.Add("@DefaultCountry", GlobalConfigurations.DefaultCountry);
            parameters.Add("@Level4RegionId", regionId);
            parameters.Add("@CheckCountry", checkCountry);
            DataTable table = new DataTable("IdsTable");
            table.Columns.Add("id", typeof(Guid));
            if (irrelevantBuyers != null && irrelevantBuyers.Count() > 0)
            {
                foreach (var id in irrelevantBuyers)
                {
                    table.Rows.Add(id);
                }
            }
            parameters.Add("@irrelevantBuyers", table);
            return parameters;
        }

    }
}
