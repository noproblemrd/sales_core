﻿using System;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel;
using DML = NoProblem.Core.DataModel;
using System.Threading;
using NoProblem.Core.DataModel.Jona;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    public class CallManager : ContextsUserBase
    {
        #region Ctor
        public CallManager(DML.Xrm.DataContext xrmDataContext, Effect.Crm.SDKProviders.CrmServiceSdk.CrmService crmService)
            : base(xrmDataContext, crmService)
        {
        }
        #endregion

        public void AuctionCompleted(AuctionResult Result)
        {
            Thread t1 = new Thread(delegate()
            {
                try
                {
                    DataAccessLayer.Incident incidentDAL = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                    DML.Xrm.incident incident = incidentDAL.Retrieve(new Guid(Result.ServiceRequestId));
                    incident.new_dialerstatus = (int)DML.Xrm.incident.DialerStatus.DELIVERED_REQUEST;
                    incidentDAL.Update(incident);

                    DataAccessLayer.IncidentAccount incidentAccountDAL = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                    DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                    Balance.BalanceManager balanceManager = new Balance.BalanceManager(XrmDataContext);
                    DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                    for (int i = 0; i < Result.Providers.Length; i++)
                    {
                        DML.Xrm.new_incidentaccount incidentAccount = incidentAccountDAL.GetIncidentAccountByIncidentAndAccount(new Guid(Result.ServiceRequestId), new Guid(Result.Providers[i].ProviderId));
                        incidentAccount.new_potentialincidentprice = Result.Providers[i].BidValue;
                        incidentAccount.new_bidlevel = i + 1;
                        incidentAccount.new_isdefaultpricechanged = Result.Providers[i].IsRealTimeBid;
                        if (Result.Providers[i].IsWinner == false)
                        {
                            incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.LOST;
                            incidentAccount.new_isrejected = Result.Providers[i].IsRejected;
                            DML.Xrm.account supplier = accountRepositoryDal.Retrieve(incidentAccount.new_accountid.Value);
                            //we need to take down money from the account's available cash because in the plugin (when turned to lost) the potetial price bid will be added back.
                            DML.Xrm.new_accountexpertise accExp = accExpDal.GetByAccountAndPrimaryExpertise(supplier.accountid, incident.new_primaryexpertiseid.Value);
                            decimal takeDown = incidentAccount.new_potentialincidentprice.Value - accExp.new_incidentprice.Value;
                            balanceManager.AddToAvailableBalance(supplier.accountid, (takeDown * -1));
                        }
                        else
                        {
                            incidentAccount.statuscode = (int)DML.Xrm.new_incidentaccount.Status.IN_AUCTION;
                        }
                        incidentAccountDAL.Update(incidentAccount);
                    }
                    XrmDataContext.SaveChanges();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "AuctionCompleted inner thread failed with exception. caseId = {0}", Result.ServiceRequestId);
                }
            });
            t1.Start();
        }

        public string CallStatus(Guid incidentId, Guid accountId, int statusCode)
        {
            DataAccessLayer.IncidentAccount incidentAccountDAL = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            DML.Xrm.new_incidentaccount incidentAccount = incidentAccountDAL.GetIncidentAccountByIncidentAndAccount(incidentId, accountId);
            DML.Xrm.new_incidentaccount.Status callStatus = DML.Xrm.new_incidentaccount.Status.Waiting;
            enumCallStatus enumStatus = (enumCallStatus)statusCode;
            switch (enumStatus)
            {
                case enumCallStatus.ADVERTISER_CONFIRMED:
                case enumCallStatus.CALL_IN_PROGRESS:
                    callStatus = DML.Xrm.new_incidentaccount.Status.IN_CONVERSATION;
                    break;
                case enumCallStatus.ERROR:
                    callStatus = DML.Xrm.new_incidentaccount.Status.ERROR;
                    break;
                case enumCallStatus.REJECTED:
                    callStatus = NoProblem.Core.DataModel.Xrm.new_incidentaccount.Status.LOST;
                    incidentAccount.new_isrejected = true;
                    incidentAccount.new_tocharge = false;
                    break;
                default:
                    break;
            }            
            incidentAccount.statuscode = (int)callStatus;
            incidentAccountDAL.Update(incidentAccount);
            XrmDataContext.SaveChanges();
            LogUtils.MyHandle.WriteToLog(8, "**** got {1} and made to {0}. incidentId:{2}. accountId:{3} ****", callStatus, enumStatus, incidentId, accountId);
            return "0";
        }
    }
}

