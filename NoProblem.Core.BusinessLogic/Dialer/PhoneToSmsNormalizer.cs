﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    public static class PhoneToSmsNormalizer
    {
        public static string NormalizePhone(string phone, string country)
        {
            if (String.IsNullOrEmpty(phone))
                return phone;
            if (String.IsNullOrEmpty(country))
                country = GlobalConfigurations.DefaultCountry;
            SmsCountryDataContainer configuration = GetConfiguration(country);
            if (phone.StartsWith(configuration.RemovePrefix))
                phone = phone.Substring(configuration.RemovePrefix.Length);
            return configuration.AddPrefix + phone;
        }

        private static SmsCountryDataContainer GetConfiguration(string country)
        {
            SmsCountryDataContainer configuration;
            if (!countriesData.TryGetValue(country, out configuration))
            {
                configuration = new SmsCountryDataContainer(String.Empty, String.Empty);
            }
            return configuration;
        }

        private static Dictionary<string, SmsCountryDataContainer> countriesData =
            new Dictionary<string, SmsCountryDataContainer>()
            {
                {
                    DataModel.Constants.Countries.ISRAEL,
                    new SmsCountryDataContainer("+972", "0")
                },
                {
                     DataModel.Constants.Countries.UNITED_STATES,
                     new SmsCountryDataContainer("+1", String.Empty)
                },
                {
                         DataModel.Constants.Countries.UNITED_KINGDOM,
                         new SmsCountryDataContainer("+44", "0")
                }
            };

        private class SmsCountryDataContainer
        {
            public SmsCountryDataContainer(string addPrefix, string removePrefix)
            {
                AddPrefix = addPrefix;
                RemovePrefix = removePrefix;
            }

            public string AddPrefix { get; private set; }
            public string RemovePrefix { get; private set; }
        }
    }
}
