﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    public class DirectCallContactedIdentificationHandler : XrmUserBase
    {
        public class DirectCallTypeContainer
        {
            public Guid SupplierId { get; set; }
            public Guid CategoryId { get; set; }
            public Guid OriginId { get; set; }
            public decimal PriceOfCall { get; set; }
            public DataModel.Xrm.account.DapazStatus? DapazStatus { get; set; }
            public int MinCallDuration { get; set; }
            public string Country { get; set; }
            public eCallType CallType { get; set; }
            public bool IsTest { get; set; }
        }

        public enum eCallType
        {
            Normal, RealTimeRouting, Rerouted, PingPhone
        }

        public DirectCallContactedIdentificationHandler(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        internal DirectCallTypeContainer IdentifyCallType(string directPhoneNumber, string dnQueryId, bool isReroutedCall, Guid? reroutedSupplierId, Guid? rerouteHeadingId, Guid? rerouteOriginId, string custom)
        {
            Guid categoryId = Guid.Empty;
            Guid supplierId = Guid.Empty;
            Guid originId = Guid.Empty;
            decimal priceForCall = 0;
            DataModel.Xrm.account.DapazStatus? dapazStatus = null;
            int minCallDuration = -1;
            string country = GlobalConfigurations.DefaultCountry;
            DirectCallTypeContainer retVal = new DirectCallTypeContainer();
            if (isReroutedCall)
            {
                HandleReroutedCallContacted(reroutedSupplierId, rerouteHeadingId, rerouteOriginId, ref categoryId, ref supplierId, ref originId, ref priceForCall, ref dapazStatus, ref minCallDuration);
                retVal.CallType = eCallType.Rerouted;
            }
            else if (APIs.SellersApi.PingPhone.CustomFieldManager.IsPingPhoneCustom(custom))//Ping Phone API
            {
                bool isTest;
                HandlePingPhoneApiCallContacted(custom, ref categoryId, ref supplierId, ref originId, ref priceForCall, ref minCallDuration, out isTest);
                retVal.CallType = eCallType.PingPhone;
                retVal.IsTest = isTest;
            }
            else//regular not rerouted call
            {
                bool realTimeRouting = HandleNormalCallContacted(directPhoneNumber, ref categoryId, ref supplierId, ref originId, ref priceForCall, ref dapazStatus, ref minCallDuration, ref country);
                retVal.CallType = eCallType.Normal;
                if (realTimeRouting)
                {
                    HandleRealTimeRoutingCallContacted(dnQueryId, categoryId, ref supplierId, ref priceForCall, ref minCallDuration);
                    retVal.CallType = eCallType.RealTimeRouting;
                }
            }
            retVal.CategoryId = categoryId;
            retVal.Country = country;
            retVal.DapazStatus = dapazStatus;
            retVal.MinCallDuration = minCallDuration;
            retVal.OriginId = originId;
            retVal.PriceOfCall = priceForCall;
            retVal.SupplierId = supplierId;
            return retVal;
        }

        private void HandlePingPhoneApiCallContacted(string custom, ref Guid categoryId, ref Guid supplierId, ref Guid originId, ref decimal priceForCall, ref int minCallDuration, out bool isTest)
        {
            APIs.SellersApi.PingPhone.IncomingCallEndedHandler handler = new APIs.SellersApi.PingPhone.IncomingCallEndedHandler(XrmDataContext);
            APIs.SellersApi.PingPhone.IncomingCallEndedHandler.CallEndedContainer container = handler.CallEnded(custom);
            categoryId = container.CategoryId;
            supplierId = container.SupplierId;
            originId = container.OriginId;
            priceForCall = container.PriceForCall;
            minCallDuration = container.MinCallDuration;
            isTest = container.IsTest;
        }

        private void HandleRealTimeRoutingCallContacted(string dnQueryId, Guid categoryId, ref Guid supplierId, ref decimal priceForCall, ref int minCallDuration)
        {
            string realTimeRoutingQuery = @"select top 1 accountextensionbase.accountid, accountextensionbase.new_mincallduration, new_accountexpertise.new_incidentprice
							from accountextensionbase with(nolock)
							join new_vnrequestextensionbase with(nolock) on accountid = new_accountid
							join new_accountexpertise with(nolock) on new_accountexpertise.deletionstatecode = 0 AND new_accountexpertise.statecode = 0 AND new_accountexpertise.new_accountid = accountextensionbase.accountid AND new_accountexpertise.new_primaryexpertiseid = @headingId
							where new_ivrsession = @sessionId";
            DataAccessLayer.DirectNumber dal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            DataAccessLayer.DirectNumber.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_directnumber>.SqlParametersList();
            parameters.Add("@sessionId", dnQueryId);
            parameters.Add("@headingId", categoryId);
            var reader = dal.ExecuteReader(realTimeRoutingQuery, parameters);
            foreach (var row in reader)
            {
                supplierId = (Guid)row["accountid"];
                priceForCall = decimal.Parse(row["new_incidentprice"].ToString());
                if (row["new_mincallduration"] != DBNull.Value)
                {
                    minCallDuration = (int)row["new_mincallduration"];
                }
            }
        }

        private bool HandleNormalCallContacted(string directPhoneNumber, ref Guid categoryId, ref Guid supplierId, ref Guid originId, ref decimal priceForCall, ref DataModel.Xrm.account.DapazStatus? dapazStatus, ref int minCallDuration, ref string country)
        {
            bool isRealTimeRouting = false;
            // Get all the properties of the direct number
            string query =
                @"SELECT 
new_accountexpertise.new_primaryexpertiseid,
new_accountexpertise.new_accountid
,new_directnumber.new_originid
--,new_directnumberid
,acc.new_status
,new_accountexpertise.new_incidentprice
,acc.new_dapazstatus
,new_mincallduration
,new_directnumber.new_realtimerouting as realtimerouting
,new_directnumber.new_primaryexpertiseid as realtimeroutingheadingid
,new_directnumber.new_country as country
					FROM new_directnumber with (nolock) 
					LEFT JOIN new_accountexpertise with(nolock) ON new_accountexpertise.deletionstatecode = 0 AND new_accountexpertise.statecode = 0 AND new_accountexpertise.new_accountexpertiseid = new_directnumber.new_accountexpertiseid  
					LEFT JOIN account acc with(nolock) ON acc.accountid = new_accountexpertise.new_accountid   
					WHERE new_directnumber.deletionstatecode = 0 AND new_directnumber.statecode = 0 AND new_mappingnumber = @mappingNumber";
            using (SqlConnection connection = new SqlConnection(DataAccessLayer.Generic.GetConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@mappingNumber", directPhoneNumber);
                    using (SqlDataReader directnumbers = command.ExecuteReader())
                    {
                        if (directnumbers.Read())
                        {
                            if (directnumbers["new_accountid"] != DBNull.Value)
                                supplierId = (Guid)directnumbers["new_accountid"];
                            if (directnumbers["new_primaryexpertiseid"] != DBNull.Value)
                                categoryId = (Guid)directnumbers["new_primaryexpertiseid"];
                            originId = (Guid)directnumbers["new_originid"];
                            //directNumberid = (Guid)directnumbers["new_directnumberid"];
                            if (directnumbers["new_incidentprice"] != DBNull.Value)
                                priceForCall = decimal.Parse(directnumbers["new_incidentprice"].ToString());
                            if (directnumbers["new_dapazstatus"] != DBNull.Value)
                            {
                                dapazStatus = (DataModel.Xrm.account.DapazStatus)((int)directnumbers["new_dapazstatus"]);
                            }
                            if (directnumbers["new_mincallduration"] != DBNull.Value)
                            {
                                minCallDuration = (int)directnumbers["new_mincallduration"];
                            }
                            if (directnumbers["realtimerouting"] != DBNull.Value)
                            {
                                isRealTimeRouting = (bool)directnumbers["realtimerouting"];
                                if (isRealTimeRouting)
                                    categoryId = (Guid)directnumbers["realtimeroutingheadingid"];
                            }
                            if (directnumbers["country"] != DBNull.Value && !String.IsNullOrEmpty((string)directnumbers["country"]))
                            {
                                country = (string)directnumbers["country"];
                            }
                        }
                    }
                }
            }
            return isRealTimeRouting;
        }

        private void HandleReroutedCallContacted(Guid? reroutedSupplierId, Guid? rerouteHeadingId, Guid? rerouteOriginId, ref Guid categoryId, ref Guid supplierId, ref Guid originId, ref decimal priceForCall, ref DataModel.Xrm.account.DapazStatus? dapazStatus, ref int minCallDuration)
        {
            supplierId = reroutedSupplierId.Value;
            categoryId = rerouteHeadingId.Value;
            originId = rerouteOriginId.Value;
            string query = @"
						select top 1 isnull(new_dapazstatus, 1) new_dapazstatus, new_mincallduration, ae.new_incidentprice
						from accountextensionbase acc
						left join new_accountexpertise ae on ae.new_accountid = acc.accountid and ae.new_primaryexpertiseid = @headingId and ae.statecode = 0 and ae.deletionstatecode = 0
						where acc.accountid = @supplierId                      
						";
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            parameters.Add("@headingId", categoryId);
            var reader = dal.ExecuteReader(query, parameters);
            var row = reader.First();
            dapazStatus = (DataModel.Xrm.account.DapazStatus)((int)row["new_dapazstatus"]);
            if (dapazStatus == DataModel.Xrm.account.DapazStatus.PPA
                || dapazStatus == DataModel.Xrm.account.DapazStatus.RarahQuote)
            {
                if (row["new_mincallduration"] != DBNull.Value)
                {
                    minCallDuration = (int)row["new_mincallduration"];
                }
                if (row["new_incidentprice"] != DBNull.Value)
                    priceForCall = decimal.Parse(row["new_incidentprice"].ToString());
            }
        }
    }
}
