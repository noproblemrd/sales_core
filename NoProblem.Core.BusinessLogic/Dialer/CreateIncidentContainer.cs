﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dialer
{
    public class CreateIncidentContainer
    {
        public NoProblem.Core.DataModel.SqlHelper.ContactMinData CustomerData { get; set; }
        public string CustomerPhone { get; set; }
        public Guid PrimaryExpertise { get; set; }
        public Guid OriginId { get; set; }
        public string DnQueryId { get; set; }
        public bool IsRarah { get; set; }
        public bool IsCallCenterCase { get; set; }
        public DataModel.Xrm.account.DapazStatus? DapazStatus { get; set; }
        public string Country { get; set; }
        public DirectCallContactedIdentificationHandler.eCallType CallType { get; set; }
        public string Custom { get; set; }
        public string VirtualNumber { get; set; }

        public CreateIncidentContainer()
        {
            IsCallCenterCase = false;
            CallType = DirectCallContactedIdentificationHandler.eCallType.Normal;
        }
    }
}
