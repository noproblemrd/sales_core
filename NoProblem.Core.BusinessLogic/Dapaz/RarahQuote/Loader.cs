﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils.Csv;

namespace NoProblem.Core.BusinessLogic.Dapaz.RarahQuote
{
    public class Loader : XrmUserBase
    {
        public Loader()
            : base(null)
        {
            accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            regAccDal = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
            mainDapazOriginId = GetMainDapazOriginId();
            headingDic = new Dictionary<string, KeyValuePair<Guid, int>>();
            config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            voucherNumber = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.RARAH_QUOTE_VOUCHER);
            voucherReasonId = new Guid(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.RARAH_QUOTE_VOUCHER_REASON_ID));
            string emailsStr = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.RARAH_QUOTE_LOAD_ERROR_EMAILS);
            string[] split = emailsStr.Split(';');
            errorEmails = new List<string>();
            foreach (string item in split)
            {
                errorEmails.Add(item);
            }
            errorEmailSubject = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.RARAH_QUOTE_LOAD_ERROR_EMAIL_SUBJECT);
            errorEmailBody = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.RARAH_QUOTE_LOAD_ERROR_EMAIL_BODY);
            GoldenAdvertiserInHebrew = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.GOLDEN_ADVERTISER_IN_HEBREW);
        }

        DataAccessLayer.AccountRepository accountRepositoryDal;
        DataAccessLayer.DirectNumber dnDal;
        DataAccessLayer.AccountExpertise accExpDal;
        DataAccessLayer.PrimaryExpertise headingDal;
        DataAccessLayer.Region regionDal;
        DataAccessLayer.RegionAccount regAccDal;
        DataAccessLayer.ConfigurationSettings config;
        Guid mainDapazOriginId;
        System.Collections.Generic.Dictionary<string, System.Collections.Generic.KeyValuePair<Guid, int>> headingDic;
        private readonly string voucherNumber;
        private readonly Guid voucherReasonId;
        private readonly List<string> errorEmails;
        private readonly string errorEmailSubject;
        private readonly string errorEmailBody;
        private readonly string GoldenAdvertiserInHebrew;
        private const string PPA = "PPA";

        public static void LoadRarahimQuotes(Stream csvStream)
        {
            List<CsvRow> data = new List<CsvRow>();
            // Read sample data from CSV file
            using (CsvFileReader reader = new CsvFileReader(csvStream))
            {
                CsvRow row = new CsvRow();
                while (reader.ReadRow(row))
                {
                    data.Add(row);
                    row = new CsvRow();
                }
            }
            Loader manager = new Loader();
            Thread tr = new Thread(new ParameterizedThreadStart(manager.LoadRarahimQuotesPrivate));
            tr.Start(data);
        }

        private void LoadRarahimQuotesPrivate(object dataObj)
        {
            Dictionary<int, string> failRows = new Dictionary<int, string>();
            int successCount = 0;
            int failCount = 0;
            List<CsvRow> data = (List<CsvRow>)dataObj;
            try
            {
                for (int i = 1; i < data.Count; i++)
                {
                    try
                    {
                        LogUtils.MyHandle.WriteToLog("Rarahim Quote Loader working on advertiser {0} from a total of {1}", i, data.Count - 1);
                        CsvRow row = data[i];
                        string zapAccountNumber = row[0];
                        string name = row[1];
                        string nameForInvoice = row[2];
                        string mainPhone = row[3];
                        string smsPhone = row[4];
                        string headingCodeDapaz = row[5];
                        string mekomonim = row[6];
                        int numberOfWantedCalls = int.Parse(row[7]);
                        string msg;
                        bool ok = CreateAutomatically(name, nameForInvoice, zapAccountNumber, mainPhone, smsPhone, headingCodeDapaz, mekomonim, numberOfWantedCalls, out msg);
                        if (ok)
                        {
                            successCount++;
                        }
                        else//no more virtual numbers
                        {
                            failCount++;
                            if (msg == "No Mover Virtual Numbers")
                            {
                                string error = string.Format("!!!!!!!!!!! Could not create rarah quote in row {0}. msg = {1} Stopping load process !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1", i, msg);
                                LogUtils.MyHandle.WriteToLog(error);
                                failRows.Add(i, error);
                                break;
                            }
                            else
                            {
                                string error = msg;
                                failRows.Add(i, error);
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception loading rarah quote in RarahQuote.Loader.LoadRarahimQuotesPrivate. Moving to next one. row number = {0}", i);
                        string error = string.Format("Exception loading advertiser in RarahQuote.Loader.LoadRarahimQuotesPrivate. Moving to next one. row number = {0}. message = {1}. stack trace = {2}", i, exc.Message, exc.StackTrace);
                        failRows.Add(i, error);
                        failCount++;
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RarahQuote.Loader.LoadRarahimQuotesPrivate");
            }
            //print summary:
            StringBuilder builder = new StringBuilder("Rarah Quote Loading Summary: \n");
            builder.Append("Total to do: ").Append(data.Count - 1).Append("\n ");
            builder.Append("Success count: ").Append(successCount).Append("\n ");
            builder.Append("Failure count: ").Append(failCount).Append(" \n");
            builder.Append("Missing after stop: ").Append(data.Count - 1 - (successCount + failCount)).Append("\n ");
            if (failRows.Count > 0)
            {
                foreach (var s in failRows)
                {
                    builder.Append("row: ").Append(s.Key).Append(". Msg: ").Append(s.Value).Append(" \n");
                }
            }
            else
            {
                builder.Append("No Failures \n");
            }
            LogUtils.MyHandle.WriteToLog(builder.ToString());
        }

        private bool CreateAutomatically(string name, string nameForInvoice, string zapAccountNumber, string mainPhone, string smsPhone, string headingCodeDapaz, string mekomonim, int numberOfWantedCalls, out string msg)
        {
            msg = null;
            DataModel.Xrm.account existingSupplier = SearchForSupplier(zapAccountNumber);
            if (existingSupplier == null)
            {
                return CreateNew(name, nameForInvoice, zapAccountNumber, mainPhone, smsPhone, headingCodeDapaz, mekomonim, numberOfWantedCalls, out msg);
            }
            else
            {
                if (!existingSupplier.new_dapazstatus.HasValue
                    || existingSupplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.PPA
                    || existingSupplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.FixGold
                    || existingSupplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.RarahGold
                    || existingSupplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.Fix
                    )
                {
                    //send error email
                    SendErrorEmails(existingSupplier);
                    return true;
                }
                else if (existingSupplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.Rarah)
                {
                    int rarahPrice;
                    string headMsg;
                    var headingId = GetHeadingIdAndRarahPrice(headingCodeDapaz, out rarahPrice, out headMsg);
                    if (rarahPrice < 0)
                    {
                        msg = headMsg;
                        return false;
                    }
                    existingSupplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Approved;
                    existingSupplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.RarahQuote;
                    existingSupplier.new_rarahquoteleft = numberOfWantedCalls;
                    existingSupplier.new_rarahquotestart = numberOfWantedCalls;
                    existingSupplier.new_rarahquotestartdate = DateTime.Now;
                    existingSupplier.new_mekomonim = mekomonim;
                    existingSupplier.new_disableautodirectnumbers = true;
                    existingSupplier.new_donotsendsms = true;
                    existingSupplier.new_dailybudget = 3;
                    existingSupplier.new_recordcalls = true;
                    int wantedBalance = rarahPrice * numberOfWantedCalls;

                    DataModel.Xrm.new_accountexpertise accExp = new NoProblem.Core.DataModel.Xrm.new_accountexpertise();
                    accExp.new_accountid = existingSupplier.accountid;
                    accExp.new_primaryexpertiseid = headingId;
                    accExp.new_incidentprice = rarahPrice;
                    accExpDal.Create(accExp);
                    XrmDataContext.SaveChanges();

                    DataModel.Xrm.new_directnumber dnEntity = null;
                    lock (DataAccessLayer.DirectNumber.lockObject)
                    {
                        dnEntity = dnDal.GetOneFreeDirectNumber();
                        if (dnEntity != null)
                        {
                            dnEntity.new_accountexpertiseid = accExp.new_accountexpertiseid;
                            dnEntity.new_originid = this.mainDapazOriginId;
                            dnDal.Update(dnEntity);
                            XrmDataContext.SaveChanges();
                        }
                    }
                    if (dnEntity == null)
                    {
                        //handle not free dn.
                        msg = "No More Virtual Numbers";
                        return false;
                    }

                    DoHours(existingSupplier.accountid);

                    DoRegions(existingSupplier.accountid, mekomonim);

                    decimal currentBalance = existingSupplier.new_cashbalance.HasValue ? existingSupplier.new_cashbalance.Value : 0;
                    int toGive = wantedBalance - decimal.ToInt32(currentBalance);
                    if (toGive != 0)
                    {
                        var pricingAns = DoDeposit(toGive, existingSupplier.accountid);
                        if (pricingAns.DepositStatus == NoProblem.Core.DataModel.SupplierPricing.eDepositStatus.OK)
                        {
                            accountRepositoryDal.Update(existingSupplier);
                            XrmDataContext.SaveChanges();
                            PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(existingSupplier.accountid);
                            inserter.InsertToStats();
                            return true;
                        }
                        else
                        {
                            msg = string.Format("Could not make deposit. pricingResponseStatus = {0}", pricingAns.DepositStatus);
                            return false;
                        }
                    }
                    else
                    {
                        accountRepositoryDal.Update(existingSupplier);
                        XrmDataContext.SaveChanges();
                        PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(existingSupplier.accountid);
                        inserter.InsertToStats();
                        return true;
                    }

                }
                else//is rarah quote
                {
                    int rarahPrice;
                    string headMsg;
                    var headingId = GetHeadingIdAndRarahPrice(headingCodeDapaz, out rarahPrice, out headMsg);
                    if (rarahPrice < 0)
                    {
                        msg = headMsg;
                        return false;
                    }
                    existingSupplier.new_rarahquotestartdate = DateTime.Now;
                    existingSupplier.new_rarahquoteleft = numberOfWantedCalls;
                    existingSupplier.new_rarahquotestart = numberOfWantedCalls;
                    int wantedBalance = rarahPrice * numberOfWantedCalls;
                    decimal currentBalance = existingSupplier.new_cashbalance.HasValue ? existingSupplier.new_cashbalance.Value : 0;
                    int toGive = wantedBalance - decimal.ToInt32(currentBalance);
                    if (toGive != 0)
                    {
                        var pricingAns = DoDeposit(toGive, existingSupplier.accountid);
                        if (pricingAns.DepositStatus == NoProblem.Core.DataModel.SupplierPricing.eDepositStatus.OK)
                        {
                            accountRepositoryDal.Update(existingSupplier);
                            XrmDataContext.SaveChanges();
                            PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(existingSupplier.accountid);
                            inserter.InsertToStats();
                            return true;
                        }
                        else
                        {
                            msg = string.Format("Could not make deposit. pricingResponseStatus = {0}", pricingAns.DepositStatus);
                            return false;
                        }
                    }
                    else
                    {
                        accountRepositoryDal.Update(existingSupplier);
                        XrmDataContext.SaveChanges();
                        PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(existingSupplier.accountid);
                        inserter.InsertToStats();
                        return true;
                    }
                }
            }
        }

        private void SendErrorEmails(DataModel.Xrm.account supplier)
        {
            Notifications notifier = new Notifications(XrmDataContext);
            string body = string.Format(errorEmailBody,
                supplier.new_bizid,
                !supplier.new_dapazstatus.HasValue || supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.PPA ?
                        PPA : GoldenAdvertiserInHebrew);
            notifier.SendEmail(errorEmails,
                string.Format(body),
                errorEmailSubject);
        }

        private NoProblem.Core.DataModel.SupplierPricing.CreateSupplierPricingResponse DoDeposit(int amount, Guid supplierId)
        {
            SupplierPricing.SupplierPricingManager pricingManager = new NoProblem.Core.BusinessLogic.SupplierPricing.SupplierPricingManager(XrmDataContext);
            var pricingAns = pricingManager.CreateSupplierPricing(
                        new NoProblem.Core.DataModel.SupplierPricing.CreateSupplierPricingRequest()
                        {
                            BonusType = NoProblem.Core.DataModel.Xrm.new_supplierpricing.BonusType.None,
                            IsAutoRenew = false,
                            PaymentAmount = amount,
                            PaymentMethod = NoProblem.Core.DataModel.Xrm.new_paymentmethod.ePaymentMethod.Voucher,
                            SupplierId = supplierId,
                            UpdateRechargeFields = true,
                            RechargeAmount = 0,
                            VoucherNumber = voucherNumber,
                            VoucherReasonId = voucherReasonId,
                            Action = DataModel.Xrm.new_balancerow.Action.Deposit
                        });
            return pricingAns;
        }

        private bool CreateNew(string name, string nameForInvoice, string zapAccountNumber, string mainPhone, string smsPhone, string headingCodeDapaz, string mekomonim, int numberOfWantedCalls, out string msg)
        {
            msg = null;
            DataModel.Xrm.account supplier = BuildSupplierObject(name, nameForInvoice, zapAccountNumber, mainPhone, smsPhone, mekomonim, numberOfWantedCalls);
            accountRepositoryDal.Create(supplier);
            XrmDataContext.SaveChanges();
            DataModel.Xrm.new_accountexpertise accExp = new NoProblem.Core.DataModel.Xrm.new_accountexpertise();
            accExp.new_accountid = supplier.accountid;
            int rarahPrice;
            string headingMsg;
            accExp.new_primaryexpertiseid = GetHeadingIdAndRarahPrice(headingCodeDapaz, out rarahPrice, out headingMsg);
            if (rarahPrice < 0)
            {
                accountRepositoryDal.Delete(supplier);
                XrmDataContext.SaveChanges();
                msg = headingMsg;
                return false;
            }
            accExp.new_incidentprice = rarahPrice;
            accExpDal.Create(accExp);
            XrmDataContext.SaveChanges();

            DataModel.Xrm.new_directnumber dnEntity = null;
            lock (DataAccessLayer.DirectNumber.lockObject)
            {
                dnEntity = dnDal.GetOneFreeDirectNumber();
                if (dnEntity != null)
                {
                    dnEntity.new_accountexpertiseid = accExp.new_accountexpertiseid;
                    dnEntity.new_originid = this.mainDapazOriginId;
                    dnDal.Update(dnEntity);
                    XrmDataContext.SaveChanges();
                }
            }
            if (dnEntity == null)
            {
                //handle not free dn.
                accExpDal.Delete(accExp);
                accountRepositoryDal.Delete(supplier);
                XrmDataContext.SaveChanges();
                msg = "No More Virtual Numbers";
                return false;
            }

            DoHours(supplier.accountid);

            DoRegions(supplier.accountid, mekomonim);

            var pricingAns = DoDeposit(numberOfWantedCalls * rarahPrice, supplier.accountid);
            if (pricingAns.DepositStatus != NoProblem.Core.DataModel.SupplierPricing.eDepositStatus.OK)
            {
                msg = string.Format("Could not make deposit. pricingResponseStatus = {0}", pricingAns.DepositStatus);
                return false;
            }

            PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(supplier.accountid);
            inserter.InsertToStats();

            return true;
        }

        private void DoRegions(Guid supplierId, string mekomonim)
        {
            List<DataModel.SqlHelper.RegionMinData> regions = new List<NoProblem.Core.DataModel.SqlHelper.RegionMinData>();
            List<Guid> level1s = new List<Guid>();
            string[] split = mekomonim.Split(';');
            foreach (string mekomon in split)
            {
                var reg = regionDal.GetRegionMinDataByCode(mekomon);
                if (reg != null && reg.Level == 2)
                {
                    regions.Add(reg);
                    level1s.Add(reg.ParentId);
                }
            }
            if (regions.Count == 0)
            {
                return;
            }
            foreach (var item in level1s.Distinct())
            {
                var children = regionDal.GetChildrenIds(item);
                bool foundAll = true;
                foreach (var childId in children)
                {
                    if (!regions.Contains(new NoProblem.Core.DataModel.SqlHelper.RegionMinData() { Id = childId }))
                    {
                        foundAll = false;
                        DataModel.Xrm.new_regionaccountexpertise regAcc = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
                        regAcc.new_accountid = supplierId;
                        regAcc.new_regionid = childId;
                        regAcc.new_regionstate = (int)DataModel.Xrm.new_regionaccountexpertise.RegionState.NotWorking;
                        regAccDal.Create(regAcc);
                    }
                }
                DataModel.Xrm.new_regionaccountexpertise regAccLevel1 = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
                regAccLevel1.new_accountid = supplierId;
                regAccLevel1.new_regionid = item;
                if (foundAll)
                    regAccLevel1.new_regionstate = (int)DataModel.Xrm.new_regionaccountexpertise.RegionState.Working;
                else
                    regAccLevel1.new_regionstate = (int)DataModel.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild;
                regAccDal.Create(regAccLevel1);
                XrmDataContext.SaveChanges();
            }
        }

        private void DoHours(Guid supplierId)
        {
            XElement xml = new XElement("SupplierAvailability");
            xml.Add(new XAttribute("SiteId", ""));
            xml.Add(new XAttribute("SupplierId", supplierId));
            XElement availability = new XElement("Availability");
            foreach (var day in Enum.GetNames(typeof(DayOfWeek)))
            {
                availability.Add(
                    new XElement("Day",
                        new XAttribute("Name", day.ToLower()),
                        new XElement("FromTime", "00:00"),
                        new XElement("TillTime", "23:59"))
                    );
            }
            xml.Add(availability);
            Supplier supplierLogic = new Supplier(XrmDataContext, null);
            supplierLogic.SetSupplierAvailability(xml.ToString(), false);
        }

        private NoProblem.Core.DataModel.Xrm.account SearchForSupplier(string zapAccountNumber)
        {
            DataModel.Xrm.account supplier = accountRepositoryDal.GetSupplierByBizId(zapAccountNumber);
            return supplier;
        }

        private Guid GetMainDapazOriginId()
        {
            var originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            Guid mainDapazOrigin = originDal.GetMainDapazOriginId();
            if (mainDapazOrigin == Guid.Empty)
            {
                throw new Exception("Main dapaz origin not configured.");
            }
            return mainDapazOrigin;
        }

        private Guid GetHeadingIdAndRarahPrice(string headingCodeDapaz, out int rarahPrice, out string headingMsg)
        {
            KeyValuePair<Guid, int> pair;
            if (!headingDic.TryGetValue(headingCodeDapaz, out pair))
            {
                var heading = headingDal.GetPrimaryExpertiseByEnglishName(headingCodeDapaz);
                if (heading == null)
                {
                    rarahPrice = -1;
                    headingMsg = string.Format("headingCodeDapaz not found. code = {0}", headingCodeDapaz);
                    return Guid.Empty;

                }
                if (!heading.new_rarahprice.HasValue)
                {
                    rarahPrice = -1;
                    headingMsg = string.Format("No rarah price set for code = {0}", headingCodeDapaz);
                    return Guid.Empty;
                }
                pair = new KeyValuePair<Guid, int>(heading.new_primaryexpertiseid, heading.new_rarahprice.Value);
                headingDic.Add(headingCodeDapaz, pair);
            }
            headingMsg = null;
            rarahPrice = pair.Value;
            return pair.Key;
        }

        private DataModel.Xrm.account BuildSupplierObject(string name, string nameForInvoice, string zapAccountNumber, string mainPhone, string smsPhone, string mekomonim, int numberOfWantedCalls)
        {
            DataModel.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account();
            supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Approved;
            supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.RarahQuote;
            supplier.new_rarahquoteleft = numberOfWantedCalls;
            supplier.new_rarahquotestart = numberOfWantedCalls;
            supplier.new_rarahquotestartdate = DateTime.Now;
            supplier.new_mekomonim = mekomonim;
            supplier.new_disableautodirectnumbers = true;
            supplier.new_custom5 = string.IsNullOrEmpty(nameForInvoice) ? name : nameForInvoice;
            DataAccessLayer.Autonumber.AutonumberManager autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
            string nextNumber = autoNumber.GetNextNumber("account");
            supplier.accountnumber = nextNumber;
            supplier.name = name;
            supplier.new_bizid = zapAccountNumber;
            supplier.telephone1 = mainPhone;
            supplier.telephone3 = smsPhone;
            supplier.new_donotsendsms = true;
            supplier.new_dailybudget = 3;
            supplier.new_recordcalls = true;
            return supplier;
        }


    }
}
