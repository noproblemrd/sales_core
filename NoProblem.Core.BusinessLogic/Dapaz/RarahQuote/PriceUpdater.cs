﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils.Csv;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dapaz.RarahQuote
{
    public class PriceUpdater : XrmUserBase
    {
        public PriceUpdater()
            : base(null)
        {
            headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            globalLow = -1;
        }

        private const string LOW = "low";
        DataAccessLayer.PrimaryExpertise headingDal;
        private int globalLow;

        public static void UpdatePrices(Stream csvStream)
        {
            List<CsvRow> data = new List<CsvRow>();
            // Read sample data from CSV file
            using (CsvFileReader reader = new CsvFileReader(csvStream))
            {
                CsvRow row = new CsvRow();
                while (reader.ReadRow(row))
                {
                    data.Add(row);
                    row = new CsvRow();
                }
            }
            PriceUpdater manager = new PriceUpdater();
            Thread tr = new Thread(new ParameterizedThreadStart(manager.UpdatePricesPrivate));
            tr.Start(data);
        }

        private void UpdatePricesPrivate(object dataObj)
        {
            Dictionary<int, string> failRows = new Dictionary<int, string>();
            int successCount = 0;
            int failCount = 0;
            List<CsvRow> data = (List<CsvRow>)dataObj;
            try
            {
                for (int i = 1; i < data.Count; i++)
                {
                    try
                    {
                        LogUtils.MyHandle.WriteToLog("Rarahim Quote Loader working on heading {0} from a total of {1}", i, data.Count - 1);
                        CsvRow row = data[i];
                        string headingCode = row[0];
                        string type = row[1];
                        int diff = int.Parse(row[2]);
                        var heading = headingDal.GetPrimaryExpertiseByEnglishName(headingCode);
                        if (heading != null)
                        {
                            string query;
                            if (type.ToLower() == LOW)
                            {
                                query = getLowQuery;
                            }
                            else //avg
                            {
                                query = getAvgQuery;
                            }
                            DataAccessLayer.PrimaryExpertise.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_primaryexpertise>.SqlParametersList();
                            parameters.Add("@headId", heading.new_primaryexpertiseid);
                            var scalar = headingDal.ExecuteScalar(query, parameters);
                            int calculated;
                            if (scalar != null && scalar != DBNull.Value)
                                calculated = (int)scalar;
                            else
                                calculated = GetGlobalLow();
                            int newPrice = calculated + diff;
                            if (newPrice < 5)
                                newPrice = 5;
                            heading.new_rarahprice = newPrice;
                            headingDal.Update(heading);
                            XrmDataContext.SaveChanges();
                            successCount++;

                        }
                        else
                        {
                            throw new Exception(string.Format("Heading with code {0} was not found", headingCode));
                        }
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception updating prices for rarah quote in RarahQuote.PriceUpdater.UpdatePricesPrivate. Moving to next one. row number = {0}", i);
                        string error = string.Format("Exception updating prices for rarah quote RarahQuote.PriceUpdater.UpdatePricesPrivate. Moving to next one. row number = {0}. message = {1}. stack trace = {2}", i, exc.Message, exc.StackTrace);
                        failRows.Add(i, error);
                        failCount++;
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RarahQuote.PriceUpdater.UpdatePricesPrivate");
            }
            //print summary:
            StringBuilder builder = new StringBuilder("Rarah Quote headings prices Loading Summary: \n");
            builder.Append("Total to do: ").Append(data.Count - 1).AppendLine();
            builder.Append("Success count: ").Append(successCount).AppendLine();
            builder.Append("Failure count: ").Append(failCount).AppendLine();
            builder.Append("Missing after stop: ").Append(data.Count - 1 - (successCount + failCount)).AppendLine();
            if (failRows.Count > 0)
            {
                foreach (var s in failRows)
                {
                    builder.Append("row: ").Append(s.Key).Append(". Msg: ").Append(s.Value).AppendLine();
                }
            }
            else
            {
                builder.Append("No Failures \n");
            }
            LogUtils.MyHandle.WriteToLog(builder.ToString());
        }

        private int GetGlobalLow()
        {
            if (globalLow == -1)
            {
                object scalar = headingDal.ExecuteScalar(getGlobalLowQuery);
                if (scalar != null && scalar != DBNull.Value)
                    globalLow = (int)scalar;
                else
                    globalLow = 5;
            }
            return globalLow;
        }

        private const string getGlobalLowQuery =
            @"select cast(min(ia.new_potentialincidentprice) as int) result
from new_incidentaccount ia with(nolock)
where
ia.statuscode in (10,12,13)
and ia.new_potentialincidentprice > 0
and ia.createdon > dateadd(month, -1, getdate())";

        private const string getLowQuery =
            @"select cast(min(ia.new_potentialincidentprice) as int) result
from new_incidentaccount ia with(nolock)
join incident inc with(nolock) on ia.new_incidentid = inc.incidentid
where
ia.statuscode in (10,12,13)
and ia.new_potentialincidentprice > 0
and inc.new_primaryexpertiseid = @headId
and ia.createdon > dateadd(month, -1, getdate())";

        private const string getAvgQuery =
    @"select cast(avg(ia.new_potentialincidentprice) as int) result
from new_incidentaccount ia with(nolock)
join incident inc with(nolock) on ia.new_incidentid = inc.incidentid
where
ia.statuscode in (10,12,13)
and ia.new_potentialincidentprice > 0
and inc.new_primaryexpertiseid = @headId";
    }
}
