﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Dapaz.Orders
{
    internal class OrdersSender : XrmUserBase
    {
        private Thread thread;
        private Guid userId;
        private eOrderType orderType;
        private Guid supplierId;
        private DataAccessLayer.AccountRepository accountRepositoryDal;
        private DataAccessLayer.ConfigurationSettings config;
        private DataAccessLayer.Autonumber.AutonumberManager autoNumber;
        private bool enabled;
        private const string SEQUENCE_NAME = "zapOrder";
        private static System.Collections.Generic.SynchronizedCollection<OrdersSender> holder = new SynchronizedCollection<OrdersSender>();

        public OrdersSender(Guid userId, eOrderType orderType, Guid supplierId)
            : base(null)
        {
            config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            enabled = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.ZAP_ORDERS_SYSTEM_ENABLED) == 1.ToString();
            if (enabled)
            {
                thread = new Thread(new ThreadStart(Run));
                this.userId = userId;
                this.orderType = orderType;
                this.supplierId = supplierId;
                accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
            }
        }

        public void SendOrder()
        {
            if (enabled)
            {
                holder.Add(this);
                thread.Start();
            }
        }

        private void Run()
        {
            try
            {
                var supplier = accountRepositoryDal.Retrieve(supplierId);
                if (supplier.new_dapazstatus.HasValue && supplier.new_dapazstatus.Value != (int)DataModel.Xrm.account.DapazStatus.PPA)
                    return;
                if (!supplier.new_isdollaraccount.HasValue || !supplier.new_isdollaraccount.Value)
                    return;
                var user = accountRepositoryDal.Retrieve(userId);
                string loginPrefix = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.ZAP_ORDERS_SYSTEM_LOGIN_PREFIX);
                
                ZapOrdersService.NPCreateOrderSIEBEL client = new NoProblem.Core.BusinessLogic.ZapOrdersService.NPCreateOrderSIEBEL();
                string url = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.ZAP_ORDERS_SYSTEM_URL);
                client.Url = url;

                ServicePointManager.DefaultConnectionLimit = 50;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                ServicePointManager.MaxServicePointIdleTime = 1000 * 5 * 1;
                ServicePointManager.UseNagleAlgorithm = true;
                ServicePointManager.Expect100Continue = false;
                //This for page doesn't have certification 
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

                var request = new NoProblem.Core.BusinessLogic.ZapOrdersService.NPCreateOrderSIEBELProcessRequest();
                request.LoginUser = loginPrefix + user.emailaddress1;

                request.NPOrder = new NoProblem.Core.BusinessLogic.ZapOrdersService.NPOrder_Type();
                request.NPOrder.OrderHeaderRec = new NoProblem.Core.BusinessLogic.ZapOrdersService.OrderHeaderRec_Type();
                request.NPOrder.OrderHeaderRec.CustomerNo = supplier.new_bizid;
                request.NPOrder.OrderHeaderRec.SalesPersonNo = GetSalesPersonNoFromSupplier(supplier);
                request.NPOrder.OrderHeaderRec.InterfaceDate = DateTime.Now;
                request.NPOrder.OrderHeaderRec.CustomerEmail = supplier.emailaddress1;
                request.NPOrder.OrderHeaderRec.TaxRegistrationNum = supplier.new_accountgovernmentid; 

                List<NoProblem.Core.BusinessLogic.ZapOrdersService.OrderItem_Type> headingsList = new List<NoProblem.Core.BusinessLogic.ZapOrdersService.OrderItem_Type>();
                if (orderType == eOrderType.HeadingsChange || orderType == eOrderType.Payment)
                {
                    if (supplier.new_stageinregistration.HasValue && supplier.new_stageinregistration.Value < (int)DataModel.Xrm.account.StageInRegistration.PAID)
                    {
                        return;
                    }

                    request.NPOrder.OrderHeaderRec.ActionType = NoProblem.Core.BusinessLogic.ZapOrdersService.OrderHeaderRec_TypeActionType.UPDATE;
                    DataAccessLayer.AccountExpertise aeDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                    foreach (var ae in aeDal.GetActiveByAccount(supplierId))
                    {
                        headingsList.Add(new NoProblem.Core.BusinessLogic.ZapOrdersService.OrderItem_Type() { ClassCode = ae.new_new_primaryexpertise_new_accountexpertise.new_englishname, ClassName = ae.new_new_primaryexpertise_new_accountexpertise.new_name });
                    }
                    if (supplier.new_rechargetypecode.HasValue)
                    {
                        if (supplier.new_rechargetypecode.Value == (int)DataModel.Xrm.account.RechargeTypeCode.BalanceLow)
                        {
                            request.NPOrder.OrderHeaderRec.ChargingType = NoProblem.Core.BusinessLogic.ZapOrdersService.OrderHeaderRec_TypeChargingType.Renew;
                        }
                        else
                        {
                            request.NPOrder.OrderHeaderRec.ChargingType = NoProblem.Core.BusinessLogic.ZapOrdersService.OrderHeaderRec_TypeChargingType.Monthly;
                        }
                    }
                    string last4digits;
                    bool found = FindCreditCardInfo(out last4digits);
                    if (found)
                    {
                        request.NPOrder.OrderHeaderRec.PaymentType = NoProblem.Core.BusinessLogic.ZapOrdersService.OrderHeaderRec_TypePaymentType.CC;
                        request.NPOrder.OrderHeaderRec.CCLast4Digits = last4digits;
                    }
                    else
                    {
                        request.NPOrder.OrderHeaderRec.PaymentType = NoProblem.Core.BusinessLogic.ZapOrdersService.OrderHeaderRec_TypePaymentType.CASH;
                    }
                    int monthlyFee;
                    int.TryParse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.MONTHLY_PAYMENT_FEE), out monthlyFee);
                    request.NPOrder.OrderHeaderRec.PPABasicMonthlySumNoVAT = Convert.ToDecimal(monthlyFee);
                    request.NPOrder.OrderHeaderRec.PPAChargingCallsSumNoVAT = supplier.new_rechargeamount;
                    //request.NPOrder.OrderHeaderRec.PPAVoucherSumNoVAT = 100;
                    //request.NPOrder.OrderHeaderRec.PPAChargingBonusSumNoVAT = 100;
                }
                else
                {
                    request.NPOrder.OrderHeaderRec.ActionType = NoProblem.Core.BusinessLogic.ZapOrdersService.OrderHeaderRec_TypeActionType.DEACTIVE;
                }
                if (headingsList.Count == 0)
                {
                    headingsList.Add(new NoProblem.Core.BusinessLogic.ZapOrdersService.OrderItem_Type() { ClassName = "10", ClassCode = "10" });
                }
                request.NPOrder.ListOfOrderItems = headingsList.ToArray();
                string nextNumber = autoNumber.GetNextNumber(SEQUENCE_NAME);
                request.SequenceNo = nextNumber;
                var response = client.process(request);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Dapaz.Orders.OrdersSender.Run");
            }
            holder.Remove(this);
        }

        private string GetSalesPersonNoFromSupplier(NoProblem.Core.DataModel.Xrm.account supplier)
        {
            string retVal = string.Empty;
            if (supplier.new_managerid.HasValue)
            {
                if (supplier.new_account_account_account.new_userbizid != null)
                    retVal = supplier.new_account_account_account.new_userbizid;
            }
            return retVal;
        }

        private static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private bool FindCreditCardInfo(out string last4digits)
        {
            string ccToken;
            string cvv2;
            string ccOwnerId;
            string ccOwnerName;
            DateTime tokenDate;
            string chargingCompanyContractId;
            string chargingCompanyAccountNumber;
            string ccType;
            string ccExpDate;
            string billingAddress;
            DataAccessLayer.SupplierPricing spDal = new NoProblem.Core.DataAccessLayer.SupplierPricing(XrmDataContext);
            bool found = spDal.FindCreditCardInfo(supplierId, out ccToken, out cvv2, out ccOwnerId, out ccOwnerName, out last4digits, out tokenDate, out chargingCompanyContractId, out chargingCompanyAccountNumber, out ccType, out ccExpDate, out billingAddress);
            return found;
        }
    }
}
