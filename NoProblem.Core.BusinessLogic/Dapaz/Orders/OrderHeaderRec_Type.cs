﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.ZapOrdersService
{
    public partial class OrderHeaderRec_Type
    {
        public OrderHeaderRec_Type()
        {
            this.ActionType = OrderHeaderRec_TypeActionType.DEACTIVE;
            this.CustomerNo = string.Empty;
            this.SalesPersonNo = string.Empty;
            this.CCLast4Digits = "0000";
        }
    }
}
