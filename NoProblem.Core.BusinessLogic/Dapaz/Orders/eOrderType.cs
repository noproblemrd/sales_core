﻿namespace NoProblem.Core.BusinessLogic.Dapaz.Orders
{
    internal enum eOrderType
    {
        HeadingsChange,
        Deactivation,
        Payment
    }
}