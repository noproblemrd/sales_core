﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils.Csv;
using NoProblem.Core.DataModel.Dapaz.GoldenNumberAdvertisers;

namespace NoProblem.Core.BusinessLogic.Dapaz
{
    public class GoldenNumberAdvertiserManager : XrmUserBase
    {
        #region Ctor
        public GoldenNumberAdvertiserManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }
        #endregion

        private static object bizIdLockObject = new object();

        #region Public Methods

        public CreateGoldenNumberAdvertiserResponse CreateGoldenNumberAdvertiser(CreateGoldenNumberAdvertiserRequest request)
        {
            CreateGoldenNumberAdvertiserResponse response = new CreateGoldenNumberAdvertiserResponse();
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = CreateSupplier(request.Name, null, request.BizId, request.PhoneNumber, request.SmsNumber, request.IsActive, request.DoNotSendSMS, accountRepositoryDal, request.TypeOfGolden == CreateGoldenNumberAdvertiserRequest.eTypeOfGolden.Fix, request.FundsStatus);
            DataAccessLayer.AccountExpertise accountExpertiseDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            DataModel.Xrm.new_accountexpertise genericAccExp = CreateAccountExpertise(supplier, accountExpertiseDal);
            DataModel.Xrm.new_directnumber directNumberEntity = null;
            lock (DataAccessLayer.DirectNumber.lockObject)
            {
                DataAccessLayer.DirectNumber directNumberDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                directNumberEntity = directNumberDal.GetDirectNumberByNumberIfFree(request.VirtualNumber);
                if (directNumberEntity != null)
                {
                    directNumberEntity.new_accountexpertiseid = genericAccExp.new_accountexpertiseid;
                    DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
                    directNumberEntity.new_originid = GetMainDapazOriginId(originDal);
                    directNumberDal.Update(directNumberEntity);
                    XrmDataContext.SaveChanges();
                }
            }
            if (directNumberEntity == null)
            {
                //handle not free dn.
                accountExpertiseDal.Delete(genericAccExp);
                accountRepositoryDal.Delete(supplier);
                XrmDataContext.SaveChanges();
                response.ResponseStatus = CreateGoldenNumberAdvertiserResponse.eCreateGoldenNumberAdvertiserStatus.VirtualNumberNotAvailable;
            }
            if (response.ResponseStatus == CreateGoldenNumberAdvertiserResponse.eCreateGoldenNumberAdvertiserStatus.OK)
            {
                response.SupplierId = supplier.accountid;
                PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(response.SupplierId);
                inserter.InsertToStats();
            }
            return response;
        }

        public UpdateGoldenNumberAdvertiserResponse UpdateGoldenNumberAdvertiser(UpdateGoldenNumberAdvertiserRequest request)
        {
            UpdateGoldenNumberAdvertiserResponse response = new UpdateGoldenNumberAdvertiserResponse();
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = accountRepositoryDal.Retrieve(request.SupplierId);
            if (supplier == null)
            {
                response.ResponseStatus = UpdateGoldenNumberAdvertiserResponse.eUpdateGoldenNumberAdvertiserStatus.SupplierDoesNotExist;
                return response;
            }
            if (!supplier.new_dapazstatus.HasValue
                ||supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPA
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.RarahQuote
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPAHold)
            {
                response.ResponseStatus = UpdateGoldenNumberAdvertiserResponse.eUpdateGoldenNumberAdvertiserStatus.SupplierIsNotDapazGoldenNumberAdvertiser;
                return response;
            }
            supplier.name = request.Name;
            //supplier.new_bizid = request.BizId; //biz id is read only after create.
            if (AreRealNumbersVirtualNumbers(request.PhoneNumber, request.SmsNumber))
            {
                throw new Exception("The advertiser's phone numbers cannot be virtual numbers");
            }
            supplier.telephone1 = request.PhoneNumber;
            supplier.telephone3 = request.SmsNumber;
            supplier.new_donotsendsms = request.DoNotSendSMS;
            supplier.new_fundsstatus = request.FundsStatus == 0 ? (int)DataModel.Xrm.account.FundsStatus.Normal : (int)request.FundsStatus;
            if (request.IsActive)
            {
                supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Approved;
                if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.Rarah)
                {
                    supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.RarahGold;
                }
                else if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.Fix)
                {
                    supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.FixGold;
                }             
            }
            else
            {
                supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Inactive;
                if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.RarahGold)
                {
                    supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.Rarah;
                }
                else if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.FixGold)
                {
                    supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.Fix;
                }   
            }
            if (!(request.NewDapazStatus.HasValue
                && (request.NewDapazStatus.Value == NoProblem.Core.DataModel.Dapaz.DapazStatusChangeOptions.PPA
                || request.NewDapazStatus.Value == NoProblem.Core.DataModel.Dapaz.DapazStatusChangeOptions.RarahQuote
                || request.NewDapazStatus.Value == DataModel.Dapaz.DapazStatusChangeOptions.PPAHold)))
            {
                DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                DataModel.Xrm.new_accountexpertise genericAccountExpertise = accExpDal.GetByAccountAndPrimaryExpertise(supplier.accountid, GetGenericHeadingId());
                if (genericAccountExpertise == null)
                {
                    genericAccountExpertise = CreateAccountExpertise(supplier, accExpDal);
                }
                else if (genericAccountExpertise.statecode != DataAccessLayer.AccountExpertise.ACTIVE)
                {
                    accExpDal.SetState(genericAccountExpertise, true);
                }

                DataAccessLayer.DirectNumber directNumberDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                var oldDirectNumberEntity = directNumberDal.GetDirectNumberByAccountExpertise(genericAccountExpertise.new_accountexpertiseid);
                if (oldDirectNumberEntity == null)
                {
                    DataModel.Xrm.new_directnumber directNumberEntity = null;
                    lock (DataAccessLayer.DirectNumber.lockObject)
                    {
                        directNumberEntity = directNumberDal.GetDirectNumberByNumberIfFree(request.VirtualNumber);
                        if (directNumberEntity != null)
                        {
                            directNumberEntity.new_accountexpertiseid = genericAccountExpertise.new_accountexpertiseid;
                            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
                            directNumberEntity.new_originid = GetMainDapazOriginId(originDal);
                            directNumberDal.Update(directNumberEntity);
                            XrmDataContext.SaveChanges();
                        }
                    }
                    if (directNumberEntity == null)
                    {
                        //handle not free dn.
                        response.ResponseStatus = UpdateGoldenNumberAdvertiserResponse.eUpdateGoldenNumberAdvertiserStatus.VirtualNumberNotAvailable;
                    }
                }
                else if (oldDirectNumberEntity.new_number != request.VirtualNumber)
                {
                    DataModel.Xrm.new_directnumber newDirectNumberEntity = null;
                    lock (DataAccessLayer.DirectNumber.lockObject)
                    {
                        newDirectNumberEntity = directNumberDal.GetDirectNumberByNumberIfFree(request.VirtualNumber);
                        if (newDirectNumberEntity != null)
                        {
                            oldDirectNumberEntity.new_accountexpertiseid = null;
                            oldDirectNumberEntity.new_originid = null;
                            oldDirectNumberEntity.new_frozendate = DateTime.Now.Date;
                            directNumberDal.Update(oldDirectNumberEntity);
                            newDirectNumberEntity.new_accountexpertiseid = genericAccountExpertise.new_accountexpertiseid;
                            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
                            newDirectNumberEntity.new_originid = GetMainDapazOriginId(originDal);
                            directNumberDal.Update(newDirectNumberEntity);
                            XrmDataContext.SaveChanges();
                        }
                    }
                    if (newDirectNumberEntity == null)
                    {
                        //handle not free dn.
                        response.ResponseStatus = UpdateGoldenNumberAdvertiserResponse.eUpdateGoldenNumberAdvertiserStatus.VirtualNumberNotAvailable;
                    }
                }
            }
            if (response.ResponseStatus == UpdateGoldenNumberAdvertiserResponse.eUpdateGoldenNumberAdvertiserStatus.OK)
            {
                accountRepositoryDal.Update(supplier);
                XrmDataContext.SaveChanges();
                PotentialStats.PotentialStatsInserter inserter = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.PotentialStatsInserter(supplier.accountid);
                inserter.InsertToStats();
                if (request.NewDapazStatus.HasValue)
                {
                    DapazStatusChanger changer = new DapazStatusChanger(XrmDataContext);
                    changer.ChangeStatus(supplier, request.NewDapazStatus.Value);
                }
            }
            return response;
        }

        public RetrieveGoldenNumberAdvertiserResponse RetrieveGoldenNumberAdvertiser(RetrieveGoldenNumberAdvertiserRequest request)
        {
            RetrieveGoldenNumberAdvertiserResponse response = new RetrieveGoldenNumberAdvertiserResponse();
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = dal.Retrieve(request.SupplierId);
            if (supplier == null)
            {
                response.ResponseStatus = RetrieveGoldenNumberAdvertiserResponse.eRetrieveGoldenNumberAdvertiserStatus.SupplierDoesNotExist;
                return response;
            }
            if (!supplier.new_dapazstatus.HasValue
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPA
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.RarahQuote)
            {
                response.ResponseStatus = RetrieveGoldenNumberAdvertiserResponse.eRetrieveGoldenNumberAdvertiserStatus.SupplierIsNotDapazGoldenNumberAdvertiser;
                return response;
            }
            response.ExpertiseName = (supplier.new_primaryexpertise_rarahaccount == null) ? string.Empty : supplier.new_primaryexpertise_rarahaccount.new_name;
            response.Region = (supplier.new_region_rarahaccount == null) ? string.Empty : supplier.new_region_rarahaccount.new_name;
            response.BizId = supplier.new_bizid;
            response.IsActive = supplier.new_status == (int)DataModel.Xrm.account.SupplierStatus.Approved;
            response.Name = supplier.name;
            response.PhoneNumber = supplier.telephone1;
            response.SmsNumber = supplier.telephone3;
            response.SupplierId = request.SupplierId;
            response.DoNotSendSMS = (supplier.new_donotsendsms.HasValue && supplier.new_donotsendsms.Value);
            response.FundsStatus = supplier.new_fundsstatus.HasValue ? (DataModel.Xrm.account.FundsStatus)supplier.new_fundsstatus.Value : NoProblem.Core.DataModel.Xrm.account.FundsStatus.Normal;
            response.DapazStatus = (DataModel.Xrm.account.DapazStatus)supplier.new_dapazstatus.Value;
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            DataModel.Xrm.new_accountexpertise genericAccountExpertise = accExpDal.GetByAccountAndPrimaryExpertise(supplier.accountid, GetGenericHeadingId());
            if (genericAccountExpertise == null)
            {
                genericAccountExpertise = CreateAccountExpertise(supplier, accExpDal);
            }
            DataAccessLayer.DirectNumber directNumberDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            var directNumberEntity = directNumberDal.GetDirectNumberByAccountExpertise(genericAccountExpertise.new_accountexpertiseid);
            if (directNumberEntity != null)
            {
                response.VirtualNumber = directNumberEntity.new_number;
            }
            return response;
        }

        public GetAvailableVirtualNumbersResponse GetAvailableVirtualNumbers()
        {
            GetAvailableVirtualNumbersResponse response = new GetAvailableVirtualNumbersResponse();
            DataAccessLayer.DirectNumber dal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            List<string> lst = dal.GetFreeDirectNumbersNumbers();
            response.NumbersList = lst;
            return response;
        }

        public static void LoadAdvertisers(Stream csvStream)
        {
            List<CsvRow> data = new List<CsvRow>();
            // Read sample data from CSV file
            using (CsvFileReader reader = new CsvFileReader(csvStream))
            {
                CsvRow row = new CsvRow();
                while (reader.ReadRow(row))
                {
                    data.Add(row);
                    row = new CsvRow();
                }
            }
            GoldenNumberAdvertiserManager manager = new GoldenNumberAdvertiserManager(null);
            Thread tr = new Thread(new ParameterizedThreadStart(manager.LoadAdvertisersPrivate));
            tr.Start(data);
        }

        #endregion

        #region Private Methods

        private void LoadAdvertisersPrivate(object dataObj)
        {
            Dictionary<int, string> failRows = new Dictionary<int, string>();
            int successCount = 0;
            int failCount = 0;
            List<CsvRow> data = (List<CsvRow>)dataObj;
            try
            {
                for (int i = 1; i < data.Count; i++)
                {
                    try
                    {
                        LogUtils.MyHandle.WriteToLog("Golden Number Advertisers Loader working on advertiser {0} from a total of {1}", i, data.Count);
                        CsvRow row = data[i];
                        string zapAccountNumber = row[1];
                        string name = row[2];
                        string nameForInvoice = row[3];
                        string mainPhone = row[4];
                        string smsPhone = row[5];
                        string msg;
                        bool ok = CreateAdvertiserAutomatically(name, nameForInvoice, zapAccountNumber, mainPhone, smsPhone, out msg);
                        if (ok)
                        {
                            successCount++;
                        }
                        else//no more virtual numbers
                        {
                            failCount++;
                            string error = string.Format("!!!!!!!!!!! Could not create advetiser in row {0}. msg = {1} Stopping load process !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1", i, msg);
                            LogUtils.MyHandle.WriteToLog(error);
                            failRows.Add(i, error);
                            break;
                        }
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception loading advertiser in GoldenNumberAdvertiserManager.LoadAdvertisersPrivate. Moving to next one. row number = {0}", i);
                        string error = string.Format("Exception loading advertiser in GoldenNumberAdvertiserManager.LoadAdvertisersPrivate. Moving to next one. row number = {0}. message = {1}. stack trace = {2}", i, exc.Message, exc.StackTrace);
                        failRows.Add(i, error);
                        failCount++;
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in GoldenNumberAdvertiserManager.LoadAdvertisersPrivate");
            }
            //print summary:
            StringBuilder builder = new StringBuilder("Golden Number Advertiser Loading Summary: \n");
            builder.Append("Total to do: ").Append(data.Count).Append("\n ");
            builder.Append("Success count: ").Append(successCount).Append("\n ");
            builder.Append("Failure count: ").Append(failCount).Append(" \n");
            builder.Append("Missing after stop: ").Append(data.Count - (successCount + failCount)).Append("\n ");
            if (failRows.Count > 0)
            {
                foreach (var s in failRows)
                {
                    builder.Append("row: ").Append(s.Key).Append(". Msg: ").Append(s.Value).Append(" \n");
                }
            }
            else
            {
                builder.Append("No Failures \n");
            }
            LogUtils.MyHandle.WriteToLog(builder.ToString());
        }

        private bool CreateAdvertiserAutomatically(string name, string nameForInvoice, string bizId, string mainPhone, string smsPhone, out string msg)
        {
            msg = null;
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = CreateSupplier(name, nameForInvoice, bizId, mainPhone, smsPhone, true, false, accountRepositoryDal, true, NoProblem.Core.DataModel.Xrm.account.FundsStatus.Normal);
            DataAccessLayer.AccountExpertise accountExpertiseDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            DataModel.Xrm.new_accountexpertise genericAccExp = CreateAccountExpertise(supplier, accountExpertiseDal);
            DataModel.Xrm.new_directnumber directNumberEntity = null;
            lock (DataAccessLayer.DirectNumber.lockObject)
            {
                DataAccessLayer.DirectNumber directNumberDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                directNumberEntity = directNumberDal.GetOneFreeDirectNumber();
                if (directNumberEntity != null)
                {
                    directNumberEntity.new_accountexpertiseid = genericAccExp.new_accountexpertiseid;
                    DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
                    directNumberEntity.new_originid = GetMainDapazOriginId(originDal);
                    directNumberDal.Update(directNumberEntity);
                    XrmDataContext.SaveChanges();
                }
            }
            if (directNumberEntity == null)
            {
                //handle not free dn.
                accountExpertiseDal.Delete(genericAccExp);
                accountRepositoryDal.Delete(supplier);
                XrmDataContext.SaveChanges();
                msg = "No Mover Virtual Numbers";
                return false;
            }
            return true;
        }

        private Guid GetMainDapazOriginId(DataAccessLayer.Origin originDal)
        {
            Guid mainDapazOrigin = originDal.GetMainDapazOriginId();
            if (mainDapazOrigin == Guid.Empty)
            {
                throw new Exception("Main dapaz origin not configured.");
            }
            return mainDapazOrigin;
        }

        private DataModel.Xrm.new_accountexpertise CreateAccountExpertise(DataModel.Xrm.account supplier, DataAccessLayer.AccountExpertise accountExpertiseDal)
        {
            DataModel.Xrm.new_accountexpertise genericAccountExpertise = new NoProblem.Core.DataModel.Xrm.new_accountexpertise();
            genericAccountExpertise.new_accountid = supplier.accountid;
            genericAccountExpertise.new_primaryexpertiseid = GetGenericHeadingId();
            genericAccountExpertise.new_incidentprice = 0;
            accountExpertiseDal.Create(genericAccountExpertise);
            XrmDataContext.SaveChanges();
            return genericAccountExpertise;
        }

        private DataModel.Xrm.account CreateSupplier(string name, string nameForInvoice, string bizId, string mainPhone, string smsPhone, bool isActive, bool doNotSendSMS, DataAccessLayer.AccountRepository accountRepositoryDal, bool isFix, DataModel.Xrm.account.FundsStatus fundsStatus)
        {
            if (AreRealNumbersVirtualNumbers(mainPhone, smsPhone))
            {
                throw new Exception("The advertiser's phone numbers cannot be virtual numbers");
            }
            DataModel.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account();
            supplier.name = name;
            supplier.new_bizid = bizId;
            supplier.telephone1 = mainPhone;
            supplier.telephone3 = smsPhone;
            supplier.new_donotsendsms = doNotSendSMS;
            supplier.new_fundsstatus = fundsStatus == 0 ? (int)DataModel.Xrm.account.FundsStatus.Normal : (int)fundsStatus;
            if (isActive)
            {
                supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Approved;
                if (isFix)
                    supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.FixGold;
                else
                    supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.RarahGold;
            }
            else
            {
                supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Inactive;
                if (isFix)
                    supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.Fix;
                else
                    supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.Rarah;
            }
            supplier.new_disableautodirectnumbers = true;
            supplier.new_custom5 = string.IsNullOrEmpty(nameForInvoice) ? name : nameForInvoice;
            lock (bizIdLockObject)
            {
                if (!accountRepositoryDal.ExistsByBizId(bizId))
                {
                    DataAccessLayer.Autonumber.AutonumberManager autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
                    string nextNumber = autoNumber.GetNextNumber("account");
                    supplier.accountnumber = nextNumber;
                    accountRepositoryDal.Create(supplier);
                    XrmDataContext.SaveChanges();
                }
                else
                {
                    throw new Exception("BIZID_ALREADY_EXISTS");
                }
            }
            return supplier;
        }

        private bool AreRealNumbersVirtualNumbers(string mainPhone, string smsPhone)
        {
            DataAccessLayer.DirectNumber dal = new DataAccessLayer.DirectNumber(XrmDataContext);
            return dal.IsDirectNumber(mainPhone) || dal.IsDirectNumber(smsPhone);
        }

        private Guid GetGenericHeadingId()
        {
            DataAccessLayer.PrimaryExpertise peDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            Guid genericHeadingId = peDal.GetGenericHeadingId();
            if (genericHeadingId == Guid.Empty)
            {
                throw new Exception("Generic heading is not configured in the system.");
            }
            return genericHeadingId;
        }

        #endregion

        public byte[] ExportAdvertisers()
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var reader = dal.ExecuteReader(exportQuery);
            List<CsvRow> rows = new List<CsvRow>();
            for (int i = 0; i < reader.Count; i++)
            {
                CsvRow row = new CsvRow();
                row.Add((i + 1).ToString());
                row.Add(Convert.ToString(reader[i]["new_bizid"]));
                row.Add(Convert.ToString(reader[i]["name"]));
                row.Add(Convert.ToString(reader[i]["new_custom5"]));
                row.Add(Convert.ToString(reader[i]["telephone1"]));
                row.Add(Convert.ToString(reader[i]["telephone3"]));
                row.Add(string.Empty);
                row.Add(string.Empty);
                row.Add(string.Empty);
                row.Add(Convert.ToString(reader[i]["new_number"]));
                rows.Add(row);
            }

            int size = 0;
            foreach (var r in rows)
            {
                foreach (var s in r)
                {
                    size += (s.Length * sizeof(char));
                }
            }

            using (MemoryStream stream = new MemoryStream(size))
            {
                Utils.Csv.CsvFileWriter writer = new CsvFileWriter(stream);
                stream.Capacity += 1 * sizeof(char);
                foreach (var r in rows)
                {
                    writer.WriteRow(r);
                }
                writer.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                byte[] bytes;
                using (BinaryReader br = new BinaryReader(stream))
                {
                    bytes = br.ReadBytes(Convert.ToInt32(stream.Length));
                }
                return bytes;
            }
        }

        private const string exportQuery = @"select new_bizid, name, new_custom5, telephone1, telephone3, new_number
from account acc with(nolock)
join new_accountexpertise ae on new_accountid = accountid
join new_directnumber dn on ae.new_accountexpertiseid = dn.new_accountexpertiseid
where acc.deletionstatecode = 0
and ae.deletionstatecode = 0
and dn.deletionstatecode = 0
and acc.new_dapazstatus = 2
order by acc.createdon";
    }
}
