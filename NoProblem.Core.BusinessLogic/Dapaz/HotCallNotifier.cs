﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Dapaz
{
    internal class HotCallNotifier
    {
        public HotCallNotifier(string bizId, string virtualNumber, string recordingId, string callId, bool isLongCall, bool isLastCall, DateTime quoteStartDate)
        {
            this.bizId = bizId;
            this.virtualNumber = virtualNumber;
            this.recordingId = recordingId;
            this.callId = callId;
            this.isLongCall = isLongCall;
            this.quoteStartDate = quoteStartDate;
            this.isLastCall = isLastCall;
            workerThread = new Thread(new ThreadStart(Run));
        }

        private Thread workerThread;
        private string bizId;
        private string virtualNumber;
        private string recordingId;
        private string callId;
        private bool isLongCall;
        private DateTime quoteStartDate;
        private bool isLastCall;

        //private const string BASE_URL = "http://noproblem.d.co.il/NPHotCallsService/GPNotifyHotCall.svc/rest";
        //private const string REST_RESOURCE = "notify";
        private const string BASE_URL = "http://www.d.co.il/cgi-prog/yp-applications/noproblem/hotcall.cgi";
        private const string BASE_URL_TEST = "http://www.d.co.il/cgi-prog/yp-applications/noproblem/hotcallTest.cgi";
        //private const string EXPECTED_RESPONSE = "<NotifyResponse><NotifyResult>OK</NotifyResult></NotifyResponse>";
        private const string EXPECTED_RESPONSE = "ok";
        private const string UNEXPECTED_HTTP_STATUS_MSG = "Dapaz hot call notify returned unexpected http status in response. HttpStatus = {0}. Content = {1}. bizId = {2}, virtual number = {3}, recordingId = {4}, callId = {5}, isLongCall = {6}, islastCall = {7}, quoteStartDate = {8}";
        private const string UNEXPECTED_CONTENT_MSG = "Dapaz hot call notify returned unexpected content. HttpStatus = {0}. Content = {1}. bizId = {2}, virtual number = {3}, recordingId = {4}, callId = {5}, isLongCall = {6}, islastCall = {7}, quoteStartDate = {8}";
        private const string UNEXPECTED_EXCEPTION_MSG = "Exception in HotCallNotifier async run. bizId = {0}, virtual number = {1}, recordingId = {2}, callId = {3}, isLongCall = {4}, islastCall = {5}, quoteStartDate = {6}";

        private const string CUSTOMER_NO = "CustomerNo";
        private const string NP_RECORDED_CALL_ID = "NPRecordedCallID";
        private const string NP_CALL_ID = "NPCallID";
        private const string HOT_CALL_TYPE = "HotCallType";
        private const string SIMS_LIMIT_START_DATE = "SimsLimitStartDate";
        private const string SIMS_PHONE_NUMBER = "SimsPhoneNumber";


        /// <summary>
        /// Send notification asynchronously to Dapaz.
        /// </summary>
        public void NotifyHotCallEvent()
        {
            workerThread.Start();
        }

        private void Run()
        {
            try
            {
                int callType = DecideCallType();
                string currentSiteId = System.Configuration.ConfigurationManager.AppSettings["SiteId"].ToLower();
                string urlToUse;
                if (currentSiteId == "np_israel")
                {
                    urlToUse = BASE_URL;
                }
                else
                {
                    urlToUse = BASE_URL_TEST;
                }
                RestSharp.RestClient restClient = new RestSharp.RestClient(urlToUse);
                RestSharp.RestRequest restRequest = new RestSharp.RestRequest(RestSharp.Method.POST); //new RestSharp.RestRequest(REST_RESOURCE, RestSharp.Method.POST);
                //restRequest.RequestFormat = RestSharp.DataFormat.Json;
                //restRequest.AddBody(
                //    new
                //    {
                //        CUSTOMER_NO = bizId,
                //        SIMS_PHONE_NUMBER = virtualNumber,
                //        NP_RECORDED_CALL_ID = recordingId,
                //        NP_CALL_ID = callId,
                //        HOT_CALL_TYPE = callType,
                //        SIMS_LIMIT_START_DATE = quoteStartDate
                //    });
                restRequest.AddParameter(CUSTOMER_NO, bizId);
                restRequest.AddParameter(SIMS_PHONE_NUMBER, virtualNumber);
                restRequest.AddParameter(NP_RECORDED_CALL_ID, recordingId);
                restRequest.AddParameter(NP_CALL_ID, callId);
                restRequest.AddParameter(HOT_CALL_TYPE, callType);
                restRequest.AddParameter(SIMS_LIMIT_START_DATE, quoteStartDate);
                if (currentSiteId == "np_israel" || currentSiteId == "israelqa")
                {
                    LogUtils.MyHandle.WriteToLog(7, "Hot call!!! CUSTOMER_NO = {0}, SIMS_PHONE_NUMBER = {1}, NP_RECORDED_CALL_ID = {2}, NP_CALL_ID = {3}, HOT_CALL_TYPE = {4}, SIMS_LIMIT_START_DATE = {5}",
                        bizId, virtualNumber, recordingId, callId, callType, quoteStartDate);
                    var restResponse = restClient.Execute(restRequest);
                    if (restResponse.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        LogUtils.MyHandle.WriteToLog(UNEXPECTED_HTTP_STATUS_MSG, restResponse.StatusCode, restResponse.Content, bizId, virtualNumber, recordingId, callId, isLongCall, isLastCall, quoteStartDate);
                    }
                    else if (restResponse.Content != EXPECTED_RESPONSE)
                    {
                        LogUtils.MyHandle.WriteToLog(UNEXPECTED_CONTENT_MSG, restResponse.StatusCode, restResponse.Content, bizId, virtualNumber, recordingId, callId, isLongCall, isLastCall, quoteStartDate);
                    }
                }
                else
                {
                    LogUtils.MyHandle.WriteToLog("Hot call!!! (non-prod environment) CUSTOMER_NO = {0}, SIMS_PHONE_NUMBER = {1}, NP_RECORDED_CALL_ID = {2}, NP_CALL_ID = {3}, HOT_CALL_TYPE = {4}, SIMS_LIMIT_START_DATE = {5}",
                        bizId, virtualNumber, recordingId, callId, callType, quoteStartDate);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, UNEXPECTED_EXCEPTION_MSG, bizId, virtualNumber, recordingId, callId, isLongCall, isLastCall, quoteStartDate);
            }
        }

        private int DecideCallType()
        {
            int callType;
            if (isLastCall && isLongCall)
                callType = 3;
            else if (isLongCall)
                callType = 1;
            else
                callType = 2;
            return callType;
        }
    }
}
