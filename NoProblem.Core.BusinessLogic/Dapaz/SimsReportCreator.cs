﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dapaz
{
    public class SimsReportCreator : XrmUserBase
    {
        public SimsReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<DataModel.Dapaz.SimsReports.SimsCallData> CreateSimsReport(DateTime date)
        {
            List<DataModel.Dapaz.SimsReports.SimsCallData> retVal = new List<NoProblem.Core.DataModel.Dapaz.SimsReports.SimsCallData>();

            DataAccessLayer.VnRequest dal = new NoProblem.Core.DataAccessLayer.VnRequest(XrmDataContext);
            DataAccessLayer.VnRequest.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_vnrequest>.SqlParametersList();
            parameters.Add("@date", date);
            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                DataModel.Dapaz.SimsReports.SimsCallData data = new NoProblem.Core.DataModel.Dapaz.SimsReports.SimsCallData();
                data.AccountNumberZap = Convert.ToString(row["new_bizid"]);
                data.AdvertiserCallStart = (DateTime)row["new_calldatetimelocal"];
                data.AdvertiserCallEnd = row["new_calldatetimeendlocal"] != DBNull.Value ? (DateTime)row["new_calldatetimeendlocal"] : data.AdvertiserCallStart;
                data.AdvertiserPhone = Convert.ToString(row["new_target"]);
                data.CallCLINo = Convert.ToString(row["new_callerid"]);
                data.CallId = Convert.ToString(row["new_sessionid"]);
                data.CallStatus = CalculateStatus(Convert.ToString(row["new_callstatus"]));
                data.CategoryCode = "generic";
                data.EntryCreatedOn = data.AdvertiserCallStart.Value;
                data.RequestId = (Guid)row["new_vnrequestid"];
                data.VirtualNumber = Convert.ToString(row["new_virtualnumber"]);
                retVal.Add(data);
            }
            return retVal;
        }

        private string CalculateStatus(string callStatus)
        {
            if (callStatus == "ANSWER")
            {
                return DataModel.Dapaz.SimsReports.SimsCallData.CallStatusOptions.ANSWER;
            }
            return DataModel.Dapaz.SimsReports.SimsCallData.CallStatusOptions.NO_ANSWER;
        }

//        private const string query =
//            @"select distinct
//acc.new_bizid
//, req.new_ivrinbound
//, req.new_ivrcallerid
//, req.new_target1
//, req.new_mappingstatus
//, sta.new_ivrstatus
//, req.new_ivrsession
//, req.createdon as starttime
//, sta.createdon as endtime
//, req.new_vnrequestid
//from new_vnrequest req with(nolock)
//left join new_vnstatus sta with(nolock) on sta.new_ivrsession = req.new_ivrsession
//left join account acc with(nolock) on acc.accountid = req.new_accountid
//where req.new_calltype = 'GoldenNumberAdvertiser'
//and 
//req.createdon >= dateadd(day,datediff(day,0,@date),0)
//AND req.createdon < dateadd(day,datediff(day,-1,@date),0)
//order by req.createdon desc";

        private const string query =
            @"
select distinct
min(new_bizid) new_bizid
,min(new_virtualnumber) new_virtualnumber
,min(new_callerid) new_callerid
,min(new_target) new_target
,min(new_callstatus) new_callstatus
,min(new_sessionid) new_sessionid
,min(new_calldatetimelocal) new_calldatetimelocal
,min(new_calldatetimeendlocal) new_calldatetimeendlocal
,new_vnrequestid
from new_callforreport
where deletionstatecode = 0
and new_calldatetimelocal between @date and dateadd(day, 1, @date)
group by new_vnrequestid, new_calldatetimelocal
order by new_calldatetimelocal desc
";
    }
}
