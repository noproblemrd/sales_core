﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Threading;

namespace NoProblem.Core.BusinessLogic.Dapaz.PpaOnlineSync
{
    class Syncher : Runnable
    {
        Guid supplierId;
        DataModel.Xrm.account supplier = null;
        string oldMekomonim = null;
        DataModel.Dapaz.PpaOnlineSync.SyncObject data;
        static DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
        private const string logoUrlBase = "http://gallery.ppcnp.com/Gallery/images/logos/{0}.jpg"; //ConfigurationManager.AppSettings.Get("LogoUrlBase");

        private Syncher()
            : base(System.Threading.ThreadPriority.Lowest)
        {
            data = new DataModel.Dapaz.PpaOnlineSync.SyncObject(SyncConfiguration.Password);
        }

        public Syncher(Guid supplierId, string oldMekomonim)
            : this()
        {
            this.oldMekomonim = oldMekomonim;
            this.supplierId = supplierId;
        }

        public Syncher(Guid supplierId)
            : this(supplierId, null)
        {
        }

        /// <summary>
        /// Executes the sync on the same thread. It doesn't check if feature is enbled, does it on anyways. To be used with first time sync of all data.
        /// </summary>
        internal void Execute()
        {
            if (supplier == null)
            {
                DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(XrmDataContext);
                supplier = dal.Retrieve(supplierId);
            }
            if (supplier.new_dapazstatus.HasValue
                && supplier.new_dapazstatus != (int)DataModel.Xrm.account.DapazStatus.PPA
                && supplier.new_dapazstatus != (int)DataModel.Xrm.account.DapazStatus.RarahQuote)
                return;

            CreateSyncObject();
            //string json = Newtonsoft.Json.JsonConvert.SerializeObject(data);

            string responseContent = Send();
            bool isSuccess = responseContent.Contains("Success");
            Log(responseContent, isSuccess);
        }

        protected override void Run()
        {
            if (!SyncConfiguration.IsEnabled)
                return;
            Thread.Sleep(15 * 1000);
            Execute();
        }        

        private string Send()
        {
            RestSharp.RestClient client = new RestSharp.RestClient(SyncConfiguration.URL);
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.RequestFormat = RestSharp.DataFormat.Json;
            request.JsonSerializer = new Utils.RestSharpCustomJsonSerializer();
            request.AddBody(data);
            var response = client.Execute(request);
            return response.Content;
        }

        private void Log(string responseContent, bool isSuccess)
        {
            var collection = DataAccessLayer.MongoDBHelper.Instance.GetCollection("ppa_online_sync_service_log");
            MongoDB.Bson.BsonDocument requestDoc
                = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<MongoDB.Bson.BsonDocument>(Newtonsoft.Json.JsonConvert.SerializeObject(data));
            MongoDB.Bson.BsonDocument doc = new MongoDB.Bson.BsonDocument(new MongoDB.Bson.BsonElement("request", requestDoc)).Add("response", responseContent).Add("CreatedOn", DateTime.Now).Add("AccountId", supplierId).Add("isSuccess", isSuccess);
            collection.Insert(doc);
        }

        private void CreateSyncObject()
        {
            data.CustomerData.AccountNumberDapaz = supplier.new_bizid;
            data.CustomerData.AccountNumberNP = supplier.accountnumber;
            data.CustomerData.Email = supplier.emailaddress1;
            data.CustomerData.Website = supplier.websiteurl;
            data.CustomerData.IsActive = supplier.new_status == (int)DataModel.Xrm.account.SupplierStatus.Approved;
            data.CustomerData.LogoUrl = String.Format(logoUrlBase, supplierId);
            data.CustomerData.CountCalls = GetCallCount();
            HandleMekomonim();
            HandleHeadings();
            data.CustomerData.SundayFrom = HandleDay(supplier.new_sundayavailablefrom);
            data.CustomerData.SundayTo = HandleDay(supplier.new_sundayavailableto);
            data.CustomerData.MondayFrom = HandleDay(supplier.new_mondayavailablefrom);
            data.CustomerData.MondayTo = HandleDay(supplier.new_mondayavailableto);
            data.CustomerData.TuesdayFrom = HandleDay(supplier.new_tuesdayavailablefrom);
            data.CustomerData.TuesdayTo = HandleDay(supplier.new_tuesdayavailableto);
            data.CustomerData.WednesdayFrom = HandleDay(supplier.new_wednesdayavailablefrom);
            data.CustomerData.WednesdayTo = HandleDay(supplier.new_wednesdayavailableto);
            data.CustomerData.ThursdayFrom = HandleDay(supplier.new_thursdayavailablefrom);
            data.CustomerData.ThursdayTo = HandleDay(supplier.new_thursdayavailableto);
            data.CustomerData.FridayFrom = HandleDay(supplier.new_fridayavailablefrom);
            data.CustomerData.FridayTo = HandleDay(supplier.new_fridayavailableto);
            data.CustomerData.SaturdayFrom = HandleDay(supplier.new_saturdayavailablefrom);
            data.CustomerData.SaturdayTo = HandleDay(supplier.new_saturdayavailableto);
            data.CustomerData.UnavailableFrom = supplier.new_unavailablelocalfrom;
            data.CustomerData.UnavailableTo = supplier.new_unavailablelocalto;
        }

        private string HandleDay(string time)
        {
            if (time == "1:00" || time == "1:01")
            {
                return null;
            }
            return time;
        }

        private void HandleHeadings()
        {
            List<DataModel.Dapaz.PpaOnlineSync.AccountSyncData.CategoryData> headings = new List<DataModel.Dapaz.PpaOnlineSync.AccountSyncData.CategoryData>();
            foreach (var ae in supplier.new_account_new_accountexpertise)
            {
                List<DataModel.Dapaz.PpaOnlineSync.AccountSyncData.VirtualNumberData> virtualNumbers = new List<DataModel.Dapaz.PpaOnlineSync.AccountSyncData.VirtualNumberData>();
                foreach (var dn in ae.new_new_accountexpertise_new_directnumber)
                {
                    var origin = dn.new_new_origin_new_directnumber;
                    if (origin.new_isdapazmainorigin.HasValue && origin.new_isdapazmainorigin.Value)
                    {
                        virtualNumbers.Add(
                            new DataModel.Dapaz.PpaOnlineSync.AccountSyncData.VirtualNumberData
                            {
                                Number = dn.new_number,
                                OriginCode = origin.new_code
                            }
                            );
                    }
                }
                headings.Add(
                    new DataModel.Dapaz.PpaOnlineSync.AccountSyncData.CategoryData
                    {
                        CallPrice = ae.new_incidentprice.HasValue ? ae.new_incidentprice.Value : 0,
                        IsActive = ae.statecode == DataAccessLayer.AccountExpertise.ACTIVE,
                        Code = ae.new_new_primaryexpertise_new_accountexpertise.new_englishname,
                        VirtualNumbers = virtualNumbers
                    }
                    );
            }
            data.CustomerData.Categories = headings;
        }

        private int GetCallCount()
        {
            DataAccessLayer.Generic.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            object scalar = dal.ExecuteScalar(callCountQuery, parameters);
            int retVal = 0;
            if (scalar != null && scalar != DBNull.Value)
            {
                retVal = (int)scalar;
            }
            return retVal;
        }

        private void HandleMekomonim()
        {
            string mekomonimStr = supplier.new_mekomonim;
            if (String.IsNullOrEmpty(mekomonimStr))
            {
                Dapaz.PotentialStats.SupplierMekomonimUpdater updater = new NoProblem.Core.BusinessLogic.Dapaz.PotentialStats.SupplierMekomonimUpdater(supplier.accountid, null, oldMekomonim);
                mekomonimStr = updater.UpdateMekomonim(false);
                if (mekomonimStr == null)
                    mekomonimStr = String.Empty;
            }
            string[] split = mekomonimStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            List<DataModel.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData> mekomonimDataLIst = new List<DataModel.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData>();
            foreach (var item in split)
            {
                DataModel.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData mekData = new DataModel.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData();
                mekData.IsActive = true;
                mekData.MekomonId = item;
                mekomonimDataLIst.Add(mekData);
            }
            if (!String.IsNullOrEmpty(oldMekomonim))
            {
                string[] oldSplit = oldMekomonim.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in oldSplit)
                {
                    if (!split.Contains<String>(item))
                    {
                        DataModel.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData mekData = new DataModel.Dapaz.PpaOnlineSync.AccountSyncData.MekomonData();
                        mekData.IsActive = false;
                        mekData.MekomonId = item;
                        mekomonimDataLIst.Add(mekData);
                    }
                }
            }
            data.CustomerData.Mekomonim = mekomonimDataLIst;
        }

        private const string callCountQuery =
            @"
select count(*)
from new_incidentaccount with(nolock)
where new_accountid = @supplierId
and new_tocharge = 1
and createdon >= dateadd(month, datediff(month, 0, getdate()),0)
";
    }
}
