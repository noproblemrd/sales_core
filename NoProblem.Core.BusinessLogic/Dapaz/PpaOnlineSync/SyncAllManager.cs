﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Dapaz.PpaOnlineSync
{
    public class SyncAllManager : Runnable
    {
        private string password;
        private const string realPassword = "Aa123456";

        public SyncAllManager(string password)
        {
            this.password = password;
        }

        protected override void Run()
        {
            if (password != realPassword)
            {
                LogUtils.MyHandle.WriteToLog("Sync All not started: Wrong password provided!!!");
                return;
            }
            LogUtils.MyHandle.WriteToLog("Sync All started.");
            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            var reader = dal.ExecuteReader(query);
            int total = reader.Count;
            int count = 1;
            foreach (var row in reader)
            {
                try
                {
                    LogUtils.MyHandle.WriteToLog("Synching account {0} from {1}", count, total);
                    Syncher syncher = new Syncher((Guid)row["accountid"]);
                    syncher.Execute();
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception synching account with id {0}", row["accountid"]);
                }
                count++;
            }
            LogUtils.MyHandle.WriteToLog("Sync All ended.");
        }

        string query =
            @"
select accountid
from Account
where (New_DapazStatus = 1 or New_DapazStatus is null or New_DapazStatus = 4)
and deletionstatecode = 0
and new_status = 1
and New_BizId is not null
";
    }
}
