﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dapaz.PpaOnlineSync
{
    class SyncConfiguration
    {
        static DataAccessLayer.ConfigurationSettings configDal = new DataAccessLayer.ConfigurationSettings(null);

        public static bool IsEnabled
        {
            get
            {
                return configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PPA_ONLINE_SYNC_ENABLED) == 1.ToString();
            }
        }

        public static string URL
        {
            get
            {
                return configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PPA_ONLINE_SYNC_URL);
            }
        }

        public static string Password
        {
            get
            {
                return configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PPA_ONLINE_SYNC_PASSWORD);
            }
        }
    }
}
