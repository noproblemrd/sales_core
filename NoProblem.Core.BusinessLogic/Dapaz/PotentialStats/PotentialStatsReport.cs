﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Dapaz.PotentialStats;

namespace NoProblem.Core.BusinessLogic.Dapaz.PotentialStats
{
    public class PotentialStatsReport : XrmUserBase
    {
        public PotentialStatsReport(DataModel.Xrm.DataContext xmrDataContext)
            : base(xmrDataContext)
        {
            mekomonimHashTable = new System.Collections.Hashtable();
        }

        private System.Collections.Hashtable mekomonimHashTable;

        public List<PotentialStatRow> CreateReport()
        {
            List<Guid> retrieved = new List<Guid>();
            List<PotentialStatRow> retVal = new List<PotentialStatRow>();
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var reader = dal.ExecuteReader(getSupplierQuery);
            foreach (var row in reader)
            {
                PotentialStatRow data = new PotentialStatRow();

                data.CustomerStatus = GetCustomerStatus(row["CustomerStatus"]);
                data.CutomerNo = Convert.ToString(row["CustomerNo"]);
                if (row["SimsLimit"] != DBNull.Value)
                {
                    data.SimsLimit = (int)row["SimsLimit"];
                }
                if (row["SimsLimitEndDate"] != DBNull.Value)
                {
                    data.SimsLimitEndDate = (DateTime)row["SimsLimitEndDate"];
                }
                if (row["SimsLimitStartDate"] != DBNull.Value)
                {
                    data.SimsLimitStartDate = (DateTime)row["SimsLimitStartDate"];
                }
                data.SimsMedia = Convert.ToString(row["SimsMedia"]);
                data.SimsPhoneLocal = data.CustomerStatus != PotentialStatRow.CustomerStatuses.Gold ? GetMekomonim(Convert.ToString(row["SimsPhoneLocal"]), (Guid)row["accountid"]) : string.Empty;
                data.SimsPhoneNumber = Convert.ToString(row["SimsPhoneNumber"]);
                data.SimsPhoneSegment = Convert.ToString(row["SimsPhoneSegment"]);
                data.SimsStartDate = (DateTime)row["SimsStartDate"];

                retVal.Add(data);
                retrieved.Add((Guid)row["accountid"]);
            }
            UpdateRetrieved(retrieved, dal);
            return retVal;
        }

        private void UpdateRetrieved(List<Guid> retrieved, DataAccessLayer.AccountRepository dal)
        {
            if (retrieved.Count == 0)
                return;
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < retrieved.Count; i++)
            {
                builder.Append("'").Append(retrieved[i]).Append("'");
                if (i < retrieved.Count - 1)
                {
                    builder.Append(",");
                }
            }
            dal.ExecuteNonQuery(string.Format(updateRetrievedSql, builder.ToString()));
        }

        private string GetMekomonim(string mekomonim, Guid supplierId)
        {
            if (!string.IsNullOrEmpty(mekomonim))
            {
                return mekomonim;
            }
            string retVal;
            object valueObj = mekomonimHashTable[supplierId];
            if (valueObj == null)
            {
                List<string> mekomonimList = CalculateMekomonim(supplierId);
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < mekomonimList.Count; i++)
                {
                    builder.Append(mekomonimList[i]);
                    if (i < mekomonimList.Count - 1)
                    {
                        builder.Append(";");
                    }
                }
                retVal = builder.ToString();
                mekomonimHashTable.Add(supplierId, retVal);
                UpdateSupplier(supplierId, retVal);
            }
            else
            {
                retVal = (string)valueObj;
            }
            return retVal;
        }

        private void UpdateSupplier(Guid supplierId, string mekomonim)
        {
            SupplierMekomonimUpdater updater = new SupplierMekomonimUpdater(supplierId, mekomonim, null);
            updater.UpdateMekomonim();
        }

        private List<string> CalculateMekomonim(Guid supplierId)
        {
            MekomonimCalculator calculator = new MekomonimCalculator(XrmDataContext);
            return calculator.CalculateMekomonim(supplierId);
        }

        private string GetCustomerStatus(object customerStatusDb)
        {
            string retVal = string.Empty;
            if (customerStatusDb == DBNull.Value)
            {
                retVal = PotentialStatRow.CustomerStatuses.PPA;
            }
            else
            {
                int dpzStatus = (int)customerStatusDb;
                if (dpzStatus == 2)
                {
                    retVal = PotentialStatRow.CustomerStatuses.Gold;
                }
                else if (dpzStatus == 4)
                {
                    retVal = PotentialStatRow.CustomerStatuses.RarahQuote;
                }
            }
            return retVal;
        }

        private const string updateRetrievedSql =
            @"
update new_zgpotentialstats
set new_retrievedon = getdate()
where new_accountid in
(
{0}
)
and new_retrievedon is null
";

        private const string getSupplierQuery =
            @"
declare @dapazOrigin uniqueidentifier
select top 1 @dapazOrigin = new_originid from new_origin with(Nolock) where DeletionStateCode = 0 and New_IsDapazMainOrigin = 1

select 
acc.accountid
, acc.new_bizid CustomerNo
, acc.New_DapazStatus CustomerStatus
, acc.new_mekomonim SimsPhoneLocal
, dn.new_originidName SimsMedia
, dn.New_number SimsPhoneNumber
, pe.New_englishname SimsPhoneSegment
, acc.new_rarahquotestart SimsLimit
, acc.new_rarahquotestartdate SimsLimitStartDate
, acc.new_rarahquoteenddate SimsLimitEndDate
, dn.ModifiedOn SimsStartDate
from
Account acc with(nolock)
join new_accountexpertise ae with(nolock) on acc.AccountId = ae.new_accountid
join New_directnumber dn with(Nolock) on ae.New_accountexpertiseId = dn.new_accountexpertiseid
join new_primaryexpertise pe with(nolock) on pe.New_primaryexpertiseId = ae.new_primaryexpertiseid
where
acc.DeletionStateCode = 0
and dn.DeletionStateCode = 0
and ae.DeletionStateCode = 0
and ae.statecode = 0 
--and New_BizId is not null
and dn.new_originid = @dapazOrigin
and acc.accountid
in(
select new_accountid
from new_zgpotentialstats with(nolock)
where deletionstatecode = 0
and new_retrievedon is null
)
";
    }
}
