﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;
using System.Configuration;

namespace NoProblem.Core.BusinessLogic.Dapaz.PotentialStats
{
    internal class PotentialStatsInserter : XrmUserBase
    {
        private static bool enabled = ConfigurationManager.AppSettings["SiteId"] == "np_israel" || ConfigurationManager.AppSettings["SiteId"] == "2" ||
            ConfigurationManager.AppSettings["SiteId"] == "1" || ConfigurationManager.AppSettings["SiteId"] == "IsraelQA";

        public PotentialStatsInserter(Guid supplierId)
            : base(null)
        {
            if (enabled)
            {
                this.thread = new Thread(new ThreadStart(Run));
                this.supplierId = supplierId;
            }
        }


        private Thread thread;
        private Guid supplierId;

        public void InsertToStats()
        {
            if (enabled)
            {
                thread.Start();
            }
        }

        private void Run()
        {
            try
            {
                DataAccessLayer.ZgPotentialStats dal = new NoProblem.Core.DataAccessLayer.ZgPotentialStats(XrmDataContext);
                if (!dal.ExistsNonRetrieved(supplierId))
                {
                    DataModel.Xrm.new_zgpotentialstats entity = new NoProblem.Core.DataModel.Xrm.new_zgpotentialstats();
                    entity.new_accountid = supplierId;
                    dal.Create(entity);
                    XrmDataContext.SaveChanges();
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in PotentialStatsInserter.Run. supplierId = {0}", supplierId);
            }
        }
    }
}
