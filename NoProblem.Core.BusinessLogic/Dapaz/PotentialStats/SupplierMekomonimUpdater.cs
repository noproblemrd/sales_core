﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;
using System.Configuration;

namespace NoProblem.Core.BusinessLogic.Dapaz.PotentialStats
{
    internal class SupplierMekomonimUpdater : XrmUserBase
    {
        private static bool enabled = ConfigurationManager.AppSettings["SiteId"] == "IsraelQA" || ConfigurationManager.AppSettings["SiteId"] == "np_israel" || ConfigurationManager.AppSettings["SiteId"] == "2" || ConfigurationManager.AppSettings["SiteId"] == "1";

        private Thread thread;
        private Guid supplierId;
        private string mekomonim;
        private string oldMekomonim;

        public SupplierMekomonimUpdater(Guid supplierId, string mekomonim, string oldMekomonim)
            : base(null)
        {
            if (enabled)
            {
                this.thread = new Thread(new ThreadStart(Run));
                this.supplierId = supplierId;
                this.mekomonim = mekomonim;
                this.oldMekomonim = oldMekomonim;
            }
        }

        public void UpdateMekomonim()
        {
            UpdateMekomonim(true);
        }

        public string UpdateMekomonim(bool doAsync)
        {
            if (enabled)
            {
                if (doAsync)
                {
                    thread.Start();
                }
                else
                {
                    Run();
                }
            }
            return mekomonim;
        }

        private void Run()
        {
            try
            {
                if (string.IsNullOrEmpty(mekomonim))
                {
                    MekomonimCalculator calculator = new MekomonimCalculator(XrmDataContext);
                    List<string> mekomonimList = calculator.CalculateMekomonim(supplierId);
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < mekomonimList.Count; i++)
                    {
                        builder.Append(mekomonimList[i]);
                        if (i < mekomonimList.Count - 1)
                        {
                            builder.Append(";");
                        }
                    }
                    mekomonim = builder.ToString();
                }
                Update();
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SupplierMekomonimUpdater.Run. SupplierId = {0}, mekomonim = {1}", supplierId, mekomonim);
            }
        }

        private void Update()
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account(XrmDataContext);
            supplier.accountid = supplierId;
            if (mekomonim.Length >= 4000)
                mekomonim = mekomonim.Substring(0, 3999);
            supplier.new_mekomonim = mekomonim;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }
    }
}
