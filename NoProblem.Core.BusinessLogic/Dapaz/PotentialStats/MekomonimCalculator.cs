﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.Dapaz.PotentialStats
{
    internal class MekomonimCalculator : XrmUserBase
    {
        private const int mekomonimLevel = 2;

        public MekomonimCalculator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<string> CalculateMekomonim(Guid supplierId)
        {
            List<string> retVal = new List<string>();
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
            parameters.Add("@supplierId", supplierId);
            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                string code = Convert.ToString(row["new_code"]);
                retVal.Add(code);
            }
            return retVal;
        }

        private const string query =
            @"
select re2.new_code
from new_regionaccountexpertise ra with(nolock)
join new_region re with(nolock) on ra.new_regionid = re.new_regionid
join new_region re2 with(nolock) on re.new_regionid = re2.new_parentregionid
left join new_regionaccountexpertise ra2 with(nolock) on ra2.new_regionid = re2.new_regionid 
and ra2.new_accountid = @supplierId and ra2.DeletionStateCode = 0
where ra.deletionstatecode = 0
and ra.new_accountid = @supplierId
and re.new_level = 1
and ra.new_regionstate in (1, 3)
and ra2.new_regionstate is null 
";

    }
}
