﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Dapaz;

namespace NoProblem.Core.BusinessLogic.Dapaz
{
    internal class DapazStatusChanger : XrmUserBase
    {
        public DapazStatusChanger(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public void ChangeStatus(DataModel.Xrm.account supplier, DapazStatusChangeOptions newStatus)
        {
            switch (newStatus)
            {
                case DapazStatusChangeOptions.PPA:
                    TurnSupplierToPPA(supplier);
                    break;
                case DapazStatusChangeOptions.Rarah:
                    TurnSupplierToRarah(supplier);
                    break;
                case DapazStatusChangeOptions.RarahGold:
                    TurnSupplierToRarahGold(supplier);
                    break;
                case DapazStatusChangeOptions.RarahQuote:
                    TurnSupplierToRarahQuote(supplier);
                    break;
                case DapazStatusChangeOptions.PPAHold:
                    TurnSupplierToPPAHold(supplier);
                    break;
            }
        }

        private void TurnSupplierToRarahQuote(NoProblem.Core.DataModel.Xrm.account supplier)
        {
            if (!supplier.new_dapazstatus.HasValue
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.Rarah
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.RarahGold
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPA
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPAHold)
            {
                if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.Rarah
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.RarahGold)
                {
                    DirectNumbersAssigner assigner = new DirectNumbersAssigner();
                    assigner.DetachAllDirectNumbersFromSupplier(supplier.accountid, true);
                    DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                    DataModel.Xrm.new_accountexpertise genericAccountExpertise = accExpDal.GetByAccountAndPrimaryExpertise(supplier.accountid, GetGenericHeadingId());
                    if (genericAccountExpertise != null)
                    {
                        accExpDal.SetState(genericAccountExpertise, false);
                    }
                }
                if (!supplier.new_rarahquoteleft.HasValue || supplier.new_rarahquoteleft.Value <= 0)
                {
                    supplier.new_rarahquoteleft = 5;
                    supplier.new_rarahquotestartdate = DateTime.Now;
                    supplier.new_rarahquotestart = 5;
                }
                if (!supplier.new_rarahquotestart.HasValue || supplier.new_rarahquotestart.Value <= 0)
                {
                    supplier.new_rarahquotestart = supplier.new_rarahquoteleft;
                }
                if (!supplier.new_rarahquotestartdate.HasValue)
                {
                    supplier.new_rarahquotestartdate = DateTime.Now;
                }
                supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.RarahQuote;
                supplier.new_disableautodirectnumbers = false;
                supplier.new_recordcalls = true;
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                dal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
        }

        private void TurnSupplierToRarahGold(NoProblem.Core.DataModel.Xrm.account supplier)
        {
            if (!supplier.new_dapazstatus.HasValue
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.RarahQuote
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPA
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPAHold)
            {
                DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
                var accountExpertises = accExpDal.GetAllByAccount(supplier.accountid);
                bool first = true;
                DataModel.Xrm.new_directnumber dnToMove = null;
                for (int i = 0; i < accountExpertises.Count(); i++)
                {
                    var dns = dnDal.GetAllDirectNumbersOfAccountExpertise(accountExpertises.ElementAt(i).new_accountexpertiseid);
                    foreach (var dn in dns)
                    {
                        if (first)
                        {
                            dnToMove = dn;
                            first = false;
                        }
                        else
                        {
                            dn.new_accountexpertiseid = null;
                            dn.new_originid = null;
                            dn.new_frozendate = DateTime.Now;
                            dnDal.Update(dn);
                        }
                    }
                    XrmDataContext.SaveChanges();
                    accExpDal.SetState(accountExpertises.ElementAt(i), false);
                }
                if (dnToMove == null)
                {
                    supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.Rarah;
                }
                else
                {
                    supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.RarahGold;
                    DataModel.Xrm.new_accountexpertise genericAccountExpertise = accExpDal.GetByAccountAndPrimaryExpertise(supplier.accountid, GetGenericHeadingId());
                    if (genericAccountExpertise == null)
                    {
                        genericAccountExpertise = CreateAccountExpertise(supplier, accExpDal);
                    }
                    else if (genericAccountExpertise.statecode != DataAccessLayer.AccountExpertise.ACTIVE)
                    {
                        accExpDal.SetState(genericAccountExpertise, true);
                    }
                    dnToMove.new_accountexpertiseid = genericAccountExpertise.new_accountexpertiseid;
                    dnToMove.new_originid = GetMainDapazOriginId();
                    dnDal.Update(dnToMove);
                    XrmDataContext.SaveChanges();
                }
                supplier.new_disableautodirectnumbers = true;
                supplier.new_donotsendsms = true;
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                dal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
        }

        private DataModel.Xrm.new_accountexpertise CreateAccountExpertise(DataModel.Xrm.account supplier, DataAccessLayer.AccountExpertise accountExpertiseDal)
        {
            DataModel.Xrm.new_accountexpertise genericAccountExpertise = new NoProblem.Core.DataModel.Xrm.new_accountexpertise();
            genericAccountExpertise.new_accountid = supplier.accountid;
            genericAccountExpertise.new_primaryexpertiseid = GetGenericHeadingId();
            genericAccountExpertise.new_incidentprice = 0;
            accountExpertiseDal.Create(genericAccountExpertise);
            XrmDataContext.SaveChanges();
            return genericAccountExpertise;
        }

        private Guid GetMainDapazOriginId()
        {
            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            Guid mainDapazOrigin = originDal.GetMainDapazOriginId();
            if (mainDapazOrigin == Guid.Empty)
            {
                throw new Exception("Main dapaz origin not configured.");
            }
            return mainDapazOrigin;
        }

        private Guid GetGenericHeadingId()
        {
            DataAccessLayer.PrimaryExpertise peDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            Guid genericHeadingId = peDal.GetGenericHeadingId();
            if (genericHeadingId == Guid.Empty)
            {
                throw new Exception("Generic heading is not configured in the system.");
            }
            return genericHeadingId;
        }

        private void TurnSupplierToRarah(NoProblem.Core.DataModel.Xrm.account supplier)
        {
            if (!supplier.new_dapazstatus.HasValue
                || supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.PPA
                || supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.RarahQuote
                || supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.PPAHold)
            {
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                DirectNumbersAssigner assigner = new DirectNumbersAssigner();
                assigner.DetachAllDirectNumbersFromSupplier(supplier.accountid, true);
                DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                foreach (var item in accExpDal.GetActiveByAccount(supplier.accountid))
                {
                    accExpDal.SetState(item, false);
                }
                supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.Rarah;
                supplier.new_disableautodirectnumbers = true;
                dal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
        }

        private void TurnSupplierToPPA(DataModel.Xrm.account supplier)
        {
            if (!supplier.new_dapazstatus.HasValue
                || supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.PPA)
            {
                return;
            }
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.PPA;
            supplier.new_disableautodirectnumbers = false; 
            supplier.new_isexemptofppamonthlypayment = true;
            supplier.new_recordcalls = true;
            if (supplier.new_dapazstatus != (int)DataModel.Xrm.account.DapazStatus.RarahQuote)
            {
                supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Candidate;
                var genericHeadingId = GetGenericHeadingId();
                DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                var genericAccExp = accExpDal.GetByAccountAndPrimaryExpertise(supplier.accountid, genericHeadingId);
                if (genericAccExp != null)
                {
                    accExpDal.SetState(genericAccExp, false);
                }
                DirectNumbersAssigner assigner = new DirectNumbersAssigner();
                assigner.DetachAllDirectNumbersFromSupplier(supplier.accountid, true);
            }
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        private void TurnSupplierToPPAHold(DataModel.Xrm.account supplier)
        {
            if (supplier.new_dapazstatus.HasValue
               && (supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.Fix
                || supplier.new_dapazstatus.Value == (int)DataModel.Xrm.account.DapazStatus.FixGold))
            {
                DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
                supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.PPAHold;
                supplier.new_disableautodirectnumbers = false;
                supplier.new_recordcalls = true;
                supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Candidate;
                var genericHeadingId = GetGenericHeadingId();
                DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                var genericAccExp = accExpDal.GetByAccountAndPrimaryExpertise(supplier.accountid, genericHeadingId);
                if (genericAccExp != null)
                {
                    accExpDal.SetState(genericAccExp, false);
                }
                DirectNumbersAssigner assigner = new DirectNumbersAssigner();
                assigner.DetachAllDirectNumbersFromSupplier(supplier.accountid, true);
                dal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
        }
    }
}
