﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NoProblem.Core.DataModel.Dapaz.UsersSync;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Dapaz.UsersSync
{
    public class UsersSyncManager : XrmUserBase
    {
        private UsersSyncManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {
            dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            notifier = new Notifications(XrmDataContext);
            config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            if (System.Configuration.ConfigurationManager.AppSettings["SiteId"].ToLower() != "np_israel")
            {
                filesUrlToUse = filesUrlTest;
            }
            else
            {
                filesUrlToUse = filesUrl;
            }
        }

        private static Timer timer;
        private DataAccessLayer.AccountRepository dal;
        private DataAccessLayer.ConfigurationSettings config;
        private Notifications notifier;
        private const string filesUrl = "ftp://ftp.d.co.il/NP_USERS_AUTH_LEVEL_{0}.csv";
        private const string filesUrlTest = "ftp://ftp.d.co.il/test/NP_USERS_AUTH_LEVEL_{0}.csv";
        private const string ftpUser = "npusers";
        private const string ftpPassword = "ZAPsynctoNP!";
        private string filesUrlToUse;

        public static void SetTimerAtAppStart()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            if (configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.DAPAZ_USERS_SYNC_ENABLED) != 1.ToString())
                return;
            TimeSpan firstStart = GetFirstStartSpan();
            timer = new Timer(
                TimerCallBack,
                null,
                firstStart,
                new TimeSpan(1, 0, 0, 0));
        }

        private static TimeSpan GetFirstStartSpan()
        {
            DateTime fourAm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 4, 0, 0);
            if (DateTime.Now.Hour >= 4)
            {
                fourAm = fourAm.AddDays(1);
            }
            return fourAm - DateTime.Now;
        }

        public static void StartSyncNow()
        {
            UsersSyncManager instance = new UsersSyncManager(null);
            instance.StartSync();
        }

        private static void TimerCallBack(object obj)
        {
            UsersSyncManager instance = new UsersSyncManager(null);
            instance.StartSync();
        }

        private void StartSync()
        {
            try
            {
                if (config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.DAPAZ_USERS_SYNC_ENABLED) != 1.ToString())
                    return;
                List<UserContainer> users = GetUsersList();
                List<CurrentUserContainer> currentUsers = GetCurrentUsers();
                foreach (var user in users)
                {
                    bool found = false;
                    foreach (var curr in currentUsers)
                    {
                        if (user.Id == curr.UserBizId)
                        {
                            found = true;
                            if (!curr.IsActive)
                            {
                                ActiveOrDeactivateUser(curr, true);
                            }
                            break;
                        }
                    }
                    if (!found)
                    {
                        CreateNewUser(user);
                    }
                }

                var activeCurrent = currentUsers.Where(x => x.IsActive);

                foreach (var active in activeCurrent)
                {
                    bool found = false;
                    foreach (var user in users)
                    {
                        if (user.Id == active.UserBizId)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        ActiveOrDeactivateUser(active, false);
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Dapaz Users Sync");
            }
        }

        private void CreateNewUser(UserContainer user)
        {
            try
            {
                DataModel.Xrm.account account = new NoProblem.Core.DataModel.Xrm.account();
                account.name = user.Name;
                account.new_userbizid = user.Id;
                account.emailaddress1 = user.Email;
                account.new_securitylevel = user.SecurityLevel;
                account.new_password = Utils.LoginUtils.GenerateNewPassword();
                account.new_status = 1;
                dal.Create(account);
                XrmDataContext.SaveChanges();
                string subject = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.DAPAZ_USERS_SYNC_EMAIL_SUBJECT);
                string body = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.DAPAZ_USERS_SYNC_EMAIL_BODY);
                body = string.Format(body, user.Email, user.Name, account.new_password);
                notifier.SendEmail(user.Email, body, subject, config);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Excetption in NoProblem.Core.BusinessLogic.Dapaz.UsersSync.UsersSyncManager.CreateNewUser; name = {0}, email = {1}, userBizId = {2}, securityLevel = {3}", user.Name, user.Email, user.Id, user.SecurityLevel);
            }
        }

        private void ActiveOrDeactivateUser(CurrentUserContainer curr, bool activate)
        {
            try
            {
                DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
                parameters.Add("@status", activate ? 1 : 3);
                parameters.Add("@id", curr.Id);
                dal.ExecuteNonQuery(queryForActivateOrDeactivateUser, parameters);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Excetption in NoProblem.Core.BusinessLogic.Dapaz.UsersSync.UsersSyncManager.ActiveOrDeactivateUser; id = {0}, isActive = {1}, userBizId = {2}, acitvate = {3}", curr.Id, curr.IsActive, curr.UserBizId, activate);
            }
        }

        private List<CurrentUserContainer> GetCurrentUsers()
        {
            var reader = dal.ExecuteReader(queryForCurrentUsers);
            List<CurrentUserContainer> currentUsers = new List<CurrentUserContainer>();
            foreach (var row in reader)
            {
                currentUsers.Add(
                    new CurrentUserContainer()
                    {
                        Id = (Guid)row["accountid"],
                        IsActive = (int)row["new_status"] == 1,
                        UserBizId = Convert.ToString(row["new_userbizid"])
                    });
            }
            return currentUsers;
        }

        private List<UserContainer> GetUsersList()
        {
            List<UserContainer> retVal = new List<UserContainer>();
            for (int i = 1; i <= 5; i++)
            {
                List<Utils.Csv.CsvRow> rows = GetRowsFromFile(i);
                if (rows.Count > 0)
                {
                    rows.RemoveAt(0);
                    foreach (var row in rows)
                    {
                        UserContainer user = new UserContainer();
                        user.Name = row[0];
                        user.Email = row[1];
                        user.Id = row[2];
                        user.SecurityLevel = i;
                        retVal.Add(user);
                    }
                }
            }
            return retVal;
        }

        private List<Utils.Csv.CsvRow> GetRowsFromFile(int securityLevel)
        {
            List<Utils.Csv.CsvRow> rows = new List<NoProblem.Core.BusinessLogic.Utils.Csv.CsvRow>();
            try
            {
                Utils.FtpFileDownloader downloader = new NoProblem.Core.BusinessLogic.Utils.FtpFileDownloader(ftpUser, ftpPassword);
                using (Stream stream = downloader.DownloadFile(string.Format(filesUrlToUse, securityLevel)))
                {
                    using (Utils.Csv.CsvFileReader csvReader = new NoProblem.Core.BusinessLogic.Utils.Csv.CsvFileReader(stream))
                    {
                        Utils.Csv.CsvRow row = new NoProblem.Core.BusinessLogic.Utils.Csv.CsvRow();
                        while (csvReader.ReadRow(row))
                        {
                            rows.Add(row);
                            row = new NoProblem.Core.BusinessLogic.Utils.Csv.CsvRow();
                        }
                    }
                }
            }
            catch (System.Net.WebException webExc)
            {
                if (webExc.Message != errorWhenFileDoesntExist)
                {
                    throw webExc;
                }
                //when a file doesn't exist a web exception will be thrown, it is not mandatory for files to exist so there is nothing to do with this exception.
            }
            return rows;
        }

        private const string errorWhenFileDoesntExist = "The remote server returned an error: (550) File unavailable (e.g., file not found, no access).";

        private const string queryForCurrentUsers =
            @"
select accountid, new_userbizid, new_status
from account with(nolock) where
deletionstatecode = 0
and new_securitylevel > 0
and isnull(new_isnoproblemuser, 0) = 0
and new_affiliateoriginid is null
";

        private const string queryForActivateOrDeactivateUser =
            @"
update accountextensionbase
set new_status = @status
where accountid = @id
";
    }
}
