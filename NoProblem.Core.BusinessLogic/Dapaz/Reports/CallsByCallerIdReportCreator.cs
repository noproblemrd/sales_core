﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Dapaz.Reports;

namespace NoProblem.Core.BusinessLogic.Dapaz.Reports
{
    public class CallsByCallerIdReportCreator : XrmUserBase
    {
        public CallsByCallerIdReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public DapazGroupedByCallerIdReportResponse CreateReport(DateTime fromDate, DateTime toDate, Guid supplierId, bool? callStatus, DataModel.Xrm.account.DapazStatus? dapazStatus)
        {
            FixDatesToFullDays(ref fromDate, ref toDate);
            DataAccessLayer.CallForReport dal = new NoProblem.Core.DataAccessLayer.CallForReport(XrmDataContext);
            DataAccessLayer.CallForReport.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_callforreport>.SqlParametersList();
            parameters.Add("@from", fromDate);
            parameters.Add("@to", toDate);
            StringBuilder query = new StringBuilder(
                @"
select
new_callerid callerId
,count(new_callforreportid) count
,sum(new_duration) durationTimeInSeconds
from new_callforreport with(nolock)
where deletionstatecode = 0
and new_calldatetimelocal between @from and @to
");
            if (callStatus.HasValue)
            {
                query.Append(" and isnull(new_issuccessfulcall, 0) = @callStatus ");
                parameters.Add("@callStatus", callStatus.Value);
            }
            if (supplierId != Guid.Empty)
            {
                query.Append(" and new_accountid = @supplierId ");
                parameters.Add("@supplierId", supplierId);
            }
            if (dapazStatus.HasValue)
            {
                query.Append(" and new_dapazstatus = @dapazStatus ");
                parameters.Add("@dapazStatus", (int)(dapazStatus.Value));
            }
            query.Append(" group by new_callerid order by count desc ");
            var reader = dal.ExecuteReader(query.ToString(), parameters);
            List<DapazGroupByCallerIdContainer> dataList = new List<DapazGroupByCallerIdContainer>();
            int callsDurationInSeconds = 0;
            foreach (var row in reader)
            {
                DapazGroupByCallerIdContainer data = new DapazGroupByCallerIdContainer();
                data.CallerId = Convert.ToString(row["callerId"]);
                data.Count = (int)row["count"];
                callsDurationInSeconds += (int)row["durationTimeInSeconds"];
                dataList.Add(data);
            }
            DapazGroupedByCallerIdReportResponse response = new DapazGroupedByCallerIdReportResponse();
            response.DataList = dataList;
            response.TotalCallers = dataList.Sum(x => x.Count);
            response.TotalMinutes = (int)Math.Round((double)callsDurationInSeconds / 60);
            response.AverageDurationInSeconds = response.TotalCallers == 0 ? 0 : callsDurationInSeconds / response.TotalCallers;
            return response;
        }
    }
}
