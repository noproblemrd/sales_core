﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Dapaz.Reports;

namespace NoProblem.Core.BusinessLogic.Dapaz.Reports
{
    public class CallsReportCreator : XrmUserBase
    {
        public CallsReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public DapazCallReportResponse CreateReport(DateTime fromDate, DateTime toDate, Guid supplierId, bool? callStatus, DataModel.Xrm.account.DapazStatus? dapazStatus)
        {
            DapazCallReportResponse response = new DapazCallReportResponse();
            FixDatesToFullDays(ref fromDate, ref toDate);
            DataAccessLayer.CallForReport dal = new NoProblem.Core.DataAccessLayer.CallForReport(XrmDataContext);
            DataAccessLayer.CallForReport.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_callforreport>.SqlParametersList();
            parameters.Add("@from", fromDate);
            parameters.Add("@to", toDate);
            StringBuilder query = new StringBuilder(
                @"
select distinct
min(new_accountname) new_accountname
,min(new_bizid) new_bizid
,min(new_calldatetimelocal) new_calldatetimelocal
,min(new_callerid) new_callerid
,min(new_virtualnumber) new_virtualnumber
,min(new_target) new_target
,min(new_duration) new_duration
,min(new_callstatus) new_callstatus
,new_accountid
,min(new_recordfilelocation) new_recordfilelocation
from new_callforreport with(nolock)
where deletionstatecode = 0
and new_calldatetimelocal between @from and @to
");
            if (callStatus.HasValue)
            {
                query.Append(" and isnull(new_issuccessfulcall, 0) = @callStatus ");
                parameters.Add("@callStatus", callStatus.Value);
            }
            if (supplierId != Guid.Empty)
            {
                query.Append(" and new_accountid = @supplierId ");
                parameters.Add("@supplierId", supplierId);
            }
            if (dapazStatus.HasValue)
            {
                query.Append(" and new_dapazstatus = @dapazStatus ");
                parameters.Add("@dapazStatus", (int)(dapazStatus.Value));
            }
            query.Append(" group by New_SessionId, new_accountid, new_calldatetimelocal order by new_calldatetimelocal desc ");
            var reader = dal.ExecuteReader(query.ToString(), parameters);
            List<DapazReportsCallContainer> callsList = new List<DapazReportsCallContainer>();
            foreach (var row in reader)
            {
                DapazReportsCallContainer data = new DapazReportsCallContainer();
                data.CallDateTime = (DateTime)row["new_calldatetimelocal"];
                data.CallDurationInSeconds = (int)row["new_duration"];
                data.CallerId = Convert.ToString(row["new_callerid"]);
                data.CallStatus = Convert.ToString(row["new_callstatus"]);
                if (row["new_accountid"] != DBNull.Value)
                {
                    data.SupplierId = (Guid)row["new_accountid"];
                    data.SupplierName = Convert.ToString(row["new_accountname"]);
                    data.BizId = Convert.ToString(row["new_bizid"]);
                }
                data.TargerNumber = Convert.ToString(row["new_target"]);
                data.VirtualNumber = Convert.ToString(row["new_virtualnumber"]);
                data.RecordFileLocation = Convert.ToString(row["new_recordfilelocation"]);
                callsList.Add(data);
            }
            response.CallsList = callsList;
            int averageCallSeconds = 0;
            int secondsSum = 0;
            if (callsList.Count > 0)
            {
                averageCallSeconds = (int)callsList.Average(x => x.CallDurationInSeconds);
                secondsSum = callsList.Sum(x => x.CallDurationInSeconds);
            }
            response.AverageCallTimeInSeconds = averageCallSeconds;
            response.NumberOfCallers = callsList.Count;
            response.NumberOfMinutes = (int)Math.Round((double)secondsSum / 60);
            return response;
        }
    }
}
