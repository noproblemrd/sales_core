﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;
using FoneApiWrapper.CallBacks;

namespace NoProblem.Core.BusinessLogic.Dapaz.Reports
{
    public class CallViewCreator : XrmUserBase
    {
        private static Timer executeTimer;
        private static volatile bool isRunning = false;
        private static object syncOjc = new object();

        private CallViewCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public static void SetTimerOnAppStart()
        {
            try
            {
                TimeSpan span = GetTimeSpanForNextExecute();
                executeTimer = new Timer(
                    TimerCallBack,
                    null,
                    span,
                    new TimeSpan(1, 0, 0)
                    );
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in BusinessLogic.Dapaz.CallViewCreator.ReportsSetTimerOnAppStart");
            }
        }

        private static TimeSpan GetTimeSpanForNextExecute()
        {
            DateTime nextTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 30, 0);
            if (DateTime.Now.Minute >= 30)
            {
                nextTime = nextTime.AddHours(1);
            }
            return nextTime - DateTime.Now;
        }

        private static void TimerCallBack(object obj)
        {
            try
            {
                //LogUtils.MyHandle.WriteToLog("BusinessLogic.Dapaz.Reports.CallViewCreator.TimerCallBack before lock. ThreadId = {0}", Thread.CurrentThread.ManagedThreadId);
                lock (syncOjc)
                {
                    //LogUtils.MyHandle.WriteToLog("BusinessLogic.Dapaz.Reports.CallViewCreator.TimerCallBack inside lock. ThreadId = {0}", Thread.CurrentThread.ManagedThreadId);
                    if (isRunning)
                    {
                        LogUtils.MyHandle.WriteToLog("BusinessLogic.Dapaz.Reports.CallViewCreator.TimerCallBack is already running -> exiting. ThreadId = {0}", Thread.CurrentThread.ManagedThreadId);
                        return;
                    }
                    isRunning = true;
                    CallViewCreator creator = new CallViewCreator(null);
                    creator.CreateCallsView();
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in BusinessLogic.Dapaz.Reports.CallViewCreator.TimerCallBack. ThreadId = {0}", Thread.CurrentThread.ManagedThreadId);
            }
            isRunning = false;
            //LogUtils.MyHandle.WriteToLog("BusinessLogic.Dapaz.Reports.CallViewCreator.TimerCallBack exiting after done. ThreadId = {0}", Thread.CurrentThread.ManagedThreadId);
        }

        private void CreateCallsView()
        {
            DataAccessLayer.CallForReport dal = new NoProblem.Core.DataAccessLayer.CallForReport(XrmDataContext);
            var reader = dal.ExecuteReader(getNewCallsQuery);
            foreach (var row in reader)
            {
                try
                {
                    DataModel.Xrm.new_callforreport call = CreateCallForReport(dal, row);
                    UpdateDoneCall(call.new_vnrequestid.Value, dal);
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception creating view for call in CreateCallsView, going to next call");
                    XrmDataContext.ClearChanges();
                }
            }
        }

        private DataModel.Xrm.new_callforreport CreateCallForReport(DataAccessLayer.CallForReport dal, Dictionary<string, object> row)
        {
            DataModel.Xrm.new_callforreport call = new NoProblem.Core.DataModel.Xrm.new_callforreport();
            if (row["accountid"] != DBNull.Value)
            {
                call.new_accountid = (Guid)row["accountid"];
                call.new_accountname = Convert.ToString(row["name"]);
                call.new_bizid = Convert.ToString(row["new_bizid"]);
                call.new_dapazstatus = row["new_dapazstatus"] != DBNull.Value ? (int)row["new_dapazstatus"] : (int)DataModel.Xrm.account.DapazStatus.PPA;
            }
            bool isSuccessfulCall;
            call.new_callstatus = CalculateStatus(Convert.ToString(row["new_mappingstatus"]), Convert.ToString(row["new_ivrstatus"]), out isSuccessfulCall,
                row["New_AdvertiserCallDuration"] != DBNull.Value ? (int)row["New_AdvertiserCallDuration"] : 0);
            call.new_issuccessfulcall = isSuccessfulCall;
            call.new_calldatetime = (DateTime)row["starttime"];
            call.new_calldatetimelocal = FixDateToLocal(call.new_calldatetime.Value);
            if (row["endtime"] != DBNull.Value)
            {
                call.new_calldatetimeend = (DateTime)row["endtime"];
                call.new_calldatetimeendlocal = FixDateToLocal(call.new_calldatetimeend.Value);
            }
            else
            {
                call.new_calldatetimeend = call.new_calldatetime;
                call.new_calldatetimeendlocal = call.new_calldatetimelocal;
            }
            call.new_duration = (int)((call.new_calldatetimeend.Value - call.new_calldatetime.Value).TotalSeconds);
            call.new_callerid = Convert.ToString(row["new_ivrcallerid"]);
            call.new_target = Convert.ToString(row["new_target1"]);
            call.new_virtualnumber = Convert.ToString(row["new_ivrinbound"]);
            call.new_sessionid = Convert.ToString(row["new_ivrsession"]);
            call.new_vnrequestid = (Guid)row["new_vnrequestid"];
            //if (row["new_vnstatusid"] != DBNull.Value)
            //{
            //    call.new_vnstatusid = (Guid)row["new_vnstatusid"];
            //}
            call.new_recordfilelocation = Convert.ToString(row["new_recordfilelocation"]);
            if (!AlreadyExists(call.new_vnrequestid.Value, dal))
            {
                dal.Create(call);
                XrmDataContext.SaveChanges();
            }
            return call;
        }

        private bool AlreadyExists(Guid vnRequestId, DataAccessLayer.CallForReport dal)
        {
            DataAccessLayer.VnRequest.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_vnrequest>.SqlParametersList();
            parameters.Add("@id", vnRequestId);
            object scalar = dal.ExecuteScalar(checkAlreadyExists, parameters);
            return (scalar != null && scalar != DBNull.Value);
        }

        private string CalculateStatus(string mappingStatus, string ivrStatus, out bool isSuccessfulCall, int advertiserCallDuration)
        {
            isSuccessfulCall = false;
            if (mappingStatus.Equals("MappingFailed;SupplierNotActive", StringComparison.InvariantCultureIgnoreCase))
            {
                return DataModel.Dapaz.SimsReports.SimsCallData.CallStatusOptions.NOT_ACTIVE;
            }
            //if (ivrStatus.Equals(HangupCauses.NO_ANSWER, StringComparison.InvariantCultureIgnoreCase)
            //    || ivrStatus.Equals(HangupCauses.USER_BUSY, StringComparison.InvariantCultureIgnoreCase)
            //    || ivrStatus.Equals("NO_POSSIBLE_REROUTE_FOUND", StringComparison.InvariantCultureIgnoreCase)
            //    || ivrStatus.StartsWith("CALL_CANCEL"))
            //{
            //    return DataModel.Dapaz.SimsReports.SimsCallData.CallStatusOptions.NO_ANSWER;
            //}
            if (advertiserCallDuration == 0)
            {
                return DataModel.Dapaz.SimsReports.SimsCallData.CallStatusOptions.NO_ANSWER;
            }
            isSuccessfulCall = true;
            return DataModel.Dapaz.SimsReports.SimsCallData.CallStatusOptions.ANSWER;
        }

        private void UpdateDoneCall(Guid vnRequestId, DataAccessLayer.CallForReport dal)
        {
            DataAccessLayer.VnRequest.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_vnrequest>.SqlParametersList();
            parameters.Add("@id", vnRequestId);
            dal.ExecuteNonQuery(updateCallAsFixedForReport, parameters);
        }

        #region Queries

        private const string checkAlreadyExists =
            @"select 1 from new_callforreport where new_vnrequestid = @id";

        private const string getNewCallsQuery =
            @"
select
  distinct
  req.new_ivrinbound
, req.new_ivrcallerid
, isnull(req.New_Target1, acc.Telephone1) new_target1
, req.new_mappingstatus
, sta.new_ivrstatus
, req.new_ivrsession
, req.createdon as starttime
, sta.createdon as endtime
, acc.accountid
, acc.name
, acc.new_bizid
, acc.new_dapazstatus
, req.new_vnrequestid
, ia.new_recordfilelocation
, sta.New_AdvertiserCallDuration
from new_vnrequest req with(nolock)
left join(
 select New_vnrequestid, min(CreatedOn) createdon, min(new_ivrstatus) new_ivrstatus, min(New_AdvertiserCallDuration) New_AdvertiserCallDuration from New_vnstatus with(nolock) group by new_vnrequestid
) sta on sta.new_vnrequestid = req.New_vnrequestId
left join account acc with(nolock) on acc.accountid = req.new_accountid
left join new_incidentaccount ia with(nolock) on ia.new_recordid = req.new_ivrsession
where
req.createdon < dateadd(minute, -15, getdate())
and req.createdon > '2013-01-27'
and isnull(req.new_fixedforreport, 0) = 0
";

        private const string updateCallAsFixedForReport =
            @"
update new_vnrequest
set new_fixedforreport = 1
where
new_vnrequestid = @id
";

        #endregion
    }
}
