﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Dapaz.Reports;

namespace NoProblem.Core.BusinessLogic.Dapaz.Reports
{
    public class CallsGroupedReportCreator : XrmUserBase
    {
        public CallsGroupedReportCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public DapazGroupedCallsResponse CreateReport(DateTime fromDate, DateTime toDate, Guid supplierId, bool? callStatus, DapazCallsReportRequest.eDapazCallsReportGroupBy? groupBy, DataModel.Xrm.account.DapazStatus? dapazStatus)
        {
            FixDatesToFullDays(ref fromDate, ref toDate);
            DataAccessLayer.CallForReport dal = new NoProblem.Core.DataAccessLayer.CallForReport(XrmDataContext);
            DataAccessLayer.CallForReport.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_callforreport>.SqlParametersList();
            parameters.Add("@from", fromDate);
            parameters.Add("@to", toDate);
            StringBuilder query = new StringBuilder(
                @"
select 
 dateadd({0}, datediff({0}, 0, new_calldatetimelocal), 0) time
,avg(new_duration) avgDurationInSeconds
,sum(new_duration) durationTimeInSeconds
,count(new_callforreportid) count
from new_callforreport with(nolock)
where deletionstatecode = 0
and new_calldatetimelocal between @from and @to
");
            if (callStatus.HasValue)
            {
                query.Append(" and isnull(new_issuccessfulcall, 0) = @callStatus ");
                parameters.Add("@callStatus", callStatus.Value);
            }
            if (supplierId != Guid.Empty)
            {
                query.Append(" and new_accountid = @supplierId ");
                parameters.Add("@supplierId", supplierId);
            }
            if (dapazStatus.HasValue)
            {
                query.Append(" and new_dapazstatus = @dapazStatus ");
                parameters.Add("@dapazStatus", (int)(dapazStatus.Value));
            }
            query.Append(" group by dateadd({0}, datediff({0}, 0, new_calldatetimelocal), 0) order by dateadd({0}, datediff({0}, 0, new_calldatetimelocal), 0) ");
            var reader = dal.ExecuteReader(string.Format(query.ToString(), GetGroupBy(groupBy)), parameters);
            List<DapazGroupedCallsContainer> dataList = new List<DapazGroupedCallsContainer>();
            foreach (var row in reader)
            {
                DapazGroupedCallsContainer data = new DapazGroupedCallsContainer();
                data.AverageDurationInSeconds = Convert.ToInt32(row["avgDurationInSeconds"]);
                data.CallsTimeInSeconds = Convert.ToInt32(row["durationTimeInSeconds"]);
                data.NumberOfCallers = Convert.ToInt32(row["count"]);
                data.Time = (DateTime)row["time"];
                dataList.Add(data);
            }
            DapazGroupedCallsResponse response = new DapazGroupedCallsResponse();
            response.DataList = dataList;
            response.TotalCallers = dataList.Sum(x => x.NumberOfCallers);
            response.TotalMinutes = (int)Math.Round(((double)dataList.Sum(x => x.CallsTimeInSeconds)) / 60);
            response.AverageDurationInSeconds = response.TotalCallers == 0 ? 0 : dataList.Sum(x => x.CallsTimeInSeconds) / response.TotalCallers;
            return response;
        }

        private string GetGroupBy(DapazCallsReportRequest.eDapazCallsReportGroupBy? groupBy)
        {
            string retVal = "month";
            if (groupBy.HasValue)
            {
                switch (groupBy.Value)
                {
                    case DapazCallsReportRequest.eDapazCallsReportGroupBy.Day:
                        return "day";
                    case DapazCallsReportRequest.eDapazCallsReportGroupBy.Hour:
                        return "hour";
                }
            }
            return retVal;
        }
    }
}
