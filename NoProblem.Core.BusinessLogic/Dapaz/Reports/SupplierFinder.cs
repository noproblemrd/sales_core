﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Dapaz.Reports;

namespace NoProblem.Core.BusinessLogic.Dapaz.Reports
{
    public class SupplierFinder : XrmUserBase
    {
        public SupplierFinder(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<DapazReportsSupplierContainer> FindSupplier(string searchValue)
        {
            List<DapazReportsSupplierContainer> retVal = new List<DapazReportsSupplierContainer>();
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
            parameters.Add("@sv", "%" + searchValue + "%");
            var reader = dal.ExecuteReader(query, parameters);
            foreach (var row in reader)
            {
                DapazReportsSupplierContainer data = new DapazReportsSupplierContainer();
                data.Id = (Guid)row["accountid"];
                data.Name = Convert.ToString(row["name"]);
                data.BizId = Convert.ToString(row["new_bizid"]);
                retVal.Add(data);
            }
            return retVal;
        }

        private const string query =
                @"SELECT accountid, name, new_bizid
    from dbo.Account with(nolock)
    where deletionstatecode = 0
                and new_securitylevel is null
                and new_affiliateoriginid is null
                --and new_dapazstatus = 2
                and name like @sv
union 
      SELECT accountid, name, new_bizid
    from dbo.Account with(nolock)
    where deletionstatecode = 0
                and new_securitylevel is null
                and new_affiliateoriginid is null
                --and new_dapazstatus = 2
                and new_bizid like @sv
union 
      SELECT accountid, name, new_bizid
    from new_directnumber dn with(nolock)
    join dbo.new_accountexpertise ae with(nolock) on ae.new_accountexpertiseid = dn.new_accountexpertiseid
    join dbo.account acc with(nolock) on acc.accountid = ae.new_accountid
    where acc.deletionstatecode = 0
                and acc.new_securitylevel is null
                and acc.new_affiliateoriginid is null
                --and acc.new_dapazstatus = 2
                and dn.new_number like @sv
  order by name";
    }
}
