﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Dapaz.DapazAccountsOnlineSync;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Dapaz.DapazAccountsOnlineSync
{
    public class DapazAccountsOnlineSyncManager : XrmUserBase
    {
        public DapazAccountsOnlineSyncManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private static object creationLockObject = new object();

        public void HandleSyncRequest(AccountForOnlineSync_Zap request)
        {
            if (string.IsNullOrEmpty(request.CustomerNo))
            {
                throw new ArgumentException("CustomerNo cannot be null nor empty.", "CustomerNo");
            }
            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            Guid supplierId = accountRepositoryDal.GetSupplierIdByBizId(request.CustomerNo);
            if (supplierId == Guid.Empty)
            {
                CreateNew(request, accountRepositoryDal);
            }
            else
            {
                UpdateExisting(request, supplierId, accountRepositoryDal);
            }
        }

        private void CreateNew(AccountForOnlineSync_Zap request, DataAccessLayer.AccountRepository accountRepositoryDal)
        {
            DataModel.Xrm.account supplier = new NoProblem.Core.DataModel.Xrm.account();
            supplier.new_bizid = request.CustomerNo;
            DataModel.Xrm.account.DapazStatus dapazStatus = GetDapazStatus(request);
            if (dapazStatus == NoProblem.Core.DataModel.Xrm.account.DapazStatus.PPA)
            {
                throw new ArgumentException("AccountKeyStatus cannot be PPA for new accounts.", "AccountKeyStatus");
            }
            supplier.new_dapazstatus = (int)dapazStatus;
            supplier.new_status = (int)DataModel.Xrm.account.SupplierStatus.Approved;
            FillDetailsFields(request, supplier, true);
            DataAccessLayer.Autonumber.AutonumberManager autoNumber = new NoProblem.Core.DataAccessLayer.Autonumber.AutonumberManager();
            string next = autoNumber.GetNextNumber("account");
            supplier.accountnumber = next;
            supplier.new_accountgovernmentid = request.BusinessId;
            lock (creationLockObject)
            {
                if (!accountRepositoryDal.ExistsByBizId(request.CustomerNo))
                {
                    accountRepositoryDal.Create(supplier);
                    XrmDataContext.SaveChanges();
                }
            }
        }

        private void UpdateExisting(AccountForOnlineSync_Zap request, Guid supplierId, DataAccessLayer.AccountRepository accountRepositoryDal)
        {
            DataModel.Xrm.account supplier = 
                    (from acc in accountRepositoryDal.All
                     where acc.accountid == supplierId
                     select new DataModel.Xrm.account { accountid = acc.accountid, telephone1 = acc.telephone1, new_dapazstatus = acc.new_dapazstatus }
                    ).First();
            DataModel.Xrm.account.DapazStatus dapazStatus = GetDapazStatus(request);
            if (!supplier.new_dapazstatus.HasValue)
                supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.PPA;
            switch (dapazStatus)
            {
                case NoProblem.Core.DataModel.Xrm.account.DapazStatus.PPA:
                    if (supplier.new_dapazstatus != (int)DataModel.Xrm.account.DapazStatus.PPA)
                    {
                        throw new Exception("You cannot change status of account to PPA");
                    }
                    break;
                case NoProblem.Core.DataModel.Xrm.account.DapazStatus.Fix:
                    if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPA)
                    {
                        throw new Exception("Account is PPA in NP, received FIX from ZAP. Didn't touch.");
                        //return;
                        //throw new Exception("You cannot change status from PPA to FIX");
                    }
                    else if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.Rarah)
                    {
                        supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.Fix;
                    }
                    else if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.RarahQuote)
                    {
                        bool hasNumber = MoveHeadingsAndNumbersFromRarahQuoteToFix(supplier);
                        if (hasNumber)
                        {
                            supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.FixGold;
                        }
                        else
                        {
                            supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.Fix;
                        }
                    }
                    else if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.RarahGold)
                    {
                        supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.FixGold;
                    }
                    break;
                case NoProblem.Core.DataModel.Xrm.account.DapazStatus.Rarah:
                    if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.PPA)
                    {
                        throw new Exception("Account is PPA in NP, received RARAH from ZAP. Didn't touch.");
                        //return;
                    }
                    else if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.FixGold)
                    {
                        supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.RarahGold;
                    }
                    else if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.Fix)
                    {
                        supplier.new_dapazstatus = (int)DataModel.Xrm.account.DapazStatus.Rarah;
                    }
                    break;
            }
            FillDetailsFields(request, supplier, false);
            accountRepositoryDal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        private bool MoveHeadingsAndNumbersFromRarahQuoteToFix(NoProblem.Core.DataModel.Xrm.account supplier)
        {
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
            var accountExpertises = accExpDal.GetAllByAccount(supplier.accountid);
            bool first = true;
            DataModel.Xrm.new_directnumber dnToMove = null;
            for (int i = 0; i < accountExpertises.Count(); i++)
            {
                var dns = dnDal.GetAllDirectNumbersOfAccountExpertise(accountExpertises.ElementAt(i).new_accountexpertiseid);
                foreach (var dn in dns)
                {
                    if (first)
                    {
                        dnToMove = dn;
                        first = false;
                    }
                    else
                    {
                        dn.new_accountexpertiseid = null;
                        dn.new_originid = null;
                        dn.new_frozendate = DateTime.Now;
                        dnDal.Update(dn);
                    }
                }
                XrmDataContext.SaveChanges();
                accExpDal.SetState(accountExpertises.ElementAt(i), false);
            }
            if (dnToMove == null)
            {
                return false;
            }
            else
            {
                DataModel.Xrm.new_accountexpertise genericAccountExpertise = accExpDal.GetByAccountAndPrimaryExpertise(supplier.accountid, GetGenericHeadingId());
                if (genericAccountExpertise == null)
                {
                    genericAccountExpertise = CreateAccountExpertise(supplier, accExpDal);
                }
                else if (genericAccountExpertise.statecode != DataAccessLayer.AccountExpertise.ACTIVE)
                {
                    accExpDal.SetState(genericAccountExpertise, true);
                }
                dnToMove.new_accountexpertiseid = genericAccountExpertise.new_accountexpertiseid;
                dnToMove.new_originid = GetMainDapazOriginId();
                dnDal.Update(dnToMove);
                XrmDataContext.SaveChanges();
                return true;
            }
        }

        private DataModel.Xrm.new_accountexpertise CreateAccountExpertise(DataModel.Xrm.account supplier, DataAccessLayer.AccountExpertise accountExpertiseDal)
        {
            DataModel.Xrm.new_accountexpertise genericAccountExpertise = new NoProblem.Core.DataModel.Xrm.new_accountexpertise();
            genericAccountExpertise.new_accountid = supplier.accountid;
            genericAccountExpertise.new_primaryexpertiseid = GetGenericHeadingId();
            genericAccountExpertise.new_incidentprice = 0;
            accountExpertiseDal.Create(genericAccountExpertise);
            XrmDataContext.SaveChanges();
            return genericAccountExpertise;
        }

        private Guid GetMainDapazOriginId()
        {
            DataAccessLayer.Origin originDal = new NoProblem.Core.DataAccessLayer.Origin(XrmDataContext);
            Guid mainDapazOrigin = originDal.GetMainDapazOriginId();
            if (mainDapazOrigin == Guid.Empty)
            {
                throw new Exception("Main dapaz origin not configured.");
            }
            return mainDapazOrigin;
        }

        private Guid GetGenericHeadingId()
        {
            DataAccessLayer.PrimaryExpertise peDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            Guid genericHeadingId = peDal.GetGenericHeadingId();
            if (genericHeadingId == Guid.Empty)
            {
                throw new Exception("Generic heading is not configured in the system.");
            }
            return genericHeadingId;
        }

        private DataModel.Xrm.account.DapazStatus GetDapazStatus(AccountForOnlineSync_Zap request)
        {
            var key = request.ListOfNoProblemKeys.FirstOrDefault();
            if (key == null)
            {
                throw new ArgumentException("The array ListOfNoProblemKeys must contain at least one item.", "ListOfNoProblemKeys");
            }

            switch (key.AccountKeyStatus)
            {
                case KeyForAccountOnlineSync_Zap.AccountSubLocals_KeyForAccountOnlineSync_Zap_options.PPA:
                    return NoProblem.Core.DataModel.Xrm.account.DapazStatus.PPA;
                case KeyForAccountOnlineSync_Zap.AccountSubLocals_KeyForAccountOnlineSync_Zap_options.FIX:
                    return NoProblem.Core.DataModel.Xrm.account.DapazStatus.Fix;
                case KeyForAccountOnlineSync_Zap.AccountSubLocals_KeyForAccountOnlineSync_Zap_options.RARAH:
                    return NoProblem.Core.DataModel.Xrm.account.DapazStatus.Rarah;
                default:
                    throw new ArgumentException(string.Format("Could not parse AccountKeyStatus. Available options are '{0}', '{1}' or '{2}'",
                        KeyForAccountOnlineSync_Zap.AccountSubLocals_KeyForAccountOnlineSync_Zap_options.PPA,
                        KeyForAccountOnlineSync_Zap.AccountSubLocals_KeyForAccountOnlineSync_Zap_options.FIX,
                        KeyForAccountOnlineSync_Zap.AccountSubLocals_KeyForAccountOnlineSync_Zap_options.RARAH), "AccountKeyStatus");
            }
        }

        private void FillDetailsFields(AccountForOnlineSync_Zap request, DataModel.Xrm.account supplier, bool isCreation)
        {
            supplier.new_firstname = request.AccountContactFirstName;
            supplier.new_lastname = request.AccountContactLastName;
            supplier.fax = CheckAddZeroToPhone(request.AccountFaxNum);
            supplier.websiteurl = request.AccountInternetSite;
            supplier.new_custom5 = request.AccountInvoiceName;
            supplier.name = request.AccountName;
            string mainPhone = CheckAddZeroToPhone(request.PhoneNumberRelatedForSims);
            string secondaryPhone = CheckAddZeroToPhone(request.AccountPhoneNum2);
            if (supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.Fix
                || supplier.new_dapazstatus == (int)DataModel.Xrm.account.DapazStatus.Rarah
                || isCreation)
            {
                supplier.telephone1 = mainPhone;
                supplier.telephone2 = secondaryPhone;
                supplier.new_donotsendsms = true;
            }
            else
            {
                if (supplier.telephone1 != mainPhone)
                {
                    supplier.telephone2 = mainPhone;
                }
                else if (!string.IsNullOrEmpty(secondaryPhone))
                {
                    supplier.telephone2 = secondaryPhone;
                }
            }
            supplier.address1_city = request.AccountPrimaryCityName;
            supplier.address1_line1 = request.AccountPrimaryStreetName;
            supplier.address1_line2 = request.AccountPrimaryHouseNum;
            supplier.address1_postalcode = request.AccountPrimaryZipcode;
            supplier.new_accountgovernmentid = request.BusinessId;
            supplier.emailaddress1 = request.Email;
            supplier.telephone3 = CheckAddZeroToPhone(request.PhoneNumberRelatedForSMS);
            int fundsStatus = GetFundsStatusFromString(request.FinanceStatus);
            if (fundsStatus < 0)
            {
                if (isCreation)
                {
                    supplier.new_fundsstatus = (int)DataModel.Xrm.account.FundsStatus.Normal;
                }
            }
            else
            {
                supplier.new_fundsstatus = fundsStatus;
            }
            if (!string.IsNullOrEmpty(request.PrimaryLocalAreaCode))
            {
                Guid? regionId = GetRegionIdFromZapCode(request.PrimaryLocalAreaCode);
                if (regionId.HasValue)
                {
                    supplier.new_rarahregionid = regionId.Value;
                }
            }
            if (!string.IsNullOrEmpty(request.PrimaryHeadingCode))
            {
                Guid? headingId = GetHeadingIdFromZapCode(request.PrimaryHeadingCode);
                if (headingId.HasValue)
                {
                    supplier.new_rarahheadingid = headingId.Value;
                }
            }
        }

        private Guid? GetHeadingIdFromZapCode(string zapHeadingCode)
        {
            Guid? retVal = null;
            DataAccessLayer.PrimaryExpertise dal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
            Guid headingId = dal.GetPrimaryExpertiseIdByEnglishName(zapHeadingCode);
            if (headingId != Guid.Empty)
            {
                retVal = headingId;
            }
            return retVal;
        }

        private Guid? GetRegionIdFromZapCode(string mekomonCode)
        {
            Guid? retVal = null;
            DataAccessLayer.Region dal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            Guid regionId = dal.GetRegionIdByCode(mekomonCode);
            if (regionId != Guid.Empty)
            {
                retVal = regionId;
            }
            return retVal;
        }

        private int GetFundsStatusFromString(string fundsStatusString)
        {
            DataModel.Xrm.account.FundsStatus retValEnum;
            try
            {
                retValEnum = (DataModel.Xrm.account.FundsStatus)Enum.Parse(typeof(DataModel.Xrm.account.FundsStatus), fundsStatusString);
            }
            catch (Exception)
            {
                //throw new ArgumentException(string.Format("Failed to parse FundsStatus. Received {0}. Available options are Normal, DelayedSale, LegalHandling, HasDelayingDebt, HasDebt, ChecksOnly, SalesAgentTreatment, DontSellOpenDebt", fundsStatusString), "FundsStatus", exc);
                return -1;
            }
            return (int)retValEnum;
        }

        private string CheckAddZeroToPhone(string phone)
        {
            //if (!string.IsNullOrEmpty(phone) && !phone.StartsWith("0"))
            //{
            //    phone = "0" + phone;
            //}
            return phone;
        }

        //private void ChangeWorkingRegion(DataModel.SqlHelper.RegionMinData workingRegion, DataModel.Xrm.account supplier)
        //{
        //    DataAccessLayer.RegionAccount regAccDal = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
        //    foreach (var item in supplier.new_account_new_regionaccountexpertise)
        //    {
        //        regAccDal.Delete(item);
        //    }
        //    XrmDataContext.SaveChanges();
        //    CreateWorkingRegion(workingRegion, supplier, regAccDal);
        //}

        //private void CreateWorkingRegion(DataModel.SqlHelper.RegionMinData workingRegion, DataModel.Xrm.account supplier)
        //{
        //    CreateWorkingRegion(workingRegion, supplier, new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext));
        //}

        //private void CreateWorkingRegion(DataModel.SqlHelper.RegionMinData workingRegion, DataModel.Xrm.account supplier, DataAccessLayer.RegionAccount regAccDal)
        //{
        //    DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
        //    var brothersId = regionDal.GetBrothersIds(workingRegion.Id);
        //    foreach (var broId in brothersId)
        //    {
        //        DataModel.Xrm.new_regionaccountexpertise regAc = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
        //        regAc.new_accountid = supplier.accountid;
        //        regAc.new_regionid = broId;
        //        regAc.new_regionstate = (int)DataModel.Xrm.new_regionaccountexpertise.RegionState.NotWorking;
        //        regAccDal.Create(regAc);
        //    }
        //    DataModel.Xrm.new_regionaccountexpertise parentRegAc = new NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise();
        //    parentRegAc.new_regionstate = (int)DataModel.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild;
        //    parentRegAc.new_accountid = supplier.accountid;
        //    parentRegAc.new_regionid = workingRegion.ParentId;
        //    regAccDal.Create(parentRegAc);
        //    XrmDataContext.SaveChanges();
        //}

        //private DataModel.SqlHelper.RegionMinData GetWorkingRegionId(AccountForOnlineSync_Zap request)
        //{
        //    if (string.IsNullOrEmpty(request.AccountPrimaryLocal))
        //    {
        //        throw new ArgumentException("AccountPrimaryLocal cannot be null nor empty", "AccountPrimaryLocal");
        //    }
        //    DataAccessLayer.Region regionDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
        //    var region = regionDal.GetRegionMinDataByCode(request.AccountPrimaryLocal);
        //    if (region == null)
        //    {
        //        throw new ArgumentException(string.Format("Mekomon with code '{0}' was not found.", request.AccountPrimaryLocal), "AccountPrimaryLocal");
        //    }
        //    if (region.Level != 2)
        //    {
        //        throw new ArgumentException(string.Format("Mekomon with code '{0}' was not found.", request.AccountPrimaryLocal), "AccountPrimaryLocal");
        //    }
        //    return region;
        //}

        //private void HandleHeadingsOnUpdate(AccountForOnlineSync_Zap request, NoProblem.Core.DataModel.Xrm.account supplier)
        //{
        //    List<Guid> sentHeadings = GetHeadingsIds(request);
        //    DataAccessLayer.AccountExpertise acExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
        //    DataAccessLayer.DirectNumber dnDal = new NoProblem.Core.DataAccessLayer.DirectNumber(XrmDataContext);
        //    IEnumerable<DataModel.Xrm.new_accountexpertise> currentHeadings = acExpDal.GetAllByAccount(supplier.accountid);
        //    foreach (var accExp in currentHeadings)
        //    {
        //        if (sentHeadings.Contains(accExp.new_primaryexpertiseid.Value))
        //        {
        //            sentHeadings.Remove(accExp.new_primaryexpertiseid.Value);
        //            if (accExp.statecode != DataAccessLayer.AccountExpertise.ACTIVE)
        //            {
        //                acExpDal.SetState(accExp, true);
        //            }
        //        }
        //        else
        //        {
        //            if (accExp.statecode == DataAccessLayer.AccountExpertise.ACTIVE)
        //            {
        //                acExpDal.SetState(accExp, false);
        //                dnDal.DetachDirectNumbersFromAccountExpertise(accExp.new_accountexpertiseid, true);
        //            }
        //        }
        //    }
        //    CreateNewHeadings(sentHeadings, supplier, acExpDal);
        //}

        //private void CreateNewHeadings(List<Guid> headings, DataModel.Xrm.account supplier)
        //{
        //    CreateNewHeadings(headings, supplier, new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext));
        //}

        //private void CreateNewHeadings(List<Guid> headings, DataModel.Xrm.account supplier, DataAccessLayer.AccountExpertise acExpDal)
        //{
        //    foreach (var headingId in headings)
        //    {
        //        DataModel.Xrm.new_accountexpertise accExp = new NoProblem.Core.DataModel.Xrm.new_accountexpertise();
        //        accExp.new_primaryexpertiseid = headingId;
        //        accExp.new_accountid = supplier.accountid;
        //        // add price per call and if is in auctions.
        //        acExpDal.Create(accExp);
        //        XrmDataContext.SaveChanges();
        //    }
        //    // add direct number assigment logic by dapaz status if needed.
        //    DirectNumbersAssigner assigner = new DirectNumbersAssigner();
        //    assigner.AssignDirectNumbersToSupplier(supplier.accountid);
        //}


        //private List<Guid> GetHeadingsIds(AccountForOnlineSync_Zap request)
        //{
        //    List<Guid> headings = new List<Guid>();
        //    DataAccessLayer.PrimaryExpertise headingDal = new NoProblem.Core.DataAccessLayer.PrimaryExpertise(XrmDataContext);
        //    if (request.ListOfNoProblemKeys != null && request.ListOfNoProblemKeys.Count > 0)
        //    {
        //        foreach (var key in request.ListOfNoProblemKeys)
        //        {
        //            if (string.IsNullOrEmpty(key.AccountSegment))
        //            {
        //                throw new ArgumentException(string.Format("AccountSegment cannot by null nor empty.", key.AccountSegment), "AccountSegment");
        //            }
        //            var heading = headingDal.GetPrimaryExpertiseByEnglishName(key.AccountSegment);
        //            if (heading == null)
        //            {
        //                throw new ArgumentException(string.Format("AccountSegment with code '{0}' was not found.", key.AccountSegment), "AccountSegment");
        //            }
        //            headings.Add(heading.new_primaryexpertiseid);
        //        }
        //    }
        //    return headings;
        //}

    }
}
