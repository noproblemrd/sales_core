﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel.Dapaz.DapazAccountsOnlineSync;

namespace NoProblem.Core.BusinessLogic.Dapaz.DapazAccountsOnlineSync
{
    public class FirstTimeSyncManager
    {
        private DataAccessLayer.AccountRepository dal;
        private DapazAccountsOnlineSyncManager syncManager;

        public FirstTimeSyncManager()
        {
            dal = new NoProblem.Core.DataAccessLayer.AccountRepository(null);
            syncManager = new DapazAccountsOnlineSyncManager(dal.XrmDataContext);                
        }

        public void Sync(int top)
        {
            Thread tr = new Thread(new ParameterizedThreadStart(Sync));
            tr.Start(top);
        }

        private void Sync(object obj)
        {
            try
            {                
                var reader = dal.ExecuteReader(string.Format(queryGet, (int)obj));
                foreach (var row in reader)
                {
                    string uniqueId = Convert.ToString(row["ACCOUNT_ID"]);
                    try
                    {
                        AccountForOnlineSync_Zap request = new AccountForOnlineSync_Zap();
                        request.AccountContactFirstName = Convert.ToString(row["ACCOUNT_CONTACT_FIRST_NAME"]);
                        request.AccountContactLastName = Convert.ToString(row["ACCOUNT_CONTACT_LAST_NAME"]);
                        request.AccountFaxNum = Convert.ToString(row["ACCOUNT_FAX_NUM"]);
                        request.AccountInternetSite = Convert.ToString(row["ACCOUNT_INTERNET_SITE"]);
                        request.AccountInvoiceName = Convert.ToString(row["ACCOUNT_INVOICE_NAME"]);
                        request.AccountName = Convert.ToString(row["ACCOUNT_NAME"]);
                        request.AccountPhoneNum2 = Convert.ToString(row["account_phone_num2"]);
                        request.AccountPrimaryCityName = Convert.ToString(row["ACCOUNT_PRIMARY_CITY_NAME"]);
                        request.AccountPrimaryHouseNum = Convert.ToString(row["ACCOUNT_PRIMARY_HOUSE_NUM"]);
                        request.AccountPrimaryStreetName = Convert.ToString(row["ACCOUNT_PRIMARY_STREET_NAME"]);
                        request.AccountPrimaryZipcode = Convert.ToString(row["ACCOUNT_PRIMARY_ZIP_CODE"]);
                        request.BusinessId = Convert.ToString(row["BUSINESS_ID"]);
                        request.CustomerNo = Convert.ToString(row["CUSTOMER_NO"]);
                        request.Email = Convert.ToString(row["EMAIL"]);
                        request.IsActive = Convert.ToString(row["ACCOUNT_STATUS"]) == "TRUE" ? true : false;
                        request.PhoneNumberRelatedForSims = Convert.ToString(row["PHONE_NUM_RELATED_FOR_SIMS"]);
                        request.PhoneNumberRelatedForSMS = Convert.ToString(row["PHONE_NUM_RELATED_FOR_SMS"]);
                        request.FinanceStatus = Convert.ToString(row["FUNDS_STATUS"]);
                        request.ListOfNoProblemKeys = new List<KeyForAccountOnlineSync_Zap>();
                        request.ListOfNoProblemKeys.Add(
                            new KeyForAccountOnlineSync_Zap()
                            {
                                AccountKeyStatus = Convert.ToString(row["ACCOUNT_KEY_STATUS"]),
                            });
                        syncManager.HandleSyncRequest(request);
                        MarkAsDoneAndOk(uniqueId);
                    }
                    catch (Exception exc)
                    {
                        MarkAsDoneAndNotOk(uniqueId, exc.Message);
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DapazAccountsOnlineSync.FirstTimeSyncManager.Sync");
            }
        }

        private void MarkAsDoneAndNotOk(string uniqueId, string errorMessage)
        {
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
            parameters.Add("@id", uniqueId);
            parameters.Add("@error", errorMessage);
            dal.ExecuteNonQuery(setNotOkQuery, parameters);
        }

        private void MarkAsDoneAndOk(string uniqueId)
        {
            DataAccessLayer.AccountRepository.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.account>.SqlParametersList();
            parameters.Add("@id", uniqueId);
            dal.ExecuteNonQuery(setOkQuery, parameters);
        }

        private const string setOkQuery =
            @"update ZapAccountsImport 
            set done = 1,
            ok = 1
            where ACCOUNT_ID = @id";

        private const string setNotOkQuery =
            @"update ZapAccountsImport 
            set done = 1,
            ok = 0,
            notes = @error
            where ACCOUNT_ID = @id";

        private const string queryGet =
            @"select top {0} * from ZapAccountsImport
               where done = 0";
    }
}
