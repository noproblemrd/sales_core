﻿using Effect.Crm.SDKProviders.CrmServiceSdk;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic
{
   public static class ErrorHandler
   {
      public static void ReportError(EntityName entity, string entityID, string version, string message, string className, string methodName)
      {
         LogUtils.MyHandle.WriteToLog("Error!!!: methodName: {0},message: {1}",methodName,message);
      }
   }
}
