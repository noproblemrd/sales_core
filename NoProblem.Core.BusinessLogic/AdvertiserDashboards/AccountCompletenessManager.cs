﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.AdvertiserDashboards;

namespace NoProblem.Core.BusinessLogic.AdvertiserDashboards
{
    public class AccountCompletenessManager
    {
        public CompletenessData GetCompletenessData(Guid supplierId)
        {
            CompletenessData data = new CompletenessData();
            bool detailsDone;
            bool workHoursDone;
            GetWhatIsDone(supplierId, out detailsDone, out workHoursDone);
            CalculateLink(data, detailsDone, workHoursDone);
            CalculatePercentageDone(data, detailsDone, workHoursDone);
            return data;
        }

        private void CalculatePercentageDone(CompletenessData data, bool detailsDone, bool workHoursDone)
        {
            int percentageDone = 50;            
            if (workHoursDone)
            {
                percentageDone += 25;
            }
            if (detailsDone)
            {
                percentageDone += 25;
            }
            data.PercentageDone = percentageDone;
        }

        private void CalculateLink(CompletenessData data, bool detailsDone, bool workHoursDone)
        {
            if (!workHoursDone)
            {
                data.LinkTo = CompletenessData.CompletenessDataLinkOptions.WorkHours;
                data.PercentageInLink = 25;
            }
            else if (!detailsDone)
            {
                data.LinkTo = CompletenessData.CompletenessDataLinkOptions.Details;
                data.PercentageInLink = 25;
            }
        }

        private void GetWhatIsDone(Guid supplierId, out bool detailsDone, out bool workHoursDone)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(null);
            string query =
                @"select top 1 new_accountcompleteness_mydetails, new_accountcompleteness_workhours
                from account with(nolock) where accountid = @supplierId";
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            var reader = dal.ExecuteReader(query, parameters);
            detailsDone = false;
            workHoursDone = false;
            foreach (var row in reader)
            {
                if (row["new_accountcompleteness_mydetails"] != DBNull.Value)
                {
                    detailsDone = (bool)row["new_accountcompleteness_mydetails"];
                }
                if (row["new_accountcompleteness_workhours"] != DBNull.Value)
                {
                    workHoursDone = (bool)row["new_accountcompleteness_workhours"];
                }                
                break;
            }
        }
    }
}
