﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview;

namespace NoProblem.Core.BusinessLogic.AdvertiserDashboards
{
    public class BillingOverviewManager : XrmUserBase
    {
        public BillingOverviewManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public GetRechargeDataResponse GetRechargeData(Guid supplierId)
        {
            GetRechargeDataResponse response = new GetRechargeDataResponse();
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = dal.Retrieve(supplierId);
            if (supplier.new_rechargeenabled.HasValue && supplier.new_rechargeenabled.Value
                && supplier.new_rechargeamount.HasValue)
            {
                int amount = supplier.new_rechargeamount.Value;
                decimal maxPrice, minPrice;
                DataAccessLayer.AccountExpertise acExDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
                acExDal.GetSupplierMinAndMaxPrices(supplierId, out maxPrice, out minPrice);
                response.RechargeAmount = amount;
                response.MinPrice = minPrice;
            }
            return response;
        }

        public GetAccountActivityResponse GetAccountActivity(Guid supplierId)
        {
            GetAccountActivityResponse response = new GetAccountActivityResponse();

            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = accountRepositoryDal.Retrieve(supplierId);

            int timeZoneCode;
            if (supplier.address1_utcoffset.HasValue)
            {
                timeZoneCode = supplier.address1_utcoffset.Value;
            }
            else
            {
                DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                timeZoneCode = int.Parse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PUBLISHER_TIME_ZONE));
            }

            DateTime now = DateTime.Now;
            DateTime firstDay = new DateTime(now.Year, now.Month, 1);
            DateTime lastDay = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));
            firstDay = FixDateToLocal(firstDay, timeZoneCode);
            lastDay = FixDateToLocal(lastDay, timeZoneCode);

            response.Month = now.Month;

            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", supplierId));
            parameters.Add(new KeyValuePair<string, object>("@from", firstDay));
            parameters.Add(new KeyValuePair<string, object>("@to", lastDay));

            var reader = accountRepositoryDal.ExecuteReader(accountActivityQuery, parameters);

            foreach (var row in reader)
            {
                response.LeadCount = (int)row["leadCount"];
                response.LeadMoney = (int)row["leadMoney"] * -1;
                response.RefundCount = (int)row["refundCount"];
                response.RefundMoney = (int)row["refundMoney"] * -1;
                response.EarnedCount = (int)row["earnedCount"];
                response.EarnedMoney = (int)row["earnedMoney"];
                response.BonusCount = (int)row["bonusCount"];
                response.BonusMoney = (int)row["bonusMoney"];
                response.TotalUsed = response.LeadMoney + response.RefundMoney;
                response.TotalBonus = response.EarnedMoney + response.BonusMoney;
                break;
            }
            
            return response;
        }

        public List<TransactionRow> GetTransactions(Guid supplierId, bool lastSix)
        {
            List<TransactionRow> retVal = new List<TransactionRow>();

            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@id", supplierId));
            DataAccessLayer.BalanceRow dal = new NoProblem.Core.DataAccessLayer.BalanceRow(XrmDataContext);
            var reader = dal.ExecuteReader(lastSix ? lastTransactionsQuery : allTransactionsQuery, parameters);
            int rowAdded = 0;
            if (reader.Count > 0)
            {
                int timeZoneCode;
                var firstRow = reader.First();
                if (firstRow["timezone"] != DBNull.Value)
                {
                    timeZoneCode = (int)firstRow["timezone"];
                }
                else
                {
                    DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                    timeZoneCode = int.Parse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PUBLISHER_TIME_ZONE));
                }
                DateTime utcNow = DateTime.Now;
                DateTime localNow = FixDateToLocal(utcNow, timeZoneCode);
                var span = localNow - utcNow;
                foreach (var row in reader)
                {
                    DataModel.Xrm.new_balancerow.Action action = (DataModel.Xrm.new_balancerow.Action)((int)row["action"]);
                    int balanceAfterAction = (int)(decimal)row["balanceAfterAction"];
                    DateTime createdOn = (DateTime)row["createdon"];
                    string description = Convert.ToString(row["description"]);
                    TransactionRow regularRow = new TransactionRow();
                    if (action == NoProblem.Core.DataModel.Xrm.new_balancerow.Action.Deposit)
                    {
                        int amount = (int)row["amount"];
                        int bonuses = (int)row["bonuses"];
                        DateTime timeLocal = FixHourWithSpan(createdOn, span);
                        if (bonuses > 0)
                        {
                            TransactionRow bonusesRow = new TransactionRow();
                            bonusesRow.Amount = bonuses;
                            bonusesRow.Balance = balanceAfterAction;
                            bonusesRow.DateLocal = timeLocal;
                            bonusesRow.DateServer = createdOn;
                            bonusesRow.Type = "Bonus credit";
                            if (!string.IsNullOrEmpty(description))
                            {
                                bonusesRow.Type += " (" + description + ")";
                            }
                            retVal.Add(bonusesRow);
                            rowAdded++;
                            if (lastSix && rowAdded == 6)
                                break;
                        }
                        regularRow.Amount = amount;
                        regularRow.Balance = balanceAfterAction - bonuses;
                        regularRow.DateLocal = timeLocal;
                        regularRow.DateServer = createdOn;
                        if (!string.IsNullOrEmpty(description))
                        {
                            regularRow.Type = description + " purchased";
                        }
                        else
                        {
                            switch (amount)
                            {
                                case 50:
                                    regularRow.Type = "Bronze Plan purchased";
                                    break;
                                case 200:
                                    regularRow.Type = "Silver Plan purchased";
                                    break;
                                case 300:
                                    regularRow.Type = "Gold Plan purchased";
                                    break;
                                case 30:
                                case 125:
                                    regularRow.Type = "Bonus credit (Registration)";
                                    break;
                                default:
                                    regularRow.Type = "Deposit";
                                    break;
                            }
                        }
                    }
                    else
                    {
                        int amountTotal = (int)(decimal)row["amountTotal"];
                        regularRow.Amount = amountTotal;
                        regularRow.Balance = balanceAfterAction;
                        regularRow.DateLocal = FixHourWithSpan(createdOn, span);
                        regularRow.DateServer = createdOn;
                        switch (action)
                        {
                            case NoProblem.Core.DataModel.Xrm.new_balancerow.Action.AutoRecharge:
                                regularRow.Type = "Auto recharge";
                                break;
                            case NoProblem.Core.DataModel.Xrm.new_balancerow.Action.BonusCredit:
                                regularRow.Type = "Bonus credit (" + description + ")";
                                break;
                            case NoProblem.Core.DataModel.Xrm.new_balancerow.Action.Call:
                                regularRow.Type = "Lead";
                                if (!string.IsNullOrEmpty(description))
                                    regularRow.Type += " " + description;
                                break;
                            case NoProblem.Core.DataModel.Xrm.new_balancerow.Action.CreditEarned:
                                regularRow.Type = "Credit earned (" + description + ")";
                                break;
                            case NoProblem.Core.DataModel.Xrm.new_balancerow.Action.Refund:
                                regularRow.Type = "Refund";
                                if (!string.IsNullOrEmpty(description))
                                    regularRow.Type += " " + description;
                                break;
                        }
                    }
                    retVal.Add(regularRow);
                    rowAdded++;
                    if (lastSix && rowAdded == 6)
                        break;
                }
            }
            return retVal;
        }

        private DateTime FixHourWithSpan(DateTime d, TimeSpan span)
        {
            if (span.Hours == 0)
            {
                return d;
            }
            else
            {
                return d.AddHours(span.Hours);
            }
        }

        #region Queries

        private const string accountActivityQuery =
            @"

declare @leadCount int
declare @leadMoney int

select
@leadCount = count(*)
, @leadMoney = sum(new_amount)
                from new_balancerow br with(nolock)
                join new_incidentaccount ia with(nolock) on ia.new_incidentaccountid = br.new_incidentaccountid
                where br.deletionstatecode = 0
 and ia.new_accountid = @id
and br.createdon between @from and @to
and br.new_action = 2
and
               (select count(*) from new_balancerow with(nolock)
				where
				new_incidentaccountid = ia.new_incidentaccountid
				and
				deletionstatecode = 0 and new_action = 4) = 0


declare @refundCount int
declare @refundMoney int

select
@refundCount = count(*)
, @refundMoney = sum(new_amount)
                from new_balancerow br with(nolock)
                join new_incidentaccount ia with(nolock) on ia.new_incidentaccountid = br.new_incidentaccountid
                where br.deletionstatecode = 0
 and ia.new_accountid = @id
and br.createdon between @from and @to
and br.new_action = 3
and
               (select count(*) from new_balancerow with(nolock)
				where
				new_incidentaccountid = ia.new_incidentaccountid
				and
				deletionstatecode = 0 and new_action = 4) = 0

declare @earnedCount int
declare @earnedMoney int

select 
@earnedCount = count(*)
,@earnedMoney = sum(new_amount)
                from new_balancerow br with(nolock)
                join new_supplierpricing sp with(nolock) on sp.new_supplierpricingid = br.new_supplierpricingid
                where br.deletionstatecode = 0 and sp.deletionstatecode = 0
                and sp.new_accountid = @id
				and br.new_action = 6
				and br.createdon between @from and @to

declare @bonusCount int
declare @bonusMoney int

select 
@bonusCount = count(*)
,@bonusMoney = sum(new_amount)
                from new_balancerow br with(nolock)
                join new_supplierpricing sp with(nolock) on sp.new_supplierpricingid = br.new_supplierpricingid
                where br.deletionstatecode = 0 and sp.deletionstatecode = 0
                and sp.new_accountid = @id
				and br.new_action = 7
				and br.createdon between @from and @to

declare @tmpCount int
declare @tmpMoney int

select 
@tmpCount = count(*)
,@tmpMoney = sum(isnull(new_credit, 0) + isnull(new_extrabonus, 0))
                from new_balancerow br with(nolock)
                join new_supplierpricing sp with(nolock) on sp.new_supplierpricingid = br.new_supplierpricingid
                where br.deletionstatecode = 0 and sp.deletionstatecode = 0
                and sp.new_accountid = @id
				and (br.new_action = 1 or br.new_action = 5)
				and br.createdon between @from and @to

set @bonusCount = @bonusCount + @tmpCount
set @bonusMoney = @bonusMoney + @tmpMoney

select 
@leadCount leadCount, isnull(@leadMoney, 0) leadMoney, 
@refundCount refundCount,  isnull(@refundMoney, 0) refundMoney, 
@earnedCount earnedCount, isnull(@earnedMoney, 0) earnedMoney,
@bonusCount bonusCount, isnull(@bonusMoney, 0) bonusMoney
";

        private const string allTransactionsQuery =
            @"
declare @timeZone int
select top 1 @timeZone = address1_utcoffset from account with(nolock) where accountid = @id

create table #tmpBalance (
id uniqueidentifier, 
createdon datetime, 
description nvarchar(200), 
action int, 
amountTotal decimal, 
balanceAfterAction decimal, 
amount int, 
bonuses int, 
isCall bit)

insert into #tmpBalance
select
new_balancerowid
,br.createdon
,new_description
,new_action
,new_amount
,new_balanceafteraction
,0
,0
,1
                from new_balancerow br with(nolock)
                join new_incidentaccount ia with(nolock) on ia.new_incidentaccountid = br.new_incidentaccountid
                where br.deletionstatecode = 0
 and ia.new_accountid = @id
and
               (select count(*) from new_balancerow with(nolock)
				where
				new_incidentaccountid = ia.new_incidentaccountid
				and
				deletionstatecode = 0 and new_action = 4) = 0
order by br.createdon desc

insert into #tmpBalance
select 
new_balancerowid
,br.createdon
,new_description
,new_action
,new_amount
,new_balanceafteraction
,sp.new_total
,isnull(sp.new_credit,0) + isnull(sp.new_extrabonus, 0)
,0
                from new_balancerow br with(nolock)
                join new_supplierpricing sp with(nolock) on sp.new_supplierpricingid = br.new_supplierpricingid
                where br.deletionstatecode = 0 and sp.deletionstatecode = 0
                and sp.new_accountid = @id


select top 400 *, @timeZone as timezone from #tmpBalance
order by createdon desc

drop table #tmpBalance";

        private const string lastTransactionsQuery =
                @"
declare @timeZone int
select top 1 @timeZone = address1_utcoffset from account with(nolock) where accountid = @id

create table #tmpBalance (
id uniqueidentifier, 
createdon datetime, 
description nvarchar(200), 
action int, 
amountTotal decimal, 
balanceAfterAction decimal, 
amount int, 
bonuses int, 
isCall bit)

insert into #tmpBalance
select
top 6
new_balancerowid
,br.createdon
,new_description
,new_action
,new_amount
,new_balanceafteraction
,0
,0
,1
                from new_balancerow br with(nolock)
                join new_incidentaccount ia with(nolock) on ia.new_incidentaccountid = br.new_incidentaccountid
                where br.deletionstatecode = 0
 and ia.new_accountid = @id
and
               (select count(*) from new_balancerow with(nolock)
				where
				new_incidentaccountid = ia.new_incidentaccountid
				and
				deletionstatecode = 0 and new_action = 4) = 0
order by br.createdon desc

insert into #tmpBalance
select 
top 6
new_balancerowid
,br.createdon
,new_description
,new_action
,new_amount
,new_balanceafteraction
,sp.new_total
,isnull(sp.new_credit,0) + isnull(sp.new_extrabonus, 0)
,0
                from new_balancerow br with(nolock)
                join new_supplierpricing sp with(nolock) on sp.new_supplierpricingid = br.new_supplierpricingid
                where br.deletionstatecode = 0 and sp.deletionstatecode = 0
                and sp.new_accountid = @id


select top 6 *, @timeZone as timezone from #tmpBalance
order by createdon desc

drop table #tmpBalance";

        #endregion
    }
}
