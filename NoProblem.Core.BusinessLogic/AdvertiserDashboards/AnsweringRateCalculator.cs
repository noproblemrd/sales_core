﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.AdvertiserDashboards
{
    public class AnsweringRateCalculator
    {
        public int GetAnsweringRate(Guid supplierId)
        {
            string query =
                @"declare @total real
                declare @closed real

                select @total = count(*) 
                from new_incidentaccount
                where deletionstatecode = 0
                and new_accountid = @supplierId

                select @closed = count(*) 
                from new_incidentaccount
                where deletionstatecode = 0
                and new_accountid = @supplierId
                and statuscode = 10--close

                if
                @total = 0
                begin
                select 0
                return
                end

                select cast(@closed / @total * 100 as int)";

            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
            DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(null);
            object scalar = dal.ExecuteScalar(query, parameters);
            return (int)scalar;
        }        
    }
}
