﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.AdvertiserDashboards;
using NoProblem.Core.BusinessLogic.TwilioBL;
using NoProblem.Core.DataModel;
using System.Configuration;
using System.Data.SqlClient;

namespace NoProblem.Core.BusinessLogic.AdvertiserDashboards
{
    public class GetStartedTestCallManager : TwilioHandlerBase
    {
        #region Ctor

        public GetStartedTestCallManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        #endregion

        #region Public Methods

        public LiveTestCallStatus GetLiveTestCallStatus(Guid testCallId)
        {
            //     May be queued, ringing, in-progress,
            //     completed, failed, busy or no-answer.
            LiveTestCallStatus retVal = new LiveTestCallStatus();
            DataAccessLayer.GetStartedTestCall dal = new NoProblem.Core.DataAccessLayer.GetStartedTestCall(XrmDataContext);
            var call = dal.Retrieve(testCallId);
            if (call.new_taskdone.HasValue && call.new_taskdone.Value)
            {
                retVal.Stage = LiveTestCallStatus.LiveTestCallStage.completed;
                retVal.ShowInReport = !call.new_closedfromreport.HasValue || !call.new_closedfromreport.Value;
            }
            else
            {
                retVal.ShowInReport = false;
                if (call.new_callstatus == "queued"
                    || call.new_callstatus == "ringing")
                {
                    retVal.Stage = LiveTestCallStatus.LiveTestCallStage.ringing;
                }
                else if (call.new_callstatus == "in-progress")
                {
                    if (!string.IsNullOrEmpty(call.new_dtmf) && call.new_dtmf != "NOTHING" && call.new_dtmf != "*" && call.new_dtmf != "0")
                    {
                        retVal.Stage = LiveTestCallStatus.LiveTestCallStage.advertiser_confirmed;
                    }
                    else
                    {
                        retVal.Stage = LiveTestCallStatus.LiveTestCallStage.advertiser_answered;
                    }
                }
                else if (call.new_callstatus == "failed" || call.new_callstatus == "failed_on_initiate")
                {
                    retVal.Stage = LiveTestCallStatus.LiveTestCallStage.failed;
                }
                else if (call.new_callstatus == "busy")
                {
                    retVal.Stage = LiveTestCallStatus.LiveTestCallStage.busy;
                }
                else if (call.new_callstatus == "no-answer")
                {
                    retVal.Stage = LiveTestCallStatus.LiveTestCallStage.no_answer;
                }
                else
                {
                    retVal.Stage = LiveTestCallStatus.LiveTestCallStage.completed;
                    if (!string.IsNullOrEmpty(call.new_dtmf) && call.new_dtmf != "NOTHING" && call.new_dtmf != "*" && call.new_dtmf != "0")
                    {
                        if (string.IsNullOrEmpty(call.new_customercallstatus))
                        {
                            retVal.Stage = LiveTestCallStatus.LiveTestCallStage.advertiser_confirmed;
                        }
                    }
                }
            }
            return retVal;
        }

        public void TestCallClosedInReport(Guid supplierId)
        {
            DataAccessLayer.GetStartedTestCall dal = new NoProblem.Core.DataAccessLayer.GetStartedTestCall(XrmDataContext);
            dal.CloseTestCallFromReport(supplierId);
        }

        public AdvDashboardLeadData GetTestCallForReport(Guid supplierId)
        {
            AdvDashboardLeadData data = null;
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string customerPhone = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.GET_STARTED_CALL_TEST_FAKE_CUSTOMER_NUMBER);
            DataAccessLayer.GetStartedTestCall dal = new NoProblem.Core.DataAccessLayer.GetStartedTestCall(XrmDataContext);
            var testCallData = dal.GetDoneTestCallOfSupplier(supplierId);
            if (testCallData != null)
            {
                data = new AdvDashboardLeadData();
                data.CreatedOn = testCallData.CreatedOn;
                data.CreatedOnSupplierTime = this.FixDateToLocal(data.CreatedOn, testCallData.TimeZoneCode);
                data.CustomerPhone = customerPhone;
                data.Description = "I need to hire your service for next week, near my neighborhood. Please call me. Thank you.";
                data.HeadingName = testCallData.HeadingIvrName;
                data.IncidentId = testCallData.Id;
                data.LeadNumber = "00001";
                data.RecordFileLocation = testCallData.RecordingUrl;
                data.Status = AdvDashboardLeadData.AdvDashboardLeadStatus.Won;
                data.Region = FixRegionName(testCallData.RegionsHirarchy);
            }
            return data;
        }
       
        public string GetStartedCallTestFakeCustomer()
        {
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            response.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.TEST_CALL_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + "fakecustomer" + AudioFilesConsts.WAV_FILE_EXTENSION);
            response.Pause(20);
            response.Hangup();
            return response.ToString();
        }

        public Guid StartTestCall(Guid supplierId)
        {
            DataAccessLayer.AccountExpertise accExpDal = new NoProblem.Core.DataAccessLayer.AccountExpertise(XrmDataContext);
            var headings = accExpDal.GetActiveByAccount(supplierId);
            Guid headingId = headings.First().new_new_primaryexpertise_new_accountexpertise.new_primaryexpertiseid;

            var regionId = FindRegionForSupplier(supplierId);

            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = accountRepositoryDal.Retrieve(supplierId);
            string supplierPhone = supplier.telephone1;

            //create test call in db
            DataAccessLayer.GetStartedTestCall dal = new NoProblem.Core.DataAccessLayer.GetStartedTestCall(XrmDataContext);
            var testCall = CreateTestCall(supplierId, regionId, headingId, supplierPhone, dal);

            TwilioBL.TwilioCallsExecuter executer = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioCallsExecuter(XrmDataContext);
            var twilioCall = executer.InitiateTestCall(supplierPhone, testCall.new_getstartedtestcallid);
            if (twilioCall != null || !String.IsNullOrEmpty(twilioCall.Sid))
            {
                //update call sid in testCall in db.
                testCall.new_callsid = twilioCall.Sid;
                testCall.new_callstatus = twilioCall.Status;
                dal.Update(testCall);
                XrmDataContext.SaveChanges();
                return testCall.new_getstartedtestcallid;
            }
            else
            {
                //update test call in db as not executed.
                testCall.new_callstatus = "failed_on_initiate";
                dal.Update(testCall);
                XrmDataContext.SaveChanges();
                return Guid.Empty;
            }
        }
               
        public string TestCallAnswered(
            string callSid, string accountSid, string from, string to,
            string callStatus, string direction, string testCallId)
        {
            DataAccessLayer.GetStartedTestCall dal = new NoProblem.Core.DataAccessLayer.GetStartedTestCall(XrmDataContext);
            DataModel.Xrm.new_getstartedtestcall testCall = new NoProblem.Core.DataModel.Xrm.new_getstartedtestcall(XrmDataContext);
            testCall.new_getstartedtestcallid = new Guid(testCallId);
            testCall.new_callstatus = callStatus;
            dal.Update(testCall);
            XrmDataContext.SaveChanges();
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
            Twilio.TwiML.TwilioResponse twilioResponse = new Twilio.TwiML.TwilioResponse();
            AddWelcomeGather(testCallId, audioFilesUrlBase, twilioResponse);
            return twilioResponse.ToString();
        }
                
        public string ClickHandler(string digits, string callSid, string testCallId)
        {
            DataAccessLayer.GetStartedTestCall dal = new NoProblem.Core.DataAccessLayer.GetStartedTestCall(XrmDataContext);
            DataModel.Xrm.new_getstartedtestcall testCall = new NoProblem.Core.DataModel.Xrm.new_getstartedtestcall(XrmDataContext);
            testCall.new_getstartedtestcallid = new Guid(testCallId);
            testCall.new_dtmf = digits;
            dal.Update(testCall);
            XrmDataContext.SaveChanges();
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
            Twilio.TwiML.TwilioResponse twilioResponse = new Twilio.TwiML.TwilioResponse();
            if (digits == "0")
            {
                twilioResponse.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.TEST_CALL_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + "reject" + AudioFilesConsts.WAV_FILE_EXTENSION);
                twilioResponse.Hangup();
            }
            else if (digits == "*")
            {
                AddWelcomeGather(testCallId, audioFilesUrlBase, twilioResponse);
            }
            else if (digits == "NOTHING")
            {
                twilioResponse.Hangup();
            }
            else
            {
                var customerNumber = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.GET_STARTED_CALL_TEST_FAKE_CUSTOMER_NUMBER);
                twilioResponse.Dial(customerNumber,
                    new
                    {
                        record = "true",
                        action = twilioCallBackUrlBase + "/TestCallConsumerCallback?TestCallId=" + testCallId + "&SiteId=" + siteId,
                        method = "GET"
                    });
            }
            return twilioResponse.ToString();
        }

        public void TestCallCallback(string callDuration, string callSid, string accountSid, string from, string to,
            string callStatus, string testCallId)
        {
            DataAccessLayer.GetStartedTestCall dal = new NoProblem.Core.DataAccessLayer.GetStartedTestCall(XrmDataContext);
            DataModel.Xrm.new_getstartedtestcall testCall = new NoProblem.Core.DataModel.Xrm.new_getstartedtestcall(XrmDataContext);
            testCall.new_getstartedtestcallid = new Guid(testCallId);
            testCall.new_callstatus = callStatus;
            testCall.new_callduration = callDuration;
            dal.Update(testCall);
            XrmDataContext.SaveChanges();
        }

        public void TestCallConsumerCallback(
            string dialCallStatus, string dialCallSid, string dialCallDuration, string recordingUrl, string testCallId)
        {
            DataAccessLayer.GetStartedTestCall dal = new NoProblem.Core.DataAccessLayer.GetStartedTestCall(XrmDataContext);
            DataModel.Xrm.new_getstartedtestcall testCall = dal.Retrieve(new Guid(testCallId));
            testCall.new_customercallstatus = dialCallStatus;
            testCall.new_customercallduration = dialCallDuration;
            testCall.new_recordingurl = recordingUrl + ".mp3";
            testCall.new_customercallsid = dialCallSid;

            int duration = int.Parse(dialCallDuration);
            if (duration > 1 && !string.IsNullOrEmpty(recordingUrl))
            {
                testCall.new_taskdone = true;
            }

            dal.Update(testCall);
            XrmDataContext.SaveChanges();

            if (testCall.new_taskdone.HasValue && testCall.new_taskdone.Value)
            {
                GetStartedTasksManager manager = new GetStartedTasksManager(XrmDataContext);
                manager.CrossOutGetStartedTask(testCall.new_accountid.Value, GetStartedTaskType.call_test);
            }
        }
        public void SetRegistrationHit(NoProblem.Core.DataModel.SupplierModel.Registration2014.LandedRegistraionPageRequest request)
        {
            string command = "EXEC [dbo].[InsertRegistrationHit] @Id, @IP, @UrlReferer, " +
                           "@Browser, @BrowserVersion, @date, @Flavor, @LandingPage";
            string _connectionString = ConfigurationManager.AppSettings["updateConnectionString"];
            
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@Id", request.ID);
                    cmd.Parameters.AddWithValue("@IP", (string.IsNullOrEmpty(request.IP) ? string.Empty : request.IP));
                    if (string.IsNullOrEmpty(request.UrlReferer))
                        cmd.Parameters.AddWithValue("@UrlReferer", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@UrlReferer", request.UrlReferer);
                    if (string.IsNullOrEmpty(request.Browser))
                        cmd.Parameters.AddWithValue("@Browser", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@Browser", request.Browser);
                    if (string.IsNullOrEmpty(request.BrowserVersion))
                        cmd.Parameters.AddWithValue("@BrowserVersion", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@BrowserVersion", request.BrowserVersion);
                    if(string.IsNullOrEmpty(request.Flavor))
                        cmd.Parameters.AddWithValue("@Flavor", DBNull.Value);
                    else
                        cmd.Parameters.AddWithValue("@Flavor", request.Flavor);
                    cmd.Parameters.AddWithValue("@LandingPage", request.LandingPage.ToString());
                    cmd.Parameters.AddWithValue("@date", request.date);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            
            
        }

        #endregion

        #region Private Methods

        private Guid FindRegionForSupplier(Guid supplierId)
        {
            Guid retVal = Guid.Empty;
            DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(XrmDataContext);
            DataAccessLayer.RegionAccount regAccDal = new NoProblem.Core.DataAccessLayer.RegionAccount(XrmDataContext);
            var regionAccounts = regAccDal.GetMinDataByAccountId(supplierId);

            var workings = regionAccounts.Where(x => x.RegionState == NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.Working).OrderByDescending(x => x.Level);

            if (workings.Count() > 0)
            {
                var firstWorking = workings.First();
                var current = regDal.Retrieve(firstWorking.RegionId);

                while (current.new_level < 4)
                {
                    var children = regDal.GetChildren(current.new_regionid);
                    var firstChild = children.FirstOrDefault();
                    if (firstChild != null)
                    {
                        current = firstChild;
                    }
                    else
                    {
                        break;
                    }
                }
                retVal = current.new_regionid;
            }
            else
            {
                var hasNotWorkingChildList = regionAccounts.Where(x => x.RegionState == NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.HasNotWorkingChild).OrderByDescending(x => x.Level);
                if (hasNotWorkingChildList.Count() > 0)
                {
                    var firstNotChild = hasNotWorkingChildList.First();
                    var current = regDal.Retrieve(firstNotChild.RegionId);
                    while (current.new_level < 4)
                    {
                        var children = regDal.GetChildren(current.new_regionid);
                        if (children.Count() > 0)
                        {
                            var firstChild = children.FirstOrDefault();
                            var thisLevelRegAccs = regionAccounts.Where(x => x.Level == firstChild.new_level.Value && x.RegionState == NoProblem.Core.DataModel.Xrm.new_regionaccountexpertise.RegionState.NotWorking);
                            foreach (var child in children)
                            {
                                bool found = false;
                                foreach (var item in thisLevelRegAccs)
                                {
                                    if (item.RegionId == child.new_regionid)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    current = child;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    retVal = current.new_regionid;
                }
            }
            return retVal;
        }

        private string FixRegionName(List<string> regions)
        {
            string retVal;
            if (regions.Count > 0)
            {
                if (regions.Count >= 4)
                {
                    retVal = regions[1] + ", " + regions[3] + " " + regions[0];
                }
                else if (regions.Count == 3)
                {
                    retVal = regions[0] + ", " + regions[2];
                }
                else if (regions.Count == 2)
                {
                    retVal = regions[0] + ", " + regions[1];
                }
                else
                {
                    retVal = regions[0];
                }
            }
            else
            {
                retVal = string.Empty;
            }
            return retVal;
        }

        private DataModel.Xrm.new_getstartedtestcall CreateTestCall(Guid supplierId, Guid regionId, Guid headingId, string supplierPhone, DataAccessLayer.GetStartedTestCall dal)
        {
            DataModel.Xrm.new_getstartedtestcall call = new NoProblem.Core.DataModel.Xrm.new_getstartedtestcall();
            call.new_accountid = supplierId;
            call.new_tophone = supplierPhone;
            call.new_primaryexpertiseid = headingId;
            call.new_regionid = regionId;
            dal.Create(call);
            XrmDataContext.SaveChanges();
            return call;
        }

        private void AddWelcomeGather(string testCallId, string audioFilesUrlBase, Twilio.TwiML.TwilioResponse twilioResponse)
        {
            twilioResponse.BeginGather(new
            {
                action = twilioCallBackUrlBase + "/TestCallClickHandler?TestCallId=" + testCallId + "&SiteId=" + siteId,
                timeout = 5,
                method = "GET",
                numDigits = 1,
                finishOnKey = string.Empty
            });
            twilioResponse.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.TEST_CALL_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + "welcome_pretext" + AudioFilesConsts.WAV_FILE_EXTENSION);
            twilioResponse.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.TEST_CALL_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + "fakedescription" + AudioFilesConsts.WAV_FILE_EXTENSION);
            twilioResponse.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.TEST_CALL_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + "welcome_posttext" + AudioFilesConsts.WAV_FILE_EXTENSION);
            twilioResponse.EndGather();
            twilioResponse.Redirect(twilioCallBackUrlBase + "/TestCallClickHandler?TestCallId=" + testCallId + "&SiteId=" + siteId + "&Digits=NOTHING", "GET");
        }

        #endregion
    }
}
