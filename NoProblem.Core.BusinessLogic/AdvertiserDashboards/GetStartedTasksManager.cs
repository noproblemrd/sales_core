﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.AdvertiserDashboards;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.AdvertiserDashboards
{
    public class GetStartedTasksManager : XrmUserBase
    {
        public GetStartedTasksManager(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        #region Public Methods

        public CrossOutGetStartedTaskResponse CrossOutGetStartedTask(Guid supplierId, GetStartedTaskType taskType)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            //DataModel.Xrm.account supplier = dal.Retrieve(supplierId);
            var query = from acc in dal.All
                        where acc.accountid == supplierId
                        select new DataModel.Xrm.account(XrmDataContext)
                        {
                            new_getstartedtask_calltest = acc.new_getstartedtask_calltest,
                            new_getstartedtask_defaultprice = acc.new_getstartedtask_defaultprice,
                            new_getstartedtask_honorcode = acc.new_getstartedtask_honorcode,
                            new_getstartedtask_listenrecording = acc.new_getstartedtask_listenrecording,
                            new_getstartedtask_watchvideo = acc.new_getstartedtask_watchvideo,
                            new_getstartedtask_workhours = acc.new_getstartedtask_workhours,
                            new_cashbalance = acc.new_cashbalance,
                            new_isemailverified= acc.new_isemailverified
                        };
            var supplier = query.First();
            supplier.accountid = supplierId;
            bool updated = false;
            switch (taskType)
            {
                case GetStartedTaskType.verify_email:
                    if (!(supplier.new_isemailverified.HasValue && supplier.new_isemailverified.Value))
                    {
                        supplier.new_isemailverified = true;
                        updated = true;
                    }
                    break;
                case GetStartedTaskType.default_price:
                    if (!(supplier.new_getstartedtask_defaultprice.HasValue && supplier.new_getstartedtask_defaultprice.Value))
                    {
                        supplier.new_getstartedtask_defaultprice = true;
                        updated = true;
                    }
                    break;
                case GetStartedTaskType.work_hours:
                    if (!(supplier.new_getstartedtask_workhours.HasValue && supplier.new_getstartedtask_workhours.Value))
                    {
                        supplier.new_getstartedtask_workhours = true;
                        supplier.new_accountcompleteness_workhours = true;
                        updated = true;
                    }
                    break;
                case GetStartedTaskType.listen_recording:
                    if (!(supplier.new_getstartedtask_listenrecording.HasValue && supplier.new_getstartedtask_listenrecording.Value))
                    {
                        supplier.new_getstartedtask_listenrecording = true;
                        updated = true;
                    }
                    break;
                case GetStartedTaskType.honor_code:
                    if (!(supplier.new_getstartedtask_honorcode.HasValue && supplier.new_getstartedtask_honorcode.Value))
                    {
                        supplier.new_getstartedtask_honorcode = true;
                        updated = true;
                    }
                    break;
                case GetStartedTaskType.call_test:
                    if (!(supplier.new_getstartedtask_calltest.HasValue && supplier.new_getstartedtask_calltest.Value))
                    {
                        supplier.new_getstartedtask_calltest = true;
                        updated = true;
                    }
                    break;
            }
            decimal balance = supplier.new_cashbalance.HasValue ? supplier.new_cashbalance.Value : 0;
            CrossOutGetStartedTaskResponse response = new CrossOutGetStartedTaskResponse();
            if (updated)
            {
                bool allDone = AllTasksDone(supplier);
                if (allDone)
                {
                    supplier.new_getstartedtasksfinishedon = DateTime.Now; 
                    response.AllTasksDone = true;
                }
                dal.Update(supplier);
                XrmDataContext.SaveChanges();
            }
            response.Balance = balance;
            response.TasksLeft = HowManyTasksLeft(supplier);
            return response;
        }

        public GetStartedTasksStatusResponse RetrieveGetStartedTasksStatus(Guid supplierId)
        {
            List<GetStartedTasksStatus> tasksList = new List<GetStartedTasksStatus>();
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = dal.Retrieve(supplierId);
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);

            GetStartedTasksStatusResponse response = new GetStartedTasksStatusResponse();
            if (GlobalConfigurations.IsUsaSystem)
            {
                bool defaultPriceDone = supplier.new_getstartedtask_defaultprice.HasValue ? supplier.new_getstartedtask_defaultprice.Value : false;
                var defaultPriceTask = CreateStatusInstance(
                         GetStartedTaskType.default_price,
                         "Define your default price.",
                         defaultPriceDone,
                         1);
                tasksList.Add(defaultPriceTask);
                response.DefaultPrice = defaultPriceTask;
            }
            else
            {
                var regTask = CreateStatusInstance(
                         GetStartedTaskType.register,
                         "Register as an advertiser.",
                         true,
                         1);
                tasksList.Add(regTask);
                response.Register = regTask;
            }

            bool emailVerifiedDone = supplier.new_isemailverified.HasValue ? supplier.new_isemailverified.Value : false;
            var emailTask = CreateStatusInstance(
                    GetStartedTaskType.verify_email,
                    String.Format( "Verify your email address - to get ${0} bonus!!!", GlobalConfigurations.GetRegistrationBonusAmount()),
                    emailVerifiedDone,
                    2);
            tasksList.Add(emailTask);
            response.VerifyEmail = emailTask;

            bool workHoursDone = supplier.new_getstartedtask_workhours.HasValue ? supplier.new_getstartedtask_workhours.Value : false;
            var hoursTask = CreateStatusInstance(
                    GetStartedTaskType.work_hours,
                    "Tell us when you want to receive calls.",
                    workHoursDone,
                    3);
            tasksList.Add(hoursTask);
            response.WorkHours = hoursTask;

            bool honorCodeDone = supplier.new_getstartedtask_honorcode.HasValue ? supplier.new_getstartedtask_honorcode.Value : false;
            var honorTask = CreateStatusInstance(
                    GetStartedTaskType.honor_code,
                    "Read NoProblem's Honor Code.",
                    honorCodeDone,
                    4);
            tasksList.Add(honorTask);
            response.HonorCode = honorTask;

            bool callTestDone = supplier.new_getstartedtask_calltest.HasValue ? supplier.new_getstartedtask_calltest.Value : false;
            var callTask = CreateStatusInstance(
                    GetStartedTaskType.call_test,
                    "Practice talking with a customer during a test call.",
                    callTestDone,
                    5);
            tasksList.Add(callTask);
            response.CallTest = callTask;
            if (!callTestDone)
            {
                response.CallerId = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.OUTBOUND_CALL_FROM_PHONE);
                response.SupplierPhone = supplier.telephone1;
            }

            bool listenRecordingDone = supplier.new_getstartedtask_listenrecording.HasValue ? supplier.new_getstartedtask_listenrecording.Value : false;
            var recordingTask = CreateStatusInstance(
                    GetStartedTaskType.listen_recording,
                    "Listen to your conversation with the customer.",
                    listenRecordingDone,
                    6);
            response.ListenRecording = recordingTask;
            tasksList.Add(recordingTask);

            response.TasksList = tasksList;
            response.TasksLeft = tasksList.Count(x => !x.Done);
            return response;
        }

        public void HonorCodePlusOne(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = dal.Retrieve(supplierId);
            supplier.new_honorcodereadcounter = supplier.new_honorcodereadcounter.HasValue ? supplier.new_honorcodereadcounter.Value + 1 : 1;
            dal.Update(supplier);
            XrmDataContext.SaveChanges();
        }

        public HonorCodeReadingStatus GetHonorCodeReadingStatus(Guid supplierId)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            DataModel.Xrm.account supplier = dal.Retrieve(supplierId);
            HonorCodeReadingStatus status = new HonorCodeReadingStatus();
            status.Counter = supplier.new_honorcodereadcounter.HasValue ? supplier.new_honorcodereadcounter.Value : 0;
            status.IsClickedDone = supplier.new_getstartedtask_honorcode.HasValue ? supplier.new_getstartedtask_honorcode.Value : false;
            return status;
        }

        #endregion

        #region Private Methods

        private static int HowManyTasksLeft(DataModel.Xrm.account supplier)
        {
            int tasksLeft = 0;
            tasksLeft += (!supplier.new_getstartedtask_calltest.HasValue || !supplier.new_getstartedtask_calltest.Value) ? 1 : 0;
            tasksLeft += (!supplier.new_getstartedtask_honorcode.HasValue || !supplier.new_getstartedtask_honorcode.Value) ? 1 : 0;
            tasksLeft += (!supplier.new_getstartedtask_listenrecording.HasValue || !supplier.new_getstartedtask_listenrecording.Value) ? 1 : 0;
            tasksLeft += (!supplier.new_getstartedtask_watchvideo.HasValue || !supplier.new_getstartedtask_watchvideo.Value) ? 1 : 0;
            tasksLeft += (!supplier.new_getstartedtask_workhours.HasValue || !supplier.new_getstartedtask_workhours.Value) ? 1 : 0;
            tasksLeft += (!supplier.new_isemailverified.HasValue || !supplier.new_isemailverified.Value) ? 1 : 0;
            return tasksLeft;
        }

        private GetStartedTasksStatus CreateStatusInstance(GetStartedTaskType type, string description, bool isDone, int rank)
        {
            GetStartedTasksStatus status = new GetStartedTasksStatus();
            status.Description = description;
            status.Done = isDone;
            status.Rank = rank;
            status.TaskType = type;
            return status;
        }

        private bool AllTasksDone(NoProblem.Core.DataModel.Xrm.account supplier)
        {           
            bool retVal =
                supplier.new_getstartedtask_listenrecording.HasValue && supplier.new_getstartedtask_listenrecording.Value
                && supplier.new_getstartedtask_calltest.HasValue && supplier.new_getstartedtask_calltest.Value
                && supplier.new_getstartedtask_workhours.HasValue && supplier.new_getstartedtask_workhours.Value
                && supplier.new_getstartedtask_watchvideo.HasValue && supplier.new_getstartedtask_watchvideo.Value
                && supplier.new_getstartedtask_honorcode.HasValue && supplier.new_getstartedtask_honorcode.Value;
            if (GlobalConfigurations.IsUsaSystem)
            {
                retVal = retVal && supplier.new_getstartedtask_defaultprice.HasValue && supplier.new_getstartedtask_defaultprice.Value;
            }
            return retVal;
        }

        #endregion
    }
}
