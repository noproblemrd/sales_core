﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.AdvertiserDashboards.BillingOverview;

namespace NoProblem.Core.BusinessLogic.AdvertiserDashboards
{
    public class BillingOverviewManager2014 : XrmUserBase
    {
        public BillingOverviewManager2014(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<TransactionRow2014> GetTransactions(Guid supplierId)
        {
            List<TransactionRow2014> retVal = new List<TransactionRow2014>();

            DataAccessLayer.AccountRepository.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@id", supplierId);

            DataAccessLayer.Generic dal = new DataAccessLayer.Generic();
            var reader = dal.ExecuteReader(queryTransactions, parameters);

            if (reader.Count > 0)
            {
                int timeZoneCode;
                var firstRow = reader.First();
                if (firstRow["timezone"] != DBNull.Value)
                {
                    timeZoneCode = (int)firstRow["timezone"];
                }
                else
                {
                    DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                    timeZoneCode = int.Parse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PUBLISHER_TIME_ZONE));
                }
                DateTime utcNow = DateTime.Now;
                DateTime localNow = FixDateToLocal(utcNow, timeZoneCode);
                var span = localNow - utcNow;

                foreach (var row in reader)
                {
                    DataModel.Xrm.new_balancerow.Action action = (DataModel.Xrm.new_balancerow.Action)((int)row["New_Action"]);
                    DateTime createdOn = (DateTime)row["CreatedOn"];
                    string description = Convert.ToString(row["New_Description"]);
                    TransactionRow2014 regularRow = new TransactionRow2014();
                    DateTime timeLocal = FixHourWithSpan(createdOn, span);
                    regularRow.DateLocal = timeLocal;
                    regularRow.DateServer = createdOn;
                    regularRow.Amount = (decimal)row["New_Amount"];
                    switch (action)
                    {
                        case DataModel.Xrm.new_balancerow.Action.Deposit:
                            regularRow.Type = "Deposit";
                            break;
                        case DataModel.Xrm.new_balancerow.Action.AutoRecharge:
                            regularRow.Type = "Auto recharge";
                            break;
                        case DataModel.Xrm.new_balancerow.Action.BonusCredit:
                            regularRow.Type = "Getting started bonus";
                            break;
                        case DataModel.Xrm.new_balancerow.Action.Call:
                            regularRow.Type = "Lead";
                            if (!string.IsNullOrEmpty(description))
                                regularRow.Type += " " + description;
                            break;
                        case DataModel.Xrm.new_balancerow.Action.CreditEarned:
                            regularRow.Type = "Earned from friend";
                            break;
                        case DataModel.Xrm.new_balancerow.Action.MonthlyFee:
                            regularRow.Type = "Monthly Fee";
                            regularRow.Amount = regularRow.Amount * -1;
                            break;
                        case DataModel.Xrm.new_balancerow.Action.MonthlyFeeRefund:
                            regularRow.Type = "Monthly Fee Refund";
                            break;
                        case DataModel.Xrm.new_balancerow.Action.Refund:
                            regularRow.Type = "Refund";
                            if (!string.IsNullOrEmpty(description))
                                regularRow.Type += " " + description;
                            break;
                        case DataModel.Xrm.new_balancerow.Action.TenLeadsBonus:
                            regularRow.Type = "10 leads cash-back";
                            break;
                    }
                    retVal.Add(regularRow);
                }
            }
            return retVal;
        }

        private DateTime FixHourWithSpan(DateTime d, TimeSpan span)
        {
            if (span.Hours == 0)
            {
                return d;
            }
            else
            {
                return d.AddHours(span.Hours);
            }
        }

        public GetAccountActivityResponse2014 GetAccountActivity(Guid supplierId)
        {
            GetAccountActivityResponse2014 response = new GetAccountActivityResponse2014();

            DataAccessLayer.AccountRepository accountRepositoryDal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = (from acc in accountRepositoryDal.All
                            where acc.accountid == supplierId
                            select new { acc.address1_utcoffset }).First();

            int timeZoneCode;
            if (supplier.address1_utcoffset.HasValue)
            {
                timeZoneCode = supplier.address1_utcoffset.Value;
            }
            else
            {
                DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                timeZoneCode = int.Parse(config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PUBLISHER_TIME_ZONE));
            }

            DateTime now = DateTime.Now;
            DateTime firstDay = new DateTime(now.Year, now.Month, 1);
            DateTime lastDay = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));
            firstDay = FixDateToLocal(firstDay, timeZoneCode);
            lastDay = FixDateToLocal(lastDay, timeZoneCode);

            response.Month = now.Month;

            DataAccessLayer.AccountRepository.SqlParametersList parameters = new DataAccessLayer.Generic.SqlParametersList();
            parameters.Add("@id", supplierId);
            parameters.Add("@from", firstDay);
            parameters.Add("@to", lastDay);

            var reader = accountRepositoryDal.ExecuteReader(queryAccountActivity, parameters);

            foreach (var row in reader)
            {
                response.LeadCount = (int)row["leadCount"];
                response.LeadMoney = (decimal)row["leadMoney"] * -1;
                response.RefundCount = (int)row["refundCount"];
                response.RefundMoney = (decimal)row["refundMoney"] * -1;
                response.EarnedFromFriendCount = (int)row["earnedCount"];
                response.EarnedFromFriendMoney = (decimal)row["earnedMoney"];
                response.GetStartedBonusCount = (int)row["bonusGetStartedCount"];
                response.GetStartedBonusMoney = (decimal)row["bonusGetStartedMoney"];
                response.TenLeadsBonusCount = (int)row["tenLeadsBonusCount"];
                response.TenLeadsBonusMoney = (decimal)row["tenLeadBonusMoney"];
                response.MonthlyFee = (decimal)row["monthlyFeeMoney"];
                response.MonthlyFeeRefund = (decimal)row["monthlyFeeRefundMoney"];
                response.TotalUsed = response.LeadMoney + response.RefundMoney + response.MonthlyFee;
                response.TotalBonus = response.EarnedFromFriendMoney + response.GetStartedBonusMoney + response.TenLeadsBonusMoney + response.MonthlyFeeRefund;
                break;
            }

            return response;
        }

        private const string queryTransactions =
            @"
declare @timeZone int
select top 1 @timeZone = address1_utcoffset from account with(nolock) where accountid = @id

If OBJECT_ID('tempdb..#QueryTransactionsTempTable') is not Null
Drop Table #QueryTransactionsTempTable
Create Table #QueryTransactionsTempTable 
				(New_Amount money null
				,New_Action int null
				,CreatedOn datetime null
				,New_Description nvarchar(200) Null
				,timezone int null)

Insert into #QueryTransactionsTempTable	
select top 400 br.New_Amount, br.New_Action, br.CreatedOn, br.New_Description, @timeZone as timezone
                from new_balancerow br with(nolock)
                where
                              br.deletionstatecode = 0
                              and br.new_accountid = @id
                              and (br.new_incidentaccountid is null
                                    or br.new_incidentaccountid not in
                                          (select new_incidentaccountid from new_balancerow br2 with(nolock)
                                                where
                                                br2.deletionstatecode = 0
                                                and br2.new_accountid = @id
                                                and br2.new_incidentaccountid is not null
                                                and br2.new_action = 4
                                          )
                              )
Select New_Amount, CreatedOn, New_Description, timezone, New_Action
From #QueryTransactionsTempTable
order by createdon desc
";

        private const string queryAccountActivity =
            @"
declare @leadCount int
declare @leadMoney decimal

select
@leadCount = count(*)
, @leadMoney = sum(new_amount)
                from new_balancerow br with(nolock)
                where br.deletionstatecode = 0
 and br.new_accountid = @id
and br.createdon between @from and @to
and br.new_action = 2
and br.new_incidentaccountid not in
               (select new_incidentaccountid from new_balancerow with(nolock)
				where
				br.deletionstatecode = 0
				and br.new_accountid = @id
				and br.createdon between @from and @to
				and br.new_action = 4)


declare @refundCount int
declare @refundMoney decimal

select
@refundCount = count(*)
, @refundMoney = sum(new_amount)
                from new_balancerow br with(nolock)
                where br.deletionstatecode = 0
 and br.new_accountid = @id
and br.createdon between @from and @to
and br.new_action = 3
and br.new_incidentaccountid not in
               (select new_incidentaccountid from new_balancerow with(nolock)
				where
				br.deletionstatecode = 0
				and br.new_accountid = @id
				and br.createdon between @from and @to
				and br.new_action = 4)
				
declare @MonthlyFeeMoney decimal

select
@MonthlyFeeMoney = SUM(new_amount)
	from new_balancerow br with(nolock)
                where br.deletionstatecode = 0
 and br.new_accountid = @id
and br.createdon between @from and @to
and br.new_action = 9

declare @MonthlyFeeRefundMoney decimal

select
@MonthlyFeeRefundMoney = SUM(new_amount)
	from new_balancerow br with(nolock)
                where br.deletionstatecode = 0
 and br.new_accountid = @id
and br.createdon between @from and @to
and br.new_action = 10

declare @TenLeadsBonusMoney decimal
declare @TenLeadsBonusCount int

select
@TenLeadsBonusCount = COUNT(*),
@TenLeadsBonusMoney = SUM(new_amount)
	from new_balancerow br with(nolock)
                where br.deletionstatecode = 0
 and br.new_accountid = @id
and br.createdon between @from and @to
and br.new_action = 8

declare @earnedCount int
declare @earnedMoney decimal

select 
@earnedCount = count(*)
,@earnedMoney = sum(new_amount)
                from new_balancerow br with(nolock)
                where br.deletionstatecode = 0
                and br.new_accountid = @id
				and br.createdon between @from and @to
				and br.new_action = 6

declare @bonusGetStartedCount int
declare @bonusGetStartedMoney decimal

select 
@bonusGetStartedCount = count(*)
,@bonusGetStartedMoney = sum(new_amount)
                from new_balancerow br with(nolock)
                join new_supplierpricing sp with(nolock) on sp.new_supplierpricingid = br.new_supplierpricingid
                where br.deletionstatecode = 0 and sp.deletionstatecode = 0
                and sp.new_accountid = @id
				and br.new_action = 7
				and br.createdon between @from and @to

select 
@leadCount leadCount, ISNULL(@leadMoney, 0) leadMoney, 
@refundCount refundCount,  ISNULL(@refundMoney, 0) refundMoney, 
@earnedCount earnedCount, ISNULL(@earnedMoney, 0) earnedMoney,
@bonusGetStartedCount bonusGetStartedCount, ISNULL(@bonusGetStartedMoney, 0) bonusGetStartedMoney
, @TenLeadsBonusCount tenLeadsBonusCount, ISNULL(@TenLeadsBonusMoney, 0) tenLeadBonusMoney
,ISNULL(@MonthlyFeeMoney, 0) monthlyFeeMoney, ISNULL(@MonthlyFeeRefundMoney, 0) monthlyFeeRefundMoney
";
    }
}
