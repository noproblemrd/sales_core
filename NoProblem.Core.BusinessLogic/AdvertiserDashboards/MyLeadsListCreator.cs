﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.AdvertiserDashboards;

namespace NoProblem.Core.BusinessLogic.AdvertiserDashboards
{
    public class MyLeadsListCreator : XrmUserBase
    {
        public MyLeadsListCreator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public GetMyLeadsResponse GetMyLeadsList(Guid supplierId, MyLeadsListPeriod period, MyLeadsListSortBy sortBy, bool sortDescending)
        {
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));

            string query =
                @"
                declare @timeZone int
                declare @recordCalls bit
                declare @isBroker bit
                declare @disableBrokerRefundsRights bit
                select top 1 @timeZone = address1_utcoffset, @recordCalls = new_recordcalls, @isBroker = new_isleadbuyer, @disableBrokerRefundsRights = new_disablebrokerrefundsrights from account with(nolock) where accountid = @supplierId

               select 
                top 400 
                ia.createdon
                , exp.new_ivrname
                , ia.new_recordfilelocation
                , inc.new_webdescription as description
                , inc.new_telephone1
                , case ia.statuscode when 16 then (case ia.new_isrejected when 1 then 'rejected' else 'lost' end) else 
                    (case ia.new_refundstatus when 3 then 'refunded' else 
                        (case when ia.new_recordfilelocation is null then 'wonNoAudio' else 'won' end) end) end as status
                , inc.ticketnumber
                , @timeZone as timeZoneCode
                , @recordCalls as recordCalls
                , @isBroker as isBroker
                , @disableBrokerRefundsRights as disableBrokerRefundsRights
				, rg1.new_name rg1
				, rg2.new_name rg2
				, rg3.new_name rg3
				, rg4.new_name rg4
                , inc.incidentid
                , ia.new_refundprocesseddate
                , ia.new_potentialincidentprice
                , inc.new_winningprice
                , ia.new_tocharge

                from new_incidentaccount ia with(nolock)
                join incident inc with(nolock) on ia.new_incidentid = inc.incidentid
                join new_primaryexpertise exp on inc.new_primaryexpertiseid = exp.new_primaryexpertiseid
				left join New_region rg1 with(nolock) on inc.new_regionid = rg1.New_regionId
                left join New_region rg2 with(nolock) on rg1.new_parentregionid = rg2.new_regionid
                left join New_region rg3 with(nolock) on rg2.new_parentregionid = rg3.new_regionid
                left join New_region rg4 with(nolock) on rg3.new_parentregionid = rg4.new_regionid

                where ia.deletionstatecode = 0 and inc.deletionstatecode = 0
                and ia.new_accountid = @supplierId
                and (ia.statuscode = 10 or ia.statuscode = 12 or ia.statuscode = 13 or ia.statuscode = 16)
                ";
            if (period != MyLeadsListPeriod.All)
            {
                query += " and ia.createdon > @fromDate ";
                DateTime fromDate;
                switch (period)
                {
                    case MyLeadsListPeriod.Month:
                        fromDate = DateTime.Now.AddMonths(-1);
                        break;
                    case MyLeadsListPeriod.Week:
                        fromDate = DateTime.Now.AddDays(-7);
                        break;
                    case MyLeadsListPeriod.Year:
                        fromDate = DateTime.Now.AddYears(-1);
                        break;
                    default:
                        fromDate = DateTime.Now;
                        break;
                }
                parameters.Add(new KeyValuePair<string, object>("@fromDate", fromDate));
            }
            switch (sortBy)
            {
                case MyLeadsListSortBy.Dates:
                    query += " order by ia.createdon ";
                    break;
                case MyLeadsListSortBy.Heading:
                    query += " order by inc.new_primaryexpertiseidname, ia.createdon ";
                    break;
                case MyLeadsListSortBy.Status:
                    query += " order by status, ia.createdon ";
                    break;
            }
            if (sortDescending)
            {
                query += " desc ";
            }
            GetMyLeadsResponse response = new GetMyLeadsResponse();
            List<AdvDashboardLeadData> dataList = new List<AdvDashboardLeadData>();
            DataAccessLayer.ConfigurationSettings dal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            var reader = dal.ExecuteReader(query, parameters);
            if (reader.Count > 0)
            {
                int timeZoneCode;
                var firstRow = reader.First();
                if (firstRow["timeZoneCode"] != DBNull.Value)
                {
                    timeZoneCode = (int)firstRow["timeZoneCode"];
                }
                else
                {
                    timeZoneCode = int.Parse(dal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PUBLISHER_TIME_ZONE));
                }
                bool recordCalls = false;
                if (firstRow["recordCalls"] != DBNull.Value)
                {
                    recordCalls = (bool)firstRow["recordCalls"];
                }
                response.IsRecordsCalls = recordCalls;
                response.IsLeadBroker = firstRow["isBroker"] != DBNull.Value ? (bool)firstRow["isBroker"] : false;
                if (response.IsLeadBroker)
                {
                    bool disableRefundRights = firstRow["disableBrokerRefundsRights"] != DBNull.Value ? (bool)firstRow["disableBrokerRefundsRights"] : false;
                    if (disableRefundRights)
                    {
                        response.IsLeadBroker = false;
                    }
                }
                DateTime utcNow = DateTime.Now;
                DateTime localNow = FixDateToLocal(utcNow, timeZoneCode);
                var span = localNow - utcNow;
                foreach (var row in reader)
                {
                    AdvDashboardLeadData lead = new AdvDashboardLeadData();
                    lead.CreatedOn = (DateTime)row["createdon"];
                    if (span.Hours == 0)
                    {
                        lead.CreatedOnSupplierTime = lead.CreatedOn;
                    }
                    else
                    {
                        lead.CreatedOnSupplierTime = lead.CreatedOn.AddHours(span.Hours);
                    }
                    lead.Description = Convert.ToString(row["description"]);
                    lead.HeadingName = Convert.ToString(row["new_ivrname"]);
                    lead.RecordFileLocation = Convert.ToString(row["new_recordfilelocation"]);
                    string status = (string)row["status"];
                    switch (status)
                    {
                        case "rejected":
                            lead.Status = AdvDashboardLeadData.AdvDashboardLeadStatus.Rejected;
                            break;
                        case "lost":
                            lead.Status = AdvDashboardLeadData.AdvDashboardLeadStatus.Lost;
                            break;
                        case "refunded":
                            lead.Status = AdvDashboardLeadData.AdvDashboardLeadStatus.Refunded;
                            break;
                        case "wonNoAudio":
                            lead.Status = AdvDashboardLeadData.AdvDashboardLeadStatus.WonNoAudio;
                            break;
                        case "won":
                            lead.Status = AdvDashboardLeadData.AdvDashboardLeadStatus.Won;
                            break;
                    }
                    lead.Charged = row["new_tocharge"] != DBNull.Value ? (bool)row["new_tocharge"] : false;
                    lead.CustomerPhone = Convert.ToString(row["new_telephone1"]);
                    //(lead.Status != AdvDashboardLeadData.AdvDashboardLeadStatus.Lost
                    //&& lead.Status != AdvDashboardLeadData.AdvDashboardLeadStatus.Rejected) 
                    //? Convert.ToString(row["new_telephone1"]) 
                    //: string.Empty;
                    if (lead.Status == AdvDashboardLeadData.AdvDashboardLeadStatus.Lost)
                    {
                        lead.MyPrice = (int)(decimal)row["new_potentialincidentprice"];
                        int winningPrice = row["new_winningprice"] != DBNull.Value ? (int)(decimal)row["new_winningprice"] : 0;
                        //winning price must be higher than given price if he lost.
                        if (winningPrice <= lead.MyPrice)
                        {
                            winningPrice = lead.MyPrice + 1;
                        }
                        lead.WinningPrice = winningPrice;
                    }
                    else if (lead.Status == AdvDashboardLeadData.AdvDashboardLeadStatus.Refunded && row["new_refundprocesseddate"] != DBNull.Value)
                    {
                        lead.RefundedOn = ((DateTime)row["new_refundprocesseddate"]).AddHours(span.Hours);
                    }
                    List<string> regions = new List<string>();
                    for (int i = 1; i <= 4; i++)
                    {
                        string rg = Convert.ToString(row["rg" + i]);
                        if (!string.IsNullOrEmpty(rg))
                        {
                            regions.Add(rg);
                        }
                        else
                        {
                            break;
                        }
                    }
                    lead.Region = FixRegionName(regions);
                    lead.LeadNumber = Convert.ToString(row["ticketnumber"]);
                    lead.IncidentId = (Guid)row["incidentid"];
                    dataList.Add(lead);
                }
            }
            else
            {
                //execute query again withou date filtering to see if there are leads at all.
                string countQuery =
                    @"
                declare @recordCalls bit
                select top 1 @recordCalls = new_recordcalls from account with(nolock) where accountid = @supplierId

                select count(*) as count, @recordCalls as recordCalls from new_incidentaccount ia with(nolock)
                where ia.deletionstatecode = 0
                and ia.new_accountid = @supplierId
                and (ia.statuscode = 10 or ia.statuscode = 12 or ia.statuscode = 13 or ia.statuscode = 16)";
                List<KeyValuePair<string, object>> countParameters = new List<KeyValuePair<string, object>>();
                countParameters.Add(new KeyValuePair<string, object>("@supplierId", supplierId));
                reader = dal.ExecuteReader(countQuery, countParameters);
                if (reader.Count > 0)
                {
                    var firstRow = reader.First();
                    response.LeadsExist = (int)firstRow["count"] > 0;
                    bool recordCalls = false;
                    if (firstRow["recordCalls"] != DBNull.Value)
                    {
                        recordCalls = (bool)firstRow["recordCalls"];
                    }
                    response.IsRecordsCalls = recordCalls;
                }
            }
            response.LeadsList = dataList;
            return response;
        }

        private string FixRegionName(List<string> regions)
        {
            string retVal;
            if (regions.Count > 0)
            {
                if (regions.Count >= 4)
                {
                    retVal = regions[1] + ", " + regions[3] + " " + regions[0];
                }
                else if (regions.Count == 3)
                {
                    retVal = regions[0] + ", " + regions[2];
                }
                else if (regions.Count == 2)
                {
                    retVal = regions[0] + ", " + regions[1];
                }
                else
                {
                    retVal = regions[0];
                }
            }
            else
            {
                retVal = string.Empty;
            }
            return retVal;
        }
    }
}
