﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.AdvertiserDashboards;

namespace NoProblem.Core.BusinessLogic.AdvertiserDashboards
{
    public class PhoneAvailabilityCalculator : XrmUserBase
    {
        public PhoneAvailabilityCalculator(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public PhoneAvailabilityData GetPhoneAvailabilityData(Guid supplierId)
        {
            PhoneAvailabilityData retVal = new PhoneAvailabilityData();
            DataAccessLayer.AccountRepository accountRepositoryDal = new DataAccessLayer.AccountRepository(XrmDataContext);
            var supplier = (from acc in accountRepositoryDal.All
                            where acc.accountid == supplierId
                            select new { OnDuty = acc.new_ondutyrequests, OffDuty = acc.new_offdutyrequests }).First();
            if (supplier.OnDuty.HasValue)
            {
                retVal.LeadsOn = supplier.OnDuty.Value;
            }
            if (supplier.OffDuty.HasValue)
            {
                retVal.LeadOff = supplier.OffDuty.Value;
            }
            retVal.PercentageOfWeeklyHours = CalculatePercentageOfWeeklyHours(supplierId);
            return retVal;
        }

        private int CalculatePercentageOfWeeklyHours(Guid supplierId)
        {
            DataAccessLayer.Availability hoursDal = new DataAccessLayer.Availability(XrmDataContext);
            var query = from x in hoursDal.All
                        where x.new_accountid == supplierId
                        select new { x.new_from, x.new_to };
            int hoursCount = 0;
            foreach (var item in query)
            {
                AddDayHours(ref hoursCount, item.new_from, item.new_to);
            }
            int percentage = (int)((double)hoursCount / 168 * 100);
            percentage = Math.Min(percentage, 100);
            return percentage;
        }

        private void AddDayHours(ref int hoursCount, string fromStr, string toStr)
        {
            if (!String.IsNullOrEmpty(fromStr) && !String.IsNullOrEmpty(toStr))
            {
                string[] fromSplit = fromStr.Split(':');
                int fromHour = int.Parse(fromSplit[0]);                
                string[] toSplit = toStr.Split(':');
                int toHour = int.Parse(toSplit[0]);
                int toMinute = int.Parse(toSplit[1]);
                if (toMinute == 59)
                {
                    toHour++;
                }
                hoursCount += (toHour - fromHour);
            }
        }
    }
}
