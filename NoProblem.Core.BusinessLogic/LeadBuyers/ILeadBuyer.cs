﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    interface ILeadBuyer
    {
        SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId);

        GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia);
    }

    public class SendLeadResponse
    {
        public bool IsSold { get; set; }

        private string reason;
        public string Reason
        {
            get
            {
                return reason;
            }
            set
            {
                string tmp = value;
                if (tmp.Length > 2000)
                {
                    tmp = tmp.Substring(0, 2000);
                }
                reason = tmp;
            }
        }

        public string BrokerId { get; set; }
        public bool IsInCall { get; set; }
        public bool MarkAsRejected { get; set; }
        public bool FailedInnerValidation { get; set; }

        public SendLeadResponse()
        {
            IsSold = false;
            IsInCall = false;
            MarkAsRejected = false;
            FailedInnerValidation = false;
        }
    }

    public class GetBrokerBidResponse : SendLeadResponse
    {
        public decimal Price { get; set; }
        public bool WantsToBid { get; set; }
        public bool IsPredefinedPrice { get; set; }

        public GetBrokerBidResponse()
        {
            WantsToBid = true;
        }
    }
}
