﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    interface IPingPostApiBuyer
    {
        PingResult Ping(PingData data);

        PostResult Post(PostData data);
    }

    public class PingData
    {
        public string Id { get; set; }
        public string Zip { get; set; }
        public string CategoryCode { get; set; }
        public string ExternalCode { get; set; }
        public decimal DefaultPrice { get; set; }
    }

    public class PingResult
    {
        public bool Accepted { get; set; }
        public string PingId { get; set; }
        public decimal PingPrice { get; set; }
        public string PingMessage { get; set; }
    }

    public class PostData
    {
        public string Id { get; set; }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = GetNonNullString(value); }
        }

        public string Zip { get; set; }
        public string ExternalCode { get; set; }
        public string CategoryCode { get; set; }
        public string PingId { get; set; }
        public string Phone { get; set; }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = GetNonNullString(value); }
        }

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = GetNonNullString(value); }
        }

        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = GetNonNullString(value);
            }
        }

        private string streetAddress;
        public string StreetAddress
        {
            get
            {
                return streetAddress;
            }
            set
            {
                streetAddress = GetNonNullString(value);
            }
        }

        private string ip;
        public string IP
        {
            get
            {
                return ip;
            }
            set
            {

                ip = GetNonNullString(value);
            }
        }

        private string GetNonNullString(string value)
        {
            return value != null ? value : String.Empty;
        }
    }

    public class PostResult
    {
        public bool Accepted { get; set; }
        public string PostMessage { get; set; }
        public string PostId { get; set; }
    }
}
