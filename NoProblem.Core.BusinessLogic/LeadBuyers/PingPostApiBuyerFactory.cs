﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class PingPostApiBuyerFactory
    {
        public static IPingPostApiBuyer GetInstance(string leadBuyerName)
        {
            if (String.IsNullOrEmpty(leadBuyerName))
            {
                return null;
            }
            switch (leadBuyerName.ToLower())
            {
                case "networx":
                    return new Networx();
                case "fourlegalleadspingpost":
                    return new FourLegalLeadsPingPost();
                case "oconco":
                    return new Oconco();
                case "thehomefixers":
                    return new TheHomeFixers();
                case "testerbid":
                    return new TesterBid();
                case "homeimprovementleads":
                    return new HomeImprovementLeads();
                case "triares":
                    return new Triares();
                case "elocaldynamicprice":
                    return new ElocalDynamicPrice();
                case "prolegalleads":
                    return new ProLegalLeads();
                case "legalbrandmarketingpingpost":
                    return new NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing.PingPostApi.LegalBrandMarketingPingPost();
                default:
                    return null;
            }
        }
    }
}
