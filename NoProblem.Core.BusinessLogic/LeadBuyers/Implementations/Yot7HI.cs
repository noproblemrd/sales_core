﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations
{
    class Yot7HI : LeadBuyerBase
    {
        private const string URL = "https://network.yot7.com/genericPostlead.php";

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                if (!Validate(incident))
                {
                    retVal.Reason = validationMessage;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                var contact = incident.incident_customer_contacts;
                RestSharp.IRestClient client = new RestSharp.RestClient(URL);
                RestSharp.IRestRequest req = new RestSharp.RestRequest(RestSharp.Method.POST);
                req.AddParameter("TYPE", 19);
                req.AddParameter("IP_Address", incident.new_ip);
                req.AddParameter("SRC", incident.incidentid);
                req.AddParameter("Landing_Page", "NoProblemPPC");
                req.AddParameter("First_Name", contact.firstname);
                req.AddParameter("Last_Name", contact.lastname);
                req.AddParameter("Address", contact.address1_line1);
                req.AddParameter("Zip", incident.new_new_region_incident.new_name);
                req.AddParameter("Email", contact.emailaddress1);
                req.AddParameter("Home_Phone", incident.new_telephone1);
                req.AddParameter("Project", accExp.new_externalcode);
                if (IsTest())
                {
                    req.AddParameter("Test_Lead", 1);
                }
                var res = client.Execute(req);
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    XElement doc;
                    try
                    {
                        doc = XElement.Parse(res.Content);
                    }
                    catch (Exception exc)
                    {
                        retVal.MarkAsRejected = true;
                        throw new Exception(String.Format("Failed to parse Yot7 response. Parser error = '{1}'. Content = '{0}'", res.Content, exc.Message));
                    }
                    string status = doc.Element("status").Value;
                    if (status == "Matched")
                    {
                        retVal.IsSold = true;
                        retVal.BrokerId = doc.Element("lead_id").Value;
                    }
                    else
                    {
                        retVal.MarkAsRejected = true;
                        if (status == "Error")
                        {
                            retVal.Reason = String.Format("Rejected at POST with status '{0}'. With error: '{1}'", status, doc.Element("error").Value);
                        }
                        else
                        {
                            retVal.Reason = String.Format("Rejected at POST with status '{0}'", status);
                            retVal.BrokerId = doc.Element("lead_id").Value;
                        }
                    }
                }
                else//bad http code.
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = String.Format("Bad HTTP status in POST. Status = '{0}'.", res.StatusCode);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Yot7HI.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            ValidateRegion(incident);
            ValidateEmail(incident);
            ValidateAddress(incident);
            ValidateFirstAndLastName(incident);
            ValidateIp(incident);
        }
    }
}
