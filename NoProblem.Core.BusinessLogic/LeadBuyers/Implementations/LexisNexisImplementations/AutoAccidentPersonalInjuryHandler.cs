﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LexisNexisImplementations
{
    internal class AutoAccidentPersonalInjuryHandler : LexisNexisBase
    {
        private const string url = "http://lawyerlocator.lawyers.com/adcng/legal/auto-accidents/rfqhp/?publisherId=51299&publisherTypeId=1798&keywordID=";

        protected override  bool Validate(SendLeadResponse retVal, NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.account supplier, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc)
        {
            DataModel.Xrm.new_personalinjurycasedata piData;
            if (incident.new_personalinjurycasedataid.HasValue)
            {
                piData = incident.new_personalinjurycasedata_incident;
                if (piData.new_doesanyonehasinsurance.HasValue && piData.new_doesanyonehasinsurance.Value == (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.No)
                {
                    retVal.Reason = "No one has insurance, can't sell to Lexis Nexis";
                    return false;
                }
                if (piData.new_medicalbills.HasValue && piData.new_medicalbills.Value == (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.noMedicalBills)
                {
                    retVal.Reason = "No medical bills, can't sell to Lexis Nexis";
                    return false;
                }
                if (piData.new_currentlyrepresented.HasValue && piData.new_currentlyrepresented.Value)
                {
                    retVal.Reason = "Already represented by an attorney, can't sell to Lexis Nexis";
                    return false;
                }
            }
            else
            {
                retVal.Reason = "No special cateogry fields found, can't sell to Lexis Nexis";
                return false;
            }
            return true;
        }

        protected override void AddHeadingSpecificParams(NoProblem.Core.DataModel.Xrm.incident incident, Utils.HttpClient client)
        {
            NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata piData;
            if (incident.new_personalinjurycasedataid.HasValue)
            {
                piData = incident.new_personalinjurycasedata_incident;
            }
            else
            {
                piData = new NoProblem.Core.DataModel.Xrm.new_personalinjurycasedata();
            }
            client.ParamItems.Add(AutoAccidentConsts.ParamNames.ADDRESS_STREETNAME, AutoAccidentConsts.ValuesConsts.NA);
            client.ParamItems.Add(AutoAccidentConsts.ParamNames.NEWSLETTERSUBSCRIBE, AutoAccidentConsts.ValuesConsts.OFF);
            client.ParamItems.Add(AutoAccidentConsts.ParamNames.KEYWORDID, string.Empty);
            client.ParamItems.Add(AutoAccidentConsts.ParamNames.ISLNNUMBER, string.Empty);
            client.ParamItems.Add(AutoAccidentConsts.ParamNames._QUESTIONSETVERSIONID_, AutoAccidentConsts.ValuesConsts.QUESTIONS_1);
            client.ParamItems.Add(AutoAccidentConsts.ParamNames.__DOES_ANYONE_HAS_INSURANCE_ISREQUIRED, AutoAccidentConsts.ValuesConsts.TRUE);
            if (piData.new_doesanyonehasinsurance.HasValue)
            {
                switch (piData.new_doesanyonehasinsurance.Value)
                {
                    case (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.Yes:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.DOES_ANYONE_HAS_INSURANCE, AutoAccidentConsts.ValuesConsts.HAS_INSURANCE_YES);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.No:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.DOES_ANYONE_HAS_INSURANCE, AutoAccidentConsts.ValuesConsts.HAS_INSURANCE_NO);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.AnyoneHasInsurance.DontKnow:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.DOES_ANYONE_HAS_INSURANCE, AutoAccidentConsts.ValuesConsts.HAS_INSURANCE_DONT_KNOW);
                        break;
                }
            }
            else
            {
                client.ParamItems.Add(AutoAccidentConsts.ParamNames.DOES_ANYONE_HAS_INSURANCE, AutoAccidentConsts.ValuesConsts.HAS_INSURANCE_YES);
            }
            client.ParamItems.Add(AutoAccidentConsts.ParamNames.__ESTIMATED_MEDICAL_BILLS_ISREQUIRED, AutoAccidentConsts.ValuesConsts.TRUE);
            if (piData.new_medicalbills.HasValue)
            {
                switch (piData.new_medicalbills.Value)
                {
                    case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.dontKnow:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.ESTIMATED_MEDICAL_BILLS, AutoAccidentConsts.ValuesConsts.ESTIMATED_MEDICAL_BILLS_DONT_KNOW);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e10kTo25k:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.ESTIMATED_MEDICAL_BILLS, AutoAccidentConsts.ValuesConsts.ESTIMATED_MEDICAL_BILLS_10000_TO_25000);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e1kTo5k:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.ESTIMATED_MEDICAL_BILLS, AutoAccidentConsts.ValuesConsts.ESTIMATED_MEDICAL_BILLS_1000_TO_5000);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e25kTo100k:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.ESTIMATED_MEDICAL_BILLS, AutoAccidentConsts.ValuesConsts.ESTIMATED_MEDICAL_BILLS_25000_TO_100000);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.e5kTo10k:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.ESTIMATED_MEDICAL_BILLS, AutoAccidentConsts.ValuesConsts.ESTIMATED_MEDICAL_BILLS_5000_TO_10000);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.lessThan1k:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.ESTIMATED_MEDICAL_BILLS, AutoAccidentConsts.ValuesConsts.ESTIMATED_MEDICAL_BILLS_LESS_THAN_1000);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.moreThan100k:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.ESTIMATED_MEDICAL_BILLS, AutoAccidentConsts.ValuesConsts.ESTIMATED_MEDICAL_BILLS_MORE_THAN_100000);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.EstimatedMedicalBills.noMedicalBills:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.ESTIMATED_MEDICAL_BILLS, AutoAccidentConsts.ValuesConsts.ESTIMATED_MEDICAL_BILLS_NO_MEDICAL_BILLS);
                        break;
                }
            }
            else
            {
                client.ParamItems.Add(AutoAccidentConsts.ParamNames.ESTIMATED_MEDICAL_BILLS, AutoAccidentConsts.ValuesConsts.ESTIMATED_MEDICAL_BILLS_DONT_KNOW);
            }
            client.ParamItems.Add(AutoAccidentConsts.ParamNames.RESULT_OF_ACCIDENT_ISREQUIRED, AutoAccidentConsts.ValuesConsts.TRUE);
            if (piData.new_accidentresult.HasValue)
            {
                switch (piData.new_accidentresult.Value)
                {
                    case (int)DataModel.Xrm.new_personalinjurycasedata.AccidentResult.hospitalization:
                    case (int)DataModel.Xrm.new_personalinjurycasedata.AccidentResult.medicalTreatment:
                    case (int)DataModel.Xrm.new_personalinjurycasedata.AccidentResult.surgery:
                    case (int)DataModel.Xrm.new_personalinjurycasedata.AccidentResult.missedWork:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.RESULT_OF_ACCIDENT, AutoAccidentConsts.ValuesConsts.RESULT_OF_ACCIDENT_HOSPITALIZATION_MEDICAL_TREATMENT_SURGERY_OR_MISSED_WORD);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.AccidentResult.wrongfulDeath:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.RESULT_OF_ACCIDENT, AutoAccidentConsts.ValuesConsts.RESULT_OF_ACCIDENT_WRONGFUL_DEATH);
                        break;
                    case (int)DataModel.Xrm.new_personalinjurycasedata.AccidentResult.noneOfTheAbove:
                        client.ParamItems.Add(AutoAccidentConsts.ParamNames.RESULT_OF_ACCIDENT, AutoAccidentConsts.ValuesConsts.RESULT_OF_ACCIDENT_NONE_OF_THE_ABOVE);
                        break;
                }
            }
            else
            {
                client.ParamItems.Add(AutoAccidentConsts.ParamNames.RESULT_OF_ACCIDENT, AutoAccidentConsts.ValuesConsts.RESULT_OF_ACCIDENT_HOSPITALIZATION_MEDICAL_TREATMENT_SURGERY_OR_MISSED_WORD);
            }
            client.ParamItems.Add(AutoAccidentConsts.ParamNames.TYPES_OF_INJURIES_ISREQUIRED, AutoAccidentConsts.ValuesConsts.TRUE);
            bool atLeastOneMarked = false;
            if (piData.new_whiplash.HasValue && piData.new_whiplash.Value)
            {
                client.ParamItems.Add(AutoAccidentConsts.ParamNames.TYPES_OF_INJURIES, AutoAccidentConsts.ValuesConsts.TYPES_OF_INJURIES_WHIPLASH);
                atLeastOneMarked = true;
            }
            if (piData.new_brokenbones.HasValue && piData.new_brokenbones.Value)
            {
                client.ParamItems.Add(AutoAccidentConsts.ParamNames.TYPES_OF_INJURIES, AutoAccidentConsts.ValuesConsts.TYPES_OF_INJURIES_BROKEN_BONES);
                atLeastOneMarked = true;
            }
            if (piData.new_lostlimb.HasValue && piData.new_lostlimb.Value)
            {
                client.ParamItems.Add(AutoAccidentConsts.ParamNames.TYPES_OF_INJURIES, AutoAccidentConsts.ValuesConsts.TYPES_OF_INJURIES_LOST_LIMB);
                atLeastOneMarked = true;
            }
            if (piData.new_spinalcordinjuryorparalysis.HasValue && piData.new_spinalcordinjuryorparalysis.Value)
            {
                client.ParamItems.Add(AutoAccidentConsts.ParamNames.TYPES_OF_INJURIES, AutoAccidentConsts.ValuesConsts.TYPES_OF_INJURIES_SPINAL_CORD_INJURY_OR_PARALYSIS);
                atLeastOneMarked = true;
            }
            if (piData.new_braininjury.HasValue && piData.new_braininjury.Value)
            {
                client.ParamItems.Add(AutoAccidentConsts.ParamNames.TYPES_OF_INJURIES, AutoAccidentConsts.ValuesConsts.TYPES_OF_INJURIES_BRAIN_INJURY);
                atLeastOneMarked = true;
            }
            if (piData.new_lossoflife.HasValue && piData.new_lossoflife.Value)
            {
                client.ParamItems.Add(AutoAccidentConsts.ParamNames.TYPES_OF_INJURIES, AutoAccidentConsts.ValuesConsts.TYPES_OF_INJURIES_LOSS_OF_LIFE);
                atLeastOneMarked = true;
            }
            if ((piData.new_other.HasValue && piData.new_other.Value) || !atLeastOneMarked)
            {
                client.ParamItems.Add(AutoAccidentConsts.ParamNames.TYPES_OF_INJURIES, AutoAccidentConsts.ValuesConsts.TYPES_OF_INJURIES_OTHER);
            }
            client.ParamItems.Add(AutoAccidentConsts.ParamNames.ARE_YOU_CURRENTLY_REPRESENTED_ISREQUIRED, AutoAccidentConsts.ValuesConsts.TRUE);
            client.ParamItems.Add(AutoAccidentConsts.ParamNames.ARE_YOU_CURRENTLY_REPRESENTED, AutoAccidentConsts.ValuesConsts.ARE_YOU_CURRENTLY_NO);
        }

        protected override string URL
        {
            get { return url; }
        }

        private static class AutoAccidentConsts
        {            
            internal static class ParamNames
            {
                internal const string ADDRESS_STREETNAME = "address.streetName";
                internal const string NEWSLETTERSUBSCRIBE = "newsLetterSubscribe";
                internal const string KEYWORDID = "keywordId";
                internal const string ISLNNUMBER = "islnNumber";
                internal const string _QUESTIONSETVERSIONID_ = "_questionSetVersionId_";
                internal const string __DOES_ANYONE_HAS_INSURANCE_ISREQUIRED = "__match__7f29767a-156a-0382-195a-7f0eac11cb3b_isrequired";
                internal const string DOES_ANYONE_HAS_INSURANCE = "__match__7f29767a-156a-0382-195a-7f0eac11cb3b";
                internal const string __ESTIMATED_MEDICAL_BILLS_ISREQUIRED = "__match__ef94f8a9-a409-ac9c-7d15-7a1dc5eb75c0_isrequired";
                internal const string ESTIMATED_MEDICAL_BILLS = "__match__ef94f8a9-a409-ac9c-7d15-7a1dc5eb75c0";
                internal const string RESULT_OF_ACCIDENT_ISREQUIRED = "__match__0a6fa381-efab-7156-7933-99e2cf7f14a2_isrequired";
                internal const string RESULT_OF_ACCIDENT = "__match__0a6fa381-efab-7156-7933-99e2cf7f14a2";
                internal const string TYPES_OF_INJURIES_ISREQUIRED = "154f7ab3-9375-b7c3-4ee4-4d92602982f2_isrequired";
                internal const string TYPES_OF_INJURIES = "154f7ab3-9375-b7c3-4ee4-4d92602982f2";
                internal const string ARE_YOU_CURRENTLY_REPRESENTED_ISREQUIRED = "__match__061190aa-a98b-f2ec-a007-47c9c2123e54_isrequired";
                internal const string ARE_YOU_CURRENTLY_REPRESENTED = "__match__061190aa-a98b-f2ec-a007-47c9c2123e54";
            }

            internal static class ValuesConsts
            {
                internal const string NA = "N/A";
                internal const string OFF = "off";
                internal const string QUESTIONS_1 = "questions_1";
                internal const string TRUE = "true";
                internal const string HAS_INSURANCE_YES = "a087a50c-e761-b2db-023b-f6cca58d18a0";
                internal const string HAS_INSURANCE_NO = "3d2dc977-f971-e301-d143-93d0c709ad00";
                internal const string HAS_INSURANCE_DONT_KNOW = "f45a34ec-7429-a98c-d4af-5a30e01accff";
                internal const string ESTIMATED_MEDICAL_BILLS_LESS_THAN_1000 = "2b8f2b37-aeda-eede-0200-db3343757fba";
                internal const string ESTIMATED_MEDICAL_BILLS_1000_TO_5000 = "bd17abc1-7c58-fc27-8d96-ce40b07dee3e";
                internal const string ESTIMATED_MEDICAL_BILLS_5000_TO_10000 = "98b0ea6a-4da0-d28b-4e0b-e6f54c726055";
                internal const string ESTIMATED_MEDICAL_BILLS_10000_TO_25000 = "eebcffd0-9a8d-9e6e-ea5f-e569e9ff0dbb";
                internal const string ESTIMATED_MEDICAL_BILLS_25000_TO_100000 = "53a59313-aa12-bd46-25b3-c171b36ae2ac";
                internal const string ESTIMATED_MEDICAL_BILLS_MORE_THAN_100000 = "3e68ec54-489d-40ae-2c56-e20b937cb597";
                internal const string ESTIMATED_MEDICAL_BILLS_DONT_KNOW = "a1c6f855-943c-3d66-16b8-377ec7ac7453";
                internal const string ESTIMATED_MEDICAL_BILLS_NO_MEDICAL_BILLS = "00c4dab7-12de-f7e4-4946-3c306438f38a";
                internal const string RESULT_OF_ACCIDENT_HOSPITALIZATION_MEDICAL_TREATMENT_SURGERY_OR_MISSED_WORD = "a24d9d1b-d68c-ab99-5347-7f1fa5c967c0";
                internal const string RESULT_OF_ACCIDENT_WRONGFUL_DEATH = "04d588b8-eccf-0fc7-123b-06d5d3b0c217";
                internal const string RESULT_OF_ACCIDENT_NONE_OF_THE_ABOVE = "99fa51a0-18a4-1bbb-1580-dc1c30638012";
                internal const string TYPES_OF_INJURIES_WHIPLASH = "04113b1c-8940-bb9b-805a-c04d6c3a3202";
                internal const string TYPES_OF_INJURIES_BROKEN_BONES = "b20abf10-deaf-3343-3223-07dfe2b60ae7";
                internal const string TYPES_OF_INJURIES_LOST_LIMB = "ad064c31-e175-b4fc-debc-6a5f436eb708";
                internal const string TYPES_OF_INJURIES_SPINAL_CORD_INJURY_OR_PARALYSIS = "078b3f62-fe4a-b9cc-aa34-9c5a656a69e9";
                internal const string TYPES_OF_INJURIES_BRAIN_INJURY = "2743108b-3f58-3249-b6be-b2eb94986eb3";
                internal const string TYPES_OF_INJURIES_LOSS_OF_LIFE = "97fd2c61-ab75-ac51-3af3-d68933e626b3";
                internal const string TYPES_OF_INJURIES_OTHER = "639bf55e-5763-a5c4-af0a-b1b6a3d48a97";
                internal const string ARE_YOU_CURRENTLY_YES = "7e180517-b563-87cc-d73f-d930d46622cf";
                internal const string ARE_YOU_CURRENTLY_NO = "968c5ed6-73f8-e9d8-8829-6daa232a62f3";
            }
        }
    }
}
