﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;
using NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LexisNexisImplementations;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class LexisNexis : LeadBuyerBase
    {
        public override SendLeadResponse SendLead(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.account supplier, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                LexisNexisBase worker;
                switch (accExp.new_externalcode.ToLower())
                {
                    case "auto-accidents":
                        worker = new AutoAccidentPersonalInjuryHandler();
                        break;
                    case "dui":
                        worker = new DuiHandler();
                        break;
                    default:
                        worker = null;
                        retVal.Reason = "Broker not configured correctly for this heading.";
                        break;
                }
                if (worker != null)
                {
                    worker.SendLead(incident, accExp, supplier, incAcc, pingId, retVal);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in LexisNexis.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }
    }
}


