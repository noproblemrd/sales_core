﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LexisNexisImplementations
{
    internal class DuiHandler : LexisNexisBase
    {
        private const string url = "http://lawyerlocator.lawyers.com/adcng/legal/dui-dwi/rfqhp/?publisherId=51299&publisherTypeId=1798&keywordID=";

        protected override bool Validate(SendLeadResponse retVal, NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.account supplier, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc)
        {
            return true;
        }

        protected override void AddHeadingSpecificParams(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.BusinessLogic.Utils.HttpClient client)
        {
            client.ParamItems.Add(DuiConsts.ParamNames.ADDRESS_STREETNAME, DuiConsts.ValuesConsts.NA);
            client.ParamItems.Add(DuiConsts.ParamNames.NEWSLETTERSUBSCRIBE, DuiConsts.ValuesConsts.OFF);
            client.ParamItems.Add(DuiConsts.ParamNames.KEYWORDID, string.Empty);
            client.ParamItems.Add(DuiConsts.ParamNames.ISLNNUMBER, string.Empty);
            client.ParamItems.Add(DuiConsts.ParamNames._QUESTIONSETVERSIONID_, DuiConsts.ValuesConsts.QUESTIONS_1);
            client.ParamItems.Add(DuiConsts.ParamNames.__HAVE_YOU_RECEIVED_A_PRIOR_DUI_CONVICTION_IS_REQUIRED, DuiConsts.ValuesConsts.TRUE);
            client.ParamItems.Add(DuiConsts.ParamNames.HAVE_YOU_RECEIVED_A_PRIOR_DUI_CONVICTION, DuiConsts.ValuesConsts.HAVE_YOU_RECEIVED_A_PRIOR_DUI_CONVICTION_ANSWER_NO);
        }

        protected override string URL
        {
            get { return url; }
        }

        private static class DuiConsts
        {
            internal static class ParamNames
            {
                internal const string ADDRESS_STREETNAME = "address.streetName";
                internal const string NEWSLETTERSUBSCRIBE = "newsLetterSubscribe";
                internal const string KEYWORDID = "keywordId";
                internal const string ISLNNUMBER = "islnNumber";
                internal const string _QUESTIONSETVERSIONID_ = "_questionSetVersionId_";
                internal const string __HAVE_YOU_RECEIVED_A_PRIOR_DUI_CONVICTION_IS_REQUIRED = "5bc33647-6d65-7011-9fe5-8b5982a2dbf8_isrequired";
                internal const string HAVE_YOU_RECEIVED_A_PRIOR_DUI_CONVICTION = "5bc33647-6d65-7011-9fe5-8b5982a2dbf8";
            }

            internal static class ValuesConsts
            {
                internal const string NA = "N/A";
                internal const string OFF = "off";
                internal const string QUESTIONS_1 = "questions_1";
                internal const string TRUE = "true";
                internal const string HAVE_YOU_RECEIVED_A_PRIOR_DUI_CONVICTION_ANSWER_NO = "501fe45a-d4fc-01f9-7304-7eca51f8ac9e";
                internal const string HAVE_YOU_RECEIVED_A_PRIOR_DUI_CONVICTION_ANSWER_YES = "458bab78-08da-8fe3-27d5-b5d1ffbe15e9";
            }
        }
    }
}
