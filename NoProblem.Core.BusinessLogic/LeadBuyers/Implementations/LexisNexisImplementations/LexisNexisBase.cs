﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LexisNexisImplementations
{
    internal abstract class LexisNexisBase
    {
        private const string regionError = "Region is not zip code level.";
        
        protected abstract string URL
        {
            get;
        }
        
        protected abstract bool Validate(SendLeadResponse retVal, NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.account supplier, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc);

        protected abstract void AddHeadingSpecificParams(NoProblem.Core.DataModel.Xrm.incident incident, Utils.HttpClient client);

        internal void SendLead(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.account supplier, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc, string pingId, SendLeadResponse retVal)
        {
            bool valid = Validate(retVal, incident, accExp, supplier, incAcc);
            if (!valid)
            {
                retVal.FailedInnerValidation = true;
                return;
            }
            var contact = incident.incident_customer_contacts;
            var firstName = contact.firstname;
            var lastName = contact.lastname;
            var email = contact.emailaddress1;
            if (string.IsNullOrEmpty(firstName))
            {
                retVal.Reason = "First Name is mandatory";
                retVal.FailedInnerValidation = true;
                return;
            }
            if (string.IsNullOrEmpty(lastName))
            {
                retVal.Reason = "Last Name is mandatory";
                retVal.FailedInnerValidation = true;
                return;
            }
            if (string.IsNullOrEmpty(email))
            {
                retVal.Reason = "Email is mandatory";
                retVal.FailedInnerValidation = true;
                return;
            }
            var region = incident.new_new_region_incident;
            if (region.new_level != 4)
            {
                retVal.Reason = regionError;
                retVal.FailedInnerValidation = true;
                return;
            }
            string zipcode = region.new_name;
            DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
            var parents = regDal.GetParents(region);
            string state = parents.First(x => x.new_level == 1).new_name;
            string[] regionNameSplit = region.new_englishname.Split(';');
            string city = regionNameSplit[1];

            Utils.HttpClient client = new NoProblem.Core.BusinessLogic.Utils.HttpClient(URL, NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpMethodEnum.Post);
            AddBasicCustomerDataParams(incident, firstName, lastName, email, zipcode, state, city, client);

            AddHeadingSpecificParams(incident, client);

            var httpResponse = client.Post();
            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                try
                {
                    XElement xml = XElement.Parse(httpResponse.Content);
                    string resultCode = xml.Element("code").Value;
                    retVal.BrokerId = resultCode;
                    if (resultCode == "200")
                    {
                        retVal.IsSold = true;
                    }
                    else
                    {
                        retVal.MarkAsRejected = true;
                        retVal.Reason = "Lexis Nexis says: ";
                        switch (resultCode)
                        {
                            case "-100":
                                retVal.Reason += "Duplicate buyer contact";
                                break;
                            case "-150":
                                retVal.Reason += "Buyer has been banned";
                                break;
                            case "-200":
                                retVal.Reason += "No seller coverage for the buyer Zip Code";
                                break;
                            case "-300":
                                retVal.Reason += "Invalid Publisher ID";
                                break;
                            case "-400":
                                retVal.Reason += "Invalid Question Set version ID";
                                break;
                            case "-600":
                                retVal.Reason += "Missing required questions and answers";
                                break;
                            case "-700":
                                retVal.Reason += "Missing required contact information";
                                break;
                            case "-800":
                                retVal.Reason += "Unknown error";
                                break;
                        }
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Could not parse Lexis Nexis' response. Content = {0}", httpResponse.Content);
                    retVal.Reason = string.Format("Could not parse Lexis Nexis' response. Content = {0}", httpResponse.Content);
                }
            }
            else
            {
                retVal.Reason = string.Format("Lexis Nexis' responded with bad http status. http status = {0}, content = {1}", httpResponse.StatusCode, httpResponse.Content);
            }
        }

        private void AddBasicCustomerDataParams(NoProblem.Core.DataModel.Xrm.incident incident, string firstName, string lastName, string email, string zipcode, string state, string city, Utils.HttpClient client)
        {
            client.ParamItems.Add(BasicParamNames.FIRST_NAME, firstName);
            client.ParamItems.Add(BasicParamNames.LAST_NAME, lastName);
            client.ParamItems.Add(BasicParamNames.EMAIL, email);
            client.ParamItems.Add(BasicParamNames.PHONE, incident.new_telephone1);
            client.ParamItems.Add(BasicParamNames.CITY, city);
            client.ParamItems.Add(BasicParamNames.STATE, state);
            client.ParamItems.Add(BasicParamNames.ZIPCODE, zipcode);
        }

        private static class BasicParamNames
        {            
            internal const string FIRST_NAME = "firstName";
            internal const string LAST_NAME = "lastName";
            internal const string PHONE = "phone";
            internal const string EMAIL = "_emailQuestion_";
            internal const string CITY = "address.city";
            internal const string STATE = "stateId";
            internal const string ZIPCODE = "_zipcodeQuestion_";
        }
    }
}
