﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    class HomeBulletin : LeadBuyerBase
    {
        private const string url = "";
        private const string urlTest = "http://gpseway.com/quotes/acceptpost-qa.aspx";
        private const string regionError = "Region is not zip code level.";
        private const string SUCCESS = "Success";

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var fullName = contact.fullname;
                var email = contact.emailaddress1;
                if (string.IsNullOrEmpty(fullName))
                {
                    retVal.Reason = "Name is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                if (string.IsNullOrEmpty(email))
                {
                    retVal.Reason = "Email is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                if (string.IsNullOrEmpty(incident.new_movetozipcode))
                {
                    retVal.Reason = "Moving to zip code is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                var moveToRegion = regDal.GetRegionByName(incident.new_movetozipcode);
                if (moveToRegion == null)
                {
                    retVal.Reason = "Moving to zip code is invalid";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string[] moveToSplit = moveToRegion.new_englishname.Split(';');
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = regionError;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string zipcode = region.new_name;
                string[] regionNameSplit = region.new_englishname.Split(';');
                string cityFrom = regionNameSplit[1];
                string cityTo = moveToSplit[1];
                var parentsFrom = regDal.GetParents(region);
                string stateFrom = parentsFrom.First(x => x.new_level == 1).new_name;
                var parentsTo = regDal.GetParents(moveToRegion);
                string stateTo = parentsTo.First(x => x.new_level == 1).new_name;
                //if (stateFrom != stateTo)//compare states
                //{
                //    incAcc.new_potentialincidentprice = 15;
                //}


                if (!incident.new_movedate.HasValue)
                {
                    retVal.Reason = "Move date is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string moveDate = incident.new_movedate.Value.ToString("MM/dd/yyyy");

                int moveSize;
                switch (incident.new_movesize)
                {
                    case "studio":
                        moveSize = 2000;
                        break;
                    case "oneBdr":
                        moveSize = 2940;
                        break;
                    case "twoBdr":
                        moveSize = 4550;
                        break;
                    case "threeBdr":
                        moveSize = 7700;
                        break;
                    case "fourBdr":
                        moveSize = 9800;
                        break;
                    case "fiveBdr":
                        moveSize = 11200;
                        break;
                    case "larger":
                        moveSize = 11200;
                        break;
                    default:
                        retVal.Reason = "Move size is mandatory";
                        retVal.FailedInnerValidation = true;
                        return retVal;
                }

                Leads leads = new Leads();
                leads.Lead.CustName = fullName;
                leads.Lead.LeadId = incident.ticketnumber;
                if (!string.IsNullOrEmpty(incident.description))
                    leads.Lead.Comments = incident.description;
                leads.Lead.DayPhone = incident.new_telephone1;
                leads.Lead.Email = email;
                leads.Lead.FromCity = cityFrom;
                leads.Lead.FromState = stateFrom;
                leads.Lead.FromZip = zipcode;
                leads.Lead.ToCity = cityTo;
                leads.Lead.ToState = stateTo;
                leads.Lead.ToZip = moveToRegion.new_name;
                leads.Lead.Source = incident.incidentid.ToString();
                leads.Lead.MoveDate = moveDate;
                leads.Lead.MoveSizeLbs = moveSize;

                RestSharp.RestClient client = new RestSharp.RestClient();
                if (IsTest())
                {
                    client.BaseUrl = new Uri(urlTest);
                }
                else
                {
                    client.BaseUrl = new Uri(url);
                }
                RestSharp.RestRequest req = new RestSharp.RestRequest(RestSharp.Method.POST);
                req.RequestFormat = RestSharp.DataFormat.Xml;
                req.AddBody(leads);
                var ans = client.Execute<Homebulletin>(req);

                //handle response
                if (ans.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (ans.Data != null && ans.Data.FirstOrDefault() != null)
                    {
                        var data = ans.Data.FirstOrDefault();
                        if (data.Status == SUCCESS)
                        {
                            retVal.IsSold = true;
                            retVal.BrokerId = data.RefNumber;
                        }
                        else
                        {
                            retVal.MarkAsRejected = true;
                            retVal.Reason = string.Format("HomeBulletin didn't return 'Success'. Status = {0}, RefNumber = {1}, ErrorMessage = {2}", data.Status, data.RefNumber, data.ErrorMessage);
                            LogUtils.MyHandle.WriteToLog("HomeBulletin didn't return 'Success'. Status = {0}, RefNumber = {1}, ErrorMessage = {2}", data.Status, data.RefNumber, data.ErrorMessage);
                        }
                    }
                    else
                    {
                        retVal.Reason = string.Format("HomeBulletin responded with unexpected content. Content= {0}", ans.Content);
                        LogUtils.MyHandle.WriteToLog("HomeBulletin responded with unexpected content. Content= {0}", ans.Content);
                    }
                }
                else
                {
                    retVal.Reason = string.Format("Bad HttpStatus received from HomeBulletin. httpstatus = {0}, content = {1}", ans.StatusCode, ans.Content);
                    LogUtils.MyHandle.WriteToLog("Bad HttpStatus received from HomeBulletin. httpstatus = {0}, content = {1}", ans.StatusCode, ans.Content);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in HomeBulletin.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        //public override decimal GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_incidentaccount ia)
        //{
        //    //DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
        //    //var moveToRegion = regDal.GetRegionByName(incident.new_movetozipcode);
        //    //if (moveToRegion == null)
        //    //{
        //    //    return base.GetBrokerBid(incident, ia);
        //    //}
        //    //string[] moveToSplit = moveToRegion.new_englishname.Split(';');
        //    //var region = incident.new_new_region_incident;
        //    //if (region.new_level != 4)
        //    //{
        //    //    return base.GetBrokerBid(incident, ia);
        //    //}
        //    //string zipcode = region.new_name;
        //    //string[] regionNameSplit = region.new_englishname.Split(';');
        //    //string stateFrom = regionNameSplit[3];
        //    //if (stateFrom != moveToSplit[3])//compare states
        //    //{
        //    //    return 15;
        //    //}
        //    return base.GetBrokerBid(incident, ia);
        //}

        private class Homebulletin : List<LeadStatus>
        {
        }

        private class LeadStatus
        {
            public string Status { get; set; }
            public string ErrorMessage { get; set; }
            public string RefNumber { get; set; }
        }

        [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
        private class Leads
        {
            public Leads()
            {
                Lead = new Lead();
            }

            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public Lead Lead { get; set; }
        }

        [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
        private class Lead
        {
            private static string USA = "USA";

            public Lead()
            {
                LeadType = string.Empty;
                EvePhone = string.Empty;
                Cell = string.Empty;
                Fax = string.Empty;
                FromCountry = USA;
                ToCountry = USA;
                CarLead = 0;
                CarMake1 = string.Empty;
                CarMake2 = string.Empty;
                CarModel1 = string.Empty;
                CarModel2 = string.Empty;
                CarYear1 = string.Empty;
                CarYear2 = string.Empty;
                Comments = string.Empty;
            }

            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string LeadId { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string LeadType { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string CustName { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string MoveDate { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string DayPhone { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string EvePhone { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string Cell { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string Fax { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string Email { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string FromCity { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string FromState { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string FromZip { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string FromCountry { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string ToCity { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string ToState { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string ToZip { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string ToCountry { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public int MoveSizeLbs { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string Comments { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string Source { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public int CarLead { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string CarMake1 { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string CarModel1 { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string CarYear1 { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string CarMake2 { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string CarModel2 { get; set; }
            [RestSharp.Serializers.SerializeAsAttribute(NameStyle = RestSharp.Serializers.NameStyle.LowerCase)]
            public string CarYear2 { get; set; }
        }
    }
}
