﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    public class TheHomeFixers : LeadBuyerBase, IPingPostApiBuyer
    {
        private const string AFFILIATE_ID = "2iD9r62wOGtL";
        private const string PING_URL = "https://www.thehomefixers.com/api/ping.php";
        private const string POST_URL = "https://www.thehomefixers.com/api/post.php";
        private const string LEAD_APPROVED = "Lead Approved";
        private const string LEAD_SENT = "Lead Sent";


        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                if (!Validate(incident))
                {
                    retVal.Reason = validationMessage;
                    retVal.FailedInnerValidation = true;
                    retVal.WantsToBid = false;
                    return retVal;
                }

                var result = Ping(
                    new PingData()
                {
                    CategoryCode = incident.new_new_primaryexpertise_incident.new_code,
                    Zip = incident.new_new_region_incident.new_name
                });

                if (result.Accepted)
                {
                    retVal.BrokerId = result.PingId;
                    retVal.Price = result.PingPrice;
                }
                else
                {
                    retVal.WantsToBid = false;
                    retVal.MarkAsRejected = false;
                    retVal.Reason = result.PingMessage;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in TheHomeFixers.GetBrokerBid");
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }

        public static void ParseLeadApprovedInPingResponse(string content, out string brokerId, out decimal price)
        {
            string withOutStatus = content.Substring(LEAD_APPROVED.Length).Trim();
            string[] split = withOutStatus.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            brokerId = split[0];
            price = decimal.Parse(split[1].Substring(1));
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var result = Post(
                    new PostData()
                    {
                        PingId = pingId,
                        Zip = incident.new_new_region_incident.new_name,
                        FirstName = contact.firstname,
                        LastName = contact.lastname,
                        Phone = incident.new_telephone1
                    });
                if (result.Accepted)
                {
                    retVal.IsSold = true;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = result.PostMessage;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in TheHomeFixers.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            ValidateRegion(incident);
            ValidateName(incident);
        }

        public PingResult Ping(PingData data)
        {
            PingResult result = new PingResult();
            RestSharp.IRestClient client = new RestSharp.RestClient(PING_URL);
            RestSharp.IRestRequest req = new RestSharp.RestRequest(RestSharp.Method.POST);
            req.AddParameter("Affiliate", AFFILIATE_ID);
            req.AddParameter("JobTypeID", data.CategoryCode);
            req.AddParameter("ZipCode", data.Zip);
            if (IsTest())
            {
                req.AddParameter("TestMode", 1);
            }
            var res = client.Execute(req);

            if (res.Content.StartsWith(LEAD_APPROVED))
            {
                string brokerId;
                decimal price;
                ParseLeadApprovedInPingResponse(res.Content, out brokerId, out price);
                result.PingId = brokerId;
                result.PingPrice = price;
                result.Accepted = true;
            }
            else
            {
                result.PingMessage = String.Format("Rejected at ping with status '{0}'", res.Content);
            }
            return result;
        }

        public PostResult Post(PostData data)
        {
            PostResult result = new PostResult();
            RestSharp.IRestClient client = new RestSharp.RestClient(POST_URL);
            RestSharp.IRestRequest req = new RestSharp.RestRequest(RestSharp.Method.POST);
            req.AddParameter("Affiliate", AFFILIATE_ID);
            req.AddParameter("LeadCode", data.PingId);
            req.AddParameter("CustomerZipCode", data.Zip);
            req.AddParameter("CustomerName", data.FirstName + " " + data.LastName);
            req.AddParameter("CustomerPhone", data.Phone);
            if (IsTest())
            {
                req.AddParameter("TestMode", 1);
            }
            var res = client.Execute(req);
            if (res.Content.Trim(' ', '\n', '\t', '\r') == LEAD_SENT)
            {
                result.Accepted = true;
            }
            else
            {
                result.PostMessage = String.Format("Rejected at POST with status '{0}'", res.Content);
            }
            return result;
        }
    }
}
