﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class EmailConnection : LeadBuyerBase
    {
        #region ILeadBuyer Members

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            var region = incident.new_new_region_incident;
            string regionName = region.new_englishname;
            string headingName = accExp.new_new_primaryexpertise_new_accountexpertise.new_name;
            var customer = incident.incident_customer_contacts;
            var name = customer.fullname;
            if (string.IsNullOrEmpty(name))
                name = "anonymous";
            var email = customer.emailaddress1;
            if (email == null)
                email = String.Empty;
            string description = incident.description;
            if (description == null)
                description = string.Empty;
            string phone = incident.new_telephone1;

            Dictionary<string, Dictionary<string, string>> mergeVarsList = new Dictionary<string, Dictionary<string, string>>();
            Dictionary<string, string> mergeVars = new Dictionary<string, string>();
            mergeVars.Add("NPVAR_CATEGORY", headingName);
            mergeVars.Add("NPVAR_ZIPCODE", regionName);
            mergeVars.Add("NPVAR_NAME", name);
            mergeVars.Add("NPVAR_PHONE", phone);
            mergeVars.Add("NPVAR_DESCRIPTION", description);
            mergeVars.Add("NPVAR_EMAIL", email);

            List<string> emailsTo = new List<string>();
            emailsTo.Add(supplier.emailaddress1);
            mergeVarsList.Add(supplier.emailaddress1, mergeVars);
            if (!string.IsNullOrEmpty(supplier.emailaddress2))
            {
                emailsTo.Add(supplier.emailaddress2);
                mergeVarsList.Add(supplier.emailaddress2, mergeVars);
            }
            string subject = "New Lead From NoProblemPPC";
            MandrillSender sender = new MandrillSender();
            Dictionary<string, bool> ans = sender.SendTemplate(emailsTo, subject, DataModel.MandrillTemplates.Lead_To_Broker, mergeVarsList, new Dictionary<string, string>());

            if (ans.Count(x => x.Value) > 0)
            {
                retVal.IsSold = true;
                retVal.BrokerId = "sent by email";
            }
            else
            {
                retVal.Reason = "Failed to send to all emails";
            }
            return retVal;
        }

        #endregion
    }
}
