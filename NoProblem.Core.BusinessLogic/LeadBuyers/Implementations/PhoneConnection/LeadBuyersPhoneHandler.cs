﻿using System;
using Effect.Crm.Logs;
using NoProblem.Core.DataModel;
using NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.PhoneConnection;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    public class LeadBuyersPhoneHandler : TwilioBL.TwilioHandlerBase
    {
        public LeadBuyersPhoneHandler(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        private const int INCIDENT_ID_INDEX = 0;
        private const int INCIDENT_ACCOUNT_ID_INDEX = 1;
        private const int BROKER_PH0NE_INDEX = 2;
        private const int HEADING_CODE_INDEX = 3;
        private const int CUSTOMER_PHONE_INDEX = 4;
        private const int DISABLE_PRESS_ONE_INDEX = 5;

        private const string DISABLE_PRESS_ONE_DIGITS = "DISABLE_PRESS_ONE";

        internal SendLeadResponse StartCall(
            DataModel.Xrm.incident incident,
            DataModel.Xrm.new_incidentaccount incAcc,
            DataModel.Xrm.new_accountexpertise accExp,
            string brokerPhone, string customerPhone
            )
        {
            var heading = accExp.new_new_primaryexpertise_new_accountexpertise;
            bool disablePressOne = false;
            disablePressOne = (heading.new_disablepressoneinphoneapi.HasValue && heading.new_disablepressoneinphoneapi.Value);
            string callId = incident.incidentid.ToString() + ";" + incAcc.new_incidentaccountid.ToString() + ";" + brokerPhone + ";" + accExp.new_new_primaryexpertise_new_accountexpertise.new_code + ";" + customerPhone + ";" + disablePressOne.ToString();
            SendLeadResponse retVal = new SendLeadResponse();
            TwilioBL.TwilioCallsExecuter executer = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioCallsExecuter(XrmDataContext);
            string callStatus;
            string callSid = executer.InitiateCall(customerPhone, "ids", callId, "LeadBuyersCustomerCallAnswered", "LeadBuyersCustomerCallCallback", null, false, true, null, out callStatus);
            if (!string.IsNullOrEmpty(callSid) && callStatus != "failed")
            {
                retVal.IsInCall = true;
                incAcc.new_dialercallid = callSid;
                DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                dal.Update(incAcc);
                XrmDataContext.SaveChanges();
            }
            else
            {
                retVal.Reason = "Failed to initiate call";
            }
            return retVal;
        }

        public string LeadBuyersCustomerCallAnswered(string callSid, string accountSid, string from, string to,
            string callStatus, string apiVersion, string direction, string answeredBy, string ids
            )
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            string[] split = ids.Split(';');
            Guid incidentId = new Guid(split[INCIDENT_ID_INDEX]);
            Guid incAccId = new Guid(split[INCIDENT_ACCOUNT_ID_INDEX]);
            string headingCode = split[HEADING_CODE_INDEX];
            bool disablePressOne = true.ToString() == split[DISABLE_PRESS_ONE_INDEX];
            DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
            string audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
            if (disablePressOne)
            {
                response.Play(audioFilesUrlBase + "/brokercall/brokercallpressone-a.mp3");
                response.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHARON + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.MP3_FILE_EXTENSION);
                response.Play(audioFilesUrlBase + "/brokercall/phone_API_nopressone_Part02.mp3");
                response.Redirect(twilioCallBackUrlBase + "/LeadBuyersCallClickHandler?ids=" + ids + "&SiteId=" + siteId + "&Digits=" + DISABLE_PRESS_ONE_DIGITS, "GET");
            }
            else
            {
                response.BeginGather(new
                {
                    action = twilioCallBackUrlBase + "/LeadBuyersCallClickHandler?ids=" + ids + "&SiteId=" + siteId,
                    timeout = 10,
                    method = "GET",
                    numDigits = 1,
                    finishOnKey = string.Empty
                });
                response.Play(audioFilesUrlBase + "/brokercall/pressingone/pressone-01-sharon.wav");
                response.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHARON + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.MP3_FILE_EXTENSION);
                response.Play(audioFilesUrlBase + "/brokercall/pressingone/pressone-02-sharon.wav");
                response.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHARON + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.MP3_FILE_EXTENSION);
                response.Play(audioFilesUrlBase + "/brokercall/pressingone/pressone-03-sharon.wav");
                //response.Play(audioFilesUrlBase + "/brokercall/pressingone/pressone-01.mp3");
                //response.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHIELA + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.MP3_FILE_EXTENSION);
                //response.Play(audioFilesUrlBase + "/brokercall/pressingone/pressone-02.mp3");
                //response.Play(audioFilesUrlBase + AudioFilesConsts.SLASH + AudioFilesConsts.SHIELA + AudioFilesConsts.SLASH + AudioFilesConsts.HEADING_RECORDINGS_FOLDER + AudioFilesConsts.SLASH + headingCode + AudioFilesConsts.MP3_FILE_EXTENSION);
                //response.Play(audioFilesUrlBase + "/brokercall/pressingone/pressone-03.mp3");
                response.EndGather();
                response.Redirect(twilioCallBackUrlBase + "/LeadBuyersCallClickHandler?ids=" + ids + "&SiteId=" + siteId + "&Digits=NOTHING", "GET");
            }
            return response.ToString();
        }

        public string LeadBuyersCallClickHandler(string ids, string digits, string callSid)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            if (digits == DISABLE_PRESS_ONE_DIGITS || (!string.IsNullOrEmpty(digits) && digits != "NOTHING"))
            {
                DataAccessLayer.ConfigurationSettings configDal = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(XrmDataContext);
                string audioFilesUrlBase = configDal.GetConfigurationSettingValue(DataModel.ConfigurationKeys.AUDIO_FILES_URL_BASE);
                string url = audioFilesUrlBase + "/brokercall/brokercallwait03.mp3";
                response.DialConference(ids,
                            new { endConferenceOnExit = true, waitUrl = url, waitMethod = "GET" },
                            new { timeLimit = 60 * 30, timeout = 120, record = true, action = twilioCallBackUrlBase + "/LeadBuyersCallConferenceCallBack?SiteId=" + siteId + "&ids=" + ids, method = "GET" }
                            );
                ExecuteCallToBrokerDelegate del = new ExecuteCallToBrokerDelegate(ExecuteCallToBrokerAsync);
                del.BeginInvoke(ids, ExecuteCallToBrokerAsyncCallback, del);
            }
            else
            {
                response.Hangup();
                string[] split = ids.Split(';');
                Guid incidentId = new Guid(split[INCIDENT_ID_INDEX]);
                Guid incAccId = new Guid(split[INCIDENT_ACCOUNT_ID_INDEX]);
                string customerPhone = split[CUSTOMER_PHONE_INDEX];
                HandleFailedDelegate del = new HandleFailedDelegate(HandleFailedAsync);
                del.BeginInvoke(incidentId, incAccId, "Customer didn't press one", HandleFailedAsyncCallback, del);
                PhoneConnectionSmsSender smsSender = new PhoneConnectionSmsSender();
                smsSender.Send(incAccId, incidentId, customerPhone);
            }
            return response.ToString();
        }

        public void LeadBuyersCallConferenceCallBack(string callSid, string recordingUrl, string ids)
        {
            string[] split = ids.Split(';');
            Guid incAccId = new Guid(split[INCIDENT_ACCOUNT_ID_INDEX]);
            DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            if (!recordingUrl.EndsWith(".mp3", StringComparison.CurrentCultureIgnoreCase))
            {
                recordingUrl = recordingUrl + ".mp3";
            }
            dal.UpdateRecordFileLocation(incAccId, recordingUrl, 0);
        }

        public void LeadBuyersCustomerCallCallback(string callDuration, string callSid, string accountSid, string from, string to,
            string callStatus, string apiVersion, string direction, string ids)
        {
            string[] split = ids.Split(';');
            Guid incidentId = new Guid(split[INCIDENT_ID_INDEX]);
            Guid incAccId = new Guid(split[INCIDENT_ACCOUNT_ID_INDEX]);
            CreateCDR(callStatus, callSid, callDuration, to, "Customer Call", NoProblem.Core.DataModel.Xrm.new_calldetailrecord.Type.jona_consumer, incAccId);
            bool isAnswered = callStatus == "completed";
            if (!isAnswered)
            {
                HandleFailedDelegate del = new HandleFailedDelegate(HandleFailedAsync);
                del.BeginInvoke(incidentId, incAccId, string.Format("Customer didn't answer. Twilio's call status = {0}", callStatus), HandleFailedAsyncCallback, del);
                PhoneConnectionSmsSender smsSender = new PhoneConnectionSmsSender();
                smsSender.Send(incAccId, incidentId, to);
            }
        }

        delegate void HandleFailedDelegate(Guid incidentId, Guid incidentAccountId, string reason);

        private static void HandleFailedAsync(Guid incidentId, Guid incidentAccountId, string reason)
        {
            LeadBuyersPhoneHandler handler = new LeadBuyersPhoneHandler(null);
            handler.HandleFailed(incidentId, incidentAccountId, reason);
        }

        private void HandleFailed(Guid incidentId, Guid incidentAccountId, string reason)
        {
            ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(XrmDataContext);
            DataAccessLayer.IncidentAccount incAccDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
            var incAcc = incAccDal.Retrieve(incidentAccountId);
            incAcc.new_rejectedreason = reason;
            manager.MarkIncidentAccountAsLostAfterLeadBuyerFailed(incAccDal, incAcc);
            manager.ContinueRequestAfterBrokerCall(false, incidentId, false);
        }

        private void HandleFailedAsyncCallback(IAsyncResult result)
        {
            try
            {
                ((HandleFailedDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in HandleFailedAsyncCallback");
            }
        }

        delegate void ExecuteCallToBrokerDelegate(string ids);

        private static void ExecuteCallToBrokerAsync(string ids)
        {
            System.Threading.Thread.Sleep(500);
            LeadBuyersPhoneHandler handler = new LeadBuyersPhoneHandler(null);
            handler.ExecuteCallToBroker(ids);
        }

        private void ExecuteCallToBroker(string ids)
        {
            string[] split = ids.Split(';');
            string brokerPhone = split[BROKER_PH0NE_INDEX];
            string customerPhone = split[CUSTOMER_PHONE_INDEX];
            TwilioBL.TwilioCallsExecuter executer = new NoProblem.Core.BusinessLogic.TwilioBL.TwilioCallsExecuter(XrmDataContext);
            if (executer.IsConferenceActive(ids))
            {
                string callStatus;
                string callSid = executer.InitiateCall(brokerPhone, "ids", ids, "LeadBuyersBrokerCallAnswered", "LeadBuyersBrokerCallCallback", null, false, false, customerPhone, out callStatus);
                if (string.IsNullOrEmpty(callSid) || callStatus == "failed")
                {
                    HandleFailedDelegate del = new HandleFailedDelegate(HandleFailedAsync);
                    del.BeginInvoke(new Guid(split[INCIDENT_ID_INDEX]), new Guid(split[INCIDENT_ACCOUNT_ID_INDEX]), "Could not execute call to broker, please check his phone is correct.", HandleFailedAsyncCallback, del);
                }
            }
            else
            {
                HandleFailedDelegate del = new HandleFailedDelegate(HandleFailedAsync);
                del.BeginInvoke(new Guid(split[INCIDENT_ID_INDEX]), new Guid(split[INCIDENT_ACCOUNT_ID_INDEX]), "Customer hung up before broker was called", HandleFailedAsyncCallback, del);
            }
        }

        public string LeadBuyersBrokerCallAnswered(string callSid, string accountSid, string from, string to,
            string callStatus, string apiVersion, string direction, string ids)
        {
            Twilio.TwiML.TwilioResponse response = new Twilio.TwiML.TwilioResponse();
            response.DialConference(ids, new { endConferenceOnExit = true }, new { timeLimit = 60 * 30, timeout = 120 });
            return response.ToString();
        }

        public void LeadBuyersBrokerCallCallback(string callDuration, string callSid, string accountSid, string from, string to,
           string callStatus, string apiVersion, string direction, string ids)
        {
            string[] split = ids.Split(';');
            Guid incidentId = new Guid(split[INCIDENT_ID_INDEX]);
            Guid incAccId = new Guid(split[INCIDENT_ACCOUNT_ID_INDEX]);
            CreateCDR(callStatus, callSid, callDuration, to, "CallCenter Call", NoProblem.Core.DataModel.Xrm.new_calldetailrecord.Type.jona_advertiser, incAccId);
            bool isAnswered = callStatus == "completed";
            if (!isAnswered)
            {
                HandleFailedDelegate del = new HandleFailedDelegate(HandleFailedAsync);
                del.BeginInvoke(incidentId, incAccId, "Broker did not answer", HandleFailedAsyncCallback, del);
            }
            else
            {
                DataAccessLayer.IncidentAccount incidentAccountDal = new NoProblem.Core.DataAccessLayer.IncidentAccount(XrmDataContext);
                var incAcc = incidentAccountDal.Retrieve(incAccId);
                var supplier = incAcc.new_account_new_incidentaccount;
                int minSecs = supplier.new_mincallduration.HasValue ? supplier.new_mincallduration.Value : -1;
                int duration;
                int.TryParse(callDuration, out duration);
                if (duration < minSecs)
                {
                    HandleFailedDelegate del = new HandleFailedDelegate(HandleFailedAsync);
                    del.BeginInvoke(incidentId, incAccId, string.Format("Call to broker was too short. call duration = {0}. minimun required duration = {1}", duration, minSecs), HandleFailedAsyncCallback, del);
                }
                else
                {
                    //success
                    DataAccessLayer.Incident incidentDal = new NoProblem.Core.DataAccessLayer.Incident(XrmDataContext);
                    ServiceRequest.ServiceRequestManager manager = new NoProblem.Core.BusinessLogic.ServiceRequest.ServiceRequestManager(XrmDataContext);
                    var incident = incidentDal.Retrieve(incidentId);
                    manager.CloseIncidentAccountAfterLeadBuyer(incAcc, incident, incidentDal, incidentAccountDal, false);
                    bool isExclusive = supplier.new_isexclusivebroker.HasValue && supplier.new_isexclusivebroker.Value;
                    manager.ContinueRequestAfterBrokerCall(true, incidentId, isExclusive);
                }
            }
        }

        private static void ExecuteCallToBrokerAsyncCallback(IAsyncResult result)
        {
            try
            {
                ((ExecuteCallToBrokerDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ExecuteCallToBrokerAsyncCallback");
            }
        }

        private void CreateCDR(string callStatus, string callSid, string callDuration, string toPhone, string name, DataModel.Xrm.new_calldetailrecord.Type type, Guid incidentAccountId)
        {
            DataModel.Xrm.new_calldetailrecord cdr = new NoProblem.Core.DataModel.Xrm.new_calldetailrecord();
            cdr.new_type = (int)type;
            cdr.new_trunk = "Phone API";
            cdr.new_tophone = toPhone;
            cdr.new_name = name;
            cdr.new_callstatus = callStatus;
            cdr.new_dialercallid = callSid;
            int duration;
            int.TryParse(callDuration, out duration);
            cdr.new_duration = duration;
            cdr.new_incidentaccountid = incidentAccountId;
            cdr.new_timeofcall = DateTime.Now;
            DataAccessLayer.CallDetailRecord dal = new NoProblem.Core.DataAccessLayer.CallDetailRecord(XrmDataContext);
            dal.Create(cdr);
            XrmDataContext.SaveChanges();
        }
    }
}
