﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.DataModel.Asterisk;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class PhoneConnection : LeadBuyerBase
    {
        private const int millisecondsSleepBeforeCalling = 15000;

        public PhoneConnection()
        {
            DataAccessLayer.ConfigurationSettings config = new NoProblem.Core.DataAccessLayer.ConfigurationSettings(null);
            implementation = config.GetConfigurationSettingValue(DataModel.ConfigurationKeys.PHONE_API_IMPLEMENTATION).ToLower();
        }

        private string implementation;
        private const string FONEAPI_IMPL = "foneapi";

        #region ILeadBuyer Members

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = null;
            try
            {
                retVal = SendLead(incident, accExp, incAcc, supplier.new_limitcallduration, supplier.telephone1, supplier.new_country, supplier.new_mincallduration);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in PhoneConnection.SendLead");
                if (retVal == null)
                    retVal = new SendLeadResponse();
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        #endregion

        public SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount incAcc, int? limitCallDuration, string advertiserPhone, string advertiserPhoneCountry, int? minimumCallDuration)
        {
            SendLeadResponse retVal;
            System.Threading.Thread.Sleep(millisecondsSleepBeforeCalling);
            if (implementation == FONEAPI_IMPL)
            {
                FoneApiBL.Handlers.PhoneApiHandler handler = new NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.PhoneApiHandler();
                retVal = handler.StartCall(incident, incAcc, accExp, advertiserPhone, advertiserPhoneCountry, incident.new_telephone1, limitCallDuration, minimumCallDuration);
            }
            else
            {
                LeadBuyersPhoneHandler phoneHandler = new LeadBuyersPhoneHandler(null);
                retVal = phoneHandler.StartCall(incident, incAcc, accExp, advertiserPhone, incident.new_telephone1);
            }
            return retVal;
        }
    }
}
