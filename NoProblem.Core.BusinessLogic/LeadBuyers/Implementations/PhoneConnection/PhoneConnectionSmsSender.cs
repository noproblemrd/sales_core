﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.PhoneConnection
{
    internal class PhoneConnectionSmsSender
    {
        private Thread thread;
        private Guid incidentAccountId;
        private Guid incidentId;
        private string customerNumber;

        public PhoneConnectionSmsSender()
        {
            thread = new Thread(new ThreadStart(Run));
        }

        private void Run()
        {
            try
            {
                DataAccessLayer.IncidentAccount dal = new NoProblem.Core.DataAccessLayer.IncidentAccount(null);
                DataAccessLayer.IncidentAccount.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_incidentaccount>.SqlParametersList();
                parameters.Add("@incId", incidentId);
                parameters.Add("@iaId", incidentAccountId);
                object scalar = dal.ExecuteScalar(query, parameters);
                if (scalar != null && scalar != DBNull.Value)
                {
                    string virtualNumber = (string)scalar;
                    Notifications notifier = new Notifications(dal.XrmDataContext);
                    string body, subject;
                    notifier.InstatiateTemplate(DataModel.TemplateNames.PHONE_API_DIDNT_PRESS_ONE_SMS, out subject, out body);
                    string phoneCountry = FindPhoneCountry();
                    notifier.SendSMSTemplate(string.Format(body, virtualNumber), customerNumber, phoneCountry);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in PhoneConnectionSmsSender.Run");
            }
        }

        private string FindPhoneCountry()
        {
            DataAccessLayer.Incident dal = new DataAccessLayer.Incident(null);
            string country = (from inc in dal.All
                         where inc.incidentid == incidentId
                         select inc.new_country).First();
            return country;
        }

        internal void Send(Guid incAccId, Guid incId, string customerNumber)
        {
            incidentAccountId = incAccId;
            incidentId = incId;
            this.customerNumber = customerNumber;
            thread.Start();
        }

        private const string query =
            @"
select top 1 dn.new_number
from new_directnumber dn
join new_accountexpertise ae on ae.new_accountexpertiseid = dn.new_accountexpertiseid 
join account acc on acc.accountid = ae.new_accountid
where
dn.deletionstatecode = 0
and ae.deletionstatecode = 0
and acc.deletionstatecode = 0
and ae.statecode = 0
and dn.statecode = 0
and ae.new_primaryexpertiseid = (select top 1 new_primaryexpertiseid from incident where incidentid = @incId)
and acc.accountid = (select top 1 new_accountid from new_incidentaccount where new_incidentaccountid = @iaId)
and dn.new_originid = (select top 1 new_originid from new_origin where new_isdefaultorigin = 1)
";
    }
}
