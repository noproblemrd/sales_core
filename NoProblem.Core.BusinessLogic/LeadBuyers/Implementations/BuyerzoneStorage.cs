﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;
using NoProblem.Core.DataModel.Consumer;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations
{
    class BuyerzoneStorage : LeadBuyerBase
    {
        private const string URL = "http://www.buyerzone.com/industrial/residential-storage-containers/rfqhp/?publisherId=39650&publisherTypeId=1798&keywordID=";

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            ValidateEmail(incident);
            ValidateAddress(incident);
            ValidateFirstAndLastName(incident);
            ValidateBuyerzoneStorageQuestions(incident);
            ValidateRegion(incident);
        }

        private void ValidateBuyerzoneStorageQuestions(DataModel.Xrm.incident incident)
        {
            if (!incident.new_storagecasedataid.HasValue)
            {
                SetValidationFailureMessage("Missing specific storage details");
            }
            else
            {
                DataModel.Xrm.new_storagecasedata storageData = incident.new_storagecasedata_incident;
                if (!storageData.new_deliverystorage.HasValue
                    || !storageData.new_financestorage.HasValue
                    || !storageData.new_howmanycontainers.HasValue
                    || !storageData.new_planningtousestorage.HasValue
                    || !storageData.new_storagelength.HasValue
                    || !storageData.new_typeofstorage.HasValue)
                {
                    SetValidationFailureMessage("Some required storage details are missing");
                }
            }
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                if (!Validate(incident))
                {
                    retVal.Reason = validationMessage;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                RestSharp.RestClient client = new RestSharp.RestClient(URL);
                RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
                AddContactDetailsParams(request, incident);
                AddBuyerzoneStorageDetails(request, incident);
                var response = client.Execute(request);
                var statusHeader = response.Headers.First(x => x.Name == "hostAndPostStatus");
                string responseCode = statusHeader.Value.ToString();
                if (responseCode == "200")
                {
                    retVal.IsSold = true;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = ExtractReason(response.Content, responseCode);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in BuyerzoneStorage.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private string ExtractReason(string responseBody, string responseCode)
        {
            string retVal = "Response code = " + responseCode;
            try
            {
                XElement doc = XElement.Parse(responseBody);

            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception parsing response body of boyerzone stoarge. responseCode = {1}. body = {0}", responseBody, responseCode);
                retVal += @". Could not parse response for more details. response body: " + responseBody;
            }
            return retVal;
        }

        private static void AddContactDetailsParams(RestSharp.RestRequest request, DataModel.Xrm.incident incident)
        {
            var contact = incident.incident_customer_contacts;
            request.AddParameter("_emailQuestion_", contact.emailaddress1);
            request.AddParameter("_zipcodeQuestion_", incident.new_new_region_incident.new_name);
            request.AddParameter("phone", incident.new_telephone1);
            request.AddParameter("address.streetName", contact.address1_line1);
            request.AddParameter("firstName", contact.firstname);
            request.AddParameter("lastName", contact.lastname);
        }

        private void AddBuyerzoneStorageDetails(RestSharp.RestRequest request, DataModel.Xrm.incident incident)
        {

            DataModel.Xrm.new_storagecasedata storageData = incident.new_storagecasedata_incident;

            request.AddParameter("_questionSetVersionId_", "questions_13");

            //business or personal use:
            //busiess = d7887dc8-95a3-9b4e-eef5-458d8b969dc6
            //personal = c5e3da8b-756a-ed84-daa1-186aaca32ae4
            string type;
            if (storageData.new_typeofstorage.Value == (int)DataModel.Consumer.ServiceRequestStorageData.eTypeOfStorage.Personal)
            {
                type = "c5e3da8b-756a-ed84-daa1-186aaca32ae4";
            }
            else
            {
                type = "d7887dc8-95a3-9b4e-eef5-458d8b969dc6";
            }
            request.AddParameter("d76c3d23-f5c7-0fc5-36e7-d2d5b54b5ff1", type);

            request.AddParameter("__match__44577e04-7741-fafb-ca96-68c8a497f409_isrequired", "true");

            //how are you planning to use:
            //Storage at my property = a2f68dca-e381-ab6b-d79a-bf28b8895979
            //Storage at a climate-controlled warehouse = cc9adf30-2eb8-4c8e-fa98-543a1a9756c0
            //Moving and storage = 686fd5c7-0304-9a63-4052-514a46c9bf5e
            //Moving only = 5bc15637-491b-2083-c94a-1222ca6d6d6a
            string plan;
            switch (storageData.new_planningtousestorage.Value)
            {
                case (int)ServiceRequestStorageData.ePlanningToUseStorage.Storage_at_my_property:
                    plan = "a2f68dca-e381-ab6b-d79a-bf28b8895979";
                    break;
                case (int)ServiceRequestStorageData.ePlanningToUseStorage.Storage_at_a_climate_controlled_warehouse:
                    plan = "cc9adf30-2eb8-4c8e-fa98-543a1a9756c0";
                    break;
                case (int)ServiceRequestStorageData.ePlanningToUseStorage.Moving_and_storage:
                    plan = "686fd5c7-0304-9a63-4052-514a46c9bf5e";
                    break;
                default:
                    plan = "5bc15637-491b-2083-c94a-1222ca6d6d6a";
                    break;
            }
            request.AddParameter("__match__44577e04-7741-fafb-ca96-68c8a497f409", plan);

            request.AddParameter("1e006722-1644-9283-dbfd-0fb659eb7660_isrequired", "true");

            //How many containers do you need?
            //1 = 567d14dd-4b44-4067-4355-142a42096a7a
            //2 - 4 = 98d2f4fc-093f-d42d-6e4f-b528f9ab6bac
            //5 - 7 = 5503e6b5-618e-c56a-05d9-5188d8fe2927
            //8+ (pelase specify) = 5054ed11-e589-2fff-edd9-0f553b1e0c78
            string howMany;
            switch (storageData.new_howmanycontainers.Value)
            {
                case (int)ServiceRequestStorageData.eHowManyContainers.one:
                    howMany = "567d14dd-4b44-4067-4355-142a42096a7a";
                    break;
                case (int)ServiceRequestStorageData.eHowManyContainers.two_to_four:
                    howMany = "98d2f4fc-093f-d42d-6e4f-b528f9ab6bac";
                    break;
                case (int)ServiceRequestStorageData.eHowManyContainers.five_to_seven:
                    howMany = "5503e6b5-618e-c56a-05d9-5188d8fe2927";
                    break;
                default://8+
                    howMany = "5054ed11-e589-2fff-edd9-0f553b1e0c78";
                    break;
            }
            request.AddParameter("1e006722-1644-9283-dbfd-0fb659eb7660", howMany);

            //if 8+ selected then specify
            request.AddParameter("1e006722-1644-9283-dbfd-0fb659eb7660_other_text",
                storageData.new_howmanycontainersspecify.HasValue ? storageData.new_howmanycontainersspecify.Value : 8);

            request.AddParameter("__match__3bcf3ff9-fb43-fada-e679-97198f2d2532_isrequired", "true");

            // What length storage container do you want? 
            // Not sure = 3d83f111-1c3b-7bd3-4295-89891ddda03a
            // 10 ft = 8171c167-6302-059c-686a-ec7b8872d797
            //20 ft = 273504fc-0cd5-0cbb-1170-0c3b463247e4
            // 40 ft = 2157b119-d42b-d0a8-7e74-8e27a54ef84b
            //48 ft = db61f27d-4931-b5f3-14d5-b1ac143c221c
            //Other (please specify) = bb95e85f-8d48-be82-1a05-fc8bb88b24f5
            string length;
            switch (storageData.new_storagelength.Value)
            {
                case (int)DataModel.Consumer.ServiceRequestStorageData.eStorageLength.not_sure:
                    length = "3d83f111-1c3b-7bd3-4295-89891ddda03a";
                    break;
                case (int)DataModel.Consumer.ServiceRequestStorageData.eStorageLength.ten:
                    length = "8171c167-6302-059c-686a-ec7b8872d797";
                    break;
                case (int)DataModel.Consumer.ServiceRequestStorageData.eStorageLength.twenty:
                    length = "273504fc-0cd5-0cbb-1170-0c3b463247e4";
                    break;
                case (int)DataModel.Consumer.ServiceRequestStorageData.eStorageLength.forty:
                    length = "2157b119-d42b-d0a8-7e74-8e27a54ef84b";
                    break;
                case (int)DataModel.Consumer.ServiceRequestStorageData.eStorageLength.forty_eight:
                    length = "db61f27d-4931-b5f3-14d5-b1ac143c221c";
                    break;
                default://other
                    length = "bb95e85f-8d48-be82-1a05-fc8bb88b24f5";
                    break;
            }
            request.AddParameter("__match__3bcf3ff9-fb43-fada-e679-97198f2d2532", length);

            //if other selected then specify
            request.AddParameter("__match__3bcf3ff9-fb43-fada-e679-97198f2d2532_other_text",
                storageData.new_storagelengthspecify.HasValue ? storageData.new_storagelengthspecify.Value.ToString() : "48");

            request.AddParameter("__match__c4df32fb-5008-ce6b-f6a4-b61bd38e5781_isrequired", "true");

            // How do you prefer to finance the storage container?
            //Not sure = 46dcb49e-21e5-dc31-ab68-90ee517e0ace
            //Rent/lease = a8c7b4f6-ae9d-3f5b-091c-9933f9884083
            //Purchase = cea4585c-2aec-c27a-589d-e64e01693f97
            // Lease to purchase = ebb16536-3996-1ebb-114b-c02877ef4fce
            string finance;
            switch (storageData.new_financestorage.Value)
            {
                case (int)DataModel.Consumer.ServiceRequestStorageData.eFinanceStorage.Purchase:
                    finance = "cea4585c-2aec-c27a-589d-e64e01693f97";
                    break;
                case (int)DataModel.Consumer.ServiceRequestStorageData.eFinanceStorage.Lease_to_purchase:
                    finance = "ebb16536-3996-1ebb-114b-c02877ef4fce";
                    break;
                case (int)DataModel.Consumer.ServiceRequestStorageData.eFinanceStorage.Rent_or_lease:
                    finance = "a8c7b4f6-ae9d-3f5b-091c-9933f9884083";
                    break;
                default://not sure
                    finance = "46dcb49e-21e5-dc31-ab68-90ee517e0ace";
                    break;
            }
            request.AddParameter("__match__c4df32fb-5008-ce6b-f6a4-b61bd38e5781", finance);

            request.AddParameter("__match__8b77a690-9a80-7ef5-ed31-40355e3dbf40_isrequired", "true");

            //When would you like your portable storage container delivered?
            //ASAP = 414275c3-0260-15f2-8c0d-75fcda329faa
            // one month = 5ef6341f-8c1c-2103-2636-f81a12f4b47f
            //  Two months = 544ebd91-74c5-efce-106f-0ad688a34abf
            // Three months or more  = 977f1d57-133a-ca70-119a-c52cf82bf392
            string when;
            switch (storageData.new_deliverystorage.Value)
            {
                case (int)DataModel.Consumer.ServiceRequestStorageData.eDeliveryStorage.ASAP:
                    when = "414275c3-0260-15f2-8c0d-75fcda329faa";
                    break;
                case (int)DataModel.Consumer.ServiceRequestStorageData.eDeliveryStorage.one_month:
                    when = "5ef6341f-8c1c-2103-2636-f81a12f4b47f";
                    break;
                case (int)DataModel.Consumer.ServiceRequestStorageData.eDeliveryStorage.two_months:
                    when = "544ebd91-74c5-efce-106f-0ad688a34abf";
                    break;
                default://three or more
                    when = "977f1d57-133a-ca70-119a-c52cf82bf392";
                    break;
            }
            request.AddParameter("__match__8b77a690-9a80-7ef5-ed31-40355e3dbf40", when);

            request.AddParameter("_zipcodeQuestion__isrequired", "true");
            request.AddParameter("_zipcodeQuestion__istypezipcode", "true");

            // If you are moving, what is the destination ZIP code?
            request.AddParameter("b25fc38f-ef9f-0980-c52d-bc2678c100c9", storageData.new_zipcodeother);

            request.AddParameter("_emailQuestion__isrequired", "true");
            request.AddParameter("_emailQuestion__istypeemail", "true");

            // Please describe any additional requirements for your storage container
            request.AddParameter("_additionalRequirementsQuestion_", incident.description);
        }
    }
}

/*
     200	Request is successfully entered into the system
    − 100	Duplicate buyer contact
    − 150	Buyer has been banned
    − 200	No seller coverage for the buyer Zip Code
    − 300	Invalid Publisher ID
    − 400	Invalid Question Set version ID
    − 600	Missing required questions and answers
    − 700	Missing required contact information
    − 800	Unknown error
     * 
     * don't expect status code 200
     * 
     * header: hostAndPostStatus has status
     * 
<?xml version="1.0" encoding="UTF-8"?>
<status>
<name>Request is Unmatched</name>
<code>-200</code>
<errors>
<error>No seller coverage for the buyer ZIP code</error>
</errors>
<post_parameters>phone=2011234567&amp;_additionalRequirementsQuestion_=bla bla bla&amp;d76c3d23-f5c7-0fc5-36e7-d2d5b54b5ff1=c5e3da8b-756a-ed84-daa1-186aaca32ae4&amp;_zipcodeQuestion__istypezipcode=true&amp;1e006722-1644-9283-dbfd-0fb659eb7660_isrequired=true&amp;__match__44577e04-7741-fafb-ca96-68c8a497f409=5bc15637-491b-2083-c94a-1222ca6d6d6a&amp;publisherTypeId=1798&amp;address.streetName=333 86th west&amp;_questionSetVersionId_=questions_13&amp;__match__8b77a690-9a80-7ef5-ed31-40355e3dbf40_isrequired=true&amp;1e006722-1644-9283-dbfd-0fb659eb7660=567d14dd-4b44-4067-4355-142a42096a7a&amp;firstName=Lester&amp;__match__c4df32fb-5008-ce6b-f6a4-b61bd38e5781_isrequired=true&amp;__match__3bcf3ff9-fb43-fada-e679-97198f2d2532=3d83f111-1c3b-7bd3-4295-89891ddda03a&amp;lastName=Tester&amp;publisherId=39650&amp;_emailQuestion_=alain@test.com&amp;_zipcodeQuestion__isrequired=true&amp;__match__3bcf3ff9-fb43-fada-e679-97198f2d2532_isrequired=true&amp;_emailQuestion__isrequired=true&amp;__match__c4df32fb-5008-ce6b-f6a4-b61bd38e5781=46dcb49e-21e5-dc31-ab68-90ee517e0ace&amp;_zipcodeQuestion_=10001&amp;b25fc38f-ef9f-0980-c52d-bc2678c100c9=10019&amp;keywordID=&amp;__match__8b77a690-9a80-7ef5-ed31-40355e3dbf40=414275c3-0260-15f2-8c0d-75fcda329faa&amp;_emailQuestion__istypeemail=true&amp;__match__44577e04-7741-fafb-ca96-68c8a497f409_isrequired=true</post_parameters>
</status>
     */
