﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class MovingGuru : LeadBuyerMoverBase
    {
        private const string URL = "http://www.movingguru.com/api";
        private const string POST_RESOURCE = "post";
        private const string PING_RESOURCE = "ping";
        private const string API_KEY = "a9a41b4dfa871521cb337da64a81f31e";
        private const string API_USER = "MGGU0004";
        private const string DATE_FORMAT = "MM/dd/yyyy";

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                if (!Validate(incident))
                {
                    retVal.Reason = validationMessage;
                    retVal.FailedInnerValidation = true;
                    retVal.WantsToBid = false;
                    return retVal;
                }

                int weight = GetMoveWeightInLbs(incident.new_movesize);

                RestSharp.RestClient client = new RestSharp.RestClient(URL);
                RestSharp.RestRequest request = new RestSharp.RestRequest(PING_RESOURCE, RestSharp.Method.POST);
                request.AddParameter("api_user", API_USER);
                request.AddParameter("api_key", API_KEY);
                request.AddParameter("from", incident.new_new_region_incident.new_name);
                request.AddParameter("to", incident.new_movetozipcode);
                request.AddParameter("movingweight", weight);
                request.AddParameter("movedate", incident.new_movedate.Value.ToString(DATE_FORMAT));
                RestSharp.IRestResponse res = client.Execute(request);
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    PingResponse data;
                    try
                    {
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<PingResponse>(res.Content);
                    }
                    catch (Exception exc)
                    {
                        throw new Exception("In PING: Unable to parse JSON response from MovingGuru. Content received = '" + res.Content + "'. Parser exception: '"+ exc.Message +"'.", exc);
                    }
                    retVal.BrokerId = data.token;
                    retVal.Price = data.price;
                    if (retVal.Price <= 0)
                    {
                        retVal.Reason = "Price given was 0. Rejected.";
                        retVal.MarkAsRejected = true;
                        retVal.WantsToBid = false;
                    }
                }
                else
                {
                    retVal.Reason = String.Format("Ping returned with bad HTTP status {0}.", res.StatusCode);
                    retVal.MarkAsRejected = true;
                    retVal.WantsToBid = false;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in MovingGuru.GetBrokerBid");
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                RestSharp.RestClient client = new RestSharp.RestClient(URL);
                RestSharp.RestRequest request = new RestSharp.RestRequest(POST_RESOURCE, RestSharp.Method.POST);
                request.AddParameter("api_user", API_USER);
                request.AddParameter("api_key", API_KEY);
                request.AddParameter("token", pingId);
                request.AddParameter("firstname", contact.firstname);
                request.AddParameter("lastname", contact.lastname);
                request.AddParameter("email", contact.emailaddress1);
                request.AddParameter("phone", incident.new_telephone1);
                RestSharp.IRestResponse res = client.Execute(request);
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    PostResponse data;
                    try
                    {
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<PostResponse>(res.Content);
                    }
                    catch (Exception exc)
                    {
                        throw new Exception("In POST: Unable to parse JSON response from MovingGuru. Content received = '" + res.Content + "'. Parser exception: '"+ exc.Message +"'.", exc);
                    }
                    if (data.message == "success")
                    {
                        retVal.IsSold = true;
                    }
                    else
                    {
                        string errors = String.Empty;
                        if (data.errors != null)
                        {
                            foreach (var item in data.errors)
                            {
                                errors += (item + ". ");
                            }
                        }
                        retVal.Reason = "REJECTED AT POST!!! message: " + data.message + ". errors: " + errors;
                        retVal.MarkAsRejected = true;
                    }
                }
                else
                {
                    throw new Exception(String.Format("Bad HTTP status received in POST. http status = {0}", res.StatusCode));
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in MovingGuru.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                    retVal.MarkAsRejected = true;
                }
            }
            return retVal;
        }

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            ValidateFirstAndLastName(incident);
            ValidateMoveDate(incident);
            ValidateMoveSize(incident);
            ValidateMoveToZipcode(incident);
            ValidateEmail(incident);
        }

        private int GetMoveWeightInLbs(string moveSize)
        {
            int retVal = 6000;
            switch (moveSize)
            {
                case "studio":
                    retVal = 1600;
                    break;
                case "oneBdr":
                    retVal = 3000;
                    break;
                case "twoBdr":
                    retVal = 6000;
                    break;
                case "threeBdr":
                    retVal = 8000;
                    break;
                case "fourBdr":
                    retVal = 10000;
                    break;
                case "fiveBdr":
                    retVal = 15000;
                    break;
                case "larger":
                    retVal = 20000;
                    break;
            }
            return retVal;
        }

        private class PingResponse
        {
            public decimal price { get; set; }
            public string token { get; set; }
        }

        private class PostResponse
        {
            public string message { get; set; }
            public IEnumerable<string> errors { get; set; }
            public string bidder { get; set; }
            public decimal price { get; set; }
        }
    }
}
