﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using Effect.Crm.Logs;
using Newtonsoft.Json;


namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class LeadsBarter : LeadBuyerBase
    {
        private const string API_KEY = "wJsk7H23shS3";
        private const string URL_PING = "http://services.leadsbarter.com/ping";
        private const string URL_POST = "http://services.leadsbarter.com/post";
        
        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {               
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = "Region is not zipcode level.";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string zipcode = region.new_name;
                string fname = incident.incident_customer_contacts.firstname;
                string lname = incident.incident_customer_contacts.lastname;
                if (String.IsNullOrEmpty(fname) || String.IsNullOrEmpty(lname))
                {
                    retVal.Reason = "First name and last name are mandatory.";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string address = incident.incident_customer_contacts.address1_line1;
                if (String.IsNullOrEmpty(address))
                {
                    retVal.Reason = "Address is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string email = incident.incident_customer_contacts.emailaddress1;
                if (String.IsNullOrEmpty(email))
                {
                    retVal.Reason = "Email is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                bool isTest = System.Configuration.ConfigurationManager.AppSettings["SiteId"] != "Sales";
                string IncidentId = incident.incidentid.ToString();//incident.ticketnumber;
                DateTime _created_on = (incident.createdon.HasValue) ? incident.createdon.Value : DateTime.Now;
                NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpResponse pingResponse =
                    DoPing(zipcode, IncidentId, _created_on, accExp, isTest);


                if (pingResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JsonSerializer serialize = new JsonSerializer();
                    JObject jobj = JObject.Parse(pingResponse.Content);

                    if (jobj.Count == 0)
                        return retVal;

                    string _status = jobj["response"]["status"].Value<string>();
                    if (_status == "success")
                    {
                        string _token = jobj["response"]["token"] != null ? jobj["response"]["token"].Value<string>() : string.Empty;
                        string _price = jobj["response"]["price"] != null ? jobj["response"]["price"].Value<string>() : string.Empty;
                        string _slots_accepted = jobj["response"]["slots_accepted"] != null ? jobj["response"]["slots_accepted"].Value<string>() : string.Empty;
                        SendPostRequest(incident, IncidentId, fname, lname, retVal, zipcode, _token,
                            incident.incident_customer_contacts.emailaddress1, _created_on,
                            incident.new_telephone1, address, accExp.new_externalcode);
                    }
                    else
                    {
                        string _message = jobj["response"]["message"] != null ? jobj["response"]["message"].Value<string>() : string.Empty;
                        retVal.Reason = _message;
                        retVal.MarkAsRejected = true;
                    }
                }
                else
                {
                    LogUtils.MyHandle.WriteToLog("Failed to parse ping response from LeadsBarter. httpStatuCode = {1}. content: {0}", pingResponse.Content, pingResponse.StatusCode);
                    retVal.Reason = "Failed to parse ping LeadsBarter's resposne: " + pingResponse.Content;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in LeadsBarter.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpResponse DoPing(string zipCode, string IncidentId, DateTime CreatedOn,
            NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, bool isTest)
        {

            NoProblem.Core.BusinessLogic.Utils.HttpClient client = new NoProblem.Core.BusinessLogic.Utils.HttpClient();
            client.Url = URL_PING;
            client.HttpMethod = NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpMethodEnum.Post;
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("key", API_KEY);
            nvc.Add("lead_id", accExp.new_externalcode);
            nvc.Add("zip", zipCode);
            nvc.Add("created", CreatedOn.ToString("yyyy-MM-dd HH:mm:ss"));
            nvc.Add("source_id", IncidentId);
            client.ParamType = NoProblem.Core.BusinessLogic.Utils.HttpClient.ParamTypeEnum.Content;
            client.ContentData = GetJson(nvc, true);
            NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpResponse ans = client.Post();
            return ans;
        }

        private void SendPostRequest(DataModel.Xrm.incident incident, string IncidentId, string FirstName, string LastName,
            SendLeadResponse retVal, string zipcode, string _token, string email, DateTime CreatedOn, string phone,
            string Address, string ExternalCode)
        {
            NoProblem.Core.BusinessLogic.Utils.HttpClient client = new NoProblem.Core.BusinessLogic.Utils.HttpClient();
            client.Url = URL_POST;
            client.ParamType = NoProblem.Core.BusinessLogic.Utils.HttpClient.ParamTypeEnum.Content;
            client.HttpMethod = NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpMethodEnum.Post;
            string _created = CreatedOn.ToString("yyyy-MM-dd HH:mm:ss");
         
            var _jsonPost = new
            {
                lead = new
                    {
                        key = API_KEY,
                        lead_id = ExternalCode,
                        zip = zipcode,
                        created = _created,
                        source_id = IncidentId,
                        first_name = FirstName,
                        last_name = LastName,
                        email = email,
                        phone = phone,
                        address = Address,
                        token = _token                
                    }
            };
           
            client.ContentData = Newtonsoft.Json.JsonConvert.SerializeObject(_jsonPost);
            NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpResponse ans = client.Post();

            if (ans.StatusCode == System.Net.HttpStatusCode.OK)
            {
                JsonSerializer serialize = new JsonSerializer();
                JObject jobj = JObject.Parse(ans.Content);

                if (jobj.Count == 0)
                    return;
                string _status = jobj["response"]["status"] != null ? jobj["response"]["status"].Value<string>() : string.Empty;
                if (_status == "success")
                {
                    string job_id = jobj["response"]["job_id"] != null ? jobj["response"]["job_id"].Value<string>() : string.Empty;
                    retVal.IsSold = true;
                    retVal.BrokerId = job_id;
                }
                else
                {
                    string _message = jobj["response"]["message"] != null ? jobj["response"]["message"].Value<string>() : string.Empty;
                    retVal.Reason = _message;
                    retVal.MarkAsRejected = true;
                }
            }
            else
            {
                LogUtils.MyHandle.WriteToLog("LeadsBarter did not return 'success' at post stage. httpStatuCode = {1}. content: {0}", ans.Content, ans.StatusCode);
                retVal.Reason = "LeadsBarter didn't return 'success'. message: " + ans.Content;
            }
        }

        string GetJson(NameValueCollection nvc, bool IsPing)
        {
            Dictionary<string, Dictionary<string, string>> dic = new Dictionary<string, Dictionary<string, string>>();
            string name = IsPing ? "ping" : "lead";
            dic.Add(name, new Dictionary<string, string>());
            foreach (string key in nvc)
            {
                dic[name].Add(key, nvc[key]);
            }
            return JsonConvert.SerializeObject(dic);
        }
    }
}
