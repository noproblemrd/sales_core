﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class ElocalDynamicPhone : DynamicPhone
    {
        private const string URL = "http://api.elocal.com/call/272a589139732f7454df85f4b8fd7d73f758350c/ping.json";

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = null;
            try
            {
                retVal = GetBrokerBid(incident.new_new_primaryexpertise_incident.new_code, incident, URL);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ElocalDynamicPhone.GetBrokerBid");
                if(retVal == null)
                    retVal = new GetBrokerBidResponse();
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }
    }
}
