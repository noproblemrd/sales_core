﻿using System;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class SevaSearch : LeadBuyerBase
    {
        private const string VERIFY_URL = "http://www.sevacall.com/3rdparty/leads/v2/verify/";
        private const string SUBMIT_URL = "http://www.sevacall.com/3rdparty/leads/v2/submit/";
        private const string API_KEY = "NoProblem";
        private const string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                if (!Validate(incident))
                {
                    retVal.Reason = validationMessage;
                    retVal.FailedInnerValidation = true;
                    retVal.WantsToBid = false;
                    return retVal;
                }
                RestSharp.IRestClient client = new RestSharp.RestClient(VERIFY_URL);
                RestSharp.IRestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
                request.AddParameter("id", incident.ticketnumber);
                var heading = incident.new_new_primaryexpertise_incident;
                request.AddParameter("category", heading.new_code);
                request.AddParameter("category_name", heading.new_name);
                request.AddParameter("category_sub_name", heading.new_name);
                request.AddParameter("zipcode", incident.new_new_region_incident.new_name);
                request.AddParameter("description", incident.description);
                request.AddParameter("exclusive", "y");
                request.AddParameter("api_key", API_KEY);
                request.AddParameter("collected_datetime", incident.createdon.Value.ToString(DATE_FORMAT));
                request.AddParameter("lcpld", incident.ticketnumber);
                var response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK ||
                    response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    if (response.Content != "no")
                    {
                        if (response.Content.StartsWith("$"))
                        {
                            string priceWithoutDollarSign = response.Content.Substring(1);
                            decimal price = Decimal.Parse(priceWithoutDollarSign);
                            retVal.Price = price;
                            retVal.BrokerId = priceWithoutDollarSign;
                        }
                        else
                        {
                            retVal.Reason = "Bad response from SevaCall. Content: '" + response.Content + "'";
                            retVal.WantsToBid = false;
                            retVal.MarkAsRejected = false;
                        }
                    }
                    else
                    {
                        retVal.Reason = "Sevacall ping response: '" + response.Content + "'";
                        retVal.WantsToBid = false;
                        retVal.MarkAsRejected = false;
                    }
                }
                else//bad http status
                {
                    retVal.Reason = "Sevacall ping response with bad http status: '" + response.StatusCode + "', content: '" + response.Content + "'";
                    retVal.WantsToBid = false;
                    retVal.MarkAsRejected = false;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SevaSearch.GetBrokerBid");
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
                retVal.MarkAsRejected = false;
            }
            return retVal;
        }

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            ValidateEmail(incident);
            ValidateFirstAndLastName(incident);
            ValidateRegion(incident);
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                RestSharp.IRestClient client = new RestSharp.RestClient(SUBMIT_URL);
                RestSharp.IRestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
                request.AddParameter("id", incident.ticketnumber);
                var heading = incident.new_new_primaryexpertise_incident;
                request.AddParameter("category", heading.new_code);
                request.AddParameter("category_name", heading.new_name);
                request.AddParameter("category_sub_name", heading.new_name);
                request.AddParameter("zipcode", incident.new_new_region_incident.new_name);
                request.AddParameter("description", incident.description);
                request.AddParameter("exclusive", "y");
                request.AddParameter("api_key", API_KEY);
                request.AddParameter("collected_datetime", incident.createdon.Value.ToString(DATE_FORMAT));
                request.AddParameter("lcpld", incident.ticketnumber);
                request.AddParameter("cpl", pingId);
                request.AddParameter("phone", incident.new_telephone1);
                request.AddParameter("name", contact.fullname);
                request.AddParameter("email", contact.emailaddress1);
                var response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK ||
                    response.StatusCode == System.Net.HttpStatusCode.Accepted)
                {
                    if (response.Content == "submitted")
                    {
                        retVal.IsSold = true;
                    }
                    else
                    {
                        retVal.MarkAsRejected = true;
                        retVal.Reason = String.Format("Rejected at POST with message '{0}'", response.Content);
                    }
                }
                else//bad http status
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = String.Format("Seva response has bad http status '{1}' at POST with message '{0}'", response.Content, response.StatusCode);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in SevaSearch.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }
    }
}
