﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.Utils;
using System.Xml.Linq;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class Identifyle : LeadBuyerBase
    {
        private const string url = "http://idtrkr.com/post.ashx";
        private const string XML = "xml";
        private const string badRegionLevelError = "Region is not zipcode level.";
        private const string campaingId = "8690";
        private const string notAcceptedReasonStr = "Identifyle returned unsuccessful status '{0}'. With error '{1}'.";
        private const string badHttpReasonStr = "Identifyle returned with bad HTTP status. HttpStatus = {0}. Content = {1}.";
        private const string excMessageForLog = "Exception in Identifyle.SendLead";
        private const string excMessageForReason = "Exception sending lead";

        #region ILeadBuyer Members

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = badRegionLevelError;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string zipcode = region.new_name;
                var contact = incident.incident_customer_contacts;
                HttpClient client = new HttpClient(url, HttpClient.HttpMethodEnum.Get);
                client.ParamItems.Add(ParamNames.CID, campaingId);
                client.ParamItems.Add(ParamNames.FULLNAME, contact.fullname);
                client.ParamItems.Add(ParamNames.PHONE, incident.new_telephone1);
                client.ParamItems.Add(ParamNames.ZIP, zipcode);
                client.ParamItems.Add(ParamNames.RESPTYPE, XML);
                var res = client.Post();
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    XElement xml = XElement.Parse(res.Content);
                    string status = xml.Element(XmlNames.STATUS).Value;
                    if (status == XmlNames.SUCCESS)
                    {
                        retVal.IsSold = true;
                        string leadId = xml.Element(XmlNames.LEADID).Value;
                        retVal.BrokerId = leadId;
                    }
                    else
                    {
                        retVal.MarkAsRejected = true;
                        string error = xml.Element(XmlNames.ERROR).Value;
                        retVal.Reason = string.Format(notAcceptedReasonStr, status, error);
                    }
                }
                else
                {
                    retVal.Reason = string.Format(badHttpReasonStr, res.StatusCode, res.Content);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, excMessageForLog);
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = excMessageForReason;
                }
            }
            return retVal;
        }

        #endregion

        private static class ParamNames
        {
            internal const string CID = "cid";
            internal const string FULLNAME = "fullname";
            internal const string PHONE = "phone";
            internal const string ZIP = "zip";
            internal const string RESPTYPE = "respType";
        }

        private static class XmlNames
        {
            internal const string STATUS = "status";
            internal const string LEADID = "leadid";
            internal const string ERROR = "error";
            internal const string SUCCESS = "success";
        }
    }
}

/*
Success response
<result>
<status>success</status>
<leadid>7569234</leadid>
<error/>
</result>

Failure response
<result>
<status>failure</status>
<leadid>-1</leadid>
<error>Zip code is invalid</error>
</result>


Text Responses

success		Lead was successfully received
failure		Failure reason will be provided
duplicate 	Lead already exists in the system and was not accepted
error		A fatal error occurred
     
*/