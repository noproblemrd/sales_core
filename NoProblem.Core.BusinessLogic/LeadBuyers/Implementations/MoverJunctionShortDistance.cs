﻿
namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class MoverJunctionShortDistance : LeadBuyerBase
    {
        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            MoverJunction moverJuction = new MoverJunction();
            return moverJuction.SendLead(incident, accExp, supplier, incAcc, pingId, false);
        }
    }
}
