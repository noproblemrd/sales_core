﻿using System;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class HomeImprovementLeads : LeadBuyerBase, IPingPostApiBuyer
    {
        private const string regionError = "Region is not zip code level.";
        private const string URL = "http://lm.hilprod.com/lead/";
        private const string PING = "ping";
        private const string POST = "post";
        private const string KEY = "b6a-bjd0E6hyLeE3E6udE3TNh4ErheO7L4OkEWT0h6XrbnTkOHNqE4yrGkD2";
        private const string SRC = "Aff165";
        private const string LANDING_PAGE = "NP";
        private const string YES = "Yes";

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                var email = contact.emailaddress1;
                if (String.IsNullOrEmpty(firstName) || String.IsNullOrEmpty(lastName))
                {
                    retVal.Reason = FNAME_LNAME_ARE_MANDATORY;
                    retVal.WantsToBid = false;
                    return retVal;
                }
                if (String.IsNullOrEmpty(email))
                {
                    retVal.Reason = EMAIL_IS_MANDATORY;
                    retVal.WantsToBid = false;
                    return retVal;
                }
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = regionError;
                    retVal.WantsToBid = false;
                    return retVal;
                }
                string address = contact.address1_line1;
                if (String.IsNullOrEmpty(address))
                {
                    retVal.Reason = ADDRESS_IS_MANDATORY;
                    retVal.WantsToBid = false;
                    return retVal;
                }

                PingData pingData = new PingData()
                {
                    ExternalCode = accExp.new_externalcode,
                    Zip = region.new_name
                };
                PingResult pingResult = Ping(pingData);
                if (pingResult.Accepted)
                {
                    retVal.WantsToBid = true;
                    retVal.BrokerId = pingResult.PingId;
                    retVal.Price = pingResult.PingPrice;
                }
                else
                {
                    retVal.WantsToBid = false;
                    retVal.Reason = pingResult.PingMessage;
                    retVal.MarkAsRejected = true;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in HomeImprovementLeads.GetBrokerBid");
                retVal.Reason = "Exception getting bid";
                retVal.WantsToBid = false;
            }
            return retVal;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                var email = contact.emailaddress1;
                var region = incident.new_new_region_incident;
                string address = contact.address1_line1;
                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                var parents = regDal.GetParents(region);
                string state = parents.First(x => x.new_level == 1).new_name;
                string[] regionNameSplit = region.new_englishname.Split(';');
                string zipCode = region.new_name;
                string city = regionNameSplit[1];

                PostData postData = new PostData()
                {
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email,
                    Zip = region.new_name,
                    ExternalCode = accExp.new_externalcode,
                    PingId = pingId,
                    Phone = incident.new_telephone1,
                    StreetAddress = address,
                    IP = incident.new_ip != null ? incident.new_ip : String.Empty
                };
                PostResult postResult = Post(postData);
                if (postResult.Accepted)
                {
                    retVal.IsSold = true;
                    retVal.BrokerId = postResult.PostId;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = postResult.PostMessage;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in HomeImprovementLeads.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        public PingResult Ping(PingData data)
        {
            PingResult pingResult = new PingResult();
            RestSharp.RestClient client = new RestSharp.RestClient(URL);
            RestSharp.RestRequest request = new RestSharp.RestRequest(PING, RestSharp.Method.GET);
            request.AddParameter(new RestSharp.Parameter() { Name = "Key", Value = KEY, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "SRC", Value = SRC, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Landing_Page", Value = LANDING_PAGE, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Zip", Value = data.Zip, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Project", Value = data.ExternalCode, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Return_Best_Price", Value = 1.ToString(), Type = RestSharp.ParameterType.GetOrPost });
            if (IsTest())
            {
                request.AddParameter(new RestSharp.Parameter() { Name = "Test_Lead", Value = 1.ToString(), Type = RestSharp.ParameterType.GetOrPost });
            }
            var response = client.Execute(request);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                try
                {
                    XElement xdoc = XElement.Parse(response.Content);
                    string status = xdoc.Element("status").Value;
                    if (status == "Matched")
                    {
                        string id = xdoc.Element("lead_id").Value;
                        pingResult.PingId = id;
                        pingResult.PingPrice = decimal.Parse(xdoc.Element("price").Value);
                        pingResult.Accepted = true;
                    }
                    else if (status == "Unmatched")
                    {
                        string id = xdoc.Element("lead_id").Value;
                        pingResult.PingMessage = String.Format("Rejected at ping stage: status = {0}. id = {1}.", status, id);
                    }
                    else
                    {
                        var errorElement = xdoc.Element("error");
                        string errorMsg = String.Empty;
                        if (errorElement != null)
                            errorMsg = errorElement.Value;
                        pingResult.PingMessage = String.Format("Rejected at ping stage: status = {0}. error message = {1}.", status, errorMsg);
                    }
                }
                catch (Exception)
                {
                    pingResult.PingMessage = String.Format("Failed to parse ping response. contents = {0}", response.Content);
                }
            }
            else
            {
                pingResult.PingMessage = String.Format("Rejected at ping stage with bad http status: httpStatus = {0}. contents = {1}", response.StatusCode, HttpUtility.HtmlEncode(response.Content));
            }
            return pingResult;
        }

        public PostResult Post(PostData data)
        {
            PostResult postResult = new PostResult();
            string city, state;
            ExtractZipcodeInfo(data.Zip, out city, out state);
            RestSharp.RestClient client = new RestSharp.RestClient(URL);
            RestSharp.RestRequest request = new RestSharp.RestRequest(POST, RestSharp.Method.GET);
            request.AddParameter(new RestSharp.Parameter() { Name = "Key", Value = KEY, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Lead_ID", Value = data.PingId, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "IP_Address", Value = data.IP, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "SRC", Value = SRC, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Landing_Page", Value = LANDING_PAGE, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "First_Name", Value = data.FirstName, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Last_Name", Value = data.LastName, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Address", Value = data.StreetAddress, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "City", Value = city, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Zip", Value = data.Zip, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "State", Value = state, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Email", Value = data.Email, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Home_Phone", Value = data.Phone, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Project", Value = data.ExternalCode, Type = RestSharp.ParameterType.GetOrPost });
            request.AddParameter(new RestSharp.Parameter() { Name = "Homeowner", Value = YES, Type = RestSharp.ParameterType.GetOrPost });
            if (IsTest())
            {
                request.AddParameter(new RestSharp.Parameter() { Name = "Test_Lead", Value = 1.ToString(), Type = RestSharp.ParameterType.GetOrPost });
            }
            if (data.ExternalCode == "Siding")
            {
                request.AddParameter(new RestSharp.Parameter() { Name = "Material", Value = "Vinyl", Type = RestSharp.ParameterType.GetOrPost });
            }
            else if (data.ExternalCode.StartsWith("Window"))
            {
                request.AddParameter(new RestSharp.Parameter() { Name = "Hear", Value = "2-3 windows", Type = RestSharp.ParameterType.GetOrPost });
            }
            var response = client.Execute(request);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                try
                {
                    XElement xdoc = XElement.Parse(response.Content);
                    string status = xdoc.Element("status").Value;
                    if (status == "Matched")
                    {
                        postResult.Accepted = true;
                        string id = xdoc.Element("lead_id").Value;
                        if (!String.IsNullOrEmpty(id))
                            postResult.PostId = id;
                        else
                            postResult.PostId = data.PingId;
                    }
                    else if (status == "Unmatched")
                    {
                        string id = xdoc.Element("lead_id").Value;
                        if (String.IsNullOrEmpty(id))
                            id = data.PingId;
                        postResult.PostMessage = String.Format("Unmatched at post stage. status = {0}. id = {1}.", status, id);
                    }
                    else
                    {
                        var errorElement = xdoc.Element("error");
                        string errorMsg = String.Empty;
                        if (errorElement != null)
                            errorMsg = errorElement.Value;
                        postResult.PostMessage = String.Format("Error at post stage. status = {0}. error message = {1}.", status, errorMsg);
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception parsing response in HomeImprovementLeads.SendLead");
                    postResult.PostMessage = String.Format("Exception parsing response.Contents = {0}", HttpUtility.HtmlEncode(response.Content));
                }
            }
            else
            {
                postResult.PostMessage = String.Format("Bad httpStatus response at post stage. httpStatus = {0}. Contents = {1}.", response.StatusCode, HttpUtility.HtmlEncode(response.Content));
            }
            return postResult;
        }
    }
}
