﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class Triares : LeadBuyerBase, IPingPostApiBuyer
    {
        private const string URL = "https://api.wiserleads.com/services";
        private const string PING_RESOURCE = "ping";
        private const string POST_RESOURCE = "post";
        private const string CAMPAIGN = "noproblem";
        private const string CAMPAIGN_TOKEN = "0f1b530be298b3ba9606d14c6385858f2d32b40c";
        private const string YES = "Yes";
        private const string TCPA_CONSENT_LANGUAGE = "By submitting this form, I authorize service providers to contact me back regarding my request";
        private const string DEFAULT_IP = "0.0.0.1";
        private string description;

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                if (!Validate(incident))
                {
                    retVal.Reason = validationMessage;
                    retVal.FailedInnerValidation = true;
                    retVal.WantsToBid = false;
                    return retVal;
                }

                PingData pingData = new PingData()
                {
                    Zip = incident.new_new_region_incident.new_name,
                    ExternalCode = accExp.new_externalcode
                };
                description = incident.description;
                PingResult pingResult = Ping(pingData);
                if (pingResult.Accepted)
                {
                    retVal.BrokerId = pingResult.PingId;
                    retVal.Price = pingResult.PingPrice;
                }
                else
                {
                    retVal.WantsToBid = false;
                    retVal.MarkAsRejected = true;
                    retVal.Reason = pingResult.PingMessage;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Triares.GetBrokerBid");
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                PostData postData = new PostData()
                {
                    Zip = incident.new_new_region_incident.new_name,
                    Email = contact.emailaddress1,
                    FirstName = contact.firstname,
                    IP = incident.new_ip,
                    LastName = contact.lastname,
                    PingId = pingId,
                    Phone = incident.new_telephone1,
                    StreetAddress = contact.address1_line1
                };
                PostResult postResult = Post(postData);
                if (postResult.Accepted)
                {
                    retVal.BrokerId = postResult.PostId;
                    retVal.IsSold = true;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = postResult.PostMessage;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Triares.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            ValidateFirstAndLastName(incident);
            ValidateEmail(incident);
            ValidateAddress(incident);
            ValidateRegion(incident);
        }

        private class PingRequest : Request
        {
            [Newtonsoft.Json.JsonProperty("description")]
            public string Description { get; set; }
            [Newtonsoft.Json.JsonProperty("service")]
            public string ServiceCode { get; set; }
        }

        private class PostRequest : Request
        {
            [Newtonsoft.Json.JsonProperty("lead_token")]
            public string LeadToken { get; set; }
            [Newtonsoft.Json.JsonProperty("firstname")]
            public string FirstName { get; set; }
            [Newtonsoft.Json.JsonProperty("lastname")]
            public string LastName { get; set; }
            [Newtonsoft.Json.JsonProperty("street_address")]
            public string StreetAddress { get; set; }
            [Newtonsoft.Json.JsonProperty("city")]
            public string City { get; set; }
            [Newtonsoft.Json.JsonProperty("state")]
            public string State { get; set; }
            [Newtonsoft.Json.JsonProperty("phone")]
            public string Phone { get; set; }
            [Newtonsoft.Json.JsonProperty("email")]
            public string Email { get; set; }
            [Newtonsoft.Json.JsonProperty("ip_address")]
            public string IPAddress { get; set; }
            [Newtonsoft.Json.JsonProperty("tcpa_consent")]
            public string TCPAConsent { get; set; }
            [Newtonsoft.Json.JsonProperty("tcpa_consent_language")]
            public string TCPAConsentLanguage { get; set; }

            public PostRequest()
            {
                TCPAConsent = YES;
                TCPAConsentLanguage = TCPA_CONSENT_LANGUAGE;
            }
        }

        private abstract class Request
        {
            [Newtonsoft.Json.JsonProperty("campaign")]
            public string Campaign { get; set; }
            [Newtonsoft.Json.JsonProperty("campaign_token")]
            public string CampaignToken { get; set; }
            [Newtonsoft.Json.JsonProperty("zipcode")]
            public string Zipcode { get; set; }

            public Request()
            {
                Campaign = CAMPAIGN;
                CampaignToken = CAMPAIGN_TOKEN;
            }
        }

        private class PingResponse : PostResponse
        {
            [Newtonsoft.Json.JsonProperty("payout")]
            public decimal Payout { get; set; }
        }

        private class PostResponse
        {
            [Newtonsoft.Json.JsonProperty("success")]
            public bool IsSuccess { get; set; }
            [Newtonsoft.Json.JsonProperty("lead_token")]
            public string LeadToken { get; set; }
            [Newtonsoft.Json.JsonProperty("error")]
            public string Error { get; set; }
        }

        public PingResult Ping(PingData data)
        {
            PingResult pingResult = new PingResult();
            RestSharp.RestClient client = new RestSharp.RestClient(URL);
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.RequestFormat = RestSharp.DataFormat.Json;
            PingRequest p = new PingRequest()
            {
                Zipcode = data.Zip,
                Description = description,
                ServiceCode = data.ExternalCode
            };
            request.JsonSerializer = new Utils.RestSharpJsonNetSerializer();
            request.AddBody(p);
            request.Resource = PING_RESOURCE;

            RestSharp.IRestResponse resp = client.Execute(request);
            PingResponse responseData = null;
            try
            {
                responseData = (PingResponse)Newtonsoft.Json.JsonConvert.DeserializeObject<PingResponse>(resp.Content);
            }
            catch (Exception) { }
            if (responseData == null)
            {
                pingResult.Accepted = false;
                pingResult.PingMessage = "Failed to parse Triares response at ping stage. Contact tech support. content: " + resp.Content;
            }
            else
            {
                if (responseData.IsSuccess)
                {
                    pingResult.Accepted = true;
                    pingResult.PingId = responseData.LeadToken;
                    pingResult.PingPrice = responseData.Payout;
                }
                else
                {
                    pingResult.Accepted = false;
                    pingResult.PingMessage = "Rejected at Ping. Reason: " + responseData.Error;
                }
            }
            return pingResult;
        }

        public PostResult Post(PostData data)
        {
            PostResult postResult = new PostResult();
            RestSharp.RestClient client = new RestSharp.RestClient(URL);
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.RequestFormat = RestSharp.DataFormat.Json;
            string city, state;
            ExtractZipcodeInfo(data.Zip, out city, out state);
            PostRequest p = new PostRequest()
            {
                Zipcode = data.Zip,
                City = city,
                Email = data.Email,
                FirstName = data.FirstName,
                IPAddress = String.IsNullOrEmpty(data.IP) ? DEFAULT_IP : data.IP,
                LastName = data.LastName,
                LeadToken = data.PingId,
                Phone = data.Phone,
                State = state,
                StreetAddress = data.StreetAddress
            };
            request.JsonSerializer = new Utils.RestSharpJsonNetSerializer();
            request.AddBody(p);
            request.Resource = POST_RESOURCE;

            RestSharp.IRestResponse resp = client.Execute(request);
            PostResponse responseData = null;
            try
            {
                responseData = (PostResponse)Newtonsoft.Json.JsonConvert.DeserializeObject<PostResponse>(resp.Content);
            }
            catch (Exception) { }
            if (responseData == null)
            {
                postResult.PostMessage = "Failed to parse Triares response at post stage. Contact tech support. content: " + resp.Content;
                postResult.Accepted = false;
            }
            else
            {
                if (responseData.IsSuccess)
                {
                    postResult.PostId = responseData.LeadToken;
                    postResult.Accepted = true;
                }
                else
                {
                    postResult.Accepted = false;
                    postResult.PostMessage = "Rejected at Post. Reason: " + responseData.Error;
                }
            }
            return postResult;
        }
    }
}
