﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal abstract class DynamicPhone : PhoneConnection
    {
        private const string OK = "ok";
        private const string NOT_INTERESTED = "not_interested";
        private const string ERROR = "error";
        
        public GetBrokerBidResponse GetBrokerBid(string categoryCode, DataModel.Xrm.incident incident, string url)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            var region = incident.new_new_region_incident;
            if (region.new_level != 4)
            {
                retVal.Reason = REGION_ERROR;
                retVal.WantsToBid = false;
                return retVal;
            }
            string zipcode = region.new_name;

            RestSharp.RestClient restClient = new RestSharp.RestClient(url);
            RestSharp.RestRequest restRequest = new RestSharp.RestRequest(RestSharp.Method.POST);
            restRequest.AddParameter("category", categoryCode);
            restRequest.AddParameter("zip", zipcode);
            restRequest.AddParameter("leadId", incident.incidentid.ToString());
            if (IsTest())
            {
                restRequest.AddParameter("isTest", 1);
            }
            var restResponse = restClient.Execute<PingResponse>(restRequest);
            if (restResponse.StatusCode == System.Net.HttpStatusCode.OK && restResponse.Data != null)
            {
                if (restResponse.Data.Status == OK)
                {
                    retVal.WantsToBid = true;
                    retVal.Price = restResponse.Data.Buyer.Bid;
                    retVal.BrokerId = restResponse.Data.Buyer.Phone + ";" + restResponse.Data.Token + ";" + restResponse.Data.Buyer.BuyerId;
                }
                else if (restResponse.Data.Status == NOT_INTERESTED)
                {
                    retVal.WantsToBid = false;
                    retVal.MarkAsRejected = true;
                    retVal.Reason = NOT_INTERESTED + ": " + restResponse.Data.Message;
                }
                else if (restResponse.Data.Status == ERROR)
                {
                    retVal.WantsToBid = false;
                    retVal.MarkAsRejected = true;
                    retVal.Reason = ERROR + ": " + restResponse.Data.Message;
                }
            }
            else
            {
                retVal.WantsToBid = false;
                retVal.MarkAsRejected = true;
                LogUtils.MyHandle.WriteToLog("DynamicPhone ping failed. ApiImpementationType = {2}, HttpStatus = {0}, Content = {1}", restResponse.StatusCode, restResponse.Content, this.GetType().ToString());
                retVal.Reason = String.Format("DynamicPhone ping failed. HttpStatus = {0}, Content = {1}", restResponse.StatusCode, restResponse.Content);
            }
            return retVal;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = null;
            try
            {
                string[] split = pingId.Split(';');
                string phone = split[0];
                retVal = SendLead(incident, accExp, incAcc, supplier.new_limitcallduration, phone, supplier.new_country, supplier.new_mincallduration);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DynamicPhone. ImplementationType = {0}", this.GetType().ToString());
                if (retVal == null)
                    retVal = new SendLeadResponse();
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private class PingResponse
        {
            public string Status { get; set; }
            public string Token { get; set; }
            public string Message { get; set; }
            public BuyerC Buyer { get; set; }

            internal class BuyerC
            {
                public string Phone { get; set; }
                public decimal Bid { get; set; }
                public string BuyerId { get; set; }
            }
        }
    }
}
