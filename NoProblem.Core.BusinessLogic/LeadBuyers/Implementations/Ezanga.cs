﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Configuration;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class Ezanga : LeadBuyerBase
    {
        private const string url = "http://apxml.ezanga.com:34500/util2/query.php";
        private const string publisherId = "1012";
        private const string publisherIdTest = "0000";
        private static readonly bool isTest = ConfigurationManager.AppSettings.Get("SiteId") != "Sales";
        private const string regionError = "Region is not zip code level.";
        private const string mediaType = "phn";
        private const string countryCode = "US";

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse bidResponse = new GetBrokerBidResponse();
            try
            {
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    bidResponse.Reason = regionError;
                    bidResponse.WantsToBid = false;
                    return bidResponse;
                }
                string zipcode = region.new_name;
                string city, state;
                ExtractZipcodeInfo(region, out city, out state);

                RestSharp.RestClient client = new RestSharp.RestClient(url);
                RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
                request.AddParameter("kw", incident.new_new_primaryexpertise_incident.new_name, RestSharp.ParameterType.GetOrPost);
                request.AddParameter("ip", "74.54.87.132", RestSharp.ParameterType.GetOrPost);
                request.AddParameter("src", incident.incidentid.ToString(), RestSharp.ParameterType.GetOrPost);
                request.AddParameter("pub", isTest ? publisherIdTest : publisherId, RestSharp.ParameterType.GetOrPost);
                request.AddParameter("co", countryCode, RestSharp.ParameterType.GetOrPost);
                request.AddParameter("ci", city, RestSharp.ParameterType.GetOrPost);
                request.AddParameter("st", state, RestSharp.ParameterType.GetOrPost);
                request.AddParameter("zip", zipcode, RestSharp.ParameterType.GetOrPost);
                request.AddParameter("ph", "123-456-7890", RestSharp.ParameterType.GetOrPost);
                request.AddParameter("mt", mediaType, RestSharp.ParameterType.GetOrPost);
                request.AddParameter("na", "1", RestSharp.ParameterType.GetOrPost);
                request.AddParameter("prod", isTest ? "0" : "1", RestSharp.ParameterType.GetOrPost);

                var response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    XDocument doc;
                    try
                    {
                        doc = XDocument.Parse(response.Content);
                        XElement root = doc.Element("AdPad");
                        XElement resultsElement = root.Element("results");
                        if (resultsElement != null)
                        {
                            var dataElement = resultsElement.Element("result");
                            if (dataElement != null)
                            {
                                string callId = dataElement.Element("callid").Value;
                                string phoneToCall = dataElement.Element("company_phone_number").Value;
                                phoneToCall = phoneToCall.Replace("-", String.Empty);
                                string bid = dataElement.Element("bid").Value;
                                string billableCallDuration = dataElement.Element("billable_call_duration").Value;
                                bidResponse.WantsToBid = true;
                                bidResponse.Price = decimal.Parse(bid);
                                bidResponse.BrokerId = phoneToCall + ";" + callId +";" + billableCallDuration;
                            }
                        }
                        if (String.IsNullOrEmpty(bidResponse.BrokerId))
                        {
                            bidResponse.WantsToBid = false;
                            bidResponse.Reason = "API returned no results";
                            bidResponse.MarkAsRejected = true;
                        }
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception parsing response in Ezanga.GetBrokerBid. content: {0}", response.Content);
                        bidResponse.WantsToBid = false;
                        bidResponse.Reason = "Exception parsing get bid response";
                    }
                }
                else
                {
                    bidResponse.WantsToBid = false;
                    bidResponse.Reason = String.Format("API returned bad http status. status = {0}. content = {1}", response.StatusCode.ToString(), response.Content);
                    bidResponse.MarkAsRejected = true;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Ezanga.GetBrokerBid");
                bidResponse.WantsToBid = false;
                bidResponse.Reason = "Exception getting bid";
            }
            return bidResponse;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = null;
            try
            {
                PhoneConnection phoneConnection = new PhoneConnection();
                string[] split = pingId.Split(';');
                string phone = split[0];
                int billableCallDuration = int.Parse(split[2]);
                retVal = phoneConnection.SendLead(incident, accExp, incAcc, supplier.new_limitcallduration, phone, supplier.new_country, billableCallDuration);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Ezange.SendLead");
                if (retVal == null)
                    retVal = new SendLeadResponse();
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }
    }
}

/*
Response:
 <AdPad>
   <copyright>eZanga.com, Inc. COPYRIGHT 2003-2013 All rights reserved.</copyright>
   <results>
      <result>
         <record>1</record>
         <userid>15398</userid>
         <callid>z1919282772.1.596H380A51404106480938</callid>
         <type>PPCall</type>
         <company_name>eZanga.com</company_name>
         <company_address>222 Carter Drive</company_address>
         <company_city>Middletown</company_city>
         <company_state>DE</company_state>
         <company_zip>19709</company_zip>
         <company_phone_number>888-439-2642</company_phone_number>
         <bid>20.00</bid>
         <billable_call_duration>120</billable_call_duration>
	   </result>
   </results>
</AdPad> 
*/