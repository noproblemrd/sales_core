﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class Oconco : LeadBuyerBase, IPingPostApiBuyer
    {
        private const string FORECLOSURE_URL = "http://www.oconcomarketing.com/lms/lead_source/foreclosure_lead_receive.php";
        private const string BANKRUPTCY_URL = "http://www.oconcomarketing.com/lms/lead_source/bankruptcy_lead_receive.php";

        private static Dictionary<string, string> urlMap = new Dictionary<string, string>
        {
            {"185", FORECLOSURE_URL},
            {"191", BANKRUPTCY_URL}
        };

        private string ip = null;
        private DataModel.Xrm.new_region region = null;

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                ip = incident.new_ip;
                var contact = incident.incident_customer_contacts;
                var email = contact.emailaddress1;
                if (String.IsNullOrEmpty(email))
                {
                    retVal.Reason = "Email is mandatory.";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = "Region is not zipcode level.";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string zipcode = region.new_name;
                var fname = contact.firstname;
                var lname = contact.lastname;
                var postResult = Post(
                    new PostData()
                    {
                        Id = incident.incidentid.ToString(),
                        Description = incident.description,
                        Zip = zipcode,
                        ExternalCode = accExp.new_externalcode,
                        Phone = incident.new_telephone1,
                        Email = email,
                        FirstName = fname,
                        LastName = lname
                    }
                    );
                if (postResult.Accepted)
                {
                    retVal.IsSold = true;
                    retVal.BrokerId = postResult.PostId;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = "Oconco didn't return 'success'. message: " + postResult.PostMessage;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Oconco.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        public PingResult Ping(PingData pingData)
        {
            PingResult result = new PingResult();
            result.PingId = "no ping";
            result.PingPrice = pingData.DefaultPrice;
            result.Accepted = true;
            return result;
        }

        public PostResult Post(PostData postData)
        {
            PostResult result = new PostResult();
            string isTest = IsTest() ? "true" : "false";
            string firstName = postData.FirstName;
            string lastName = postData.LastName;
            if (String.IsNullOrEmpty(firstName))
                firstName = "-";
            if (String.IsNullOrEmpty(lastName))
                lastName = "-";
            if (String.IsNullOrEmpty(ip))
                ip = "0.0.0.0";
            string city, state;
            if (region == null)
            {
                DataAccessLayer.Region regionDal = new DataAccessLayer.Region(null);
                region = regionDal.GetRegionByName(postData.Zip);
            }
            ExtractZipcodeInfo(region, out city, out state);
            RestSharp.RestClient client = new RestSharp.RestClient(urlMap[postData.ExternalCode]);
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
            request.AddParameter("vendor_id", postData.ExternalCode);
            request.AddParameter("ip_address", ip);
            request.AddParameter("first_name", firstName);
            request.AddParameter("last_name", lastName);
            request.AddParameter("address", "address");
            request.AddParameter("city", city);
            request.AddParameter("state", state);
            request.AddParameter("zip", postData.Zip);
            request.AddParameter("home_phone", postData.Phone);
            request.AddParameter("work_phone", postData.Phone);
            request.AddParameter("email", postData.Email);
            request.AddParameter("best_time_to_call", "Evening");
            request.AddParameter("lender_name", "lender");
            request.AddParameter("mortgage_balance", 1);
            request.AddParameter("property_value", 1);
            request.AddParameter("monthly_payment", 1);
            request.AddParameter("months_behind", 3);
            request.AddParameter("bankruptcy", "no");
            request.AddParameter("notes", postData.Description);
            request.AddParameter("debug", isTest);
            var response = client.Execute(request);
            result.PostMessage = response.Content;
            result.PostId = result.PostMessage;
            result.Accepted = response.Content.Contains("SUCCESS");
            return result;
        }
    }
}
