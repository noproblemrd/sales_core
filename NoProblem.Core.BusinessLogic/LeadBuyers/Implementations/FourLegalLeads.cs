﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class FourLegalLeads : LeadBuyerBase, IPingPostApiBuyer
    {
        private const string url = "https://leads.4legalleads.com/genericPostlead.php";
        private const string regionError = "Region is not zip code level.";

        #region ILeadBuyer Members

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                var email = contact.emailaddress1;
                if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
                {
                    retVal.Reason = "FirstName and LastName are mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                if (string.IsNullOrEmpty(email))
                {
                    retVal.Reason = "Email is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = regionError;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                string[] regionNameSplit = region.new_englishname.Split(';');
                string zipCode = regionNameSplit[0];
                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                var parents = regDal.GetParents(region);
                string state = parents.First(x => x.new_level == 1).new_name;

                var postData = new PostData()
                {
                    Description = incident.description,
                    Zip = zipCode,
                    ExternalCode = accExp.new_externalcode,
                    Phone = incident.new_telephone1,
                    Email = email,
                    FirstName = firstName,
                    LastName = lastName,
                };
                var postResult = Post(postData);
                retVal.IsSold = postResult.Accepted;
                if (retVal.IsSold)
                {
                    retVal.BrokerId = postResult.PostId;
                }
                else
                {
                    retVal.Reason = postResult.PostMessage;
                    retVal.MarkAsRejected = true;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in FourLegalLeads.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        #endregion

        public PingResult Ping(PingData pingData)
        {
            PingResult result = new PingResult();
            result.PingId = "no ping";
            result.PingPrice = pingData.DefaultPrice;
            result.Accepted = true;
            return result;
        }

        public PostResult Post(PostData postData)
        {
            PostResult result = new PostResult();
            string src = "NoProblem";
            if (IsTest())
            {
                src = "test";
            }
            HttpClient client = new HttpClient(url, HttpClient.HttpMethodEnum.Post);
            client.ParamItems.Add("TYPE", "31");
            client.ParamItems.Add("SRC", src);
            client.ParamItems.Add("Landing_Page", "noproblemppc.com");
            client.ParamItems.Add("First_Name", postData.FirstName);
            client.ParamItems.Add("Last_Name", postData.LastName);
            DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
            var regionData = regDal.GetRegionMinDataByName(postData.Zip);
            var parents = regDal.GetParents(regionData);
            string state = parents.First(x => x.Level == 1).Name;
            client.ParamItems.Add("State", state);
            client.ParamItems.Add("Zip", postData.Zip);
            client.ParamItems.Add("Phone", postData.Phone);
            client.ParamItems.Add("Email", postData.Email);
            client.ParamItems.Add("Best_Time_To_Call", "Anytime");
            client.ParamItems.Add("Type_Of_Legal_Problem", postData.ExternalCode);
            client.ParamItems.Add("Have_Attorney", "No");
            client.ParamItems.Add("Comments", postData.Description);
            client.ParamItems.Add("Meso_Question_1", "No");
            client.ParamItems.Add("Meso_Question_2", "No");
            client.ParamItems.Add("TCPA", "Yes");
            var ans = client.Post();

            if (ans.StatusCode == System.Net.HttpStatusCode.OK)
            {
                try
                {
                    XElement xml = XElement.Parse(ans.Content);
                    string status = xml.Element("status").Value;
                    if (status == "Matched" || status == "Unmatched")
                    {
                        string leadId = xml.Element("lead_id").Value;
                        result.PostId = leadId;
                        if (status == "Matched")
                        {
                            result.Accepted = true;
                        }
                        else
                        {
                            result.PostMessage = "4LegalLead returned 'Unmathced'. Content: " + ans.Content;
                        }
                    }
                    else
                    {
                        result.PostMessage = "4LegalLead didn't return 'matched'. Content: " + ans.Content;
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Exception parsing XML from 4LegalLeads. content = {0}", ans.Content);
                    result.PostMessage = string.Format("Exception parsing XML from 4LegalLeads. content = {0}", ans.Content);
                }
            }
            else
            {
                result.PostMessage = string.Format("Bad HttpStatus received from 4LegalLeads. Status = {0}, Content = {1}", ans.StatusCode, ans.Content);
                LogUtils.MyHandle.WriteToLog("Bad HttpStatus received from 4LegalLeads. Status = {0}, Content = {1}", ans.StatusCode, ans.Content);
            }
            return result;
        }
    }
}
