﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal abstract class LeadBuyerMoverBase : LeadBuyerBase
    {
        protected const string MOVE_TO_ZIP_MANDATORY = "Move to zipcode is mandatory.";
        protected const string MOVE_TO_ZIP_INVALID = "Move to zipcode is invalid.";
        protected const string MOVE_SIZE_MANDATORY = "Move size is mandatory.";
        protected const string MOVE_DATE_MANDTORY = "Move date is mandatory.";

        protected void ValidateMoveToZipcode(DataModel.Xrm.incident incident)
        {
            if (String.IsNullOrEmpty(incident.new_movetozipcode))
            {
                SetValidationFailureMessage(MOVE_TO_ZIP_MANDATORY);
            }
            DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
            var moveToRegion = regDal.GetRegionByName(incident.new_movetozipcode);
            if (moveToRegion == null)
            {
                SetValidationFailureMessage(MOVE_TO_ZIP_INVALID);
            }
        }

        protected void ValidateMoveDate(DataModel.Xrm.incident incident)
        {
            if (!incident.new_movedate.HasValue)
            {
                SetValidationFailureMessage(MOVE_DATE_MANDTORY);
            }
        }

        protected void ValidateMoveSize(DataModel.Xrm.incident incident)
        {
            if (String.IsNullOrEmpty(incident.new_movesize))
            {
                SetValidationFailureMessage(MOVE_SIZE_MANDATORY);
            }
        }
    }
}
