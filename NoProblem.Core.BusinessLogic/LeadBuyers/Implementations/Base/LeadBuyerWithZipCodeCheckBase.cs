﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal abstract class LeadBuyerWithZipCodeCheckBase : LeadBuyerBase
    {
        private const string ZIPCODE_NOT_IN_LIST = "Zip code not in list";

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            try
            {
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    SendLeadResponse retVal = new SendLeadResponse();
                    retVal.Reason = REGION_ERROR;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                if (!CheckZipCodeOk(region.new_name, accExp.new_accountexpertiseid))
                {
                    SendLeadResponse retVal = new SendLeadResponse();
                    retVal.Reason = ZIPCODE_NOT_IN_LIST;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                return SendLeadAfterZipCodeCheck(incident, accExp, supplier, incAcc, pingId, region.new_name);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in LeadBuyerWithZipCodeCheckBase.SendLead");
                SendLeadResponse retVal = new SendLeadResponse();
                retVal.Reason = "Exception sending lead";
                return retVal;
            }            
        }

        protected abstract SendLeadResponse SendLeadAfterZipCodeCheck(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.account supplier, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc, string pingId, string zipcode);

        protected bool CheckZipCodeOk(string zipcode, Guid accExpId)
        {
            DataAccessLayer.AccountExpertiseZipCode dal = new NoProblem.Core.DataAccessLayer.AccountExpertiseZipCode(null);
            DataAccessLayer.AccountExpertiseZipCode.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_accountexpertisezipcode>.SqlParametersList();
            parameters.Add("@accExpId", accExpId);
            parameters.Add("@zipCode", zipcode);
            object scalar = dal.ExecuteScalar(zipCodeQuery, parameters);
            bool retVal = (scalar != null);
            return retVal; ;
        }

        private const string zipCodeQuery =
            @"select top 1 1 from new_accountexpertisezipcode with(nolock)
                where deletionstatecode = 0 and new_accountexpertiseid = @accExpId and new_name = @zipCode";
    }
}
