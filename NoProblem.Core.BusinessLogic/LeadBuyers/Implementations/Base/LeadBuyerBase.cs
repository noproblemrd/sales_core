﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    public abstract class LeadBuyerBase : ILeadBuyer
    {
        protected const string FNAME_LNAME_ARE_MANDATORY = "FirstName and LastName are mandatory.";
        protected const string NAME_IS_MANDATORY = "Name is mandatory.";
        protected const string EMAIL_IS_MANDATORY = "Email is mandatory.";
        protected const string REGION_ERROR = "Region is not zip code level.";
        protected const string OUTSIDE_OF_COVERAGE_AREA = "Outside of coverage area.";
        protected const string ADDRESS_IS_MANDATORY = "Street address is mandatory.";
        private const string LEAD_ID_TOKEN_MANDATORY = "LeadId Token is mandatory.";
        private const string BIRTH_DATE_MANDATORY = "Birth date is mandatory.";
        private const string IP_MANDATORY = "IP is mandatory.";
        protected string validationMessage = String.Empty;
        private bool validationOk = true;

        #region ILeadBuyer Members

        public abstract SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId);

        public virtual GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            retVal.Price = ia.new_potentialincidentprice.Value;
            retVal.IsPredefinedPrice = true;
            return retVal;
        }

        #endregion

        protected virtual void ExecuteValidationSet(DataModel.Xrm.incident incident) { }

        protected bool IsTest()
        {
            return ConfigurationManager.AppSettings.Get("SiteId") != DataModel.Constants.SiteIds.SALES;
        }

        /// <summary>
        /// Returns the city and the state (two letter abreviation) of the zipcode.
        /// </summary>
        /// <param name="region"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        protected void ExtractZipcodeInfo(DataModel.Xrm.new_region region, out string city, out string state)
        {
            string[] regionNameSplit = region.new_englishname.Split(';');
            city = regionNameSplit[1];
            state = GetStateCached(region);
        }

        protected void ExtractZipcodeInfo(string zipcode, out string city, out string state)
        {
            DataAccessLayer.Region regionDal = new DataAccessLayer.Region(null);
            var region = regionDal.GetRegionByName(zipcode);
            ExtractZipcodeInfo(region, out city, out state);
        }

        private string GetStateCached(DataModel.Xrm.new_region region)
        {
            return DataAccessLayer.CacheManager.Instance.Get<string>("LeadBuyerBase.GetState", region.new_regionid.ToString(), GetState, DataAccessLayer.CacheManager.ONE_YEAR);
        }

        private string GetState(string regionId)
        {
            string state;
            DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
            var region = regDal.GetRegionMinDataById(new Guid(regionId));
            var parents = regDal.GetParents(region);
            state = parents.First(x => x.Level == 1).Name;
            return state;
        }

        protected bool Validate(DataModel.Xrm.incident incident)
        {
            ExecuteValidationSet(incident);
            validationMessage = validationMessage.Trim();
            return validationOk;
        }

        protected void ValidateRegion(DataModel.Xrm.incident incident)
        {
            var region = incident.new_new_region_incident;
            if (region.new_level != 4)
            {
                SetValidationFailureMessage(REGION_ERROR);
            }
        }

        protected void ValidateAddress(DataModel.Xrm.incident incident)
        {
            var contact = incident.incident_customer_contacts;
            if (String.IsNullOrEmpty(contact.address1_line1))
            {
                SetValidationFailureMessage(ADDRESS_IS_MANDATORY);
            }
        }

        protected void ValidateEmail(DataModel.Xrm.incident incident)
        {
            var contact = incident.incident_customer_contacts;
            if (String.IsNullOrEmpty(contact.emailaddress1))
            {
                SetValidationFailureMessage(EMAIL_IS_MANDATORY);
            }
        }

        protected void ValidateFirstAndLastName(DataModel.Xrm.incident incident)
        {
            var contact = incident.incident_customer_contacts;
            if (String.IsNullOrEmpty(contact.firstname) || String.IsNullOrEmpty(contact.lastname))
            {
                SetValidationFailureMessage(FNAME_LNAME_ARE_MANDATORY);
            }
        }

        protected void ValidateName(DataModel.Xrm.incident incident)
        {
            var contact = incident.incident_customer_contacts;
            if (String.IsNullOrEmpty(contact.fullname))
            {
                SetValidationFailureMessage(NAME_IS_MANDATORY);
            }
        }

        protected void ValidateLeadIdToken(DataModel.Xrm.incident incident)
        {
            if (String.IsNullOrEmpty(incident.new_leadidtoken))
            {
                SetValidationFailureMessage(LEAD_ID_TOKEN_MANDATORY);
            }
        }

        protected void ValidateBirthDate(DataModel.Xrm.incident incident)
        {
            if (!incident.incident_customer_contacts.birthdate.HasValue)
            {
                SetValidationFailureMessage(BIRTH_DATE_MANDATORY);
            }
        }

        protected void ValidateIp(DataModel.Xrm.incident incident)
        {
            if (String.IsNullOrEmpty(incident.new_ip))
            {
                SetValidationFailureMessage(IP_MANDATORY);
            }
        }

        protected void SetValidationFailureMessage(string error)
        {
            validationOk = false;
            validationMessage += " " + error;
        }
    }
}
