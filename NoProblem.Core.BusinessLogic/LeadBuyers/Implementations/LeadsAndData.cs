﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class LeadsAndData : ILeadBuyer, IPingPostApiBuyer
    {
        private const string PING_URL = "http://leads.datastreamgrp.com/Leads/LeadPing.aspx";
        private const string POST_URL = "http://leads.datastreamgrp.com/Leads/LeadPost.aspx";

        public SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            throw new NotImplementedException();
        }

        public GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            throw new NotImplementedException();
        }

        public PingResult Ping(PingData data)
        {
            throw new NotImplementedException();
        }

        public PostResult Post(PostData data)
        {
            throw new NotImplementedException();
        }
    }
}
