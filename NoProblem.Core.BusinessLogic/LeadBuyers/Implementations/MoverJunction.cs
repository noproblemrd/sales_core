﻿using System;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class MoverJunction
    {
        private const string URL = "http://www.moverjunction.com/mjadmin/httpinpostmovinglead.asp";
        private const string REF_VALUE = "nppc";
        private const string regionError = "Region is not zip code level.";
        private const string DATE_FORMAT = "MM/dd/yyyy";
        private const string MY_IP = "74.54.87.132";
        private const string TEST = "TEST";

        public SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId, bool isLongDistanceAccount)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                var email = contact.emailaddress1;
                if (string.IsNullOrEmpty(firstName))
                {
                    retVal.Reason = "First name is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                if (string.IsNullOrEmpty(lastName))
                {
                    retVal.Reason = "Last name is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                if (string.IsNullOrEmpty(email))
                {
                    retVal.Reason = "Email is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                var moveToRegion = regDal.GetRegionByName(incident.new_movetozipcode);
                if (moveToRegion == null)
                {
                    retVal.Reason = "Moving to zip code is invalid";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string[] moveToSplit = moveToRegion.new_englishname.Split(';');
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = regionError;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string zipcode = region.new_name;
                string[] regionNameSplit = region.new_englishname.Split(';');
                string stateFrom = regionNameSplit[3];
                bool isLongDistanceLead = (stateFrom != moveToSplit[3]);
                if (isLongDistanceAccount)
                {
                    if (!isLongDistanceLead)
                    {
                        retVal.Reason = "Long distance only for this account.";
                        retVal.FailedInnerValidation = true;
                        return retVal;
                    }
                }
                else
                {
                    if (isLongDistanceLead)
                    {
                        retVal.Reason = "Local distance only for this account.";
                        retVal.FailedInnerValidation = true;
                        return retVal;
                    }
                }

                if (!incident.new_movedate.HasValue)
                {
                    retVal.Reason = "Move date is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string moveDate = incident.new_movedate.Value.ToString(DATE_FORMAT);
                string moveSize;
                switch (incident.new_movesize)
                {
                    case "studio":
                        moveSize = "2000";
                        break;
                    case "oneBdr":
                        moveSize = "2940";
                        break;
                    case "twoBdr":
                        moveSize = "4550";
                        break;
                    case "threeBdr":
                        moveSize = "7700";
                        break;
                    case "fourBdr":
                        moveSize = "9800";
                        break;
                    case "fiveBdr":
                        moveSize = "11200";
                        break;
                    case "larger":
                        moveSize = "12000";
                        break;
                    default:
                        retVal.Reason = "Move size is mandatory";
                        retVal.FailedInnerValidation = true;
                        return retVal;
                }
                string ip = incident.new_ip;
                if (string.IsNullOrEmpty(ip))
                {
                    ip = MY_IP;
                }

                HttpClient client = new HttpClient();
                client.ParamType = HttpClient.ParamTypeEnum.ParamsList;
                client.HttpMethod = HttpClient.HttpMethodEnum.Post;
                client.ParamItems.Add(ParameterNames.RFQID, (int.Parse(incident.ticketnumber) * 11).ToString());
                client.ParamItems.Add(ParameterNames.FROMZIPCODE, zipcode);
                client.ParamItems.Add(ParameterNames.TOZIPCODE, incident.new_movetozipcode);
                client.ParamItems.Add(ParameterNames.WEIGHT, moveSize);
                client.ParamItems.Add(ParameterNames.FIRSTNAME, firstName);
                client.ParamItems.Add(ParameterNames.LASTNAME, lastName);
                client.ParamItems.Add(ParameterNames.EMAILADDRESS, email);
                client.ParamItems.Add(ParameterNames.CELLPHONE, incident.new_telephone1);
                client.ParamItems.Add(ParameterNames.MOVEDATE, moveDate);
                client.ParamItems.Add(ParameterNames.REF, REF_VALUE);
                client.ParamItems.Add(ParameterNames.IPADDRESS, ip);
                client.ParamItems.Add(ParameterNames.LEADCOST, accExp.new_incidentprice.Value.ToString());

                if (!GlobalConfigurations.IsProductionUsa())
                {
                    client.ParamItems.Add(ParameterNames.FIRSTNAME, TEST);
                    client.ParamItems.Add(ParameterNames.LASTNAME, TEST);
                }

                client.Url = URL;
                var ans = client.Post();
                if (ans.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string content = ans.Content;
                    if (content.StartsWith("Lead ID:"))
                    {
                        retVal.IsSold = true;
                        string leadId = content.Substring(content.IndexOf(':') + 1).Trim();
                        retVal.BrokerId = leadId;
                    }
                    else
                    {
                        retVal.MarkAsRejected = true;
                        retVal.Reason = string.Format("MoverJunction didn't return a successful response. Content = {0}", ans.Content);
                        LogUtils.MyHandle.WriteToLog("MoverJunction didn't return a successful response. Content = {0}", ans.Content);
                    }
                }
                else
                {
                    retVal.Reason = string.Format("Bad HttpStatus received from MoverJunction. status = {0}, content = {1}", ans.StatusCode, ans.Content);
                    LogUtils.MyHandle.WriteToLog("Bad HttpStatus received from MoverJunction. status = {0}, content = {1}", ans.StatusCode, ans.Content);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in MoverJunction.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private class ParameterNames
        {
            internal const string RFQID = "RFQID";
            internal const string FROMZIPCODE = "FROMZIPCODE";
            internal const string TOZIPCODE = "TOZIPCODE";
            internal const string WEIGHT = "WEIGHT";
            internal const string FIRSTNAME = "FIRSTNAME";
            internal const string LASTNAME = "LASTNAME";
            internal const string EMAILADDRESS = "EMAILADDRESS";
            internal const string CELLPHONE = "CELLPHONE";
            internal const string MOVEDATE = "MOVEDATE";
            internal const string REF = "REF";
            internal const string IPADDRESS = "IPADDRESS";
            internal const string LEADCOST = "LEADCOST";
        }
    }
}
