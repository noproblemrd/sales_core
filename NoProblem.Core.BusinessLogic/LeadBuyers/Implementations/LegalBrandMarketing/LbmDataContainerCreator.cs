﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    internal class LbmDataContainerCreator
    {
        private string affId;
        private string campId;
        private string domain;
        private string typeId;
        LbmAdditionalInfoFactory additionalInfoFactory;

        public LbmDataContainerCreator(LbmAdditionalInfoFactory additionalInfoFactory, string affId, string campId, string domain, string typeId)
        {
            this.additionalInfoFactory = additionalInfoFactory;
            this.affId = affId;
            this.campId = campId;
            this.domain = domain;
            this.typeId = typeId;
        }

        public LbmDataContainer CreateContainer(NoProblem.Core.DataModel.Xrm.incident incident, string categoryIdInLbm, string categoryNameInLbm, string zipcode, string firstName, string lastName, string email)
        {
            LbmDataContainer data = new LbmDataContainer();
            data.PapInfo.affid = affId;
            data.PapInfo.campid = campId;
            data.PapInfo.domainname = domain;
            data.PapInfo.typeid = typeId;
            data.LeadInfo.categoryId = categoryIdInLbm;
            data.LeadInfo.categoryName = categoryNameInLbm;
            data.LeadInfo.firstName = firstName;
            data.LeadInfo.lastName = lastName;
            data.LeadInfo.leadId = incident.ticketnumber;
            data.LeadInfo.cellPhone = incident.new_telephone1;
            data.LeadInfo.homePhone = string.Empty;
            data.LeadInfo.workPhone = string.Empty;
            data.LeadInfo.zip = zipcode;
            data.LeadInfo.emailAddress = email;
            LbmAdditionalInfo additionalInfo = additionalInfoFactory.CreateAdditionalInfo(incident);
            if (additionalInfo == null)
            {
                data = null;
            }
            else
            {
                data.LeadInfo.AdditionalInfo = additionalInfo;
            }
            return data;
        }
    }
}
