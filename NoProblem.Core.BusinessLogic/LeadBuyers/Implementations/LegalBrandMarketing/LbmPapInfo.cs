﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    //This is public for the XmlSerializer
    public class LbmPapInfo
    {
        public string affid { get; set; }
        public string campid { get; set; }
        public string typeid { get; set; }
        public string domainname { get; set; }
    }
}
