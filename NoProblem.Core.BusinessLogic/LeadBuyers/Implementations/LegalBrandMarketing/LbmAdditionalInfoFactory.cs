﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    internal abstract class LbmAdditionalInfoFactory : XrmUserBase
    {
        public LbmAdditionalInfoFactory()
            : base(null)
        {

        }

        internal abstract LbmAdditionalInfo CreateAdditionalInfo(DataModel.Xrm.incident incident);
    }
}
