﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    //This is public for the XmlSerializer
    [System.Xml.Serialization.XmlRoot("XMLdata")]
    public class LbmDataContainer
    {
        [System.Xml.Serialization.XmlElement("leadinfo")]
        public LbmLeadInfo LeadInfo { get; set; }
        [System.Xml.Serialization.XmlElement("papinfo")]
        public LbmPapInfo PapInfo { get; set; }

        public LbmDataContainer()
        {
            LeadInfo = new LbmLeadInfo();
            PapInfo = new LbmPapInfo();
        }
    }
}
