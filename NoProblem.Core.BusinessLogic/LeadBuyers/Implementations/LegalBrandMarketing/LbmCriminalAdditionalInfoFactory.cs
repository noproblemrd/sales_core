﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    internal class LbmCriminalAdditionalInfoFactory : LbmAdditionalInfoFactory
    {
        private const string YES = "yes";
        private const string NO = "no"; 

        internal override LbmAdditionalInfo CreateAdditionalInfo(DataModel.Xrm.incident incident)
        {
            LbmCriminalAdditionalInfo additionalInfo = null;
            if (incident.new_criminallawattorneycasedataid.HasValue)
            {
                additionalInfo = new LbmCriminalAdditionalInfo();
                DataAccessLayer.CriminalLawAttorneyCaseData dal = new NoProblem.Core.DataAccessLayer.CriminalLawAttorneyCaseData(XrmDataContext);
                var data = dal.Retrieve(incident.new_criminallawattorneycasedataid.Value);
                if (data.new_canyouaffordanattorney.HasValue)
                {
                    additionalInfo.CanYouAffordAnAttorney = data.new_canyouaffordanattorney.Value ? YES : NO;
                }
                else
                {
                    additionalInfo.CanYouAffordAnAttorney = YES;
                }
                if (data.new_doyouhavecurrentchargesagainstyou.HasValue)
                {
                    additionalInfo.DoYouHaveCurrentCharges = data.new_doyouhavecurrentchargesagainstyou.Value ? YES : NO;
                }
                else
                {
                    additionalInfo.DoYouHaveCurrentCharges = YES;
                }
            }
            return additionalInfo;
        }
    }
}
