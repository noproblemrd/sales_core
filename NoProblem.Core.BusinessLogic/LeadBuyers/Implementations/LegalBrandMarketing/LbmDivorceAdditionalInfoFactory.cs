﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    internal class LbmDivorceAdditionalInfoFactory : LbmAdditionalInfoFactory
    {
        private class TermsAgreedValues
        {
            internal const string SOME = "We agree on some but not all";
            internal const string NONE = "We do not agree";
            internal const string ALL = "We agree on all terms";
        }

        private class HowSoonToFileValues
        {
            internal const string IMMEDIATELY = "Immediately";
            internal const string ONE_TO_SIX_MONTHS = "1 to 6 months";
            internal const string SIX_MONTHS_AND_LATER = "6 months and later";
            internal const string DONT_WANT_TO_FILE = "We do not want to file";
        }

        private class FinancingValues
        {
            internal const string CC_OR_PERSONAL_LOAN = "Credit card or personal loan";
            internal const string PERSONAL_SAVINGS = "Personal savings";
            internal const string FAMILY_SUPPORT = "Family support";
            internal const string DISCUSS_WITH_ATTORNEY = "I will discuss payment options with my attorney";
            internal const string CANNOT_AFFORD = "I cannot afford legal fees";
        }

        private class IncomeValues
        {
            internal const string LESS_THAN_10k = "Less than $10,000";
            internal const string TENK_TO_30K = "$10,000 to $30,000";
            internal const string THIRTYK_TO_60K = "$30,000 to $60,000";
            internal const string SIXTYK_TO_100K = "$60,000 to $100,000";
            internal const string MORE_THAN_100K = "More than $100,000";
        }

        private class PropertyValues
        {
            internal const string HOUSE = "House";
            internal const string VEHICLES = "Vehicle(s)";
            internal const string VACATION_PROPERTY = "Vacation property";
            internal const string JEWELRY = "Jewelry";
            internal const string PENSION = "Pension";
            internal const string STOCKS = "Stocks";
            internal const string IRA = "IRA";
            internal const string OTHER = "Other:";
        }

        private class ChildrenValues
        {
            internal const string ZERO = "0";
            internal const string ONE = "1";
            internal const string TWO = "2";
            internal const string THREE = "3";
            internal const string FOUR = "4";
            internal const string FIVE = "5";
            internal const string MORE_THAN_FIVE = "More than 5";
        }

        private const string NO = "No";
        private const string YES = "Yes";

        internal override LbmAdditionalInfo CreateAdditionalInfo(NoProblem.Core.DataModel.Xrm.incident incident)
        {
            LbmDivorceAdditionalInfo additionalInfo = null;
            if (incident.new_divorceattorneycasedataid.HasValue)
            {
                additionalInfo = new LbmDivorceAdditionalInfo();
                DataAccessLayer.DivorceAttorneyCaseData dal = new NoProblem.Core.DataAccessLayer.DivorceAttorneyCaseData(XrmDataContext);
                var data = dal.Retrieve(incident.new_divorceattorneycasedataid.Value);
                AddTerms(additionalInfo, data);
                AddHowSoonToFile(additionalInfo, data);
                AddDoYouCurrentlyLiveWithSpouse(additionalInfo, data);
                AddHasDivorceBeenFiled(additionalInfo, data);
                AddAreYouRepresented(additionalInfo, data);
                AddFinancing(additionalInfo, data);
                AddYourIncome(additionalInfo, data);
                AddSpouseIncome(additionalInfo, data);
                AddOwnedProperty(additionalInfo, data);
                AddChildren(additionalInfo, data);
            }
            return additionalInfo;
        }

        private void AddChildren(LbmDivorceAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata data)
        {
            string childrenStr;
            if (data.new_howmanychildren.HasValue)
            {
                switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren)data.new_howmanychildren.Value)
                {
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.Zero:
                        childrenStr = ChildrenValues.ZERO;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.One:
                        childrenStr = ChildrenValues.ONE;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.Three:
                        childrenStr = ChildrenValues.THREE;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.Four:
                        childrenStr = ChildrenValues.FOUR;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.Five:
                        childrenStr = ChildrenValues.FIVE;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceChildren.MoreThan5:
                        childrenStr = ChildrenValues.MORE_THAN_FIVE;
                        break;
                    default:
                        childrenStr = ChildrenValues.TWO;
                        break;
                }
            }
            else
            {
                childrenStr = ChildrenValues.TWO;
            }
            additionalInfo.HowManyChildrenDoYouHaveWithYourSpouse = childrenStr;
        }

        private void AddOwnedProperty(LbmDivorceAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata data)
        {
            string propertyStr;
            if (data.new_ownedproperty.HasValue)
            {
                switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty)data.new_ownedproperty.Value)
                {
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.HouseOrCondo:
                        propertyStr = PropertyValues.HOUSE;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.IRA:
                        propertyStr = PropertyValues.IRA;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.Jewelry:
                        propertyStr = PropertyValues.JEWELRY;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.Pension:
                        propertyStr = PropertyValues.PENSION;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.StocksOrBonds:
                        propertyStr = PropertyValues.STOCKS;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.VacationProperty:
                        propertyStr = PropertyValues.VACATION_PROPERTY;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceProperty.Vehicles:
                        propertyStr = PropertyValues.VEHICLES;
                        break;
                    default:
                        propertyStr = PropertyValues.OTHER;
                        break;
                }
            }
            else
            {
                propertyStr = PropertyValues.OTHER;
            }
            additionalInfo.DoYouAndYourSpouseOwnAnyProperty = propertyStr;
        }

        private void AddSpouseIncome(LbmDivorceAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata data)
        {
            string incomeStr = GetIncomeString(data.new_spouseannualincome);
            additionalInfo.WhatIsYourSpousesApproximateAnnualIncome = incomeStr;
        }

        private void AddYourIncome(LbmDivorceAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata data)
        {
            string incomeStr = GetIncomeString(data.new_yourannualincome);
            additionalInfo.WhatIsYourApproximateAnnualIncome = incomeStr;
        }

        private string GetIncomeString(int? income)
        {
            string incomeStr;
            if (income.HasValue)
            {
                switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome)income.Value)
                {
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.MoreThan100K:
                        incomeStr = IncomeValues.MORE_THAN_100K;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.SixtyKTo100K:
                        incomeStr = IncomeValues.SIXTYK_TO_100K;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.TenKTo30K:
                        incomeStr = IncomeValues.TENK_TO_30K;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceIncome.ThirtyKTo60K:
                        incomeStr = IncomeValues.THIRTYK_TO_60K;
                        break;
                    default:
                        incomeStr = IncomeValues.LESS_THAN_10k;
                        break;
                }
            }
            else
            {
                incomeStr = IncomeValues.LESS_THAN_10k;
            }
            return incomeStr;
        }

        private void AddFinancing(LbmDivorceAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata data)
        {
            string financeStr;
            if (data.new_howareyoufinancing.HasValue)
            {
                switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing)data.new_howareyoufinancing.Value)
                {
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.CreditCardOrPersonalLoan:
                        financeStr = FinancingValues.CC_OR_PERSONAL_LOAN;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.FamilySupport:
                        financeStr = FinancingValues.FAMILY_SUPPORT;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.ICannotAffordLegalFees:
                        financeStr = FinancingValues.CANNOT_AFFORD;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.PersonalSavings:
                        financeStr = FinancingValues.PERSONAL_SAVINGS;
                        break;
                    default:
                        financeStr = FinancingValues.DISCUSS_WITH_ATTORNEY;
                        break;
                }
            }
            else
            {
                financeStr = FinancingValues.DISCUSS_WITH_ATTORNEY;
            }
            additionalInfo.HowDoYouPlanOnFinancingYourDivorceCase = financeStr;
        }

        private void AddAreYouRepresented(LbmDivorceAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata data)
        {
            if (data.new_areyoucurrentlyrepresented.HasValue)
            {
                additionalInfo.AreYouCurrentlyRepresented = data.new_areyoucurrentlyrepresented.Value ? YES : NO;
            }
            else
            {
                additionalInfo.AreYouCurrentlyRepresented = NO;
            }
        }

        private void AddHasDivorceBeenFiled(LbmDivorceAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata data)
        {
            if (data.new_hasadivorcecasebeenfiledincourt.HasValue)
            {
                additionalInfo.HasADivorceCaseBeenFiledInCourt = data.new_hasadivorcecasebeenfiledincourt.Value ? YES : NO;
            }
            else
            {
                additionalInfo.HasADivorceCaseBeenFiledInCourt = NO;
            }
        }

        private void AddDoYouCurrentlyLiveWithSpouse(LbmDivorceAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata data)
        {
            if (data.new_doyoucurrentlylivewithyourspouse.HasValue)
            {
                additionalInfo.DoYouCurrentlyLiveWithYourSpouse = data.new_doyoucurrentlylivewithyourspouse.Value ? YES : NO;
            }
            else
            {
                additionalInfo.DoYouCurrentlyLiveWithYourSpouse = NO;
            }
        }

        private void AddHowSoonToFile(LbmDivorceAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata data)
        {
            string howSoonStr;
            if (data.new_howsoonareyoulookingtofile.HasValue)
            {
                switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon)data.new_howsoonareyoulookingtofile.Value)
                {
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon.OneToSixMonths:
                        howSoonStr = HowSoonToFileValues.ONE_TO_SIX_MONTHS;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon.SixMonthsOrLater:
                        howSoonStr = HowSoonToFileValues.SIX_MONTHS_AND_LATER;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.DivorceHowSoon.WeDoNotWantToFile:
                        howSoonStr = HowSoonToFileValues.DONT_WANT_TO_FILE;
                        break;
                    default:
                        howSoonStr = HowSoonToFileValues.IMMEDIATELY;
                        break;
                }
            }
            else
            {
                howSoonStr = HowSoonToFileValues.IMMEDIATELY;
            }
            additionalInfo.HowSoonAreYouLookingToFile = howSoonStr;
        }

        private void AddTerms(LbmDivorceAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata data)
        {
            string terms;
            if (data.new_termsofdivorceagreedby.HasValue)
            {
                switch ((DataModel.Xrm.new_divorceattorneycasedata.TermsOfDivorce)data.new_termsofdivorceagreedby.Value)
                {
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.TermsOfDivorce.WeAgreeOnAllTerms:
                        terms = TermsAgreedValues.ALL;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_divorceattorneycasedata.TermsOfDivorce.WeDoNotAgree:
                        terms = TermsAgreedValues.NONE;
                        break;
                    default:
                        terms = TermsAgreedValues.SOME;
                        break;
                }
            }
            else
            {
                terms = TermsAgreedValues.SOME;
            }
            additionalInfo.TermsOfDivorceAgreedBy = terms;
        }
    }
}
