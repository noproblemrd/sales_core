﻿using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System;
using System.Xml.Linq;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class LegalBrandMarketing : LeadBuyerBase
    {

        //private const string baseUrl = "http://aws.1800duilaws.com/exec/postleadzip-s.asp";
        private const string source = "http://noproblemppc.com";
        //private const string duiCategoryName = "DUI-DWI";
        private const string anonymous = "anonymous";
        private const string regionError = "Region is not zip code level.";
        private const string affId = "e06528af";
        private const string campId = "44148542";
        private const string typeId = "90037a92";
        private static string[][] dic = new string[5][] { 
            new string[] { "DUI-DWI", "http://aws.1800duilaws.com/exec/postleadzip-s.asp" }, 
            new string[] { "Personal-Injury", "http://aws.1800duilaws.com/exec/postleadzip-pi.asp" },
            new string[] { "Bankruptcy", "http://aws.1800duilaws.com/exec/postleadzip-bnk.asp" },
            new string[] { "Divorce", "http://aws.1800duilaws.com/exec/postleadzip-div.asp" },
            new string[] { "Criminal", "http://aws.1800duilaws.com/exec/postleadzip-cri.asp" }
        };

        #region ILeadBuyer Members

        public override SendLeadResponse SendLead(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.account supplier, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {

                string[] categoryInfo = null;
                for (int i = 0; i < dic.Length; i++)
                {
                    if (dic[i][0] == accExp.new_externalcode)
                    {
                        categoryInfo = dic[i];
                        break;
                    }
                }
                if (categoryInfo == null)
                {
                    retVal.Reason = "Partner is not configured correctly";
                    LogUtils.MyHandle.WriteToLog("Parter LegalBrandMarketing is not configured correctly. IncidentId = {0}", incident.incidentid);
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = regionError;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                string[] regionNameSplit = region.new_englishname.Split(';');
                string zipCode = regionNameSplit[0];
                string county = regionNameSplit[2];

                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                var parents = regDal.GetParents(region);
                string state = parents.First(x => x.new_level == 1).new_name;

                var contact = incident.incident_customer_contacts;
                string firstName, lastName;
                var fullName = contact.fullname;
                if (string.IsNullOrEmpty(fullName))
                {
                    firstName = anonymous;
                    lastName = string.Empty;
                }
                else
                {
                    firstName = contact.firstname;
                    lastName = contact.lastname;
                }

                data data = new data();
                data.leadinfo.affid = affId;
                data.leadinfo.campid = campId;
                data.leadinfo.categoryId = 1;
                data.leadinfo.categoryName = accExp.new_externalcode;
                data.leadinfo.county = county;
                data.leadinfo.firstName = firstName;
                data.leadinfo.lastName = lastName;
                data.leadinfo.leadId = incident.ticketnumber;
                data.leadinfo.numericPhone = incident.new_telephone1;
                data.leadinfo.source = source;
                data.leadinfo.state = state;
                data.leadinfo.typeid = typeId;
                data.leadinfo.zip = zipCode;

                string utf8EncodedXml;
                var serializer = new XmlSerializer(typeof(data));
                using (var memoryStream = new MemoryStream())
                {
                    using (var streamWriter = new StreamWriter(memoryStream, System.Text.Encoding.UTF8))
                    {
                        serializer.Serialize(streamWriter, data);
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        using (var streamReader = new StreamReader(memoryStream, System.Text.Encoding.UTF8))
                        {
                            utf8EncodedXml = streamReader.ReadToEnd();
                        }
                    }
                }

                Utils.HttpClient client = new NoProblem.Core.BusinessLogic.Utils.HttpClient(categoryInfo[1], NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpMethodEnum.Post);
                client.ParamType = NoProblem.Core.BusinessLogic.Utils.HttpClient.ParamTypeEnum.Content;
                client.ContentData = utf8EncodedXml;
                var response = client.Post();

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    try
                    {
                        XElement root = XElement.Parse(response.Content);
                        string statusValue = root.Element("status").Value;
                        if (statusValue == "received")
                        {
                            retVal.IsSold = true;
                            retVal.BrokerId = "received";
                        }
                        else
                        {
                            retVal.MarkAsRejected = true;
                            retVal.Reason = string.Format("LegalBrandMarkenting didn't return 'received'. Content = {0}", response.Content);
                        }
                    }
                    catch (Exception)
                    {
                        retVal.Reason = string.Format("Failed to parse XML response. Content = {0}", response.Content);
                    }
                }
                else
                {
                    retVal.Reason = string.Format("Bad HTTP status received. StatusCode = {0}, Content = {1}", response.StatusCode, response.Content);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in LegalBrandMarketing.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        #endregion
    }

    //this is public and outside of the bigger class for xmlserializer
    public class data
    {
        public Leadinfo leadinfo { get; set; }

        public data()
        {
            leadinfo = new Leadinfo();
        }
    }

    //this is public and outside of the bigger class for xmlserializer
    public class Leadinfo
    {
        public string affid { get; set; }
        public string campid { get; set; }
        public string typeid { get; set; }
        public string leadId { get; set; }
        public string source { get; set; }
        public string categoryName { get; set; }
        public int categoryId { get; set; }
        public string zip { get; set; }
        public string state { get; set; }
        public string county { get; set; }
        public string numericPhone { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
    }
}
