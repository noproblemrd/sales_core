﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    //This is public for the XmlSerializer
    public class LbmDivorceAdditionalInfo : LbmAdditionalInfo
    {
        [System.Xml.Serialization.XmlElement("Terms_of_divorce_agreed_by")]
        public string TermsOfDivorceAgreedBy { get; set; }
        [System.Xml.Serialization.XmlElement("How_soon_are_you_looking_to_file")]
        public string HowSoonAreYouLookingToFile { get; set; }
        [System.Xml.Serialization.XmlElement("Do_you_currently_live_with_your_spouse")]
        public string DoYouCurrentlyLiveWithYourSpouse { get; set; }
        [System.Xml.Serialization.XmlElement("Has_a_divorce_case_been_filed_in_court")]
        public string HasADivorceCaseBeenFiledInCourt { get; set; }
        [System.Xml.Serialization.XmlElement("Are_you_currently_represented")]
        public string AreYouCurrentlyRepresented { get; set; }
        [System.Xml.Serialization.XmlElement("How_do_you_plan_on_financing_your_divorce_case")]
        public string HowDoYouPlanOnFinancingYourDivorceCase { get; set; }
        [System.Xml.Serialization.XmlElement("What_is_you_approximate_annual_income")]
        public string WhatIsYourApproximateAnnualIncome { get; set; }
        [System.Xml.Serialization.XmlElement("What_is_your_spouses_approximate_annual_income")]
        public string WhatIsYourSpousesApproximateAnnualIncome { get; set; }
        [System.Xml.Serialization.XmlElement("Do_you_and_your_spouse_own_any_property")]
        public string DoYouAndYourSpouseOwnAnyProperty { get; set; }
        [System.Xml.Serialization.XmlElement("How_many_children_do_you_have_with_your_spouse")]
        public string HowManyChildrenDoYouHaveWithYourSpouse { get; set; }
    }
}
