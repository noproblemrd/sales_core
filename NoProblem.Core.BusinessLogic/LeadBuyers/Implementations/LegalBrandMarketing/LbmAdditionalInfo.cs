﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    //This is public for the XmlSerializer
    [System.Xml.Serialization.XmlInclude(typeof(LbmCriminalAdditionalInfo))]
    [System.Xml.Serialization.XmlInclude(typeof(LbmDuiAdditionalInfo))]
    [System.Xml.Serialization.XmlInclude(typeof(LbmDivorceAdditionalInfo))]
    [System.Xml.Serialization.XmlInclude(typeof(LbmBankruptcyAdditionalInfo))]
    public abstract class LbmAdditionalInfo
    {
    }
}
