﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    //This is public for the XmlSerializer
    public class LbmLeadInfo
    {        
        public string leadId { get; set; }
        public string categoryName { get; set; }
        public string categoryId { get; set; }
        public string zip { get; set; }
        public string homePhone { get; set; }
        public string workPhone { get; set; }
        public string cellPhone { get; set; }
        public string emailAddress { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public LbmAdditionalInfo AdditionalInfo { get; set; }
    }
}
