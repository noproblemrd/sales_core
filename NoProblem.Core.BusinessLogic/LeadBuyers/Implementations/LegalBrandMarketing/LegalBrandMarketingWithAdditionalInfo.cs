﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.LeadBuyers;
using System.IO;
using System.Xml.Linq;
using NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class LegalBrandMarketingWithAdditionalInfo : LeadBuyerBase
    {
        private const string anonymous = "anonymous";
        private const string affId = "e06528af";
        private const string campId = "44148542";
        private const string typeId = "90037a92";
        private const string domain = "www.noproblemppc.com";
        private const string testZipcode = "96910";

        private const string CRIMINAL = "Criminal";
        private const string BANKRUPTCY = "Bankruptcy";
        private const string DIVORCE = "Divorce";

        private const int CATEGORY_NAME_INDEX = 0;
        private const int URL_INDEX = 1;
        private const int CATEGORY_ID_INDEX = 2;

        private static string[][] dic = new string[3][] { 
            //new string[] { "DUI-DWI", "http://aws.1800duilaws.com/exec/postleadzip-s.asp" }, 
            //new string[] { "Personal-Injury", "http://aws.1800duilaws.com/exec/postleadzip-pi.asp" },
            new string[] { BANKRUPTCY, "http://aws.1800duilaws.com/exec/script/xml-processor-post.asp", "17" },
            new string[] { DIVORCE, "http://aws.1800duilaws.com/exec/script/xml-processor-post.asp", "18" },
            new string[] { CRIMINAL, "http://aws.1800duilaws.com/exec/script/xml-processor-post.asp", "19" }
        };

        public override SendLeadResponse SendLead(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.account supplier, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                string[] categoryInfo = ResolveCategoryInfo(accExp);
                if (categoryInfo == null)
                {
                    retVal.Reason = "Partner is not configured correctly";
                    LogUtils.MyHandle.WriteToLog("Parter LegalBrandMarketing is not configured correctly. IncidentId = {0}", incident.incidentid);
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = "Region is not zipcode level.";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string zipcode = region.new_name;

                var contact = incident.incident_customer_contacts;
                string firstName, lastName;
                firstName = string.IsNullOrEmpty(contact.firstname) ? anonymous : contact.firstname;
                lastName = string.IsNullOrEmpty(contact.lastname) ? anonymous : contact.lastname;

                var email = contact.emailaddress1;
                if (string.IsNullOrEmpty(email))
                {
                    retVal.Reason = "Email is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                bool isTest = System.Configuration.ConfigurationManager.AppSettings["SiteId"] != "Sales";
                if (isTest)
                {
                    zipcode = testZipcode;
                }

                LbmDataContainer data = CreateDataContainer(incident, categoryInfo, zipcode, firstName, lastName, email);
                if (data == null)
                {
                    retVal.Reason = "Addtional category specific data could not be found.";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                string utf8EncodedXml = SerializeDataToXml(data);

                Utils.HttpClient client = new NoProblem.Core.BusinessLogic.Utils.HttpClient(categoryInfo[URL_INDEX], NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpMethodEnum.Post);
                client.ParamType = NoProblem.Core.BusinessLogic.Utils.HttpClient.ParamTypeEnum.Content;
                client.ContentData = utf8EncodedXml;
                var response = client.Post();
                

                ParseResponse(retVal, response);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in LegalBrandMarketingWithAdditionalInfo.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private string[] ResolveCategoryInfo(NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp)
        {
            string[] categoryInfo = null;
            for (int i = 0; i < dic.Length; i++)
            {
                if (dic[i][CATEGORY_NAME_INDEX] == accExp.new_externalcode)
                {
                    categoryInfo = dic[i];
                    break;
                }
            }
            return categoryInfo;
        }

        private LbmDataContainer CreateDataContainer(NoProblem.Core.DataModel.Xrm.incident incident, string[] categoryInfo, string zipcode, string firstName, string lastName, string email)
        {
            LbmDataContainer container = null;
            string categoryName = categoryInfo[CATEGORY_NAME_INDEX];
            LbmAdditionalInfoFactory additionalInfoFactory = null;
            switch (categoryName)
            {
                case CRIMINAL:
                    additionalInfoFactory = new LbmCriminalAdditionalInfoFactory();
                    break;
                case DIVORCE:
                    additionalInfoFactory = new LbmDivorceAdditionalInfoFactory();
                    break;
                case BANKRUPTCY:
                    additionalInfoFactory = new LbmBankruptcyAdditionalInfoFactory();
                    break;
            }
            LbmDataContainerCreator creator = new LbmDataContainerCreator(additionalInfoFactory, affId, campId, domain, typeId);
            container = creator.CreateContainer(incident, categoryInfo[CATEGORY_ID_INDEX], categoryName, zipcode, firstName, lastName, email);
            return container;
        }

        private string SerializeDataToXml(LbmDataContainer data)
        {
            string utf8EncodedXml;
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(LbmDataContainer));
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream, System.Text.Encoding.UTF8))
                {
                    serializer.Serialize(streamWriter, data);
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    using (var streamReader = new StreamReader(memoryStream, System.Text.Encoding.UTF8))
                    {
                        utf8EncodedXml = streamReader.ReadToEnd();
                    }
                }
            }
            return utf8EncodedXml;
        }

        private void ParseResponse(SendLeadResponse retVal, NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpResponse response)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                try
                {
                    XElement root = XElement.Parse(response.Content);
                    string statusValue = root.Element("status_code").Value;
                    string detailsValue = root.Element("details").Value;
                    if (statusValue == "200")
                    {
                        retVal.IsSold = true;
                        retVal.BrokerId = detailsValue;
                    }
                    else
                    {
                        retVal.MarkAsRejected = true;
                        retVal.Reason = string.Format("LegalBrandMarkenting didn't return 'received'. status_code = {0}. details = {1}", statusValue, detailsValue);
                    }
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to parse XML response. Content = {0}", response.Content);
                    retVal.Reason = string.Format("Failed to parse XML response. Content = {0}", response.Content);
                }
            }
            else
            {
                retVal.Reason = string.Format("Bad HTTP status received. StatusCode = {0}, Content = {1}", response.StatusCode, response.Content);
            }
        }
    }
}
