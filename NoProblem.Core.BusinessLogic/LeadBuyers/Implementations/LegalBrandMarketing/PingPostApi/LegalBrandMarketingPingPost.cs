﻿using Effect.Crm.Logs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing.PingPostApi
{
    internal class LegalBrandMarketingPingPost : LeadBuyerBase, IPingPostApiBuyer
    {
        private const string PING_URL = "http://aws.1800duilaws.com/exec/ping_coverage/getcoveragezip.asp";

        private static readonly Dictionary<string, CategoryData> categoryCodesToNames =
            new Dictionary<string, CategoryData>()
            {
                { "31", new CategoryData() { 
                    Name = "Estate Planning", 
                    PostUrl = "http://aws.1800duilaws.com/exec/estate_planning/processor-ep.asp",
                    TypeId = "5ea06864",
                    CampId = "5f79e884"
                }},
                { "32", new CategoryData() { 
                    Name = "Foreclosures", 
                    PostUrl = "http://aws.1800duilaws.com/exec/foreclosures/processor-for.asp",
                    TypeId = "9e5b4abe",
                    CampId = "c2491777"
                }},
                { "33", new CategoryData() { 
                    Name = "Auto Accident", 
                    PostUrl = "http://aws.1800duilaws.com/exec/auto_accident/processor-aa.asp",
                    TypeId = "b265d847",
                    CampId = "ed2af662"
                }},
                { "34", new CategoryData() { 
                    Name = "Workers Compensation", 
                    PostUrl = "http://aws.1800duilaws.com/exec/workers_compensation/processor-wc.asp",
                    TypeId = "816e88be",
                    CampId = "c92ed3e0"
                }}
            };

        private const string NO = "no";
        private const string REFID_AND_AFFID = "f1e4defa";

        private struct CategoryData
        {
            public string Name { get; set; }
            public string PostUrl { get; set; }
            public string TypeId { get; set; }
            public string CampId { get; set; }
        }

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = null;
            try
            {
                retVal = base.GetBrokerBid(incident, accExp, ia);
                var result = Ping(
                    new PingData()
                    {
                        ExternalCode = accExp.new_externalcode,
                        Zip = incident.new_new_region_incident.new_name,
                        DefaultPrice = retVal.Price
                    });

                if (result.Accepted)
                {
                    retVal.BrokerId = result.PingId;
                    retVal.Price = result.PingPrice;
                }
                else
                {
                    retVal.WantsToBid = false;
                    retVal.MarkAsRejected = false;
                    retVal.Reason = result.PingMessage;
                }
            }
            catch (Exception exc)
            {
                if (retVal == null)
                    retVal = new GetBrokerBidResponse();
                LogUtils.MyHandle.HandleException(exc, "Exception in LegalBrandMarketingPingPost.GetBrokerBid");
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var result = Post(
                    new PostData()
                    {
                        Zip = incident.new_new_region_incident.new_name,
                        FirstName = contact.firstname,
                        LastName = contact.lastname,
                        Phone = incident.new_telephone1,
                        Email = contact.emailaddress1,
                        Description = incident.description,
                        ExternalCode = accExp.new_externalcode,
                        Id = incident.incidentid.ToString()
                    });
                if (result.Accepted)
                {
                    retVal.IsSold = true;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = result.PostMessage;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in LegalBrandMarketingPingPost.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        public PingResult Ping(PingData data)
        {
            PingResult pingResult = new PingResult();
            RestSharp.IRestClient client = new RestSharp.RestClient(PING_URL);
            RestSharp.IRestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.RequestFormat = RestSharp.DataFormat.Xml;
            CoveragePingData payload = new CoveragePingData();
            payload.datainfo.categoryID = data.ExternalCode;
            payload.datainfo.zipcode = data.Zip;
            request.AddBody(payload);
            var response = client.Execute<LbmApiResponse>(request);
            if (response.Data != null)
            {
                if (response.Data.status_code == 200)
                {
                    pingResult.Accepted = true;
                }
                else
                {
                    pingResult.Accepted = false;
                    pingResult.PingMessage = response.Data.details;
                }
            }
            else
            {
                pingResult.Accepted = false;
                pingResult.PingMessage = "Failed to parse PING response. Content = " + response.Content;
            }
            return pingResult;
        }

        public PostResult Post(PostData data)
        {
            PostResult postResult = new PostResult();
            XMLData payload = new XMLData();
            payload.LeadInfo.category = categoryCodesToNames[data.ExternalCode].Name;
            payload.LeadInfo.categoryId = data.ExternalCode;
            string city, state;
            ExtractZipcodeInfo(data.Zip, out city, out state);
            payload.LeadInfo.county = city;
            payload.LeadInfo.emailAddress = data.Email;
            payload.LeadInfo.firstname = data.FirstName;
            payload.LeadInfo.lastname = data.LastName;
            payload.LeadInfo.leadId = data.Id;
            payload.LeadInfo.notes = data.Description;
            payload.LeadInfo.phone = data.Phone;
            payload.LeadInfo.represented = NO;
            payload.LeadInfo.state = state;
            payload.LeadInfo.zip = data.Zip;
            payload.PapInfo.affID = REFID_AND_AFFID;
            payload.PapInfo.campid = categoryCodesToNames[data.ExternalCode].CampId;
            payload.PapInfo.refID = REFID_AND_AFFID;
            payload.PapInfo.typeid = categoryCodesToNames[data.ExternalCode].TypeId;
            RestSharp.IRestClient client = new RestSharp.RestClient(categoryCodesToNames[data.ExternalCode].PostUrl);
            RestSharp.IRestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.RequestFormat = RestSharp.DataFormat.Xml;
            request.AddBody(payload);
            var response = client.Execute<LbmApiResponse>(request);
            if (response.Data != null)
            {
                if (response.Data.status_code == 200)
                {
                    postResult.Accepted = true;
                }
                else
                {
                    postResult.Accepted = false;
                    postResult.PostMessage = response.Data.details;
                }
            }
            else
            {
                postResult.Accepted = false;
                postResult.PostMessage = "Failed to parse POST response. Content = " + response.Content;
            }
            return postResult;
        }

        //private string SerializeToXml(object data)
        //{
        //    string utf8EncodedXml;
        //    var serializer = new XmlSerializer(typeof(data));
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        using (var streamWriter = new StreamWriter(memoryStream, System.Text.Encoding.UTF8))
        //        {
        //            serializer.Serialize(streamWriter, data);
        //            memoryStream.Seek(0, SeekOrigin.Begin);
        //            using (var streamReader = new StreamReader(memoryStream, System.Text.Encoding.UTF8))
        //            {
        //                utf8EncodedXml = streamReader.ReadToEnd();
        //            }
        //        }
        //    }
        //    return utf8EncodedXml;
        //}
    }

    public class LbmApiResponse
    {
        public int status_code { get; set; }
        public string details { get; set; }
    }

    [RestSharp.Serializers.SerializeAs(Name = "data")]
    [System.Xml.Serialization.XmlRoot("data")]
    public class CoveragePingData
    {
        public PingDataInfo datainfo { get; set; }

        public CoveragePingData()
        {
            datainfo = new PingDataInfo();
        }
    }

    public class PingDataInfo
    {
        public string categoryID { get; set; }
        public string zipcode { get; set; }
    }

    [RestSharp.Serializers.SerializeAs(Name = "XMLdata")]
    [System.Xml.Serialization.XmlRoot("XMLdata")]
    public class XMLData
    {
        [RestSharp.Serializers.SerializeAs(Name = "leadinfo")]
        [System.Xml.Serialization.XmlElement("leadinfo")]
        public LeadInfo LeadInfo { get; set; }

        [RestSharp.Serializers.SerializeAs(Name = "papinfo")]
        [System.Xml.Serialization.XmlElement("papinfo")]
        public PapInfo PapInfo { get; set; }

        public XMLData()
        {
            LeadInfo = new LeadInfo();
            PapInfo = new PapInfo();
        }
    }

    public class PapInfo
    {
        public string refID { get; set; }
        public string affID { get; set; }
        public string typeid { get; set; }
        public string campid { get; set; }
    }


    public class LeadInfo
    {
        public string leadId { get; set; }
        public string categoryId { get; set; }
        public string category { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string state { get; set; }
        public string county { get; set; }
        public string zip { get; set; }
        public string phone { get; set; }
        public string emailAddress { get; set; }
        public string represented { get; set; }
        public string notes { get; set; }
    }
}
