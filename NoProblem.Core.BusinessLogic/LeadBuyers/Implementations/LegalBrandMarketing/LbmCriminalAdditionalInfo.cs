﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    //This is public for the XmlSerializer
    public class LbmCriminalAdditionalInfo : LbmAdditionalInfo
    {
        [System.Xml.Serialization.XmlElement("Canyouaffordanattorney")]
        public string CanYouAffordAnAttorney { get; set; }
        [System.Xml.Serialization.XmlElement("currentchargesagainstyou")]
        public string DoYouHaveCurrentCharges { get; set; }
    }
}
        