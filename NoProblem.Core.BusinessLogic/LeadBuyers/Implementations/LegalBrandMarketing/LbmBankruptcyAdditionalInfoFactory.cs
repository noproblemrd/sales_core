﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    internal class LbmBankruptcyAdditionalInfoFactory : LbmAdditionalInfoFactory
    {
        private const string NO = "no";
        private const string YES = "yes";

        private class FilingReasonsValues
        {
            internal const string GARNISHMENT = "Garnishment";
            internal const string CREDITOR_HARASSMENT = "Creditor Harassment";
            internal const string REPOSSESSION = "Repossession";
            internal const string FORECLOSURE = "Foreclosure";
            internal const string LAWSUITS = "Lawsuits";
            internal const string ILLNESS = "Illness";
            internal const string DISABILITY = "Disability";
            internal const string LICENSE_SUSPENSION = "License Suspension";
            internal const string DIVORCE = "Divorce";
            internal const string LOSS_OF_INCOME = "Loss of Income";
            internal const string OTHER = "Other:";
        }

        private class BillsValues
        {
            internal const string CC = "Credit Cards";
            internal const string STORE_CARDS = "Store Cards";
            internal const string PERSONAL_LOANS = "Personal Loans";
            internal const string CHILD_SUPPORT = "Child Support";
            internal const string STUDENT_LOANS = "Student Loans";
            internal const string AUTO_LOANS_INCOME_TAXES = "Auto Loans Income Taxes";
            internal const string PAYDAY_LOANS = "Payday Loans";
            internal const string MEDICAL_BILLS = "Medical Bills";
            internal const string OTHER = "Other:";
        }

        private class MoneyValues
        {
            internal const string LESS_THAN_3K = "Less than $3,000";
            internal const string THREEK_TO_6K = "$3,000 to $6,000";
            internal const string SIXK_TO_10K = "$6,000 to $10,000";
            internal const string TENK_TO_15K = "$10,000 to $15,000";
            internal const string MORE_THAN_15K = "More than $15,000";
        }

        private class TypesOfAssetsValues
        {
            internal const string EMPLOYED_FULL_TIME = "Employed, Full-time";
            internal const string EMPLOYED_PART_TIME = "Employed, Part-time";
            internal const string SOCIAL_SECURITY = "Social Security";
            internal const string PENSION = "Pension";
            internal const string RETIREMENT_CHILD_SUPPORT = "Retirement Child Support";
            internal const string MAINTENANCE = "Maintenance";
            internal const string OTHER = "Other:";
            internal const string NO_INCOME = "No Income";
        }

        internal override LbmAdditionalInfo CreateAdditionalInfo(NoProblem.Core.DataModel.Xrm.incident incident)
        {
            LbmBankruptcyAdditionalInfo additionalInfo = null;
            if (incident.new_bankruptcyattorneycasedataid.HasValue)
            {
                DataAccessLayer.BankruptcyAttorneyCaseData dal = new NoProblem.Core.DataAccessLayer.BankruptcyAttorneyCaseData(XrmDataContext);
                var data = dal.Retrieve(incident.new_bankruptcyattorneycasedataid.Value);
                additionalInfo = new LbmBankruptcyAdditionalInfo();
                AddReason(additionalInfo, data);
                AddBills(additionalInfo, data);
                AddMonthlyExpenses(additionalInfo, data);
                AddMonthlyIncome(additionalInfo, data);
                AddRealEstate(additionalInfo, data);
                AddAutomobile(additionalInfo, data);
                AddAdditionalAssets(additionalInfo, data);
                AddTypesOfIncome(additionalInfo, data);
                AddAffordAttorney(additionalInfo, data);
            }
            return additionalInfo;
        }

        private void AddAffordAttorney(LbmBankruptcyAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata data)
        {
            if (data.new_canyouaffordanattorney.HasValue)
            {
                additionalInfo.AffordAttorney = data.new_canyouaffordanattorney.Value ? YES : NO;
            }
            else
            {
                additionalInfo.AffordAttorney = YES;
            }
        }

        private void AddTypesOfIncome(LbmBankruptcyAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata data)
        {
            string typesStr;
            if (data.new_typesofincome.HasValue)
            {
                switch ((DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy)data.new_typesofincome.Value)
                {
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.EmployedFullTime:
                        typesStr = TypesOfAssetsValues.EMPLOYED_FULL_TIME;
                        break;
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.EmployedPartTime:
                        typesStr = TypesOfAssetsValues.EMPLOYED_PART_TIME;
                        break;
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.Maintenance:
                        typesStr = TypesOfAssetsValues.MAINTENANCE;
                        break;
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.NoIncome:
                        typesStr = TypesOfAssetsValues.NO_INCOME;
                        break;
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.Pension:
                        typesStr = TypesOfAssetsValues.PENSION;
                        break;
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.RetirementChildSupport:
                        typesStr = TypesOfAssetsValues.RETIREMENT_CHILD_SUPPORT;
                        break;
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.TypesOfIncomeBankruptcy.SocialSecurity:
                        typesStr = TypesOfAssetsValues.SOCIAL_SECURITY;
                        break;
                    default:
                        typesStr = TypesOfAssetsValues.OTHER;
                        break;
                }
            }
            else
            {
                typesStr = TypesOfAssetsValues.OTHER;
            }
            additionalInfo.IncomeTypes = typesStr;
        }

        private void AddAdditionalAssets(LbmBankruptcyAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata data)
        {
            if (data.new_haveadditionalassets.HasValue)
            {
                additionalInfo.AdditionalAssets = data.new_haveadditionalassets.Value ? YES : NO;
            }
            else
            {
                additionalInfo.AdditionalAssets = NO;
            }
        }

        private void AddRealEstate(LbmBankruptcyAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata data)
        {
            if (data.new_behindinrealestatepayments.HasValue)
            {
                additionalInfo.OwnRealEstate = data.new_behindinrealestatepayments.Value ? YES : NO;
            }
            else
            {
                additionalInfo.OwnRealEstate = YES;
            }
        }

        private void AddAutomobile(LbmBankruptcyAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata data)
        {
            if (data.new_behindinautomobilepayments.HasValue)
            {
                additionalInfo.OwnAutomobile = data.new_behindinautomobilepayments.Value ? YES : NO;
            }
            else
            {
                additionalInfo.OwnAutomobile = YES;
            }
        }

        private void AddMonthlyExpenses(LbmBankruptcyAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata data)
        {
            string str = GetIncomeString(data.new_estimatetotalmonthlyexpenses);
            additionalInfo.TotalMonthlyExpenses = str;
        }

        private void AddMonthlyIncome(LbmBankruptcyAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata data)
        {
            string str = GetIncomeString(data.new_estimatetotalmonthlyincome);
            additionalInfo.TotalMonthlyIncome = str;
        }

        private string GetIncomeString(int? money)
        {
            string str;
            if (money.HasValue)
            {
                switch ((DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy)money.Value)
                {
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.MoreThan15K:
                        str = MoneyValues.MORE_THAN_15K;
                        break;
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.SixKTo10K:
                        str = MoneyValues.SIXK_TO_10K;
                        break;
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.TenKTo15K:
                        str = MoneyValues.TENK_TO_15K;
                        break;
                    case DataModel.Xrm.new_bankruptcyattorneycasedata.MoneyEstimateBankruptcy.ThreeKTo6K:
                        str = MoneyValues.THREEK_TO_6K;
                        break;
                    default:
                        str = MoneyValues.LESS_THAN_3K;
                        break;
                }
            }
            else
            {
                str = MoneyValues.LESS_THAN_3K;
            }
            return str;
        }

        private void AddBills(LbmBankruptcyAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata data)
        {
            string billsStr;
            if (data.new_whichbillsdoyouhave.HasValue)
            {
                switch ((DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy)data.new_whichbillsdoyouhave.Value)
                {
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.AutoLoansIncomeTaxes:
                        billsStr = BillsValues.AUTO_LOANS_INCOME_TAXES;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.ChildSupport:
                        billsStr = BillsValues.CHILD_SUPPORT;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.CreditCards:
                        billsStr = BillsValues.CC;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.MedicalBills:
                        billsStr = BillsValues.MEDICAL_BILLS;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.PaydayLoans:
                        billsStr = BillsValues.PAYDAY_LOANS;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.PersonalLoans:
                        billsStr = BillsValues.PERSONAL_LOANS;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.StoreCards:
                        billsStr = BillsValues.STORE_CARDS;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.StudentLoans:
                        billsStr = BillsValues.STUDENT_LOANS;
                        break;
                    default:
                        billsStr = BillsValues.OTHER;
                        break;
                }
            }
            else
            {
                billsStr = BillsValues.OTHER;
            }
            additionalInfo.Bills = billsStr;
        }

        private void AddReason(LbmBankruptcyAdditionalInfo additionalInfo, NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata data)
        {
            string reasonsStr;
            if (data.new_whyconsideringbankruptcy.HasValue)
            {
                switch ((DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy)data.new_whyconsideringbankruptcy.Value)
                {
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.CreditorHarassment:
                        reasonsStr = FilingReasonsValues.CREDITOR_HARASSMENT;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Disability:
                        reasonsStr = FilingReasonsValues.DISABILITY;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Divorce:
                        reasonsStr = FilingReasonsValues.DIVORCE;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Foreclosure:
                        reasonsStr = FilingReasonsValues.FORECLOSURE;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Garnishment:
                        reasonsStr = FilingReasonsValues.GARNISHMENT;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Illness:
                        reasonsStr = FilingReasonsValues.ILLNESS;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Lawsuits:
                        reasonsStr = FilingReasonsValues.LAWSUITS;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.LicenseSuspension:
                        reasonsStr = FilingReasonsValues.LICENSE_SUSPENSION;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.LossOfIncome:
                        reasonsStr = FilingReasonsValues.LOSS_OF_INCOME;
                        break;
                    case NoProblem.Core.DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Repossession:
                        reasonsStr = FilingReasonsValues.REPOSSESSION;
                        break;
                    default:
                        reasonsStr = FilingReasonsValues.OTHER;
                        break;
                }
            }
            else
            {
                reasonsStr = FilingReasonsValues.OTHER;
            }
            additionalInfo.FilingReasons = reasonsStr;
        }
    }
}
