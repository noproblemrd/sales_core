﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing
{
    //This is public for the XmlSerializer
    public class LbmBankruptcyAdditionalInfo : LbmAdditionalInfo
    {
        [System.Xml.Serialization.XmlElement("FilingReasons")]
        public string FilingReasons { get; set; }
        [System.Xml.Serialization.XmlElement("Bills")]
        public string Bills { get; set; }
        [System.Xml.Serialization.XmlElement("TotalMonthlyExpenses")]
        public string TotalMonthlyExpenses { get; set; }
        [System.Xml.Serialization.XmlElement("ownrealestate")]
        public string OwnRealEstate { get; set; }
        [System.Xml.Serialization.XmlElement("ownautomobile")]
        public string OwnAutomobile { get; set; }
        [System.Xml.Serialization.XmlElement("AdditionalAssets")]
        public string AdditionalAssets { get; set; }
        [System.Xml.Serialization.XmlElement("IncomeTypes")]
        public string IncomeTypes { get; set; }
        [System.Xml.Serialization.XmlElement("TotalMonthlyIncome")]
        public string TotalMonthlyIncome { get; set; }
        [System.Xml.Serialization.XmlElement("affordattorney")]
        public string AffordAttorney { get; set; }
    }
}
