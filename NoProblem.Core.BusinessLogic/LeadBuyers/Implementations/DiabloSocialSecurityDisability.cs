﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations
{
    internal class DiabloSocialSecurityDisability : LeadBuyerBase
    {
        private const string ACCOUNT_ID = "051gmrfjx";
        private const string CAMPAIGN_ID = "05410eohh";
        private const string SITE_ID = "0555bxmhx";
        private const string PUBLISHER_ID = "58065";

        private const string URL = "https://app.leadconduit.com/v2/PostLeadAction";

        private const string IP_IS_MANDATORY = "IP is mandatory";
        private const string DOB_IS_MANDATORY = "Date of birth is mandatory.";
        private const string HEALTH_CONDITION_IS_MANDATORY = "Health condition description is mandatory.";
        private const string SOCIAL_SECURITY_DATA_IS_MANDATORY = "Social security disability specific data is missing";
        private const string YES = "yes";
        private const string NO = "no";

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                if (!incident.new_socialsecuritydisabilitydataid.HasValue)
                {
                    retVal.Reason = SOCIAL_SECURITY_DATA_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                var socialSecurityData = incident.new_socialsecuritydisabilitycasedata_incident;
                bool? ableToWork = socialSecurityData.new_abletowork;
                bool? missWork = socialSecurityData.new_misswork;
                bool? alreadyReceiveBenefits = socialSecurityData.new_alreadyreceivebenefits;
                bool? haveAttorney = socialSecurityData.new_haveattorney;
                bool? prescribedMedication = socialSecurityData.new_prescribedmedication;
                bool? work5of10 = socialSecurityData.new_work5of10;

                if (!ableToWork.HasValue || !missWork.HasValue || !alreadyReceiveBenefits.HasValue || !haveAttorney.HasValue || !prescribedMedication.HasValue || !work5of10.HasValue)
                {
                    retVal.Reason = SOCIAL_SECURITY_DATA_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                if (String.IsNullOrEmpty(firstName) || String.IsNullOrEmpty(lastName))
                {
                    retVal.Reason = FNAME_LNAME_ARE_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                var email = contact.emailaddress1;
                if (String.IsNullOrEmpty(email))
                {
                    retVal.Reason = EMAIL_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                var dateOfBirth = contact.birthdate;
                if (!dateOfBirth.HasValue)
                {
                    retVal.Reason = DOB_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = REGION_ERROR;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string city, state;
                ExtractZipcodeInfo(region, out city, out state);
                string zipcode = region.new_name;
                string phone = incident.new_telephone1;
                string ip = incident.new_ip;
                if (String.IsNullOrEmpty(ip))
                {
                    retVal.Reason = IP_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string healthCondition = incident.description;
                if (String.IsNullOrEmpty(healthCondition))
                {
                    retVal.Reason = HEALTH_CONDITION_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string subId = incident.incidentid.ToString();

                RestSharp.RestClient restClient = new RestSharp.RestClient(URL);
                RestSharp.RestRequest restRequest = new RestSharp.RestRequest(RestSharp.Method.POST);
                restRequest.AddParameter("xxAccountId", ACCOUNT_ID);
                restRequest.AddParameter("xxCampaignId", CAMPAIGN_ID);
                restRequest.AddParameter("xxSiteId", SITE_ID);
                restRequest.AddParameter("first_name", firstName);
                restRequest.AddParameter("last_name", lastName);
                restRequest.AddParameter("email", email);
                restRequest.AddParameter("phone_home", phone);
                restRequest.AddParameter("city", city);
                restRequest.AddParameter("state", state);
                restRequest.AddParameter("postal_code", zipcode);
                restRequest.AddParameter("dob", dateOfBirth.Value.ToString("yyyy-MM-dd"));
                restRequest.AddParameter("ip_address", ip);
                restRequest.AddParameter("health_condition", healthCondition);
                restRequest.AddParameter("miss_work", GetValueStringFromBool(missWork.Value));
                restRequest.AddParameter("already_receive_benefits", GetValueStringFromBool(alreadyReceiveBenefits.Value));
                restRequest.AddParameter("have_attorney", GetValueStringFromBool(haveAttorney.Value));
                restRequest.AddParameter("able_to_work", GetValueStringFromBool(ableToWork.Value));
                restRequest.AddParameter("prescribed_medication", GetValueStringFromBool(prescribedMedication.Value));
                restRequest.AddParameter("publisher_id", PUBLISHER_ID);
                restRequest.AddParameter("sub_id", subId);
                restRequest.AddParameter("work5of10", GetValueStringFromBool(work5of10.Value));
                if (IsTest())
                {
                    restRequest.AddParameter("xxTest", "true");
                }

                RestSharp.IRestResponse restResponse = restClient.Execute(restRequest);

                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    try
                    {
                        XElement root = XElement.Parse(restResponse.Content);
                        string result = root.Element("result").Value;
                        if (result == "success" || result == "queued")
                        {
                            retVal.IsSold = true;
                        }
                        else
                        {
                            retVal.MarkAsRejected = true;
                            var reasons = root.Elements("reason");
                            StringBuilder reasonsBuilder = new StringBuilder();
                            if (reasons != null)
                            {
                                foreach (var item in reasons)
                                {
                                    reasonsBuilder.Append(item.Value).Append(", ");
                                }
                            }
                            retVal.Reason = String.Format("Rejected with result code: {0}. Reasons: {1}", result, reasonsBuilder.ToString());
                        }
                        var leadIdElement = root.Element("leadId");
                        if (leadIdElement != null)
                        {
                            retVal.BrokerId = leadIdElement.Value;
                        }
                    }
                    catch (Exception exc)
                    {
                        LogUtils.MyHandle.HandleException(exc, "Exception in the parsing response part of Diablo API. content: {0}", restResponse.Content);
                        retVal.MarkAsRejected = true;
                        retVal.Reason = String.Format("Exception parsing XML in the response. content: {0}", restResponse.Content);
                    }
                }
                else//bad http status code
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = String.Format("Bad http status code received. http status code: {0}.", restResponse.StatusCode);
                }

            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DiabloSocialSecurityDisability.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private string GetValueStringFromBool(bool b)
        {
            return (b ? YES : NO);
        }
    }
}
