﻿using Effect.Crm.Logs;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class ProLegalLeads : LeadBuyerBase, IPingPostApiBuyer
    {
        private const string PING_URL = "http://legalleadcorp.com/hipinging.app";
        private const string POST_URL = "http://legalleadcorp.com/hiposting.app";
        private const string UNIQUE_API_KEY = "nopro5852";
        private const string SUCCESS = "success";

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                var result = Ping(
                    new PingData()
                    {
                        ExternalCode = accExp.new_externalcode,
                        Zip = incident.new_new_region_incident.new_name,
                        DefaultPrice = ia.new_potentialincidentprice.Value
                    });

                if (result.Accepted)
                {
                    retVal.BrokerId = result.PingId;
                    retVal.Price = result.PingPrice;
                }
                else
                {
                    retVal.WantsToBid = false;
                    retVal.MarkAsRejected = false;
                    retVal.Reason = result.PingMessage;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ProLegalLeads.GetBrokerBid");
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var result = Post(
                    new PostData()
                    {
                        PingId = pingId,
                        Zip = incident.new_new_region_incident.new_name,
                        FirstName = contact.firstname,
                        LastName = contact.lastname,
                        Phone = incident.new_telephone1,
                        Email = contact.emailaddress1,
                        Description = incident.description,
                        ExternalCode = accExp.new_externalcode
                    });
                if (result.Accepted)
                {
                    retVal.IsSold = true;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = result.PostMessage;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ProLegalLeads.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        public PingResult Ping(PingData data)
        {
            PingResult pingResult = new PingResult();
            RestClient client = new RestClient(PING_URL);
            RestRequest request = new RestRequest(Method.GET);
            request.AddParameter("zip", data.Zip);
            request.AddParameter("issue", data.ExternalCode);
            request.AddParameter("uniquekey", UNIQUE_API_KEY);
            var response = client.Execute(request);
            ProLegalLeadsPingResult result = null;
            try
            {
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<ProLegalLeadsPingResult>(response.Content);
            }
            catch (Exception) { }
            if (result != null)
            {
                if (result.Result.Response == SUCCESS)
                {
                    pingResult.Accepted = true;
                    pingResult.PingId = result.Result.PingKey;
                    pingResult.PingPrice = data.DefaultPrice;
                }
                else
                {
                    pingResult.Accepted = false;
                    pingResult.PingMessage = "Rejected at ping with message: '" + result.Result.Error + "'.";
                }
            }
            else
            {
                pingResult.PingMessage = String.Format("Failed to parse ProLegalLeads ping response. response = {0}. httpstatus = {1}", response.Content, response.StatusCode);
                LogUtils.MyHandle.WriteToLog(pingResult.PingMessage);
                pingResult.Accepted = false;
            }
            return pingResult;
        }

        public PostResult Post(PostData data)
        {
            PostResult postResult = new PostResult();
            RestClient client = new RestClient(POST_URL);
            RestRequest request = new RestRequest(Method.GET);
            string city, state;
            ExtractZipcodeInfo(data.Zip, out city, out state);
            request.AddParameter("zip", data.Zip);
            request.AddParameter("firstname", data.FirstName);
            request.AddParameter("lastname", data.LastName);
            request.AddParameter("uniquekey", UNIQUE_API_KEY);
            request.AddParameter("email", data.Email);
            request.AddParameter("issue", data.ExternalCode);
            request.AddParameter("summary", data.Description);
            request.AddParameter("phonenumber", data.Phone);
            request.AddParameter("state", state);
            request.AddParameter("pingkey", data.PingId);
            var response = client.Execute(request);
            ProLegalLeadsPostResult result = null;
            try
            {
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<ProLegalLeadsPostResult>(response.Content);
            }
            catch (Exception) { }
            if (result != null)
            {
                if (result.Result.Response == SUCCESS)
                {
                    postResult.Accepted = true;
                }
                else
                {
                    postResult.Accepted = false;
                    postResult.PostMessage = "Rejected at post with message: '" + GetStringFromStringArray(result.Result.Error) + "'.";
                }
            }
            else
            {
                postResult.PostMessage = String.Format("Failed to parse ProLegalLeads post response. response = {0}. httpstatus = {1}", response.Content, response.StatusCode);
                LogUtils.MyHandle.WriteToLog(postResult.PostMessage);
                postResult.Accepted = false;
            }
            return postResult;
        }

        private static string GetStringFromStringArray(string[] array)
        {
            if (array != null && array.Length > 0)
                return String.Join(", ", array);
            return String.Empty;
        }

        private class ProLegalLeadsPingResult
        {
            public InnerData Result { get; set; }

            public class InnerData
            {
                public string Response { get; set; }
                public string Error { get; set; }
                public string PingKey { get; set; }
            }
        }

        private class ProLegalLeadsPostResult
        {
            public InnerData Result { get; set; }

            public class InnerData
            {
                public string Response { get; set; }
                public string[] Error { get; set; }
            }
        }
    }
}
