﻿using Effect.Crm.Logs;
using System;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class DatalotFacade : LeadBuyerBase
    {
        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            try
            {
                Datalot instance = Implementations.DatalotApi.DatalotFactory.CreateInstance(accExp.new_externalcode);
                if (instance != null)
                {
                    return instance.SendLead(incident, accExp, supplier, incAcc, pingId);
                }
                else
                {
                    throw new Exception("Category is not configured correctly. Contact R&D for help.");
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in DatalotFacade.SendLead");
                return new SendLeadResponse()
                    {
                        FailedInnerValidation = true,
                        Reason = exc.Message
                    };
            }
        }
    }
}
