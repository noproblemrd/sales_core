﻿
namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.DatalotApi
{
    internal class DatalotFactory
    {
        public static Datalot CreateInstance(string externalCode)
        {
            switch (externalCode)
            {
                case "238"://home security
                    return new DatalotHomeSecurity();
                case "227":
                    return new DatalotHealthInsurance();
                default:
                    return null;
            }
        }
    }
}
