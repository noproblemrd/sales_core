﻿using System;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.DatalotApi
{
    class DatalotHealthInsurance : Datalot
    {
        private const string HEIGHT_IS_MANDATORY = "Height is mandatory.";
        private const string WEIGHT_IS_MANDATORY = "Weight is mandatory.";
        private const string HEALTH_DATA_MISSING = "Health insurance specific data is missing.";

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            if (incident.new_healthinsurancecasedataid.HasValue)
            {
                DataModel.Xrm.new_healthinsurancecasedata healthCaseData = incident.new_healthinsurancecasedata_incident;
                if (!healthCaseData.new_issmoker.HasValue
                    || !healthCaseData.new_isinsured.HasValue
                    || !healthCaseData.new_expectantparent.HasValue
                    || !healthCaseData.new_previouslydenied.HasValue)
                {
                    SetValidationFailureMessage(HEALTH_DATA_MISSING);
                }
            }
            else
            {
                SetValidationFailureMessage(HEALTH_DATA_MISSING);
            }
            var customer = incident.incident_customer_contacts;
            if (!customer.new_heightfeet.HasValue || !customer.new_heightinches.HasValue)
            {
                SetValidationFailureMessage(HEIGHT_IS_MANDATORY);
            }
            double stam;
            if (String.IsNullOrEmpty(customer.new_weight) || !double.TryParse(customer.new_weight, out stam))
            {
                SetValidationFailureMessage(WEIGHT_IS_MANDATORY);
            }
            //Call base to execute general validations for all Datalot
            base.ExecuteValidationSet(incident);
        }

        protected override System.Xml.Linq.XElement CreateProductInfoElement(DataModel.Xrm.incident incident)
        {
            DataModel.Xrm.new_healthinsurancecasedata healthInsuranceData = incident.new_healthinsurancecasedata_incident;
            var contact = incident.incident_customer_contacts;
            XElement productInfo = new XElement("product_info");
            productInfo.Add(new XElement("height_feet", contact.new_heightfeet));
            productInfo.Add(new XElement("height_inches", contact.new_heightinches));
            productInfo.Add(new XElement("weight", contact.new_weight));
            productInfo.Add(new XElement("age", GetAgeFromDOB(contact.birthdate.Value)));
            productInfo.Add(new XElement("is_smoker", BooleanToBoolString(healthInsuranceData.new_issmoker.Value)));
            productInfo.Add(new XElement("is_insured", BooleanToBoolString(healthInsuranceData.new_isinsured.Value)));
            productInfo.Add(new XElement("existing_condition", ConditionsToDatalotAccepted(healthInsuranceData.new_existingcondition)));
            productInfo.Add(new XElement("expectant_parent", BooleanToBoolString(healthInsuranceData.new_expectantparent.Value)));
            productInfo.Add(new XElement("previously_denied", BooleanToBoolString(healthInsuranceData.new_previouslydenied.Value)));
            if (healthInsuranceData.new_household.HasValue)
                productInfo.Add(new XElement("household", healthInsuranceData.new_household.Value));
            if (healthInsuranceData.new_income.HasValue)
                productInfo.Add(new XElement("income", healthInsuranceData.new_income.Value));
            return productInfo;
        }

        /*          
         public enum eExistingCondition
        {
            None,
            AidsOrHiv,
            Diabetes,
            Liver,
            Alzheimers,
            Lung,
            DrugAbuse,
            Mental,
            Cancer,
            Heart,
            Stroke,
            Kidney,
            Vascular
        }          
        */


        private string ConditionsToDatalotAccepted(string condition)
        {
            string retVal;
            switch (condition)
            {
                case "AidsOrHiv":
                    retVal = "AIDS/HIV";
                    break;
                case "Diabetes":
                    retVal = "Diabetes";
                    break;
                case "Liver":
                    retVal = "Liver Disease";
                    break;
                case "Alzheimers":
                    retVal = "Alzheimer's Disease";
                    break;
                case "Lung":
                    retVal = "Lung Disease";
                    break;
                case "DrugAbuse":
                    retVal = "Drug Abuse";
                    break;
                case "Mental":
                    retVal = "Mental Illness";
                    break;
                case "Cancer":
                    retVal = "Cancer";
                    break;
                case "Heart":
                    retVal = "Heart Disease";
                    break;
                case "Stroke":
                    retVal = "Stroke";
                    break;
                case "Kidney":
                    retVal = "Kidney Disease";
                    break;
                case "Vascular":
                    retVal = "Vascular Disease";
                    break;
                default:
                    retVal = "None";
                    break;
            }
            return retVal;
        }

        private int GetAgeFromDOB(DateTime bday)
        {
            DateTime today = DateTime.Today;
            int age = today.Year - bday.Year;
            if (bday > today.AddYears(-age)) age--;
            return age;
        }

        private string BooleanToBoolString(bool b)
        {
            return b ? "true" : "false";
        }
    }
}
