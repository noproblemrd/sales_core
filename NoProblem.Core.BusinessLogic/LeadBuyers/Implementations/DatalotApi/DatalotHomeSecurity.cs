﻿using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.DatalotApi
{
    internal class DatalotHomeSecurity : Datalot
    {
        private const string HOME_SECURITY_DATA_MISSING = "Home security specific data is missing in case.";
        private const string PROJECT_TIME_FRAME_MISSING = "Project time frame is mandatory.";
        private const string OWN_PROPERTY_MISSING = "Own Property? is mandatory.";
        private const string CREDIT_RANKING = "Credit ranking is mandatory.";

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            bool exists = ValidateHomeSecurityDataExists(incident);
            if (exists)
            {
                DataModel.Xrm.new_homesecuritycasedata homeSecurityData = incident.new_homesecuritycasedata_incident;
                ValidateTimeFrame(homeSecurityData);
                ValidateOwnProperty(homeSecurityData);
                ValidateCreditRank(homeSecurityData);
            }
            //Call base to execute general validations for all Datalot
            base.ExecuteValidationSet(incident);
        }

        protected override System.Xml.Linq.XElement CreateProductInfoElement(DataModel.Xrm.incident incident)
        {
            DataModel.Xrm.new_homesecuritycasedata homeSecurityData = incident.new_homesecuritycasedata_incident;
            XElement productInfo = new XElement("product_info");
            XElement services = new XElement("services");
            XElement homeSecurity = new XElement("home_security");
            string timeFrame = ConvertToDatalotValue((DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityTimeFrame)homeSecurityData.new_timeframe.Value);
            homeSecurity.Add(new XElement("timeframe", timeFrame));
            homeSecurity.Add(new XElement("own_property", homeSecurityData.new_ownproperty.Value ? "true" : "false"));
            string propertyType = ConvertToDatalotValue((DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityPropertyType)homeSecurityData.new_propertytype.Value);
            homeSecurity.Add(new XElement("property_type", propertyType));
            string creditRank = ConvertToDatalotValue((DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityCreditRanking)homeSecurityData.new_creditranking);
            homeSecurity.Add(new XElement("credit_self_reported", creditRank));
            services.Add(homeSecurity);
            productInfo.Add(services);
            return productInfo;
        }

        private string ConvertToDatalotValue(DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityCreditRanking eHomeSecurityCreditRanking)
        {
            string retVal;
            switch (eHomeSecurityCreditRanking)
            {
                case DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityCreditRanking.Fair:
                    retVal = "fair";
                    break;
                case DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityCreditRanking.Good:
                    retVal = "good";
                    break;
                case DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityCreditRanking.Poor:
                    retVal = "poor";
                    break;
                default:
                    retVal = "excellent";
                    break;
            }
            return retVal;
        }

        private string ConvertToDatalotValue(DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityPropertyType eHomeSecurityPropertyType)
        {
            string retVal;
            switch (eHomeSecurityPropertyType)
            {
                case DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityPropertyType.Business:
                    retVal = "business";
                    break;
                default:
                    retVal = "residence";
                    break;
            }
            return retVal;
        }

        private string ConvertToDatalotValue(DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityTimeFrame eHomeSecurityTimeFrame)
        {
            string retVal;
            switch (eHomeSecurityTimeFrame)
            {
                case DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityTimeFrame.unknown:
                    retVal = "unknown";
                    break;
                case DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityTimeFrame.two_three_months:
                    retVal = "2_3_months";
                    break;
                case DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityTimeFrame.two_four_weeks:
                    retVal = "2_4_weeks";
                    break;
                case DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityTimeFrame.over_three_months:
                    retVal = "over_3_months";
                    break;
                case DataModel.Xrm.new_homesecuritycasedata.eHomeSecurityTimeFrame.one_two_weeks:
                    retVal = "1_2_weeks";
                    break;
                default:
                    retVal = "now";
                    break;
            }
            return retVal;
        }

        private void ValidateTimeFrame(DataModel.Xrm.new_homesecuritycasedata homeSecurityData)
        {
            if (!homeSecurityData.new_timeframe.HasValue)
            {
                SetValidationFailureMessage(PROJECT_TIME_FRAME_MISSING);
            }
        }

        private void ValidateOwnProperty(DataModel.Xrm.new_homesecuritycasedata homeSecurityData)
        {
            if (!homeSecurityData.new_ownproperty.HasValue)
            {
                SetValidationFailureMessage(OWN_PROPERTY_MISSING);
            }
        }

        private void ValidateCreditRank(DataModel.Xrm.new_homesecuritycasedata homeSecurityData)
        {
            if (!homeSecurityData.new_creditranking.HasValue)
            {
                SetValidationFailureMessage(CREDIT_RANKING);
            }
        }

        private bool ValidateHomeSecurityDataExists(DataModel.Xrm.incident incident)
        {
            if (!incident.new_homesecuritycasedataid.HasValue)
            {
                SetValidationFailureMessage(HOME_SECURITY_DATA_MISSING);
                return false;
            }
            return true;
        }
    }
}
