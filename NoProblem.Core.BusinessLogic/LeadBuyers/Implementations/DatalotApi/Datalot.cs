﻿using Effect.Crm.Logs;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal abstract class Datalot : LeadBuyerBase
    {
        private const string API_ACCESS_KEY = "9jdj2huxst";
        private const string SOURCE_ID = "9772";
        private const string URL = "https://api.datalot.com/contact/create/v2";
        private readonly Dictionary<string, string> campaignIdDict =
            new Dictionary<string, string>(){
                { "238", "homesec" },
                { "227", "healthhost"}
            };

        private string productId;

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                if (!Validate(incident))
                {
                    retVal.Reason = validationMessage;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                productId = accExp.new_externalcode;
                XElement doc = CreateDocument(incident);

                RestSharp.IRestClient client = new RestSharp.RestClient(URL);
                RestSharp.IRestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
                request.AddParameter("text/xml", doc.ToString(), RestSharp.ParameterType.RequestBody);
                RestSharp.IRestResponse response = client.Execute(request);

                XElement resDoc = XElement.Parse(response.Content);
                string code = resDoc.Attribute("code").Value;
                if (code == "100")
                {
                    retVal.IsSold = true;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = String.Format("Rejected with code {0}", code);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Datalot.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private XElement CreateDocument(DataModel.Xrm.incident incident)
        {
            XElement doc = new XElement("contact_create");
            AddAuthElements(doc);
            XElement permissions = CreatePermissionsElement();
            doc.Add(permissions);
            XElement contactInfo = new XElement("contact_info");
            XElement generalInfo = CreateGeneralInfoElement(incident);
            XElement productInfo = CreateProductInfoElement(incident);
            contactInfo.Add(generalInfo);
            contactInfo.Add(productInfo);
            doc.Add(contactInfo);
            doc.Add(CreateTestElement());
            return doc;
        }

        private XElement CreateTestElement()
        {
            return new XElement("test", IsTest() ? "true" : "false");
        }

        private XElement CreateGeneralInfoElement(DataModel.Xrm.incident incident)
        {
            XElement info = new XElement("general_info");
            var contact = incident.incident_customer_contacts;
            info.Add(new XElement("first_name", contact.firstname));
            info.Add(new XElement("last_name", contact.lastname));
            info.Add(new XElement("street1", contact.address1_line1));
            string city, state;
            var region = incident.new_new_region_incident;
            ExtractZipcodeInfo(region, out city, out state);
            info.Add(new XElement("city", city));
            info.Add(new XElement("state", state));
            info.Add(new XElement("zip_code", region.new_name));
            info.Add(new XElement("email", contact.emailaddress1));
            info.Add(new XElement("phone_cell", incident.new_telephone1));
            info.Add(new XElement("ip_address", incident.new_ip));
            info.Add(new XElement("dob_year", contact.birthdate.Value.Year));
            info.Add(new XElement("dob_month", contact.birthdate.Value.Month));
            info.Add(new XElement("dob_day", contact.birthdate.Value.Day));
            info.Add(new XElement("datetime_collected", incident.createdon.Value.ToString("yyyy-MM-ddTHH:mm:ssZ")));
            return info;
        }

        private XElement CreatePermissionsElement()
        {
            XElement permissions = new XElement("contact_permission");
            permissions.Add(new XElement("phone_explicit", "true"));
            permissions.Add(new XElement("mobile_explicit", "true"));
            return permissions;
        }

        private void AddAuthElements(XElement doc)
        {
            doc.Add(new XElement("access_key", API_ACCESS_KEY));
            doc.Add(new XElement("source_id", SOURCE_ID));
            doc.Add(new XElement("product_id", productId));
            doc.Add(new XElement("campaign_id", campaignIdDict[productId]));
        }

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            //General validations for all Datalot
            ValidateFirstAndLastName(incident);
            ValidateAddress(incident);
            ValidateRegion(incident);
            ValidateEmail(incident);
            ValidateIp(incident);
            ValidateBirthDate(incident);
        }

        protected abstract XElement CreateProductInfoElement(DataModel.Xrm.incident incident);
    }
}



/*
Code	Description
100	Success
300	Error
310	Invalid request
312	Invalid XML
320	Invalid SourceId and/or AccessKey
321	Missing SourceId
322	Missing AccessKey
324	Missing ProductId
325	Invalid ProductId
328	Invalid PassThrough
329	Invalid Campaign Id
330	Unrecognized XML tag
332	Missing XML tag
334	Missing required data
335	Invalid ProductInfo tag
336	Invalid Option
340	Invalid input
341	Invalid input
346	Duplicate phone number submitted within 5 days
347	Duplicate email address submitted within 5 days
351	Duplicate detection error
352	Contact creation/database error
353	ProductInfo creation/database error
360	Contract error
394	Zipcode not found
395	Suspicious Data
400	Invalid Product Data
410	Bad Address Data
500	Unable to locate valid bid
700	Validation Error
*/