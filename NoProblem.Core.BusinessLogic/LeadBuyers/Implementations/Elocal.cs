﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class Elocal : LeadBuyerBase
    {
        private const string apiKey = "272a589139732f7454df85f4b8fd7d73f758350c";
        private const string urlProd = "http://api.elocal.com/lead/";
        private const string urlTest = "http://stage.api.elocal.com/lead/";

        #region ILeadBuyer Members

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var name = contact.fullname;
                if (string.IsNullOrEmpty(name))
                {
                    retVal.Reason = "Name is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                var email = contact.emailaddress1;
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = "Region is not zipcode level.";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string zipcode = region.new_name;

                RestSharp.RestClient client = new RestSharp.RestClient(urlProd);
                if (IsTest())
                {
                    client.BaseUrl = new Uri(urlTest);
                }

                RestSharp.RestRequest pingReq = new RestSharp.RestRequest(RestSharp.Method.POST);

                pingReq.RequestFormat = RestSharp.DataFormat.Json;
                pingReq.AddBody(new
                {
                    ping = new
                    {
                        key = apiKey,
                        zip_code = zipcode,
                        need_id = incident.new_new_primaryexpertise_incident.new_code,
                        sender_id = incident.ticketnumber
                    }
                });

                pingReq.Resource = "ping";
                var pingResponse = client.Execute<PingResponse>(pingReq);

                if (pingResponse.Data != null)
                {
                    if (pingResponse.Data.response.status == "success")
                    {
                        SendPostRequest(incident, retVal, name, zipcode, client, pingResponse.Data.response.token, email);
                    }
                    else
                    {
                        //LogUtils.MyHandle.WriteToLog("Lead not accepted by eLocal at ping stage. httpStatuCode = {1}. content: {0}", pingResponse.Content, pingResponse.StatusCode);
                        retVal.Reason = pingResponse.Data.response.message;
                        retVal.MarkAsRejected = true;
                    }
                }
                else
                {
                    LogUtils.MyHandle.WriteToLog("Failed to parse ping response from eLocal. httpStatuCode = {1}. content: {0}", pingResponse.Content, pingResponse.StatusCode);
                    retVal.Reason = "Failed to parse ping elocal's resposne: " + pingResponse.Content;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Elocal.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private void SendPostRequest(DataModel.Xrm.incident incident, SendLeadResponse retVal, string name, string zipcode, RestSharp.RestClient client, string token, string email)
        {
            RestSharp.RestRequest postReq = new RestSharp.RestRequest(RestSharp.Method.POST);

            postReq.RequestFormat = RestSharp.DataFormat.Json;
            postReq.AddBody(new
            {
                post = new
                {
                    key = apiKey,
                    zip_code = zipcode,
                    need_id = incident.new_new_primaryexpertise_incident.new_code,
                    sender_id = incident.ticketnumber,
                    ping_token = token,
                    first_name = name,
                    phone = incident.new_telephone1,
                    description = incident.description,
                    email = email
                }
            });

            postReq.Resource = "post";
            var postResponse = client.Execute<PostResponse>(postReq);

            if (postResponse.Data != null)
            {
                if (postResponse.Data.response.status == "success")
                {
                    retVal.IsSold = true;
                    retVal.BrokerId = postResponse.Data.response.elocal_lead_id;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    LogUtils.MyHandle.WriteToLog("eLocal did not return 'success' at post stage. httpStatuCode = {1}. content: {0}", postResponse.Content, postResponse.StatusCode);
                    retVal.Reason = "elocal didn't return 'success'. message: " + postResponse.Data.response.message;
                }
            }
            else
            {
                LogUtils.MyHandle.WriteToLog("Failed to parse post response from eLocal. httpStatuCode = {1}. content: {0}", postResponse.Content, postResponse.StatusCode);
                retVal.Reason = "Failed to parse post elocal's resposne: " + postResponse.Content;
            }
        }

        private class PostResponse
        {
            public Response response { get; set; }

            internal class Response
            {
                public string status { get; set; }
                public string token { get; set; }
                public string sender_id { get; set; }
                public string message { get; set; }
                public string elocal_lead_id { get; set; }
            }
        }

        private class PingResponse
        {
            public Response response { get; set; }

            internal class Response
            {
                public string status { get; set; }
                public string token { get; set; }
                public string sender_id { get; set; }
                public string message { get; set; }
            }
        }

        #endregion
    }
}
