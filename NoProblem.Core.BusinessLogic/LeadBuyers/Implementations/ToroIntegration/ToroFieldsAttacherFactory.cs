﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.ToroIntegration
{
    internal class ToroFieldsAttacherFactory
    {
        internal static IToroAreaOfPracticeFieldsAttacher Create(string externalCode)
        {
            if (String.IsNullOrEmpty(externalCode))
                return null;
            IToroAreaOfPracticeFieldsAttacher retVal;
            switch (externalCode.ToUpper())
            {
                case LeadType.BANKRUPTCY:
                    retVal = new ToroBankruptcyFieldsAttacher();
                    break;
                case LeadType.PERSONAL_INJURY:
                    retVal = new ToroPersonalInjuryFieldsAttacher();
                    break;
                case LeadType.DIVORCE:
                    retVal = new ToroDivorceFieldsAttacher();
                    break;
                case LeadType.SOCIAL_SECURITY_DISABILITY:
                    retVal = new ToroSSDFieldsAttacher();
                    break;
                case LeadType.DUI:
                    retVal = new ToroDUIFieldsAttacher();
                    break;
                default:
                    retVal = null;
                    break;
            }
            return retVal;
        }
    }
}
