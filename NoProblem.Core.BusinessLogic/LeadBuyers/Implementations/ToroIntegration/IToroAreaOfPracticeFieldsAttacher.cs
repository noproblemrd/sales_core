﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.ToroIntegration
{
    internal interface IToroAreaOfPracticeFieldsAttacher
    {
        bool AttachFields(DataModel.Xrm.incident incident, RestSharp.RestRequest restRequest, out string reason);
    }
}
