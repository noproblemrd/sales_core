﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.ToroIntegration
{
    internal class ToroDivorceFieldsAttacher : IToroAreaOfPracticeFieldsAttacher
    {
        #region IToroAreaOfPracticeFieldsAttacher Members

        public bool AttachFields(DataModel.Xrm.incident incident, RestSharp.RestRequest restRequest, out string reason)
        {
            reason = null;
            if (incident.new_divorceattorneycasedataid.HasValue)
            {
                var divorceData = incident.new_divorceattorneycasedata_incident;
                if (divorceData.new_areyoucurrentlyrepresented.HasValue && divorceData.new_areyoucurrentlyrepresented.Value)
                {
                    reason = "Already has an attorney.";
                    return false;
                }
                restRequest.AddParameter("hasAttorney", "2");
                if (!divorceData.new_howareyoufinancing.HasValue)
                {
                    reason = "Missing financing data";
                    return false;
                }
                string financingValue;
                switch ((DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing)divorceData.new_howareyoufinancing.Value)
                {
                    case DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.CreditCardOrPersonalLoan:
                        financingValue = "1";//Borrowing
                        break;
                    case DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.FamilySupport:
                        financingValue = "3";
                        break;
                    case DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.ICannotAffordLegalFees:
                        financingValue = "6";
                        break;
                    case DataModel.Xrm.new_divorceattorneycasedata.DivorceFinancing.PersonalSavings:
                        financingValue = "2";
                        break;
                    default:
                        financingValue = "5";//will discuss with my lawyer.
                        break;
                }
                if (financingValue == "6")
                {
                    reason = "Cannot afford legal fees.";
                    return false;
                }
                restRequest.AddParameter("financing", financingValue);
                return true;
            }
            else
            {
                reason = "Divorce specific data is missing.";
                return false;
            }
        }

        #endregion
    }
}
