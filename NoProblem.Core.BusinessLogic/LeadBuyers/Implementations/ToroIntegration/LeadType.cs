﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.ToroIntegration
{
    internal static class LeadType
    {
        internal const string BANKRUPTCY = "BK";
        internal const string DIVORCE = "DIV";
        internal const string CHILD_CUSTODY = "CC";
        internal const string DUI = "DUI";
        internal const string PERSONAL_INJURY = "PI";
        internal const string CRIMINAL_DEFENSE = "CD";
        internal const string SOCIAL_SECURITY_DISABILITY = "SSD";
        internal const string RETAINER_FUNDING_LOANS = "LRET";
        internal const string AUTO_TITLE_FUNDING_LOANS = "LAUTO";
        internal const string PAYDAY_LOANS = "LPAY";
    }
}
