﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.ToroIntegration
{
    internal class ToroPersonalInjuryFieldsAttacher : IToroAreaOfPracticeFieldsAttacher
    {
        #region IToroAreaOfPracticeFieldsAttacher Members

        public bool AttachFields(DataModel.Xrm.incident incident, RestSharp.RestRequest restRequest, out string reason)
        {
            reason = null;
            if (incident.new_personalinjurycasedataid.HasValue)
            {
                var piData = incident.new_personalinjurycasedata_incident;
                if (piData.new_currentlyrepresented.HasValue && piData.new_currentlyrepresented.Value)
                {
                    reason = "Already has attorney";
                    return false;
                }
                restRequest.AddParameter("hasAttorney", "2");
                if (piData.new_isyourfault.HasValue && piData.new_isyourfault.Value)
                {
                    reason = "The customer is at fault.";
                    return false;
                }
                restRequest.AddParameter("fault", "2");
                return true;
            }
            else
            {
                reason = "Personal injury specific data is missing.";
                return false;
            }
        }

        #endregion
    }
}
