﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.ToroIntegration
{
    class ToroSSDFieldsAttacher : IToroAreaOfPracticeFieldsAttacher
    {
        #region IToroAreaOfPracticeFieldsAttacher Members

        public bool AttachFields(DataModel.Xrm.incident incident, RestSharp.RestRequest restRequest, out string reason)
        {
            reason = null;
            if (incident.new_socialsecuritydisabilitydataid.HasValue)
            {
                var ssdData = incident.new_socialsecuritydisabilitycasedata_incident;
                if (ssdData.new_haveattorney.HasValue && ssdData.new_haveattorney.Value)
                {
                    reason = "Already has attorney.";
                    return false;
                }
                restRequest.AddParameter("hasAttorney", "2");
                if (!incident.incident_customer_contacts.birthdate.HasValue)
                {
                    reason = "Birth date is missing.";
                    return false;
                }
                restRequest.AddParameter("dofb", incident.incident_customer_contacts.birthdate.Value.ToString("yyyy-MM-dd"));
                if (ssdData.new_abletowork.HasValue && ssdData.new_abletowork.Value)
                {
                    reason = "Currently working fulltime.";
                    return false;
                }
                restRequest.AddParameter("curWorking", "2");
                if (ssdData.new_alreadyreceivebenefits.HasValue && ssdData.new_alreadyreceivebenefits.Value)
                {
                    reason = "Already receives benefits";
                    return false;
                }
                restRequest.AddParameter("benefits", "2");
                if (ssdData.new_hasdoctor.HasValue && !ssdData.new_hasdoctor.Value)
                {
                    reason = "Does not have a doctor";
                    return false;
                }
                restRequest.AddParameter("hasDoctor", "1");
                if (String.IsNullOrEmpty(incident.description))
                {
                    reason = "description is missing";
                    return false;
                }
                restRequest.AddParameter("otherComments", incident.description);
                return true;
            }
            else
            {
                reason = "SSD specific data is missing.";
                return false;
            }
        }

        #endregion
    }
}
