﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.ToroIntegration
{
    class ToroDUIFieldsAttacher : IToroAreaOfPracticeFieldsAttacher
    {
        #region IToroAreaOfPracticeFieldsAttacher Members

        public bool AttachFields(DataModel.Xrm.incident incident, RestSharp.RestRequest restRequest, out string reason)
        {
            reason = null;
            if (incident.new_divorceattorneycasedataid.HasValue)
            {
                var duiData = incident.new_duiattorneycasedata_incident;
                if (!duiData.new_financing.HasValue)
                {
                    reason = "Missing financing data.";
                    return false;
                }
                string financingValue = GetFinancingMapping(duiData);
                if (financingValue == "6")//cannot afford
                {
                    reason = "Cannot afford legal fees.";
                    return false;
                }
                restRequest.AddParameter("financing", financingValue);
                restRequest.AddParameter("otherComments", incident.description);
                return true;
            }
            else
            {
                reason = "DUI specific data is missing.";
                return false;
            }
        }

        private string GetFinancingMapping(DataModel.Xrm.new_duiattorneycasedata duiData)
        {
            string value;
            switch ((DataModel.Xrm.new_duiattorneycasedata.Financing)duiData.new_financing.Value)
            {
                case DataModel.Xrm.new_duiattorneycasedata.Financing.Borrowing:
                    value = "1";
                    break;
                case DataModel.Xrm.new_duiattorneycasedata.Financing.CannotAfford:
                    value = "6";
                    break;
                case DataModel.Xrm.new_duiattorneycasedata.Financing.CurrentIncome:
                    value = "4";
                    break;
                case DataModel.Xrm.new_duiattorneycasedata.Financing.FamilySupport:
                    value = "3";
                    break;
                case DataModel.Xrm.new_duiattorneycasedata.Financing.PersonalSavings:
                    value = "2";
                    break;
                default://WillDiscussWithAttorney:
                    value = "5";
                    break;
            }
            return value;
        }

        #endregion
    }
}
