﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.ToroIntegration
{
    internal class ToroBankruptcyFieldsAttacher : IToroAreaOfPracticeFieldsAttacher
    {
        #region IToroAreaOfPracticeFieldsAttacher Members

        public bool AttachFields(DataModel.Xrm.incident incident, RestSharp.RestRequest restRequest, out string reason)
        {
            reason = null;
            restRequest.AddParameter("otherComments", incident.description);
            if (incident.new_bankruptcyattorneycasedataid.HasValue)
            {
                var bkData = incident.new_bankruptcyattorneycasedata_incident;
                if (bkData.new_whyconsideringbankruptcy.HasValue)
                {
                    string reasonValue = GetReasonMapping(bkData);
                    if (reasonValue != null)
                        restRequest.AddParameter("reasons", reasonValue);
                }
                if (bkData.new_whichbillsdoyouhave.HasValue)
                {
                    string billValue = GetBillsMapping(bkData);
                    if (billValue != null)
                        restRequest.AddParameter("billTypes", billValue);
                }
            }
            return true;
        }

        private string GetBillsMapping(DataModel.Xrm.new_bankruptcyattorneycasedata bkData)
        {
            string billsValue = null;
            switch ((DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy)bkData.new_whichbillsdoyouhave.Value)
            {
                case DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.AutoLoansIncomeTaxes:
                    billsValue = "6";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.ChildSupport:
                    billsValue = "3";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.MedicalBills:
                    billsValue = "8";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.Other:
                    billsValue = "99";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.PaydayLoans:
                    billsValue = "7";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.PersonalLoans:
                    billsValue = "2";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.StoreCards:
                case DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.CreditCards:
                    billsValue = "1";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.BillsBankruptcy.StudentLoans:
                    billsValue = "4";
                    break;
            }
            return billsValue;
        }

        private static string GetReasonMapping(DataModel.Xrm.new_bankruptcyattorneycasedata bkData)
        {
            string reasonValue = null;
            switch ((DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy)bkData.new_whyconsideringbankruptcy.Value)
            {
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.CreditorHarassment:
                    reasonValue = "2";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Disability:
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Illness:
                    reasonValue = "6";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Divorce:
                    reasonValue = "8";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Foreclosure:
                    reasonValue = "4";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Garnishment:
                    reasonValue = "1";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Lawsuits:
                    reasonValue = "5";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.LicenseSuspension:
                    reasonValue = "7";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.LossOfIncome:
                    reasonValue = "9";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Other:
                    reasonValue = "99";
                    break;
                case DataModel.Xrm.new_bankruptcyattorneycasedata.ConsideringBankruptcy.Repossession:
                    reasonValue = "3";
                    break;
            }
            return reasonValue;
        }

        #endregion
    }
}
