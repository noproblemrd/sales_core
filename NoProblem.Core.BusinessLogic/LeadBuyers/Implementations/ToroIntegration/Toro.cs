﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations
{
    /*
     Response examples
     
 <?xml version="1.0" encoding="utf-8"?> 
<response xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> 
<status>
<statuscode>FAILED</statuscode>
<statusmessage>invalid fields</statusmessage>
</status>
<errors><field>financing invalid</field></errors>
</response>


<?xml version="1.0" encoding="utf-8"?> 
<response xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> 
<status>
<statuscode>SUCCESS</statuscode>
<statusmessage>LeadID 53298</statusmessage>
</status>
<errors></errors>
</response>

     */

    internal class Toro : LeadBuyerBase
    {
        private const string URL_POST = "http://www.toromediaserver.com/postLeadService.cfm";
        private const string URL_COVERAGE_CHECK = "http://www.toromediaserver.com/partners/coverageCheck.cfm";
        private const int timeout = 60 * 1000;
        private const string SUBMITTER_ID = "1029";
        private const string DEFAULT_IP = "0.0.0.0";
        private const string YES = "yes";

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = REGION_ERROR;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string zipcode = region.new_name;
                string areaOfPractice = accExp.new_externalcode;

                bool isInCoverageArea = CheckCoverageArea(zipcode, areaOfPractice);

                if (!isInCoverageArea)
                {
                    retVal.Reason = OUTSIDE_OF_COVERAGE_AREA;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                RestSharp.RestRequest restRequest = new RestSharp.RestRequest(RestSharp.Method.GET);

                ToroIntegration.IToroAreaOfPracticeFieldsAttacher fieldAttacher = ToroIntegration.ToroFieldsAttacherFactory.Create(areaOfPractice);
                if (fieldAttacher == null)
                {
                    retVal.Reason = "Advertisers is not configured correctly for this heading.";
                    LogUtils.MyHandle.WriteToLog("Advertiser Toro (API) is not configured correctly");
                    return retVal;
                }

                string reason;
                bool ok = fieldAttacher.AttachFields(incident, restRequest, out reason);
                if (!ok)
                {
                    retVal.Reason = reason;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                string firstName, lastName, email;
                var contact = incident.incident_customer_contacts;
                firstName = contact.firstname;
                lastName = contact.lastname;
                if (String.IsNullOrEmpty(firstName) || String.IsNullOrEmpty(lastName))
                {
                    retVal.Reason = FNAME_LNAME_ARE_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                email = contact.emailaddress1;
                if (String.IsNullOrEmpty(email))
                {
                    retVal.Reason = EMAIL_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                if (IsTest())
                {
                    firstName = lastName = "test";
                    email = "test@test.com";
                }
                restRequest.AddParameter("submitter", SUBMITTER_ID);
                restRequest.AddParameter("ipaddress", String.IsNullOrEmpty(incident.new_ip) ? DEFAULT_IP : incident.new_ip);
                restRequest.AddParameter("leadtype", areaOfPractice);
                restRequest.AddParameter("firstname", firstName);
                restRequest.AddParameter("lastname", lastName);
                restRequest.AddParameter("email", email);
                restRequest.AddParameter("phonenumber", incident.new_telephone1);
                restRequest.AddParameter("zipcode", zipcode);
                restRequest.AddParameter("terms", "1");
                restRequest.AddParameter("tracking1", incident.incidentid.ToString());

                RestSharp.RestClient restClient = new RestSharp.RestClient(URL_POST);
                restClient.Timeout = timeout;
                RestSharp.IRestResponse restResponse = restClient.Execute(restRequest);
                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    XElement root = XElement.Parse(restResponse.Content);
                    var status = root.Element("status");
                    string statusCode = status.Element("statuscode").Value;
                    string statusMessage = status.Element("statusmessage").Value;
                    if (statusCode == "SUCCESS")
                    {
                        retVal.IsSold = true;
                        retVal.BrokerId = statusMessage;
                    }
                    else
                    {
                        retVal.MarkAsRejected = true;
                        StringBuilder builder = new StringBuilder();
                        builder.Append(statusMessage).Append(": ");
                        foreach (var element in root.Elements("errors"))
                        {
                            if (element.HasElements)
                            {
                                foreach (var child in element.Elements())
                                {
                                    builder.Append(child.Value).Append("; ");
                                }
                            }
                            else
                            {
                                builder.Append(element.Value).Append("; ");
                            }
                        }
                        retVal.Reason = builder.ToString();
                    }
                }
                else
                {
                    retVal.Reason = String.Format("Bad http status code. Status = {0}. Content = {1}", restResponse.StatusCode, restResponse.Content);
                    retVal.MarkAsRejected = true;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Toro.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                    retVal.FailedInnerValidation = true;
                }
            }
            return retVal;
        }

        private bool CheckCoverageArea(string zipcode, string areaOfPractice)
        {
            RestSharp.RestRequest restRequest = new RestSharp.RestRequest(RestSharp.Method.GET);
            restRequest.AddParameter("leadtype", areaOfPractice);
            restRequest.AddParameter("submitter", SUBMITTER_ID);
            restRequest.AddParameter("zipcode", zipcode);
            RestSharp.RestClient restClient = new RestSharp.RestClient(URL_COVERAGE_CHECK);
            RestSharp.IRestResponse restResponse = restClient.Execute(restRequest);
            return restResponse.Content == YES;
        }
    }
}
