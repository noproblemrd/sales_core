﻿using System;
using System.Linq;
using System.Xml.Linq;
using NoProblem.Core.BusinessLogic.Utils;
using System.Collections.Generic;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class T3leads : LeadBuyerBase
    {
        private const string url = "https://t3leads.com/system/lead_channel/server_post.php";
        private const string ip = "74.54.87.132";
        private const string email = "user{0}@aol.com";
        private const string address = "123 x street";
        private const string regionError = "Region is not zip code level.";
        private const string insuranceLiteProductType = "auto";
        private const string insuranceLitePhoneType = "cell";
        private const string INSURANCELITE = "insuranceLite";
        private const string PAYDAYLOANS = "paydayLoans";
        private static string[][] dic = new string[2][] { 
            new string[] { "3.41013", INSURANCELITE, "uuBgRvaHvwpMpUHREQoU" }, 
            new string[] { "4.41013", PAYDAYLOANS, "HxVdJfddWbCkaknawrhr" } };
        private const string dateFormat = "yyyy-MM-dd";

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();

            string[] categoryInfo = null;
            for (int i = 0; i < dic.Length; i++)
            {
                if (dic[i][0] == accExp.new_externalcode)
                {
                    categoryInfo = dic[i];
                    break;
                }
            }
            if (categoryInfo == null)
            {
                retVal.Reason = "Partner is not configured correctly";
                retVal.FailedInnerValidation = true;
                LogUtils.MyHandle.WriteToLog("Parter t3leads is not configured correctly. IncidentId = {0}", incident.incidentid);
                return retVal;
            }
            var region = incident.new_new_region_incident;
            if (region.new_level != 4)
            {
                retVal.Reason = regionError;
                retVal.FailedInnerValidation = true;
                return retVal;
            }
            Utils.HttpClient client;
            switch (categoryInfo[1])
            {
                case INSURANCELITE:
                    client = PrepareRequestForInsuranceLite(retVal, incident, region, accExp);
                    break;
                case PAYDAYLOANS:
                    client = PrepareRequestForPaydayLoans(retVal, incident, region, accExp);
                    break;
                default:
                    retVal.Reason = "Parter is not configured correctly";
                    retVal.FailedInnerValidation = true;
                    LogUtils.MyHandle.WriteToLog("Parter t3leads is not configured correctly. IncidentId = {0}", incident.incidentid);
                    client = null;
                    break;
            }

            if (client == null)
            {
                retVal.FailedInnerValidation = true;
                return retVal;
            }

            AddGeneralParams(categoryInfo, client);

            var response = client.Post();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                ParseXmlResponse(retVal, response);
            }
            else
            {
                retVal.Reason = string.Format("Bad HTTP received from T3leads. StatusCode = {0}, Content = {1}", response.StatusCode, response.Content);
            }
            return retVal;
        }

        private void AddGeneralParams(string[] categoryInfo, HttpClient client)
        {
            client.ParamItems.Add(PostParamNamesGeneral.ID, categoryInfo[0]);
            client.ParamItems.Add(PostParamNamesGeneral.CLIENT_IP_ADDRESS, ip);
            client.ParamItems.Add(PostParamNamesGeneral.MINIMUM_PRICE, "0");
            client.ParamItems.Add(PostParamNamesGeneral.PASSWORD, categoryInfo[2]);
            if (System.Configuration.ConfigurationManager.AppSettings["SiteId"] != "Sales")
            {
                client.ParamItems.Add(PostParamNamesGeneral.TEST_STATUS, "sold");
            }
        }

        private void ParseXmlResponse(SendLeadResponse retVal, NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpResponse response)
        {
            /*
             <Response>
                <Status>sold</Status>
                <LeadID>1000049</LeadID>
                <Price>77</Price>
            </Response>
            */
            try
            {
                XElement root = XElement.Parse(response.Content);
                string statusValue = root.Element("Status").Value;
                if (statusValue == "sold")
                {
                    retVal.IsSold = true;
                    retVal.BrokerId = root.Element("LeadID").Value + ", Price: " + root.Element("Price").Value;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = string.Format("T3leads didn't return 'sold'. Content = {0}", response.Content);
                }
            }
            catch (Exception)
            {
                retVal.Reason = string.Format("Unable to parse XML response. Context = {0}", response.Content);
            }
        }

        private static class PostParamNamesGeneral
        {
            internal const string ID = "id";
            internal const string PASSWORD = "password";
            internal const string CLIENT_IP_ADDRESS = "client_ip_address";
            internal const string MINIMUM_PRICE = "minimum_price";
            internal const string TEST_STATUS = "test_status";
        }

        private void ExtractZipcodeInfo(DataModel.Xrm.new_region region, out string zipCode, out string city, out string state)
        {
            string[] regionNameSplit = region.new_englishname.Split(';');
            zipCode = regionNameSplit[0];
            city = regionNameSplit[1];
            DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
            var parents = regDal.GetParents(region);
            state = parents.First(x => x.new_level == 1).new_name;
        }

        #region Payday Loans

        private Utils.HttpClient PrepareRequestForPaydayLoans(SendLeadResponse response, DataModel.Xrm.incident incident, DataModel.Xrm.new_region region, DataModel.Xrm.new_accountexpertise accExp)
        {
            if (!incident.new_paydayloandataid.HasValue)
            {
                response.Reason = "Request didn't have mandatory pay day loan data";
                return null;
            }
            DataAccessLayer.PaydayLoanData paydayDataDal = new NoProblem.Core.DataAccessLayer.PaydayLoanData(null);
            DataModel.Xrm.new_paydayloandata paydayData = paydayDataDal.Retrieve(incident.new_paydayloandataid.Value);

            if (!paydayData.new_accounttype.HasValue)
            {
                response.Reason = "Account type is mandatory";
                return null;
            }
            DataModel.Xrm.new_paydayloandata.pdlAccountType accountTypeEnum = (DataModel.Xrm.new_paydayloandata.pdlAccountType)paydayData.new_accounttype.Value;
            string accountType = null;
            switch (accountTypeEnum)
            {
                case NoProblem.Core.DataModel.Xrm.new_paydayloandata.pdlAccountType.Checking:
                    accountType = "CHECKING";
                    break;
                case NoProblem.Core.DataModel.Xrm.new_paydayloandata.pdlAccountType.Saving:
                    accountType = "SAVING";
                    break;
            }
            if (!paydayData.new_lenghtofaccount.HasValue)
            {
                response.Reason = "length of account is mandatory";
                return null;
            }
            string lenghtOfAccount = paydayData.new_lenghtofaccount.Value.ToString();
            if (string.IsNullOrEmpty(paydayData.new_bankphone))
            {
                response.Reason = "Bank phone is mandatory";
                return null;
            }
            string bankPhone = paydayData.new_bankphone;
            if (string.IsNullOrEmpty(paydayData.new_bankname))
            {
                response.Reason = "Bank name is mandatory";
                return null;
            }
            string bankName = paydayData.new_bankname;
            if (string.IsNullOrEmpty(paydayData.new_bankaccountnumber))
            {
                response.Reason = "Bank account number is mandatory";
                return null;
            }
            string bankAccountNumber = paydayData.new_bankaccountnumber;
            if (string.IsNullOrEmpty(paydayData.new_abarounting))
            {
                response.Reason = "ABA/Routing is mandatory";
                return null;
            }
            string abaRouting = paydayData.new_abarounting;
            if (!paydayData.new_secondpayday.HasValue)
            {
                response.Reason = "Second payday is mandatory";
                return null;
            }
            string secondPayday = paydayData.new_secondpayday.Value.ToString(dateFormat);
            if (!paydayData.new_nextpayday.HasValue)
            {
                response.Reason = "Next payday is mandatory";
                return null;
            }
            string nextPayday = paydayData.new_nextpayday.Value.ToString(dateFormat);
            if (!paydayData.new_directdeposit.HasValue)
            {
                response.Reason = "Direct deposit is mandatory";
                return null;
            }
            string directDeposit = paydayData.new_directdeposit.Value ? "1" : "0";
            if (!paydayData.new_payfrequency.HasValue)
            {
                response.Reason = "Pay frequency is mandatory";
                return null;
            }
            DataModel.Xrm.new_paydayloandata.pdlPayFrequency payFrequencyEnum = (DataModel.Xrm.new_paydayloandata.pdlPayFrequency)paydayData.new_payfrequency.Value;
            string payFrequency = null;
            switch (payFrequencyEnum)
            {
                case NoProblem.Core.DataModel.Xrm.new_paydayloandata.pdlPayFrequency.Biweekly:
                    payFrequency = "BIWEEKLY";
                    break;
                case NoProblem.Core.DataModel.Xrm.new_paydayloandata.pdlPayFrequency.Weekly:
                    payFrequency = "WEEKLY";
                    break;
                case NoProblem.Core.DataModel.Xrm.new_paydayloandata.pdlPayFrequency.Monthly:
                    payFrequency = "MONTHLY";
                    break;
                case NoProblem.Core.DataModel.Xrm.new_paydayloandata.pdlPayFrequency.TwiceMonthly:
                    payFrequency = "TWICEMONTHLY";
                    break;
            }
            string military = paydayData.new_activemilitary.HasValue ? (paydayData.new_activemilitary.Value ? "1" : "0") : "0";
            var contact = incident.incident_customer_contacts;
            string firstName = contact.firstname;
            if (string.IsNullOrEmpty(firstName))
            {
                response.Reason = "First name is mandatory";
                return null;
            }
            string lastName = contact.lastname;
            if (string.IsNullOrEmpty(lastName))
            {
                response.Reason = "Last name is mandatory";
                return null;
            }
            string contactEmail = contact.emailaddress1;
            if (string.IsNullOrEmpty(email))
            {
                contactEmail = string.Format(email, incident.new_telephone1);
            }
            string zipCode;
            string city;
            string state;
            ExtractZipcodeInfo(region, out zipCode, out city, out state);

            Utils.HttpClient client = new HttpClient(url, HttpClient.HttpMethodEnum.Post);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.FIRST_NAME, firstName);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.LAST_NAME, lastName);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.ZIP, zipCode);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.STATE, state);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.CITY, city);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.EMAIL, contactEmail);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.BANK_ACCOUNT_TYPE, accountType);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.BANK_ACCOUNT_LENGTH_MONTHS, lenghtOfAccount);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.BANK_PHONE, bankPhone);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.BANK_NAME, bankName);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.BANK_ACCOUNT_NUMBER, bankAccountNumber);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.BANK_ABA, abaRouting);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.PAY_DATE2, secondPayday);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.PAY_DATE1, nextPayday);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.DIRECT_DEPOSIT, directDeposit);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.PAY_FREQUENCY, payFrequency);
            client.ParamItems.Add(PostParamNamesForPaydayLoans.ACTIVE_MILITARY, military);

            return client;
        }

        private static class PostParamNamesForPaydayLoans
        {
            internal const string FIRST_NAME = "first_name";
            internal const string LAST_NAME = "last_name";
            internal const string EMAIL = "email";
            internal const string ZIP = "zip";
            internal const string STATE = "state";
            internal const string CITY = "city";
            internal const string ADDRESS = "address";
            internal const string REQUESTED_AMOUNT = "requested_amount";
            internal const string EMPLOYER = "employer";
            internal const string JOB_TITLE = "job_title";
            internal const string ACTIVE_MILITARY = "active_military";
            internal const string EMPLOYED_MONTHS = "employed_months";
            internal const string INCOME_TYPE = "income_type";
            internal const string MONTHLY_INCOME = "monthly_income";
            internal const string PAY_DATE1 = "pay_date1";
            internal const string PAY_DATE2 = "pay_date2";
            internal const string PAY_FREQUENCY = "pay_frequency";
            internal const string DRIVERS_LICENSE_NUMBER = "drivers_license_number";
            internal const string DRIVERS_LICENSE_STATE = "drivers_license_state";
            internal const string BANK_NAME = "bank_name";
            internal const string BANK_PHONE = "bank_phone";
            internal const string BANK_ABA = "bank_aba";
            internal const string BANK_ACCOUNT_NUMBER = "bank_account_number";
            internal const string BANK_ACCOUNT_TYPE = "bank_account_type";
            internal const string BANK_ACCOUNT_LENGTH_MONTHS = "bank_account_length_months";
            internal const string DIRECT_DEPOSIT = "direct_deposit";
            internal const string SSN = "ssn";
            internal const string BIRTH_DATE = "birth_date";
            internal const string OWN_HOME = "own_home";
            internal const string ADDRESS_LENGTH_MONTHS = "address_length_months";
            internal const string HOME_PHONE = "home_phone";
            internal const string WORK_PHONE = "work_phone";
            internal const string BEST_TIME_TO_CALL = "best_time_to_call";
        }

        #endregion

        #region Insurance Lite

        private Utils.HttpClient PrepareRequestForInsuranceLite(SendLeadResponse response, DataModel.Xrm.incident incident, DataModel.Xrm.new_region region, DataModel.Xrm.new_accountexpertise accExp)
        {
            var name = incident.incident_customer_contacts.fullname;
            if (string.IsNullOrEmpty(name))
            {
                response.Reason = "Name is mandatory";
                return null;
            }
            string zipCode;
            string city;
            string state;
            ExtractZipcodeInfo(region, out zipCode, out city, out state);
            Utils.HttpClient client = new NoProblem.Core.BusinessLogic.Utils.HttpClient(url, HttpClient.HttpMethodEnum.Post);
            AddParametersForInsuranceLite(incident, accExp, name, zipCode, city, state, client);
            return client;
        }

        private void AddParametersForInsuranceLite(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, string name, string zipCode, string city, string state, Utils.HttpClient client)
        {
            client.ParamItems.Add(PostParamNamesForInsuranceLite.ADDRESS, address);
            client.ParamItems.Add(PostParamNamesForInsuranceLite.CITY, city);
            client.ParamItems.Add(PostParamNamesForInsuranceLite.EMAIL, string.Format(email, incident.new_telephone1));
            client.ParamItems.Add(PostParamNamesForInsuranceLite.FIRST_NAME, name);
            client.ParamItems.Add(PostParamNamesForInsuranceLite.LAST_NAME, name);
            client.ParamItems.Add(PostParamNamesForInsuranceLite.PHONE, incident.new_telephone1);
            client.ParamItems.Add(PostParamNamesForInsuranceLite.PHONE_TYPE, insuranceLitePhoneType);
            client.ParamItems.Add(PostParamNamesForInsuranceLite.PRODUCT_TYPE, insuranceLiteProductType);
            client.ParamItems.Add(PostParamNamesForInsuranceLite.STATE, state);
            client.ParamItems.Add(PostParamNamesForInsuranceLite.ZIP, zipCode);
        }

        private static class PostParamNamesForInsuranceLite
        {
            internal const string FIRST_NAME = "first_name";
            internal const string LAST_NAME = "last_name";
            internal const string PHONE = "phone";
            internal const string EMAIL = "email";
            internal const string ZIP = "zip";
            internal const string STATE = "state";
            internal const string CITY = "city";
            internal const string ADDRESS = "address";
            internal const string PHONE_TYPE = "phone_type";
            internal const string PRODUCT_TYPE = "product_type";
        }

        #endregion
    }
}
