﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class TesterBid : LeadBuyerBase, IPingPostApiBuyer
    {
        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            decimal predefinedPrice = ia.new_potentialincidentprice.Value;
            retVal.Price = Math.Ceiling(predefinedPrice + 1);
            retVal.BrokerId = "pingfromtestbid";
            return retVal;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            return new SendLeadResponse()
            {
                BrokerId = "test bid sold",
                IsSold = true,
            };
        }

        public PingResult Ping(PingData data)
        {
            System.Threading.Thread.Sleep(10000);
            PingResult result = new PingResult();
            result.Accepted = true;
            result.PingId = "pingfromtestbid";
            result.PingPrice = data.DefaultPrice + 1;
            return result;
        }

        public PostResult Post(PostData data)
        {
            return new PostResult()
            {
                Accepted = true
            };
        }
    }
}
