﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Collections.Specialized;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations
{
    internal class WebFore : LeadBuyerBase
    {
        private const string username = "noproblem";
        private const string password = "chf4ZdRj@w3";
        private const string baseUrl = "http://wbf-offers.com/";
        private const string authenticationResource = "authentication/login";
        private const string leadsUrlPrefix = "rest/landingpages/";
        private const string createLeadResource = "leads/create";
        private const string US = "US";
        private const string MALE = "MALE";
        private const string FEMALE = "FEMALE";
        private const string OTHER_VERTICAL_CODE = "noproblem_other";
        private const string FNAME_LNAME_ARE_MANDATORY = "FirstName and LastName are mandatory";
        private const string EMAIL_IS_MANDATORY = "Email is mandatory";
        private static Random random = new Random();

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse response = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                var email = contact.emailaddress1;
                if (String.IsNullOrEmpty(firstName) | String.IsNullOrEmpty(lastName))
                {
                    response.Reason = FNAME_LNAME_ARE_MANDATORY;
                    response.FailedInnerValidation = true;
                    return response;
                }
                if (String.IsNullOrEmpty(email))
                {
                    response.Reason = EMAIL_IS_MANDATORY;
                    response.FailedInnerValidation = true;
                    return response;
                }
                string token;
                string authMessage;
                bool authOk = Authenticate(out authMessage, out token);
                if (!authOk)
                {
                    response.Reason = authMessage;
                }
                else
                {
                    DateTime bDay = GetBirthDay(contact);
                    bool isFemale = FindGender(contact);
                    string postMessage;
                    bool ok = PostLead(incident.new_telephone1, firstName, lastName, email, token, accExp.new_externalcode, isFemale, bDay, out postMessage);
                    if (ok)
                    {
                        response.IsSold = true;
                        response.BrokerId = postMessage;
                    }
                    else
                    {
                        response.MarkAsRejected = true;
                        response.Reason = postMessage;
                    }
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in WebFore.SendLead");
                if (!response.IsSold && string.IsNullOrEmpty(response.Reason))
                {
                    response.Reason = "Exception sending lead";
                }
            }
            return response;
        }

        private static DateTime GetBirthDay(NoProblem.Core.DataModel.Xrm.contact contact)
        {
            DateTime bDay;
            if (contact.birthdate.HasValue)
            {
                bDay = contact.birthdate.Value;
            }
            else
            {
                int yearsBack = random.Next(20, 46);
                int month = random.Next(1, 13);
                int day = random.Next(1, 29);
                bDay = new DateTime(DateTime.Now.Year - yearsBack, month, day);
            }
            return bDay;
        }

        private static bool FindGender(NoProblem.Core.DataModel.Xrm.contact contact)
        {
            bool isFemale = false;
            if (contact.gendercode.HasValue)
            {
                isFemale = contact.gendercode.Value == (int)DataModel.Xrm.contact.GenderCode.FEMALE;
            }
            return isFemale;
        }

        private bool PostLead(string phone, string firstName, string lastName, string email, string token, string externalCode, bool isFemale, DateTime bDay, out string postMessage)
        {
            bool retVal = false;
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            AddParametersToPostLeadRequest(phone, firstName, lastName, email, token, isFemale, bDay, request);
            string url = BuildUrlForLeadPosting(externalCode);
            RestSharp.RestClient client = new RestSharp.RestClient(url);
            request.Resource = createLeadResource;
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            var response = client.Execute<WebForeResponse>(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                postMessage = String.Format("bad http status on post. http status = {0}. content = {1}", response.StatusCode, response.Content);
            }
            else if (response.Data == null)
            {
                postMessage = String.Format("could not parse response on post. http status = {0}. content = {1}", response.StatusCode, response.Content);
            }
            else if (response.Data.isOK)
            {
                postMessage = response.Data.status;
                retVal = true;
            }
            else
            {
                postMessage = String.Format("Error returned in post. status = {0}. status_message = {1}", response.Data.status, response.Data.statusMessage);
            }
            return retVal;
        }

        private string BuildUrlForLeadPosting(string externalCode)
        {
            StringBuilder retVal = new StringBuilder(baseUrl);
            retVal.Append(leadsUrlPrefix);
            if (!String.IsNullOrEmpty(externalCode))
            {
                retVal.Append(externalCode);
            }
            else
            {
                retVal.Append(OTHER_VERTICAL_CODE);
            }
            return retVal.ToString();
        }

        private static void AddParametersToPostLeadRequest(string phone, string firstName, string lastName, string email, string token, bool isFemale, DateTime bDay, RestSharp.RestRequest request)
        {
            request.Parameters.Add(
                new RestSharp.Parameter()
                {
                    Name = ParamNames.ACCESS_TOKEN,
                    Type = RestSharp.ParameterType.GetOrPost,
                    Value = token
                }
                );
            request.Parameters.Add(
                new RestSharp.Parameter()
                {
                    Name = ParamNames.FIRST_NAME,
                    Type = RestSharp.ParameterType.GetOrPost,
                    Value = firstName
                }
                );
            request.Parameters.Add(
                new RestSharp.Parameter()
                {
                    Name = ParamNames.LAST_NAME,
                    Type = RestSharp.ParameterType.GetOrPost,
                    Value = lastName
                }
                );
            request.Parameters.Add(
                new RestSharp.Parameter()
                {
                    Name = ParamNames.COUNTRY_CODE,
                    Type = RestSharp.ParameterType.GetOrPost,
                    Value = US
                }
                );
            request.Parameters.Add(
                new RestSharp.Parameter()
                {
                    Name = ParamNames.EMAIL,
                    Type = RestSharp.ParameterType.GetOrPost,
                    Value = email
                }
                );
            request.Parameters.Add(
                new RestSharp.Parameter()
                {
                    Name = ParamNames.PHONE,
                    Type = RestSharp.ParameterType.GetOrPost,
                    Value = phone
                }
                );
            request.Parameters.Add(
                new RestSharp.Parameter()
                {
                    Name = ParamNames.BIRTH_DATE,
                    Type = RestSharp.ParameterType.GetOrPost,
                    Value = bDay
                }
                );
            request.Parameters.Add(
                new RestSharp.Parameter()
                {
                    Name = ParamNames.GENDER,
                    Type = RestSharp.ParameterType.GetOrPost,
                    Value = (isFemale ? FEMALE : MALE)
                }
                );
        }

        private bool Authenticate(out string authMessage, out string token)
        {
            bool ok = false;
            token = null;
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.Parameters.Add(
                new RestSharp.Parameter()
                {
                    Name = ParamNames.USERNAME,
                    Type = RestSharp.ParameterType.GetOrPost,
                    Value = username
                }
                );
            request.Parameters.Add(
                new RestSharp.Parameter()
                {
                    Name = ParamNames.PASSWORD,
                    Type = RestSharp.ParameterType.GetOrPost,
                    Value = password
                }
                );
            RestSharp.RestClient client = new RestSharp.RestClient(baseUrl);
            request.Resource = authenticationResource;
            var response = client.Execute<WebForeResponse>(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                authMessage = String.Format("bad http status on authentication. http status = {0}. content = {1}", response.StatusCode, response.Content);
            }
            else if (response.Data == null)
            {
                authMessage = String.Format("could not parse response on authentication. http status = {0}. content = {1}", response.StatusCode, response.Content);
            }
            else if (response.Data.isOK)
            {
                authMessage = null;
                token = response.Data.accessToken;
                ok = true;
            }
            else
            {
                authMessage = String.Format("Error returned in authentication. status = {0}. status_message = {1}", response.Data.status, response.Data.statusMessage);
            }
            return ok;
        }

        private class ParamNames
        {
            public const string USERNAME = "username";
            public const string PASSWORD = "password";
            public const string ACCESS_TOKEN = "access_token";
            public const string FIRST_NAME = "data[first_name]";
            public const string LAST_NAME = "data[last_name]";
            public const string PHONE = "data[phone]";
            public const string COUNTRY_CODE = "data[country_code]";
            public const string EMAIL = "data[email]";
            public const string GENDER = "data[gender]";
            public const string BIRTH_DATE = "data[birth_date]";
        }

        private class WebForeResponse
        {
            public String status { get; set; }
            public String statusMessage { get; set; }
            public String accessToken { get; set; }
            public String expiresIn { get; set; }
            public NameValueCollection resource { get; set; }

            public Boolean isOK
            {
                get
                {
                    return status.Equals("SUCCESS");
                }
            }
        }

    }
}
