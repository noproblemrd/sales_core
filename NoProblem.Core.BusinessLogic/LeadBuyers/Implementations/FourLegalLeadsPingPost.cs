﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class FourLegalLeadsPingPost : LeadBuyerBase, IPingPostApiBuyer
    {
        private const string url = "https://leads.4legalleads.com/api.php";
        private const string regionError = "Region is not zip code level.";
        private const string SRC = "NoProblem";
        private const string API_KEY = "naZHtK0LtamQDNm3n26btcj2tan3t59cXcX0tcWJ9afQXRv39K4i95QbOLPw";
        private const string NO = "No";

        public override GetBrokerBidResponse GetBrokerBid(NoProblem.Core.DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                var email = contact.emailaddress1;
                if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
                {
                    retVal.Reason = FNAME_LNAME_ARE_MANDATORY;
                    retVal.WantsToBid = false;
                    return retVal;
                }
                if (string.IsNullOrEmpty(email))
                {
                    retVal.Reason = EMAIL_IS_MANDATORY;
                    retVal.WantsToBid = false;
                    return retVal;
                }
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = regionError;
                    retVal.WantsToBid = false;
                    return retVal;
                }
                if (string.IsNullOrEmpty(incident.description))
                {
                    retVal.Reason = "Description is mandatory";
                    retVal.WantsToBid = false;
                    return retVal;
                }
                string[] regionNameSplit = region.new_englishname.Split(';');
                string zipCode = regionNameSplit[0];

                var pingResult = Ping(
                    new PingData()
                    {
                        Id = incident.incidentid.ToString(),
                        Zip = zipCode,
                        ExternalCode = accExp.new_externalcode,
                        DefaultPrice = ia.new_potentialincidentprice.Value
                    }
                    );
                if (!pingResult.Accepted)
                {
                    //rejected at ping stage
                    retVal.MarkAsRejected = true;
                    retVal.BrokerId = pingResult.PingId;
                    retVal.Reason = "Rejected at PING stage. " + pingResult.PingMessage;
                    retVal.WantsToBid = false;
                }
                else
                {
                    retVal.Price = pingResult.PingPrice;
                    retVal.BrokerId = pingResult.PingId;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in FourLegalLeadsPingPost.GetBrokerBid");
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }

        public override SendLeadResponse SendLead(NoProblem.Core.DataModel.Xrm.incident incident, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.account supplier, NoProblem.Core.DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                var email = contact.emailaddress1;
                var region = incident.new_new_region_incident;
                string[] regionNameSplit = region.new_englishname.Split(';');
                string zipCode = regionNameSplit[0];
                string leadId = pingId;
                var postResult = Post(
                    new PostData()
                    {
                        Id = incident.incidentid.ToString(),
                        Description = incident.description,
                        Zip = zipCode,
                        ExternalCode = accExp.new_externalcode,
                        PingId = leadId,
                        Phone = incident.new_telephone1,
                        Email = email,
                        FirstName = firstName,
                        LastName = lastName
                    }
                    );
                if (!postResult.Accepted)
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = string.Format("Unexpected response at POST stage. Response = {0}", postResult.PostMessage);
                }
                else
                {
                    retVal.BrokerId = string.IsNullOrEmpty(postResult.PostId) ? leadId : postResult.PostId;
                    retVal.IsSold = true;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in FourLegalLeadsPingPost.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private bool ParseResponse(Utils.HttpClient.HttpResponse response, out string message, out string leadId, out decimal price)
        {
            price = -1;
            leadId = null;
            message = null;
            bool retVal = false;
            try
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    XElement xml = XElement.Parse(response.Content);
                    XElement statusElement = xml.Element("status");
                    if (statusElement != null)
                    {
                        string status = statusElement.Value;
                        if (status == "Error")
                        {
                            string error = xml.Element("error").Value;
                            message = string.Format("lead status = {1}. Error message = {0}", error, status);
                        }
                        else
                        {
                            leadId = xml.Element("lead_id").Value;
                            if (status == "Matched")
                            {
                                XElement priceElement = xml.Element("price");
                                if (priceElement != null)
                                {
                                    price = decimal.Parse(priceElement.Value);
                                }
                                retVal = true;
                            }
                            else
                            {
                                message = string.Format("lead status = {0}. leadId = {1}", status, leadId);
                            }
                        }
                    }
                    else//no status node
                    {
                        string error = xml.Element("errors").Element("error").Value;
                        message = string.Format("lead status = Error. Error message = {0}", error);
                    }
                }
                else
                {
                    message = string.Format("Bad http status. httpStatus = {0}. Content = {1}", response.StatusCode, response.Content);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception parsing response in FourLegalLeadsPingPost. Response content = {0}", response.Content);
                message = string.Format("Response could not be parsed. content = {0}", response.Content);
            }
            return retVal;
        }

        private Utils.HttpClient.HttpResponse DoPost(
            string zipCode,
            string externalCode,
            bool isTest,
            string leadId,
            string firstName,
            string lastName,
            string email,
            string phone,
            string description)
        {
            HttpClient client = new HttpClient(url, HttpClient.HttpMethodEnum.Post);
            client.ParamItems.Add("Key", API_KEY);
            client.ParamItems.Add("API_Action", "pingPostLead");
            client.ParamItems.Add("Mode", "post");
            client.ParamItems.Add("TYPE", "31");
            client.ParamItems.Add("SRC", SRC);
            client.ParamItems.Add("Zip", zipCode);
            client.ParamItems.Add("Type_Of_Legal_Problem", externalCode);
            client.ParamItems.Add("First_Name", firstName);
            client.ParamItems.Add("Last_Name", lastName);
            client.ParamItems.Add("Phone", phone);
            client.ParamItems.Add("Best_Time_To_Call", "Anytime");
            client.ParamItems.Add("Landing_Page", "noproblemppc.com");
            client.ParamItems.Add("Lead_ID", leadId);
            client.ParamItems.Add("Email", email);
            client.ParamItems.Add("Comments", description);
            client.ParamItems.Add("TCPA", "Yes");
            if (isTest)
            {
                client.ParamItems.Add("Test_Lead", 1.ToString());
            }
            var ans = client.Post();
            return ans;
        }

        private Utils.HttpClient.HttpResponse DoPing(string zipCode, string externalCode)
        {
            HttpClient client = new HttpClient(url, HttpClient.HttpMethodEnum.Post);
            client.ParamItems.Add("Key", API_KEY);
            client.ParamItems.Add("API_Action", "pingPostLead");
            client.ParamItems.Add("Mode", "ping");
            client.ParamItems.Add("TYPE", "31");
            client.ParamItems.Add("SRC", SRC);
            client.ParamItems.Add("Zip", zipCode);
            client.ParamItems.Add("Type_Of_Legal_Problem", externalCode);
            client.ParamItems.Add("Have_Attorney", NO);
            client.ParamItems.Add("Meso_Question_1", NO);
            client.ParamItems.Add("Meso_Question_2", NO);
            client.ParamItems.Add("Return_Best_Price", 1.ToString());
            client.ParamItems.Add("TCPA", "Yes");
            if (IsTest())
            {
                client.ParamItems.Add("Test_Lead", 1.ToString());
            }
            var ans = client.Post();
            return ans;
        }

        public PingResult Ping(PingData pingData)
        {
            PingResult pingResult = new PingResult();
            Utils.HttpClient.HttpResponse pingResponse = DoPing(pingData.Zip, pingData.ExternalCode);
            string pingMessage;
            string leadId;
            decimal price;
            bool ok = ParseResponse(pingResponse, out pingMessage, out leadId, out price);
            pingResult.PingId = leadId;
            pingResult.PingPrice = price;
            pingResult.PingMessage = pingMessage;
            pingResult.Accepted = ok;
            return pingResult;
        }

        public PostResult Post(PostData postData)
        {
            PostResult result = new PostResult();
            Utils.HttpClient.HttpResponse postRestponse = DoPost(postData.Zip, postData.ExternalCode, IsTest(), postData.PingId, postData.FirstName, postData.LastName, postData.Email, postData.Phone, postData.Description);
            string postMessage;
            string postLeadId;
            decimal price;
            bool postOk = ParseResponse(postRestponse, out postMessage, out postLeadId, out price);
            result.Accepted = postOk;
            result.PostId = postLeadId;
            result.PostMessage = postMessage;
            return result;
        }
    }
}
