﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    [Obsolete("Don't use this. We are using iFrame for this integration.", true)]
    internal class RoundskyPayDayLoans : LeadBuyerBase
    {
        private const string PARTNER = "noproblem";
        private const string PARTNER_PASSWORD = "f3090ae33cb761447d5a";
        private const string urlTest = "http://www.leadhorizon.com/leads/payday/test.php";
        private const string urlLive = "https://www.leadhorizon.com/leads/payday/live.php";

        /*
         * When sending Test leads, to get an approved decision use "approved" for first name; to get declined decision use "declined" for first name.
         */

        private static decimal[] mTiers = new decimal[] { 120, 101, 91, 81, 71, 51, 37, 31, 26, 16, 8 };
        private const int TIER_ONE_INX = 0;

        private DataModel.Xrm.incident mIncident;
        private DataModel.Xrm.new_paydayloandata mPayDayLoanData;
        private DataModel.Xrm.contact mCustomer;

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse bidResponse = new GetBrokerBidResponse();
            try
            {
                mIncident = incident;
                var region = mIncident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    bidResponse.Reason = "Region is not zipcode level.";
                    bidResponse.WantsToBid = false;
                    return bidResponse;
                }
                mPayDayLoanData = incident.new_paydayloandata_incident;
                mCustomer = incident.incident_customer_contacts;
                if (ValidateRequiredFields(bidResponse) && ValidateLeadFilters(bidResponse))
                {
                    bidResponse.IsPredefinedPrice = false;
                    bidResponse.Price = mTiers[TIER_ONE_INX];
                }
                else
                {
                    bidResponse.MarkAsRejected = true;
                    bidResponse.WantsToBid = false;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RoundskyPayDayLoans.GetBrokerBid");
                bidResponse.WantsToBid = false;
                bidResponse.Reason = "Exception getting bid";
            }
            return bidResponse;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse sendReponse = new SendLeadResponse();
            try
            {
                mIncident = incident;
                mPayDayLoanData = incident.new_paydayloandata_incident;
                mCustomer = incident.incident_customer_contacts;
                string url = IsTest() ? urlTest : urlLive;
                int currentTierInx = TIER_ONE_INX;
                RestSharp.RestRequest httpRequest = BuildHttpRequest();
                decimal nextPrice = decimal.Parse(pingId);
                while (currentTierInx < mTiers.Length && mTiers[currentTierInx] > nextPrice)
                {
                    httpRequest.Parameters.First(x => x.Name == "tier").Value = currentTierInx + 1;
                    RestSharp.RestClient httpClient = new RestSharp.RestClient(url);
                    RestSharp.IRestResponse httpResponse = httpClient.Execute(httpRequest);
                    if (httpResponse.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        sendReponse.Reason = "Bad http status received. Status = " + httpResponse.StatusCode;
                        break;
                    }
                    if (String.IsNullOrEmpty(httpResponse.Content))
                    {
                        sendReponse.Reason = "The server response had no valid content";
                    }
                    string content = httpResponse.Content.Trim(new char[] { ' ', '\n', '\r' });
                    string[] split = content.Split(new char[] { '|' });
                    if (split.Length < 5)
                    {
                        sendReponse.Reason = "The server responded with unexpected content. Content = " + content;
                        break;
                    }
                    string decision = split[0];
                    string leadId = split[1];
                    sendReponse.BrokerId = leadId;
                    if (decision == "APPROVED")
                    {
                        sendReponse.IsSold = true;
                        break;
                    }
                    else
                    {
                        string message = split[3];
                        sendReponse.Reason = message;
                    }
                    currentTierInx++;
                }
                if (!sendReponse.IsSold)
                {
                    sendReponse.MarkAsRejected = true;
                }
                else
                {
                    decimal price = mTiers[currentTierInx];
                    ServiceRequest.BiddingManager.UpdateCallPrice(supplier, incAcc, price);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in RoundskyPayDayLoans.SendLead");
                if (!sendReponse.IsSold && string.IsNullOrEmpty(sendReponse.Reason))
                {
                    sendReponse.Reason = "Exception sending lead";
                }
            }
            return sendReponse;
        }

        private RestSharp.RestRequest BuildHttpRequest()
        {
            RestSharp.RestRequest httpRequest = new RestSharp.RestRequest(RestSharp.Method.POST);
            httpRequest.AddParameter("partner", PARTNER);
            httpRequest.AddParameter("partner_password", PARTNER_PASSWORD);
            httpRequest.AddParameter("customer_ip", mIncident.new_ip);
            httpRequest.AddParameter("tier", 1);
            httpRequest.AddParameter("sub_id", mIncident.incidentid);
            httpRequest.AddParameter("domain", "noproblemppc.com");
            httpRequest.AddParameter("time_allowed", 10);
            httpRequest.AddParameter("response_type", "pipe");
            httpRequest.AddParameter("first_name", mCustomer.firstname);
            httpRequest.AddParameter("last_name", mCustomer.lastname);
            httpRequest.AddParameter("email", mCustomer.emailaddress1);
            httpRequest.AddParameter("home_phone", mCustomer.telephone1);
            string city, state;
            base.ExtractZipcodeInfo(mIncident.new_new_region_incident, out city, out state);
            httpRequest.AddParameter("state", state);
            httpRequest.AddParameter("zip", mIncident.new_new_region_incident.new_name);
            httpRequest.AddParameter("address", mCustomer.address1_line1);
            httpRequest.AddParameter("city", city);
            httpRequest.AddParameter("housing", mPayDayLoanData.new_isrentaddress.Value ? "rent" : "own");
            httpRequest.AddParameter("monthly_income", mPayDayLoanData.new_monthlynetincome.Value);
            httpRequest.AddParameter("account_type", "checking");
            httpRequest.AddParameter("direct_deposit", mPayDayLoanData.new_directdeposit.Value ? "true" : "false");
            httpRequest.AddParameter("pay_period", GetPayPeriod());
            httpRequest.AddParameter("next_pay_date", mPayDayLoanData.new_nextpayday.Value.ToString("yyyy-MM-dd"));
            if (mPayDayLoanData.new_secondpayday.HasValue && IsSecondPayDayValid(mPayDayLoanData.new_nextpayday.Value, mPayDayLoanData.new_secondpayday.Value))
            {
                httpRequest.AddParameter("second_pay_date", mPayDayLoanData.new_secondpayday.Value.ToString("yyyy-MM-dd"));
            }
            httpRequest.AddParameter("requested_loan_amount", mPayDayLoanData.new_loanamount.Value);
            httpRequest.AddParameter("months_at_residence", mPayDayLoanData.new_legthataddress);
            httpRequest.AddParameter("income_type", mPayDayLoanData.new_incomesource.Value == (int)DataModel.Xrm.new_paydayloandata.pdlIncomeSource.Benefit ? "benefits" : "employment");
            httpRequest.AddParameter("active_military", "false");
            if (!String.IsNullOrEmpty(mPayDayLoanData.new_jobtitle))
                httpRequest.AddParameter("occupation", mPayDayLoanData.new_jobtitle);
            httpRequest.AddParameter("work_phone", mPayDayLoanData.new_workphone);
            httpRequest.AddParameter("months_employed", mPayDayLoanData.new_employedmonths);
            httpRequest.AddParameter("bank_name", mPayDayLoanData.new_bankname);
            httpRequest.AddParameter("account_number", mPayDayLoanData.new_bankaccountnumber);
            httpRequest.AddParameter("routing_number", mPayDayLoanData.new_abarounting);
            httpRequest.AddParameter("months_with_bank", mPayDayLoanData.new_monthswithbank);
            httpRequest.AddParameter("driving_license_state", mPayDayLoanData.new_driverlicensestate);
            httpRequest.AddParameter("driving_license_number", mPayDayLoanData.new_driverlicense);
            httpRequest.AddParameter("birth_date", mCustomer.birthdate.Value.ToString("yyyy-MM-dd"));
            httpRequest.AddParameter("social_security_number", mPayDayLoanData.new_ssn);
            return httpRequest;
        }

        private bool IsSecondPayDayValid(DateTime nextPayDay, DateTime secondPayDay)
        {
            if ((secondPayDay.Date - nextPayDay.Date).Days < 7
                || secondPayDay.DayOfWeek == DayOfWeek.Saturday
                || secondPayDay.DayOfWeek == DayOfWeek.Sunday)
            {
                return false;
            }
            return true;
        }

        private string GetPayPeriod()
        {
            switch (mPayDayLoanData.new_payfrequency.Value)
            {
                case (int)DataModel.Xrm.new_paydayloandata.pdlPayFrequency.Biweekly:
                    return "biweekly";
                case (int)DataModel.Xrm.new_paydayloandata.pdlPayFrequency.Monthly:
                    return "monthly";
                case (int)DataModel.Xrm.new_paydayloandata.pdlPayFrequency.TwiceMonthly:
                    return "twice monthly";
                case (int)DataModel.Xrm.new_paydayloandata.pdlPayFrequency.Weekly:
                    return "weekly";
                default:
                    return "monthly";
            }
        }

        private bool ValidateLeadFilters(GetBrokerBidResponse bidResponse)
        {
            if (mPayDayLoanData.new_accounttype.Value == (int)DataModel.Xrm.new_paydayloandata.pdlAccountType.Saving)
            {
                AddFilterFailure("Only checking accounts are accepted", bidResponse);
                return false;
            }
            if (mPayDayLoanData.new_activemilitary.Value)
            {
                AddFilterFailure("No military is accepted", bidResponse);
                return false;
            }
            int age = CalcuateAge();
            if (age < 20 || age > 65)
            {
                AddFilterFailure("Ages 20 to 65 are accepted only", bidResponse);
                return false;
            }
            if (mPayDayLoanData.new_monthlynetincome.Value < 1200 || mPayDayLoanData.new_monthlynetincome.Value > 10000)
            {
                AddFilterFailure("Monthly income 1,200 to 10,000 is accepted only", bidResponse);
                return false;
            }
            if (mPayDayLoanData.new_workphone == mIncident.new_telephone1)
            {
                AddFilterFailure("The two phone numbers given cannot be the same", bidResponse);
                return false;
            }
            if (mCustomer.address1_line1.ToLower().Contains("po box") || mCustomer.address1_line1.ToLower().Contains("p.o. box"))
            {
                AddFilterFailure("The address cannot contain P.O. BOX", bidResponse);
                return false;
            }
            if (!CheckEmployerName())
            {
                AddFilterFailure("Employer cannot be a government agency or unemployed", bidResponse);
                return false;
            }
            if (mPayDayLoanData.new_nextpayday.Value.DayOfWeek == DayOfWeek.Saturday
                || mPayDayLoanData.new_nextpayday.Value.DayOfWeek == DayOfWeek.Sunday)
            {
                AddFilterFailure("Next pay day cannot be Saturday nor Sunday", bidResponse);
                return false;
            }
            if (mCustomer.emailaddress1.EndsWith("gov") || mCustomer.emailaddress1.EndsWith("mil"))
            {
                AddFilterFailure("Email cannot end with gov or mil", bidResponse);
                return false;
            }
            return true;
        }

        private bool CheckEmployerName()
        {
            string employerName = mPayDayLoanData.new_employername.ToLower();
            if (
                employerName.Equals("military")
                || employerName.Equals("attorney general")
                || employerName.Equals("air reserve")
                || employerName.Equals("air force")
                || employerName.Equals("army")
                || employerName.Equals("reserve")
                || employerName.Equals("usmc")
                || employerName.Equals("rotc")
                || employerName.Equals("student")
                || employerName.Equals("temp")
                || employerName.Equals("unemployed")
                || employerName.Equals("unemployment")
                )
            {
                return false;
            }
            return true;
        }

        private int CalcuateAge()
        {
            DateTime today = DateTime.Today;
            int age = today.Year - mCustomer.birthdate.Value.Year;
            if (mCustomer.birthdate.Value > today.AddYears(-age))
                age--;
            return age;
        }

        private void AddFilterFailure(string txt, GetBrokerBidResponse bidResponse)
        {
            bidResponse.Reason = txt;
            bidResponse.FailedInnerValidation = true;
        }

        private bool ValidateRequiredFields(GetBrokerBidResponse bidResponse)
        {
            if (mPayDayLoanData == null)
            {
                bidResponse.Reason = "Pay day loan specific fields were not found in lead.";
                bidResponse.FailedInnerValidation = true;
                return false;
            }
            Dictionary<string, string> fieldsToCheck = new Dictionary<string, string>();
            fieldsToCheck.Add("ip", mIncident.new_ip);
            fieldsToCheck.Add("first name", mCustomer.firstname);
            fieldsToCheck.Add("last name", mCustomer.lastname);
            fieldsToCheck.Add("email", mCustomer.emailaddress1);
            fieldsToCheck.Add("address", mCustomer.address1_line1);
            fieldsToCheck.Add("employer", mPayDayLoanData.new_employername);
            fieldsToCheck.Add("work phone", mPayDayLoanData.new_workphone);
            fieldsToCheck.Add("bank name", mPayDayLoanData.new_bankname);
            fieldsToCheck.Add("account number", mPayDayLoanData.new_bankaccountnumber);
            fieldsToCheck.Add("routing number", mPayDayLoanData.new_abarounting);
            fieldsToCheck.Add("driving license state", mPayDayLoanData.new_driverlicensestate);
            fieldsToCheck.Add("driving license number", mPayDayLoanData.new_driverlicense);
            fieldsToCheck.Add("social security number", mPayDayLoanData.new_ssn);

            foreach (var item in fieldsToCheck)
            {
                if (String.IsNullOrEmpty(item.Value))
                {
                    AddRequiredValidationFailure(item.Key, bidResponse);
                    return false;
                }
            }

            if (!mPayDayLoanData.new_isrentaddress.HasValue)
            {
                AddRequiredValidationFailure("Rent/Own", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_monthlynetincome.HasValue)
            {
                AddRequiredValidationFailure("Monthly income", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_accounttype.HasValue)
            {
                AddRequiredValidationFailure("Account type", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_directdeposit.HasValue)
            {
                AddRequiredValidationFailure("Direct deposit", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_payfrequency.HasValue)
            {
                AddRequiredValidationFailure("Pay period", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_nextpayday.HasValue)
            {
                AddRequiredValidationFailure("Next pay day", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_loanamount.HasValue)
            {
                AddRequiredValidationFailure("Loan amount", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_legthataddress.HasValue)
            {
                AddRequiredValidationFailure("Months at residence", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_incomesource.HasValue)
            {
                AddRequiredValidationFailure("Income type", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_activemilitary.HasValue)
            {
                AddRequiredValidationFailure("Active military", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_employedmonths.HasValue)
            {
                AddRequiredValidationFailure("Months employed", bidResponse);
                return false;
            }
            if (!mPayDayLoanData.new_monthswithbank.HasValue)
            {
                AddRequiredValidationFailure("Months with bank", bidResponse);
                return false;
            }
            if (!mCustomer.birthdate.HasValue)
            {
                AddRequiredValidationFailure("Birth date", bidResponse);
                return false;
            }

            return true;
        }

        private void AddRequiredValidationFailure(string name, GetBrokerBidResponse bidResponse)
        {
            bidResponse.FailedInnerValidation = true;
            bidResponse.Reason = name + " is mandatory.";
        }
    }
}
