﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class FourLegalLeadsWithZipCodeCheck : LeadBuyerBase
    {
        private const string regionError = "Region is not zip code level.";

        #region ILeadBuyer Members

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = regionError;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                if (CheckZipCodeOk(region.new_name, accExp.new_accountexpertiseid))
                {
                    FourLegalLeads fourLegalLeads = new FourLegalLeads();
                    retVal = fourLegalLeads.SendLead(incident, accExp, supplier, incAcc, pingId);
                }
                else
                {
                    retVal.Reason = "Zipcode not in broker's list";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in FourLegalLeadsWithZipCodeCheck.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private bool CheckZipCodeOk(string zipcode, Guid accExpId)
        {
            DataAccessLayer.AccountExpertiseZipCode dal = new NoProblem.Core.DataAccessLayer.AccountExpertiseZipCode(null);
            DataAccessLayer.AccountExpertiseZipCode.SqlParametersList parameters = new NoProblem.Core.DataAccessLayer.DalBase<NoProblem.Core.DataModel.Xrm.new_accountexpertisezipcode>.SqlParametersList();
            parameters.Add("@accExpId", accExpId);
            parameters.Add("@zipCode", zipcode);
            object scalar = dal.ExecuteScalar(zipCodeQuery, parameters);
            bool retVal = (scalar != null);
            return retVal; ;
        }

        private const string zipCodeQuery =
            @"select top 1 1 from new_accountexpertisezipcode with(nolock)
                where deletionstatecode = 0 and new_accountexpertiseid = @accExpId and new_name = @zipCode";

        #endregion
    }
}
