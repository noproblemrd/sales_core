﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class Tester : LeadBuyerBase
    {
        #region ILeadBuyer Members

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            return new SendLeadResponse()
            {
                BrokerId = "test sold",
                IsSold = true,
            };
        }

        #endregion
    }
}
