﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class Networx : LeadBuyerBase, IPingPostApiBuyer
    {
        private const string baseUrl = "https://api.networx.com";
        private const string nxAccessKey = "67ac390069";
        private const string nxUserId = "9462";
        private const string testZipcode = "96822";

        private static class NetworxParamNames
        {
            internal const string NX_ACCESS_KEY = "nx_access_key";
            internal const string NX_USERID = "nx_userId";
            internal const string F_NAME = "f_name";
            internal const string L_NAME = "l_name";
            internal const string ZIPCODE = "zipcode";
            internal const string TASK_ID = "task_id";
            internal const string PHONE = "phone";
            internal const string EMAIL = "email";
            internal const string COMMENTS = "comments";
            internal const string CUSTOM_ID = "custom_id";
            internal const string TOKEN = "token";
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            var region = incident.new_new_region_incident;
            if (region.new_level != 4)
            {
                retVal.Reason = "Region is not zip code level.";
                retVal.FailedInnerValidation = true;
                return retVal;
            }
            string zipcode = region.new_name;
            if (IsTest())
            {
                zipcode = testZipcode;
            }
            string taksId = accExp.new_externalcode;
            if (string.IsNullOrEmpty(taksId))
            {
                retVal.Reason = "No external code mapping for heading.";
                retVal.FailedInnerValidation = true;
                return retVal;
            }
            var fname = incident.incident_customer_contacts.firstname;
            var lname = incident.incident_customer_contacts.lastname;
            if (string.IsNullOrEmpty(fname) && string.IsNullOrEmpty(lname))
            {
                retVal.Reason = "Name is mandatory";
                retVal.FailedInnerValidation = true;
                return retVal;
            }
            if (string.IsNullOrEmpty(fname))
            {
                fname = lname;
                lname = string.Empty;
            }

            var pingResult = Ping(
                new PingData()
                {
                    Id = incident.incidentid.ToString(),
                    Zip = zipcode,
                    ExternalCode = taksId,
                    DefaultPrice = incAcc.new_potentialincidentprice.Value
                });
            if (pingResult.Accepted)
            {
                var postResult = Post(
                    new PostData()
                    {
                        Id = incident.incidentid.ToString(),
                        Description = incident.description,
                        Zip = zipcode,
                        ExternalCode = taksId,
                        PingId = pingResult.PingId,
                        Phone = incident.new_telephone1,
                        Email = incident.incident_customer_contacts.emailaddress1,
                        FirstName = fname,
                        LastName = lname
                    }
                    );
                retVal.IsSold = postResult.Accepted;
                if (retVal.IsSold)
                {
                    retVal.BrokerId = postResult.PostId;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = postResult.PostMessage;
                }
            }
            else
            {
                retVal.Reason = pingResult.PingMessage;
            }

            return retVal;
        }

        public PostResult Post(PostData postData)
        {
            PostResult result = new PostResult();
            string description = postData.Description;
            if (description == null)
                description = string.Empty;
            string email = postData.Email;
            if (email == null)
                email = string.Empty;
            Utils.HttpClient ps = new Utils.HttpClient(baseUrl);
            ps.ParamItems.Add(NetworxParamNames.NX_ACCESS_KEY, nxAccessKey);
            ps.ParamItems.Add(NetworxParamNames.NX_USERID, nxUserId);
            ps.ParamItems.Add(NetworxParamNames.F_NAME, postData.FirstName);
            ps.ParamItems.Add(NetworxParamNames.L_NAME, postData.LastName);
            ps.ParamItems.Add(NetworxParamNames.ZIPCODE, postData.Zip);
            ps.ParamItems.Add(NetworxParamNames.TASK_ID, postData.ExternalCode);
            ps.ParamItems.Add(NetworxParamNames.PHONE, postData.Phone);
            ps.ParamItems.Add(NetworxParamNames.EMAIL, email);
            ps.ParamItems.Add(NetworxParamNames.COMMENTS, description);
            ps.ParamItems.Add(NetworxParamNames.CUSTOM_ID, postData.Id);
            ps.ParamItems.Add(NetworxParamNames.TOKEN, postData.PingId);
            ps.HttpMethod = Utils.HttpClient.HttpMethodEnum.Post;
            Utils.HttpClient.HttpResponse response = ps.Post();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                try
                {
                    XElement xel = XElement.Parse(response.Content);
                    XElement successCodeXel = xel.Element("successCode");
                    result.PostId = successCodeXel.Value;
                    result.Accepted = true;
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to parse XML from Networx in post stage after http code 200.");
                    result.PostMessage = "Failed to parse XML from Networx in post stage after http code 200.";
                }
            }
            else
            {
                result.PostMessage = string.Format("Networx returned bad HTTP response in post stage. httpStatuCode = {0}. content: {1}", response.StatusCode, response.Content);
            }
            return result;
        }

        public PingResult Ping(PingData pingData)
        {
            PingResult result = new PingResult();
            result.PingPrice = pingData.DefaultPrice;
            Utils.HttpClient ping = new NoProblem.Core.BusinessLogic.Utils.HttpClient(baseUrl);
            ping.ParamItems.Add(NetworxParamNames.NX_ACCESS_KEY, nxAccessKey);
            ping.ParamItems.Add(NetworxParamNames.NX_USERID, nxUserId);
            ping.ParamItems.Add(NetworxParamNames.ZIPCODE, pingData.Zip);
            ping.ParamItems.Add(NetworxParamNames.TASK_ID, pingData.ExternalCode);
            ping.HttpMethod = Utils.HttpClient.HttpMethodEnum.Post;
            Utils.HttpClient.HttpResponse pingResponse = ping.Post();
            if (pingResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                try
                {
                    XElement xel = XElement.Parse(pingResponse.Content);
                    XElement tokenXel = xel.Element("token");
                    string token = tokenXel.Value;
                    result.PingId = token;
                    result.Accepted = true;
                }
                catch (Exception exc)
                {
                    LogUtils.MyHandle.HandleException(exc, "Failed to parse XML from Networx in ping stage. httpStatuCode = {0}. content: {1}", pingResponse.StatusCode, pingResponse.Content);
                    result.PingMessage = string.Format("Failed to parse XML in ping stage. httpStatuCode = {0}. content: {1}", pingResponse.StatusCode, pingResponse.Content);
                }
            }
            else
            {
                result.PingMessage = "Rejected at ping stage. HttpStatusCode = " + pingResponse.StatusCode + " Content = " + pingResponse.Content;
            }
            return result;
        }
    }
}
