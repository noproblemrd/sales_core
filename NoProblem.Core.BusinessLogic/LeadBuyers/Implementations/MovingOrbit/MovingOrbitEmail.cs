﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal abstract class MovingOrbitEmail : MovingOrbit
    {
        protected override bool DoSendAction(string movigOrbitsEmails, string zipcode, string moveToZipcode, string firstName, string lastName, string phone, DateTime moveDate, string moveSize, string moveType, string email, string ip, out string reason)
        {
            reason = null;
            Dictionary<string, string> mergeVars = new Dictionary<string, string>();
            mergeVars.Add("NPVAR_ZIPCODE", zipcode);
            mergeVars.Add("NPVAR_MOVETOZIPCODE", moveToZipcode);
            mergeVars.Add("NPVAR_FNAME", firstName);
            mergeVars.Add("NPVAR_LNAME", lastName);
            mergeVars.Add("NPVAR_PHONE", phone);
            mergeVars.Add("NPVAR_DATE", moveDate.ToString("MM/dd/yyyy"));
            mergeVars.Add("NPVAR_SIZE", moveSize);
            mergeVars.Add("NPVAR_TYPE", moveType);
            mergeVars.Add("NPVAR_EMAIL", email);
            mergeVars.Add("NPVAR_ISTEST", IsTest() ? "yes" : "no");

            MandrillSender sender = new MandrillSender();
            bool sent = sender.SendTemplate(movigOrbitsEmails, "New Moving Lead From NoProblemPPC", "Lead_To_Broker_MovingOrbit", mergeVars);
            if (!sent)
                reason = "Failed to send email, check logs.";
            return sent;
        }
    }
}
