﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations
{
    internal class MovingOrbitLongEmail : MovingOrbitEmail
    {
        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            return base.GetBrokerBid(incident, accExp, ia, true);
        }
    }
}
