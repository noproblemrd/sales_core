﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal abstract class MovingOrbitApi : MovingOrbit
    {
        private const string URL = "http://www.movingorbit.us/do/post";
        private const string AFF = "";

        protected override bool DoSendAction(string movigOrbitsEmails, string zipcode, string moveToZipcode, string firstName, string lastName, string phone, DateTime moveDate, string moveSize, string moveType, string email, string ip, out string reason)
        {
            RestSharp.RestClient client = new RestSharp.RestClient(URL);
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.AddParameter("dispatch", "addlead");
            request.AddParameter("first", firstName);
            request.AddParameter("last", lastName);
            request.AddParameter("zip", zipcode);
            request.AddParameter("city", "");
            request.AddParameter("state", "");
            request.AddParameter("address", "");
            request.AddParameter("country", "");
            request.AddParameter("movingAddress", "");
            request.AddParameter("movingZip", moveToZipcode);
            request.AddParameter("movingCity", "");
            request.AddParameter("movingState", "");
            request.AddParameter("movingCountry", "");
            request.AddParameter("phone", phone);
            request.AddParameter("phoneAlt", "");
            request.AddParameter("autoshipping", "no");
            request.AddParameter("model", "");
            request.AddParameter("modelType", "");
            request.AddParameter("carType", "");
            request.AddParameter("modelStatus", "");
            request.AddParameter("modelYear", "");
            request.AddParameter("domain", "noproblemppc.com");
            request.AddParameter("date", moveDate.ToString("yyyy-MM-dd"));
            request.AddParameter("sizeofmove", moveSize);
            request.AddParameter("ipaddress", ip);
            request.AddParameter("email", email);
            request.AddParameter("type", moveType);
            request.AddParameter("aff", AFF);
            request.AddParameter("aff2", "");
            request.AddParameter("extRef", "0");
            request.AddParameter("returnURL", "");
            request.AddParameter("test", IsTest() ? "true" : "false");
            var response = client.Execute(request);
            bool sent = false;
            reason = null;
            if (!String.IsNullOrEmpty(response.Content))
            {
                int idx = response.Content.IndexOf("SUCCESS");
                if (idx > -1)
                {
                    sent = true;
                    reason = response.Content.Substring(idx);
                    if (reason.Length > 50)
                        reason = reason.Substring(0, 50);
                }
            }
            if (!sent)
                reason = response.Content;
            return sent;
        }
    }
}

/*
response exampble:
Lead: beans.Lead@49be92cb[id=0,name=Alain Alder,address=,city=,state=,zip=10019,movingAddress=,movingCity=,movingState=,movingZip=90002,phone=2011234567,phoneAlt=,message=,autoshipping=no,model=,year=,modelType=,modelStatus=,domain=noproblemppc.com,movingDate=<null>,date=<null>,ipaddress=194.90.222.218,email=alain@noproblemppc.com,type=residential,status=<null>,amountSold=0,access=4 Bdr, 9800lbs/1400 Cf,referer=,movingCountry=,country=,leadType=,referenceId=0,carType=,process=<null>,externalReference=0,estimatedTime=,estimateSize=,storageReason=,storageContainer=,storageType=,affiliateId=,affiliateId2=,reason=<null>,owner=movingorbit,vendor=<null>,vendorRef=<null>,optin=<null>,ivrCalls=0,callSid=<null>,ivrStatus=<null>]
SUCCESS:1418196037386
*/
