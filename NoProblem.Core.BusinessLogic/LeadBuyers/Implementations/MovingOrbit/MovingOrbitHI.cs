﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations
{
    internal class MovingOrbitHI : LeadBuyerBase
    {
        private const string URL = "https://admin.professionals.info/apiXML.php";

        private const string pingXml =
            @"<?xml version=""1.0""?>
<Request>
	<Key>rgmB2uva2uF72un_27SWR76WZ39i8uSW8gn_24ZN8uvjZk9BXN-K23Vj</Key>
	<API_Action>pingPostLead</API_Action>
	<Mode>ping</Mode>
	<TYPE>19</TYPE>
	<Test_Lead>$IS_TEST</Test_Lead>
    <Return_Best_Price>1</Return_Best_Price>
	<Data>
		<TCPA>Yes</TCPA>
		<SRC>Aff110</SRC>
		<Landing_Page>noproblemppc</Landing_Page>
		<Zip>$ZIP</Zip>
		<Phone_Type>mobile</Phone_Type>
		<Homeowner>Yes</Homeowner>
		<Material>NA</Material>
		<Number_of_Windows>NA</Number_of_Windows>
		<Consent>Yes</Consent>
		<Trade>$CATEGORY</Trade>
	</Data>
</Request>";

        private const string postXml =
            @"<?xml version=""1.0""?>
<Request>
	<Key>rgmB2uva2uF72un_27SWR76WZ39i8uSW8gn_24ZN8uvjZk9BXN-K23Vj</Key>
	<API_Action>pingPostLead</API_Action>
	<Mode>post</Mode>
	<Lead_ID>$LEAD_ID</Lead_ID>	
	<TYPE>19</TYPE>
	<Test_Lead>$IS_TEST</Test_Lead>	
	<Data>
		<IP_Address>$IP_ADDRESS</IP_Address>
		<First_Name>$FIRST_NAME</First_Name>
		<Last_Name>$LAST_NAME</Last_Name>
		<Address>$STREET</Address>
		<City>$CITY</City>
		<Primary_Phone>$PHONE</Primary_Phone>
		<Email>$EMAIL</Email>
	</Data>
</Request>";

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            ValidateAddress(incident);
            ValidateEmail(incident);
            ValidateFirstAndLastName(incident);
            ValidateIp(incident);
            ValidateRegion(incident);
        }

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
             GetBrokerBidResponse retVal = new GetBrokerBidResponse();
             try
             {
                 if (!Validate(incident))
                 {
                     retVal.Reason = validationMessage;
                     retVal.FailedInnerValidation = true;
                     retVal.WantsToBid = false;
                     return retVal;
                 }
                 string xmlData = pingXml
                     .Replace("$ZIP", incident.new_new_region_incident.new_name)
                     .Replace("$IS_TEST", IsTest() ? "1" : "0")
                     .Replace("$CATEGORY", accExp.new_externalcode);
                 RestSharp.IRestClient client = new RestSharp.RestClient(URL);
                 RestSharp.IRestRequest req = new RestSharp.RestRequest(RestSharp.Method.POST);
                 req.AddParameter("text/xml", xmlData, RestSharp.ParameterType.RequestBody);
                 var res = client.Execute(req);
                 XElement responseXml = XElement.Parse(res.Content);
                 string status = responseXml.Element("status").Value;
                 if (status == "Matched")
                 {
                     retVal.BrokerId = responseXml.Element("lead_id").Value;
                     retVal.Price = decimal.Parse(responseXml.Element("price").Value);
                 }
                 else
                 {
                     retVal.WantsToBid = false;
                     retVal.MarkAsRejected = false;
                     retVal.Reason = String.Format("Rejected at ping with status '{0}'", status);
                 }
             }
             catch (Exception exc)
             {
                 LogUtils.MyHandle.HandleException(exc, "Exception in MovingOrbitHI.GetBrokerBid");
                 retVal.WantsToBid = false;
                 retVal.Reason = "Exception getting bid";
             }
             return retVal;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                string city, state;
                ExtractZipcodeInfo(incident.new_new_region_incident, out city, out state);
                string xmlData = postXml
                         .Replace("$LEAD_ID", pingId)
                         .Replace("$IS_TEST", IsTest() ? "1" : "0")
                         .Replace("$IP_ADDRESS", incident.new_ip)
                         .Replace("$FIRST_NAME", contact.firstname)
                         .Replace("$LAST_NAME", contact.lastname)
                         .Replace("$STREET", contact.address1_line1)
                         .Replace("$CITY", city)
                         .Replace("$PHONE", incident.new_telephone1)
                         .Replace("$EMAIL", contact.emailaddress1);
                RestSharp.IRestClient client = new RestSharp.RestClient(URL);
                RestSharp.IRestRequest req = new RestSharp.RestRequest(RestSharp.Method.POST);
                req.AddParameter("text/xml", xmlData, RestSharp.ParameterType.RequestBody);
                var res = client.Execute(req);
                XElement responseXml = XElement.Parse(res.Content);
                string status = responseXml.Element("status").Value;
                if (status == "Matched")
                {
                    retVal.IsSold = true;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = String.Format("Rejected at POST with status '{0}'", status);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in MovingOrbitHI.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }
    }
}
