﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal abstract class MovingOrbit : LeadBuyerBase
    {
        private const string regionError = "Region is not zip code level.";

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                if (lastName == null)
                    lastName = String.Empty;
                var email = contact.emailaddress1;
                if (string.IsNullOrEmpty(firstName))
                {
                    retVal.Reason = "FirstName is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                if (string.IsNullOrEmpty(email))
                {
                    retVal.Reason = "Email is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                if (string.IsNullOrEmpty(incident.new_movetozipcode))
                {
                    retVal.Reason = "Moving to zip code is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                var moveToRegion = regDal.GetRegionByName(incident.new_movetozipcode);
                if (moveToRegion == null)
                {
                    retVal.Reason = "Moving to zip code is invalid";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string[] moveToSplit = moveToRegion.new_englishname.Split(';');
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = regionError;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string zipcode = region.new_name;

                if (!incident.new_movedate.HasValue)
                {
                    retVal.Reason = "Move date is mandatory";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                string moveSize;
                switch (incident.new_movesize)
                {
                    case "studio":
                        moveSize = "Studio, 2000lbs/285 Cf";
                        break;
                    case "oneBdr":
                        moveSize = "1 Bdr, 2940lbs/420 C";
                        break;
                    case "twoBdr":
                        moveSize = "2 Bdr, 4550lbs/650 Cf";
                        break;
                    case "threeBdr":
                        moveSize = "3 Bdr, 7700lbs/1100 Cf";
                        break;
                    case "fourBdr":
                        moveSize = "4 Bdr, 9800lbs/1400 Cf";
                        break;
                    case "fiveBdr":
                        moveSize = "5 Bdr, 11200lbs/1600 C";
                        break;
                    case "larger":
                        moveSize = "Over 11200lbs/1600 Cf";
                        break;
                    default:
                        retVal.Reason = "Move size is mandatory";
                        retVal.FailedInnerValidation = true;
                        return retVal;
                }

                string moveType;
                switch (incident.new_movetype)
                {
                    case "residential":
                        moveType = "residential";
                        break;
                    case "office":
                        moveType = "office";
                        break;
                    default:
                        retVal.Reason = "Move types accepted are residential and office only";
                        retVal.FailedInnerValidation = true;
                        return retVal;
                }

                string isTest = IsTest() ? "yes" : "no";

                string reason;
                bool sent = DoSendAction(supplier.emailaddress1, zipcode, moveToRegion.new_name, firstName, lastName, incident.new_telephone1, incident.new_movedate.Value, moveSize, moveType, email, incident.new_ip, out reason);

                if (sent)
                {
                    retVal.IsSold = true;
                    retVal.BrokerId = reason;
                }
                else
                {
                    retVal.FailedInnerValidation = true;
                    retVal.Reason = reason;
                    retVal.MarkAsRejected = true;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in MovingOrbit.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        protected abstract bool DoSendAction(string movigOrbitsEmails, string zipcode, string moveToZipcode, string firstName, string lastName, string phone, DateTime moveDate, string moveSize, string moveType, string email, string ip, out string reason);

        public GetBrokerBidResponse GetBrokerBid(NoProblem.Core.DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, NoProblem.Core.DataModel.Xrm.new_incidentaccount ia, bool isAccountLongDistance)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                var moveToRegion = regDal.GetRegionByName(incident.new_movetozipcode);
                if (moveToRegion == null)
                {
                    retVal.WantsToBid = false;
                    retVal.Reason = "Move to zipcode is mandatory";
                    return retVal;
                }
                string[] moveToSplit = moveToRegion.new_englishname.Split(';');
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.WantsToBid = false;
                    retVal.Reason = regionError;
                    return retVal;
                }
                string zipcode = region.new_name;
                string[] regionNameSplit = region.new_englishname.Split(';');
                string stateFrom = regionNameSplit[3];
                bool leadLongDistance = stateFrom != moveToSplit[3];
                if (
                    (isAccountLongDistance && leadLongDistance)
                    || (!isAccountLongDistance && !leadLongDistance)
                    )
                {
                    retVal = base.GetBrokerBid(incident, accExp, ia);
                }
                else
                {
                    retVal.Reason = "Not the distance for this account.";
                    retVal.WantsToBid = false;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in MovingOrbit.GetBrokerBid");
                if (retVal == null)
                    retVal = new GetBrokerBidResponse();
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }
    }
}
