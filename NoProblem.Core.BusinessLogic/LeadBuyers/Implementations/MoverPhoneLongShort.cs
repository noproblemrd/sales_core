﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class MoverPhoneLongShort : PhoneConnection
    {
        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                MoverValidator validator = new MoverValidator();
                string moverValidationMessage;
                bool valid = validator.Validate(incident, out moverValidationMessage);
                if (!valid)
                {
                    retVal.WantsToBid = false;
                    retVal.Reason = moverValidationMessage;
                    return retVal;
                }

                bool isLongDistance = IsLongDistance(incident);
                if (isLongDistance)//compare states
                {
                    //is long distance
                    retVal.Price = GetLongDistancePrice(accExp);
                }
                else
                {
                    //local
                    retVal.Price = GetShortDistancePrice(accExp);
                }
                if (retVal.Price <= 0)
                {
                    retVal.WantsToBid = false;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Pay4PerformanceMoving.MoverPhoneLongShort");
                if (retVal == null)
                    retVal = new GetBrokerBidResponse();
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }

        private decimal GetLongDistancePrice(DataModel.Xrm.new_accountexpertise accExp)
        {
            return GetPrices(accExp).Long;
        }

        private PricesJson GetPrices(DataModel.Xrm.new_accountexpertise accExp)
        {
            PricesJson prices = Newtonsoft.Json.JsonConvert.DeserializeObject<PricesJson>(accExp.new_externalcode);
            return prices;
        }

        private decimal GetShortDistancePrice(DataModel.Xrm.new_accountexpertise accExp)
        {
            return GetPrices(accExp).Short;
        }

        private bool IsLongDistance(DataModel.Xrm.incident incident)
        {
            DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
            var moveToRegion = regDal.GetRegionByName(incident.new_movetozipcode);
            string[] moveToSplit = moveToRegion.new_englishname.Split(';');
            var region = incident.new_new_region_incident;
            string zipcode = region.new_name;
            string[] regionNameSplit = region.new_englishname.Split(';');
            string stateFrom = regionNameSplit[3];
            return stateFrom != moveToSplit[3];
        }

        private class PricesJson
        {
            public decimal Long { get; set; }
            public decimal Short { get; set; }
        }

        private class MoverValidator : LeadBuyerMoverBase
        {
            public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
            {
                throw new NotImplementedException();
            }

            public bool Validate(DataModel.Xrm.incident incident, out string message)
            {
                bool valid = base.Validate(incident);
                message = validationMessage;
                return valid;
            }

            protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
            {
                ValidateRegion(incident);
                ValidateMoveToZipcode(incident);
            }
        }
    }
}
