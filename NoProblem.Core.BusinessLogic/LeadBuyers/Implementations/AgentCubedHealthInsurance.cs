﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;
using System.Security;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class AgentCubedHealthInsurance : LeadBuyerWithZipCodeCheckBase
    {
        private const string BASE_URL = "https://dataexchange.agentcubed.com/PortalService/PortalService.asmx";
        private const string RESOURCE = "AddLeadsUsingXMLString";
        private const string TEST = "TEST";
        private const string DATE_FORMAT = "yyyy-MM-ddTHH:mm:ss";
        private const string LEADSOURCEKEY_PROD = "b44b19ab-db7a-4871-84e8-0a854a99e2c7";
        private const string LEADSOURCEKEY_TEST = "9eca05e2-7cd3-4fba-9f65-f2b175cf7c59";
        private static readonly Dictionary<string, string> insuranceTypesMap;
        private static readonly XNamespace ns = "http://dataexchange.agentcubed.com";

        static AgentCubedHealthInsurance()
        {
            insuranceTypesMap = new Dictionary<string, string>();
            insuranceTypesMap.Add("Individual & family health insurance", "Health");
            insuranceTypesMap.Add("Critical illness insurance", "Critical Illness");
            insuranceTypesMap.Add("Accident medical insurance", "Accident");
            insuranceTypesMap.Add("Short term health insurance", "Short Term Health");
        }

        protected override SendLeadResponse SendLeadAfterZipCodeCheck(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId, string zipcode)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                if (!Validate(incident))
                {
                    retVal.Reason = validationMessage;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                string insuranceType = GetInsuranceType(incident.new_subtype);
                if (insuranceType == null)
                {
                    retVal.Reason = "Invalid insurance type was passed";
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }

                string leadSourceKey = IsTest() ? LEADSOURCEKEY_TEST : LEADSOURCEKEY_PROD;

                var contact = incident.incident_customer_contacts;                

                string xml = XML_BASE.
                    Replace("*|F_NAME|*", SecurityElement.Escape(contact.firstname)).
                    Replace("*|L_NAME|*", SecurityElement.Escape(contact.lastname)).
                    Replace("*|EMAIL|*", SecurityElement.Escape(contact.emailaddress1)).
                    Replace("*|INSURANCE_TYPE|*", SecurityElement.Escape(insuranceType)).
                    Replace("*|PHONE|*", SecurityElement.Escape(incident.new_telephone1)).
                    Replace("*|ZIPCODE|*", SecurityElement.Escape(zipcode)).
                    Replace("*|LEAD_DATE_TIME|*", SecurityElement.Escape(incident.createdon.Value.ToString(DATE_FORMAT))).
                    Replace("*|LEAD_ID|*", SecurityElement.Escape(incident.new_leadidtoken)).
                    Replace("*|LEADSOURCEKEY|*", SecurityElement.Escape(leadSourceKey));

                RestSharp.RestClient rClient = new RestSharp.RestClient(BASE_URL);
                RestSharp.RestRequest rRequest = new RestSharp.RestRequest(RestSharp.Method.POST);
                rRequest.AddParameter("xmlstring", xml);
                rRequest.Resource = RESOURCE;
                RestSharp.IRestResponse rResponse = rClient.Execute(rRequest);

                XElement doc = XElement.Parse(rResponse.Content);
                var x = doc.Element(ns + "PortalServiceReturnDataList");
                string statusCode = x.Element(ns + "StatusCode").Value;
                string statusDescription = x.Element(ns + "StatusDescription").Value;
                string statusDetails = x.Element(ns + "StatusDetails").Value;
                string referenceID = x.Element(ns + "ReferenceID").Value;
                string referenceOpportunityID = x.Element(ns + "ReferenceOpportunityID").Value;

                if (statusCode == "0")//success
                {
                    retVal.IsSold = true;
                    retVal.BrokerId = referenceID + ";" + referenceOpportunityID;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = statusDescription + ";" + statusDetails;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AgentCubedHealthInsurance.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private string GetInsuranceType(string subType)
        {
            string insuranceType;
            if (!String.IsNullOrEmpty(subType) && insuranceTypesMap.TryGetValue(subType, out insuranceType))
            {
                return insuranceType;
            }
            return null;
        }

        protected override void ExecuteValidationSet(DataModel.Xrm.incident incident)
        {
            ValidateLeadIdToken(incident);
            ValidateFirstAndLastName(incident);
            ValidateEmail(incident);
        }

        private const string XML_BASE =
@"<?xml version=""1.0"" encoding=""utf-8""?>
<AgentCubedAPI xmlns=""http://dataexchange.agentcubed.com"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
  <LoginCredentials>
    <ErrorNotificationEmail></ErrorNotificationEmail>
    <Username>velapoint</Username>
    <Password>velapointLive=#1!</Password>
    <Group_WebLead_ID>20122</Group_WebLead_ID>
    <LeadSourceKey>*|LEADSOURCEKEY|*</LeadSourceKey>
  </LoginCredentials>
  <Leads>
    <Lead>
      <LeadInformation>
        <TrackingKey10></TrackingKey10>
        <TrackingKey09></TrackingKey09>
        <TrackingKey08></TrackingKey08>
        <TrackingKey07></TrackingKey07>
        <TrackingKey06></TrackingKey06>
        <TrackingKey05></TrackingKey05>
        <TrackingKey04></TrackingKey04>
        <TrackingKey03></TrackingKey03>
        <TrackingKey02>B01</TrackingKey02>
        <TrackingKey01>NP2014</TrackingKey01>
        <LeadOrigin></LeadOrigin>
        <Comments></Comments>
        <ReferenceKeyType></ReferenceKeyType>
        <ReferenceKey></ReferenceKey>
        <LeadGeneratedDateTime>*|LEAD_DATE_TIME|*</LeadGeneratedDateTime>
      </LeadInformation>
      <LeadIndividuals>
        <Individual IndividualID=""0"">             
          <LastName>*|F_NAME|*</LastName>
          <FirstName>*|L_NAME|*</FirstName>
          <Email>*|EMAIL|*</Email>
          <RelationType>Applicant</RelationType>
        </Individual>        
      </LeadIndividuals>
      <LeadOpportunities>
        <Opportunity>
          <InsuranceType>*|INSURANCE_TYPE|*</InsuranceType>
          <LeadID>*|LEAD_ID|*</LeadID>
        </Opportunity>
      </LeadOpportunities>
      <LeadContactDetails>
        <PrimaryPhone>*|PHONE|*</PrimaryPhone>
        <Address>
          <ZipCode>*|ZIPCODE|*</ZipCode>
        </Address>
      </LeadContactDetails>
    </Lead>
  </Leads>
</AgentCubedAPI>";

    }
}


/*
Response example:

<?xml version="1.0" encoding="utf-8"?>
<ArrayOfPortalServiceReturnDataList xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://dataexchange.agentcubed.com">
  <PortalServiceReturnDataList>
    <ReferenceKey />
    <StatusCode>0</StatusCode>
    <StatusDescription>Successful</StatusDescription>
    <StatusDetails>Successfully added lead to the system.</StatusDetails>
    <ReferenceID>1658298</ReferenceID>
    <ReferenceOpportunityID>1858677</ReferenceOpportunityID>
  </PortalServiceReturnDataList>
</ArrayOfPortalServiceReturnDataList>

*/