﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class Pay4PerformanceMoving : PhoneConnection
    {
        private const decimal localDistancePrice = 10;
        private const decimal longDistancePrice = 20;

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                var moveToRegion = regDal.GetRegionByName(incident.new_movetozipcode);
                if (moveToRegion == null)
                {
                    retVal.WantsToBid = false;
                    retVal.Reason = "Move to zipcode is mandatory";
                    return retVal;
                }
                string[] moveToSplit = moveToRegion.new_englishname.Split(';');
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.WantsToBid = false;
                    retVal.Reason = REGION_ERROR;
                    return retVal;
                }
                string zipcode = region.new_name;
                string[] regionNameSplit = region.new_englishname.Split(';');
                string stateFrom = regionNameSplit[3];
                if (stateFrom != moveToSplit[3])//compare states
                {
                    //is long distance
                    retVal.Price = longDistancePrice;
                }
                else
                {
                    //local
                    retVal.Price = localDistancePrice;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Pay4PerformanceMoving.GetBrokerBid");
                if (retVal == null)
                    retVal = new GetBrokerBidResponse();
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }
    }
}
