﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class EmisMedia : LeadBuyerBase
    {
        private const string url = "http://www.emisltd.com/l/get_lead.php";
        private const string user = "noproblem";
        private const string password = "gkk6m$fA";
        private const string regionError = "Region is not zip code level.";
        private const string minusSign = "-";
        private const string REJECT = "REJECT";
        private const string MOVING = "Moving";
        private const string ERROR = "ERROR";

        #region ILeadBuyer Members

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = regionError;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string[] regionNameSplit = region.new_englishname.Split(';');
                string zipCode = regionNameSplit[0];
                string city = regionNameSplit[1];
                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                var parents = regDal.GetParents(region);
                string state = parents.First(x => x.new_level == 1).new_name;

                DataModel.Xrm.new_region moveToRegion = null;
                string moveSize = string.Empty;
                //add moving logic.
                if (accExp.new_externalcode == MOVING)
                {
                    if (string.IsNullOrEmpty(incident.new_movetozipcode))
                    {
                        retVal.Reason = "Moving to zip code is mandatory";
                        retVal.FailedInnerValidation = true;
                        return retVal;
                    }
                    moveToRegion = regDal.GetRegionByName(incident.new_movetozipcode);
                    if (moveToRegion == null)
                    {
                        retVal.Reason = "Moving to zip code is invalid";
                        retVal.FailedInnerValidation = true;
                        return retVal;
                    }
                    if (string.IsNullOrEmpty(incident.incident_customer_contacts.emailaddress1))
                    {
                        retVal.Reason = "email is mandatory";
                        retVal.FailedInnerValidation = true;
                        return retVal;
                    }
                    if (!incident.new_movedate.HasValue)
                    {
                        retVal.Reason = "moving date is mandatory";
                        retVal.FailedInnerValidation = true;
                        return retVal;
                    }
                    switch (incident.new_movesize)
                    {
                        case "studio":
                            moveSize = "Studio 1500 lbs";
                            break;
                        case "oneBdr":
                            moveSize = "1 BR Small 3000 lbs";
                            break;
                        case "twoBdr":
                            moveSize = "2 BR Small 4500 lbs";
                            break;
                        case "threeBdr":
                            moveSize = "3 BR Small 8000 lbs";
                            break;
                        case "fourBdr":
                            moveSize = "4 BR Small 10000 lbs";
                            break;
                        case "fiveBdr":
                        case "larger":
                            moveSize = "Over 12000 lbs";
                            break;
                        default:
                            retVal.Reason = "Move size is mandatory";
                            retVal.FailedInnerValidation = true;
                            return retVal;
                    }
                }

                HttpClient clientping = new HttpClient(url, HttpClient.HttpMethodEnum.Post);
                clientping.ParamItems.Add("user", user);
                clientping.ParamItems.Add("password", password);
                clientping.ParamItems.Add("zip", zipCode);
                clientping.ParamItems.Add("category", accExp.new_externalcode);
                var resping = clientping.Post();

                if (resping.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (resping.Content != REJECT)
                    {
                        if (incident.new_telephone1.Length != 10)
                        {
                            retVal.Reason = "Phone was not 10 digits";
                            retVal.FailedInnerValidation = true;
                            return retVal;
                        }
                        ExecutePostStage(incident, accExp, retVal, zipCode, city, regDal, state, moveToRegion, moveSize, resping.Content);
                    }
                    else
                    {
                        retVal.MarkAsRejected = true;
                        retVal.Reason = string.Format("Rejected at ping stage. Content = {0}", resping.Content);
                    }
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = string.Format("Bad HTTP status received at ping stage. HttpStatus = {0}. Content = {1}.", resping.StatusCode, resping.Content);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in EmisMedia.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        #endregion


        private void ExecutePostStage(
            DataModel.Xrm.incident incident,
            DataModel.Xrm.new_accountexpertise accExp,
            SendLeadResponse retVal,
            string zipCode,
            string city,
            DataAccessLayer.Region regDal,
            string state,
            DataModel.Xrm.new_region moveToRegion,
            string moveSize,
            string pingCode)
        {
            string phone1 = incident.new_telephone1.Substring(0, 3);
            string phone2 = incident.new_telephone1.Substring(3, 3);
            string phone3 = incident.new_telephone1.Substring(6, 4);

            var contact = incident.incident_customer_contacts;
            var firstName = contact.firstname;
            var lastName = contact.lastname;
            var email = contact.emailaddress1;
            if (string.IsNullOrEmpty(firstName) || firstName == minusSign)
            {
                firstName = "D";
            }
            if (string.IsNullOrEmpty(lastName) || lastName == minusSign)
            {
                lastName = "D";
            }

            string info = incident.description;
            if (System.Configuration.ConfigurationManager.AppSettings["SiteId"] != "Sales")
            {
                info = "test";
            }
            HttpClient clientpost = new HttpClient(url, HttpClient.HttpMethodEnum.Post);
            clientpost.ParamItems.Add("user", user);
            clientpost.ParamItems.Add("password", password);
            clientpost.ParamItems.Add("zip", zipCode);
            clientpost.ParamItems.Add("category", accExp.new_externalcode);
            clientpost.ParamItems.Add("ping", pingCode);
            clientpost.ParamItems.Add("fname", firstName);
            clientpost.ParamItems.Add("lname", lastName);
            clientpost.ParamItems.Add("phone_1", phone1);
            clientpost.ParamItems.Add("phone_2", phone2);
            clientpost.ParamItems.Add("phone_3", phone3);
            clientpost.ParamItems.Add("email", email);
            clientpost.ParamItems.Add("state", state);
            clientpost.ParamItems.Add("city", city);
            clientpost.ParamItems.Add("street", minusSign);
            clientpost.ParamItems.Add("info", info);
            if (accExp.new_externalcode == MOVING)
            {
                string[] moveToSplit = moveToRegion.new_englishname.Split(';');
                string toCity = moveToSplit[1];
                var toParents = regDal.GetParents(moveToRegion);
                string toState = toParents.First(x => x.new_level == 1).new_name;
                clientpost.ParamItems.Add("state_to", toState);
                clientpost.ParamItems.Add("city_to", toCity);
                clientpost.ParamItems.Add("day", string.Format("{0:00}", incident.new_movedate.Value.Day));
                clientpost.ParamItems.Add("month", string.Format("{0:00}", incident.new_movedate.Value.Month));
                clientpost.ParamItems.Add("year", incident.new_movedate.Value.Year.ToString());
                clientpost.ParamItems.Add("weight", moveSize);
            }
            var respost = clientpost.Post();

            if (respost.StatusCode == System.Net.HttpStatusCode.OK)
            {
                retVal.BrokerId = respost.Content;
                if (respost.Content == REJECT || respost.Content.StartsWith(ERROR))
                {
                    retVal.Reason = string.Format("EmisMedia responded with {0} at post stage.", respost.Content);
                    retVal.MarkAsRejected = true;
                }
                else
                {
                    retVal.IsSold = true;
                }
            }
            else
            {
                retVal.Reason = string.Format("Bad HTTP status received at post stage. HttpStatus = {0}. Content = {1}.", respost.StatusCode, respost.Content);
            }
        }
    }
}
