﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Web;
using NoProblem.Core.DataModel.Consumer;
using System.Net;
using System.IO;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class Renex : LeadBuyerBase
    {
        private const string PING_TEST_URL = "http://affiliate.renovationexperts.com/aff_testing/lemond_ping.asp";
        private const string PING_REAL_URL = "http://affiliate.renovationexperts.com/pingpost/lemond_ping.asp";
        private const string POST_TEST_URL = "http://affiliate.renovationexperts.com/aff_testing/lemond_addlead_product.asp";
        private const string POST_REAL_URL = "http://affiliate.renovationexperts.com/pingpost/lemond_addlead_product.asp";
        private const string IP_IS_MANDATORY = "IP is mandatory";
        private const string PHONE_IS_INVALID = "Phone is invalid, it must be ten digits exaclty.";
        private const string CHECKSUM = "B4rM7kq25duX";
        private const string USERNAME_HOME = "noproblem";
        private const string PASSWORD_HOME = "Pass@word1";
        private const string SID_HOME = "531";
        private const string USERNAME_OTHER = "DonnyReich";
        private const string PASSWORD_OTHER = "Dr123456";
        private const string SID_OTHER = "647";
        private const string HOME_SERVICE_CODE = "home";
        private bool isHomeService;
        private const decimal MOVER_LONG_DISTANCE_PRICE = 22;
        private const string MOVING_LONG_CODE = "MOVING_LONG";
        private const string MOVING_MTID = "478";
        private const string MOVING_LONG_STID = "808";
        private const string MOVING_LONG_PROJECT_TYPE = "Long Distance Moving";

        private static readonly Dictionary<eBudget, string> budgetMap =
            new Dictionary<NoProblem.Core.DataModel.Consumer.eBudget, string>
            {
                {eBudget.UpTo500, HttpUtility.HtmlEncode("up to $500")},
                {eBudget.From500To1000, HttpUtility.HtmlEncode("$500 to $1000")},
                {eBudget.From1000To2500, HttpUtility.HtmlEncode("$1000 to $2500")},
                {eBudget.From2500To5000, HttpUtility.HtmlEncode("$2500 to $5000")},
                {eBudget.From5000To10000, HttpUtility.HtmlEncode("$5000 to $10000")},
                {eBudget.From10000To15000, HttpUtility.HtmlEncode("$10000 to $25000")},
                {eBudget.From15000To25000, HttpUtility.HtmlEncode("$10000 to $25000")},
                {eBudget.From25000To50000, HttpUtility.HtmlEncode("$25000 to $50000")},
                {eBudget.From50000To100000, HttpUtility.HtmlEncode("$50000 to $100000")},
                {eBudget.From100000To200000, HttpUtility.HtmlEncode("$100000 to $200000")},
                {eBudget.From200000AndMore, HttpUtility.HtmlEncode("$200000 +")}
            };

        /// <summary>
        /// Change the default price only if is long distance moving.
        /// </summary>
        /// <param name="incident"></param>
        /// <param name="accExp"></param>
        /// <param name="ia"></param>
        /// <returns></returns>
        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            if (!String.IsNullOrEmpty(incident.new_movetozipcode))
            {
                string[] externalCodeSplit = accExp.new_externalcode.Split(';');
                string mtid = externalCodeSplit[1];
                if (mtid == MOVING_MTID)
                {
                    DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                    var moveToRegion = regDal.GetRegionByName(incident.new_movetozipcode);
                    if (moveToRegion == null)
                    {
                        return base.GetBrokerBid(incident, accExp, ia);
                    }
                    string[] moveToSplit = moveToRegion.new_englishname.Split(';');
                    var region = incident.new_new_region_incident;
                    if (region.new_level != 4)
                    {
                        return base.GetBrokerBid(incident, accExp, ia);
                    }
                    string[] regionNameSplit = region.new_englishname.Split(';');
                    string stateFrom = regionNameSplit[3];
                    bool isLongDistanceLead = (stateFrom != moveToSplit[3]);
                    if (isLongDistanceLead)
                    {
                        GetBrokerBidResponse retVal = new GetBrokerBidResponse();
                        retVal.Price = MOVER_LONG_DISTANCE_PRICE;
                        retVal.BrokerId = MOVING_LONG_CODE;
                        return retVal;
                    }
                }
            }
            return base.GetBrokerBid(incident, accExp, ia);
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                string[] externalCodeSplit = accExp.new_externalcode.Split(';');
                string stid = externalCodeSplit[0];
                string mtid = externalCodeSplit[1];
                string projectType = externalCodeSplit[2];
                isHomeService = externalCodeSplit[3] == HOME_SERVICE_CODE;
                if (mtid == MOVING_MTID && pingId == MOVING_LONG_CODE)
                {
                    stid = MOVING_LONG_STID;
                    projectType = MOVING_LONG_PROJECT_TYPE;
                }
                eBudget budgetEnum;
                if (!incident.new_budget.HasValue)
                {
                    //if no budget we send default up to 500.
                    budgetEnum = eBudget.UpTo500;
                }
                else
                {
                    budgetEnum = (eBudget)incident.new_budget.Value;
                }
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = REGION_ERROR;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string zipcode = region.new_name;
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                var email = contact.emailaddress1;
                var streetAddress = contact.address1_line1;
                if (isHomeService && string.IsNullOrEmpty(streetAddress))
                {
                    retVal.Reason = ADDRESS_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
                {
                    retVal.Reason = FNAME_LNAME_ARE_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                if (string.IsNullOrEmpty(email))
                {
                    retVal.Reason = EMAIL_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string ip = incident.new_ip;
                if (string.IsNullOrEmpty(ip))
                {
                    retVal.Reason = IP_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string phone = incident.new_telephone1;
                if (phone.Length != 10)
                {
                    retVal.Reason = PHONE_IS_INVALID;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                bool isTest = IsTest();
                string pingSeed;
                bool acceptedAtPing = SendPing(zipcode, stid, mtid, isTest, out pingSeed);
                if (acceptedAtPing)
                {
                    string city, state;
                    ExtractZipcodeInfo(region, out city, out state);
                    string budget = isHomeService ? budgetMap[budgetEnum] : String.Empty;
                    string postMessage;
                    bool okPost = SendPost(zipcode, incident.new_telephone1, incident.new_ip, firstName, lastName, email, streetAddress, city, state, incident.description, budget, pingSeed, stid, mtid, projectType, isTest, out postMessage);
                    if (okPost)
                    {
                        retVal.IsSold = true;
                        retVal.BrokerId = postMessage;
                    }
                    else
                    {
                        retVal.MarkAsRejected = true;
                        retVal.Reason = string.Format("BAD RESPONSE FROM API AT POST. MESSAGE: {0}", postMessage);
                    }
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = string.Format("Not accepted at ping. Message: {0}", pingSeed);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Renex.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private bool SendPost(string zipcode, string phone, string ip, string firstName, string lastName, string email, string streetAddress, string city, string state, string description, string budget, string pingSeed, string stid, string mtid, string projectType, bool isTest, out string postMessage)
        {
            string url = isTest ? POST_TEST_URL : POST_REAL_URL;
            RestSharp.RestClient client = new RestSharp.RestClient(url);
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
            AddGeneralParams(request, zipcode, stid, mtid);
            request.AddParameter(ArgsNamesPost.ADDR1, streetAddress);
            request.AddParameter(ArgsNamesPost.BUDGET, budget);
            request.AddParameter(ArgsNamesPost.CITY, city);
            request.AddParameter(ArgsNamesPost.EMAIL, email);
            request.AddParameter(ArgsNamesPost.FNAME, firstName);
            request.AddParameter(ArgsNamesPost.IP, ip);
            request.AddParameter(ArgsNamesPost.LNAME, lastName);
            request.AddParameter(ArgsNamesPost.PROJECT_DETAILS, description);
            request.AddParameter(ArgsNamesPost.PROJECT_TYPE, System.Web.HttpUtility.HtmlEncode(projectType));
            request.AddParameter(ArgsNamesPost.SEED, pingSeed);
            request.AddParameter(ArgsNamesPost.STATE, state);
            string phoneAreaCode = phone.Substring(0, 3);
            string phoneMiddle = phone.Substring(3, 3);
            string phoneLast4 = phone.Substring(6, 4);
            request.AddParameter(ArgsNamesPost.PHONE_AC, phoneAreaCode);
            request.AddParameter(ArgsNamesPost.PHONE_MID, phoneMiddle);
            request.AddParameter(ArgsNamesPost.PHONE_LAST, phoneLast4);
            var response = client.Execute(request);
            return ParsePostResponse(response, out postMessage);
        }

        private bool ParsePostResponse(RestSharp.IRestResponse response, out string postMessage)
        {
            //NEW LEAD ID:490
            bool retVal = false;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                postMessage = HttpUtility.HtmlEncode(response.Content);
                retVal = response.Content.StartsWith("NEW LEAD ID:");
            }
            else
            {
                postMessage = string.Format("BAD HTTP STATUS RECEIVED IN POST. HTTP STATUS = {0}. CONTENT = {1}.", response.StatusCode, HttpUtility.HtmlEncode(response.Content));
            }
            return retVal;
        }

        private bool SendPing(string zipcode, string stid, string mtid, bool isTest, out string pingSeed)
        {
            string url = isTest ? PING_TEST_URL : PING_REAL_URL;

            RestSharp.RestClient client = new RestSharp.RestClient(url);
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.GET);
            AddGeneralParams(request, zipcode, stid, mtid);
            var response = client.Execute(request);
            return ParsePingResponse(response, out pingSeed);
        }

        private bool ParsePingResponse(RestSharp.IRestResponse response, out string pingSeed)
        {
            //<BR>ACCEPT:YES<BR>SEED:ma37389422kn<BR>
            bool retVal = false;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string[] split = response.Content.Split(new string[] { "<BR>" }, StringSplitOptions.RemoveEmptyEntries);
                string[] acceptSplit = split[0].Split(':');
                if (split.Length >= 2)
                {
                    pingSeed = split[1].Split(':')[1];
                }
                else
                {
                    pingSeed = acceptSplit[1];
                }
                bool accepted = acceptSplit[1] == "YES";
                if (accepted)
                {
                    retVal = true;
                }
            }
            else
            {
                pingSeed = string.Format("BAD HTTP STATUS RECEIVED IN PING. HTTP STATUS = {0}. CONTENT = {1}.", response.StatusCode, HttpUtility.HtmlEncode(response.Content));
            }
            return retVal;
        }

        private void AddGeneralParams(RestSharp.RestRequest request, string zipcode, string stid, string mtid)
        {
            request.AddParameter(ArgsNames.CHECKSUM, CHECKSUM);
            request.AddParameter(ArgsNames.USERN, isHomeService ? USERNAME_HOME : USERNAME_OTHER);
            request.AddParameter(ArgsNames.PASSW, isHomeService ? PASSWORD_HOME : PASSWORD_OTHER);
            request.AddParameter(ArgsNames.SID, isHomeService ? SID_HOME : SID_OTHER);
            request.AddParameter(ArgsNames.ZIP, zipcode);
            request.AddParameter(ArgsNames.STID, stid);
            request.AddParameter(ArgsNames.MTID, mtid);
        }

        private static class ArgsNames
        {
            internal const string CHECKSUM = "checksum";
            internal const string USERN = "usern";
            internal const string PASSW = "passw";
            internal const string SID = "sid";
            internal const string ZIP = "zip";
            internal const string STID = "stid";
            internal const string MTID = "mtid";
        }

        private static class ArgsNamesPost
        {
            internal const string SEED = "seed";
            internal const string IP = "ip";
            internal const string FNAME = "fname";
            internal const string LNAME = "lname";
            internal const string PHONE_AC = "phone_ac";
            internal const string PHONE_MID = "phone_mid";
            internal const string PHONE_LAST = "phone_last";
            internal const string EMAIL = "email";
            internal const string ADDR1 = "addr1";
            internal const string CITY = "city";
            internal const string STATE = "state";
            internal const string PROJECT_TYPE = "project_type";
            internal const string PROJECT_DETAILS = "project_details";
            internal const string BUDGET = "budget";
        }
    }
}
