﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Configuration;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    class ElocalDynamicPrice : LeadBuyerBase, IPingPostApiBuyer
    {
        private const string apiKey = "272a589139732f7454df85f4b8fd7d73f758350c";
        private const string urlProd = "http://api.elocal.com/lead/";
        private const string urlTest = "http://stage.api.elocal.com/lead/";

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var name = contact.fullname;
                if (string.IsNullOrEmpty(name))
                {
                    retVal.Reason = "Name is mandatory";
                    retVal.WantsToBid = false;
                    return retVal;
                }
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = "Region is not zipcode level.";
                    retVal.WantsToBid = false;
                    return retVal;
                }
                PingData pingData = new PingData()
                {
                    Zip = region.new_name,
                    CategoryCode = incident.new_new_primaryexpertise_incident.new_code,
                    Id = incident.ticketnumber
                };
                PingResult pingResult = Ping(pingData);
                if (pingResult.Accepted)
                {
                    retVal.Price = pingResult.PingPrice;
                    retVal.BrokerId = pingResult.PingId;
                }
                else
                {
                    retVal.Reason = pingResult.PingMessage;
                    retVal.MarkAsRejected = true;
                    retVal.WantsToBid = false;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ElocalDynamicPrice.GetBrokerBid");
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var name = contact.fullname;
                var email = contact.emailaddress1;
                var zipcode = incident.new_new_region_incident.new_name;
                PostData postData = new PostData()
                {
                    Zip = zipcode,
                    CategoryCode = incident.new_new_primaryexpertise_incident.new_code,
                    Id = incident.ticketnumber,
                    PingId = pingId,
                    FirstName = name,
                    Phone = incident.new_telephone1,
                    Description = incident.description,
                    Email = email,
                };
                PostResult postResult = Post(postData);
                if (postResult.Accepted)
                {
                    retVal.IsSold = true;
                    retVal.BrokerId = postResult.PostId;
                }
                else
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = postResult.PostMessage;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in ElocalDynamicPrice.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private class PingResponse
        {
            public Response response { get; set; }

            internal class Response
            {
                public string status { get; set; }
                public string token { get; set; }
                public string sender_id { get; set; }
                public string message { get; set; }
                public decimal price { get; set; }
            }
        }

        private class PostResponse
        {
            public Response response { get; set; }

            internal class Response
            {
                public string status { get; set; }
                public string token { get; set; }
                public string sender_id { get; set; }
                public string message { get; set; }
                public string elocal_lead_id { get; set; }
            }
        }

        public PingResult Ping(PingData data)
        {
            PingResult pingResult = new PingResult();
            RestSharp.RestClient client = new RestSharp.RestClient(urlProd);
            if (IsTest())
            {
                client.BaseUrl = new Uri(urlTest);
            }
            RestSharp.RestRequest pingReq = new RestSharp.RestRequest(RestSharp.Method.POST);

            pingReq.RequestFormat = RestSharp.DataFormat.Json;
            pingReq.AddBody(new
            {
                ping = new
                {
                    key = apiKey,
                    zip_code = data.Zip,
                    need_id = data.CategoryCode,
                    sender_id = data.Id,
                    tcpa_consent = true,
                    exclusive = true
                }
            });

            pingReq.Resource = "ping";
            var pingResponse = client.Execute<PingResponse>(pingReq);

            if (pingResponse.Data != null)
            {
                if (pingResponse.Data.response.status == "success")
                {
                    pingResult.PingPrice = pingResponse.Data.response.price;
                    pingResult.PingId = pingResponse.Data.response.token;
                    pingResult.Accepted = true;
                }
                else
                {
                    pingResult.PingMessage = pingResponse.Data.response.message;
                    pingResult.Accepted = false;
                }
            }
            else
            {
                LogUtils.MyHandle.WriteToLog("Failed to parse ping response from eLocal. httpStatuCode = {1}. content: {0}", pingResponse.Content, pingResponse.StatusCode);
                pingResult.PingMessage = "Failed to parse ping elocal's response: " + pingResponse.Content;
                pingResult.Accepted = false;
            }
            return pingResult;
        }

        public PostResult Post(PostData data)
        {
            PostResult postResult = new PostResult();
            RestSharp.RestClient client = new RestSharp.RestClient(urlProd);
            if (IsTest())
            {
                client.BaseUrl = new Uri(urlTest);
            }
            RestSharp.RestRequest postReq = new RestSharp.RestRequest(RestSharp.Method.POST);
            postReq.RequestFormat = RestSharp.DataFormat.Json;
            postReq.AddBody(new
            {
                post = new
                {
                    key = apiKey,
                    zip_code = data.Zip,
                    need_id = data.CategoryCode,
                    sender_id = data.Id,
                    ping_token = data.PingId,
                    first_name = data.FirstName,
                    phone = data.Phone,
                    description = data.Description,
                    email = data.Email,
                    tcpa_consent = true,
                    exclusive = true
                }
            });

            postReq.Resource = "post";
            var postResponse = client.Execute<PostResponse>(postReq);

            if (postResponse.Data != null)
            {
                if (postResponse.Data.response.status == "success")
                {
                    postResult.Accepted = true;
                    postResult.PostId = postResponse.Data.response.elocal_lead_id;
                }
                else
                {
                    postResult.Accepted = false;
                    LogUtils.MyHandle.WriteToLog("eLocal (dynamic price) did not return 'success' at post stage. httpStatuCode = {1}. content: {0}", postResponse.Content, postResponse.StatusCode);
                    postResult.PostMessage = "elocal (dynamic price) didn't return 'success' at post stage. message: " + postResponse.Data.response.message;
                }
            }
            else
            {
                LogUtils.MyHandle.WriteToLog("Failed to parse post response from eLocal. httpStatuCode = {1}. content: {0}", postResponse.Content, postResponse.StatusCode);
                postResult.PostMessage = "Failed to parse post elocal's response: " + postResponse.Content;
                postResult.Accepted = false;
            }
            return postResult;
        }
    }
}
