﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;
using System.Xml.Linq;

namespace NoProblem.Core.BusinessLogic.LeadBuyers.Implementations
{
    internal class Quotebound : LeadBuyerBase
    {
        private const string regionError = "Region is not zip code level.";
        private const string pingUrl = "http://leads.quotebound.com/Leads/LeadPing.aspx";
        private const string postUrl = "http://leads.quotebound.com/Leads/LeadPost.aspx";

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var region = incident.new_new_region_incident;
                string city, state;
                ExtractZipcodeInfo(region, out city, out state);
                string zipcode = region.new_name;
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                var email = contact.emailaddress1;
                string address = contact.address1_line1;

                RestSharp.RestClient client = new RestSharp.RestClient(postUrl);
                RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
                request.AddParameter("PingId", pingId);
                request.AddParameter("SubId", incident.incidentid);
                request.AddParameter("FirstName", firstName);
                request.AddParameter("LastName", lastName);
                request.AddParameter("Address1", address);
                request.AddParameter("City", city);
                request.AddParameter("State", state);
                request.AddParameter("Zip", zipcode);
                request.AddParameter("Phone", incident.new_telephone1);
                request.AddParameter("Email", email);
                request.AddParameter("CampaignId", accExp.new_externalcode);
                if (IsTest())
                {
                    request.AddParameter("IsTest", "true");
                }
                var response = client.Execute(request);
                string content = response.Content;
                content = content.Substring(content.IndexOf('<'));//this is to bypass a bug on their side. They send a weird character at the beggining of the XML that causes the XML no be invalid and the parsers to fail.
                XDocument doc = XDocument.Parse(content);
                XElement root = doc.Element("Response");
                bool accepted = root.Element("IsValid").Value == "True";
                if (!accepted)
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = String.Format("Rejected at POST stage!!! Code: {0}. Details: {1}", root.Element("ResponseCode").Value, root.Element("ResponseDetail").Value);
                }
                else
                {
                    retVal.IsSold = true;
                    retVal.BrokerId = root.Element("LeadId").Value;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Quotebound.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse bidResponse = new GetBrokerBidResponse();
            try
            {
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    bidResponse.Reason = regionError;
                    bidResponse.WantsToBid = false;
                    return bidResponse;
                }
                var contact = incident.incident_customer_contacts;
                if (String.IsNullOrEmpty(contact.firstname))
                {
                    bidResponse.Reason = "First name is mandatory";
                    bidResponse.WantsToBid = false;
                    return bidResponse;
                }
                if (String.IsNullOrEmpty(contact.lastname))
                {
                    bidResponse.Reason = "Last name is mandatory";
                    bidResponse.WantsToBid = false;
                    return bidResponse;
                }
                if (String.IsNullOrEmpty(contact.address1_line1))
                {
                    bidResponse.Reason = "street address is mandatory";
                    bidResponse.WantsToBid = false;
                    return bidResponse;
                }
                if (String.IsNullOrEmpty(contact.emailaddress1))
                {
                    bidResponse.Reason = "Email is mandatory";
                    bidResponse.WantsToBid = false;
                    return bidResponse;
                }

                string zipcode = region.new_name;
                string campaignId = accExp.new_externalcode;

                RestSharp.RestClient restClient = new RestSharp.RestClient(pingUrl);
                RestSharp.RestRequest restRequest = new RestSharp.RestRequest(RestSharp.Method.POST);
                restRequest.AddParameter("Zip", zipcode);
                restRequest.AddParameter("SubId", incident.incidentid);
                restRequest.AddParameter("CampaignId", campaignId);
                if (IsTest())
                {
                    restRequest.AddParameter("IsTest", "true");
                }
                
                RestSharp.IRestResponse restResponse = restClient.Execute(restRequest);

                string content = restResponse.Content;
                content = content.Substring(content.IndexOf('<'));//this is to bypass a bug on their side. They send a weird character at the beggining of the XML that causes the XML no be invalid and the parsers to fail.
                XDocument doc = XDocument.Parse(content);
                XElement root = doc.Element("Response");
                bool accepted = root.Element("IsValid").Value == "True";
                if (!accepted)
                {
                    bidResponse.WantsToBid = false;
                    bidResponse.MarkAsRejected = true;
                    bidResponse.Reason = String.Format("Reject at ping with code: {0}. Details: {1}", root.Element("ResponseCode").Value, root.Element("ResponseDetail").Value);
                }
                else
                {
                    bidResponse.BrokerId = root.Element("PingId").Value;
                    bidResponse.Price = decimal.Parse(root.Element("Price").Value);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in Quotebound.GetBrokerBid");
                bidResponse.WantsToBid = false;
                bidResponse.Reason = "Exception getting bid";
            }
            return bidResponse;
        }
    }
}
