﻿using System;
using System.Linq;
using System.Xml.Linq;
using Effect.Crm.Logs;
using NoProblem.Core.BusinessLogic.Utils;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class AbcLeads : LeadBuyerBase
    {
        private const string EMAIL_IS_MANDATORY = "Email is mandatory";
        private const string regionError = "Region is not zip code level.";
        private const string LIFE_INSURANCE_CODE = "lif";

        public override GetBrokerBidResponse GetBrokerBid(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.new_incidentaccount ia)
        {
            GetBrokerBidResponse retVal = new GetBrokerBidResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var email = contact.emailaddress1;
                if (string.IsNullOrEmpty(email))
                {
                    retVal.Reason = EMAIL_IS_MANDATORY;
                    retVal.WantsToBid = false;
                    return retVal;
                }
                var region = incident.new_new_region_incident;
                if (region.new_level != 4)
                {
                    retVal.Reason = regionError;
                    retVal.WantsToBid = false;
                    return retVal;
                }
                if (string.IsNullOrEmpty(contact.address1_line1))
                {
                    retVal.Reason = "Address is mandatory";
                    retVal.WantsToBid = false;
                    return retVal;
                }
                if (accExp.new_externalcode == LIFE_INSURANCE_CODE)
                {
                    if (String.IsNullOrEmpty(contact.new_height))
                    {
                        retVal.Reason = "Height is mandatory";
                        retVal.WantsToBid = false;
                        return retVal;
                    }
                    if (String.IsNullOrEmpty(contact.new_weight))
                    {
                        retVal.Reason = "Height is mandatory";
                        retVal.WantsToBid = false;
                        return retVal;
                    }
                    if (!contact.birthdate.HasValue)
                    {
                        retVal.Reason = "Birthdate is mandatory";
                        retVal.WantsToBid = false;
                        return retVal;
                    }
                }
                string[] regionNameSplit = region.new_englishname.Split(';');
                string zipCode = regionNameSplit[0];
                bool isTest = System.Configuration.ConfigurationManager.AppSettings["SiteId"] != "Sales";
                Utils.HttpClient.HttpResponse pingResponse = DoPing(zipCode, accExp, isTest);
                string pingMessage;
                string pingId;
                decimal price;
                bool ok = ParsePingResponse(pingResponse, out pingMessage, out pingId, out price);
                if (!ok)
                {
                    //rejected at ping stage
                    retVal.MarkAsRejected = true;
                    retVal.BrokerId = pingId;
                    retVal.Reason = "Rejected at PING stage. " + pingMessage;
                    retVal.WantsToBid = false;
                }
                else
                {
                    retVal.Price = price;
                    retVal.BrokerId = pingId;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AbcLeads.GetBrokerBid");
                retVal.WantsToBid = false;
                retVal.Reason = "Exception getting bid";
            }
            return retVal;
        }

        public override SendLeadResponse SendLead(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var contact = incident.incident_customer_contacts;
                var firstName = contact.firstname;
                var lastName = contact.lastname;
                var email = contact.emailaddress1;
                string address = contact.address1_line1;
                var region = incident.new_new_region_incident;
                string[] regionNameSplit = region.new_englishname.Split(';');
                string zipCode = region.new_name;
                string city = regionNameSplit[1];
                DataAccessLayer.Region regDal = new NoProblem.Core.DataAccessLayer.Region(null);
                var parents = regDal.GetParents(region);
                string state = parents.First(x => x.new_level == 1).new_name;
                bool isTest = System.Configuration.ConfigurationManager.AppSettings["SiteId"] != "Sales";
                //string pingId = bidResponse.BrokerId;
                //UpdateCallPrice(supplier, incAcc, bidResponse.Price);
                Utils.HttpClient.HttpResponse postRestponse = DoPost(zipCode, accExp, isTest, pingId, firstName, lastName, email, incident.new_telephone1, incident.description, incident.incidentid, address, city, state, contact.new_height, contact.new_weight, contact.birthdate);
                string postMessage;
                string postLeadId;
                bool postOk = ParsePostResponse(postRestponse, out postMessage, out postLeadId);
                
                retVal.BrokerId = pingId + "_" + postLeadId;
                if (!postOk)
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = string.Format("Unexpected response at POST stage. Response = {0}", postMessage);
                }
                else
                {
                    retVal.IsSold = true;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in AbcLeads.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }

        private bool ParsePostResponse(HttpClient.HttpResponse response, out string message, out string leadId)
        {
            leadId = null;
            message = null;
            bool retVal = false;
            try
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    XElement xml = XElement.Parse(response.Content);
                    XElement approvedElement = xml.Element("approved");
                    string status = approvedElement.Value;
                    bool isApproved = status == "1";
                    if (!isApproved)
                    {
                        XElement errorElement = xml.Element("error");
                        string error = ".";
                        if (errorElement != null)
                        {
                            error = xml.Element("error").Value;
                        }
                        message = string.Format("lead not approved at post stage. Error message = {0}", error);
                    }
                    else
                    {
                        leadId = xml.Element("leademail").Value;
                        retVal = true;
                        message = string.Format("lead approved at post stage. leademail = {0}.", leadId);
                    }
                }
                else
                {
                    message = string.Format("Bad http status. httpStatus = {0}. Content = {1}", response.StatusCode, response.Content);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception parsing response in AbcLeads. Response content = {0}", response.Content);
                message = string.Format("Response could not be parsed at post stage. content = {0}", response.Content);
            }
            return retVal;
        }

        private HttpClient.HttpResponse DoPost(string zipCode, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, bool isTest, string pingId, string firstName, string lastName, string email, string phone, string description, Guid incidentId, string address, string city, string state, string height, string weight, DateTime? birthDate)
        {
            HttpClient client = new HttpClient();
            client.Url = "https://www.abcleads.com/xmlpost/postxml.php";
            client.ParamType = HttpClient.ParamTypeEnum.Content;
            client.HttpMethod = HttpClient.HttpMethodEnum.Post;

            if (isTest)
            {
                zipCode = "02818";
                firstName = "test";
                lastName = firstName;
            }

            XElement doc = new XElement("Leads");
            XElement inner = new XElement("lead");
            doc.Add(inner);
            XElement affElement = new XElement("affid");
            affElement.Value = "5584";
            XElement typeElement = new XElement("leadtype");
            typeElement.Value = accExp.new_externalcode;
            XElement zipElement = new XElement("zip");
            zipElement.Value = zipCode;
            XElement emailElement = new XElement("email");
            emailElement.Value = email;
            XElement pingIdElement = new XElement("pingid");
            pingIdElement.Value = pingId;
            XElement fnameElement = new XElement("firstname");
            fnameElement.Value = firstName;
            XElement lnameElement = new XElement("lastname");
            lnameElement.Value = lastName;
            XElement phoneElement = new XElement("phone1");
            phoneElement.Value = phone;
            XElement addressElement = new XElement("address1");
            addressElement.Value = address;
            XElement cityElement = new XElement("city");
            cityElement.Value = city;
            XElement stateElement = new XElement("state");
            stateElement.Value = state;
            XElement tcpaElement = new XElement("tcpa_consent");
            tcpaElement.Value = "yes";
            XElement tcpaLanguageElement = new XElement("tcpa_language");
            tcpaLanguageElement.Value = "english";
            XElement phoneTypeElement = new XElement("phone_type");
            phoneTypeElement.Value = "moblie";
            XElement sourceTypeElement = new XElement("source_type");
            sourceTypeElement.Value = "Non-Mobile Site";
            XElement sourceElement = new XElement("source");
            sourceElement.Value = "noproblemppc";
            XElement subIdElement = new XElement("subid");
            subIdElement.Value = incidentId.ToString();

            inner.Add(affElement);
            inner.Add(typeElement);
            inner.Add(zipElement);
            inner.Add(emailElement);
            inner.Add(pingIdElement);
            inner.Add(fnameElement);
            inner.Add(lnameElement);
            inner.Add(phoneElement);
            inner.Add(addressElement);
            inner.Add(cityElement);
            inner.Add(stateElement);
            inner.Add(tcpaElement);
            inner.Add(tcpaLanguageElement);
            inner.Add(phoneTypeElement);
            inner.Add(sourceTypeElement);
            inner.Add(sourceElement);
            inner.Add(subIdElement);

            if (accExp.new_externalcode == LIFE_INSURANCE_CODE)
            {
                XElement birthdateElement = new XElement("birthday");
                birthdateElement.Value = birthDate.Value.ToString("MM/dd/yyyy");
                inner.Add(birthdateElement);
                XElement heightElement = new XElement("height");
                heightElement.Value = height;
                inner.Add(heightElement);
                XElement weightElement = new XElement("weight");
                weightElement.Value = weight;
                inner.Add(weightElement);
            }

            string x = doc.ToString();
            client.ContentData = x;
            var ans = client.Post();
            return ans;
        }

        private NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpResponse DoPing(string zipCode, NoProblem.Core.DataModel.Xrm.new_accountexpertise accExp, bool isTest)
        {
            if (isTest)
                zipCode = "02818";
            HttpClient client = new HttpClient();
            client.Url = "https://www.abcleads.com/xmlpost/pingxml.php";
            client.ParamType = HttpClient.ParamTypeEnum.Content;
            client.HttpMethod = HttpClient.HttpMethodEnum.Post;
            XElement doc = new XElement("ping");
            XElement affIdElement = new XElement("affid");
            affIdElement.Value = "5584";
            XElement proTypeElement = new XElement("protype");
            proTypeElement.Value = accExp.new_externalcode;
            XElement zipElement = new XElement("zip");
            zipElement.Value = zipCode;
            XElement tcpaElement = new XElement("tcpa_consent");
            tcpaElement.Value = "yes";
            XElement tcpaLanguageElement = new XElement("tcpa_language");
            tcpaLanguageElement.Value = "english";
            XElement sourceElement = new XElement("source");
            sourceElement.Value = "noproblemppc";
            XElement sourceTypeElement = new XElement("source_type");
            sourceTypeElement.Value = "Non-Mobile Site";
            XElement phoneTypeElement = new XElement("phone_type");
            phoneTypeElement.Value = "moblie";

            doc.Add(affIdElement);
            doc.Add(proTypeElement);
            doc.Add(zipElement);
            doc.Add(tcpaElement);
            doc.Add(tcpaLanguageElement);
            doc.Add(sourceElement);
            doc.Add(sourceTypeElement);
            doc.Add(phoneTypeElement);

            string x = doc.ToString();
            client.ContentData = x;
            var ans = client.Post();
            return ans;
        }

        private bool ParsePingResponse(NoProblem.Core.BusinessLogic.Utils.HttpClient.HttpResponse response, out string message, out string pingId, out decimal price)
        {
            pingId = null;
            price = -1;
            message = null;
            bool retVal = false;
            try
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    XElement xml = XElement.Parse(response.Content);
                    XElement approvedElement = xml.Element("approved");
                    string status = approvedElement.Value;
                    bool isApproved = status == "1";
                    if (!isApproved)
                    {
                        XElement errorElement = xml.Element("error");
                        string error = ".";
                        if (errorElement != null)
                        {
                            error = errorElement.Value;
                        }
                        message = string.Format("lead not approved at ping stage. Error message = {0}", error);
                    }
                    else
                    {
                        pingId = xml.Element("pingid").Value;
                        string payoutStr = xml.Element("payout").Value;
                        price = decimal.Parse(payoutStr);
                        retVal = true;
                        message = string.Format("lead approved at ping stage. pingid = {0}. payout = {1}", pingId, payoutStr);
                    }
                }
                else
                {
                    message = string.Format("Bad http status. httpStatus = {0}. Content = {1}", response.StatusCode, response.Content);
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception parsing response in AbcLeads. Response content = {0}", response.Content);
                message = string.Format("Response could not be parsed at ping stage. content = {0}", response.Content);
            }
            return retVal;
        }
    }
}
