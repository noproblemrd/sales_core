﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class OYO : LeadBuyerWithZipCodeCheckBase
    {
        //http://homeimprovementcampaign.com/welcome.php?fname=Jack&lname=Smith&address=435%20South%20St&city=Philadelphia&state=PA&zip=19107&phn=2154485565
        //&email=jacksmith@yahoo.com&imp=Bathroom%20Remodeling&timeframe=1-2%20Weeks&budget=$5,000-$10,000&date=09/04/2013

        private const string url = "http://homeimprovementcampaign.com/welcome.php";
        private const string EMAIL_IS_MANDATORY = "Email is mandatory";
        private const string ADDRESS_IS_MANDTORY = "Address is mandatory";
        private const string NAME_IS_MANDATORY = "First and last name are mandatory";
        private const string DATE_FORMAT = "MM/dd/yyyy";

        protected override SendLeadResponse SendLeadAfterZipCodeCheck(DataModel.Xrm.incident incident, DataModel.Xrm.new_accountexpertise accExp, DataModel.Xrm.account supplier, DataModel.Xrm.new_incidentaccount incAcc, string pingId, string zipcode)
        {
            SendLeadResponse retVal = new SendLeadResponse();
            try
            {
                var region = incident.new_new_region_incident;                
                string[] regionNameSplit = region.new_englishname.Split(';');
                string zipCode = regionNameSplit[0];
                string city = regionNameSplit[1];
                string state = regionNameSplit[3];
                var contact = incident.incident_customer_contacts;
                var email = contact.emailaddress1;
                if (string.IsNullOrEmpty(email))
                {
                    retVal.Reason = EMAIL_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string fname = contact.firstname;
                string lname = contact.lastname;
                if (string.IsNullOrEmpty(fname) || string.IsNullOrEmpty(lname))
                {
                    retVal.Reason = NAME_IS_MANDATORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }                
                string address = contact.address1_line1;
                if (string.IsNullOrEmpty(address))
                {
                    retVal.Reason = ADDRESS_IS_MANDTORY;
                    retVal.FailedInnerValidation = true;
                    return retVal;
                }
                string budget;
                if (incident.new_budget.HasValue)
                {
                    DataModel.Consumer.eBudget budE = (DataModel.Consumer.eBudget)incident.new_budget.Value;
                    budget = budE.ToString();
                }
                else
                {
                    budget = DataModel.Consumer.eBudget.From500To1000.ToString();
                }
                string date = DateTime.Now.Date.ToString(DATE_FORMAT);
                string category = accExp.new_externalcode;

                RestSharp.RestClient client = new RestSharp.RestClient(url);
                RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
                //http://homeimprovementcampaign.com/welcome.php?fname=Jack&lname=Smith&address=435%20South%20St&city=Philadelphia&state=PA&zip=19107&phn=2154485565
                //&email=jacksmith@yahoo.com&imp=Bathroom%20Remodeling&timeframe=1-2%20Weeks&budget=$5,000-$10,000&date=09/04/2013
                request.AddParameter(new RestSharp.Parameter() { Name = "fname", Value = fname, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "lname", Value = lname, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "address", Value = address, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "city", Value = city, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "state", Value = state, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "zip", Value = zipCode, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "phn", Value = incident.new_telephone1, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "email", Value = email, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "imp", Value = category, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "timeframe", Value = string.Empty, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "budget", Value = budget, Type = RestSharp.ParameterType.GetOrPost });
                request.AddParameter(new RestSharp.Parameter() { Name = "date", Value = date, Type = RestSharp.ParameterType.GetOrPost });

                var response = client.Execute(request);

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    retVal.MarkAsRejected = true;
                    retVal.Reason = string.Format("Unexpected HTTP status in response from OYO. status = {0}", response.StatusCode);
                }
                else
                {
                    retVal.IsSold = true;
                }
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in OYO.SendLead");
                if (!retVal.IsSold && string.IsNullOrEmpty(retVal.Reason))
                {
                    retVal.Reason = "Exception sending lead";
                }
            }
            return retVal;
        }
    }
}
