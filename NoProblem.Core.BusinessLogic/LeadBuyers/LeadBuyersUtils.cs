﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    public class LeadBuyersUtils : XrmUserBase
    {
        public LeadBuyersUtils(DataModel.Xrm.DataContext xrmDataContext)
            : base(xrmDataContext)
        {

        }

        public List<DataModel.PublisherPortal.GuidStringPair> GetAllBrokers()
        {
            return DataAccessLayer.CacheManager.Instance.Get<List<DataModel.PublisherPortal.GuidStringPair>>("LeadBuyersUtils", "GetAllBrokers", GetAllBrokersMethod);
        }

        private List<DataModel.PublisherPortal.GuidStringPair> GetAllBrokersMethod()
        {
            List<DataModel.PublisherPortal.GuidStringPair> result = new List<NoProblem.Core.DataModel.PublisherPortal.GuidStringPair>();
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            var reader = dal.ExecuteReader(getBrokersQuery);
            foreach (var row in reader)
            {
                DataModel.PublisherPortal.GuidStringPair pair = new NoProblem.Core.DataModel.PublisherPortal.GuidStringPair();
                pair.Id = (Guid)row["accountid"];
                pair.Name = Convert.ToString(row["name"]);
                result.Add(pair);
            }
            return result;
        }

        private const string getBrokersQuery =
           @"
select accountid, name
from account with(nolock)
where new_isleadbuyer = 1
and deletionstatecode = 0
order by name
";
    }
}
