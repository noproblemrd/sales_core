﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoProblem.Core.BusinessLogic.LeadBuyers.Implementations;

namespace NoProblem.Core.BusinessLogic.LeadBuyers
{
    internal class LeadBuyerFactory
    {
        public static ILeadBuyer GetInstance(string leadBuyerName)
        {
            if (String.IsNullOrEmpty(leadBuyerName))
            {
                return null;
            }
            switch (leadBuyerName.ToLower())
            {
                case "networx":
                    return new Networx();
                case "elocal":
                    return new Elocal();
                case "phone":
                    return new PhoneConnection();
                case "sevasearch":
                    return new SevaSearch();
                case "tester":
                    return new Tester();
                case "testerno":
                    return new TesterNo();
                case "email":
                    return new EmailConnection();
                case "legalbrandmarketing":
                    return new LegalBrandMarketing();
                case "t3leads":
                    return new T3leads();
                case "fourlegalleads":
                    return new FourLegalLeads();
                case "movingorbitlongemail":
                    return new MovingOrbitLongEmail();
                case "movingorbitshortemail":
                    return new MovingOrbitShortEmail();
                case "movingorbitlongapi":
                    return new MovingOrbitLongApi();
                case "movingorbitshortapi":
                    return new MovingOrbitShortApi();
                case "fourlegalleadswithzipcode":
                    return new FourLegalLeadsWithZipCodeCheck();
                case "emismedia":
                    return new EmisMedia();
                case "identifyle":
                    return new Identifyle();
                case "homebulletin":
                    return new HomeBulletin();
                case "lexisnexis":
                    return new LexisNexis();
                case "fourlegalleadspingpost":
                    return new FourLegalLeadsPingPost();
                case "moverjunctionshortdistance":
                    return new MoverJunctionShortDistance();
                case "moverjunctionlongdistance":
                    return new MoverJunctionLongDistance();
                case "legalbrandmarketingwithadditionalinfo":
                    return new LegalBrandMarketingWithAdditionalInfo();
                case "abcleads":
                    return new AbcLeads();
                case "renex":
                    return new Renex();
                case "oyo":
                    return new OYO();
                case "leadsbarter":
                    return new LeadsBarter();
                case "homeimprovementleads":
                    return new HomeImprovementLeads();
                case "testerbid":
                    return new TesterBid();
                case "testerbidno":
                    return new TesterBidNo();
                case "webfore":
                    return new WebFore();
                case "ezanga":
                    return new Ezanga();
                case "elocaldynamicprice":
                    return new ElocalDynamicPrice();
                case "oconco":
                    return new Oconco();
                case "quotebound":
                    return new Quotebound();
                case "diablosocialsecuritydisability":
                    return new DiabloSocialSecurityDisability();
                case "toro":
                    return new Toro();
                case "elocaldynamicphone":
                    return new ElocalDynamicPhone();
                case "pay4performancemoving":
                    return new Pay4PerformanceMoving();
                case "agentcubedhealthinsurance":
                    return new AgentCubedHealthInsurance();
                case "triares":
                    return new Triares();
                case "datalot":
                    return new DatalotFacade();
                case "moverphonelongshort":
                    return new MoverPhoneLongShort();
                case "movingguru":
                    return new MovingGuru();
                case "movingorbithi":
                    return new MovingOrbitHI();
                case "thehomefixers":
                    return new TheHomeFixers();
                case "yot7hi":
                    return new Yot7HI();
                case "buyerzonestorage":
                    return new BuyerzoneStorage();
                case "prolegalleads":
                    return new ProLegalLeads();
                case "legalbrandmarketingpingpost":
                    return new NoProblem.Core.BusinessLogic.LeadBuyers.Implementations.LegalBrandMarketing.PingPostApi.LegalBrandMarketingPingPost();
                default:
                    return null;
            }
        }
    }
}
