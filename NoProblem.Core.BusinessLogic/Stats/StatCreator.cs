﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Effect.Crm.Logs;

namespace NoProblem.Core.BusinessLogic.Stats
{
    public class StatCreator : XrmUserBase
    {
        private StatCreator()
            : base(null)
        {

        }

        internal static void CreateRequestStats(DataModel.Xrm.incident incident)
        {
            StatCreator creator = new StatCreator();
            CreateRequestStatsDelegate del = new CreateRequestStatsDelegate(creator.CreateRequestStatsAsync);
            del.BeginInvoke(incident, creator.CreateRequestStatsAsyncCallback, del);
        }

        delegate void CreateRequestStatsDelegate(DataModel.Xrm.incident incident);

        private void CreateRequestStatsAsync(DataModel.Xrm.incident incident)
        {
            DataAccessLayer.AccountRepository dal = new NoProblem.Core.DataAccessLayer.AccountRepository(XrmDataContext);
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@PrimaryExpertiseId", incident.new_primaryexpertiseid.Value));
            var region = incident.new_new_region_incident;
            parameters.Add(new KeyValuePair<string, object>("@RegionLevel", region.new_level.Value));
            parameters.Add(new KeyValuePair<string, object>("@RegionCode", region.new_code));
            parameters.Add(new KeyValuePair<string, object>("@DayOfWeek", ((int)DateTime.Now.DayOfWeek) + 1));
            parameters.Add(new KeyValuePair<string, object>("@Availability", DateTime.Now));
            var reader = dal.ExecuteReader(getSuppliersQuery, parameters);
            StringBuilder onDutyIds = new StringBuilder();
            StringBuilder offDutyIds = new StringBuilder();
            foreach (var row in reader)
            {
                Guid id = (Guid)row["New_AccountId"];
                bool onDuty = (bool)row["isAvailableHours"];
                if (onDuty)
                {
                    onDutyIds.Append("'").Append(id.ToString()).Append("'").Append(",");
                }
                else
                {
                    offDutyIds.Append("'").Append(id.ToString()).Append("'").Append(",");
                }
            }
            string updateCmd = null;
            if (onDutyIds.Length > 0)
            {
                onDutyIds.Remove(onDutyIds.Length - 1, 1);
                updateCmd += string.Format(updateOnDuty, onDutyIds.ToString());
            }
            if (offDutyIds.Length > 0)
            {
                offDutyIds.Remove(offDutyIds.Length - 1, 1);
                updateCmd += string.Format(updateOffDuty, offDutyIds.ToString());
            }
            if (updateCmd != null)
            {
                dal.ExecuteNonQuery(updateCmd);
            }
        }

        private void CreateRequestStatsAsyncCallback(IAsyncResult result)
        {
            try
            {
                ((CreateRequestStatsDelegate)result.AsyncState).EndInvoke(result);
            }
            catch (Exception exc)
            {
                LogUtils.MyHandle.HandleException(exc, "Exception in CreateRequestStatsAsyncCallback");
            }
        }

        private const string updateOnDuty =
     @"update account 
                    set new_ondutyrequests = isnull(new_ondutyrequests,0) + 1
                    where
                    accountid in ({0})
                    ";

        private const string updateOffDuty =
             @"update account 
                    set new_offdutyrequests = isnull(new_offdutyrequests,0) + 1
                    where
                    accountid in ({0})
                    ";

        private const string getSuppliersQuery =
            @"
            create table #TempMatchPrimaryExpertise
	(New_AccountExpertiseId uniqueidentifier primary key, accountId uniqueidentifier)

/*** check for suppliers that match to @PrimaryExpertiseId ****/
insert into #TempMatchPrimaryExpertise (New_AccountExpertiseId, accountId)
SELECT tempAccounts.New_AccountExpertiseId, tempAccounts.new_accountid
FROM 
(SELECT ae.New_AccountExpertiseId
, ae.new_accountid
FROM Account with (nolock)
INNER JOIN New_AccountExpertise ae with (nolock) ON ae.New_accountId = Account.AccountId AND 
	ae.deletionstatecode = 0 AND ae.statecode = 0 AND ae.new_primaryexpertiseid = @PrimaryExpertiseId
) tempAccounts 

----------------------------------------------------------------------------------------

create table #TempMatchCity
	(New_AccountExpertiseId uniqueidentifier primary key)

create table #tmpRegionHirarchy ( RegionLevel int, RegionId uniqueidentifier, ParentRegionId uniqueidentifier );
WITH RegionHirarchy(RegionLevel,RegionId,ParentRegionId)
AS
(
	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
	FROM new_region with(nolock)
	WHERE new_region.new_level = @RegionLevel AND new_code = @RegionCode and new_region.deletionstatecode = 0
	UNION ALL
	SELECT new_region.new_level,new_region.new_regionid,new_region.new_parentregionid
	FROM new_region with(nolock)
	INNER JOIN RegionHirarchy ON RegionHirarchy.ParentRegionId = new_regionid
)
insert into #tmpRegionHirarchy
	select RegionLevel,  RegionId,  ParentRegionId from RegionHirarchy
	
	create table #tmpJoinning ( AccountExpertiseId uniqueidentifier, RegionId uniqueidentifier, RegionState int)
	insert into #tmpJoinning
			select #TempMatchPrimaryExpertise.New_AccountExpertiseId , regaccount.new_regionid, regaccount.New_regionstate
	from #TempMatchPrimaryExpertise
INNER JOIN new_regionaccountexpertise regaccount with (nolock) ON regaccount.new_accountid = #TempMatchPrimaryExpertise.accountId AND 
	regaccount.deletionstatecode = 0 
INNER JOIN #tmpRegionHirarchy ON #tmpRegionHirarchy.RegionId = regaccount.new_regionid	 
	
	create table #tmpNotGoodCity (accountexpertise uniqueidentifier)
	insert into #tmpNotGoodCity
	select #tmpJoinning.AccountExpertiseId
	from #tmpJoinning	
	where
     (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel = @RegionLevel) and #tmpJoinning.Regionstate != 1)
     or (#tmpJoinning.Regionid in (select RegionId from #tmpRegionHirarchy where RegionLevel != @RegionLevel) and #tmpJoinning.Regionstate = 2)
		
insert into #TempMatchCity (New_AccountExpertiseId)
SELECT distinct #tmpJoinning.AccountExpertiseId
FROM #tmpJoinning
where
	#tmpJoinning.AccountexpertiseId not in ( select accountexpertise from #tmpNotGoodCity )

------------------------------------------------------------------

create table #TempAvailabilityDate
	(
New_AccountId uniqueidentifier
--, isAvailableHours bit
)

insert into #TempAvailabilityDate
select New_AccountExpertiseExtensionBase.new_accountid
 from #TempMatchCity
join New_AccountExpertiseExtensionBase with (nolock)
ON New_AccountExpertiseExtensionBase.New_AccountExpertiseId = #TempMatchCity.New_AccountExpertiseId
join new_availability with (nolock) ON (new_availability.new_accountid = New_AccountExpertiseExtensionBase.new_accountid
		AND new_availability.deletionstatecode = 0 AND new_day = @DayOfWeek)
where 
(new_availability.new_from <= CONVERT(char(8),@Availability,108) AND new_availability.new_to >= CONVERT(char(8),@Availability,108))


create table #TempAvailabilityDateAll
	(
New_AccountId uniqueidentifier
--, isAvailableHours bit
)

insert into #TempAvailabilityDateAll
select New_AccountExpertiseExtensionBase.new_accountid
 from #TempMatchCity
join New_AccountExpertiseExtensionBase with (nolock)
ON New_AccountExpertiseExtensionBase.New_AccountExpertiseId = #TempMatchCity.New_AccountExpertiseId
join new_availability with (nolock) ON (new_availability.new_accountid = New_AccountExpertiseExtensionBase.new_accountid
		AND new_availability.deletionstatecode = 0 AND new_day = @DayOfWeek)
--where 
--not(new_availability.new_from <= CONVERT(char(8),@Availability,108) AND new_availability.new_to >= CONVERT(char(8),@Availability,108))


create table #tmpFinal(
New_AccountId uniqueidentifier
, isAvailableHours bit
)

insert into #tmpFinal
select new_accountid 
, case when new_accountid in (select new_accountid from #TempAvailabilityDate) then 1 else 0 end
from #TempAvailabilityDateAll


------------------------------------------------------------------

SELECT	DISTINCT New_AccountId	
		,isAvailableHours
FROM	#tmpFinal

drop table #TempMatchPrimaryExpertise
drop table #TempMatchCity
drop table #tmpRegionHirarchy
drop table #tmpJoinning
drop table #tmpNotGoodCity
drop table #TempAvailabilityDate
drop table #TempAvailabilityDateAll
drop table #tmpFinal
";
    }
}
