﻿using System;
using Effect.Crm.Logs;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using NoProblem.Core.DataAccessLayer.Autonumber;

namespace NoProblem.Core.BusinessLogic
{
   public class Contact : ContextsUserBase
   { 
       #region Ctor
       public Contact(NoProblem.Core.DataModel.Xrm.DataContext xrmDataContext, CrmService crmService)
           : base(xrmDataContext, crmService)
       {
       } 
       #endregion    

      public Guid SearchContact(string phone)
      {
          DataAccessLayer.Contact dal = new NoProblem.Core.DataAccessLayer.Contact(XrmDataContext);
          return dal.SearchContactByOneOfHisPhones(phone);
      }

      public Guid createAnonymousUser(string phoneNumber, Guid originId)
      {
         Guid resultContactId = Guid.Empty;
         try
         {
            DynamicEntity usr = new DynamicEntity();
            usr.Name = "contact";
            usr.Properties = new Property[5];
            
            usr.Properties[0] = new PicklistProperty();
            ((PicklistProperty)usr.Properties[0]).Name = "customertypecode";
            ((PicklistProperty)usr.Properties[0]).Value = new Picklist();
            ((PicklistProperty)usr.Properties[0]).Value.Value = 200000; //Anonymous

            usr.Properties[1] = new StringProperty();
            ((StringProperty)usr.Properties[1]).Name = "firstname";
            ((StringProperty)usr.Properties[1]).Value = phoneNumber;

            usr.Properties[2] = new StringProperty();
            ((StringProperty)usr.Properties[2]).Name = "mobilephone";
            ((StringProperty)usr.Properties[2]).Value = phoneNumber;

            usr.Properties[3] = new LookupProperty();
            ((LookupProperty)usr.Properties[3]).Name = "new_originid";
            ((LookupProperty)usr.Properties[3]).Value = new Lookup();
            ((LookupProperty)usr.Properties[3]).Value.type = "new_origin";
            ((LookupProperty)usr.Properties[3]).Value.Value = originId;

            AutonumberManager autoNumber = new AutonumberManager();
            string consumerNumber = autoNumber.GetNextNumber("contact");
            usr.Properties[4] = new StringProperty();
            ((StringProperty)usr.Properties[4]).Name = "new_consumernumber";
            ((StringProperty)usr.Properties[4]).Value = consumerNumber;

            resultContactId = CrmService.Create(usr);
         }
         catch (System.Web.Services.Protocols.SoapException soapEx)
         {
            LogUtils.MyHandle.HandleException(soapEx, "createAnonymousUser: Create contact failed.");
         }

         return resultContactId;
      }
   }
}
