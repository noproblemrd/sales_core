﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JavascriptCompilingServices.Controllers;
using System.IO;

namespace JavascriptCompilingServices.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string js = File.ReadAllText(@"C:\Users\alain\Downloads\hello.js");
            RestSharp.RestClient client = new RestSharp.RestClient("http://10.154.28.138/JSCompilerServices/api/compile");
            RestSharp.RestRequest request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.AddJsonBody(js);
            var response = client.Execute(request);
            var content = response.Content;
            string compiled = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(content);
            File.WriteAllText(@"C:\Users\alain\Downloads\hello-compiled.js", compiled);
        }
    }
}
