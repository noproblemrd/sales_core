﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoneApiWrapper.CallBacks
{
    public class ArgsTypes
    {
        public const string FAPI_EVENT = "fapi_event";
        public const string FAPI_DESCRIPTION = "fapi_description";
        public const string FAPI_CALL_ID = "fapi_call_id";
        public const string FAPI_HANGUP_CAUSE = "fapi_hangup_cause";
        public const string FAPI_APP_ID = "fapi_app_id";
        public const string FAPI_CALL_SOURCE_ADDRESS = "fapi_call_source_address";
        public const string FAPI_DID_NUMBER = "fapi_did_number";
        public const string FAPI_CALLER_ID_NUMBER = "fapi_caller_id_number";
        public const string FAPI_RECORDING_URL = "fapi_recording_url";
        public const string FAPI_CALL_DURATION = "fapi_call_duration";
        public const string FAPI_DIGITS = "fapi_digits";
    }

    public class EventTypes
    {
        public const string HANGUP = "hangup";
        public const string CALL_ANSWERED = "call_answered";
        public const string INCOMING_CALL = "incoming_call";
        public const string PLAY_FILE_COMPLETE = "play_file_complete";
        public const string STOP_RECORD_COMPLETE = "stop_record_complete";
        public const string BRIDGE_END = "bridge_end";
        public const string PLAY_AND_GET_DTMF_COMPLETE = "play_and_get_dtmf_complete";
        public const string DIAL_COMPLETE = "dial_complete";
        public const string GET_DTMF_COMPLETE = "get_dtmf_complete";
    }

    public class HangupCauses
    {
        public const string NORMAL_CLEARING = "NORMAL_CLEARING";
        public const string USER_BUSY = "USER_BUSY";
        public const string ORIGINATOR_CANCEL = "ORIGINATOR_CANCEL";
        public const string NORMAL_UNSPECIFIED = "NORMAL_UNSPECIFIED";
        public const string NO_ANSWER = "NO_ANSWER";
        public const string NO_USER_RESPONSE = "NO_USER_RESPONSE";
        public const string UNALLOCATED_NUMBER = "UNALLOCATED_NUMBER";
        public const string INVALID_NUMBER_FORMAT = "INVALID_NUMBER_FORMAT";
        public const string CALL_REJECTED = "CALL_REJECTED";
        public const string RECOVERY_ON_TIMER_EXPIRE = "RECOVERY_ON_TIMER_EXPIRE";
        public const string SERVICE_NOT_IMPLEMENTED = "SERVICE_NOT_IMPLEMENTED";
        public const string NETWORK_OUT_OF_ORDER = "NETWORK_OUT_OF_ORDER";
        public const string MEDIA_TIMEOUT = "MEDIA_TIMEOUT";
        public const string INCOMPATIBLE_DESTINATION = "INCOMPATIBLE_DESTINATION";
        public const string NORMAL_TEMPORARY_FAILURE = "NORMAL_TEMPORARY_FAILURE";
        public const string DESTINATION_OUT_OF_ORDER = "DESTINATION_OUT_OF_ORDER";

        public static bool IsValidHangup(string Hangup_Causes)
        {
            switch(Hangup_Causes)
            {
                case (NORMAL_CLEARING):
                case (USER_BUSY):
                case (NO_ANSWER):
                case (NO_USER_RESPONSE):
                case (CALL_REJECTED):
                case (RECOVERY_ON_TIMER_EXPIRE):
                case (NETWORK_OUT_OF_ORDER):
                case (MEDIA_TIMEOUT):
                case (NORMAL_TEMPORARY_FAILURE):
                    return true;

            }
            return false;
        }
    }
}
