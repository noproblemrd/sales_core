﻿using System;
using System.ComponentModel;
using System.Web.Services;
using Effect.Crm.Logs;
using Effect.Crm.Platform;
using System.Security.Principal;
using NoProblem.Core.BusinessLogic.Handlers;

namespace NoProblem.Core.Plugins.WS
{
   /// <summary>
   /// Summary description for IncidentAccountService
   /// </summary>
   [WebService(Namespace = "http://effectcrm.co.il/webservices/")]
   public class IncidentAccountService : BaseCalloutService
   {
      [WebMethod]
      public override void HandlePostUpdateRequest(ref byte[] contextBytes)
      {
         WindowsImpersonationContext impersonation = WindowsIdentity.Impersonate(IntPtr.Zero);
         try
         {
            LogUtils.MyHandle.WriteToLog(8, "IncidentAccountService:HandlePostUpdateRequest");
            Effect.Crm.Plugins.DataContext.ExecutionContext context = new Effect.Crm.Plugins.DataContext.ExecutionContext(contextBytes);
            IncidentAccountHandler incidentAccountHandler = new IncidentAccountHandler(null, null);
            incidentAccountHandler.HandlePostUpdateEvent(context);
         }
         catch (Exception exc)
         {
            LogUtils.MyHandle.HandleException(exc, "HandlePostUpdateRequest");
         }
         finally
         {
            impersonation.Undo();
         }
      }

      [WebMethod]
      public override void HandlePostCreateRequest(ref byte[] contextBytes)
      {
         WindowsImpersonationContext impersonation = WindowsIdentity.Impersonate(IntPtr.Zero);
         try
         {
            LogUtils.MyHandle.WriteToLog(8, "IncidentAccountService:HandlePostCreateRequest");
            Effect.Crm.Plugins.DataContext.ExecutionContext context = new Effect.Crm.Plugins.DataContext.ExecutionContext(contextBytes);
            IncidentAccountHandler incidentAccountHandler = new IncidentAccountHandler(null, null);
            incidentAccountHandler.HandlePostCreateEvent(context);
         }
         catch (Exception exc)
         {
            LogUtils.MyHandle.HandleException(exc, "HandlePostCreateRequest");
         }
         finally
         {
            impersonation.Undo();
         }
      }
   }
}
