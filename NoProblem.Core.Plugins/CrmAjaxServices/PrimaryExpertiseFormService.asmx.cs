﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace NoProblem.Core.Plugins.CrmAjaxServices
{
    /// <summary>
    /// Summary description for PrimaryExpertiseFormService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PrimaryExpertiseFormService : System.Web.Services.WebService
    {
        private const string APP_JSON = "application/json";

        [WebMethod]
        public void ValidateAccountForDidSlider(string accountId, string headingId)
        {
            Context.Response.ContentType = APP_JSON;
            BusinessLogic.AjaxHandlers.PrimaryExpertiseFormHandler handler = new NoProblem.Core.BusinessLogic.AjaxHandlers.PrimaryExpertiseFormHandler();
            var response = handler.ValidateAccountForDidSlider(new Guid(accountId), new Guid(headingId));
            string json = response.ToString();
            Context.Response.Write(json);
        }
    }
}
