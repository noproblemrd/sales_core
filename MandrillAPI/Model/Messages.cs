﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace MandrillApi.Model
{
    //[DataContract]
    //public class TemplateContent
    //{
    //    [DataMember]
    //    public string name { get; set; }
    //    [DataMember]
    //    public string content { get; set; }
    //}

    [DataContract]
    public class Recipient
    {
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string name { get; set; }
    }

    [DataContract]
    public class RecipientReturn
    {
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string status { get; set; }
    }

    [DataContract]
    public class Attachments
    {
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string content { get; set; }
    }

    [DataContract]
    public class SearchReturn
    {
        [DataMember]
        public int ts { get; set; }
        [DataMember]
        public string sender { get; set; }
        [DataMember]
        public string subject { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string[] tags { get; set; }
        [DataMember]
        public int opens { get; set; }
        [DataMember]
        public int clicks { get; set; }
        [DataMember]
        public string state { get; set; }
    }

    [DataContract]
    public class MessageTemplate
    {
        [DataMember]
        public string subject { get; set; }
        [DataMember]
        public string from_email { get; set; }
        [DataMember]
        public string from_name { get; set; }
        [DataMember]
        public List<ToItem> to { get; private set; }
        [DataMember]
        public bool track_opens { get; set; }
        [DataMember]
        public bool track_clicks { get; set; }
        [DataMember]
        public bool auto_text { get; set; }
        [DataMember]
        public bool url_strip_qs { get; set; }
        [DataMember]
        public List<MergeVar> global_merge_vars { get; private set; }
        [DataMember]
        public List<RecepientMergeVars> merge_vars { get; private set; }
        [DataMember]
        public string bcc_address { get; set; }

        public MessageTemplate()
        {
            track_opens = true;
            track_clicks = true;
            auto_text = true;
            url_strip_qs = true;
            merge_vars = new List<RecepientMergeVars>();
            global_merge_vars = new List<MergeVar>();
            to = new List<ToItem>();
        }
    }

    public class RecepientMergeVars
    {
        [DataMember]
        public string rcpt { get; set; }
        public List<MergeVar> vars { get; private set; }

        public RecepientMergeVars()
        {
            vars = new List<MergeVar>();
        }
    }

    public class MergeVar
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string content { get; set; }
    }

    public class ToItem
    {
        [DataMember]
        public string email { get; set; }
    }
}
