﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Diagnostics;

namespace JavascriptCompilingServices.Controllers
{
    public class CompileController : ApiController
    {
        private const string DIR = @"C:\ProgramData\jscompiler";
        private static readonly Random random = new Random();

        // POST api/compile
        public string Post([FromBody]string value)
        {
            try
            {
                if (!Directory.Exists(DIR))
                {
                    Directory.CreateDirectory(DIR);
                }
                string fileName = random.Next(int.MaxValue).ToString() + ".js";
                string filePath = Path.Combine(DIR, fileName);
                string outFilePath = filePath + "-compiled.js";
                File.WriteAllText(filePath, value);
                ProcessStartInfo processInfo = new ProcessStartInfo();
                processInfo.FileName = @"C:\Program Files\Java\jre1.8.0_65\bin\java.exe";
                processInfo.Arguments = "-jar C:\\ProgramData\\jscompiler\\compiler.jar --compilation_level ADVANCED_OPTIMIZATIONS --js " + filePath + " --js_output_file " + outFilePath;
                using (Process process = Process.Start(processInfo))
                {
                    process.WaitForExit();
                    if (process.ExitCode != 0)
                    {
                        throw new Exception("Failed to compile with exit code " + process.ExitCode);
                    }
                }
                string compiled = File.ReadAllText(outFilePath).Trim();
                File.Delete(filePath);
                File.Delete(outFilePath);
                return compiled;
            }
            catch (Exception exc)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent(exc.Message) });
            }
        }
    }
}
