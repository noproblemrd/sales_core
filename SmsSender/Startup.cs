﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SmsSender.Startup))]
namespace SmsSender
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
