using System;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.IO;
using System.Collections;

using Effect.Crm.Configuration;
using Effect.Crm.SDKProviders.CrmServiceSdk;
using Effect.Crm.SDKProviders.MetadataServiceSdk;


namespace Effect.Crm.SDKProviders {
    public class ProviderUtils {

        #region Constractor
        #endregion

        #region Fields
        //public static Dictionary<string, CrmService> serviceColection = new Dictionary<string, CrmService>();
        //public static Dictionary<string, MetadataService> metadataColection = new Dictionary<string, MetadataService>();
        #endregion

        #region Methods

        /// <summary>
        /// Return crserive based organization name from web config
        /// </summary>
        /// <returns></returns>
        public static CrmService GetCrmService() {
            return GetCrmService(ConfigurationUtils.GetConfigurationItem("OrganizationName"));
        }

        public static CrmService GetCrmService(string organizationName) {
                CrmService service = GetCrmService(Guid.Empty, organizationName);
                return service;
        }

        public static CrmService GetCrmService(Guid callerID) {
            CrmService rslt = new CrmService();
            if (Convert.ToString(ConfigurationUtils.GetConfigurationItem("crmServiceUrl")) != "")
                rslt.Url = Convert.ToString(ConfigurationUtils.GetConfigurationItem("crmServiceUrl"));
            
            Effect.Crm.SDKProviders.CrmServiceSdk.CrmAuthenticationToken token = new Effect.Crm.SDKProviders.CrmServiceSdk.CrmAuthenticationToken();
            token.AuthenticationType = 0;
            token.OrganizationName = Convert.ToString(ConfigurationUtils.GetConfigurationItem("OrganizationName"));
            token.CallerId = callerID;
            rslt.CrmAuthenticationTokenValue = token;
            rslt.Credentials = GetCredentials();
            return rslt;
        }

        public static CrmService GetCrmService(Guid callerID, string organizationName) {
            CrmService rslt = new CrmService();
            if (Convert.ToString(ConfigurationUtils.GetConfigurationItem("crmServiceUrl")) != "")
                rslt.Url = Convert.ToString(ConfigurationUtils.GetConfigurationItem("crmServiceUrl"));
            Effect.Crm.SDKProviders.CrmServiceSdk.CrmAuthenticationToken token = new Effect.Crm.SDKProviders.CrmServiceSdk.CrmAuthenticationToken();
            token.AuthenticationType = 0;
            token.OrganizationName = organizationName;
            token.CallerId = callerID;
            rslt.CrmAuthenticationTokenValue = token;
            rslt.Credentials = GetCredentials();
            return rslt;
        }

        public static MetadataService GetMetadataService() {
            MetadataService rslt = new MetadataService();
            if (Convert.ToString(ConfigurationUtils.GetConfigurationItem("metadataServiceUrl")) != "")
                rslt.Url = Convert.ToString(ConfigurationUtils.GetConfigurationItem("metadataServiceUrl"));
            Effect.Crm.SDKProviders.MetadataServiceSdk.CrmAuthenticationToken token = new Effect.Crm.SDKProviders.MetadataServiceSdk.CrmAuthenticationToken();
            token.AuthenticationType = 0;
            token.OrganizationName = Convert.ToString(ConfigurationUtils.GetConfigurationItem("OrganizationName"));
            rslt.CrmAuthenticationTokenValue = token;
            rslt.Credentials = GetCredentials();
            return rslt;
        }

        public static MetadataService GetMetadataService(string organizationName) {
                MetadataService metadata = new MetadataService();
                if (Convert.ToString(ConfigurationUtils.GetConfigurationItem("metadataServiceUrl")) != "")
                    metadata.Url = Convert.ToString(ConfigurationUtils.GetConfigurationItem("metadataServiceUrl"));
                Effect.Crm.SDKProviders.MetadataServiceSdk.CrmAuthenticationToken token = new Effect.Crm.SDKProviders.MetadataServiceSdk.CrmAuthenticationToken();
                token.AuthenticationType = 0;
                token.OrganizationName = organizationName;
                metadata.CrmAuthenticationTokenValue = token;
                metadata.Credentials = GetCredentials();
                return metadata;
    
        }
		
        private static void writeToLog(string functionName, string message) {
            using (TextWriter tw = new StreamWriter(@"c:\effectlogs\log2.txt", true)) {
                tw.WriteLine(DateTime.Now + "," + functionName + "," + message);
                tw.Close();
            }
        }

        public static ICredentials GetCredentials() {
            if (Convert.ToString(ConfigurationUtils.GetConfigurationItem("serviceUserName")) != "") {
                NetworkCredential rslt = new NetworkCredential(Convert.ToString(ConfigurationUtils.GetConfigurationItem("serviceUserName")), Convert.ToString(ConfigurationUtils.GetConfigurationItem("servicePassword")), Convert.ToString(ConfigurationUtils.GetConfigurationItem("serviceDomain")));
                return rslt;
            }
            return CredentialCache.DefaultCredentials;
        }
        #endregion

        #region Properties
        #endregion
    }
}
