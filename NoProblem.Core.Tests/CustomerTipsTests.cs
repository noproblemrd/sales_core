﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.PublisherPortal;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class CustomerTipsTests
    {
        [TestMethod]
        public void GetCaseDataForDesktopApp()
        {
            NoProblem.Core.Interfaces.Core.CustomersService c = new Interfaces.Core.CustomersService();
            var r = c.GetCaseDataForDesktopApp(new Guid("133D7C2D-8C3A-4F26-8B25-59E94347F131"));
        }


        [TestMethod]
        public void GetAdvertiserCallHistoryTest()
        {
            NoProblem.Core.Interfaces.Core.CustomersService c = new Interfaces.Core.CustomersService();
            var callHistoryRequest = new GetAdvertiserCallHistoryRequest
            {
                CustomerId = Guid.Parse("5C3965C5-EF20-44B5-8F84-E30027DBAFE2"),
                AdvertiserId = Guid.Parse("8E362741-7FC6-4501-BC5C-E36411CF4394"),
                LeadAccountId = Guid.Parse("BA91D60D-E1AC-E211-9CA6-001517D1792A")
            };
            Result<GetAdvertiserCallHistoryResponse> callHistory = c.GetAdvertiserCallHistory(callHistoryRequest);
        }
    }
}
