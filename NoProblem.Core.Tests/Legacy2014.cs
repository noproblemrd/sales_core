﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    /// <summary>
    /// Unit Tests for the legacy registration process
    /// </summary>
    /// @Authors: Alain Adler
    
    [TestClass]
    public class Legacy2014
    {
        private static Guid supplierId = new Guid("748BB4AB-4BB6-43E3-9646-BAC2452B2D40");//adv: legacy_test_account_dont_touch
        private static string phone = "05433889112341";
        private static string password = "308759";

        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassInitialize()]
        public static void Before(TestContext testContext)
        {
            string strCmdText;
            strCmdText = @"/C ""C:\Program Files (x86)\Cisco Systems\VPN Client\vpnclient.exe"" connect onecall user C70415 pwd h4mm3rh34d!";
            var process = System.Diagnostics.Process.Start("CMD.exe", strCmdText);
            process.WaitForExit();

            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(null);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
            supplier.telephone1 = phone;
            supplier.new_password = password;
            supplier.accountid = supplierId;
            dal.Update(supplier);
            dal.XrmDataContext.SaveChanges();
        }

        //[TestMethod]
        //public void CreateLegacyNoPayment()
        //{
        //    DataAccessLayer.Account dal = new DataAccessLayer.Account(null);
        //    DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
        //    supplier.accountid = supplierId;
        //    supplier.new_steplegacy2014 = null;
        //    supplier.new_stepinregistration2014 = null;
        //    supplier.new_legacyadvertiser2014 = true;
        //    supplier.new_isexemptofppamonthlypayment = false;
        //    dal.Update(supplier);
        //    dal.XrmDataContext.SaveChanges();
        //}

        //[TestMethod]
        //public void CreateLegacyYesPayment()
        //{
        //    DataAccessLayer.Account dal = new DataAccessLayer.Account(null);
        //    DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
        //    supplier.accountid = supplierId;
        //    supplier.new_steplegacy2014 = null;
        //    supplier.new_stepinregistration2014 = null;
        //    supplier.new_legacyadvertiser2014 = true;
        //    supplier.new_isexemptofppamonthlypayment = true;
        //    supplier.new_participatesinservicerequests = true;
        //    supplier.new_subscriptionplan = DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING;
        //    dal.Update(supplier);
        //    dal.XrmDataContext.SaveChanges();
        //}

        [TestMethod]
        public void TestNotLegacy()
        {
            Interfaces.Core.Supplier s = new Interfaces.Core.Supplier();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(null);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_steplegacy2014 = null;
            supplier.new_stepinregistration2014 = null;
            supplier.new_legacyadvertiser2014 = null;
            dal.Update(supplier);
            dal.XrmDataContext.SaveChanges();
            var res = s.PhoneLogIn_Legacy2014(
                new DataModel.SupplierModel.Registration2014.Legacy.PhoneLogInRequest()
                {
                    Phone = phone,
                    Password = password
                });
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.LegacyStatuses.NOT_LEGACY_SEND_TO_EMAIL_LOGIN, res.Value.LegacyStatus);            
        }

        [TestMethod]
        public void TestWrongPassword()
        {
            Interfaces.Core.Supplier s = new Interfaces.Core.Supplier();
            var res = s.PhoneLogIn_Legacy2014(
                new DataModel.SupplierModel.Registration2014.Legacy.PhoneLogInRequest()
                {
                    Phone = phone,
                    Password = password + "1"
                });
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.LegacyStatuses.PHONE_PASSWORD_COMBINATION_NOT_FOUND, res.Value.LegacyStatus);
        }

        [TestMethod]
        public void TestLegacy_loginPhone_OK()
        {
            string contactName = "legacy tester";
            string companyName = "legacy_test_account_dont_touch";
            Interfaces.Core.Supplier s = new Interfaces.Core.Supplier();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(null);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_steplegacy2014 = null;
            supplier.new_stepinregistration2014 = null;
            supplier.new_legacyadvertiser2014 = true;
            supplier.new_facebookid = "stam";
            supplier.new_googleid = null;
            supplier.name = companyName;
            supplier.new_fullname = contactName;
            supplier.new_subscriptionplan = null;
            supplier.new_cashbalance = null;
            dal.Update(supplier);
            dal.XrmDataContext.SaveChanges();
            var res = s.PhoneLogIn_Legacy2014(
                new DataModel.SupplierModel.Registration2014.Legacy.PhoneLogInRequest()
                {
                    Phone = phone,
                    Password = password
                });
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.LegacyStatuses.SEND_TO_LEGACY_STEP, res.Value.LegacyStatus);
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.CONFIRM_EMAIL_1, res.Value.StepLegacy);
            Assert.AreEqual<Guid>(supplierId, res.Value.SupplierId);
            Assert.AreEqual<string>(contactName, res.Value.ContactName, "contact name");
            Assert.AreEqual<string>(companyName, res.Value.Name, "company name");
            Assert.IsNull(res.Value.SubscriptionPlan);
            Assert.AreEqual<decimal>(0, res.Value.Balance);
        }

        [TestMethod]
        public void TestLegacy_loginPhone_already_finished()
        {
            Interfaces.Core.Supplier s = new Interfaces.Core.Supplier();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(null);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_steplegacy2014 = DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.FINISHED_3;
            supplier.new_stepinregistration2014 = (int)DataModel.Xrm.account.StepInRegistration2014.Dashboard_7;
            supplier.new_legacyadvertiser2014 = true;
            dal.Update(supplier);
            dal.XrmDataContext.SaveChanges();
            var res = s.PhoneLogIn_Legacy2014(
                new DataModel.SupplierModel.Registration2014.Legacy.PhoneLogInRequest()
                {
                    Phone = phone,
                    Password = password
                });
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.LegacyStatuses.LEGACY_FINISHED_SEND_TO_EMAIL_LOGIN, res.Value.LegacyStatus);
        }

        [TestMethod]
        public void TestLegacy_loginPhone_pass_to_normal()
        {
            Interfaces.Core.Supplier s = new Interfaces.Core.Supplier();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(null);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_steplegacy2014 = DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.FINISHED_3;
            supplier.new_stepinregistration2014 = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
            supplier.new_legacyadvertiser2014 = true;
            dal.Update(supplier);
            dal.XrmDataContext.SaveChanges();
            var res = s.PhoneLogIn_Legacy2014(
                new DataModel.SupplierModel.Registration2014.Legacy.PhoneLogInRequest()
                {
                    Phone = phone,
                    Password = password
                });
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.LegacyStatuses.SEND_TO_NORMAL_STEP, res.Value.LegacyStatus);
            Assert.AreEqual<int>((int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2, res.Value.Step);
            Assert.AreEqual<Guid>(supplierId, res.Value.SupplierId);
        }

        [TestMethod]
        public void TestLegacy_EmailVerifiyLegacyPreLoad()
        {
            Interfaces.Core.Supplier s = new Interfaces.Core.Supplier();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(null);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_steplegacy2014 = null;
            supplier.new_stepinregistration2014 = null;
            string email = "alain@onecall.co.il1243";
            supplier.emailaddress1 = email;
            supplier.new_legacyadvertiser2014 = true;
            dal.Update(supplier);
            dal.XrmDataContext.SaveChanges();
            var res = s.EmailVerifiyLegacyPreLoad_Legacy2014(supplierId);
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.LegacyStatuses.SEND_TO_LEGACY_STEP, res.Value.LegacyStatus);
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.CONFIRM_EMAIL_1, res.Value.StepLegacy);
            Assert.AreEqual<String>(email, res.Value.Email);
        }

        [TestMethod]
        public void TestLegacyNoPayment_EmailVerified()
        {
            Interfaces.Core.Supplier s = new Interfaces.Core.Supplier();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(null);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_steplegacy2014 = null;
            supplier.new_stepinregistration2014 = null;
            supplier.new_legacyadvertiser2014 = true;
            supplier.new_isexemptofppamonthlypayment = null;
            dal.Update(supplier);
            dal.XrmDataContext.SaveChanges();
            var res = s.EmailVerified_Legacy2014(supplierId);
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.LegacyStatuses.SEND_TO_NORMAL_STEP, res.Value.LegacyStatus);
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.FINISHED_3, res.Value.StepLegacy);
            Assert.AreEqual<int>((int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2, res.Value.Step);
        }

        [TestMethod]
        public void TestLegacyYesPayment_EmailVerified()
        {
            Interfaces.Core.Supplier s = new Interfaces.Core.Supplier();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(null);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_steplegacy2014 = null;
            supplier.new_stepinregistration2014 = null;
            supplier.new_legacyadvertiser2014 = true;
            supplier.new_isexemptofppamonthlypayment = true;
            dal.Update(supplier);
            dal.XrmDataContext.SaveChanges();
            var res = s.EmailVerified_Legacy2014(supplierId);
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.LegacyStatuses.SEND_TO_LEGACY_STEP, res.Value.LegacyStatus);
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.PAYMENT_METHOD_2, res.Value.StepLegacy);
            Assert.AreEqual<int>((int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2, res.Value.Step);
        }

        [TestMethod]
        public void TestLegacyYesPayment_UpdatePaymentMethodOnLoad_ok()
        {
            Interfaces.Core.Supplier s = new Interfaces.Core.Supplier();
            DataAccessLayer.AccountRepository dal = new DataAccessLayer.AccountRepository(null);
            DataModel.Xrm.account supplier = new DataModel.Xrm.account(dal.XrmDataContext);
            supplier.accountid = supplierId;
            supplier.new_steplegacy2014 = DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.PAYMENT_METHOD_2;
            supplier.new_stepinregistration2014 = (int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2;
            supplier.new_legacyadvertiser2014 = true;
            supplier.new_isexemptofppamonthlypayment = true;
            string email = "alain@onecall.co.il1243";
            supplier.emailaddress1 = email;
            string contactName = "legacy tester";
            supplier.new_fullname = contactName;
            string companyName = "legacy_test_account_dont_touch";
            supplier.name = companyName;
            supplier.new_subscriptionplan = DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING;
            dal.Update(supplier);
            dal.XrmDataContext.SaveChanges();
            var res = s.UpdatePaymentMethodOnLoad_Legacy2014(supplierId);
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.LegacyStatuses.SEND_TO_LEGACY_STEP, res.Value.LegacyStatus);
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.Legacy.StepsLegacy.PAYMENT_METHOD_2, res.Value.StepLegacy);
            Assert.AreEqual<int>((int)DataModel.Xrm.account.StepInRegistration2014.ChoosePlan_2, res.Value.Step);
            Assert.AreEqual<String>(contactName, res.Value.ContactName);
            Assert.AreEqual<String>(email, res.Value.Email);
            Assert.AreEqual<String>(DataModel.SupplierModel.Registration2014.SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING, res.Value.SubscriptionPlan);
            Assert.AreEqual<String>(companyName, res.Value.Name);
        }
    }
}
