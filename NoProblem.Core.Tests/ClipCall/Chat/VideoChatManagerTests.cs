﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic.ClipCall.Chat;
using NoProblem.Core.DataAccessLayer.Bases;
using NoProblem.Core.DataModel;
using OpenTokSDK;
using System.Threading;
using NoProblem.Core.BusinessLogic.ClipCall;

namespace NoProblem.Core.Tests.ClipCall.Chat
{
    [TestClass]
    public class VideoChatManagerTests
    {

        private VideoChatManager sut;

        [TestInitialize]
        public void Setup()
        {
            var dbContext = XrmDataContext.Create();
            int apiKey = int.Parse(ConfigurationManager.AppSettings["openTokApiKey"]);
            string secret = ConfigurationManager.AppSettings["openTokSecret"];

            sut = new VideoChatManager(new OpenTok(apiKey, secret), new Repository(dbContext));
        }


        [TestMethod]
        public void CreateInvitation()
        {
            var supplierId = Guid.Parse("84300890-2CA4-46E6-A765-BA5B0419FE4B");//84300890-2CA4-46E6-A765-BA5B0419FE4B - RegisQA mobile new 18
            sut.CreateVideoChatInvitation(supplierId, global::ClipCall.DomainModel.Entities.VideoInvitation.Request.CreateInvitationRequest.eInvitationType.VIDEO);
        }
        //VideoSession AcceptVideoChatInvitation(Guid customerId, Guid invitationId)
        [TestMethod]
        public void AcceptVideoChatInvitation()
        {
            var customerId = Guid.Parse("0AB94D9F-DC8B-4202-9799-7728619EF9E0");
            var invitationId = new Guid("ccd036d9-f6e0-43ba-a845-8fda09d3d24e");
            var vs = sut.InvitationCheckIn(invitationId);
            int t = 0;
        }
        // VideoSession JoinVideoChatConversation(Guid customerId,  Guid invitationId)
        [TestMethod]
        public void JoinVideoChatConversation()
        {
            var customerId = Guid.Parse("0AB94D9F-DC8B-4202-9799-7728619EF9E0");
            var invitationId = new Guid("ccd036d9-f6e0-43ba-a845-8fda09d3d24e");
            var vs = sut.CreateVideoChatCall(customerId, invitationId);
            
            int t = 0;
        }
        [TestMethod]
        public void OpentokArchiveStatusChanges()
        {
            Guid archiveId = new Guid("9283fba2-2d97-4079-8e42-65b8c8ef5939");
            string sessionId = "2_MX40NTQzNjA2Mn5-MTQ1MTg0MTk5MzYzN34wTDFuSU9iWFlwSVRTUVUxUUhOWGhGSll-fg";
            string videoUrl = "";
            long duration = 5400;
            string status = "uploaded";
            int partnerId = 45436062;

            sut.OpentokArchiveStatusChanges(archiveId, partnerId, sessionId, videoUrl, duration, status);
            int t = 0;
        }
        /*
        [TestMethod]
        public void GetArchiveDetails()
        {
            Guid archiveId = new Guid("9283fba2-2d97-4079-8e42-65b8c8ef5939");
            string sessionId = "1_MX40NTQzNjA2Mn5-MTQ1MTg5NTcxMzI2Mn5rV3BSSWVONXJqcTMvcExwV3BxQTFNVDd-fg";

            var result = sut.GetRecording(archiveId.ToString());
            int t = 0;
        }
         * */
        [TestMethod]
        public void checkPushOfir()
        {
            VideoChatJoinPushNotification noti = new VideoChatJoinPushNotification(new Guid("C07B48EC-7509-4322-A1EE-033A2FD7EC53"), "+(1) 0543388914",
                "http://stam", new Guid("C07B48EC-7509-4322-A1EE-033A2FD7EC53"), 1, "2_MX40NTQzNjA2Mn5-MTQ1MjA2OTk2NDAwOX5WUE92aFlkMmVhMy95ZUg2V3gxNjU5WUt-fg",
                "T1==cGFydG5lcl9pZD00NTQzNjA2MiZzaWc9YTM2ODBkMTAzYzhmZWNjMjJhZDQ3MGUwNzYxYTM3Y2ExNGJmZWMzNTpzZXNzaW9uX2lkPTJfTVg0ME5UUXpOakEyTW41LU1UUTFNakEyT1RrMk5EQXdPWDVXVUU5MmFGbGtNbVZoTXk5NVpVZzJWM2d4TmpVNVdVdC1mZyZjcmVhdGVfdGltZT0xNDUyMDY5OTYzJm5vbmNlPTk4MzAyMSZyb2xlPVBVQkxJU0hFUg==");
            noti.SendAsyncNotification();
            int t = 0;
        }
        [TestMethod]
        public void GetArchiveDetails()
        {
            var vr = sut.GetRecording("51d91f9a-aab2-4ae1-b6df-47719238a690");
            int t = 0;
        }
    }


}
