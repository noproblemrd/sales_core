﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class GoogleCloudMessaging
    {
        [TestMethod]
        public void TestMethod1()
        {
            BusinessLogic.PushNotifications.Android.GoogleCloundMessagingServer server = new BusinessLogic.PushNotifications.Android.GoogleCloundMessagingServer();
            BusinessLogic.PushNotifications.Android.AndroidPushNotification notification =
                new BusinessLogic.PushNotifications.Android.AndroidPushNotification();
            notification.RegistrationIds.Add("balala");
            notification.Data =
                new { minvalue = 34, description = "hello, world!" };
            notification.DryRun = true;
            var result = server.SendPushNotification(notification);
        }
    }
}
