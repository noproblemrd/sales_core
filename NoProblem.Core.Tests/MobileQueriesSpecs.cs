﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI.MobileControls;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic.ClipCall.Suppliers;
using NoProblem.Core.BusinessLogic.CreditCardSolek;
using NoProblem.Core.DataAccessLayer;
using NoProblem.Core.DataAccessLayer.Bases;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.APIs.MobileApi;
using NoProblem.Core.DataModel.SupplierModel.Mobile;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class MobileQueriesSpecs
    {
        [TestMethod]
        public void GetSupplierLeads()
        {
            var mail = new NoProblem.Core.BusinessLogic.Notifications(null);
            mail.SendEmail("yoave@noproblem.me", "stam", "stam");
            /*
            var m = new BusinessLogic.SupplierBL.MobileQueries();
            var request = new LeadsListRequest
            {
                PageNumber = 0,
                PageSize = 100,
                SupplierId = Guid.Parse("47f94628-352a-45a9-8519-76579f0ae494"),
                supplierLeadStatus = LeadsListRequest.SupplierLeadStatus.NEW
            };
            LeadsListResponse response = m.GetSupplierLeads(request);
             * */
        }

        [TestMethod]
        public void GetSupplierLead()
        {
            var m = new BusinessLogic.SupplierBL.MobileQueries();
            var supplierId = Guid.Parse("47f94628-352a-45a9-8519-76579f0ae494");
            var leadId = Guid.Parse("8ac8c99f-bd41-e511-9372-001517d1792a");
            GetLeadForMobileResponse lead = m.GetLead(supplierId, leadId);
        }

        [TestMethod]
        public void RemoveCategory()
        {
            var dbContext = XrmDataContext.Create();
            var incidentAccountRepository = new IncidentAccount(dbContext);
            var accountRepository = new AccountRepository(dbContext);
            var repository = new Repository(dbContext);
            var manager = new SuppliersManager(incidentAccountRepository, accountRepository, repository);
            var supplierId = Guid.Parse("685af426-2515-46a2-96cd-8f1f1d1abbce");
            var categoryId = Guid.Parse("01fcc66a-4124-e311-a119-001517d1792a");
            bool result = manager.RemoveSupplierCategory(supplierId, categoryId);
        }
    }

     [TestClass]
    public class PaymentSpecs
    {
         [TestMethod]
         public void GetSupplierLead()
         {
             var paymentManager = new BusinessLogic.SupplierBL.Registration2014.PaymentMethodManager(null);

            // {
            //        "supplierId": "47f94628-352a-45a9-8519-76579f0ae494",
            //        "paymentMethod": {
            //            "encryptedCreditCardNumber": "",
            //            "creditCardLastFourDigits": "",
            //            "encryptedCvv": "",
            //            "billingFirstName": "",
            //            "billingLastName": "",
            //            "creditCardType": "",
            //            "creditCardExpirationYear": 2015,
            //            "creditCardExpirationMonth": 8,
            //            "billingAddress": "",
            //            "billingPhone": "",
            //            "billingCity": "",
            //            "billingState": "",
            //            "billingZip": ""
            //        }
            //}
             var request = new CreatePaymentMethodRequest
             { 
                 SupplierId = Guid.Parse("47f94628-352a-45a9-8519-76579f0ae494"),
                 BillingFirstName = "shai",
                 BillingAddress = "MY ROAD",
                 BillingCity = "TEL AVIV",
                 BillingLastName = "Perez",
                 BillingState = "AL",
                 BillingZip = "12345",
                 CreditCardType = "VISA",
                 ExpirationYear = 2016,
                 BillingEmail = "shai.perez@gmail.com",
                 ExpirationMonth = 10,
                 EncryptedCreditCardNumber = "$bsios_0_0_1$La6axpbln+NnUE/34Sg9pbJw+QGkoPCr9UdSm/4Y6jcq4SFImIrssxSfX5cT2+GQ1wrMZ+5EMvoSJU4mvQ97HyrZjSo5iCmeE2vjQGVIZK/sNzph6zOEgUuoFqJqvLHbhByYb5kzAFM4SUIyTtVW3q88Z+dxR9B6Ydv2dkQ2sGao0qB7BPhRr2oOhUA6fW4jBoFaOZaDBPFdXn5pE4k3zIckcyfFipIg2IGTQAVEjaa9xWp4X+MjKemDYwyZqFC2b8qssD8ZOGVoc+6G9EPps72tRT8qTUDZiUU5fW+vVx5l4uOdz0kEabxz0sfwtKRq1Z/gh19MRViu0tm5aXg8gw==$Sr8ZfEZ9TQ0FlVSmrXGelg8sxI5WVPDY5uEfqUxVfE+iEaUmLnUcC5wfy9AoIPkk$NMcNBqs7PyS+O+FdHHauDpJOedTGjS/UH8XX/GN0cVg=",
                 UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36",
                 UserIP = "74.54.87.132",
                 Last4Digits = "2822",
                 UserHost = "74.54.87.132",
                 EcryptedCvv = "$bsios_0_0_1$efts+xMOfsF+S9jfPhPIjvfmBxFdyIZxo73XPlcRLILCkbo2C2LtXriApg6HdsosgUcyBhHdMq71fYdm041KhxaDTWQkihXohkXdvpibyBmz2k7cguTWctAvuZttbXwHkDoiv81zgXZasPGz/xL9kyvZLH5aowTyKKRnzmztVdHkhXKyQ1MoVNmG0BUK4elXuc2a7PCVOVBeya16o2G6mQtHHDllcfSAyy0yOWmnfDWX3lNZaMBcS/pYYq3T5nS+yqKwkxZtUL1zCminbX/8o/VVzG+LzWQNhwKQl/8dvyqSKK1iU60968eVNw7ukX20gNvvfMLJ9ih57GGPszEdrw==$Bwdn04pbZEd/xuIukqRtr/iORAcUJ9u1oHxLZzhRIiE=$EoVmqQCdbSQGwaWJRgG/MwqubFwKUx/ZOqufHmDzR1Q="
             };
             bool success = paymentManager.CreatePaymentMethod(request);
             Assert.IsTrue(success);
         }

            [TestMethod]
         public void TestOrder()
         {
             var order = new PlimusOrder
             {
                 SoftDescription = "NoProblem",
                 OrderingShopper = new OrderingShopper
                 {
                     CreditCard = new CreditCardRef
                     {
                         LastFourDigits = "1114",
                         Type = "VISA"
                     },
                     ShopperId = "20255231",
                     WebInfo = new WebInfo
                     {
                         UserAgent =
                             "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36",
                         Ip = "10.10.10.10",
                         RemoteHost = "localhost"
                     },
                     

                 },
                 Cart = new Cart
                 {
                     Item = new CartItem
                     {
                         Quantity = 1,
                         
                         Sku = new Sku
                         {
                             Id = "2148776",
                             ChargePrice = new SkuChargePrice
                             {
                                 Amount = 10,
                                 ChargeType = "initial",
                                 Currency = "USD"
                             }
                         },
                         Parameters = new List<SkuParameter>
                         {
                             new SkuParameter
                             {
                                 Name = "ifRecharger",
                                 Value = "TRUE"
                             },
                             new SkuParameter
                             {
                                 Name = @"I agree to the &lt;a href=http://www2.noproblemppc.com/LegalAdvTerms.htm target=_new &gt;Terms of service&lt;/a&gt;",
                                 Value = "Y"
                             }
                         }
                     }
                 }
             };

             string value = Serialize(order);
             Assert.IsNotNull(value);


         }

         public static string Serialize<T>(T value)
         {

             if (value == null)
             {
                 return null;
             }

             
             var ns = new XmlSerializerNamespaces();
             ns.Add("", "http://ws.plimus.com");
             var serializer = new XmlSerializer(typeof(T));

             var settings = new XmlWriterSettings
             {
                 Encoding = new UnicodeEncoding(false, false),
                 Indent = false,
                 OmitXmlDeclaration = true
             };

             using (var textWriter = new StringWriter())
             {
                 using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                 {
                     serializer.Serialize(xmlWriter, value, ns);
                 }
                 return textWriter.ToString();
             }
         }

    }
}
