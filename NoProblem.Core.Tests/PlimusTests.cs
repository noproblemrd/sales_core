﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic.Balance;
using NoProblem.Core.BusinessLogic.SupplierBL.Registration2014;
using NoProblem.Core.DataModel.APIs.MobileApi;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class PlimusTests
    {
        [TestMethod]
        public void CreateShopper()
        {

            Guid supplierId = Guid.Parse("27053649-e1e4-4b4c-8350-cc3df20ed0c0");
            var paymentMethodManager = new PaymentMethodManager(null);
            var request = new CreatePaymentMethodRequest
            {
                BillingAddress = "11",
                BillingCity = "11",
                BillingZip = "12201",
                BillingState = "NY",
                BillingLastName = "Meh",
                BillingFirstName = "Moh",
                BillingEmail = "dorona@noproblem.me",
                CreditCardType = "MASTERCARD",
                EcryptedCvv =
                    "$bsios_0_0_1$qw6EqzIUl0aO9blgypXzYz3EJ+m0UceryBiJt8Z6c4G4YxjI0VP474YP2IpWKyn1wphow1PAZQqx6tE7EmAxwAhOb3mIWyAeHsLkjGLEmTVVIltlT82srFONcKTu3WjGN/RJqn/CI5ck7bUFKoEfnDJ2E5MCxhbBpIsRFL6h6Lhvyh1R1NlfAIjEupwI/Jwn4pHcSEha+zPD/DN5zxZNpG0aWRmTRpyojdzTGKxoUyuHwRmHi36zQUsk6ZK4s8fKzebOD9Oe7jUq7S4JQqz1surdGGsnWA98TlujxjQwuKzYAgUW+IgGZMtTx0zxA/J2c8vaYFjAuhqvwmD1WbeRmA==$eTY1oHP8NiPBqBWtjxntkPAC26bXTUChZnvT21QwCdY=$Uwx8jYeE/c3t+EvoUXBPqQ0dn7i4kViRjQwn5quUYXk=",
                EncryptedCreditCardNumber =
                    "$bsios_0_0_1$Qmv+dYzPP4NLy1cFhI0T2uSWzV9uICBJ74Zztw70DyY7mxA7ovm+2wLgqrzuwdtxAC4ZNSAh2B0iRYxIvzVicZkvek3xc4F+V8WVGgwh4mM5HJO5mTAy/OCEVkIjvO0OBU5szDkvyJ7H4MwM7Z5XHZygLM05vvPPWJm4H36hsomXY7FRt25vm2XCP79uIRHnSXMZCIrRKxOKwH7wTBdX2W70bOZgT+USP/9Qbw1/2HGyH7vKrz6tyd+qHhWLiRLCVZa6gec3jZiVGMyF01lrYp+tTildM8aQInW/gAsO+uoP6/RdMam7/72CIwYg+nd33/mOJ7A6LZpIxH4kaCIftQ==$HF1wUs4r5MDhTQUnXFv7xIo6Xd9SWS//vM8ejIDBQjn6JozXpEcMRe/C4ZskFH3B$VfTbtGKHjpg4+Ps712J3CHR2/9IVHAYwwqK9xo9+Tec=",
                ExpirationMonth = 1,
                ExpirationYear = 2016,
                Last4Digits = "3052",
                SupplierId = supplierId,
                UserIP = "62.219.121.253",
                UserAgent =
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36",
                UserHost = "doron.noproblem.com"
            };
            bool createdSuccessfully = paymentMethodManager.CreatePaymentMethod(request);
            Assert.IsTrue(createdSuccessfully);
            Guid leadAccountId = Guid.Parse("9A6DE68F-BBA7-461F-AA82-D34C36FE0FA3");
            var balanceManager = new BalanceManager(null);
            balanceManager.ChargeLead(supplierId, 10, new_balancerow.Action.Interval, leadAccountId,"testing withdrawing");


            //BusinessLogic.CreditCardSolek.Plimus plimus = new BusinessLogic.CreditCardSolek.Plimus();
            //string shopperId = plimus.CreateShopper(
            //    new BusinessLogic.CreditCardSolek.Solek.CreateShopperData()
            //    {
            //        IP = "62.219.121.253",
            //        ShopperFirstName = "John",
            //        ShopperLastName = "Doe",
            //        ShopperEmail = "jdoe@johndoeandsons.com",
            //        ShopperCompanyName = "JohnDoeAndSons",
            //        ShopperStreetAddress = "138 Market st",
            //        ShopperCity = "San Francisco",
            //        ShopperZip = "756543",
            //        ShopperState = "CA",
            //        ShopperPhone = "1413555666",
            //        ShopperFax = "1413555666789",
            //        BillingFirstName = "John",
            //        BillingLastName = "Doe",
            //        BillingStreetAddress = "138 Market st",
            //        BillingCity = "San Francisco",
            //        BillingZip = "756543",
            //        BillingState = "CA",
            //        EncryptedCardNumber = "%24bsjs_1_0_1%24ZRFrrwHOgG%2bRTUBEn1m%2bW%2fpHkyi9VLz2IGcxVLBuoL12Qkh%2bbEjQSux5VL5L75%2fnShOqOPZds67TPhqUXmU%2bFngjDWkqwsCtELmVjcXP8YIM3wDJUDzQmPAZNXXJ%2fViQg78uWWdFg3i3Z%2bp7FM1hF5yWMZtv6TFIQQ19wjBbuqUNhoOeUsiCBtpoH%2b%2fZKv4Fyi2Xw0gh1u6I5M0LBNODq5AOS6JBf7HqFoiYjsxqUXIckAoVbWfGOiA786LfChgXMrvGJGZN3aymKpFeCDqF7ntUzjQoczurf%2bV5rMEsljO7RP7IKwEHQ6UUuYiwKGpwzm%2fH0vFLldVrJ5nocZlzfw%3d%3d%24kGNk%2f7q71MhVC89p8olKDksvh7xhzf7jvla7ovyZiKxu%2fL9oTV5sYxSa0V%2bdVYS5%24Pua0xkk1GMaoVRrWEzO0w7nduwuM7JH%2f%2fWBeLf%2fjQaE%3d",
            //        EncryptedCVV = "%24bsjs_1_0_1%24tpAj%2b1XXAs2mU4baU7%2bP5HzWpBHvW%2bOXCMnGblY%2fBxuKoYqxZGSI2XTLBaM3mCdArnYDPg%2fGL7RzKYuSmbvgIK0wIRp%2blI6JJCBJa%2bOA7RKnlmMvSnRKd4mFuouvyJ8RiloQgulS2Dme4BdDfiLkF2Qcm%2bOVuz4UREaAtc6pgYCZZvYTTQ8u4o4uiaJswpACPgl7vMwPYR2oc7i%2blsk0VCTbo6%2fZ%2fBqWxZ03MV5FTNjvDsz6egAkxhoDKoTt9w93m0K7HUsjQYdClicDuIo6qtxvwuQ0c3jC7XMzgph9%2bq%2fe%2fUn2YM02FPIbpr%2f03ESE4ci1K04ETZmZs4QRGx2x8w%3d%3d%24tXPRDHCpcQi0NFLRAH1FmCB8TbtcxzQxBLgdY7wVYks%3d%241VQXdtDBKFZiFqKutOfnX25aM9NHHQE2qzH7qfKUXUo%3d",
            //        CreditCardType = "VISA",
            //        ExpirationMonth = "09",
            //        ExpirationYear = 2019
            //    });
            //Assert.IsNotNull(shopperId);
        }
    }
}
