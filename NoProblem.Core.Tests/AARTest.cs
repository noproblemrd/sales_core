﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic.AAR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class AARTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(null);
            DataModel.Xrm.incident _incident = incidentDal.Retrieve(new Guid("ADDF1A04-4BE0-43E9-8883-B24721E47519"));
            DataModel.Xrm.new_primaryexpertise category = _incident.new_new_primaryexpertise_incident;
            DataAccessLayer.Region regionDal = new DataAccessLayer.Region(null);
            var parents = regionDal.GetParents(_incident.new_new_region_incident);
            var city = parents.First(x => x.new_level == 3);
            Guid cityId = city.new_regionid;
            var state = parents.First(x => x.new_level == 1);
            string locationName = city.new_name + ", " + state.new_name;
            CitygridApiHelper api = new CitygridApiHelper(category, _incident, locationName);
            api.GetSuppliersFromApi();
        }
        [TestMethod]
        public void TestMethod2()
        {
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(null);
            DataModel.Xrm.incident _incident = incidentDal.Retrieve(new Guid("F839870D-E834-4BC4-BE9A-5B77C86D669F"));
            DataModel.Xrm.new_primaryexpertise category = _incident.new_new_primaryexpertise_incident;
            DataAccessLayer.Region regionDal = new DataAccessLayer.Region(null);
            var parents = regionDal.GetParents(_incident.new_new_region_incident);
            var city = parents.First(x => x.new_level == 3);
            Guid cityId = city.new_regionid;
            var state = parents.First(x => x.new_level == 1);
            string locationName = city.new_name + ", " + state.new_name;
            ypPlacesManager api = new ypPlacesManager(null, _incident);
            api.GetSuppliersFromApi();
        }
        [TestMethod]
        public void TestMethod3()
        {
            DataAccessLayer.Incident incidentDal = new DataAccessLayer.Incident(null);
            DataModel.Xrm.incident _incident = incidentDal.Retrieve(new Guid("DA3E8F5C-14DA-4E23-821F-AE99FB0996C4"));
            DataModel.Xrm.new_primaryexpertise category = _incident.new_new_primaryexpertise_incident;
            DataAccessLayer.Region regionDal = new DataAccessLayer.Region(null);
            var parents = regionDal.GetParents(_incident.new_new_region_incident);
            var city = parents.First(x => x.new_level == 3);
            Guid cityId = city.new_regionid;
            var state = parents.First(x => x.new_level == 1);
            string locationName = city.new_name + ", " + state.new_name;
            CitygridPlacesManager api = new CitygridPlacesManager(null, _incident);
            api.GetSuppliersFromApi();
        }
        [TestMethod]
        public void TestMethod4()
        {
            string s = NoProblem.Core.BusinessLogic.SmsEngine.SmsUtility.GetCleanPhoneFromTwillio("%2B18557456655");
            string s2 = System.Web.HttpUtility.UrlDecode("%2B1760");
            int t = 0;
        }
        [TestMethod]
        public void TestMethod5()
        {
            AarManager am = new AarManager(null);
            AarCallIncidentData response = am.GetSupplierByCallId(Guid.Empty);
            int t = 0;
        }

    }
}
