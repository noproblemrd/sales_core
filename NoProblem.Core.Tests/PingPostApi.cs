﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class PingPostApi
    {
        [TestMethod]
        public void Ping1()
        {
            BusinessLogic.APIs.SellersApi.PingPost.PingManager m = new BusinessLogic.APIs.SellersApi.PingPost.PingManager();
            var r = m.Ping(
                new DataModel.APIs.SellersApi.PingPost.PingRequest()
                {
                    CategoryCode = "2090",
                    LeadId = "bla6",
                    SellerId = new Guid("43EBFE7D-DF6C-4DB7-A73D-7B2D18197960"),
                    Zip = "10001"
                });
        }

        [TestMethod]
        public void ValidatePingId()
        {
            Assert.IsFalse(BusinessLogic.APIs.SellersApi.PingPost.PingIdValidator.IsNpPingIdValidForPost("549ab8713c456d0d60dd6c40"));
            Assert.IsTrue(BusinessLogic.APIs.SellersApi.PingPost.PingIdValidator.IsNpPingIdValidForPost("549ab27d3c456d13247f8297"));
        }

        [TestMethod]
        public void Post1()
        {
            BusinessLogic.APIs.SellersApi.PingPost.PostManager m = new BusinessLogic.APIs.SellersApi.PingPost.PostManager();
            var r = m.Post(new DataModel.APIs.SellersApi.PingPost.PostRequest()
            {
                FirstName = "Alain",
                LastName = "Adler",
                Email = "al@dads.com",
                NpPingId = "549fff623c456d14d8e87fe4",
                Phone = "123234",
                SellerId = new Guid("43EBFE7D-DF6C-4DB7-A73D-7B2D18197960"),
                StreetAddress = null
            });
        }
    }
}
