﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class DesktopAppCallsTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            NoProblem.Core.Interfaces.Core.Deskapp _deskapp = new NoProblem.Core.Interfaces.Core.Deskapp();
            NoProblem.Core.DataModel.Deskapp.CallRecordRequest request = new NoProblem.Core.DataModel.Deskapp.CallRecordRequest();
            request.ContactId = new Guid("74a6aaa3-ae6a-45cd-8f27-f4932967c54d");
            request.BobPhone = "0543388911";
            DataModel.Result<NoProblem.Core.DataModel.Deskapp.CallRecordResponse> result = _deskapp.RecordCall(request);

        }
    }
}
/*
 *  public Guid ContactId { get; set; }
        public string BobPhone { get; set; }
        public bool BlockCallerId { get; set; }
 * */
