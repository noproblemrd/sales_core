﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class PhoneTrunkCountryConfigTest
    {
        [TestMethod]
        public void SmsConfigCountry()
        {
            Assert.AreEqual<String>("+972543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToSmsNormalizer.NormalizePhone("0543388911", DataModel.Constants.Countries.ISRAEL));
            Assert.AreEqual<String>("+449876543210", NoProblem.Core.BusinessLogic.Dialer.PhoneToSmsNormalizer.NormalizePhone("9876543210", DataModel.Constants.Countries.UNITED_KINGDOM));
            Assert.AreEqual<String>("+19876543210", NoProblem.Core.BusinessLogic.Dialer.PhoneToSmsNormalizer.NormalizePhone("9876543210", DataModel.Constants.Countries.UNITED_STATES));
            Assert.AreEqual<String>("0543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToSmsNormalizer.NormalizePhone("0543388911", "China"));
            Assert.AreEqual<String>(null, NoProblem.Core.BusinessLogic.Dialer.PhoneToSmsNormalizer.NormalizePhone(null, null));
            Assert.AreEqual<String>("+449876543210", NoProblem.Core.BusinessLogic.Dialer.PhoneToSmsNormalizer.NormalizePhone("09876543210", DataModel.Constants.Countries.UNITED_KINGDOM));
        }

        [TestMethod]
        public void CheckCountryTrunkData()
        {
            var isr012 = NoProblem.Core.BusinessLogic.Dialer.DialerCountryTrunkConfiguration.GetConfiguration(DataModel.Constants.Countries.ISRAEL, DataModel.Constants.Trunks.ISR_012);
            Assert.AreEqual<string>("", isr012.AddPrefix);
            Assert.AreEqual<string>("", isr012.RemovePrefix);

            var shtuyot = NoProblem.Core.BusinessLogic.Dialer.DialerCountryTrunkConfiguration.GetConfiguration("china", "014");
            Assert.AreEqual<string>("", shtuyot.AddPrefix);
            Assert.AreEqual<string>("", shtuyot.RemovePrefix);

            var isrx = NoProblem.Core.BusinessLogic.Dialer.DialerCountryTrunkConfiguration.GetConfiguration(DataModel.Constants.Countries.ISRAEL, DataModel.Constants.Trunks.XCASTLABS);
            Assert.AreEqual<string>("011972", isrx.AddPrefix);
            Assert.AreEqual<string>("0", isrx.RemovePrefix);

            var ukx = NoProblem.Core.BusinessLogic.Dialer.DialerCountryTrunkConfiguration.GetConfiguration(DataModel.Constants.Countries.UNITED_KINGDOM, DataModel.Constants.Trunks.XCASTLABS);
            Assert.AreEqual<string>("01144", ukx.AddPrefix);
            Assert.AreEqual<string>("0", ukx.RemovePrefix);
        }

        [TestMethod]
        public void CheckPhoneToDialNormalizer()
        {
            Assert.AreEqual<String>("0543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToDialNormalizer.NormalizePhone("0543388911", DataModel.Constants.Countries.ISRAEL, DataModel.Constants.Trunks.ISR_012));
            Assert.AreEqual<String>("012449543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToDialNormalizer.NormalizePhone("9543388911", DataModel.Constants.Countries.UNITED_KINGDOM, DataModel.Constants.Trunks.ISR_012));
            Assert.AreEqual<String>("012449543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToDialNormalizer.NormalizePhone("09543388911", DataModel.Constants.Countries.UNITED_KINGDOM, DataModel.Constants.Trunks.ISR_012));
            Assert.AreEqual<String>("01219543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToDialNormalizer.NormalizePhone("9543388911", DataModel.Constants.Countries.UNITED_STATES, DataModel.Constants.Trunks.ISR_012));

            Assert.AreEqual<String>("011972543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToDialNormalizer.NormalizePhone("0543388911", DataModel.Constants.Countries.ISRAEL, DataModel.Constants.Trunks.XCASTLABS));
            Assert.AreEqual<String>("011449543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToDialNormalizer.NormalizePhone("9543388911", DataModel.Constants.Countries.UNITED_KINGDOM, DataModel.Constants.Trunks.XCASTLABS));
            Assert.AreEqual<String>("011449543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToDialNormalizer.NormalizePhone("09543388911", DataModel.Constants.Countries.UNITED_KINGDOM, DataModel.Constants.Trunks.XCASTLABS));
            Assert.AreEqual<String>("9543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToDialNormalizer.NormalizePhone("9543388911", DataModel.Constants.Countries.UNITED_STATES, DataModel.Constants.Trunks.XCASTLABS));

            Assert.AreEqual<String>("0543388911", NoProblem.Core.BusinessLogic.Dialer.PhoneToDialNormalizer.NormalizePhone("0543388911", "China", "014"));
            Assert.AreEqual<String>(null, NoProblem.Core.BusinessLogic.Dialer.PhoneToDialNormalizer.NormalizePhone(null, "China", "014"));
        }
    }
}
