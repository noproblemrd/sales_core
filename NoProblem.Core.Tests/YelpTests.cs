﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class YelpTests
    {
        [TestMethod]
        public void TestYelpByPhone()
        {
            var yelp = BusinessLogic.YelpHelper.GetYelp();
            var businesses = yelp.SearchByPhone("5184491782");
            Assert.IsNotNull(businesses);
            Assert.IsNotNull(businesses.businesses);
            Assert.IsNotNull(businesses.businesses.FirstOrDefault());

            var allData = yelp.GetBusiness(businesses.businesses.First().id);
            Assert.IsNotNull(allData.reviews.First().excerpt);

        }
    }
}
