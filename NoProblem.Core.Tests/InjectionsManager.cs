﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class InjectionsManager
    {
        Guid id;

        [TestMethod]
        public void TestMethod1()
        {
            BusinessLogic.Origins.InjectionsCockpit.InjectionsManager m1 =
                new BusinessLogic.Origins.InjectionsCockpit.InjectionsManager(null);

            string name = "test" + new Random().Next();

            id = m1.UpsertInjectionDetails(
                new DataModel.Injection.InjectionData()
            {
                Name = name,
                ScriptBefore = "bla bla",
                ScriptUrl = "some url"
            });

            BusinessLogic.Origins.InjectionsCockpit.InjectionsManager m2 =
              new BusinessLogic.Origins.InjectionsCockpit.InjectionsManager(null);

            var details = m2.GetInjectionDetails(id);

            Assert.AreEqual<string>("bla bla", details.ScriptBefore);
            Assert.AreEqual<string>("some url", details.ScriptUrl);
            Assert.AreEqual<string>(name, details.Name);

            BusinessLogic.Origins.InjectionsCockpit.InjectionsManager m3 =
            new BusinessLogic.Origins.InjectionsCockpit.InjectionsManager(null);

            var otherId = m3.UpsertInjectionDetails(
                new DataModel.Injection.InjectionData()
                {
                    Name = name + "2",
                    ScriptBefore = "bla bla 2",
                    ScriptUrl = "some url 2",
                    InjectionId = id
                });

            Assert.AreEqual<Guid>(id, otherId);

            BusinessLogic.Origins.InjectionsCockpit.InjectionsManager m4 =
             new BusinessLogic.Origins.InjectionsCockpit.InjectionsManager(null);

            var details2 = m4.GetInjectionDetails(id);

            Assert.AreEqual<string>("bla bla 2", details2.ScriptBefore);
            Assert.AreEqual<string>("some url 2", details2.ScriptUrl);
            Assert.AreEqual<string>(name + "2", details2.Name);
        }

        [TestCleanup]
        public void TestAfter()
        {
            DeleteInjection(id);
        }

        private void DeleteInjection(Guid id)
        {
            if (id != Guid.Empty)
            {
                DataAccessLayer.Injection dal = new DataAccessLayer.Injection(null);
                DataModel.Xrm.new_injection inj = new DataModel.Xrm.new_injection(dal.XrmDataContext);
                inj.new_injectionid = id;
                dal.Delete(inj);
                dal.XrmDataContext.SaveChanges();
            }
        }
    }
}
