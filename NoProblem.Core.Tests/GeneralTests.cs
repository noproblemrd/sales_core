﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic.AddOns;
using NoProblem.Core.BusinessLogic.Expertises;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class GeneralTests
    {
        [TestMethod]
        public void CustomerIdTest()
        {
            DataAccessLayer.Incident dal = new DataAccessLayer.Incident(null);
            var inci = dal.Retrieve(new Guid("011F0D60-20B8-4B7A-8B07-D4F155AAB64D"));
            Assert.IsNotNull(inci.customerid.name);
        }

        [TestMethod]
        public void DrillDown()
        {
            NoProblem.Core.BusinessLogic.Reports.Creators.InjectionReportCreator c = new BusinessLogic.Reports.Creators.InjectionReportCreator(null);
            c.CreateDrillDown(
                new DataModel.Reports.Request.InjectionReportRequest()
                {
                    FromDate = new DateTime(2015, 1, 21),
                    ToDate = new DateTime(2015, 1, 30),
                    InjectionId = new Guid("d4ce7ac9-755f-e411-b45d-001517d1792a")
                }
                );
        }

        [TestMethod]
        public void CostAndBonus()
        {
            NoProblem.Core.Interfaces.Core.Supplier s = new Interfaces.Core.Supplier();
            var x = s.GetSubscriptionPlanCost();

        }

        [TestMethod]
        public void TheHomeFixersParserTest()
        {
            string brokerId;
            decimal price;
            NoProblem.Core.BusinessLogic.LeadBuyers.TheHomeFixers.ParseLeadApprovedInPingResponse("Lead Approved esf8324a3X $15.84", out brokerId, out price);
            Assert.AreEqual<string>("esf8324a3X", brokerId);
            Assert.AreEqual<decimal>(new decimal(15.84), price);

            string brokerId2;
            decimal price2;
            NoProblem.Core.BusinessLogic.LeadBuyers.TheHomeFixers.ParseLeadApprovedInPingResponse("Lead Approved 24a3X $16", out brokerId2, out price2);
            Assert.AreEqual<string>("24a3X", brokerId2);
            Assert.AreEqual<decimal>(new decimal(16), price2);
        }

        [TestMethod]
        public void ZipCodeFixer()
        {
            Assert.AreEqual<string>("W1D", BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode("W1D 1BX"));
            Assert.AreEqual<string>("W1D", BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode("W1D-1BX"));
            Assert.AreEqual<string>("10001", BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode("10001"));
            Assert.AreEqual<string>("1234", BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode("1234 6541"));
            Assert.AreEqual<string>("1234", BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode("1234-6541"));
            Assert.AreEqual<string>("1", BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode("1-6541"));
            Assert.AreEqual<string>("1", BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode("1 6541"));
            Assert.AreEqual<string>("", BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode(""));
            Assert.AreEqual<string>(null, BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode(null));
            Assert.AreEqual<string>("  ", BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode("  "));
            Assert.AreEqual<string>(" ", BusinessLogic.ServiceRequest.ServiceRequestManager.FixZipCode(" "));
        }

        [TestMethod]
        public void GetPaymentMethodByCode()
        {
            DataAccessLayer.PaymentMethod pm = new DataAccessLayer.PaymentMethod(null);
            var x = pm.GetPaymentMethodByCode(DataModel.Xrm.new_paymentmethod.ePaymentMethod.CreditCard);
            Assert.AreEqual<string>("Credit Card", x.new_name);
        }

        [TestMethod]
        public void GetDefaultPricingMethod()
        {
            DataAccessLayer.PricingMethod pm = new DataAccessLayer.PricingMethod(null);
            var x = pm.GetDefaultPricingMethod();
            Assert.AreEqual<string>("Cash Bank", x.new_name);
        }
    }
}
