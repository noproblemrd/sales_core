﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class SlackTest
    {
        [TestMethod]
        public void SendTest()
        {
            BusinessLogic.Slack.SlackNotifier s = new BusinessLogic.Slack.SlackNotifier("unit-test message");
            s.Start();
            s.Join();
        }

        [TestMethod]
        public void SendTestWithChannel()
        {
            BusinessLogic.Slack.SlackNotifier s = new BusinessLogic.Slack.SlackNotifier("unit-test message requests", BusinessLogic.Slack.SlackNotifier.eSlackChannel.new_request);
            s.Start();
            s.Join();

            BusinessLogic.Slack.SlackNotifier s3 = new BusinessLogic.Slack.SlackNotifier("unit-test message requests3", BusinessLogic.Slack.SlackNotifier.eSlackChannel.new_request);
            s3.Start();
            s3.Join();

            BusinessLogic.Slack.SlackNotifier s2 = new BusinessLogic.Slack.SlackNotifier("unit-test message notifications", BusinessLogic.Slack.SlackNotifier.eSlackChannel.np_notifications);
            s2.Start();
            s2.Join();
        }
    }
}
