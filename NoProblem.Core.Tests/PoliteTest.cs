﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic.ServiceRequest.AutoResubmission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class PoliteTest
    {
        [TestMethod]
        public void TestPolite()
        {
            ResubmissionScheduler rs = new ResubmissionScheduler(new Guid("1254AE20-6DE4-4E7F-8C37-459DF987DCA2"));
            rs.TestPolite();
        }
    }
}
