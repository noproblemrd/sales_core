﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic.CreditCardSolek;
using NoProblem.Core.BusinessLogic.Invoices;
using NoProblem.Core.DataModel.SupplierModel.Registration2014;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class InvoiceDispatcherSpecs
    {

        private InvoiceDataContainer container;
        
        [TestInitialize]
        public void BeforeEach()
        {
            container = new InvoiceDataContainer()
            {
                InvoiceUrl = "http://google.com",
                InvoiceNumber = "1234",
                BillingCity = "new york",
                BillingEmail = "dorona@noproblemppc.com",
                BillingFirstName = "Alain",
                BillingLastName = "Adler",
                BillingState = "NY",
                BillingZip = "10001"
            };
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SendClipCallInvoice_MonthlyInvoiceType_ShouldThrowException()
        {
            var supplier = new DefaultDispatcher.SupplierRef
            {
                SupplierId = new Guid("27053649-E1E4-4B4C-8350-CC3DF20ED0C0"),
                SupplierName = "Foo",
                SubscriptionPlan = SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION
            };
            var sender6 = new Sender2(container, eInvoiceType.Monthly,supplier , 5);
            sender6.Run();
        }

        [TestMethod]
        public void SendClipCallInvoice_RegistrationInvoiceType_ShouldSendEmail()
        {
            var supplier = new DefaultDispatcher.SupplierRef
            {
                SupplierId = new Guid("27053649-E1E4-4B4C-8350-CC3DF20ED0C0"),
                SupplierName = "Foo",
                SubscriptionPlan = SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION
            };
            var sender6 = new Sender2(container, eInvoiceType.Registration, supplier, 5);
            sender6.Run();           
        }

        [TestMethod]
        public void SendClipCallInvoice_SingleLeadInvoiceType_ShouldSendEmail()
        {
            var supplier = new DefaultDispatcher.SupplierRef
            {
                SupplierId = new Guid("27053649-E1E4-4B4C-8350-CC3DF20ED0C0"),
                SupplierName = "Foo",
                SubscriptionPlan = SubscriptionPlansContract.Plans.CLIP_CALL_SUBSCRIPTION
            };
            var sender6 = new Sender2(container, eInvoiceType.SingleLead, supplier, 5);
            sender6.Run();
        }


        [TestMethod]
        public void SendLeadBoostepInvoice_SingleLeadInvoiceType_ShouldSendEmail()
        {
            var supplier = new DefaultDispatcher.SupplierRef
            {
                SupplierId = new Guid("27053649-E1E4-4B4C-8350-CC3DF20ED0C0"),
                SupplierName = "Foo",
                SubscriptionPlan = SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING
            };
            var sender6 = new Sender2(container, eInvoiceType.SingleLead, supplier, 5);
            sender6.Run();
        }


        [TestMethod]
        public void SendLeadBoostepInvoice_RegistrationInvoiceType_ShouldSendEmail()
        {
            var supplier = new DefaultDispatcher.SupplierRef
            {
                SupplierId = new Guid("27053649-E1E4-4B4C-8350-CC3DF20ED0C0"),
                SupplierName = "Foo",
                SubscriptionPlan = SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING
            };
            var sender6 = new Sender2(container, eInvoiceType.Registration, supplier, 5);
            sender6.Run();
        }

        [TestMethod]
        public void SendLeadBoostepInvoice_MonthlyInvoiceType_ShouldSendEmail()
        {
            var supplier = new DefaultDispatcher.SupplierRef
            {
                SupplierId = new Guid("27053649-E1E4-4B4C-8350-CC3DF20ED0C0"),
                SupplierName = "Foo",
                SubscriptionPlan = SubscriptionPlansContract.Plans.LEAD_BOOSTER_AND_FEATURED_LISTING
            };
            var sender6 = new Sender2(container, eInvoiceType.Monthly, supplier, 5);
            sender6.Run();
        }
    }

    public class Sender2 : DefaultDispatcher.Sender
    {
        public Sender2(InvoiceDataContainer invoiceDataContainer, eInvoiceType type, DefaultDispatcher.SupplierRef supplier, decimal amount)
            : base(invoiceDataContainer, type, supplier, amount)
        {
        }

        public new void Run()
        {
            base.Run();
        }
    }
}
