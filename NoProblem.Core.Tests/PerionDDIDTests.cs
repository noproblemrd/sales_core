﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class PerionDDIDTests
    {
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassInitialize()]
        public static void Before(TestContext testContext)
        {
            //string strCmdText;
            //strCmdText = @"/C ""C:\Program Files (x86)\Cisco Systems\VPN Client\vpnclient.exe"" connect onecall user C70415 pwd h4mm3rh34d!";
            //var process = System.Diagnostics.Process.Start("CMD.exe", strCmdText);
            //process.WaitForExit();
        }

        private Guid numberId;
        private Guid numberId2;
        private string sessionId = "417CA4E8-AFB0-4BAE-9C6A-DC2A8743B43E";
        private string sessionId2 = "CCFFF9CB-1D24-4D70-AAAA-F821C37CE93F";

        [TestInitialize]
        public void TestBefore()
        {
           // numberId = CreateNewNumber("testtest", sessionId);
          //  numberId2 = CreateNewNumber("testtest2", sessionId2);
        }

        [TestCleanup]
        public void TestAfter()
        {
          //  DeleteNumber(numberId);
          //  DeleteNumber(numberId2);
        }

        [TestMethod]
        public void TestGetSession()
        {
            //BusinessLogic.Dialer.PerionDDID.PerionDDIDManager manager = new BusinessLogic.Dialer.PerionDDID.PerionDDIDManager();
            //string session = manager.GetSessionIdByNumber("testtest");
            //Assert.AreEqual<string>(sessionId, session);

            //session = manager.GetSessionIdByNumber("testtestbabaab");
            //Assert.IsNull(session);

            //session = manager.GetSessionIdByNumber("testtest2");
          
            //List<string> lst = new List<string>();
            //foreach (var item in numbers)
            //{
            //    if (item.new_perionddid.HasValue && item.new_perionddid.Value && !String.IsNullOrEmpty(item.new_perionsessionid))
            //    {
            //        lst.Add(item.new_perionsessionid);
            //    }
            //}
            //if (!lst.Contains<string>(session))
            //{
            //    Assert.Fail("random not found");
            //}
        }

        [TestMethod]
        public void TestAllocateNewNumber(Guid ExpertiseId)
        {
            BusinessLogic.Dialer.PerionDDID.PerionDDIDManager manager = new BusinessLogic.Dialer.PerionDDID.PerionDDIDManager();
            string number = manager.GetNumber(sessionId, ExpertiseId);

            DataAccessLayer.DirectNumber dal = new DataAccessLayer.DirectNumber(null);
            DataModel.Xrm.new_directnumber dn = (from x in dal.All
                                                 where x.new_number == number
                                                 select x).FirstOrDefault();
            if (dn == null)
            {
                Assert.Fail();
            }
            Assert.AreEqual<string>(sessionId, dn.new_perionsessionid);
            Assert.AreEqual<string>(sessionId.ToLower(), dn.new_originid.ToString().ToLower());
            Assert.IsTrue(dn.new_perionddid.HasValue);
            Assert.IsTrue(dn.new_perionddid.Value);


            var secondNumber = manager.GetNumber(sessionId, ExpertiseId);
            Assert.AreEqual<string>(number, secondNumber);

            var s2 = manager.GetNumber(sessionId, ExpertiseId);
            Assert.AreNotEqual<string>(number, s2);
        }

        private void DeleteNumber(Guid id)
        {
            DataAccessLayer.DirectNumber dal = new DataAccessLayer.DirectNumber(null);
            DataModel.Xrm.new_directnumber dn = new DataModel.Xrm.new_directnumber(dal.XrmDataContext);
            dn.new_directnumberid = id;
            dal.Delete(dn);
            dal.XrmDataContext.SaveChanges();
        }

        private Guid CreateNewNumber(string number, string session)
        {
            DataAccessLayer.DirectNumber dal;
            DataModel.Xrm.new_directnumber dn;
            dal = new DataAccessLayer.DirectNumber(null);
            dn = new DataModel.Xrm.new_directnumber();
            dn.new_number = number;
            dn.new_perionsessionid = session;
            dn.new_perionddid = true;
            dal.Create(dn);
            dal.XrmDataContext.SaveChanges();
            return dn.new_directnumberid;
        }
    }
}
