﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class Registration_2014
    {
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestInitialize()]
        public void Before()
        {
            string strCmdText;
            strCmdText = @"/C ""C:\Program Files (x86)\Cisco Systems\VPN Client\vpnclient.exe"" connect onecall user C70415 pwd h4mm3rh34d!";
            var process = System.Diagnostics.Process.Start("CMD.exe", strCmdText);
            process.WaitForExit();
        }

        [TestMethod]
        public void TestChangingWorkHoursAndRetrievengInSEOPage()
        {
            BusinessLogic.SupplierBL.Registration2014.WorkingHours.WorkingHoursManager manager = new BusinessLogic.SupplierBL.Registration2014.WorkingHours.WorkingHoursManager(null);
            var req = new DataModel.SupplierModel.Registration2014.WorkingHours.SetWorkingHoursRequest()
                {
                    SupplierId = new Guid("b2ddcc06-c398-4b31-94b0-c43042c9a4ed")
            
                };

            var u2 = new DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit()
            {
                AllDay = true,
                Order = 1
            };
            u2.DaysOfWeek.Add(DayOfWeek.Monday);
            u2.DaysOfWeek.Add(DayOfWeek.Tuesday);

            var u1 = new DataModel.SupplierModel.Registration2014.WorkingHours.WorkingHoursUnit()
            {
                AllDay = false,
                Order = 2,
                FromHour = 9,
                FromMinute = 30,
                ToHour = 16,
                ToMinute = 0
            };
            u1.DaysOfWeek.Add(DayOfWeek.Wednesday);



            req.Units.Add(u1);
            req.Units.Add(u2);

            manager.SetWorkingHours(
                req
                );

            BusinessLogic.SEODataServices.SEOSupplierDataRetriever
               retriever = new BusinessLogic.SEODataServices.SEOSupplierDataRetriever(null);
            var res = retriever.GetSupplierPageData(
                new DataModel.SeoData.SEOGetSupplierPageDataRequest()
                {
                    SupplierId = new Guid("b2ddcc06-c398-4b31-94b0-c43042c9a4ed")
                });

            Assert.AreEqual<int>(2, res.WorkingHours.Count);
            Assert.AreEqual<int>(2, res.WorkingHours.First().DaysOfWeek.Count);
            Assert.AreEqual<int>(0, res.WorkingHours.First().From);
            Assert.AreEqual<int>(0, res.WorkingHours.First().FromMinute);
            Assert.AreEqual<int>(24, res.WorkingHours.First().To);
            Assert.AreEqual<int>(0, res.WorkingHours.First().ToMInute);

            var uback2 = res.WorkingHours.ElementAt(1);
            Assert.AreEqual<int>(1, uback2.DaysOfWeek.Count);
            Assert.AreEqual<int>(9, uback2.From);
            Assert.AreEqual<int>(30, uback2.FromMinute);
            Assert.AreEqual<int>(16, uback2.To);
            Assert.AreEqual<int>(0, uback2.ToMInute);
        }

        [TestMethod]
        public void TestLeadsInSeoPage()
        {
            BusinessLogic.SEODataServices.SEOSupplierDataRetriever
                retriever = new BusinessLogic.SEODataServices.SEOSupplierDataRetriever(null); 
            var res = retriever.GetSupplierPageData(
                new DataModel.SeoData.SEOGetSupplierPageDataRequest()
                {
                    SupplierId = new Guid("B2DDCC06-C398-4B31-94B0-C43042C9A4ED")
                });
        }

        [TestMethod]
        public void TestChangingDataInBusinessProfileAndRetrievingInSEOPage()
        {
            Random ran = new Random();
            string about = Guid.NewGuid().ToString();
            string address = ran.Next(100, 1000) + " El Camino Real, San Jose, CA 95126, United States";
            string accName = "Adler Dev Ops " + ran.Next(100, 1000);
            string phone = "0543388" + ran.Next(100, 999);
            string website = "http://mysite.com/" + ran.Next(100, 1000);
            BusinessLogic.SupplierBL.Registration2014.BusinessProfileManager bsM = new BusinessLogic.SupplierBL.Registration2014.BusinessProfileManager(null);
            bsM.SetBusinessProfileData(
                new DataModel.SupplierModel.Registration2014.SetBusinessProfileDataRequest()
                {

                    About = about,
                    SupplierId = new Guid("b2ddcc06-c398-4b31-94b0-c43042c9a4ed"),
                    FullAddress = address,
                    Name = accName,
                    Phone = phone,
                    Website = website,
                    PayWithAMEX = true,
                    PayWithMASTERCARD = false,
                    PayWithVISA = false,
                    PayWithCASH = true
                });

            BusinessLogic.SEODataServices.SEOSupplierDataRetriever
                retriever = new BusinessLogic.SEODataServices.SEOSupplierDataRetriever(null);
            var res = retriever.GetSupplierPageData(
                new DataModel.SeoData.SEOGetSupplierPageDataRequest()
                {
                    SupplierId = new Guid("b2ddcc06-c398-4b31-94b0-c43042c9a4ed")
                });

            Assert.AreEqual(0, res.Videos.Count);
            Assert.AreEqual(accName, res.Name);
            Assert.AreEqual(address, res.Address);
            Assert.AreEqual(about, res.About);
            Assert.AreEqual(phone, res.Phone);
            Assert.AreEqual(website, res.Website);
            Assert.IsTrue(res.PayWithAMEX);
            Assert.IsFalse(res.PayWithMASTERCARD);
            Assert.IsTrue(res.PayWithCASH);
            Assert.IsFalse(res.PayWithVISA);
        }
    }
}
