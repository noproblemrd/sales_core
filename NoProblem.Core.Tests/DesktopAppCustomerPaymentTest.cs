﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class DesktopAppCustomerPaymentTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            BusinessLogic.ConsumerDesktop.PaymentsManager pay = new BusinessLogic.ConsumerDesktop.PaymentsManager(null);
            var request = new DataModel.Consumer.DesktopApp.CustomerPaymentInDesktopApp()
                {
                    ContactId = new Guid("6BFC4EE0-B6E2-489B-9D48-3393CC548760"),
                    AmountDollars = 15,
                    Last4Digits = "1111",
                    BillingAddress = "123 some street",
                    BillingCity = "new york",
                    BillingFirstName = "Alain",
                    BillingLastName = "Adler",
                    BillingPhone = "2121231232",
                    BillingState = "NY",
                    BillingZip = "10001",
                    CreditCardType = "VISA",
                    EcryptedCvv = "",
                    EncryptedCreditCardNumber = "",
                    ExpirationMonth = 12,
                    ExpirationYear = 2017,
                    UserAgent = "",
                    UserHost = "",
                    UserIP = ""
                };
            //pay.AddPayment(request);
        }
    }
}
