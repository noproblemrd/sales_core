﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic.ServiceRequest;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class CustomerAfterCaseServiceTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            CustomerAfterCaseService service = new CustomerAfterCaseService(null);

            service.SendAfterCaseEmail(new Guid("F4CA9C3F-4381-457A-A588-86C20D11A214"), "alain@onecall.co.il");
        }
    }
}
