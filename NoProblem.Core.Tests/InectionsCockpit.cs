﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.DataModel.Injection;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class InectionsCockpit
    {
        Guid origin1, injection1;

        [TestInitialize]
        public void TestBefore()
        {
            origin1 = CreateOrigin();
            injection1 = CreateInjection();
        }

        [TestCleanup]
        public void TestAfter()
        {
            DeleteOrigin(origin1);
            DeleteInjection(injection1);
        }

        [TestMethod]
        public void TestInjectionCockpit()
        {
            BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager m1 = new BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager(null);
            m1.UpdateConnectionsByInjection(injection1, new List<InjectionOriginData>() { new InjectionOriginData(){ OriginId = origin1 }});
            DataAccessLayer.InjectionOrigin dal1 = new DataAccessLayer.InjectionOrigin(null);
            var query1 = from c in dal1.All
                        where c.new_originid == origin1
                        && c.new_injectionid == injection1
                        select new { c.new_injectionoriginid };
            Assert.AreEqual<int>(1, query1.Count());

            BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager m2 = new BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager(null);
            var injectionsByOrigin = m2.GetInjectionsByOrigin(origin1);
            Assert.AreEqual<int>(1, injectionsByOrigin.Count);
            Assert.AreEqual<Guid>(injection1, injectionsByOrigin.First().InjectionId);

            BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager m3 = new BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager(null);
            var originsByInjection = m3.GetOriginsByInjection(injection1);
            Assert.AreEqual<int>(1, originsByInjection.Count);
            Assert.AreEqual<Guid>(origin1, originsByInjection.First().OriginId);

            BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager m4 = new BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager(null);
            m4.UpdateConnectionsByOrigin(origin1, new List<InjectionOriginData>());

            DataAccessLayer.InjectionOrigin dal2 = new DataAccessLayer.InjectionOrigin(null);
            var query2 = from c in dal2.All
                        where c.new_originid == origin1
                        && c.new_injectionid == injection1
                        select new { c.new_injectionoriginid };
            Assert.AreEqual<int>(0, query2.Count());

            BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager m5 = new BusinessLogic.Origins.InjectionsCockpit.ConnectionsManager(null);
            var injectionsByOrigin2 = m5.GetInjectionsByOrigin(origin1);
            Assert.AreEqual<int>(0, injectionsByOrigin2.Count);
        }

        private void DeleteInjection(Guid id)
        {
            DataAccessLayer.Injection dal = new DataAccessLayer.Injection(null);
            DataModel.Xrm.new_injection inj = new DataModel.Xrm.new_injection(dal.XrmDataContext);
            inj.new_injectionid = id;
            dal.Delete(inj);
            dal.XrmDataContext.SaveChanges();
        }

        private void DeleteOrigin(Guid id)
        {
            DataAccessLayer.Origin dal = new DataAccessLayer.Origin(null);
            DataModel.Xrm.new_origin o = new DataModel.Xrm.new_origin(dal.XrmDataContext);
            o.new_originid = id;
            dal.Delete(o);
            dal.XrmDataContext.SaveChanges();
        }

        private Guid CreateInjection()
        {
            DataModel.Xrm.new_injection test = new DataModel.Xrm.new_injection();
            DataAccessLayer.Injection dal = new DataAccessLayer.Injection(null);
            dal.Create(test);
            dal.XrmDataContext.SaveChanges();
            return test.new_injectionid;
        }

        private Guid CreateOrigin()
        {
            DataModel.Xrm.new_origin test = new DataModel.Xrm.new_origin();
            DataAccessLayer.Origin dal = new DataAccessLayer.Origin(null);
            dal.Create(test);
            dal.XrmDataContext.SaveChanges();
            return test.new_originid;
        }
    }
}
