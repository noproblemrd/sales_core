﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic.ClipCall.Leads;
using NoProblem.Core.BusinessLogic.Refunds;
using NoProblem.Core.DataAccessLayer.Bases;
using NoProblem.Core.DataModel;
using NoProblem.Core.DataModel.ClipCall;
using NoProblem.Core.DataModel.Refunds;
using NoProblem.Core.DataModel.Xrm;

namespace NoProblem.Core.Tests
{
       
    [TestClass]
    public class ReportLeadSpecs
    {

        private RefundsManager sut;

        [TestInitialize]
        public void BeforeEach()
        {
            sut = new RefundsManager(null);
        }

        [TestMethod]
        public void ReportLead()
        {
            //var request = new RefundFromMobileRequest
            //{
            //    Comment = "Foo",
            //    SupplierId = Guid.Parse("ca6203c2-9f9c-4e2c-9a6b-a60914dc67c4"),
            //    ReasonCode = 5,
            //    IncidentAccountId = Guid.Parse("95842171-64e0-4d69-a159-4e7331a97aef"),                
            //};

            DataContext dataContext = XrmDataContext.Create();
            var repository = new Repository(dataContext);
            var notifier = new BusinessLogic.Notifications(null);
            var leadsManager = new LeadsManager(repository, notifier);
            var leadReport = new LeadReport
            {
                Comment = "Foo",
                SupplierId = Guid.Parse("ca6203c2-9f9c-4e2c-9a6b-a60914dc67c4"),
                ReasonCode = 11,
                IncidentAccountId = Guid.Parse("95842171-64e0-4d69-a159-4e7331a97aef"),  
            };
            //leadsManager.ClearLeadReport(leadReport);

            leadsManager.ReportLead(leadReport);

            //GetAllRefundReasonsResponse getAllRefundReasonsResponse = sut.GetAllActiveMobileRefundReasons();
            //sut.RefundFromMobile(request);
        }
    }
}
