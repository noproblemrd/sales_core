﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic.AdvertiserDashboards;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class PhonePercentageCalcualtions
    {
        [TestMethod]
        public void TestMethod1()
        {
            PhoneAvailabilityCalculator calculator = new PhoneAvailabilityCalculator(null);
            var ans = calculator.GetPhoneAvailabilityData(new Guid("B2DDCC06-C398-4B31-94B0-C43042C9A4ED"));
            Assert.AreEqual<int>(100, ans.PercentageOfWeeklyHours);
        }
    }
}
