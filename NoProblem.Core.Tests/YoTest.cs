﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class YoTest
    {
        [TestMethod]
        public void YoTestBasic()
        {
            NoProblem.Core.BusinessLogic.Yo.YoNotifier yoNotifier = new NoProblem.Core.BusinessLogic.Yo.YoNotifier(NoProblem.Core.BusinessLogic.Yo.YoNotifier.eYoApps.NPNEWADV);
            yoNotifier.Start();
            yoNotifier.Join();
        }
        [TestMethod]
        public void YoTestSms()
        {
            NoProblem.Core.BusinessLogic.FoneApiBL.Handlers.AarTextMessage.AarTextMessageHandler atm = new BusinessLogic.FoneApiBL.Handlers.AarTextMessage.AarTextMessageHandler();
            atm.SendAfterCall(new Guid("0F80D2C6-3A17-464C-BBE1-B1392A14239F"), new Guid("7C97B170-2BD4-454C-8A21-A2B8B3441518"), true);
        }
    }
}
