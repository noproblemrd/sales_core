﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class RealTimeRouter
    {
        [TestMethod]
        public void IsSomeoneAvailable()
        {
            var ans = BusinessLogic.Dialer.RealTimeRouting.RealTimeRouter.IsSomeoneAvailable(
                new Guid("AAE69263-8E43-E211-A9A0-001517D10F6E")//computer repair
                );
            Assert.IsTrue(ans);
        }
    }
}
