﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoProblem.Core.BusinessLogic;

namespace NoProblem.Core.Tests
{
    [TestClass]
    public class MandrilSender
    {
        [TestMethod]
        public void SendWithBcc()
        {
            Dictionary<string, string> vars = new Dictionary<string, string>();
            vars.Add("NPVAR_VERIFY_URL", "http://google.com");
            vars.Add("NPVAR_AD_EMAIL", "alain.adler@gmail.com");
            MandrillSender sender = new MandrillSender();
            bool sentOk = sender.SendTemplate("noproblemreports@gmail.com", "alain.adler@gmail.com", "test subject this is with bcc 3", "E_AD_EmailVerificaiton_No_Social_Registration2014", vars);
            Assert.IsTrue(sentOk);
        }

        [TestMethod]
        public void SendWithoutBcc()
        {
            Dictionary<string, string> vars = new Dictionary<string, string>();
            vars.Add("NPVAR_VERIFY_URL", "http://google.com");
            vars.Add("NPVAR_AD_EMAIL", "alain.adler@gmail.com");
            MandrillSender sender = new MandrillSender();
            bool sentOk = sender.SendTemplate("alain.adler@gmail.com", "test subject no bcc", "E_CUSTOMER_POST_LEAD_DESKTOP_APP", vars);
            Assert.IsTrue(sentOk);
        }     
    }
}
