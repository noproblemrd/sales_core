﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using System.Reflection;

namespace AdvertiserRegisterJob
{
    class Program
    {
        private static readonly ILog LOG = LogManager.GetLogger(
           MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {

            AdvRegJob job = new AdvRegJob();
            try
            {
                job.ExecJob();
            }
            catch(Exception exc)
            {
                LOG.Error("Exception in job.ExecJob", exc);
            }
        }
    }
}
