﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using NoProblem.Core.BusinessLogic.PushNotifications;
using System.Data.SqlClient;
using log4net;
using System.Reflection;

namespace AdvertiserRegisterJob
{
    internal class AdvRegJob
    {
        private static readonly ILog LOG = LogManager.GetLogger(
          MethodBase.GetCurrentMethod().DeclaringType);
       
        string connectionString;
        string ClipCallServiceUrl;
        internal AdvRegJob()
        {
            ClipCallServiceUrl = System.Configuration.ConfigurationManager.AppSettings.Get("ClipCallService");
            connectionString = System.Configuration.ConfigurationManager.AppSettings.Get("dbConnectionString");
        }
        internal void ExecJob()
        {
            List<AccountData> list = new List<AccountData>();
            string command = "EXEC [dbo].[GetIncomleteRegistration]";
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                using(SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        AccountData ad = new AccountData();
                        ad.AccountId = (Guid)reader["AccountId"];
                        ad.OS = (string)reader["OS"];
                        ad.UID = (string)reader["UID"];
                      //  string missing = (string)reader["FieldMissing"];
                     //   ad.TypeMissing = (PushTypeMissing.eTypeMissing)Enum.Parse(typeof(PushTypeMissing.eTypeMissing), missing);
                        ad.TypeMissing = (string)reader["FieldMissing"];
                        list.Add(ad);
                    }
                    conn.Close();
                }
            }
            foreach (AccountData ad in list)
            {
                ClipCallService.NotificationServiceManagerRequest request = new ClipCallService.NotificationServiceManagerRequest();
                ClipCallService.KeyValuePairString[] data = new ClipCallService.KeyValuePairString[1];
                data[0] = new ClipCallService.KeyValuePairString();
                data[0].Key = "typemissing";
                data[0].Value = ad.TypeMissing;//.ToString();
                request.data = data;
                request.DeviceId = ad.UID;
                request.PushType = ClipCallService.ePushTypes.REGISTRATION_UNFINISHED;
                request.OS = ad.OS;
                ClipCallService.ResultOfBoolean result = null;
                ClipCallService.ClipCallReport cc = new ClipCallService.ClipCallReport();
                cc.Url = ClipCallServiceUrl;
                try
                {
                    result = cc.SendPushNotification(request);
                }
                catch(Exception exc)
                {
                    LOG.Error("Exception in ClipCallReport.SendPushNotification", exc);
                    continue;
                }
                if (result.Type == ClipCallService.eResultType.Failure)
                    continue;
                if (result.Value)
                    SetSentNotification(ad.AccountId);
            }
        }

        private void SetSentNotification(Guid accountId)
        {
            string command = "UPDATE dbo.AccountExtensionBase SET New_sentNotComleteRegistration = 1 WHERE AccountId = @accountId";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(command, conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@accountId", accountId);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
    }
}
