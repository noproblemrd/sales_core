﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;

namespace NoProblem.Core.Scraper.Parsers.Implementations
{
    internal class SuperPagesParser : IParser
    {
        private static string urlBase = "http://yellowpages.superpages.com/listings.jsp?C=";

        private string BuildUrl(string heading, string city, int pageNumber)
        {
            string url = urlBase + heading + " in " + city + "&PI=" + (pageNumber * 10).ToString();
            return url;
        }

        #region IParser Members

        public Dictionary<string, string> Parse(string heading, string city, int max)
        {
            Dictionary<string, string> retVal = new Dictionary<string, string>();
            bool done = false;
            int pageNumber = 0;
            int excCount = 0;
            while (!done)
            {
                try
                {
                    Console.WriteLine("in page {0}", pageNumber.ToString());
                    string url = BuildUrl(heading, city, pageNumber);
                    string html = MyWebClient.GetWebText(url);
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(html);
                    done = DoListings(retVal, doc);
                    pageNumber++;
                    if (retVal.Count >= max)
                    {
                        break;
                    }
                }
                catch (Exception exc)
                {
                    excCount++;
                    Console.WriteLine("Exception: " + exc.Message);
                    if (excCount >= 5)
                    {
                        break;
                    }
                }
            }
            return retVal;
        }

        #endregion

        private bool DoListings(Dictionary<string, string> retVal, HtmlDocument doc)
        {
            bool isDone = false;
            int count = -1;
            while (true)
            {
                count++;
                HtmlNode advNode = doc.GetElementbyId("lstg" + count);

                if (advNode == null)
                {
                    if (count == 0)
                    {
                        isDone = true;
                    }
                    break;
                }

                string name = advNode.ChildNodes[1].ChildNodes[1].ChildNodes[3].ChildNodes[1].ChildNodes[1].ChildNodes[1].InnerHtml;
                name = name.Replace("\n", "");
                HtmlNode phoneNode = doc.GetElementbyId("sphoneLink" + count);
                if (phoneNode == null)
                {
                    continue;
                }
                var spanNode = phoneNode.ChildNodes.FirstOrDefault(x => x.Name == "span");
                if (spanNode == null)
                {
                    continue;
                }
                string phone = spanNode.InnerHtml;

                for (int i = phone.Length - 1; i >= 0; i--)
                {
                    if (char.IsNumber(phone, i) == false)
                    {
                        phone = phone.Remove(i, 1);
                    }
                }
                if (phone.Length != 10)
                {
                    continue;
                }
                if (retVal.ContainsKey(phone) == false)
                {
                    retVal.Add(phone, name);
                }

            }

            return isDone;
        }
    }
}
