﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;

namespace NoProblem.Core.Scraper.Parsers.Implementations
{
    internal class YellowBookParser : IParser
    {
        private static string urlBase = "http://www.yellowbook.com/yellow-pages/?what=";

        public string BuildUrl(string heading, string city, int pageNumber)
        {
            string url = urlBase + heading + "&where=" + city + "&page=" + pageNumber.ToString();
            return url;
        }

        #region IParser Members

        public Dictionary<string, string> Parse(string heading, string city, int max)
        {
            Dictionary<string, string> retVal = new Dictionary<string, string>();
            bool done = false;
            int pageNumber = 1;
            int excCount = 0;
            while (!done)
            {
                try
                {
                    Console.WriteLine("in page {0}", pageNumber.ToString());
                    string url = BuildUrl(heading, city, pageNumber);
                    string html = MyWebClient.GetWebText(url);
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(html);
                    done = DoListings(retVal, doc);
                    pageNumber++;
                    if (retVal.Count >= max)
                    {
                        break;
                    }
                }
                catch (Exception exc)
                {
                    excCount++;
                    Console.WriteLine("Exception: " + exc.Message);
                    if (excCount >= 5)
                    {
                        break;
                    }
                }
            }
            return retVal;
        }

        #endregion

        private bool DoListings(Dictionary<string, string> retVal, HtmlDocument doc)
        {
            int count = 0;
            bool isDone = false;
            while (true)
            {
                count++;
                var advNode = doc.GetElementbyId("divInAreaSummary_" + count);
                if (advNode == null)
                {
                    if (count == 1)
                    {
                        isDone = true;
                    }
                    break;
                }
                HtmlNode vCardNode = null;
                foreach (var node in advNode.ChildNodes)
                {
                    if (node.Attributes["class"] != null && node.Attributes["class"].Value == "contact vcard")
                    {
                        vCardNode = node;
                        break;
                    }
                }
                if (vCardNode == null)
                    continue;
                HtmlNode bizNameclearBlockNode = null;
                foreach (var node in vCardNode.ChildNodes)
                {
                    if (node.Attributes["class"] != null && node.Attributes["class"].Value == "bizName clearBlock")
                    {
                        bizNameclearBlockNode = node;
                        break;
                    }
                }
                if (bizNameclearBlockNode == null)
                    continue;
                string name = bizNameclearBlockNode.ChildNodes[1].ChildNodes[1].InnerHtml;

                HtmlNode bizPhoneNode = null;
                foreach (var node in vCardNode.ChildNodes)
                {
                    if (node.Attributes["class"] != null && node.Attributes["class"].Value == "bizPhone")
                    {
                        bizPhoneNode = node;
                        break;
                    }
                }
                if (bizPhoneNode == null)
                    continue;
                HtmlNode calltelNode = null;
                foreach (var node in bizPhoneNode.ChildNodes)
                {
                    if (node.Attributes["class"] != null && node.Attributes["class"].Value == "call tel")
                    {
                        calltelNode = node;
                    }
                }
                if (calltelNode == null)
                    continue;
                string phone = calltelNode.ChildNodes[1].InnerHtml;
                for (int i = phone.Length - 1; i >= 0; i--)
                {
                    if (char.IsNumber(phone, i) == false)
                    {
                        phone = phone.Remove(i, 1);
                    }
                }
                if (phone.Length != 10)
                {
                    continue;
                }
                if (retVal.ContainsKey(phone) == false)
                {
                    retVal.Add(phone, name);
                }
            }
            return isDone;
        }
    }
}
