﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoProblem.Core.Scraper.Parsers.Implementations;

namespace NoProblem.Core.Scraper.Parsers
{
    internal class ParserFactory
    {
        internal static IParser CreateParser(string parserName)
        {
            switch (parserName)
            {
                case "YellowBookParser":
                    return new YellowBookParser();
                case "SuperPagesParser":
                    return new SuperPagesParser();
            }
            return null;
        }
    }
}
