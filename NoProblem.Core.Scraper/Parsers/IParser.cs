﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoProblem.Core.Scraper.Parsers
{
    interface IParser
    {
        Dictionary<string, string> Parse(string heading, string city, int max);
    }
}
