﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace NoProblem.Core.Scraper
{
    internal class MyWebClient
    {
        /// <summary>
        /// Gets the response text for a given url.
        /// </summary>
        /// <param name="url">The url whose text needs to be fetched.</param>
        /// <returns>The text of the response.</returns>
        internal static string GetWebText(string url)
        {
            string html;
            using (WebClient client = new WebClient())
            {
                html = client.DownloadString(url);
            }
            return html;
        }
    }
}
