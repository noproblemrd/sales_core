﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace NoProblem.Core.Scraper.DataModel
{
    [DataContract]
    public class RequestContainer
    {
        [DataMember]
        public Guid HeadingId { get; set; }   

        [DataMember]
        public string HeadingName { get; set; }

        [DataMember]
        public Guid RegionId { get; set; }

        [DataMember]
        public string RegionName { get; set; }

        [DataMember]
        public Guid CaseId { get; set; }
    }
}
