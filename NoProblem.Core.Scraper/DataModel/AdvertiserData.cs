﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoProblem.Core.Scraper.DataModel
{
    public class AdvertiserData
    {
        public AdvertiserData()
        {
            Headings = new List<string>();
            Cities = new List<string>();
        }

        public AdvertiserData(string name, string phone, string heading, string city)
            : this()
        {
            Name = name;
            Phone = phone;
            Headings.Add(heading);
            Cities.Add(city);
        }

        public string Name { get; set; }
        public string Phone { get; set; }
        public List<string> Headings { get; set; }
        public List<string> Cities { get; set; }

        public override bool Equals(object obj)
        {
            AdvertiserData other = (AdvertiserData)obj;
            return other.Phone == this.Phone;
        }

        public override int GetHashCode()
        {
            return Phone.GetHashCode();
        }
    }
}
