﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml.Linq;

namespace NoProblem.Core.Scraper
{
    // NOTE: If you change the interface name "IService1" here, you must also update the reference to "IService1" in Web.config.
    [ServiceContract]
    public interface IScraperService
    {
        [OperationContract]
        void MakeNewRequest(DataModel.RequestContainer data);

        [OperationContract]
        XElement GetDoneRequest();
    }
}
