﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Threading;
using log4net.Config;
using log4net;
using System.Reflection;

namespace NoProblem.Core.Scraper
{
    public class Global : System.Web.HttpApplication
    {
        private static readonly ILog LOG = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        private static Timer newRequestTimer = new Timer(Manager.DoNextRequest);

        protected void Application_Start(object sender, EventArgs e)
        {
            XmlConfigurator.Configure();
            LOG.Info("Application_Start");
            newRequestTimer.Change(20000, 60000);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            newRequestTimer.Dispose();
            LOG.Info("Application_End");
        }
    }
}