﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml.Linq;
using System.Configuration;
using NoProblem.Core.Scraper.Parsers;
using System.Xml;
using log4net;
using System.Reflection;
using System.Text;

namespace NoProblem.Core.Scraper
{
    internal class Manager
    {
        private static readonly ILog LOG = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);
        private static String[] parsers = new String[] { "YellowBookParser", "SuperPagesParser" };
        private HashSet<DataModel.AdvertiserData> results;
        private static int max = int.Parse(ConfigurationManager.AppSettings["MaxAdvertiserToScrap"]);
        private static string scrappedFilesDirectory = ConfigurationManager.AppSettings["ScrapedFilesDirectory"];
        private static string newRequestDirectory = ConfigurationManager.AppSettings["NewRequestsDirectory"];
        private static string doneRequestDirectory = ConfigurationManager.AppSettings["DoneRequestsDirectory"];

        internal Manager()
        {
            results = new HashSet<DataModel.AdvertiserData>();
        }

        internal static void DoNextRequest(object obj)
        {
            try
            {
                LOG.Info("DoNextRequest started");
                Manager manager = new Manager();
                DataModel.RequestContainer request = manager.ReadRequestFromFile();
                if (request != null)
                {
                    XElement doc = manager.StartCrawl(request.HeadingName, request.RegionName);
                    doc.Add(new XAttribute("CaseId", request.CaseId));
                    doc.Add(new XAttribute("RegionId", request.RegionId));
                    doc.Add(new XAttribute("RegionName", request.RegionName));
                    doc.Add(new XAttribute("HeadingId", request.HeadingId));
                    doc.Add(new XAttribute("HeadingName", request.HeadingName));
                    manager.WriteFile(doc, request.CaseId);
                }
            }
            catch (Exception exc)
            {
                LOG.Error("Exception in method DoNextRequest", exc);
            }
        }

        private void WriteFile(XElement doc, Guid caseId)
        {
            if (!Directory.Exists(scrappedFilesDirectory))
            {
                Directory.CreateDirectory(scrappedFilesDirectory);
            }
            string filePath = scrappedFilesDirectory + "\\scraped_" + caseId + ".xml";
            using (var stream = File.Create(filePath))
            {
                using (XmlWriter writer = XmlWriter.Create(stream))
                {
                    doc.WriteTo(writer);
                }
            }
        }

        private DataModel.RequestContainer ReadRequestFromFile()
        {
            DataModel.RequestContainer retVal = null;
            DirectoryInfo directoryInfo = new DirectoryInfo(newRequestDirectory);
            var result = directoryInfo.GetFiles("*.*", SearchOption.TopDirectoryOnly).OrderBy(t => t.LastWriteTime);
            var fileInfo = result.FirstOrDefault();
            if (fileInfo != null)
            {
                using (FileStream file = File.Open(fileInfo.FullName, FileMode.Open))
                {
                    using (XmlReader reader = XmlReader.Create(file))
                    {
                        XElement doc = XElement.Load(reader);
                        retVal = new NoProblem.Core.Scraper.DataModel.RequestContainer();
                        retVal.CaseId = new Guid(doc.Attribute("CaseId").Value);
                        var regElement = doc.Element("Region");
                        retVal.RegionName = regElement.Value;
                        retVal.RegionId = new Guid(regElement.Attribute("Id").Value);
                        var headElement = doc.Element("Heading");
                        retVal.HeadingName = headElement.Value;
                        retVal.HeadingId = new Guid(headElement.Attribute("Id").Value);
                    }
                }
                if (!Directory.Exists(doneRequestDirectory))
                {
                    Directory.CreateDirectory(doneRequestDirectory);
                }
                File.Move(fileInfo.FullName, doneRequestDirectory + "\\" + Path.GetFileName(fileInfo.FullName));
            }
            return retVal;
        }

        private XElement StartCrawl(string heading, string region)
        {
            foreach (var parserName in parsers)
            {
                IParser parser = ParserFactory.CreateParser(parserName);
                var dic = parser.Parse(heading, region, max);
                AddAdvertisersToResult(dic, heading, region);
            }
            return WriteResultsToXml();
        }

        private XElement WriteResultsToXml()
        {
            XElement resultsXml = new XElement("Scraper");
            resultsXml.Add(new XAttribute("Date", DateTime.Now.ToString()));
            foreach (var item in results)
            {
                XElement advertiser = new XElement("Advertiser");
                advertiser.Add(new XElement("Name", XmlCharacterWhitelist(item.Name)));
                advertiser.Add(new XElement("Phone", XmlCharacterWhitelist(item.Phone)));
                resultsXml.Add(advertiser);
            }
            return resultsXml;
        }

        private void AddAdvertisersToResult(Dictionary<string, string> dic, string heading, string city)
        {
            foreach (var item in dic)
            {
                results.Add(new DataModel.AdvertiserData(item.Value, item.Key, heading, city));
            }
        }

        private static string XmlCharacterWhitelist(string in_string)
        {
            if (in_string == null) return null;
            StringBuilder sbOutput = new StringBuilder();
            char ch;
            for (int i = 0; i < in_string.Length; i++)
            {
                ch = in_string[i];
                if ((ch >= 0x0020 && ch <= 0xD7FF) ||
                        (ch >= 0xE000 && ch <= 0xFFFD) ||
                        ch == 0x0009 ||
                        ch == 0x000A ||
                        ch == 0x000D)
                {
                    sbOutput.Append(ch);
                }
            }
            return sbOutput.ToString();
        }
    }
}
