﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Configuration;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using log4net;
using System.Reflection;

namespace NoProblem.Core.Scraper
{
    // NOTE: If you change the class name "Service1" here, you must also update the reference to "Service1" in Web.config and in the associated .svc file.    
    public class ScraperService : IScraperService
    {
        private static readonly ILog LOG = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);
        private static string newRequestDirectory = ConfigurationManager.AppSettings["NewRequestsDirectory"];
        private static string doneRequestDirectory = ConfigurationManager.AppSettings["DoneRequestsDirectory"];
        private static string scrappedFilesDirectory = ConfigurationManager.AppSettings["ScrapedFilesDirectory"];
        private static string scrappedTakenDirectory = ConfigurationManager.AppSettings["ScrapedTakenDirectory"];
        private static string failedFilesDirectory = ConfigurationManager.AppSettings["FailedFilesDirectory"];

        public void MakeNewRequest(NoProblem.Core.Scraper.DataModel.RequestContainer data)
        {
            try
            {
                LOG.Info("MakeNewRequest started");
                if (!Directory.Exists(newRequestDirectory))
                {
                    Directory.CreateDirectory(newRequestDirectory);
                }
                XElement doc = new XElement("Request", new XAttribute("CaseId", data.CaseId));
                XElement regElement = new XElement("Region", data.RegionName);
                regElement.Add(new XAttribute("Id", data.RegionId));
                doc.Add(regElement);
                XElement headElement = new XElement("Heading", data.HeadingName);
                headElement.Add(new XAttribute("Id", data.HeadingId));
                doc.Add(headElement);
                using (var stream = File.Create(newRequestDirectory + "\\request_" + data.CaseId + ".xml"))
                {
                    using (XmlWriter writer = XmlWriter.Create(stream))
                    {
                        doc.WriteTo(writer);
                    }
                }
            }
            catch (Exception exc)
            {
                LOG.Error("Exception in MakeNewRequest", exc);
            }
        }

        public XElement GetDoneRequest()
        {
            XElement doc = null;
            try
            {
                LOG.Info("GetDoneRequest started");
                DirectoryInfo directoryInfo = new DirectoryInfo(scrappedFilesDirectory);
                var result = directoryInfo.GetFiles("*.*", SearchOption.TopDirectoryOnly).OrderBy(t => t.LastWriteTime);
                FileInfo fileInfo = result.FirstOrDefault();
                if (fileInfo != null)
                {
                    string filePath = fileInfo.FullName;
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        try
                        {
                            using (var file = File.Open(filePath, FileMode.Open))
                            {
                                using (var xmlReader = XmlReader.Create(file))
                                {
                                    doc = XElement.Load(xmlReader);
                                }
                            }
                        }
                        catch (Exception exc)
                        {
                            LOG.Error(string.Format("Exception reading file in GetDoneRequest, moving file to failed directory. fileName:{0}", Path.GetFileName(filePath)), exc);
                            //move file to failed directory
                            if (!Directory.Exists(failedFilesDirectory))
                            {
                                Directory.CreateDirectory(failedFilesDirectory);
                            }
                            File.Move(filePath, failedFilesDirectory + "\\" + Path.GetFileName(filePath));
                        }
                        //move file to taken directory
                        if (!Directory.Exists(scrappedTakenDirectory))
                        {
                            Directory.CreateDirectory(scrappedTakenDirectory);
                        }
                        File.Move(filePath, scrappedTakenDirectory + "\\" + Path.GetFileName(filePath));
                    }
                }
            }
            catch (Exception exc)
            {
                LOG.Error("Exception in GetDoneRequest", exc);
            }
            return doc;
        }
    }
}
