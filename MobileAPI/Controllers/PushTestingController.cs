﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MobileAPI.Controllers
{
    public class PushTestingController : Controller
    {
        //
        // GET: /PushTesting/Create

        private const string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

        public ActionResult Create()
        {
            var model = new MobileAPI.Models.PushTestModel();
            model.DeviceId = "c555260d51467c96a82f1009a55048d0e12fece9664f21eb5e81e4ca8606c5d3";
            model.DataKey1 = "leadAccountId";
            model.DataValue1 = "7A6817E5-43E3-4E3A-8305-AD9F27DBD10A";
            model.DataKey2 = "description";
            model.DataValue2 = "I need a plumber to fix my sink ASAP.";
            model.DataKey3 = "max_bid";
            model.DataValue3 = "99.00";
            model.DataKey4 = "min_bid";
            model.DataValue4 = "5.00";
            model.DataKey5 = "category";
            model.DataValue5 = "Plumber";
            model.DataKey6 = "timeout_at_utc";
            model.DataValue6 = DateTime.Now.AddMinutes(2).ToString(DATE_FORMAT);
            model.DataKey7 = "default_bid";
            model.DataValue7 = "15.00";
            model.DataKey8 = "time_stamp_utc";
            model.DataValue8 = DateTime.Now.ToString(DATE_FORMAT);
            model.DataKey9 = "address";
            model.DataValue9 = "New York, NY";
            model.DataKey10 = "customer_name";
            model.DataValue10 = "Ofir *****";
            model.DataKey11 = "phone_number";
            model.DataValue11 = "123-123-****";
            return View(model);
        }

        /*
         * 
         *  { "id", ia.new_incidentaccountid.ToString() },
                    { "description", incident.description },
                    { "max_bid", maxPrice.ToString() },
                    { "min_bid", minPrice.ToString() },
                    { "category", incident.new_new_primaryexpertise_incident.new_name }
         * 
         */

        //
        // POST: /PushTesting/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                var client = CoreConnectors.ClientsFactory.GetClient<OneCallUtilitiesService.OneCallUtilities>();
                var coreRequest = new OneCallUtilitiesService.SendTestPushRequest()
                {
                    PushType = Models.PushTestModel.EPushType.BID_REQUEST.ToString(),
                    DeviceId = Convert.ToString(collection["DeviceId"])
                };
                List<OneCallUtilitiesService.PushDataPair> data = new List<OneCallUtilitiesService.PushDataPair>();
                for (int i = 1; i <= 10; i++)
                {
                    string keyName = "DataKey" + i;
                    string key = Convert.ToString(collection[keyName]);
                    if (!String.IsNullOrEmpty(key))
                    {
                        string valueName = "DataValue" + i;
                        data.Add(new OneCallUtilitiesService.PushDataPair() { Key = key, Value = Convert.ToString(collection[valueName]) });
                    }
                }
                coreRequest.Data = data.ToArray();
                bool result = client.SendTestPushNotificationIOS(coreRequest);
                return RedirectToAction("Create");
            }
            catch (Exception exc)
            {
                return View();
            }
        }

        //
        // GET: /PushTesting/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /PushTesting/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PushTesting/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /PushTesting/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
