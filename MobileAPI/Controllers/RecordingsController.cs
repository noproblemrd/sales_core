﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using ApiCommons.Controllers;
using log4net;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers
{
    public class RecordingsController : ControllersBase
    {
        private static readonly ILog logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        // GET api/recordings/5
        /// <summary>
        /// 
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="id">incidentaccountid</param>
        /// <returns></returns>
        public List<Recording> Get([FromUri]Guid supplierId, [FromUri]Guid id)
        {
            try
            {
                DashboardConnector connector = new DashboardConnector();
                List<Recording> recordings = connector.GetRecording(supplierId, id);
                return recordings;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }

        ////[ValidateMimeMultipartContentFilter]
        //[Filters.TokenAuthorizationFilter]
        //public async Task<HttpResponseMessage> Post([FromUri]Guid supplierId, [FromUri]Guid id)
        //{
        //    if (!Request.Content.IsMimeMultipartContent())
        //    {
        //        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted"));
        //    }
        //    try
        //    {
        //        CoreConnectors.DashboardConnector connector = new CoreConnectors.DashboardConnector();
        //        string path = connector.GetRecordingSaveLocation(supplierId, id);
        //        if (String.IsNullOrEmpty(path))
        //        {
        //            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest));
        //        }
        //        string fileName = id + "_" + DateTime.Now.Ticks;
        //        var streamProvider = new MobileAPI.ControllersHelpers.MultipartFileStreamProviderWithFileName(path, fileName);
        //        var readResult = Request.Content.ReadAsMultipartAsync(streamProvider);
        //        await readResult;
        //        if (streamProvider.SaveOk())
        //        {
        //            connector.PostNewRecoring(supplierId, id, Path.Combine(path, streamProvider.GetLocalFileName(null)));
        //            var done = new Task<HttpResponseMessage>(GetResponse);
        //            done.Start();
        //            return await done;
        //        }
        //        else
        //        {
        //            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.UnsupportedMediaType));
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        ExceptionHandler.HandleException(exc);
        //        return null;
        //    }
        //}

        private HttpResponseMessage GetResponse()
        {
            return new HttpResponseMessage(HttpStatusCode.Created);
        }
    }
}
