﻿using ApiCommons.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileAPI.Controllers
{
    public class LegalAndFaqController : ControllersBase
    {
        [HttpGet]
        public Models.PrivacyPolicy PrivacyPolicy()
        {
            try
            {
                return new Models.PrivacyPolicy();
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }

        [HttpGet]
        public Models.TermsOfService TermsOfService()
        {
            try
            {
                return new Models.TermsOfService();
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }

        [HttpGet]
        public Models.Faq FAQ()
        {
            try
            {
                return new Models.Faq();
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
    }
}
