﻿using ApiCommons.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileAPI.Controllers
{
    public class StatsController : ControllersBase
    {
        [HttpPost]
        public void AppUninstalled(Models.AppUninstalledData data)
        {
            ValidateModel(data, ModelState);
            try
            {
                CoreConnectors.GeneralConnector connector = new CoreConnectors.GeneralConnector();
                connector.AppUninstalled(data);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
            }
        }
    }
}
