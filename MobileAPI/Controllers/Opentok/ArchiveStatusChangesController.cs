﻿
using ClipCall.DomainModel.Entities.VideoInvitation.Request;
using ClipCall.ServiceModel.Abstract.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileAPI.Controllers.Opentok
{
     [RoutePrefix("api/opentok")]
    public class ArchiveStatusChangesController : ApiController
    {
        private readonly IVideoChatService videoChatService;

        public ArchiveStatusChangesController(IVideoChatService videoChatService)
        {
            this.videoChatService = videoChatService;
        }
         [HttpPost]
         [OverrideAuthorization]
         [Route("")]
         public void ArchiveStatusChanges(OpentokArchiveStatusChangesRequest request)
         {
             videoChatService.OpentokArchiveStatusChanges(request);
         }
    }
}
