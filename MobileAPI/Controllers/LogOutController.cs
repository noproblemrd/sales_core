﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers
{
    public class LogOutController : ControllersBase
    {
        // POST api/advertisers/{}/logout
        public StandardResponse Post([FromUri]Guid supplierId)
        {
            var connector = new LogOnConnector();
            connector.LogOut(supplierId);
            return new StandardResponse();
        }
    }
}
