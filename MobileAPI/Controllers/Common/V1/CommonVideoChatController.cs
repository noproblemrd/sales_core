﻿
using ClipCall.DomainModel.Entities.VideoInvitation.Request;
using ClipCall.DomainModel.Entities.VideoInvitation.Response;
using ClipCall.ServiceModel.Abstract.Services;
using MobileAPI.CustomerService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileAPI.Controllers.Common.V1
{
     [RoutePrefix("api/videochat")]
    public class CommonVideoChatController : ApiController
    {
        private readonly IVideoChatService videoChatService;

        public CommonVideoChatController(IVideoChatService videoChatService)
        {
            this.videoChatService = videoChatService;
        }
        [HttpPost]
        [Route("{invitationId:guid}")]
        [OverrideAuthorization]
        public InvitationCheckInResponse InvitationCheckIn([FromUri]Guid invitationId)
        {
            VideoChatRequestBase vcRequest = new VideoChatRequestBase()
            {
                invitationId = invitationId
            };
            return videoChatService.InvitationCheckIn(vcRequest);
        }
        [HttpGet]
        [Route("{invitationId:guid}/{categoryId:guid}")]
        [OverrideAuthorization]
        public ClipCall.DomainModel.Entities.VideoInvitation.Response.GetFavoriteServiceProviderResponse GetFavoriteServiceProvider([FromUri]Guid invitationId, [FromUri]Guid categoryId)
        {
            InvitationFavoriteRequest ifRequest = new InvitationFavoriteRequest()
            {
                invitationId = invitationId,
                ExpertiseId = categoryId
            };
           return videoChatService.GetFavoriteServiceProvider(ifRequest);
            
        }
    }
}
