﻿using MobileAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileAPI.Controllers.Common.V1
{
     [RoutePrefix("api/commontip")]
    public class CommonTipController : ApiController
    {
         [HttpGet]
         [OverrideAuthorization]
         [Route("{categoryId:guid}")]
         public List<Tip> Get([FromUri]Guid categoryId)//[FromBody]Models.TipsRequest tip_request)
         {
             //       ValidateModel(tip_request, ModelState);
             CoreConnectors.DataConnector connector = new CoreConnectors.DataConnector();
             Tips retVal = connector.GetTips(categoryId);// connector.GetTips(tip_request.ExpertiseId);
             if (retVal == null)
                 return new List<Tip>();
             return retVal.GetListOfTips();
         }
    }
}
