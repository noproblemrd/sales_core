﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.ControllersHelpers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Common.V1
{
     [RoutePrefix("api/categories")]
    public class CategoriesController : ControllersBase
    {
        // GET api/categories/GetByPrefix?prefix={prefix}
         /*
        public List<CategoryModel> GetByPrefix([FromUri]string prefix)
        {
            try
            {
                List<CategoryModel> retVal = CategoriesAutoCompleter.GetInstance().GetCategoriesByPrefix(prefix);
                return retVal;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
          */
        // GET api/categories/GetCategoryKeywords
        [HttpGet]
        [OverrideAuthorization]
        [Route("GetCategoryKeywords")]
        public List<Keyword> GetCategoryKeywords()
        {
            try
            {
                DataConnector connector = new DataConnector();
                var retVal = connector.GetAllKeywordsWithCategoy();
                return retVal;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
         /*
        // POST api/advertisers/{supplierId}/regcategories
        public StandardResponse Post([FromUri]Guid supplierId, [FromBody]List<CategoryModel> categories)
        {
            ValidateModel(categories, ModelState);
            try
            {
                RegistrationFlowConnector connector = new RegistrationFlowConnector();
                connector.SetCategories(supplierId, categories);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
            }
            return new StandardResponse();
        }

        public List<CategoryModel> Get([FromUri]Guid supplierId)
        {
            try
            {
                RegistrationFlowConnector connector = new RegistrationFlowConnector();
                return connector.GetCategories(supplierId);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
          * */
    }
}
