﻿using System.Web.Http;
using System.Web.Http.Cors;
using ApiCommons.Controllers;

namespace MobileAPI.Controllers.Common.V1
{
    [RoutePrefix("api/phonevalidation")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PhoneValidationController : ControllersBase
    {
        // POST api/phonevalidation/validate
        [HttpPost]
        [OverrideAuthorization]
        [Route("validate")]
        public Models.StandardResponse Validate([FromBody] Models.PhoneValidationRequest request)
        {
            ValidateModel(request, ModelState);
            var connector = new CoreConnectors.GeneralConnector();
            bool ok = connector.SendPhoneValidation(request);
            return new Models.StandardResponse(ok);
            
        }
    }
}
