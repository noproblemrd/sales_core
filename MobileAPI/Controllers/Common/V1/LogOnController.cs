﻿using System;
using System.Web.Http;
using System.Web.Http.Cors;
using log4net;
using System.Reflection;
using ApiCommons.Controllers;

namespace MobileAPI.Controllers.Common.V1
{
     [RoutePrefix("api/LogOn")]
    public class LogOnController : ControllersBase
    {
        private readonly CoreConnectors.LogOnConnector connector;

        private static readonly ILog logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public LogOnController()
        {
            connector = new CoreConnectors.LogOnConnector();
        }

        [HttpPost]
        [OverrideAuthorization]
       // [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("CustomerLogOn")]
        public Models.CustomerLogOnResponse CustomerLogOn([FromBody]Models.CustomerLogOnRequest request)
        {
            ValidateModel(request, ModelState);
            try
            {
                var result = connector.CustomerLogOn(request);
                return result;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
         /*
        [HttpPost]
        [OverrideAuthorization]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public Models.CustomerLogOnResponse SupplierLogin([FromBody]Models.CustomerLogOnRequest request)
        {
            ValidateModel(request, ModelState);
            try
            {
                var result = connector.CustomerLogOn(request);
                return result;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
         */

        /*
        [HttpPost]
        [OverrideAuthorization]
        public Models.LogOnResponse SocialLogOn([FromBody]Models.SocialLogOnRequest request)
        {
            logger.InfoFormat("POST: LogOn/SocialLogOn invoked. request = {0}", Newtonsoft.Json.JsonConvert.SerializeObject(request));
            ValidateModel(request, ModelState);
            try
            {
                var result = connector.SocialLogOn(request);
                return CreateApiResponseFromCoreResult(result);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
        */
        /*
        [HttpPost]
        [OverrideAuthorization]
        public Models.LogOnResponse Register([FromBody]Models.NormalLogOnRequest request)
        {
            ValidateModel(request, ModelState);
            try
            {
                return NormalLogOn(request, true);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
         * */
        /*
        // not in used ???
        [HttpPost]
        [OverrideAuthorization]
        public Models.LogOnResponse LogIn([FromBody]Models.NormalLogOnRequest request)
        {
            ValidateModel(request, ModelState);
            try
            {
                return NormalLogOn(request, false);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
        */
        [HttpPost]
        [OverrideAuthorization]
        [Route("ResetPassword")]
        public Models.StandardResponse ResetPassword([FromBody]Models.ResetPasswordRequest request)
        {
            ValidateModel(request, ModelState);
            try
            {
                bool ok = connector.ResetPassword(request.Email);
                return new Models.StandardResponse(ok);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
        /*
        private Models.LogOnResponse NormalLogOn(Models.NormalLogOnRequest request, bool isRegister)
        {
            var result = connector.NormalLogOn(request, isRegister);
            return CreateApiResponseFromCoreResult(result);
        }
        */
        private Models.LogOnResponse CreateApiResponseFromCoreResult(SupplierService.LogOnResponse result)
        {
            Models.LogOnResponse response = new Models.LogOnResponse(result.StatusCode);
            response.Step = Models.Steps.StepNumberToString(result.Step);
            response.SupplierId = result.SupplierId;
            response.Token = result.ApiToken;
            return response;
        }
    }
}
