﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers
{
    public class AlertsController : ControllersBase
    {
        public List<Alert> Get([FromUri]Guid supplierId)
        {
            try
            {
                GeneralConnector connector = new GeneralConnector();
                List<Alert> alerts = connector.GetAlerts(supplierId);
                return alerts;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
    }
}
