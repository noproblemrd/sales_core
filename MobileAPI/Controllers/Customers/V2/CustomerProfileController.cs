﻿using ApiCommons.Controllers;
using MobileAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileAPI.Controllers.Customers.V2
{
    [RoutePrefix("api/v2/customers/{customerId:guid}/profile")]
    public class CustomerProfileController : ControllersBase
    {
        [HttpPost]
        [Route("changephone")]
        public Models.CustomerLogOnResponse ChangePhoneNumber([FromUri]Guid customerId, [FromBody]CustomerLogOnRequest request)
        {
            ValidateModel(request, ModelState);
            CoreConnectors.LogOnConnector connector = new CoreConnectors.LogOnConnector();
            return connector.CustomerChangePhoneNumber(customerId, request);
        }
    }
}
