﻿using System;
using System.Web.Http;
using AutoMapper;
using Castle.Core.Logging;
using MobileAPI.CoreConnectors;
using MobileAPI.CustomerService;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Customers.V2
{

    //This is s sample controller for version 2,
    // the route prefix includes the version number, in this case v2
    // the name of the controller ends with  {versionNumber}Controller,
    // in this case V2Controller
    [RoutePrefix("api/v2/customers/{customerId:guid}/leads")]
    public class CustomerLeadsV2Controller : ApiController
    {
        private readonly CustomerLeadsConnector connector;

        public ILogger Logger { get; set; }

        public CustomerLeadsV2Controller(CustomerLeadsConnector connector)
        {
            this.connector = connector;
        }

        //This is s sample
        [Route("{leadId}")]
        public LeadModel GetLead([FromUri]Guid customerId, [FromUri]GetCustomerLeadRequest request)
        {
            GetCustomerLeadResponse response = connector.GetCustomerLead(request);
            LeadModel lead = Mapper.Map<ConsumerIncidentData, LeadModel>(response.Lead);
            return lead;
        }
       
    }    
}
