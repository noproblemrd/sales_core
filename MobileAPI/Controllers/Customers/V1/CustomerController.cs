﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ApiCommons.Controllers;
using AutoMapper;
using MobileAPI.CoreConnectors;
using MobileAPI.CustomerService;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Customers.V1
{
    [RoutePrefix("api/customers/{customerId:guid}")]
    public class CustomerController : ApiController
    {
        private readonly AmazonS3Connector amazonConnector;
        private static readonly string siteId = System.Configuration.ConfigurationManager.AppSettings["siteId"];

        public CustomerController(AmazonS3Connector amazonConnector)
        {
            this.amazonConnector = amazonConnector;
        }

        [Route("profile")]
        public CustomerProfileModel GetProfile([FromUri]Guid customerId)
        {
            var connector = new CustomerConnector();
            CustomerProfileData customerProfile = connector.GetCustomerProfile(customerId);
            CustomerProfileModel result = Mapper.Map<CustomerProfileData, CustomerProfileModel>(customerProfile);
            return result;
        }

        [Route("profile")]
        [HttpPost]
        public StandardResponse UpdateProfile([FromUri]Guid customerId, [FromBody]UpdateCustomerProfileRequest request)
        {
            var connector = new CustomerConnector();
            request.CustomerId = customerId;
            connector.UpdateProfile(request);
            return new StandardResponse();
        }


        [Route("profile/logo")]
        [HttpPost]
        public Logo updateProfileLogo([FromUri]Guid customerId)
        {
            try
            {
                amazonConnector.BaseDirectory = ConfigurationManager.AppSettings["amazonCustomerBaseDirectory"];
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        // string LogoFileName = UploadLogo(supplierId, postedFile);
                        string logoFileName = amazonConnector.UploadFile(postedFile, customerId);
                        var request = new UpdateCustomerProfileRequest
                        {
                            CustomerId = customerId,
                            ImageUrl = logoFileName,
                        };
                        var connector = new CustomerConnector();
                        request.CustomerId = customerId;
                        connector.UpdateProfile(request);

                        return new Logo{ Url = logoFileName };
                    }
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent("No file found") });
                }
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
            }
            return null;
        }

        [Route("settings")]
        [HttpGet]
        public HttpResponseMessage GetCustomerSettings([FromUri]Guid customerId)
        {
            var connector = new CustomerConnector();
            CustomerSettingsData customerSettings = connector.GetCustomerSettings(customerId);
            CustomerSettingsModel result = Mapper.Map<CustomerSettingsData, CustomerSettingsModel>(customerSettings);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("settings")]
        [HttpPost]
        public StandardResponse UpdateCustomerSettings([FromUri]Guid customerId, [FromBody]UpdateCustomerSettingsRequest request)
        {
            request.CustomerId = customerId;
            var connector = new CustomerConnector();
            connector.UpdateCustomerSettings(request);
            return new StandardResponse();
        }


        [Route("logout")]
        public StandardResponse Post([FromUri]Guid customerId, [FromBody] Guid deviceId)
        {
            var connector = new CustomerConnector();
            connector.LogOut(customerId, deviceId);
            return new StandardResponse();
        }
        
    }
}
