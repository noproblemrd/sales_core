﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.Extensibility.WebApi.Filters;
using MobileAPI.Models;

namespace MobileAPI.Controllers
{
    [RoutePrefix("api/customers/{customerId:guid}/tips")]
    public class TipsController : ControllersBase
    {
        // POST api/customers/{customerId}/tips/{id}
        [HttpGet]
        [Route("{id:guid}")]
        public List<Tip> Get([FromUri]Guid customerId, [FromUri]Guid id)//[FromBody]Models.TipsRequest tip_request)
        {
     //       ValidateModel(tip_request, ModelState);
            CoreConnectors.DataConnector connector = new CoreConnectors.DataConnector();
            Tips retVal = connector.GetTips(id);// connector.GetTips(tip_request.ExpertiseId);
            if (retVal == null)
                return new List<Tip>();
            return retVal.GetListOfTips();
        }

       
    }
}
