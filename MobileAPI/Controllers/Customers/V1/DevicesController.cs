﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Customers.V1
{
     [RoutePrefix("api/customers/{customerId:guid}")]
    public class DevicesController : ControllersBase
    {
        private readonly LogOnConnector connector = new LogOnConnector();

        [Route("devices")]
        public DeviceDataResponse Post([FromUri]Guid customerId, [FromBody]DeviceDataRequest request)
        {
            ValidateModel(request, ModelState);
            DeviceDataResponse response = connector.UpdateDeviceData(customerId, request);
            return response;
        }
    }
}
