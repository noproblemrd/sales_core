﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace MobileAPI.Controllers
{
    [RoutePrefix("api/customers/{customerId:guid}")]
    public class SwitchToAdvertiserController : ControllersBase
    {
        private readonly LogOnConnector connector;

        public SwitchToAdvertiserController()
        {
            connector = new LogOnConnector();
        }
        // POST api/customers/{customer_id}/switchtoadvertiser
        [Route("switchtoadvertiser")]
        public LogOnResponse Post([FromUri]Guid customerId)
        {
            IEnumerable<string> headerValues = Request.Headers.GetValues("nptoken");
            var result = connector.SwitchToAdvertiserMode(customerId, headerValues.FirstOrDefault());
            return result;
        }
    }
}
