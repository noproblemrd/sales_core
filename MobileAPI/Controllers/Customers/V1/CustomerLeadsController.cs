﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Castle.Core.Logging;
using ClipCall.ServiceModel.Abstract.DataContracts.Request;
using ClipCall.ServiceModel.Abstract.DataContracts.Response;
using ClipCall.ServiceModel.Abstract.Services;
using MobileAPI.CoreConnectors;
using MobileAPI.CustomerService;
using MobileAPI.Models;
using MobileAPI.Proxy;

namespace MobileAPI.Controllers.Customers.V1
{
    [RoutePrefix("api/customers/{customerId:guid}/leads")]
    public class CustomerLeadsController : ApiController
    {
        private readonly CustomerLeadsConnector connector;

        public ILogger Logger { get; set; }

        public CustomerLeadsController(CustomerLeadsConnector connector)
        {
            this.connector = connector;
        }


        [Route("")]
        public List<LeadModel> GetLeads([FromUri]Guid customerId, [FromUri]GetCustomerLeadsRequest request)
        {
            GetConsumerIncidentsResponse response = connector.GetCustomerLeads(request);
            List<LeadModel> leads = Mapper.Map<ConsumerIncidentData[], List<LeadModel>>(response.ConsumerIncidents);
            return leads;
        }

        [Route("{leadId}")]
        public LeadDetailsResponseData GetLeadDetails([FromUri]Guid customerId, [FromUri]LeadDetailsRequestData detailsRequest)
        {     
            var proxy = new ServiceWrapper<ICustomersService>();
            LeadDetailsResponseData detailsResponseData = proxy.Channel.GetLeadDetails(detailsRequest);
            return detailsResponseData;
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage CreateNewLead([FromUri]Guid customerId, [FromBody]NewVideoRequest lead)
        {
            lead.CustomerId = customerId;
            Guid leadId = connector.CreateNewLead(lead);
            if (leadId == Guid.Empty)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new StandardResponse(false));
            }
            return Request.CreateResponse(HttpStatusCode.OK, new NewLeadResponse {LeadId = leadId});
        }

        [HttpPost]
        [Route("{leadId:guid}/relaunch")]
        public StandardResponse RelaunchLead([FromUri]Guid customerId, [FromUri]Guid leadId)
        {            
            var request = new RelaunchLeadRequest() { CustomerId = customerId, LeadId = leadId };
            RelaunchLeadResponse reviveIncidentResposne = connector.RelaunchIncident(request);
            return new StandardResponse(reviveIncidentResposne.IsSuccess);
        }

        [HttpGet]
        [Route("{leadId:guid}/advertisers")]
        public HttpResponseMessage GetLeadAdvertisers([FromUri]Guid customerId, [FromUri] GetConsumerIncidentDataRequest request)
        {
            GetConsumerIncidentDataResponse response = connector.GetIncidentSuppliers(request);
            List<LeadAdvertiserModel> advertisers = Mapper.Map<ConsumerIncidentAccountData[], List<LeadAdvertiserModel>>(response.IncidentAccounts);
            return this.Request.CreateResponse(HttpStatusCode.OK, advertisers);
        }


        [HttpGet]
        [Route("{leadId:guid}/advertisers/{advertiserId:guid}/about")]
        public HttpResponseMessage GetCustomerAdvertiserAbout([FromUri]Guid customerId, [FromUri] GetCustomerAdvertiserAboutRequest request)
        {
            GetCustomerAdvertiserAboutResponse reviveIncidentResposne = connector.GetCustomerAdvertiserAbout(request);
            AdvertiserAboutDataModel aboutData = Mapper.Map<AdvertiserAboutData, AdvertiserAboutDataModel>(reviveIncidentResposne.AboutData);
            return this.Request.CreateResponse(HttpStatusCode.OK, aboutData);
        }

        [HttpGet]
        [Route("{leadAccountId:guid}/advertisers/{advertiserId:guid}/calls/history")]
        public HttpResponseMessage GetAdvertiserCallHistory([FromUri]Guid customerId, [FromUri] GetAdvertiserCallHistoryRequest request)
        {
            GetAdvertiserCallHistoryResponse reviveIncidentResposne = connector.GetAdvertiserCallHistory(request);
            List<CallRecordModel> callRecords = Mapper.Map<CallRecordData[], List<CallRecordModel>>(reviveIncidentResposne.CallRecords);            
            return this.Request.CreateResponse(HttpStatusCode.OK, callRecords);
        }

        [HttpPost]
        [Route("{leadId:guid}/closelead")]
        public StandardResponse CloseLeadRequest([FromUri]Guid customerId, [FromUri]Guid leadId)
        {
            CloseLeadRequest request = new CloseLeadRequest() { CustomerId = customerId, LeadId = leadId };
            CloseLeadResponse cancelFutureCallsResponse = connector.CloseLeadRequest(request);
            return new StandardResponse(cancelFutureCallsResponse.IsSuccess);
        }
        [HttpGet]
        [Route("{categoryId:guid}/favorite")]
        public GetFavoriteServiceProviderResponse[] GetFavoriteServiceProvider([FromUri]Guid customerId, [FromUri]Guid categoryId)
        {
            GetFavoriteServiceProviderRequest request = new GetFavoriteServiceProviderRequest
            {
                CustomerId = customerId,
                ExpertiseId = categoryId
            };
            GetFavoriteServiceProviderResponse[] response = connector.GetFavoriteServiceProvider(request);
            return response;
        }
    }    
}
