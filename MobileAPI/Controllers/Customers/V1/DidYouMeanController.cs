﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Customers.V1
{
    [RoutePrefix("api/customers/{customerId:guid}")]
    public class DidYouMeanController : ControllersBase
    {
        [Route("didyoumean")]
        public DidYouMeanResponse Get([FromUri]Guid customerId, [FromUri]string search)
        {
            var connector = new CustomerLeadsConnector();
            DidYouMeanResponse response = connector.DidYouMean(search);
            return response;
        }
    }
}
