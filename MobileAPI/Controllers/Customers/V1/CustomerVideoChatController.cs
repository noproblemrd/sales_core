﻿using System.Web.Http;
using ClipCall.ServiceModel.Abstract.Services;
using System;
using ClipCall.DomainModel.Entities.VideoInvitation.Response;
using ClipCall.DomainModel.Entities.VideoInvitation.Request;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Customers.V1
{
    [RoutePrefix("api/customers/{customerId:guid}/videochat")]
    public class CustomerVideoChatController : ApiController
    {
        private readonly IVideoChatService videoChatService;

        public CustomerVideoChatController(IVideoChatService videoChatService)
        {
            this.videoChatService = videoChatService;
        }

        
        [HttpPost]
        [Route("{invitationId:guid}/call")]
        public CreateVideoChatConversationResponse JoinVideoChatConversation([FromUri]Guid customerId, [FromUri]Guid invitationId)
        {
            VideoChatCustomerBaseRequest vcRequest = new VideoChatCustomerBaseRequest()
            {
                CustomerId = customerId,
                invitationId = invitationId
            };
            return videoChatService.CreateVideoChatCall(vcRequest);
        }
        [HttpPost]
        [Route("{invitationId:guid}/setfavorite")]
        public StandardResponse SetFavorite([FromUri]Guid customerId, [FromUri]Guid invitationId)
        {
            VideoChatCustomerBaseRequest vcRequest = new VideoChatCustomerBaseRequest()
            {
                CustomerId = customerId,
                invitationId = invitationId
            };
            videoChatService.SetFavoriteServiceProvider(vcRequest);
            return new Models.StandardResponse(true);
        }
       
        
    }
}