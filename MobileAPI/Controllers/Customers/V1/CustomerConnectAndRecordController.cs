﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;

namespace MobileAPI.Controllers.Customers.V1
{
    [RoutePrefix("api/customers/{customerId:guid}")]
    public class CustomerConnectAndRecordController : ControllersBase
    {
        [HttpPost]
        [Route("connectandrecord/{incidentAccountId:guid}")]
        public Models.StandardResponse Post([FromUri]Guid customerId, [FromUri]Guid incidentAccountId)
        {
            var connector = new CoreConnectors.GeneralConnector();
            var result = connector.ConnectAndRecordConsumer(customerId, incidentAccountId);
            return new Models.StandardResponse(result);
        }
        
    }
}
