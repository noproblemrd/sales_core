﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Customers.V1
{
     [RoutePrefix("api/customers/{customerId:guid}/love")]
    public class LoveController : ControllersBase
    {
         [HttpPost]
         [Route("{SupplierId:guid}")]
         public HttpResponseMessage Post([FromUri]Guid customerId, [FromUri]Guid SupplierId)
         {
             LoveConnector connector = new LoveConnector();
             connector.AddLove(customerId, SupplierId);
             return this.Request.CreateResponse(HttpStatusCode.OK,new StandardResponse(true));
         }
         /*
         [HttpGet]
         [Route("{SupplierId:guid}")]
         public HttpResponseMessage Get([FromUri]Guid customerId, [FromUri]Guid SupplierId)
         {
             LoveConnector connector = new LoveConnector();
             CustomerService.LoveResponse _response = connector.GetLoves(customerId, SupplierId);
       //      var result = new { love_count = count };
             return this.Request.CreateResponse(HttpStatusCode.OK, _response);
         }
          */
    }
}
