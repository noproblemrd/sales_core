﻿using ApiCommons.Controllers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using ClipCall.ServiceModel.Abstract.DataContracts.Request;
using ClipCall.ServiceModel.Abstract.DataContracts.Response;
using ClipCall.ServiceModel.Abstract.Services;
using MobileAPI.Proxy;

namespace MobileAPI.Controllers
{
    [RoutePrefix("api/suppliers/{supplierId:guid}/refunds")]
    public class RefundsController : ApiController
    {
        private readonly CoreConnectors.GeneralConnector connector = new CoreConnectors.GeneralConnector();

        [HttpGet]
        [Route("")]
        public List<LeadReportStatusData> GetRefundReasons()
        {
            //return connector.GetRefundReasons();
            var proxy = new ServiceWrapper<ISuppliersService>();
            List<LeadReportStatusData> leadReportStatusDatas = proxy.Channel.GetLeadReportStatuses();
            return leadReportStatusDatas;
        }

        [HttpPost]
        [Route("{leadAccountId:guid}")]
        public Models.StandardResponse Post([FromUri]Guid supplierId, [FromUri]Guid leadAccountId, [FromBody]Models.RefundRequest request)
        {
            var proxy = new ServiceWrapper<ISuppliersService>();
            proxy.Channel.ReportLead(new LeadReportRequestData
            {
                Comment = request.Comment,
                SupplierId = supplierId,
                IncidentAccountId = leadAccountId,
                ReasonCode = request.ReasonCode
            });
            return new Models.StandardResponse(ok:true);
            
        }
    }
}
