﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers
{
    public class FeedbackController : ControllersBase
    {
        public StandardResponse Post([FromUri]Guid supplierId, [FromBody] Feedback feedback)
        {
            ValidateModel(feedback, ModelState);
            try
            {
                GeneralConnector connector = new GeneralConnector();
                connector.SendFeedback(supplierId, feedback.Header, feedback.Comment);
                return new StandardResponse();
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
    }
}
