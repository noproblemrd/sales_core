﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.Models;

namespace MobileAPI.Controllers
{
    public class ProjectsController : ControllersBase
    {
        //Get all the customer's projects
        // GET api/customers/{customer_id}/projects
        public StandardResponse Get([FromUri]Guid customerId)
        {
            try
            {
                //not implemented yet!!!
                return new StandardResponse();
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
    }
}
