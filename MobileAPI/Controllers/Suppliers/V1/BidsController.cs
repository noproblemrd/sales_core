﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Suppliers.V1
{
    [RoutePrefix("api/suppliers/{supplierId:guid}/bids")]
    public class BidsController : ControllersBase
    {
        // POST api/advertisers/{supplier_id}/bids/{leadAccountId}
        [HttpPost]
        [Route("{leadAccountId:guid}")]
        public SetBidResult Post([FromUri]Guid supplierId, [FromUri]Guid leadAccountId, [FromBody] SetBidRequest request)
        {
            ValidateModel(request, ModelState);
            try
            {
                BiddingConnector connector = new BiddingConnector();
                SetBidResult result = connector.SetBid(supplierId, leadAccountId, request);
                return result;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
        [HttpGet]
        [Route("")]
        public List<LiveBid> Get([FromUri]Guid supplierId)
        {
            try
            {
                BiddingConnector connector = new BiddingConnector();
                var result = connector.GetLiveBids(supplierId);
                return result;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
    }
}
