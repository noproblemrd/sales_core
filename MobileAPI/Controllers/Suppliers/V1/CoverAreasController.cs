﻿using ApiCommons.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MobileAPI.Extensibility.WebApi.Filters;

namespace MobileAPI.Controllers.Suppliers.V1
{
      [RoutePrefix("api/suppliers/{supplierId:guid}/CoverAreas")]
    public class CoverAreasController : ControllersBase
    {
        CoreConnectors.RegistrationFlowConnector connector;

        public CoverAreasController()
        {
            connector = new CoreConnectors.RegistrationFlowConnector();
        }

        // GET api/regareaoptions
          /*
        public Models.CoverAreaData Get([FromUri]Guid supplierId, [FromUri]decimal? latitude, [FromUri]decimal? longitude)
        {
            ValidateQueryStringParameters(
                new ApiCommons.Models.QueryStringValidationObject(new Models.LatitudeValidationAttribute(), latitude),
                new ApiCommons.Models.QueryStringValidationObject(new Models.LongitudeValidationAttribute(), longitude)
                );
            Models.CoverAreaData retVal = connector.GetCoverAreas(latitude.Value, longitude.Value);
            return retVal;
        }
           * */
        [HttpGet]
        [Route("")]
        public Models.CoverAreaData Get([FromUri]Guid supplierId)
        {
            try
            {
                var result = connector.GetCoverAreas(supplierId);
                return result;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }

        [HttpPost]
        [Route("")]
        public Models.StandardResponse Post([FromUri]Guid supplierId, [FromBody]Models.SetCoverAreaData coverArea)
        {
            ValidateModel(coverArea, ModelState);
            try
            {
                connector.SetCoverAreas(supplierId, coverArea);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
            }
            return new Models.StandardResponse();
        }
        [HttpPost]
        [Route("bussinessaddress")]
        public Models.CoverAreaData SetBussinessAddress([FromUri]Guid supplierId, [FromBody]Models.BusinessAddress businessAddress)
        {
            try
            {
                var result = connector.SetBusinessAddress(supplierId, businessAddress);
                return result;
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
    }
}
