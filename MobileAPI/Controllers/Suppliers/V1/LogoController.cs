﻿using ApiCommons.Controllers;
using Castle.Core.Logging;
using MobileAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MobileAPI.Extensibility.WebApi.Filters;
using MobileAPI.CoreConnectors;

namespace MobileAPI.Controllers.Suppliers.V1
{
    [RoutePrefix("api/suppliers/{supplierId:guid}/logo")]
    public class LogoController : ControllersBase
    {
        private static readonly string professionalLogosWeb = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];
        private static readonly string professionalLogos = System.Configuration.ConfigurationManager.AppSettings["professionalLogos"];
  //      private static readonly string connectionString = System.Configuration.ConfigurationManager.AppSettings["connectionString"];
        private static readonly string siteId = System.Configuration.ConfigurationManager.AppSettings["siteId"];


        public ILogger Logger { get; set; }
        public AmazonS3Connector amazonConnector { get; set; }
        [Route("")]
        [HttpPost]
        public Logo Post([FromUri]Guid supplierId)
        {
            try
            {
                amazonConnector.BaseDirectory = ConfigurationManager.AppSettings["amazonBaseDirectory"];
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        RegistrationFlowConnector connector = new RegistrationFlowConnector();
                        var postedFile = httpRequest.Files[file];
                       // string LogoFileName = UploadLogo(supplierId, postedFile);
                        string LogoFileName = amazonConnector.UploadFile(postedFile, supplierId);
                        SupplierService.SupplierLogoUploadRequest _request = new SupplierService.SupplierLogoUploadRequest();
                        _request.ImageUrl = LogoFileName;// postedFile.FileName;
                        _request.SiteId = siteId;
                        _request.SupplierId = supplierId;
                        return new Logo() { Url = connector.UpdateLogoSupplier(_request).ImageUrl };
                    }
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent("No file found") });
                }
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
            }
            return null;
        }
        [Route("")]
        [HttpGet]
        public Logo Get([FromUri]Guid supplierId)
        {
            try
            {
                RegistrationFlowConnector connector = new RegistrationFlowConnector();
                string _image = connector.GetLogoSupplier(supplierId).ImageUrl;
                return new Logo()
                {
                    //Url = GetLogoUrl(supplierId)
                    
                    Url = string.IsNullOrEmpty(_image) ? null : _image + "?rnd=" + DateTime.Now.Millisecond
                };
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
        /*
        public static string GetLogoUrl(Guid supplierId)
        {
            const string command = "EXEC dbo.GetAdvertiserDetails @UserId, @SiteNameId";
            string logo = null;
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                var cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@UserId", supplierId);
                cmd.Parameters.AddWithValue("@SiteNameId", siteId);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (!reader.IsDBNull(1))
                        logo = reader.GetString(1);
                }
                conn.Close();
            }
            string logoUrl;
            if (!string.IsNullOrEmpty(logo))
            {
                logoUrl = professionalLogosWeb + supplierId + "/" + logo + "?rnd=" + DateTime.Now.Millisecond;
            }
            else
            {
                logoUrl = string.Empty;
            }
            return logoUrl;
        }
        */
        /*
        private static readonly List<string> supportedFormats =
            new List<string>(){
                ".jpg", ".png", ".gif", ".jpeg"
            };

        private string UploadLogo(Guid supplierId, HttpPostedFile postedFile)
        {
            string _path = professionalLogos + supplierId + @"\";
            string uploadedFile = postedFile.FileName;        
            string _name = System.IO.Path.GetFileNameWithoutExtension(uploadedFile);
            string _extension = System.IO.Path.GetExtension(uploadedFile);
            string new_name = CleanSpecialCharsUrlFreindly3(_name) + "_logo" + _extension;

            

            if (postedFile.ContentType.ToLower() == "image/jpeg")
            {
                _extension = ".jpeg";
            }
            
            if (string.IsNullOrEmpty(_extension) || !supportedFormats.Contains(_extension.ToLower()))
            {
                throw new ArgumentException("Invalid file format. Supported formats are JPG, PNG, GIF and JPEG");
            }


            

            string str_path_standard = _path + new_name;
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);
            try
            {
                if (str_path_standard.IndexOf("?") >= 0)
                {
                    str_path_standard = str_path_standard.Substring(0, str_path_standard.IndexOf("?"));
                }

                Logger.DebugFormat("Uploding logo, Path : {0}, ", str_path_standard);
                postedFile.SaveAs(str_path_standard);
            }
            catch (Exception ex)
            {
                Logger.Error("Failed saving uploaded logo with error" + ex.Message, ex);
                throw new IOException("Unable to write file to disk");
            }
            /*
            string command = "EXEC dbo.SetAdvertiser @UserId, @SiteId, @LogoFile";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@UserId", supplierId);
                cmd.Parameters.AddWithValue("@SiteId", siteId);
                cmd.Parameters.AddWithValue("@LogoFile", new_name);
                cmd.ExecuteScalar();
                conn.Close();
            }
             * * /
            return new_name;
        }

        public static string CleanSpecialCharsUrlFreindly3(string param)
        {
            string tempParam = "";

            if (!string.IsNullOrEmpty(param))
            {
                tempParam = param.Replace("&", "");
                tempParam = tempParam.Replace("/", "");
                tempParam = tempParam.Replace("\"", "");
                tempParam = tempParam.Replace("\'", "");
                tempParam = tempParam.Replace(".", "");
                tempParam = tempParam.Replace("+", "");
            }

            return tempParam;
        }
         */
    }
}
