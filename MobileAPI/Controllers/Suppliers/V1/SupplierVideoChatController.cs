﻿using System.Web.Http;
using ClipCall.ServiceModel.Abstract.Services;
using System;
using ClipCall.DomainModel.Entities.VideoInvitation.Response;
using ClipCall.DomainModel.Entities.VideoInvitation.Request;
using MobileAPI.Models.Invitation;

namespace MobileAPI.Controllers.Suppliers.V1
{
    [RoutePrefix("api/suppliers/{supplierId:guid}/videochat")]
    public class SupplierVideoChatController : ApiController
    {
        private readonly IVideoChatService videoChatService;

        public SupplierVideoChatController(IVideoChatService videoChatService)
        {
            this.videoChatService = videoChatService;
        }
        [HttpPost]
      //  [OverrideAuthorization]
        [Route("create")]
        public CreateVideoChatInvitationResponse CreateVideoChatInvitation([FromUri]Guid supplierId, [FromBody]InvitationRequest request)
        {
            CreateInvitationRequest invitationRequest = new CreateInvitationRequest();
            invitationRequest.SupplierId = supplierId;
            invitationRequest.InvitationType = (request != null && request.invitationType.HasValue) ? request.invitationType.Value :
                ClipCall.DomainModel.Entities.VideoInvitation.Request.CreateInvitationRequest.eInvitationType.VIDEO_CHAT;

            CreateVideoChatInvitationResponse response = videoChatService.CreateVideoChatInvitation(invitationRequest);
            return response;
        }
       
        [HttpPost]
        [Route("{invitationId:guid}/sentsms")]
        public Models.StandardResponse OnSentTextMessage([FromUri]Guid supplierId, [FromUri]Guid invitationId)
        {
            VideoChatSupplierBaseRequest vcRequest = new VideoChatSupplierBaseRequest()
            {
                SupplierId = supplierId,
                invitationId = invitationId
            };
            videoChatService.OnSentTextMessage(vcRequest);
            return new Models.StandardResponse(true);
        }

        [HttpPost]
        [Route("{invitationId:guid}/answer")]
        public Models.StandardResponse OnSupplierAnswer([FromUri]Guid supplierId, [FromUri]Guid invitationId)
        {
            VideoChatSupplierBaseRequest vcRequest = new VideoChatSupplierBaseRequest()
            {
                SupplierId = supplierId,
                invitationId = invitationId
            };
            videoChatService.OnSupplierAnswer(vcRequest);
            return new Models.StandardResponse(true);
        }
        
    }
}