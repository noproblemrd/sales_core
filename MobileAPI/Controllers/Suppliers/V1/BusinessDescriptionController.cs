﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Suppliers.V1
{
     [RoutePrefix("api/suppliers/{supplierId:guid}/businessdescription")]
    public class BusinessDescriptionController : ControllersBase
    {
        RegistrationFlowConnector connector;

        public BusinessDescriptionController()
        {
            connector = new RegistrationFlowConnector();
        }

         [HttpGet]
         [Route("")]
        public BusinessDescription Get([FromUri]Guid supplierId)
        {
            try
            {
                return connector.GetBusinessDescription(supplierId);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }

         [HttpPost]
         [Route("")]
        public StandardResponse Post([FromUri]Guid supplierId, [FromBody]BusinessDescription businessDescription)
        {
            ValidateModel(businessDescription, ModelState);
            try
            {
                connector.UpdateBusinessDescription(supplierId, businessDescription);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
            }
            return new StandardResponse();
        }
    }
}
