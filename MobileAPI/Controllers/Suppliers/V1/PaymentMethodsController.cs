﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Suppliers.V1
{
    [RoutePrefix("api/suppliers/{supplierId:guid}/paymentmethods")]
    public class PaymentMethodsController : ControllersBase
    {
        [HttpPost]
        [Route("")]
        public Models.StandardResponse Post([FromUri]Guid supplierId, [FromBody]Models.PaymentMethod paymentMethod)
        {
            ValidateModel(paymentMethod, ModelState);
            try
            {
                CoreConnectors.RegistrationFlowConnector connector = new CoreConnectors.RegistrationFlowConnector();
                bool result = connector.CreatePaymentMethod(paymentMethod, supplierId);
                return new Models.StandardResponse(result);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
        [HttpGet]
        [Route("")]
        public PaymentMethodResponse Get([FromUri]Guid supplierId)
        {
            try
            {
                var connector = new CoreConnectors.RegistrationFlowConnector();
                PaymentMethod result = connector.GetPaymentMethod(supplierId);
                var response = new PaymentMethodResponse()
                {
                   PaymentMethod = result,
                   Status = result == null ? "FAILED" : "OK"
                };
                return response;

            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
    }
}
