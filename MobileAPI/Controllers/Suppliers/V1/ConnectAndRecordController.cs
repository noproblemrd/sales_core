﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Suppliers.V1
{
    [RoutePrefix("api/suppliers/{supplierId:guid}/connectandrecord")]
    public class ConnectAndRecordController : ControllersBase
    {
        [HttpPost]
        [Route("{leadAccountId:guid}")]
        public StandardResponse Post([FromUri]Guid supplierId, [FromUri]Guid leadAccountId)
        {
            var connector = new GeneralConnector();
            var result = connector.ConnectAndRecord(supplierId, leadAccountId);
            return new StandardResponse(result);
        }
    }
}
