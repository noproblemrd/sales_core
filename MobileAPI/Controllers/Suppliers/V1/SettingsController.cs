﻿using ApiCommons.Controllers;
using System;
using System.Web.Http;

namespace MobileAPI.Controllers.Suppliers.V1
{
    [RoutePrefix("api/suppliers/{supplierId:guid}/settings")]
    public class SettingsController : ControllersBase
    {
        private readonly CoreConnectors.GeneralConnector connector = new CoreConnectors.GeneralConnector();

        [HttpGet]
        [Route("")]
        public Models.Settings Get([FromUri]Guid supplierId)
        {
            return connector.GetSettings(supplierId);
        }
        [HttpPost]
        [Route("")]
        public Models.StandardResponse Post([FromUri]Guid supplierId, [FromBody]Models.Settings settings)
        {
            ValidateModel(settings, ModelState);
            connector.SetSettings(supplierId, settings);
            return new Models.StandardResponse();
        }
    }
}
