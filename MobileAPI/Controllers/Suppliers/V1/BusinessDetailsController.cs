﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Suppliers.V1
{
    [RoutePrefix("api/suppliers/{supplierId:guid}/businessdetails")]
    public class BusinessDetailsController : ApiController
    {
        CoreConnectors.RegistrationFlowConnector connector;

        public BusinessDetailsController()
        {
            connector = new CoreConnectors.RegistrationFlowConnector();
        }

        // POST api/advertisers/{supplierId}/RegBusinessDetails

        [Route("")]
        public StandardResponse Post([FromUri]Guid supplierId, [FromBody]BusinessDetails businessDetails)
        {
            //ValidateModel(businessDetails, ModelState);
            try
            {
                connector.UpdateBusinessDetails(supplierId, businessDetails);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
            }
            return new StandardResponse();
        }
        [Route("")]
        public BusinessDetails Get([FromUri]Guid supplierId)
        {
            try
            {
                return connector.GetBusinessDetails(supplierId);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
    }
}
