﻿using System;
using System.Web.Http;
using ApiCommons.Controllers;
using ApiCommons.Models;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Suppliers.V1
{
    [RoutePrefix("api/suppliers/{supplierId:guid}/leads")]
    public class SupplierLeadsController : ControllersBase
    {
        private readonly DashboardConnector connector;

        public SupplierLeadsController(DashboardConnector connector)
        {
            this.connector = connector;
        }

        // GET api/suppliers/{supplier_id}/leads/leadStatus
        [HttpGet]
        [Route("")]
        public LeadsListResponse GetLeads([FromUri]Guid supplierId, [FromUri]DashboardLeadsRequest leadRequest)//, [FromUri]int? pageNumber, [FromUri]int? pageSize)
        {
            
            ValidateQueryStringParameters(
               new QueryStringValidationObject(new PageNumberValidationAttribute(), leadRequest.PageNumber),
               new QueryStringValidationObject(new PageSizeValidationAttribute(), leadRequest.PageSize)
               );

            var response = connector.GetLeadsList(supplierId, leadRequest.supplierLeadStatus, leadRequest.PageSize.Value,
                leadRequest.PageNumber.Value);//pageSize.Value, pageNumber.Value);
            return response;
        }

        // GET api/suppliers/{supplier_id}/leads/{leadAccountId}
        [HttpGet]
        [Route("{leadAccountId:guid}")]
        public SupplierLeadModel Get([FromUri]Guid supplierId, [FromUri]Guid leadAccountId)
        {
            SupplierLeadModel lead = connector.GetSupplierLead(supplierId, leadAccountId);
            return lead;
        }
        
        public StandardResponse Post([FromUri]Guid supplierId, [FromUri]Guid id, [FromBody]LeadDataSupplierUpdate data)
        {
            ValidateModel(data, ModelState);
            try
            {
                DashboardConnector connector = new DashboardConnector();
                switch (data.FieldToUpdate)
                {
                    case LeadDataSupplierUpdate.FieldsAvailable.Address:
                        connector.UpdateLeadAddressBySupplier(data.NewValue, id);
                        break;
                    case LeadDataSupplierUpdate.FieldsAvailable.CustomerName:
                        connector.UpdateLeadCustomerNameBySupplier(data.NewValue, id);
                        break;
                }
                return new StandardResponse();
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
         
    }
}
