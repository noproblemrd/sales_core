﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiCommons.Controllers;
using MobileAPI.ControllersHelpers;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;

namespace MobileAPI.Controllers.Suppliers.V1
{
    [RoutePrefix("api/suppliers/{supplierId:guid}/Categories")]
    public class SupplierCategoriesController : ControllersBase
    {
        [HttpPost]
        [Route("")]
        public StandardResponse Post([FromUri]Guid supplierId, [FromBody]List<CategoryModel> categories)
        {
            ValidateModel(categories, ModelState);
            try
            {
                RegistrationFlowConnector connector = new RegistrationFlowConnector();
                connector.SetCategories(supplierId, categories);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
            }
            return new StandardResponse();
        }
        [HttpGet]
        [Route("")]
        public List<CategoryModel> Get([FromUri]Guid supplierId)
        {
            try
            {
                RegistrationFlowConnector connector = new RegistrationFlowConnector();
                return connector.GetCategories(supplierId);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
                return null;
            }
        }
    }
}
