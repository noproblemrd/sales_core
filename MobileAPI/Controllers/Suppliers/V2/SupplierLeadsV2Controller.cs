﻿using ApiCommons.Controllers;
using ApiCommons.Models;
using MobileAPI.CoreConnectors;
using MobileAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MobileAPI.Controllers.Suppliers.V2
{
    [RoutePrefix("api/v2/suppliers/{supplierId:guid}/leads")]
    public class SupplierLeadsV2Controller : ControllersBase
    {
         private readonly DashboardConnector connector;

         public SupplierLeadsV2Controller(DashboardConnector connector)
        {
            this.connector = connector;
        }
        /*
         [HttpGet]
         [Route("")]
         public LeadsListResponse GetLeads([FromUri]Guid supplierId, [FromUri]SupplierLeadsRequest leadRequest)//, [FromUri]int? pageNumber, [FromUri]int? pageSize)
         {

             ValidateQueryStringParameters(
                new QueryStringValidationObject(new PageNumberValidationAttribute(), leadRequest.PageNumber),
                new QueryStringValidationObject(new PageSizeValidationAttribute(), leadRequest.PageSize)
                );

             var response = connector.GetLeadsListV2(supplierId, leadRequest.PageSize.Value,
                 leadRequest.PageNumber.Value);//pageSize.Value, pageNumber.Value);
             return response;
         }
         */
    }
}
