﻿using MobileAPI.Models.Supplier.v2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiCommons.Controllers;

namespace MobileAPI.Controllers.Suppliers.V2
{
    [RoutePrefix("api/v2/suppliers/{supplierId:guid}/businessdetails")]
    public class BusinessDetailsV2Controller : ApiController
    {
         CoreConnectors.RegistrationFlowConnector connector;

         public BusinessDetailsV2Controller()
        {
            connector = new CoreConnectors.RegistrationFlowConnector();
        }

        // POST api/advertisers/{supplierId}/RegBusinessDetails

        [Route("")]
        [HttpPost]
        public MobileAPI.Models.StandardResponse Post([FromUri]Guid supplierId, [FromBody]BusinessDetails businessDetails)
        {
            //ValidateModel(businessDetails, ModelState);
            try
            {
                connector.UpdateBusinessDetails(supplierId, businessDetails);
            }
            catch (Exception exc)
            {
                ExceptionHandler.HandleException(exc);
            }
            return new MobileAPI.Models.StandardResponse();
        }
       
    }
}
