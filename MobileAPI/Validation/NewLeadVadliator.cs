﻿using System.Reflection;
using FluentValidation;
using MobileAPI.CustomerService;
using MobileAPI.Models;

namespace MobileAPI.Validation
{
    public class NewLeadVadliator : AbstractValidator<NewVideoRequest>
    {
        public NewLeadVadliator()
        {
            RuleFor(l => l.VideoLeadUrl).NotEmpty();
        }
    }
}