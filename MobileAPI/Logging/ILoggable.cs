﻿using System;
using System.Collections.Generic;
using System.Text;
using Castle.Windsor;
using MobileAPI.Models;
using Newtonsoft.Json;

namespace MobileAPI.Logging
{
    public interface ILoggable
    {
        string Description(object input);
    }

    public interface ILoggable<in T>
    {
        string Description(T input);
    }

    public abstract class AbstractLoggable<T> :ILoggable<T>, ILoggable
    {
        public abstract string Description(T input);

        string ILoggable.Description(object input)
        {
            return Description((T)input);
        }
    }

    public class KeywordsLog : AbstractLoggable<List<Keyword>>
    {
        public override string Description(List<Keyword> input)
        {

            int totalItemsToLog = Math.Min(input.Count, 4);

            var builder = new StringBuilder();
            builder.AppendLine(string.Format("Total Categories: {0}", input.Count));
            builder.AppendLine("[");
            for (int i = 0; i < totalItemsToLog; i++)
            {
                Keyword keyword = input[i];
                builder.AppendLine(JsonConvert.SerializeObject(keyword)+",");
            }

            builder.AppendLine("...");
            builder.AppendLine("]");
            return builder.ToString();
        }
    }

    public class LoggableSelector
    {
        private readonly IWindsorContainer container;

        public LoggableSelector(IWindsorContainer container)
        {
            this.container = container;
        }

        public virtual ILoggable SelectLoggable(object input)
        {
            var loggableType = typeof(ILoggable<>).MakeGenericType(input.GetType());

            if (!container.Kernel.HasComponent(loggableType))
                return null;
            var loggable = container.Resolve(loggableType) as ILoggable;
            return loggable;
        }
    }
}
