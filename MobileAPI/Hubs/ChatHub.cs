﻿using System.Diagnostics;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Microsoft.AspNet.SignalR;

namespace MobileAPI.Hubs
{
    public class ChatHub : Hub
    {

        public ILogger Logger { get; set; }

        private static int _userCount = 0;

        public void Send(string name, string message)
        {
            // Call the broadcastMessage method to update clients.

            var totalUsers = _userCount;
            Clients.Others.foo(message);
        }


        public void SendChatMessage(string senderId, string message)
        {
            Clients.All.onChatMessageReceived(senderId, message);
        }



        public override Task OnConnected()
        {
            _userCount++;
            return base.OnConnected();
        }
        public override Task OnReconnected()
        {
            _userCount++;
            return base.OnReconnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            _userCount--;
            return base.OnDisconnected(stopCalled);
        }
    }
}