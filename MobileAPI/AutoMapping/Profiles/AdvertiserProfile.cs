﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
//using MobileAPI.CustomerService;
using MobileAPI.Models;
using MobileAPI.SupplierService;

namespace MobileAPI.AutoMapping.Profiles
{
    public class AdvertiserProfile : Profile
    {
        protected override void Configure()
        {

            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();
            /*
            CreateMap<Category, CategoryModel>()
                  .ForMember(d => d.CategoryId, o => o.MapFrom(s => s.Id))
                  .ForMember(d => d.CategoryName, o => o.MapFrom(s => s.Name));


            CreateMap<AdvertiserAboutData, AdvertiserAboutDataModel>();
             * */
            CreateMap<SupplierLogoResponse, LogoSupplier>();


           
        }

        public override string ProfileName
        {
            get { return this.GetType().Name; }
        }
    }
}