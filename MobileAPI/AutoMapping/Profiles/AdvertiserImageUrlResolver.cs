using System;
using System.Data.SqlClient;
using AutoMapper;
using MobileAPI.CustomerService;

namespace MobileAPI.AutoMapping.Profiles
{
    public class AdvertiserImageUrlResolver : ValueResolver<ConsumerIncidentAccountData,string>
    {
        private static readonly string professionalLogosWeb = System.Configuration.ConfigurationManager.AppSettings["professionalLogosWeb"];
        private static readonly string connectionString = System.Configuration.ConfigurationManager.AppSettings["connectionString"];
        private static readonly string siteId = System.Configuration.ConfigurationManager.AppSettings["siteId"];

        protected override string ResolveCore(ConsumerIncidentAccountData source)
        {
            const string command = "EXEC dbo.GetAdvertiserDetails @UserId, @SiteNameId";
            string logo = null;
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                var cmd = new SqlCommand(command, conn);
                cmd.Parameters.AddWithValue("@UserId", source.SupplierId);
                cmd.Parameters.AddWithValue("@SiteNameId", siteId);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (!reader.IsDBNull(1))
                        logo = reader.GetString(1);
                }
                conn.Close();
            }
            string logoUrl;
            if (!string.IsNullOrEmpty(logo))
            {
                logoUrl = professionalLogosWeb + source.SupplierId + "/" + logo + "?rnd=" + DateTime.Now.Millisecond;
            }
            else
            {
                logoUrl = string.Empty;
            }
            return logoUrl;
        }
    }
}