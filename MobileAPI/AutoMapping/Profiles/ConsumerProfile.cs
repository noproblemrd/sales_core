﻿using AutoMapper;
using MobileAPI.CustomerService;
using MobileAPI.Models;
using MobileAPI.SupplierService;

namespace MobileAPI.AutoMapping.Profiles
{
    public class ConsumerProfile : Profile
    {
        protected override void Configure()
        {

            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();

            CreateMap<Category, CategoryModel>()
                  .ForMember(d => d.CategoryId, o => o.MapFrom(s => s.Id))
                  .ForMember(d => d.CategoryName, o => o.MapFrom(s => s.Name));
                  

            CreateMap<AdvertiserAboutData, AdvertiserAboutDataModel>(); 
              


            CreateMap<ConsumerIncidentData, LeadModel>()
                .ForMember(d => d.Id, o => o.MapFrom(s => s.IncidentId))
                .ForMember(d => d.SuppliersCount, o => o.MapFrom(s => s.OrderedProviders))
                .ForMember(d => d.ZipCode, o => o.MapFrom(s => s.RegionName));

            CreateMap<CustomerProfileData, CustomerProfileModel>();

            CreateMap<CustomerSettingsData, CustomerSettingsModel>();


            CreateMap<ConsumerIncidentAccountData, LeadAdvertiserModel>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.SupplierName))
                .ForMember(d => d.Id, o => o.MapFrom(s => s.SupplierId))
            //    .ForMember(d => d.ImageUrl, o => o.ResolveUsing<AdvertiserImageUrlResolver>())
            ;

            CreateMap<CallRecordData, CallRecordModel>();

            CreateMap<GetLeadForMobileResponse, SupplierLeadModel>()
                .ForMember(d => d.LeadAccountId, o => o.MapFrom(s => s.IncidentAccountId));
            CreateMap<SupplierCategory, SupplierCategoryModel>();

            CreateMap<RecordingInLead, RecordingModel>()
                .ForMember(d => d.CallRecordUrl, o => o.MapFrom(s => s.Url))
                .ForMember(d => d.CreateDate, o => o.MapFrom(s => s.CreatedOn));

        }

        public override string ProfileName
        {
            get { return this.GetType().Name; }
        }
    }
}