﻿using System;
using MobileAPI.CustomerService;
using MobileAPI.Models;

namespace MobileAPI.CoreConnectors
{
    public class CustomerConnector
    {
        private readonly CustomersService customersService;

        public CustomerConnector()
        {
            customersService = ClientsFactory.GetClient<CustomersService>();
        }
        public CustomerProfileData GetCustomerProfile(Guid customerId)
        {
            var coreResult = customersService.GetCustomerProfile(customerId);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        public void UpdateProfile(UpdateCustomerProfileRequest request)
        {
            var coreResult = customersService.UpdateCustomerProfile(request);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            
        }

        public void UpdateCustomerSettings(UpdateCustomerSettingsRequest request)
        {
            var coreResult = customersService.UpdateCustomerSettings(request);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }

        public CustomerSettingsData GetCustomerSettings(Guid customerId)
        {
            var coreResult = customersService.GetCustomerSettings(customerId);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }

            return coreResult.Value;
        }

        public void LogOut(Guid customerId, Guid deviceId)
        {
            var coreResult = customersService.LogOut(customerId, deviceId);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }
    }
}