﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAPI.CoreConnectors
{
    public class LoveConnector
    {
        public void AddLove(Guid customerId, Guid SupplierId)
        {
            CustomerService.CustomersService client = ClientsFactory.GetClient<CustomerService.CustomersService>();
            var coreResult = client.AddLove(new CustomerService.LoveRequest()
            {
                SupplierId = SupplierId,
                ContactId = customerId
            });
            if (coreResult.Type == CustomerService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
     //       return coreResult.Value;
        }
        /*
        public CustomerService.LoveResponse GetLoves(Guid customerId, Guid SupplierId)
        {
            CustomerService.Customer client = ClientsFactory.GetClient<CustomerService.Customer>();
            var coreResult = client.GetLoves(new CustomerService.LoveRequest()
            {
                SupplierId = SupplierId,
                ContactId = customerId
            });
            if (coreResult.Type == CustomerService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }
         * */
    }
}