﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;

namespace MobileAPI.CoreConnectors
{
    public class LogOnConnector
    {
        private const string MOBILE_APP = "MOBILE_APP";
        
        internal Models.DeviceDataResponse UpdateDeviceData(Guid customerId, Models.DeviceDataRequest request)
        {
            CustomerService.CustomersService client = ClientsFactory.GetClient<CustomerService.CustomersService>();
            CustomerService.MobileLogOnData coreRequest = new CustomerService.MobileLogOnData();
            coreRequest.DeviceName = request.DeviceName;
            coreRequest.DeviceOS = request.DeviceOs;
            coreRequest.DeviceOSVersion = request.OsVersion;
            coreRequest.DeviceUId = request.Uid;
            coreRequest.NpAppVersion = request.NpAppVersion;
            coreRequest.isDebug = request.isDebug;
            var coreResult = client.UpdateMobileDevice(customerId, coreRequest);
            if (coreResult.Type == CustomerService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            Models.DeviceDataResponse response = new Models.DeviceDataResponse();
            response.Token = coreResult.Value;
            return response;
        }
        //not in used
        /*
        internal MobileAPI.SupplierService.LogOnResponse NormalLogOn(Models.NormalLogOnRequest request, bool isRegister)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            SupplierService.NormalLogOnRequest coreRequest = new SupplierService.NormalLogOnRequest();
            coreRequest.Email = request.Email;
            coreRequest.ComeFrom = MOBILE_APP;
            coreRequest.IsFromMobileApp = true;
            coreRequest.MobileLogOnData = new SupplierService.MobileLogOnData();
            coreRequest.MobileLogOnData.DeviceName = request.DeviceName;
            coreRequest.MobileLogOnData.DeviceOS = request.DeviceOs;
            coreRequest.MobileLogOnData.DeviceOSVersion = request.OsVersion;
            coreRequest.MobileLogOnData.DeviceUId = request.Uid;
            coreRequest.MobileLogOnData.NpAppVersion = request.NpAppVersion;
            coreRequest.Password = request.Password;
            SupplierService.ResultOfLogOnResponse coreResult;
            if (isRegister)
                coreResult = client.CreateUserNormal_Registration2014(coreRequest);
            else
                coreResult = client.LogInNormal_Registration2014(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }
         * */
        //not in used
        /*
        internal MobileAPI.SupplierService.LogOnResponse SocialLogOn(Models.SocialLogOnRequest request)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            SupplierService.SocialMediaLogOnRequest coreRequest = new SupplierService.SocialMediaLogOnRequest();
            coreRequest.Email = request.Email;
            coreRequest.ComeFrom = MOBILE_APP;
            coreRequest.IsFromMobileApp = true;
            coreRequest.MobileLogOnData = new SupplierService.MobileLogOnData();
            coreRequest.MobileLogOnData.DeviceName = request.DeviceName;
            coreRequest.MobileLogOnData.DeviceOS = request.DeviceOs;
            coreRequest.MobileLogOnData.DeviceOSVersion = request.OsVersion;
            coreRequest.MobileLogOnData.DeviceUId = request.Uid;
            coreRequest.MobileLogOnData.NpAppVersion = request.NpAppVersion;
            if (request.SocialNetwork == Models.SocialNetworkValidationAttribute.Facebook)
            {
                coreRequest.FacebookId = request.SocialNetworkToken;
            }
            else
            {
                coreRequest.GoogleId = request.SocialNetworkToken;
            }
            coreRequest.ContactName = request.UserFullName;
            SupplierService.ResultOfLogOnResponse coreResult = client.SocialMediaLogOn_Registration2014(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }
         * */
        //not in used (not delete)
        internal void LogOut(Guid supplierId)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.LogOutFromMobile(supplierId);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }

        internal bool ResetPassword(string email)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.SendResetEmail(email);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        internal Models.LogOnResponse SwitchToAdvertiserMode(Guid customerId, string token)
        {
            
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.SwitchToAdvertiserMode(customerId);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }           
            Models.LogOnResponse response = new Models.LogOnResponse(coreResult.Value.StatusCode);
            response.Step = Models.Steps.StepNumberToString(coreResult.Value.Step);
            response.SupplierId = coreResult.Value.SupplierId;
            response.Token = coreResult.Value.ApiToken;
            MobileAPI.Extensibility.WebApi.Filters.TokenAuthorizationFilter.RemoveFromCache(customerId, token, false);
            MobileAPI.Extensibility.WebApi.Filters.TokenAuthorizationFilter.RemoveFromCache(coreResult.Value.SupplierId, token, true);
            return response;
        }

        internal Models.CustomerLogOnResponse CustomerLogOn(Models.CustomerLogOnRequest request)
        {
            var client = ClientsFactory.GetClient<CustomerService.CustomersService>();
            var coreResult = client.CustomerLogOn(new CustomerService.CustomerLogOnRequest()
                        {
                            MobileLogOnData = new CustomerService.MobileLogOnData()
                            {
                                DeviceName = request.DeviceName,
                                DeviceOS = request.DeviceOs,
                                DeviceOSVersion = request.OsVersion,
                                DeviceUId = request.Uid,
                                NpAppVersion = request.NpAppVersion
                            },
                            PhoneNumber = request.PhoneNumber
                        });
            if (coreResult.Type == CustomerService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return new Models.CustomerLogOnResponse()
            {
                CustomerId = coreResult.Value.CustomerId,
                Token = coreResult.Value.ApiToken
            };
        }
        internal Models.CustomerLogOnResponse CustomerLogOn(Models.DeviceDataRequest request)
        {
            var client = ClientsFactory.GetClient<CustomerService.CustomersService>();
            var coreResult = client.CustomerLogOn(new CustomerService.CustomerLogOnRequest()
            {
                MobileLogOnData = new CustomerService.MobileLogOnData()
                {
                    DeviceName = request.DeviceName,
                    DeviceOS = request.DeviceOs,
                    DeviceOSVersion = request.OsVersion,
                    DeviceUId = request.Uid,
                    NpAppVersion = request.NpAppVersion
                },
                PhoneNumber = null
            });
            if (coreResult.Type == CustomerService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return new Models.CustomerLogOnResponse()
            {
                CustomerId = coreResult.Value.CustomerId,
                Token = coreResult.Value.ApiToken
            };
        }
        internal Models.CustomerLogOnResponse CustomerChangePhoneNumber(Guid customerId, MobileAPI.Models.CustomerLogOnRequest request)
        {
            var client = ClientsFactory.GetClient<CustomerService.CustomersService>();
            var coreResult = client.CustomerChangePhoneNumber(new CustomerService.CustomerChangePhoneNumberRequest()
            {
                MobileLogOnData = new CustomerService.MobileLogOnData()
                {
                    DeviceName = request.DeviceName,
                    DeviceOS = request.DeviceOs,
                    DeviceOSVersion = request.OsVersion,
                    DeviceUId = request.Uid,
                    NpAppVersion = request.NpAppVersion
                },
                PhoneNumber = request.PhoneNumber,
                customerId = customerId
            });
            if (coreResult.Type == CustomerService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return new Models.CustomerLogOnResponse()
            {
                CustomerId = coreResult.Value.CustomerId,
                Token = coreResult.Value.ApiToken
            };

        }
    }
}