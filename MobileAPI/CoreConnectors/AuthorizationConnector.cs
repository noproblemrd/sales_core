﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MobileAPI.CoreConnectors
{
    internal class AuthorizationConnector
    {
        internal bool CheckAuthorization(string token, Guid id, bool isSupplier)
        {
            AuthTokensService.AuthTokens client = ClientsFactory.GetClient<AuthTokensService.AuthTokens>();
            var result = client.VerifyToken(token, id, isSupplier);
            if (result.Type == AuthTokensService.eResultType.Failure)
            {
                throw new HttpResponseException(System.Net.HttpStatusCode.InternalServerError);
            }
            return result.Value;
        }
    }
}