﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAPI.CoreConnectors
{
    public class GeneralConnector
    {
        internal bool ConnectAndRecord(Guid supplierId, Guid incidentAccountId)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.ConnectAndRecord(incidentAccountId, supplierId);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }
        internal bool ConnectAndRecordConsumer(Guid CustomerId, Guid incidentAccountId)
        {
            CustomerService.CustomersService client = ClientsFactory.GetClient<CustomerService.CustomersService>();
            var coreResult = client.ConnectAndRecord(incidentAccountId, CustomerId);
            if (coreResult.Type == CustomerService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        internal List<Models.Alert> GetAlerts(Guid supplierId)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.GetUserAlerts(supplierId);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            List<Models.Alert> lst = new List<Models.Alert>();
            foreach (var item in coreResult.Value.Alerts)
            {
                lst.Add(
                    new Models.Alert()
                    {
                        Text = item.Text,
                        Type = item.Type
                    });
            }
            return lst;
        }

        internal void AppUninstalled(Models.AppUninstalledData data)
        {
            //TODO: implement logic when app is uninstalled.
        }

        internal bool SendPhoneValidation(Models.PhoneValidationRequest request)
        {
            MobileApiHelperService.MobileApiHelperService client = ClientsFactory.GetClient<MobileApiHelperService.MobileApiHelperService>();
            var coreResult = request.Call.Value ? client.PhoneVerificationCall(request.Phone, request.Code) : client.SendApplicationPhoneValidationSms(request.Phone, request.Code);
            if (coreResult.Type == MobileApiHelperService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        internal void SetSettings(Guid supplierId, Models.Settings settings)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreSettings = new SupplierService.Settings();

            coreSettings.RecordMyCalls = settings.RecordMyCalls.Value;
            if (settings.StopNotifications.HasValue && settings.StopNotifications.Value)
            {
                DateTime stopUntil;
                if (!DateTime.TryParseExact(settings.StopNotificationsUntilUtc, Models.Settings.DateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out stopUntil))
                {
                    throw new ArgumentException("stop_notifications_until_utc must use format " + Models.Settings.DateFormat);
                }
                coreSettings.StopNotificationsUntil = stopUntil;
            }
            var coreResult = client.SetMobileSettings(supplierId, coreSettings);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }

        internal Models.Settings GetSettings(Guid supplierId)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.GetMobileSettings(supplierId);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return new Models.Settings(coreResult.Value.RecordMyCalls, coreResult.Value.StopNotificationsUntil, coreResult.Value.CreditCardLastFourDigits);
        }

        internal bool Refund(Guid supplierId, Guid incidentAccountId, int code, string comment)
        {
            SiteService.Site client = ClientsFactory.GetClient<SiteService.Site>();
            var coreResult = client.RefundFromMobile(
                new SiteService.RefundFromMobileRequest()
                {
                    Comment = comment,
                    IncidentAccountId = incidentAccountId,
                    ReasonCode = code,
                    SupplierId = supplierId
                });
            if (coreResult.Type == SiteService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        internal List<Models.RefundReason> GetRefundReasons()
        {
            SiteService.Site client = ClientsFactory.GetClient<SiteService.Site>();
            var coreResult = client.GetAllRefundReasons();
            if (coreResult.Type == SiteService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            List<Models.RefundReason> retVal = new List<Models.RefundReason>();
            foreach (var item in coreResult.Value.RefundReasons)
            {
                if (!item.Inactive)
                {
                    retVal.Add(new Models.RefundReason()
                    {
                        Code = item.Code,
                        Text = item.Name
                    });
                }
            }
            return retVal;
        }

        internal void SendFeedback(Guid supplierId, string header, string comment)
        {
            MobileApiHelperService.MobileApiHelperService client = ClientsFactory.GetClient<MobileApiHelperService.MobileApiHelperService>();
            var coreResult = client.SendFeedback(
                new MobileApiHelperService.Feedback()
                {
                    Comment = comment,
                    Header = header,
                    SupplierId = supplierId
                });
            if (coreResult.Type == MobileApiHelperService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }
    }
}