namespace MobileAPI.CoreConnectors
{
    public class CurrentRequestData
    {
        public string Ip { get; set; }
        public string Host { get; set; }
        public string UserAgent { get; set; }
    }
}