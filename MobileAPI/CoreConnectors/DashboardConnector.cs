﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MobileAPI.Models;
using MobileAPI.SupplierService;
using LeadsListResponse = MobileAPI.Models.LeadsListResponse;
using Recording = MobileAPI.Models.Recording;
using SupplierLeadStatus = MobileAPI.Models.SupplierLeadStatus;

namespace MobileAPI.CoreConnectors
{
    public class DashboardConnector
    {
        public virtual LeadsListResponse GetLeadsList(Guid supplierId, SupplierLeadStatus supplierLeadStatus, int pageSize, int pageNumber)
        {
            var client = ClientsFactory.GetClient<Supplier>();
            var coreRequest = new LeadsListRequest {SupplierId = supplierId};
            switch(supplierLeadStatus)
            {
                case(SupplierLeadStatus.Lost):
                    coreRequest.supplierLeadStatus = SupplierService.SupplierLeadStatus.LOST;
                    break;
                case(SupplierLeadStatus.New):
                    coreRequest.supplierLeadStatus = SupplierService.SupplierLeadStatus.NEW;
                    break;
                case(SupplierLeadStatus.Won):
                    coreRequest.supplierLeadStatus = SupplierService.SupplierLeadStatus.WIN;
                    break;
                case(SupplierLeadStatus.Any):
                    coreRequest.supplierLeadStatus = SupplierService.SupplierLeadStatus.ANY;
                    break;
            }
            coreRequest.PageNumber = pageNumber;
            coreRequest.PageSize = pageSize;
            var coreResult = client.GetLeadsListForMobile(coreRequest);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            var response = new LeadsListResponse();
            foreach (var item in coreResult.Value.Leads)
            {
                var lead = new LeadsListResponse.LeadEntry
                {
                    CategoryName = item.CategoryName,
                    Status = item.Status,
                    CreatedOn = item.CreatedOn,
                    LeadAccountId = item.IncidentAccountId,
                    ZipCode =item.Region,
                    VideoUrl = item.VideoUrl,
                    PreviewVideoImageUrl = item.PreviewVideoImageUrl,
                    VideoDuration = item.Videoduration,
                    CustomerProfileImage = item.CustomerProfileImage,
                    WonTitle = item.WonTitle
                };
                response.Leads.Add(lead);
            }
            return response;
        }
        /*
        public virtual LeadsListResponse GetLeadsListV2(Guid supplierId, int pageSize, int pageNumber)
        {
            var client = ClientsFactory.GetClient<Supplier>();
            var coreRequest = new LeadsListRequestV2 { SupplierId = supplierId };
           
            coreRequest.PageNumber = pageNumber;
            coreRequest.PageSize = pageSize;
            var coreResult = client.GetLeadsListForMobileV2(coreRequest);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            var response = new LeadsListResponse();
            foreach (var item in coreResult.Value.Leads)
            {
                var lead = new LeadsListResponse.LeadEntry
                {
                    CategoryName = item.CategoryName,
                    Status = item.Status,
                    CreatedOn = item.CreatedOn,
                    LeadAccountId = item.IncidentAccountId,
                    ZipCode = item.Region,
                    VideoUrl = item.VideoUrl,
                    PreviewVideoImageUrl = item.PreviewVideoImageUrl,
                    VideoDuration = item.Videoduration,
                    CustomerProfileImage = item.CustomerProfileImage

                };
                response.Leads.Add(lead);
            }
            return response;
        }
         * */

        public virtual List<Recording> GetRecording(Guid supplierId, Guid incidentAccountId)
        {
            var client = ClientsFactory.GetClient<Supplier>();
            var coreResult = client.GetRecordingsForLead(supplierId, incidentAccountId);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value.Select(item => new Recording() { Url = item.Url }).ToList();
        }

        public virtual SupplierLeadModel GetSupplierLead(Guid supplierId, Guid leadAccountId)
        {
            var client = ClientsFactory.GetClient<Supplier>();
            var coreRequest = new GetLeadForMobileRequest
            {
                SupplierId = supplierId,
                IncidentAccountId = leadAccountId
            };
            var coreResult = client.GetLeadForMobile(coreRequest);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }

            GetLeadForMobileResponse response = coreResult.Value;
            SupplierLeadModel supplierLeadModel = Mapper.Map<GetLeadForMobileResponse,SupplierLeadModel>(response);
            return supplierLeadModel;
        }
        public virtual void UpdateLeadAddressBySupplier(string newAddress, Guid incidentAccountId)
        {
            var client = ClientsFactory.GetClient<Supplier>();
            var coreResult = client.UpdateLeadAddressBySupplier(newAddress, incidentAccountId);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }

        public virtual void UpdateLeadCustomerNameBySupplier(string newName, Guid incidentAccountId)
        {
            var client = ClientsFactory.GetClient<Supplier>();
            var coreResult = client.UpdateLeadCustomerNameBySupplier(newName, incidentAccountId);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }
    }
}