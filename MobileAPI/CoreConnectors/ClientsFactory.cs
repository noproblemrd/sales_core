﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace MobileAPI.CoreConnectors
{
    public class ClientsFactory
    {
        private readonly static string coreServicesUrl = ConfigurationManager.AppSettings.Get("CoreServicesUrlBase");

        private const string CUSTOMER = "/Core/CustomersService.asmx";
        private const string SUPPLIER = "/Core/Supplier.asmx";
        private const string SITE = "/Core/Site.asmx";
        private const string ONECALLUTILITIES = "/Core/OneCallUtilities.asmx";
        private const string AUTH_TOKENS = "/Core/APIs/MobileApi/AuthTokens.asmx";
        private const string HELPER = "/Core/APIs/MobileApi/MobileApiHelperService.asmx";

        public static T GetClient<T>() where T : System.Web.Services.Protocols.SoapHttpClientProtocol, new()
        {
            T client = new T();
            Type typeOfT = typeof(T);
            if (typeOfT == typeof(CustomerService.CustomersService))
            {
                client.Url = coreServicesUrl + CUSTOMER;
            }
            else if (typeOfT == typeof(AuthTokensService.AuthTokens))
            {
                client.Url = coreServicesUrl + AUTH_TOKENS;
            }
            else if (typeOfT == typeof(SupplierService.Supplier))
            {
                client.Url = coreServicesUrl + SUPPLIER;
            }
            else if (typeOfT == typeof(SiteService.Site))
            {
                client.Url = coreServicesUrl + SITE;
            }
            else if (typeOfT == typeof(MobileApiHelperService.MobileApiHelperService))
            {
                client.Url = coreServicesUrl + HELPER;
            }
            else if (typeOfT == typeof(OneCallUtilitiesService.OneCallUtilities))
            {
                client.Url = coreServicesUrl + ONECALLUTILITIES;
            }
            return client;
        }
    }
}