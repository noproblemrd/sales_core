﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileAPI.Models;

namespace MobileAPI.CoreConnectors
{
    public class DataConnector
    {
        public List<CategoryModel> GetAllCategories()
        {
            SiteService.Site client = ClientsFactory.GetClient<SiteService.Site>();
            var coreResult = client.GetAllExpertiseSliderData();
            if (coreResult.Type == SiteService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            List<CategoryModel> retVal = new List<CategoryModel>();
            foreach (var item in coreResult.Value)
            {
                CategoryModel category = new CategoryModel()
                {
                    CategoryName = item.Name,
                    CategoryId = item.Id
                };
                retVal.Add(category);
            }
            return retVal;
        }

        internal List<Models.Keyword> GetAllKeywordsWithCategoy()
        {
            return CacheManager.Instance.Get<List<Models.Keyword>>("DataConnector", "GetAllKeywordsWithCategoy", GetAllKeywordsWithCategoyPrivate, CacheManager.ONE_HOUR);
        }
        internal void ClearAllKeywordsWithCategoy()
        {
            CacheManager.Instance.ClearCache("DataConnector", "GetAllKeywordsWithCategoy");
        }
        private List<Models.Keyword> GetAllKeywordsWithCategoyPrivate()
        {
            SiteService.Site client = ClientsFactory.GetClient<SiteService.Site>();
            var coreResult = client.GetAllKeywordsWithCategory();
            if (coreResult.Type == SiteService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            List<Models.Keyword> lst = new List<Models.Keyword>();
            foreach (var item in coreResult.Value)
            {
                Models.Keyword keyword = new Models.Keyword();
                keyword.CategoryId = item.CategoryId;
                keyword.CategoryName = item.CategoryName;
                keyword.Value = item.Keyword;
                lst.Add(keyword);
            }
            return lst;
        }
        internal MobileAPI.Models.Tips GetTips(Guid ExpertiseId)
        {
            MobileAPI.Models.ExpertiseTips tips = GetAllTipsWithCategoy();
            return tips.GetTipsObj(ExpertiseId);
        }
        private MobileAPI.Models.ExpertiseTips GetAllTipsWithCategoy()
        {
            return CacheManager.Instance.Get<MobileAPI.Models.ExpertiseTips>("DataConnector", "GetAllTipsWithCategoy", GetAllTipsWithCategoyPrivate, CacheManager.ONE_DAY);
        }

        private MobileAPI.Models.ExpertiseTips GetAllTipsWithCategoyPrivate()
        {
            SiteService.Site client = ClientsFactory.GetClient<SiteService.Site>();
            SiteService.ResultOfListOfTipData coreResult = client.GetAllTips();
       
            if (coreResult.Type == SiteService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            MobileAPI.Models.ExpertiseTips lst = new MobileAPI.Models.ExpertiseTips();
            foreach (SiteService.TipData item in coreResult.Value)
            {
                MobileAPI.Models.Tips tips = lst.GetTipsObj(item.HeadingId);
                if (tips == null)
                {
                    tips = new Models.Tips();
                    tips.ExpertiseId = item.HeadingId;
                    tips.tips = new SortedDictionary<int,Models.Tip>();
                    lst.Add(tips);
                }
                Models.Tip tip = new Models.Tip();
                tip.Value = item.Subtitle;
                tip.Title = item.Title;
                if (tips.tips.ContainsKey(item.Order))
                    continue;
                tips.tips.Add(item.Order, tip);
                
            }
            return lst;
        }
    }
}