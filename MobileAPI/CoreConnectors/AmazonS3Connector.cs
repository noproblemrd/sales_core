﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using Castle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MobileAPI.CoreConnectors
{
    public class AmazonS3Connector
    {
        static readonly string accessKeyID;
        static readonly string secretAccessKeyID;
        static readonly string bucketName;

        const string AMAZON_DOMAIN = "https://s3.amazonaws.com/";
        static AmazonS3Connector()
        {
            accessKeyID = ConfigurationManager.AppSettings["amazonAccessKeyID"];
            secretAccessKeyID = ConfigurationManager.AppSettings["amazonSecretAccessKeyID"];
            bucketName = ConfigurationManager.AppSettings["amazonBucketName"];
        }
        public ILogger Logger { get; set; }
        private Guid supplierId;
        IAmazonS3 client;
        Random rnd;

        public string BaseDirectory { get; set; }

        public AmazonS3Connector()
        {
            
            rnd = new Random();
        }
        /*
        public AmazonS3Connector()
        {
            this.supplierId = new Guid();
            rnd = new Random();
        }
         * */
        public string UploadFile(HttpPostedFile postedFile, Guid supplierId)
        {
            this.supplierId = supplierId;
            string fullFileName;
            try
            {
                /*
                do
                {
                    string originaFilelName = postedFile.FileName;
                    string new_name = GetNewName(originaFilelName);
                    fullFileName = GetFilePath(new_name);
                } while (IsExists(fullFileName));
                 * */
                fullFileName = GetFilePath(GetNewName(postedFile.FileName));
                SessionAWSCredentials tempCredentials =
                        GetTemporaryCredentials(accessKeyID, secretAccessKeyID);
                using (client = new AmazonS3Client(tempCredentials, Amazon.RegionEndpoint.USEast1))
                {
                    var transferUtility = new TransferUtility(client);
                    TransferUtilityUploadRequest uploadRequest = new TransferUtilityUploadRequest()
                    {
                        InputStream = postedFile.InputStream,
                        Key = fullFileName,
                        BucketName = bucketName,
                        CannedACL = S3CannedACL.PublicRead
                         
                    };
                    transferUtility.Upload(uploadRequest);
                }
            }
            catch(Exception exc)
            {
                Logger.Error("error UploadFile Amazon", exc);
                return null;
            }
            return GetFullFilePath(fullFileName);
        }
        private string GetNewName(string fileName)
        {
             string _name = System.IO.Path.GetFileNameWithoutExtension(fileName);
            string _extension = System.IO.Path.GetExtension(fileName);
            return CleanSpecialCharsUrlFreindly3(_name) + "_" + rnd.Next(1000) +  _extension;
     //       return CleanSpecialCharsUrlFreindly3(_name) + "_logo" + _extension;
        }
        private SessionAWSCredentials GetTemporaryCredentials(
                         string accessKeyId, string secretAccessKeyId)
        {
            AmazonSecurityTokenServiceClient stsClient =
                new AmazonSecurityTokenServiceClient(accessKeyId,
                                                     secretAccessKeyId);

            GetSessionTokenRequest getSessionTokenRequest =
                                             new GetSessionTokenRequest();
            getSessionTokenRequest.DurationSeconds = 7200; // seconds

            GetSessionTokenResponse sessionTokenResponse =
                          stsClient.GetSessionToken(getSessionTokenRequest);
            Credentials credentials = sessionTokenResponse.Credentials;

            SessionAWSCredentials sessionCredentials =
                new SessionAWSCredentials(credentials.AccessKeyId,
                                          credentials.SecretAccessKey,
                                          credentials.SessionToken);

            return sessionCredentials;
        }
        private string GetFilePath(string fileName)
        {
            return this.BaseDirectory + "/" + supplierId.ToString() + "/" + fileName;
        }
        private string GetFullFilePath(string fileName)
        {
            return AMAZON_DOMAIN + bucketName + "/" + fileName;
        }
        private string CleanSpecialCharsUrlFreindly3(string param)
        {
            string tempParam = "";

            if (!string.IsNullOrEmpty(param))
            {
                tempParam = param.Replace("&", "");
                tempParam = tempParam.Replace("/", "");
                tempParam = tempParam.Replace("\"", "");
                tempParam = tempParam.Replace("\'", "");
                tempParam = tempParam.Replace(".", "");
                tempParam = tempParam.Replace("+", "");
            }

            return tempParam;
        }
        private static readonly List<string> supportedFormats =
            new List<string>(){
                ".jpg", ".png", ".gif", ".jpeg"
            };
        
        
        private bool IsExists(string fileKey)
        {
            try
            {
                GetObjectMetadataRequest request = new GetObjectMetadataRequest();
                request.BucketName = bucketName;
                request.Key = fileKey;
                GetObjectMetadataResponse response = client.GetObjectMetadata(request);

                return true;
            }

            catch (Amazon.S3.AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;

                //status wasn't not found, so throw the exception
                throw;
            }            
        }
        private void DeleteObject(string fileKey)
        {
            DeleteObjectRequest request = new DeleteObjectRequest
            {
                BucketName = bucketName,
                Key = fileKey
               
            };
            DeleteObjectResponse response = client.DeleteObject(request);            
            		
        }

    }
}