﻿using System;
using MobileAPI.CustomerService;
using DidYouMeanResponse = MobileAPI.Models.DidYouMeanResponse;

namespace MobileAPI.CoreConnectors
{
    public class CustomerLeadsConnector
    {
        private readonly CustomersService service;

        public CustomerLeadsConnector()
        {
            service = ClientsFactory.GetClient<CustomersService>();
        }
        public virtual Guid CreateNewLead(NewVideoRequest newLead)
        {
            var coreResult = service.CraeteNewVideoLead(newLead);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            if(coreResult.Value.UpdateCache)
            {
                DataConnector dc = new DataConnector();
                dc.ClearAllKeywordsWithCategoy();
            }
            return coreResult.Value.LeadId;
        }

        public virtual GetConsumerIncidentsResponse GetCustomerLeads(GetCustomerLeadsRequest request)
        {
            ResultOfGetConsumerIncidentsResponse coreResult = service.GetCustomerIncidents(request);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        public virtual DidYouMeanResponse DidYouMean(string searchTerm)
        {
            var coreResult = service.DidYouMean(searchTerm);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            DidYouMeanResponse response = new DidYouMeanResponse();
            foreach (var item in coreResult.Value.Categories)
            {
                response.AddCategory(item.Name, item.Id);
            }
            return response;
        }

        public virtual RelaunchLeadResponse RelaunchIncident(RelaunchLeadRequest request)
        {
            ResultOfRelaunchLeadResponse coreResult = service.RelaunchIncident(request);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        public virtual GetConsumerIncidentDataResponse GetIncidentSuppliers(GetConsumerIncidentDataRequest request)
        {
            ResultOfGetConsumerIncidentDataResponse coreResult = service.GetConsumerIncidentData(request);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        public virtual CloseLeadResponse CloseLeadRequest(CloseLeadRequest request)
        {
            ResultOfCloseLeadResponse coreResult = service.CloseLeadRequest(request);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        public virtual GetCustomerAdvertiserAboutResponse GetCustomerAdvertiserAbout(GetCustomerAdvertiserAboutRequest request)
        {
            ResultOfGetCustomerAdvertiserAboutResponse coreResult = service.GetCustomerAdvertiserAbout(request);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }


        public virtual GetAdvertiserCallHistoryResponse GetAdvertiserCallHistory(GetAdvertiserCallHistoryRequest request)
        {
            ResultOfGetAdvertiserCallHistoryResponse coreResult = service.GetAdvertiserCallHistory(request);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        public virtual GetCustomerLeadResponse GetCustomerLead(GetCustomerLeadRequest request)
        {
            ResultOfGetCustomerLeadResponse coreResult = service.GetCustomerLead(request);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }
        public virtual GetFavoriteServiceProviderResponse[] GetFavoriteServiceProvider(GetFavoriteServiceProviderRequest request)
        {
            ResultOfListOfGetFavoriteServiceProviderResponse coreResult = service.GetFavoriteServiceProvider(request);
            if (coreResult.Type == eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }
    }
}