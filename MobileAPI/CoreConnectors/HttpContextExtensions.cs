using System;
using System.Web;

namespace MobileAPI.CoreConnectors
{
    public static class HttpContextExtensions
    {
        public static CurrentRequestData CurrentData(this HttpContext context)
        {
            string hostAddress = context.Request.UserHostAddress;
            string hostName = hostAddress;
            string userAgent = System.Web.HttpContext.Current.Request.UserAgent;
            try
            {
                var hostEntry = System.Net.Dns.GetHostEntry(System.Net.IPAddress.Parse(hostAddress));
                if (hostEntry != null)
                {
                    hostName = hostEntry.HostName;
                }
            }
            catch (Exception exc)
            {
                
                try
                {
                    var hostEntry = System.Net.Dns.GetHostEntry(System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"]);
                    hostName = hostEntry.HostName;
                }
                catch (Exception exc2)
                {

                }
            }

            return new CurrentRequestData
            {
                Host = hostName,
                UserAgent = userAgent,
                Ip = hostAddress
            };
        }
    }
}