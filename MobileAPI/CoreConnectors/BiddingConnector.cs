﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAPI.CoreConnectors
{
    public class BiddingConnector
    {
        internal Models.SetBidResult SetBid(Guid supplierId, Guid incidentAccountId, Models.SetBidRequest request)
        {
            string hostAddress;
            string hostName;
            string hostAgent;
            RegistrationFlowConnector.GetUserHostData(out hostAddress, out hostName, out hostAgent);
            var client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreRequest = new SupplierService.SetBidForLeadRequest
            {
                SupplierId = supplierId,
                IncidentAccountId = incidentAccountId,
                Price = request.Price.Value,
                Status = request.Status,
                Host = hostName,
                IP = hostAddress,
                UserAgent = hostAgent
            };
            var coreResult = client.SetBidForLead(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return new Models.SetBidResult()
            {
                Status = coreResult.Value.Ok ? Models.SetBidResult.StatusOptions.Ok : Models.SetBidResult.StatusOptions.Failed,
                faildMessage = coreResult.Value.FaildMessage
            };
        }

        internal List<Models.LiveBid> GetLiveBids(Guid supplierId)
        {
            var client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.GetLiveBidRequest(supplierId);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value.Select(item => new Models.LiveBid(item.BidTimeOutAtUtc, item.LeadCreatedOnUtc)
            {
                Category = item.Category, Description = item.Description, Id = item.IncidentAccountId, MaxBid = item.MaxBid, MinBid = item.MinBid, DefaultBid = item.DefaultBid, Address = item.CustomerCityAndState, PhoneNumber = item.CustomerHiddenPhone, CustomerName = item.CustomerHiddenName
            }).ToList();
        }
    }
}