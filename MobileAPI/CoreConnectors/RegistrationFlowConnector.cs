﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml.Linq;
using AutoMapper;

namespace MobileAPI.CoreConnectors
{
    internal class RegistrationFlowConnector
    {
        private static readonly ILog logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        public bool CreatePaymentMethod(Models.PaymentMethod paymentMethod, Guid supplierId)
        {
            string hostAddress;
            string hostName;
            string hostAgent;
            GetUserHostData(out hostAddress, out hostName, out hostAgent);
            logger.Info(String.Format("HostAddress={0} HostName={1} HostAgent={2}", hostAddress, hostName, hostAgent));
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreRequest = new SupplierService.CreatePaymentMethodRequest()
            {
                BillingAddress = paymentMethod.BillingAddress,
                BillingCity = paymentMethod.BillingCity,
                BillingEmail = paymentMethod.BillingEmail,
                BillingFirstName = paymentMethod.BillingFirstName,
                BillingLastName = paymentMethod.BillingLastName,
                BillingState = paymentMethod.BillingState,
                BillingZip = paymentMethod.BillingZip,
                CreditCardType = paymentMethod.CreditCardType,
                EcryptedCvv = paymentMethod.EncryptedCvv,
                EncryptedCreditCardNumber = paymentMethod.EncryptedCreditCardNumber,
                ExpirationMonth = paymentMethod.CreditCardExpirationMonth,
                ExpirationYear = paymentMethod.CreditCardExpirationYear,
                Last4Digits = paymentMethod.CreditCardLastFourDigits,
                SupplierId = supplierId,
                UserAgent = hostAgent,
                UserHost = hostName,
                UserIP = hostAddress
            };
            var coreResult = client.CreatePaymentMethod(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }

        internal Models.PaymentMethod GetPaymentMethod(Guid supplierId)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.GetPaymentMethodData(supplierId);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            Models.PaymentMethod paymentMethod = new Models.PaymentMethod();

            if (coreResult.Value == null)
                return null;
            if(coreResult.Value.Data == null)
            {
                paymentMethod.IsActive = coreResult.Value.IsActive;
                return paymentMethod;
            }
            paymentMethod.IsActive = coreResult.Value.IsActive;
            paymentMethod.BillingAddress = coreResult.Value.Data.BillingAddress;
            int indexOfSpace = coreResult.Value.Data.CcOwnerName.IndexOf(" ");
            if (indexOfSpace > 0)
            {
                paymentMethod.BillingFirstName = coreResult.Value.Data.CcOwnerName.Substring(0, indexOfSpace);
                paymentMethod.BillingLastName = coreResult.Value.Data.CcOwnerName.Substring(indexOfSpace).Trim();
            }
            else
            {
                paymentMethod.BillingFirstName = coreResult.Value.Data.CcOwnerName;
            }
            paymentMethod.BillingPhone = coreResult.Value.Data.Phone;
            int month;
            if (!String.IsNullOrEmpty(coreResult.Value.Data.ExpDate) && coreResult.Value.Data.ExpDate.Length >= 4)
            {
                if (int.TryParse(coreResult.Value.Data.ExpDate.Substring(0, 2), out month))
                {
                    paymentMethod.CreditCardExpirationMonth = month;
                }
                int year;
                if (int.TryParse(coreResult.Value.Data.ExpDate.Substring(2, 2), out year))
                {
                    paymentMethod.CreditCardExpirationYear = year + 2000;
                }
            }
            paymentMethod.CreditCardLastFourDigits = coreResult.Value.Data.Last4Digits;
            paymentMethod.CreditCardType = coreResult.Value.Data.CreditCardType;
            paymentMethod.BillingZip = coreResult.Value.Data.BillingZip;
            paymentMethod.BillingState = coreResult.Value.Data.BillingState;
            paymentMethod.BillingCity = coreResult.Value.Data.BillingCity;
            paymentMethod.BillingEmail = coreResult.Value.Data.Email;

            return paymentMethod;
        }

        public static void GetUserHostData(out string hostAddress, out string hostName, out string hostAgent)
        {
            hostAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
            hostName = hostAddress;
            hostAgent = System.Web.HttpContext.Current.Request.UserAgent;
            try
            {
                var hostEntry = System.Net.Dns.GetHostEntry(System.Net.IPAddress.Parse(hostAddress));
                if (hostEntry != null)
                {
                    hostName = hostEntry.HostName;
                }
            }
            catch (Exception exc)
            {
                logger.Error("Exception looking for host data. First try. hostAddress= " + hostAddress + " hostAgent= " + hostAgent + " hostName= " + hostName, exc);
                try
                {
                    var hostEntry = System.Net.Dns.GetHostEntry(System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"]);
                    hostName = hostEntry.HostName;
                }
                catch (Exception exc2)
                {
                    logger.Error("Exception looking for host data. Second try. hostAddress= " + hostAddress + " hostAgent= " + hostAgent + " hostName= " + hostName, exc2);
                }
            }
        }

        public void UpdateBusinessDetails(Guid supplierId, Models.BusinessDetails details)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            SupplierService.EnterBusinessDetailsRequest coreRequest =
                new SupplierService.EnterBusinessDetailsRequest();
            coreRequest.SupplierId = supplierId;
            coreRequest.Address = details.BusinessAddress;
            coreRequest.ContactPersonName = details.ContactName;
            coreRequest.Latitude = details.Latitude;
            coreRequest.Longitude = details.Longitude;
            coreRequest.Name = details.CompanyName;
            coreRequest.Phone = details.MobileNumber;
            var coreResult = client.EnterBusinessDetails_Registration2014(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }
        public void UpdateBusinessDetails(Guid supplierId, Models.Supplier.v2.BusinessDetails details)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            SupplierService.EnterBusinessDetailsRequest coreRequest =
                new SupplierService.EnterBusinessDetailsRequest();
            coreRequest.SupplierId = supplierId;            
            coreRequest.Name = details.CompanyName;
            coreRequest.Phone = details.MobileNumber;
            coreRequest.Email = details.Email;
            coreRequest.Website = details.WebSite;
            coreRequest.ReferralCode = details.ReferralCode;
            var coreResult = client.EnterBusinessDetails_Registration2014(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }

        public Models.BusinessDetails GetBusinessDetails(Guid supplierId)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.GetBusinessDetails_Registration2014(supplierId);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            Models.BusinessDetails retVal = new Models.BusinessDetails();
            retVal.BusinessAddress = coreResult.Value.Address;
            retVal.Latitude = coreResult.Value.Latitude;
            retVal.Longitude = coreResult.Value.Longitude;
            retVal.MobileNumber = coreResult.Value.Phone;
            retVal.CompanyName = coreResult.Value.Name;
            retVal.ContactName = coreResult.Value.ContactPersonName;
            retVal.Email = coreResult.Value.Email;
            retVal.WebSite = coreResult.Value.Website;
            retVal.ReferralCode = coreResult.Value.ReferralCode;
            return retVal;
        }

        public void SetCategories(Guid supplierId, List<Models.CategoryModel> categories)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            XElement requestXml = new XElement("SupplierExpertise");
            requestXml.Add(new XAttribute("SiteId", ""));
            requestXml.Add(new XAttribute("SupplierId", supplierId));
            foreach (var item in categories)
            {
                requestXml.Add(
                            new XElement("PrimaryExpertise",
                                new XAttribute("ID", item.CategoryId),
                                new XAttribute("Certificate", false))
                                );
            }
            string response = client.CreateSupplierExpertise(
                new SupplierService.CreateSupplierExpertiseRequest()
                {
                    Request = requestXml.ToString(),
                    UserId = supplierId
                });
            //evaluate response:
            if (response == "<SupplierExpertise><Error>Failed</Error></SupplierExpertise>")
            {
                throw new Exception("Failed at core app.");
            }
        }

        internal List<Models.CategoryModel> GetCategories(Guid supplierId)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.GetSupplierPrices(
                new SupplierService.GetSupplierPricesRequest()
                {
                    SupplierId = supplierId
                });
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            var retVal = new List<Models.CategoryModel>();
            foreach (var item in coreResult.Value.Expertises)
            {
                Models.CategoryModel data = new Models.CategoryModel();
                data.CategoryId = item.ExpertiseId;
                data.CategoryName = item.ExpertiseName;
                retVal.Add(data);
            }
            return retVal;
        }

        public Models.CoverAreaData GetCoverAreas(Guid supplierId)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            SupplierService.GetCoverAreaRequest coreRequest = new SupplierService.GetCoverAreaRequest();
            coreRequest.SupplierId = supplierId;
            var coreResult = client.GetCoverAreas_Registration2014_obj(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            Models.CoverAreaData areaData = TranslateCoreAreaDataToModel(coreResult.Value);
            return areaData;
        }

        public Models.CoverAreaData SetBusinessAddress(Guid supplierId, MobileAPI.Models.BusinessAddress businessAddress)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            SupplierService.SetBussinessAddressRequest coreRequest = new SupplierService.SetBussinessAddressRequest();
            coreRequest.SupplierId = supplierId;
           
            coreRequest.BusinessAddress = businessAddress.businessAddress;
            coreRequest.Latitude = businessAddress.Latitude;
            coreRequest.Longitude = businessAddress.Longitude;
            var coreResult = client.SetBusinessAddress__Registration2014(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            Models.CoverAreaData areaData = TranslateCoreAreaDataToModel(coreResult.Value);
            return areaData;
        }

        public void SetCoverAreas(Guid supplierId, Models.SetCoverAreaData coverArea)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreRequest = new SupplierService.SetCoverAreasRequest();
            coreRequest.SupplierId = supplierId;
            List<SupplierService.AreaRequest> areaList = new List<SupplierService.AreaRequest>();
            foreach (var item in coverArea.Areas)
            {
                SupplierService.AreaRequest area = new SupplierService.AreaRequest();
                area.Address = item.BusinessAddress;
                area.Latitude = item.Latitude.Value;
                area.Longitude = item.Longitude.Value;
                area.Order = item.Order.Value;
                area.Radius = item.SelectedId;
                areaList.Add(area);
            }
            coreRequest.Areas = areaList.ToArray();
            var coreResult = client.SetCoverAreas__Registration2014(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }

        public Models.CoverAreaData GetCoverAreas(decimal latitude, decimal longitude)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.GetDefaultCoverAreas(
                new SupplierService.GetDefaultCoverAreasRequest()
                {
                    Latitude = latitude,
                    Longitude = longitude
                });
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            Models.CoverAreaData areaData = TranslateCoreAreaDataToModel(coreResult.Value);
            return areaData;
        }

        private Models.CoverAreaData TranslateCoreAreaDataToModel(SupplierService.GetCoverAreaResponse coreCoverArea)
        {
            Models.CoverAreaData areaData = new Models.CoverAreaData();
            areaData.Areas = new List<Models.CoverAreaData.CoverArea>();
            for (int i = 0; i < coreCoverArea.Area.Length; i++)
            {
                var coverArea = new Models.CoverAreaData.CoverArea();
                coverArea.Order = i;
                coverArea.Longitude = coreCoverArea.Area[i].Longitude;
                coverArea.Latitude = coreCoverArea.Area[i].Latitude;
                coverArea.BusinessAddress = coreCoverArea.Area[i].FullAddress;
                coverArea.AreasInList = new List<Models.CoverAreaData.CoverArea.AreaInList>();

                string selection = coreCoverArea.Area[i].Radius.ToLower();

                int j;
                for (j = 0; j < coreCoverArea.Area[i].SelectedAreas.Length; j++)
                {
                    string coreRegionId = coreCoverArea.Area[i].SelectedAreas[j].RegionId.ToLower();
                    string coreName = coreCoverArea.Area[i].SelectedAreas[j].Name;
                    var areaInList = new Models.CoverAreaData.CoverArea.AreaInList();
                    areaInList.Id = coreRegionId;
                    areaInList.Name = coreName;
                    areaInList.Selected = coreRegionId == selection;
                    areaInList.ListOrder = j;
                    areaInList.IsRange = false;
                    coverArea.AreasInList.Add(areaInList);
                }
                //add ranges
                foreach (var item in ranges)
                {
                    var areaInList = new Models.CoverAreaData.CoverArea.AreaInList();
                    areaInList.Id = item.Key.ToString();
                    areaInList.Name = item.Value;
                    areaInList.Selected = areaInList.Id.ToString().ToLower() == selection;
                    areaInList.ListOrder = j++;
                    areaInList.IsRange = true;
                    coverArea.AreasInList.Add(areaInList);
                }
                areaData.Areas.Add(coverArea);
            }
            return areaData;
        }

        private static readonly SortedDictionary<double, string> ranges =
            new SortedDictionary<double, string>()
            {
                {0.5, "0.5mi"},
                {1, "1mi"},
                {2, "2mi"},
                {3, "3mi"},
                {5, "5mi"},
                {10, "10mi"},
                {12.5, "12.5mi"},
                {20, "20mi"},
                {30, "30mi"},    
                {50, "50mi"},
                {75, "75mi"},
                {100, "100mi"},
                {125, "125mi"},
                {150, "150mi"},
                {175, "175mi"}
            };

        internal void UpdateBusinessDescription(Guid supplierId, Models.BusinessDescription businessDescription)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            SupplierService.UpdateBusinessDescriptionRequest coreRequest = new SupplierService.UpdateBusinessDescriptionRequest();
            coreRequest.description = businessDescription.Description;
            coreRequest.licenseId = businessDescription.LicenseNumber;
            coreRequest.supplierId = supplierId;
            var coreResult = client.UpdateBusinessDescription(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
        }

        internal Models.BusinessDescription GetBusinessDescription(Guid supplierId)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.GetBusinessDescription(supplierId);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return new Models.BusinessDescription()
            {
                Description = coreResult.Value.description,
                LicenseNumber=coreResult.Value.licenseId
            };
        }
        internal Models.LogoSupplier UpdateLogoSupplier(SupplierService.SupplierLogoUploadRequest request)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.UpdateSupplierLogo(request);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            Models.LogoSupplier ls = Mapper.Map<SupplierService.SupplierLogoResponse, Models.LogoSupplier>(coreResult.Value);
            return ls;
            /*
            return new Models.LogoSupplier()
            {
                ImageUrl = coreResult.Value.ImageUrl
            };
             * */
        }
        internal Models.LogoSupplier GetLogoSupplier(Guid supplierId)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            var coreResult = client.GetCustomertLogo(supplierId);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            Models.LogoSupplier ls = Mapper.Map<SupplierService.SupplierLogoResponse, Models.LogoSupplier>(coreResult.Value);
            return ls;
            /*
            return new Models.LogoSupplier()
            {
                ImageUrl = coreResult.Value.ImageUrl
            };
             * */
        }
        /*
        public bool SetLicenseId(Guid supplierId, MobileAPI.Models.Supplier.v2.LicenseNumber licenseNumber)
        {
            SupplierService.Supplier client = ClientsFactory.GetClient<SupplierService.Supplier>();
            SupplierService.SetLicenseRequest coreRequest =
                new SupplierService.SetLicenseRequest();
            coreRequest.licenseId = licenseNumber.LicenseId;
            coreRequest.supplierId = supplierId;
            var coreResult = client.SetLicenseId(coreRequest);
            if (coreResult.Type == SupplierService.eResultType.Failure)
            {
                throw new Exception(String.Join(" ", coreResult.Messages));
            }
            return coreResult.Value;
        }
         * */

    }
}