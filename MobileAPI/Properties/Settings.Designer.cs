﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MobileAPI.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost/NoProblem.Core.Interfaces/Core/CustomersService.asmx")]
        public string MobileAPI_CustomerService_Customer {
            get {
                return ((string)(this["MobileAPI_CustomerService_Customer"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost/NoProblem.Core.Interfaces/Core/APIs/MobileApi/AuthTokens.asmx")]
        public string MobileAPI_AuthTokensService_AuthTokens {
            get {
                return ((string)(this["MobileAPI_AuthTokensService_AuthTokens"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost/NoProblem.Core.Interfaces/Core/Supplier.asmx")]
        public string MobileAPI_SupplierService_Supplier {
            get {
                return ((string)(this["MobileAPI_SupplierService_Supplier"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost/NoProblem.Core.Interfaces/Core/Site.asmx")]
        public string MobileAPI_SiteService_Site {
            get {
                return ((string)(this["MobileAPI_SiteService_Site"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost/NoProblem.Core.Interfaces/Core/APIs/MobileApi/MobileApiHelperSer" +
            "vice.asmx")]
        public string MobileAPI_MobileApiHelperService_MobileApiHelperService {
            get {
                return ((string)(this["MobileAPI_MobileApiHelperService_MobileApiHelperService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost/NoProblem.Core.Interfaces/Core/OneCallUtilities.asmx")]
        public string MobileAPI_OneCallUtilitiesService_OneCallUtilities {
            get {
                return ((string)(this["MobileAPI_OneCallUtilitiesService_OneCallUtilities"]));
            }
        }
    }
}
