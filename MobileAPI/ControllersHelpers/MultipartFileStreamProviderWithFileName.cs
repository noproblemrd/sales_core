﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace MobileAPI.ControllersHelpers
{
    public class MultipartFileStreamProviderWithFileName : MultipartFileStreamProvider
    {
        private string fileName;
        private string extension;
        private bool saveOk;

        public MultipartFileStreamProviderWithFileName(string rootPath, string fileName)
            : base(rootPath)
        {
            saveOk = false;
            this.fileName = fileName;
        }

        public override System.IO.Stream GetStream(HttpContent parent, System.Net.Http.Headers.HttpContentHeaders headers)
        {
            // restrict what images can be selected
            var extensions = new[] { "wav", "mp3" };
            var filename = headers.ContentDisposition.FileName.Replace("\"", string.Empty);

            if (filename.IndexOf('.') < 0)
            {
                return Stream.Null;
            }

            var extension = filename.Split('.').Last();

            foreach (var ext in extensions)
            {
                if (ext.Equals(extension, StringComparison.InvariantCultureIgnoreCase))
                {
                    this.extension = ext;
                    saveOk = true;
                    return base.GetStream(parent, headers);
                }
            }
            return Stream.Null;
        }

        public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
        {
            return fileName + "." + extension;
        }

        public bool SaveOk()
        {
            return saveOk;
        }
    }
}