﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileAPI.Models;

namespace MobileAPI.ControllersHelpers
{
    public class CategoriesAutoCompleter
    {
        private Trie.Trie<CategoryModel> categoriesTrie;
        private static object locker = new object();
        private volatile static CategoriesAutoCompleter instance;
        private static DateTime lastUpdate = DateTime.MinValue;
        private const int CLEAR_CACHE_INTERVAL_HOURS = 12;
        private readonly List<CategoryModel> emptyList = new List<CategoryModel>();
        private CoreConnectors.DataConnector connector;

        internal static CategoriesAutoCompleter GetInstance()
        {
            if (ItsTimeToUpdate())
            {
                lock (locker)
                {
                    if (ItsTimeToUpdate())
                    {
                        instance = null;
                        instance = new CategoriesAutoCompleter();
                    }
                }
            }
            return instance;
        }

        private static bool ItsTimeToUpdate()
        {
            return instance == null || (DateTime.Now - lastUpdate).TotalHours > CLEAR_CACHE_INTERVAL_HOURS;
        }

        private CategoriesAutoCompleter()
        {
            //get categories from CRM
            connector = new CoreConnectors.DataConnector();
            var data = connector.GetAllCategories();
            categoriesTrie = new Trie.Trie<CategoryModel>();
            foreach (var item in data)
            {
                categoriesTrie.Put(item.CategoryName.ToLower(), item);
            }
            lastUpdate = DateTime.Now;
        }

        internal List<CategoryModel> GetCategoriesByPrefix(string prefix)
        {
            if (String.IsNullOrEmpty(prefix))
                return emptyList;
            var matcher = categoriesTrie.GetNewMatcher();
            var charArray = prefix.ToLower().ToCharArray();
            int i;
            for (i = 0; i < charArray.Length; i++)
            {
                bool found = matcher.NextMatch(charArray[i]);
                if (!found)
                    break;
            }
            if (i == 0)
                return emptyList;
            var matches = matcher.GetPrefixMatches();
            matches.Sort();
            return matches;
        }
    }
}