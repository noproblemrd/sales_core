﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using MobileAPI.Extensibility.SingalR;
using MobileAPI.Extensibility.WebApi.DependencyInjection;
using Owin;

[assembly: OwinStartup(typeof(MobileAPI.SignalRChatStartUp))]

namespace MobileAPI
{

    public class SignalRChatStartUp
    {
        public void Configuration(IAppBuilder app)
        {
            //IWindsorContainer container = new WindsorContainer();
            //container.Install(FromAssembly.This());
            //container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel, true));
            //var dependencyResolver = new SingalRDependencyResolver(container);

            //app.Map("/signalr", map =>
            //{
            //    // Setup the CORS middleware to run before SignalR.
            //    // By default this will allow all origins. You can 
            //    // configure the set of origins and/or http verbs by
            //    // providing a cors options with a different policy.
            //    map.UseCors(CorsOptions.AllowAll);
            //    var hubConfiguration = new HubConfiguration
            //    {
            //        // You can enable JSONP by uncommenting line below.
            //        // JSONP requests are insecure but some older browsers (and some
            //        // versions of IE) require JSONP to work cross domain
            //        // EnableJSONP = true
            //        Resolver = dependencyResolver
            //    };
            //    // Run the SignalR pipeline. We're not using MapSignalR
            //    // since this branch already runs under the "/signalr"
            //    // path.
            //    map.RunSignalR(hubConfiguration);
            //});   
        }
    }
}
