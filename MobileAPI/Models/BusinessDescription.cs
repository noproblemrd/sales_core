﻿using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class BusinessDescription
    {
        [Required]
        public string Description { get; set; }
        public string LicenseNumber { get; set; }
    }
}