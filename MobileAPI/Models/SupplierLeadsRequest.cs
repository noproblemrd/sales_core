﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class SupplierLeadsRequest
    {
        public SupplierLeadsRequest()
        {
            PageNumber = 0;
            PageSize = 40;
        }
         
        [PageNumberValidation]
        public int? PageNumber { get; set; }

        [PageSizeValidation]
        public int? PageSize { get; set; }
    }
}