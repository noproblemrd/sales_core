﻿using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class CustomerLeadStatus
    {
        [Required]
        public string Status { get; set; }
    }
}