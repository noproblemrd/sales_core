﻿using System;

namespace MobileAPI.Models
{
    public class CallRecordModel
    {
        public string CallRecordUrl { get; set; }
        public DateTime CreateDate { get; set; }
      //  public string Status { get; set; }
        public string Type { get; set; }
        public LeadAdvertiserModel Advertiser { get; set; }
        public int Duration { get; set; }
        //public IncidentCustomer Customer { get; set; }
    }
}