﻿using MobileAPI.CustomerService;

namespace MobileAPI.Models
{
    public class StandardResponse
    {
        private const string Ok = "OK";
        private const string Failed = "FAILED";

        public string Status { get; set; }

        public StandardResponse()
            : this(true)
        {
        }

        public StandardResponse(bool ok)
        {
            Status = ok ? Ok : Failed;
        }
    }

    public class ResponseMessage<T>

    {
        public eStatusCode StatusCode { get; set; }
        public eStatusReason StatusReason { get; set; }
        public T Payload { get; set; }
    }

    public enum eStatusReason
    {
        NotFound
    }

    public enum eStatusCode
    {
        Success,
        Failure
    }
}