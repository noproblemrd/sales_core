﻿using MobileAPI.CustomerService;

namespace MobileAPI.Models
{
    public class DashboardLeadsRequest
    {
        
        public DashboardLeadsRequest()
        {
            PageNumber = 0;
            PageSize = 40;
        }
         
        [PageNumberValidation]
        public int? PageNumber { get; set; }

        [PageSizeValidation]
        public int? PageSize { get; set; }

        public SupplierLeadStatus supplierLeadStatus { get; set; }
    }

    public enum SupplierLeadStatus
    {
        New,
        Won,
        Lost,
        Any
    }
}