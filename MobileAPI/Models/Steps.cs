﻿namespace MobileAPI.Models
{
    internal static class Steps
    {
        internal enum ESteps
        {
            VerifyYourEmail1 = 1,
            ChoosePlan2,
            BusinessDetailsGeneral3,
            BusinessDetailsCategory4,
            BusinessDetailsArea5,
            Checkout6,
            Dashboard7
        }

        internal static string StepNumberToString(int stepNumber)
        {
            if (stepNumber < 1 || stepNumber > 7)
                return null;
            return ((ESteps)stepNumber).ToString();
        }
    }
}