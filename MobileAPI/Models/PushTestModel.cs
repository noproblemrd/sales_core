﻿namespace MobileAPI.Models
{
    public class PushTestModel
    {
        public string DeviceId { get; set; }
        public string DataKey1 { get; set; }
        public string DataValue1 { get; set; }
        public string DataKey2 { get; set; }
        public string DataValue2 { get; set; }
        public string DataKey3 { get; set; }
        public string DataValue3 { get; set; }
        public string DataKey4 { get; set; }
        public string DataValue4 { get; set; }
        public string DataKey5 { get; set; }
        public string DataValue5 { get; set; }
        public string DataKey6 { get; set; }
        public string DataValue6 { get; set; }
        public string DataKey7 { get; set; }
        public string DataValue7 { get; set; }
        public string DataKey8 { get; set; }
        public string DataValue8 { get; set; }
        public string DataKey9 { get; set; }
        public string DataValue9 { get; set; }
        public string DataKey10 { get; set; }
        public string DataValue10 { get; set; }
        public string DataKey11 { get; set; }
        public string DataValue11 { get; set; }
        public string DataKey12 { get; set; }
        public string DataValue12 { get; set; }

        public enum EPushType
        {
            BID_REQUEST
        }
    }


}