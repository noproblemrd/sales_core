﻿namespace MobileAPI.Models
{
    public class SupplierCategoryModel
    {
        public decimal MaxPrice { get; set; }
        public decimal MediumPrice { get; set; }
        public decimal MinPrice { get; set; }
        public string Name { get; set; }
    }
}