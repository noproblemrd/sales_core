﻿namespace MobileAPI.Models
{
    public class Alert
    {
        public string Type { get; set; }
        public string Text { get; set; }
    }
}