﻿using System;

namespace MobileAPI.Models
{
    public class LeadModel
    {
        public Guid Id { get; set; }
        public int? SuppliersCount { get; set; }
        public string ExpertiseName { get; set; }
        public string ZipCode { get; set; }
        public string SupplierAvailabilityStatus { get; set; }
        public string AvailableAction { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool CanReviveIncident { get; set; }
        public string VideoUrl { get; set; }
        public string PreviewVideoImageUrl { get; set; }
        public int? VideoDuration { get; set; }
    }
}