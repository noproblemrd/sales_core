﻿using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class PhoneValidationRequest
    {
        [PhoneValidation]
        [Required]
        public string Phone { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 4)]
        public string Code { get; set; }

        [NullableRequired]
        public bool? Call { get; set; }
    }
}