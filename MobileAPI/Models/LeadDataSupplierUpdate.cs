﻿using System.ComponentModel.DataAnnotations;
using MobileAPI.Models.ValidationAttributes;

namespace MobileAPI.Models
{
    public class LeadDataSupplierUpdate
    {
        [LeadDataSupplierUpdateFieldValidation]
        public string FieldToUpdate { get; set; }

        [Required]
        public string NewValue { get; set; }

        internal class FieldsAvailable
        {
            public const string CustomerName = "CUSTOMER_NAME";
            public const string Address = "ADDRESS";
        }
    }
}