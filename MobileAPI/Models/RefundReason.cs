﻿namespace MobileAPI.Models
{
    public class RefundReason
    {
        public int Code { get; set; }
        public string Text { get; set; }
    }
}