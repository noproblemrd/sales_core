﻿using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class AppUninstalledData
    {
        [Required]
        public string Uid { get; set; }
    }
}