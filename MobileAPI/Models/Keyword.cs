﻿using System;

namespace MobileAPI.Models
{
    public class Keyword
    {
        public string Value { get; set; }
        public string CategoryName { get; set; }
        public Guid CategoryId { get; set; }
    }
}