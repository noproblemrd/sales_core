﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models.Supplier.v2
{
    public class BusinessDetails
    {
        [Required]
        public string CompanyName { get; set; }

        [Required]
        [EmailValidation]
        public string Email { get; set; }

        [Required]
        [PhoneValidation]
        public string MobileNumber { get; set; }

        [UrlValidation]
        public string WebSite { get; set; }
        public string ReferralCode { get; set; }
    }
}