﻿using System;

namespace MobileAPI.Models
{
    public class LeadAdvertiserModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public Guid LeadAccountId { get; set; }
        public double? GoogleRating { get; set; }
        public double? YelpRating { get; set; }
        public int MyLove { get; set; }
        public int AllLove { get; set; }
        public string GoogleUrl { get; set; }
        public string YelpUrl { get; set; }
    }
}