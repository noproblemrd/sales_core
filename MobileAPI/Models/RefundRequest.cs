﻿using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class RefundRequest
    {
        [Required]
        public int  ReasonCode { get; set; }

        [Required]
        public string Comment { get; set; }
    }
}