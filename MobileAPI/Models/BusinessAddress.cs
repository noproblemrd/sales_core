﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class BusinessAddress
    {
       
        [Required]
        public string businessAddress { get; set; }

        [LongitudeValidation]
        public decimal Longitude { get; set; }

        [LatitudeValidation]
        public decimal Latitude { get; set; }

    }
}