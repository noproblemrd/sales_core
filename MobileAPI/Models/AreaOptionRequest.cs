﻿namespace MobileAPI.Models
{
    public class AreaOptionRequest
    {
        [LatitudeValidation]
        public decimal? Latitude { get; set; }

        [LongitudeValidation]
        public decimal? Longitude { get; set; }
    }
}