﻿using System;
using System.Collections.Generic;

namespace MobileAPI.Models
{
    public class Tip
    {
        public string Title { get; set; }
        public string Value { get; set; }
    }
    public class Tips 
    {
        public SortedDictionary<int, Tip> tips { get; set; }
        public Guid ExpertiseId { get; set; }

        public List<Tip> GetListOfTips()
        {
            List<Tip> list = new List<Tip>();
            foreach(KeyValuePair<int, Tip> kvp in tips)
                list.Add(kvp.Value);
            return list;
        }

       
    }
    public class ExpertiseTips : List<Tips>
    {
        public Tips GetTipsObj(Guid expertiseId)
        {
            foreach(Tips t in this)
            {
                if (t.ExpertiseId == expertiseId)
                    return t;
            }
            return null;
        }

        
    }
}