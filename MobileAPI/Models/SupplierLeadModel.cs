﻿using System;
using System.Collections.Generic;

namespace MobileAPI.Models
{
    public class SupplierLeadModel
    {
        public string Address { get; private set; }        
        public bool CanRefund { get; set; }
        public SupplierCategoryModel Category { get; set; }
        public DateTime CreatedOn { get; private set; }
        public string CustomerName { get; private set; }
        public string EvaluatingText { get; set; }
        public bool IsFree { get; set; }
        public Guid LeadAccountId { get; set; }
        public string LostReason { get; set; }
        public string PhoneNumber { get; private set; }
        public string PreviewVideoImageUrl { get; private set; }
        public string Price { get; private set; }        
        public List<RecordingModel> Recordings { get; set; }
        public string Status { get; private set; }
        public int VideoDuration { get; private set; }
        public string VideoUrl { get; private set; }
        public string WinPrice { get; private set; }
        public string ZipCode { get; set; }
        public string CustomerProfileImage { get; set; }
    }
}