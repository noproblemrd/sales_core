﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class PaymentMethod
    {
        public PaymentMethod()
        {
            EncryptedCreditCardNumber = String.Empty;
            CreditCardLastFourDigits = String.Empty;
            EncryptedCvv = String.Empty;
            BillingFirstName = String.Empty;
            BillingLastName = String.Empty;
            CreditCardType = String.Empty;
            CreditCardExpirationYear = DateTime.Now.Year;
            CreditCardExpirationMonth = DateTime.Now.Month;
            BillingEmail = string.Empty;
            BillingAddress = String.Empty;
            BillingPhone = String.Empty;
            BillingCity = String.Empty;
            BillingState = String.Empty;
            BillingZip = String.Empty;
            
        }
        [Required]
        public string BillingEmail { get; set; }

        [Required]
        public string EncryptedCreditCardNumber { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 4)]
        public string CreditCardLastFourDigits { get; set; }

        [Required]
        public string EncryptedCvv { get; set; }

        [Required]
        public string BillingFirstName { get; set; }

        [Required]
        public string BillingLastName { get; set; }

        [Required]
        [CreditCardTypeValidation]
        public string CreditCardType { get; set; }

        [YearValidation]
        public int CreditCardExpirationYear { get; set; }

        [MonthValidation]
        public int CreditCardExpirationMonth { get; set; }

        [Required]
        public string BillingAddress { get; set; }

        [Required]
        public string BillingPhone { get; set; }

        [Required]
        public string BillingCity { get; set; }

        [Required]
        [StateValidation]
        public string BillingState { get; set; }

        [Required]
        [StringLength(15, MinimumLength = 5)]
        public string BillingZip { get; set; }

        public bool IsActive { get; set; }
    }
}