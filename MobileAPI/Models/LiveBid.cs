﻿using System;

namespace MobileAPI.Models
{
    public class LiveBid
    {
        private const string DateFormat = "yyyy-MM-dd HH:mm:ss";

        public LiveBid(DateTime bidTimeoutAtUtc, DateTime leadCreatedOnUtc)
        {
            this.TimeoutAtUtc = bidTimeoutAtUtc.ToString(DateFormat);
            this.TimeStampUtc = leadCreatedOnUtc.ToString(DateFormat);
        }

        public decimal MinBid { get; set; }
        public decimal MaxBid { get; set; }
        public string TimeoutAtUtc { get; private set; }
        public string Description { get; set; }
        public Guid Id { get; set; }
        public string Category { get; set; }
        public decimal DefaultBid { get; set; }
        public string TimeStampUtc { get; private set; }
        public string Address { get; set; }
        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }
    }
}