﻿namespace MobileAPI.Models
{
    public static class Constants
    {
        public static class Date
        {
            public const string DateFormat = "yyyy-MM-dd HH:mm:ss";
        }
    }
}