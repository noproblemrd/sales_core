﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class SetCoverAreaData
    {
        [Required]
        public List<CoverArea> Areas { get; set; }

        public class CoverArea
        {
            [Required]
            public string BusinessAddress { get; set; }

            [LongitudeValidation]
            public decimal? Longitude { get; set; }

            [LatitudeValidation]
            public decimal? Latitude { get; set; }

            [NullableRequired]
            public int? Order { get; set; }

            [Required]
            public string SelectedId { get; set; }
        }
    }
}