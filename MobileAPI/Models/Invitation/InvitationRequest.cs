﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileAPI.Models.Invitation
{
    public class InvitationRequest
    {
        public ClipCall.DomainModel.Entities.VideoInvitation.Request.CreateInvitationRequest.eInvitationType? invitationType { get; set; }
    }
}