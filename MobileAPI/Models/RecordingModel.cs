﻿using System;

namespace MobileAPI.Models
{
    public class RecordingModel
    {
        public DateTime CreateDate { get; private set; }
        public int Duration { get; set; }
        public string CallRecordUrl { get; set; }
    }
}