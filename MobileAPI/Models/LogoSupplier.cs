﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class LogoSupplier
    {
        [Required]
        public string ImageUrl { get; set; }
    }
}