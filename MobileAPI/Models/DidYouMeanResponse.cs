﻿using System;
using System.Collections.Generic;

namespace MobileAPI.Models
{
    public class DidYouMeanResponse
    {
        public DidYouMeanResponse()
        {
            Categories = new List<CategoryIdNamePair>();
        }

        public void AddCategory(string categoryName, Guid categoryId)
        {
            Categories.Add(
                new CategoryIdNamePair()
                {
                    CategoryId = categoryId,
                    CategoryName = categoryName
                }
                );
        }

        public List<CategoryIdNamePair> Categories { get; private set; }

        public class CategoryIdNamePair
        {
            public Guid CategoryId { get; set; }
            public string CategoryName { get; set; }
        }
    }
}