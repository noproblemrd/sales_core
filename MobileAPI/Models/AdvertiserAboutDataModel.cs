using System.Collections.Generic;

namespace MobileAPI.Models
{
    public class AdvertiserAboutDataModel
    {
        public string PhoneNumber { get; set; }
        public string FullAddress { get; set; }
        public string ContactName { get; set; }
        public List<CategoryModel> Categories { get; set; }
        public string BusinessName { get; set; }
        public string BusinessDescription { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public int? NumberOfEmployees { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}