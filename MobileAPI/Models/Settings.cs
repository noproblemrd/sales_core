﻿using System;

namespace MobileAPI.Models
{
    public class Settings
    {
        internal const string DateFormat = "yyyy-MM-dd HH:mm:ss";
        private static readonly string MinDate = DateTime.MinValue.ToString(DateFormat);

        public Settings()
        {

        }

        public Settings(bool recordMyCalls, DateTime? stopNotificationsUntil, string ccLast4Digits)
        {
            RecordMyCalls = recordMyCalls;
            if (stopNotificationsUntil.HasValue && stopNotificationsUntil.Value > DateTime.Now)
            {
                StopNotifications = true;
                StopNotificationsUntilUtc = stopNotificationsUntil.Value.ToString(DateFormat);
            }
            else
            {
                StopNotifications = false;
                StopNotificationsUntilUtc = MinDate;
            }
            CreditCardLastFourDigits = ccLast4Digits;
        }

        [NullableRequired]
        public bool? RecordMyCalls { get; set; }

      //  [NullableRequired]
        public bool? StopNotifications { get; set; }

        public string StopNotificationsUntilUtc { get; set; }

        public string CreditCardLastFourDigits { get; set; }
    }
}