﻿using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class Feedback
    {
        [Required]
        public string Header { get; set; }

        [Required]
        public string Comment { get; set; }
    }
}