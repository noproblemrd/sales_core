﻿using System.Collections.Generic;

namespace MobileAPI.Models
{
    public class CoverAreaData
    {
        public List<CoverArea> Areas { get; set; }

        public class CoverArea
        {
            public string BusinessAddress { get; set; }
            public decimal Longitude { get; set; }
            public decimal Latitude { get; set; }
            public int Order { get; set; }
            public List<AreaInList> AreasInList { get; set; }

            public class AreaInList
            {
                public bool Selected { get; set; }
                public string Name { get; set; }
                public string Id { get; set; }
                public int ListOrder { get; set; }
                public bool IsRange { get; set; }
            }
        }
    }
}