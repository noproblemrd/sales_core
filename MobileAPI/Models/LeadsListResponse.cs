﻿using System;
using System.Collections.Generic;

namespace MobileAPI.Models
{
    public class LeadsListResponse
    {
        public List<LeadEntry> Leads { get; private set; }

        public LeadsListResponse()
        {
            Leads = new List<LeadEntry>();
        }

        public class LeadEntry
        {
            //public LeadEntry(string categoryName, string status, DateTime createdOn, Guid incidentAccountId, string zipCode,
            //    string videoUrl, string previewVideoImageUrl, int videoDuration)
            //{
            //    this.CategoryName = categoryName;
            //    this.Status = status;
            //    this.CreatedOn = createdOn;      
            //    this.LeadAccountId = incidentAccountId;
            //    this.ZipCode = zipCode;
            //    this.VideoUrl = videoUrl;
            //    this.PreviewVideoImageUrl = previewVideoImageUrl;
            //    this.VideoDuration = videoDuration;
            //}

            public string CategoryName { get; set; }
            public string ZipCode { get; set; }
            public string Status { get; set; }
            public DateTime CreatedOn { get; set; }
            public Guid LeadAccountId { get; set; }
            public string VideoUrl { get; set; }
            public string PreviewVideoImageUrl { get; set; }
            public int VideoDuration { get; set; }
            public string CustomerProfileImage { get; set; }
            public string WonTitle { get; set; }
        }
    }
}