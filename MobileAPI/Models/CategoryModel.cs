﻿using System;

namespace MobileAPI.Models
{
    public class CategoryModel
    {
        [RequiredGuidValidation]
        public Guid CategoryId { get; set; }

        public string CategoryName { get; set; }
    }
}