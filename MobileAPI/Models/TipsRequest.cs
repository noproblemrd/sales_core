﻿using System;

namespace MobileAPI.Models
{
    public class TipsRequest
    {
        [RequiredGuidValidation]
        public Guid ExpertiseId { get; set; }
    }
}