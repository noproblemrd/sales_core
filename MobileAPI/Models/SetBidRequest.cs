﻿namespace MobileAPI.Models
{
    public class SetBidRequest
    {
        [SetBidStatusValidation]
        public string Status { get; set; }

        [SetBidPriceValidation]
        public decimal? Price { get; set; }

        internal class StatusOptions
        {
            public const string Accepted = "ACCEPTED";
            public const string Rejected = "REJECTED";
            public const string Missed = "MISSED";
        }
    }
}