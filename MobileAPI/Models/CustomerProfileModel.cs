﻿namespace MobileAPI.Models
{
    public class CustomerProfileModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ImageUrl { get; set; }
    }
}