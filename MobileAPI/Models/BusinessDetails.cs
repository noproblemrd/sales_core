﻿using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class BusinessDetails
    {
        [Required]
        public string CompanyName { get; set; }

        [Required]
        public string ContactName { get; set; }

  //      [Required]
        public string BusinessAddress { get; set; }

 //       [LongitudeValidation]
        public decimal? Longitude { get; set; }

 //       [LatitudeValidation]
        public decimal? Latitude { get; set; }

        [Required]
        [PhoneValidation]
        public string MobileNumber { get; set; }

        public string Email { get; set; }
        public string WebSite { get; set; }
        public string ReferralCode { get; set; }
    }
}