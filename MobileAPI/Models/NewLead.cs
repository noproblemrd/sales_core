﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class NewLead
    {
        [RequiredGuidValidation]
        public Guid CategoryId { get; set; }

        [Required]
        [StringLength(5, MinimumLength = 5)]
        public string ZipCode { get; set; }

        [Required]
        public string VideoLeadUrl { get; set; }
        
        [Required]
        public string PreviewPicUrl { get; set; }

        [Required]
        public int VideoDuration { get; set; }

        [Required]
        public string CategoryName { get; set; }

        public Guid CustomerId { get; set; }
    }
}