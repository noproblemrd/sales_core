﻿using System.Collections.Generic;

namespace MobileAPI.Models
{
    public class Faq
    {
        public List<Question> Advertiser { get { return AdvertiserFaq; } }
        public List<Question> Publisher { get { return PublisherFaq; } }

        public class Question
        {
            public string question { get; set; }
            public string Answer { get; set; }
        }

        private static readonly List<Question> AdvertiserFaq =
            new List<Question>()
            {
                new Question(){ question = "What is my name?", Answer = "Heisenberg" },
                new Question(){ question = "Are you OK?", Answer = "Yes"}
            };

        private static readonly List<Question> PublisherFaq =
           new List<Question>()
            {
                new Question(){ question = "What is my name?", Answer = "Moshe" },
                new Question(){ question = "Are you OK?", Answer = "No"}
            };
    }
}