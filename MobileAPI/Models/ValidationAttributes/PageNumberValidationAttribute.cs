﻿using ApiCommons.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class PageNumberValidationAttribute : ValidationAttribute, IQueryStringParameterValidationAttribute
    {
        private const string Error = "page_number is a number grater than or equals to zero.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (IsValueValid(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(Error);
            }
        }

        public bool IsValueValid(object value)
        {
            
            int? number = value as int?;
            if (!number.HasValue)
            {
                value = 1;
                return true;
            }
           return (number.HasValue && number.Value >= 0);
            


        }

        public string GetErrorMessage()
        {
            return Error;
        }
    }
}