﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class YearValidationAttribute : ValidationAttribute
    {
        private const string Error = "Please enter a valid 4 digit year.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (IsValueValid(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(Error);
            }
        }

        public bool IsValueValid(object value)
        {
            int? year = value as int?;
            return year.HasValue && year.Value >= DateTime.Now.Year && year.Value <= 9999;
        }
    }
}