﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace MobileAPI.Models
{
    public class EmailValidationAttribute : ValidationAttribute
    {
        private const string emailRegex = @"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$";
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success;
            string email = value as string;
            if (string.IsNullOrEmpty(email))
                return ValidationResult.Success;
            if (Regex.IsMatch(email, emailRegex))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Email not valid");
            }
        }
    }
}