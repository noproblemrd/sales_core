﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class RequiredGuidValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Guid? id = value as Guid?;
            if (id.HasValue && id != Guid.Empty)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("id field is required.");
            }
        }
    }
}