﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace MobileAPI.Models
{
    public class PhoneValidationAttribute : ValidationAttribute
    {
        private const string PhoneRegex = "^[0,2-9]\\d{9}$";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value == null)
                return ValidationResult.Success;
            string phone = value as string;
            if(string.IsNullOrEmpty(phone))
                return ValidationResult.Success;
            if (Regex.IsMatch(phone, PhoneRegex))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("phone must be either exactly 10 digits (no dashes or spaces or nothing)");
            }
        }
    }
}