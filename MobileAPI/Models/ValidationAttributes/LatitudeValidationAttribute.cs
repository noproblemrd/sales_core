﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ApiCommons.Models;

namespace MobileAPI.Models
{
    public class LatitudeValidationAttribute : ValidationAttribute, IQueryStringParameterValidationAttribute
    {
        private const string Error = "latitude is a number between -90 and 90.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (IsValueValid(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(Error);
            }
        }

        public bool IsValueValid(object value)
        {
            Decimal? latitude = value as Decimal?;
            return latitude.HasValue && latitude.Value >= -90 && latitude.Value <= 90;
        }

        public string GetErrorMessage()
        {
            return Error;
        }
    }
}