﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class SetBidStatusValidationAttribute : ValidationAttribute
    {
        private const string Error = "status must be ACCEPTED, REJECTED or MISSED.";

        protected override System.ComponentModel.DataAnnotations.ValidationResult IsValid(object value, System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            if (IsValueValid(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(Error);
            }
        }

        private bool IsValueValid(object value)
        {
            string status = value as string;
            return (!String.IsNullOrEmpty(status) 
                && 
                    (status == SetBidRequest.StatusOptions.Accepted 
                    || status == SetBidRequest.StatusOptions.Rejected 
                    || status == SetBidRequest.StatusOptions.Missed)
                );
        }
    }
}