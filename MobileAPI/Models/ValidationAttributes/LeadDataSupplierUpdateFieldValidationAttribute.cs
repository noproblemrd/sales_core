﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models.ValidationAttributes
{
    public class LeadDataSupplierUpdateFieldValidationAttribute : ValidationAttribute
    {
        private const string Error = "field_to_update must be CUSTOMER_NAME or ADDRESS";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (IsValueValid(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(Error);
            }
        }

        public bool IsValueValid(object value)
        {
            string field = value as String;
            return field == Models.LeadDataSupplierUpdate.FieldsAvailable.CustomerName
                || field == Models.LeadDataSupplierUpdate.FieldsAvailable.Address;
        }
    }
}