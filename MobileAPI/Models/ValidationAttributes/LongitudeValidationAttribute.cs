﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ApiCommons.Models;

namespace MobileAPI.Models
{
    public class LongitudeValidationAttribute : ValidationAttribute, IQueryStringParameterValidationAttribute
    {
        private const string Error = "longitude is a number between -180 and 180.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (IsValueValid(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(Error);
            }
        }

        public string GetErrorMessage()
        {
            return Error;
        }

        public bool IsValueValid(object value)
        {
            Decimal? longitude = value as Decimal?;
            return (longitude.HasValue && longitude.Value >= -180 && longitude.Value <= 180);
        }
    }
}