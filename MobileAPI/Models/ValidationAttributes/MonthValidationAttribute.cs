﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class MonthValidationAttribute : ValidationAttribute
    {
        private const string Error = "Month in invalid.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (IsValueValid(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(Error);
            }
        }

        public bool IsValueValid(object value)
        {
            int? month = value as int?;
            return month.HasValue && month.Value >= 1 && month.Value <= 12;
        }
    }
}