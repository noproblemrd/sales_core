﻿using ApiCommons.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class PageSizeValidationAttribute : ValidationAttribute, IQueryStringParameterValidationAttribute
    {
        private const string Error = "page size is a number between 1 and 40.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (IsValueValid(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(Error);
            }
        }

        public bool IsValueValid(object value)
        {
            int? size = value as int?;
            if (!size.HasValue)
            {
                value = 40;
                return true;
            }
            return (size.HasValue && size.Value > 0 && size <= 150);
        }

        public string GetErrorMessage()
        {
            return Error;
        }
    }
}