﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class StateValidationAttribute : ValidationAttribute
    {
        private const string Error = "State must be the two letter abbreviation.";

        private static readonly List<string> States = new List<string>()
        {
            "AL",
"AK",
"AZ",
"AR",
"CA",
"CO",
"CT",
"DE",
"FL",
"GA",
"HI",
"ID",
"IL",
"IN",
"IA",
"KS",
"KY",
"LA",
"ME",
"MD",
"MA",
"MI",
"MN",
"MS",
"MO",
"MT",
"NE",
"NV",
"NH",
"NJ",
"NM",
"NY",
"NC",
"ND",
"OH",
"OK",
"OR",
"PA",
"RI",
"SC",
"SD",
"TN",
"TX",
"UT",
"VT",
"VA",
"WA",
"WV",
"WI",
"WY",
"AS",
"DC",
"FM",
"GU",
"MH",
"MP",
"PW",
"PR",
"VI",
"AE",
"AA",
"AE",
"AE",
"AE",
"AP",

        };

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (IsValueValid(value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(Error);
            }
        }

        public bool IsValueValid(object value)
        {
            string state = value as String;
            return States.Contains(state);
        }
    }
}