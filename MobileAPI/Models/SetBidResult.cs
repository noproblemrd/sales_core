﻿namespace MobileAPI.Models
{
    public class SetBidResult
    {
        public class StatusOptions
        {
            public const string Ok = "OK";
            public const string Failed = "FAILED";
        }

        public string Status { get; set; }
        public string faildMessage { get; set; }
    }
}