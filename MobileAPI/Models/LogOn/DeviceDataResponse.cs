﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace MobileAPI.Models
{
     [DataContract]
    public class DeviceDataResponse
    {
          [DataMember]
        public string Token { get; set; }
    }
}