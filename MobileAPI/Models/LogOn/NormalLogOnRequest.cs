﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class NormalLogOnRequest : LogOnRequest
    {
        [Required]
        //[MinLength(6, ErrorMessage = "The field password must be a string with a minimum length of '6'.")]
        [StringLength(20, MinimumLength = 6)]
        public string Password { get; set; }   
    }
}