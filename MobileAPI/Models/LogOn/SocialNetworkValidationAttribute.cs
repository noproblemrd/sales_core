﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    internal class SocialNetworkValidationAttribute : ValidationAttribute
    {
        public const string Facebook = "FACEBOOK";
        public const string Google = "GOOGLE";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string network = (string)value;
            if (network == Facebook || network == Google)
                return ValidationResult.Success;
            return new ValidationResult("The field social_network must be either FACEBOOK or GOOGLE");
        }
    }
}