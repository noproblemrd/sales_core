﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class CustomerLogOnRequest : DeviceDataRequest
    {
        //[Required]
        [PhoneValidation]
        public string PhoneNumber { get; set; }

    }
}