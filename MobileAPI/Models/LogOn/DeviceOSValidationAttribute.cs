﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class DeviceOsValidationAttribute : ValidationAttribute
    {
        private const string Android = "ANDROID";
        private const string Ios = "IOS";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string os = (string)value;
            if (os == Android || os == Ios)
                return ValidationResult.Success;
            return new ValidationResult("The field device_os must be either ANDROID or IOS");
        }
    }
}