﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileAPI.Models
{
    public class DeviceDataRequest
    {
        [StringLength(255)]
        public string Uid { get; set; }

        [Required]
        [DeviceOsValidation]
        public string DeviceOs { get; set; }

        [StringLength(40)]
        public string DeviceName { get; set; }

        [StringLength(40)]
        public string OsVersion { get; set; }

        [StringLength(40)]
        public string NpAppVersion { get; set; }

        public bool isDebug { get; set; }
    }
}