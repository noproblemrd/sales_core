﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MobileAPI.Models
{
    public class SocialLogOnRequest : LogOnRequest
    {
        [Required]
        [SocialNetworkValidation]
        public string SocialNetwork { get; set; }

        [Required]
        public string SocialNetworkToken { get; set; }

        [Required]
        public string UserFullName { get; set; }
    }
}