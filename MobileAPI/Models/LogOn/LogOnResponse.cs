﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace MobileAPI.Models
{
    [DataContract]
    public class LogOnResponse : DeviceDataResponse
    {
        public LogOnResponse(string status)
        {
            this.Status = MessagesDict[status];
        }
        [DataMember]
        public string Status { get; private set; }
         [DataMember]
        public Guid SupplierId { get; set; }
         [DataMember]
        public string Step { get; set; }

        private static readonly Dictionary<string, string> MessagesDict = new Dictionary<string, string>(){
            {StatusCodes.Ok, StatusMessages.Ok},
            {StatusCodes.WrongEmailPasswordCombination, StatusMessages.WrongEmailPasswordCombination},
            {StatusCodes.AnotherAccountAlreadyHasThisEmail, StatusMessages.AnotherAccountAlreadyHasThisEmail},
            {StatusCodes.InactiveAccount, StatusMessages.InactiveAccount},
            {StatusCodes.SendToLegacyProcess, StatusMessages.SendToLegacyProcess}
        };

        private static class StatusMessages
        {
            public const string Ok = "OK";
            public const string WrongEmailPasswordCombination = "Oops! Your email or password are incorrect. Please try again.";
            public const string AnotherAccountAlreadyHasThisEmail = "The email you entered already exists in our records. Please login.";
            public const string InactiveAccount = "Your account has been paused. To resolve please contact us at support@noproblem.me";
            public const string SendToLegacyProcess = "OK";
        }

        private static class StatusCodes
        {
            public const string Ok = "OK";
            public const string WrongEmailPasswordCombination = "WRONG_EMAIL_PASSWORD_COMBINATION";
            public const string AnotherAccountAlreadyHasThisEmail = "ANOTHER_ACCOUNT_ALREADY_HAS_THIS_EMAIL";
            public const string InactiveAccount = "INACTIVE_ACCOUNT";
            public const string SendToLegacyProcess = "SEND_TO_LEGACY_PROCESS";
        }
    }
}