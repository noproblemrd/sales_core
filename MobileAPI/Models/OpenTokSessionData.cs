﻿namespace MobileAPI.Models
{
    public class OpenTokSessionData
    {
        public int ApiKey { get; set; }
        public string SessionId { get; set; }
        public string Token { get; set; }
    }
}