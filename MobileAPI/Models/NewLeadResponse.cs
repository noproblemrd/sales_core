﻿using System;

namespace MobileAPI.Models
{
    public class NewLeadResponse:StandardResponse
    {
        public Guid LeadId { get; set; }
    }

    public class PaymentMethodResponse : StandardResponse
    {
        public PaymentMethod PaymentMethod { get; set; }
    }
}