﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace MobileAPI.Extensibility.SingalR
{
    public class SingalRDependencyResolver : Microsoft.AspNet.SignalR.DefaultDependencyResolver
    {

         private readonly IWindsorContainer container;

         public SingalRDependencyResolver(IWindsorContainer container)
        {
            this.container = container;
        }


        

        public override object GetService(Type serviceType)
        {
            return container.Kernel.HasComponent(serviceType) ? container.Resolve(serviceType) : base.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (!container.Kernel.HasComponent(serviceType))
            {
                return base.GetServices(serviceType);
            }

            return container.ResolveAll(serviceType).Cast<object>();
        }       
    }
}