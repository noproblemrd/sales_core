﻿using System.Web.Http.ExceptionHandling;
using Castle.Core.Logging;

namespace MobileAPI.Extensibility.WebApi.Exceptions
{
    public class ClipCallExceptionLogger : ExceptionLogger
    {

        public ILogger Logger { get; set; }

        public override void Log(ExceptionLoggerContext context)
        {
            Logger.Error("error",context.Exception);
        }

        public override bool ShouldLog(ExceptionLoggerContext context)
        {
            return true;
        }
    }
}