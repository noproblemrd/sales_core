﻿using System.Net;
using System.Net.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;
using MobileAPI.Models;

namespace MobileAPI.Extensibility.WebApi.Exceptions
{
    public class ClipCallExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            var status = new StandardResponse(ok:false);
            var resposne = context.Request.CreateResponse<StandardResponse>(HttpStatusCode.InternalServerError, status);
            context.Result = new ResponseMessageResult(resposne);
        }

        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return true;
        }
    }
}