﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Controllers;
using log4net;
using MobileAPI.CoreConnectors;
using Newtonsoft.Json;

namespace MobileAPI.Extensibility.WebApi.Filters
{
  public class TokenAuthorizationFilter : AuthorizeAttribute
    {
        private const string CachePrefix = "TokenAuthorizationFilterAttribute";
        private static readonly AuthorizationConnector connector;
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        static TokenAuthorizationFilter()
        {
            connector = new AuthorizationConnector();
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!IsAuthorized(actionContext))
            {
                var requestContext = new UnauthorizedRequestContext
                {
                    Action = actionContext.ActionDescriptor.ActionName,
                    Controller = actionContext.ControllerContext.Controller.GetType().Name,
                    Headers = actionContext.Request.Headers,
                    Url = actionContext.Request.RequestUri.AbsoluteUri,
                    RouteData = actionContext.RequestContext.RouteData.Values
                };

                Logger.WarnFormat("User is not authorized. request context:  {0}", JsonConvert.SerializeObject(requestContext));
              
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            base.OnAuthorization(actionContext);
        }

        //      private bool IsAuthorized(HttpActionContext filterContext)
        protected override bool IsAuthorized(HttpActionContext filterContext)
        {
            var headers = filterContext.Request.Headers;
            IEnumerable<string> headerValues;
            if (headers.TryGetValues("nptoken", out headerValues))
            {
                string token = headerValues.FirstOrDefault();
                if (!String.IsNullOrEmpty(token))
                {
                    object idObj;
                    bool isSupplier = false;
                    if ((filterContext.Request.RequestUri.AbsoluteUri.Contains("suppliers") && (isSupplier = filterContext.RequestContext.RouteData.Values.TryGetValue("supplierId", out idObj)))
                        ||
                        //       (filterContext.Request.RequestUri.AbsoluteUri.Contains("customers") && filterContext.ActionArguments.TryGetValue("customerId", out idObj)))
                        (filterContext.Request.RequestUri.AbsoluteUri.Contains("customers") && filterContext.RequestContext.RouteData.Values.TryGetValue("customerId", out idObj)))
                    {
                        Guid id = Guid.Empty;
                        if (idObj is string)
                            Guid.TryParse((string)idObj, out id);
                        else if (idObj is Guid)
                            id = (Guid)idObj;
                        if (id != Guid.Empty)
                        {
                            var isAuthorizedResult = CacheManager.Instance.Get<CheckAuthorizationResult>(CachePrefix, token + ";" + id.ToString() + ";" + (isSupplier ? "1" : "0"), CheckAuthorization, CacheManager.ONE_HOUR);
                            return isAuthorizedResult.IsAuthorized;
                        }
                    }
                }
            }
            return false;
        }

        private CheckAuthorizationResult CheckAuthorization(string tokenAndIdBundle)
        {
            var split = tokenAndIdBundle.Split(';');
            if (split.Length != 3)
                return new CheckAuthorizationResult(false);
            string token = split[0];
            Guid id = new Guid(split[1]);
            bool isSupplier = split[2] == "1";
            bool isAuthorized = connector.CheckAuthorization(token, id, isSupplier);
            return new CheckAuthorizationResult(isAuthorized);
        }

        private class CheckAuthorizationResult
        {
            public CheckAuthorizationResult(bool isAuthorized)
            {
                this.IsAuthorized = isAuthorized;
            }

            public bool IsAuthorized { get; private set; }
        }
      public static void RemoveFromCache(Guid id, string token, bool isSupplier)
      {
          CacheManager.Instance.ClearCache(CachePrefix, GetKey(id, token, isSupplier));
      }
      private static string GetKey(Guid id, string token, bool isSupplier)
      {
          return token + ";" + id.ToString() + ";" + (isSupplier ? "1" : "0");
      }
    }


    public class UnauthorizedRequestContext
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public HttpRequestHeaders Headers { get; set; }
        public string Url { get; set; }
        public IDictionary<string, object> RouteData { get; set; }
    }
}