﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Castle.Core.Logging;
using MobileAPI.Logging;
using MobileAPI.Models;
using Newtonsoft.Json;

namespace MobileAPI.Extensibility.WebApi.Filters
{
    public class LoggingFilter : ActionFilterAttribute
    {
        private readonly ILoggerFactory factory;
        private readonly LoggableSelector selector;

        private const string StopwatchKey = "StopwatchFilter.Value";

        public LoggingFilter(ILoggerFactory factory, LoggableSelector selector)

        {
            this.factory = factory;
            this.selector = selector;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var requestEntry = new RequestLogEntry
            {
                Arguments = actionContext.ActionArguments,
                Method = actionContext.Request.Method,
                Url = actionContext.Request.RequestUri.PathAndQuery,
                Token = actionContext.Request.Headers.Contains("nptoken") ? actionContext.Request.Headers.GetValues("nptoken").FirstOrDefault() : null
            };

            actionContext.Request.Properties[StopwatchKey] = Stopwatch.StartNew();


            ILogger logger = factory.Create(actionContext.ControllerContext.Controller.GetType());
            logger.DebugFormat("OnActionExecuting:{0}, context:{1}", actionContext.ActionDescriptor.ActionName,
                JsonConvert.SerializeObject(requestEntry));
            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            ILogger logger = factory.Create(actionExecutedContext.ActionContext.ControllerContext.Controller.GetType());
            Stopwatch stopwatch = (Stopwatch)actionExecutedContext.Request.Properties[StopwatchKey];
            if (actionExecutedContext.Response == null)
            {
                logger.DebugFormat("OnActionExecuted:{0}, response is null. duration: {1}", actionExecutedContext.ActionContext.ActionDescriptor.ActionName, stopwatch.Elapsed);
                return;
            }
            var objectContent = actionExecutedContext.Response.Content as ObjectContent;
            if (objectContent == null || objectContent.Value == null)
            {
                logger.DebugFormat("OnActionExecuted:{0}, response content is null. duration: {1}", actionExecutedContext.ActionContext.ActionDescriptor.ActionName, stopwatch.Elapsed);
                return;
            }

            var objectValue = objectContent.Value;
            ILoggable loggable = selector.SelectLoggable(objectValue);
            string response = loggable != null ? loggable.Description(objectValue) : JsonConvert.SerializeObject(objectContent.Value);
            logger.DebugFormat("OnActionExecuted:{0}, response:{1}. duration: {2}", actionExecutedContext.ActionContext.ActionDescriptor.ActionName, response, stopwatch.Elapsed);
            base.OnActionExecuted(actionExecutedContext);
        }
    }

    public class RequestLogEntry
    {
        public string Url { get; set; }
        public HttpMethod Method { get; set; }
        public string Token { get; set; }
        public Dictionary<string, object> Arguments { get; set; }
    }
}