using System;
using System.Linq;
using Castle.Core.Logging;
using Castle.DynamicProxy;
using Castle.Windsor;
using FluentValidation;
using FluentValidation.Results;

namespace MobileAPI.Extensibility.Windsor.Interceptors
{
    public class ValidationInterceptor : IInterceptor
    {
        private readonly IWindsorContainer container;

        public ValidationInterceptor(IWindsorContainer container)
        {
            this.container = container;
        }

        public void Intercept(IInvocation invocation)
        {
            object input = invocation.Arguments.FirstOrDefault();
            if (input == null)
            {
                invocation.Proceed();
                return;
            }

            Type genericType = typeof (IValidator<>).MakeGenericType(input.GetType());
            if(container.Kernel.HasComponent(genericType) == false)
            {
                invocation.Proceed();
                return;
            }

            var validator = container.Resolve(genericType) as IValidator;
            if (validator == null)
            {
                invocation.Proceed();
                return;
            }

            ValidationResult validationResult = validator.Validate(input);
            if (validationResult.IsValid)
            {
                invocation.Proceed();
                return;
            }

           
            invocation.Proceed();
        }
    }

    public class ExceptionInterceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}