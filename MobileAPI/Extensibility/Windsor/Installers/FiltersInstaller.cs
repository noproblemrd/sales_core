﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MobileAPI.Extensibility.WebApi.Exceptions;
using MobileAPI.Extensibility.WebApi.Filters;

namespace MobileAPI.Extensibility.Windsor.Installers
{
    public class FiltersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<LoggingFilter>());
            container.Register(Component.For<ClipCallExceptionHandler>());
            container.Register(Component.For<ClipCallExceptionLogger>());
            
                

        }
    }
}