﻿using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using log4net.Config;
using MobileAPI.Logging;

namespace MobileAPI.Extensibility.Windsor.Installers
{
    [assembly: XmlConfigurator(Watch = true)]
    public class LoggerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            XmlConfigurator.Configure();
            container.AddFacility<LoggingFacility>(f => f.UseLog4Net());

            container.Register(Component.For<LoggableSelector>().UsingFactoryMethod(
                (kernel, creationContext) => new LoggableSelector(container)              
                ));

            container.Register(Classes.FromThisAssembly()
               .BasedOn(typeof(AbstractLoggable<>))
               .WithService.AllInterfaces()           
               .LifestyleTransient());
        }
    }
}