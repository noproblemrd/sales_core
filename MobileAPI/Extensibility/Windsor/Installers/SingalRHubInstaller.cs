﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.AspNet.SignalR;

namespace MobileAPI.Extensibility.Windsor.Installers
{


    public class SingalRHubInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container,
            IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                .BasedOn<Hub>()
                .LifestyleSingleton());
        }
    }
}