﻿using System.Web.Http;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ClipCall.ServiceModel.Abstract.Services;
using MobileAPI.Proxy;

namespace MobileAPI.Extensibility.Windsor.Installers
{
    public class ApiControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container,
            IConfigurationStore store)
        {

 

            container.Register(Classes.FromThisAssembly()
                .BasedOn<ApiController>()
                .LifestylePerWebRequest());

            container.Register(Component.For<MobileAPI.CoreConnectors.AmazonS3Connector>());
            container.Register(Component.For<IVideoChatService>().UsingFactoryMethod((kernel,creationContext) =>
            {
                var proxy = new ServiceWrapper<IVideoChatService>();
                return proxy.Channel;
            }).LifestylePerWebRequest());
        }
    }
}