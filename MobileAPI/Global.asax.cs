﻿using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Http;
using Appccelerate.Bootstrapper;
using log4net.Config;
using MobileAPI.Bootstrapping;
using MobileAPI.Bootstrapping.Extensions;

namespace MobileAPI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : HttpApplication
    {

        private static DefaultBootstrapper<IClipCallBootstrappingExtension> bootstrapper;

        protected void Application_Start()
        {

            FileInfo log4NetConfigFile = new FileInfo(Server.MapPath(ConfigurationManager.AppSettings.Get("log4net.Config")));
            XmlConfigurator.ConfigureAndWatch(log4NetConfigFile);

            bootstrapper = new DefaultBootstrapper<IClipCallBootstrappingExtension>();
             var strategy = new ClipCallBootstrappingStrategy();
            bootstrapper.Initialize(strategy); 
            bootstrapper.AddExtension(new AutoMapperBootstrappingExtension());
            bootstrapper.AddExtension(new ConnectorsBootstrappingExtension());
            bootstrapper.AddExtension(new ValidationBootstrappingExtension());
            bootstrapper.AddExtension(new WebApiBootstrappingExtension(GlobalConfiguration.Configuration));
            bootstrapper.AddExtension(new MvcBootstrappingExtension());
            bootstrapper.Run();                      
        }

        protected void Application_End()
        {
            bootstrapper.Shutdown();    
        }
    }
}