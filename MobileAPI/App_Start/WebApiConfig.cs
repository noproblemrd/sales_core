﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Castle.Windsor;

namespace MobileAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "NamedMethodApi",
                routeTemplate: "api/{controller}/{action}"
            );

            // config.Routes.MapHttpRoute(
            //    name: "DefaultApi2",
            //    routeTemplate: "api/advertisers/{supplierId}/{controller}/{action}"
            //);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/advertisers/{supplierId}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

           

            config.Routes.MapHttpRoute(
               name: "DefaultCustomersApi",
               routeTemplate: "api/customers/{customerId}/{controller}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );


            
        }       
    }
}
