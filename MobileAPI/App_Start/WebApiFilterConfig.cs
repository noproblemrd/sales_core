﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http.Filters;
using MobileAPI.Extensibility.WebApi.Filters;

namespace MobileAPI.App_Start
{
    public class WebApiFilterConfig
    {
        public static void RegisterHttpFilters(HttpFilterCollection filters)
        {
            filters.Add(new TokenAuthorizationFilter());

        }
    }

    
}