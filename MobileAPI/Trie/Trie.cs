﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MobileAPI.Trie
{
    /// <summary>
    /// Trie data structure which maps strings to generic values.
    /// </summary>
    /// <typeparam name="V">Type of values to map to. Must be referencable</typeparam>
    public class Trie<V> where V : class
    {

        private TrieNode<V> root;

        /// <summary>
        /// Matcher object for matching prefixes of strings to the strings stored in this trie.
        /// </summary>
        public IPrefixMatcher<V> GetNewMatcher()
        {
            return new PrefixMatcher<V>(this.root);
        }

        /// <summary>
        /// Create an empty trie with an empty root node.
        /// </summary>
        public Trie()
        {
            this.root = new TrieNode<V>(null, '\0');
        }

        /// <summary>
        /// Put a new key value pair, overwriting the existing value if the given key is already in use.
        /// </summary>
        /// <param name="key">Key to search for value by.</param>
        /// <param name="value">Value associated with key.</param>
        public void Put(string key, V value)
        {
            TrieNode<V> node = root;
            foreach (char c in key)
            {
                node = node.AddChild(c);
            }
            node.Value = value;
        }
    }
}