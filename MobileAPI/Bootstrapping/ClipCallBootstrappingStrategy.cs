﻿using System.Configuration;
using System.IO;
using Appccelerate.Bootstrapper;
using Appccelerate.Bootstrapper.Syntax;
using Castle.Windsor;
using log4net.Config;

namespace MobileAPI.Bootstrapping
{
    public class ClipCallBootstrappingStrategy : AbstractStrategy<IClipCallBootstrappingExtension>
    {
        protected override void DefineRunSyntax(ISyntaxBuilder<IClipCallBootstrappingExtension> builder)
        {

            IWindsorContainer container = new WindsorContainer();

            builder.Execute(extension => extension.Starting());
            builder.Execute(extension => extension.Start());

            builder.Execute(extension => extension.ContainerInitializing(container));
            builder.Execute(extension => extension.ContainerInitialized(container));
            builder.Execute(extension => extension.Ready());
        }


        protected override void DefineShutdownSyntax(ISyntaxBuilder<IClipCallBootstrappingExtension> builder)
        {
            builder.Execute(extension => extension.Stop());
        }
    }
}