﻿using Appccelerate.Bootstrapper;
using Castle.Windsor;

namespace MobileAPI.Bootstrapping
{
    public interface IClipCallBootstrappingExtension:IExtension
    {
        void Starting();
        void Start();
        void ContainerInitializing(IWindsorContainer container);
        void ContainerInitialized(IWindsorContainer container);
        void Ready();
        void Restart();
        void Stop();

    }
}
