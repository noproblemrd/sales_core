﻿using Appccelerate.Formatters;
using Castle.Windsor;
using log4net;

namespace MobileAPI.Bootstrapping
{
    public abstract class BootstrappingExtensionBase : IClipCallBootstrappingExtension
    {
        protected WindsorContainer Container { get; private set; }
        protected ILog Logger { get; private set; }
        public abstract string Describe();
        public string Name { get { return this.GetType().FullNameToString(); }}

        public void Starting()
        {
            this.Logger = LogManager.GetLogger(GetType());
            this.Logger.Debug("Starting " + this.Describe());
        }

        public virtual void Start(){}
        public virtual void ContainerInitializing(IWindsorContainer container) { }
        public void ContainerInitialized(IWindsorContainer container)
        {
            this.Container = Container;
            this.ContainerInitializedCore(container);
            this.Logger.Debug("container initialized for " + this.Describe());
        }

        public virtual void Ready()
        {
            this.Logger.Debug(this.Describe() + " is ready...");
        }

        public virtual void Restart()
        {
            this.Logger.Debug(this.Describe() + " is restarting...");
        }

        protected virtual void ContainerInitializedCore(IWindsorContainer container) { }

        public virtual void Stop()
        {
            this.Logger.Debug(this.Describe() + " is stopping...");
        }
    }
}