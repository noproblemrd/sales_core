﻿using AutoMapper;
using MobileAPI.AutoMapping.Profiles;

namespace MobileAPI.Bootstrapping.Extensions
{
    public class AutoMapperBootstrappingExtension:BootstrappingExtensionBase
    {
        public override string Describe()
        {
            return "Auto Mapper bootstrapping extension";
        }

        public override void Start()
        {
            this.Logger.Debug("About to register auto mapping profiles....");

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<ConsumerProfile>();
                cfg.AddProfile<AdvertiserProfile>();
            });
        }
    }
}