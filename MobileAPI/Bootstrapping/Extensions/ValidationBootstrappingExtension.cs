using Castle.MicroKernel.Registration;
using Castle.Windsor;
using FluentValidation;
using MobileAPI.Extensibility.Windsor.Interceptors;
using MobileAPI.Validation;

namespace MobileAPI.Bootstrapping.Extensions
{
    public class ValidationBootstrappingExtension : BootstrappingExtensionBase
    {
        public override string Describe()
        {
            return "Validation bootstrapping extension";
        }

        public override void ContainerInitializing(IWindsorContainer container)
        {
            var interceptor = new ValidationInterceptor(container);
            container.Register(Component.For<ValidationInterceptor>().Instance(interceptor));

            container.Register(Classes.FromAssemblyContaining<NewLeadVadliator>()
                .BasedOn(typeof(AbstractValidator<>))
                .WithService.AllInterfaces()
                .Configure(c => c.LifestyleSingleton()));
        }
    }
}