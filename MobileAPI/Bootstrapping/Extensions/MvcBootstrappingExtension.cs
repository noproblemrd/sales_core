﻿using System.Web.Mvc;

namespace MobileAPI.Bootstrapping.Extensions
{
    public class MvcBootstrappingExtension : BootstrappingExtensionBase
    {
        public override string Describe()
        {
            return "Asp Mvc bootstrapping extension";
        }

        public override void Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }
    }
}