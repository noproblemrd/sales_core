﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Dispatcher;
using System.Web.Http.ExceptionHandling;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Installer;
using MobileAPI.App_Start;
using MobileAPI.Extensibility.WebApi.DependencyInjection;
using MobileAPI.Extensibility.WebApi.Exceptions;
using MobileAPI.Extensibility.WebApi.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WebGrease.Css.Extensions;

namespace MobileAPI.Bootstrapping.Extensions
{
    public class WebApiBootstrappingExtension : BootstrappingExtensionBase
    {
        private readonly HttpConfiguration config;

        public WebApiBootstrappingExtension(HttpConfiguration config)
        {
            this.config = config;
        }

        public override string Describe()
        {
            return "Web Api bootstrapping extension";
        }

        public override void Start()
        {
            // Web API routes
            config.EnableCors();

            var corsAttr = new EnableCorsAttribute(origins: "*", headers: "*", methods: "*");
            config.EnableCors(corsAttr);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "NamedMethodApi",
                routeTemplate: "api/{controller}/{action}"
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/suppliers/{supplierId}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
               name: "DefaultCustomersApi",
               routeTemplate: "api/customers/{customerId}/{controller}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                DateFormatString = "yyyy-MM-dd H:mm:ss"
            };

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.DateFormatString = "yyyy-MM-dd H:mm:ss";

          
            WebApiFilterConfig.RegisterHttpFilters(GlobalConfiguration.Configuration.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            
        }

        protected override void ContainerInitializedCore(IWindsorContainer container)
        {
            container.Install(FromAssembly.This());
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel, true));
            var dependencyResolver = new WindsorDependencyResolver(container);
            config.DependencyResolver = dependencyResolver;

            config.Services.Replace(typeof(IHttpControllerActivator),
                new WindsorCompositionRoot(container));

            var filter = container.Resolve<LoggingFilter>();
            config.Filters.Add(filter);

            var clipCallExceptionLogger = container.Resolve<ClipCallExceptionLogger>();
            var clipCallExceptionHandler = container.Resolve<ClipCallExceptionHandler>();

            config.Services.Add(typeof(IExceptionLogger), clipCallExceptionLogger);
            config.Services.Replace(typeof(IExceptionHandler), clipCallExceptionHandler);
        }

        public override void Ready()
        {
            GlobalConfiguration.Configuration.EnsureInitialized();
            var builder = new StringBuilder();
            config.Routes.ForEach(r => builder.AppendLine(r.RouteTemplate));

            this.Logger.DebugFormat(this.Describe() + " registered the following routes: {0}", builder);
        }
    }
}