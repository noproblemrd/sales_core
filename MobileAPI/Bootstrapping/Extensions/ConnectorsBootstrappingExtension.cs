﻿using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using MobileAPI.CoreConnectors;
using MobileAPI.Extensibility.Windsor.Interceptors;

namespace MobileAPI.Bootstrapping.Extensions
{
    public class ConnectorsBootstrappingExtension:BootstrappingExtensionBase
    {
        public override string Describe()
        {
            return "Connectors bootstrapping extension";
        }

        public override void ContainerInitializing(IWindsorContainer container)
        {
            container.Register(Component.For<CustomerLeadsConnector>()
                .Interceptors(InterceptorReference.ForType <ValidationInterceptor>()).Last);

            container.Register(Component.For<DashboardConnector>()
              .Interceptors(InterceptorReference.ForType<ValidationInterceptor>()).Last);


        }
    }
}